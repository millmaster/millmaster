{********************************************************************
TADVSTRINGGRID component
for Delphi 1.0,2.0,3.0,4.0,5.0,C++Builder 1.0,3.0,4.0
version 1.86 - rel. November 22, 1999

written by TMS Software
           copyright � 1996-1999
           Email : info@tmssoftware.com
           Web : http://www.tmssoftware.com

The source code is given as is. The author is not responsible
for any possible damage done due to the use of this code.
The component can be freely used in any application. The complete
source code remains property of the author and may not be distributed,
published, given or sold in any form as such. No parts of the source
code can be included in any other component or application without
written authorization of the author.


Achtung:  - Vers. 1.90 hat Fehler.

12.01.2001 SDO  : - 'DB init error' mit TMMSetupModul und
                    TAdvStringGrid.ColCount / RowCount <> Default-Wert

********************************************************************}

{$S-,I-}

{$IFDEF VER80}
 {$DEFINE DELPHI1}
 {$DEFINE ISDELPHI}
{$ENDIF}

{$IFDEF VER90}
 {$DEFINE DELPHI2}
 {$DEFINE DELPHI2_LVL}
 {$DEFINE ISDELPHI}
{$ENDIF}

{$IFDEF VER93}
 {$DEFINE BCB1}
 {$DEFINE DELPHI2_LVL}
 {$DEFINE ISBCB}
{$ENDIF}

{$IFDEF VER100}
 {$DEFINE DELPHI3}
 {$DEFINE DELPHI3_LVL}
 {$DEFINE DELPHI3_4_ONLY}
 {$DEFINE ISDELPHI}
{$ENDIF}
                 
{$IFDEF VER110}
 {$DEFINE BCB3}
 {$DEFINE DELPHI3_LVL}
 {$DEFINE ISBCB}
{$ENDIF}

{$IFDEF VER120}
 {$DEFINE DELPHI4}
 {$DEFINE DELPHI3_LVL}
 {$DEFINE DELPHI4_LVL}
 {$DEFINE DELPHI3_4_ONLY}
 {$DEFINE ISDELPHI}
{$ENDIF}

{$IFDEF VER125}
 {$DEFINE DELPHI4}
 {$DEFINE DELPHI3_LVL}
 {$DEFINE DELPHI4_LVL}
{$ENDIF}

{$IFDEF VER130}
 {$DEFINE DELPHI5}
 {$DEFINE DELPHI3_LVL}
 {$DEFINE DELPHI4_LVL}
 {$DEFINE DELPHI3_4_ONLY}
 {$DEFINE ISDELPHI}
{$ENDIF}

{$IFNDEF DELPHI1}
 {$DEFINE EDITCONTROLS}
 {$DEFINE noSPREADSHEET}
{$ENDIF}

{$DEFINE noDEBUG}
{$DEFINE noFREEWARE}
{$DEFINE noDLLUSAGE}
{$DEFINE SHOWHINT}

{$IFDEF VER110}
 {$ObjExportAll On}
{$ENDIF}

{$IFDEF VER125}
 {$ObjExportAll On}
{$ENDIF}

unit AdvGrid;

interface

uses
  {$IFDEF DELPHI1}
  Wintypes, winprocs,
  {$ELSE}
  Windows,
  {$ENDIF}
  Graphics, SysUtils, Messages, Classes, Controls, Grids, ClipBrd,
  Dialogs, Printers, Forms, stdctrls, buttons, advutil, extctrls, inifiles
  {$IFDEF EDITCONTROLS} ,advspin,asgedit,comctrls, advobj, advcombo {$ENDIF}
  {$IFDEF WIN32} , richedit {$ENDIF}
  {$IFDEF BCB1} , commctrl, Registry, oleauto, OleCtnrs, shellapi {$ENDIF}
  {$IFDEF DELPHI2} , commctrl, Registry ,OleAuto, OleCtnrs, shellapi {$ENDIF}
  {$IFDEF DELPHI3_LVL} , commctrl, Registry ,ComObj, OleCtnrs, shellapi, winspool {$ENDIF}
  {$IFDEF DELPHI4_LVL} , imglist {$ENDIF}
  {$IFDEF DELPHI3_4_ONLY} ,activex,ddintf {$ENDIF}
  ;
  {$IFDEF WIN32}
  {$R INTELLI.RES}
  {$ENDIF}

const
  maxcolumns = 256;
  LINEFEED = #13;
  Alignments: array[TAlignment] of Word = (DT_LEFT,DT_RIGHT,DT_CENTER);

type
  TAdvStringGrid = class;

  pboolarray = ^tboolarray;
  tboolarray = array[0..MaxColumns] of boolean;
  twidtharray = array[0..MaxColumns] of smallint;

  EAdvGridError = class(Exception);

  {$IFDEF EDITCONTROLS}
  TEditorType = (edNormal,edSpinEdit,edComboEdit,edComboList,edEditBtn,edCheckBox,edDateEdit,edDateEditUpDown,
                 edTimeEdit,edButton,edDataCheckBox,edNumeric,edFloat,edCapital,edMixedCase,edPassword,edUnitEditBtn,edLowerCase,edUpperCase);
  TGetEditorTypeEvent = procedure(Sender:TObject;aCol,aRow:integer; var aEditor:TEditorType) of object;
  TEllipsClickEvent = procedure(Sender:TObject;aCol,aRow:integer; var S:string) of object;

  TCheckBoxClickEvent = procedure(Sender:TObject;aCol,aRow:integer;state:boolean) of object;
  TRadioClickEvent = procedure(Sender:TObject;aCol,aRow,aIdx:integer) of object;
  TComboChangeEvent = procedure(Sender:TObject;aCol,aRow,aItemIndex:integer;aSelection:string) of object;
  TSpinClickEvent = procedure(Sender:TObject;aCol,aRow,aValue:integer;updown:boolean) of object;
  {$ENDIF}

  TScrollHintType = (shNone,shVertical,shHorizontal,shBoth);

  TSortStyle = (ssAutomatic, ssAlphabetic, ssNumeric, ssDate, ssAlphaNoCase, ssAlphaCase,
                ssShortDateEU, ssShortDateUS, ssCustom, ssFinancial, ssAnsiAlphaCase, ssAnsiAlphaNoCase, ssRaw, ssHTML);

  TPrintPosition = (ppNone,ppTopLeft,ppTopRight,ppTopCenter,ppBottomLeft,ppBottomRight,ppBottomCenter);

  TPrintBorders = (pbNoborder,pbSingle,pbDouble,pbVertical,pbHorizontal,pbAround,pbAroundVertical,pbAroundHorizontal,pbCustom);

  TCellBorder = (cbTop,cbLeft,cbRight,cbBottom);

  TCellBorders = set of TCellBorder;

  TPrintMethod = (prPreview,prPrint,prCalcPrint,prCalcPreview);

  TSortDirection = (sdAscending,sdDescending);

  TAdvanceDirection = (adLeftRight,adTopBottom);

  TIntelliPan = (ipVertical,ipHorizontal,ipBoth);

  TInsertPosition = (pInsertBefore,pInsertAfter);

  TScrollType = (ssNormal,ssFlat,ssEncarta);

  TVAlignment = (vtaCenter,vtaTop,vtaBottom);

  TAutoInsertRowEvent = procedure(Sender:TObject; ARow:longint) of object;
  TAutoInsertColEvent = procedure(Sender:TObject; ACol:longint) of object;

  TAutoDeleteRowEvent = procedure(Sender:TObject; aRow:longint) of object;

  TClickSortEvent = procedure(Sender:TObject; aCol:longint) of object;

  TCanSortEvent = procedure(Sender:TObject; aCol:longint; var dosort:boolean) of object;

  TNodeClickEvent = procedure(Sender:TObject; arow,arowreal:integer) of object;

  TCustomCompareEvent = procedure(Sender:TObject; str1,str2:string;var res:integer) of object;

  TRawCompareEvent = procedure(Sender:TObject; col,row1,row2:longint;var res:integer) of object;

  TGridFormatEvent = procedure (Sender : TObject; ACol:longint;var AStyle:TSortStyle; var aPrefix,aSuffix:string) of object;

  TGridColorEvent = procedure (Sender: TObject; ARow, ACol: Longint;
                               AState: TGridDrawState;
                               ABrush: TBrush; AFont: TFont ) of object;

  TGridBorderEvent = procedure (Sender: TObject; ARow, ACol: Longint; APen:TPen;var borders:TCellBorders) of object;

  TGridAlignEvent = procedure (Sender: TObject; ARow, ACol: Longint;
                               var AAlignment: TAlignment) of object;

  TGridHintEvent = procedure (Sender:TObject; Arow, Acol:longint;
                              var hintstr:string) of object;

  TRowChangingEvent = procedure(Sender:TObject; OldRow, NewRow: longint; var Allow:boolean) of object;
  TColChangingEvent = procedure(Sender:TObject; OldCol, NewCol: longint; var Allow:boolean) of object;
  TCellChangingEvent = procedure(Sender:TObject; OldRow,OldCol,NewRow,NewCol: longint; var Allow:boolean) of object;

  TScrollHintEvent = procedure (Sender:TObject; Arow:longint;var hintstr:string) of object;

  TGridPrintPageEvent = procedure (Sender:TObject; Canvas:tCanvas; pagenr,pagexsize,pageysize:integer) of object;

  TGridPrintStartEvent = procedure (Sender:TObject; NrOfPages:integer;var FromPage,ToPage:integer) of object;

  TGridPrintNewPageEvent = procedure (Sender:TObject; aRow:integer; var newpage:boolean) of object;

  TGridPrintColumnWidthEvent = procedure (Sender:TObject; aCol:integer; var width:integer) of object;
  TGridPrintRowHeightEvent = procedure (Sender:TObject; aRow:integer; var height:integer) of object;

  TOnResizeEvent = procedure (Sender:TObject) of object;

  {$IFDEF DELPHI4_LVL}
  TColumnSizeEvent = procedure (Sender:TObject; aCol:integer; var allow:boolean) of object;
  {$ENDIF}

  TEndColumnSizeEvent = procedure (Sender:TObject; aCol:integer) of object;

  TClickCellEvent = procedure (Sender:TObject;Arow,Acol:longint) of object;

  TDblClickCellEvent = procedure (Sender:TObject;Arow,Acol:longint) of object;

  TCanEditCellEvent = procedure (Sender:TObject;Arow,Acol:longint;var canedit:boolean) of object;

  TIsFixedCellEvent = procedure (Sender:TObject;Arow,Acol:longint;var isfixed:boolean) of object;
  TIsPasswordCellEvent = procedure (Sender:TObject;Arow,Acol:longint;var ispassword:boolean) of object;

  TAnchorClickEvent = procedure(Sender:TObject;Arow,Acol:integer; anchor:string; var AutoHandle:boolean) of object;

  TCellValidateEvent = procedure(Sender: TObject; Col, Row: Longint;
                       var Value: String; var Valid: Boolean) of object;

  TDoFitToPageEvent = procedure(Sender:TObject;var scalefactor:double;var allow:boolean) of object;

  TFindParameters = (fnMatchCase,fnMatchFull,fnMatchRegular,fnDirectionLeftRight,
                     fnFindInCurrentRow,fnFindInCurrentCol,fnIncludeFixed,fnAutoGoto);

  TCellHAlign = (haLeft,haRight,haCenter,haBeforeText,haAfterText);
  TCellVAlign = (vaTop,vaBottom,vaCenter,vaUnderText,vaAboveText);
  TCellType = (ctBitmap,ctIcon,ctNone,ctImageList,ctCheckBox,ctDataCheckBox,ctRotated,ctDataImage,ctNode,ctRadio,ctEmpty,ctImages,ctPicture,ctValue);

  TFitToPage = (fpNever,fpGrow,fpShrink,fpAlways,fpCustom);

  TFindParams = set of TFindParameters;

  TCellGraphic = class(Tobject)
                   CellType:TCellType;
                   CellBitmap:TBitmap;
                   CellIcon:tIcon;
                   CellVAlign:TCellVAlign;
                   CellHAlign:TCellHAlign;
                   CellIndex:integer;
                   CellTransparent:boolean;
                   CellBoolean:boolean;
                   CellAngle:smallint;
                   CellValue:double;
                   CellObject:tobject;
                   CellData:integer;
                 public
                   constructor Create;
                   destructor Destroy; override;
                   procedure SetBitmap(abmp:tbitmap;transparent:boolean;hal:tCellHAlign;val:tCellVAlign);
                   procedure SetPicture(apicture:tpicture;transparent:boolean;hal:tCellHAlign;val:tCellVAlign);
                   procedure SetImageIdx(idx:integer;hal:tCellHAlign;val:tCellVAlign);
                   procedure SetDataImage(idx:integer;hal:tCellHAlign;val:tCellVAlign);
                   procedure SetMultiImage(col,row,dir:integer;hal:tCellHAlign;val:tCellVAlign;notifier:TImageChangeEvent);
                   procedure SetIcon(aicon:ticon;hal:tCellHAlign;val:tCellVAlign);
                   procedure SetCheckBox(value,data:boolean;hal:tCellHAlign;val:tCellVAlign);
                   procedure SetAngle(aAngle:smallint);
                 end;


  {$IFNDEF WIN32}
  TRegIniFile = class(TIniFile);
  {$ENDIF}

  TNodeType = (cnFlat,cn3D,cnGlyph);

  TCellNode = class(TPersistent)
  private
   fColor:tColor;
   fNodeType:TNodeType;
   fNodeColor:tColor;
   fExpandGlyph:TBitmap;
   fContractGlyph:TBitmap;
   fOwner:TAdvStringGrid;
   procedure SetExpandGlyph(value:TBitmap);
   procedure SetContractGlyph(value:TBitmap);
   procedure SetNodeType(value:TNodeType);
  public
   constructor Create(aOwner:TAdvStringGrid);
   destructor Destroy; override;
  published
   property Color:tcolor read fColor write fColor;
   property NodeType:TNodeType read fNodeType write SetNodeType;
   property NodeColor:tcolor read fNodeColor write fNodeColor;
   property ExpandGlyph:TBitmap read fExpandGlyph write SetExpandGlyph;
   property ContractGlyph:TBitmap read fContractGlyph write SetContractGlyph;
  end;

  TSizeWhileTyping = class(TPersistent)
  private
    FHeight: boolean;
    FWidth : boolean;
  public
    constructor Create;
    destructor Destroy; override;
  published
    property Height:boolean read FHeight write FHeight;
    property Width:boolean read FWidth write FWidth;
  end;

  TMouseActions = class(TPersistent)
  private
    FColSelect:boolean;
    FRowSelect:boolean;
    FAllSelect:boolean;
    FDirectEdit:boolean;
    FDisjunctRowSelect:boolean;
    FAllColumnSize:boolean;
    {$IFDEF WIN32}
    FCaretPositioning:boolean;
    {$ENDIF}
  public
    constructor Create;
    destructor Destroy; override;
  published
    property AllSelect:boolean read FAllSelect write FAllSelect;
    property ColSelect:boolean read FColSelect write FColSelect;
    property RowSelect:boolean read FRowSelect write FRowSelect;
    property DirectEdit:boolean read FDirectEdit write FDirectEdit;
    property DisjunctRowSelect:boolean read FDisjunctRowSelect write FDisjunctRowSelect;
    property AllColumnSize:boolean read fAllColumnSize write fAllColumnSize;
    {$IFDEF WIN32}
    property CaretPositioning:boolean read FCaretPositioning write FCaretPositioning;
    {$ENDIF}
  end;

  TColumnSizeLocation = (clRegistry,clInifile);

  TColumnSize = class(TPersistent)
  private
    FSave: boolean;
    FKey : string;
    FSection : string;
    FStretch:boolean;
    {$IFDEF DELPHI4_LVL}
    FLocation: TColumnSizeLocation;
    {$ENDIF}
    Owner:TComponent;
    procedure SetStretch(value:boolean);
  public
    constructor Create(aOwner:TComponent);
    destructor Destroy; override;
  published
    property Save:boolean read FSave write FSave;
    property Key:string read FKey write FKey;
    property Section:string read FSection write FSection;
    property Stretch:boolean read FStretch write SetStretch;
    {$IFDEF DELPHI4_LVL}
    property Location:TColumnSizeLocation read fLocation write fLocation;
    {$ENDIF}
  end;

  TNavigation = class(TPersistent)
  private
    FAllowInsertRow:boolean;
    FAllowDeleteRow:boolean;
    FAdvanceOnEnter:boolean;
    FAdvanceInsert:boolean;
    FAutoGotoWhenSorted:boolean;
    FAutoGotoIncremental:boolean;
    FAutoComboDropSize:boolean;
    FAllowClipboardShortcuts:boolean;
    FAllowRTFClipboard:boolean;
    FAllowSmartClipboard:boolean;
    FAdvanceDirection:TAdvanceDirection;
    FAdvanceAuto:boolean;
    FCursorWalkEditor:boolean;
    FMoveRowOnSort:boolean;
    {$IFDEF WIN32}
    FImproveMaskSel:boolean;
    FAlwaysEdit:boolean;
    {$ENDIF}
    FInsertPosition:TInsertPosition;
    procedure SetAutoGoto(avalue:boolean);
  public
    constructor Create;
    destructor Destroy; override;
  published
    property AllowInsertRow:boolean read FAllowInsertRow write FAllowInsertRow;
    property AllowDeleteRow:boolean read FAllowDeleteRow write FAllowDeleteRow;
    property AdvanceOnEnter:boolean read FAdvanceOnEnter write FAdvanceOnEnter;
    property AdvanceInsert:boolean read FAdvanceInsert write FAdvanceInsert;
    property AutoGotoWhenSorted:boolean read FAutoGotoWhenSorted write SetAutoGoto;
    property AutoGotoIncremental:boolean read FAutoGotoIncremental write FAutoGotoIncremental;
    property AutoComboDropSize:boolean read FAutoComboDropSize write FAutoComboDropSize;
    property AdvanceDirection:TAdvanceDirection read FAdvanceDirection write FAdvanceDirection;
    property AllowClipboardShortCuts:boolean read FAllowClipboardShortcuts write FAllowClipboardShortcuts;
    property AllowSmartClipboard:boolean read FAllowSmartClipboard write FAllowSmartClipboard;
    property AllowRTFClipboard:boolean read FAllowRTFClipboard write FAllowRTFClipboard;
    property AdvanceAuto:boolean read FAdvanceAuto write FAdvanceAuto;
    property InsertPosition:TInsertPosition read FInsertPosition write FInsertPosition;
    property CursorWalkEditor:boolean read FCursorWalkEditor write FCursorWalkEditor;
    property MoveRowOnSort:boolean read FMoveRowOnSort write FMoveRowOnSort;
    {$IFDEF WIN32}
    property ImproveMaskSel:boolean read FImproveMaskSel write FImproveMaskSel;
    property AlwaysEdit:boolean read FAlwaysEdit write FAlwaysEdit;
    {$ENDIF}
  end;

  THTMLSettings = class(TPersistent)
  private
   fSaveColor:boolean;
   fSaveFonts:boolean;
   fFooterFile:string;
   fHeaderFile:string;
   fBorderSize:integer;
   fCellSpacing:integer;
   fTableStyle:string;
   fPrefixTag:string;
   fSuffixTag:string;
   fWidth:integer;
  public
   constructor Create;
  published
   property BorderSize:integer read fBorderSize write fBorderSize default 1;
   property CellSpacing:integer read fCellSpacing write fCellSpacing default 0;
   property SaveColor:boolean read fSaveColor write fSaveColor default true;
   property SaveFonts:boolean read fSaveFonts write fSaveFonts default true;
   property FooterFile:string read fFooterFile write fFooterFile;
   property HeaderFile:string read fHeaderFile write fHeaderFile;
   property TableStyle:string read fTableStyle write fTableStyle;
   property PrefixTag:string read fPrefixTag write fPrefixTag;
   property SuffixTag:string read fSuffixTag write fSuffixTag;
   property Width:integer read fWidth write fWidth;
  end;

  TPrintsettings = class(TPersistent)
  private
    FTime: TPrintPosition;
    FDate: TPrintPosition;
    FPageNr: TPrintPosition;
    FPageNumSep : string;
    FDateFormat : string;
    FTitle: TPrintPosition;
    FFont: TFont;
    FHeaderFont: TFont;
    FFooterFont: TFont;
    FBorders:TPrintBorders;
    FBorderStyle:TPenStyle;
    FTitleText:string;
    FTitleLines:TStringList;
    FCentered:boolean;
    FRepeatFixedRows:boolean;
    FRepeatFixedCols:boolean;
    FFooterSize:integer;
    FHeaderSize:integer;
    FLeftSize:integer;
    FRightSize:integer;
    FColumnSpacing:integer;
    FRowSpacing:integer;
    FTitleSpacing:integer;
    FOrientation: TPrinterOrientation;
    FPagePrefix:string;
    FPageSuffix:string;
    FFixedHeight:integer;
    FUseFixedHeight:boolean;
    FFixedWidth:integer;
    FUseFixedWidth:boolean;
    FFitToPage:TFitToPage;
    FNoAutoSize:boolean;
    FPrintGraphics:boolean;
    FJobName:string;
    procedure SetPrintFont(value:tFont);
    procedure SetPrintHeaderFont(value:tFont);
    procedure SetPrintFooterFont(value:tFont);
    procedure SetTitleLines(value:tstringlist);
  protected
  public
    constructor Create;
    destructor Destroy; override;
  published
    property FooterSize:integer read FFooterSize write FFooterSize;
    property HeaderSize:integer read FHeaderSize write FHeaderSize;
    property Time: TPrintPosition read FTime write FTime;
    property Date: TPrintPosition read FDate write FDate;
    property DateFormat : string read fDateFormat write FDateFormat;
    property PageNr: TPrintPosition read FPageNr write FPageNr;
    property Title: TPrintPosition read FTitle write FTitle;
    property TitleText : string read FTitleText write FTitleText;
    property TitleLines: tstringlist read fTitleLines write SetTitleLines;
    property Font: TFont read FFont write SetPrintFont;
    property HeaderFont: TFont read FHeaderFont write SetPrintHeaderFont;
    property FooterFont: TFont read FFooterFont write SetPrintFooterFont;
    property Borders : TPrintBorders read FBorders write FBorders;
    property BorderStyle : TPenStyle read FBorderStyle write FBorderStyle;
    property Centered : boolean read FCentered write FCentered;
    property RepeatFixedRows : boolean read FRepeatFixedRows write FRepeatFixedRows;
    property RepeatFixedCols : boolean read FRepeatFixedCols write FRepeatFixedCols;
    property LeftSize:integer read FLeftSize write FLeftSize;
    property RightSize:integer read FRightSize write FRightSize;
    property ColumnSpacing:integer read FColumnSpacing write FColumnSpacing;
    property RowSpacing:integer read FRowSpacing write FRowSpacing;
    property TitleSpacing:integer read FTitleSpacing write FTitleSpacing;
    property Orientation:TPrinterOrientation read FOrientation write FOrientation;
    property PagePrefix:string read FPagePrefix write FPagePrefix stored true;
    property PageSuffix:string read FPageSuffix write FPageSuffix;
    property FixedWidth:integer read FFixedWidth write FFixedWidth;
    property FixedHeight:integer read FFixedHeight write FFixedHeight;
    property UseFixedHeight:boolean read FUseFixedHeight write FUseFixedHeight;
    property UseFixedWidth:boolean read FUseFixedWidth write FUseFixedWidth;
    property FitToPage:TFitToPage read FFitToPage write FFitToPage;
    property JobName:string read fJobName write fJobName;
    property PageNumSep:string read fPageNumSep write fPageNumSep;
    property NoAutoSize:boolean read fNoAutoSize write fNoAutoSize;
    property PrintGraphics:boolean read fPrintGraphics write fPrintGraphics;
  end;

  TBackGroundDisplay = (bdTile,bdFixed);

  TBackGround = class(TPersistent)
  private
     fGrid:TAdvStringGrid;
     ftop:integer;
     fleft:integer;
     fdisplay:TBackGroundDisplay;
     procedure SetBitmap(value:tBitmap);
     procedure SetTop(value:integer);
     procedure SetLeft(value:integer);
     procedure SetDisplay(value:TBackgroundDisplay);
  private
    fBitmap: tBitmap;
  public
    constructor Create(aGrid:tAdvStringGrid);
    destructor Destroy; override;
  published
     property Top:integer read fTop write SetTop;
     property Left:integer read fLeft write SetLeft;
     property Display:TBackgroundDisplay read fDisplay write SetDisplay;
     property Bitmap:tBitmap read fBitmap write SetBitmap;
  end;

  {$IFDEF EDITCONTROLS}
  TGridCombo = class(TAdvCombobox)
           private
            forced:boolean;
            workmode:boolean;
           public
            grid:tadvstringgrid;
            procedure WMSetFocus(var Msg: TWMSetFocus); message WM_SETFOCUS;
            procedure DoChange;
            constructor Create(aOwner:tComponent); override;
            procedure SizeDropDownWidth;
           protected
            procedure KeyPress(var key:char); override;
            procedure KeyDown(var Key: Word; Shift: TShiftState); override;
            procedure KeyUp(var Key: Word; Shift: TShiftState); override;
            procedure DoExit; override;
           published
           end;

  TGridSpin = class(TAdvSpinEdit)
           private
            procedure WMChar(var Msg:TWMChar); message wm_char;
           protected
            procedure UpClick (Sender: TObject); override;
            procedure DownClick (Sender: TObject); override;
            procedure KeyDown(var Key: Word; Shift: TShiftState); override;
            procedure KeyUp(var Key: Word; Shift: TShiftState); override;
            procedure Keypress(var key:char); override;
            procedure DoExit; override;
           published
           public
             grid:tadvstringgrid;
             constructor Create(aOwner:tComponent); override;
           end;

  {$IFDEF DELPHI3_LVL}
  TGridDatePicker = class(TDateTimePicker)
                    private
                     procedure WMKeyDown(var Msg:TWMKeydown); message wm_keydown;
                     procedure WMNCPaint (var Message: TMessage); message WM_NCPAINT;
                    protected
                     procedure Keypress(var key:char); override;
                     procedure KeyDown(var Key: Word; Shift: TShiftState); override;
                     procedure DoExit; override;
                    published
                    public
                     grid:tadvstringgrid;
                     constructor Create(aOwner:tComponent); override;
                    end;
  {$ENDIF}

  TGridCheckBox = class(TCheckBox)
                    private
                     procedure WMLButtonDown(var Msg:TWMLButtonDown); message wm_lbuttondown;
                    protected
                     procedure KeyDown(var Key: Word; Shift: TShiftState); override;
                     procedure DoExit; override;
                    published
                    public
                     grid:tadvstringgrid;
                     constructor Create(aOwner:tComponent); override;
                    end;

  TGridEditBtn = class(TEditBtn)
                    private
                     procedure WMChar(var Msg:TWMChar); message wm_char;
                    protected
                     procedure ExtClick(Sender:tObject);
                     procedure KeyDown(var Key: Word; Shift: TShiftState); override;
                     procedure Keypress(var key:char); override;
                     procedure DoExit; override;
                    published
                    public
                     grid:tadvstringgrid;
                     constructor Create(aOwner:TComponent); override;
                    end;

  TGridUnitEditBtn = class(TUnitEditBtn)
                    private
                     procedure WMChar(var Msg:TWMChar); message wm_char;
                    protected
                     procedure KeyDown(var Key: Word; Shift: TShiftState); override;
                     procedure DoExit; override;
                    published
                    public
                     grid:tadvstringgrid;
                     constructor Create(aOwner:TComponent); override;
                    end;

  TGridButton = class(TButton)
                    private
                    protected
                     procedure WMLButtonUp(var Msg:TWMLButtonDown); message wm_lbuttonup;
                     procedure KeyUp(var Key: Word; Shift: TShiftState); override;
                     procedure KeyDown(var Key: Word; Shift: TShiftState); override;
                     procedure DoExit; override;
                    published
                    public
                     grid:tadvstringgrid;
                     constructor Create(aOwner:TComponent); override;
                    end;
  {$ENDIF}

  {TAdvInplaceEdit}

  TAdvInplaceEdit = class(TInplaceEdit)
         private
          fLengthLimit:smallint;
          fvalign:boolean;
          fwordwrap:boolean;
          gotkey:boolean;
          workmode:boolean;
          procedure setvalign(value:boolean);
          procedure setwordwrap(value:boolean);
          procedure WMSetFocus(var Msg: TWMSetFocus); message WM_SETFOCUS;
          procedure WMKillFocus(var Msg: TWMKillFocus); message WM_KILLFOCUS;
          {$IFDEF EDITCONTROLS}
          procedure WMChar(var Msg:TWMKey); message WM_CHAR;
          procedure WMPaste(var Msg:TMessage); message WM_PASTE;
          {$ENDIF}
         public
          procedure DoChange;
         protected
          procedure CreateParams(var Params:TCreateParams); override;
          procedure CreateWnd; override;
          procedure KeyDown(var Key: Word; Shift: TShiftState); override;
          procedure KeyUp(var Key: Word; Shift: TShiftState); override;
          procedure KeyPress(var Key: Char); override;
          {$IFDEF WIN32}
          procedure UpdateContents; override;
          {$ENDIF}
         published
          property valign:boolean read fvalign write setvalign;
          property wordwrap:boolean read fwordwrap write setwordwrap;
          property LengthLimit:smallint read fLengthLimit write fLengthLimit;
         end;

  {TFilterData}

  TFilterData = class(TCollectionItem)
  private
    fColumn:smallint;
    fCondition:string;
  published
    property Column:smallint read fColumn write fColumn;
    property Condition:string read fCondition write fCondition;
  end;

  {TFilter}

  TFilter = class(TCollection)
  private
   fOwner:TAdvStringGrid;
   function GetItem(Index: Integer): TFilterData;
   procedure SetItem(Index: Integer; Value: TFilterData);
  public
   constructor Create(aOwner:TAdvStringGrid);
   function Add:TFilterData;
   function Insert(index:integer): TFilterData;
   property Items[Index: Integer]: TFilterData read GetItem write SetItem;
  protected
   {$IFDEF DELPHI3_LVL}
   function GetOwner: tPersistent; override;
   {$ELSE}
   function GetOwner: tPersistent;
   {$ENDIF}
  end;


  {TGridItem}

  TGridItem = class(TCollectionItem)
  private
     fIdx:integer;
     fItems:tstrings;
      procedure SetIdx(const Value: integer);
      procedure SetItems(const Value: tstrings);
  public
     constructor Create(Collection:TCollection); override;
     destructor Destroy; override;
  published
     property Idx:integer read FIdx write SetIdx;
     property Items:tstrings read FItems write SetItems;
  end;

  {$IFDEF WIN32}
  TAdvRichEdit = class(TRichEdit)
    private
     procedure SelFormat(offset:integer);
    public
     procedure SelSubscript;
     procedure SelSuperscript;
     procedure SelNormal;
  end;
  {$ENDIF}


  {TAdvStringGrid}

  TAdvStringGrid = class(TStringGrid)
  private
    FMoveColInd : integer;
    FMoveRowInd : integer;
    FSortColumn : integer;
    FGroupColumn : integer;
    FGroupCaption : string;
    FGroupWidth : integer;
    FSortShow : boolean;
    FSortFull : boolean;
    FSortAutoFormat : boolean;
    FSortDirection : TSortDirection;
    FAutoSize : boolean;
    FAutoNumAlign : boolean;
    FEnhTextSize : boolean;
    FOemConvert : boolean;
    FLookup : boolean;
    FLookupCaseSensitive: boolean;
    FDeselectState : boolean;
    FMouseDown : boolean;
    FLookupHistory : boolean;
    FEnhRowColMove : boolean;
    FSizeWithForm : boolean;
    FMultilinecells : boolean;
    FSortFixedCols:boolean;
    FOnGetCellColor: TGridColorEvent;
    FOnGetCellPrintColor: TGridColorEvent;
    FOnGetCellBorder:TGridBorderEvent;
    FOnGetCellPrintBorder: TGridBorderEvent;
    FOnGetAlignment: TGridAlignEvent;
    FOnGetFormat:TGridFormatEvent;
    FOnGridHint:TGridHintEvent;
    FOnRowChanging:TRowChangingEvent;
    FOnColChanging:TColChangingEvent;
    FOnCellChanging:TCellChangingEvent;
    FOnShowHint:TShowHintEvent;
    FOnAutoInsertRow:TAutoInsertRowEvent;
    FOnAutoInsertCol:TAutoInsertColEvent;
    FOnAutoDeleteRow:TAutoDeleteRowEvent;
    FOnClickSort:TClickSortEvent;
    FOnCanSort:TCanSortEvent;
    FOnExpandNode:TNodeClickEvent;
    FOnContractNode:TNodeClickEvent;
    FCustomCompare:TCustomCompareEvent;
    FRawCompare:TRawCompareEvent;
    FOnResize:TOnResizeEvent;
    FOnPrintStart:TGridPrintStartEvent;
    FOnPrintPage:TGridPrintPageEvent;
    FOnPrintNewPage:TGridPrintNewPageEvent;
    FOnPrintSetColumnWidth:TGridPrintColumnWidthEvent;
    FOnPrintSetRowHeight:TGridPrintRowHeightEvent;
    FDoFitToPage:TDoFitToPageEvent;
    FOnClickCell:TClickCellEvent;
    FOnRightClickCell:TClickCellEvent;
    FOnDblClickCell:TDblClickCellEvent;
    FOnCanEditCell:TCanEditCellEvent;
    FOnIsFixedCell:TIsFixedCellEvent;
    FOnIsPasswordCell:TIsPasswordCellEvent;
    FOnAnchorClick:TAnchorClickEvent;
    FOnCellValidate:TCellValidateEvent;
    FHintColor:tcolor;
    FHintShowCells:boolean;
    FLastHintPos:tpoint;
    FSortUpGlyph:tbitmap;
    FSortDownGlyph:tbitmap;
    FSortIndexes:TIntList;
    FBackGround:TBackGround;
    {$IFDEF DELPHI3_4_ONLY}
    FOleDropTarget:boolean;
    FOleDropSource:boolean;
    FOleDropRTF:boolean;
    FOleDropTargetAssigned:boolean;
    {$ENDIF}
    {$IFDEF DELPHI3_LVL}
    ArwU,ArwD,ArwL,ArwR:TArrowWindow;
    {$ENDIF}
    {$IFDEF DELPHI4_LVL}
    FOnColumnSize:TColumnSizeEvent;
    {$ENDIF}
    FOnEndColumnSize:TEndColumnSizeEvent;
    FPrintSettings:TPrintSettings;
    FFastPrint:boolean;
    FHTMLSettings:THTMLSettings;
    FNavigation:TNavigation;
    FColumnSize:TColumnSize;
    FScrollProportional:boolean;
    FCellNode:TCellNode;
    FSizeWhileTyping:TSizeWhileTyping;
    FMouseActions:TMouseActions;
    FVisiblecol:tboolarray;
    FAllColWidths:twidtharray;
    FUpdateCount:integer;
    FNumNodes:integer;
    FNumHidden:integer;
    FSelectionColor:tColor;
    FSelectionTextColor:tColor;
    FSelectionRectangle:boolean;
    FSelectionRTFKeep:boolean;
    FVAlignment:TVAlignment;
    FVAlign:integer;
    {$IFDEF WIN32}
    FURLShow:boolean;
    FURLFull:boolean;
    FURLColor:tcolor;
    FURLEdit:boolean;
    FGridImages:TImageList;
    FIntelliPan:TIntelliPan;
    FScrollType: TScrollType;
    FScrollColor: TColor;
    FScrollWidth:integer;
    FScrollHints:TScrollHintType;
    FScrollHintWnd:THintWindow;
    FScrollHintShow:boolean;
    FIsFlat: boolean;
    FRichEdit:TAdvRichEdit;
    {$ENDIF}
    FFixedFont:tFont;
    FColumnHeaders:TStringList;
    FRowHeaders:TStringList;
    FLookupItems:TStringList;
    FRowSelect:TList;
    FFixedFooters:integer;
    FFixedRightCols:integer;
    FDelimiter:char;
    FPasswordChar:char;
    FJavaCSV:boolean;
    FCheckTrue:string;
    FCheckFalse:string;
    FEnableGraphics:boolean;
    FSaveFixedCells:boolean;
    FWordWrap:boolean;
    FModified:Boolean;
    FHovering:boolean;
    fOldCellText:String;
    fOldCol,fOldRow:Longint;
    fOldModifiedValue:Boolean;
    fOldCursor:integer;
    fBlockFocus:boolean;
    fDblClk:boolean;
    fOldSelection:tGridRect;
    fMoveSelection:tGridRect;
    fEntered:boolean;
    fFindBusy:boolean;
    fComboIdx:integer;
    sortdir:integer;
    sortrow:integer;
    searchcell:tpoint;
    showhintassigned:boolean;
    resizeassigned:boolean;
    fprintrect:tgridrect;
    ffindparams:tfindparams;
    cellcache:string;
    searchcache:string;
    searchinc:string;
    zoomfactor:integer;
    colchgflg:boolean;
    colmoveflg:boolean;
    colsizeflg:boolean;
    colsized:boolean;
    colclicked:longint;
    rowclicked:longint;
    movecell:integer;
    moveofsx:integer;
    moveofsy:integer;
    clickposx:integer;
    clickposy:integer;
    clickposdx:integer;
    clickposdy:integer;
    invokedchange:boolean;
    {$IFDEF WIN32}
    wheelmsg:cardinal;
    wheelscrl:integer;
    wheelpan:boolean;
    wheelpanpos:tpoint;
    wheeltimer:thandle;
    prevcurs:hicon;
    fprinterdriverfix:boolean;
    {$ENDIF}
    prevrect:trect;
    fontscalefactor:double;
    fprintpagewidth:integer;
    fprintpagerect:trect;
    fprintcolstart:integer;
    fprintcolend:integer;
    fprintpagefrom:integer;
    fprintpageto:integer;
    fprintpagenum:integer;
    fExcelClipboardFormat:boolean;
    fGridTimerID:integer;
    maxwidths:array[0..maxcolumns] of integer;
    indents:array[0..maxcolumns] of integer;
    {$IFDEF SPREADSHEET}
    FGridFormula:TGridFormula;
    {$ENDIF}
    {$IFDEF EDITCONTROLS}
    FOnGetEditorType:TGetEditorTypeEvent;
    FOnEllipsClick:TEllipsClickEvent;
    FOnCheckBoxClick:TCheckBoxClickEvent;
    FOnRadioClick:TRadioClickEvent;
    FOnComboChange:TComboChangeEvent;
    FOnSpinClick:TSpinClickEvent;
    editcombo:tgridcombo;
    editspin:tgridspin;
    {$IFDEF DELPHI3_LVL}
    editdate:tgriddatepicker;
    FOnScrollHint:TScrollHintEvent;
    {$ENDIF}
    editcheck:tgridcheckbox;
    editbtn:tgrideditbtn;
    uniteditbtn:tgriduniteditbtn;
    gridbutton:tgridbutton;
    movebutton:tgridbutton;
    editcontrol:TEditorType;
    fgriditems:tcollection;
    ffilter:tfilter;
    ffilterActive:boolean;
    {$IFDEF HEADER}
    fheader:theader;
    {$ENDIF}

    {
    fflat:boolean;
    fFixedRows:integer;
    fFixedCols:integer;
    procedure SetFlat(const value:boolean);
    procedure SetFixedRows(const value:integer);
    function GetFixedRows:integer;
    procedure SetFixedCols(const value:integer);
    function GetFixedCols:integer;
    }

    procedure RestoreCache;
    procedure HideEditControl(acol,arow:integer);
    procedure ShowEditControl(acol,arow:integer);
    function IsFixed(Acol,Arow:integer):boolean;
    function IsEditable(Acol,Arow:integer):boolean;
    function IsPassword(Acol,Arow:integer):boolean;
    procedure HandleRadioClick(Acol,Arow,Xpos,Ypos:integer);
    function HasStaticEdit(Acol,Arow:integer):boolean;
    {$ENDIF}
    procedure AdvanceEdit(acol,arow:integer;advance,show,frwrd:boolean);
    function GetInplaceEditor:tAdvInplaceEdit;
    function ToggleCheck(Acol,Arow:integer;fromedit:boolean):boolean;
    procedure SetAutoSize(aAutoSize:boolean);
    procedure SetSortShow(aSortShow:boolean);
    procedure SetSortColumn(aSortColumn:integer);
    procedure SetGroupColumn(aGroupColumn:integer);
    procedure QuickSortRows(col,left,right:integer);
    procedure QuickSortRowsIndexed(col,left,right:integer);
    procedure SetVAlignment(aVAlignment:TVAlignment);
    function BuildPages(canvas:tcanvas;printmethod:tprintmethod;maxpages:integer):integer;
    function Compare(col,arow1,arow2:longint):integer;
    function CompareLine(col,arow1,arow2:longint):integer;
    function CompareLineIndexed(colidx,arow1,arow2:longint):integer;
    function MatchCell(col,row:integer):boolean;
    procedure WMSetFocus(var Msg: TWMSetFocus); message WM_SETFOCUS;
    procedure WMLButtonUp(var Msg:TWMLButtonUp); message wm_lbuttonup;
    procedure WMLButtonDown(var Msg:TWMLButtonDown); message wm_lbuttondown;
    procedure WMRButtonDown(var Msg:TWMLButtonDown); message wm_rbuttondown;
    procedure WMLButtonDblClk(var Message: TWMLButtonDblClk); message wm_lbuttondblclk;
    procedure WMKeyDown(var Msg:TWMKeydown); message wm_keydown;
    procedure WMSize(var Msg: TWMSize); message WM_SIZE;
    procedure WMPaint(var Msg: TWMPAINT); message WM_PAINT;
    procedure WMEraseBkGnd(var Message:TMessage); message WM_ERASEBKGND;
    procedure WMTimer(var Msg:TWMTimer); message WM_TIMER;
    {$IFDEF WIN32}
    procedure WMVScroll(var WMScroll:TWMScroll ); message WM_VSCROLL;
    procedure WMHScroll(var WMScroll:TWMScroll ); message WM_HSCROLL;
    {$ENDIF}
    procedure CMCursorChanged(var Message: TMessage); message CM_CURSORCHANGED;
    procedure CMHintShow(Var Msg: TMessage); Message CM_HINTSHOW;
    procedure ShowHintProc(var HintStr: string; var CanShow: Boolean; var HintInfo: THintInfo);
    procedure DrawSortIndicator(canvas:tcanvas;x,y:integer); 
    procedure GridResize(Sender:tObject);
    function GetCellType(ACol,ARow:Integer):TCellType;
    function GetCellGraphic(ACol,ARow:Integer):TCellGraphic;
    function GetCellGraphicSize(ACol,ARow:Integer):TPoint;
    function FreeCellGraphic(ACol,ARow:Integer):boolean;
    function CreateCellGraphic(ACol,ARow:Integer):TCellGraphic;
    function GetCellImages(Acol,Arow:integer):TIntList;
    procedure SetInts(ACol,ARow:Integer;const Value:integer);
    function GetInts(ACol,ARow:Integer):integer;
    procedure SetDates(ACol,ARow:Integer;const Value:tdatetime);
    function GetDates(ACol,ARow:Integer):tdatetime;
    function GetRowSelect(ARow:integer):boolean;
    procedure SetRowSelect(ARow:integer;value:boolean);
    function GetRowSelectCount:integer;
    procedure RepaintRect(r:trect);
    procedure RepaintRow(aRow:integer);
    procedure SelectToRowSelect(aRow:integer);
    procedure SetFixedFont(value:tFont);
    procedure FixedFontChanged(Sender: TObject);
    procedure MultiImageChanged(Sender: TObject; Acol,Arow:integer);
    procedure SetColumnHeaders(Value: TStringList);
    procedure ColHeaderChanged(Sender: TObject);
    procedure SetRowHeaders(Value: TStringList);
    procedure RowHeaderChanged(Sender: TObject);
    function GetPrintColWidth(aCol:integer):integer;
    function GetPrintColOffset(aCol:integer):integer;
    procedure SetLookupItems(Value: TStringList);
    function PasteFunc(acol,arow:integer):integer;
    procedure CopyFunc(gd:tGridRect);
    procedure CopyRTFFunc(acol,arow:integer);
    procedure SetPreviewPage(value:integer);
    function GetUpGlyph: TBitmap;
    procedure SetUpGlyph(Value: TBitmap);
    function GetDownGlyph: TBitmap;
    procedure SetDownGlyph(Value: TBitmap);
    procedure SetBackground(Value: TBackground);
    {$IFDEF WIN32}
    procedure RTFPaint(acol,arow:integer;canvas:tcanvas;arect:trect);
    procedure FlatInit;
    procedure FlatDone;
    procedure FlatUpdate;
    procedure FlatSetScrollProp(index,newValue:integer;fRedraw:bool);
    procedure FlatSetScrollInfo(code:integer;var scrollinfo:tscrollinfo;fRedraw:bool);
    { procedure FlatSetScrollPos(code,pos:integer); }
    procedure FlatShowScrollBar(code:integer;show:bool);
    procedure UpdateVScrollBar;
    procedure UpdateHScrollBar;
    procedure UpdateType;
    procedure UpdateColor;
    procedure UpdateWidth;
    procedure SetScrollType(const Value: TScrollType);
    procedure SetScrollColor(const Value: TColor);
    procedure SetScrollWidth(const Value: integer);
    procedure SetScrollProportional(value:boolean);
    {$ENDIF}
    function GetLockFlag : boolean;
    procedure SetLockFlag(AValue : boolean);
    function RemapCol(acol:integer):integer;
    function RemapColInv(acol:integer):integer;
    function RemapRow(arow:integer):integer;
    function RemapRowInv(arow:integer):integer;
    procedure setvisiblecol(i:integer;avalue:boolean);
    function getvisiblecol(i:integer):boolean;
    function MaxLinesInGrid:integer;
    function MaxLinesInRow(arow:integer):integer;
    function MaxCharsInCol(acol:integer):integer;
    procedure SizeToLines(const ARow,Lines,Padding: LongInt);
    procedure SizeToWidth(const ACol:longint;inconly:boolean);
    function GetCellTextSize(aCol,aRow:integer):tsize;
    function GetCellAlignment(aCol,aRow:integer):TAlignment;
    {$IFDEF WIN32}
    procedure DrawIntelliFocusPoint;
    procedure EraseIntelliFocusPoint;
    procedure SetImages(value:tImageList);
    procedure CalcTextPos(var aRect:TRect;aAngle:Integer;aTxt:String;tal:tAlignment);
    {$ENDIF}
    procedure SetFixedFooters(value:integer);
    procedure SetFixedRightCols(value:integer);
    procedure SetFixedColWidth(value:integer);
    procedure SetHovering(value:boolean);
    function GetFixedColWidth:integer;
    procedure SetFixedRowHeight(value:integer);
    function GetFixedRowHeight:integer;
    procedure SetWordWrap(value:boolean);
    procedure SetSelectionColor(aColor:tColor);
    procedure SetSelectionTextColor(aColor:tColor);
    procedure SetSelectionRectangle(avalue:boolean);
    procedure SetFilterActive(const Value:boolean);
    function MatchFilter(arow:integer):boolean;
    procedure ApplyFilter;
    function GetCellsEx(i,j:integer):string;
    procedure SetCellsEx(i,j:integer;value:string);
    function GetStrippedCell(i,j:integer):string;
    function HiddenRow(j:integer):tstrings;
    function PasteText(aCol,aRow:integer;p:pchar):integer;
    procedure InputFromCSV(FileName:string;insertmode:boolean);
    procedure OutputToCSV(FileName:string;appendmode:boolean);
    {$IFDEF DELPHI3_LVL}
    function GetDateTimePicker:TDateTimePicker;
    procedure SetArrowColor(value:tcolor);
    function GetArrowColor:tcolor;
    {$ENDIF}
    {$IFDEF DELPHI3_4_ONLY}
    procedure SetOleDropTarget(avalue:boolean);
    procedure SetOleDropRTF(avalue:boolean);
    {$ENDIF}
   public
    lbutflg:boolean;
    compares:longint;
    swaps:longint;
    sorttime:dword;
    sortlist:tstringlist;
    prevsizex,prevsizey:integer;
    {$IFDEF EDITCONTROLS}
    editmode:boolean;
    procedure ClearComboString;
    procedure AddComboString(const s:string);
    function RemoveComboString(const s:string):boolean;
    function SetComboSelectionString(const s:string):boolean;
    procedure SetComboSelection(idx:integer);
    function GetComboCount:integer;
    {$ENDIF}
    constructor Create(aOwner:tComponent); override;
    destructor Destroy; override;
    function GetVersionNr:integer; virtual;
    function GetVersionString:string; virtual;
    function ValidateCell(const s:string):boolean; virtual;
    procedure RemoveRows(RowIndex, RCount: LongInt); virtual;
    procedure InsertRows(RowIndex, RCount: LongInt); virtual;
    procedure RemoveCols(ColIndex, CCount: LongInt); virtual;
    procedure InsertCols(ColIndex, CCount: LongInt); virtual;
    procedure MergeCols(ColIndex1, ColIndex2 : Longint);
    procedure SwapColumns(aCol1,aCol2:longint);
    procedure HideColumn(colindex:integer);
    procedure UnhideColumn(colindex:integer);
    procedure HideColumns(fromcol,tocol:integer);
    procedure UnhideColumns(fromcol,tocol:integer);
    procedure UnhideColumnsAll;
    function IsHiddenColumn(colindex:integer):boolean;
    function NumHiddenColumns:integer;
    procedure Group(colindex:integer);
    procedure UnGroup;
    procedure HideRow(rowindex:integer);
    procedure HideRows(fromrow,torow:integer);
    procedure UnHideRow(rowindex:integer);
    procedure UnHideRows(fromrow,torow:integer);
    procedure UnHideRowsAll;
    function IsHiddenRow(rowindex:integer):boolean;
    function NumHiddenRows:integer;
    function RealRowIndex(arow:integer):integer;
    function RealColIndex(acol:integer):integer;
    function DisplRowIndex(arow:integer):integer;
    function DisplColIndex(acol:integer):integer;
    function GetRealCol:integer;
    function GetRealRow:integer;
    procedure HideSelection;
    procedure UnHideSelection;
    procedure MoveRow(FromIndex, ToIndex: Longint);
    procedure MoveColumn(FromIndex, ToIndex: Longint);
    procedure SwapRows(aRow1,aRow2:longint);
    procedure SortSwapRows(aRow1,aRow2:longint);
    procedure ClearRect(aCol1,aRow1,aCol2,aRow2:integer); virtual;
    procedure Clear;
    procedure ClearRows(RowIndex, RCount: LongInt);
    procedure ClearCols(ColIndex, CCount: LongInt);
    procedure ClearNormalCells;
    procedure ClearRowSelect;
    procedure SelectRows(RowIndex, RCount: LongInt);
    procedure SelectCols(ColIndex, CCount: LongInt);
    procedure SelectRange(FromCol,ToCol,FromRow,ToRow: Longint);
    function isCell(SubStr: String; var ACol, ARow: LongInt): Boolean;
    procedure SaveToFile(FileName: String);
    procedure SaveToHTML(FileName: String);
    procedure SaveToXML(FileName: String; ListDescr, RecordDescr:string;FieldDescr:tstrings);
    procedure SaveToASCII(FileName: String);
    procedure SaveToCSV(fileName: String);
    procedure AppendToCSV(fileName: String);
    procedure SaveToStream(stream:TStream);
    procedure LoadFromFile(FileName: String);
    procedure LoadFromCSV(FileName: String);
    procedure LoadFromStream(stream:TStream);
    procedure LoadFromFixed(FileName:string;positions:tintlist);
    procedure InsertFromCSV(FileName: String);
    procedure SaveColSizes;
    procedure LoadColSizes;
    procedure SavePrintSettings(key,section:string);
    procedure LoadPrintSettings(key,section:string);
    procedure CutToClipboard;
    procedure CopyToClipBoard;
    procedure CopySelectionToClipboard;
    procedure PasteFromClipboard;
    procedure PasteSelectionFromClipboard;
    procedure ShowColumnHeaders;
    procedure ClearColumnHeaders;
    procedure ShowRowHeaders;
    procedure ClearRowHeaders;
    procedure RandomFill(doFixed:boolean;rnd:integer);
    {$IFDEF WIN32}
    procedure Zoom(x:integer);
    procedure SaveToXLS(Filename:string);
    procedure LoadFromXLS(Filename:string);
    procedure SaveToDOC(Filename:string);
    procedure RichToCell(col,row:integer;richeditor:trichedit);
    procedure CellToRich(col,row:integer;richeditor:trichedit);
    {$ENDIF}
    {$IFDEF ISDELPHI}
    function CellToReal(aCol,aRow:LongInt): real;
    {$ENDIF}
    procedure AutoSizeCells(const DoFixedCells: boolean; const PaddingX,PaddingY:integer);
    procedure AutoSizeColumns(const DoFixedCols: Boolean; const Padding: Integer);
    procedure AutoSizeCol(const aCol: LongInt);
    procedure AutoSizeRows(const DoFixedRows: Boolean; const Padding: Integer);
    procedure AutoSizeRow(const aRow: LongInt);
    procedure StretchRightColumn;
    procedure AutoNumberCol(const aCol:longint);
    function IsSelected(aCol,aRow:longint):boolean;
    function SelectedText:string;
    {$IFDEF EDITCONTROLS}
    procedure ShowInplaceEdit;
    {$ENDIF}
    procedure QSort;
    procedure QSortIndexed;
    procedure QSortGroup;
    procedure Print;
    procedure PrintRect(gridrect:tgridrect);
    procedure PrintPreview(canvas:tcanvas;displayrect:trect);
    procedure PrintPreviewRect(canvas:tcanvas;displayrect:trect;gridrect:tgridrect);
    procedure SortByColumn(col:integer);
    procedure QuickSort(col,left,right:integer);
    procedure QuickSortIndexed(left,right:integer);
    function SortLine(col,arow1,arow2:longint):boolean;
    function Search(s:string):longint;
    function FindFirst(s:string;findparams:tfindparams):tpoint;
    function FindNext:tpoint;
    function MapFontHeight(pointsize:integer):integer;
    function MapFontSize(height:integer):integer;
    procedure AddBitmap(aCol,aRow:integer;abmp:tbitmap;transparent:boolean;hal:TCellHalign;val:TCellValign);
    procedure RemoveBitmap(aCol,aRow:integer);
    function GetBitmap(aCol,aRow:integer):tbitmap;
    procedure AddPicture(aCol,aRow:integer;apicture:tpicture;transparent:boolean;hal:TCellHalign;val:TCellValign);
    procedure RemovePicture(aCol,aRow:integer);
    function GetPicture(aCol,aRow:integer):tpicture;
    procedure AddNode(aRow,Span:integer);
    procedure RemoveNode(aRow:integer);
    function IsNode(aRow:integer):boolean;
    function GetNodeState(ARow:integer):boolean;
    procedure SetNodeState(ARow:integer;value:boolean);
    procedure ExpandNode(aRow:integer);
    procedure ContractNode(aRow:integer);
    procedure ExpandAll;
    procedure ContractAll;
    procedure AddRadio(aCol,aRow,DirRadio,IdxRadio:integer;sl:tstrings);
    procedure RemoveRadio(aCol,aRow:integer);
    function IsRadio(Acol,Arow:integer):boolean;
    function GetRadioIdx(Acol,Arow:integer;var IdxRadio:integer):boolean;
    function SetRadioIdx(Acol,Arow,IdxRadio:integer):boolean;
    {$IFDEF WIN32}
    procedure AddImageIdx(Acol,Arow,Aidx:integer;hal:TCellHalign;val:TCellValign);
    procedure RemoveImageIdx(Acol,Arow:integer);
    function GetImageIdx(Acol,Arow:integer;var idx:integer):boolean;
    procedure AddMultiImage(Acol,Arow,Dir:integer;hal:TCellHalign;val:TCellValign);
    procedure RemoveMultiImage(Acol,Arow:integer);
    procedure AddDataImage(Acol,Arow,Aidx:integer;hal:TCellHalign;val:TCellValign);
    procedure RemoveDataImage(Acol,Arow:integer);
    procedure AddRotated(Acol,Arow:integer;aAngle:smallint;s:string);
    procedure RemoveRotated(Acol,Arow:integer);
    function IsRotated(Acol,Arow:integer;var aAngle:integer):boolean;
    {$ENDIF}
    procedure AddIcon(Acol,Arow:integer;aicon:ticon;hal:TCellHalign;val:TCellValign);
    procedure RemoveIcon(Acol,Arow:integer);
    procedure AddCheckBox(Acol,Arow:integer;state,data:boolean);
    procedure RemoveCheckBox(Acol,Arow:integer);
    function HasCheckBox(Acol,Arow:integer):boolean;
    function GetCheckBoxState(Acol,Arow:integer;var state:boolean):boolean;
    function SetCheckBoxState(Acol,Arow:integer;state:boolean):boolean;
    function ToggleCheckBox(Acol,Arow:integer):boolean;
    function ColumnSum(acol,fromrow,torow:integer):double;
    procedure BeginUpdate;
    procedure EndUpdate;
    procedure ResetUpdate;
    property LockUpdate:boolean read GetLockFlag write SetLockFlag;
    property CellTypes[ACol,Arow:integer]:TCellType read GetCellType;
    property CellGraphics[Acol,Arow:integer]:TCellGraphic read GetCellGraphic;
    property CellGraphicSize[Acol,Arow:integer]:TPoint read GetCellGraphicSize;
    property CellImages[Acol,Arow:integer]:TIntList read GetCellImages;
    property Ints[ACol,ARow:Integer]:integer read GetInts write SetInts;
    property Dates[ACol,ARow:Integer]:tdatetime read GetDates write SetDates;
    property Delimiter:char read FDelimiter write FDelimiter;
    property PasswordChar:char read FPasswordChar write FPasswordChar;
    property JavaCSV:boolean read FJavaCSV write FJavaCSV;
    property FastPrint:boolean read fFastPrint write fFastPrint;
    property CheckTrue:string read FCheckTrue write FCheckTrue;
    property CheckFalse:string read FCheckFalse write FCheckFalse;
    property SaveFixedCells:boolean read FSaveFixedCells write FSaveFixedCells;
    property SortIndexes:TIntList read FSortIndexes;
    property OriginalCellValue:string read CellCache;
    {$IFDEF DELPHI3_LVL}
    property DateTimePicker:tDateTimePicker read GetDateTimePicker;
    {$ENDIF}
    {$IFDEF EDITCONTROLS}
    property Combobox:TGridCombo read EditCombo;
    property SpinEdit:TGridSpin read EditSpin;
    property BtnEdit:TGridEditBtn read EditBtn;
    property BtnUnitEdit:TGridUnitEditbtn read UnitEditBtn;
    property RichEdit:TAdvRichEdit read fRichEdit;
    property NormalEdit:TAdvInplaceEdit read GetInplaceEditor;
    {$ENDIF}
    {$IFDEF WIN32}
    property PrinterDriverFix:boolean read fPrinterDriverFix write fPrinterDriverFix;
    {$ENDIF}
    property RowSelect[aRow:integer]:boolean read GetRowSelect write SetRowSelect;
    property RowSelectCount:integer read GetRowSelectCount;
    property NodeState[aRow:integer]:boolean read GetNodeState write SetNodeState;
    property FindBusy:boolean read fFindBusy;
    property PrintPageRect:trect read fPrintPageRect;
    property PrintPageWidth:integer read fPrintPageWidth;
    property PrintColWidth[aCol:integer]:integer read GetPrintColWidth;
    property PrintColOffset[aCol:integer]:integer read GetPrintColOffset;
    property PrintColStart:integer read fPrintColStart;
    property PrintColEnd:integer read fPrintColEnd;
    property PrintNrOfPages:integer read fPrintPageNum;
    property ExcelClipboardFormat:boolean read fExcelClipboardFormat write FExcelClipboardFormat;
    property PreviewPage:integer read fPrintPageFrom write SetPreviewPage;
    property AllCells[i,j:integer]:string read GetCellsEx write SetCellsEx;
    property StrippedCells[i,j:integer]:string read GetStrippedCell;
    {$IFDEF DELPHI3_LVL}
    property ArrowColor:tColor read GetArrowColor write SetArrowColor;
    {$ENDIF}
    property GroupColumn:integer read fGroupColumn write SetGroupColumn;
    property RealRow:integer read GetRealRow;
    property RealCol:integer read GetRealCol;
    property VersionNr:integer read GetVersionNr;
    property VersionString:string read GetVersionString;
   protected
    fcliptopleft:tpoint;
    procedure DrawCell(ACol,ARow:Longint;ARect:TRect;AState:TGridDrawState); override;
    procedure PasteNotify(orig:tpoint;gr:tgridrect); virtual;
    function CalcCell(ACol,ARow:integer):string; virtual;
    procedure UpdateCell(ACol,ARow:integer); virtual;
    procedure KeyUp(var Key: Word; Shift: TShiftState); override;
    function CreateEditor:TInplaceEdit; override;
    function CanEditShow:boolean; override;
    procedure SetEditText(ACol, ARow: Longint; const Value: string); override;
    function GetEditText(ACol, ARow: Longint): string; override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure ColumnMoved(FromIndex, ToIndex: Longint); override;
    procedure RowMoved(FromIndex, ToIndex: Longint); override;
    procedure KeyPress(var Key:char); override;
    procedure DestroyWnd; override;
    procedure CreateWnd; override;
    procedure Loaded; override;
    function  SelectCell(ACol, ARow: Longint): Boolean; override;
    {$IFDEF WIN32}
    procedure WndProc(var Message:tMessage); override;
    procedure SizeChanged(OldColCount, OldRowCount: Longint); override;
    procedure Notification(AComponent: TComponent; AOperation: TOperation); override;
    {$ENDIF}
    {$IFDEF DELPHI4_LVL}
    procedure CalcSizingState(X, Y: Integer; var State: TGridState;
      var Index: Longint; var SizingPos, SizingOfs: Integer;
      var FixedInfo: TGridDrawInfo); override;
    {$ENDIF}
    procedure Click; override;
    procedure DoEnter; override;
    procedure DoExit; override;
    procedure InitValidate(acol,arow:integer); virtual;
    procedure Paint; override;
    procedure ColWidthsChanged; override;
    procedure RowHeightsChanged; override;
    procedure InvalidateGridRect(r:tgridrect);
    {$IFDEF EDITCONTROLS}
    procedure TopLeftChanged; override;
    procedure EllipsClick; virtual;
    {$ENDIF}
   published
    property AutoNumAlign:boolean read fAutoNumAlign write fAutoNumAlign;
    property AutoSize:boolean read FAutoSize write SetAutoSize;
    property VAlignment:TVAlignment read FVAlignment write SetVAlignment;
    property EnhTextSize:boolean read FEnhTextSize write FEnhTextSize;
    property EnhRowColMove:boolean read FEnhRowColMove write FEnhRowColMove;
    property SortFixedCols:boolean read FSortFixedCols write FSortFixedCols;
    property SizeWithForm:boolean read FSizeWithForm write FSizeWithForm;
    property Multilinecells:boolean read FMultilinecells write FMultilinecells;
    property OnGetCellColor: TGridColorEvent read FOnGetCellColor
      write FOnGetCellColor;
    property OnGetCellPrintColor: TGridColorEvent read FOnGetCellPrintColor
      write FOnGetCellPrintColor;
    property OnGetCellPrintBorder: TGridBorderEvent read FOnGetCellPrintBorder
      write FOnGetCellPrintBorder;
    property OnGetCellBorder: TGridBorderEvent read FOnGetCellBorder
      write FOnGetCellBorder;
    property OnGetAlignment: TGridAlignEvent read FOnGetAlignment
      write FOnGetAlignment;
    property OnGetFormat: TGridFormatEvent read FOnGetFormat
      write FOnGetFormat;
    property OnGridHint:TGridHintEvent read FOnGridHint
      write FOnGridHint;
    property OnRowChanging:TRowChangingEvent read FOnRowChanging
      write FOnRowChanging;
    property OnColChanging:TColChangingEvent read FOnColChanging
      write FOnColChanging;
    property OnCellChanging:TCellChangingEvent read FOnCellChanging
      write FOnCellChanging;
    property OnPrintPage:TGridPrintPageEvent read FOnPrintPage
      write FOnPrintPage;
    property OnPrintStart:TGridPrintStartEvent read FOnPrintStart
      write FOnPrintStart;
    property OnFitToPage:TDoFitToPageEvent read FDoFitToPage
      write FDoFitToPage;
    property OnPrintNewPage:TGridPrintNewPageEvent read FOnPrintNewPage
      write FOnPrintNewPage;
    property OnPrintSetColumnWidth:TGridPrintColumnWidthEvent read FOnPrintSetColumnWidth
      write FOnPrintSetColumnWidth;
    property OnPrintSetRowHeight:TGridPrintRowHeightEvent read FOnPrintSetRowHeight
      write FOnPrintSetRowHeight;
    property OnAutoInsertRow:TAutoInsertRowEvent read FOnAutoInsertRow
      write FOnAutoInsertRow;
    property OnAutoInsertCol:TAutoInsertColEvent read FOnAutoInsertCol
      write FOnAutoInsertCol;
    property OnAutoDeleteRow:TAutoDeleteRowEvent read FOnAutoDeleteRow
      write FOnAutoDeleteRow;
    property OnClickSort:TClickSortEvent read FOnClickSort
      write FOnClickSort;
    property OnCanSort:TCanSortEvent read FOnCanSort
      write FOnCanSort;
    property OnExpandNode:TNodeClickEvent read FOnExpandNode
      write FOnExpandNode;
    property OnContractNode:TNodeClickEvent read FOnContractNode
      write FOnContractNode;
    property OnCustomCompare:TCustomCompareEvent read FCustomCompare
      write FCustomCompare;
    property OnRawCompare:TRawCompareEvent read FRawCompare
      write FRawCompare;
    property OnClickCell:TClickCellEvent read FOnClickCell
      write FOnClickCell;
    property OnRightClickCell:TClickCellEvent read FOnRightClickCell
      write FOnRightClickCell;
    property OnDblClickCell:TDblClickCellEvent read FOnDblClickCell
      write FOnDblClickCell;
    property OnCanEditCell:TCanEditCellEvent read FOnCanEditCell
      write FOnCanEditCell;
    property OnIsFixedCell:TIsFixedCellEvent read FOnIsFixedCell
      write FOnIsFixedCell;
    property OnIsPasswordCell:TIsPasswordCellEvent read FOnIsPasswordCell
      write FOnIsPasswordCell;
    property OnAnchorClick:TAnchorClickEvent read FOnAnchorClick
      write FOnAnchorClick;
    property OnCellValidate: TCellValidateEvent read FOnCellValidate
      write FOnCellValidate;
    property SortDirection:TSortDirection read FSortDirection
      write FSortDirection;
    {$IFDEF EDITCONTROLS}
    property OnGetEditorType:TGetEditorTypeEvent read FOnGetEditorType
      write FOnGetEditorType;
    property OnEllipsClick:TEllipsClickEvent read FOnEllipsClick
      write FOnEllipsClick;
    property OnCheckBoxClick:TCheckBoxClickEvent read FOnCheckBoxClick
      write FOnCheckBoxClick;
    property OnRadioClick:TRadioClickEvent read FOnRadioClick
      write FOnRadioClick;
    property OnComboChange:TComboChangeEvent read FOnComboChange
      write FOnComboChange;
    property OnSpinClick:TSpinClickEvent read FOnSpinClick
      write FOnSpinClick;
    {$ENDIF}
    property SortFull: boolean read FSortFull write FSortFull;
    property SortAutoFormat: boolean read fSortAutoFormat write fSortAutoFormat;
    property SortShow: boolean read FSortShow write SetSortShow;
    property EnableGraphics:boolean read FEnableGraphics write FEnableGraphics;
    property SortColumn:integer read FSortColumn write SetSortColumn;
    property HintColor:tcolor read FHintcolor write FHintcolor;
    property SelectionColor:tcolor read FSelectionColor write SetSelectionColor;
    property SelectionTextColor:tcolor read FSelectionTextColor write SetSelectionTextColor;
    property SelectionRectangle:boolean read FSelectionRectangle write SetSelectionRectangle;
    property SelectionRTFKeep:boolean read FSelectionRTFKeep write FSelectionRTFKeep;
    {$IFDEF DELPHI3_LVL}
    property HintShowCells:boolean read FHintShowCells write FHintShowCells;
    property OnScrollHint:TScrollHintEvent read FOnScrollHint
      write FOnScrollHint;
    {$ENDIF}
    {$IFDEF DELPHI4_LVL}
    property OnColumnSize:TColumnSizeEvent read FOnColumnSize
      write FOnColumnSize;
    {$ENDIF}
    property OnEndColumnSize:TEndColumnSizeEvent read FOnEndColumnSize
      write FOnEndColumnSize;
    {$IFDEF DELPHI3_4_ONLY}
    property OleDropTarget:boolean read FOleDropTarget write SetOleDropTarget;
    property OleDropSource:boolean read FOleDropSource write FOleDropSource;
    property OleDropRTF:boolean read FOleDropRTF write SetOleDropRTF;
    {$ENDIF}
    property PrintSettings:TPrintSettings read FPrintSettings write FPrintSettings;
    property HTMLSettings:THTMLSettings read FHTMLSettings write FHTMLSettings;
    property Navigation:TNavigation read FNavigation write FNavigation;
    property ColumnSize:TColumnSize read FColumnSize write FColumnSize;
    property CellNode:tCellNode read fCellNode write fCellNode;
    property SizeWhileTyping:TSizeWhileTyping read FSizeWhileTyping write FSizeWhileTyping;
    property MouseActions:TMouseActions read FMouseActions write FMouseActions;
    {$IFDEF WIN32}
    property GridImages:TImageList read FGridImages write SetImages;
    property IntelliPan:TIntelliPan read FIntelliPan write FIntelliPan;
    property URLColor:tcolor read FURLColor write FURLColor;
    property URLShow:boolean read FURLShow write FURLShow;
    property URLFull:boolean read FURLFull write FURLFull;
    property URLEdit:boolean read FURLEdit write FURLEdit;
    property ScrollType:TScrollType read FScrollType write SetScrollType;
    property ScrollColor:TColor read FScrollColor write SetScrollColor;
    property ScrollWidth:integer read FScrollWidth write SetScrollWidth;
    property ScrollProportional:boolean read FScrollProportional write SetScrollProportional;
    property ScrollHints:TScrollHintType read FScrollHints write FScrollHints;
    property OemConvert:boolean read fOemConvert write fOemConvert;
    {$ENDIF}
    property FixedFooters:integer read fFixedFooters write SetFixedFooters;
    property FixedRightCols:integer read fFixedRightCols write SetFixedRightCols;
    property FixedColWidth:integer read GetFixedColwidth write SetFixedColwidth;
    property FixedRowHeight:integer read GetFixedRowHeight write SetFixedRowHeight;
    property FixedFont:tFont read fFixedFont write SetFixedFont;
    property WordWrap:boolean read fWordWrap write SetWordWrap;
    property ColumnHeaders: TStringList read FColumnHeaders write SetColumnHeaders;
    property RowHeaders: TStringList read FRowHeaders write SetRowHeaders;
    property LookupItems: TStringList read FLookupItems write SetLookupItems;
    property Lookup: boolean read FLookup write FLookup;
    property LookupCaseSensitive: boolean read FLookupCaseSensitive write FLookupCaseSensitive;
    property LookupHistory: boolean read FLookupHistory write FLookupHistory;
    property SortUpGlyph:tbitmap read GetUpGlyph write SetUpGlyph;
    property SortDownGlyph:tbitmap read GetDownGlyph write SetDownGlyph;
    property BackGround:tBackground read fBackground write SetBackground;
    property Hovering:boolean read FHovering write SetHovering;
    property Filter:tfilter read fFilter write fFilter;
    property FilterActive:boolean read fFilterActive write SetFilterActive;
    {
    property Flat:boolean read fFlat write SetFlat;
    property FixedRows:integer read GetFixedRows write SetFixedRows;
    property FixedCols:integer read GetFixedCols write SetFixedCols;
    }
  end;

  {$IFDEF DELPHI3_4_ONLY}
  TGridDropTarget = class(TDropTarget)
                    private
                     FGrid:TAdvStringGrid;
                    public
                     constructor Create(aGrid:TAdvStringGrid);
                     procedure DropText(pt:tpoint;s:string); override;
                     procedure DropRTF(pt:tpoint;s:string); override;
                     procedure DropFiles(pt:tpoint;files:tstrings); override;
                    end;
  {$ENDIF}

implementation // 16.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

 filectrl;

const
 {$IFDEF WIN32}
  comctrl = 'comctl32.dll';



 {------------------------------------------------}
 {constant definitions for flat/encarta scrollbars}
 {------------------------------------------------}
  WSB_PROP_CYVSCROLL  = $0000001;
  WSB_PROP_CXHSCROLL  = $0000002;
  WSB_PROP_CYHSCROLL  = $0000004;
  WSB_PROP_CXVSCROLL  = $0000008;
  WSB_PROP_CXHTHUMB   = $0000010;
  WSB_PROP_CYVTHUMB   = $0000020;
  WSB_PROP_VBKGCOLOR  = $0000040;
  WSB_PROP_HBKGCOLOR  = $0000080;
  WSB_PROP_VSTYLE     = $0000100;
  WSB_PROP_HSTYLE     = $0000200;
  WSB_PROP_WINSTYLE   = $0000400;
  WSB_PROP_PALETTE    = $0000800;
  WSB_PROP_MASK       = $0000FFF;

  FSB_FLAT_MODE       =    2;
  FSB_ENCARTA_MODE    =    1;
  FSB_REGULAR_MODE    =    0;


 {---------------------------------------------------}
 {constant definitions for OLE automation with Word  }
 {---------------------------------------------------}
  wdAlignParagraphLeft = 0;
  wdAlignParagraphCenter = 1;
  wdAlignParagraphRight = 2;
  wdAlignParagraphJustify = 3;

 {---------------------------------------------------}
 {constant definitions for OLE automation with Excel }
 {---------------------------------------------------}
  xlAddIn = 18;
  xlCSV = 6;
  xlCSVMac = 22;
  xlCSVMSDOS = 24;
  xlCSVWindows = 23;
  xlDBF2 = 7;
  xlDBF3 = 8;
  xlDBF4 = 11;
  xlDIF = 9;
  xlExcel2 = 16;
  xlExcel2FarEast = 27;
  xlExcel3 = 29;
  xlExcel4 = 33;
  xlExcel5 = 39;
  xlExcel7 = 39;
  xlExcel9795 = 43;
  xlExcel4Workbook = 35;
  xlIntlAddIn = 26;
  xlIntlMacro = 25;
  xlWorkbookNormal = -4143;
  xlSYLK = 2;
  xlTemplate = 17;
  xlCurrentPlatformText = -4158;
  xlTextMac = 19;
  xlTextMSDOS = 21;
  xlTextPrinter = 36;
  xlTextWindows = 20;
  xlWJ2WD1 = 14;
  xlWK1 = 5;
  xlWK1ALL = 31;
  xlWK1FMT = 30;
  xlWK3 = 15;
  xlWK4 = 38;
  xlWK3FM3 = 32;
  xlWKS = 4;
  xlWorks2FarEast = 28;
  xlWQ1 = 34;
  xlWJ3 = 40;
  xlWJ3FJ3 = 41;

  xlA1 = 1;
  xlR1C1 = -4150;

 {----------------------------------------------}
 {constant definitions for Intellimouse support }
 {----------------------------------------------}
  MSH_MOUSEWHEEL = 'MSWHEEL_ROLLMSG';
  MOUSEZ_CLASSNAME = 'MouseZ';               // wheel window class
  MOUSEZ_TITLE     = 'Magellan MSWHEEL';     // wheel window title
  MSH_WHEELSUPPORT = 'MSH_WHEELSUPPORT_MSG'; // name of msg to query for wheel support
  MSH_SCROLL_LINES = 'MSH_SCROLL_LINES_MSG';
  MSH_WHEELMODULE_CLASS = MOUSEZ_CLASSNAME;
  MSH_WHEELMODULE_TITLE = MOUSEZ_TITLE;

  {$IFDEF DELPHI2_LVL}
  {defined in Delphi 3,4 and BCB3 but not in Delphi 2, BCB1}
  SPI_GETWHEELSCROLLLINES = 105;
  SPI_SETWHEELSCROLLLINES = 105;
  WM_MOUSEWHEEL = $20A;
  {$ENDIF}

 {$ENDIF}

 {---------------------------------------}
 {constant definitions for extra cursors }
 {---------------------------------------}
  crURLcursor = 8009;
  crHorzArr = 8010;
  crVertArr = 8011;
  crAsgCross = 8012;

  Numeric_Characters = [$30..$39,$8,$9,$D];
  Float_Characters = [$30..$39,$8,$9,$D,ord('-'),ord(','),ord('.')];

  CSVSeparators:array[1..10] of char = (',',';','#',#9,'|','@','*','-','+','&');

{$I HTMLENGL.PAS}

{$IFDEF DELPHI4}
procedure OutputDebugStr(s:string);
begin
 OutputDebugString(pchar(s));
end;

procedure OutputDebugInt(i:integer);
begin
 OutputDebugStr(inttostr(i));
end;
{$ENDIF}

function LongMulDiv(Mult1, Mult2, Div1: Longint): Longint; stdcall;
  external 'kernel32.dll' name 'MulDiv';

{$IFDEF WIN32}
function GetFileVersion(filename:string):integer;
var
 filehandle:dword;
 l:integer;
 pvs:PVSFixedFileInfo;
 lptr:uint;
 querybuf:array[0..255] of char;
 buf:pchar;
begin
 result:=-1;

 strpcopy(querybuf,filename);
 l:=getfileversioninfosize(querybuf,filehandle);
 if (l>0) then
  begin
   getmem(buf,l);
   getfileversioninfo(querybuf,filehandle,l,buf);
   if verqueryvalue(buf,'\',pointer(pvs),lptr) then
    begin
     if (pvs^.dwSignature=$FEEF04BD) then
      begin
       result:=pvs^.dwFileVersionMS;
      end;
    end;
   freemem(buf);
  end;
end;
{$ENDIF}

procedure TColumnSize.SetStretch(value:boolean);
begin
 fStretch:=value;
 (Owner as TAdvStringGrid).StretchRightColumn;
end;

{$IFDEF EDITCONTROLS}
constructor TGridCheckBox.Create(aOwner:tComponent);
begin
 inherited Create(aOwner);
 grid:=aOwner as TAdvStringGrid;
end;

procedure TGridCheckBox.DoExit;
begin
 grid.HideEditControl(grid.col,grid.row);
 grid.hideeditor;

 inherited DoExit;
 {
 self.visible:=false;
 parent.setfocus;
 (parent as tAdvStringGrid).topleftchanged;
 }
end;

procedure TGridCheckBox.WMLButtonDown(var Msg:TWMLButtonDown);
begin
 self.toggle;
 inherited;
end;

procedure TGridCheckBox.Keydown(var Key: Word; Shift: TShiftState);
begin
 if (key=vk_space) then self.toggle;
 if (key in [vk_down,vk_up,vk_left,vk_right,vk_prior,vk_next,vk_end,vk_home,vk_return]) then
  begin
   if (key=vk_escape) then self.text:=grid.cells[grid.col,grid.row];
   grid.HideEditControl(grid.col,grid.row);
   grid.hideeditor;
   grid.setfocus;
   if (key=vk_return) then grid.AdvanceEdit(grid.col,grid.row,false,true,true);
   if key in [vk_down,vk_up] then grid.keydown(key,shift);
  end
 else
  inherited;
end;

constructor TGridSpin.Create(aOwner:TComponent);
begin
 inherited Create(aOwner);
 grid:=aOwner as TAdvStringGrid;
 self.maxvalue:=100;
 self.minvalue:=0;
 self.increment:=1;
end;

procedure TGridSpin.UpClick(sender:tObject);
begin
 inherited;
 if assigned(grid.OnSpinClick) then
   grid.OnSpinClick(grid,grid.col,grid.row,self.value,true);
end;

procedure TGridSpin.DownClick(sender:tObject);
begin
 inherited;
 if assigned(grid.OnSpinClick) then
   grid.OnSpinClick(grid,grid.col,grid.row,self.value,false);
end;

procedure TGridSpin.DoExit;
begin
 grid.HideEditcontrol(grid.col,grid.row);
 grid.HideEditor;
 inherited DoExit;
end;

procedure TGridSpin.KeyUp(var Key: Word; Shift: TShiftState);
begin
 inherited KeyUp(key,shift);
end;

procedure TGridSpin.WMChar(var Msg:TWMChar);
begin
 if (msg.charcode=ord(#13)) then msg.result:=0 else
 inherited;
end;

procedure TGridSpin.Keypress(var key:char);
begin
 if (key<>#13) then inherited Keypress(key);
end;

procedure TGridSpin.Keydown(var Key: Word; Shift: TShiftState);
var
 ae:boolean;
begin
 if (key=vk_next) and (self.value-self.increment>=self.minvalue) then
        self.value:=self.value-self.increment;
 if (key=vk_prior) and (self.value+self.increment<=self.maxvalue) then
        self.value:=self.value+self.increment;
 if (key=vk_end) then self.value:=self.minvalue;
 if (key=vk_home) then self.value:=self.maxvalue;

 if assigned(grid.OnSpinClick) then
  begin
   if (key in [vk_prior,vk_home]) then
    grid.OnSpinClick(grid,grid.col,grid.row,self.value,false);
   if (key in [vk_next,vk_end]) then
    grid.OnSpinClick(grid,grid.col,grid.row,self.value,false);
  end;

 if (key in [vk_down,vk_up,vk_escape,vk_return]) then
  begin
   if (key in [vk_return,vk_up,vk_down]) then
    begin
     grid.cells[grid.realcol,grid.row]:=self.text;
     if not grid.ValidateCell(self.text) then
       begin
        self.text:=grid.cells[grid.realcol,grid.row];
        self.selstart:=length(self.text);
        self.repaint;
        exit;
       end;
    end;

   if (key=vk_escape) then self.text:=grid.cells[grid.realcol,grid.row];

   grid.fBlockFocus:=true;

   grid.HideEditControl(grid.col,grid.row);
   grid.HideEditor;

   if key in [vk_down,vk_up] then grid.keydown(key,shift);
   ae:=grid.Navigation.AlwaysEdit;
   grid.Navigation.AlwaysEdit:=false;
   grid.SetFocus;
   if (key=vk_return) then grid.AdvanceEdit(grid.col,grid.row,false,true,true);
   grid.Navigation.AlwaysEdit:=ae;

   grid.fBlockFocus:=false;
 end
 else
  inherited;
end;

procedure TGridCombo.DoExit;
begin
 if grid.lookuphistory and (self.text<>'') then
   begin
    if (items.indexof(text)=-1) then items.Add(text);
   end;
 grid.HideEditcontrol(grid.col,grid.row);
 grid.HideEditor;
 inherited DoExit;
end;

constructor TGridCombo.Create(aOwner:TComponent);
begin
 inherited Create(aOwner);
 workmode:=true;
 grid:=aOwner as TAdvStringGrid;
 buttonwidth:=16;
end;

procedure TGridCombo.SizeDropDownWidth;
var
 i:integer;
 tw,nw:integer;
begin
 tw:=self.Width;

 for i:=1 to self.Items.Count do
  begin
   nw:=6+grid.canvas.textwidth(self.items[i-1]); {account for border size?}
   if (nw>tw) then tw:=nw;
  end;

 sendmessage(self.handle,CB_SETDROPPEDWIDTH,tw,0);
end;

procedure TGridCombo.DoChange;
var
 c:string;
 i:integer;
 UsrStr,AutoAdd:string;

begin
 if not workmode then exit;
 if not grid.lookup then exit;

 c:=upstr(text);
 c:=copy(c,1,selstart);

 if (items.count>0) then
  for i:=0 to items.count-1 do
    begin
     if pos(c,upstr(items[i]))=1 then
      begin
       UsrStr:=copy(text,1,length(c));
       AutoAdd:=copy(items[i],length(c)+1,255);
       text:=UsrStr+AutoAdd;
       SendMessage(Handle,CB_SETEDITSEL,0,makelong(length(c),length(text)));
       exit;
      end;
   end;
end;

procedure TGridCombo.WMSetFocus(var Msg: TWMSetFocus);
var
 lpPoint:tpoint;
 i:integer;
 hwndedit:thandle;
begin
 inherited;

 if not (parent as TAdvStringGrid).lbutflg then exit;

 getcursorpos(lpPoint);
 lpPoint:=screentoclient(lpPoint);

 if (lpPoint.x<0) or (lpPoint.y<0) or
    (lpPoint.x>width) or (lpPoint.y>height) then exit;

 hwndEdit := FindWindowEx(self.handle, 0,nil,nil);
 i:=sendmessage(hwndedit,EM_CHARFROMPOS, 0,makelong(lpPoint.x,lpPoint.y));
 if (i=-1) then exit;

 selstart:=loword(i);
 sellength:=0;
 (parent as TAdvStringGrid).lbutflg:=false;
end;

procedure TGridCombo.Keypress(var Key: char);
begin
 if (key=#13) then key:=#0;
 inherited Keypress(key);
end;

procedure TGridCombo.KeyUp(var Key: Word; Shift: TShiftState);
begin
 inherited KeyUp(key,shift);
 self.DoChange;
end;

procedure TGridCombo.Keydown(var Key: Word; Shift: TShiftState);
var
 ae:boolean;
 condition:boolean;
 comboselstate:boolean;
begin
 forced:=false;
 if (self.style = csDropDownList) or (DroppedDown) then
   condition:=key in [vk_left,vk_right,vk_prior,vk_next,vk_end,vk_home,vk_escape,vk_tab,vk_return]
 else
   condition:=key in [vk_down,vk_up,vk_prior,vk_next,vk_end,vk_home,vk_escape,vk_tab,vk_return];

 workmode:=not (key in [vk_back,vk_delete]);

 comboselstate:=(self.style = csDropDownList) or (DroppedDown);

 if (condition) then
  begin
    if (key=vk_escape) then self.text:=grid.cells[grid.realcol,grid.row];

    if (key in [vk_return,vk_up,vk_down]) then
     begin
      grid.cells[grid.realcol,grid.row]:=self.text;
      if not grid.ValidateCell(self.text) then
        begin
         self.text:=grid.cells[grid.realcol,grid.row];
         self.selstart:=length(self.text);
         self.repaint;
         exit;
        end;
     end;

    grid.fBlockFocus:=true;
    grid.HideEditControl(grid.col,grid.row);
    grid.HideEditor;

    if (comboselstate) then
     begin
      if key in [vk_left,vk_right] then grid.keydown(key,shift)
     end
    else
     begin
      if key in [vk_up,vk_down] then grid.keydown(key,shift);
     end;

    ae:=grid.Navigation.AlwaysEdit;
    grid.Navigation.AlwaysEdit:=false;

    grid.setfocus;

    if (key=vk_return) then grid.AdvanceEdit(grid.col,grid.row,false,true,true);

    grid.Navigation.AlwaysEdit:=ae;

    grid.fBlockFocus:=false;
  end
 else
  inherited;
end;

{$IFDEF DELPHI3_LVL}

procedure TGridDatePicker.WMKeyDown(var Msg:TWMKeydown);
begin
 inherited;
end;

procedure TGridDatePicker.WMNCPaint (var Message: TMessage);
var
 DC: HDC;
 arect:trect;
 windowbrush:hbrush;
begin
 inherited;
 DC:=GetWindowDC(Handle);
 WindowBrush:=0;
 try
  WindowBrush:=CreateSolidBrush(ColorToRGB(clwindow));
  GetWindowRect(Handle, ARect);
  offsetrect(arect,-arect.left,-arect.top);

  FrameRect(DC, ARect, WindowBrush);
  inflaterect(arect,-1,-1);
  FrameRect(DC, ARect, WindowBrush);

 finally
  DeleteObject(windowbrush);
  ReleaseDC(DC, Handle);
 end;
end;

constructor TGridDatePicker.Create(aOwner:tComponent);
begin
 inherited Create(aOwner);
 grid:=aOwner as TAdvStringGrid;
end;

procedure TGridDatePicker.DoExit;
begin
 grid.HideEditControl(grid.col,grid.row);
 grid.HideEditor;
 inherited DoExit;
end;

procedure TGridDatePicker.Keypress(var key:char);
begin
 inherited Keypress(key);
end;

procedure TGridDatePicker.KeyDown(var Key: Word; Shift: TShiftState);
var
 ae:boolean;
begin
 if (key in [vk_escape,vk_down,vk_up,vk_prior,vk_next,vk_end,vk_home,vk_return]) then
  begin
   if (key=vk_escape) then
      try
       self.date:=strtodate(grid.cells[grid.realcol,grid.row]);;
      except
       self.date:=Now;
      end;
   grid.HideEditControl(grid.col,grid.row);
   grid.HideEditor;
   if key in [vk_up,vk_down] then grid.keydown(key,shift);
   ae:=grid.Navigation.AlwaysEdit;
   grid.Navigation.AlwaysEdit:=false;
   grid.setfocus;
   if (key=vk_return) then grid.AdvanceEdit(grid.col,grid.row,false,true,true);
   grid.Navigation.AlwaysEdit:=ae;
  end
 else
  inherited;
end;
{$ENDIF}

{TGridEditBtn}
procedure TGridEditBtn.DoExit;
begin
 grid.HideEditcontrol(grid.col,grid.row);
 grid.HideEditor;
 inherited DoExit;
end;

procedure TGridEditBtn.ExtClick(Sender:tObject);
begin
(parent as tadvstringgrid).ellipsclick;
 with (parent as tstringgrid) do
  begin
   self.text:=cells[grid.realcol,row];
  end;
 self.repaint;
end;

constructor TGridEditBtn.Create(aOwner:TComponent);
begin
 inherited Create(aOwner);
 grid:=aOwner as TAdvStringGrid;
 onclickbtn:=extclick;
end;

procedure TGridEditBtn.WMChar(var Msg:TWMChar);
begin
 if (msg.charcode=ord(#13)) then msg.result:=0 else
 inherited;
end;

procedure TGridEditBtn.Keypress(var key:char);
begin
 if (key<>#13) then inherited Keypress(key);
end;

procedure TGridEditBtn.KeyDown(var Key: Word; Shift: TShiftState);
var
 ae:boolean;
begin
 if (key in [vk_down,vk_up,vk_escape,vk_return,vk_prior,vk_next]) then
  begin
   if (key in [vk_return,vk_up,vk_down]) then
    begin
     grid.cells[grid.realcol,grid.row]:=self.text;
     if not grid.ValidateCell(self.text) then
       begin
        self.text:=grid.cells[grid.realcol,grid.row];
        self.selstart:=length(self.text);
        self.repaint;
        exit;
       end;
    end;
   if (key=vk_escape) then self.text:=grid.cells[grid.realcol,grid.row];

   grid.HideEditControl(grid.col,grid.row);
   grid.HideEditor;

   if key in [vk_down,vk_up] then grid.keydown(key,shift);
   ae:=grid.Navigation.AlwaysEdit;
   grid.Navigation.AlwaysEdit:=false;
   grid.setfocus;
   if (key=vk_return) then grid.AdvanceEdit(grid.col,grid.row,false,true,true);
   grid.Navigation.AlwaysEdit:=ae;
  end
 else
  inherited;
end;

{TGridUnitEditBtn}
procedure TGridUnitEditBtn.DoExit;
begin
 grid.HideEditcontrol(grid.col,grid.row);
 grid.HideEditor;
 inherited DoExit;
end;

constructor TGridUnitEditBtn.Create(aOwner:TComponent);
begin
 inherited Create(aOwner);
 grid:=aOwner as TAdvStringGrid;
end;

procedure TGridUnitEditBtn.WMChar(var Msg:TWMChar);
begin
 if (msg.charcode=ord(#13)) then msg.result:=0 else
 inherited;
end;

procedure TGridUnitEditBtn.KeyDown(var Key: Word; Shift: TShiftState);
var
 ae:boolean;
begin
 if (key in [vk_down,vk_up,vk_escape,vk_return,vk_prior,vk_next]) then
  begin
   if (key in [vk_return,vk_up,vk_down]) then
    begin
     grid.cells[grid.col,grid.row]:=self.text;
     if not grid.ValidateCell(self.text) then
       begin
        self.text:='';
        exit;
       end;
    end;
   if (key=vk_escape) then self.text:=grid.cells[grid.col,grid.row];

   grid.HideEditControl(grid.col,grid.row);
   grid.HideEditor;

   if key in [vk_down,vk_up] then grid.keydown(key,shift);
   ae:=grid.Navigation.AlwaysEdit;
   grid.Navigation.AlwaysEdit:=false;
   grid.setfocus;
   if (key=vk_return) then grid.AdvanceEdit(grid.col,grid.row,false,true,true);
   grid.Navigation.AlwaysEdit:=ae;
  end
 else
  inherited;
end;

{TGridButton}
procedure TGridButton.DoExit;
begin
 grid.HideEditcontrol(grid.col,grid.row);
 grid.HideEditor;
 inherited DoExit;
end;

constructor TGridButton.Create(aOwner:TComponent);
begin
 inherited Create(aOwner);
 grid:=aOwner as TAdvStringGrid;
end;

procedure TGridButton.WMLButtonUp(var Msg:TWMLButtonUp);
begin
 with (parent as tadvstringgrid) do
  begin
   ellipsclick;
   self.caption:=cells[col,row];
  end;
 inherited;
end;

procedure TGridButton.Keyup(var Key: Word; Shift: TShiftState);
begin
 if (key in [vk_left,vk_right,vk_down,vk_up]) then
  begin
   sendmessage(parent.handle,wm_keydown,key,0);
   self.visible:=false;
   self.enabled:=false;
   (parent as tadvstringgrid).editmode:=false;
   parent.setfocus;
   sendmessage(parent.handle,wm_keyup,key,0);
  end;

 if (key=vk_space) then
  begin
   with (parent as tadvstringgrid) do
    begin
     ellipsclick;
     self.caption:=cells[col,row];
    end;
   self.setfocus;
   self.repaint;
  end;
end;

procedure TGridButton.Keydown(var Key: Word; Shift: TShiftState);
begin
 if (key in [{vk_down,vk_up,}vk_escape]) then
  begin
   self.visible:=false;
   self.enabled:=false;
   (parent as tadvstringgrid).editmode:=false;
   parent.setfocus;
   if (key=vk_escape) then (parent as tadvstringgrid).RestoreCache;
   sendmessage(parent.handle,wm_keydown,key,0);
  end
 else
  inherited;
end;

{$ENDIF}

constructor TCellNode.Create(aOwner:TAdvStringGrid);
begin
 inherited Create;
 fColor:=clSilver;
 fNodeColor:=clBlack;
 fNodeType:=cnFlat;
 fContractGlyph:=TBitmap.Create;
 fExpandGlyph:=TBitmap.Create;
 fOwner:=aOwner;
end;


destructor TCellNode.Destroy;
begin
 fContractGlyph.Free;
 fExpandGlyph.Free;
 inherited Destroy;
end;

procedure TCellNode.SetNodeType(value:TNodeType);
begin
 if value<>fNodeType then
  begin
   fNodeType:=value;
   fOwner.Repaint;
  end;
end;

procedure TCellNode.SetExpandGlyph(value:TBitmap);
begin
 fExpandGlyph.Assign(value);
end;

procedure TCellNode.SetContractGlyph(value:TBitmap);
begin
 fContractGlyph.Assign(value);
end;

constructor TSizeWhileTyping.Create;
begin
 inherited Create;
end;

destructor TSizeWhileTyping.Destroy;
begin
 inherited Destroy;
end;

constructor TMouseActions.Create;
begin
 inherited Create;
end;

destructor TMouseActions.Destroy;
begin
 inherited Destroy;
end;


constructor TCellGraphic.Create;
begin
 inherited Create;
end;

destructor TCellGraphic.Destroy;
begin
 if celltype=ctImages then TIntList(CellBitmap).Free;
 inherited Destroy;
end;

procedure TCellGraphic.SetBitmap(abmp:tbitmap;transparent:boolean;hal:tCellHAlign;val:tCellVAlign);
begin
 CellBitmap:=abmp;
 CellType:=ctBitmap;
 CellHAlign:=hal;
 CellVAlign:=val;
 CellTransparent:=transparent;
end;

procedure TCellGraphic.SetPicture(apicture:tpicture;transparent:boolean;hal:tCellHAlign;val:tCellVAlign);
begin
 CellBitmap:=tbitmap(apicture);
 CellType:=ctPicture;
 CellHAlign:=hal;
 CellVAlign:=val;
 CellTransparent:=transparent;
end;


procedure TCellGraphic.SetCheckBox(value,data:boolean;hal:tCellHAlign;val:tCellVAlign);
begin
 if data then CellType:=ctDataCheckBox else CellType:=ctCheckbox;
 CellBoolean:=value;

 CellHAlign:=hal;
 CellVAlign:=val;
 {
 if data then CellHAlign:=haLeft else CellHAlign:=haBeforeText;
 CellVAlign:=vaCenter;
 }
end;

procedure TCellGraphic.SetIcon(aicon:ticon;hal:tCellHAlign;val:tCellVAlign);
begin
 CellIcon:=aicon;
 CellType:=ctIcon;
 CellHAlign:=hal;
 CellVAlign:=val;
end;

procedure TCellGraphic.SetDataImage(idx:integer;hal:tCellHAlign;val:tCellVAlign);
begin
 CellType:=ctDataImage;
 CellIndex:=idx;
 CellHAlign:=hal;
 CellVAlign:=val;
end;

procedure TCellGraphic.SetMultiImage(col,row,dir:integer;hal:tCellHAlign;val:tCellVAlign;notifier:TImageChangeEvent);
begin
 CellType:=ctImages;
 CellHAlign:=hal;
 CellVAlign:=val;
 CellBoolean:=(dir=0);
 CellBitmap:=TBitmap(TIntList.Create(col,row));
 TIntList(CellBitmap).OnChange:=notifier;
end;

procedure TCellGraphic.SetImageIdx(idx:integer;hal:tCellHAlign;val:tCellVAlign);
begin
 CellType:=ctImageList;
 CellIndex:=idx;
 CellHAlign:=hal;
 CellVAlign:=val;
end;

procedure TCellGraphic.SetAngle(aAngle:smallint);
begin
 CellType:=ctRotated;
 while (aAngle<0) do aAngle:=aAngle+360;
 while (aAngle>360) do aAngle:=aAngle-360;
 CellAngle:=aAngle;
end;

constructor TColumnSize.Create(aOwner:TComponent);
begin
 inherited Create;
 Owner:=aOwner;
end;

destructor TColumnSize.Destroy;
begin
 inherited Destroy;
end;

constructor TNavigation.Create;
begin
 inherited Create;
end;

destructor TNavigation.Destroy;
begin
 inherited Destroy;
end;

procedure TNavigation.SetAutoGoto(avalue:boolean);
begin
 FAutoGotoWhenSorted:=avalue;
end;

constructor THTMLSettings.Create;
begin
 inherited Create;
 FSaveColor:=true;
 FSaveFonts:=true;
 FBorderSize:=1;
 FCellSpacing:=0;
 FWidth:=100;
end;

constructor TPrintSettings.Create;
begin
 inherited Create;
 FFont:=TFont.Create;
 FHeaderFont:=TFont.Create;
 FFooterFont:=TFont.Create;
 FTitleLines:=TStringList.Create;
 FPagePrefix:='';
 FPageSuffix:='';
 FPageNumSep:='/';
 FDateFormat:='dd/mm/yyyy';
 FTitleSpacing:=0;
end;

destructor TPrintSettings.Destroy;
begin
 FFont.Free;
 FHeaderFont.Free;
 FFooterFont.Free;
 FTitleLines.Free;
 inherited Destroy;
end;

procedure TPrintSettings.SetPrintFont(value:tFont);
begin
 FFont.Assign(value);
end;

procedure TPrintSettings.SetPrintHeaderFont(value:tFont);
begin
 FHeaderFont.Assign(value);
end;

procedure TPrintSettings.SetPrintFooterFont(value:tFont);
begin
 FFooterFont.Assign(value);
end;

procedure TPrintSettings.SetTitleLines(value:tstringlist);
begin
 FTitleLines.Assign(value);
end;


{$IFDEF EDITCONTROLS}
function ShiftCase(Name: string): string;

 function LowCase(C: char): char;
 begin
  if C in ['A' .. 'Z'] then LowCase := Chr(Ord(C) - Ord('A') + Ord('a'))
  else Lowcase := C;
 end;

const
  Terminators = [' ',',','.','-',''''];
var
  I, L: integer;
  NewName: string;
  First: boolean;
begin
  First:= true;
  NewName:= Name;
  L := Length(Name);
  for I:= 1 to L do begin
    if NewName[I] in Terminators then First:= true
    else if First then begin
      NewName[I]:= Upcase(Name[I]);
      First:= false;
    end
    else NewName[I]:= Lowcase(Name[I]);
    if (Copy(NewName, 1, I) = 'Mc') or
      ((Pos (' Mc', NewName) = I - 2) and (I > 2)) or
      ((I > L - 3) and ((Copy(NewName, I - 1, 2) = ' I') or
        (Copy(NewName, I - 2, 3) = ' II'))) then
          First:= true;
  end;
  ShiftCase := NewName;
end;

procedure TAdvInplaceEdit.WMPaste(var Msg: TMessage);
var
 Data:thandle;
 content:pchar;
 s,ct:string;
 i:integer;
 len:smallint;
begin
 if ClipBoard.HasFormat(CF_TEXT) then
  begin
   ClipBoard.Open;
   Data:=GetClipBoardData(CF_TEXT);
   try
    if (Data<>0) then content:=PChar(GlobalLock(Data))
                 else content:=nil
   finally
    if (Data<>0) then GlobalUnlock(Data);
   ClipBoard.Close;
   end;
   if content=nil then exit;

   len:=strlen(content);
   if (fLengthLimit>0) and (len>fLengthLimit) then exit;
   s:='';
   case (parent as TAdvStringGrid).EditControl of
   edNumeric:begin
              if IsType(strpas(content))=atNumeric then s:=strpas(content);
             end;
   edFloat:begin
              if IsType(strpas(content)) in [atNumeric,atFloat] then s:=strpas(content);
             end;
   edLowerCase:s:=AnsiLowerCase(strpas(content));
   edUpperCase:s:=AnsiUpperCase(strpas(content));
   edMixedCase:s:=ShiftCase(strpas(content));
   else
    s:=strpas(content);
   end;
   if (s<>'') then
    begin
     ct:=self.text;
     i:=selstart;
     system.delete(ct,selstart+1,sellength);
     system.insert(s,ct,selstart+1);
     self.text:=ct;
     selstart:=i+length(s);
    end;
  end;
end;

procedure TAdvInplaceEdit.DoChange;
var
 s,c,d:string;
 i:integer;
 se,ss:integer;
 {$IFNDEF WIN32}
 tmp:tstringlist;
 {$ENDIF}

begin
 sendMessage(Handle,EM_GETSEL,integer(@se),integer(@ss));

 if not workmode then exit;

 if (ss<>se) then exit;

 if (parent as TAdvStringGrid).LookupCaseSensitive then
  c:=edittext
 else
  c:=upstr(edittext);

 c:=copy(c,1,selstart);

 {$IFNDEF WIN32}
 tmp:=(parent as TAdvStringGrid).LookupItems;
 if not assigned(tmp) then exit;
 {$ELSE}
 if not assigned((parent as TAdvStringGrid).LookupItems) then exit;
 {$ENDIF}

 if ((parent as TAdvStringGrid).LookupItems.count>0) and
    ((parent as TAdvStringGrid).Lookup) then
  for i:=0 to (parent as TAdvStringGrid).LookupItems.count-1 do
    begin
      if (parent as TAdvStringGrid).LookupCaseSensitive then
        d:=(parent as TAdvStringGrid).LookupItems.Strings[i]
      else
        d:=upstr((parent as TAdvStringGrid).LookupItems.Strings[i]);

     if (pos(c,d)=1) then
      begin
       s:=copy(text,1,length(c))+copy((parent as TAdvStringGrid).LookupItems.Strings[i],length(c)+1,255);
       edittext:=s;
       SendMessage(Handle,EM_SETSEL,length(c),length(s));
       gotkey:=false;
       exit;
      end;
   end;
end;

procedure TAdvInplaceEdit.WMChar(var Msg: TWMKey);
var
 oldselstart:integer;
 s:string;
begin
 if (flengthlimit>0) and (length(text)=flengthlimit)
    and (msg.charcode<>vk_back) then exit;

 case (parent as TAdvStringGrid).EditControl of
 edNormal,edPassword:inherited;
 edNumeric:if msg.charcode in Numeric_Characters then inherited else messagebeep(0);
 edFloat:begin
           if msg.charcode in Float_Characters then
             begin
             if ((msg.charcode=ord('.')) or
                 (msg.charcode=ord(','))) and
                ((pos('.',self.text)>0) or
                 (pos(',',self.text)>0)) then exit;
             if (msg.charcode=ord('-')) and
                ((selstart<>0) or (pos('-',self.text)>0)) then
                 begin
                  messagebeep(0);
                  exit;
                 end;
             inherited;
             end;
         end;
  edCapital,edUpperCase:begin
             s:=ansiUpperCase(chr(msg.charcode));
             msg.charcode:=ord(s[1]);
             inherited;
            end;
  edLowerCase:begin
               s:=ansiLowerCase(chr(msg.charcode));
               msg.charcode:=ord(s[1]);
               inherited;
              end;
  edMixedCase:begin
                oldselstart:=selstart;
                inherited;
                self.text:=shiftcase(self.text);
                selstart:=oldselstart+1;
              end;
 end;
end;
{$ENDIF}

procedure TAdvInplaceEdit.WMKillFocus(var Msg: TWMKillFocus);
begin
 if (parent as TAdvStringGrid).LookupHistory then
  begin
   if ((parent as TAdvStringGrid).LookupItems.IndexOf(Text)=-1) then
     begin
      (parent as TAdvStringGrid).LookupItems.Add(Text);
     end;
  end;
 inherited;
end;

procedure TAdvInplaceEdit.WMSetFocus(var Msg: TWMSetFocus);
var
 lpPoint:tPoint;
 i:integer;
begin
 inherited;
 {$IFDEF WIN32}
 with (parent as TAdvStringGrid) do
  begin
   if assigned(OnGetCellColor) then
    begin
     canvas.brush.color:=color;
     OnGetCellColor(self.parent,row,col,[],canvas.brush,canvas.font);
     self.color:=canvas.brush.color;
    end;
  end;
 if (editmask<>'') then
  with (parent as TAdvStringGrid) do
   begin
    if not lbutflg and Navigation.ImproveMaskSel then
      begin
       selstart:=0;
       sellength:=1;
       exit;
     end;
   end;
 if not (parent as TAdvStringGrid).lbutflg then exit;
 getcursorpos(lpPoint);
 lpPoint:=screentoclient(lpPoint);

 if (lpPoint.x<0) or (lpPoint.y<0) or
    (lpPoint.x>width) or (lpPoint.y>height) then exit;

 i:=sendmessage(self.handle,EM_CHARFROMPOS, 0,makelong(lpPoint.x,lpPoint.y));
 if (i=-1) then exit;

 selstart:=loword(i);
 sellength:=0;
 {$ENDIF}
 (parent as TAdvStringGrid).lbutflg:=false;
end;

procedure TAdvInplaceEdit.CreateWnd;
begin
 inherited CreateWnd;
 if (parent as TAdvStringGrid).editcontrol=edPassword then
  SendMessage(self.Handle, EM_SETPASSWORDCHAR, Ord((parent as TAdvStringGrid).PassWordChar), 0);
end;

procedure TAdvInplaceEdit.CreateParams(var Params:TCreateParams);
const
 WordWraps: array[Boolean] of LongInt = (0,ES_AUTOHSCROLL);
begin
 inherited CreateParams(params);
 fwordwrap:=(parent as TAdvStringGrid).wordwrap;

 if (fvalign) then
  begin
   params.style:=params.style AND NOT (ES_LEFT);
   params.style:=params.style OR (ES_RIGHT);
  end;

 if (parent as TAdvStringGrid).editcontrol=edPassword then
  begin
   params.style:=params.style OR ES_PASSWORD;
   {multiline conflicts with ES_PASSWORD!}
   params.style:=params.style AND NOT ES_MULTILINE;
  end;

 params.style:=params.style AND NOT WordWraps[fwordwrap];
 gotkey:=false;
 workmode:=true;
end;

{$IFDEF WIN32}
procedure TAdvInplaceEdit.UpdateContents;
begin
 inherited UpdateContents;
end;
{$ENDIF}

procedure TAdvInplaceEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
 case Key of
 vk_back,vk_delete:workmode:=false;

 vk_right:begin
            if (self.sellength=0) and (self.selstart=length(text)) and (shift=[]) then
             with (parent as TAdvStringGrid) do
              begin
                if Navigation.CursorWalkEditor then
                  begin
                   AdvanceEdit(col,row,true,true,true);
                   key:=vk_return;
                  end;
              end;
          end;
 vk_left:begin
            if (self.sellength=0) and (self.selstart=0) and (shift=[]) then
             with (parent as TAdvStringGrid) do
               begin
                if Navigation.CursorWalkEditor then
                  begin
                   AdvanceEdit(col,row,true,true,false);
                   key:=vk_return;
                  end;
               end;
         end;
 vk_up:begin
         if sendmessage(self.handle,EM_LINEFROMCHAR,selstart,0)>0 then exit;
       end;
 vk_down:begin
          if (sendmessage(self.handle,EM_LINEFROMCHAR,selstart,0)<
             sendmessage(self.handle,EM_LINEFROMCHAR,length(self.text),0)) then exit;
         end;
 {
 vk_up:begin
         if (shift=[]) then
          begin
            key:=vk_return;
            with (parent as TAdvStringGrid) do
             begin
              if (row>fixedrows) then row:=row-1;
              showeditor;
            end;

          end;
       end;
 vk_down:begin
         if (shift=[]) then
          begin
            key:=vk_return;
            with (parent as TAdvStringGrid) do
             begin
              if (row+1<rowcount) then row:=row+1;
              showeditor;
             end;
          end;
         end;
 }
 vk_return:begin
             {ctrl-enter pressed for a new line}
             if (ssCtrl in Shift) then
              with (parent as TAdvStringGrid) do
                if  (FSizeWhileTyping.Height) and (MultiLineCells) then
                 begin
                  if LinesInText(self.text+#13#10+' ',fMultiLineCells)<MaxLinesInRow(row) then
                     AutoSizeRow(row)
                  else
                     SizeToLines(row,LinesInText(self.text+#13#10+' ',fMultiLineCells),0);
                 end;

             {
             if ((parent as TAdvStringGrid).LookupItems.IndexOf(Text)<>-1) then
              begin
               text:=(parent as TAdvStringGrid).LookupItems.Strings[(parent as TAdvStringGrid).LookupItems.IndexOf(Text)];
               self.Change;
               postmessage(self.handle,wm_keydown,VK_TAB,0);
              end;
             }
           end
  else workmode:=true;
 end;
 inherited KeyDown(key,shift);
end;

procedure TAdvInplaceEdit.KeyUp(var Key: Word; Shift: TShiftState);
(*
var
 r:trect;
 mh:integer;
 {$IFNDEF WIN32}
 buf:array[0..255] of char;
 {$ENDIF}
*)
begin
 inherited KeyUp(key,shift);

 if not (key in [VK_LEFT, VK_RIGHT]) then self.Dochange;

 with (parent as TAdvStringGrid) do
  begin
   if FSizeWhileTyping.Width then
    begin
     SizeToWidth(col,true);
    end;

   if FSizeWhileTyping.Height and WordWrap then
    begin
     AutoSizeRow(row);
     (*
     r:=cellrect(col,row);
     inc(r.left,2);
     dec(r.right,2);
     inc(r.top,2);

     {$IFNDEF WIN32}
     strpcopy(buf,self.text);
     mh:=DrawText(Canvas.Handle,buf,StrLen(buf),R,(DT_EXPANDTABS or DT_NOPREFIX)
     or DT_WORDBREAK or fValign or DT_CALCRECT);
     {$ELSE}
     mh:=DrawText(Canvas.Handle,pchar(self.text),Length(self.text),R,(DT_EXPANDTABS or DT_NOPREFIX)
     or DT_WORDBREAK or fValign or DT_CALCRECT);
     {$ENDIF}
     if (mh+2>rowHeights[row]) then
      rowHeights[row]:=mh+4;
     *)
    end;
  end;
end;

procedure TAdvInplaceEdit.KeyPress(var Key: Char);
begin
 if (key=#13) then

 with (parent as TAdvStringGrid) do
  if not validatecell(self.text) then
    begin
     key:=#0;
     exit;
    end;

 inherited Keypress(key);

 if tadvstringgrid(parent).navigation.advanceauto then
 if (pos(' ',self.text)=0) and (self.selstart=length(self.text)) and (self.editmask<>'') then
  begin
   key:=#13;
   tadvstringgrid(parent).keypress(key);
  end;
end;

procedure TAdvInplaceEdit.SetvAlign(value:boolean);
begin
 fvalign:=value;
 recreatewnd;
end;

procedure TAdvInplaceEdit.SetWordWrap(value:boolean);
begin
 fwordwrap:=value;
 recreatewnd;
end;

function TAdvStringGrid.CreateEditor:TInplaceEdit;
begin
 result:=TAdvInplaceEdit.Create(self);
end;

constructor TAdvStringGrid.Create(aOwner:tComponent);
var
{$IFNDEF WIN32}
 tmp:tshowhintevent;
 tmpr:tonresizeevent;
{$ENDIF}
 i:integer;
{$IFDEF WIN32}
 scrollmsg:integer;
 mshwheel:thandle;
 verinfo:tosversioninfo;
{$ENDIF}

begin
 inherited Create(aOwner);
 FPrintSettings:=TPrintSettings.Create;
 FHTMLSettings:=THTMLSettings.Create;
 FNavigation:=TNavigation.Create;
 FColumnSize:=TColumnSize.Create(self);
 FCellNode:=TCellNode.Create(self);
 FSizeWhileTyping:=TSizeWhileTyping.Create;
 FMouseActions:=TMouseActions.Create;
 FColumnHeaders:=TStringList.Create;
 FColumnHeaders.OnChange:=ColHeaderChanged;
 FFixedFont:=tfont.Create;
 FFixedFont.OnChange:=FixedFontChanged;
 FRowHeaders:=TStringList.Create;
 FRowHeaders.OnChange:=RowHeaderChanged;
 SortList:=TStringList.Create;
 FLookupItems:=TStringList.Create;
 FRowSelect:=TList.Create;
 FSortIndexes:=TIntList.Create(0,0);
 FSortUpGlyph:=TBitmap.Create;
 FSortDownGlyph:=TBitmap.Create;
 FBackground:=TBackground.Create(self);
 FScrollHintWnd:=THintWindow.Create(self);
 FScrollHintShow:=false;
 FDeselectState:=false;
 FMouseDown:=false;
 FUpdateCount:=0;
 FGroupColumn:=-1;
 FClipTopLeft:=point(-1,-1);
 FSortFull:=true;
 FSortAutoFormat:=true;
 FGridItems:=tcollection.Create(TGridItem);
 FFilter:=tfilter.Create(self);
 autosize:=false;
 invokedchange:=false;
 fHintColor:=clYellow;
 fSelectionColor:=clHighLight;
 fSelectionTextColor:=clHighLightText;
 fVAlignment:=vtaTop;
 {$IFDEF DELPHI3_4_ONLY}
 fOleDropTarget:=false;
 fOleDropTargetAssigned:=false;
 {$ENDIF}

 {$IFDEF DELPHI3_LVL}
 if not (csDesigning in ComponentState) then
 begin
  arwu:=tarrowwindow.Create(self,arrUp);
  arwu.parent:=self;
  arwu.visible:=false;
  arwd:=tarrowwindow.Create(self,arrDown);
  arwd.parent:=self;
  arwd.visible:=false;
  arwl:=tarrowwindow.Create(self,arrLeft);
  arwl.parent:=self;
  arwl.visible:=false;
  arwr:=tarrowwindow.Create(self,arrRight);
  arwr.parent:=self;
  arwr.visible:=false;
 end;
 {$ENDIF}


 showhintassigned:=false;
 resizeassigned:=false;
 fsavefixedcells:=true;
 fFindBusy:=false;
 fComboIdx:=-1;
 fPrintPageFrom:=1;
 fPrintPageTo:=1;
 fPrintPageNum:=0;
 {$IFDEF WIN32}
 fPrinterDriverFix:=false;
 fExcelClipboardFormat:=false;
 {$ENDIF}

 FOnShowHint:=nil;

 {$IFNDEF SHOWHINT}
 if not (csDesigning in ComponentState) then
  begin
    {$IFNDEF WIN32}
    tmp:=Application.OnshowHint;
    if assigned(tmp) then
    {$ELSE}
    if assigned(Application.OnshowHint) then
    {$ENDIF}
    begin
      FOnShowHint:=Application.OnshowHint;
      showhintassigned:=true;
    end;
    Application.Onshowhint:=ShowHintProc;
  end;
 {$ENDIF}

 FLastHintPos:=point(-1,-1);

 if (aowner is tform) and not (csDesigning in ComponentState) then
  begin
   {$IFNDEF WIN32}
   tmpr:=(aowner as tform).OnResize;
   if assigned(tmpr) then
   {$ELSE}
   if assigned( (aowner as tform).OnResize ) then
   {$ENDIF}
     begin
      FOnResize:=(aowner as tform).OnResize;
      resizeassigned:=true;
     end;

   (aowner as tform).OnResize:=GridResize;
   prevsizex:=(aowner as tform).width;
   prevsizey:=(aowner as tform).height;
  end;


 for i:=0 to MaxColumns do fvisiblecol[i]:=true;

 FNumHidden:=0;
 FDelimiter:=#0;
 FPasswordChar:='*';
 FJavaCSV:=false;
 FCheckTrue:='Y';
 FCheckFalse:='N';
 DefaultRowHeight:=21;
 zoomfactor:=0;
 colchgflg:=true;
 colclicked:=-1;
 rowclicked:=-1;
 colsized:=false;
 searchinc:='';
 lbutflg:=false;

 {$IFNDEF DLLUSAGE}
 Screen.cursors[crURLcursor]:=LoadCursor(HInstance,PChar(crURLcursor));
 Screen.cursors[crHorzArr]:=LoadCursor(HInstance,PChar(crHorzArr));
 Screen.cursors[crVertArr]:=LoadCursor(HInstance,PChar(crVertArr));
 Screen.cursors[crAsgCross]:=LoadCursor(HInstance,PChar(crAsgCross));
 {$ENDIF}

{$IFDEF WIN32}

 fIsFlat:=false;
 fScrollType:=ssNormal;
 fScrollColor:=clNone;
 fScrollWidth:=GetSystemMetrics(SM_CXVSCROLL);

 wheelmsg:=0;
 wheelscrl:=0;
 wheelpan:=false;

 verinfo.dwOSVersionInfoSize:=sizeof(tosversioninfo);
 getversionex(verinfo);

 if ((verinfo.dwPlatformId = VER_PLATFORM_WIN32_WINDOWS) AND
    ( (verinfo.dwMajorVersion <4) OR
      (verinfo.dwMajorVersion =4) AND (verinfo.dwMinorVersion<10)) ) or
    ((verinfo.dwPlatformId = VER_PLATFORM_WIN32_NT) AND
     (verinfo.dwMajorVersion < 4)) then
   begin
   {$IFDEF DEBUG}
   outputdebugstring('win nt 3.x found or win95 found');
   {$ENDIF}
   mshwheel:=FindWindow(MSH_WHEELMODULE_CLASS,MSH_WHEELMODULE_TITLE);
   if (mshwheel>0) then
    begin
     scrollmsg:=RegisterWindowMessage(MSH_SCROLL_LINES);
     wheelmsg:=RegisterWindowMessage(MSH_SCROLL_LINES);
     wheelscrl:=SendMessage(mshwheel,scrollmsg,0,0);
    end;
   end
  else {its win nt 4+ or Win98 ?}
   begin
    {$IFDEF DEBUG}
    outputdebugstring('win nt 4 found or win98 found');
    {$ENDIF}
    SystemParametersInfo(SPI_GETWHEELSCROLLLINES,0,@i,0);
    wheelscrl:=i;
    wheelmsg:=WM_MOUSEWHEEL;
   end;
 fRichEdit:=TAdvRichEdit.Create(self);
{$ENDIF}

 {$IFDEF SPREADSHEET}
 fGridFormula:=tGridFormula.Create(self);
 {$ENDIF}

 {$IFDEF HEADER}
 fHeader:=tHeader.Create(self);
 fHeader.parent:=self;
 fHeader.height:=defaultrowheight;
 {$ENDIF}

 {$IFDEF EDITCONTROLS}
 if not (csDesigning in ComponentState) then
  begin
   editcombo:=tgridcombo.create(self);
   editcombo.parent:=self;
   editcombo.enabled:=false;
   editcombo.visible:=false;

   editmode:=false;
   editcontrol:=edNormal;

   editspin:=tgridspin.create(self);
   editspin.parent:=self;
   editspin.enabled:=false;
   editspin.visible:=false;
   editspin.borderstyle:=bsNone;

   {$IFDEF DELPHI3_LVL}
   if (getfileversion(comctrl)>=$00040046) then
    begin
     editdate:=tgriddatepicker.create(self);
     editdate.parent:=self;
     editdate.enabled:=false;
     editdate.visible:=false;
    end;
   {$ENDIF}

   editcheck:=tgridcheckbox.create(self);
   editcheck.parent:=self;
   editcheck.enabled:=false;
   editcheck.visible:=false;

   editbtn:=tgrideditbtn.create(self);
   editbtn.parent:=self;
   editbtn.enabled:=false;
   editbtn.visible:=false;
   editbtn.buttoncaption:='...';
   editbtn.borderstyle:=bsNone;

   uniteditbtn:=tgriduniteditbtn.create(self);
   uniteditbtn.parent:=self;
   uniteditbtn.enabled:=false;
   uniteditbtn.visible:=false;
   uniteditbtn.buttoncaption:='...';
   uniteditbtn.borderstyle:=bsNone;

   gridbutton:=tgridbutton.create(self);
   gridbutton.parent:=self;
   gridbutton.enabled:=false;
   gridbutton.visible:=false;

   movebutton:=tgridbutton.create(self);
   movebutton.parent:=self;
   movebutton.enabled:=false;
   movebutton.visible:=false;
  end;
 {$ENDIF}

end;

{---------------------------------------------
 v 0.98 : added code to save column widths to
          ini file or registry
----------------------------------------------}
destructor TAdvStringGrid.Destroy;
begin
 if (owner is tform) then {restore owner resize handler}
  begin
   (owner as tform).OnResize:=FOnResize;
  end;

 if not (csDesigning in ComponentState) then
 if (fHovering) then killtimer(self.handle,fGridTimerID);

 if (FColumnSize.Save) then SaveColSizes;

 self.clear;

 {$IFNDEF SHOWHINT}
 if not (csDesigning in ComponentState) then
  begin
   Application.OnShowHint:=FOnShowHint;
  end;
 {$ENDIF}

 FPrintSettings.Free;
 FHTMLSettings.Free;
 FNavigation.Free;
 FColumnSize.Free;
 FCellNode.Free;
 FSizeWhileTyping.Free;
 FMouseActions.Free;
 FColumnHeaders.Free;
 FFixedFont.Free;
 FRowHeaders.Free;
 FLookupItems.Free;
 FRowSelect.Free;
 FSortIndexes.Free;
 FSortUpGlyph.Free;
 FSortDownGlyph.Free;
 FBackground.Free;
 FScrollHintWnd.Free;

 {$IFDEF DELPHI3_LVL}
 if not (csDesigning in ComponentState) then
  begin
   ArwU.Free;
   ArwD.Free;
   ArwL.Free;
   ArwR.Free;
  end;
 {$ENDIF}

 SortList.Free;

 FGridItems.Free;
 FFilter.Free;

 Cursor:=foldcursor;
 {$IFDEF WIN32}
  fRichEdit.Free;
 {$ENDIF}

 {$IFDEF SPREADSHEET}
 fGridFormula.Free;
 {$ENDIF}

 {$IFDEF EDITCONTROLS}
 if not (csDesigning in ComponentState) then
  begin
   editcombo.free;
   editspin.free;
   {$IFDEF DELPHI3_LVL}
   if (GetFileVersion(comctrl)>=$00040046) then editdate.free;
   {$ENDIF}
   editcheck.free;
   editbtn.free;
   uniteditbtn.Free;
   gridbutton.free;
   movebutton.free;
  end;
 {$ENDIF}
 inherited Destroy;
end;

procedure TAdvStringGrid.DestroyWnd;
begin
 inherited DestroyWnd;
end;

procedure TAdvStringGrid.CreateWnd;
begin
 if not (parent is TWinControl) then exit;
// messagedlg('in here',mtinformation,[mbok],0);
 inherited CreateWnd;
 {$IFDEF WIN32}
 fRichEdit.Parent:=self;
 fRichEdit.left:=0;
 fRichEdit.width:=0;
 fRichEdit.visible:=false;
 fRichEdit.BorderStyle:=bsNone;
 {$ENDIF}
 if (FColumnSize.Save) then LoadColSizes;
end;

{------------------------------------------
 v 0.98 : added loaded proc for setting
          save column width values
-------------------------------------------}
procedure TAdvStringGrid.Loaded;
begin
 {$IFDEF WIN32}
 inherited;
 {$ENDIF}
 foldcursor:=self.cursor;
 ShowColumnHeaders;
 ShowRowHeaders;
end;

procedure TAdvStringGrid.SaveColSizes;
var
 i:integer;
 {$IFDEF DELPHI4_LVL}
 inifile:tcustominifile;
 {$ELSE}
 inifile:tinifile;
 {$ENDIF}

begin
 if (FColumnSize.Key<>'') and
    (FColumnSize.Section<>'') and
    (not (csDesigning in ComponentState)) then
  begin
   {$IFDEF DELPHI4_LVL}
   if FColumnSize.Location = clRegistry then
    inifile:=tregistryinifile.Create(FColumnSize.key)
   else
   {$ENDIF}
    inifile:=tinifile.Create(FColumnSize.key);

   with inifile do
    begin
     for i:=0 to colcount-1 do
      begin
       writeinteger(FColumnSize.section,'col'+inttostr(i),colwidths[i]);
      end;
    end;
   inifile.Free;
  end;
end;

procedure TAdvStringGrid.LoadColSizes;
var
 i:integer;
 {$IFDEF DELPHI4_LVL}
 inifile:tcustominifile;
 {$ELSE}
 inifile:tinifile;
 {$ENDIF}
 newwidth:integer;
begin
 if (FColumnSize.Key<>'') and
    (FColumnSize.Section<>'') and
    (not (csDesigning in ComponentState)) then
  begin
   {$IFDEF DELPHI4_LVL}
   if FColumnSize.location=clRegistry then
    inifile:=tregistryinifile.Create(FColumnSize.key)
   else
   {$ENDIF}
    inifile:=tinifile(tinifile.Create(FColumnSize.key));

   with inifile do
    begin
     for i:=0 to colcount-1 do
      begin
       newwidth:=readinteger(FColumnSize.section,'col'+inttostr(i),-1);
       if (newwidth<>-1) then
         begin
          colwidths[i]:=newwidth;
         end;
      end;
    end;
   inifile.Free;
  end;
end;

procedure TAdvStringGrid.SavePrintSettings(key,section:string);
var
 inifile:tinifile;

 function Bool2String(b:boolean):string;
 begin
  if b then result:='Y' else result:='N';
 end;

 function Set2Int(fs:TFont):integer;
 begin
  result:=0;
  if (fsBold in fs.Style) then result:=result+1;
  if (fsItalic in fs.Style) then result:=result+2;
  if (fsUnderLine in fs.Style) then result:=result+4;
  if (fsStrikeOut in fs.Style) then result:=result+8;
 end;


begin
 inifile:=tinifile.Create(key);

 inifile.writeinteger(section,'ColumnSpacing',fPrintSettings.ColumnSpacing);
 inifile.WriteInteger(section,'RowSpacing',fPrintSettings.RowSpacing);
 inifile.WriteInteger(section,'TitleSpacing',fPrintSettings.TitleSpacing);

 inifile.writeinteger(section,'FixedHeight',fPrintSettings.FixedHeight);
 inifile.writeinteger(section,'FixedWidth',fPrintSettings.FixedWidth);

 inifile.WriteString(section,'Centered',bool2string(fPrintSettings.Centered));
 inifile.WriteString(section,'NoAutoSize',bool2string(fPrintSettings.NoAutoSize));
 inifile.WriteString(section,'UseFixedHeight',bool2string(fPrintSettings.UseFixedHeight));
 inifile.WriteString(section,'UseFixedWidth',bool2string(fPrintSettings.UseFixedWidth));
 inifile.WriteString(section,'RepeatFixedRows',bool2string(fPrintSettings.RepeatFixedRows));
 inifile.WriteString(section,'RepeatFixedCols',bool2string(fPrintSettings.RepeatFixedCols));

 inifile.WriteInteger(section,'Borders',ord(fPrintSettings.Borders));
 inifile.WriteInteger(section,'BorderStyle',ord(fPrintSettings.BorderStyle));
 inifile.WriteInteger(section,'Date',ord(fPrintSettings.Date));
 inifile.WriteInteger(section,'PageNr',ord(fPrintSettings.PageNr));
 inifile.WriteInteger(section,'Title',ord(fPrintSettings.Title));
 inifile.WriteInteger(section,'Time',ord(fPrintSettings.Time));

 inifile.WriteString(section,'DateFormat',fPrintSettings.DateFormat);
 inifile.WriteString(section,'TitleText',fPrintSettings.TitleText);

 inifile.WriteInteger(section,'FooterSize',fPrintSettings.FooterSize);
 inifile.WriteInteger(section,'HeaderSize',fPrintSettings.HeaderSize);
 inifile.WriteInteger(section,'LeftSize',fPrintSettings.LeftSize);
 inifile.WriteInteger(section,'RightSize',fPrintSettings.RightSize);

 inifile.WriteInteger(section,'FitToPage',ord(fPrintSettings.FitToPage));

 inifile.WriteString(section,'FontName',fPrintSettings.Font.name);
 inifile.WriteInteger(section,'FontSize',fPrintSettings.Font.Size);
 inifile.WriteInteger(section,'FontStyle',Set2Int(fPrintSettings.Font));
 inifile.WriteInteger(section,'FontColor',ord(fPrintSettings.Font.Color));

 inifile.WriteString(section,'HeaderFontName',fPrintSettings.HeaderFont.name);
 inifile.WriteInteger(section,'HeaderFontSize',fPrintSettings.HeaderFont.Size);
 inifile.WriteInteger(section,'HeaderFontStyle',Set2Int(fPrintSettings.HeaderFont));
 inifile.WriteInteger(section,'HeaderFontColor',ord(fPrintSettings.HeaderFont.Color));

 inifile.WriteString(section,'FooterFontName',fPrintSettings.FooterFont.name);
 inifile.WriteInteger(section,'FooterFontSize',fPrintSettings.FooterFont.Size);
 inifile.WriteInteger(section,'FooterFontStyle',Set2Int(fPrintSettings.FooterFont));
 inifile.WriteInteger(section,'FooterFontColor',ord(fPrintSettings.FooterFont.Color));

 inifile.WriteString(section,'PageNumSep',fPrintSettings.PageNumSep);
 inifile.WriteString(section,'PageSuffix',fPrintSettings.PageSuffix);
 inifile.WriteString(section,'PagePrefix',fPrintSettings.PagePrefix);

 inifile.WriteInteger(section,'Orientation',ord(fPrintSettings.Orientation));

 inifile.Free;

 {
   With Dest do begin
    JobName := Source.JobName;
    TitleLines.Assign(Source.TitleLines);
  end;
  }

end;

procedure TAdvStringGrid.LoadPrintSettings(key,section:string);
var
 inifile:tinifile;

 function Int2Set(i:integer;fs:TFont):integer;
 begin
  result:=0;
  if (i and 1>0) then fs.Style:=fs.Style+[fsBold];
  if (i and 2>0) then fs.Style:=fs.Style+[fsItalic];
  if (i and 4>0) then fs.Style:=fs.Style+[fsUnderLine];
  if (i and 8>0) then fs.Style:=fs.Style+[fsStrikeOut];
 end;

 function Int2Pos(i:integer):TPrintPosition;
 begin

  case i of
   0:result:=ppNone;
   1:result:=ppTopLeft;
   2:result:=ppTopRight;
   3:result:=ppTopCenter;
   4:result:=ppBottomLeft;
   5:result:=ppBottomRight;
   6:result:=ppBottomCenter;
  else result:=ppNone; 
  end;
 end;


begin
 inifile:=tinifile.Create(key);

 fPrintSettings.ColumnSpacing:=inifile.readinteger(section,'ColumnSpacing',20);
 fPrintSettings.RowSpacing:=inifile.readInteger(section,'RowSpacing',20);
 fPrintSettings.TitleSpacing:=inifile.readInteger(section,'TitleSpacing',20);

 fPrintSettings.FixedHeight:=inifile.readinteger(section,'FixedHeight',0);
 fPrintSettings.FixedWidth:=inifile.readinteger(section,'FixedWidth',0);

 fPrintSettings.Centered:=inifile.ReadString(section,'Centered','Y')='Y';
 fPrintSettings.NoAutoSize:=inifile.ReadString(section,'NoAutoSize','N')='Y';
 fPrintSettings.UseFixedHeight:=inifile.ReadString(section,'UseFixedHeight','N')='Y';
 fPrintSettings.UseFixedWidth:=inifile.ReadString(section,'UseFixedWidth','N')='Y';
 fPrintSettings.RepeatFixedRows:=inifile.ReadString(section,'RepeatFixedRows','N')='Y';
 fPrintSettings.RepeatFixedCols:=inifile.ReadString(section,'RepeatFixedCols','N')='Y';

 fPrintSettings.DateFormat:=inifile.ReadString(section,'DateFormat','');
 fPrintSettings.TitleText:=inifile.ReadString(section,'TitleText','');

 fPrintSettings.FooterSize:=inifile.ReadInteger(section,'FooterSize',100);
 fPrintSettings.HeaderSize:=inifile.ReadInteger(section,'HeaderSize',100);
 fPrintSettings.LeftSize:=inifile.ReadInteger(section,'LeftSize',100);
 fPrintSettings.RightSize:=inifile.ReadInteger(section,'RightSize',100);

 fPrintSettings.PageNumSep:=inifile.ReadString(section,'PageNumSep','');
 fPrintSettings.PageSuffix:=inifile.ReadString(section,'PageSuffix','');
 fPrintSettings.PagePrefix:=inifile.ReadString(section,'PagePrefix','');

 case inifile.ReadInteger(section,'Borders',0) of
  0:fPrintSettings.Borders:=pbNoborder;
  1:fPrintSettings.Borders:=pbSingle;
  2:fPrintSettings.Borders:=pbDouble;
  3:fPrintSettings.Borders:=pbVertical;
  4:fPrintSettings.Borders:=pbHorizontal;
  5:fPrintSettings.Borders:=pbAround;
  6:fPrintSettings.Borders:=pbAroundVertical;
  7:fPrintSettings.Borders:=pbAroundHorizontal;
  8:fPrintSettings.Borders:=pbCustom;
 end;

 case inifile.ReadInteger(section,'BorderStyle',0) of
  0:fPrintSettings.BorderStyle:=psSolid;
  1:fPrintSettings.BorderStyle:=psDash;
  2:fPrintSettings.BorderStyle:=psDot;
  3:fPrintSettings.BorderStyle:=psDashDot;
  4:fPrintSettings.BorderStyle:=psDashDotDot;
  5:fPrintSettings.BorderStyle:=psClear;
  6:fPrintSettings.BorderStyle:=psInsideFrame;
 end;

 fPrintSettings.Date:=int2pos(inifile.ReadInteger(section,'Date',0));
 fPrintSettings.PageNr:=int2pos(inifile.ReadInteger(section,'PageNr',0));
 fPrintSettings.Title:=int2pos(inifile.ReadInteger(section,'Title',0));
 fPrintSettings.Time:=int2pos(inifile.ReadInteger(section,'Time',0));

 case inifile.readinteger(section,'FitToPage',0) of
 0:fPrintSettings.FitToPage:=fpNever;
 1:fPrintSettings.FitToPage:=fpGrow;
 2:fPrintSettings.FitToPage:=fpShrink;
 3:fPrintSettings.FitToPage:=fpAlways;
 4:fPrintSettings.FitToPage:=fpCustom;
 end;

 fPrintSettings.Font.name:=inifile.ReadString(section,'FontName','Arial');
 fPrintSettings.Font.Size:=inifile.ReadInteger(section,'FontSize',10);
 Int2Set(inifile.ReadInteger(section,'FontStyle',0),fPrintSettings.Font);
 fPrintSettings.Font.Color:=inifile.ReadInteger(section,'FontColor',clBlack);

 fPrintSettings.HeaderFont.name:=inifile.ReadString(section,'HeaderFontName','Arial');
 fPrintSettings.HeaderFont.Size:=inifile.ReadInteger(section,'HeaderFontSize',10);
 Int2Set(inifile.ReadInteger(section,'HeaderFontStyle',0),fPrintSettings.HeaderFont);
 fPrintSettings.HeaderFont.Color:=inifile.ReadInteger(section,'HeaderFontColor',clBlack);

 fPrintSettings.FooterFont.name:=inifile.ReadString(section,'FooterFontName','Arial');
 fPrintSettings.FooterFont.Size:=inifile.ReadInteger(section,'FooterFontSize',10);
 Int2Set(inifile.ReadInteger(section,'FooterFontStyle',0),fPrintSettings.FooterFont);
 fPrintSettings.FooterFont.Color:=inifile.ReadInteger(section,'FooterFontColor',clBlack);

 case inifile.Readinteger(section,'Orientation',0) of
 0:fPrintSettings.Orientation:=poPortrait;
 1:fPrintSettings.Orientation:=poLandScape;
 end;

 inifile.Free;
end;


procedure TAdvStringGrid.AddRadio(Acol,Arow,DirRadio,IdxRadio:integer;sl:tstrings);
begin
 with CreateCellGraphic(Acol,Arow) do
  begin
   CellType:=ctRadio;
   CellAngle:=sl.Count;
   CellBoolean:=(DirRadio<>0);
   CellIndex:=IdxRadio;
   CellBitmap:=Tbitmap(sl);
  end;
end;

function TAdvStringGrid.GetRadioIdx(Acol,Arow:integer;var IdxRadio:integer):boolean;
var
 cg:TCellGraphic;
begin
 result:=false;
 cg:=GetCellGraphic(Acol,Arow);
 if cg=nil then exit;
 result:=cg.CellType=ctRadio;
 IdxRadio:=cg.CellIndex;
end;

function TAdvStringGrid.SetRadioIdx(Acol,Arow,IdxRadio:integer):boolean;
var
 cg:TCellGraphic;
begin
 result:=false;
 cg:=GetCellGraphic(Acol,Arow);
 if cg=nil then exit;
 result:=cg.CellType=ctRadio;
 cg.CellIndex:=IdxRadio;
end;


function TAdvStringGrid.IsRadio(Acol,Arow:integer):boolean;
var
 cg:TCellGraphic;
begin
 result:=false;
 cg:=GetCellGraphic(Acol,Arow);
 if cg=nil then exit;
 result:=cg.CellType=ctRadio;
end;

procedure TAdvStringGrid.RemoveRadio(Acol,Arow:integer);
var
 cg:TCellGraphic;
begin
 cg:=GetCellGraphic(Acol,Arow);
 if cg=nil then exit;
 if (cg.CellType=ctRadio) then FreeCellGraphic(Acol,Arow);
end;

procedure TAdvStringGrid.AddNode(Arow,Span:integer);
begin
 aRow:=remaprow(arow);
 with CreateCellGraphic(0,arow) do
  begin
   CellType:=ctNode;
   CellAngle:=arow;
   CellBoolean:=false;
   CellIndex:=span;
  end;
 inc(fNumNodes);
 if (col=0) then col:=col+1;
end;

procedure TAdvStringGrid.RemoveNode(Arow:integer);
var
 cg:TCellGraphic;
begin
 aRow:=remaprow(arow);
 cg:=GetCellGraphic(0,Arow);
 if cg=nil then exit;
 if cg.CellType=ctNode then FreeCellGraphic(0,Arow);
 if (fNumNodes>0) then dec(fNumNodes);
end;

function TAdvStringGrid.GetNodeState(ARow:integer):boolean;
var
 cg:TCellGraphic;
begin
 aRow:=remaprow(arow);
 result:=false;
 cg:=GetCellGraphic(0,Arow);
 if cg=nil then exit;
 if cg.CellType=ctNode then result:=cg.CellBoolean;
end;


procedure TAdvStringGrid.SetNodeState(ARow:integer;value:boolean);
var
 cg:TCellGraphic;

begin
 aRow:=remaprow(arow);

 cg:=GetCellGraphic(0,Arow);
 if cg=nil then exit;

 if cg.CellType=ctNode then
  begin
   if value<>cg.CellBoolean then
    if value then ContractNode(arow) else ExpandNode(arow);
  end;
end;

procedure TAdvStringGrid.ExpandAll;
var
 i,j:integer;
begin
 j:=self.Rowcount+NumHiddenRows;
 BeginUpdate;
 for i:=1 to j-1 do
  ExpandNode(i);
 EndUpdate;
end;

procedure TAdvStringGrid.ContractAll;
var
 i,j:integer;
begin
 j:=self.Rowcount+NumHiddenRows;
 BeginUpdate;
 for i:=j-1 downto 1 do
  self.ContractNode(i);
 EndUpdate;
end;

procedure TAdvStringGrid.ExpandNode(arow:integer);
var
 i,j:integer;
 cg:TCellGraphic;
begin
 arow:=remaprow(arow);

 cg:=GetCellGraphic(0,Arow);
 if cg=nil then exit;
 if cg.CellType=ctNode then
   cg.CellBoolean:=false
 else exit;

 i:=arow+1;
 if (cg.CellIndex=0) then
  while (i<rowcount) do
    begin
     if IsNode(i) then break;
     inc(i);
    end;

 j:=remaprowinv(i);
 i:=remaprowinv(arow);

 UnHideRows(i+1,j-1);

end;

procedure TAdvStringGrid.ContractNode(arow:integer);
var
 i,j:integer;
 cg:TCellGraphic;

begin
 arow:=remaprow(arow);
 cg:=GetCellGraphic(0,Arow);
 if cg=nil then exit;
 if cg.CellType=ctNode then
  cg.CellBoolean:=true
 else exit;

 i:=arow+1;

 if (cg.CellIndex>0) then i:=arow+cg.CellIndex
 else
   while (i<rowcount) do
    begin
     if IsNode(i) then break;
     inc(i);
    end;

 {
  messagedlg('collapse : '+inttostr(arow)+'->'+inttostr(i),mtinformation,[mbok],0);
  result:=(self.objects[0,remaprow(arow)] as TCellGraphic).CellType=ctNode;
 }

 j:=remaprowinv(i);
 i:=remaprowinv(arow);

 HideRows(i+1,j-1);
end;

function TAdvStringGrid.IsNode(arow:integer):boolean;
begin
 result:=(CellTypes[0,arow]=ctNode);
end;

procedure TAdvStringGrid.AddBitmap(Acol,Arow:integer;abmp:tbitmap;transparent:boolean;hal:TCellHalign;val:TCellValign);
begin
 with CreateCellGraphic(Acol,Arow) do SetBitmap(abmp,transparent,hal,val);
end;

function TAdvStringGrid.GetBitmap(Acol,Arow:integer):tbitmap;
var
 cg:TCellGraphic;
begin
 result:=nil;
 cg:=GetCellGraphic(Acol,Arow);
 if cg=nil then exit;
 if (cg.CellType = ctBitmap) then result:=cg.CellBitmap;
end;

procedure TAdvStringGrid.RemoveBitmap(Acol,Arow:integer);
var
 cg:TCellGraphic;
begin
 cg:=GetCellGraphic(Acol,Arow);
 if cg=nil then exit;
 if cg.CellType=ctBitmap then FreeCellGraphic(Acol,Arow);
end;

procedure TAdvStringGrid.AddPicture(Acol,Arow:integer;apicture:tpicture;transparent:boolean;hal:TCellHalign;val:TCellValign);
begin
 with CreateCellGraphic(Acol,Arow) do
   begin
     SetPicture(apicture,transparent,hal,val);
   end;
end;

function TAdvStringGrid.GetPicture(Acol,Arow:integer):tpicture;
var
 cg:TCellGraphic;
begin
 result:=nil;
 cg:=GetCellGraphic(Acol,Arow);
 if cg=nil then exit;
 if (cg.CellType = ctPicture) then result:=tpicture(cg.CellBitmap);
end;

procedure TAdvStringGrid.RemovePicture(Acol,Arow:integer);
var
 cg:TCellGraphic;
begin
 cg:=GetCellGraphic(Acol,Arow);
 if cg=nil then exit;
 if cg.CellType=ctPicture then FreeCellGraphic(Acol,Arow);
end;


procedure TAdvStringGrid.AddIcon(Acol,Arow:integer;aicon:ticon;hal:TCellHalign;val:TCellValign);
begin
 with CreateCellGraphic(Acol,Arow) do SetIcon(aicon,hal,val);
end;

procedure TAdvStringGrid.RemoveIcon(Acol,Arow:integer);
var
 cg:TCellGraphic;
begin
 cg:=GetCellGraphic(Acol,Arow);
 if cg=nil then exit;
 if cg.CellType=ctIcon then FreeCellGraphic(Acol,Arow);
end;

procedure TAdvStringGrid.AddCheckBox(Acol,Arow:integer;state,data:boolean);
begin
  with CreateCellGraphic(Acol,Arow) do
   begin
    case VAlignment of
    vtaTop:(self.objects[acol,arow] as TCellGraphic).SetCheckBox(state,data,haBeforeText,vaTop);
    vtaCenter:(self.objects[acol,arow] as TCellGraphic).SetCheckBox(state,data,haBeforeText,vaCenter);
    vtaBottom:(self.objects[acol,arow] as TCellGraphic).SetCheckBox(state,data,haBeforeText,vaBottom);
    end;
   end;
end;

procedure TAdvStringGrid.RemoveCheckBox(Acol,Arow:integer);
var
 cg:TCellGraphic;
begin
 cg:=GetCellGraphic(Acol,Arow);
 if cg=nil then exit;
 if cg.CellType in [ctCheckBox,ctDataCheckBox] then FreeCellGraphic(Acol,Arow);
end;

function TAdvStringGrid.HasCheckBox(Acol,Arow:integer):boolean;
begin
 result:=false;
 if not fEnableGraphics then exit;
 result:=(CellTypes[ACol,ARow] in [ctCheckBox,ctDataCheckBox]);
end;

function TAdvStringGrid.HasStaticEdit(Acol,Arow:integer):boolean;
begin
 result:=false;
 if not fEnableGraphics then exit;
 result:=(CellTypes[ACol,ARow] in [ctCheckBox,ctDataCheckBox,ctRadio]);
end;


function TAdvStringGrid.GetCheckBoxState(Acol,Arow:integer;var state:boolean):boolean;
var
 cg:tCellGraphic;
begin
 result:=false;
 if not fEnableGraphics then exit;

 cg:=GetCellGraphic(Acol,Arow);
 if cg=nil then exit;

 if (cg.CellType=ctCheckBox) then
  begin
   state:=cg.CellBoolean;
   result:=true;
  end;
 if (cg.CellType=ctDataCheckBox) then
  begin
   state:=self.cells[acol,arow]=FCheckTrue;
   result:=true;
  end;
end;

function TAdvStringGrid.SetCheckBoxState(Acol,Arow:integer;state:boolean):boolean;
var
 cg:TCellGraphic;
begin
 result:=false;
 cg:=GetCellGraphic(Acol,Arow);
 if cg=nil then exit;

 if cg.CellType=ctCheckBox then
  begin
   cg.CellBoolean:=state;
   self.cells[acol,arow]:=self.cells[acol,arow];
   result:=true;
  end;

 if cg.CellType=ctDataCheckBox then
  begin
   if state then self.cells[acol,arow]:=FCheckTrue else self.cells[acol,arow]:=FCheckFalse;
   result:=true;
  end;

end;

function TAdvStringGrid.ToggleCheck(Acol,Arow:integer;fromedit:boolean):boolean;
var
 cg:TCellGraphic;
 r:trect;
 canedit:boolean;

begin
 result:=false;
 if not fEnableGraphics then exit;
 cg:=GetCellGraphic(Acol,Arow);
 if cg=nil then exit;
 if (cg.CellType in [ctCheckBox,ctDataCheckBox]) then
  begin
   {$IFDEF WIN32}
   if assigned(OnCanEditCell) and fromedit then
    begin
     canedit:=true;
     OnCanEditCell(self,arow,acol,canedit);
     if not canedit then exit;
    end;
   {$ENDIF}

   if (cg.CellType=ctCheckBox) then
    cg.CellBoolean:=not cg.CellBoolean
   else
    begin
     if (self.cells[acol,arow]=FCheckTrue) then
      self.cells[acol,arow]:=FCheckFalse
     else
      self.cells[acol,arow]:=FCheckTrue;
     end;
     r:=cellrect(acol,arow);
     drawcell(acol,arow,r,[gdSelected]);
     canvas.drawfocusrect(r);
     result:=true;
    end;
end;

function TAdvStringGrid.ToggleCheckBox(Acol,Arow:integer):boolean;
begin
 result:=togglecheck(acol,arow,false);
end;

{$IFDEF WIN32}
function TAdvStringGrid.GetImageIdx(Acol,Arow:integer;var idx:integer):boolean;
begin
 result:=false;
 if not assigned(FGridImages) then exit;

 if assigned(self.objects[acol,arow]) then
  begin
   if not (self.objects[acol,arow] is TCellGraphic) then exit;
   if ((self.objects[acol,arow] as TCellGraphic).CellType=ctImageList)  then
    begin
     idx:=(self.objects[acol,arow] as TCellGraphic).CellIndex;
     result:=true;
    end;
  end;
end;

procedure TAdvStringGrid.AddDataImage(Acol,Arow,Aidx:integer;hal:TCellHalign;val:TCellValign);
begin
 if not assigned(FGridImages) then exit;
 if assigned(self.objects[acol,arow]) then
  begin
   if not (self.objects[acol,arow] is TCellGraphic) then exit;
   (self.objects[acol,arow] as TCellGraphic).Free;
   self.objects[acol,arow]:=nil;
  end;
 self.objects[acol,arow]:=TCellGraphic.Create;
 (self.objects[acol,arow] as TCellGraphic).SetDataImage(Aidx,hal,val);
end;

procedure TAdvStringGrid.RemoveDataImage(Acol,Arow:integer);
begin
 RemoveImageIdx(Acol,Arow);
end;

procedure TAdvStringGrid.AddMultiImage(Acol,Arow,Dir:integer;hal:TCellHalign;val:TCellValign);
begin
  if not assigned(FGridImages) then exit;
  if assigned(self.objects[acol,arow]) then
   begin
    if not (self.objects[acol,arow] is TCellGraphic) then exit;
    (self.objects[acol,arow] as TCellGraphic).Free;
    self.objects[acol,arow]:=nil;
   end;

  self.objects[acol,arow]:=TCellGraphic.Create;
 (self.objects[acol,arow] as TCellGraphic).SetMultiImage(acol,arow,dir,hal,val,MultiImageChanged);
end;

procedure TAdvStringGrid.RemoveMultiImage(Acol,Arow:integer);
begin
  if assigned(self.objects[acol,arow]) then
   begin
   if not (self.objects[acol,arow] is TCellGraphic) then exit;
   if (self.objects[acol,arow] as TCellGraphic).CellType=ctImages then
      TIntList((self.objects[acol,arow] as TCellGraphic).CellBitmap).Free;
   (self.objects[acol,arow] as TCellGraphic).Free;
   self.objects[acol,arow]:=nil;
   end;
end;

procedure TAdvStringGrid.AddImageIdx(Acol,Arow,Aidx:integer;hal:TCellHalign;val:TCellValign);
begin
  if not assigned(FGridImages) then exit;
  if assigned(self.objects[acol,arow]) then
   begin
    if not (self.objects[acol,arow] is TCellGraphic) then exit;
    (self.objects[acol,arow] as TCellGraphic).Free;
    self.objects[acol,arow]:=nil;
   end;

  self.objects[acol,arow]:=TCellGraphic.Create;
  (self.objects[acol,arow] as TCellGraphic).SetImageIdx(aidx,hal,val);
end;

procedure TAdvStringGrid.RemoveImageIdx(Acol,Arow:integer);
begin
  if assigned(self.objects[acol,arow]) then
   begin
   if not (self.objects[acol,arow] is TCellGraphic) then exit;
   (self.objects[acol,arow] as TCellGraphic).Free;
   self.objects[acol,arow]:=nil;
   end;
end;

procedure TAdvStringGrid.SetImages(value:tImageList);
begin
  if value<>FGridImages then
   begin
    FGridImages:=value;
   end;
end;

procedure TAdvStringGrid.AddRotated(Acol,Arow:integer;aAngle:smallint;s:string);
begin
  if not fEnableGraphics then exit;

  {make sure the cell gets allocated}
  if cells[acol,arow]='' then cells[acol,arow]:='';

  {check if an object has already been assigned}
  if assigned(self.objects[acol,arow]) then
   begin
    if not (self.objects[acol,arow] is TCellGraphic) then exit;
    (self.objects[acol,arow] as TCellGraphic).Free;
    self.objects[acol,arow]:=nil;
   end;
  self.objects[acol,arow]:=TCellGraphic.Create;
  (self.objects[acol,arow] as TCellGraphic).SetAngle(aAngle);
  self.cells[acol,arow]:=s;
end;

procedure TAdvStringGrid.RemoveRotated(Acol,Arow:integer);
begin
  if not fEnableGraphics then exit;
  if assigned(self.objects[acol,arow]) then
   begin
    if not (self.objects[acol,arow] is TCellGraphic) then exit;
    (self.objects[acol,arow] as TCellGraphic).Free;
    self.objects[acol,arow]:=nil;
   end;
end;

function TAdvStringGrid.IsRotated(acol,arow:integer;var aAngle:integer):boolean;
begin
 result:=false;
 if not fEnableGraphics then exit;
 if not assigned(objects[acol,arow]) then exit;
 if (objects[acol,arow] is tcellgraphic) then
  if (objects[acol,arow] as tcellgraphic).celltype=ctRotated then
   begin
    aAngle:=(objects[acol,arow] as tcellgraphic).cellAngle;
    result:=true;
   end;
end;
{$ENDIF}

procedure TAdvStringGrid.MoveColumn(FromIndex, ToIndex: Longint);
begin
 self.columnmoved(FromIndex,ToIndex);
end;

procedure TAdvStringGrid.MoveRow(FromIndex, ToIndex: Longint);
begin
 self.rowmoved(FromIndex,ToIndex);
end;

procedure TAdvStringGrid.UnHideSelection;
begin
 self.selection:=foldselection;
end;

procedure TAdvStringGrid.HideSelection;
begin
 {$IFDEF WIN32}
 foldselection:=self.selection;
 self.selection:=tgridrect(rect(colcount,rowcount,colcount,rowcount));
 {$ENDIF}
end;

{------------------------------------------
 v 0.99 : added hide/unhide proc for columns
-------------------------------------------}
procedure TAdvStringgrid.Hidecolumn(colindex:integer);
begin
 HideColumns(colindex,colindex);
end;

procedure TAdvStringgrid.Hidecolumns(fromcol,tocol:integer);
var
 i,j:integer;
begin
 for j:=fromcol to tocol do
  begin
   if (getvisiblecol(j)) and (j<colcount+fNumHidden) and (colcount>=2) then
    begin
      fallcolwidths[j]:=colwidths[remapcolinv(j)];

      for i:=remapcolinv(j) to colcount-2 do
        colwidths[i]:=colwidths[i+1];
      inc(fnumhidden);
      setvisiblecol(j,false);

      colcount:=colcount-1;
    end;
  end;
 self.repaint;
end;

procedure TAdvStringGrid.UnhideColumn(colindex:integer);
begin
 UnHideColumns(colindex,colindex);
{
  if (getvisiblecol(colindex)=false) then
   begin
     dec(fnumhidden);
     setvisiblecol(colindex,true);
     colcount:=colcount+1;
     self.repaint;
   end;
}
end;

procedure TAdvStringGrid.UnHideColumnsAll;
begin
 UnHideColumns(0,colcount+fNumHidden);
end;

procedure TAdvStringGrid.UnhideColumns(fromcol,tocol:integer);
var
 i,j:integer;
begin
 for j:=fromcol to tocol do
  begin
   if (getvisiblecol(j)=false) then
    begin
      dec(fnumhidden);
      setvisiblecol(j,true);
      colcount:=colcount+1;
      for i:=colcount-1 downto remapcolinv(j+1) do colwidths[i]:=colwidths[i-1];
      colwidths[remapcolinv(j)]:=fallcolwidths[j];
    end;
  end;
 self.repaint;
end;


function TAdvStringGrid.Ishiddencolumn(colindex:integer):boolean;
begin
  if (colindex<MaxColumns) then
    IsHiddenColumn:=not fVisibleCol[colindex]
  else
    IsHiddenColumn:=false;
end;

function TAdvStringGrid.NumHiddenColumns:integer;
begin
 NumHiddenColumns:=fNumHidden;
end;

function TAdvStringGrid.RemapRowInv(aRow:integer):integer;
var
 i,k:integer;
begin
 i:=0;
 k:=0;

 while (k<=arow) do
  begin
   if not IsHiddenRow(i) then inc(k);
   inc(i);
  end;

 result:=i-1;
end;

function TAdvStringGrid.RemapRow(aRow:integer):integer;
var
 i,j:integer;
begin
 i:=aRow;
 for j:=1 to fgriditems.Count do
  begin
   if (fgriditems.Items[j-1] as TGridItem).Idx<i then dec(arow);
  end;
 result:=arow;
end;

procedure TAdvStringGrid.SetGroupColumn(aGroupColumn:integer);
begin
 if aGroupColumn=-1 then UnGroup else
 if (aGroupColumn>0) then Group(aGroupColumn);
end;

procedure TAdvStringGrid.Group(colindex:integer);
var
 i,np:integer;
 lc,nc:string;
 grp,grpc:integer;
begin
  if (colindex<=fixedcols) then exit;
  if (fGroupColumn<>-1) then UnGroup;

  BeginUpdate;

  fGroupColumn:=colindex;
  fGroupWidth:=colwidths[colindex];
  if (fixedrows>0) then fGroupCaption:=cells[colindex,0];

  grp:=colindex;
  grpc:=1+fixedcols;

  {sort the column to group first}
  sortcolumn:=colindex;
  qsort;

  if (grpc=colindex) then inc(grpc);

  np:=-1;
  lc:='';
  i:=fixedrows;

  while (i<=rowcount-1) do
   begin
    nc:=cells[colindex,i];
    if (lc<>nc) then
     begin
      if (np<>-1) then addnode(np,i-np);
      insertrows(i,1);
      cells[grpc,i]:=nc;
      np:=i;
      inc(i);
     end;
    lc:=cells[grp,i];
    inc(i);
   end;
   addnode(np,i-np);
   removecols(grp,1);
   row:=fixedrows;
   EndUpdate;
end;

procedure TAdvStringGrid.UnGroup;
var
 i:integer;
 nc:string;
 grpc:integer;
begin
 if fGroupColumn<=0 then exit;

 expandall;

 if fGroupColumn=1 then grpc:=2 else grpc:=1;

 insertcols(fGroupColumn,1);
 colwidths[fGroupColumn]:=fGroupWidth;

 if (fixedrows>0) then cells[fGroupColumn,0]:=fGroupCaption;

 i:=fixedrows;
 while (i<=rowcount-1) do
  begin
   if IsNode(i) then
    begin
     nc:=cells[grpc,i];
     removenode(i);
     removerows(i,1);
    end
   else
    begin
     cells[fGroupColumn,i]:=nc;
     inc(i);
    end;
  end;
  fGroupColumn:=-1;
end;


procedure TAdvStringGrid.HideRow(rowindex:integer);
var
 j,k:integer;
begin
 k:=RowIndex;
 {count nr. of hidden items under RowIndex}
 for j:=1 to fgriditems.Count do
  begin
   if (fgriditems.Items[j-1] as TGridItem).Idx<RowIndex then dec(k);
   if (fgriditems.Items[j-1] as TGridItem).Idx=RowIndex then exit;
  end;
 with (fgriditems.Add as TGridItem) do
  begin
   Items.Assign(self.rows[k]);
   Idx:=RowIndex;
  end;
 self.removerows(k,1);
end;

procedure TAdvStringGrid.HideRows(fromrow,torow:integer);
var
 j,k:integer;
begin
 k:=fromrow;
 {count nr. of hidden items under RowIndex}

 for j:=1 to fgriditems.Count do
  begin
   if (fgriditems.Items[j-1] as TGridItem).Idx<fromrow then dec(k);
   if (fgriditems.Items[j-1] as TGridItem).Idx=fromrow then exit;
  end;

 for j:=fromrow to torow do
 with (fgriditems.Add as TGridItem) do
  begin
   Items.Assign(self.rows[k+j-fromrow]);
   Idx:=fromrow+(j-fromrow);
  end;

 self.removerows(k,torow-fromrow+1);
end;

procedure TAdvStringGrid.UnHideRowsAll;
begin
 while fGridItems.Count>0 do
   UnHideRow((fgriditems.Items[0] as TGridItem).Idx);
end;

procedure TAdvStringGrid.UnHideRow(rowindex:integer);
var
 j,k,l:integer;
 flg:boolean;
begin
 k:=RowIndex;
 flg:=false;
 l:=0;
 {count nr. of hidden items under rowindex}
 for j:=1 to fgriditems.Count do
  begin
   if (fgriditems.Items[j-1] as TGridItem).Idx<RowIndex then dec(k);
   if (fgriditems.Items[j-1] as TGridItem).Idx=RowIndex then
       begin
        flg:=true;
        l:=j-1;
       end;
  end;

 if not flg then exit;

 Self.insertrows(k,1);

 with (fgriditems.Items[l] as TGridItem) do
  begin
   self.rows[k].Assign(Items);
  end;
 (fgriditems.Items[l] as TGridItem).Free;
end;

procedure TAdvStringGrid.UnHideRows(fromrow,torow:integer);
var
 i,j,k,l:integer;
 flg:boolean;
 num:integer;

begin
 k:=fromRow;
 flg:=false;
 {count nr. of hidden items under rowindex}
 for j:=1 to fgriditems.Count do
  begin
   if (fgriditems.Items[j-1] as TGridItem).Idx<FromRow then dec(k);
   if (fgriditems.Items[j-1] as TGridItem).Idx=FromRow then flg:=true;
  end;

 if not flg then exit;

 num:=torow-fromrow+1;

 for j:=fromrow to torow do if not IsHiddenRow(j) then dec(num);
 if num=0 then exit;

 Self.insertrows(k,num);

 i:=0;
 while (i<fgriditems.count) and (fgriditems.count>0) do
  begin
   l:=(fgriditems.Items[i] as TGridItem).Idx;
   if (l>=fromrow) and (l<=torow) then
    begin
     self.rows[k+l-fromrow].Assign((fgriditems.Items[i] as TGridItem).Items);
    (fgriditems.Items[i] as TGridItem).Free;
   end
  else inc(i);
 end;
end;


function TAdvStringGrid.IsHiddenRow(rowindex:integer):boolean;
var
 j:integer;
begin
 result:=false;

 if fgriditems.Count=0 then exit;

 for j:=1 to fgriditems.Count do
  begin
   if (fgriditems.Items[j-1] as TGridItem).Idx=RowIndex then result:=true;
  end;
end;

function TAdvStringGrid.NumHiddenRows:integer;
begin
 result:=fGridItems.Count;
end;

function TAdvStringGrid.GetRealCol:integer;
begin
 result:=remapcol(col);
end;

function TAdvStringGrid.GetRealRow:integer;
begin
 result:=remaprowinv(row);
end;

function TAdvStringGrid.RealRowIndex(aRow:integer):integer;
begin
 result:=remaprowinv(arow);
end;

function TAdvStringGrid.RealColIndex(aCol:integer):integer;
begin
 result:=remapcol(acol);
end;

function TAdvStringGrid.DisplRowIndex(aRow:integer):integer;
begin
 result:=remaprow(arow);
end;

function TAdvStringGrid.DisplColIndex(aCol:integer):integer;
begin
 result:=remapcolinv(acol);
end;


procedure TAdvStringGrid.setvisiblecol(i:integer;avalue:boolean);
begin
 fVisibleCol[i]:=avalue;
end;

function TAdvStringGrid.getvisiblecol(i:integer):boolean;
begin
 getvisiblecol:=fVisibleCol[i];
end;

function TAdvStringGrid.remapcolinv(acol:integer):integer;
var
 i:integer;
 remapvalue:integer;
begin
 remapvalue:=acol;
 for i:=0 to acol-1 do
  begin
   if not fVisibleCol[i] then dec(remapvalue);
  end;
 result:=remapvalue; 
end;

function TAdvStringGrid.remapcol(acol:integer):integer;
var
 i:integer;
 remapvalue:integer;
begin
 remapvalue:=0;
 remapcol:=0;
 i:=0;
 if (acol>=maxcolumns) then
    begin
     remapcol:=acol;
     exit;
    end;
 while (i<MaxColumns) do
  begin
    if (remapvalue=acol) and (fVisiblecol[i]) then
     begin
       remapcol:=i;
       exit;
     end;
   if (fVisibleCol[i]) then inc(remapvalue);
   inc(i);
  end;
end;

{$IFDEF WIN32}
procedure TAdvStringGrid.UpdateType;
begin
  case FScrollType of
  ssNormal:FlatSetScrollProp(WSB_PROP_VSTYLE,FSB_REGULAR_MODE,true);
  ssFlat:FlatSetScrollProp(WSB_PROP_VSTYLE,FSB_FLAT_MODE,true);
  ssEncarta:FlatSetScrollProp(WSB_PROP_VSTYLE,FSB_ENCARTA_MODE,true);
  end;
  case FScrollType of
  ssNormal:FlatSetScrollProp(WSB_PROP_HSTYLE,FSB_REGULAR_MODE,true);
  ssFlat:FlatSetScrollProp(WSB_PROP_HSTYLE,FSB_FLAT_MODE,true);
  ssEncarta:FlatSetScrollProp(WSB_PROP_HSTYLE,FSB_ENCARTA_MODE,true);
  end;
end;

procedure TAdvStringGrid.UpdateColor;
begin
 if (fScrollColor=clNone) then exit;
 FlatSetScrollProp(WSB_PROP_VBKGCOLOR,longint(fScrollColor),true);
 FlatSetScrollProp(WSB_PROP_HBKGCOLOR,longint(fScrollColor),true);
end;

procedure TAdvStringGrid.UpdateWidth;
begin
 FlatSetScrollProp(WSB_PROP_CXVSCROLL,fScrollWidth,true);
 FlatSetScrollProp(WSB_PROP_CYHSCROLL,fScrollWidth,true);
end;

procedure TAdvStringGrid.UpdateVScrollBar;
var
 scrollinfo:tscrollinfo;
begin
 if not (ScrollBars in [ssBoth,ssVertical]) or not fIsFlat then exit;

 scrollinfo.fMask:=SIF_ALL;
 scrollinfo.cbSize:=sizeof(scrollinfo);
 GetScrollInfo(self.handle,SB_VERT,scrollinfo);
 scrollinfo.fMask:=SIF_ALL;
 scrollinfo.cbSize:=sizeof(scrollinfo);
 if (scrollinfo.npos>127) or (scrollinfo.npos<0) then scrollinfo.npos:=0;
 scrollinfo.nmax:=127;
 scrollinfo.nmin:=0;
 scrollinfo.npage:=round(128*visiblerowcount/rowcount);
 scrollinfo.npos:=round((128-scrollinfo.npage)*(scrollinfo.npos/127));

 FlatSetScrollInfo(SB_VERT,scrollinfo,true);
end;

procedure TAdvStringGrid.UpdateHScrollBar;
var
 scrollinfo:tscrollinfo;
begin
 if not (ScrollBars in [ssBoth,ssHorizontal]) or not fIsFlat then exit;
 scrollinfo.fMask:=SIF_ALL;
 scrollinfo.cbSize:=sizeof(scrollinfo);
 GetScrollInfo(self.handle,SB_HORZ,scrollinfo);
 scrollinfo.fMask:=SIF_ALL;
 scrollinfo.cbSize:=sizeof(scrollinfo);
 if (scrollinfo.npos>127) or (scrollinfo.npos<0) then scrollinfo.npos:=0;
 scrollinfo.nmax:=127;
 scrollinfo.nmin:=0;
 scrollinfo.npage:=round(128*visiblecolcount/colcount);
 scrollinfo.npos:=round((128-scrollinfo.npage)*(scrollinfo.npos/127));
 FlatSetScrollInfo(SB_HORZ,scrollinfo,true)
end;
         
{
procedure TAdvStringGrid.FlatSetScrollPos(code,pos: integer);
var
 scrollinfo:tscrollinfo;
begin
 scrollinfo.cbSize :=sizeof(scrollinfo);
 scrollinfo.fMask :=SIF_POS;
 scrollinfo.nPos :=pos;
 self.FlatSetScrollInfo(code,scrollinfo,true);
end;
}

procedure TAdvStringGrid.FlatSetScrollInfo(code: integer;var scrollinfo:tscrollinfo;fRedraw: bool);
var
 ComCtl32DLL: THandle;
 _FlatSB_SetScrollInfo:function(wnd:hwnd;code:integer;var scrollinfo:tscrollinfo;fRedraw:bool):integer; stdcall;

begin
 ComCtl32DLL:=GetModuleHandle(comctrl);
 if (ComCtl32DLL>0) then
  begin
   @_FlatSB_SetScrollInfo:=GetProcAddress(ComCtl32DLL,'FlatSB_SetScrollInfo');
   if assigned(_FlatSB_SetScrollInfo) then
     begin
      _FlatSB_SetScrollInfo(self.handle,code,scrollinfo,fRedraw);
     end;
  end;
end;

procedure TAdvStringGrid.FlatSetScrollProp(index, newValue: integer;
  fRedraw: bool);
var
 ComCtl32DLL: THandle;
 _FlatSB_SetScrollProp:function(wnd:hwnd;Index,newValue:integer;fredraw:bool):bool stdcall;

begin
 if not fIsFlat then exit;
 ComCtl32DLL:=GetModuleHandle(comctrl);
 if (ComCtl32DLL>0) then
  begin
   @_FlatSB_SetScrollProp:=GetProcAddress(ComCtl32DLL,'FlatSB_SetScrollProp');
   if assigned(_FlatSB_SetScrollProp) then
     _FlatSB_SetScrollProp(self.handle,index,newValue,fRedraw);
  end;
end;

procedure TAdvStringGrid.FlatShowScrollBar(code:integer;show:bool);
var
 ComCtl32DLL: THandle;
 _FlatSB_ShowScrollBar:function(wnd:hwnd;code:integer;show:bool):integer; stdcall;

begin
 if not fIsFlat then exit;

 case code of
 SB_VERT:if not (ScrollBars in [ssBoth,ssVertical]) then exit;
 SB_HORZ:if not (ScrollBars in [ssBoth,ssHorizontal]) then exit;
 end;

 ComCtl32DLL:=GetModuleHandle(comctrl);
 if (ComCtl32DLL>0) then
  begin
   @_FlatSB_ShowScrollBar:=GetProcAddress(ComCtl32DLL,'FlatSB_ShowScrollBar');
   if assigned(_FlatSB_ShowScrollBar) then
     _FlatSB_ShowScrollBar(self.handle,code,show);
  end;
end;

procedure TAdvStringGrid.FlatUpdate;
begin
 UpdateType;
 UpdateColor;
 UpdateWidth;
 if (visiblerowcount+fixedrows<rowcount) then
   begin
    FlatShowScrollBar(SB_VERT,true);
    UpdateVScrollBar;
   end;
 if (visiblecolcount+fixedcols<colcount) then
   begin
    FlatShowScrollBar(SB_HORZ,true);
    UpdateHScrollBar;
   end;
end;

procedure TAdvStringGrid.FlatInit;
var
 ComCtl32DLL: THandle;
 _InitializeFlatSB: function(wnd:hwnd):Bool stdcall;
begin
 ComCtl32DLL:=GetModuleHandle(comctrl);
 if (ComCtl32DLL>0) then
  begin
   @_InitializeFlatSB:=GetProcAddress(ComCtl32DLL,'InitializeFlatSB');
   if assigned(_InitializeFlatSB) then _InitializeFlatSB(self.handle);
   fIsFlat:=assigned(_InitializeFlatSB);
  end;
end;

procedure TAdvStringGrid.FlatDone;
var
 ComCtl32DLL: THandle;
 _UninitializeFlatSB: function(wnd:hwnd):Bool stdcall;
begin
 fisFlat:=false;
 ComCtl32DLL:=GetModuleHandle(comctrl);
 if (ComCtl32DLL>0) then
  begin
   @_UninitializeFlatSB:=GetProcAddress(ComCtl32DLL,'UninitializeFlatSB');
   if assigned(_UninitializeFlatSB) then _UninitializeFlatSB(self.handle);
  end;
end;

function TAdvStringGrid.GetUpGlyph: TBitmap;
begin
 Result:=FSortUpGlyph;
end;

procedure TAdvStringGrid.SetUpGlyph(Value: TBitmap);
begin
 FSortUpGlyph.Assign(Value);
end;

function TAdvStringGrid.GetDownGlyph: TBitmap;
begin
 Result:=FSortDownGlyph;
end;

procedure TAdvStringGrid.SetDownGlyph(Value: TBitmap);
begin
 FSortDownGlyph.Assign(Value);
end;

procedure TAdvStringGrid.SetBackground(Value: TBackground);
begin
 FBackground.Assign(Value);
 self.Repaint;
end;

procedure TAdvStringGrid.SetHovering(value:boolean);
begin
 if (value<>fHovering) then
  begin
   fHovering:=value;
   if not (csDesigning in ComponentState) then
    begin
     if value then
       fGridTimerID:=settimer(self.handle,111,1000,nil)
     else
       KillTimer(self.handle,fGridTimerID);
    end;
  end;
end;

procedure TAdvStringGrid.SetScrollType(const Value: TScrollType);
begin
 FScrollType := Value;
 UpdateType;
end;

procedure TAdvStringGrid.SetScrollColor(const Value: TColor);
begin
 FScrollColor := Value;
 UpdateColor;
end;

procedure TAdvStringGrid.SetScrollWidth(const Value: integer);
begin
 FScrollWidth := Value;
 UpdateWidth;
end;

procedure TAdvStringGrid.SetScrollProportional(value:boolean);
var
 scrollinfo:tscrollinfo;
begin
 FScrollProportional:=value;
 if value then
  begin
   FlatInit;
   FlatUpdate;
  end
 else
  if fIsflat then
   begin
    FlatDone;
    scrollinfo.cbSize:=sizeof(scrollinfo);
    scrollinfo.fMask:=SIF_PAGE;
    scrollinfo.nPage :=0;
    setscrollinfo(Handle,SB_HORZ,scrollinfo,true);
    scrollinfo.cbSize:=sizeof(scrollinfo);
    scrollinfo.fMask:=SIF_PAGE;
    scrollinfo.nPage :=0;
    setscrollinfo(Handle,SB_VERT,scrollinfo,true);
   end;
end;

procedure TAdvStringGrid.WMHScroll(var WMScroll: TWMScroll);
var
 page:integer;
 r:trect;
 s:string;
 pt:tpoint;
 nr:integer;
 fcr,tcr:trect;

begin
 if fScrollHints in [shHorizontal,shBoth] then
 begin
   if (wmScroll.scrollcode=SB_ENDSCROLL) then
      begin
       FScrollHintWnd.ReleaseHandle;
       FScrollHintShow:=false;
       {$IFDEF DEBUG}
       outputdebugstr('end scroll');
       {$ENDIF}
      end;
   {$IFDEF DELPHI3_LVL}
   if (wmScroll.scrollcode=SB_THUMBTRACK) then
      begin
       nr:=fixedrows+longmuldiv(wmScroll.pos,colcount-visiblecolcount-fixedcols,maxshortint);

       s:='Col : '+inttostr(nr);

       if assigned(OnScrollHint) then
         OnScrollHint(self,nr,s);

       r:=FScrollHintWnd.CalcHintRect(100,s,nil);
       FScrollHintWnd.Caption:=s;
       FScrollHintWnd.Color:=FHintColor;

       getcursorpos(pt);
       r.left:=r.left+pt.x+10;
       r.right:=r.right+pt.x+10;
       r.top:=R.top+pt.y;
       r.bottom:=r.bottom+pt.y;

       FScrollHintWnd.ActivateHint(r,s);
       FScrollHintShow:=true;
      end;
    {$ENDIF}
  end;


 if (wmScroll.scrollcode=SB_THUMBPOSITION) and (fIsFlat) then
   begin
    page:=round(128*visiblecolcount/colcount);
    wmScroll.pos:=round(127*wmScroll.pos/(128-page));
   end;
 with fBackground do
  if not Bitmap.Empty and (Display=bdFixed) then
   begin
    mousetocell(left,top,longint(fcr.left),longint(fcr.top));
    mousetocell(left+Bitmap.width,top+Bitmap.height,longint(fcr.right),longint(fcr.bottom));
   end;

 inherited;

 with fBackground do
  if not Bitmap.Empty and (Display=bdFixed) then
   begin
    mousetocell(left,top,longint(tcr.left),longint(tcr.top));
    mousetocell(left+Bitmap.width,top+Bitmap.height,longint(tcr.right),longint(tcr.bottom));
    if (wmScroll.scrollcode<>SB_THUMBTRACK) and not EqualRect(fcr,tcr) then
     begin
      RepaintRect(fcr);
      RepaintRect(tcr);
     end;
   end;

 UpdateHScrollBar;
 if HasCheckBox(col,row) then HideEditor;
end;


procedure TAdvStringGrid.WMVScroll(var WMScroll: TWMScroll);
var
 page:integer;
 r:trect;
 s:string;
 pt:tpoint;
 nr:integer;
 fcr,tcr:trect;
begin
 if fScrollHints in [shVertical,shBoth] then
 begin
   if (wmScroll.scrollcode=SB_ENDSCROLL) then
      begin
       FScrollHintWnd.ReleaseHandle;
       FScrollHintShow:=false;
       {$IFDEF DEBUG}
       outputdebugstr('end scroll');
       {$ENDIF}
      end;

   {$IFDEF DELPHI3_LVL}
   if (wmScroll.scrollcode=SB_THUMBTRACK) then
      begin
//       page:=round(128*visiblerowcount/rowcount);
       nr:=fixedrows+longmuldiv(wmScroll.pos,rowcount-visiblerowcount-fixedrows,maxshortint);

       s:='Row : '+inttostr(nr);

       if assigned(OnScrollHint) then
         OnScrollHint(self,nr,s);

       r:=FScrollHintWnd.CalcHintRect(100,s,nil);
       FScrollHintWnd.Caption:=s;
       FScrollHintWnd.Color:=FHintColor;

       getcursorpos(pt);
       r.left:=r.left+pt.x+10;
       r.right:=r.right+pt.x+10;
       r.top:=R.top+pt.y;
       r.bottom:=r.bottom+pt.y;

       FScrollHintWnd.ActivateHint(r,s);
       FScrollHintShow:=true;
      end;
    {$ENDIF}
  end;

 if (wmScroll.scrollcode=SB_THUMBPOSITION) and (fIsflat) then
   begin
    page:=round(128*visiblerowcount/rowcount);
    wmScroll.pos:=round(127*wmScroll.pos/(128-page));
   end;
 {added stuff}

 with fBackground do
  if not Bitmap.Empty and (Display=bdFixed) then
   begin
    mousetocell(left,top,longint(fcr.left),longint(fcr.top));
    mousetocell(left+Bitmap.width,top+Bitmap.height,longint(fcr.right),longint(fcr.bottom));
   end;

 inherited;

 with fBackground do
  if not Bitmap.Empty and (Display=bdFixed) then
   begin
    mousetocell(left,top,longint(tcr.left),longint(tcr.top));
    mousetocell(left+Bitmap.width,top+Bitmap.height,longint(tcr.right),longint(tcr.bottom));
    if (wmScroll.scrollcode<>SB_THUMBTRACK) and not EqualRect(fcr,tcr) then
     begin
      RepaintRect(fcr);
      RepaintRect(tcr);
     end;
   end;

 {end of added}

 UpdateVScrollbar;

 if HasCheckBox(col,row) then HideEditor;
end;

procedure TAdvStringGrid.Notification(AComponent: TComponent; AOperation: TOperation); 
begin
 if (aOperation=opRemove) and (aComponent=fGridImages) then fGridImages:=nil;
 inherited;
end;

procedure TAdvStringGrid.SizeChanged(OldColCount, OldRowCount: Longint);
begin
 if (parent=nil) then exit;

 if FColumnSize.fStretch then StretchRightColumn;
 inherited SizeChanged(OldColCount,OldRowCount);

 FlatShowScrollBar(SB_VERT,visiblerowcount+fixedrows<rowcount);
 UpdateVScrollBar;

 FlatShowScrollBar(SB_HORZ,visiblecolcount+fixedcols<colcount);
 UpdateHScrollBar;
end;
{$ENDIF}

{$IFDEF DELPHI3_LVL}
function TAdvStringGrid.GetArrowColor:tcolor;
begin
 result:=arwu.Color;
end;

procedure TAdvStringGrid.SetArrowColor(value:tcolor);
begin
 arwu.color:=value;
 arwd.color:=value;
 arwl.color:=value;
 arwr.color:=value;
end;
{$ENDIF}

{$IFDEF DELPHI3_4_ONLY}
procedure TAdvStringGrid.SetOleDropRTF(avalue:boolean);
begin
 ddintf.RTFAware := avalue;
end;

procedure TAdvStringGrid.SetOleDropTarget(avalue:boolean);
begin
 FOleDropTarget:=avalue;

 if not (csDesigning in ComponentState) then
  begin
   if FOleDropTarget then
    begin
     FOleDropTargetAssigned:=RegisterDragDrop(self.Handle, TGridDropTarget.Create(self))=s_OK;
    end
   else
     if FOleDropTargetAssigned then RevokeDragDrop(self.Handle);
  end;
end;
{$ENDIF}

procedure TAdvStringGrid.StretchRightColumn;
var
 i,w:integer;
begin
 if (colcount=0) then exit;

 if not FColumnSize.fStretch then exit;

 colchgflg:=false;
 if colcount=1 then
   begin
    colwidths[0]:=clientrect.right;
    colchgflg:=true;
    exit;
   end;
 w:=0;

 {real used column width is colwidth[]+1 !}
 for i:=0 to colcount-2 do
  begin
   w:=w+colwidths[i]+gridlinewidth;
  end;

 if (w<clientrect.right) then
  colwidths[colcount-1]:=clientrect.right-w-1;

 colchgflg:=true;
end;

procedure TAdvStringGrid.ColWidthsChanged;
begin
 if colchgflg then StretchRightColumn;

 if (csDesigning in ComponentState) then
  begin
   if FScrollHintShow then FScrollHintWnd.ReleaseHandle;
   FScrollHintShow:=false;
  end;

 inherited ColWidthsChanged;
 colsizeflg:=true;
 {$IFDEF DEBUG}
 outputdebugstr('cw : '+inttostr(colclicked)+'.'+inttostr(rowclicked));
 {$ENDIF}
 if assigned(FOnEndColumnSize) and colsized then FOnEndColumnSize(self,colclicked);
 colsized:=false;
 colclicked:=-1;
 rowclicked:=-1;
 if HasCheckBox(col,row) then HideEditor;
end;

procedure TAdvStringGrid.RowHeightsChanged;
begin
 if (csDesigning in ComponentState) then
  begin
   if FScrollHintShow then FScrollHintWnd.ReleaseHandle;
   FScrollHintShow:=false;
  end;
 inherited RowHeightsChanged;
 if HasCheckBox(col,row) then HideEditor;
end;


{-----------------------------------------------
 v0.98 : added function to cache cell content
         when active cell changes
--------------------------------------------------}

{$IFDEF EDITCONTROLS}
procedure TAdvStringGrid.ClearComboString;
begin
 editcombo.items.clear;
end;

function TAdvStringGrid.RemoveComboString(const s:string):boolean;
var
 i:integer;
begin
 i:=editcombo.items.indexof(s);
 result:=(i<>-1);
 if (i<>-1) then editcombo.items.delete(i);
end;

function TAdvStringGrid.SetComboSelectionString(const s:string):boolean;
var
 i:integer;
begin
 i:=editcombo.items.indexof(s);
 result:=(i<>-1);
 if (i<>-1) then fcomboidx:=i;
end;

procedure TAdvStringGrid.AddComboString(const s:string);
begin
 editcombo.items.add(s);
end;

procedure TAdvStringGrid.SetComboSelection(idx:integer);
begin
 fcomboidx:=idx;
end;

function TAdvStringGrid.GetComboCount:integer;
begin
 GetComboCount:=editcombo.items.count;
end;

function TAdvStringGrid.IsPassword(Acol,Arow:integer):boolean;
var
 isPassword:boolean;
begin
 result:=false;
 if assigned(OnIsPasswordCell) then
  begin
   OnIsPasswordCell(self,acol,arow,isPassword);
   result:=isPassword;
  end;
end;

function TAdvStringGrid.IsEditable(Acol,Arow:integer):boolean;
var
 isfixed,isedit:boolean;
begin
 result:=true;
 if (not assigned(OnCanEditCell)) and
    (not assigned(OnIsFixedCell)) then exit;

 isfixed:=false; isedit:=true;
 if assigned(OnCanEditCell) then OnCanEditCell(self,Arow,Acol,isedit);
 if assigned(OnIsFixedCell) then OnIsFixedCell(self,Arow,Acol,isfixed);
 result:=isedit{ and not isfixed};
end;

function TAdvStringGrid.IsFixed(Acol,Arow:integer):boolean;
var
 isfixed:boolean;
begin
 result:=false;

 if (arow >= rowcount-fixedfooters) or
    (acol >= colcount-fixedrightcols) then
     begin
      result:=true;
      exit;
     end;

 if (not assigned(OnIsFixedCell)) then exit;
 isfixed:=false;
 OnIsFixedCell(self,Arow,Acol,isfixed);
 result:=isfixed;
end;


procedure TAdvStringGrid.HideEditControl(acol,arow:integer);
begin
 {
 (edNormal,edSpinEdit,edComboEdit,edComboList,edEditBtn,edCheckBox,edDateEdit);
 }
 acol:=remapcol(acol);

 case editcontrol of
 edComboEdit,edComboList:
   begin
    if editmode then
     begin
      self.cells[acol,arow]:=editcombo.text;
      editcombo.enabled:=false;
      editcombo.visible:=false;
      editmode:=false;
     end;
   end;
 edSpinEdit:
   begin
      self.cells[acol,arow]:=inttostr(editspin.value);
      editspin.enabled:=false;
      editspin.visible:=false;
      editmode:=false;
   end;
 edDateEdit,edDateEditUpDown:
   begin
      {$IFDEF DELPHI3_LVL}
      if (GetFileVersion('COMCTL32.DLL')>=$00040046) then
       begin
        self.cells[acol,arow]:=datetostr(editdate.date);
        editdate.enabled:=false;
        editdate.visible:=false;
        editmode:=false;
       end;
      {$ENDIF}
   end;
 edTimeEdit:
   begin
      {$IFDEF DELPHI3_LVL}
      if (GetFileVersion('COMCTL32.DLL')>=$00040046) then
       begin
        self.cells[acol,arow]:=timetostr(editdate.time);
        editdate.enabled:=false;
        editdate.visible:=false;
        editmode:=false;
       end;
      {$ENDIF}
   end;
 edCheckBox:
   begin
      editcheck.enabled:=false;
      editcheck.visible:=false;
      editmode:=false;
   end;
 edEditBtn:
   begin
      self.cells[acol,arow]:=editbtn.text;
      editbtn.enabled:=false;
      editbtn.visible:=false;
      editmode:=false;
   end;
 edUnitEditBtn:
   begin
      self.cells[acol,arow]:=uniteditbtn.text+uniteditbtn.unitid;
      uniteditbtn.enabled:=false;
      uniteditbtn.visible:=false;
      editmode:=false;
   end;
 edButton:
   begin
      gridbutton.enabled:=false;
      gridbutton.visible:=false;
      editmode:=false;
   end;

 end;
end;

procedure TAdvStringGrid.ShowEditControl(acol,arow:integer);
var
 r:trect;
 s:string;
begin
 {
 (edNormal,edSpinEdit,edComboEdit,edComboList,edEditBtn,edCheckBox,edDateEdit);
 }
 {$IFDEF DEBUG}
 outputdebugstr('show edit call');
 {$ENDIF}
 r:=self.cellrect(acol,arow);

 acol:=remapcol(acol);

 case editcontrol of
 edComboEdit:
   begin
     editmode:=true;
     editcombo.Style := csDropDown;
     editcombo.top:=r.top;
     editcombo.left:=r.left;
     editcombo.width:=r.right-r.left;
     editcombo.height:=r.bottom-r.top+200;
     editcombo.text:=self.cells[acol,arow];
     if fNavigation.FAutoComboDropSize then
        editcombo.SizeDropDownWidth;

     editcombo.enabled:=true;
     editcombo.visible:=true;
     editcombo.setfocus;
   end;
 edComboList:
   begin
     editmode:=true;
     editcombo.Style := csDropDownList;
     editcombo.itemindex := editcombo.Items.IndexOf(self.cells[acol,arow]);

     if ((fcomboidx=-1) and (editcombo.itemindex=-1)) then editcombo.itemindex:=0
     else if (editcombo.itemindex=-1) then editcombo.itemindex:=fComboIdx;

     editcombo.top:=r.top;
     editcombo.left:=r.left;
     editcombo.width:=r.right-r.left;
     editcombo.height:=r.bottom-r.top+200;
     editcombo.text:=self.cells[acol,arow];
     if fNavigation.FAutoComboDropSize then
        editcombo.SizeDropDownWidth;

     editcombo.enabled:=true;
     editcombo.visible:=true;
     editcombo.setfocus;
   end;

 edSpinEdit:
   begin
     editmode:=true;
     editspin.recreatewnd;
     editspin.top:=r.top;
     editspin.left:=r.left;
     editspin.width:=r.right-r.left;
     editspin.height:=r.bottom-r.top;
     editspin.value:=self.ints[acol,arow];
     editspin.visible:=true;
     editspin.enabled:=true;
     editspin.setfocus;
   end;
 edDateEdit,edDateEditUpdown:
   begin
     {$IFDEF DELPHI3_LVL}
     if (GetFileVersion('COMCTL32.DLL')>=$00040046) then
      begin
       editmode:=true;
       editdate.recreatewnd;
       try
        editdate.date:=strtodate(self.cells[acol,arow]);;
       except
        editdate.date:=Now;
       end;
       editdate.kind:=dtkDate;
       if editcontrol=edDateEditUpdown then editdate.DateMode :=dmUpDown;
       editdate.top:=r.top;
       editdate.left:=r.left;
       editdate.width:=r.right-r.left;
       editdate.enabled:=true;
       editdate.visible:=true;
       editdate.setfocus;
      end;
     {$ENDIF}
   end;
 edTimeEdit:
   begin
     {$IFDEF DELPHI3_LVL}
     if (GetFileVersion('COMCTL32.DLL')>=$00040046) then
      begin
       editmode:=true;
       editdate.recreatewnd;
       try
        editdate.time:=strtotime(self.cells[acol,arow]);;
       except
        editdate.time:=Now;
       end;
       editdate.kind:=dtkTime;
       editdate.top:=r.top;
       editdate.left:=r.left;
       editdate.width:=r.right-r.left;
       editdate.enabled:=true;
       editdate.visible:=true;
       editdate.setfocus;
      end;
     {$ENDIF}
   end;
 edCheckBox:
   begin
     editmode:=true;
     editcheck.recreatewnd;
     editcheck.top:=r.top;
     editcheck.left:=r.left;
     editcheck.width:=r.right-r.left;
     editcheck.height:=r.bottom-r.top;
     editcheck.caption:=self.cells[acol,arow];
     editcheck.enabled:=true;
     editcheck.checked:=false;
     editcheck.state:=cbUnchecked;
     editcheck.visible:=true;
     editcheck.setfocus;
   end;
 edEditBtn:
   begin
    editmode:=true;
    editbtn.recreatewnd;
    editbtn.top:=r.top;
    editbtn.left:=r.left;
    editbtn.width:=r.right-r.left;
    editbtn.text:=self.cells[acol,arow];
    editbtn.visible:=true;
    editbtn.enabled:=true;
    editbtn.setfocus;
    editbtn.height:=r.bottom-r.top;
   end;
 edUnitEditBtn:
   begin
    editmode:=true;
    uniteditbtn.recreatewnd;
    uniteditbtn.top:=r.top;
    uniteditbtn.left:=r.left;
    uniteditbtn.width:=r.right-r.left;
    s:=self.cells[acol,arow];
    uniteditbtn.UnitID:='';
    uniteditbtn.text:='';
    while (length(s)>0) do
     begin
      if s[1] in ['0'..'9','.',',','-'] then
      uniteditbtn.text:=uniteditbtn.text+s[1] else
      uniteditbtn.unitid:=uniteditbtn.unitid+s[1];
      delete(s,1,1);
     end;
    uniteditbtn.visible:=true;
    uniteditbtn.enabled:=true;
    uniteditbtn.setfocus;
    uniteditbtn.height:=r.bottom-r.top;
   end;
 edButton:
   begin
    editmode:=true;
    gridbutton.recreatewnd;
    gridbutton.top:=r.top-1;
    gridbutton.left:=r.left-1;
    gridbutton.width:=r.right-r.left+2;
    gridbutton.height:=r.bottom-r.top+2;
    gridbutton.text:=self.cells[acol,arow];
    gridbutton.visible:=true;
    gridbutton.enabled:=true;
    gridbutton.setfocus;
   end;
 end;
end;

procedure TAdvStringGrid.RestoreCache;
begin
 cells[remapcol(col),row]:=cellcache;
 {cause an edit update when cell is remapped due to hidden column}
 cells[col,row]:=cells[col,row];
end;
{$ENDIF}

function TAdvStringGrid.CanEditShow:boolean;
begin
 {$IFDEF DEBUG}
  senddebug('in caneditshow');
 {$ENDIF}

 result:=inherited CanEditshow;

 {$IFDEF DEBUG}
 outputdebugstr('canshowedit called for '+inttostr(col)+':'+inttostr(row));
 if editmode=false then outputdebugstr('editmode=false');
 if result=false then outputdebugstr('inherited caneditshow=false');
 {$ENDIF}

 {$IFDEF EDITCONTROLS}
 if (csDesigning in ComponentState) then exit;

 if result and (editmode=false) and (HasStaticEdit(remapcol(col),row)=true) then
    result:=false;

 if result and (editmode=false) then
  begin
   if assigned(OnGetEditorType) then
    begin {do caching}
     cellcache:=cells[remapcol(col),row];
     editcontrol:=edNormal;
     OnGetEditorType(self,remapcol(col),row,editcontrol);
     if not (editcontrol in [edNormal,edNumeric,edFloat,edCapital,edMixedCase,edPassword,edUpperCase,edLowerCase]) then
       begin
        ShowEditControl(col,row);
        result:=false;
       end;
    end;
  end;
 {$ENDIF}
end;

function TAdvStringGrid.SelectCell(ACol, ARow: Longint): Boolean;
var
 canedit:boolean;
 canchange:boolean;
begin
 canchange:=true;
 if (arow<>row) and assigned(fOnRowChanging) then
   fOnRowChanging(self,row,arow,canchange);

 if (acol<>col) and assigned(fOnColChanging) then
   fOnColChanging(self,col,acol,canchange);

 if ((acol<>col) or (arow<>row)) and assigned(fOnCellChanging) then
   fOnCellChanging(self,row,col,arow,acol,canchange);

 if ((acol=0) and (fNumNodes>0)) or not canchange then
   begin
    result:=false;
    exit;
   end;
   
 cellcache:=cells[remapcol(acol),arow];

 if Assigned(FOnCanEditCell) and not HasStaticEdit(acol,arow) then
     begin
      canedit:=true;
      FOnCanEditCell(self,arow,remapcol(acol),canedit);
      if canedit then
       self.Options:=self.Options+[goEditing]
      else
       self.Options:=self.Options-[goEditing];
     end;

 if IsFixed(acol,arow) then
   result:=false
  else
   begin
    result:=inherited SelectCell(Acol,Arow);
   end;
end;

function TAdvStringGrid.FreeCellGraphic(ACol,Arow:integer):boolean;
var
 cg:TCellGraphic;
begin
 result:=false;
 cg:=GetCellGraphic(Acol,Arow);
 if cg=nil then exit else cg.Free;
 self.Objects[Acol,Arow]:=nil;
 result:=true;
end;

function TAdvStringGrid.CreateCellGraphic(ACol,Arow:integer):TCellGraphic;
var
 cg:TCellGraphic;
begin
 cg:=GetCellGraphic(Acol,Arow);
 if assigned(cg) then cg.Free;
 cg:=TCellGraphic.Create;
 if (cells[acol,arow]='') then cells[acol,arow]:=' ';
 self.Objects[Acol,Arow]:=cg;
 {make sure object gets allocated}
 result:=cg;
end;

function TAdvStringGrid.GetCellGraphic(ACol,Arow:integer):TCellGraphic;
{$IFNDEF WIN32}
var
 tmp:tcellgraphic;
{$ENDIF}
begin
 result:=nil;
 if not EnableGraphics then exit;
 {$IFNDEF WIN32}
 tmp:=self.objects[acol,arow] as tcellgraphic;
 if assigned(tmp) then
 {$ELSE}
 if assigned(self.objects[acol,arow]) then
 {$ENDIF}
 begin
  if not (self.objects[acol,arow] is TCellGraphic) then exit;
  result:=(self.objects[acol,arow] as TCellGraphic);
 end;
end;

function TAdvStringGrid.GetCellGraphicSize(ACol,Arow:integer):TPoint;
var
 cg:tcellgraphic;
 w,h:integer;
 s:string;
begin
 result.x:=0;
 result.y:=0;
 if not EnableGraphics then exit;
 cg:=CellGraphics[acol,arow];
 if cg=nil then exit;
 w:=0; h:=0;
 s:=cells[acol,arow];

 case cg.celltype of
 ctIcon:
  begin
   if (cg.CellHAlign in [haBeforeText,haAfterText]) then
          w:=cg.cellicon.width
   else if (s='') then w:=cg.cellicon.width;

   h:=cg.cellicon.height;
   {
   if (cg.CellVAlign in [vaUnderText,vaAboveText]) then
          h:=cg.cellicon.height
   else if (s='') then h:=cg.cellicon.height;
   }
  end;

 ctPicture:
  begin
   h:=tpicture(cg.cellbitmap).bitmap.height;
   w:=tpicture(cg.cellbitmap).bitmap.width;
  end;

 ctBitmap:
  begin
   if (cg.CellHAlign in [haBeforeText,haAfterText]) then
         w:=cg.cellbitmap.width
   else if (s='') then w:=cg.cellbitmap.width;

   h:=cg.cellbitmap.height;
   {
   if (cg.CellVAlign in [vaUnderText,vaAboveText]) then
         h:=cg.cellbitmap.height
   else if (s='') then h:=cg.cellbitmap.height;
   }
  end;
 ctImageList:
  begin
   if (cg.CellHAlign in [haBeforeText,haAfterText]) then
         w:=gridimages.width
   else if (s='') then w:=gridimages.width;

   h:=gridimages.height;
   {
   if (cg.CellVAlign in [vaUnderText,vaAboveText]) then
          h:=gridimages.height
   else if (s='') then h:=gridimages.height;
   }
  end;
 ctCheckbox,ctDataCheckBox:
  begin
    w:=15;
    h:=15;
  end;
 ctImages:
  begin
   if cg.CellBoolean then
    begin
     w:=CellImages[acol,arow].Count*gridimages.width;
     h:=gridimages.height;
    end
   else
    begin
     h:=CellImages[acol,arow].Count*gridimages.height;
     w:=gridimages.width;
    end;
  end;
 end;
 result.x:=w;
 result.y:=h;
end;



function TAdvStringGrid.GetCellType(ACol,Arow:integer):TCellType;
{$IFNDEF WIN32}
var
 tmp:tcellgraphic;
{$ENDIF}
begin
 result:=ctEmpty;
 if not EnableGraphics then exit;
 {$IFNDEF WIN32}
 tmp:=self.objects[acol,arow] as tcellgraphic;
 if assigned(tmp) then
 {$ELSE}
 if assigned(self.objects[acol,arow]) then
 {$ENDIF}
  begin
   if not (self.objects[acol,arow] is TCellGraphic) then exit;
   result:=(self.objects[acol,arow] as TCellGraphic).CellType;
  end;
end;

function TAdvStringGrid.GetCellImages(Acol,Arow:integer):TIntList;
begin
  if (celltypes[acol,arow]=ctImages) then
   result:=TIntList((self.Objects[acol,arow] as TCellGraphic).CellBitmap) else result:=nil;
end;

procedure TAdvStringGrid.SetInts(ACol,ARow:Integer;const Value:integer);
begin
 cells[acol,arow]:=inttostr(value);
end;

function TAdvStringGrid.GetInts(ACol,ARow:Integer):integer;
var
 s:string;
 res,code:integer;
begin
 s:=cells[acol,arow];
 if (s='') then s:='0';
 val(s,res,code);
 if (code<>0) then raise EAdvGridError.Create('Cell does not contain integer value');
 GetInts:=res;
end;

function TAdvStringGrid.GetPrintColWidth(Acol:integer):integer;
begin
 result:=-1;
 if (acol<maxcolumns) and (acol>=0) then
   result:=maxwidths[acol]
 else
   EAdvGridError.Create('Columns is not in valid range');
end;

function TAdvStringGrid.GetPrintColOffset(Acol:integer):integer;
begin
 result:=-1;
 if (acol<maxcolumns) and (acol>=0) then
   result:=indents[acol]
 else
   EAdvGridError.Create('Columns is not in valid range');
end;

procedure TAdvStringGrid.SetLookupItems(Value: TStringList);
begin
 if assigned(value) then
  flookupitems.assign(value);
end;

procedure TAdvStringGrid.FixedFontChanged(sender:tObject);
begin
 self.repaint;
end;

procedure TAdvStringGrid.MultiImageChanged(Sender: TObject; Acol,Arow:integer);
begin
 {force a cell update}
 self.Cells[acol,arow]:= self.Cells[acol,arow];
end;

procedure TAdvStringGrid.SetFixedFont(value:tfont);
begin
 ffixedfont.assign(value);
 self.Repaint;
end;

procedure TAdvStringGrid.SetColumnHeaders(Value: TStringList);
begin
 FColumnHeaders.Assign(Value);
 if (FixedRows>0) then ClearColumnHeaders;
 ShowColumnHeaders;
end;

procedure TAdvStringGrid.ColHeaderChanged(Sender:tObject);
begin
 ShowColumnHeaders;
end;

procedure TAdvStringGrid.ClearColumnHeaders;
var I: Integer;
begin
 if (ColCount>0) then
  for I:=0 to ColCount-1 do Cells[I,0]:='';
end;

procedure TAdvStringGrid.ShowColumnHeaders;
var I: Integer;
begin
 if (FixedRows>0) then
  for I:=0 to FColumnHeaders.Count-1 do
    if (I<ColCount) then Cells[I,0] := CLFToLf(FColumnHeaders[I]);
end;

procedure TAdvStringGrid.SetRowHeaders(Value: TStringList);
begin
 FRowHeaders.Assign(Value);
 if (csDesigning in ComponentState) then
  begin
   if (FixedCols>0) then ClearRowHeaders;
  end;
 ShowRowHeaders;
end;

procedure TAdvStringGrid.RowHeaderChanged(Sender:tObject);
begin
 ShowRowHeaders;
end;

procedure TAdvStringGrid.ClearRowHeaders;
var I: Integer;
begin
 if (RowCount>0) then
  for I:=0 to RowCount-1 do Cells[0,I]:='';
end;

procedure TAdvStringGrid.ShowRowHeaders;
var I: Integer;
begin
 if (FixedCols>0) then
  for I:=0 to FRowHeaders.Count-1 do
    if (I<RowCount) then Cells[0,I] := CLFToLF(FRowHeaders[I]);
end;

procedure TAdvStringGrid.RandomFill(doFixed:boolean;rnd:integer);
var
 i,j:integer;
 ro,co:integer;
begin
 if dofixed then
  begin
   ro:=0;
   co:=0;
  end
 else
  begin
   ro:=fixedrows;
   co:=fixedcols;
  end;

 for i:=ro to rowcount-1 do
  for j:=co to colcount-1 do
   begin
    ints[j,i]:=random(rnd);
   end;

end;

procedure TAdvStringGrid.SetDates(ACol,ARow:Integer;const Value:tDatetime);
begin
 cells[acol,arow]:=DateToStr(value);
end;

function TAdvStringGrid.GetDates(ACol,ARow:Integer):tdatetime;

begin
  GetDates:=StrToDate(cells[acol,arow]);
end;

function TAdvStringGrid.GetRowSelect(ARow:integer):boolean;
var
 i,j:integer;

begin
 if (Arow>=Rowcount) or (Arow<0) then
    raise EAdvGridError.Create('Invalid row accessed');

 i:=fRowSelect.Count;

 if (i<Arow+1) then
   begin
    fRowSelect.Count:=Arow+1;
    for j:=i to fRowSelect.Count-1 do
     fRowSelect.Items[j]:=nil;
   end;
 GetRowSelect:=(fRowSelect.Items[Arow]<>nil) {or ((arow<=y2) and (arow>=y1))};
end;

procedure TAdvStringGrid.RepaintRect(r:trect);
var
 r1,r2,ur:trect;
begin
 r1:=cellrect(r.left,r.top);
 r2:=cellrect(r.right,r.bottom);
 unionrect(ur,r1,r2);
 if IsRectEmpty(r1) or IsRectEmpty(r2) then repaint else
 invalidaterect(self.handle,@ur,true);
end;

procedure TAdvStringGrid.RepaintRow(aRow:integer);
var
 r:trect;
begin
 r:=cellrect(0,arow);
 r.right:=r.left+self.width;
 invalidaterect(self.handle,@r,true);
end;

procedure TAdvStringGrid.SelectRows(RowIndex, RCount: LongInt);
var
 gr:tgridrect;
begin
 gr.left:=fixedcols;
 gr.right:=colcount-1;
 gr.top:=rowindex;
 gr.bottom:=rowindex+rcount-1;
 selection:=gr;
end;

procedure TAdvStringGrid.SelectCols(ColIndex, CCount: LongInt);
var
 gr:tgridrect;
begin
 gr.left:=ColIndex;
 gr.right:=ColIndex+CCount-1;
 gr.top:=fixedrows;
 gr.bottom:=rowcount-1;
 selection:=gr;
end;

procedure TAdvStringGrid.SelectRange(FromCol,ToCol,FromRow,ToRow: Longint);
var
 gr:tgridrect;
begin
 gr.left:=fromcol;
 gr.right:=tocol;
 gr.top:=fromrow;
 gr.bottom:=torow;
 selection:=gr;
end;


procedure TAdvStringGrid.ClearRowSelect;
var
 i:integer;
begin
 if (fRowSelect.Count<=0) then exit;
 for i:=0 to fRowSelect.Count-1 do
   begin
    if fRowSelect.Items[i]<>nil then RepaintRow(i);
    fRowSelect.Items[i]:=nil;
   end;
end;

procedure TAdvStringGrid.SelectToRowSelect(aRow:integer);
var
 i,y1,y2:integer;
begin
 y1:=selection.Top;
 y2:=selection.Bottom;
 if (y1>=0) and (y2>=0) then
  for i:=y1 to y2 do
   begin
    if RowSelect[i]=false then
    {optimize for repaint: do not repaint, range already repainted}
    if (y1<>y1) then
     fRowSelect.Items[Arow]:=pointer(1)
    else
     RowSelect[i]:=true;
   end;
end;

function TAdvStringGrid.GetRowSelectCount:integer;
var
 ret,i:integer;
begin
 ret:=0;
 for i:=1 to fRowSelect.Count do
  if (fRowSelect.Items[i-1]<>nil) then inc(ret);
 result:=ret;
end;

procedure TAdvStringGrid.SetRowSelect(ARow:integer;value:boolean);
var
 i,j:integer;
begin
 if (Arow>=Rowcount) or (Arow<0) then
    raise EAdvGridError.Create('Invalid row accessed');

 i:=fRowSelect.Count;
 if (i<Arow+1) then
   begin
    fRowSelect.Count:=Arow+1;
    for j:=i to fRowSelect.Count-1 do
     fRowSelect.Items[j]:=nil;
   end;

 if value then
  begin
   if (fRowSelect.Items[Arow]<>pointer(1)) then repaintrow(arow);
   fRowSelect.Items[Arow]:=pointer(1);
  end
 else
  begin
   if (fRowSelect.Items[Arow]<>nil) then repaintrow(arow);
   fRowSelect.Items[Arow]:=nil;
  end;
end;

function TAdvStringGrid.GetInplaceEditor:TAdvInplaceEdit;
begin
 result:=tadvinplaceedit(inplaceeditor);
end;

{------------------------------------------
 v 0.98 : added functionality to restore cell
          content when ESC is pressed
-------------------------------------------}
procedure TAdvStringGrid.AdvanceEdit(acol,arow:integer;advance,show,frwrd:boolean);
var
 oldcol:integer;
begin
 oldcol:=acol;
 if (not FNavigation.AdvanceOnEnter) and (not advance) then exit;
 if (frwrd) then
 begin
 if FNavigation.AdvanceDirection=adLeftRight then
  begin
   if (acol=colcount-1-ffixedrightcols) then
    begin
     if (arow=rowcount-1-ffixedfooters) then
       begin
        if (FNavigation.AdvanceInsert) then {automatic arowinsert}
          begin
           rowcount:=rowcount+1;
           arow:=arow+1;
           acol:=fixedcols;
           if Assigned(FOnAutoInsertrow) then
              FOnAutoInsertrow(self,rowcount-1-ffixedfooters);
          end
        else {skip back to first cell}
          begin
           arow:=fixedrows;
           acol:=fixedcols;
          end;
       end
     else
       begin
        acol:=fixedcols;
        arow:=arow+1;
      end;
   { changed in v1.83 -- repaint; }
    end
   else
    begin
     acol:=acol+1;
    end;
  end;

 if FNavigation.AdvanceDirection=adTopBottom then
  begin
   if (arow=rowcount-1-ffixedfooters) then
    begin
     if (acol=colcount-1-ffixedrightcols) then
       begin
        if (FNavigation.AdvanceInsert) then
          begin
           colcount:=colcount+1;
           acol:=acol+1;
           arow:=fixedrows;
           if Assigned(FOnAutoInsertcol) then
              FOnAutoInsertcol(self,colcount-1-ffixedrightcols);
          end
         else
          begin
           arow:=fixedrows;
           acol:=fixedcols;
          end;
       end
     else
       begin
        arow:=fixedrows;
        acol:=acol+1;
      end;
     repaint;
    end
   else
    begin
     arow:=arow+1;
    end;
  end;
 end
 else
 begin
  if FNavigation.AdvanceDirection=adLeftRight then
   begin
    if (acol>fixedcols) then acol:=acol-1
    else if (arow<fixedrows) then
      begin
       arow:=arow-1;
       acol:=colcount-1-ffixedrightcols;
      end;
   end;

 if FNavigation.AdvanceDirection=adTopBottom then
  begin
   if (arow<fixedrows) then arow:=arow-1
   else if (acol<fixedcols) then
    begin
     acol:=acol-1;
     arow:=rowcount-1-ffixedfooters;
    end;
  end;
 end;

 if not IsEditable(remapcol(acol),arow) then AdvanceEdit(acol,arow,advance,show,frwrd)
 else
   begin
    row:=arow;
    col:=acol;
   end;

 {fixed code in v1.82, focus StaticEdit control on Advance}
 if show or HasStaticEdit(acol,arow) then
   begin
    ShowEditor;
    {refocus static edits}
    if hasstaticedit(acol,arow) then
      begin
       col:=oldcol;
       col:=acol;
       row:=arow;
      end;

   end;
 {
 if show and not HasStaticEdit(acol,arow) then ShowEditor;
 }
end;

procedure TAdvStringGrid.KeyPress(var Key:Char);
var
 p:integer;
begin
 {$IFDEF DEBUG}
 senddebug('key : '+inttostr(ord(key)));
 {$ENDIF}

 if (key=#27) then
   begin
    if (goEditing in Options) then RestoreCache;
    exit;
   end;

 if (key=#13) and ((getfocus<>self.handle) or HasStaticEdit(col,row)) then
  begin
   AdvanceEdit(col,row,false,false,true);
  end;

 if not(goEditing in Options) and
   (FNavigation.AutoGotoWhenSorted) then
   begin
    if (FNavigation.AutoGotoIncremental) then
     begin
      if (key=#8) then delete(searchinc,length(searchinc),1)
      else
       searchinc:=searchinc+upcase(key);
     end
    else searchinc:=upcase(key);

    p:=self.search(searchinc);
    if (p<>-1) then
     begin
      self.row:=p;
     end;
   end;

 inherited Keypress(key);
end;

procedure TAdvStringGrid.SetFixedFooters(value:integer);
begin
 fFixedFooters:=value;
 repaint;
end;

procedure TAdvStringGrid.SetFixedColWidth(value:integer);
begin
 colwidths[0]:=value;
end;

function TAdvStringGrid.GetFixedColWidth:integer;
begin
 result:= colwidths[0];
end;

procedure TAdvStringGrid.SetFixedRowHeight(value:integer);
begin
 rowheights[0]:=value;
end;

function TAdvStringGrid.GetFixedRowHeight:integer;
begin
 result:=rowheights[0];
end;

procedure TAdvStringGrid.SetFixedRightCols(value:integer);
begin
 fFixedRightCols:=value;
 repaint;
end;

procedure TAdvStringGrid.SetWordWrap(value:boolean);
begin
 fWordWrap:=value;
 if (inplaceeditor<>nil) then
  begin
   TAdvInplaceEdit(self.inplaceeditor).wordwrap:=value;
  end;
 repaint;
end;

function TAdvStringGrid.MatchFilter(arow:integer):boolean;
var
 i:integer;
begin
 result:=true;
 for i:=1 to fFilter.count do
  begin
   if not matchstr(fFilter.Items[i-1].Condition,cells[fFilter.Items[i-1].Column,arow]) then
     begin
      result:=false;
      break;
     end;
  end;
end;

procedure TAdvStringGrid.ApplyFilter;
var
 i:integer;
begin
 i:=fixedrows;
 while (i<rowcount) do
  begin
   if not MatchFilter(i) then hiderow(remaprowinv(i)) else inc(i);
  end;
end;

procedure TAdvStringGrid.SetFilterActive(const value:boolean);
begin
 if fFilterActive<>value then
  begin
   fFilterActive:=value;
   if fFilterActive then ApplyFilter else UnHideRowsAll;
  end;
end;

procedure TAdvStringGrid.SetSelectionColor(acolor:tcolor);
begin
 fSelectionColor:=aColor;
 self.repaint;
end;

procedure TAdvStringGrid.SetSelectionTextColor(acolor:tcolor);
begin
 fSelectionTextColor:=aColor;
 self.repaint;
end;

procedure TAdvStringGrid.SetSelectionRectangle(avalue:boolean);
begin
 fSelectionRectangle:=aValue;
 self.repaint;
end;

procedure TAdvStringGrid.SetSortColumn(aSortColumn:integer);
begin
 fSortColumn:=aSortColumn;
 self.repaint;
end;

procedure TAdvStringGrid.SetSortShow(aSortShow:boolean);
begin
 fSortShow:=aSortShow;
 repaint;
end;

procedure TAdvStringGrid.SetVAlignment(aVAlignment:TVAlignment);
begin
 fVAlignment:=aVAlignment;
 fvAlign:=DT_VCENTER;
 case fVAlignment of
 vtaTop:fvAlign:=DT_TOP;
 vtaBottom:fvAlign:=DT_BOTTOM;
 end;
 repaint;
end;


procedure TAdvStringGrid.SetAutoSize(aAutoSize:boolean);
begin
 fAutoSize:=aAutoSize;
 if fAutosize then AutoSizeColumns(false,10)
end;

function TAdvStringGrid.GetCellAlignment(acol,arow:integer):TAlignment;
var
 ret:TAlignment;
begin
 ret:=taLeftJustify;

 if fAutoNumAlign then
  begin
   if IsType(self.cells[acol,arow]) in [atNumeric,atFloat] then
    ret:=taRightJustify;
  end;

 if Assigned(FOnGetAlignment) then
  FOnGetAlignment(Self, ARow, ACol, ret);

 result:=ret;
end;

function TAdvStringGrid.GetCellTextSize(aCol,aRow:integer):tsize;
var
 s,su,anchor,stripped:string;
 maxsize,newsize,numlines:integer;
 r:trect;
begin
 maxsize:=0;
 numlines:=0;
 s:=Cells[aCol,aRow];

 if (pos('</',s)<>0) then
  begin
   fillchar(r,sizeof(r),0);
   r.right:=$ffff;
   r.bottom:=$ffff;
   HTMLDraw(canvas,s,r,gridimages,0,0,false,false,false,0.0,fURLColor,anchor,stripped,integer(result.cx),integer(result.cy));
   exit;
  end;

 if pos('{\',S)=1 then
  begin
   celltorich(aCol,aRow,self.RichEdit);
   s:=Self.RichEdit.text;
  end;
  
 repeat
  su:=GetNextLine(s,fMultiLineCells);
  newsize:=canvas.textwidth(su);
  if (newsize>maxsize) then maxsize:=newsize;
  inc(numlines);
 until (s='');
 
 GetCellTextSize.cx:=maxsize;
 GetCellTextSize.cy:=numlines*canvas.textheight('gh');
end;

procedure TAdvStringGrid.Paint;
var
 r:trect;
 i,j:integer;
begin
 inherited Paint;

 {
 r:=cellrect(2,1);
 s:=cellrect(3,1);
 r.right:=s.right;
 self.drawcell(2,1,r,[gdFixed]);
 }

 if assigned(OnIsFixedCell) then
  begin
   canvas.pen.color:=clBlack;
   for j:=fixedcols to colcount-1-ffixedrightcols do
    begin
       for i:=fixedrows to rowcount-1-ffixedfooters do
         begin
          if isFixed(j,i) then
           begin
            r:=cellrect(j,i);
            if (goFixedHorzLine in options) then
             begin
              canvas.moveto(r.left,r.bottom);
              canvas.lineto(r.right,r.bottom);
             end;

             if (goFixedVertLine in options) then
              begin
               canvas.moveto(r.right,r.top);
               canvas.lineto(r.right,r.bottom);
               dec(r.left);
               canvas.moveto(r.left,r.top);
               canvas.lineto(r.left,r.bottom);
              end;
           end;
         end;
    end;
  end;


 if (ffixedrightcols>0) and (ffixedrightcols<colcount) then
  begin
   canvas.pen.color:=clBlack;

   for j:=1 to ffixedrightcols do
    begin
       for i:=0 to rowcount-1 do
         begin
          r:=cellrect(colcount-j,i);
          if (goFixedHorzLine in options) then
           begin
            canvas.moveto(r.left,r.bottom);
            canvas.lineto(r.right,r.bottom);
           end;

          if (goFixedVertLine in options) then
           begin
            canvas.moveto(r.right,r.top);
            canvas.lineto(r.right,r.bottom);
            dec(r.left);
            canvas.moveto(r.left,r.top);
            canvas.lineto(r.left,r.bottom);
           end;
         end;
     end;
  end;


 if (ffixedfooters>0) and (ffixedfooters<rowcount) then
  begin
   canvas.pen.color:=clBlack;
   for j:=1 to ffixedfooters do
    begin
      if (goFixedHorzLine in options) then
       begin
        r:=cellrect(colcount-1,rowcount-j);
        dec(r.top);
        canvas.pen.color:=clBlack;
        canvas.moveto(0,r.top);
        canvas.lineto(r.right,r.top);
        canvas.moveto(0,r.bottom);
        canvas.lineto(r.right,r.bottom);
       end;

     if (goFixedVertLine in options) then
       begin
        for i:=0 to colcount-1 do
         begin
          r:=cellrect(i,rowcount-j);
          canvas.moveto(r.right,r.top);
          canvas.lineto(r.right,r.bottom);
         end;
       end;
    end;
  end;


end;

{$IFDEF WIN32}
procedure TAdvStringGrid.RTFPaint(acol,arow:integer;canvas:tcanvas;arect:trect);
const
 RTF_OFFSET :integer = 2;

type
  rFormatRange = record
    hdc: HDC;
    hdcTarget: HDC;
    rc: TRect;
    rcPage: TRect;
    chrg: TCharRange;
  end;

var
 fr:rFORMATRANGE;
 nLogPixelsX,nLogPixelsY:integer;
 mm:integer;
 pt:tpoint;
begin
 celltorich(acol,arow,frichedit);

 if (acol>=selection.left) and (acol<=selection.right) and
    (arow>=selection.top) and (arow<=selection.bottom) and
    ((row<>arow) or (col<>acol) or (getfocus<>self.handle)) and
    (not FSelectionRTFKeep) and (getmapmode(canvas.handle)=MM_TEXT) then
  begin
   frichedit.selstart:=0;
   frichedit.sellength:=255;
   frichedit.selattributes.Color:=fSelectionTextColor;
  end;

 FillChar(fr, SizeOf(TFormatRange), 0);

 lptodp(canvas.handle,arect.topleft,1);
 lptodp(canvas.handle,arect.bottomright,1);

 nLogPixelsX := GetDeviceCaps(canvas.handle,LOGPIXELSX);
 nLogPixelsY := GetDeviceCaps(canvas.handle,LOGPIXELSY);

 pt.x:=2; pt.y:=0;
 dptolp(canvas.handle,pt,1);

 RTF_OFFSET:= ((pt.x*nLogPixelsX) div 96);

 with fr do
  begin
   fr.hdc:=canvas.handle;
   fr.hdctarget:=canvas.handle;

   {convert to twips}
   fr.rcPage.left:=round(((arect.left+RTF_OFFSET)/nLogPixelsX)*1440);
   fr.rcPage.top:=round(((arect.top+RTF_OFFSET)/nLogPixelsY)*1440);
   fr.rcPage.right:=fr.rcPage.left+round(((arect.right-arect.left-RTF_OFFSET)/nLogPixelsX) * 1440);
   fr.rcPage.bottom:=(fr.rcPage.top+round(((arect.bottom-arect.top-RTF_OFFSET)/nLogPixelsY) * 1440));
   fr.rc.left:=fr.rcPage.left;  { 1440 TWIPS = 1 inch. }
   fr.rc.top:=fr.rcPage.top;
   fr.rc.right:=fr.rcPage.right;
   fr.rc.bottom:=fr.rcPage.bottom;
   fr.chrg.cpMin := 0;
   fr.chrg.cpMax := -1;
 end;

 mm:=getmapmode(canvas.handle);
 setmapmode(canvas.handle,mm_text);

 sendmessage(frichedit.handle,EM_FORMATRANGE,1,longint(@fr));

 {clear the richtext cache}
 sendmessage(frichedit.handle,EM_FORMATRANGE,0,0);

 setmapmode(canvas.handle,mm);
end;
{$ENDIF}

function TAdvStringGrid.CalcCell(ACol,ARow:integer):string;
begin
 result:=cells[ACol,ARow];
end;

procedure TAdvStringGrid.DrawSortIndicator(canvas:tcanvas;x,y:integer);
var
 left,vpos:integer;
begin
 left:=x;
 vpos:=y;
 if sortdirection=sdDescending then
 begin
  if (FSortUpGlyph.Empty) or (FSortDownGlyph.Empty) then
   begin
    canvas.pen.color:=clWhite;
    canvas.pen.width:=1;
    canvas.moveto(left+4,vpos-4);
    canvas.lineto(left,vpos+4);
    canvas.pen.color:=clGray;
    canvas.lineto(left-4,vpos-4);
    canvas.lineto(left+4,vpos-4);
    canvas.pen.color:=clBlack;
   end
   else
   begin
    {$IFDEF DELPHI3_LVL}
    FSortDownGlyph.Transparent:=true;
    FSortDownGlyph.TransparentMode:=tmAuto;
    {$ENDIF}
    canvas.Draw(left-4,vpos-4,FSortDownGlyph);
    {reset bk color since this is a Delphi 3 bug}
    setbkcolor(canvas.handle,ColorToRGB(fixedColor));
   end;
 end
else
 begin
  if (FSortUpGlyph.Empty) or (FSortDownGlyph.Empty) then
   begin
    canvas.pen.color:=clWhite;
    canvas.pen.width:=1;
    canvas.moveto(left-4,vpos+4);
    canvas.lineto(left+4,vpos+4);
    canvas.lineto(left,vpos-4);
    canvas.pen.color:=clGray;
    canvas.lineto(left-4,vpos+4);
    canvas.pen.color:=clBlack;
   end
   else
   begin
    {$IFDEF DELPHI3_LVL}
    FSortUpGlyph.Transparent:=true;
    FSortUpGlyph.TransparentMode:=tmAuto;
    {$ENDIF}
    canvas.Draw(left-4,vpos-4,FSortUpGlyph);
    {reset bk color since this is a Delphi 3 bug}
    setbkcolor(canvas.handle,ColorToRGB(fixedColor));
   end;
 end;
end;

procedure TAdvStringGrid.DrawCell(ACol, ARow : Longint; ARect : TRect;
  AState : TGridDrawState);
const
 WordWraps: array[Boolean] of LongInt=(DT_SINGLELINE,DT_WORDBREAK or DT_EDITCONTROL);

var
 graphicwidth,graphicheight:integer;
 maxtextwidth,maxtextheight:integer;
 hal:tCellHAlign;
 val:tCellVAlign;
 tal:TAlignment;
 FrameFlags1, FrameFlags2: longint;
 displtext:boolean;
 vpos:integer;
{$IFNDEF WIN32}
 tmp:tcellgraphic;
 tmpproc:tDrawCellEvent;
{$ENDIF}
{$IFDEF WIN32}
 LFont:TLogFont;
 hOldFont,hNewFont:HFont;
{$ENDIF}
{$IFDEF SPREADSHEET}
 r:double;
{$ENDIF}
 anchor,stripped:string;
 xsize,ysize:integer;

    procedure DrawBorders(acol,arow:integer;tr:trect);
    var
      oldpen:tpen;
      borders:tCellBorders;
    begin
     if assigned(fOnGetCellBorder) then
       begin
        oldpen:=tpen.create;
        oldpen.assign(canvas.pen);
        borders:=[];
        fOnGetCellBorder(self,arow,acol,canvas.pen,borders);

        tr.left:=tr.left+(canvas.pen.width shr 1);
        tr.right:=tr.right-(canvas.pen.width shr 1);
        tr.top:=tr.top+(canvas.pen.width shr 1);
        tr.bottom:=tr.bottom-(canvas.pen.width shr 1);

        if cbLeft in borders then
          begin
           canvas.moveto(tr.left,tr.top);
           canvas.lineto(tr.left,tr.bottom);
          end;
        if cbRight in borders then
          begin
           canvas.moveto(tr.right,tr.top);
           canvas.lineto(tr.right,tr.bottom);
          end;
        if cbTop in borders then
          begin
           canvas.moveto(tr.left,tr.top);
           canvas.lineto(tr.right,tr.top);
          end;
        if cbBottom in borders then
          begin
           canvas.moveto(tr.left,tr.bottom);
           canvas.lineto(tr.right,tr.bottom);
          end;
        canvas.pen.assign(oldpen);
        oldpen.Free;
      end;
  end;


  procedure DrawCheck(R:TRect;State:boolean);
  var
    DrawState: Integer;
    DrawRect: TRect;
  begin
    {$IFDEF WIN32}
    if state then
      DrawState:=DFCS_BUTTONCHECK or DFCS_CHECKED
    else
      DrawState:=DFCS_BUTTONCHECK;
    DrawRect.Left:=R.Left+(R.Right-R.Left-15) div 2;
    DrawRect.Top:=R.Top+(R.Bottom-R.Top-15) div 2;
    DrawRect.Right:=DrawRect.Left+15;
    DrawRect.Bottom:=DrawRect.Top+15;
    DrawFrameControl(self.Canvas.Handle,DrawRect,DFC_BUTTON,DrawState);
    {$ENDIF}
  end;

  procedure DrawRadio(R:TRect;Num,Idx:integer;dir:boolean;sl:tstrings);
  var
    DrawState: Integer;
    DrawRect: TRect;
    DrawNum:integer;
    DrawOfs,Th:integer;
    s:string;
  begin
    DrawOfs:=0;

    for DrawNum:=1 to Num do
     begin
      DrawState:=DFCS_BUTTONRADIO;
      if (DrawNum-1=Idx) then DrawState:=DrawState or DFCS_CHECKED;

      if dir then
       begin
        DrawRect.Left:=DrawOfs+R.Left+2+(DrawNum-1)*12;
        DrawRect.Top:=R.Top+(R.Bottom-R.Top-10) div 2;

        if assigned(sl) then
         begin
          if DrawNum<=sl.Count then
           begin
            s:= sl.Strings[DrawNum-1];
            canvas.textout(drawrect.left+10,drawrect.top-2,s);
            DrawOfs:=DrawOfs+canvas.textwidth(s);

            if (idx=-1) and (s=cells[acol,arow]) then
               DrawState:=DrawState or DFCS_CHECKED;
           end
          end;

       end
      else
       begin
        th:=canvas.textheight('gh');
        if assigned(sl) then
         begin
          if DrawNum<=sl.Count then
           begin
            s:=sl.Strings[DrawNum-1];
            DrawRect.Left:=R.Left+2;
            DrawRect.Top:=R.Top+2+(DrawNum-1)*th;
            canvas.textout(drawrect.left+th,drawrect.top-2,s);
            if (idx=-1) and (s=cells[acol,arow]) then
                 DrawState:=DrawState or DFCS_CHECKED;
           end;
         end
        else
         begin
          DrawRect.Left:=R.Left+(R.Right-R.Left-10) div 2;
          DrawRect.Top:=R.Top+2+(DrawNum-1)*th;
         end;
       end;
      DrawRect.Right:=DrawRect.Left+10;
      DrawRect.Bottom:=DrawRect.Top+10;
      DrawFrameControl(self.Canvas.Handle,DrawRect,DFC_BUTTON,DrawState);
     end;
  end;

  procedure DrawCellGraphic(r:trect;cellgraphic:tcellgraphic);
  var
   tgtrect,srcrect:trect;
   tmpbmp:tbitmap;
   srcColor:tcolor;
   bmpwi,bmphe,idx:integer;

  begin
   bmpwi:=0;
   bmphe:=0;
   if cellgraphic.CellType=ctBitmap then
     begin
      bmpwi:=cellgraphic.cellbitmap.width;
      bmphe:=cellgraphic.cellbitmap.height;
     end;

   if cellgraphic.CellType=ctPicture then
     begin
      bmpwi:=tpicture(cellgraphic.cellbitmap).bitmap.width;
      bmphe:=tpicture(cellgraphic.cellbitmap).bitmap.height;
     end;


   if cellgraphic.CellType=ctIcon then
     begin
      bmpwi:=cellgraphic.cellicon.width;
      bmphe:=cellgraphic.cellicon.height;
     end;

   if (cellgraphic.CellType in [ctCheckBox,ctDataCheckBox]) then
     begin
      bmpwi:=15;
      bmphe:=15;
     end;

   if (cellgraphic.CellType in [ctRadio]) then
     begin
      bmpwi:=0;
      bmphe:=0;
     end;

   {$IFDEF WIN32}
   if (cellgraphic.CellType in [ctImageList,ctDataImage]) then
     begin
      bmpwi:=self.FGridImages.Width;
      bmphe:=self.FGridImages.Height;
     end;
   if (cellgraphic.CellType = ctImages) then
     begin
      if cellgraphic.CellBoolean then
       begin
        bmpwi:=self.FGridImages.Width*TIntList(cellgraphic.CellBitmap).Count;
        bmphe:=self.FGridImages.Height;
       end
      else
       begin
        bmphe:=self.FGridImages.Height*TIntList(cellgraphic.CellBitmap).Count;
        bmpwi:=self.FGridImages.Width;
       end;
     end;

   {$ENDIF}

   srcRect.top:=0;
   srcrect.left:=0;
   srcrect.right:=bmpWi;
   srcrect.bottom:=bmphe;

   maxtextwidth:=maxtextwidth+3;

   case cellgraphic.CellHAlign  of
   haLeft:begin
            tgtrect.left:=r.left+1;
            tgtrect.right:=tgtrect.left+bmpwi;
          end;
   haRight:begin
            tgtrect.right:=r.right-1;
            tgtrect.left:=tgtrect.right-bmpwi;
           end;
   haCenter:begin
             if (bmpwi<r.right-r.left) then
               begin
                tgtrect.left:=r.left+(r.right-r.left-bmpwi) shr 1;
                tgtrect.right:=tgtrect.left+bmpwi;
               end
             else
               begin
                tgtrect.left:=r.left+1;
                tgtrect.right:=tgtrect.left+bmpwi;
               end;
            end;
   haBeforeText:begin
                  if (tal=taLeftJustify) then
                    begin
                     tgtrect.left:=r.left+1;
                     tgtrect.right:=tgtrect.left+bmpwi;
                    end
                  else
                    begin
                     tgtrect.left:=r.right-maxtextwidth-bmpwi;
                     tgtrect.right:=tgtrect.left+bmpwi;
                     if tgtrect.left<r.left then tgtrect.left:=r.left+1;
                    end
                end;
   haAfterText:begin
                  if tal=taRightJustify then
                    begin
                     tgtrect.right:=r.right-1;
                     tgtrect.left:=tgtrect.right-bmpwi;
                    end
                  else
                    begin
                     tgtrect.left:=r.left+maxtextwidth;
                     tgtrect.right:=tgtrect.left+bmpwi;
                    end
               end;
   end;

   case cellgraphic.CellVAlign  of
   vaTop,vaAboveText:begin
          tgtrect.top:=r.top+1;
          tgtrect.bottom:=tgtrect.top+bmphe;
         end;
   vaBottom:begin
             tgtrect.bottom:=r.bottom-1;
             tgtrect.top:=tgtrect.bottom-bmphe;
            end;
   vaCenter:begin
              if (bmphe<(r.bottom-r.top)) then
               begin
                tgtrect.top:=r.top+(r.bottom-r.top-bmphe) shr 1;
                tgtrect.Bottom:=tgtrect.Top + bmpHe;
               end
              else
               begin
                tgtrect.top:=r.top+1;
                tgtrect.bottom:=tgtrect.top+bmphe;
               end;
            end;
   vaUnderText:begin
                tgtrect.top:=r.top+1+maxtextheight;
                tgtrect.bottom:=tgtrect.top+bmphe;
               end;
   end;

   if (cellgraphic.CellType in [ctCheckBox,ctDataCheckBox]) then
     begin
      case GetCellAlignment(aCol,aRow) of
      taLeftJustify:begin
                     tgtrect.left:=r.left+1;
                     tgtrect.right:=tgtrect.left+bmpwi;
                    end;
      taRightJustify:begin
                       if cellgraphic.CellType=ctDataCheckBox then
                          tgtrect.left:=r.right-bmpwi
                       else
                          tgtrect.left:=r.right-maxtextwidth-bmpwi;

                       if tgtrect.left<r.left then tgtrect.left:=r.left+1;
                       tgtrect.right:=tgtrect.left+bmpwi;
                     end;
      taCenter:begin
                 if cellgraphic.CellType=ctDataCheckBox then
                   tgtrect.left:=r.left+((r.right-bmpwi-r.left) shr 1)
                 else
                   tgtrect.left:=r.left-bmpwi+(r.right-maxtextwidth-r.left) shr 1;

                 if tgtrect.left<r.left then tgtrect.left:=r.left+1;
                 tgtrect.right:=tgtrect.left+bmpwi;

               end;
      end;
     end;

   {$IFDEF WIN32}
   if (cellgraphic.CellType=ctRotated) then
    begin
      srcrect:=r;
      CalcTextPos(srcrect,cellGraphic.CellAngle,cells[acol,arow],tal);

      GetObject(self.canvas.Font.Handle,SizeOf(LFont),Addr(LFont));
      LFont.lfEscapement:=cellGraphic.CellAngle*10;
      LFont.lfOrientation:=cellGraphic.CellAngle*10;

      hNewFont:=CreateFontIndirect(LFont);
      hOldFont:=SelectObject(self.canvas.Handle,hNewFont);

      SetTextAlign(self.canvas.handle,TA_TOP);

      self.canvas.TextRect(r,srcrect.Left,srcrect.Top,self.cells[acol,arow]);

      hNewFont := SelectObject(self.canvas.Handle,hOldFont);
      DeleteObject(hNewFont);
    end;
   {$ENDIF}

   if (cellgraphic.CellType=ctBitmap) and (cellgraphic.celltransparent) then
    begin
     TmpBmp := TBitmap.Create;
     TmpBmp.Height := bmpHe;
     TmpBmp.Width := bmpWi;

     if ((gdSelected in astate) or
        (RowSelect[Arow] and (goRowSelect in Options) and (ACol>=FixedCols)))
         and not (gdFocused in astate) then
         TmpBmp.Canvas.Brush.Color:=fSelectionColor
     else
       TmpBmp.Canvas.Brush.Color:=self.canvas.brush.color;

     srcColor:=cellgraphic.CellBitmap.canvas.pixels[0,0];

     TmpBmp.Canvas.BrushCopy(srcRect,cellgraphic.CellBitmap,srcRect,srcColor );
     self.Canvas.CopyRect(tgtRect, TmpBmp.Canvas, srcRect);
     TmpBmp.Free;
    end;

   if (cellgraphic.CellType=ctBitmap) and (not cellgraphic.celltransparent) then
    begin
      self.canvas.draw(tgtrect.left,tgtrect.top,cellGraphic.cellbitmap);
    end;

   if (cellgraphic.CellType=ctPicture) then
    begin
      self.canvas.draw(tgtrect.left,tgtrect.top,tpicture(cellGraphic.cellbitmap).Graphic);
    end;

   if cellgraphic.CellType=ctIcon then
    begin
     self.Canvas.Draw(tgtrect.left,tgtrect.top,cellGraphic.cellIcon);
    end;

   if cellgraphic.CellType = ctCheckBox then
    begin
      DrawCheck(tgtrect,cellGraphic.CellBoolean);
    end;

   if cellgraphic.CellType = ctRadio then
    begin
     DrawRadio(r,cellgraphic.cellangle,cellgraphic.cellindex,cellgraphic.cellboolean,
               tstringlist(cellgraphic.cellbitmap));
    end;

   if cellgraphic.CellType = ctDataCheckBox then
    begin
      DrawCheck(tgtrect,cells[acol,arow]=FCheckTrue);
    end;

   {$IFDEF WIN32}
   if cellgraphic.CellType = ctImageList then
    begin
     FGridImages.Draw(self.canvas,tgtrect.left,tgtrect.top,cellgraphic.CellIndex);
    end;

   if cellgraphic.CellType = ctImages then
    begin
     for idx:=1 to TIntList(cellgraphic.CellBitmap).Count do
      begin
       FGridImages.Draw(self.canvas,tgtrect.left,tgtrect.top,TIntList(cellgraphic.CellBitmap).Items[idx-1]);
       if cellGraphic.CellBoolean then
        tgtrect.left:=tgtrect.left+gridimages.Width
       else
        tgtrect.top:=tgtrect.top+gridimages.Height;
      end;
    end;

   if cellgraphic.CellType = ctDataImage then
    begin
     try
      idx:=ints[acol,arow];
      if (idx+cellgraphic.CellIndex<FGridImages.Count) then
       FGridImages.Draw(self.canvas,tgtrect.left,tgtrect.top,idx+cellgraphic.CellIndex);
     finally
     end;

    end;

   if cellgraphic.CellType = ctNode then
    begin
     canvas.brush.color:=fCellNode.Color;

     if fCellNode.NodeType=cn3D then
      begin
       canvas.rectangle(r.left,r.top,r.right,r.Bottom);
       Frame3D(canvas,r,clWhite,clGray,1);
      end
     else
      begin
//       canvas.brush.Color:=fCellNode.Color;
//       canvas.pen.color:=fCellNode.Color;
//       canvas.rectangle(r.left,r.top,r.right,r.Bottom+1);
      end;

     if (fCellNode.NodeType=cnGlyph) and
        (not FCellNode.ExpandGlyph.Empty) and
        (not FCellNode.ContractGlyph.Empty) then
      begin
       if cellGraphic.cellboolean then
        begin
         DrawBitmapTransp(self.canvas,fCellNode.fContractGlyph,self.color,r);
        end
       else
        begin
         DrawBitmapTransp(canvas,fCellNode.fExpandGlyph,self.color,r);
        end;
       exit;
      end;

     r.left:=r.left+2;
     r.right:=r.left+8;
     r.top:=r.top+(r.bottom-r.top-8) shr 1;
     r.bottom:=r.top+8;

     if fCellNode.NodeType=cnFlat then
      begin
       canvas.pen.color:=fCellNode.NodeColor;
       canvas.rectangle(r.left-1,r.top-1,r.right+1,r.bottom+1);
       if cellGraphic.cellboolean then
        begin
         canvas.moveto(r.left+1,r.top+3);
         canvas.lineto(r.left+6,r.top+3);
         canvas.moveto(r.left+3,r.top+1);
         canvas.lineto(r.left+3,r.top+6);
        end
       else
        begin
         canvas.moveto(r.left+1,r.top+3);
         canvas.lineto(r.left+6,r.top+3);
        end;
      end
     else
      begin
       if cellGraphic.CellBoolean then
        DrawEdge(canvas.handle,r,EDGE_RAISED,BF_RECT or BF_SOFT)
       else
        DrawEdge(canvas.handle,r,EDGE_SUNKEN,BF_RECT or BF_SOFT);
      end;

    end;

   {$ENDIF}

  end;


  {$IFDEF WIN32}
  procedure DrawCellTextExt;
  var
    Text:array[0..255] of Char;
    AlignValue:TAlignment;
    Rect:TRect;

  begin
    Rect:=ARect;

    AlignValue:=GetCellAlignment(Acol,Arow);
    tal:=AlignValue;

    {centering text in cell}

    {drawing of text}
    with Rect do
     begin
      Dec(Right,2);
      Inc(Left,2);
      rect.top:=rect.top+2;
     end;

    if (alignvalue=taLeftJustify) and (hal=haBeforeText) then
     begin
      rect.left:=rect.left+graphicwidth;
     end;
    if (alignvalue=taLeftJustify) and (hal=haAfterText) then
     begin
      rect.right:=rect.right-graphicwidth;
     end;
    if (alignvalue=taRightJustify) and (hal=haAfterText) then
     begin
      rect.right:=rect.right-graphicwidth;
     end;
    if (alignvalue=taRightJustify) and (hal=haBeforeText) then
     begin
      rect.left:=rect.left+graphicwidth;
     end;
    if (val=vaAboveText) then
     begin
      rect.top:=rect.top+graphicheight;
     end;

    strpcopy(text,Cells[ACol,ARow]);

    DrawTextEx(Canvas.Handle, Text, StrLen(Text), Rect,(DT_EXPANDTABS or DT_NOPREFIX
    or fVAlign or DT_END_ELLIPSIS or DT_WORDBREAK) or Alignments[AlignValue],nil);

  end;
  {$ENDIF}

  procedure DrawWallPaperFixed(crect:trect);
  var
   SrcRect,DstRect,Irect:TRect;
   x,y,ox,oy:integer;
   dst:tpoint;
  begin
   dst.x:=FBackground.Left;
   dst.y:=FBackground.top;
   x:=FBackground.Bitmap.Width;
   y:=FBackground.Bitmap.Height;

   DstRect.top:=dst.y;
   DstRect.left:=dst.x;
   DstRect.right:=DstRect.left+x;
   DstRect.bottom:=DstRect.top+y;

   if not IntersectRect(irect,crect,dstrect) then exit;

   setbkmode(canvas.handle,TRANSPARENT);

   ox:=crect.left-dst.x;
   oy:=crect.top-dst.y;

   SrcRect.left:=ox;
   SrcRect.top:=oy;
   SrcRect.right:=ox+crect.right-crect.left;
   SrcRect.Bottom:=oy+crect.bottom-crect.top;

   DstRect:=crect;

   if (ox<=0) then
      begin
       DstRect.left:=dst.x;
       SrcRect.left:=0;
       SrcRect.right:=DstRect.right-DstRect.left;
      end;

   if (oy<=0) then
      begin
       DstRect.top:=dst.y;
       SrcRect.top:=0;
       SrcRect.Bottom:=DstRect.bottom-DstRect.top;
      end;

   if (SrcRect.left+(DstRect.right-DstRect.left)>x) then
       begin
        DstRect.right:=DstRect.left+x-SrcRect.left;
        SrcRect.Right:=x;
       end;

   if (SrcRect.top+DstRect.bottom-DstRect.top>y) then
       begin
        DstRect.bottom:=DstRect.top+y-SrcRect.top;
        SrcRect.bottom:=y;
       end;

   Canvas.CopyRect(DstRect,FBackground.Bitmap.Canvas,SrcRect);

  end;

  procedure DrawWallPaperTile(crect:trect);
  var
   SrcRect,DstRect:TRect;
   x,y,xo,yo,ox,oy,i:integer;
  begin
   x:=FBackground.Bitmap.Width;
   y:=FBackground.Bitmap.Height;
   setbkmode(canvas.handle,TRANSPARENT);

   ox:=0;
   for i:=FixedCols+1 to Acol do ox:=ox+ColWidths[i-1];
   oy:=0;
   for i:=FixedRows+1 to ARow do oy:=oy+RowHeights[i-1];

   ox:=ox mod x;
   oy:=oy mod y;

   SrcRect.left:=ox;
   SrcRect.top:=oy;
   SrcRect.right:=x;
   SrcRect.Bottom:=y;
   {$IFDEF DEBUG}
   outputdebugstr('wallpaper : '+inttostr(ox)+'-'+inttostr(oy)+' * '+inttostr(x-ox)+'-'+inttostr(y-oy)+' at ['+inttostr(acol)+':'+inttostr(arow)+']');
   {$ENDIF}
   yo:=cRect.Top;

   while (yo<cRect.Bottom) do
    begin
     xo:=cRect.Left;
     SrcRect.left:=ox;
     SrcRect.right:=x;
     while (xo<cRect.Right) do
      begin
       DstRect:=Rect(xo,yo,xo+srcRect.right-srcRect.left,yo+srcRect.bottom-srcRect.top);
       Canvas.CopyRect(DstRect,FBackground.Bitmap.Canvas,SrcRect);
       xo:=xo+srcRect.right-srcRect.left;
       srcrect.left:=0;
       srcrect.right:=x;
      end;
     yo:=yo+srcRect.bottom-srcRect.top;
     srcrect.top:=0;
     srcrect.bottom:=y;
    end;
  end;

  procedure DrawCellText;
  var
   {$IFNDEF WIN32}
   Text:array[0..255] of Char;
   {$ENDIF}
   AlignValue:TAlignment;
   FontHeight:Integer;
   Rect:TRect;
   tmpstr,su:string;
   cellwidth:integer;

  function ResizeString(s:string;size:integer):string;
  begin
    {$IFDEF WIN32}
    if URLShow and not URLFull then
     begin
      if (pos('://',s)>0) then delete(s,1,pos('://',s)+2);
      if (pos('mailto:',s)>0) then delete(s,1,pos('mailto:',s)+6);
     end;
    {$ENDIF}

    if (canvas.textwidth(s)>size) and fenhtextsize then
     begin
      while (canvas.textwidth(s+'...')>size) and (length(s)>0) do
        begin
         delete(s,length(s),1);
        end;
      if (length(s)>0) then s:=s+'...';
     end;
    if (canvas.textwidth(s)>maxtextwidth) then maxtextwidth:=canvas.textwidth(s);
    Resizestring:=s;
  end;

  function IsInRect(rc:tgridrect;c,r:integer):boolean;
  begin
   IsInRect:=(c>=rc.left) and (c<=rc.right) and (r>=rc.top) and (r<=rc.bottom);
  end;

  begin
    {$IFDEF WIN32}
    if ((gdSelected in Astate) or
       (RowSelect[Arow] and (goRowSelect in Options) and (Acol>=FixedCols))) and not
       ((gdFocused in AState) and not
       (goDrawFocusSelected in Options)) then
     begin
      if (fSelectionColor<>clNone) then canvas.brush.color:=fSelectionColor;
      if (fSelectionTextColor<>clNone) then canvas.font.color:=fSelectionTextColor;
     end;
    {$ENDIF}

    tmpstr:=Cells[ACol,ARow];

    if (pos('=',tmpstr)=1) then tmpstr:=CalcCell(Acol,Arow);

    if IsPassword(acol,arow) then StringToPassword(tmpstr,PasswordChar);

    if (pos('</',tmpstr)>0) then
     begin
      arect.left:=arect.left+2;
      arect.top:=arect.top+2;
      HTMLDraw(canvas,tmpstr,arect,gridimages,
             arect.left,arect.top,false,false,false,0.0,fURLColor,anchor,stripped,xsize,ysize);

      if (fsortshow) and (aCol=fSortColumn) and (arow=0) and (fixedRows>0) and ((acol+1>fixedcols) or (fSortFixedCols)) then
       begin
        case vAlignment of
        vtaTop:vpos:=arect.top+8;
        vtaCenter:vpos:=top+((arect.bottom-arect.top) shr 1);
        vtaBottom:vpos:=arect.bottom-8;
        end;
        DrawSortIndicator(canvas,arect.left+xsize+8,vpos);
       end;

      exit;
     end;

    {$IFDEF WIN32}
    if (pos('{\',tmpstr)=1) then
      begin
       canvas.pen.color:=canvas.brush.color;
       if not (gdSelected in aState) or (gdFocused in aState) or (fSelectionColor<>clNone) then
         canvas.rectangle(arect.left,arect.top,arect.right,arect.bottom);
       canvas.brush.Style := bsClear;
       RTFPaint(acol,arow,canvas,arect);
       canvas.brush.Style:= bsSolid;
       exit;
      end;
    {$ENDIF}

    {determine text alignment}
    AlignValue:=GetCellAlignment(Acol,Arow);
    {using of Brush for background color}
    tal:=AlignValue;

    Rect:=ARect;

    if (acol>=fixedcols) and (arow>=fixedrows) and (not fMouseDown) then
     if fMouseActions.DisjunctRowSelect and (RowSelect[arow]=false) then
      begin
       canvas.brush.color:=self.color;
       canvas.font.color:=self.font.color;

       if Assigned(FOnGetCellColor) then
          FOnGetCellColor(Self, ARow, ACol, AState, Canvas.Brush, Canvas.Font);
      end;

    if (fBackGround.Bitmap.Empty) or (gdFixed in AState) or (gdSelected in AState) then Canvas.FillRect(Rect)
    else
     begin
      if fBackground.Display=bdTile then
         DrawWallPaperTile(rect)
      else
         DrawWallPaperFixed(rect);
     end;

    if not displtext then exit;

    {now something complex to do it}

    if fSelectionRectangle then
    begin
      canvas.pen.color:=clBlack;
      canvas.pen.width:=2;

      with canvas do
      begin
      if (selection.left=acol) and isinrect(selection,acol,arow) then
        begin
          moveto(rect.left+1,rect.bottom-1);
          lineto(rect.left+1,rect.top+1);
        end;

      if (selection.right=acol) and isinrect(selection,acol,arow) then
        begin
          moveto(rect.right-1,rect.bottom-1);
          lineto(rect.right-1,rect.top+1);
        end;

      if (selection.top=arow) and isinrect(selection,acol,arow)  then
        begin
          moveto(rect.left+1,rect.top+1);
          lineto(rect.right-1,rect.top+1);
        end;

      if (selection.bottom=arow) and isinrect(selection,acol,arow) then
        begin
          moveto(rect.left+1,rect.bottom-1);
          lineto(rect.right-1,rect.bottom-1);
        end;
      end;

      canvas.pen.width:=1;
     { draw selection handle at bottom of selection
     if (arow=selection.bottom) and (acol=selection.right) then
      begin
        canvas.rectangle(rect.right-4,rect.bottom-4,rect.right,rect.bottom);
      end;
     }
    end;

    {centering text in cell}
    FontHeight:=Canvas.TextHeight('hg');
    {drawing of text}
    with Rect do
     begin
      Dec(Right,2);
      Inc(Left,2);
     end;

    if (alignvalue=taCenter) and (arow=0) and fsortshow and (acol=sortcolumn) then
     begin
      rect.left:=rect.left+10;
     end;

    {change here cell rectangle dependant of bitmap}

    if (alignvalue=taLeftJustify) and (hal=haBeforeText) then
     begin
      rect.left:=rect.left+graphicwidth;
     end;
    if (alignvalue=taLeftJustify) and (hal=haAfterText) then
     begin
      rect.right:=rect.right-graphicwidth;
     end;
    if (alignvalue=taRightJustify) and (hal=haAfterText) then
     begin
      rect.right:=rect.right-graphicwidth;
     end;
    if (alignvalue=taRightJustify) and (hal=haBeforeText) then
     begin
      rect.left:=rect.left+graphicwidth;
     end;

    if (val=vaAboveText) then
     begin
      rect.top:=rect.top+graphicheight;
     end;

    if (val=vaUnderText) then
     begin
      rect.bottom:=rect.bottom-graphicheight;
     end;

    rect.top:=rect.top+2;

    if (arow=0) and (aCol=fSortColumn) and fsortshow then
      cellwidth:=rect.right-rect.left-14
    else
      cellwidth:=rect.right-rect.left;

    {$IFDEF WIN32}
    if (URLShow) then
    if (pos('://',cells[acol,arow])>0) or (pos('mailto:',cells[acol,arow])>0) then
     begin
      canvas.font.style:=canvas.font.style+[fsUnderline];
      canvas.font.color:=URLColor;
     end;
    {$ENDIF}

    {
    if (acol=0) and (arow<=selection.bottom) and (arow>=selection.top) then
      canvas.font.Style:=canvas.font.Style+[fsBold];
    if (arow=0) and (acol<=selection.right) and (acol>=selection.left) then
      canvas.font.Style:=canvas.font.Style+[fsBold];
    }

    if LinesInText(tmpstr,fMultiLineCells)=1 then
     begin
      su:=GetNextLine(tmpstr,fMultiLineCells);
      su:=resizestring(su,cellwidth);
      {$IFNDEF WIN32}
      StrPLCopy(Text,su,255);
      DrawText(Canvas.Handle, Text, StrLen(Text), Rect,(DT_EXPANDTABS or DT_NOPREFIX)
      or WordWraps[WordWrap] or Alignments[AlignValue] or fValign);
      {$ELSE}
      DrawText(Canvas.Handle,pchar(su),Length(su), Rect,(DT_EXPANDTABS or DT_NOPREFIX)
      or WordWraps[WordWrap] or Alignments[AlignValue] or fValign);
      {$ENDIF}
     end
    else
     begin
      {$IFNDEF WIN32}
      StrPLCopy(Text,tmpstr,255);
      DrawText(Canvas.Handle,Text,StrLen(text), Rect,(DT_EXPANDTABS or DT_NOPREFIX)
      or WordWraps[WordWrap] or Alignments[AlignValue] or fValign);
      {$ELSE}
      DrawText(Canvas.Handle,pchar(tmpstr),length(tmpstr), Rect,(DT_EXPANDTABS or DT_NOPREFIX)
      or WordWraps[WordWrap] or Alignments[AlignValue] or fValign);
      {$ENDIF}
     end;

    maxtextheight:=Fontheight;

    if (fsortshow) and (aCol=fSortColumn) and (arow=0) and (fixedRows>0) and ((acol+1>fixedcols) or (fSortFixedCols)) then
     with rect do
      begin
       Rect:=ARect;
       case alignvalue of
       taLeftJustify:left:=left+canvas.Textwidth(su)+10;
       taRightJustify:left:=right-canvas.TextWidth(su)-14;
       taCenter:left:=left+8;
       end;

       case vAlignment of
       vtaTop:vpos:=top+8;
       vtaCenter:vpos:=top+((bottom-top) shr 1);
       vtaBottom:vpos:=bottom-8;
       end;

       DrawSortIndicator(canvas,left,vpos);

      end;
  end;

begin
  if multilinecells then wordwraps[false]:=0;
  graphicwidth:=0;
  graphicheight:=0;
  displtext:=true;
  maxtextwidth:=0;
  maxtextheight:=0;
  hal:=haLeft;
  val:=vaTop;

  {recalculate real col based on hidden cols}

  acol:=remapcol(acol);

  if (acol<fixedcols) or (arow<fixedrows) then
   canvas.font.assign(fFixedFont) else canvas.font.assign(self.Font);

  if fEnableGraphics then
  begin

  {$IFNDEF WIN32}
  tmp:=objects[acol,arow] as tcellgraphic;
  if assigned(tmp) then
  {$ELSE}
  if assigned(objects[acol,arow]) then
  {$ENDIF}
   if (objects[acol,arow] is tcellgraphic) then
     begin
      if (objects[acol,arow] as tcellgraphic).celltype=ctBitmap then
        begin
         graphicwidth:=(objects[acol,arow] as tcellgraphic).cellbitmap.width;
         graphicheight:=(objects[acol,arow] as tcellgraphic).cellbitmap.height;
        end;

      if (objects[acol,arow] as tcellgraphic).celltype=ctPicture then
        begin
         graphicwidth:=tpicture((objects[acol,arow] as tcellgraphic).cellbitmap).bitmap.width;
         graphicheight:=tpicture((objects[acol,arow] as tcellgraphic).cellbitmap).bitmap.height;
        end;

      if (objects[acol,arow] as tcellgraphic).celltype=ctCheckBox then
        begin
         graphicwidth:=16;
         graphicheight:=0;
        end;

      if (objects[acol,arow] as tcellgraphic).celltype=ctDataCheckBox then
        begin
         graphicwidth:=16;
         graphicheight:=0;
         displtext:=false;
        end;

      if (objects[acol,arow] as tcellgraphic).celltype=ctIcon then
        begin
         graphicwidth:=(objects[acol,arow] as tcellgraphic).cellicon.width;
         graphicheight:=(objects[acol,arow] as tcellgraphic).cellicon.height;
        end;

      {$IFDEF WIN32}
      if ((objects[acol,arow] as tcellgraphic).celltype in [ctImageList,ctDataImage]) then
        begin
         graphicwidth:=FGridImages.width;
         graphicheight:=FGridImages.height;
        end;

      if ((objects[acol,arow] as tcellgraphic).celltype in [ctImages]) then
        begin
         if (objects[acol,arow] as tcellgraphic).cellBoolean then
          begin
           graphicwidth:=FGridImages.width * CellImages[acol,arow].Count;
           graphicheight:=FGridImages.height;
          end
         else
          begin
           graphicheight:=FGridImages.height * CellImages[acol,arow].Count;
           graphicwidth:=FGridImages.width;
          end;
        end;

      if (objects[acol,arow] as tcellgraphic).celltype=ctDataImage then
        begin
         displtext:=false;
        end;

      if (objects[acol,arow] as tcellgraphic).celltype=ctRotated then
       begin
        displtext:=false;
       end;

      if (objects[acol,arow] as tcellgraphic).celltype=ctRadio then
       begin
        displtext:=false;
       end;

      {$ENDIF}

      hal:=(objects[acol,arow] as tcellgraphic).cellhalign;
      val:=(objects[acol,arow] as tcellgraphic).cellvalign;
     end;
  end;

  {
  if (fFixedFooters>0) and (rowcount-arow<=fFixedFooters) then
    begin
      canvas.brush.color:=self.Fixedcolor;
      astate:=[gdFixed];
    end;
  if (fFixedRightCols>0) and (colcount-acol+fNumhidden<=fFixedRightCols) or (isfixed(acol,arow)) then
    begin
     canvas.brush.color:=self.Fixedcolor;
     astate:=[gdFixed];
    end;
   }

   if IsFixed(remapcolinv(acol),arow) then
    begin
     canvas.brush.color:=self.Fixedcolor;
     astate:=[gdFixed];
    end;

  {prepare color and font selection}
  if Assigned(FOnGetCellColor) then
    FOnGetCellColor(Self, ARow, ACol, AState, Canvas.Brush, Canvas.Font);

  if (gdSelected in Astate) and not (gdFocused in Astate) then
   begin
    if fSelectionColor=clNone then Canvas.brush.Color:=clHighLight
   end;

  canvas.font.size:=canvas.font.size+zoomfactor;

  {text draw with alignment}

  if DefaultDrawing then DrawCellText
  else inherited DrawCell(ACol,ARow,ARect,AState);

  DrawBorders(acol,arow,arect);

  if IsFixed(remapcolinv(acol),arow) then
   begin
    FrameFlags1:=0;
    FrameFlags2:=0;
    {$IFDEF WIN32}
    if goFixedVertLine in Options then
    begin
      FrameFlags1 := BF_RIGHT;
      FrameFlags2 := BF_LEFT;
    end;
    if goFixedHorzLine in Options then
    begin
      FrameFlags1 := FrameFlags1 or BF_BOTTOM;
      FrameFlags2 := FrameFlags2 or BF_TOP;
    end;
    if Ctl3D then
     begin
      DrawEdge(Canvas.Handle, aRect, BDR_RAISEDINNER, FrameFlags1);
      DrawEdge(Canvas.Handle, aRect, BDR_RAISEDINNER, FrameFlags2);
     end
    else
     begin
      canvas.pen.color:=clBlack;
      canvas.pen.width:=1;
      if goFixedHorzLine in Options then
       begin
        canvas.moveto(arect.left-1,arect.top-1);
        canvas.lineto(arect.right+1,arect.top-1);
        canvas.moveto(arect.left-1,arect.bottom+1);
        canvas.lineto(arect.right+1,arect.bottom+1);
       end;
      if goFixedVertLine in Options then
       begin
        canvas.moveto(arect.left-1,arect.top-1);
        canvas.lineto(arect.left-1,arect.bottom+1);
        canvas.moveto(arect.right+1,arect.top-1);
        canvas.lineto(arect.right+1,arect.bottom+1);
       end;
     end;
    {$ENDIF}
   end;

  if (fFixedRightCols>0) and (colcount-acol+fnumhidden<=fFixedRightCols) then
   begin
    FrameFlags1:=0;
    FrameFlags2:=0;
    {$IFDEF WIN32}
    if goFixedVertLine in Options then
    begin
      FrameFlags1 := BF_RIGHT;
      FrameFlags2 := BF_LEFT;
    end;
    if goFixedHorzLine in Options then
    begin
      FrameFlags1 := FrameFlags1 or BF_BOTTOM;
      FrameFlags2 := FrameFlags2 or BF_TOP;
    end;
    if Ctl3D then
     begin
      DrawEdge(Canvas.Handle, aRect, BDR_RAISEDINNER, FrameFlags1);
      DrawEdge(Canvas.Handle, aRect, BDR_RAISEDINNER, FrameFlags2);
     end
    else
     begin
      canvas.pen.color:=clBlack;
      canvas.pen.width:=1;

      if goFixedHorzLine in Options then
       begin
        canvas.moveto(arect.left-1,arect.top-1);
        canvas.lineto(arect.right+1,arect.top-1);
        canvas.moveto(arect.left-1,arect.bottom+1);
        canvas.lineto(arect.right+1,arect.bottom+1);
       end;
      if goFixedVertLine in Options then
       begin
        canvas.moveto(arect.left-1,arect.top-1);
        canvas.lineto(arect.left-1,arect.bottom+1);
        canvas.moveto(arect.right+1,arect.top-1);
        canvas.lineto(arect.right+1,arect.bottom+1);
       end;
     end;
    {$ENDIF}
   end;

  if fEnableGraphics then
  begin
   {hide out cell lines in node column v1.82}
   if (fNumNodes>0) and (acol=0) and (arow>=fixedrows) then
    begin
     if (fixedcols>0) and (acol=0) then
      begin
       canvas.pen.color:=FixedColor;
//       canvas.brush.color:=FixedColor;
      end
     else
      canvas.pen.color:=self.color;

     if (arow=toprow) then
      canvas.rectangle(arect.left,arect.top,arect.right,arect.Bottom+1)
     else
      canvas.rectangle(arect.left,arect.top-1,arect.right,arect.Bottom+1);
     {
     canvas.MoveTo(arect.left,arect.bottom+1);
     canvas.Lineto(arect.right,arect.bottom+1);
     if (arow<>toprow) then
      begin
       canvas.MoveTo(arect.left,arect.top-1);
       canvas.Lineto(arect.right,arect.top-1);
      end;

     canvas.moveto(arect.left-1,arect.top);
     canvas.lineto(arect.left-1,arect.bottom);
     canvas.moveto(arect.right+1,arect.top);
     canvas.lineto(arect.right+1,arect.bottom);
     }
    end;


  {$IFNDEF WIN32}
  tmp:=objects[acol,arow] as tcellgraphic;
  if assigned(tmp) then
  {$ELSE}
  if assigned(objects[acol,arow]) then
  {$ENDIF}
   begin
    if objects[acol,arow] is tcellgraphic then
     drawcellgraphic(arect,(objects[acol,arow] as tcellgraphic));
   end;
  end;


  {$IFDEF WIN32}
  if assigned(OnDrawCell) then OnDrawCell(self,ACol,ARow,ARect,AState);
  {$ELSE}
  tmpproc:=OnDrawCell;
  if assigned(tmpproc) then OnDrawCell(self,ACol,ARow,ARect,AState);
  {$ENDIF}
                            {
  inherited Drawcell(ACol,ARow,ARect,AState);
                            }
end;


function TAdvStringGrid.Search(s:string):longint;
var
 i:longint;
 c:string;
 res,scol:longint;

begin
 Search:=-1;

 if (rowcount<2) then exit;

 res:=-1;

 if sortshow then scol:=sortcolumn else scol:=1;

 for i:=self.fixedrows to self.rowcount-1 do
  begin
   c:=cells[scol,i];
   c:=upstr(copy(c,1,length(s)));
   if (s=c) then
     begin
      res:=i;
      break;
     end;
  end;

 Search:=res;
end;

function TAdvStringGrid.MatchCell(col,row:integer):boolean;
var
 res1,res2:boolean;
 ct:string;
begin
 res2:=true;

 col:=remapcol(col);
 ct:=cells[col,row];

 if fnMatchCase in fFindParams then
    res1:=(pos(searchcache,ct)>0)
 else
    res1:=(pos(uppercase(searchcache),uppercase(ct))>0);

 if fnMatchFull in fFindParams then
    res2:=(uppercase(searchcache)=uppercase(ct));

 if (fnMatchregular in fFindParams) then
  begin
   if (fnMatchCase in fFindParams) then
    MatchCell:=MatchStr(searchcache,ct)
   else
    MatchCell:=MatchStr(uppercase(searchcache),uppercase(ct))
  end
 else
  MatchCell:=res1 and res2;
end;

function TAdvStringGrid.FindFirst(s:string;findparams:tfindparams):tpoint;
var
 res:tpoint;
 i,j:integer;
 maxcol,maxrow:integer;

begin
 res.x:=-1;
 res.y:=-1;
 if (fnIncludeFixed in findparams) then
  begin
   searchcell.x:=0;
   searchcell.y:=0;
  end
 else
  begin
   searchcell.x:=fixedcols;
   searchcell.y:=fixedrows;


  end;
 searchcache:=s;
 ffindparams:=findparams;
 fFindBusy:=true;
 FindFirst:=res;

 if (colcount=fixedcols) or (colcount=0) then exit;
 if (rowcount=fixedrows) or (rowcount=0) then exit;

 maxcol:=colcount;
 maxrow:=rowcount;

 i:=searchcell.x;
 j:=searchcell.y;


 if (fnFindInCurrentRow in ffindparams) then j:=row;
 if (fnFindInCurrentCol in ffindparams) then i:=col;

 if (fnFindInCurrentRow in ffindparams) then maxrow:=row+1;
 if (fnFindInCurrentCol in ffindparams) then maxcol:=col+1;

 if (fnDirectionLeftRight in ffindparams) then
  begin
   while (j<maxrow) do
    begin
      while (i<maxcol) do
       begin
        if MatchCell(i,j) then
          begin
           searchcell.x:=i;
           searchcell.y:=j;
           FindFirst:=searchcell;
           if (fnAutoGoto in ffindparams) then
            begin
             row:=j;
             col:=i;
            end;
           exit;
          end;
        inc(i);
       end;

     if (fnFindInCurrentCol in ffindparams) then
        i:=col
     else
        i:=fixedcols;

     inc(j);
    end;
  end
 else
  begin
   while (i<maxcol) do
    begin
      while (j<maxrow) do
       begin
        if MatchCell(i,j) then
          begin
           searchcell.x:=i;
           searchcell.y:=j;
           FindFirst:=searchcell;
           if (fnAutoGoto in ffindparams) then
            begin
             row:=j;
             col:=i;
            end;

           exit;
          end;
        inc(j);
       end;

     if (fnFindInCurrentRow in ffindparams) then
       j:=row
     else
       j:=fixedrows;

     inc(i);
    end;
  end;

 fFindBusy:=false;
end;

function TAdvStringGrid.FindNext:tpoint;
var
 res:tpoint;
 i,j:integer;
 maxcol,maxrow:integer;

begin
 res.x:=-1;
 res.y:=-1;

 if (colcount=fixedcols) or (colcount=0) then
   begin
    fFindBusy:=false;
    exit;
   end;
 if (rowcount=fixedrows) or (rowcount=0) then
   begin
    fFindBusy:=false;
    exit;
   end;

 maxcol:=colcount;
 maxrow:=rowcount;

 //if (fnFindInCurrentRow in ffindparams) then j:=row;
 //if (fnFindInCurrentCol in ffindparams) then i:=col;

 if (fnFindInCurrentRow in ffindparams) then maxrow:=row+1;
 if (fnFindInCurrentCol in ffindparams) then maxcol:=col+1;

 if (fnDirectionLeftRight in ffindparams) then
  begin
   i:=searchcell.x+1;
   j:=searchcell.y;
   if j<0 then j:=0;

   while (j<maxrow) do
    begin
      while (i<maxcol) do
       begin
        if MatchCell(i,j) then
          begin
           searchcell.x:=i;
           searchcell.y:=j;
           FindNext:=searchcell;
           if (fnAutoGoto in ffindparams) then
            begin
             row:=j;
             col:=i;
            end;

           exit;
          end;
        inc(i);
       end;
     if (fnFindInCurrentCol in ffindparams) then
       i:=col
     else
       i:=fixedcols;
     inc(j);
    end;
  end
 else
  begin
   i:=searchcell.x;
   j:=searchcell.y+1;
   if i<0 then i:=0;

   while (i<maxcol) do
    begin
      while (j<maxrow) do
       begin
        if MatchCell(i,j) then
          begin
           searchcell.x:=i;
           searchcell.y:=j;
           FindNext:=searchcell;
           if (fnAutoGoto in ffindparams) then
            begin
             row:=j;
             col:=i;
            end;

           exit;
          end;
        inc(j);
       end;

      if (fnFindInCurrentRow in ffindparams) then
        j:=row
      else
        j:=fixedrows;

      inc(i);
    end;
  end;

 fFindBusy:=false;
 searchcell.x:=-1;
 searchcell.y:=-1;
 res.x:=-1;
 res.y:=-1;
 FindNext:=res;
end;

procedure TAdvStringGrid.Click;
begin
 try
  inherited Click;
 finally
  if fEntered then ValidateCell(cells[col,row]);
 end;
 InitValidate(col,row);
end;

procedure TAdvStringGrid.InitValidate(acol,arow:integer);
begin
 fOldCol:=aCol;
 fOldRow:=aRow;
 fOldCellText:=Cells[remapcol(fOldCol), fOldRow];
 fOldModifiedValue:=fModified;
end;

procedure TAdvStringGrid.UpdateCell(ACol,ARow:integer);
begin

end;

function TAdvStringGrid.ValidateCell(const s:string):boolean;
var
 Value: String;
 Valid: Boolean;
 ROldCol: integer;
begin
 valid:=true;
 ROldCol:=remapcol(fOldCol);

 if (fOldCellText<>Cells[ROldCol,fOldRow]) then
 begin
  UpdateCell(ROldCol,fOldRow);

  Value:=Cells[ROldCol,fOldRow];
  Valid:=True;
  if Assigned(fOnCellValidate) then
     fOnCellValidate(Self,ROldCol,fOldRow,Value,Valid);
  {Since Value is also a VAR parameter, we always
   use it if it was changed in OnCellValidate.  }
  if not Valid then
  begin
    if (Value<>Cells[ROldCol,fOldRow]) then
      begin
       Cells[ROldCol,fOldRow]:=Value;
      end
    else
      begin
       Cells[ROldCol,fOldRow]:=fOldCellText;
       row:=fOldRow;
       col:=fOldCol;
      end;

    fModified:=fOldModifiedValue;
  end
  else if (Value<>Cells[ROldCol,fOldRow]) then
     begin
       Cells[ROldCol,fOldRow]:=Value;
     end;
 end;

 InitValidate(remapcol(col),row);
 result:=valid;
end;

procedure TAdvStringGrid.WMSetFocus(var Msg: TWMSetFocus);
begin
 inherited;

 if HasCheckBox(col,row) then exit;

 if assigned(normaledit) then
  begin
   if (msg.FocusedWnd <> normaledit.handle) and
      fNavigation.AlwaysEdit then
        begin
         ShowInplaceEdit;
         msg.result:=0;
         exit;
        end;
  end else
  if fNavigation.AlwaysEdit then ShowInplaceEdit;
end;

procedure TAdvStringGrid.DoEnter;
begin
 if fBlockFocus then exit;
 try
  inherited DoEnter;
  fEntered:=True;
  SelectCell(col,row);
  {$IFDEF WIN32}

  {$ENDIF}
 finally
  InitValidate(col,row);
 end;
end;

procedure TAdvStringGrid.DoExit;
begin
 if fBlockFocus then exit;
 if fEntered then ValidateCell(cells[col,row]);
 inherited DoExit;
end;

Procedure TAdvStringGrid.CMHintShow(Var Msg: TMessage);
{$IFDEF DELPHI2_LVL}
type
 PHintInfo = ^THintInfo;
{$ENDIF}
var
 CanShow: Boolean;
 hi: PHintInfo;
{$IFDEF DELPHI2_LVL}
 s:string;
{$ENDIF}

Begin
 CanShow := True;
 hi := PHintInfo(Msg.LParam);
 {$IFDEF DELPHI2_LVL}
 s:=self.hint;
 ShowHintProc(s, CanShow, hi^);

 self.hint:=s;
 {$ELSE}
 ShowHintProc(hi.HintStr, CanShow, hi^);
 {$ENDIF}
 Msg.Result := Ord(Not CanShow);
end;

procedure TAdvStringGrid.CMCursorChanged(var Message: TMessage);
begin
 inherited;
 if not invokedchange then foldcursor:=self.cursor;
 invokedchange:=false;
end;

procedure TAdvStringGrid.BeginUpdate;
begin
  inc(FUpdateCount);
end;

procedure TAdvStringGrid.EndUpdate;
begin
  if FUpdateCount > 0 then dec(FUpdateCount);
  if FUpdateCount = 0 then
    invalidaterect(self.handle, nil, false);
end;

procedure TAdvStringGrid.ResetUpdate;
begin
 fUpdateCount:=0;
end;

Function TAdvStringGrid.GetLockFlag : boolean;
begin
  Result:=FUpdateCount<>0;
end;

procedure TAdvStringGrid.SetLockFlag(AValue : boolean);
begin
  if AValue then
    BeginUpdate
  else
    EndUpdate;
end;


procedure TAdvStringGrid.WMTimer(var Msg: TWMTimer);
var
 lp:tpoint;
 acol,arow:longint;
 r:trect;
begin
 if (msg.TimerID = fGridTimerID) then
  begin
   r:=getclientrect;
   getcursorpos(lp);
   lp:=screentoclient(lp);
   if ptinrect(r,lp) then
    begin
     self.mousetocell(lp.x,lp.y,acol,arow);
     if (acol>=fixedcols) and (arow>=fixedrows) then
      begin
       self.Row:=arow;
       self.Col:=acol;
      end;
     end;
  end
 else inherited;
end;

procedure TAdvStringGrid.WMPaint(var Msg: TWMPaint);
begin
 if (FUpdateCount>0) then msg.result:=0 else inherited;
end;

procedure TAdvStringGrid.WMEraseBkGnd(var Message: TMessage);
begin
 if (FUpdateCount>0) then message.result:=0 else inherited;
end;


procedure TAdvStringGrid.WMSize(var Msg: TWMSize);
begin
 inherited;
 if FColumnSize.fStretch then StretchRightColumn;
 {$IFDEF WIN32}
 UpdateVScrollBar;
 UpdateHScrollBar;
 FlatShowScrollBar(SB_HORZ,visiblecolcount+fixedcols<colcount);
 FlatShowScrollBar(SB_VERT,visiblerowcount+fixedrows<rowcount);
 {$ENDIF}

 {$IFDEF HEADER}
 fheader.left:=-1;
 fheader.width:=self.width;
 fheader.top:=self.height-getsystemmetrics(SM_CYHSCROLL)-defaultrowheight;
 {$ENDIF}
end;

procedure TAdvStringGrid.WMKeydown(var Msg:TWMKeydown);
var
 canedit,chk:boolean;
const
 vk_c = $43;
 vk_v = $56;
 vk_x = $58;

begin
 fmoveselection:=selection;
 inherited;

 if ((fmoveselection.top<>selection.top) or
    (fmoveselection.right<>selection.right) or
    (fmoveselection.bottom<>selection.bottom) or
    (fmoveselection.left<>selection.left)) and (fSelectionRectangle) then
   begin {old selection and new selection ?}
    invalidategridrect(selection);
    fmoveselection:=selection;
   end;

 if (msg.charcode=vk_space) then
   begin
    canedit:=(goEditing in self.Options);
    if Assigned(FOnCanEditCell) and HasStaticEdit(col,row) then
     begin
      canedit:=true;
      FOnCanEditCell(self,row,col,canedit);
     end;
    if canedit then
     begin
      ToggleCheck(col,row,true);
      GetCheckBoxState(col,row,chk);
      {$IFDEF EDITCONTROLS}
      if assigned(FOnCheckBoxClick) then FOnCheckBoxClick(self,col,row,chk);
      {$ENDIF}
     end;
   end;

 if (msg.charcode=vk_x) and
    FNavigation.AllowClipboardShortcuts and
    (getkeystate(vk_control) and $8000=$8000) then
   begin
    cuttoclipboard;
    exit;
   end;

 if (msg.charcode=vk_v) and
    FNavigation.AllowClipboardShortcuts and
    (getkeystate(vk_control) and $8000=$8000) then
   begin
    pasteselectionfromclipboard;
    exit;
   end;

 if (msg.charcode=vk_c) and
    FNavigation.AllowClipboardShortcuts and
    (getkeystate(vk_control) and $8000=$8000) then
   begin
    copyselectiontoclipboard;
    exit;
   end;

 if (msg.charcode=vk_insert) and
    FNavigation.AllowClipboardShortcuts and
    (getkeystate(vk_control) and $8000=$8000) then
  begin
   copyselectiontoclipboard;
   exit;
  end;

 if (msg.charcode=vk_insert) and
    FNavigation.AllowClipboardShortcuts and
    (getkeystate(vk_shift) and $8000=$8000) then
  begin
   pasteselectionfromclipboard;
   exit;
  end;

 if (msg.charcode=vk_delete) and
    FNavigation.AllowClipboardShortcuts and
    (getkeystate(vk_shift) and $8000=$8000) then
  begin
   cuttoclipboard;
   exit;
  end;

 if (msg.charcode=vk_insert)
    and FNavigation.AllowInsertRow
    and (getkeystate(vk_control) and $8000=0)
    and (getkeystate(vk_shift) and $8000=0) then
  begin
   if FNavigation.InsertPosition=pInsertAfter then
     begin
      insertrows(row+1,1);
      if Assigned(FOnAutoInsertRow) then FOnAutoInsertRow(self,row+1);
      row:=row+1;
     end
   else
     begin
      insertrows(row,1);
      if Assigned(FOnAutoInsertRow) then FOnAutoInsertRow(self,row);
     end;
  end;
 if (msg.charcode=vk_delete) and FNavigation.AllowDeleteRow then
  begin
   if (rowcount-fixedfooters-fixedrows>=1) then
    begin
     if (rowcount-fixedrows=1) then
      clearrows(row,1) else removerows(row,1);

     if (row>=rowcount-fixedfooters) then
       begin
        row:=row-1;
        if Assigned(FOnAutoDeleteRow) then FOnAutoDeleteRow(self,row+1);
       end
     else
      if Assigned(FOnAutoDeleteRow) then FOnAutoDeleteRow(self,row);
    end;

  end;

 {
 if goEditing in Options then exit;
 if not (FNavigation.AutoGotoWhenSorted) then exit;
 s:=upcase(chr(msg.charcode));
 p:=self.search(s);
 if (p<>-1) then
  begin
   self.row:=p;
  end;
 }
end;

procedure TAdvStringGrid.WMRButtonDown(var Msg: TWMRButtonDown);
var
 x,y:longint;

begin
 inherited;
 self.mousetocell(msg.xpos,msg.ypos,x,y);
 if Assigned(FOnRightClickCell) then FOnRightClickCell(self,y,x);
 {$IFDEF WIN32}
 if URLShow and URLEdit then
  begin
   if (pos('://',cells[remapcol(x),y])>0) or (pos('mailto:',cells[remapcol(x),y])>0) then
    begin
     self.col:=x;
     self.row:=y;
     self.showeditor;
    end;
  end;
 {$ENDIF}
end;

procedure TAdvStringGrid.WMLButtonDblClk(var Message: TWMLButtonDblClk);
var
 x,y:longint;
 r:trect;
begin
 self.mousetocell(message.xpos,message.ypos,x,y);

 if (y=0) and (goColSizing in Options) then
  begin
    r:=cellrect(x,y);
    if abs(message.xpos-r.left)<4 then
      begin
       fdblclk:=true;
       if (x-1>=FixedCols) then
         begin
          AutoSizeCol(x-1);
          exit;
         end;
      end;
    if abs(message.xpos-r.right)<4 then
      begin
       if (x>=FixedCols) then
         begin
          fdblclk:=true;
          AutoSizeCol(x);
          exit;
         end;
      end;
  end;

 inherited;
 if Assigned(FOnDblClickCell) then FOnDblClickCell(self,y,x);
 {force url cells to editmode ?}
end;

procedure TAdvStringGrid.ColumnMoved(FromIndex, ToIndex: Longint);
var
 cw:integer;
begin
 inherited ColumnMoved(FromIndex,ToIndex);

 if fEnhRowColMove then
  begin
   cw:=colwidths[FromIndex];
   colwidths[FromIndex]:=colwidths[ToIndex];
   colwidths[ToIndex]:=cw;
   colmoveflg:=true;
  end;

 if (fromindex=fsortcolumn) then fsortcolumn:=toindex;
end;

procedure TAdvStringGrid.RowMoved(FromIndex, ToIndex: Longint);
var
 rh:integer;
begin
 inherited RowMoved(FromIndex,ToIndex);
 if fEnhRowColMove then
  begin
   rh:=rowheights[FromIndex];
   rowheights[FromIndex]:=rowheights[ToIndex];
   rowheights[ToIndex]:=rh;
   colmoveflg:=true;
  end;
end;

procedure TAdvStringGrid.WMLButtonUp(var Msg:TWMLButtonUp);
var
 x,y:longint;
 cx,cy:longint;
 doit:boolean;
 r:trect;

begin
 colmoveflg:=false;
 colsizeflg:=false;
 fMouseDown:=false;

 {$IFDEF DELPHI3_LVL}
 if not (csDesigning in ComponentState) then
  begin
   arwU.visible:=false;
   arwD.visible:=false;
   arwL.visible:=false;
   arwR.visible:=false;
  end;
 {$ENDIF}

 if (csDesigning in ComponentState) and (FScrollHintShow) then
   begin
     FScrollHintWnd.ReleaseHandle;
     FScrollHintShow:=false;
   end;

 self.mousetocell(msg.xpos,msg.ypos,x,y);

 if (fMouseActions.DisjunctRowSelect) and
    (y>=fixedrows) then
    begin
     SelectToRowSelect(y);
     if FDeselectState then RowSelect[y]:=false;
     FDeselectState:=false;
    end;

 if fEnhRowColMove and ((x=0) or (y=0)) then LockWindowUpdate(self.handle);

 {$IFDEF WIN32}

 if (screen.cursor=crDrag) and
    (fgridstate in [gsColMoving,gsRowMoving]) and (fEnhRowColMove) then
   begin
    screen.cursor:=crDefault;
    movebutton.enabled:=false;
    movebutton.Visible:=false;

    if (FGridState=gsColMoving) and (movecell>=0) and (x>=fixedcols) then
     MoveColumn(Movecell,x);

    if (FGridState=gsRowMoving) and (movecell>=0) and (y>=fixedrows) then
     MoveRow(Movecell,y);

    if (fGridState in [gsRowMoving,gsColMoving]) then KillTimer(handle,1);

    fgridState:=gsNormal;
   end;
 {$ENDIF}

 inherited;


 if fEnhRowColMove and ((x=0) or (y=0)) then LockWindowUpdate(0);


 if colmoveflg or colsizeflg or
    ((x < self.fixedcols) and not fSortFixedCols) then exit;

 x:=remapcol(x);

 {handle here if it is a sortable column}
 doit:=true;


 if fdblclk then
  begin
   fdblclk:=false;
   exit;
  end;

 if (y=0) and (goColSizing in Options) then
  begin
    r:=cellrect(x,y);
    if (abs(msg.xpos-r.left)<4)
    or (abs(msg.xpos-r.right)<4) then exit;
  end;

 if (y=0) and
    (self.fixedrows>0) and
    (sortshow) and
    (self.rowcount>2) then
   begin
    if (Assigned(FOnCanSort)) then FOnCanSort(self,x,doit);
   end;

 mousetocell(clickposx,clickposy,cx,cy);

 if (y=0) and (cy=0) and
    (self.fixedrows>0) and
    (sortshow) and
    (self.rowcount>2) and
    (doit) then
  begin
    if (x=sortcolumn) then
      begin
       if sortdirection=sdAscending then sortdirection:=sdDescending else
                                         sortdirection:=sdAscending;
      end
    else
      begin
        sortdirection:=sdAscending;
        sortcolumn:=x;
      end;
    sorttime:=gettickcount;
    if fNumNodes>0 then self.qsortgroup else self.qsort;
    sorttime:=gettickcount-sorttime;
    if Assigned(FOnClickSort) then FOnClickSort(self,sortcolumn);
  end;
end;

procedure TAdvStringGrid.HandleRadioClick(Acol,Arow,Xpos,Ypos:integer);
var
 cg:tCellGraphic;
 ofs1,ofs2,i:integer;
 s:string;
 sl:tstrings;

begin
 if assigned(OnGetCellColor) then
  OnGetCellColor(self,Acol,Arow,[],canvas.brush,canvas.font);

 cg:=GetCellGraphic(Acol,Arow);

 col:=acol;
 row:=arow;

 ofs1:=0;
 ofs2:=0;

 sl:=tstringlist(cg.CellBitmap);

 if cg.CellBoolean and assigned(sl) then
  begin
   for i:=1 to sl.Count do
    begin
     s:=sl.Strings[i-1];

     ofs2:=ofs2 + canvas.TextWidth(s) + 12;

     if (xpos<ofs2) and (xpos>ofs1) then
       begin
        if cg.CellIndex=-1 then
         cells[acol,arow]:=s
        else
         cg.CellIndex:=i-1;

        cells[acol,arow]:=cells[acol,Arow];
        if assigned(fOnRadioClick) then
         fOnRadioClick(self,aCol,aRow,i-1);
        break;
       end;
     ofs1:=ofs2;
    end;
  end
 else
  begin
   for i:=1 to sl.Count do
    begin
     ofs2:=ofs2 + 12;
     s:=sl.Strings[i-1];
     
     if (ypos<ofs2) and (ypos>ofs1) then
       begin
        if cg.CellIndex=-1 then
         cells[acol,arow]:=s
        else
         cg.CellIndex:=i-1;
        cells[acol,arow]:=cells[acol,Arow];
        if assigned(fOnRadioClick) then
         fOnRadioClick(self,aCol,aRow,i-1);
        break;
       end;
     ofs1:=ofs2;
    end;
  end;
end;

{------------------------------------------
 v 0.99 : added event handle to ask appl.
          if column can be sorted on click
-------------------------------------------}
procedure TAdvStringGrid.WMLButtonDown(var Msg:TWMLButtonDown);
var
 x,y:longint;
 r:trect;
 s,anchor,stripped:string;
 canedit,chk:boolean;
 {$IFDEF DELPHI3_4_ONLY}
 dwEffects:longint;
 {$ENDIF}
 xsize,ysize:integer;
 handle:boolean;

begin
 searchinc:='';
 fmovecolind:=-1;
 fmoverowind:=-1;

 self.mousetocell(msg.xpos,msg.ypos,x,y);

 {$IFDEF WIN32}
 if fEnhRowColMove and ((x<FixedCols) or (y<FixedRows)) then LockWindowUpdate(self.handle);
 if FMouseActions.CaretPositioning then lbutflg:=true;
 {$ENDIF}

 if (y=0) then
  self.mousetocell(msg.xpos-4,msg.ypos,colclicked,rowclicked)
 else if (x=0) then
  self.mousetocell(msg.xpos,msg.ypos-4,colclicked,rowclicked)
 else
  self.mousetocell(msg.xpos,msg.ypos,colclicked,rowclicked);

 if not (csDesigning in ComponentState) then
  begin
   if IsFixed(x,y) then exit;

   if (y>=FixedRows) and (x=0) then
    if IsNode(y) then
      begin
       row:=y;
       if not (objects[0,y] as TCellGraphic).CellBoolean then
        begin
         ContractNode(remaprowinv(y));
         if assigned(fOnContractNode) then
          fOnContractNode(self,y,remaprowinv(y));
        end
       else
        begin
         ExpandNode(remaprowinv(y));
         if assigned(fOnExpandNode) then
          fOnExpandNode(self,y,remaprowinv(y));
        end;
       exit;
      end;

   if (y>=FixedRows) and (x>=FixedCols) then
    if IsRadio(x,y) then
     begin
      r:=cellrect(x,y);
      HandleRadioClick(x,y,msg.xpos-r.left,msg.ypos-r.top);
      exit;
     end;

   if (x=0) and (fNumNodes>0) then exit;

  {$IFDEF DELPHI3_4_ONLY}
   r:=cellrect(x,y);
    if ((abs(r.left-msg.xpos)<2) or
       (abs(r.right-msg.xpos)<2) or
       (abs(r.top-msg.ypos)<2) or
       (abs(r.bottom-msg.ypos)<2)) and
       IsSelected(x,y) and
       FOleDropSource then
        begin
         StartTextDoDragDrop(SelectedText,cells[x,y],DROPEFFECT_COPY or DROPEFFECT_MOVE,dwEffects);
         exit;
        end;
  {$ENDIF}
  end;

 if (x>=colcount) or (y>=rowcount) or (x<0) or (y<0) then
    begin
     inherited;
     exit;
    end;

 clickposx:=msg.xpos;
 clickposy:=msg.ypos;

 r:=cellrect(colclicked,rowclicked);
 {$IFDEF DEBUG}
 outputdebugstr('click : '+inttostr(colclicked)+':'+inttostr(rowclicked));
 {$ENDIF}

 clickposdx:=-r.right+clickposx;
 clickposdy:=-r.bottom+clickposy;

 {if ctrl-pressed / shift pressed }

 if (fMouseActions.DisjunctRowSelect) and (y>=fixedrows) then
  begin
   if (getkeystate(vk_Control) and $8000=$8000) then
    begin
     if RowSelect[y] then FDeselectState:=true;
    end
   else
    begin
     fMouseDown:=true;
     ClearRowSelect;
    end;
 end;

 {$IFDEF WIN32}
 if URLShow then
  begin
   anchor:='';
   s:=cells[remapcol(x),y];
   if (pos('://',s)>0) or (pos('mailto:',s)>0) and (pos('</',s)=0) then
     anchor:='s'
   else
   if (pos('</',s)<>0) then
    begin
     r.left:=r.left+2;
     r.top:=r.top+2;
     HTMLDraw(canvas,s,r,gridimages,
              msg.xpos,msg.ypos,true,false,false,0.0,fURLColor,anchor,stripped,xsize,ysize);
    end;
   if (anchor<>'') then
    begin
     handle:=true;
     if assigned(fOnAnchorClick) then
      begin
        fOnAnchorClick(self,x,y,anchor,handle);
      end;
     if handle then
        ShellExecute(Application.Handle,'open',pchar(anchor), nil, nil, SW_NORMAL);
     exit;
    end;
  end;

 movecell:=-1;

 if (x<FixedCols) and (y<FixedRows) and
    (FMouseActions.AllSelect) and (goRangeSelect in Options) and
    (self.cursor=crAsgCross) then
  begin
   hideselection;
   selection:=tgridrect(rect(fixedcols,fixedrows,colcount-1,rowcount-1));
   self.doexit;   {force validatecell call}
  end;

 if (x<FixedCols) and (y>=FixedRows) and
    (FMouseActions.RowSelect) and (goRangeSelect in Options) and
    (self.cursor=crHorzArr) then
  begin
   hideselection;
   selection:=tgridrect(rect(fixedcols,y,colcount-1,y));
   self.doexit;   {force validatecell call}
  end;

 if (x>=FixedCols) and (y<FixedRows) and
    (FMouseActions.ColSelect) and (goRangeSelect in Options) and
    (self.cursor=crVertArr) then
  begin
   hideselection;
   selection:=tgridrect(rect(x,fixedrows,x,rowcount-1));
   self.doexit;   {force validatecell call}
  end;
 {$ENDIF}

 if (x>=FixedCols) and (y<FixedRows) then movecell:=x;
 if (x<FixedCols) and (y>=FixedRows) then movecell:=y;

 r:=self.cellrect(x,y);

 moveofsx:=msg.xpos-r.left;
 moveofsy:=msg.ypos-r.top;

 canedit:=(goEditing in self.Options);
 if Assigned(FOnCanEditCell) {and HasCheckBox(x,y)} then
  begin
   canedit:=true;
   FOnCanEditCell(self,y,x,canedit);
  end;

 if (x<colcount) and (y<rowcount) and
    (x>=FixedCols) and (y>=FixedRows) and
    (canedit) then
  begin
   togglecheck(remapcol(x),y,true);
   GetCheckBoxState(remapcol(x),y,chk);
   {$IFDEF EDITCONTROLS}
   if assigned(FOnCheckBoxClick) then FOnCheckBoxClick(self,x,y,chk);
   {$ENDIF}
  end;

 inherited;

 if fEnhRowColMove and ((x=0) or (y=0)) then LockWindowUpdate(0);

 if Assigned(FOnClickCell) then FOnClickCell(self,y,x);

 r:=self.cellrect(x,y);

 if (msg.xpos-r.left)<4 then exit;
 if (r.right-msg.xpos)<4 then exit;

 if (x>=self.fixedcols) and
    (y>=self.fixedrows) and
    (canedit) and
    (FMouseActions.DirectEdit) then
      begin
       showeditor;
      end;
end;


function TAdvStringGrid.Compare(col,arow1,arow2:longint):integer;
var
 astyle:tSortStyle;
 r1,r2:double;
 code1,code2:integer;
 dt1,dt2:tdatetime;
 res:integer;
 s1,s2:string;
 prefix,suffix:string;

 function strToShortdateUS(s:string):tdatetime;
 var
  da,mo,ye,i:word;
  code:integer;
 begin
  result:=0;

  i:=pos('-',s);
  if i=0 then i:=pos('/',s);
  if i=0 then i:=pos('.',s);

  if (i>0) then val(copy(s,1,i-1),mo,code) else exit;
  delete(s,1,i);

  i:=pos('-',s);
  if i=0 then i:=pos('/',s);
  if i=0 then i:=pos('.',s);

  if (i>0) then val(copy(s,1,i-1),da,code) else exit;

  delete(s,1,i);
  val(s,ye,code);
  if ye<=25 then ye:=ye+2000 else ye:=ye+1900;
  result:=encodedate(ye,mo,da);
 end;

 function strToShortDateEU(s:string):tdatetime;
 var
  da,mo,ye,i:word;
  code:integer;
 begin
  result:=0;

  i:=pos('-',s);
  if i=0 then i:=pos('/',s);
  if i=0 then i:=pos('.',s);

  if (i>0) then val(copy(s,1,i-1),da,code) else exit;
  delete(s,1,i);

  i:=pos('-',s);
  if i=0 then i:=pos('/',s);
  if i=0 then i:=pos('.',s);

  if (i>0) then val(copy(s,1,i-1),mo,code) else exit;

  delete(s,1,i);
  val(s,ye,code);
  if ye<=25 then ye:=ye+2000 else ye:=ye+1900;
  result:=encodedate(ye,mo,da);
 end;

begin
 inc(compares);

 if fSortAutoFormat then aStyle:=ssAutomatic else aStyle:=ssAlphabetic;

 {
 col:=remapcol(col);
 }

 prefix:='';
 suffix:='';

 if Assigned(FOnGetFormat) then
  begin
   FOnGetFormat(self,col,aStyle,prefix,suffix);
  end;

 res:=1;

 if aStyle=ssAutomatic then
  begin
   s1:=self.Cells[col,arow1];
   if (IsType(s1) in [atNumeric,atFloat]) then aStyle:=ssNumeric
   else
    begin
      code1:=pos('/',s1);
      if (code1>1) and (length(s1)>code1) then
        begin
         if (s1[code1-1] in ['0'..'9']) and (s1[code1+1] in ['0'..'9']) then aStyle:=ssDate else aStyle:=ssAlphabetic;
        end
      else
       aStyle:=ssAlphabetic;
    end;
  end;

 case aStyle of
 ssAlphabetic,ssAlphaCase:
  begin
   if (self.Cells[col,arow1]>self.Cells[col,arow2]) then res:=1
    else
     if
       (self.Cells[col,arow1]=self.Cells[col,arow2]) then res:=0
       else
        res:=-1;

  end;
 ssHTML:
  begin
   if (self.StrippedCells[col,arow1]>self.StrippedCells[col,arow2]) then res:=1
    else
     if
       (self.StrippedCells[col,arow1]=self.StrippedCells[col,arow2]) then res:=0
       else
        res:=-1;
  end;
 ssAlphaNoCase:
  begin
   if (UpperCase(self.Cells[col,arow1])>UpperCase(self.Cells[col,arow2])) then res:=1
    else
     if
       (self.Cells[col,arow1]=self.Cells[col,arow2]) then res:=0
       else
        res:=-1;
  end;
 ssAnsiAlphaCase:
  begin
   res:=AnsiCompareStr(self.Cells[col,arow1],self.Cells[col,arow2]);
   if res>0 then res:=1 else if res<0 then res:=-1;
  end;
 ssAnsiAlphaNoCase:
  begin
   res:=AnsiCompareText(self.Cells[col,arow1],self.Cells[col,arow2]);
   if res>0 then res:=1 else if res<0 then res:=-1;
  end;
 ssNumeric,ssFinancial:
  begin
   s1:=self.Cells[col,arow1];
   s2:=self.Cells[col,arow2];

   if (suffix<>'') then
    begin
     if (pos(suffix,s1)>0) then delete(s1,pos(suffix,s1),length(suffix));
     if (pos(suffix,s2)>0) then delete(s2,pos(suffix,s2),length(suffix));
    end;

   if (prefix<>'') then
    begin
     if (pos(prefix,s1)>0) then delete(s1,pos(prefix,s1),length(prefix));
     if (pos(prefix,s2)>0) then delete(s2,pos(prefix,s2),length(prefix));
    end;

   if (aStyle=ssFinancial) then
    begin
     {delete the thousandseparator}
     while (pos(thousandseparator,s1)<>0) do delete(s1,pos(thousandseparator,s1),1);
     while (pos(thousandseparator,s2)<>0) do delete(s2,pos(thousandseparator,s2),1);
    end;

   if {(aStyle=ssNumeric) and} (decimalseparator<>'.') then
    begin
     if (pos(decimalseparator,s1)<>0) then s1[pos(decimalseparator,s1)]:='.';
     if (pos(decimalseparator,s2)<>0) then s2[pos(decimalseparator,s2)]:='.';
    end;

   val(s1,r1,code1);
   val(s2,r2,code2);

   if (code1<>0) then
    begin
     if self.cells[col,arow1]='' then
       begin
         r1:=0;
         code1:=0;
       end;
    end;
   if (code2<>0) then
    begin
     if self.cells[col,arow2]='' then
       begin
         r2:=0;
         code2:=0;
       end;
    end;

   if (code1<>0) or (code2<>0) then res:=0
   else
    begin
     if r1>r2 then res:=1
      else
       if r1=r2 then res:=0
        else
         res:=-1;
    end;
  end;

 ssCustom:begin
           res:=0;
           if assigned(FCustomCompare) then
              FCustomCompare(self,self.Cells[col,arow1],self.Cells[col,arow2],res);
          end;

 ssRaw:begin
         res:=0;
         if assigned(FRawCompare) then
            FRawCompare(self,col,arow1,arow2,res);
       end;

 ssDate,ssShortdateUS,ssShortDateEU:
  begin
   dt1:=0; dt2:=0;
   case aStyle of
   ssDate:begin
           try
            dt1:=StrToDatetime(self.Cells[col,arow1]);
           except
            dt1:=0;
           end;
           try
            dt2:=StrToDatetime(self.Cells[col,arow2]);
           except
            dt2:=0;
           end;
          end;
   ssShortDateUS:begin
           try
            dt1:=StrToShortDateUS(self.Cells[col,arow1]);
           except
            dt1:=0;
           end;
           try
            dt2:=StrToShortDateUS(self.Cells[col,arow2]);
           except
            dt2:=0;
           end;
          end;
   ssShortDateEU:begin
           try
            dt1:=StrToShortDateEU(self.Cells[col,arow1]);
           except
            dt1:=0;
           end;
           try
            dt2:=StrToShortDateEU(self.Cells[col,arow2]);
           except
            dt2:=0;
           end;
          end;
   end;


   if (dt1>dt2) then res:=1
    else
     if (dt1=dt2) then res:=0
      else
       res:=-1;
  end;
 end;
 compare:=res;
end;

function TAdvStringGrid.CompareLine(col,arow1,arow2:longint):integer;
var
 res:integer;
begin
 res:=self.Compare(col,arow1,arow2);

 if (res=0) and fSortFull then
   begin
    if (col<=colcount-2) then
      begin
        inc(col);
        res:=self.CompareLine(col,arow1,arow2);
      end;
   end;

 CompareLine:=res;
end;

function TAdvStringGrid.CompareLineIndexed(colidx,arow1,arow2:longint):integer;
var
 res:integer;
begin
 res:=self.Compare(FSortIndexes.Items[colidx],arow1,arow2);
 if (res=0) and fSortFull then
  begin
   if (colidx<FSortIndexes.Count-1) then
    begin
     inc(colidx);
     res:=self.CompareLineIndexed(colidx,arow1,arow2);
    end;
  end;
 CompareLineIndexed:=res;
end;


function TAdvStringGrid.SortLine(col,arow1,arow2:longint):boolean;
var
 res:integer;

begin
 res:=self.Compare(col,arow1,arow2);
 sortline:=false;

 if (res=sortdir) then
   begin
    self.SortSwapRows(arow1,arow2);
    sortline:=true;
   end
 else
   if (res=0) then
    begin
     if (col<colcount-1+NumHiddenColumns) then
       begin
         inc(col);
         sortline:=sortline(col,arow1,arow2);
       end;
    end
   else sortline:=false;
end;

procedure TAdvStringGrid.SortByColumn(col:integer);
var
 Idx:Word;
 Changed:Boolean;
begin
 if (rowcount<2) then exit;

 if FSortDirection = sdAscending then sortdir:=1 else sortdir:=-1;

 repeat
  Changed:=False;
  for Idx:=FixedRows to Rowcount-2-Fixedfooters do
   begin
     if (sortdir=self.Compare(col,idx,idx+1)) then
       begin
         self.SwapRows(idx,idx+1);
         changed:=true;
       end;
   end;
 until Changed = False;
end;

procedure TAdvStringGrid.QuickSortRows(col,left,right:integer);
var
 i,j:integer;
begin
 if (FSortDirection=sdAscending) then sortdir:=1 else sortdir:=-1;

 i:=left;
 j:=right;

 {copy middle row here}
 rows[rowcount-2]:=rows[(left+right) shr 1];

 repeat
   while (self.compareline(col,rowcount-2,i)=sortdir) do inc(i);
   while (self.compareline(col,j,rowcount-2)=sortdir) do dec(j);
   if (i<=j) then
    begin
      if (i<>j) then self.SortSwaprows(i,j);
      inc(i);
      dec(j);
    end;
 until (i>j);

 if (left<j) then QuicksortRows(col,left,j);
 if (i<right) then QuickSortRows(col,i,right);
end;


procedure TAdvStringGrid.QuickSort(col,left,right:integer);
var
 i:integer;
 cw,cc:integer;

begin
 rowcount:=rowcount+3;
 {necessary to save this due to Delphi 1,2,3 bug in TStringGrid!}
 cc:=colcount-1;
 cw:=colwidths[cc];

 colcount:=colcount+NumHiddenColumns;

 self.QuickSortRows(col,left,right);

 for i:=0 to colcount-1 do
  begin
   cells[i,rowcount-1]:='';
   objects[i,rowcount-1]:=nil;
   cells[i,rowcount-2]:='';
   objects[i,rowcount-2]:=nil;
  end;

 colcount:=colcount-NumHiddenColumns;
 colwidths[cc]:=cw;
 rowcount:=rowcount-3;
end;

procedure TAdvStringGrid.QuickSortRowsIndexed(col,left,right:integer);
var
 i,j:integer;
begin
 if (FSortDirection=sdAscending) then sortdir:=1 else sortdir:=-1;

 i:=left;
 j:=right;

 {copy middle row here}
 rows[rowcount-2]:=rows[(left+right) shr 1];

 repeat
   while (self.comparelineindexed(col,rowcount-2,i)=sortdir) do inc(i);
   while (self.comparelineindexed(col,j,rowcount-2)=sortdir) do dec(j);
   if (i<=j) then
    begin
      if (i<>j) then self.SortSwaprows(i,j);
      inc(i);
      dec(j);
    end;
 until (i>j);

 if (left<j) then QuicksortRowsIndexed(col,left,j);
 if (i<right) then QuickSortRowsIndexed(col,i,right);
end;


procedure TAdvStringGrid.QuickSortIndexed(left,right:integer);
var
 i:integer;
 cw,cc:integer;

begin
 rowcount:=rowcount+3;
 {necessary to save this due to Delphi 1,2,3 bug in TStringGrid!}
 cc:=colcount-1;
 cw:=colwidths[cc];

 colcount:=colcount+NumHiddenColumns;

 self.QuickSortRowsIndexed(0,left,right);

 for i:=0 to colcount-1 do
  begin
   cells[i,rowcount-1]:='';
   objects[i,rowcount-1]:=nil;
   cells[i,rowcount-2]:='';
   objects[i,rowcount-2]:=nil;
  end;

 colcount:=colcount-NumHiddenColumns;
 colwidths[cc]:=cw;
 rowcount:=rowcount-3;
end;


procedure TAdvStringGrid.QSortGroup;
var
 i,j,r1,r2:integer;
 cw,cc:integer;
begin
 BeginUpdate;
 try
  rowcount:=rowcount+3;

  {necessary to save this due to Delphi 1,2,3 bug in TStringGrid!}
  cc:=colcount-1;
  cw:=colwidths[cc];

  colcount:=colcount+NumHiddenColumns;
  sortrow:=row;

  if FNavigation.MoveRowOnSort then row:=rowcount-3;
  r1:=0; r2:=0;
  j:=1;
  for i:=1 to self.fNumNodes do
   begin
    if IsNode(j) then
      begin
       inc(j);
       r1:=j;
       while not IsNode(j) and (j<rowcount-3) do inc(j);
       r2:=j;
      end;

    if (r2-r1>1) then
      begin
       self.QuickSortRows(sortcolumn,r1,r2-1);
      end;

   end;

  {set all added rows back to nil}
  for i:=0 to colcount-1 do
   begin
    cells[i,rowcount-1]:='';
    objects[i,rowcount-1]:=nil;
    cells[i,rowcount-2]:='';
    objects[i,rowcount-2]:=nil;
   end;

  row:=sortrow;
  colcount:=colcount-NumHiddenColumns;
  colwidths[cc]:=cw;
  rowcount:=rowcount-3;
 finally
  EndUpdate;
 end;
end;


procedure TAdvStringGrid.QSort;
var
 i:integer;
 cw,cc:integer;
 enterstate:boolean;

begin
 BeginUpdate;
 try
  rowcount:=rowcount+3;

  {necessary to save this due to Delphi 1,2,3 bug in TStringGrid!}
  cc:=colcount-1;
  cw:=colwidths[cc];

  colcount:=colcount+NumHiddenColumns;
  sortrow:=row;

  if FNavigation.MoveRowOnSort then row:=rowcount-3;

  self.QuickSortRows(sortcolumn,self.FixedRows,(self.Rowcount-1)-3-fFixedFooters);

  {set all added rows back to nil}
  for i:=0 to colcount-1 do
   begin
    cells[i,rowcount-1]:='';
    objects[i,rowcount-1]:=nil;
    cells[i,rowcount-2]:='';
    objects[i,rowcount-2]:=nil;
   end;

  enterstate:=fEntered;
  fEntered:=false;

  row:=sortrow;

  fEntered:=enterstate;

  colcount:=colcount-NumHiddenColumns;
  colwidths[cc]:=cw;
  rowcount:=rowcount-3;
 finally
  EndUpdate;
 end;
end;

procedure TAdvStringGrid.QSortIndexed;
var
 i:integer;
 cw,cc:integer;
 enterstate:boolean;

begin
 BeginUpdate;
 try
  rowcount:=rowcount+3;

  {necessary to save this due to Delphi 1,2,3 bug in TStringGrid!}
  cc:=colcount-1;
  cw:=colwidths[cc];

  colcount:=colcount+NumHiddenColumns;
  sortrow:=row;

  if FNavigation.MoveRowOnSort then row:=rowcount-3;

  self.QuickSortRowsIndexed(0,self.FixedRows,(self.Rowcount-1)-3-fFixedFooters);

  {set all added rows back to nil}
  for i:=0 to colcount-1 do
   begin
    cells[i,rowcount-1]:='';
    objects[i,rowcount-1]:=nil;
    cells[i,rowcount-2]:='';
    objects[i,rowcount-2]:=nil;
   end;

  enterstate:=fEntered;
  fEntered:=false;

  row:=sortrow;

  fEntered:=enterstate;

  colcount:=colcount-NumHiddenColumns;
  colwidths[cc]:=cw;
  rowcount:=rowcount-3;
 finally
  EndUpdate;
 end;
end;


procedure TAdvStringGrid.RemoveRows(RowIndex, RCount : LongInt);
var
  i:LongInt;
  cc,cw,cr:integer;
  tr:longint;

begin
  {necessary to save this due to Delphi 1,2,3 bug in TStringGrid!}
  cc:=colcount-1;
  cw:=colwidths[cc];

  {tr:=toprow;}

  ColCount:=ColCount+fNumHidden;

  {
  ClearRows(RowIndex,RCount);
  for i:=RowIndex to RowCount-1 do Rows[i]:=Rows[i+RCount];
  for i:=RowIndex to RowCount-1 do RowHeights[i]:=RowHeights[i+RCount];
  RowCount:=RowCount-RCount;
  }
  beginupdate;
  tr:=toprow;
  cr:=row;
  for i:=1 to rcount do
    begin
     DeleteRow(RowIndex);
     if fMouseActions.DisjunctRowSelect then fRowSelect.Delete(RowIndex);
    end;
  if cr<rowcount then
    begin
     row:=cr;
     toprow:=tr;
    end;

  ColCount:=ColCount-fNumHidden;
  colwidths[cc]:=cw;
  endupdate;
end;

procedure TAdvStringGrid.ClearRect(aCol1,aRow1,aCol2,aRow2:integer);
var
 i,j:integer;
 {$IFNDEF WIN32}
 tmp:tcellgraphic;
 {$ENDIF}
begin
  for j:=arow1 to arow2 do
   begin
    for i:=acol1 to acol2 do
     begin
      if (Cells[i,j]<>'') then Cells[i,j]:='';
      if fEnableGraphics then
       begin
        {$IFNDEF WIN32}
        tmp:=Objects[i,j] as tcellgraphic;
        if assigned(tmp) then
        {$ELSE}
        if assigned(Objects[i,j]) then
        {$ENDIF}
         if (Objects[i,j] is TCellGraphic) then
          begin
          (Objects[i,j] as TCellGraphic).Free;
          Objects[i,j]:=nil;
          end;
       end;
     end;
   end;
end;

procedure TAdvStringGrid.ClearRows(RowIndex,RCount:LongInt);
begin
 if (rowcount>0) and (colcount>0) and (RCount>0) then
 self.ClearRect(0,RowIndex,colcount-1+FNumHidden,RowIndex+RCount-1);
end;

procedure TAdvStringGrid.InsertRows(RowIndex,RCount:LongInt);
var
  i,j:LongInt;
  cw,cc:integer;
begin
  {necessary to save this due to Delphi 1,2,3 bug in TStringGrid!}
  cc:=colcount-1;
  cw:=colwidths[cc];

  colcount:=colcount+fNumHidden;

  RowCount:=RowCount+RCount;

  for i:=RowCount-1 downto (RowIndex+Rcount) do Rows[i]:=Rows[i-RCount];
  for i:=RowCount-1 downto (RowIndex+Rcount) do RowHeights[i]:=RowHeights[i-RCount];

  for i:=RowIndex to RowIndex+Rcount-1 do RowHeights[i]:=defaultrowheight;

  if fEnableGraphics then
  begin
  for i:=0 to RCount-1 do
    for j:=0 to ColCount-1 do
     Objects[j,RowIndex+i]:=nil;
  end;
  ClearRows(RowIndex,Rcount);

  colcount:=colcount-fNumHidden;
  colwidths[cc]:=cw;
end;

procedure TAdvStringGrid.ClearCols(ColIndex,CCount:LongInt);
begin
 if (rowcount>0) and (colcount>0) and (CCount>0) then
 self.ClearRect(ColIndex,0,ColIndex+CCount-1,rowcount-1);
end;

procedure TAdvStringGrid.RemoveCols(ColIndex,CCount:LongInt);
var
  i: LongInt;
begin
  ClearCols(ColIndex,CCount);
  for i:=ColIndex to ColCount-1 do Cols[i]:=Cols[i+CCount];
  for i:=ColIndex to ColCount-1 do Colwidths[i]:=Colwidths[i+CCount];

  ColCount:=ColCount-CCount;
end;

procedure TAdvStringGrid.InsertCols(ColIndex,CCount:LongInt);
var
  i,j: LongInt;
begin
  ColCount:=ColCount+CCount;
  for i:=ColCount-1 downto (ColIndex+Ccount) do Cols[i]:=Cols[i-CCount];
  for i:=ColCount-1 downto (ColIndex+Ccount) do Colwidths[i]:=Colwidths[i-CCount];

  for i:=ColIndex to ColIndex+CCount-1 do ColWidths[i]:=defaultcolwidth;

  if fEnableGraphics then
  begin
  for i:=0 to CCount-1 do
    for j:=0 to RowCount-1 do
     Objects[ColIndex+i,j]:=nil;
  end;
  ClearCols(ColIndex,Ccount);
end;

procedure TAdvStringGrid.MergeCols(ColIndex1, ColIndex2 : LongInt);
var
 i:integer;
 s:string;
begin
 for i:=FixedRows to self.RowCount-1 do
  begin
   s:=Cells[ColIndex1,i]+' '+Cells[ColIndex2,i];
   Cells[ColIndex1,i]:=Trim(s);
  end;
 RemoveCols(ColIndex2,1);
end;

procedure TAdvStringGrid.ClearNormalCells;
begin
 if (rowcount>0) and (colcount>0) then
 self.ClearRect(fixedcols,fixedrows,colcount-1+fNumHidden-fixedRightCols,rowcount-1-fixedFooters);
end;

procedure TAdvStringGrid.Clear;
begin
 if (rowcount>0) and (colcount>0) then
 self.ClearRect(0,0,colcount-1+fNumHidden,rowcount-1);
end;

function TAdvStringGrid.isCell(SubStr: String; var ACol, ARow: LongInt): Boolean;
var
  i,j:LongInt;
begin
  for i:=0 to RowCount-1 do
  begin
    for j:=0 to ColCount-1 do
    begin
      if (Rows[i].Strings[j]=SubStr) then
      begin
        ARow:=i;
        ACol:=j;
        Result:=True;
        exit;
      end;
    end;
  end;
  Result:=False;
end;


procedure TAdvStringGrid.SaveToFile(FileName: String);
var
  i,j:LongInt;
  ss:string;
  f:TextFile;
  strtcol,strtrow:integer;

begin
  AssignFile(f, FileName);
  Rewrite(f);

  strtcol:=FixedCols;
  strtrow:=FixedRows;

  if fSaveFixedCells then
   begin
    strtcol:=0;
    strtrow:=0;
   end;

  ss:=IntToStr(ColCount+fNumHidden-strtcol)+','+IntToStr(RowCount-strtrow);
  Writeln(f,ss);

  for i:=0 to colcount-1+fNumHidden do
   begin
    ss:='cw '+inttostr(i)+','+inttostr(colwidths[i]);
    Writeln(f,ss);
   end;

  for i:=strtrow to RowCount-1 do
  begin
    for j:=strtcol to ColCount-1+fNumHidden do
    begin
      if (Cells[j,i]<>'') then
      begin
        ss:=IntToStr(j)+','+IntToStr(i)+','+lftofile(Cells[j,i]);
        Writeln(f,ss);
      end;
    end;
  end;
  CloseFile(f);
end;

procedure TAdvStringGrid.LoadFromFile(FileName: String);
var
  X,Y,CW:Integer;
  ss,ss1:string;
  f:TextFile;
  strtcol,strtrow:integer;

  function mStrToInt(s:string):integer;
  var
   code,i:integer;
  begin
   val(s,i,code);
   result:=i;
  end;

begin
  AssignFile(f, FileName);
  Reset(f);
  if (IOResult<>0) then raise EAdvGridError.Create('File ' + FileName + ' not found');

  strtcol:=FixedCols;
  strtrow:=FixedRows;

  if fSaveFixedCells then
   begin
    strtcol:=0;
    strtrow:=0;
   end;

  Readln(f,ss);
  if (ss<>'') then
  begin
    ss1:=Copy(ss,1,Pos(',',ss)-1);
    ColCount:=mStrToInt(ss1)+strtcol;
    ss1:=Copy(ss,Pos(',',ss)+1,Length(ss));
    RowCount:=mStrToInt(ss1)+strtrow;
  end;

  if (colcount=0) or (rowcount=0) then
    begin
     closefile(f);
     raise EAdvGridError.Create('File contains no data or corrupt file '+FileName);
    end;

  while not Eof(f) do
  begin
    Readln(f, ss);

    if pos('cw',ss)=1 then {parse cw i,width }
     begin
      ss1:=copy(ss,4,pos(',',ss)-4);
      ss:=copy(ss,pos(',',ss)+1,255);
      CW:=mstrtoint(ss1);
      if (cw>=0) and (cw<colcount) then
         colwidths[cw]:=mstrtoint(ss);
     end
    else
     begin
      ss1:=Copy(ss,1,Pos(',',ss)-1);
      ss:=Copy(ss,Pos(',',ss)+1,Length(ss));
      X:=mStrToInt(ss1);

      ss1:=Copy(ss,1,Pos(',',ss)-1);
      ss:=Copy(ss, Pos(',',ss)+1,Length(ss));
      Y:=mStrToInt(ss1);

      if (X<colcount) and (Y<rowcount) then
        begin
         Cells[X,Y]:=filetolf(ss,fMultiLineCells);
        end;
     end;
  end;
  CloseFile(f);
end;

{$IFDEF ISDELPHI}
function TAdvStringGrid.CellToReal(ACol, ARow: LongInt): Real;
var
 i:Real;
 Code:Integer;
begin
 result:=0.0;
 if (Cells[ACol,ARow]<>'') then
 begin
  Val(Cells[ACol, ARow], i, Code);
  if Code <> 0 then raise
        EAdvGridError.Create('Error at position: ' +
        IntToStr(Code) + ' in Cell [' + IntToStr(ACol) + ', ' +
        IntToStr(ARow) + '].')
   else
   Result := i;
 end;
end;
{$ENDIF}

procedure TAdvStringGrid.SaveToASCII(FileName: String);
var
 s,z:integer;
 cellstr,str,alistr,remainingstr:string;
 i:integer;
 MultiLineList: TStringlist;
 OutputFile:TextFile;
 anotherlinepos: integer;
 blanksfiller: String;
 blankscount,NeededLines: integer;
 AlignValue:TAlignment;
 colchars:array[0..maxcolumns] of byte;

begin
 screen.Cursor:=crHourGlass;
 AssignFile(OutputFile,FileName);
 Rewrite(OutputFile);

 for i:=0 to colcount-1 do
  colchars[i]:=maxcharsincol(remapcol(i));

 try
   MultiLineList := TStringlist.Create;
   for z:=0 to rowcount-1 do
    begin
     str:='';
     for s:=0 to colcount-1 do
      begin
       if (pos(#13#10, cells[remapcol(s),z])>0) and (multilinecells) then  {handle multiline cells}
        begin
         cellstr := copy(cells[remapcol(s),z],0, pos(#13#10, cells[remapcol(s),z])-1);
         remainingstr := copy(cells[remapcol(s),z], pos(#13#10, cells[remapcol(s),z])+2, length(cells[remapcol(s),z]));
         NeededLines := 0;
         repeat
          inc(NeededLines);
          blanksfiller:='';
          blankscount:=0;

          if (MultiLineList.Count<NeededLines) then  {we haven't already added a new line for an earlier colunn}
                 MultiLineList.Add('');

          {nr of spaces before cell text}
          for i:=0 to s-1 do blankscount:=blankscount+ColChars[i]+1;

          {add to line sufficient blanks}
          for i := 0 to (blankscount-length(MultiLineList[NeededLines-1])-1) do blanksfiller:=blanksfiller+' ';

          MultiLineList[NeededLines-1]:=MultiLineList[NeededLines-1]+blanksfiller;

          anotherlinepos := pos(#13#10, remainingstr);

          if anotherlinepos>0 then
            begin
             alistr:=copy(remainingstr, 0, anotherlinepos-1);
             remainingstr:=copy(remainingstr,pos(#13#10,remainingstr)+2,length(remainingstr));
            end
           else
            begin
             alistr:=remainingstr;
            end;

           AlignValue :=GetCellAlignment(s,z);

           case AlignValue of
           taRightJustify:while (length(alistr)<colchars[s]) do alistr:=' '+alistr;
           taCenter:while (length(alistr)<colchars[s]) do alistr:=' '+alistr+' ';
           end;
           MultiLineList[NeededLines-1]:=MultiLineList[NeededLines-1]+alistr;

          until anotherlinepos=0;
         end
         else
          cellstr:=cells[remapcol(s),z];
          if (pos(#13#10,cellstr)>0) then cellstr:=copy(cellstr,0,pos(#13#10,cellstr)-1);

          AlignValue:=GetCellAlignment(s,z);
          case AlignValue of
          taRightJustify:while (length(cellstr)<colchars[s]) do cellstr:=' '+cellstr;
          taCenter:while (length(cellstr)<colchars[s]) do cellstr:=' '+cellstr+' ';
          end;

          blanksfiller:='';
          blankscount:=colchars[s];
          for i:=0 to (blankscount-length(cellstr)) do blanksfiller:=blanksfiller+' ';
          str:=str+cellstr+blanksfiller;
       end;  {column}

       Writeln(OutputFile,Str);
       for i := 0 to MultiLineList.Count-1 do
         Writeln(OutputFile, MultiLineList[i]);     {finally, add the extra lines for this row}
       MultiLineList.Clear;

     end;    {row}
   MultiLineList.Free;
 finally
   CloseFile(OutputFile);
   Screen.Cursor := crDefault;
 end;
end;

{---------------------------------------------------
v 0.98 : added column alignment HTML code
v 0.99 : fixed issue about not saving first column
         added saving cell colors too
---------------------------------------------------}

procedure TAdvStringGrid.SaveToHTML(Filename:string);
{$IFDEF WIN32}
var
  i,j:Integer;
  s,al,ac,afs,afe,ass,ase:string;
  slist:tStringlist;
  AlignValue:tAlignment;
  toprow:integer;
  strtcol,strtrow:integer;
  f,hdr:textfile;

const
 HexDigit:array[0..$F] of char = '0123456789ABCDEF';

  function MakeHex(l:longint):string;
  var
  lw,hw:word;

  begin
   lw:=loword(l);
   hw:=hiword(l);
   MakeHex:=HexDigit[Lo(lw) shr 4]+HexDigit[Lo(lw) and $F]+
            HexDigit[Hi(lw) shr 4]+HexDigit[Hi(lw) and $F]+
            HexDigit[Lo(hw) shr 4]+HexDigit[Lo(hw) and $F];
  end;

  function SetLineBreaks(s:string):string;
  var
   s1:string;
   i:integer;
   begin
   s1:='';
   for i:=1 to length(s) do
    begin
     if (s[i]<>#13) then s1:=s1+s[i] else s1:=s1+'<br>';
    end;
   SetLineBreaks:=s1;
  end;

  function MakeHREF(s:string):string;
  begin
   result:=s;
   {$IFDEF WIN32}
   if not URLshow then exit;
   if (pos('://',s)>0) and (pos('</',s)=0) then
    begin
     if not URLFull then
      result:='<A HREF='+s+'>'+copy(s,pos('://',s)+3,255)+'</A>'
     else
      result:='<A HREF='+s+'>'+s+'</A>';
    end;
   if (pos('mailto:',s)>0) and (pos('</',s)=0) then
    begin
     if not URLFull then
      result:='<A HREF='+s+'>'+copy(s,pos('mailto:',s)+3,255)+'</A>'
     else
      result:='<A HREF='+s+'>'+s+'</A>';
    end;
   {$ENDIF}
  end;

begin
  strtcol:=FixedCols;
  strtrow:=FixedRows;
  if fSaveFixedCells then
   begin
    strtcol:=0;
    strtrow:=0;
   end;

  sList:=tStringlist.Create;
  sList.sorted:=false;

  with SList do
   begin
    if fHTMLSettings.PrefixTag<>'' then
         Add(fHTMLSettings.PrefixTag);

    Add('<table border="'+inttostr(fHTMLSettings.BorderSize)+
        '" cellspacing="'+inttostr(fHTMLSettings.CellSpacing)+
        '" width="'+inttostr(fHTMLSettings.Width)+'%">');        { begin the table }

    toprow:=strtrow;

    if (fixedrows>0) and (fSaveFixedCells) then
     begin
      Add('<tr>');
       for i:=strtcol to ColCount-1 do
        begin
         s:=SetLineBreaks(Cells[remapcol(i),0]);
         s:=MakeHREF(s);
         if s='' then s:='<br>';
         al:='';
         ac:='';
         afs:='';
         afe:='';

         AlignValue:=GetCellAlignment(i,0);

         if (s<>'') and (AlignValue<>taLeftJustify) then
           begin
            if AlignValue=taRightJustify then
              al:=' align="right"';
            if AlignValue=taCenter then
              al:=' align="center"';
           end;

         ac:=' BGCOLOR="#'+makehex(GetSysColor(COLOR_ACTIVEBORDER))+'"';
         Add('<td nowrap'+al+ac+'><B>'+s+'</B></td>');
        end;
      Add('</tr>');
      toprow:=1;
     end;


    for i:=toprow to RowCount-1 do
     begin
      Add('<tr>');
      for j:=strtcol to ColCount-1 do
       begin
        s:=SetLineBreaks(Cells[remapcol(j),i]);
        s:=MakeHREF(s);
        al:=''; ac:=''; afs:=''; afe:=''; ass:=''; ase:='';
        AlignValue:=GetCellAlignment(j,i);

        if (AlignValue<>taLeftJustify) and (s<>'') then
          begin
           if AlignValue=taRightJustify then
             al:=' align="right"';
           if AlignValue=taCenter then
             al:=' align="center"';
          end;

        if (i<fixedrows) or (j<fixedcols) then
         begin
           ac:=' BGCOLOR="#'+makehex(GetSysColor(COLOR_ACTIVEBORDER))+'"';
         end
        else
        if Assigned(FOnGetCellColor) then
          begin
           canvas.brush.color:=$7fffffff;
           canvas.font.color:=$7fffffff;
           canvas.font.style:=[];
           FOnGetCellColor(Self,i,j,[],canvas.Brush,canvas.Font);
           if (canvas.brush.color<>$7fffffff) and fHTMLSettings.SaveColor then
            ac:=' BGCOLOR="#'+makehex(longint(canvas.brush.color))+'"';
           if (canvas.font.color<>$7fffffff) and fHTMLSettings.SaveColor then
             begin
              afs:='<font color="#'+makehex(longint(canvas.font.color))+'">';
              afe:='</font>';
             end;

           if fHTMLSettings.SaveFonts then
           begin
           if (fsBold in canvas.font.style) then
               begin
                ass:=ass+'<B>';
                ase:=ase+'</B>';
               end;
           if (fsItalic in canvas.font.style) then
               begin
                ass:=ass+'<I>';
                ase:='</I>'+ase;
               end;
           if (fsUnderline in canvas.font.style) then
               begin
                ass:=ass+'<U>';
                ase:='</U>'+ase;
               end;
           end;
          end;

        if s='' then s:='<br>';
        Add('<td nowrap'+al+ac+'>'+ afs + ass + s + ase + afe + '</td>');
      end; { for }
      Add('</tr>');
    end;

    Add('</table><p>');
   if (fHTMLSettings.SuffixTag<>'') then
     Add(fHTMLSettings.SuffixTag);

  end; { with SList }

  assignfile(f,filename);
  {$i-}
  Rewrite(f);
  {$i+}
  if (IOResult<>0) then
     begin
      slist.free;
      raise EAdvGridError.Create('Cannot create ' + FileName);
     end;

  if (fHTMLSettings.HeaderFile<>'') then
   begin
    assignfile(hdr,fHTMLSettings.HeaderFile);
    {$i-}
    reset(hdr);
    {$i+}
    if (ioresult=0) then
     begin
       while not eof(hdr) do
        begin
         readln(hdr,s);
         writeln(f,s);
        end;
       closefile(hdr);
     end;
   end;

  for i:=1 to slist.count do
   begin
    writeln(f,slist.strings[i-1]);
   end;

  if (fHTMLSettings.FooterFile<>'') then
   begin
    assignfile(hdr,fHTMLSettings.FooterFile);
    {$i-}
    reset(hdr);
    {$i+}
    if ioresult=0 then
     begin
       while not eof(hdr) do
        begin
         readln(hdr,s);
         writeln(f,s);
        end;
       closefile(hdr);
     end;

   end;

  closefile(f);
  slist.free;
end;
{$ELSE}
begin
end;
{$ENDIF}

procedure TAdvStringGrid.SaveToXML(FileName: String; ListDescr, RecordDescr:string;FieldDescr:tstrings);
var
 i,j:integer;
 f:textfile;
begin
 assignfile(f,filename);
 {$i-}
 rewrite(f);
 {$i+}
 if (ioresult<>0) then
  raise EAdvGridError.Create('Cannot create file '+FileName);

 writeln(f,'<?XML version="1.0"?>');
 writeln(f,'<'+ListDescr+'>');

 for i:=FixedRows to self.Rowcount-1 do
  begin
   writeln(f,'<'+RecordDescr+'>');
   for j:=FixedCols to ColCount-1 do
    begin
     if j<FieldDescr.Count then
       write(f,'<'+fielddescr.strings[j-FixedCols]+'>')
     else
       write(f,'<FIELD'+inttostr(j-FixedCols)+'>');
     write(f,cells[j,i]);
     if j<FieldDescr.Count then
       writeln(f,'</'+fielddescr.strings[j-FixedCols]+'>')
     else
       writeln(f,'</FIELD'+inttostr(j-FixedCols)+'>');
    end;
   writeln(f,'</'+RecordDescr+'>');
  end;

 writeln(f,'</'+ListDescr+'>');

 closefile(f);

end;

procedure TAdvStringGrid.CopyRTFFunc(acol,arow:integer);
var
 s:string;
 GHandle:thandle;
 Gptr:pchar;
 cf_rtf:word;

begin
 s:=cells[acol,arow];
 if (pos('{\',s)=0) then exit;

 GHandle:=GlobalAlloc(gmem_MoveAble,length(s));
 if GHandle=0 then
    begin
     GlobalFree(GHandle);
     exit;
    end;

 GPtr:=GlobalLock(GHandle);
 if GPtr=nil then
    begin
     exit;
     GlobalFree(GHandle);
     exit;
    end;
 strcopy(gptr,'');
 strcat(gptr,pchar(s));
 GlobalUnlock(GHandle);

 if not OpenClipBoard(self.handle) then GlobalFree(GHandle)
   else
    begin
     cf_rtf:=RegisterClipboardformat('Rich Text Format');
     SetClipBoardData(cf_rtf,GHandle);
     CloseClipBoard;
    end;
end;

procedure TAdvStringGrid.CopyFunc(gd:tGridRect);
var
 s,z:integer;
 len:integer;
 buffer,ptr:PChar;
 ct:string;

begin
 fClipTopLeft:=point(gd.left,gd.top);
 {calculate required buffer space}
 len:=1;
 for z:=gd.top to gd.bottom do
  begin
   for s:=gd.left to gd.right do
    begin
     ct:=cells[s,z];
     if (pos('{\',ct)>0) then
       begin
        celltorich(s,z,self.richedit);
        ct:=self.richedit.Text;
       end;
     if (linesintext(ct,fMultiLineCells)>1) and
        (fExcelClipboardformat) then LineFeedsToCSV(ct);
     len:=len+length(ct)+1;{tab}
    end;
   len:=len+1;
  end;

 buffer:=nil;
 {fill buffer and copy to clipboard}
 try
  GetMem(buffer,len);
  {fill buffer}
  buffer^:=#0;
  ptr:=buffer;
  for z:=gd.top to gd.bottom do
   begin
    for s:=gd.left to gd.right do
     begin
      ct:=cells[s,z];
      if (pos('{\',ct)>0) then
        begin
         celltorich(s,z,self.richedit);
         ct:=self.richedit.Text;
        end;

      if (linesintext(ct,fMultiLineCells)>1) and
          (fExcelClipboardFormat) then LineFeedsToCSV(ct);
      ptr:=StrEnd(StrPCopy(ptr,ct+#9));
     end;
     dec(ptr);
     ptr:=StrEnd(StrPCopy(ptr,#13+#10));
   end;
   ptr:=ptr-2;
   ptr^:=#0;
   ClipBoard.SetTextBuf(buffer)
 finally
  FreeMem(buffer,len);
 end;
 if fNavigation.AllowRTFClipboard then CopyRTFFunc(gd.left,gd.top);
end;

procedure TAdvStringGrid.CopySelectionToClipboard;
begin
 CopyFunc(selection);
end;

procedure TAdvStringGrid.CopyToClipboard;
var
 gd:tGridRect;
begin
 gd.top:=0;
 gd.left:=0;
 gd.right:=colcount-1;
 gd.bottom:=rowcount-1;
 copyfunc(gd);
end;

procedure TAdvStringGrid.CutToClipboard;
var
 s,z:integer;
begin
 CopySelectionToClipboard;
 with selection do
   begin
    for s:=Left to Right do
       for z:=Top to Bottom do
        if IsEditable(s,z) then cells[s,z]:='';
   end;
end;

procedure TAdvStringGrid.PasteSelectionFromClipboard;
var
 i,j,k:integer;
 s1,s2:string;
 dt1,dt2,dtv:tdatetime;
 f1,f2,fv:double;
 i1,i2,iv,err:integer;
 da1,da2,mo1,mo2,ye1,ye2,dmo,dye,dda:word;

begin
 i:=self.PasteFunc(selection.left,selection.top);

 if not fNavigation.AllowSmartClipboard then exit;
 if (i=1) and
 ((selection.left<>selection.right) or (selection.top<>selection.bottom)) then
   begin
    for j:=selection.left to selection.right do
     for k:=selection.top to selection.bottom do
      if iseditable(j,k) then
      self.cells[j,k]:=self.cells[selection.left,selection.top];
   end;

 if (i=2) and
 ((selection.left=selection.right) or (selection.top=selection.Bottom)) then
  begin
   s1:=self.cells[selection.left,selection.top];
   if (selection.left=selection.right) then
    s2:=self.cells[selection.left,selection.top+1];
   if (selection.top=selection.Bottom) then
    s2:=self.cells[selection.left+1,selection.top];
   {try dates}
   if ((pos(dateseparator,s1)>0) or (pos(timeseparator,s1)>0)) and
      ((pos(dateseparator,s2)>0) or (pos(timeseparator,s2)>0)) then
      begin
       try
        dt1:=strtodatetime(s1);
        dt2:=strtodatetime(s2);
        dtv:=dt2-dt1;

        decodedate(dt1,ye1,mo1,da1);
        decodedate(dt2,ye2,mo2,da2);

        dmo:=mo2-mo1;
        dye:=ye2-ye1;
        dda:=da2-da1;

        for j:=selection.left to selection.right do
         for k:=selection.top to selection.bottom do
          begin
           if iseditable(j,k) then
            begin
             if pos(dateseparator,s1)>0 then
              self.cells[j,k]:=datetostr(dt1)
             else
              self.cells[j,k]:=timetostr(dt1);
            end;
          {$IFDEF DEBUG}
           outputdebugstr('nxt:'+inttostr(dye)+'-'+inttostr(dmo)+'-'+inttostr(dda));
          {$ENDIF}
           dt1:=GetNextDate(dt1,dye,dmo,dda,dtv);
          end;
       finally
       end;
       exit;
      end;
   {try floats}
   if (pos(decimalseparator,s1)>0) or (pos(decimalseparator,s2)>0) then
    begin
     try
      f1:=StrToFloat(s1);
      f2:=StrToFloat(s2);
      fv:=f2-f1;
      {$IFDEF DEBUG}
      outputdebugstr(floattostr(fv));
      {$ENDIF}
      for j:=selection.left to selection.right do
       for k:=selection.top to selection.bottom do
        begin
         if iseditable(j,k) then
           self.cells[j,k]:=floattostrf(f1,ffGeneral,0,0);
         f1:=f1+fv;
        end;
      exit;
     finally;
     end;
    end;
    {try integer}
    val(s1,i1,err);
    if (err<>0) then exit;
    val(s2,i2,err);
    if (err<>0) then exit;
    iv:=i2-i1;
    for j:=selection.left to selection.right do
     for k:=selection.top to selection.bottom do
      begin
       if iseditable(j,k) then
           self.ints[j,k]:=i1;
       i1:=i1+iv;
      end;
  end;
end;

procedure TAdvStringGrid.PasteFromClipboard;
begin
 self.PasteFunc(0,0);
end;

function TAdvStringGrid.PasteText(acol,arow:integer;p:pchar):integer;
var
 content,endofrow:pchar;
 cr,tab:pchar;
 ct:string;
 s,z:integer;
 numcells:integer;
 gr:tgridrect;

begin
 z:=arow;
 s:=acol;
 content:=p;
 endofrow:=StrScan(content,#0);
 numcells:=0;
 gr.top:=arow;
 gr.left:=acol;
 gr.right:=acol;
 gr.bottom:=arow;

 repeat
   cr:=StrScan(content,#13);
   if cr=nil then cr:=Endofrow;

   repeat
    tab:=StrScan(content,#9);
    if (tab>cr) or (tab=nil) then tab:=cr;
    ct:=copy(StrPas(content),1,tab-content);

    if (pos(#10,ct)>0) then
      begin
       if fExcelClipboardFormat then
        begin
         if pos('"',ct)=1 then delete(ct,1,1);
         if pos('"',ct)=length(ct) then delete(ct,length(ct),1);
         CSVToLineFeeds(ct);
        end;
      end;
    if iseditable(s,z) then cells[s,z]:=ct;
    inc(numcells);
    inc(s);
    if (s>colCount) then colcount:=s;
    if (s>gr.right) then gr.right:=s;
    content:=tab+1;
   until (Tab=Cr);

   inc(content); {delete cr marker}
   s:=acol;
   inc(z);
   if (z=RowCount) and (cr<>endofrow) then RowCount:=z+1;
   if (z>gr.bottom) then gr.bottom:=z;

 until cr=endofrow;

 gr.bottom:=gr.bottom-1;
 gr.right:=gr.right-1;

 PasteNotify(fClipTopLeft,gr);

 result:=numcells;
end;

procedure TAdvStringGrid.PasteNotify(orig:tpoint;gr:tgridrect);
begin
end;

function TAdvStringGrid.PasteFunc(acol,arow:integer):integer;
var
 content:pchar;
 Data:THandle;
 cf_rtf:integer;
 s:string;
begin
 result:=0;

 if fNavigation.AllowRTFClipboard then
  begin
   OpenClipboard(self.handle);
   cf_rtf:=RegisterClipboardformat('Rich Text Format');
   CloseClipboard;
   if Clipboard.HasFormat(cf_rtf) then
     begin
       Clipboard.Open;
       Data:=GetClipboardData(CF_RTF);
       try
          if (Data<>0) then content:=PChar(GlobalLock(Data))
                       else content:=nil
       finally
          if (Data<>0) then GlobalUnlock(Data);
          ClipBoard.Close;
       end;
      if content=nil then exit;
      s:='';
      while (content^<>#0) do
       begin
        s:=s+content^;
        content:=content+1;
       end;
      if IsEditable(acol,arow) then cells[acol,arow]:=s;
      exit;
     end;
  end;

 if not Clipboard.HasFormat(CF_TEXT) then exit;

 Clipboard.Open;
 Data:=GetClipboardData(CF_TEXT);
 try
  if (Data<>0) then content:=PChar(GlobalLock(Data))
               else content:=nil
 finally
  if (Data<>0) then GlobalUnlock(Data);

  ClipBoard.Close;
 end;
 if content=nil then exit;

 result:=self.PasteText(acol,arow,content);
end;

{$IFDEF WIN32}
procedure TAdvStringGrid.LoadFromXLS(filename:string);
var
 fexcel:variant;
 fworkbook:variant;
 fworksheet:variant;
 farray:variant;
 s,z:integer;
 rangestr:string;
 startstr,endstr:string;
 code:integer;
 sr,er,sc,ec:integer;
 strtcol,strtrow:integer;

begin
 screen.cursor:=crHourGlass;

 try
  FExcel:=CreateOleObject('excel.application');
 except
  screen.cursor:=crDefault;
  raise EAdvGridError.Create('Excel OLE server not found');
  exit;
 end;

 {FExcel.visible:=true;}
 FWorkBook := FExcel.WorkBooks.Open(filename);
 FWorkSheet := FWorkBook.ActiveSheet;

 rangestr:=FWorkSheet.UsedRange.Address;

 {decode here how many cells are required, $A$1:$D$8 for example}

 startstr:='';
 endstr:='';
 sr:=-1; er:=-1; sc:=-1; ec:=-1;

 if (pos(':',rangestr)>0) then
   begin
    startstr:=copy(rangestr,1,pos(':',rangestr)-1);
    endstr:=copy(rangestr,pos(':',rangestr)+1,255);

    if (pos('$',startstr)=1) then delete(startstr,1,1);
    if (pos('$',endstr)=1) then delete(endstr,1,1);

    if (pos('$',startstr)>0) then val(copy(startstr,pos('$',startstr)+1,255),sr,code);
    if (code<>0) then sr:=-1;

    if (pos('$',endstr)>0) then val(copy(endstr,pos('$',endstr)+1,255),er,code);
    if (code<>0) then er:=-1;

    {now decode the columns}

    if (pos('$',startstr)>0) then startstr:=copy(startstr,1,pos('$',startstr)-1);
    if (pos('$',endstr)>0) then endstr:=copy(endstr,1,pos('$',endstr)-1);

    if startstr<>'' then sc:=ord(startstr[1])-64;
    if length(startstr)>1 then sc:=sc*26+ord(startstr[2])-64;

    if endstr<>'' then ec:=ord(endstr[1])-64;
    if length(endstr)>1 then ec:=ec*26+ord(endstr[2])-64;
   end;

 {
 messagedlg('rows from '+inttostr(sr)+' to '+inttostr(er),mtinformation,[mbok],0);
 messagedlg('cols from '+inttostr(sc)+' to '+inttostr(ec),mtinformation,[mbok],0);
 }

 if (sr<>-1) and (er<>-1) and (sc<>-1) and (ec<>-1) then
   begin
    colcount:=ec-sc+1;
    rowcount:=er-sr+1;
   end;

 farray:=vararraycreate([0,rowcount-1,0,colcount-1],varVariant);

 rangestr:='A1:';

 if (colcount>26) then
    begin
     rangestr:=rangestr+chr(ord('A')-1+(colcount div 26));
     rangestr:=rangestr+chr(ord('A')-1+(colcount mod 26));
    end
 else
  rangestr:=rangestr+chr(ord('A')-1+colcount);

 rangestr:=rangestr+inttostr(rowcount);

 farray:=FWorkSheet.Range[rangestr].value;

 if fSaveFixedCells then
  begin
   strtcol:=0;
   strtrow:=0;
  end
 else
  begin
   strtcol:=FixedCols;
   strtrow:=FixedRows;
  end;

 for s:=1 to rowcount do
  begin
  for z:=1 to colcount do
   begin
    cells[z-1+strtcol,s-1+strtrow]:=farray[s,z];
   end;
  end;

 { replaced by improved code now
 for s:=0 to colcount-1 do
  begin
  for z:=0 to rowcount-1 do
   begin
    FCell:=FWorkSheet.Cells[z+1,s+1];
    cells[s,z]:=FCell.value;
   end;
  end;
 }

 FWorkBook.Close(SaveChanges:=false);

 FExcel.Quit;
 FExcel:=unassigned;

 screen.cursor:=crDefault;
end;

procedure TAdvStringGrid.SavetoDOC(filename:string);
var
 fword:variant;
 fdoc:variant;
 ftable:variant;
 frng:variant;
 fcell:variant;
 s,z:integer;
begin
 screen.cursor:=crHourGlass;
 try
  fword:=CreateOLEObject('word.application');
 except
  screen.cursor:=crDefault;
  raise EAdvGridError.Create('Word OLE server not found');
  exit;
 end;

 {fword.visible:=false;}
 fdoc:=fword.Documents.Add;
 frng:=fdoc.Range(start:=0,end:=0);
 ftable:=fdoc.Tables.Add(frng,numrows:=rowcount,numcolumns:=colcount);

 for s:=1 to rowcount do
  for z:=1 to colcount do
   begin
    fcell:=ftable.Cell(row:=s,column:=z);
    fcell.Range.InsertAfter(self.cells[remapcol(z-1),s-1]);
    case GetCellAlignment(z-1,s-1) of
    taRightJustify:fcell.Range.ParagraphFormat.Alignment:=wdAlignParagraphRight;
    taCenter:fcell.Range.ParagraphFormat.Alignment:=wdAlignParagraphCenter;
    end;
    {
    .Bold = True
    .ParagraphFormat.Alignment = wdAlignParagraphCenter
    .Font.Name = "Arial"
    }
   end;

 fdoc.SaveAs(filename);
 fword.Quit;
 fword:=unassigned;

 screen.cursor:=crDefault;
end;

procedure TAdvStringGrid.SavetoXLS(filename:string);
var
 fexcel:variant;
 fworkbook:variant;
 fworksheet:variant;
 farray:variant;
 s,z:integer;
 rangestr:string;
 strtcol,strtrow:integer;

begin
 screen.cursor:=crHourGlass;

 try
  FExcel:=CreateOleObject('excel.application');
 except
  screen.cursor:=crDefault;
  raise EAdvGridError.Create('Excel OLE server not found');
  exit;
 end;

 FWorkBook := FExcel.WorkBooks.Add;
 FWorkSheet := FWorkBook.WorkSheets.Add;

 if fSaveFixedCells then
  begin
   strtcol:=0;
   strtrow:=0;
  end
 else
  begin
   strtcol:=FixedCols;
   strtrow:=FixedRows;
  end;

 farray:=vararraycreate([0,rowcount-1-strtrow,0,colcount-1-strtcol],varVariant);

 for s:=strtrow to rowcount-1 do
  begin
  for z:=strtcol to colcount-1 do
   begin
    farray[s-strtrow,z-strtcol]:=LineFeedsToXLS(cells[remapcol(z),s]);
   end;
  end;

 rangestr:='A1:';

 if (colcount>26) then
    begin
     rangestr:=rangestr+chr(ord('A')-1+((colcount-strtcol) div 26));
     rangestr:=rangestr+chr(ord('A')-1+((colcount-strtcol) mod 26));
    end
 else
  rangestr:=rangestr+chr(ord('A')-1+(colcount-strtcol));

 rangestr:=rangestr+inttostr(rowcount-strtrow);

 FWorkSheet.Range[rangestr].Value:=fArray;

 FWorkbook.SaveAs(filename);

 FExcel.Quit;
 FExcel:=unassigned;

 screen.cursor:=crDefault;
end;
{$ENDIF}

procedure TAdvStringGrid.OutputToCSV(FileName:String;appendmode:boolean);
var
  f:textfile;
  z,s:integer;
  strtcol,strtrow:integer;
  celltext:string;
  delim:char;

begin
  {force focus, if editcontrol is still active}
  //self.setfocus;

  strtcol:=FixedCols;
  strtrow:=FixedRows;
  if fSaveFixedCells then
   begin
    strtcol:=0;
    strtrow:=0;
   end;
  if fdelimiter=#0 then delim:=',' else delim:=fdelimiter;

  assignfile(f,FileName);
  if appendmode then
   begin
   {$i-}
    reset(f);
   {$i+}
   if (ioresult<>0) then rewrite(f) else append(f);
   end
  else rewrite(f);

  for z:=strtrow to RowCount-1 do
    begin
      for s:=strtcol to ColCount-1 do
       begin
         if (s>strtcol) then write(f,Delim);
         celltext:=cells[remapcol(s),z];
         if fOemConvert then StringToOem(celltext);
         if (celltext='') then
            begin
             if JavaCSV then celltext:='^' else celltext:='""';
            end;
         if (pos(Delim,celltext)>0) or (linesintext(celltext,fMultiLineCells)>1) then
           begin
            if JavaCSV then LinefeedstoJava(celltext)
            else LinefeedsToCSV(celltext);
           end;

         write(f,celltext);
       end;
     writeln(f)
    end;
 closefile(f)
end;

procedure TAdvStringGrid.SaveToCSV(FileName:String);
begin
 OutputToCSV(FileName,false);
end;

procedure TAdvStringGrid.AppendToCSV(FileName:String);
begin
 OutputToCSV(FileName,true);
end;

procedure TAdvStringGrid.InputFromCSV(Filename:string;insertmode:boolean);
var
 buffer,celltext:string;
 s,z:integer;
 f:textfile;
 strtcol,strtrow:integer;
 c1,c2,cm:integer;
 olddelimiter:char;
 linecount:integer;


begin
 strtcol:=FixedCols;
 strtrow:=FixedRows;

 if fSaveFixedCells or insertmode then
  begin
   strtcol:=0;
   strtrow:=0;
  end;

 AssignFile(f, FileName);
 Reset(f);
 if (IOResult<>0) then raise EAdvGridError.Create('File ' + FileName + ' not found');

 z:=0;

 if insertmode then
    begin
     z:=rowcount;
     rowcount:=rowcount+1;
    end;

 olddelimiter:=fdelimiter;

 {do intelligent estimate of the separator}
 if fdelimiter=#0 then
  begin
   celltext:='';
   readln(f,buffer);
   if not eof(f) then readln(f,celltext);
   reset(f);
   cm:=0;
   for s:=1 to 5 do
   begin
    c1:=numsinglechar(CSVSeparators[s],buffer);
    c2:=numsinglechar(CSVSeparators[s],celltext);
    if (c1=c2) and (c1>cm) then
       begin
        fdelimiter:=CSVSeparators[s];
        cm:=c1;
       end;
   end;
  end;

 {setting rowcount in advance improves memory utilization}
 linecount:=0;
 while not eof(f) do
   begin
    readln(f,buffer);
    inc(linecount);
   end;
 if insertmode then rowcount:=rowcount+linecount else rowcount:=strtrow+linecount;
 reset(f);

 
 while not eof(f) do
 begin
  readln(f,buffer);
  if fOemConvert then OemToString(buffer);
  s:=0;

  while (pos(FDelimiter,buffer)>0) do
   begin
    if (buffer[1]='"') then
      begin
       delete(buffer,1,1); {delete first quote from buffer}

       {search for next single quote}
       if (pos('"',buffer)<>1) then
        begin
         celltext:=copy(buffer,1,singlepos('"',buffer)-1);
         celltext:=DoubleToSingleChar('"',celltext);
        end
       else celltext:=''; 

       delete(buffer,1,pos('"',buffer));
      end
    else
      begin
       celltext:=copy(buffer,1,pos(FDelimiter,buffer)-1);
      end;

    if JavaCSV then
      JavaToLineFeeds(celltext)
    else
      CSVToLineFeeds(celltext);

    cells[s+strtcol,z+strtrow]:=celltext;

    delete(buffer,1,pos(FDelimiter,buffer));

    inc(s);
    if (s+strtcol>ColCount) then ColCount:=s+strtcol;
   end;

  if (length(buffer)>0) then
     begin
      if (buffer[1]='"') then delete(buffer,1,1);
      if (buffer[length(buffer)]='"') then
           delete(buffer,length(buffer),1);

      celltext:=DoubleToSingleChar('"',buffer);
      if JavaCSV then
        JavaToLineFeeds(celltext)
      else
        CSVToLineFeeds(celltext);

      cells[s+strtcol,z+strtrow]:=celltext;

      inc(s);
      if (s+strtcol>ColCount) then ColCount:=s+strtcol;
     end;

  inc(z);
  if (z+strtrow>RowCount) then RowCount:=z+strtrow;
 end;
 fdelimiter:=olddelimiter;
 closefile(f);
end;

procedure TAdvStringGrid.LoadFromCSV(Filename:string);
begin
 InputFromCSV(Filename,false);
end;

procedure TAdvStringGrid.InsertFromCSV(Filename:string);
begin
 InputFromCSV(FileName,true);
end;

procedure TAdvStringGrid.SavetoStream(stream:tstream);
var
 ss:string;
 i,j:integer;

 procedure writestring(s:string);
 var
  buf:pchar;
  c:array[0..1] of char;
 begin
  getmem(buf,length(s)+1);
  strplcopy(buf,s,length(s));
  stream.writebuffer(buf^,length(s));
  c[0]:=#13;
  c[1]:=#10;
  stream.writebuffer(c,2);
  freemem(buf);
 end;

begin
  ss:=IntToStr(ColCount)+','+IntToStr(RowCount);
  Writestring(ss);

  for i:=0 to RowCount-1 do
  begin
    for j:=0 to ColCount-1 do
    begin
      if (Cells[j,i]<>'') then
      begin
        ss:=IntToStr(j)+','+IntToStr(i)+','+lftofile(Cells[j,i]);
        Writestring(ss);
      end;
    end;
  end;
end;

procedure TAdvStringGrid.LoadFromFixed(filename:string;positions:tintlist);
var
 f:textfile;
 s,sub:string;
 c,i:integer;
 strtcol,strtrow:integer;
begin
 AssignFile(f, FileName);
 {$i-}
 Reset(f);
 {$i+}
 if (IOResult<>0) then raise EAdvGridError.Create('File ' + FileName + ' not found');

 strtcol:=FixedCols;
 strtrow:=FixedRows;

 if fSaveFixedCells then
  begin
   strtcol:=0;
   strtrow:=0;
  end;

 self.Colcount:=FixedCols+positions.Count-1;

 while not eof(f) do
  begin
   readln(f,s);
   c:=strtcol;

   for i:=2 to positions.Count do
    begin
     sub:=copy(s,positions.items[i-2],positions.items[i-1]-positions.items[i-2]);
     cells[c,strtrow]:=trim(sub);
     inc(c);

    end;

   inc(strtrow);

   if (strtrow>=self.Rowcount) and not eof(f) then
     self.RowCount:=strtrow+1;
  end;

end;

procedure TAdvStringGrid.LoadFromStream(stream:tStream);
var
  X,Y:Integer;
  ss,ss1:string;

  function readstring(var s:string):integer;
  var
   c:char;
  begin
   c:='0';
   s:='';
   while (stream.position<stream.size) and (c<>#13) do
     begin
      stream.read(c,1);
      if (c<>#13) then s:=s+c;
     end;
   stream.read(c,1); {read the #10 newline marker}
   readstring:=length(s);
  end;

begin
  {allow to put other data before grid's data in the stream}
  {stream.position:=0;}

  if (stream.position<stream.size) then
   begin
    if (Readstring(ss)>0) then
      begin
       ss1:=Copy(ss,1,Pos(',',ss)-1);
       ColCount:=StrToInt(ss1);
       ss1:=Copy(ss,Pos(',',ss)+1,Length(ss));
       RowCount:=StrToInt(ss1);
      end;
   end;

  while (stream.position<stream.size) do
   begin
    ReadString(ss);
    ss1:=Copy(ss,1,Pos(',',ss)-1);
    ss:=Copy(ss,Pos(',',ss)+1,Length(ss));
    X:=StrToInt(ss1);
    ss1:=Copy(ss,1,Pos(',',ss)-1);
    ss:=Copy(ss, Pos(',',ss)+1,Length(ss));
    Y:=StrToInt(ss1);
    Cells[X,Y]:=filetolf(ss,fMultiLineCells);
  end;

end;

function TAdvStringGrid.ColumnSum(acol,fromrow,torow:integer):double;
var
 i,code:integer;
 sum,av:double;

begin
 sum:=0;

 for i:=fromrow to torow do
  begin
    val(cells[acol,i],av,code);
    if code=0 then sum:=sum+av;
  end;

 ColumnSum:=sum;
end;

function TAdvStringGrid.SelectedText:string;
var
 s,z:integer;
 ct,ts:string;
begin
 ts:='';
 {calculate required buffer space}
 for z:=selection.top to selection.bottom do
  begin
   for s:=selection.left to selection.right do
    begin
     ct:=cells[s,z];
     if (pos('{\',ct)>0) then
      begin
       celltorich(s,z,frichedit);
       ct:=frichedit.Text;
      end;
     if (linesintext(ct,fMultiLineCells)>1) and
        (fExcelClipboardformat) then LineFeedsToCSV(ct);
     if s<>selection.right then ts:=ts+ct+#9 else ts:=ts+ct;
    end;
    if z<>selection.bottom then ts:=ts+#13#10;
  end;
 result:=ts;
end;

function TAdvStringGrid.IsSelected(aCol,aRow:longint):boolean;
begin
 IsSelected:=(aCol>=Selection.Left) and
    (aCol<=Selection.Right) and
    (aRow>=Selection.Top) and
    (aRow<=Selection.Bottom);
end;

procedure TAdvStringGrid.AutoNumberCol(const ACol:longint);
var
 r:longint;
begin
 if rowcount=0 then exit;
 for r:=fixedrows to rowcount-1 do
  begin
   cells[ACol,r]:=inttostr(r-fixedrows+1);
  end;
end;

procedure TAdvStringGrid.AutoSizeCells(const DoFixedCells: Boolean; const PaddingX,PaddingY:integer);
var
 i,j,x,y:integer;
 textsize:tsize;
 pt:tpoint;

begin
 if DoFixedCells then
  begin
   x:=fixedcols;
   y:=fixedrows;
  end
 else
  begin
   x:=0;
   y:=0;
  end;

 BeginUpdate;

 for i:=x to ColCount-1 do colwidths[i]:=0;
 for j:=y to RowCount-1 do rowheights[j]:=0;

 for i:=x to ColCount-1 do
  for j:=y to RowCount-1 do
    begin
     if (i<fixedcols) or (j<fixedrows) then
       canvas.font.assign(self.fixedfont)
     else
       canvas.font.assign(self.font);

     if Assigned(FOnGetCellColor) then
       FOnGetCellColor(Self,j,i,[],Canvas.Brush,Canvas.Font);
     //canvas.font.size:=canvas.font.size+zoomfactor;

     pt:=CellGraphicSize[i,j];
     TextSize:=GetCellTextSize(remapcol(i),j);

     TextSize.cx:=TextSize.cx+pt.x+paddingx;
     TextSize.cy:=TextSize.cy+pt.y+paddingy;

     if (TextSize.cx>colwidths[i]) then colwidths[i]:=textsize.cx;
     if (TextSize.cy>rowheights[j]) then rowheights[j]:=textsize.cy;
   end;

 EndUpdate;
end;

procedure TAdvStringGrid.AutoSizeColumns(const DoFixedCols: Boolean; const Padding:integer);
var
   i: Integer;
begin
 if DoFixedCols then
    for i:=0 to FixedCols-1 do
    begin
         AutoSizeCol(i);
         if Padding <> 0 then
            ColWidths[i]:=ColWidths[i]+Padding;
    end;
 for i:=FixedCols to ColCount-1 do
 begin
      AutoSizeCol(i);
      if (Padding<>0) then
         ColWidths[i]:=ColWidths[i]+Padding;
 end;
end;

procedure TAdvStringGrid.SizeToWidth(const Acol:longint;inconly:boolean);
var
 MaxWidth, TextW, NewW, i: Integer;
begin
 MaxWidth:=0;

 for i:=0 to RowCount-1 do
  begin
   if (acol<fixedcols) or (i<fixedrows) then
     canvas.font.assign(self.fixedfont)
   else
     canvas.font.assign(self.font);

   if Assigned(FOnGetCellColor) then
     FOnGetCellColor(Self,i,ACol,[],Canvas.Brush,Canvas.Font);
   canvas.font.size:=canvas.font.size+zoomfactor;

   TextW:=GetCellTextSize(remapcol(acol),i).cx+CellGraphicSize[acol,i].x;

   if (TextW>MaxWidth) then MaxWidth:=TextW;

  end;
 {NewW:=MaxWidth+Canvas.TextWidth('xx');}

 {allow 2 pixel spacing at begin & end}
 NewW:=MaxWidth+2+2;

 if (inconly and (NewW>ColWidths[ACol])) or not inconly then ColWidths[ACol]:=NewW;

end;

procedure TAdvStringGrid.AutoSizeCol(const ACol:LongInt);
begin
 SizetoWidth(acol,false);
end;

procedure TAdvStringGrid.AutoSizeRows(const DoFixedRows: Boolean; const Padding:integer);
var
 i,j:integer;
begin
 if rowcount=0 then exit;

 if dofixedrows then j:=0 else j:=fixedrows;

 for i:=j to rowcount-1 do
  if wordwrap then AutoSizeRow(i) else self.SizeToLines(i,MaxLinesInRow(i),padding)
end;

procedure TAdvStringGrid.SizeToLines(const ARow,Lines,padding:longint);
var
 th:integer;
begin
 th:=canvas.textheight('gh');
 rowheights[arow]:=padding+((th + (th shr 2))*lines);
end;

procedure TAdvStringGrid.AutoSizeRow(const ARow:longint);
var
 i,j,k,mh,gh:integer;
 r:trect;
 {$IFNDEF WIN32}
 buf:array[0..255] of char;
 {$ENDIF}
 s:string;

begin
  j:=DefaultRowHeight;
  for i:=0 to colcount-1 do
   begin
    k:=remapcol(i);
    if (i<fixedcols) or (arow<fixedrows) then
     canvas.font.assign(self.fixedfont)
    else
     canvas.font.assign(self.font);

    if assigned(OnGetCellColor) then
      OnGetCellColor(self,arow,i,[],canvas.brush,canvas.font);

    r.left:=0;
    r.right:=colwidths[i]-4;
    r.top:=0;
    r.bottom:=defaultrowheight;

    {$IFNDEF WIN32}
    strpcopy(buf,cells[k,arow]);
    mh:=DrawText(Canvas.Handle,buf,StrLen(buf),R,(DT_EXPANDTABS or DT_NOPREFIX)
    or DT_WORDBREAK or fValign or DT_CALCRECT or DT_NOCLIP);
    {$ELSE}

    s:=cells[k,arow];
    if (pos('{\',s)>0) then
       begin
        celltorich(i,arow,richedit);
        s:=frichedit.text;

       end;
    mh:=DrawText(Canvas.Handle,pchar(s),Length(s),R,(DT_EXPANDTABS or DT_NOPREFIX)
    or DT_WORDBREAK or fValign or DT_CALCRECT or DT_NOCLIP);
    {$ENDIF}
    gh:=CellGraphicSize[k,arow].y;
    if (gh>mh) then mh:=gh;
    if (mh+2>j) then j:=mh+4;
   end;
 rowheights[arow]:=j;
end;

procedure TAdvStringGrid.SwapColumns(ACol1, ACol2: Longint);
var
 cw:integer;
begin
 colcount:=colcount+1+fNumHidden;
 cols[colcount-1]:=cols[acol1];
 cols[acol1]:=cols[acol2];
 cols[acol2]:=cols[colcount-1];
 colcount:=colcount-1-fNumHidden;
 cw:=colwidths[acol1];
 colwidths[acol1]:=colwidths[acol2];
 colwidths[acol2]:=cw;
 if sortcolumn=acol1 then sortcolumn:=acol2
 else
  if sortcolumn=acol2 then sortcolumn:=acol1;
end;

procedure TAdvStringGrid.SwapRows(ARow1, ARow2: Longint);
var
 rh:integer;
begin
 rowcount:=rowcount+1;
 rows[rowcount-1]:=rows[arow1];
 rows[arow1]:=rows[arow2];
 rows[arow2]:=rows[rowcount-1];
 rowcount:=rowcount-1;
 rh:=rowheights[arow1];
 rowheights[arow1]:=rowheights[arow2];
 rowheights[arow2]:=rh;
end;

procedure TAdvStringGrid.SortSwapRows(ARow1, ARow2: Longint);
var
 h1,h2:integer;
 rs:boolean;
begin
 inc(swaps);

 h1:=rowheights[arow1];
 h2:=rowheights[arow2];

 sortlist.assign(rows[arow1]);
 rows[arow1]:=rows[arow2];
 rows[arow2].assign(sortlist);

 {
 rows[rowcount-1]:=rows[arow1];
 rows[arow1]:=rows[arow2];
 rows[arow2]:=rows[rowcount-1];
 }

 if (h1<>h2) then
  begin
   rowheights[arow1]:=h2;
   rowheights[arow2]:=h1;
  end;

 if fMouseActions.fDisjunctRowSelect then
  begin
   rs:=RowSelect[arow1];
   RowSelect[arow1]:=RowSelect[arow2];
   RowSelect[arow2]:=rs;

  end;

 if arow1=sortrow then sortrow:=arow2
 else
  if arow2=sortrow then sortrow:=arow1;
end;

procedure TAdvStringGrid.SetPreviewPage(value:integer);
begin
 fPrintPageFrom:=value;
 fPrintPageTo:=value;
end;

procedure TAdvStringGrid.PrintPreview(canvas:tcanvas;displayrect:trect);
var
 gr:tgridrect;
begin
 gr.top:=0;
 gr.left:=0;
 gr.bottom:=self.rowcount-1;
 gr.right:=self.colcount-1;
 PrintPreviewRect(canvas,displayrect,gr);
end;

procedure TAdvStringGrid.PrintPreviewRect(canvas:tcanvas;displayrect:trect;gridrect:tgridrect);
var
 i:integer;
begin
 fprintrect:=gridrect;
 setmapmode(canvas.handle,mm_lometric); {everything in 0.1mm}
 prevrect:=displayrect;
 if not fFastPrint then
  begin
   i:=self.BuildPages(canvas,prCalcPreview,-1);
   fPrintPageNum:=i;
  end
 else i:=1;   
 self.BuildPages(canvas,prPreview,i);
end;

{---------------------------------------------------------
 v0.99 : modified to print only rows/columns in printrect
----------------------------------------------------------}
procedure TAdvStringGrid.Print;
var
 gr:tgridrect;
begin
 gr.top:=0;
 gr.left:=0;
 gr.bottom:=self.rowcount-1;
 gr.right:=self.colcount-1;
 self.PrintRect(gr);
end;

{---------------------------------------------------------
 v0.99 : added proc to print only rows/columns in printrect
----------------------------------------------------------}
procedure TAdvStringGrid.PrintRect(gridrect:tGridRect);
var
 i:integer;

begin
 fPrintRect:=gridrect;
 with Printer do
  begin
   orientation:=fPrintSettings.orientation;

   prevrect.top:=0;
   prevrect.left:=0;
   prevrect.right:=round(254/getdevicecaps(printer.handle,LOGPIXELSX)*(printer.pagewidth));
   prevrect.bottom:=round(254/getdevicecaps(printer.handle,LOGPIXELSY)*(printer.pageheight));

   if not fFastPrint then
     i:=self.BuildPages(self.canvas,prCalcPrint,-1)
   else i:=1;

   fPrintPageFrom:=1;
   fPrintPageTo:=i;
   fPrintPageNum:=i;

   if assigned(fOnPrintStart) then
    begin
     fOnPrintStart(self,i,fPrintPageFrom,fPrintPageTo);

     if (fPrintPageFrom=0) or (fPrintPageTo=0) or
        (fPrintPageTo<fPrintPageFrom) then exit;
    end;
   fPrintPageNum:=fPrintPageTo;

   title:=fPrintSettings.JobName;
   begindoc;
   setmapmode(canvas.handle,mm_lometric);

   self.BuildPages(canvas,prPrint,i);
   enddoc;
  end;
end;


function TAdvStringGrid.MaxLinesInGrid:integer;
var
 i,j,k,l:integer;

begin
 MaxLinesInGrid:=1;
 if colcount<=0 then exit;
 if rowcount<=0 then exit;
 if fmultilinecells=false then exit;
 l:=1;
 for i:=0 to colcount-1 do
  for j:=0 to rowcount-1 do
   begin
    k:=LinesInText(cells[remapcol(i),j],fMultiLineCells);
    if (k>l) then l:=k;
   end;
 MaxLinesInGrid:=l;
end;

function TAdvStringGrid.MaxCharsInCol(acol:integer):integer;
var
 i,k,res:integer;
 s,substr:string;

begin
 res:=0;
 for i:=0 to rowcount-1 do
  begin
   s:=cells[remapcol(acol),i];
   repeat
    substr:=GetNextLine(s,fMultiLineCells);
    k:=length(substr);
    if (k>res) then res:=k;
   until (s='');
  end;
 MaxCharsInCol:=res;
end;

{$IFDEF WIN32}
procedure PositionText(var aRect:TRect;hal:TAlignment;val:TVAlignment;aAngle,x1,x2,y1,y2:integer);
var
 Q:byte;
begin
 Q:=4;
 if aAngle<=90 then Q:=1
 else if aAngle<=180 then Q:=2
 else if aAngle<=270 then Q:=3;

 case Q of
 1:begin
   case hal of
   taLeftJustify:aRect.left:=aRect.left;
   taRightJustify:aRect.Left:=aRect.Right-x1-x2;
   taCenter:aRect.Left:=aRect.Left+((aRect.right-Arect.left-x2-x1) shr 1);
   end;
   case val of
   vtaTop:aRect.top:=aRect.top+y1;
   vtaCenter:aRect.top:=arect.top+((arect.bottom-arect.top-y2-y1) shr 1)+y1;
   vtaBottom:aRect.top:=aRect.bottom-y2;
   end;
   end;
 2:begin
   case hal of
   taLeftJustify:aRect.left:=aRect.left+x1;
   taRightJustify:aRect.Left:=aRect.Right-x2;
   taCenter:aRect.Left:=aRect.right-((aRect.right-Arect.left+x2-x1) shr 1);
   end;
   case val of
   vtaTop:aRect.top:=aRect.top+y1+y2;
   vtaCenter:aRect.top:=arect.top+((arect.bottom-arect.top-y2-y1) shr 1)+y1+y2;
   vtaBottom:aRect.top:=aRect.bottom;
   end;
   end;
 3:begin
   case hal of
   taLeftJustify:aRect.left:=aRect.left+x2+x1;
   taRightJustify:aRect.Left:=aRect.Right;
   taCenter:aRect.Left:=aRect.right-((aRect.right-Arect.left-x2-x1) shr 1);
   end;
   case val of
   vtaTop:aRect.top:=aRect.top+y2;
   vtaCenter:aRect.top:=arect.top+((arect.bottom-arect.top-y2-y1) shr 1)+y2;
   vtaBottom:aRect.top:=aRect.bottom-y1;
   end;
   end;
 4:begin
   case hal of
   taLeftJustify:aRect.left:=aRect.left+x2;
   taRightJustify:aRect.Left:=aRect.Right-x1;
   taCenter:aRect.Left:=aRect.Left+((aRect.right-Arect.left+x2-x1) shr 1);
   end;
   case val of
   vtaTop:aRect.top:=arect.top;
   vtaCenter:aRect.top:=aRect.top+((aRect.bottom-Arect.top-x2-x1) shr 1);
   vtaBottom:aRect.top:=aRect.bottom-y2-y1;
   end;
   end;
 end;
end;

procedure TAdvStringGrid.CalcTextPos(var aRect:TRect;aAngle:Integer;aTxt:String;tal:tAlignment);
var
 DC:HDC;
 hSaveFont:HFont;
 Size:TSize;
 y1,y2:Integer;
 x1,x2:Integer;
 {$IFNDEF WIN32}
 cStr:array[0..255] of Char;
 {$ENDIF}

begin
  DC:=GetDC(0);
  hSaveFont:=SelectObject(DC,Font.Handle);

  {$IFNDEF WIN32}
  StrPCopy(cStr,aTxt+'w');
  GetTextExtentPoint32(DC,cStr,Length(aTxt),Size);
  {$ELSE}
  GetTextExtentPoint32(DC,pchar(aTxt+'w'),Length(aTxt),Size);
  {$ENDIF}

  SelectObject(DC,hSaveFont);
  ReleaseDC(0,DC);

  aRect.right:=aRect.right-2;
  aRect.Bottom:=aRect.Bottom-2;

  x1:=Abs(Trunc(Size.cx * cos(aAngle*Pi/180)));
  x2:=Abs(Trunc(Size.cy * sin(aAngle*Pi/180)));

  y1:=Abs(Trunc(Size.cx * sin(aAngle*Pi/180)));
  y2:=Abs(Trunc(Size.cy * cos(aAngle*Pi/180)));

  PositionText(aRect,tal,valignment,aAngle,x1,x2,y1,y2);

  arect.left:=arect.left+2;
  arect.top:=arect.top+2;
end;
{$ENDIF}


function TAdvStringGrid.MaxLinesInRow(arow:integer):integer;
var
 i,k,l:integer;

begin
 MaxLinesInRow:=1;
 if (colcount<=0) then exit;
 if fmultilinecells=false then exit;

 l:=1;
 for i:=0 to colcount-fNumHidden do
   begin
    k:=LinesInText(cells[remapcol(i),arow],fMultiLineCells);
    if (k>l) then l:=k;
   end;
 MaxLinesInRow:=l;
end;

{---------------------------------------------------------
 Font mapping conversion routines :
 used round in previous version!
----------------------------------------------------------}
function TAdvStringGrid.MapFontHeight(pointsize:integer):integer;
begin
 MapFontHeight:=-trunc(pointsize*254/72*fontscalefactor);
end;

function TAdvStringGrid.MapFontSize(height:integer):integer;
begin
 MapFontSize:=trunc(height*72/254);
end;

procedure AsgPrintBitmap(Canvas:  TCanvas; DestRect:  TRect;  Bitmap:  TBitmap);
var
 BitmapHeader:  pBitmapInfo;
 BitmapImage :  POINTER;
 HeaderSize  :  DWORD;
 ImageSize   :  DWORD;
begin
 GetDIBSizes(Bitmap.Handle, HeaderSize, ImageSize);
 GetMem(BitmapHeader, HeaderSize);
 GetMem(BitmapImage,  ImageSize);
 try
   GetDIB(Bitmap.Handle, Bitmap.Palette, BitmapHeader^, BitmapImage^);
   StretchDIBits(Canvas.Handle,
                 DestRect.Left, DestRect.Top,     // Destination Origin
                 DestRect.Right  - DestRect.Left, // Destination Width
                 DestRect.Bottom - DestRect.Top,  // Destination Height
                 0, 0,                            // Source Origin
                 Bitmap.Width, Bitmap.Height,     // Source Width & Height
                 BitmapImage,
                 TBitmapInfo(BitmapHeader^),
                 DIB_RGB_COLORS,
                 SRCCOPY)
 finally
  FreeMem(BitmapHeader);
  FreeMem(BitmapImage)
 end;
end;

procedure PrintBitmapRop(Canvas:  TCanvas; DestRect:  TRect;  Bitmap:  TBitmap; rop:dword);
var
 BitmapHeader:  pBitmapInfo;
 BitmapImage :  POINTER;
 HeaderSize  :  DWORD;
 ImageSize   :  DWORD;
begin
  GetDIBSizes(Bitmap.Handle, HeaderSize, ImageSize);
  GetMem(BitmapHeader, HeaderSize);
  GetMem(BitmapImage,  ImageSize);
  try
   GetDIB(Bitmap.Handle, Bitmap.Palette, BitmapHeader^, BitmapImage^);
   StretchDIBits(Canvas.Handle,
                 DestRect.Left, DestRect.Top,     // Destination Origin
                 DestRect.Right  - DestRect.Left, // Destination Width
                 DestRect.Bottom - DestRect.Top,  // Destination Height
                 0, 0,                            // Source Origin
                 Bitmap.Width, Bitmap.Height,     // Source Width & Height
                 BitmapImage,
                 TBitmapInfo(BitmapHeader^),
                 DIB_RGB_COLORS,
                 ROP)
 finally
  FreeMem(BitmapHeader);
  FreeMem(BitmapImage)
 end;
end;


{---------------------------------------------------------
 Routine to build the page on the canvas
----------------------------------------------------------}
function TAdvStringGrid.BuildPages(canvas:tcanvas;printmethod:tPrintMethod;maxpages:integer):integer;
var
 i,j,m,tw,th,pagnum,pagcol,pagrow:integer;
 yposprint:integer;
 fntsize,lastrow:integer;
 hfntsize,ffntsize:integer;
 xsize,ysize:integer;
 spacing:integer;
 indent,topindent,footindent:integer;
 AlignValue:tAlignment;
 fntvspace,fnthspace,fntlineheight:word;
 oldfont,newfont:tfont;
 oldbrush,newbrush:tbrush;
 oldpen:tpen;
 totalwidth:integer;
 headersize:integer;
 footersize:integer;
 startcol,endcol:integer;
 spaceforcols,spaceforfixedcols:integer;
 orgsize:integer;
 repeatcols,multicol:boolean;
 forcednewpage:boolean;
 allowfittopage:boolean;
 scalefactor:double;
 angle:integer;
 resfactor,htmlfactor:double;
 prevendcol:integer;
 {$IFDEF WIN32}
 LFont:TLogFont;
 hOldFont,hNewFont:HFont;
 {$ENDIF}
 bmp:tbitmap;
 iconinfo:ticoninfo;
 hdc:thandle;
  {-------------------------------------------------
   Get x,y dimensions of grid cell in canvas mapmode
   -------------------------------------------------}
  function GetTextSize(col,row:integer):tsize;
  var
   s,su,anchor,stripped:string;
   maxsize,newsize,numlines,oldsize:integer;
   gt:tpoint;
   r:trect;
   htmlsize:tpoint;

  begin
   s:=Cells[Col,Row];

   if (pos('=',s)=1) then
    begin
     s:=CalcCell(col,row);
    end;

   {$IFDEF WIN32}
   if (pos('{\',s)=1) then
     begin
      celltorich(col,row,frichedit);
      s:=frichedit.Text;
     end;
   {$ENDIF}

   if (pos('</',s)>0) then
     begin
      fillchar(r,sizeof(r),0);
      r.right:=1000;
      r.bottom:=1000;

      oldsize:=canvas.font.size;
      setmapmode(canvas.handle,mm_text);
      canvas.font.size:=fprintsettings.font.size;
     
      HTMLDraw(canvas,s,r,gridimages,0,0,false,true,false,1.0,fURLColor,anchor,stripped,integer(htmlsize.x),integer(htmlsize.y));
      {correct it for the new mapping mode}

      {$IFDEF DEBUG}
      outputdebugstring(pchar('orig:'+inttostr(htmlsize.x)));
      {$ENDIF}
      setmapmode(canvas.handle,mm_lometric);
      dptolp(canvas.handle,htmlsize,1);

      {$IFDEF DEBUG}
      outputdebugstring(pchar('->'+inttostr(htmlsize.x)));
      {$ENDIF}
      canvas.font.size:=oldsize;
      result.cx:=htmlsize.x;
      result.cy:=-htmlsize.y;
      exit;
     end;

   if Assigned(FOnGetCellPrintColor) then
    begin
     oldfont.assign(canvas.font);
     newfont.assign(canvas.font);
     newfont.size:=orgsize;
     newfont.color:=clBlack;
     FOnGetCellPrintColor(Self,row,col,[],oldbrush,newfont);
     canvas.font.assign(newfont);
     canvas.font.Height:=MapFontHeight(newfont.size);
    end;

    {$IFDEF WIN32}
    if IsRotated(col,row,angle) then
     begin
      GetObject(canvas.Font.Handle,SizeOf(LFont),Addr(LFont));
      LFont.lfEscapement:=-Angle*10;
      LFont.lfOrientation:=-Angle*10;
      hNewFont:=CreateFontIndirect(LFont);
      hOldFont:=SelectObject(canvas.Handle,hNewFont);
     end;
    {$ENDIF}

    maxsize:=0;
    numlines:=0;

    {added for wordwrap feature v1.83}
    if wordwrap and fPrintSettings.NoAutoSize then
    begin
    r.top:=0;
    r.left:=0;
    r.right:=maxwidths[col];
    r.bottom:=50;
    r.bottom:=drawtext(canvas.handle,pchar(s),length(s),r,DT_CALCRECT or DT_WORDBREAK);
    result.cx := r.right;
    result.cy:= -r.bottom;
    end
    else
    begin
    {end of added for feature v1.83}
    if fPrintSettings.PrintGraphics then
     begin
      gt:=CellGraphicSize[col,row];
      gt.x:=round(gt.x*resfactor);
      gt.y:=round(gt.y*resfactor);
     end
    else gt:=point(0,0);

    repeat
      su:=GetNextLine(s,fMultiLineCells);
      newsize:=canvas.textwidth(su)+gt.x;
//      newsize:=canvas.textwidth(su);
      if (newsize>maxsize) then maxsize:=newsize;
      inc(numlines);
    until (s='');

    GetTextSize.cx:=maxsize;

    if (numlines*canvas.textheight('gh')<gt.y) then GetTextSize.cy:=gt.y
    else GetTextSize.cy:=numlines*canvas.textheight('gh');

//    GetTextSize.cy:=numlines*canvas.textheight('gh');
    end;
    {$IFDEF WIN32}
    if IsRotated(col,row,angle) then
     begin
      result.cx:=Abs(Trunc(result.cx * cos(Angle*Pi/180)))+
                 Abs(Trunc(result.cy * sin(Angle*Pi/180)));

      result.cy:=Abs(Trunc(maxsize * sin(Angle*Pi/180)))+
                 Abs(Trunc(result.cy * cos(Angle*Pi/180)));
      hNewFont := SelectObject(Canvas.Handle,hOldFont);
      DeleteObject(hNewFont);
     end;
    {$ENDIF}

   if Assigned(FOnGetCellPrintColor) then
    begin
     canvas.font.assign(oldfont);
    end;
  end;

  {-------------------------------------------------
   Calculate required column widths for all columns
   -------------------------------------------------}
  procedure CalculateWidths;
  var
   i,j:integer;
  begin

   for j:=fprintrect.left to fprintrect.right do maxwidths[j]:=0;
    for i:=fprintrect.top to fprintrect.bottom do
     begin
      for j:=fprintrect.left to fprintrect.right do
       begin
         if fPrintSettings.FUseFixedWidth then
           tw:=fPrintSettings.FFixedWidth
         else
           tw:=GetTextSize(remapcol(j),i).cx+fnthspace;

         if (tw>maxwidths[j]) then maxwidths[j]:=tw;
         {screen mapping!}
         {for wordwrapping support}
         if fPrintSettings.NoAutoSize then
         maxwidths[j]:=round((self.colwidths[j]-4)*resfactor);
        { end of wordwrapping support}
       end;
     end;

   end;

  {-------------------------------------------------
   Calculate required row height
   -------------------------------------------------}
  function GetRowHeight(arow:integer):integer;
  var
   j,nh,mh:integer;
  begin
   mh:=0;
   if fPrintSettings.UseFixedHeight then
     result:=fPrintSettings.FixedHeight
   else
    begin
     if assigned(FOnPrintSetRowHeight) then
      begin
       mh:=fPrintSettings.FixedHeight;
       OnPrintSetRowHeight(self,aRow,mh);
       result:=mh;
      end
     else
      begin
        for j:=fprintrect.left to fprintrect.right do
         begin
          nh:=GetTextSize(remapcol(j),arow).cy;
          if (nh>mh) then mh:=nh;
         end;
       result:=mh;
      end;
    end;
  end;

  {$IFDEF DELPHI3_LVL}
  function GetPrinterOrientation:integer;
  var
   Device : array[0..255] of char;
   Driver : array[0..255] of char;
   Port : array[0..255] of char;
   hDMode : THandle;
  begin
   {Printer.PrinterIndex := Printer.PrinterIndex; not required ?}
   Printer.GetPrinter(Device, Driver, Port, hDMode);
   result:=winspool.DeviceCapabilities(device,port,DC_ORIENTATION,nil,nil);
  end;
  {$ENDIF}


  {-------------------------------------------------
   Print a single row of the grid
   -------------------------------------------------}
  function BuildColumnsRow(ypos,col1,col2,arow,hght:integer):integer;
  var
   c,d:integer;
   cn:integer;
   th,thm,tb,yp,cr:integer;
   s,su,cs,anchor,stripped:string;
   tr,tp,rg,ir:trect;
   first:integer;
   x1,x2,y1,y2:integer;
   textpos:tpoint;
   Q:byte;
   cg:tcellgraphic;
   borders:TCellBorders;
   flg,swp:integer;
   checkstate:boolean;
   mm,xsize,ysize,oldsize:integer;

  begin
    fontscalefactor:=scalefactor;

    thm:=GetRowHeight(arow);

    thm:=thm+(thm shr 3);
    result:=thm+2*fntvspace;

    if (printmethod in [prCalcPreview,prCalcPrint]) then exit;

    {if (printmethod=prPreview) and (pagnum<>PreviewPage) then exit;}

    if ((pagnum+1)<fPrintPageFrom) or ((pagnum+1)>fPrintPageTo) then exit;

    for c:=col1 to col2 do
      begin
       cn:=remapcol(c);

       {if rtf then do RTFPaint()}
       tr.left:=indents[c];
       tr.right:=indents[c+1];
       tr.top:=ypos;
       tr.bottom:=ypos-thm-2*fntvspace;

       cs:=self.Cells[cn,arow];

       if (pos('=',cs)=1) then cs:=CalcCell(cn,arow);

       {$IFDEF WIN32}
       if (pos('{\',cs)=1) then RTFPaint(cn,arow,canvas,tr)
       else
        if (pos('</',cs)<>0) then
         begin
          lptodp(canvas.handle,tr.topleft,1);
          lptodp(canvas.handle,tr.bottomright,1);
          mm:=getmapmode(canvas.handle);
          oldsize:=canvas.font.size;
          setmapmode(canvas.handle,mm_text);
          canvas.font.size:=fprintsettings.font.size;
          HTMLDraw(canvas,cs,tr,gridimages,0,0,false,false,true,htmlfactor,fURLColor,anchor,stripped,xsize,ysize);
          setmapmode(canvas.handle,mm);
          canvas.font.size:=oldsize;
         end
       else
         begin
       {$ENDIF}

       if Assigned(FOnGetCellPrintColor) then
         begin
          oldbrush.assign(canvas.brush);
          oldfont.assign(canvas.font);
          newbrush.assign(canvas.brush);
          newfont.assign(canvas.font); {copy everything except size, which is mapped into mm_lometric}
          newfont.size:=orgsize;
          newfont.color:=clBlack;
          FOnGetCellPrintColor(self,arow,cn,[],newbrush,newfont);
          {added ...}
          canvas.brush.assign(newbrush);
          canvas.font.assign(newfont);
          canvas.font.Height:=MapFontHeight(newfont.size);
          {added ...}
          settextcolor(canvas.handle,newfont.color);
         end;

       AlignValue:=GetCellAlignment(cn,arow);

       th:=canvas.textheight('gh')+fPrintSettings.RowSpacing;

       first:=0;
       s:=cs;
       repeat
        su:=GetNextLine(s,fMultiLineCells);
        tb:=canvas.textwidth(su);
        if tb>first then first:=tb;
       until (s='');
       tb:=first;

       s:=cs;

       angle:=0;

       {$IFDEF WIN32}
       if IsRotated(cn,arow,angle) then
        begin
         GetObject(canvas.Font.Handle,SizeOf(LFont),Addr(LFont));
         if (PrintMethod=prPreview)
            or (not printerdriverfix)
            {$IFDEF DELPHI3_LVL}
            or (GetPrinterOrientation=270) {$ENDIF} then
          begin
           LFont.lfEscapement:=-Angle*10;
           LFont.lfOrientation:=-Angle*10;
          end
         else
          begin
           LFont.lfEscapement:=Angle*10;
           LFont.lfOrientation:=Angle*10;
          end;

         hNewFont:=CreateFontIndirect(LFont);
         hOldFont:=SelectObject(canvas.Handle,hNewFont);
        end;
       {$ENDIF}

       x1:=Abs(Trunc(tb * cos(Angle*Pi/180)));
       x2:=Abs(Trunc(th * sin(Angle*Pi/180)));
       y1:=Abs(Trunc(tb * sin(Angle*Pi/180)));
       y2:=Abs(Trunc(th * cos(Angle*Pi/180)));
       th:=y1+y2;

       first:=fntvspace;

       tr.left:=indents[c];
       tr.right:=indents[c+1];
       tr.top:=ypos;
       tr.bottom:=ypos-thm-2*fntvspace;

       {in progress : bitmap printing support}
       cg:=cellgraphics[c,arow];

       if (cg<>nil) and (fPrintSettings.PrintGraphics) then
        begin
         case cg.CellHalign of
         haBeforeText:
           begin
            rg.left:=tr.left+round(1*resfactor);
            rg.right:=rg.left+round(CellGraphicSize[c,arow].x*resfactor);
            tr.left:=rg.right;
           end;
         haLeft:
           begin
            cg.CellHalign:=haBeforeText;
            rg.left:=tr.left+round(1*resfactor);
            rg.right:=rg.left+round(CellGraphicSize[c,arow].x*resfactor);
            tr.left:=rg.right;
            cg.CellHalign:=haLeft;
           end;
         haAfterText:
           begin
            rg.right:=tr.right;
            rg.left:=rg.right-round(CellGraphicSize[c,arow].x*resfactor);
            tr.right:=rg.left;
           end;
         haRight:
           begin
            cg.CellHalign:=haBeforeText;
            rg.right:=tr.right;
            rg.left:=rg.right-round(CellGraphicSize[c,arow].x*resfactor);
            tr.right:=rg.left;
            cg.CellHAlign:=haRight;
           end;
         haCenter:
           begin
            cg.CellHAlign:=haBeforetext;
            rg.left:=tr.left+(((tr.right-tr.left)-round(CellGraphicSize[c,arow].x*resfactor) ) shr 1);
            rg.right:=rg.left+round(CellGraphicSize[c,arow].x*resfactor);
            cg.CellHAlign:=haCenter;
           end;
         end;

         case cg.CellValign of
         vaTop:begin
                rg.top:=tr.top;
                rg.bottom:=tr.top-round(CellGraphicSize[c,arow].y*resfactor);
               end;
         vaCenter:begin
                   rg.top:=tr.top-(((-tr.bottom+tr.top)-round(CellGraphicSize[c,arow].y*resfactor)) shr 1);
                   rg.bottom:=rg.top-round(CellGraphicSize[c,arow].y*resfactor);
                  end;
         vaBottom:begin
                   rg.top:=tr.bottom+round(CellGraphicSize[c,arow].y*resfactor);
                   rg.bottom:=tr.bottom;
                  end;
         vaUnderText:begin
                      rg.top:=tr.bottom+round(CellGraphicSize[c,arow].y*resfactor);
                      rg.bottom:=tr.bottom;
                      tr.bottom:=rg.top;
                     end;
         vaAboveText:begin
                      rg.top:=tr.top;
                      rg.bottom:=tr.top-round(CellGraphicSize[c,arow].y*resfactor);
                      tr.top:=rg.bottom;
                     end;
         end;

         case cg.celltype of
         ctCheckbox,ctDataCheckBox:begin
                     rg.bottom:=rg.top+round(CellGraphicSize[c,arow].y*resfactor);

                     {canvas.rectangle(rg.left,rg.top,rg.right,rg.bottom);}

                     GetCheckBoxState(c,arow,checkstate);
                     if checkstate then
                      DrawFrameControl(canvas.handle,rg,DFC_BUTTON,DFCS_BUTTONCHECK or DFCS_CHECKED)
                     else
                      DrawFrameControl(canvas.handle,rg,DFC_BUTTON,DFCS_BUTTONCHECK);
                     
                    end;
         ctBitmap:AsgPrintBitmap(canvas,rg,cg.CellBitmap);
         ctIcon:begin
                 geticoninfo(cg.CellIcon.handle,iconinfo);
                 bmp:=tbitmap.Create;
                 bmp.handle:=iconinfo.hbmmask;
                 PrintBitmapRop(canvas,rg,bmp,SRCCOPY);
                 bmp.free;

                 geticoninfo(cg.CellIcon.handle,iconinfo);
                 bmp:=tbitmap.Create;
                 bmp.handle:=iconinfo.hbmcolor;
                 PrintBitmapROP(canvas,rg,bmp,SRCINVERT);
                 bmp.free;

                end;
         ctImageList:begin
                      bmp:=tbitmap.Create;
                      fgridimages.GetBitmap(cg.CellIndex,bmp);
                      AsgPrintBitmap(canvas,rg,bmp);
                      bmp.free;
                     end;
         ctImages:begin
                      ir:=rg;
                      for d:=1 to CellImages[c,arow].Count do
                       begin
                        bmp:=tbitmap.Create;
                        fgridimages.GetBitmap(TIntList(cg.CellBitmap).Items[d-1],bmp);

                        if cg.cellboolean then
                         begin
                          ir.left:=rg.left+(d-1)*round(fgridimages.width*resfactor);
                          ir.right:=ir.left+round(fgridimages.width*resfactor);
                         end
                        else
                         begin
                          ir.top:=rg.top+(d-1)*round(fgridimages.height*resfactor);
                          ir.bottom:=ir.top+round(fgridimages.height*resfactor);
                         end;

                        AsgPrintBitmap(canvas,ir,bmp);
                        bmp.free;
                       end;
                  end;
         end;
        end;
       {end;
       end; { of bitmap printing support}

       {in progress : added for wordwrap feature}
       if wordwrap and fPrintSettings.noAutoSize then
       begin
       tr.left:=tr.left+round(2*resfactor);
       tr.right:=tr.left+maxwidths[c];
       flg:=DT_LEFT;
       case AlignValue of
       taLeftJustify:flg:=DT_LEFT;
       taRightJustify:flg:=DT_RIGHT;
       taCenter:flg:=DT_CENTER;
       end;

       drawtext(canvas.handle,pchar(s),length(s),tr,DT_WORDBREAK or DT_EDITCONTROL or DT_EXPANDTABS or DT_NOPREFIX or flg);
       {end of added for wordwrap feature}
       end
       else
       begin

       if LinesInText(s,fMultiLineCells)=1 then
        begin
         tp:=tr;
         su:=GetNextLine(s,fMultiLineCells);
         {
         tp.top:=ypos-first;
         tp.bottom:=ypos-thm-first;
         }
         tp.top:=tr.top;
         tp.bottom:=tr.bottom;
         {$IFDEF WIN32}
         PositionText(tp,AlignValue,fVAlignment,Angle,x1,x2,-y1,-y2);
         case AlignValue of
         taLeftJustify:tp.left:=tp.left+fnthspace;
         taRightJustify:tp.left:=tp.left-fnthspace;
         end;
         {$ElSE}
         case AlignValue of
         taLeftJustify:tp.left:=tp.left+fnthspace;
         taRightJustify:tp.left:=tp.right-fnthspace-canvas.textwidth(su);
         taCenter:tp.left:=tp.left+( (tp.right-tp.left-canvas.textwidth(su)) shr 1);
         end;
         {$ENDIF}

         canvas.textrect(tr,tp.left,tp.top,su);
        end
       else
         repeat
          tp:=tr;
          su:=GetNextLine(s,fMultiLineCells);
          {PositionText(tp,AlignValue,fVAlignment,Angle,x1,x2,-y1,-y2);}
          case AlignValue of
          taLeftJustify:tp.left:=tp.left+fnthspace;
          taRightJustify:tp.left:=tp.right-fnthspace-canvas.textwidth(su);
          taCenter:tp.left:=tp.left+( (tp.right-tp.left-canvas.textwidth(su)) shr 1);
          end;

          canvas.textrect(tr,tp.left,tr.top-first,su);

          if first>0 then
            begin
             tr.top:=tr.top-first;
             first:=0;
            end;
          tr.top:=tr.top-(th+(th shr 3));
         until (s='');
        end;
       {$IFDEF WIN32}
       if IsRotated(cn,arow,angle) then
        begin
         hNewFont := SelectObject(Canvas.Handle,hOldFont);
         DeleteObject(hNewFont);
        end;

       if Assigned(FOnGetCellPrintColor) then
        begin
         canvas.font.assign(oldfont);
         canvas.brush.assign(oldbrush);
        end;
       end;
       {$ENDIF}

       if assigned(fOnGetCellPrintBorder) then
        begin
         oldpen.assign(canvas.pen);
         borders:=[];
         fOnGetCellPrintBorder(self,arow,cn,canvas.pen,borders);

         tr.left:=indents[c];
         tr.right:=indents[c+1]-(canvas.pen.width shr 1)-1;
         tr.top:=ypos;
         tr.bottom:=ypos-thm-2*fntvspace+(canvas.pen.width shr 1)-1;

         if cbLeft in borders then
           begin
            canvas.moveto(tr.left,tr.top);
            canvas.lineto(tr.left,tr.bottom);
           end;
         if cbRight in borders then
           begin
            canvas.moveto(tr.right,tr.top);
            canvas.lineto(tr.right,tr.bottom);
           end;
         if cbTop in borders then
           begin
            canvas.moveto(tr.left,tr.top);
            canvas.lineto(tr.right,tr.top);
           end;
         if cbBottom in borders then
           begin
            canvas.moveto(tr.left,tr.bottom);
            canvas.lineto(tr.right,tr.bottom);
           end;
         canvas.pen.assign(oldpen);
        end;
     end;

     if (fprintsettings.fborders in [pbSingle,pbDouble,pbHorizontal,pbAroundHorizontal]) then
      begin
       canvas.moveto(indents[col1],ypos);
       canvas.lineto(indents[col2+1],ypos);
      end;
     fontscalefactor:=1.0;
   end;

   function BuildHeader:integer;
   var
     tl,tr,tc,bl,br,bc:string;
     i,j:integer;

    function PagNumStr:string;
    begin
     if multicol then
      result:=inttostr(pagrow+1)+'-'+inttostr(pagcol)
     else
      begin
       if (fPrintSettings.PageNumSep<>'') and (maxpages>1) then
         result:=inttostr(pagnum+1)+fPrintSettings.PageNumSep+inttostr(maxpages)
       else
         result:=inttostr(pagnum+1);
      end;
    end;

   begin
     result:=0;
     if ((pagnum+1)<fPrintPageFrom) or ((pagnum+1)>fPrintPageTo) then exit;
     if (PrintMethod in [prCalcPreview,prCalcPrint]) then exit;

{     if ((PrintMethod=prPreview) and (pagnum<>PreviewPage)) or
         (PrintMethod in [prCalcPreview,prCalcPrint]) then exit; }

     tl:='';
     tr:='';
     tc:='';
     bl:='';
     br:='';
     bc:='';
     with fPrintSettings do
     begin
     case fTime of
     ppTopLeft:tl:=formatdatetime('hh:nn',now)+' '+tl;
     ppTopRight:tr:=tr+' '+formatdatetime('hh:nn',now);
     ppTopCenter:tc:=tc+' '+formatdatetime('hh:nn',now);
     ppBottomLeft:bl:=formatdatetime('hh:nn',now)+' '+bl;
     ppBottomRight:br:=br+' '+formatdatetime('hh:nn',now);
     ppBottomCenter:bc:=bc+' '+formatdatetime('hh:nn',now);
     end;
     case fDate of
     ppTopLeft:tl:=formatdatetime(fDateFormat,now)+' '+tl;
     ppTopRight:tr:=tr+' '+formatdatetime(fDateFormat,now);
     ppTopCenter:tc:=tc+' '+formatdatetime(fDateFormat,now);
     ppBottomLeft:bl:=formatdatetime(fDateFormat,now)+' '+bl;
     ppBottomRight:br:=br+' '+formatdatetime(fDateFormat,now);
     ppBottomCenter:bc:=bc+' '+formatdatetime(fDateFormat,now);
     end;
     case fPageNr of
     ppTopLeft:tl:=fPagePrefix+' '+PagNumStr+' '+fPageSuffix+' '+tl;
     ppTopRight:tr:=tr+' '+fPagePrefix+' '+PagNumStr+' '+fPageSuffix;
     ppTopCenter:tc:=tc+' '+fPagePrefix+' '+PagNumStr+' '+fPageSuffix;
     ppBottomLeft:bl:=fPagePrefix+' '+PagNumStr+' '+fPageSuffix+' '+bl;
     ppBottomRight:br:=br+' '+fPagePrefix+' '+PagNumStr+' '+fPageSuffix;
     ppBottomCenter:bc:=bc+' '+fPagePrefix+' '+PagNumStr+' '+fPageSuffix;
     end;

     if (fTitleText<>'') then
      begin
       case fTitle of
       ppTopLeft:tl:=fTitleText+' '+tl;
       ppTopRight:tr:=tr+' '+fTitleText;
       ppTopCenter:tc:=tc+' '+fTitleText;
       ppBottomLeft:bl:=fTitleText+' '+bl;
       ppBottomRight:br:=br+' '+fTitleText;
       ppBottomCenter:bc:=bc+' '+fTitleText;
       end;
      end
     else
      begin
      if (fTitleLines.Count>0) then
        case fTitle of
        ppTopLeft:tl:=fTitleLines.Strings[0]+' '+tl;
        ppTopRight:tr:=tr+' '+fTitleLines.Strings[0];
        ppTopCenter:tc:=tc+' '+fTitleLines.Strings[0];
        ppBottomLeft:bl:=fTitleLines.Strings[0]+' '+bl;
        ppBottomRight:br:=br+' '+fTitleLines.Strings[0];
        ppBottomCenter:bc:=bc+' '+fTitleLines.Strings[0];
        end;
      end;
     end;

     {$IFDEF FREEWARE}
     if (PrintMethod=prPrint) then
     bc:=bc+' printed with demo version of '+self.classname;
     {$ENDIF}

     oldfont.assign(canvas.font);
     canvas.font.assign(fPrintSettings.HeaderFont);
     canvas.font.Height:=MapFontHeight(canvas.font.size); {map into mm_lometric space}

     if (tl<>'') then canvas.textout(indent,-headersize,tl);
     if (tr<>'') then canvas.textout(xsize-canvas.textwidth(tr)-20-fprintsettings.RightSize ,-headersize,tr);
     if (tc<>'') then canvas.textout((xsize-canvas.textwidth(tc)) shr 1,-headersize,tc);

     if (fPrintSettings.fTitle in [ppTopLeft,ppTopRight,ppTopCenter]) and (fPrintSettings.fTitleLines.Count>1) then
      begin
       j:=0;
       for i:=2 to fPrintSettings.fTitleLines.Count do
       begin
        j:=j+canvas.textheight('gh');
        tc:=fPrintSettings.TitleLines[i-1];
        case fPrintSettings.fTitle of
        ppTopLeft:canvas.textout(indent,-headersize-j,tc);
        ppTopRight:canvas.textout(xsize-canvas.textwidth(tc)-20-fprintsettings.RightSize ,-headersize-j,tc);
        ppTopCenter:canvas.textout((xsize-canvas.textwidth(tc)) shr 1,-headersize-j,tc);
        end;
       end;
      end;

     canvas.font.assign(fPrintSettings.FooterFont);
     canvas.font.Height:=MapFontHeight(canvas.font.size); {map into mm_lometric space}

     if (bl<>'') then canvas.textout(indent,ysize+ffntsize+fntvspace+footersize,bl);
     if (br<>'') then canvas.textout(xsize-canvas.textwidth(br)-20-fprintsettings.RightSize ,ysize+ffntsize+fntvspace+footersize,br);
     if (bc<>'') then canvas.textout((xsize-canvas.textwidth(bc)) shr 1,ysize+ffntsize+fntvspace+footersize,bc);

     if (fPrintSettings.fTitle in [ppBottomLeft,ppBottomRight,ppBottomCenter]) and (fPrintSettings.fTitleLines.Count>1) then
      begin
       j:=0;
       for i:=2 to fPrintSettings.fTitleLines.Count do
       begin
        j:=j+canvas.textheight('gh');
        tc:=fPrintSettings.TitleLines[i-1];
        case fPrintSettings.fTitle of
        ppBottomLeft:canvas.textout(indent,ysize+ffntsize+fntvspace+footersize-j,tc);
        ppBottomRight:canvas.textout(xsize-canvas.textwidth(tc)-20-fprintsettings.RightSize ,ysize+ffntsize+fntvspace+footersize-j,tc);
        ppBottomCenter:canvas.textout((xsize-canvas.textwidth(tc)) shr 1,ysize+ffntsize+fntvspace+footersize-j,tc);
        end;
       end;
      end;


     canvas.font.assign(oldfont);
   end;

   procedure DrawBorderAround(startcol,endcol,yposprint:integer);
   var
    k:integer;
   begin
     if ((pagnum+1)<fPrintPageFrom) or ((pagnum+1)>fPrintPageTo) then exit;
     if (PrintMethod in [prCalcPreview,prCalcPrint]) then exit;

{     if ((PrintMethod=prPreview) and (pagnum<>PreviewPage)) or
         (PrintMethod in [prCalcPreview,prCalcPrint]) then exit;}

     if (fPrintSettings.fBorders in [pbAround,pbAroundVertical,pbAroundHorizontal]) then
      begin
       if repeatcols then k:=0 else k:=startcol;
       canvas.moveto(indents[k],-topindent-fPrintSettings.TitleSpacing);
       canvas.lineto(indents[endcol+1],-topindent-fPrintSettings.TitleSpacing);
       canvas.lineto(indents[endcol+1],yposprint+(fntvspace shr 1));
       canvas.lineto(indents[k],yposprint+(fntvspace shr 1));
       canvas.lineto(indents[k],-topindent-fPrintSettings.TitleSpacing);
      end;

     if (fprintsettings.fborders in [pbSingle,pbDouble,pbHorizontal,pbAroundHorizontal]) then
      begin
       if repeatcols then
         canvas.moveto(indents[0],yposprint+(fntvspace shr 1))
       else
         canvas.moveto(indents[startcol],yposprint+(fntvspace shr 1));

       canvas.lineto(indents[endcol+1],yposprint+(fntvspace shr 1));
      end;

     if (fprintsettings.fborders in [pbSingle,pbDouble,pbVertical,pbAroundVertical]) then
      begin
       {draw here the vertical columns too}
       if repeatcols then
        begin
         for k:=0 to FixedCols-1 do
          begin
           canvas.moveto(indents[k],-topindent-fPrintSettings.TitleSpacing);
           canvas.lineto(indents[k],yposprint+(fntvspace shr 1));
          end;
        end;

       for k:=startcol to endcol+1 do
        begin
         canvas.moveto(indents[k],-topindent-fPrintSettings.TitleSpacing);
         canvas.lineto(indents[k],yposprint+(fntvspace shr 1));
        end;
      end;
   end;

   procedure StartNewPage;
   begin
    if ((pagnum+2)<fPrintPageFrom) or ((pagnum+2)>fPrintPageTo) then exit;

    if (PrintMethod=prPrint) then Printer.newpage;


    setmapmode(canvas.handle,MM_LOMETRIC);

    if (PrintMethod in [prPrint,prPreview]) then
    if assigned(FOnPrintPage) then
     begin
      FOnPrintPage(self,canvas,pagnum+1,xsize,ysize);
     end;
   end;

begin
  setmapmode(canvas.handle,MM_TEXT);
  try
    hdc:=GetDC(self.Handle);
    htmlfactor:=getdevicecaps(canvas.handle,LOGPIXELSX)/getdevicecaps(hdc,LOGPIXELSX);
    releasedc(self.handle,hdc);
  except
    htmlfactor:=0.0;
  end;

  setmapmode(canvas.handle,MM_LOMETRIC);
  fontscalefactor:=1.0;

  newfont:=tfont.create;
  oldfont:=tfont.create;
  oldpen:=tpen.create;
  newbrush:=tbrush.create;
  oldbrush:=tbrush.create;
  canvas.pen.color:=clBlack;
  canvas.pen.style:=fPrintSettings.fBorderStyle;

  if fPrintSettings.fBorders=pbDouble then canvas.pen.width:=10 else canvas.pen.width:=2;

  canvas.font:=fPrintSettings.HeaderFont;
  canvas.font.Height:=MapFontHeight(canvas.font.size);

  hfntsize:=canvas.TextHeight('gh');

  canvas.font:=fPrintSettings.FooterFont;
  canvas.font.Height:=MapFontHeight(canvas.font.size);
  ffntsize:=canvas.TextHeight('gh');

  canvas.font:=fPrintSettings.Font;
  orgsize:=canvas.font.size;
  {$IFDEF DEBUG}
  outputdebugstr('fontsize '+inttostr(self.font.size));
  {$ENDIF}
  canvas.font.Height:=MapFontHeight(canvas.font.size); {map into mm_lometric space}

  {$IFDEF DEBUG}
  outputdebugstr('fontheight '+inttostr(canvas.font.height));
  {$ENDIF}

  fntlineheight:=canvas.TextHeight('gh');

  fntsize:=fntlineheight*MaxLinesInGrid;

  fntvspace:=fntsize shr 3;
  fnthspace:=canvas.textwidth('a'); {adapt for multiline}

  case PrintMethod of
  prPrint:begin
           xsize:=canvas.cliprect.right-canvas.cliprect.left;
           ysize:=canvas.cliprect.bottom;
          end;
  prCalcPrint:begin
           xsize:=prevrect.right-prevrect.left;
           ysize:=-prevrect.bottom-prevrect.top;
         end;
  prPreview,prCalcPreview:begin
             xsize:=round(254/getdevicecaps(canvas.handle,LOGPIXELSX)*(prevrect.right-prevrect.Left));
             ysize:=-round(254/getdevicecaps(canvas.handle,LOGPIXELSY)*(prevrect.bottom-prevrect.top));
            end;
  end;

  resfactor:= getdevicecaps(canvas.handle,LOGPIXELSX)/getdevicecaps(self.canvas.handle,LOGPIXELSX)*(254/getdevicecaps(canvas.handle,LOGPIXELSX));



  {$IFDEF DEBUG}
  outputdebugstr('factor = '+floattostr(resfactor));
  {$ENDIF}

  fPrintPageRect:=rect(0,0,xsize,ysize);

  indent:=fPrintsettings.fLeftSize;

  if fPrintSettings.FUseFixedWidth then
   spacing:=0
  else
   spacing:=fPrintSettings.ColumnSpacing+20; {min. 2mm space}

  CalculateWidths;

  {total space req. for columns}
  totalwidth:=0;
  for j:=fprintrect.left to fprintrect.right do
   begin
    totalwidth:=totalwidth+maxwidths[j]+spacing;
   end;

   {$IFDEF DEBUG}
   outputdebugstr('tot width : '+inttostr(totalwidth)+' - size : '+inttostr(xsize));
   {$ENDIF}

   scalefactor:=1.0;
   {$IFDEF WIN32}
   if (fPrintSettings.fFitToPage<>fpNever) then
    begin
     tw:=(fprintrect.right-fprintrect.Left+1)*spacing;
     scalefactor:=(xsize-fPrintSettings.fRightSize-fPrintSettings.fLeftSize-tw)/(totalwidth-tw);

     if (scalefactor>1.0) and (fPrintSettings.fFitToPage=fpShrink) then fontscalefactor:=1.0;
     if (scalefactor<1.0) and (fPrintSettings.fFitToPage=fpGrow) then fontscalefactor:=1.0;

     if (scalefactor<>1.0) and (fPrintSettings.fFitToPage=fpCustom) then
       begin
        allowfittopage:=true;
        if assigned(OnFitToPage) then
          OnFitToPage(self,scalefactor,allowfittopage);
        if not allowfittopage then scalefactor:=1.0;
       end;

     if (scalefactor<>1.0) then
      begin
       for j:=fprintrect.left to fprintrect.right do
        maxwidths[j]:=trunc(maxwidths[j]*scalefactor);
        totalwidth:=0;
        {recalculate total req. width}
        for j:=fprintrect.left to fprintrect.right do
         begin
          totalwidth:=totalwidth+maxwidths[j]+spacing;
         end;
      end;
    end;

    if assigned(FOnPrintSetColumnWidth) then
     begin
      for j:=fprintrect.left to fprintrect.right do
       begin
        FOnPrintSetColumnWidth(self,j,maxwidths[j]);
       end;
      totalwidth:=0;
      for j:=fprintrect.left to fprintrect.right do
       begin
       totalwidth:=totalwidth+maxwidths[j]+spacing;
       end;
     end;
    {$ENDIF}
   startcol:=fprintrect.left;
   endcol:=fprintrect.left;
   pagnum:=0; {page counter}
   pagcol:=0;

   lastrow:=-1;

   {calculate the size of the fixed columns}
   multicol:=false;

   while (endcol<=fprintrect.right) do
    begin
      {calculate new endcol here}
      spaceforfixedcols:=0;

      prevendcol:=endcol;

      pagrow:=0;
      inc(pagcol);

      {added fixed spaceforcols here if repeatfixedcols is set}

      repeatcols:= (endcol>fprintrect.left) and (FixedCols>0) and
                  (fPrintSettings.fRepeatFixedCols);

      if repeatcols then
       begin
        for m:=0 to FixedCols-1 do spaceforfixedcols:=spaceforfixedcols+maxwidths[m]+spacing;
       end;

      spaceforcols:=spaceforfixedcols;

      while (spaceforcols<=xsize-fPrintSettings.fRightSize) and (endcol<=fprintrect.right) do
       begin
        spaceforcols:=spaceforcols+maxwidths[endcol]+spacing;
        if (spaceforcols<=xsize-fPrintSettings.fRightSize) then inc(endcol);
       end;

      {space for cols is the width of the printout}
      if (endcol>fprintrect.right) then endcol:=fprintrect.right;

      if not (spaceforcols<=xsize-fPrintSettings.fRightSize) then
         begin
          spaceforcols:=spaceforcols-maxwidths[endcol]-spacing;
          dec(endcol);
         end;

      if endcol<=prevendcol then endcol:=prevendcol+1;


      multicol:= multicol or (endcol<fprintrect.right);

      fPrintPageWidth:=spaceforcols;

      if fPrintSettings.fCentered then
       begin
        indents[startcol]:=0;
        indents[0]:=0;
       end
      else
       begin
        indents[startcol]:=indent;
        indents[0]:=indent;
       end;

      for j:=startcol+1 to endcol do
       begin
        indents[j]:=indents[j-1]+maxwidths[j-1]+spacing;
       end;

      fPrintColStart:=startcol;
      fPrintColEnd:=endcol;

      indents[endcol+1]:=indents[endcol]+maxwidths[endcol]+spacing;

      if repeatcols then
        begin
         for m:=1 to FixedCols-1 do
          begin
           indents[m]:=indents[m-1]+maxwidths[m-1]+spacing;
          end;
        end;

       if (fPrintSettings.fCentered) and (spaceforcols<xsize) then
        begin
         spaceforcols:=(xsize-spaceforcols) shr 1;
         for j:=startcol to endcol+1 do
          begin
           indents[j]:=indents[j]+spaceforcols;
          end;

         {add centering space to repeated columns}
         if repeatcols then
          for m:=0 to fixedcols do
           begin
            indents[m]:=indents[m]+spaceforcols;
           end;
        end;

       {add spacing if required for repeat fixed columns}
       for j:=startcol to endcol+1 do
        begin
         indents[j]:=indents[j]+spaceforfixedcols;
        end;

       {fixed columns}
       //np:=(-ysize) div (fntsize+fntvspace);  {nr. of lines per page}
       //np:=1+((fprintrect.bottom-fprintrect.top+1) div np);   {nr. of pages}

       j:=0;
       yposprint:=-topindent;
       topindent:=0; {reserve a line for header}

       headersize:=fPrintSettings.FHeaderSize;
       footersize:=fPrintSettings.FFooterSize;

       if (fPrintSettings.fTime in [ppTopLeft,ppTopCenter,ppTopRight]) or
          (fPrintSettings.fDate in [ppTopLeft,ppTopCenter,ppTopRight]) or
          (fPrintSettings.fPageNr in [ppTopLeft,ppTopCenter,ppTopRight]) or
          (fPrintSettings.fTitle in [ppTopLeft,ppTopCenter,ppTopRight]) then
        begin
         topindent:=1*(hfntsize+fntvspace)+headersize;
         if fPrintSettings.fTitleLines.Count>0 then
          topindent:=topindent+hfntsize*(fPrintSettings.fTitleLines.Count-1);
        end
       else
        begin
         topindent:=headersize;
        end;

       if (fPrintSettings.fTime in [ppBottomLeft,ppBottomCenter,ppBottomRight]) or
          (fPrintSettings.fDate in [ppBottomLeft,ppBottomCenter,ppBottomRight]) or
          (fPrintSettings.fPageNr in [ppBottomLeft,ppBottomCenter,ppBottomRight]) or
          (fPrintSettings.fTitle in [ppBottomLeft,ppBottomCenter,ppBottomRight]) then
        begin
         footindent:=2*(ffntsize+fntvspace)+footersize;
         if fPrintSettings.fTitleLines.Count>0 then
          footindent:=footindent+ffntsize*(fPrintSettings.fTitleLines.Count-1);
        end
       else
         footindent:=1*(ffntsize+fntvspace)+footersize;

       i:=fprintrect.top;

       if (PrintMethod in [prPrint,prPreview]) then
       if assigned(FOnPrintPage) then
        begin
          FOnPrintPage(self,canvas,1,xsize,ysize);
        end;

       {print all rows here}
       while (i<fprintrect.bottom+1) do
        begin
         {at start of page.. print header}
         if (j=0) then
            begin
             yposprint:=-topindent;
             BuildHeader;
             yposprint:=yposprint-fPrintSettings.TitleSpacing;
            end;

         if (j=0) and (i>0) and
            ((pagnum>0) or (fprintrect.top>=fixedrows)) and
            (fPrintSettings.fRepeatFixedRows) and
            (fixedrows>0) then
          begin
           {here headers are reprinted}
           for m:=0 to FixedRows-1 do
            begin
             {if necessary, reprint fixed columns}
             if (repeatcols) then
              begin
               BuildColumnsRow(yposprint,0,FixedCols-1,m,0);
              end;

             {print columns}
             th:=BuildColumnsRow(yposprint,startcol,endcol,m,0);
             yposprint:=yposprint-th;

            end;
           inc(j);
          end;

         if repeatcols then
          begin
           BuildColumnsRow(yposprint,0,FixedCols-1,i,0);
          end;

         th:=BuildColumnsRow(yposprint,startcol,endcol,i,0);
         yposprint:=yposprint-th;

         inc(i);
         inc(j);

         forcednewpage:=false;

         if assigned(FOnPrintNewPage) then
           begin
            FOnPrintNewPage(self,i,forcednewpage);
           end;

         if ((yposprint-GetRowHeight(j)<ysize+footindent) and
            (i<fPrintRect.bottom+1)) or
            (forcednewpage) then
           begin
            DrawBorderAround(startcol,endcol,yposprint);
            j:=0;
            lastrow:=i;
            StartNewPage;
            inc(pagnum);
            inc(pagrow);
           end;
        end;

      {
     if (pagnum=pagprev) or (PrintMethod=prPrint) then
      begin
       if (lastrow=-1) or ((i<>lastrow) and not (PrintMethod=prPreview)) then
          DrawBorderAround(startcol,endcol,yposprint);
      end;
      }

     if (lastrow=-1) or (i<>lastrow) then
          DrawBorderAround(startcol,endcol,yposprint);

     startcol:=endcol+1;
     endcol:=startcol;
     if (endcol<=fprintrect.Right) then StartNewPage;
     inc(pagnum);

  end; {end of while endcol<fprintrect.right}

 {free temporary font and brush objects}
 newfont.free;
 oldfont.free;
 oldpen.free;
 newbrush.free;
 oldbrush.free;
 result:=pagnum;
end;


procedure TAdvStringGrid.InvalidateGridRect(r:tgridrect);
var
 gdr1,gdr2,gdr3:trect;
begin
 gdr1:=cellrect(selection.left,selection.top);
 gdr2:=cellrect(selection.right,selection.bottom);
 unionrect(gdr3,gdr1,gdr2);
 invalidaterect(self.handle,@gdr3,false);
end;

procedure TAdvStringGrid.MouseMove(Shift: TShiftState; X, Y: Integer);
var
 acol,arow,mrg,i:longint;
 nondef:boolean;
 s,anchor,stripped:string;
 r:trect;
 pt:tpoint;
 xsize,ysize:integer;
 cc,rc:integer;

begin
 nondef:=false;
 self.mousetocell(x,y,acol,arow);

 {$IFDEF DEBUG}
 outputdebugstr('x='+inttostr(x)+':y='+inttostr(y));
 {$ENDIF}

 {mark selection}

 if not (csDesigning in Componentstate) then
 begin
  {$IFDEF WIN32}
  if (FGridState=gsColSizing) then
    begin
      colsized:=true;

      if (colclicked=fixedrows) and (fMouseActions.fAllColumnSize) then
       begin
        cc:=colclicked;
        rc:=rowclicked;
        r:=cellrect(colclicked,rowclicked);
        xsize:=x-r.left-clickposdx;
        for i:=fixedcols to colcount-1 do colwidths[i]:=xsize;
        clickposdx:=0;
        colclicked:=cc;
        rowclicked:=rc;
      end;
    end;

  if (FGridState in [gsColMoving,gsRowMoving]) and
     ( (clickposx<>x) or (clickposy<>y)) and (fEnhRowColMove) then
   begin
    lockwindowupdate(0);

    if (fGridState=gsColMoving) and (movecell>=fixedcols) then
     begin
      movebutton.text:=cells[movecell,0];
      movebutton.width:=colwidths[movecell];
      movebutton.height:=rowheights[0];
     end;

    if (fGridState=gsRowMoving) and (movecell>=fixedrows) then
     begin
      movebutton.text:=cells[0,movecell];
      movebutton.height:=rowheights[movecell];
      movebutton.width:=colwidths[0];
     end;

    movebutton.left:=x-moveofsx;
    movebutton.top:=y-moveofsy;

    {this is new 1.83 functionality }

    {$IFDEF DELPHI3_LVL}
    if (fGridState=gsColMoving) then
     begin
      if (fmovecolind>=fixedcols) then
       begin
        r:=cellrect(fmovecolind,0);
        r.topleft:=clienttoscreen(r.topleft);
        r.bottomright:=clienttoscreen(r.bottomright);

        arwU.top:=r.bottom;
        arwU.left:=r.left-5;
        arwD.top:=r.top-8;
        arwD.left:=r.left-5;
       end;
      arwD.visible:=(fmovecolind>=fixedcols) and (fmovecolind-leftcol<visiblecolcount);
      arwU.visible:=(fmovecolind>=fixedcols) and (fmovecolind-leftcol<visiblecolcount);
      fmovecolind:=acol;
      fmoverowind:=arow;
     end;

    if (fGridState=gsRowMoving) then
     begin
      if (fmoverowind>=fixedrows) and (fmoverowind-toprow<visiblerowcount) then
       begin
        r:=cellrect(0,fmoverowind);
        r.topleft:=clienttoscreen(r.topleft);
        r.bottomright:=clienttoscreen(r.bottomright);
        arwL.top:=r.bottom-5;
        arwL.left:=r.left-10;
        arwR.top:=r.bottom-5;
        arwR.left:=r.right;
       end;
      arwR.visible:=(fmoverowind>=fixedrows) and (fmoverowind-toprow<visiblerowcount) ;
      arwL.visible:=(fmoverowind>=fixedrows) and (fmoverowind-toprow<visiblerowcount) ;
      fmovecolind:=acol;
      fmoverowind:=arow;
     end;

    {$ENDIF}

    {this is end of new 1.83 functionality }

    if (fGridState=gsColMoving) and (VisibleColCount<ColCount) then
     begin
      mrg:=0;
      for i:=1 to fixedcols do mrg:=mrg+colwidths[i-1];
      if (x<mrg) and (leftcol>fixedcols) then
          begin
           leftcol:=leftcol-1;
           repaint;
          end;
      if (x>width) and (leftcol+visiblecolcount<colcount) then
          begin
           leftcol:=leftcol+1;
           repaint;
          end;
     end;

    if (fGridState=gsRowMoving) and (VisibleRowCount<RowCount) then
     begin
      mrg:=0;
      for i:=1 to fixedrows do mrg:=mrg+rowheights[i-1];
      if (y<mrg) and (toprow>fixedrows) then
          begin
           toprow:=toprow-1;
           repaint;
          end;
      if (y>height) and (toprow+visiblerowcount<rowcount) then
          begin
           toprow:=toprow+1;
           repaint;
          end;
     end;

    if not movebutton.enabled then movebutton.enabled:=true;
    if not movebutton.visible then movebutton.visible:=true;

    if (screen.cursor<>crDrag) then screen.cursor:=crDrag;
   end;
  {$ENDIF}

  if (arow<self.fixedrows) and
     (arow>=0) and (acol>=0) and
     (acol<self.fixedcols) and
     (FMouseActions.AllSelect) then
   begin
     nondef:=true;
     invokedchange:=true;
     if (self.cursor<>crAsgCross) then self.cursor:=crAsgCross;
   end;

  if (arow<self.fixedrows) and
     (arow>=0) and (acol>=0) and
     (acol>=self.fixedcols) and
     (FMouseActions.ColSelect) then
   begin
     nondef:=true;
     invokedchange:=true;
     if (self.cursor<>crVertArr) then self.cursor:=crVertArr;
   end;

  if (acol<self.fixedcols) and
     (arow>=0) and (acol>=0) and
     (arow>=self.fixedrows) and (FMouseActions.RowSelect) then
   begin
     nondef:=true;
     invokedchange:=true;
     if (self.cursor<>crHorzArr) then self.cursor:=crHorzArr;
   end;
 end;

   {$IFDEF WIN32}
   self.mousetocell(x,y,acol,arow);
   if (acol<colcount) and (arow<rowcount) and
      (acol>=0) and (arow>=0) then
   begin
    s:=cells[remapcol(acol),arow];

    if URLShow and (pos('</',s)=0) then
     begin
      if (pos('://',s)>0) or (pos('mailto:',s)>0) and (pos('</',s)=0) then
        begin
         nondef:=true;
         invokedchange:=true;
         if (self.cursor<>crURLcursor) then self.cursor:=crURLcursor;
        end;
     end;

    if (pos('</',s)<>0) then
      begin
       r:=cellrect(acol,arow);
       r.left:=r.left+2;
       r.top:=r.top+2;
       HTMLDraw(canvas,s,r,gridimages,
                x,y,true,false,false,0.0,fURLColor,anchor,stripped,xsize,ysize);
       if (anchor<>'') then
        begin
         nondef:=true;
         invokedchange:=true;
         if (self.cursor<>crURLcursor) then self.cursor:=crURLcursor;
        end;
      end;
   end;
  {$ENDIF}

 if not nondef and (self.Cursor<>foldcursor) then
    begin
     invokedchange:=true;
     self.cursor:=foldcursor;
    end;

 if (FLastHintPos.x>=0) and (FLastHintPos.y>=0) then
  begin
   self.mousetocell(x,y,acol,arow);

   if (acol<>FLastHintPos.x) or (arow<>FLastHintPos.y) then
    begin
       Application.CancelHint;
       FLastHintPos:=Point(-1,-1);
     end;
  end;

 if (gsSelecting=FGridState) and (fSelectionRectangle) and
    ((fmoveselection.top<>selection.top) or
     (fmoveselection.right<>selection.right) or
     (fmoveselection.bottom<>selection.bottom) or
     (fmoveselection.left<>selection.left)) then
    begin {old selection and new selection ?}
     invalidategridrect(selection);
     fmoveselection:=selection;
    end;

 if (fGridState in [gsRowMoving,gsColMoving]) and (fEnhRowColMove) then lockwindowupdate(self.handle);
 {
 if (gsColMoving=fGridState) then
    for i:=1 to fixedrows do y:=y+rowheights[i-1];
 if (gsRowMoving=fGridState) then
    for i:=1 to fixedcols do x:=x+colwidths[i-1];
 }
 inherited MouseMove(Shift, X, Y);

{$IFDEF DELPHI3_LVL}
 if (csDesigning in ComponentState) and
    ((FGridState=gsColSizing) or (FGridState=gsRowSizing))  then
  begin
   r:=cellrect(colclicked,rowclicked);
   if (FGridState=gsColSizing) then
     begin
      s:='cw='+inttostr(x-r.left-clickposdx);
      pt.x:=r.Left;
      pt.y:=r.top;
     end;

   if (FGridState=gsRowSizing) then
     begin
      s:='rh='+inttostr(y-r.top-clickposdy);
      pt.x:=r.left;
      pt.y:=r.top;
     end;

   pt:=clienttoscreen(pt);

   r:=FScrollHintWnd.CalcHintRect(150,s,nil);
   FScrollHintWnd.Caption:=s;
   FScrollHintWnd.Color:=application.hintcolor;

   r.left:=r.left+pt.x;
   r.right:=r.right+pt.x;
   r.top:=r.top+pt.y;
   r.bottom:=r.bottom+pt.y;

   FScrollHintWnd.ActivateHint(r,s);
   FScrollHintShow:=true;
  end;
 {$ENDIF}


 if (fEnhRowColMove) then
   begin
   lockwindowupdate(0);
   update;
  end;

end;

{---------------------------------------------------------
 v0.99 : fixed to disable autoresize in designmode
----------------------------------------------------------}
procedure TAdvStringGrid.GridResize(Sender:tObject);
var
 nx,ny:integer;
begin
 if csDesigning in ComponentState then exit;

 if (sender is tform) and FSizeWithForm then
  begin
   nx:=(sender as tform).width;
   ny:=(sender as tform).height;

   if (self.width+nx-prevsizex>0) and (self.height+ny-prevsizey>0) then
     begin
      self.width:=self.width+nx-prevsizex;
      self.height:=self.height+ny-prevsizey;
      prevsizex:=nx;
      prevsizey:=ny;
     end;
  end;

 if resizeassigned then
 begin
   try
    FOnResize(Sender);
   except
   end;
 end;
 StretchRightColumn;
end;


procedure TAdvStringGrid.ShowHintProc(var HintStr: string; var CanShow: Boolean; var HintInfo: THintInfo);
var
 col,row:longint;
 hintpos:trect;
begin
 if (Hintinfo.Hintcontrol=self) then
   begin
    Hintinfo.Hintcolor:=FHintColor;
    self.mousetocell(hintinfo.cursorpos.x,hintinfo.cursorpos.y,col,row);

    if Assigned(FOnGridHint) then
     begin
      FOnGridHint(self,row,col,hintstr);
     end;

    FLastHintPos:=point(col,row);

    hintpos:=self.Cellrect(col,row);
    hintinfo.hintpos.x:=hintpos.left;
    hintinfo.hintpos.y:=hintpos.bottom+6;

    {$IFDEF DELPHI3_LVL}
    if fhintshowcells and
       (col>=fixedcols) and
       (row>=fixedrows) and
       (col<colcount-fixedrightcols) and
       (row<rowcount-fixedfooters) then
     begin
      hintinfo.hintpos.y:=hintpos.top;
      hintstr:=cells[col,row];
     end;
    {$ENDIF}
    hintinfo.hintpos:=self.clienttoscreen(hintinfo.hintpos);
   end;

 if showhintassigned then
 begin
   try
    FOnShowHint(HintStr,CanShow,HintInfo);
   except
   end;
 end;
end;

procedure TAdvStringGrid.SetEditText(ACol, ARow: Longint; const Value: string);
begin
 acol:=remapcol(acol);
 inherited SetEditText(Acol,Arow,value);
end;

function TAdvStringGrid.GetEditText(ACol, ARow: Longint): string;
var
 AlignValue:tAlignment;
begin
 acol:=remapcol(Acol);

 if (inplaceeditor<>nil) then
 begin
  AlignValue :=GetCellAlignment(ACol,Arow);
  TAdvInplaceEdit(self.inplaceeditor).valign:=(alignvalue=taRightJustify);
 end;

 result:=inherited GetEditText(acol,arow);
end;

{$IFDEF WIN32}
procedure TAdvStringGrid.CellToRich(col,row:integer;richeditor:trichedit);
var
 ms:tmemorystream;
 s:string;
 i:integer;
begin
 s:=cells[col,row];
 if s<>'' then
  begin
   ms:=tmemorystream.create;
   for i:=1 to length(s) do ms.write(s[i],1);
   ms.position:=0;
   richeditor.lines.loadfromstream(ms);
   ms.free;
  end
 else
  begin
   richeditor.clear;
  end;
end;

procedure TAdvStringGrid.RichToCell(col,row:integer;richeditor:trichedit);
var
 ms:tmemorystream;
 s:string;
 i:integer;
 ch:char;
begin
 s:='';
 ms:=tmemorystream.create;
 richeditor.lines.savetostream(ms);
 ms.position:=0;
 if ms.size>0 then
 for i:=0 to ms.size-1 do
  begin
   ms.read(ch,1);
   s:=s+ch;
  end;
 ms.free;
 cells[col,row]:=s;
end;
{$ENDIF}

{$IFDEF DELPHI4_LVL}
procedure TAdvStringGrid.CalcSizingState(X, Y: Integer; var State: TGridState;
  var Index: Longint; var SizingPos, SizingOfs: Integer;
  var FixedInfo: TGridDrawInfo);
var
 cx,cy:integer;
 allow:boolean;
begin
 inherited;
 mousetocell(x-4,y,cx,cy);
 if (cy=0) then
  begin
   allow:=true;
   if assigned(FOnColumnSize) then
    begin
     FOnColumnSize(self,cx,allow);
     if not allow then state:=gsNormal;
    end;
  end;
end;
{$ENDIF}

function TAdvStringGrid.HiddenRow(j:integer):tstrings;
var
 i:integer;
begin
 result:=nil;
 for i:=1 to fgridItems.Count do
  begin
   if (fGridItems.Items[i-1] as TGridItem).Idx=j then
    result:=(fGridItems.Items[i-1] as TGridItem).Items;
  end;
end;

function TAdvStringGrid.GetStrippedCell(i,j:integer):string;
var
 r:trect;
 anchor,stripped:string;
 xsize,ysize:integer;
begin
 r:=getclientrect;
 HTMLDraw(canvas,cells[i,j],r,nil,0,0,true,true,false,0.0,fURLColor,anchor,stripped,xsize,ysize);
 result:=stripped;
end;

function TAdvStringGrid.GetCellsEx(i,j:integer):string;
begin
 if IsHiddenRow(j) then
  begin
   result:=HiddenRow(j).Strings[i];
  end
 else
  begin
   j:=RemapRow(j);
   result:=Cells[i,j];
  end;
end;

procedure TAdvStringGrid.SetCellsEx(i,j:integer;value:string);
begin

 if IsHiddenRow(j) then
  begin
   HiddenRow(j).Strings[i]:=value;
  end
 else
  begin
   j:=RemapRow(j);
   Cells[i,j]:=value;
  end;
end;

{$IFDEF WIN32}
procedure TAdvStringGrid.Zoom(x:integer);
var
 i:integer;
begin
 if zoomfactor+x>10 then exit;
 if zoomfactor+x<-10 then exit;
 zoomfactor:=zoomfactor+x;

 for i:=0 to colcount-1 do
  begin
   if (colwidths[i]+x>0) then
       colwidths[i]:=colwidths[i]+x;
  end;

 for i:=0 to rowcount-1 do
  begin
   if (rowheights[i]+x>0) then
     rowheights[i]:=rowheights[i]+x;
  end;
end;

procedure TAdvStringGrid.DrawIntelliFocusPoint;
var
 focusbmp,tmpbmp:tbitmap;
 arect,brect:trect;

begin
 focusbmp:=tbitmap.create;

 case FIntelliPan of
 ipBoth:focusbmp.loadfromresourceName(hinstance,'INTLI1');
 ipHorizontal:focusbmp.loadfromresourceName(hinstance,'INTLI2');
 ipVertical:focusbmp.loadfromresourceName(hinstance,'INTLI3');
 end;

 ARect:=Rect(0,0,focusbmp.Width,focusbmp.Height);
 with wheelpanpos do
 BRect:=Rect(x,y,x+focusbmp.Width,y+focusbmp.height);
 BRect:=Rect(0,0,focusbmp.Width,focusbmp.Height);

 TmpBmp:=TBitmap.Create;
 TmpBmp.Height:=focusbmp.Height;
 TmpBmp.Width:=focusbmp.Width;
 TmpBmp.Canvas.Brush.Color:=clWhite;
 TmpBmp.Canvas.BrushCopy(ARect,focusbmp,BRect,focusbmp.canvas.pixels[0,0]);
 with wheelpanpos do
 ARect:=Rect(x,y,x+focusbmp.Width,y+focusbmp.height);
 self.Canvas.CopyRect(ARect,TmpBmp.Canvas,BRect);

 focusbmp.free;
 tmpbmp.free;
end;

procedure TAdvStringGrid.EraseIntelliFocusPoint;
var
 r:trect;
begin
 r:=rect(wheelpanpos.x,wheelpanpos.y,wheelpanpos.x+32,wheelpanpos.y+32);
 invalidaterect(self.handle,@r,false);
end;

procedure TAdvStringGrid.WndProc(var Message:tMessage);
var
 nr:integer;
 pt:tpoint;
 xinc,yinc:integer;
 cursid:integer;

begin

 if (message.msg=wm_command) and
    (message.wparamhi =CBN_SELCHANGE) then
  begin
   if assigned(FOnComboChange) then
    begin
     FOnComboChange(self,col,row,
        editcombo.itemindex,editcombo.items[editcombo.itemindex]);
    end;
  end;

 if (message.msg=wm_command) and
    (message.wparamhi =CBN_EDITCHANGE) then
  begin
   if assigned(FOnComboChange) then
    begin
     FOnComboChange(self,col,row,-1,editcombo.text);
    end;
  end;

 if (message.msg=wm_timer) and (wheelmsg>0) and (wheelpan) then
  begin
   getcursorpos(pt);
   pt:=screentoclient(pt);  
   yinc:=0;
   xinc:=0;

   if (pt.x<wheelpanpos.x-5) then xinc:=-1;
   if (pt.x>wheelpanpos.x+5) then xinc:=1;
   if (pt.y<wheelpanpos.y-5) then yinc:=-1;
   if (pt.y>wheelpanpos.y+5) then yinc:=1;

   if FIntelliPan=ipHorizontal then yinc:=0;
   if FIntelliPan=ipVertical then xinc:=0;

   cursid:=8000;
   if (yinc=0) and (xinc=0) then cursid:=8000;
   if (yinc=0) and (xinc>0) then cursid:=8003;
   if (yinc=0) and (xinc<0) then cursid:=8002;
   if (yinc>0) and (xinc=0) then cursid:=8001;
   if (yinc<0) and (xinc=0) then cursid:=8004;
   if (yinc<0) and (xinc<0) then cursid:=8006;
   if (yinc<0) and (xinc>0) then cursid:=8005;
   if (yinc>0) and (xinc<0) then cursid:=8008;
   if (yinc>0) and (xinc>0) then cursid:=8007;

   if (self.col>fixedcols) and (xinc=-1) then
      begin
       if (col+xinc<leftcol) or (col+xinc>=leftcol+visiblecolcount) then EraseIntelliFocusPoint;
       self.col:=self.col+xinc;
      end;
   if (self.col<colcount-fixedrightcols-1) and (xinc=1) then
      begin
       if (col+xinc<leftcol) or (col+xinc>=leftcol+visiblecolcount) then EraseIntelliFocusPoint;
       self.col:=self.col+xinc;
      end;
   if (self.row>fixedrows) and (yinc=-1) then
      begin
       if (row+yinc<toprow) or (row+yinc>=toprow+visiblerowcount) then EraseIntelliFocusPoint;
       self.row:=self.row+yinc;
      end;
   if (self.row<rowcount-fixedfooters-1) and (yinc=1) then
      begin
       if (row+yinc<toprow) or (row+yinc>=toprow+visiblerowcount) then EraseIntelliFocusPoint;
       self.row:=self.row+yinc;
      end;

   setcursor(loadcursor(hinstance,makeintresource(cursid)));
   DrawIntelliFocusPoint;
  end;

 if (message.msg=wm_Mbuttondown) and (wheelmsg>0) and (not wheelpan) then
  begin
   cursor:=loadcursor(hinstance,makeintresource(8000));
   prevcurs:=SetCursor(Cursor);
   wheelpan:=true;
   wheeltimer:=settimer(self.handle,999,10,nil);
   wheelpanpos.x:=loword(message.lparam);
   wheelpanpos.y:=hiword(message.lparam);
   setcapture(self.handle);
   showhint:=false;
   DrawIntelliFocusPoint;
  end
 else

 if ((message.msg=wm_Mbuttondown) and (wheelmsg>0) and (wheelpan)) or
    ((message.msg=wm_lbuttondown) and (wheelmsg>0) and (wheelpan)) or
    ((message.msg=wm_rbuttondown) and (wheelmsg>0) and (wheelpan)) or
    ((message.msg=wm_keydown) and (wheelmsg>0) and (wheelpan)) then
  begin
   releasecapture;
   killtimer(self.handle,wheeltimer);
   setcursor(prevcurs);
   showhint:=true;
   wheelpan:=false;
   EraseIntelliFocusPoint;
  end;

 if (message.msg=wheelmsg) and (wheelmsg>0) and (not wheelpan) then {intellimouse event here}
  begin
   if (getkeystate(VK_CONTROL) and $8000=$8000) then
    begin  {zoom}
     if (message.wparam<0) then
       self.zoom(wheelscrl)
     else
      self.zoom(-wheelscrl);
    end
   else
    begin {normal scrolling}
     if (message.wparam<0) then
      nr:=row+wheelscrl
     else
      nr:=row-wheelscrl;
     if (nr<fixedrows) then nr:=fixedrows;
     if (nr>=rowcount-fixedfooters) then nr:=rowcount-1;
     row:=nr;
    end;
  end;

 inherited;
end;
{$ENDIF}

{$IFDEF EDITCONTROLS}
procedure TAdvStringGrid.ShowInplaceEdit;
begin
 self.showeditor;
end;

procedure TAdvStringGrid.TopLeftChanged;
begin
 inherited TopLeftChanged;

 if (editmode) and (editcontrol<>edNormal) then
   begin
    HideEditControl(col,row);
    HideEditor;
   end;
 UpdateVScrollBar;
 UpdateHScrollBar;
end;

procedure TAdvStringGrid.EllipsClick;
var
 s:string;
begin
 if Assigned(OnEllipsClick) then
   begin
    s:=cells[col,row];
    OnEllipsClick(self,col,row,s);
    cells[col,row]:=s;
   end;
end;
{$ENDIF}

{$IFDEF DELPHI3_LVL}
function TAdvStringGrid.GetDateTimePicker:tDateTimePicker;
begin
 result:=TDateTimePicker(editdate);
end;
{$ENDIF}


procedure TAdvStringGrid.KeyUp(var Key: Word; Shift: TShiftState);
begin
 if key in [vk_left,vk_right,vk_down,vk_up,vk_home,vk_end,vk_prior,vk_next,vk_insert,vk_delete] then searchinc:='';
 {$IFDEF WIN32}
 inherited;
 if key in [vk_left,vk_right,vk_down,vk_up] then
   if fNavigation.AlwaysEdit then ShowInplaceEdit;
 {$ELSE}
 inherited KeyUp(key,shift);
 {$ENDIF}
end;

function TAdvStringGrid.GetVersionNr:integer;
begin
 result:=makelong(1,86);
end;

function TAdvStringGrid.GetVersionString:string;
begin
 result:='1.86, Nov 22 1999';
end;

{
procedure TAdvStringGrid.SetFlat(const value:boolean);
begin
 if fFlat<>value then
  begin
   fFlat:=value;
   invalidate;
  end;
end;

procedure TAdvStringGrid.SetFixedRows(const value:integer);
begin
 if fFlat then
  begin
   inherited FixedRows:=value;
   fFixedRows:=value;
  end
 else
  begin
   inherited FixedRows:=value;
  end;
end;

function TAdvStringGrid.GetFixedRows:integer;
begin
 if fFlat then result:=fFixedRows else result:=inherited FixedRows;
end;

procedure TAdvStringGrid.SetFixedCols(const value:integer);
begin
 if fFlat then
  begin
   inherited FixedCols:=value;
   fFixedCols:=value;
  end
 else
  begin
   inherited FixedCols:=value;
  end;
end;

function TAdvStringGrid.GetFixedCols:integer;
begin
 if fFlat then result:=fFixedCols else result:=inherited FixedCols;
end;
}


{$IFDEF DELPHI3_4_ONLY}
constructor TGridDropTarget.Create(aGrid: TAdvStringGrid);
begin
 inherited Create;
 fGrid:=aGrid;
end;

procedure TGridDropTarget.DropText(pt: tpoint; s: string);
var
 c,r:integer;
begin
 pt:=fGrid.ScreenToClient(pt);
 fGrid.mousetocell(pt.x,pt.y,c,r);
 fGrid.PasteText(c,r,pchar(s));
end;

procedure TGridDropTarget.DropRTF(pt: tpoint; s: string);
var
 c,r:integer;
begin
 pt:=fGrid.ScreenToClient(pt);
 fGrid.mousetocell(pt.x,pt.y,c,r);
 fGrid.Cells[c,r]:=s;
end;

procedure TGridDropTarget.DropFiles(pt:tpoint; files:tstrings);
begin
 if files.count=1 then
  begin
   if (pos('.CSV',files[0])>0) or (pos('.csv',files[0])>0) then
    fGrid.LoadFromCSV(files[0]);
   if (pos('.XLS',files[0])>0) or (pos('.xls',files[0])>0) then
    fGrid.LoadFromXLS(files[0]);
  end;
end;

procedure Initialize;
var
  Result : HRESULT;
begin
  Result := OleInitialize (nil);
  Assert (Result in [S_OK, S_FALSE], Format ('OleInitialize failed ($%x)', [Result]));
end;

{$ENDIF}

{ TGridItem }
constructor TGridItem.Create(Collection: TCollection);
begin
 inherited Create(collection);
 fItems:=tstringlist.Create;
end;

destructor TGridItem.Destroy;
begin
 fItems.Free;
 inherited Destroy;
end;

procedure TGridItem.SetIdx(const Value: integer);
begin
  FIdx := Value;
end;

procedure TGridItem.SetItems(const Value: tstrings);
begin
  FItems := Value;
end;

{ TAdvRichEdit }
{$IFDEF WIN32}
procedure TAdvRichEdit.SelNormal;
begin
 SelFormat(0);
end;

procedure TAdvRichEdit.SelSubscript;
begin
 SelFormat(-40);
end;

procedure TAdvRichEdit.SelSuperscript;
begin
 SelFormat(40);
end;

procedure TAdvRichEdit.SelFormat(offset:integer);
var
 format: TCharFormat; { defined in Unit RichEdit }
begin
 FillChar( format, sizeof(format), 0);
 With format Do Begin
  cbSize:= Sizeof(format);
  dwMask:= CFM_OFFSET;
  yOffset:= offset; { superscript by 40 twips, negative values give subscripts}
 End;
 Perform( EM_SETCHARFORMAT, SCF_SELECTION,LongInt(@format));
end;
{$ENDIF}


{ TFilter }

function TFilter.Add: TFilterData;
begin
  result:=TFilterData(inherited Add);
end;

constructor TFilter.Create(aOwner: TAdvStringGrid);
begin
 inherited Create(TFilterData);
 fOwner:=aOwner;
end;

function TFilter.GetItem(Index: Integer): TFilterData;
begin
 Result := TFilterData(inherited GetItem(Index));
end;

function TFilter.GetOwner: tPersistent;
begin
 result:=fOwner;
end;

function TFilter.Insert(index: integer): TFilterData;
begin
 result:=TFilterData(inherited Add);
end;

procedure TFilter.SetItem(Index: Integer; Value: TFilterData);
begin
  inherited SetItem(Index, Value);
end;




{ TBackGround }

constructor TBackGround.Create(aGrid: tAdvStringGrid);
begin
 fGrid:=aGrid;
 inherited Create;
 fBitmap:=TBitmap.Create;
end;

destructor TBackGround.Destroy;
begin
 fBitmap.Free;
 inherited Destroy;
end;

procedure TBackGround.SetBitmap(value: tBitmap);
begin
 fBitmap.Assign(value);
 fGrid.Repaint;
end;

procedure TBackGround.SetLeft(value: integer);
begin
 fLeft:=value;
 fGrid.Repaint;
end;

procedure TBackGround.SetTop(value: integer);
begin
 fTop:=value;
 fGrid.Repaint;
end;

procedure TBackGround.SetDisplay(value: TBackgroundDisplay);
begin
 fDisplay:=value;
 fGrid.Repaint;
end;

{$IFDEF DELPHI3_4_ONLY}

initialization
  Initialize
finalization
  OleUninitialize
{$ENDIF}
end.


