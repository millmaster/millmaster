inherited TKClassSel: TTKClassSel
  Left = 516
  Top = 182
  Caption = '(*)Spindelbereich'
  ClientHeight = 182
  ClientWidth = 204
  PixelsPerInch = 96
  TextHeight = 13
  inherited bOK: TmmButton
    Left = 12
    Top = 152
    OnClick = bOKClick
  end
  inherited bCancel: TmmButton
    Left = 104
    Top = 152
    Width = 91
  end
  object mmListBox1: TmmListBox
    Left = 8
    Top = 8
    Width = 185
    Height = 129
    Enabled = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier'
    Font.Style = [fsBold]
    ItemHeight = 13
    ParentFont = False
    TabOrder = 2
    Visible = True
    OnDblClick = bOKClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 176
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
end
