unit Assignments_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88  $
// File generated on 24.01.2000 15:58:21 from Type Library described below.

// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
// ************************************************************************ //
// Type Lib: \\Wetsrvbde\BDE_Dev\Delphi\Act\GUI\Assignments\Assignments.tlb (1)
// IID\LCID: {0B91B571-D26A-11D3-A09E-00A024B76A4A}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (E:\WINNT\System32\StdOle2.Tlb)
//   (2) v4.0 StdVCL, (E:\WINNT\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  AssignmentsMajorVersion = 1;
  AssignmentsMinorVersion = 0;

  LIBID_Assignments: TGUID = '{0B91B571-D26A-11D3-A09E-00A024B76A4A}';

  IID_IAssignmentsServer: TGUID = '{0B91B572-D26A-11D3-A09E-00A024B76A4A}';
  CLASS_AssignmentsServer: TGUID = '{0B91B574-D26A-11D3-A09E-00A024B76A4A}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IAssignmentsServer = interface;
  IAssignmentsServerDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  AssignmentsServer = IAssignmentsServer;


// *********************************************************************//
// Interface: IAssignmentsServer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {0B91B572-D26A-11D3-A09E-00A024B76A4A}
// *********************************************************************//
  IAssignmentsServer = interface(IDispatch)
    ['{0B91B572-D26A-11D3-A09E-00A024B76A4A}']
  end;

// *********************************************************************//
// DispIntf:  IAssignmentsServerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {0B91B572-D26A-11D3-A09E-00A024B76A4A}
// *********************************************************************//
  IAssignmentsServerDisp = dispinterface
    ['{0B91B572-D26A-11D3-A09E-00A024B76A4A}']
  end;

// *********************************************************************//
// The Class CoAssignmentsServer provides a Create and CreateRemote method to          
// create instances of the default interface IAssignmentsServer exposed by              
// the CoClass AssignmentsServer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAssignmentsServer = class
    class function Create: IAssignmentsServer;
    class function CreateRemote(const MachineName: string): IAssignmentsServer;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TAssignmentsServer
// Help String      : AssignmentsServer Object
// Default Interface: IAssignmentsServer
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TAssignmentsServerProperties= class;
{$ENDIF}
  TAssignmentsServer = class(TOleServer)
  private
    FIntf:        IAssignmentsServer;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TAssignmentsServerProperties;
    function      GetServerProperties: TAssignmentsServerProperties;
{$ENDIF}
    function      GetDefaultInterface: IAssignmentsServer;
  protected
    procedure InitServerData; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IAssignmentsServer);
    procedure Disconnect; override;
    property  DefaultInterface: IAssignmentsServer read GetDefaultInterface;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TAssignmentsServerProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TAssignmentsServer
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TAssignmentsServerProperties = class(TPersistent)
  private
    FServer:    TAssignmentsServer;
    function    GetDefaultInterface: IAssignmentsServer;
    constructor Create(AServer: TAssignmentsServer);
  protected
  public
    property DefaultInterface: IAssignmentsServer read GetDefaultInterface;
  published
  end;
{$ENDIF}


procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
 ComObj;

class function CoAssignmentsServer.Create: IAssignmentsServer;
begin
  Result := CreateComObject(CLASS_AssignmentsServer) as IAssignmentsServer;
end;

class function CoAssignmentsServer.CreateRemote(const MachineName: string): IAssignmentsServer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AssignmentsServer) as IAssignmentsServer;
end;

procedure TAssignmentsServer.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{0B91B574-D26A-11D3-A09E-00A024B76A4A}';
    IntfIID:   '{0B91B572-D26A-11D3-A09E-00A024B76A4A}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TAssignmentsServer.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IAssignmentsServer;
  end;
end;

procedure TAssignmentsServer.ConnectTo(svrIntf: IAssignmentsServer);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TAssignmentsServer.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TAssignmentsServer.GetDefaultInterface: IAssignmentsServer;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TAssignmentsServer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TAssignmentsServerProperties.Create(Self);
{$ENDIF}
end;

destructor TAssignmentsServer.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TAssignmentsServer.GetServerProperties: TAssignmentsServerProperties;
begin
  Result := FProps;
end;
{$ENDIF}

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TAssignmentsServerProperties.Create(AServer: TAssignmentsServer);
begin
  inherited Create;
  FServer := AServer;
end;

function TAssignmentsServerProperties.GetDefaultInterface: IAssignmentsServer;
begin
  Result := FServer.DefaultInterface;
end;

{$ENDIF}

procedure Register;
begin
  RegisterComponents('Servers',[TAssignmentsServer]);
end;

end.
