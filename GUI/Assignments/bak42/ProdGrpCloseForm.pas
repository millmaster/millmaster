(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: ProdGrpForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 28.12.1999  1.00  Mg  | Datei erstellt
| 16.03.2000  1.01  Mg  | BroadcastMsg inserted
| 04.04.2000  1.02  Wss/Nue  | TAssMachine- and TCloseHandle-Component inserted (only Components and main links)
| 23.08.2000  1.03  Mg  | Print inserted
| 29.08.2000  1.04  Mg  | No Broadcast of refresh
| 17.12.2001  1.05  Wss | Icons changed
| 13.03.2002  1.05  Wss | Diverse Captions geloescht, da diese im Multilizer auftauchten
| 09.04.2002  1.05  Wss | CleanUpObjectsInListView implemented to cleanup memory leaks
| 02.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
| 07.06.2004        SDO | Neue Func. PrintList
| 08.06.2004        SDO | Liste wird nicht mehr ausgedruckt -> acPrint.Visible = FALSE
|=============================================================================*)
unit ProdGrpCloseForm;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IvDictio, IvMulti, IvEMulti, mmTranslator, ActnList,
  Db, mmActionList, ToolWin, ComCtrls, mmToolBar,
  Buttons, mmSpeedButton, MMUglobal, StdCtrls, mmListBox, AssignComp, Grids,
  AdvGrid, mmStringGrid, mmListView, CloseHandlerComp, MMSecurity,
  LotParameterForm, LoepfeGlobal, BaseGlobal, ImgList, mmImageList,
  BASEFORM, ADODB, mmADODataSet, mmADOConnection, u_dmAssignment;
const
  cNoProdGrpSelected =
    '(*)Bitte Partie markieren.'; // ivlm
type
//..............................................................................
//  TProdGrpClose = class(TBaseStickerForm)
  TProdGrpClose = class(TmmForm)
    mmToolBar1: TmmToolBar;
    mmActionList1: TmmActionList;
    acExit: TAction;
    acCloseProdGrp: TAction;
    ToolButton1: TToolButton;
    mmListView1: TmmListView;
    ToolButton2: TToolButton;
    mmSpeedButton1: TmmSpeedButton;
    acSecurity: TAction;
    MMSecurityControl1: TMMSecurityControl;
    ToolButton3: TToolButton;
    acLotParameter: TAction;
    CloseHandler1: TCloseHandler;
    Machine1: TMachine;
    ToolButton4: TToolButton;
    acHelp: TAction;
    acPrint: TAction;
    ToolButton5: TToolButton;
    mmImageList: TmmImageList;
    ToolButton6: TToolButton;
    bLotParameter: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    mmTranslator: TmmTranslator;
    conDefault: TmmADOConnection;
    dseQuery1: TmmADODataSet;
    procedure acExitExecute(Sender: TObject);
    procedure acCloseProdGrpExecute(Sender: TObject);
    procedure mmListView1SelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure acSecurityExecute(Sender: TObject);
    procedure mmActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure acLotParameterExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPrintExecute(Sender: TObject);
  private
    mMachineQuery: string;
    fMachineID: integer;
    fLotParameterEnabled: boolean;
    procedure UpdateTable;
    procedure SetMachineID(aMachineID: integer);
//    procedure SetProdGrpID(aProdGrpID: integer);
    procedure SetLotParameterEnabled(aEnabled: boolean);
    procedure CleanUpObjectsInListView;
    procedure PrintList;
  public
    constructor Create(aOwner: TComponent; aDataBase: TmmAdoConnection); reintroduce; virtual;
    property MachineID: integer read fMachineID write SetMachineID;
    property LotParameterEnabled: boolean read fLotParameterEnabled write SetLotParameterEnabled;
  end;
//..............................................................................
  TProdGrpItem = class(TObject)
  private
    fProdGrpID: integer;
  public
    constructor Create(aProdID: integer);
    property ProdGrpID: integer read fProdGrpID;
  end;
//..............................................................................
implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,
  MMMessages, Printers, CobChooserPrintForm, PrintSettingsTemplateForm;
const
  cProdGrpObjectPos = 4;

  cGetProdGrps =
    'select t_ps.c_prod_id, t_p.c_spindle_first, t_p.c_spindle_last, t_p.c_machine_id, ' +
    't_p.c_prod_name, t_p.c_prod_start, t_p.c_machine_name ' +
    'from t_prodgroup_state t_ps, t_prodgroup t_p ' +
    'where ' +
    't_ps.c_prod_id = t_p.c_prod_id ';

  cOrderPart =
    ' order by t_p.c_machine_name,t_p.c_spindle_first';
//------------------------------------------------------------------------------
  { TProdGrp }
//------------------------------------------------------------------------------
constructor TProdGrpItem.Create(aProdID: integer);
begin
  inherited Create;
  fProdGrpID := aProdID;
end;
//------------------------------------------------------------------------------
 { TProdGrpClose }
//------------------------------------------------------------------------------
constructor TProdGrpClose.Create(aOwner: TComponent; aDataBase: TmmAdoConnection);
begin
  inherited Create(aOwner);
  if assigned(aDataBase) then begin
(*      dseQuery1.Connection := TmmADOConnection.Create(dseQuery1);
      dseQuery1.Connection.ConnectionString := GetDefaultConnectionString;
    end else begin*)
    dseQuery1.Connection := aDataBase;
  end; // if not(assigned(aDataBase)) then begin
  fMachineID := 0;
  mMachineQuery := '';
  Width := 600;
  Height := 400;
  fLotParameterEnabled := bLotParameter.Visible;
end;
//------------------------------------------------------------------------------
procedure TProdGrpClose.SetMachineID(aMachineID: integer);
begin
  fMachineID := aMachineID;
  if fMachineID <> 0 then
    mMachineQuery := 'and t_p.c_machine_id = ' + InttoStr(fMachineID);
  UpdateTable;
end;
//------------------------------------------------------------------------------
//procedure TProdGrpClose.SetProdGrpID(aProdGrpID: integer);
//var xProdGrp: TProdGrp;
//begin
//  inherited;
//  try
//    xProdGrp := TProdGrp.Create(dseQuery1);
//    xProdGrp.ProdGrpID := aProdGrpID;
//    CloseHandler1.SpdRange := xProdGrp;
//    CloseHandler1.CloseProdGrp;
//    xProdGrp.Free;
//  except
//    on e: Exception do begin
//      xProdGrp.Free;
//      SystemErrorMsg_('TProdGrpClose.SetProdGrpID failed. ' + e.Message);
//    end;
//  end;
//end;
//------------------------------------------------------------------------------
procedure TProdGrpClose.SetLotParameterEnabled(aEnabled: boolean);
begin
  fLotParameterEnabled := aEnabled;
  bLotParameter.Visible := fLotParameterEnabled;
end;
//------------------------------------------------------------------------------
procedure TProdGrpClose.CleanUpObjectsInListView;
var
  i: Integer;
begin
  with mmListView1.Items do try
    for i := 0 to Count - 1 do
      Item[i].SubItems.Objects[cProdGrpObjectPos].Free;
  except
  end;
end;
//------------------------------------------------------------------------------
procedure TProdGrpClose.UpdateTable;
var xListItem: TListItem;
begin
  try
    CleanUpObjectsInListView;
    with dseQuery1 do begin
      Close;
      CommandText := cGetProdGrps + mMachineQuery + cOrderPart;
      Open;
      mmListView1.Enabled := False;
      mmListView1.Items.Clear;
      while not EOF do begin // ADO Conform
        xListItem := mmListView1.Items.Add;
        xListItem.Caption := FieldByName('c_machine_name').AsString;
        xListItem.SubItems.Add(FieldByName('c_spindle_first').AsString);
        xListItem.SubItems.Add(FieldByName('c_spindle_last').AsString);
        xListItem.SubItems.Add(FieldByName('c_prod_name').AsString);
        xListItem.SubItems.Add(FieldByName('c_prod_start').AsString);
        xListItem.SubItems.AddObject(FieldByName('c_prod_name').AsString,
          TProdGrpItem.Create(FieldByName('c_prod_id').AsInteger));
        Next;
      end;
    end;
    mmListView1.Enabled := True;
    //mmListView1.Items[0].Selected := true;
   // mmListView1.Items[0].Focused  := true;

  except
    on e: Exception do begin
      SystemErrorMsg_('TProdGrpClose.UpdateTable failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TProdGrpClose.acExitExecute(Sender: TObject);
begin
  inherited;
  Close;
end;
//------------------------------------------------------------------------------
procedure TProdGrpClose.acCloseProdGrpExecute(Sender: TObject);
var
  xListItem: TListItem;
  xProdGrp: TProdGrp;
begin
  try
    xListItem := mmListView1.ItemFocused;
    if Assigned(xListItem) then begin
      xProdGrp := TProdGrp.Create(dseQuery1);
      xProdGrp.ProdGrpID := (xListItem.SubItems.Objects[cProdGrpObjectPos] as TProdGrpItem).ProdGrpID;
      CloseHandler1.SpdRange := xProdGrp;
      CloseHandler1.CloseProdGrp;
      xProdGrp.Free;
      UpdateTable;
    end
    else begin
      InfoMsg(Translate(cNoProdGrpSelected));
    end;
  except
    on e: Exception do begin
      FreeAndNil(xProdGrp);
      SystemErrorMsg_('TProdGrpClose.acCloseProdGrpExecute failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TProdGrpClose.mmListView1SelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
  inherited;
    //button disabled zeichnen ec...
end;
//------------------------------------------------------------------------------
procedure TProdGrpClose.acSecurityExecute(Sender: TObject);
begin
  inherited;
  MMSecurityControl1.Configure;
end;
//------------------------------------------------------------------------------
procedure TProdGrpClose.mmActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
  inherited;
  acCloseProdGrp.Enabled := Assigned(mmListView1.ItemFocused) and MMSecurityControl1.CanEnabled(acCloseProdGrp);
  acLotParameter.Enabled := Assigned(mmListView1.ItemFocused) and MMSecurityControl1.CanEnabled(acLotParameter);
end;
//------------------------------------------------------------------------------
procedure TProdGrpClose.acLotParameterExecute(Sender: TObject);
var xListItem: TListItem;
  xProdGrpID: integer;
  xLotParForm: TLotParameter;
begin
  inherited;
  xListItem := mmListView1.ItemFocused;
  if Assigned(xListItem) then begin
    xProdGrpID := (xListItem.SubItems.Objects[cProdGrpObjectPos] as TProdGrpItem).ProdGrpID;
    xLotParForm := TLotParameter.Create(self);
    xLotParForm.ProdGrpID := xProdGrpID;
    xLotParForm.ShowModal;
  end
  else begin
    InfoMsg(Translate(cNoProdGrpSelected));
  end;
end;
//------------------------------------------------------------------------------
procedure TProdGrpClose.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  CleanUpObjectsInListView;
    //MMBroadCastMessage ( RegisterWindowMessage ( cMsgAppl ), cMMRefresh );
end;
//------------------------------------------------------------------------------
procedure TProdGrpClose.acPrintExecute(Sender: TObject);
begin
  inherited;
  Print;
  //PrintList;
end;
//------------------------------------------------------------------------------
procedure TProdGrpClose.PrintList;
var
  xTSG: TStringGrid;
  c, r, x: Integer;
  xText : String;
begin

{

  xTSG := TStringGrid.Create(Self);
  xTSG.RowCount := mmListView1.Items.Count;
  //xTSG.ColCount := mmListView1.Items.Item[0].SubItems.Count;
  xTSG.ColCount :=  mmListView1.Columns.Count;
  mmListView1.Items.Item[x].

  for x:= 0 to  mmListView1.Items.Count -1 do begin

     xTSG.Rows[x].AddStrings( mmListView1.Items.Item[x].SubItems );
  end;


  with TfrmCobChooserPrint.Create(Self) do
  try

    with TfrmPrintSettings.Create(self) do begin


         mPrintSetup.BlackWhiteEnable := FALSE;

         ShowClearerSettings := FALSE;

         if ShowModal = mrOk then begin

            mQuickRep.UsePrinterIndex(mPrintSetup.PrinterIndex);


            mQuickRep.ReportTitle := Format('%s  (%s)',[gApplicationName,  Self.Caption]);

            //Texte aus Self.Caption fuer Report extrahieren
            xText := Copy(Self.Caption, 1, Pos(':',Self.Caption) );
            xText := StringReplace(xText , ':', '', [rfReplaceAll]);
            Trim(xText);
            qlApplication.Caption := xText;

            QRPrintStringGrid.DataGrid := xTSG;

            QRPrintStringGrid.Print;
         end;

         Free;
    end;




  finally
    Free;
    xTSG.Free;
  end;
}
end;
//------------------------------------------------------------------------------


end.

