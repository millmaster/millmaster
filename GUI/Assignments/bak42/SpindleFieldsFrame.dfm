object SpindleFields: TSpindleFields
  Left = 0
  Top = 0
  Width = 427
  Height = 99
  TabOrder = 0
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 427
    Height = 99
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object laFirst: TmmLabel
      Left = 115
      Top = 20
      Width = 58
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '(10)Erste :'
      FocusControl = edFirst
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laLast: TmmLabel
      Left = 115
      Top = 44
      Width = 58
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '(10)Letzte :'
      FocusControl = edLast
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laPilot: TmmLabel
      Left = 115
      Top = 68
      Width = 58
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '(10)Pilot :'
      FocusControl = edPilot
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laSpindleRange: TmmLabel
      Left = 112
      Top = 0
      Width = 107
      Height = 13
      Caption = '(20)Spulstellenbereich:'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laGrpNr: TmmLabel
      Left = 8
      Top = 0
      Width = 49
      Height = 13
      Caption = '(8)Grp. Nr.'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laAssign: TmmLabel
      Left = 48
      Top = 16
      Width = 24
      Height = 21
      Alignment = taCenter
      AutoSize = False
      Caption = 'A'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laYarnCount: TmmLabel
      Left = 270
      Top = 20
      Width = 80
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '(12)Garn Nr:'
      FocusControl = edYarnCnt
      Visible = True
      AutoLabel.Control = lbYarnCountUnit
      AutoLabel.Distance = 48
      AutoLabel.LabelPosition = lpRight
    end
    object laSlip: TmmLabel
      Left = 270
      Top = 44
      Width = 80
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '(12)Schlupf:'
      FocusControl = edSlip
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object lbYarnCountUnit: TmmLabel
      Left = 398
      Top = 20
      Width = 16
      Height = 13
      Caption = 'Nm'
      FocusControl = laYarnCount
      Visible = True
      AutoLabel.Distance = 30
      AutoLabel.LabelPosition = lpLeft
    end
    object laThreads: TmmLabel
      Left = 278
      Top = 68
      Width = 80
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '(12)Fadenzahl:'
      FocusControl = edThreadCnt
      Visible = False
      AutoLabel.LabelPosition = lpLeft
    end
    object udFirst: TmmUpDown
      Left = 201
      Top = 16
      Width = 15
      Height = 21
      Associate = edFirst
      Min = 1
      Max = 80
      Position = 1
      TabOrder = 3
      Wrap = False
    end
    object udLast: TmmUpDown
      Left = 201
      Top = 40
      Width = 15
      Height = 21
      Associate = edLast
      Min = 1
      Max = 80
      Position = 1
      TabOrder = 4
      Wrap = False
    end
    object udPilot: TmmUpDown
      Left = 201
      Top = 64
      Width = 15
      Height = 21
      Associate = edPilot
      Min = 1
      Max = 80
      Position = 1
      TabOrder = 5
      Wrap = False
    end
    object edFirst: TmmMaskEdit
      Left = 176
      Top = 16
      Width = 25
      Height = 21
      TabOrder = 0
      Text = '1'
      Visible = True
      OnChange = edChange
      OnExit = edExit
      OnKeyPress = edKeyPress
      AutoLabel.Control = laFirst
      AutoLabel.Distance = 3
      AutoLabel.LabelPosition = lpLeft
    end
    object edLast: TmmMaskEdit
      Left = 176
      Top = 40
      Width = 25
      Height = 21
      Hint = '(*)Letzte Spulstelle der Gruppe'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = '1'
      Visible = True
      OnChange = edChange
      OnExit = edExit
      OnKeyPress = edKeyPress
      AutoLabel.Control = laLast
      AutoLabel.Distance = 3
      AutoLabel.LabelPosition = lpLeft
    end
    object edPilot: TmmMaskEdit
      Left = 176
      Top = 64
      Width = 25
      Height = 21
      Hint = '(*)Anzahl Pilot Spulstellen'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Text = '1'
      Visible = True
      OnChange = edChange
      OnExit = edExit
      OnKeyPress = edKeyPress
      AutoLabel.Control = laPilot
      AutoLabel.Distance = 3
      AutoLabel.LabelPosition = lpLeft
    end
    object cobGrpNr: TmmComboBox
      Left = 8
      Top = 16
      Width = 41
      Height = 21
      Style = csDropDownList
      Color = clWindow
      DropDownCount = 13
      ItemHeight = 13
      TabOrder = 6
      Visible = True
      OnChange = cobGrpNrChange
      AutoLabel.LabelPosition = lpLeft
      Edit = True
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
    end
    object mmPanel2: TmmPanel
      Left = 8
      Top = 69
      Width = 49
      Height = 17
      BevelInner = bvLowered
      BevelOuter = bvSpace
      TabOrder = 7
      Visible = False
    end
    object edYarnCnt: TmmMaskEdit
      Left = 353
      Top = 16
      Width = 41
      Height = 21
      Hint = '(*)Garnnummer des produzierenden Artikels'
      MaxLength = 5
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      Text = '120'
      Visible = True
      OnChange = edChange
      OnExit = edExit
      OnKeyPress = edYarnCntKeyPress
      AutoLabel.Control = laYarnCount
      AutoLabel.Distance = 3
      AutoLabel.LabelPosition = lpLeft
    end
    object edSlip: TmmMaskEdit
      Left = 353
      Top = 40
      Width = 41
      Height = 21
      Hint = '(*)Schlupf der Maschine'
      MaxLength = 5
      ParentShowHint = False
      ShowHint = True
      TabOrder = 9
      Text = '1.000'
      Visible = True
      OnChange = edChange
      OnExit = edExit
      OnKeyPress = edSlipKeyPress
      AutoLabel.Control = laSlip
      AutoLabel.Distance = 3
      AutoLabel.LabelPosition = lpLeft
    end
    object edThreadCnt: TmmMaskEdit
      Left = 361
      Top = 64
      Width = 17
      Height = 21
      Hint = '(*)Anzahl der Faeden im Garn'
      MaxLength = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 10
      Text = '1'
      Visible = False
      OnChange = edChange
      OnExit = edExit
      OnKeyPress = edThreadCntKeyPress
      AutoLabel.Control = laThreads
      AutoLabel.Distance = 3
      AutoLabel.LabelPosition = lpLeft
    end
    object mmUpDown1: TmmUpDown
      Left = 378
      Top = 64
      Width = 15
      Height = 21
      Associate = edThreadCnt
      Min = 1
      Max = 9
      Position = 1
      TabOrder = 11
      Visible = False
      Wrap = False
    end
  end
end
