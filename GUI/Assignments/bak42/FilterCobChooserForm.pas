(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: FilterCobChooserForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows2000
| Target.system.: Windows2000
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 12.05.2003  1.00  Nue | Datei erstellt
| 27.08.2003  1.01  Nue | Modifikation MaxDate f�r DemoDB und Zeitumstellungen,
|                       | wenn es auf einmal Daten in der Zukunft hat. (Als Parameter in Create)
|=============================================================================*)
unit FilterCobChooserForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton, wwdbdatetimepicker, mmLabel, mmCheckBox, mmEdit,
  IvDictio, IvMulti, IvEMulti, mmTranslator, BASEFORM, mmRadioButton,
  MMUGlobal, LoepfeGlobal, AssignComp, mmNumCtrl, XMLDef;

type
  TFilterQueryParameters = class(TQueryParameters)
  private
    fProdStateAssigned: boolean;
    fYarnCount: single;
    procedure SetYarnCount(const Value: single);
  public
    property YarnCount: single read fYarnCount write SetYarnCount;
    property ProdStateAssigned: boolean read fProdStateAssigned write fProdStateAssigned;
  end;

  TFilter = class(TmmForm)
    laVon: TmmLabel;
    tpStart: TwwDBDateTimePicker;
    laBis: TmmLabel;
    tpEnd: TwwDBDateTimePicker;
    bOk: TmmButton;
    bCancel: TmmButton;
    edStyleName: TmmEdit;
    laStyleName: TmmLabel;
    edLotName: TmmEdit;
    laLotName: TmmLabel;
    edTemplateName: TmmEdit;
    laTemplateName: TmmLabel;
    mmTranslator1: TmmTranslator;
    cbTotTimeRange: TmmCheckBox;
    laYarnCount: TmmLabel;
    laTimeInfo: TmmLabel;
    cbAssigned: TmmCheckBox;
    edYarnCount: TmmNumEdit;
    procedure tpChange(Sender: TObject);
    procedure bCancelClick(Sender: TObject);
    procedure bOkClick(Sender: TObject);
    procedure cbAssignedClick(Sender: TObject);
    procedure cbTotTimeRangeClick(Sender: TObject);
    procedure edStyleNameChange(Sender: TObject);
    procedure edLotNameChange(Sender: TObject);
    procedure edTemplateNameChange(Sender: TObject);
    procedure edYarnCountChange(Sender: TObject);
    procedure edYarnCountKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    mQueryParameters: TFilterQueryParameters;
    fActSource: TSourceTypes;
    mTotTimeRange: Boolean;
    fInitState: Boolean;
    procedure SetInitState(const Value: Boolean);
    procedure SetNoFilter(const Value: Boolean);
  public
    constructor Create(Sender: TComponent; aMaxDate: TDate);reintroduce; overload;
    destructor Destroy; override;
    property ActSource: TSourceTypes read fActSource;
    property NoFilter: Boolean write SetNoFilter;
    property InitState: Boolean read fInitState write SetInitState;
    procedure InitBeforeShow(aActSource: TSourceTypes; aYarnUnit: TYarnUnit);
    function GetWhere: string;
  end;

var
  Filter: TFilter;

implementation
uses
  mmMBCS,MMHtmlHelp;
{$R *.DFM}

//------------------------------------------------------------------------------

procedure TFilter.tpChange(Sender: TObject);
begin
  if (Sender=tpStart) then begin
    if tpEnd.Date<tpStart.Date then
      tpEnd.Date := tpStart.Date;
    mQueryParameters.TimeFrom := tpStart.Date;
  end;
  if (Sender=tpEnd) then begin
    if tpEnd.Date<tpStart.Date then
      tpStart.Date := tpEnd.Date;
    mQueryParameters.TimeTo := tpEnd.Date;
  end;
end;
//------------------------------------------------------------------------------

procedure TFilter.bCancelClick(Sender: TObject);
begin
  Close;
end;
//------------------------------------------------------------------------------

procedure TFilter.bOkClick(Sender: TObject);
begin
  Close;
  ModalResult := mrOK;
end;
//------------------------------------------------------------------------------

procedure TFilter.cbAssignedClick(Sender: TObject);
begin
  mQueryParameters.ProdStateAssigned := cbAssigned.Checked;
  InitState := False;
end;
//------------------------------------------------------------------------------

procedure TFilter.cbTotTimeRangeClick(Sender: TObject);
begin
  if cbTotTimeRange.Checked then begin
    tpStart.Enabled := False;
    tpEnd.Enabled := False;
  end
  else begin
    tpStart.Enabled := True;
    tpEnd.Enabled := True;
  end; //else
  mTotTimeRange := cbTotTimeRange.Checked;
  InitState := False;
end;
//------------------------------------------------------------------------------

function TFilter.GetWhere: string;
begin
  result := ' WHERE 1=1 ';
  if assigned(mQueryParameters) then begin

//: ----------------------------------------------
    if (mQueryParameters.ProdNames <> '') and (fActSource=stHistory) then begin
      result := result + ' AND t_p.c_prod_name like ''' + mQueryParameters.ProdNames + ''' ';
    end;// if mQueryParameters.ProdNames <> '' then begin

//: ----------------------------------------------
    if mQueryParameters.StyleNames <> '' then begin
      result := result + ' AND t_s.c_style_name like ''' + mQueryParameters.StyleNames + ''' ';
    end;// if mQueryParameters.StyleNames <> '' then begin

//: ----------------------------------------------
    if mQueryParameters.YMSetNames <> '' then begin
      result := result + ' AND t_ym.c_ym_set_name like ''' + mQueryParameters.YMSetNames + ''' ';
    end;// if mQueryParameters.YMSetNames <> '' then begin

//: ----------------------------------------------
    if mQueryParameters.YarnCount > 0.0 then begin
      if fActSource=stHistory then
        result := result + ' AND t_p.c_act_yarncnt BETWEEN ' + FloatToStr(mQueryParameters.YarnCount-cYarnCountDelta) +
                                                 ' AND '+ FloatToStr(mQueryParameters.YarnCount+cYarnCountDelta) + ' '
      else
        result := result + ' AND t_s.c_yarncnt BETWEEN ' + FloatToStr(mQueryParameters.YarnCount-cYarnCountDelta) +
                                                 ' AND '+ FloatToStr(mQueryParameters.YarnCount+cYarnCountDelta) + ' '

    end;// if mQueryParameters.YarnCount <> '' then begin

//: ----------------------------------------------
    if mQueryParameters.ProdStateAssigned  and (fActSource=stHistory) then begin
      result := result + ' AND (t_p.c_prod_state in (1,3,5)) ';
    end;// if mmmQueryParameters.YMSetIDs <> '' then begin

//: ----------------------------------------------
    // Time ID's (Interval ID's) haben die h�here Priorit�t
    if not cbTotTimeRange.Checked and (fActSource=stHistory) then begin   //Nicht ganzer Zeitbereich gew�hlt
      result := result + ' AND (((t_p.c_prod_start>=' + FloatToStr(mQueryParameters.TimeFrom) + ') AND (t_p.c_prod_start<=' + FloatToStr(mQueryParameters.TimeTo) +
     ')) or (t_p.c_prod_end>=' + FloatToStr(mQueryParameters.TimeFrom) + ') AND (t_p.c_prod_end<=' + FloatToStr(mQueryParameters.TimeTo) + ')) ';
    end;// if mQueryParameters.TimeIDs <> '' then begin

//: ----------------------------------------------
  end;// if assigned(mQueryParameters) then begin

end;
//-----------------------------------------------------------------------------

constructor TFilter.Create(Sender: TComponent; aMaxDate: TDate);
begin
  inherited Create(Sender);
  HelpContext := GetHelpContext('WindingMaster\Zuordnung\ZUO_DLG_Filter.htm');

  mQueryParameters := TFilterQueryParameters.Create;
  InitState        := True;

  //Setzen Felder in Filterform
  tpStart.MaxDate := aMaxDate+1;
  tpStart.MinDate := (tpStart.MaxDate)-(10*365{ca.10Jahre});
  tpEnd.MaxDate := aMaxDate+1;
  tpEnd.MinDate := tpStart.MinDate;
  fActSource := stHistory;
end;
//-----------------------------------------------------------------------------


{ TFiltermmQueryParameters }

procedure TFilterQueryParameters.SetYarnCount(const Value: single);
begin
  if fYarnCount <> Value then
  begin
    fYarnCount := Value;
//    ParamChanged;
  end;
end;
//-----------------------------------------------------------------------------

procedure TFilter.edStyleNameChange(Sender: TObject);
begin
  if edStyleName.Text = '*' then
    mQueryParameters.StyleNames := ''
  else begin
    mQueryParameters.StyleNames := StringReplace(edStyleName.Text, '*', '%',[rfReplaceAll]);
    InitState := False;
  end;
end;
//-----------------------------------------------------------------------------

procedure TFilter.edLotNameChange(Sender: TObject);
begin
  if edLotName.Text = '*' then
    mQueryParameters.ProdNames := ''
  else begin
    mQueryParameters.ProdNames := StringReplace(edLotName.Text, '*', '%',[rfReplaceAll]);
    InitState := False;
  end;
end;
//-----------------------------------------------------------------------------

procedure TFilter.edTemplateNameChange(Sender: TObject);
begin
  if edTemplateName.Text = '*' then
    mQueryParameters.YMSetNames := ''
  else begin
    mQueryParameters.YMSetNames := StringReplace(edTemplateName.Text, '*', '%',[rfReplaceAll]);
    InitState := False;
  end;
end;
//-----------------------------------------------------------------------------

procedure TFilter.edYarnCountChange(Sender: TObject);
begin
//  if edYarnCount.Text = '' then
//    mQueryParameters.YarnCount := 0.0
//  else begin
    mQueryParameters.YarnCount := edYarnCount.Value;
  if edYarnCount.Text <> '' then
    InitState := False;
//  end;
end;
//-----------------------------------------------------------------------------

procedure TFilter.edYarnCountKeyPress(Sender: TObject; var Key: Char);
begin
  if Key<>'*' then
    CheckFloatChar(Key);
end;
//-----------------------------------------------------------------------------

procedure TFilter.InitBeforeShow(aActSource: TSourceTypes; aYarnUnit: TYarnUnit);
begin
  fActSource := aActSource;
  //Setzen der GarnEinheit
  laYarnCount.Caption := Format(laYarnCount.Caption, [cYarnUnitsStr[aYarnUnit]]);

  case fActSource of
    stTemplate: begin
      laStyleName.Visible := False;
      edStyleName.Visible := False;
      laLotName.Visible := False;
      edLotName.Visible := False;
//      cbTotTimeRange.Checked := True;
      cbTotTimeRange.Visible := False;
//      cbAssigned.Checked := False;
      cbAssigned.Visible := False;
      laTimeInfo.Visible := False;
      laVon.Visible := False;
      tpStart.Visible := False;
      laBis.Visible := False;
      tpEnd.Visible := False;
      laYarnCount.Visible := False;
      edYarnCount.Visible := False;
    end;
    stStyle: begin
      laLotName.Visible := False;
      edLotName.Visible := False;
//      cbTotTimeRange.Checked := True;
      cbTotTimeRange.Enabled := False;
//      cbAssigned.Checked := False;
      cbAssigned.Visible := False;
      laTimeInfo.Visible := False;
      laVon.Visible := False;
      tpStart.Visible := False;
      laBis.Visible := False;
      tpEnd.Visible := False;
    end;
    else //stHistory
      laStyleName.Visible := True;
      edStyleName.Visible := True;
      laLotName.Visible := True;
      edLotName.Visible := True;
//      cbTotTimeRange.Checked := True;
      cbTotTimeRange.Visible := True;
      cbTotTimeRange.Enabled := True;
//      cbAssigned.Checked := True;
      cbAssigned.Visible := True;
      laTimeInfo.Visible := True;
      laVon.Visible := True;
      tpStart.Visible := True;
      laBis.Visible := True;
      tpEnd.Visible := True;
      laYarnCount.Visible := True;
      edYarnCount.Visible := True;
  end; //case

end;
//-----------------------------------------------------------------------------

procedure TFilter.FormShow(Sender: TObject);
begin
  cbTotTimeRange.Checked := mTotTimeRange;
  cbAssigned.Checked := mQueryParameters.ProdStateAssigned;
//  InitState := True;
end;
//-----------------------------------------------------------------------------

procedure TFilter.SetInitState(const Value: Boolean);
begin
  if Value then begin
    cbAssigned.Checked := True;
    mQueryParameters.ProdStateAssigned := cbAssigned.Checked;
    cbTotTimeRange.Checked := True;
    mTotTimeRange := cbTotTimeRange.Checked;
    tpStart.Enabled := False;
    tpEnd.Enabled := False;
    edStyleName.Text := '*';
    edLotName.Text := '*';
    edTemplateName.Text := '*';
    edYarnCount.Text := '';
    //Zeitfelder mit Initialrange von ca. 1 Monat f�llen
    tpEnd.Date := Now;
    tpStart.Date := tpEnd.Date-(30{ca.1Monat});
    fInitState := True;
  end
  else begin
    fInitState := False;
  end;
end;
//-----------------------------------------------------------------------------
procedure TFilter.SetNoFilter(const Value: Boolean);
begin
  if Value then begin
    SetInitState(True);
    cbAssigned.Checked := False;  //Unterschied zwischen InitState und NoFilter
  end;
end;
//-----------------------------------------------------------------------------

destructor TFilter.Destroy;
begin
  FreeAndNil(mQueryParameters);

  inherited Destroy;
end;

end.
