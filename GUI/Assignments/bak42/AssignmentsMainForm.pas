(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: Main.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Assignment Applikation to start and stopp Produktin Groups
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 03.12.1999  1.00  Mg  | Projekt started
| 12.07.2001  1.01  Nue | Release 2.01.01
| 17.08.2001  1.02  Nue | cNoMachUploadDone added
| 14.01.2001  1.03  Nue | cNoMachUploadDone moved from AssignmentsMainForm to AssignComp
| 24.01.2002  1.04  Nue | Several minor changes for version 2.03.01 in several files.
| 02.10.2002        LOK | Umbau ADO
|=============================================================================*)
unit AssignmentsMainForm;           
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEAPPLMAIN, Menus, mmMainMenu, IvDictio, IvAMulti, IvBinDic,
  mmDictionary, IvMulti, IvEMulti, mmTranslator, mmLogin, mmPopupMenu,
  mmOpenDialog, IvMlDlgs, mmSaveDialog, mmPrintDialog,
  mmPrinterSetupDialog, Db, StdActns,
  ActnList, mmActionList, ImgList, mmImageList, ComCtrls, mmStatusBar,
  ToolWin, mmToolBar, mmAnimate, ExtCtrls, mmPanel,AssignForm,
  MachineSelForm, Grids, DBGrids,TemplateForm,ProdGrpCloseForm, MMSecurity,
  MMHtmlHelp,LotParameterForm, ADODB, mmADODataSet, mmADOConnection;

type
  TApplMainForm = class(TBaseApplMainForm)
    acAssign: TAction;
    acTemplate: TAction;
    acCloseProdGrp: TAction;
    MMSecurityDB1: TMMSecurityDB;
    MMHtmlHelp: TMMHtmlHelp;
    conDefault: TmmADOConnection;
    dseQuery: TmmADODataSet;
    procedure FormCreate(Sender: TObject);
    procedure acAssignExecute(Sender: TObject);
    procedure acTemplateExecute(Sender: TObject);
    procedure acCloseProdGrpExecute(Sender: TObject);
    procedure acLotParameterExecute(Sender: TObject);
    procedure mDictionaryAfterLangChange(Sender: TObject);
  private
  public
  end;

var
  ApplMainForm: TApplMainForm;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS, mmCS,
  AssignComp,
  YMParaDef, mmDialogs;
//------------------------------------------------------------------------------
  procedure TApplMainForm.FormCreate(Sender: TObject);
  var  x              : Integer;
       xComponent     : TComponent;
       xMenuItem      : TMenuItem;
       xAction        : TAction;
  begin
    // Delete all MenuItems and Menu there have the Tag = 2
    for x:= 0 to  ComponentCount -1 do begin
      xComponent := Components[x];
      // MenuItems
      if (xComponent is TMenuItem) then  begin
          xMenuItem:= Components[x] as TMenuItem;
          if xMenuItem.Tag = 2 then
          xMenuItem.Visible:=False;
      end;
      // Actions
      if (xComponent is TAction) then  begin
          xAction:= Components[x] as TAction;
          if xAction.Tag = 2 then
          xAction.Visible:=False;
      end;
    end;
  end;
//------------------------------------------------------------------------------
  procedure TApplMainForm.acAssignExecute(Sender: TObject);
  var   xForm : TAssign;
        xMachSel : TMachineSel;
        xMachine : AssignComp.TMachine;
  begin
    try
      screen.Cursor := crHourGlass;
      inherited;
      xMachSel := TMachineSel.Create ( self, dseQuery );
      if xMachSel.ShowModal = mrOK then begin
        xForm := TAssign.Create ( self, conDefault );
        xMachine := AssignComp.TMachine.Create ( nil );
        xMachine.Query := dseQuery;
        xMachine.MachineID := xMachSel.MachineID;
        //Nue: 17.8.01
        if xMachine.AWEMachType=amtUnknown then begin
          if (MMMessageDlg( Format('%s %s', [cNoMachUploadDone1,cNoMachUploadDone2]) , mtWarning , [mbOk], 0)= mrOk) then begin
            xForm.acExitExecute(self);
          end;
        end
        else begin
          xForm.SpdRange := xMachine;
          xForm.show;
          xMachine.Free;
        end;

      end;
      xMachSel.Free;
    finally
      screen.Cursor := crDefault;
    end;//
  end;
//------------------------------------------------------------------------------
  procedure TApplMainForm.acTemplateExecute(Sender: TObject);
  var   xForm : TTemplate;
  begin
    xForm := TTemplate.Create ( self, conDefault );
    with xForm do begin
      mmTranslator.DictionaryName := Self.mTranslator.DictionaryName;
      mmTranslator.Translate;
      Show;
      Init;
    end;
  end;
//------------------------------------------------------------------------------
  procedure TApplMainForm.acCloseProdGrpExecute(Sender: TObject);
  var   xForm : TProdGrpClose;
  begin
    inherited;
    xForm := TProdGrpClose.Create ( self, conDefault );
    xForm.MachineID := 0;
    xForm.LotParameterEnabled := false;
    xForm.show;
  end;
//------------------------------------------------------------------------------
  procedure TApplMainForm.acLotParameterExecute(Sender: TObject);
  var   xForm : TLotParameter;
  begin
    inherited;
    xForm := TLotParameter.Create ( self );
    xForm.ProdGrpID := -2147483571;
    xForm.Show;
  end;
//------------------------------------------------------------------------------
procedure TApplMainForm.mDictionaryAfterLangChange(Sender: TObject);
begin
  MMHtmlHelp.LoepfeIndex := mDictionary.LoepfeIndex;
end;
//------------------------------------------------------------------------------
end.

