(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: AssignHandlerComp.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.12.1999  1.00  Mg  | Datei erstellt
| 04.04.2000  1.01  Nue | xJob.StartGrpEv.MachineGrp    := MaGrpNr.GrpNr; added
| 20.04.2000  1.10  Mg  | INavChange implemented, Creation of AddYmSettings at Startup
| 15.05.2000  1.11  Mg  | State rmProdGrpStoppedOk in ProcessAssDoneMsg allowed
| 19.05.2000  1.12  Mg  | Procedures for Sumulator inserted
| 14.07.2000  1.13  Mg  | ProcessAssDoneMsg : First ErrMsg then Close Animation Window
| 08.03.2001  1.14  Nue | Handling of ProdGrpName inserted in AskAssign
| 12.07.2001  1.15  Nue | Release 2.01.01, additionals in AskAssign
| 01.10.2001  1.16  Nue | IsOnlyTemplateSetsAvailable added.
| 27.11.2001  1.17  Nue | cbProdGrpName limited to 40 and focus on that field added.
| 09.04.2002  1.18  Nue | mAskAssign.YMSetName := (SettingsNav.ActCobChooserItem as TProdGrpItem).YMSetName; added when rbProdGrp.
| 18.09.2002  1.19  Nue | Umbau ADO
| 29.01.2003        Wss | Anpassungen an neue Packages
| 14.02.2003        Wss | gMMSetup durch TMMSettingsReader.Instance ersetzt wegen Probleme mit WinXP
| 27.08.2003  1.20  Nue | Ueberarbeitung Assignment.
| 26.01.2004  1.21  Wss | Folgefenster werden nur zur Laufzeit erstellt und im Destroy ignoriert, da
                          diese über die Komponentenverwaltung automatisch wieder freigegeben werden
|=============================================================================*)
unit AssignHandlerComp;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  AssignComp, BaseGlobal, SettingsFrame, SpindleFieldsFrame,
  MiscFrame, MMUGlobal, LoepfeGlobal, SettingsNavFrame, AssignAnimationForm, mmTimer,
  AddYMSettingsForm, AskAssignForm, IvDictio, TypInfo, mmMemo, SettingsReader,
  mmDialogs, YMParaDef, //Nue 8.8.01
  ListCobChooserForm;
//Nue: 8.8.01
resourcestring
  rsBadSpdRangeMsg = '(*)Der von Ihnen gewaehlte Spulstellenbereich kann nicht zugeordnet werden.'; // ivlm
  rsAssMachineOfflineMsg = '(*)Zuordnung von Reiniger Einstellungen ist zurzeit nicht moeglich, weil die Maschine keine Verbindung zum MillMaster besitzt.'; // ivlm
  rsAssignFailedMsg = '(*)Die Zuordnung der Reiniger Einstellungen konnte nicht durchgefuehrt werden.'; // ivlm
  rsMMNotRunning = '(*)Vermutlich ist Millmaster-Basissystem abgestellt!'; //ivlm
  rsAssignTimeoutMsg = '(*)Das MillMaster Server System ist beschaeftigt. Bitte versuchen Sie die Einstellungen erneut zuzuordnen.'; // ivlm
  rsAssignProdGrpBusy = '(*)Von den gewaehlten Spulstellen werden zur Zeit Daten akquiriert. Bitte versuchen Sie die Einstellungen erneut zuzuordnen.'; // ivlm
  rsMachineOffline1 = '(*)Zuordnung von Reiniger Einstellungen nicht moeglich. Grund :'; //ivlm
  rsMachineOffline2 = '(*)Die Maschine ist ausgeschaltet, nicht am Netzwerk angeschlossen oder an der Maschine werden Einstellungen geaendert.'; // ivlm
  rsBadMachineGrpMsg = '(*)Die Gruppen Nummer ist bereits in produktion, bitte andere waehlen.'; //ivlm
  rsGrpNrAlreadyInUse = '(*)Die von Ihnen gewaehlte Gruppen Nummer ist bereits in Produktion. Bitte andere waehlen.'; //ivlm

type
//..............................................................................
  // Procedure definitions for Simulator
  TOnSimulatorAssign = procedure(aJob: PJobRec) of object;
  TOnSimulatorTimeout = procedure of object;
  TOnSumulatorServerResult = procedure(aReceivedMsg: TResponseMsgTyp) of object;
//..............................................................................
  TAssignHandler = class(TBaseHandler, INavChange)
  private
    mAssignAnimation: TAssignAnimation;
    mAskAssign: TAskAssign;
    mAddYMSettings: TAddYMSettings;
    fSpindleFields: TSpindleFields;
    fSettingsNav: TSettingsNav;
    fSpdRangeList: TSpindleRangeList;
    fSimulator: boolean;
    fOnSimAssign: TOnSimulatorAssign;
    fOnSimTimeout: TOnSimulatorTimeout;
    fOnSimServerRes: TOnSumulatorServerResult;
    fLastStartedProdGrpID: integer;
    fSpdRange: TMachine;  //Nue:21.05.03
    fSettings: TSettings;   
    procedure DoAssign;
    function AskAssign: boolean;
    procedure UpdateComponent(aBaseSpindleRange: TBaseSpindleRange; aXMLSettingsCollection: PXMLSettingsCollection; aMiscInfo: IMiscInfo);
//wss    procedure UpdateComponent(aBaseSpindleRange: TBaseSpindleRange; aMachSettings: PSettingsArr; aMiscInfo: IMiscInfo);
    function CheckUserValue: boolean;
    procedure SetSpdRangeList(aSpdRangeList: TSpindleRangeList);
    procedure SetSimulator(aSimulator: Boolean);
    procedure SetSettings(const Value: TSettings);
  protected
    procedure OnTimeout(Sender: TObject); override;
    procedure OnMsgReceived(aReceivedMsg: PResponseRec); override;
    procedure OnWndMsg(msg: TMessage); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure ProcessAssDoneMsg(aMsgTyp: TResponseMsgTyp);
    procedure ProcessAssTimeoutMsg;
    function GetIsAssignPending: boolean;
    procedure Loaded; override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function Assign: boolean;   //Vorher procedure Nue:21.8.03
    function EditAddYMSettings: boolean; // if AddSettings has changed the Result will be true
    function IsAssignAllowed(aUserMsg: boolean): boolean;
    property LastStartedProdGrpID: integer read fLastStartedProdGrpID;
    property OnSimAssign: TOnSimulatorAssign read fOnSimAssign write fOnSimAssign;
    property OnSimTimeout: TOnSimulatorTimeout read fOnSimTimeout write fOnSimTimeout;
    property OnSimServerRes: TOnSumulatorServerResult read fOnSimServerRes write fOnSimServerRes;
    property IsAssignPending: boolean read GetIsAssignPending;
    property SpdRange: TMachine read fSpdRange write fSpdRange; // Nue:21.05.03
  published
    property Settings: TSettings read fSettings write SetSettings;
    property SpindleFields: TSpindleFields read fSpindleFields write fSpindleFields;
    property SettingsNav: TSettingsNav read fSettingsNav write fSettingsNav;
    property Simulator: boolean read fSimulator write SetSimulator;
    property SpdRangeList: TSpindleRangeList read fSpdRangeList write SetSpdRangeList;
  end;
//..............................................................................
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  YMBasicProdParaBox, BASEFORM; //Nue: 17.8.01
//------------------------------------------------------------------------------
const
  cASSIGN_TIMEOUT = 90000;
  cDoAssPipeName = cAssignmentsPipeName + 'DoAss' + cAddOnChannelName;
//------------------------------------------------------------------------------
 { TAssignHandler }
//------------------------------------------------------------------------------
constructor TAssignHandler.Create(aOwner: TComponent);
begin
  inherited Create(aOwner, cDoAssPipeName);

  if not(csDesigning in ComponentState) then begin
    mAddYMSettings   := TAddYMSettings.Create(aOwner);
    mAskAssign       := TAskAssign.Create ( aOwner );
    mAssignAnimation := TAssignAnimation.Create (aOwner);
    mAssignAnimation.AnimationTyp := atAssign;
  end;

  mTimer.Interval := cASSIGN_TIMEOUT;
  fSettings := nil;
  fSpindleFields := nil;
  fSettingsNav := nil;
  fOnSimAssign := nil;
  fOnSimTimeout := nil;
  fOnSimServerRes := nil;
  fLastStartedProdGrpID := 0;
  fSimulator := False;
end;
//------------------------------------------------------------------------------
destructor TAssignHandler.Destroy;
begin
{wss, Jan 2004: da diese Fenster entweder mit aOwner oder Self erstellt werden,
                müssen diese Fenster nicht manuell freigegeben werden.
  FreeAndNil(mAssignAnimation);
  FreeAndNil(mAskAssign);
  FreeAndNil(mAddYMSettings);
{}
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TAssignHandler.Assign: Boolean;
var
  xSettingsNav: TSettingsNav;
  xSaveMode: TYMSettinsGUIMode;
  xModeSaved: Boolean;
begin
  Result := False;
  xModeSaved := False;
  try
    if not IsAssignAllowed(True) then Exit;

//Nue: 8.8.01
      if Assigned(fSettings) then begin
        xSettingsNav := (SpdRangeList.SettingsNavObject as TSettingsNav);

        xSettingsNav.UnMachineCobChooser := True;  //Maschinenabhängige Infos in cobChooser unterdrücken Nue:6.5.03

        xSaveMode := Settings.GUIMode;
        xModeSaved := True;
        if Settings.GUIMode = gmTransparent then begin
          Settings.ActMachineYMConfigRec := xSettingsNav.SpdRangeList.AssMachine.MachineYMConfigRec;

          Settings.GUIMode := gmAssignment;
        end;
      end
//End Nue
      else begin  //Nue: 14.9.04
        raise Exception.Create('fSettings not assigned!');
      end;

      try
        xSettingsNav.UnMachineCobChooser := True;  //Maschinenabhängige Infos in cobChooser unterdrücken Nue:6.5.03

        if AskAssign then begin
    //        if (xModeSaved) and (xSaveMode = gmTransparent) then begin
    //          Settings.GUIMode := gmTransparent;
    //        end; //if

          Result := True;
          try
            SendMessage(mWindowHandle, RegisterWindowMessage(ASSIGN_REG_STR), WM_DOASSIGN, 0);
            if not Simulator then begin
              mAssignAnimation.ParentWindow := (Owner as TWinControl).Handle;
              mAssignAnimation.ShowModal;

              if (xModeSaved) and (xSaveMode = gmTransparent) then begin
                //Damit der cobChooser-Schlitz mit der soeben von Mode Maschine geladenen, neuen ProdGrp refresht wird
                xSettingsNav.ActSourceType := stProdGrp;
                SpdRangeList.UpdateAllComponents;
              end; //if

            end;
          except
            on e: Exception do begin
              //Nue:24.02.04
              UserErrorMsg(Format('%s %s',[rsAssignFailedMsg,rsMMNotRunning]), Owner);
              EXIT;
            end;
          end;
        end;
      finally
        xSettingsNav.UnMachineCobChooser := False;  //Maschinenabhängige Infos in cobChooser einschalten Nue:6.5.03
      end;
/////    end;
  except
    on e: Exception do begin
      SystemErrorMsg_('TAssignHandler.Assign failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TAssignHandler.EditAddYMSettings: boolean;
begin
  Result := False;
  try
    mAddYMSettings.ParentWindow := (Owner as TWinControl).Handle;
    Result := (mAddYMSettings.ShowModal = mrOK);
    if mAddYMSettings.Changed and Assigned(fSpdRangeList) then
      fSpdRangeList.ItemChanged(ucAddSettings);
  except
    on e: Exception do begin
      SystemErrorMsg_('TAssignHandler.EditAddYMSettings failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TAssignHandler.IsAssignAllowed(aUserMsg: boolean): boolean;
begin
  Result := True;
    // Check overstart of Spindlerange
  if AssMachine.IsAbleToOverstartMaGroup then Exit; // only check if machine is not able to overstart a GrpNo
  if (ucSpindleRange in SpdRangeList.ChangedItems) then begin // only settings are allowed to change
    // Check if the grp nr is already in produktion
    if SpdRangeList.CheckGrpNr(SpindleFields.GrpNr) then begin
      Result := False;
      if aUserMsg and not Simulator then
        InfoMsg(rsGrpNrAlreadyInUse);
    end;
  end
  else begin
    // Only Settings has changed
    Result := True;
  end;
end;
//------------------------------------------------------------------------------
procedure TAssignHandler.OnTimeout(Sender: TObject);
begin
  inherited;
  SendMessage(mWindowHandle, RegisterWindowMessage(ASSIGN_REG_STR), WM_ASSIGN_TIMEOUT, 0);
end;
//------------------------------------------------------------------------------
procedure TAssignHandler.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Assigned(fSettings) then
    if (Operation = opRemove) and (AComponent = fSettings) then
      fSettings := nil;
  if Assigned(fSpindleFields) then
    if (Operation = opRemove) and (AComponent = fSpindleFields) then
      fSpindleFields := nil;
  if Assigned(fSettingsNav) then
    if (Operation = opRemove) and (AComponent = fSettingsNav) then
      fSettingsNav := nil;
  if Assigned(fSpdRangeList) then
    if (Operation = opRemove) and (AComponent = fSpdRangeList) then
      fSpdRangeList := nil;
end;
//------------------------------------------------------------------------------
procedure TAssignHandler.OnMsgReceived(aReceivedMsg: PResponseRec);
begin
  inherited;
  fLastStartedProdGrpID := aReceivedMsg.ProdGrpID;
  SendMessage(mWindowHandle, RegisterWindowMessage(ASSIGN_REG_STR), WM_ASSIGN_DONE, ORD(aReceivedMsg.MsgTyp));
end;
//------------------------------------------------------------------------------
procedure TAssignHandler.OnWndMsg(msg: TMessage);
begin
  case msg.wParam of
    WM_DOASSIGN: DoAssign;
    WM_ASSIGN_DONE: ProcessAssDoneMsg(TResponseMsgTyp(msg.lParam));
    WM_ASSIGN_TIMEOUT: ProcessAssTimeoutMsg;
  else
  end;
end;
//------------------------------------------------------------------------------
procedure TAssignHandler.ProcessAssDoneMsg(aMsgTyp: TResponseMsgTyp);
begin
  try
    if mState <> asWaitJobRespons then
      Exit;
    mState := asNoJob;

    if Simulator and Assigned(OnSimServerRes) then
      OnSimServerRes(aMsgTyp);

    case aMsgTyp of
      rmBadSpdRange: begin
          if not Simulator then begin
            UserErrorMsg(rsBadSpdRangeMsg, Owner);
            mAssignAnimation.Close;
          end;
        end;
      rmBadMachineGrp: begin
          if not Simulator then begin
            UserErrorMsg(rsBadMachineGrpMsg, Owner);
            mAssignAnimation.Close;
          end;
        end;
      rmMachineOffline: begin
          if not Simulator then begin
            UserErrorMsg(rsAssMachineOfflineMsg, Owner);
            mAssignAnimation.Close;
          end;
        end;
      rmProdGrpStartedOk: begin
          SendMessage((Owner as TForm).Handle, RegisterWindowMessage(ASSIGN_REG_STR), WM_ASSIGN_DONE, 0);
          if not Simulator then begin
            SendMessage((mAssignAnimation as TForm).Handle, RegisterWindowMessage(ASSIGN_REG_STR), WM_ASSIGN_DONE, 0);
          end;
        end;
      rmProdGrpStartedNOk: begin
          if not Simulator then begin
            UserErrorMsg(rsAssignFailedMsg, Owner);
            mAssignAnimation.Close;
          end;
        end;
      rmProdGrpStateBad: begin
          if not Simulator then begin
            InfoMsg(rsAssignProdGrpBusy);
            mAssignAnimation.Close;
          end;
        end;
      rmProdGrpStoppedOk: begin
          // This Msg is possible when ProdGrp is overstarted
          mState := asWaitJobRespons;
        end;
      rmProdGrpStoppedNOk,
        rmNodeListOnDBOk, rmNodeListOnDBNotOk: begin
          if not Simulator then begin
            mAssignAnimation.Close;
            raise Exception.Create('Unexpected response. Type = ' + GetEnumName(TypeInfo(TResponseMsgTyp), Ord(aMsgTyp)));
          end;
        end;
    else
      if not Simulator then begin
        mAssignAnimation.Close;
        raise Exception.Create('MsgTyp not defined. Type = ' + GetEnumName(TypeInfo(TResponseMsgTyp), Ord(aMsgTyp)));
      end;
    end;
  except
    on e: Exception do begin
      SystemErrorMsg_('TAssignHandler.ProcessAssDoneMsg failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TAssignHandler.ProcessAssTimeoutMsg;
begin
  mState := asNoJob;
  if not Simulator then begin
    mAssignAnimation.Close;
    InfoMsg(rsAssignTimeoutMsg);
  end
  else begin
    if Assigned(OnSimTimeout) then
      OnSimTimeout;
  end;
end;
//------------------------------------------------------------------------------
function TAssignHandler.GetIsAssignPending: boolean;
begin
  Result := not (mState = asNoJob);
end;
//------------------------------------------------------------------------------
function TAssignHandler.AskAssign: boolean;
begin
  Result := False;

  if AssMachine.IsReadyToAssign then begin
    mAskAssign.MachineName := AssMachine.MachineName;
//    mAskAssign.SpindleFirst := IntToStr(SpindleFields.SpindleFirst);
//    mAskAssign.SpindleLast := IntToStr(SpindleFields.SpindleLast);
//    mAskAssign.MachGrpNr := IntToStr(SpindleFields.GrpNr);
    mAskAssign.WishedSettings := SettingsNav.Settings;
    mAskAssign.StyleID := 0; //Nue:4.6.03 No Reference on a style

    if (SettingsNav.ActSourceType=stTemplate) then begin
      mAskAssign.StyleName := '';
      mAskAssign.YMSetName := SettingsNav.ActCobChooserItem.Name;
    end
    else if (SettingsNav.ActSourceType=stProdGrp) then begin
//      mAskAssign.StyleInfo := (SettingsNav.ActCobChooserItem as TProdGrpItem).StyleInfo; //Nue:4.6.03
      mAskAssign.StyleID := (SettingsNav.ActCobChooserItem as TProdGrpItem).StyleID; //Nue:4.6.03
      if (SettingsNav.ActCobChooserItem as TProdGrpItem).StyleID = cDefaultStyleID then begin
        mAskAssign.StyleName := '';
        mAskAssign.YMSetName := (SettingsNav.ActCobChooserItem as TProdGrpItem).YMSetName;
      end
      else begin
        mAskAssign.StyleName := SettingsNav.OrderPosition.StyleName;
//Alt:bis 9.4.02          mAskAssign.YMSetName    := '';
        mAskAssign.YMSetName := (SettingsNav.ActCobChooserItem as TProdGrpItem).YMSetName; //Nue:9.4.02
      end
    end
    else if (SettingsNav.ActSourceType=stMaMemory) then begin
      mAskAssign.StyleName := '';
      mAskAssign.YMSetName := (SettingsNav.ActCobChooserItem as TPreselectItem).YMSetName;
    end
    else if (SettingsNav.ActSourceType=stStyle) then begin
//      mAskAssign.StyleInfo := (SettingsNav.ActCobChooserItem as TStyleItem).StyleInfo; //Nue:4.6.03
      mAskAssign.StyleID := (SettingsNav.ActCobChooserItem as TStyleItem).StyleID; //Nue:4.6.03
//      mAskAssign.StyleName := SettingsNav.OrderPosition.StyleName; //Added on 12.11.01 Nue
      mAskAssign.StyleName := (SettingsNav.ActCobChooserItem as TStyleItem).Name; //Nue:16.7.03
      mAskAssign.YMSetName := (SettingsNav.ActCobChooserItem as TStyleItem).YMSetName; //Added on 31.1.02 Nue
    end
    else {if (SettingsNav.ActSourceType=stHistory) } begin
//      mAskAssign.StyleInfo := (SettingsNav.ActCobChooserItem as TProdGrpItem).StyleInfo; //Nue:4.6.03
      mAskAssign.StyleID := (SettingsNav.ActCobChooserItem as TProdGrpItem).StyleID; //Nue:4.6.03
      mAskAssign.StyleName := SettingsNav.OrderPosition.StyleName; //Added on 12.11.01 Nue
      mAskAssign.YMSetName := (SettingsNav.ActCobChooserItem as TProdGrpItem).YMSetName; //Added on 31.1.02 Nue
    end;

 //Nue Abfrage ProdgrpNamensvergabe

//      if (NOT(gMMSetup.IsPackageLabMaster) and NOT (gMMSetup.IsPackageDispoMaster) or
//         (gMMSetup.IsOnlyTemplateSetsAvailable)) then begin
    if (not TMMSettingsReader.Instance.IsComponentStyle) or TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable then begin
      mAskAssign.FillCbProdGrpName(False);
      mAskAssign.ProdGrpName := SettingsNav.ProdGrpName;
    end
    else
      case TPartieIndex(TMMSettingsReader.Instance.Value[cDefaultPartieIndex]) of
        piArtNameId: begin
            mAskAssign.FillCbProdGrpName(True);
            mAskAssign.ProdGrpName := Format('%s-%d', [SettingsNav.OrderPosition.StyleName,
              mAskAssign.GetStyleSubID(SettingsNav.OrderPosition.StyleID)]);
          end;
        piArtNameDate: begin
            mAskAssign.FillCbProdGrpName(True);
            mAskAssign.ProdGrpName := Format('%s-%s', [SettingsNav.OrderPosition.StyleName, DateTimeToStr(Now)]);
          end;
        piArtName: begin
            mAskAssign.FillCbProdGrpName(False);
            mAskAssign.ProdGrpName := SettingsNav.OrderPosition.StyleName;
          end;
        piUserInput: begin
            mAskAssign.FillCbProdGrpName(False);
            mAskAssign.ProdGrpName := Format('%s', [TMMSettingsReader.Instance.Value[cDefaultPartieName]]);
          end;
      else (*piNoneInput*)
        mAskAssign.FillCbProdGrpName(False);
      end;

//      mAskAssign.ProdGrpName  := SettingsNav.ProdGrpName;
    mAskAssign.ProdGrpColor := SettingsNav.ActColor;
    mAskAssign.MachGrpNrOverstart := SpdRangeList.CheckGrpNr(SpindleFields.GrpNr);

    if Simulator then begin
      mAssignAnimation.Simulation := True;
      mAskAssign.ProdGrpName := 'Simulator Start';
      mAskAssign.ProdGrpColor := random($00FFFFFF);
      Result := True;
      Exit;
    end;

    mAskAssign.ParentWindow := (Owner as TWinControl).Handle;
    if mAskAssign.ShowModal = mrOK then
      Result := True;
  end
  else begin
    if not Simulator then
      WarningMsg(rsMachineOffline1 + rsMachineOffline2)
    else
      OnSimServerRes(rmProdGrpStateBad);
  end;
end;
//------------------------------------------------------------------------------
procedure TAssignHandler.UpdateComponent(aBaseSpindleRange: TBaseSpindleRange; aXMLSettingsCollection: PXMLSettingsCollection; aMiscInfo: IMiscInfo);
//wss procedure TAssignHandler.UpdateComponent(aBaseSpindleRange: TBaseSpindleRange; aMachSettings: PSettingsArr; aMiscInfo: IMiscInfo);
begin
  mAddYMSettings.updateComponent(aBaseSpindleRange);
end;
//------------------------------------------------------------------------------
function TAssignHandler.CheckUserValue: boolean;
begin
  Result := True;
end;
//------------------------------------------------------------------------------
procedure TAssignHandler.SetSpdRangeList(aSpdRangeList: TSpindleRangeList);
begin
  fSpdRangeList := aSpdRangeList;
  if Assigned(fSpdRangeList) then  begin
    fSpdRangeList.add(self);

    //Zuweisen von SpindleRangeList zu Komponenten im AskAssignForm
    if not(csDesigning in ComponentState) then 
      mAskAssign.mSpindleFields.SpdRangeList := fSpdRangeList;  //Nue:8.4.03
  end;
end;
//------------------------------------------------------------------------------
procedure TAssignHandler.SetSimulator(aSimulator: Boolean);
begin
  fSimulator := aSimulator;
  if fSimulator then
    WarningMsg('Komponent AssignHandler is configurated as simulator. Only allowed by Loepfe tests.');
end;
//------------------------------------------------------------------------------
procedure TAssignHandler.DoAssign;
var
//  xJob: TJobRec;
  xJob: PJobRec;
  xBuffer: PYMSettingsByteArr;
  xXMLData: TStringList;
  xSize: DWord;
  xBinSize: Word;
begin
  try
    if not Assigned(fSettings) then
      raise Exception.Create('Settings not assigned.');
    if not Assigned(AssMachine) then
      raise Exception.Create('AssMachine not assigned. ');
    if not Assigned(fSpindleFields) then
      raise Exception.Create('SpindleFields not assigned. ');
    if not Assigned(fSettingsNav) then
      raise Exception.Create('SettingsNav not assigned. ');
    if not Assigned(mAddYMSettings) then
      raise Exception.Create('AddYMSettings not assigned. ');


{ DONE -owss :
XML: Hier werden die YMSettings von der GUI gesammelt und abgefüllt. Also
muss auch hier die Konvertierung nach XML erfolgen. xJob muss somit
dynamisch mit AllocMem erstellt werden. }
    xBuffer  := AllocMem(cBinBufferSize);
    xXMLData := TStringList.Create;
    try
// TODO wss: hier werden dann die XML Settings anstelle der binären Daten ausgelesen
      Settings.GetScreenSettings(xBuffer^, xBinSize);
      xSize := Length(xXMLData.CommaText);
      xJob  := AllocMem(GetJobHeaderSize(jtStartGrpEv) + xSize);
      with xJob^ do begin
        // JobHeader
        JobTyp                  := jtStartGrpEv;
        StartGrpEv.MachineID    := AssMachine.MachineID;
        NetTyp                  := AssMachine.NetTyp;
        // JobData
        StartGrpEv.MachineID    := AssMachine.MachineID;
        with StartGrpEv.SettingsRec do begin
          // konvertierte XMLSettings in Jobbuffer kopieren
          System.Move(PChar(xXMLData.CommaText)^, XMLData, xSize);
          SpindleFirst := SpindleFields.SpindleFirst;
          SpindleLast  := SpindleFields.SpindleLast;
          Group        := SpindleFields.GrpNr;
          ComputerName := mPort.LocalComputerName;
          MMUserName   := mAskAssign.MMSecurityControl.CurrentMMUser;
          Port         := mPort.PortName;
          Color        := mAskAssign.ProdGrpColor;

          StrLCopy(@YMSetName, PChar(mAskAssign.YMSetName), SizeOf(TText50));
          YMSetChanged := (ucSettings in SpdRangeList.ChangedItems);


//          with ProdGrpParam.ProdGrpInfo do begin
            StrPCopy(@ProdName, mAskAssign.ProdGrpName);
            OrderPositionID := SettingsNav.OrderPosition.OrderPositionID;
           OrderId          := SettingsNav.OrderPosition.OrderID;
//            c_machine_id        := AssMachine.MachineID;
            StyleID          := SettingsNav.OrderPosition.StyleID;
{ TODO -oNue : Wird MeterToProd weggelassen? 31.8.04 }
//            c_m_to_prod         := SettingsNav.OrderPosition.MeterToProd;
            Slip              := Round(SpindleFields.Slip * cSlipFactor);
{ TODO -oNue : Wird StyleName, OrderPositionName weggelassen? 31.8.04 }
//            StrPCopy(@StyleName, SettingsNav.OrderPosition.StyleName);
//            StrPCopy(@c_order_position_name, SettingsNav.OrderPosition.OrderPositionName);
//          end;
//
//          with ProdGrpParam.ProdGrpYMPara do begin
//            group         := SpindleFields.GrpNr;
//            spdl.start    := SpindleFields.SpindleFirst;
//            spdl.stop     := SpindleFields.SpindleLast;
            PilotSpindles := SpindleFields.PilotSpindle;
            YarnCnt       := ConvertYarnCountFloatToInt(SpindleFields.YarnCnt); //wss
//            yarnCntUnit   := ORD(SpindleFields.YarnCntUnit);
            YarnCntUnit   := SpindleFields.YarnCntUnit;
            NrOfThreads   := SpindleFields.ThreadCnt;
            SpeedRamp     := mAddYMSettings.SpeedRamp;
            Speed         := mAddYMSettings.Speed;
            lengthWindow  := mAddYMSettings.LengthWindow;
            lengthMode    := mAddYMSettings.LengthMode;
//          end;
        end;
      end;
    finally
      xXMLData.Free;
      FreeMem(xBuffer);
    end;


    fLastStartedProdGrpID := 0;

    if Simulator and Assigned(fOnSimAssign) then
      fOnSimAssign(xJob);

    NewJob(xJob);

  except
    on e: Exception do begin
      raise Exception.Create('TAssignHandler.Assign failed. ' + e.Message);
    end;
  end;


//  try
//    if not Assigned(fSettings) then
//      raise Exception.Create('Settings not assigned.');
//    if not Assigned(AssMachine) then
//      raise Exception.Create('AssMachine not assigned. ');
//    if not Assigned(fSpindleFields) then
//      raise Exception.Create('SpindleFields not assigned. ');
//    if not Assigned(fSettingsNav) then
//      raise Exception.Create('SettingsNav not assigned. ');
//    if not Assigned(mAddYMSettings) then
//      raise Exception.Create('AddYMSettings not assigned. ');
//
//    FillChar(xJob, sizeof(xJob), 0);
//
//    xJob.JobTyp                  := jtStartGrpEv;
//    xJob.StartGrpEv.MachineID    := AssMachine.MachineID;
//    xJob.NetTyp                  := AssMachine.NetTyp;
//    xJob.StartGrpEv.SpindleFirst := SpindleFields.SpindleFirst;
//    xJob.StartGrpEv.SpindleLast  := SpindleFields.SpindleLast;
//    xJob.StartGrpEv.MachineGrp   := SpindleFields.GrpNr;
//    xJob.StartGrpEv.ComputerName := mPort.LocalComputerName;
//    xJob.StartGrpEv.MMUserName   := mAskAssign.MMSecurityControl.CurrentMMUser;
//    xJob.StartGrpEv.Port         := mPort.PortName;
//    xJob.StartGrpEv.Color        := mAskAssign.ProdGrpColor;
//
////Alt bis 15.1.02      xJob.StartGrpEv.YM_Set_Name   := Settings.YMSetName;   //Nue:18.9.01
//    StrLCopy(@xJob.StartGrpEv.YM_Set_Name, PChar(mAskAssign.YMSetName), sizeOf(TText50)); //Nue:15.1.02
//    xJob.StartGrpEv.YM_Set_Changed := (ucSettings in SpdRangeList.ChangedItems); //Nue:03.10.01
//    xBuffer  := AllocMem(cBinBufferSize);
//    xXMLData := TStringList.Create;
//    try
//      Settings.GetScreenSettings(xBuffer, xSize);
//
//    finally
//      xXMLData.Free;
//      FreeMem(xBuffer);
//    end;
//
//    Settings.GetScreenSettings(xJob.StartGrpEv.DataArray, xJob.StartGrpEv.LengthData);
//
//    with xJob.StartGrpEv.ProdGrpParam.ProdGrpInfo do begin
//      StrPCopy(@c_prod_name, mAskAssign.ProdGrpName);
//      c_order_position_id := SettingsNav.OrderPosition.OrderPositionID;
//      c_order_id          := SettingsNav.OrderPosition.OrderID;
//      c_machine_id        := AssMachine.MachineID;
//      c_style_id          := SettingsNav.OrderPosition.StyleID;
//      c_m_to_prod         := SettingsNav.OrderPosition.MeterToProd;
//      c_slip              := Round(SpindleFields.Slip * cSlipFactor);
//      StrPCopy(@c_style_name, SettingsNav.OrderPosition.StyleName);
//      StrPCopy(@c_order_position_name, SettingsNav.OrderPosition.OrderPositionName);
//    end;
//
//    with xJob.StartGrpEv.ProdGrpParam.ProdGrpYMPara do begin
//      group         := SpindleFields.GrpNr;
//      spdl.start    := SpindleFields.SpindleFirst;
//      spdl.stop     := SpindleFields.SpindleLast;
//      pilotSpindles := SpindleFields.PilotSpindle;
//      yarnCnt       := ConvertYarnCountFloatToInt(SpindleFields.YarnCnt); //wss
////        yarnCnt       := Round ( Misc.YarnCnt * cYarnCntFactor );
//      yarnCntUnit   := ORD(SpindleFields.YarnCntUnit);
//      nrOfThreads   := SpindleFields.ThreadCnt;
//      speedRamp     := mAddYMSettings.SpeedRamp;
//      speed         := mAddYMSettings.Speed;
//      lengthWindow  := mAddYMSettings.LengthWindow;
//      lengthMode    := mAddYMSettings.LengthMode;
//    end;
//
//    fLastStartedProdGrpID := 0;
//
//    if Simulator and Assigned(OnSimAssign) then
//      OnSimAssign(xJob);
//
//    NewJob(xJob);
//
//  except
//    on e: Exception do begin
//      raise Exception.Create('TAssignHandler.Assign failed. ' + e.Message);
//    end;
//  end;
end;
//------------------------------------------------------------------------------
procedure TAssignHandler.SetSettings(const Value: TSettings);
begin
  fSettings := Value;

  if csDesigning in ComponentState then exit;

  if Assigned(fSettings) and not(csDesigning in ComponentState) then
    mAskAssign.mSpindleFields.Settings := fSettings;  //Nue:18.6.03
end;

//------------------------------------------------------------------------------

procedure TAssignHandler.Loaded;
begin
  inherited Loaded;
{ TODO 2 -oNue : Die folgenden Zuweisungen brauchts eigentlich nicht mehr. Aber Achtung! Konsistenz bei weglöschen beachten!(Nue:17.6.03) }
  if csDesigning in ComponentState then exit;
  try
    if Assigned(mAskAssign.mSpindleFields) then
      fSpindleFields :=  mAskAssign.mSpindleFields;
  finally
  end;

end;
//------------------------------------------------------------------------------

end.

