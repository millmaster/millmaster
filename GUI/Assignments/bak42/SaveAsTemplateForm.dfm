inherited SaveAsTemplate: TSaveAsTemplate
  Left = 482
  Top = 264
  Caption = '(20)Angabe zur Vorlage'
  ClientHeight = 140
  ClientWidth = 259
  PixelsPerInch = 96
  TextHeight = 13
  object mmLabel1: TmmLabel [0]
    Left = 8
    Top = 25
    Width = 142
    Height = 13
    Caption = '(20)Name der neuen Vorlage :'
    FocusControl = edTemplateName
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  inherited bOK: TmmButton
    Left = 59
    Top = 110
    ModalResult = 0
    TabOrder = 1
    OnClick = bOKClick
  end
  inherited bCancel: TmmButton
    Left = 159
    Top = 110
    TabOrder = 2
  end
  object edTemplateName: TmmMaskEdit
    Left = 8
    Top = 48
    Width = 185
    Height = 21
    MaxLength = 20
    TabOrder = 0
    Visible = True
    AutoLabel.Control = mmLabel1
    AutoLabel.Distance = 10
    AutoLabel.LabelPosition = lpTop
  end
  object bColor: TmmColorButton
    Left = 208
    Top = 48
    Width = 33
    Height = 21
    AutoLabel.LabelPosition = lpLeft
    Caption = '&Weitere...'
    Color = clBlack
    TabOrder = 3
    TabStop = True
    Visible = True
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 232
    Top = 8
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
end
