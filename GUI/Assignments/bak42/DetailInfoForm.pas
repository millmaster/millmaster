unit DetailInfoForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, ComCtrls, mmTreeView, StdCtrls, mmLabel, VirtualTrees,
  ExtCtrls, mmPanel, IvDictio, IvMulti, IvEMulti,
  mmTranslator, mmTimer, AssignComp, mmMemo, StyleUserFields, mmRichEdit;

type
  TfrmDetailInfo = class (TmmForm)
    laStyleName: TmmLabel;
    laYTitle: TmmLabel;
    mmLabel2: TmmLabel;
    pnMain: TmmPanel;
    mmTranslator1: TmmTranslator;
    timClose: TmmTimer;
    StyleUserFields: TStyleUserFields;
    memoInfo: TmmRichEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDeactivate(Sender: TObject);
    procedure timCloseTimer(Sender: TObject);
    procedure ControlsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ControlsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
  private
    mMousePoint: TPoint;
  protected
  public
    constructor Create(aOwner: TComponent; aStyleID: Integer; aStyleName: string); reintroduce; virtual;
    destructor Destroy; override;
  end;

var
  frmDetailInfo: TfrmDetailInfo;

implementation
{$R *.DFM}
uses
  mmCS, LoepfeGlobal,
  MMUSBSelectNavigatorFrame, LabMasterDef;

//:---------------------------------------------------------------------------
//:--- Class: TfrmDetailInfo
//:---------------------------------------------------------------------------
constructor TfrmDetailInfo.Create(aOwner: TComponent; aStyleID: Integer; aStyleName: string);
var
  xCount: Integer;
  xTab: Array[1..1] of Integer;
begin
  inherited Create(aOwner);

  try
    laStyleName.Caption := aStyleName;
    StyleUserFields.StyleID := aStyleID;   //Aufruf um DetailStyleInfo-Daten ab DB zu holen

    xTab[1] := 0;

    //L�ngster Text aus StyleUserFields.Title bestimmen und anhand dieses Wertes die Tabulatoren setzen
    //Holen der Werte aus der StyleUserFields-Komponente und ins Memo-Field schreiben
    for xCount:=0 to StyleUserFields.FieldCount-1 do begin
      if Canvas.TextWidth(StyleUserFields.Title[xCount])>xTab[1] then
        xTab[1] := Canvas.TextWidth(StyleUserFields.Title[xCount]);
    end; //for

//    xTab[1] := xTab[1]+5;
    //Tabs f�r Memo
    SendMessage(memoInfo.Handle, EM_SETTABSTOPS, 1, Integer(@xTab));
    memoInfo.Invalidate;

  except
    on e: Exception do begin
      raise Exception.Create('TfrmDetailInfo.Create (Db Access failed.) ' + e.Message);
    end;
  end;
end;

//:---------------------------------------------------------------------------
destructor TfrmDetailInfo.Destroy;
begin
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TfrmDetailInfo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caHide;
end;

//----------------------------------------------------------------------------
procedure TfrmDetailInfo.FormDeactivate(Sender: TObject);
begin
  Close;
end;

//----------------------------------------------------------------------------
procedure TfrmDetailInfo.timCloseTimer(Sender: TObject);
begin
  Close;
end;
//------------------------------------------------------------------------------
procedure TfrmDetailInfo.ControlsMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if ssLeft in Shift then begin
    Left := Left + (X - mMousePoint.x);
    Top  := Top + (Y - mMousePoint.y);
    timClose.Restart;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmDetailInfo.ControlsMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  mMousePoint := Point(X, Y);
end;
//------------------------------------------------------------------------------
procedure TfrmDetailInfo.FormShow(Sender: TObject);
var
  xCount:Integer;
  
begin
  with memoInfo do begin
    Clear;
    //Holen der Werte aus der StyleUserFields-Komponente und ins Memo-Field schreiben
    for xCount:=0 to StyleUserFields.FieldCount-1 do begin
      memoInfo.Lines.Add(Format('   %s:'#9'%s',[StyleUserFields.Title[xCount], StyleUserFields.Value[xCount]]));
    end;
  end;//with

  timClose.Restart;
end;

end.




