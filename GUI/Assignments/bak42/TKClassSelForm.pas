(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: TKClassSelForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows 2000
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 21.08.2003  1.00  Nue | Datei erstellt
| 27.08.2003        Nue | Ueberarbeitung Assignment.
|=============================================================================*)
unit TKClassSelForm;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, StdCtrls, Buttons, mmBitBtn, mmListBox,
  IvDictio, IvMulti, IvEMulti, mmTranslator, LoepfeGlobal,BaseGlobal,
  mmButton;

type
//..............................................................................
  TTKClassSel = class(TDialogBottom)
    mmListBox1: TmmListBox;
    mmTranslator1: TmmTranslator;
    procedure bOKClick(Sender: TObject);
    procedure mmListBox1DblClick(Sender: TObject);
  private
    fItemID : integer;
    procedure InitItemID;
  public
    constructor Create ( aOwner : TComponent; aStringList : TStringList); reintroduce; virtual;
    property ItemID  : integer read fItemID;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
{$R *.DFM}
  //------------------------------------------------------------------------------
{ TTKClassSel }
//------------------------------------------------------------------------------
  constructor TTKClassSel.Create ( aOwner : TComponent; aStringList : TStringList );
  begin
    inherited Create ( aOwner );
    try
      mmListBox1.Clear;
      mmListBox1.Items.Assign(aStringList);
      if mmListBox1.Items.Count > 0 then
        mmListBox1.ItemIndex := 0;
    except
      on e:Exception do begin
        SystemErrorMsg_('TTKClassSel.Create failed. ' + e.Message );
      end;
    end;
  end;
//------------------------------------------------------------------------------
  procedure TTKClassSel.InitItemID;
  begin
    if mmListBox1.Items.Count > 0 then
      fItemID := mmListBox1.ItemIndex;
  end;
//------------------------------------------------------------------------------
  procedure TTKClassSel.bOKClick(Sender: TObject);
  begin
    inherited;
    InitItemID;
  end;
//------------------------------------------------------------------------------
  procedure TTKClassSel.mmListBox1DblClick(Sender: TObject);
  begin
    inherited;
    {
    InitMachineID;
    Close;
    self.ModalResult := mrOK;
    }
  end;
//------------------------------------------------------------------------------
end.
