(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: SettingsFrame.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 13.12.1999  1.00  Mg  | Datei erstellt
| 20.04.2000  1.10  Mg  | Spindelranges inserted
| 11.07.2000  1.11  Mg  | TSettings.UpdateComponent : ConfigCode always initialize
| 30.01.2001  1.12  Nue | File added to project.
| 02.07.2001  1.13  Wss | implementation of Loaded moved to SetQuery because the Templateform is created at runtime
| 12.07.2001  1.14  Nue | Several changes for release 2.01.01
| 18.09.2001  1.15  Nue | Handling of YM_set_name added.
| 03.10.2001  1.16  Nue | GetScreenSettings new as function. Functionvalue indicates Changed if True.
| 19.12.2001  1.17  Nue | mYMSettings.LoadFromDB from SettingsNavFrame to SettingsFrame.
| 20.12.2001        Nue | ActSensingHeadClass added.
| 14.02.2002  1.18  Nue | mYMSettings.ProdGrpYarnCount neu effektiver YarnCount zugewiesen (NIcht mehr normiert auf Nm).
| 28.02.2002  1.19  Nue | Changings because of yarnCnt-Handling.
| 10.04.2002  1.20  Nue | fFirstCall added because the TSettings.UpdateComponent will be called before TSettingsNav.UpdateComponent.
| 02.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
| 07.11.2002        LOK | In TSettings.UpdateComponent wird visible auf true gesetzt. F�r Maschinen
|                       | mit Fix Spindle Range kann visible = false sein, da die Daten zuerst von der Maschine
|                       | geladen werden m�ssen und w�hrend dieser Zeit die Settings nicht angezeigt werden d�rfen.
|                       | (siehe 'TAssign.SetSpdRange()')
| 09.10.2003  1.21  Nue | TClearerSettings from ClearerSettingsForm not further used.
| 03.12.2003  1.22  Nue | SaveSettingsAsTemplate(aColor: TColor; aItem: TBaseItem) und Templatehandling im Zu-
|                       | sammenhang mit Artikel (TSaveTemplateDLGForm) eingef�gt.
| 26.01.2004  1.22  Wss | alter Code in Loaded entfernt
| 23.02.2004  1.23  Wss | PreselectItem m�glich als Template zu speichern.
|=============================================================================*)
unit SettingsFrame;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  AssignComp, YMSettingsGUI, IvDictio,
  MMUGlobal, BaseGlobal, LoepfeGlobal, YMParaDef,
  YMBasicProdParaBox, mmAdoDataset, AdoDB, //Nue: 30.07.01 wegen TYMSettingsGUIMode
  SettingsReader, SaveTemplateDLG, XMLDef; //Nue:2.12.03

const
  cNewTemplateSuccessfulCreated =
    '(*)Neue Vorlage wurde erfolgreich erstellt.'; //ivlm
  cTemplateSuccessfulModified =
    '(*)Vorlage wurde erfolgreich veraendert.'; //ivlm
//alt bis 14.2.02  cTruncCorrection = 0.0001;
  cTruncCorrection = 0.001;

type
  TSettings = class(TFrame, INavChange)
  private
//    mSaveAsTemplate: TSaveAsTemplate;
//    mYarnUnit: TYarnUnits;
    mYMSettings: TGUIYMSettings;
    fYMSetName: TText50;
    fSpdRangeList: TSpindleRangeList;
    fQuery: TmmADoDataset;
    fOnValueChanged: TOnValueChange;
    fActMachineYMConfigRec: TMachineYMConfigRec;
    fFirstCall: Boolean; //Nue:10.4.02
    procedure SetSpdRangeList(aSpdRangeList: TSpindleRangeList);
    function CheckUserValue: boolean;
    procedure SetYarnCnt(aYarnCnt: Double);
    procedure SetSpeed(aSpeed: integer);
    procedure SetSpeedRamp(aSpeedRamp: integer);
    procedure SetPilotSpindles(aPilotSpindles: integer);
//    function  GetSpindleFirst():Integer;
//    function  GetSpindleLast():Integer;

    procedure SetSpindleFirst(const Value: Integer);
    procedure SetSpindleLast(const Value: Integer);
    function GetReadOnly: Boolean;
    procedure SetReadOnly(aReadOnly: Boolean);
    function GetGUIMode: TYMSettinsGUIMode; //Nue 17.8.01
    procedure SetGUIMode(aYMSettingsGUIMode: TYMSettinsGUIMode); //Nue 17.8.01
    procedure SetCurveSelection(aEnable: Boolean); //Nue:22.8.01
    procedure OnChange(aOwner: TComponent);
    function GetActSensingHeadClass: TSensingHeadClass;
    procedure SetActMachineYMConfigRec(aActMachineYMConfigRec: TMachineYMConfigRec);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetQuery(aQuery: TmmAdoDataset);
    procedure ValueChanged;
  public
/////Neu 14.4.03 von private to public
    procedure UpdateComponent(aProdGrp: TBaseSpindleRange; aXMLSettingsCollection: PXMLSettingsCollection; aMiscInfo: IMiscInfo);
//wss    procedure UpdateComponent(aProdGrp: TBaseSpindleRange; aMachSettings: PSettingsArr; aMiscInfo: IMiscInfo);
    //Nue: 7.8.01
    function CheckEqualTKClassInRangeFromProdGrp(aFirstSpindle, aLastSpindle: Integer; aTKClass: TSensingHeadClass): Boolean;
    //Nue: 7.8.01
    function CheckEqualTKClassInRange(aFirstSpindle, aLastSpindle: Integer): Boolean;

    procedure LoadFromDB(aSetID: integer);
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function GetScreenSettings(var aSettings: TYMSettingsByteArr; var aSize: Word): Boolean;
    procedure SetScreenSettings(aSettings: TYMSettingsByteArr);
//    function SaveSettingsAsTemplate(aColor: TColor): string; overload;
    function SaveSettingsAsTemplate(aColor: TColor; aItem: TBaseItem; aCallType: TCallType; aVisualType: TTemplateType = ttNone): string; //overload;
    procedure Update(aSetID: integer);
//    procedure UpdateColor(aSetID: integer; aColor: TColor);
    procedure Delete(aSetID: integer);
    procedure New;
//    function GetActMachineYMConfigRec: TMachineYMConfigRec;

    property OnValueChanged: TOnValueChange read fOnValueChanged write fOnValueChanged; // only used in Template
    property YarnCnt: Double write SetYarnCnt; // no read because YarnCount will be truncated
    property Speed: integer write SetSpeed;
    property SpeedRamp: integer write SetSpeedRamp;
    property PilotSpindles: integer write SetPilotSpindles;
    property SpindleFirst: Integer write SetSpindleFirst;
    property SpindleLast: Integer write SetSpindleLast;
    property ActMachineYMConfigRec: TMachineYMConfigRec read fActMachineYMConfigRec write SetActMachineYMConfigRec; //Nue 9.7.01
    property GUIMode: TYMSettinsGUIMode read GetGUIMode write SetGUIMode; //Nue 17.8.01
    property CurveSelection: Boolean write SetCurveSelection;
    property YMSetName: TText50 read fYMSetName write fYMSetName; //Nue 18.9.01
    property ActSensingHeadClass: TSensingHeadClass read GetActSensingHeadClass; //Nue 20.12.01
  published
    property SpdRangeList: TSpindleRangeList read fSpdRangeList write SetSpdRangeList;
    property Query: TmmAdoDataset read fQuery write SetQuery;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly;
  end;
//procedure Register;
implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS, mmADOCommand,

//  SetupMMDefaults,
  TemplateForm, //Nue: 30.07.01 wegen TTemplate
  SpindleFieldsFrame, //Nue: 7.8.01 wegen edFirst und edLast
  SettingsNavFrame, ListCobChooserForm, //Nue 6.7.01
//  ClearerSettingsForm, //Nue 04.12.02
  mmCS, //Nue 15.04.03
  AssignForm; //Nue:21.8.03
const
  cUpdateColorQuery =
    'update t_ym_settings ' +
    'set c_color = :c_color ' +
    'where c_ym_set_id = :c_ym_set_id';
//------------------------------------------------------------------------------
{
  procedure Register;
  begin
    RegisterComponents('Assignments', [TSettings]);
  end;
{}
//------------------------------------------------------------------------------
constructor TSettings.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
//  mSaveAsTemplate := nil;
  fQuery := nil;
  fFirstCall := True;
{Nue 4.7.01: Rausgenommen fuer Tests Kr. Braucht es das wirklich? Ev. fuer Simulator? Sequenz wird definitv in SetQuery aufgerufen.
}

  mYMSettings := TGUIYMSettings.CreateStandAlone(self);
  mYMSettings.Parent := self;
  mYMSettings.Align := alClient;
  mYMSettings.OnChange := OnChange;
{}
end;
//------------------------------------------------------------------------------
destructor TSettings.Destroy;
begin
// Wss: will be freed automatically because it is created with Create(Self) as owner
//    mYMSettings.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TSettings.GetScreenSettings(var aSettings: TYMSettingsByteArr; var aSize: Word): Boolean;
begin
  try
    Result := mYMSettings.GetFromScreen(aSettings, aSize);
  except
    on e: Exception do begin
      raise Exception.Create('TSettings.GetScreenSettings failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TSettings.SetScreenSettings(aSettings: TYMSettingsByteArr);
begin
  try
    mYMSettings.PutToScreen(aSettings);
  except
    on e: Exception do begin
      raise Exception.Create('TSettings.SetScreenSettings failed. ' + e.Message);
    end;
  end;
end;

//------------------------------------------------------------------------------
//function TSettings.SaveSettingsAsTemplate(aColor: TColor): string;
//var xSetID: integer;
//begin
//  Result := '';
//  try
//    mSaveAsTemplate := TSaveAsTemplate.Create(Self, fQuery);
//    mSaveAsTemplate.ParentWindow := (Owner as TWinControl).Handle;
//    mSaveAsTemplate.Color := aColor;
//   //Nue 13.12.01
//    mSaveAsTemplate.SensingHeadClass := mYMSettings.ActSensingHeadClass;
//
//    if mSaveAsTemplate.ShowModal = mrOK then begin
//      xSetID := mYMSettings.NewTemplate(mSaveAsTemplate.TemplateName);
//      UpdateColor(xSetID, mSaveAsTemplate.Color);
//      Result := mSaveAsTemplate.TemplateName;
//      InfoMsg(Translate(cNewTemplateSuccessfulCreated));
//    end;
//    mSaveAsTemplate.Free;
//  except
//    on e: Exception do begin
//      SystemErrorMsg_('TSettings.SaveSettingsAsTemplate failed. ' + e.Message);
//    end;
//  end;
//end;
//------------------------------------------------------------------------------

function TSettings.SaveSettingsAsTemplate(aColor: TColor; aItem: TBaseItem; aCallType: TCallType; aVisualType: TTemplateType = ttNone): string;
const
  cxQrySelStyles = 'select distinct(c_style_id) from t_style_settings where c_ym_set_id=:c_ym_set_id';
  cxQryGetYMSetID = 'select y.c_ym_set_id c_ym_set_id from t_style_settings ts, t_ym_settings y where '+
    '(ts.c_style_id=:c_style_id) and (ts.c_ym_set_id=y.c_ym_set_id) and (y.c_head_class=:c_head_class) ';
  cxQrySelYMSetInfo = 'select c_ym_set_name, c_color, c_template_set from t_ym_settings where c_ym_set_id=:c_ym_set_id';
  cxQryIsVorlage = 'select c_ym_set_id, c_template_set from t_ym_settings where (c_ym_set_name=:c_ym_set_name) and (c_head_class=:c_head_class)';
var
  xSetID, xTemplateSetID: integer;
  xSetName, xColor: string;
//wss  xNew,
  xIsVorlage: Boolean;

  //--local-----------------------------------------------------------------------------
  function CheckVorlageExists(aYMSetName: string; aHeadClass: Integer): Integer;
  //Checks if there is a template-setting available with the given aYMSetName: string; and aHeadClass: Integer
  begin
    Result := 0;
    try
      with fQuery do begin
        Close;
        CommandText := cxQryIsVorlage;
        Parameters.ParamByName('c_ym_set_name').value := aYMSetName;
        Parameters.ParamByName('c_head_class').value := aHeadClass;
        Open;
        if ( not EOF ) then begin
          if FieldByName ( 'c_template_set' ).AsBoolean then
            Result := FieldByName ( 'c_ym_set_id' ).AsInteger;
        end; //if
      end; //with
    except
      on e:Exception do begin
        raise Exception.Create ( 'TSettings.SaveSettingsAsTemplate cxQrySelYMSetInfo failed. ' + e.Message );
      end;
    end;
  end;

  //--local-----------------------------------------------------------------------------
  procedure SelYMSetInfo(aSetID: Integer; var aIsVorlage: Boolean);
  //Get Infos from settings with ID aSetID
  begin
    xSetName := 'NoName';
    xColor := '0';
    aIsVorlage := False;
    try
      with fQuery do begin
        Close;
        CommandText := cxQrySelYMSetInfo;
        Parameters.ParamByName('c_ym_set_id').value := aSetID;
        Open;
        while ( not EOF ) do begin
          xSetName := FieldByName ( 'c_ym_set_name' ).AsString;
          xColor := FieldByName ( 'c_color' ).AsString;
          aIsVorlage := FieldByName ( 'c_template_set' ).AsBoolean;
          Next;
        end; //while
      end; //with
    except
      on e:Exception do begin
        raise Exception.Create ( 'TSettings.SaveSettingsAsTemplate cxQrySelYMSetInfo failed. ' + e.Message );
      end;
    end;
  end;

  //--local-----------------------------------------------------------------------------
  function SelStyles(aSetID: Integer): string;
  //Returns a commatext separeted string with all StyleId's referencing on the template-set with ID aSetID
//  var
//wss    xStrList: TStringList;
  begin
    Result := '';
    try
      with fQuery do begin
        Close;
        CommandText := cxQrySelStyles;
        Parameters.ParamByName('c_ym_set_id').value := aSetID;
        Open;
        with TStringList.Create do try
          while ( not EOF ) do begin
            Add(FieldByName ( 'c_style_id' ).AsString);
            Next;
          end; //while
          Result := CommaText;
        finally
          free;
        end;
      end; //with
    except
      on e:Exception do begin
        raise Exception.Create ( 'TSettings.SaveSettingsAsTemplate cxQrySelYMSetInfo failed. ' + e.Message );
      end;
    end;
  end;

//--end local-----------------------------------------------------------------------------


begin
  Result := '';
  try
    if not Assigned(fQuery) then
      raise Exception.Create('Query not assigned. ');
//      mSaveAsTemplate := TSaveAsTemplate.Create ( nil, fQuery );

    //Einbau neues Handling von Templatezuweisungen 1.12.03 Nue
//    if TMMSettingsReader.Instance.IsComponentStyle and not TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable and
//       (aItem is TBaseSetIDItem) {Kein Call aus Maschinensicht} then begin

      with TSaveTemplateDLGForm.Create(self, aCallType, aVisualType) do try
        // set properties to dialog
        SensingHeadClass   := mYMSettings.ActSensingHeadClass;;
        StyleIDs      := '';
        IsTemplateSet := True;

        if (aItem is TBaseSetIDItem) {Kein Call aus Maschinensicht} then begin
          YMSettingID := IntToStr((aItem as TBaseSetIDItem).SetID);
          SelYMSetInfo((aItem as TBaseSetIDItem).SetID, xIsVorlage); //Local procedure call
          TemplateName  := (aItem as TBaseSetIDItem).Name;
          Color         := (aItem as TBaseSetIDItem).Color;

          xTemplateSetID := (aItem as TBaseSetIDItem).SetID;
        end
        else begin   //PreselectItem
          YMSettingID := '0';
          xIsVorlage := False;
          TemplateName  := '';
          Color         := cDefaultStyleIDColor;
          xTemplateSetID := 0;
        end;

        if TMMSettingsReader.Instance.IsComponentStyle and not TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable then begin
        //Artikelverwaltung
          if aItem is TProdGrpItem then begin  //stProdGrp, stHistory
            IsTemplateSet := False;
            TemplateName  := (aItem as TProdGrpItem).YMSetName;
            xTemplateSetID := CheckVorlageExists(TemplateName, ORD((aItem as TBaseSetIDItem).HeadClass));
            if xTemplateSetID<>0 then begin
              StyleIDs := SelStyles(xTemplateSetID);
              //Es muss nicht sein, dass der aktuelle Artikel nun in der Liste StyleIDs enthalten ist!!!!!!
              if StyleIDs = '' then begin
                //Kein Artikel ist dem Templateset zugeordnet.
                TemplateType := ttModifyTemplate;  //Has to be set before call of TSaveTemplateDLGForm.Init!!
              end;
            end
            else begin
              //Kein TemplateSet gefunden
              TemplateType := ttActualSetIsNoTemplate; //Has to be set before call of TSaveTemplateDLGForm.Init!!
            end;
          end
          else if aItem is TStyleItem then begin  //stOrderPosition, stStyle
            IsTemplateSet := False;
            TemplateName  := (aItem as TStyleItem).YMSetName;
            StyleIDs := SelStyles((aItem as TBaseSetIDItem).SetID);
          end
          else if aItem is TTemplateItem then begin  //stTemplate
            IsTemplateSet := False;
            TemplateName  := (aItem as TTemplateItem).Name;
            StyleIDs := SelStyles((aItem as TTemplateItem).SetID);
          end

          else if aItem is TPreselectItem then begin  //stPreselect
            //Kein TemplateSet gefunden
            TemplateType := ttActualSetIsNoTemplate; //Has to be set before call of TSaveTemplateDLGForm.Init!!
          end;

        end
        else begin //Templateverwaltung
          if aItem is TProdGrpItem then begin  //stProdGrp, stHistory
            TemplateName  := (aItem as TProdGrpItem).YMSetName;
            if NOT xIsVorlage then begin
              //Kein TemplateSet gefunden
              TemplateType := ttActualSetIsNoTemplate;
            end
          end
          else if aItem is TStyleItem then begin  //stOrderPosition, stStyle
            TemplateName  := (aItem as TStyleItem).YMSetName;
          end

          else if aItem is TPreselectItem then begin  //stPreselect
            //Kein TemplateSet gefunden
            TemplateType := ttActualSetIsNoTemplate; //Has to be set before call of TSaveTemplateDLGForm.Init!!
          end

          else begin
          end
        end;
//@@        OverrideAllow      := True; //False;
//@@        SingleNameOnly     := True;

//        Init;

        if ShowModal = mrOK then begin
//wss          xNew := False;
          if ((TemplateType = ttModifySettingsToAllAssignedStyles) or (TemplateType =  ttModifyTemplate)) then begin
          //Update bestehendes Template
//            Self.Update((aItem as TBaseSetIDItem).SetID); //Update der Reinigereinstellungen
//            xSetID := (aItem as TBaseSetIDItem).SetID;
            Self.Update(xTemplateSetID); //Update der Reinigereinstellungen
            xSetID := xTemplateSetID;
          end
          else if (TemplateType = ttRenameTemplateOnly) then
          begin
//            xSetID := (aItem as TBaseSetIDItem).SetID;
            xSetID := xTemplateSetID;
            //Do NOTHING in this case; Update of Color or/and TemplateName in RebuildDBConsistensy
          end
          else begin
          //Neues Reinigerset mit c_template_set=true
            xSetID := mYMSettings.NewTemplate(TemplateName);  //Einf�gen neue Reinigereinstellungen
//wss            xNew := True;
            // Color or/and TemplateName speichern in RebuildDBConsistensy
          end;
          RebuildDBConsistensy(xSetID);  //In TSaveTemplateDLGForm werden hier die n�tigen von xSetID tangierten, artikelabh�ngigen DB-Tabellen nachgef�hrt.

          Result := TemplateName;
//13.1.04 Nue: Nachfolgende Infomeldungen wurden von Kast als �berfl�ssig taxiert!
//          if xNew then
//            InfoMsg(Translate(cNewTemplateSuccessfulCreated))
//          else
//            InfoMsg(Translate(cTemplateSuccessfulModified));

        end; //if
      finally
        Free;
      end;
//    end //IF
//    else begin
//      Result := SaveSettingsAsTemplate(aColor);
//    end
  except
    on e: Exception do begin
      SystemErrorMsg_('TSettings.SaveSettingsAsTemplate failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TSettings.Update(aSetID: integer);
begin
  try
    mYMSettings.UpdateOnDB(aSetID);
  except
    on e: Exception do begin
      raise Exception.Create('TSettings.Update failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
//procedure TSettings.UpdateColor(aSetID: integer; aColor: TColor);
//begin
//  try
//    if not Assigned(fQuery) then
//      raise Exception.Create('Query not assigned. ');
//
//    with TmmADOCommand.Create(nil) do begin
//      try
//        Connection := fQuery.Connection;
//        CommandText := cUpdateColorQuery;
//        Parameters.ParamByName('c_ym_set_id').value := aSetID;
//        Parameters.ParamByName('c_color').value := aColor;
//        Execute;
//      finally
//        Free;
//      end; // try finally
//    end; // with TmmADOCommand.Create(nil)do begin
//  except
//    on e: Exception do begin
//      raise Exception.Create('TSettings.UpdateColor failed. ' + e.Message);
//    end;
//  end;
//end;
//------------------------------------------------------------------------------
procedure TSettings.Delete(aSetID: integer);
begin
  try
    mYMSettings.Delete(aSetID);
  except
    on e: Exception do begin
      raise Exception.Create('TSettings.Delete failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TSettings.New;
begin
  mYMSettings.PutDefaultToScreen;
//Nue:10.9.01: SetMaxSensingHeadClass should be called only here, because of programmlogic of KR!!?
  mYMSettings.SetMaxSensingHeadClass; //Nue: 17.8.01  Makes the facility's higest TK-Class visilble with the before loaded settings
end;
//------------------------------------------------------------------------------
procedure TSettings.SetSpdRangeList(aSpdRangeList: TSpindleRangeList);
begin
  fSpdRangeList := aSpdRangeList;
  if Assigned(fSpdRangeList) then
    fSpdRangeList.add(self);
end;
//------------------------------------------------------------------------------
procedure TSettings.UpdateComponent(aProdGrp: TBaseSpindleRange; aXMLSettingsCollection: PXMLSettingsCollection; aMiscInfo: IMiscInfo);
//wss procedure TSettings.UpdateComponent(aProdGrp: TBaseSpindleRange; aMachSettings: PSettingsArr; aMiscInfo: IMiscInfo);
var
  xSettingsNav: TSettingsNav;
  xMode: TYMSettinsGUIMode;
  xText50: TText50;
begin
EnterMethod('TSettings.UpdateComponent');
  try
    xSettingsNav := (SpdRangeList.SettingsNavObject as TSettingsNav);
    if (xSettingsNav.ActSourceType = stMaMemory) then begin
      xMode := gmTransparent; //Nue: 30.07.01
    end
    else begin
      xMode := gmAssignment; //Nue: 30.07.01
    end;
    mYMSettings.Mode := xMode; //Nue: 29.08.01

      //Before Mode=gmAssignment always set MachineYMConfigRec of actual selected machine first.
    if (Assigned(aProdGrp)) and
      (mYMSettings.Mode <> gmTransparent) then begin //Nue added 16.1.02
        //Selektierter Spindlerange
      mYMSettings.MachineConfig := aProdGrp.MachineYMConfigRec;
      mYMSettings.FromSpindle := aProdGrp.SpindleFirst;
      mYMSettings.ToSpindle := aProdGrp.SpindleLast;
      if (aProdGrp is TProdGrp) then begin
        mYMSettings.LoadFromDB((aProdGrp as TProdGrp).YMSetID, fYMSetName);
          //Nue 14.02.02 mYMSettings.ProdGrpYarnCount neu effektiver YarnCount zugewiesen (NIcht mehr normiert auf Nm).
        mYMSettings.ProdGrpYarnCount := ConvertYarnCountFloatToInt((aProdGrp as TProdGrp).YarnCnt);
        mYMSettings.ProdGrpYarnUnit := Integer((aProdGrp as TProdGrp).YarnCntUnit);
        mYMSettings.ProdGrpPilot := (aProdGrp as TProdGrp).PilotSpindles;
        mYMSettings.ProdGrpSpeed := aProdGrp.Speed;
        mYMSettings.ProdGrpSpeedRamp := aProdGrp.SpeedRamp;
codesite.SendFmtMsg('TSettings.UC: (1) (Assigned(aProdGrp)(aProdGrp is TProdGrp) (LoadFromDB: YMSetID:%d)',[(aProdGrp as TProdGrp).YMSetID]);
      end
//Start: Keine laufende ProdGrp selektiert  Nue:6.8.03
      else begin//(aProdGrp is TBaseSpindleRange) ProdGrp nicht assigned Nue:6.8.03
        if Assigned(xSettingsNav.ActCobChooserItem) then begin
          StrLCopy(@xText50, PChar((xSettingsNav.ActCobChooserItem as TBaseSetIDItem).Name), sizeOf(xText50)); //Nue: 19:12.01
          mYMSettings.LoadFromDB((xSettingsNav.ActCobChooserItem as TBaseSetIDItem).SetID, xText50); //Nue: 19:12.01
        end;
      end; //else
//End: Keine laufende ProdGrp selektiert  Nue:6.8.03

codesite.SendMsg('TSettings.UC: (11) (Assigned(aProdGrp)');
    end
    else begin
      if xMode = gmAssignment then begin
  //mYMSettings.Mode := xMode;  //Nue: 17.08.01  ??????????????????????????????????????????????????????Kr
        mYMSettings.MachineConfig := SpdRangeList.AssMachine.MachineYMConfigRec;
//        mYMSettings.MachineConfig := SpdRangeList.AssMachine.GetMachineYMConfigRec;
          //Nue: 19:12.01
        if Assigned(xSettingsNav.ActCobChooserItem) then begin
          StrLCopy(@xText50, PChar((xSettingsNav.ActCobChooserItem as TBaseSetIDItem).Name), sizeOf(xText50)); //Nue: 19:12.01
          mYMSettings.LoadFromDB((xSettingsNav.ActCobChooserItem as TBaseSetIDItem).SetID, xText50); //Nue: 19:12.01
codesite.SendFmtMsg('TSettings.UC: (2) xMode = gmAssignment Assigned(xSettingsNav.ActCobChooserItem) (LoadFromDB: SetID=%d)',
  [(xSettingsNav.ActCobChooserItem as TBaseSetIDItem).SetID]);
        end;
codesite.SendMsg('TSettings.UC: (21) xMode = gmAssignment ');
      end
      else begin
codesite.SendMsg('TSettings.UC: (3) NOT(xMode = gmAssignment) ');
        if Assigned(aProdGrp) then begin //Nue:7.4.03
          SetScreenSettings(xSettingsNav.ActPreselectItemSettings);
codesite.SendMsg('TSettings.UC: (31) NOT(xMode = gmAssignment) SetScreenSettings');
        end;
      end;
      if Assigned(aMiscInfo) then begin
        mYMSettings.ProdGrpYarnCount := ConvertYarnCountFloatToInt(aMiscInfo.getYarnCnt); //Nue 28.2.02 //wss
      end;
    end;
(*
      //Nue:10.4.02 Needed because the TSettings.UpdateComponent will be called before TSettingsNav.UpdateComponent
    if fFirstCall then begin
      fFirstCall := False;
      xSettingsNav.UpdateActList;
    end;
*)
  finally
      // LOK (7.11.2002): TSettings wird ausgeblendet, w�hrend die Daten von der Maschine geholt werden.
      // ==> Hier wieder einblenden
    if (Self.Owner is TAssign) then begin
       if (Self.Owner as TAssign).SettingsVisible then begin
         visible := True;
       end;
    end   
    else begin
      visible := True;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TSettings.CheckUserValue: boolean;
begin
  Result := True;
end;
//------------------------------------------------------------------------------
procedure TSettings.OnChange(aOwner: TComponent);
begin
  ValueChanged;
end;
//------------------------------------------------------------------------------
procedure TSettings.SetYarnCnt(aYarnCnt: Double);
begin
  mYMSettings.ProdGrpYarnCount := ConvertYarnCountFloatToInt(aYarnCnt); //wss
end;
//------------------------------------------------------------------------------
procedure TSettings.SetSpeed(aSpeed: integer);
begin
  mYMSettings.ProdGrpSpeed := aSpeed;
end;
//------------------------------------------------------------------------------
procedure TSettings.SetSpeedRamp(aSpeedRamp: integer);
begin
  mYMSettings.ProdGrpSpeedRamp := aSpeedRamp;
end;
//------------------------------------------------------------------------------
procedure TSettings.SetPilotSpindles(aPilotSpindles: integer);
begin
  mYMSettings.ProdGrpPilot := aPilotSpindles;
end;
//------------------------------------------------------------------------------
function TSettings.CheckEqualTKClassInRangeFromProdGrp(aFirstSpindle, aLastSpindle: Integer; aTKClass: TSensingHeadClass): Boolean;
begin
  Result := mYMSettings.EqualSensingHeadClassInRange(aFirstSpindle, aLastSpindle, aTKClass);
end;
//------------------------------------------------------------------------------
function TSettings.CheckEqualTKClassInRange(aFirstSpindle, aLastSpindle: Integer): Boolean;
begin
  Result := mYMSettings.EqualSensingHeadClassInRange(aFirstSpindle, aLastSpindle);
end;
//------------------------------------------------------------------------------

function TSettings.GetGUIMode: TYMSettinsGUIMode; //Nue 17.8.01
begin
  Result := mYMSettings.Mode;
end;
//------------------------------------------------------------------------------

procedure TSettings.SetGUIMode(aYMSettingsGUIMode: TYMSettinsGUIMode); //Nue 17.8.01
begin
  mYMSettings.Mode := aYMSettingsGUIMode;
end;
//------------------------------------------------------------------------------
procedure TSettings.SetCurveSelection(aEnable: Boolean); //Nue:22.8.01
begin
  mYMSettings.CurveSelection := aEnable;
end;
//------------------------------------------------------------------------------

procedure TSettings.LoadFromDB(aSetID: integer);
begin
  mYMSettings.LoadFromDB(aSetID, fYMSetName);
end;
//------------------------------------------------------------------------------
procedure TSettings.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Assigned(SpdRangeList) then
    if (Operation = opRemove) and (AComponent = SpdRangeList) then
      fSpdRangeList := nil;
  if Assigned(Query) then
    if (Operation = opRemove) and (AComponent = Query) then
      fQuery := nil;
end;
//------------------------------------------------------------------------------
procedure TSettings.SetQuery(aQuery: TmmAdoDataset);
begin
  fQuery := aQuery;
{}
  if (not (csDesigning in ComponentState)) then try
      // implementation of Loaded moved to here because the Templateform is created at runtime
      //Nue: 30.07.01
    with fQuery do begin
      Close;
      CommandText := cQryGetMachineNode;
      Open;

      with TMachine.Create(nil) do try
        Query := TmmAdoDataset.Create(nil);
        Query.Assign(fQuery);
          //With the MachineYMConfigRec of every machine in the system MaxMachineConfig will be built.
        while not EOF do begin // ADO Conform
          // hier wird immer in die gleiche instanz der "Bauzustand" ausgelesen...
          MachineID := FieldByName('c_machine_id').AsInteger;
          //...und dieser wird dann den Komponenten von Roli �bergeben, damti dieser die Informationen zusammentragen kann
          mYMSettings.CollectMaxMachineConfig := MachineYMConfigRec;
//          mYMSettings.CollectMaxMachineConfig := GetMachineYMConfigRec;
          Next;
        end; //while
          //Assign MaxMachineConfig to the actual MachineConfig
        mYMSettings.BuildMaxMachineConfig := True; //Nue: 30.07.01
        if (self.Owner is TTemplate) then
//Nue:9.10.03          or (self.Owner is TClearerSettings) then //Nue: 04.12.02
          mYMSettings.Mode := gmTemplate; //Nue: 30.07.01
      finally
        Query.Free;
        Query := nil;
        Free;
      end; //with TMachine try..finally
    end; //with fQuery

//xx      mYMSettings.MachineAssigned := False; //Nue 04.07.01
  except
    on e: Exception do begin
      raise Exception.Create('TSettings.SetQuery failed. ' + e.Message);
    end;
  end; // try except
{}
end;
//------------------------------------------------------------------------------
procedure TSettings.ValueChanged;
begin
  if Assigned(fSpdRangeList) then
    fSpdRangeList.ItemChanged(ucSettings);
  if Assigned(fOnValueChanged) then
    OnValueChanged(ucSettings, True);
end;
//------------------------------------------------------------------------------
procedure TSettings.SetSpindleFirst(const Value: Integer);
begin
  mYMSettings.FromSpindle := Value;
end;
//------------------------------------------------------------------------------

procedure TSettings.SetSpindleLast(const Value: Integer);
begin
  mYMSettings.ToSpindle := Value;
end;
//------------------------------------------------------------------------------
function TSettings.GetReadOnly: Boolean;
begin
  Result := mYMSettings.ReadOnly;
end;
//------------------------------------------------------------------------------
procedure TSettings.SetReadOnly(aReadOnly: Boolean);
begin
  mYMSettings.ReadOnly := aReadOnly;
end;
//------------------------------------------------------------------------------

procedure TSettings.SetActMachineYMConfigRec(aActMachineYMConfigRec: TMachineYMConfigRec); //Called by SpdRangeList
begin
  fActMachineYMConfigRec := aActMachineYMConfigRec;
  //Assign MaxMachineConfig to the actual MachineConfig from the actual selected machine
//xx  mYMSettings.MachineAssigned := False; //Nue 09.07.01
  mYMSettings.MachineConfig := SpdRangeList.AssMachine.MachineYMConfigRec;
//  mYMSettings.MachineConfig := SpdRangeList.AssMachine.GetMachineYMConfigRec;
end;
//------------------------------------------------------------------------------
{
function TSettings.GetSpindleLast: Integer;
begin
  Result := mYMSettings.ToSpindle;
end;
//------------------------------------------------------------------------------
function TSettings.GetSpindleFirst: Integer;
begin
  Result := mYMSettings.FromSpindle;
end;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

function TSettings.GetActSensingHeadClass: TSensingHeadClass;
begin
  Result := mYMSettings.ActSensingHeadClass;
end;
//------------------------------------------------------------------------------

//function TSettings.GetActMachineYMConfigRec: TMachineYMConfigRec;
//begin
//  Result := fActMachineYMConfigRec;
//end;

end.

