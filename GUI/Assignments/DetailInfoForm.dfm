inherited frmDetailInfo: TfrmDetailInfo
  Left = 577
  Top = 290
  BorderStyle = bsNone
  ClientHeight = 205
  ClientWidth = 311
  Position = poScreenCenter
  OnClose = FormClose
  OnDeactivate = FormDeactivate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnMain: TmmPanel
    Left = 0
    Top = 0
    Width = 311
    Height = 205
    Align = alClient
    Color = clInfoBk
    TabOrder = 0
    OnMouseDown = ControlsMouseDown
    OnMouseMove = ControlsMouseMove
    object laYTitle: TmmLabel
      Left = 8
      Top = 4
      Width = 59
      Height = 13
      Caption = '(20)Artikel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = True
      OnMouseDown = ControlsMouseDown
      OnMouseMove = ControlsMouseMove
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel2: TmmLabel
      Left = 8
      Top = 20
      Width = 96
      Height = 13
      Caption = '(*)Benutzerfelder'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = True
      OnMouseDown = ControlsMouseDown
      OnMouseMove = ControlsMouseMove
      AutoLabel.LabelPosition = lpLeft
    end
    object laStyleName: TmmLabel
      Left = 80
      Top = 4
      Width = 8
      Height = 13
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = True
      OnMouseDown = ControlsMouseDown
      OnMouseMove = ControlsMouseMove
      AutoLabel.LabelPosition = lpLeft
    end
    object memoInfo: TmmRichEdit
      Left = 1
      Top = 40
      Width = 309
      Height = 164
      Align = alBottom
      BorderStyle = bsNone
      Color = clInfoBk
      Lines.Strings = (
        'memoInfo')
      PlainText = True
      ReadOnly = True
      ScrollBars = ssHorizontal
      TabOrder = 0
      WordWrap = False
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 120
    Top = 8
    TargetsData = (
      1
      5
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0)
      (
        'TmmMemo'
        'Text'
        0)
      (
        'TmmRichEdit'
        'Text'
        0))
  end
  object timClose: TmmTimer
    Enabled = False
    Interval = 120000
    OnTimer = timCloseTimer
    Left = 152
    Top = 8
  end
  object StyleUserFields: TStyleUserFields
    Left = 184
    Top = 8
  end
end
