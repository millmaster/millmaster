(*==============================================================================
|-------------------------------------------------------------------------------
| Filename......: AssignmentsServerIMPL.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Assignment Applikation to start and stopp Produktin Groups
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 24.01.2000  1.00  Mg  | File created
| 20.04.2000  1.10  Mg  | Spindelranges inserted
| 18.05.2000  1.11  Mg  | Only One Assign Window per Machine inserted
| 28.06.2000  1.12  Nue | xLotParamForm.Visible := True;  added.
| 05.07.2001  1.13  Wss | Toolbartext and Bitmap added to empty commands too
| 26.07.2001  2.01  Wss | implementation changed to use WrapperForm
| 21.01.2002  2.03  Wss | ciCloseProdGrp: Visible property set to True
| 09.04.2002  2.03  Wss | ciCloseProdGrp: owner of TCloseHandler changed to xWrap -> see Create
| 02.10.2002        LOK | Umbau ADO
| 15.11.2002  2.03  Wss | Beim suchen nach einem vorhandenen Form gabs auf NT eine Exception
                          -> war sowieso noch falsch wegen WrapperForm -> fixed
| 27.11.2002  2.03  Wss | Beim TemplateForm wurde das Dictionary manuell gesetzt. Dies f�hrte
                          jedoch unter Windows NT zu einem A/V -> Translator findet normalerweise
                          ein geeignetes Dictionary selber
| 02.06.2003  2.04  Nue | ListCobChooserForm und ciHistoryProdGrp eingebaut.
| 26.08.2003  2.10  Nue | Ueberarbeitung Assignment: Einbau Check TK-Klassen.
| 27.01.2004  2.11  Wss | Bei Partie stoppen, wenn eine Sektion markiert ist, wird die nicht visuelle
                          Komponente TCloseHandler verwendet. Diese erstellt im Konstruktor weitere
                          Fenster, welche das Wrapperfenster ben�tigen. Das Wrapperfenster muss jedoch
                          manuell wieder freigegeben werden, da sonst das EXE im Speicher h�ngen bleibt.
| 08.12.2004  2.20  Nue | Umbau auf XML Version 5.0
| 07.06.2006  2.21  Nue | Check von ung�ltigen Spindelbereichen in EqualSensingHeadClassInRangeLoc
|=============================================================================*)
unit AssignmentsServerIMPL;                
interface
uses
  ComObj, ActiveX, Assignments_TLB, StdVcl, LoepfePluginIMPL, LOEPFELIBRARY_TLB,
  LoepfeGlobal, BaseGlobal;

type
  TAssignmentsServer = class(TLoepfePlugin, IAssignmentsServer)
  protected
  public
    function ExecuteCommand: SYSINT; override;
    procedure Initialize; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
{$R 'Toolbar.res'}
uses
  mmMBCS, mmCS, WrapperForm,

  Classes, ComServ, sysutils, Forms, Controls, BaseForm,
  AssignForm, TemplateForm, ProdGrpCloseForm, MachineSelForm, AssignComp,
  CloseHandlerComp, LotParameterForm, AskCloseForm,
  YMParaDef, mmDialogs, Dialogs, mmAdoDataset, mmAdoConnection, //Nue: 14.1.02
  ListCobChooserForm,
  XMLDef, YMParaUtils, TKClassSelForm, XMLSettingsModel, xmlMaConfigClasses, XMLGlobal;

type
  TCommandID = (ciAssign, ciTemplate, ciCloseProdGrp, ciLotSettings, ciHistoryProdGrp);
const
  cInvalidSpdRange =
    '(*)Bitte einen gueltigen Spindelbereich waehlen!'; //ivlm

  cNoProdGrpSelected =
    '(*)Bitte eine Partie markieren.'; //ivlm

  cProdGrpAlreadyStopped =
    '(*)Partie ist bereits gestoppt.'; //ivlm

  cNoLotSettingsOfStoppedProdGrp =
    '(*)Gestoppter Spulstellenbereich besitzt keine Partieeinstellungen.'; //ivlm
//------------------------------------------------------------------------------
  cFloorMenuDef: array[0..11] of TFloorMenuRec = (
 {
    // --- Main Menu File ---
  (CommandID: Ord(ciAssign); SubMenu: 0; MenuBmpName: '';
   MenuText: '(*)Reiniger Einstellungen'; MenuHint: '(*)Reiniger Einstellungen anzeigen'; MenuStBar: '(*)Reiniger Einstellungen anzeigen';  //ivlm
   ToolbarText: '';
   MenuGroup: mgMain; MenuTyp: mtFile; MenuState: msEnabled; MenuAvailable: maAll),

  (CommandID: Ord(ciTemplate); SubMenu: 0; MenuBmpName: 'TEMPLATE';
   MenuText: '(*)Vorlagen fuer Reiniger Einstellungen'; MenuHint: '(*)Vorlagenverwaltung fuer Reiniger Einstellungen'; MenuStBar: '(*)Vorlagenverwaltung fuer Reiniger Einstellungen';  //ivlm
   ToolbarText: 'Vorlagenverwaltung';
   MenuGroup: mgMain; MenuTyp: mtFile; MenuState: msEnabled; MenuAvailable: maAll),

  (CommandID: Ord(ciCloseProdGrp); SubMenu: 0; MenuBmpName: 'PARTIESTOP';
   MenuText: '(*)Partieerfassung stoppen'; MenuHint: '(*)Partieerfassung stoppen'; MenuStBar: '(*)Partieerfassung stoppen';  //ivlm
   ToolbarText: 'Partie stopp';
   MenuGroup: mgMain; MenuTyp: mtFile; MenuState: msEnabled; MenuAvailable: maAll),
  }
    (CommandID: Ord(ciAssign); SubMenu: 0; MenuBmpName: 'PARTIESTART';
    MenuText: '(*)Reiniger Einstellungen'; MenuHint: '(*)Reiniger Einstellungen anzeigen'; MenuStBar: '(*)Reiniger Einstellungen anzeigen'; //ivlm
    ToolbarText: 'Reiniger Einstellungen';
    MenuGroup: mgMachine; MenuTyp: mtMachine; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: Ord(ciCloseProdGrp); SubMenu: 0; MenuBmpName: 'PARTIESTOP';
    MenuText: '(*)Partieerfassung stoppen'; MenuHint: '(*)Partieerfassung stoppen'; MenuStBar: '(*)Partieerfassung stoppen'; //ivlm
    ToolbarText: 'Partie stopp'; //ToolbarText: '';
    MenuGroup: mgMachine; MenuTyp: mtMachine; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: Ord(ciLotSettings); SubMenu: 0; MenuBmpName: 'PARTIEPARAMETER';
    MenuText: '(*)Partie Eigenschaften'; MenuHint: '(*)Partie Eigenschaften anzeigen'; MenuStBar: '(*)Partie Einstellungen anzeigen'; //ivlm
    ToolbarText: 'Partie Eigenschaften';
    MenuGroup: mgMachine; MenuTyp: mtMachine; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: Ord(ciHistoryProdGrp); SubMenu: 0; MenuBmpName: 'HISTORY';
    MenuText: '(*)Partieliste'; MenuHint: '(*)Partieliste'; MenuStBar: '(*)Partieliste'; //ivlm
    ToolbarText: 'Partieliste'; //ToolbarText: '';
    MenuGroup: mgMachine; MenuTyp: mtMachine; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: Ord(ciAssign); SubMenu: 0; MenuBmpName: 'PARTIESTART';
    MenuText: '(*)Reiniger Einstellungen'; MenuHint: '(*)Reiniger Einstellungen anzeigen'; MenuStBar: '(*)Reiniger Einstellungen anzeigen'; //ivlm
    ToolbarText: 'Reiniger Einstellungen'; //ToolbarText: '';
    MenuGroup: mgGroup; MenuTyp: mtGroup; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: Ord(ciCloseProdGrp); SubMenu: 0; MenuBmpName: 'PARTIESTOP';
    MenuText: '(*)Partieerfassung stoppen'; MenuHint: '(*)Partieerfassung stoppen'; MenuStBar: '(*)Partieerfassung stoppen'; //ivlm
    ToolbarText: 'Partie stopp'; //ToolbarText: '';
    MenuGroup: mgGroup; MenuTyp: mtGroup; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: 0; SubMenu: - 1; MenuBmpName: '';
    MenuText: '(*)Zuordnungen'; MenuHint: '(*)Zuordnungen'; MenuStBar: '(*)Zuordnungen'; //ivlm
    ToolbarText: '';
    MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),

    (CommandID: Ord(ciAssign); SubMenu: 0; MenuBmpName: 'PARTIESTART';
    MenuText: '(*)Reiniger Einstellungen'; MenuHint: '(*)Reiniger Einstellungen anzeigen'; MenuStBar: '(*)Reiniger Einstellungen anzeigen'; //ivlm
    ToolbarText: 'Reiniger Einstellungen'; //ToolbarText: '';
    MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),

    (CommandID: Ord(ciLotSettings); SubMenu: 0; MenuBmpName: 'PARTIEPARAMETER';
    MenuText: '(*)Partie Eigenschaften'; MenuHint: '(*)Partie Eigenschaften anzeigen'; MenuStBar: '(*)Partie Einstellungen anzeigen'; //ivlm
    ToolbarText: 'Partie Eigenschaften'; //ToolbarText: '';
    MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),

    (CommandID: Ord(ciCloseProdGrp); SubMenu: 0; MenuBmpName: 'PARTIESTOP';
    MenuText: '(*)Partieerfassung stoppen'; MenuHint: '(*)Partieerfassung stoppen'; MenuStBar: '(*)Partieerfassung stoppen'; //ivlm
    ToolbarText: 'Partie stopp';
    MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),

    (CommandID: Ord(ciTemplate); SubMenu: 0; MenuBmpName: 'TEMPLATE';
    MenuText: '(*)Vorlagen fuer Reiniger Einstellungen'; MenuHint: '(*)Vorlagenverwaltung fuer Reiniger Einstellungen'; MenuStBar: '(*)Vorlagenverwaltung fuer Reiniger Einstellungen'; //ivlm
    ToolbarText: 'Vorlagenverwaltung';
    MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),

    (CommandID: Ord(ciHistoryProdGrp); SubMenu: 0; MenuBmpName: 'HISTORY';
    MenuText: '(*)Partieliste'; MenuHint: '(*)Partieliste'; MenuStBar: '(*)Partieliste'; //ivlm
    ToolbarText: 'Partieliste'; //ToolbarText: '';
    MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled)

    );
//------------------------------------------------------------------------------
function TAssignmentsServer.ExecuteCommand: SYSINT;
var xAssForm: TAssign;
  xTemplateForm: TTemplate;
  xProdGrpCloseForm: TProdGrpClose;
  xMachSel: TMachineSel;
  xStringList: TStringList;
  xMachine: TMachine;
  xProdGrp: TProdGrp;
  xMachineID: integer;
  xLotParamForm: TLotParameter;
//  xListCobChooser: TListCobChooser;
  x: integer;
  xWrap: TmmForm;
  xDataSet: TmmAdoDataSet;
  xConnection: TmmADoConnection;

  xCount: Integer;
  xCheckEqualTKClass: Boolean;
  xTKRanges: TStringList;
  xSpdFirst: Integer;
  xSpdLast: Integer;
  xTKClassSel: TTKClassSel;
  xInvalidRange: Boolean;

//local ------------------------------------------------------------------------------
function EqualSensingHeadClassInRangeLoc(aSpdFirst:Integer; aSpdLast:Integer;
    aMaConfigReader: TMaConfigReader; out aInvalidRange: Boolean): Boolean;
//Nue:25.08.03
//Eigene function und nicht die von Kr in TGUIYMSettings, weil er dort TK-8XX kompatibel zu allen anderen TK-Klassen setzt
// sprich: bei einer Maschien mit 1-39 TK8xx und 40-72 TK9xx gibt die Funktion EqualSensingHeadClassInRange(1-72) von Kr TRUE retour!
var
  xCount: Integer;
  xFound: Boolean;
  xSensingHead: TSensingHead;
begin
  Result := True;
  aInvalidRange := False;
  xFound := False;
//  with TYMMachineConfig.Create do begin
    with aMaConfigReader do begin
      if (FirstGroupSpindleFromSpindle(aSpdFirst,False)=0) then begin  //Nue:7.6.06
      //Ung�ltiger Spindelbereich gew�hlt  
        aInvalidRange := True;
      end
      else begin
        xCount := 0;
        while (GroupValueDef[xCount,cXPSpindleFromItem,1] <> 0) and
              (GroupValueDef[xCount,cXPSpindleFromItem,1] <> $FF) and (xCount < GroupCount) do begin
          if  (GroupValueDef[xCount,cXPSpindleFromItem,1] >= aSpdFirst+1) and (GroupValueDef[xCount,cXPSpindleFromItem,1] <= aSpdLast) then begin
  //          if xFound and (ConvertSensingHeadTypeToClass(GroupValueDef[xCount,cXPSensingHeadItem,1]) <> ConvertSensingHeadTypeToClass(GroupValueDef[xCount-1,cXPSensingHeadItem,1])) then begin   //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  //          if xFound and (ConvertSensingHeadTypeToClass(GetSensingHeadValue(GroupValueDef[xCount,cXPSensingHeadItem,cSensingHeadNames[shTK830]])) <>
  //                         ConvertSensingHeadTypeToClass(GetSensingHeadValue(GroupValueDef[xCount-1,cXPSensingHeadItem,cSensingHeadNames[shTK830]]))) then begin
  codesite.SendMsgEx(csmInfo, GroupValueDef[xCount,cXPSensingHeadItem,cSensingHeadNames[shTK830]]);
  codesite.SendMsgEx(csmInfo, GroupValueDef[xCount-1,cXPSensingHeadItem,cSensingHeadNames[shTK830]]);
            if  (GetSensingHeadClass(GetSensingHeadValue(GroupValueDef[xCount,cXPSensingHeadItem,cSensingHeadNames[shTK830]])) <>
                           GetSensingHeadClass(GetSensingHeadValue(GroupValueDef[xCount-1,cXPSensingHeadItem,cSensingHeadNames[shTK830]]))) then begin
              Result := False;
              BREAK; //!!!!!!!!!!!!!!!!!!
            end; //if
  //          xFound := True;
          end; //if
          inc(xCount);
        end; //while
      end; //else
  //          if xFound and (GetSensingHeadClass(GetSensingHeadValue(GroupValueDef[xCount,cXPSensingHeadItem,cSensingHeadNames[shTK830]])) <>
  //                         GetSensingHeadClass(GetSensingHeadValue(GroupValueDef[xCount-1,cXPSensingHeadItem,cSensingHeadNames[shTK830]]))) then begin
  //            Result := False;
  //            BREAK; //!!!!!!!!!!!!!!!!!!
  //          end; //if
  //          xFound := True;
  //        end; //if
  //        inc(xCount);
  //      end; //while
    end; //with
//  end; //with
end;
//NUE1
//function EqualSensingHeadClassInRangeLoc(aSpdFirst:Integer; aSpdLast:Integer; aMachineYMConfigRec:TMachineYMConfigRec):Boolean;
////Nue:25.08.03
////Eigene function und nicht die von Kr in TGUIYMSettings, weil er dort TK-8XX kompatibel zu allen anderen TK-Klassen setzt
//// sprich: bei einer Maschien mit 1-39 TK8xx und 40-72 TK9xx gibt die Funktion EqualSensingHeadClassInRange(1-72) von Kr TRUE retour!
//var
//  xCount: Integer;
//  xFound: Boolean;
//  xSensingHead: TSensingHead;
//begin
//  Result := True;
//  xFound := False;
//  with TYMMachineConfig.Create do begin
//    with aMachineYMConfigRec do begin
//      xCount := 1;
//      while (spec[xCount].spindle.start <> 0) and (spec[xCount].spindle.start <> $FF) do begin
//        if  (spec[xCount].spindle.start >= aSpdFirst) and (spec[xCount].spindle.start <= aSpdLast) then begin
//          if xFound and (ConvertSensingHeadTypeToClass(spec[xCount].sensingHead) <> ConvertSensingHeadTypeToClass(spec[xCount-1].sensingHead)) then begin   //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//            Result := False;
//            BREAK; //!!!!!!!!!!!!!!!!!!
//          end; //if
//          xFound := True;
//        end; //if
//        inc(xCount);
//      end; //while
//    end; //with
//  end; //with
//end;
//end local ------------------------------------------------------------------------------

begin
  xCheckEqualTKClass := False;

  Result := 0;
  xDataSet := nil;
  xConnection := TmmAdoConnection.Create(nil);
  try
    xConnection.AutoConfig := acAuto;
    xDataset := TmmAdoDataset.Create(nil);
    xDataset.Connection := xConnection;
    try
      case TCommandID(CommandIndex) of

        ciAssign: begin
            xMachineID := 0;
            xStringList := TStringList.Create;
            try
              xStringList.CommaText := SelectedMachines;
              if xStringList.Count <> 1 then begin
                xMachSel := TMachineSel.Create(self.MainForm, xDataset);
                if xMachSel.ShowModal = mrOK then
                  xMachineID := xMachSel.MachineID;
                xMachSel.Free;
              end
              else begin
                xMachineID := StrToInt(xStringList.Strings[0]);
              end;
            finally
              xStringList.Free;
            end;// try finally

            //xAssForm := nil;
            if xMachineID <> 0 then begin
              if ProdID <> 0 then begin
                xMachine := TProdGrp.Create(xDataset);
                (xMachine as TProdGrp).ProdGrpID := ProdID;
              end
              else begin
                if SpdFirst <> 0 then begin
                  xMachine := TSpindleRange.Create(xDataset);
                  (xMachine as TSpindleRange).Init(xMachineID, SpdFirst, SpdLast);
                end
                else begin
                  xMachine := TMachine.Create(nil);
                  xMachine.Query := xDataset;
                  xMachine.MachineID := xMachineID;
                end;

                //Check unterschiedliche Tastk�pfe im Zielbereich weiter unten
                xCheckEqualTKClass := True;    //Nue:25.8.03

              end;

//            //Nue: 14.1.02
//NUE1              if xMachine.AWEMachType = amtUnknown then begin
              if NOT xMachine.Uploaded then begin
                if (MMMessageDlg(Format('%s %s', [cNoMachUploadDone1, cNoMachUploadDone2]), mtWarning, [mbOk], 0) = mrOk) then begin
                  Exit;
                end;
              end;

              if xCheckEqualTKClass then begin
              //Check unterschiedliche Tastk�pfe im Zielbereich  Nue:25.8.03
                if SpdFirst <> 0 then begin
                   xSpdFirst := SpdFirst;
                   xSpdLast := SpdLast;
                end
                else begin
                   xSpdFirst := 1;
                   xSpdLast := xMachine.NumSpindles;
                end;

                try
codesite.SendMsgEx(csmInfo, FormatXML(xMachine.MaConfigReader.DOM));
codesite.SendMsgEx(csmInfo, Format('1: Group 0: %s-%s; Group 1: %s-%s; Group 2: %s-%s;',
 [xMachine.MaConfigReader.GroupValueDef[0,cXPSpindleFromItem,0],
 xMachine.MaConfigReader.GroupValueDef[0,cXPSpindleToItem,0],
 xMachine.MaConfigReader.GroupValueDef[1,cXPSpindleFromItem,0],
 xMachine.MaConfigReader.GroupValueDef[1,cXPSpindleToItem,0],
 xMachine.MaConfigReader.GroupValueDef[2,cXPSpindleFromItem,0],
 xMachine.MaConfigReader.GroupValueDef[2,cXPSpindleToItem,0]]
 ));
                  if not EqualSensingHeadClassInRangeLoc(xSpdFirst, xSpdLast, xMachine.MaConfigReader, xInvalidRange) then begin
                    xStringList := TStringList.Create;
                    try
                      with xMachine.MaConfigReader do begin
                        xCount := 0;
codesite.SendMsgEx(csmInfo, Format('2: Group 0: %s-%s; Group 1: %s-%s; Group 2: %s-%s;',
 [GroupValueDef[0,cXPSpindleFromItem,0],
 GroupValueDef[0,cXPSpindleToItem,0],
 GroupValueDef[1,cXPSpindleFromItem,0],
 GroupValueDef[1,cXPSpindleToItem,0],
 GroupValueDef[2,cXPSpindleFromItem,0],
 GroupValueDef[2,cXPSpindleToItem,0]]
 ));
                        while (GroupValueDef[xCount,cXPSpindleFromItem,1] <> 0) and
                              (GroupValueDef[xCount,cXPSpindleFromItem,1] <> $FF) and (xCount < GroupCount) do begin
                          xStringList.Add(Format('%2d-%2d: %s ', [Integer(GroupValueDef[xCount,cXPSpindleFromItem,1]), Integer(GroupValueDef[xCount,cXPSpindleToItem,1]),
                                          cGUISensingHeadNames[GetSensingHeadValue(GroupValueDef[xCount,cXPSensingHeadItem,cSensingHeadNames[shTK830]])]]));
                          inc(xCount);
                        end; //while
                      end; //with
//NUE1
//                      with xMachine.MachineYMConfigRec do begin
//                        xCount := 1;
//                        while (spec[xCount].spindle.start <> 0) and (spec[xCount].spindle.start <> $FF) do begin
//                          xStringList.Add(Format('%2d-%2d: %s ', [spec[xCount].spindle.start, spec[xCount].spindle.stop, cGUISensingHeadNames[spec[xCount].sensingHead]]));
//                          inc(xCount);
//                        end; //while
//                      end; //with
                      if xStringList.Count>0 then begin
                        xTKClassSel := TTKClassSel.Create(self.MainForm, xStringList);
                        try
                          if xTKClassSel.ShowModal = mrOK then begin
                            xSpdFirst := xMachine.MaConfigReader.GroupValueDef[xTKClassSel.ItemID,cXPSpindleFromItem,1];
                            xSpdLast  := xMachine.MaConfigReader.GroupValueDef[xTKClassSel.ItemID,cXPSpindleToItem,1];
//NUE1
//                            xSpdFirst := xMachine.MachineYMConfigRec.spec[xTKClassSel.ItemID+1].spindle.start;
//                            xSpdLast  := xMachine.MachineYMConfigRec.spec[xTKClassSel.ItemID+1].spindle.stop;
                            xMachine.Free;  //Freigeben der alten Instanz
                            xMachine := TSpindleRange.Create(xDataset);  //Neue Instanz mit neuer Zuweisung
                            (xMachine as TSpindleRange).Init(xMachineID, xSpdFirst, xSpdLast);
                          end
                          else begin
                            EXIT;
                          end;
                        finally
                          xTKClassSel.Free;
                        end; //finally
                      end;
                    finally
                      xStringList.Free
                    end; //finally
                  end
                  else begin //Nue:7.6.06
                    if xInvalidRange then begin
                      InfoMsg(Translate(cInvalidSpdRange));
                      xMachine.Free;
                      EXIT; ////////////////////////////
                    end; //if
                  end; //else
                except
                  on e: Exception do begin
                    SystemErrorMsg_('TAssignmentsServer.ExecuteCommand CheckEqualTKClassInRange failed. ' + e.Message);
                  end;
                end; //try
              end; //if xCheckEqualTKClass


              // Search existing Windows with MachineID = xMachineID
              xAssForm := nil;
              for x := 0 to Application.ComponentCount - 1 do begin
                if Application.Components[x] is TfrmWrapper then begin
                  with TfrmWrapper(Application.Components[x]) do begin
                    if WrapperForm is TAssign then begin
                      xAssForm := TAssign(WrapperForm);
                      if xAssForm.SpdRange.MachineID = xMachineID then begin
                        CodeSite.SendMsg('Found TAssign with same machine id');
                        xAssForm.BringToFront;
                        Break;
                      end
                      else begin
                        xAssForm := nil;
                      end;
                    end;
                  end;
                end;
              end;

              if not Assigned(xAssForm) then begin
                if GetWrappedDataForm(nil, TForm(xAssForm), xWrap) then begin
                  xAssForm := TAssign.Create(xWrap, xConnection);
                  PrepareChildForm(xAssForm, xWrap);
                  Result := xWrap.Handle;
                end;
              end;

              xAssForm.SpdRange := xMachine;
              xAssForm.Visible := True;
              xMachine.Free;
            end;
          end;

        ciTemplate: begin
            if GetWrappedDataForm(nil, TForm(xTemplateForm), xWrap) then begin
              Result := xWrap.Handle;
              xTemplateForm := TTemplate.Create(xWrap, nil);
              PrepareChildForm(xTemplateForm, xWrap);
              xTemplateForm.Init;
            end;
          end;

        ciCloseProdGrp: begin
            if ProdID = 0 then begin
              if SpdFirst = 0 then begin
                if GetWrappedDataForm(nil, TForm(xProdGrpCloseForm), xWrap) then begin
                  xProdGrpCloseForm := TProdGrpClose.Create(xWrap, nil);
                  PrepareChildForm(xProdGrpCloseForm, xWrap);
                  xProdGrpCloseForm.MachineID := 0;
                  xProdGrpCloseForm.LotParameterEnabled := False;
                  xProdGrpCloseForm.ShowModal; //Visible := True;
                end;
              end
              else begin
                InfoMsg(Translate(cProdGrpAlreadyStopped));
              end;
            end
            else begin
              xProdGrp := TProdGrp.Create(xDataset);
              xProdGrp.ProdGrpID := ProdID;
              if GetWrappedDataForm(nil, TForm(xProdGrpCloseForm), xWrap) then begin
                with TCloseHandler.Create(xWrap) do
                try
                  PrepareChildForm(mAskClose, xWrap);
                  AssMachine := xProdGrp;
                  SpdRange := xProdGrp;
                  CloseProdGrp;
                finally
                  Free;
                  // da hier eine nicht visuelle Komponente erstellt wird, wo das Wrapper Fenster
                  // als Owner f�r die Unterfenster �bergeben wird, muss das Wrapper Fenster manuell
                  // wieder freigegeben werden, da sonst die Unterfenster nicht gel�scht werden und
                  // das EXE so im Speicher h�ngen bleibt.
                  xWrap.Free;
                end;
              end;
              xProdGrp.Free;
            end;
          end;
        ciLotSettings: begin
            if ProdID <> 0 then begin
              if GetWrappedDataForm(TLotParameter, TForm(xLotParamForm), xWrap) then begin
                Result := xWrap.Handle;
                xLotParamForm.ProdGrpID := ProdID;
                xLotParamForm.Visible := True; //Nue: 28.06.01
              end;
            end
            else begin
              if GetWrappedDataForm(nil, TForm(xWrap), xWrap) then try
                if SpdFirst = 0 then begin
                  InfoMsg(Translate(cNoProdGrpSelected), xWrap);
                end
                else begin
                  InfoMsg(Translate(cNoLotSettingsOfStoppedProdGrp), xWrap);
                end;
              finally
                xWrap.Close;
              end
            end;
          end;
        ciHistoryProdGrp: begin
              with TListCobChooser.Create(self.MainForm, nil) do  //Im Context von Create wird InitGrid f�r's Grid aufgerufen
              try
                ShowModal;  //Im Context von FormShow wird FillList f�r's Grid aufgerufen
              finally
                Free;
              end;
          end;
//        ciHistoryProdGrp: begin
//              xListCobChooser := TListCobChooser.Create(self.MainForm, nil);  //Im Context von Create wird InitGrid f�r's Grid aufgerufen
//              xListCobChooser.ShowModal;  //Im Context von FormShow wird FillList f�r's Grid aufgerufen
//              xListCobChooser.Free;
//          end;
      else
      end;
    except
      on e: Exception do begin
        SystemErrorMsg_('TAssignmentsServer.ExecuteCommand failed. ' + e.Message);
      end;
    end;
  finally
    FreeAndNil(xDataSet);
    FreeAndNil(xConnection);
  end;
end;
//------------------------------------------------------------------------------
procedure TAssignmentsServer.Initialize;
begin
  inherited Initialize;
  InitMenu(cFloorMenuDef);
end;
//------------------------------------------------------------------------------
initialization
  InitializeObjectFactory(TAssignmentsServer, Class_AssignmentsServer);
{
  TAutoObjectFactory.Create(ComServer, TAssignmentsServer, Class_AssignmentsServer,
    ciMultiInstance, tmApartment);
}
end.

