inherited Filter: TFilter
  Left = 643
  Top = 429
  BorderStyle = bsDialog
  Caption = '(*)Filter'
  ClientHeight = 351
  ClientWidth = 436
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object laTimeInfo: TmmLabel
    Left = 24
    Top = 179
    Width = 304
    Height = 26
    Caption = 
      '(*)Zeitbereich: Alle Partien, die mit Start oder Ende innerhalb ' +
      'des gewaehlten Bereichs sind.'
    Visible = True
    WordWrap = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laVon: TmmLabel
    Left = 24
    Top = 210
    Width = 41
    Height = 22
    AutoSize = False
    Caption = '(6)Von:'
    Layout = tlCenter
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laBis: TmmLabel
    Left = 222
    Top = 210
    Width = 41
    Height = 22
    AutoSize = False
    Caption = '(6)Bis:'
    Layout = tlCenter
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laStyleName: TmmLabel
    Left = 24
    Top = 21
    Width = 106
    Height = 13
    Hint = '(*)Platzhalter "*" und "?"'
    AutoSize = False
    Caption = '(20)Artikelname'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laLotName: TmmLabel
    Left = 24
    Top = 61
    Width = 106
    Height = 13
    Hint = '(*)Platzhalter "*" und "?"'
    AutoSize = False
    Caption = '(*)Partiename'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laTemplateName: TmmLabel
    Left = 24
    Top = 101
    Width = 106
    Height = 13
    Hint = '(*)Platzhalter "*" und "?"'
    AutoSize = False
    Caption = '(*)Vorlage'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laYarnCount: TmmLabel
    Left = 24
    Top = 141
    Width = 106
    Height = 13
    Hint = '(*)Platzhalter "*" und "?"'
    AutoSize = False
    Caption = '(*)Garn Nr [%s]'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object tpStart: TwwDBDateTimePicker
    Tag = 3
    Left = 66
    Top = 210
    Width = 145
    Height = 21
    Hint = '(6)Von:'
    CalendarAttributes.Font.Charset = DEFAULT_CHARSET
    CalendarAttributes.Font.Color = clWindowText
    CalendarAttributes.Font.Height = -11
    CalendarAttributes.Font.Name = 'MS Sans Serif'
    CalendarAttributes.Font.Style = []
    CalendarAttributes.Options = [mdoDayState, mdoWeekNumbers]
    CalendarAttributes.FirstDayOfWeek = wwdowMonday
    DateFormat = dfLong
    Date = 37417
    Epoch = 1950
    MinDate = 37417
    ShowButton = True
    TabOrder = 0
    UnboundDataType = wwDTEdtDate
    DisplayFormat = 'ddd, d. MMMM yyyy'
    OnChange = tpChange
  end
  object tpEnd: TwwDBDateTimePicker
    Tag = 3
    Left = 263
    Top = 210
    Width = 145
    Height = 21
    Hint = '(6)Bis:'
    CalendarAttributes.Font.Charset = DEFAULT_CHARSET
    CalendarAttributes.Font.Color = clWindowText
    CalendarAttributes.Font.Height = -11
    CalendarAttributes.Font.Name = 'MS Sans Serif'
    CalendarAttributes.Font.Style = []
    CalendarAttributes.Options = [mdoDayState, mdoWeekNumbers]
    CalendarAttributes.FirstDayOfWeek = wwdowMonday
    DateFormat = dfLong
    Date = 37412
    Epoch = 1950
    ShowButton = True
    TabOrder = 1
    UnboundDataType = wwDTEdtDate
    DisplayFormat = 'ddd, d. MMMM yyyy'
    OnChange = tpChange
  end
  object bOk: TmmButton
    Left = 256
    Top = 320
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 2
    Visible = True
    OnClick = bOkClick
    AutoLabel.LabelPosition = lpLeft
  end
  object bCancel: TmmButton
    Left = 344
    Top = 320
    Width = 75
    Height = 25
    Caption = '(*)Abbrechen'
    ModalResult = 2
    TabOrder = 3
    Visible = True
    OnClick = bCancelClick
    AutoLabel.LabelPosition = lpLeft
  end
  object edStyleName: TmmEdit
    Left = 135
    Top = 18
    Width = 282
    Height = 21
    Hint = '(*)Platzhalter "*" und "?"'
    Color = clWindow
    TabOrder = 4
    Text = '*'
    Visible = True
    OnChange = edStyleNameChange
    AutoLabel.LabelPosition = lpLeft
    Decimals = 0
    NumMode = False
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object edLotName: TmmEdit
    Left = 135
    Top = 58
    Width = 282
    Height = 21
    Hint = '(*)Platzhalter "*" und "?"'
    Color = clWindow
    TabOrder = 5
    Text = '*'
    Visible = True
    OnChange = edLotNameChange
    AutoLabel.LabelPosition = lpLeft
    Decimals = 0
    NumMode = False
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object edTemplateName: TmmEdit
    Left = 135
    Top = 98
    Width = 282
    Height = 21
    Hint = '(*)Platzhalter "*" und "?"'
    Color = clWindow
    TabOrder = 6
    Text = '*'
    Visible = True
    OnChange = edTemplateNameChange
    AutoLabel.LabelPosition = lpLeft
    Decimals = 0
    NumMode = False
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object cbTotTimeRange: TmmCheckBox
    Left = 24
    Top = 248
    Width = 313
    Height = 17
    Caption = '(*)Gesamter Zeitraum gewaehlt'
    Checked = True
    State = cbChecked
    TabOrder = 7
    Visible = True
    OnClick = cbTotTimeRangeClick
    AutoLabel.LabelPosition = lpLeft
  end
  object cbAssigned: TmmCheckBox
    Left = 24
    Top = 280
    Width = 321
    Height = 17
    Caption = '(*)Nur Status '#39'A'#39' sichtbar (Alle produzierenden Partien)'
    Checked = True
    State = cbChecked
    TabOrder = 9
    Visible = True
    OnClick = cbAssignedClick
    AutoLabel.LabelPosition = lpLeft
  end
  object edYarnCount: TmmNumEdit
    Left = 135
    Top = 138
    Width = 76
    Height = 21
    Decimals = 2
    Digits = 12
    Masks.PositiveMask = '#,###.#'
    Max = 9999
    NumericType = ntGeneral
    OnChange = edYarnCountChange
    OnKeyPress = edYarnCountKeyPress
    TabOrder = 8
    UseRounding = True
    Validate = False
    ValidateString = 
      '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
      's %s'
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 400
    Top = 8
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
end
