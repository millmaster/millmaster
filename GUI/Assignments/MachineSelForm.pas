(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MachineSelForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 28.12.1999  1.00  Mg  | Datei erstellt
| 22.02.2002  1.00  Wss | In Query cGetMachineData vergleich mit c_node_id <> '0' geaendert (Stringvergleich)
| 02.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
| 09.12.2003  1.01  Nue | destructor TMachineSel.Destroy;  added.
|=============================================================================*)
unit MachineSelForm;                   
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, StdCtrls, Buttons, mmBitBtn, mmListBox, AssignComp,
  IvDictio, IvMulti, IvEMulti, mmTranslator, LoepfeGlobal,BaseGlobal,
  mmButton, mmAdoDataset;

type
//..............................................................................
  TMachine = class ( TObject )
  private
    fMachineID : integer;
  public
    constructor Create ( aMachineID : integer );
    property MachineID : integer read fMachineID;
  end;
//..............................................................................
  TMachineSel = class(TDialogBottom)
    mmListBox1: TmmListBox;
    mmTranslator1: TmmTranslator;
    procedure bOKClick(Sender: TObject);
    procedure mmListBox1DblClick(Sender: TObject);
  private
    fMachineID : integer;
    procedure InitMachineID;
  public
    constructor Create ( aOwner : TComponent; aQuery : TmmAdoDataset ); reintroduce; virtual;
    destructor Destroy; override;
    property MachineID  : integer read fMachineID;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
{$R *.DFM}
const
  cGetMachineData =
  'select c_machine_name, c_machine_id ' +
  'from t_machine where c_node_id <> ''0'' order by c_machine_name';
//------------------------------------------------------------------------------
 { TMachine }
//------------------------------------------------------------------------------
  constructor TMachine.Create ( aMachineID : integer );
  begin
    inherited Create;
    fMachineID := aMachineID;
  end;
//------------------------------------------------------------------------------
{ TMachineSel }
//------------------------------------------------------------------------------
  constructor TMachineSel.Create ( aOwner : TComponent; aQuery : TmmAdoDataset );
  begin
    inherited Create ( aOwner );
    try
      with aQuery do begin
        Close;
        CommandText := cGetMachineData;
        Open;
        mmListBox1.Clear;
        while not EOF do begin   // ADO Conform
          mmListBox1.Items.AddObject ( FieldByName ( 'c_machine_name' ).AsString,
                                       TMachine.Create ( FieldByName ( 'c_machine_id' ).AsInteger ) );
          Next;
        end;
        if mmListBox1.Items.Count > 0 then
          mmListBox1.ItemIndex := 0;
      end;
    except
      on e:Exception do begin
        SystemErrorMsg_('TMachineSel.Create failed. ' + e.Message );
      end;
    end;
  end;
//------------------------------------------------------------------------------
  procedure TMachineSel.InitMachineID;
  begin
    if mmListBox1.Items.Count > 0 then
      fMachineID := ( mmListBox1.Items.Objects[mmListBox1.ItemIndex] as TMachine ).MachineID;
  end;
//------------------------------------------------------------------------------
  procedure TMachineSel.bOKClick(Sender: TObject);
  begin
    inherited;
    InitMachineID;
  end;
//------------------------------------------------------------------------------
  procedure TMachineSel.mmListBox1DblClick(Sender: TObject);
  begin
    InitMachineID;
    ModalResult := mrOK;
  end;
//------------------------------------------------------------------------------
destructor TMachineSel.Destroy;
var
  i : Integer;
begin
  for i:=0 to (mmListBox1.Items.Count-1) do begin
    mmListBox1.Items.Objects[i].Free;
  end; //for
  inherited;
end;
//------------------------------------------------------------------------------

end.
