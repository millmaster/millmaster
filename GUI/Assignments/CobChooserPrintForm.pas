{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: CobChooserPrintForm.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Print-Repot
| Info..........:
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
|                       | File created
| 21.06.2004  1.01  SDo | Layout Anpassung
===============================================================================}
unit CobChooserPrintForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, QRPrintGrid, QuickRpt, Qrctrls, mmQRSysData, ExtCtrls,
  mmQuickRep, mmQRLabel, mmQRImage, IvDictio, IvMulti, IvEMulti,
  mmTranslator;

type
  TfrmCobChooserPrint = class(TmmForm)
    mQuickRep: TmmQuickRep;
    PageFooterBand: TQRBand;
    PageHeaderBand: TQRBand;
    DetailBand: TQRBand;
    ColumnHeaderBand: TQRBand;
    mmQRSysData1: TmmQRSysData;
    qlPageValue: TmmQRSysData;
    QRPrintStringGrid: TQRPrintStringGrid;
    qlApplication: TmmQRLabel;
    qlCompanyName: TmmQRLabel;
    mmQRImage1: TmmQRImage;
    mmTranslator1: TmmTranslator;
    qlTitle: TmmQRLabel;
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
  frmCobChooserPrint: TfrmCobChooserPrint;

implementation

uses ListCobChooserForm, SettingsReader;

{$R *.DFM}

procedure TfrmCobChooserPrint.FormCreate(Sender: TObject);
begin
  inherited;
  try
   qlCompanyName.Caption := TMMSettingsReader.Instance.Value[cCompanyName];
  except
   qlCompanyName.Caption :='';
  end;

  qlPageValue.Text := qlPageValue.Text + ' ';

  qlTitle.Caption := '';
end;

end.
