(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: ListcobChooserForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows2000
| Target.system.: Windows2000
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis. | Reason
|-------------------------------------------------------------------------------
| 19.05.2003  1.00  Nue  | Datei erstellt
| 08.07.2003  1.00  Wss  | Ausdrucken der Liste implementiert mit QRPrintStringGrid
| 27.08.2003  1.10  Nue  | Ueberarbeitung Assignment.
| 01.10.2003  1.11  Wss  | Drucken der Liste nur noch �ber QRPrintStringGrid ausf�hren
|                        | und nicht mehr �ber QuickReport direkt
| 27.08.2003  1.12  Nue  | Modifikation MaxDate f�r DemoDB und Zeitumstellungen,
|                        | wenn es auf einmal Daten in der Zukunft hat.
| 04.06.2004  1.13  SDo  | Neu: Print DLG & Layout Report; -> Func. PrintList, PrintSettings
| 23.12.2004  2.00  Nue  | Modifikationen wegen Umbau auf XML V5.0.
| 20.01.2005  2.01  SDo  | Anpassungen an PrintOut
| 13.05.2005  2.01  SDo  | QReport print im PrintSettings aktiviren
| 07.01.2007  2.02  Roda | Erh�hung Feldbreite cLotStart und cLotEnd von 78 auf 128 und ClientWidth von 952 auf 986
|=============================================================================*)
unit ListCobChooserForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, AdvGrid, mmExtStringGrid, ComCtrls, ToolWin, mmToolBar,
  mmStatusBar, ExtCtrls, mmPanel, ActnList, mmActionList, ImgList,
  mmImageList, YMParaDef, Db, ADODB, mmADODataSet, mmADOConnection,
  AssignComp, SettingsFrame, MMUGlobal, BaseGlobal, FilterCobChooserForm, LotParameterForm,
  mmPageControl, mmUpDown, StdCtrls, mmLabel, BASEFORM, IvDictio, IvMulti,
  IvEMulti, mmTranslator,
  YMBasicProdParaBox, XMLDef, XMLSettingsModel, VCLXMLSettingsModel; //Nue:10.7.03

//------------------------------------------------------------------------------

type

//  TSourceSet = set of TSourceTypes;
//..............................................................................
  TColProperty = record
    Name: string;
    Width: Integer;
    Align: TAlignment;
  end; // TColProperty = record

//..............................................................................
  TColProperties = array[0..15] of TColProperty;
//------------------------------------------------------------------------------

const
//  cProdGrpQuery =
//    'select top 300 t_ym.c_head_class, t_ym.c_ym_set_name, t_p.c_prod_id, t_p.c_machine_name, t_p.c_spindle_first, t_p.c_spindle_last, ' +
//    't_p.c_prod_start, t_p.c_prod_end, t_p.c_YM_set_id, t_p.c_prod_state, ' +
//    't_p.c_order_position_id, t_p.c_prod_name, t_s.c_color, t_p.c_style_id, t_p.c_style_name, ' +
//    't_p.c_act_yarncnt, t_p.c_slip, t_p.c_machineGroup, ' + //Nue:27.3.03
//    't_p.c_start_mode ' + //Nue
//    'from t_prodgroup t_p, t_style t_s, t_YM_settings t_ym ' +
//    'where (t_p.c_style_id = t_s.c_style_id) and (t_ym.c_ym_set_id=t_p.c_ym_set_id) ' +
//    'and (t_p.c_machine_id=:c_machine_id) ' +  //Nue: 19.8.03 Einschr�nkung auf aktuelle Maschine
//    'and (t_p.c_prod_state in (1,3,5)) ' + //Nue: 17.12.01
////    'order by c_prod_start desc ';  //alt bis 25.8.03 Nue
//    'order by c_spindle_first asc ';  //neu seit 25.8.03 Nue
  cProdGrpQuery = //Nue:XML Anpassung
    'select top 300 t_ym.c_head_class, t_ym.c_ym_set_name, t_p.c_prod_id, t_p.c_machine_name, t_p.c_spindle_first, t_p.c_spindle_last, ' +
    't_p.c_prod_start, t_p.c_prod_end, t_p.c_YM_set_id, t_p.c_prod_state, ' +
    't_p.c_order_position_id, t_p.c_prod_name, t_s.c_color, t_p.c_style_id, t_p.c_style_name, ' +
    't_p.c_act_yarncnt, t_p.c_slip, t_p.c_machineGroup, ' + //Nue:27.3.03
    't_p.c_start_mode ' + //Nue
    'from t_prodgroup t_p, t_style t_s, t_xml_YM_settings t_ym ' +
    'where (t_p.c_style_id = t_s.c_style_id) and (t_ym.c_ym_set_id=t_p.c_ym_set_id) ' +
    'and (t_p.c_machine_id=:c_machine_id) ' +  //Nue: 19.8.03 Einschr�nkung auf aktuelle Maschine
    'and (t_p.c_prod_state in (1,3,5)) ' + //Nue: 17.12.01
//    'order by c_prod_start desc ';  //alt bis 25.8.03 Nue
    'order by c_spindle_first asc ';  //neu seit 25.8.03 Nue
//------------------------------------------------------------------------------
//  cPreselectQuery = //Nue:8.4.02
//    'select c_YM_set_name from t_ym_settings where c_ym_set_id=(select c_ym_set_id from t_prodgroup_state ' +
//    'where c_prod_id=:c_prod_id)';
  cPreselectQuery = //Nue:XML Anpassung
    'select c_YM_set_name from t_xml_ym_settings where c_ym_set_id=(select c_ym_set_id from t_prodgroup_state ' +
    'where c_prod_id=:c_prod_id)';

//------------------------------------------------------------------------------
  cTemplateGrid: TColProperties =
    ((Name: ''; Width: 20; Align: taLeftJustify),
    (Name: cTemplateName; Width: 200; Align: taLeftJustify),
    (Name: cTKClass; Width: 200; Align: taLeftJustify),
    (Name: 'Item'; Width: 30; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify)
    );
//------------------------------------------------------------------------------

  cOrderPositionGrid: TColProperties =
    ((Name: ''; Width: 20; Align: taLeftJustify),
    (Name: cStyle; Width: 113; Align: taLeftJustify),
    (Name: cTemplateName; Width: 101; Align: taLeftJustify),
    (Name: cTKClass; Width: 81; Align: taLeftJustify),
    (Name: cAssortment; Width: 106; Align: taLeftJustify),
    (Name: cYarnCount; Width: 59; Align: taRightJustify),
    (Name: cSlip; Width: 59; Align: taRightJustify),
    (Name: 'Item'; Width: 30; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify)
    );
//------------------------------------------------------------------------------

  cStyleGrid: TColProperties =
    ((Name: ''; Width: 20; Align: taLeftJustify),
    (Name: cStyle; Width: 113; Align: taLeftJustify),
    (Name: cTemplateName; Width: 101; Align: taLeftJustify),
    (Name: cTKClass; Width: 81; Align: taLeftJustify),
    (Name: cAssortment; Width: 106; Align: taLeftJustify),
    (Name: cYarnCount; Width: 59; Align: taRightJustify),
    (Name: cSlip; Width: 59; Align: taRightJustify),
    (Name: 'Item'; Width: 30; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify)
    );
//------------------------------------------------------------------------------

  cProdGrpGrid: TColProperties =
    ((Name: ''; Width: 20; Align: taLeftJustify),
    (Name: cMachine; Width: 65; Align: taLeftJustify), {110}
    (Name: cMachGroup; Width: 47; Align: taRightJustify), {64}
    (Name: cSpdFirst; Width: 49; Align: taRightJustify),
    (Name: cSpdLast; Width: 49; Align: taRightJustify),
    (Name: cStyle; Width: 101; Align: taLeftJustify),
    (Name: cLotName; Width: 110; Align: taLeftJustify),
    (Name: cTemplateName; Width: 101; Align: taLeftJustify),
    (Name: cTKClass; Width: 68; Align: taLeftJustify),
    (Name: cLotStart; Width: 126; Align: taLeftJustify), {106}
    (Name: cLotEnd; Width: 126; Align: taLeftJustify),
    (Name: cState; Width: 42; Align: taCenter),
    (Name: AssignComp.cStart; Width: 42; Align: taCenter), {52}
    (Name: cYarnCount; Width: 59; Align: taRightJustify), {59}
    (Name: cSlip; Width: 56; Align: taRightJustify), {59}
    (Name: 'Item'; Width: 30; Align: taLeftJustify)
    );
//------------------------------------------------------------------------------

  cMemoryGrid: TColProperties =
    ((Name: ''; Width: 20; Align: taLeftJustify),
    (Name: cMachGroup; Width: 64; Align: taRightJustify),
    (Name: cSpdFirst; Width: 64; Align: taRightJustify),
    (Name: cSpdLast; Width: 64; Align: taRightJustify),
    (Name: cLotName; Width: 118; Align: taRightJustify),
    (Name: cTemplateName; Width: 101; Align: taRightJustify),
    (Name: cState; Width: 52; Align: taCenter),
    (Name: cYarnCount; Width: 59; Align: taRightJustify),
    (Name: cSlip; Width: 59; Align: taRightJustify),
    (Name: 'Item'; Width: 30; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify),
    (Name: ''; Width: 0; Align: taLeftJustify)
    );
//------------------------------------------------------------------------------

type
//..............................................................................

  TListCobChooser = class(TmmForm)
    mmImageList1: TmmImageList;
    mmActionList1: TmmActionList;
    acTime: TAction;
    acShowAssigned: TAction;
    acFilter: TAction;
    acLotParameter: TAction;
    acInitial: TAction;
    conDefault: TmmADOConnection;
    dseQuery1: TmmADODataSet;
    acNoFilter: TAction;
    acPrint: TAction;
    acHelp: TAction;
    acCancel: TAction;
    acAssign: TAction;
    mmToolBar1: TmmToolBar;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    ToolButton18: TToolButton;
    ToolButton19: TToolButton;
    ToolButton20: TToolButton;
    mMainPanel: TmmPageControl;
    tsInfo: TTabSheet;
    tsSettings: TTabSheet;
    mFillerPanel: TmmPanel;
    Settings1: TSettings;
    mExtStringGrid: TmmExtStringGrid;
    mStatusBar: TmmStatusBar;
    mmTranslator1: TmmTranslator;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    acMoveUp: TAction;
    acMoveDown: TAction;
    ToolButton4: TToolButton;
    VCLXMLSettingsModel1: TVCLXMLSettingsModel;
    procedure mExtStringGridClickCell(Sender: TObject; Arow, Acol: Integer);
    procedure mExtStringGridClickSort(Sender: TObject; aCol: Integer);
    procedure mExtStringGridDblClickCell(Sender: TObject; Arow, Acol: Integer);
    procedure mExtStringGridGetFormat(Sender: TObject; ACol: Integer; var AStyle: TSortStyle; var aPrefix, aSuffix: string);
    procedure acFilterExecute(Sender: TObject);
    procedure acLotParameterExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acNoFilterExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mExtStringGridRowChanging(Sender: TObject; OldRow, NewRow: Integer; var Allow: Boolean);
    procedure mExtStringGridGetAlignment(Sender: TObject; ARow, ACol: Integer; var AAlignment: TAlignment);
    procedure ToolButton13Click(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure acAssignExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure tsSettingsShow(Sender: TObject);
    procedure acMoveUpDownExecute(Sender: TObject);
    procedure mmActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure mExtStringGridGetCellColor(Sender: TObject; ARow,
      ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    FActTKClass: TSensingHeadClass;
    mActSource: TSourceTypes;
    mTakeOverSettings: Boolean;
    mSpindleRangeList: TSpindleRangeList;
//NUE1    fSettings: TSettings;
    fSettingsHandler: TGetSettingsHandler;
    mYarnUnit: TYarnUnit;
    mFilterForm: TFilter;
    fGridObjectCol: Integer; //Colonne des InfoGrids, welche das Objekt enth�lt
    mSetToProdID: Integer;
    mInfoGrid: TColProperties;
    mNoItemFound: Boolean;
    mSaveGUIState: TYMSettinsGUIMode;
    procedure ToolbarManager;
    procedure PrintList;
    procedure PrintSettings;
    procedure SetFormCaption;
    procedure GetSettings(aRow: Integer);
    procedure AssignTemplateItem(aTemplItem :TTemplateItem; aQuery: TmmADODataSet);
    procedure AssignStyleItem(aStyleItem :TStyleItem; aQuery: TmmADODataSet);
    function GetSensingHeadClass: TSensingHeadClass;
    function IsValidTKClass(aRow: Integer): Boolean;
//    function GetFirstStyleItem: TStyleItem;  //Holt den ersten Style der TK-Klasse des selektierten Maschinenbereichs in AssMachine
//    function GetFirstTemplateItem: TTemplateItem;  //Holt das erste Template der TK-Klasse des selektierten Maschinenbereichs in AssMachine
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public
    constructor Create(aOwner: TComponent; aSpindleRangeList: TSpindleRangeList); reintroduce;
    destructor Destroy; override;
    function FillList(aSource: TSourceTypes): boolean;
    procedure InitGrid(aColProperties: TColProperties);
    property ActTKClass: TSensingHeadClass read FActTKClass write FActTKClass;
//NUE1    property Settings: TSettings read fSettings write fSettings; //Ueberfluessig?? Nue 15.01.02
    property SettingsHandler: TGetSettingsHandler read fSettingsHandler write fSettingsHandler;
    property GridObjectCol: Integer read fGridObjectCol; //Colonne des InfoGrids, welche das Objekt enth�lt
//    property FirstStyleItem: TStyleItem read GetFirstStyleItem; //Holen des ersten Artikel auf DB (alphabetisch,TK-abh�ngig)
//    property FirstTemplateItem: TTemplateItem read GetFirstTemplateItem; //Holen des ersten Template auf DB (alphabetisch,TK-abh�ngig)
  end;

var
  ListCobChooser: TListCobChooser;

implementation
uses
  mmMBCS, mmCS, Printers,
  LoepfeGlobal, SettingsReader, CobChooserPrintForm,
  PrintSettingsTemplateForm, PrintTemplateForm, MMHtmlHelp, FPatternClasses,
  YMParaUtils;

{$R *.DFM}

//------------------------------------------------------------------------------
resourcestring
  cStylelist = '(*)Artikelliste'; //ivlm
  cTemplatelist = '(*)Vorlagenliste'; //ivlm
  cLotlist = '(*)Partieliste'; //ivlm
//------------------------------------------------------------------------------
const
//Alt bis 20.12.01
//  cTemplateQuery =
//  'select c_ym_set_name, c_color, c_YM_set_id, c_head_class from t_ym_settings where c_template_set = 1 ' +
//  'order by c_ym_set_name';
////------------------------------------------------------------------------------
//  cTemplateQuery =
//    'select c_ym_set_name, c_color, c_YM_set_id, c_head_class from t_ym_settings where c_template_set = 1 ' +
//    'and c_head_class=:c_head_class and c_YM_set_id<>1 order by c_ym_set_name';
////------------------------------------------------------------------------------
//  cTemplateQuery1 =
//    'select c_ym_set_name, c_color, c_YM_set_id, c_head_class from t_ym_settings where c_template_set = 1 ' +
//    'and c_YM_set_id<>1 order by c_ym_set_name';
//------------------------------------------------------------------------------
  cWhereDummy = 'where 1=1 ';
  cTemplateQueryTop1 =
    'select top 1 c_ym_set_name, c_color, c_YM_set_id, c_head_class from t_xml_ym_settings t_ym ';
  cTemplateQuery =
    'select c_ym_set_name, c_color, c_YM_set_id, c_head_class from t_xml_ym_settings t_ym ';
  cTemplateQueryWhere =
    ' AND c_template_set = 1 and c_YM_set_id<>1 ';
  cTemplateQueryWhere1 =
    ' AND c_head_class=:c_head_class ';
  cTemplateQueryOrder =
    ' order by c_ym_set_name ';
//------------------------------------------------------------------------------
{Nue: alt bis 17.12.01
  cProdGrpQuery =
  'select top 300 t_ym.c_head_class, t_ym.c_ym_set_name, t_p.c_prod_id, t_p.c_machine_name, t_p.c_spindle_first, t_p.c_spindle_last, '+
  't_p.c_prod_start, t_p.c_prod_end, t_p.c_YM_set_id, t_p.c_prod_state, '+
  't_p.c_order_position_id, t_p.c_prod_name, t_s.c_color, t_p.c_style_id, t_p.c_style_name '+
  'from t_prodgroup t_p, t_style t_s, t_YM_settings t_ym '+
  'where (t_p.c_style_id = t_s.c_style_id) and (t_ym.c_ym_set_id=t_p.c_ym_set_id) '+
  'order by c_prod_start desc ';
{}
  cProdGrpTimedQuery =
    'select t_ym.c_head_class, t_ym.c_ym_set_name, t_p.c_prod_id, t_p.c_machine_name, t_p.c_spindle_first, t_p.c_spindle_last, ' +
    't_p.c_prod_start, t_p.c_prod_end, t_p.c_YM_set_id, t_p.c_prod_state, ' +
    't_p.c_order_position_id, t_p.c_prod_name, t_s.c_color, t_p.c_style_id, t_s.c_style_name, ' +
    't_p.c_act_yarncnt, t_p.c_slip, t_p.c_machineGroup, ' + //Nue:27.3.03
    't_p.c_start_mode ' + //Nue
    'from t_prodgroup t_p, t_style t_s, t_xml_YM_settings t_ym ';
  cProdGrpTimedQueryWhere =
    ' and (t_p.c_style_id = t_s.c_style_id) and (t_ym.c_ym_set_id=t_p.c_ym_set_id) ' +
    'order by c_prod_start desc ';
//    'where (t_p.c_style_id = t_s.c_style_id) and (t_ym.c_ym_set_id=t_p.c_ym_set_id) ' +
////    'and (t_p.c_prod_start>=:c_prod_start) and (t_p.c_prod_end<=:c_prod_end) ' + //Nue: 17.12.01
//    'and (((t_p.c_prod_start>=:c_prod_start) and (t_p.c_prod_start<=:c_prod_end)) ' + //Nue: 13.05.03
//    'or (t_p.c_prod_end>=:c_prod_start) and (t_p.c_prod_end<=:c_prod_end)) ' + //Nue: 13.05.03
//    'order by c_prod_start desc ';

//------------------------------------------------------------------------------
//Alt bis 20.12.01
//  cStyleQuery =
//  'select s.c_style_id, s.c_style_name, ss.c_ym_set_id, y.c_color/*1.17*/, y.c_YM_set_name, ' +
//  'a.c_assortment_name, y.c_head_class '+
//  'from t_style s, t_YM_settings y, t_assortment a, t_style_settings ss ' +
//  'where (ss.c_YM_set_id=y.c_YM_set_id) and (s.c_assortment_id=a.c_assortment_id) and (s.c_style_id=ss.c_style_id) '+
//  'and c_style_state<>2 '+// Style status: 1=active; 2=inactive
//  'order by c_style_name';
////------------------------------------------------------------------------------
//  cStyleQuery =
//    'select s.c_style_id, s.c_style_name, ss.c_ym_set_id, y.c_color/*1.17*/, y.c_YM_set_name, ' +
//    'a.c_assortment_name, y.c_head_class, ' +
//    's.c_yarncnt, s.c_slip '+ //Nue:27.3.03
//    'from t_style s, t_YM_settings y, t_assortment a, t_style_settings ss ' +
//    'where (ss.c_YM_set_id=y.c_YM_set_id) and (s.c_assortment_id=a.c_assortment_id) and (s.c_style_id=ss.c_style_id) ' +
//    'and c_style_state<>2 ' + // Style status: 1=active; 2=inactive
//    'and c_head_class=:c_head_class order by c_style_name';
////------------------------------------------------------------------------------
//  cStyleQuery1 =
//    'select s.c_style_id, s.c_style_name, ss.c_ym_set_id, y.c_color/*1.17*/, y.c_YM_set_name, ' +
//    'a.c_assortment_name, y.c_head_class, ' +
//    's.c_yarncnt, s.c_slip '+ //Nue:27.3.03
//    'from t_style s, t_YM_settings y, t_assortment a, t_style_settings ss ' +
//    'where (ss.c_YM_set_id=y.c_YM_set_id) and (s.c_assortment_id=a.c_assortment_id) and (s.c_style_id=ss.c_style_id) ' +
//    'and c_style_state<>2 ' + // Style status: 1=active; 2=inactive
//    'order by c_style_name';
//------------------------------------------------------------------------------
  cStyleQuery =
    'select t_s.c_style_id, t_s.c_style_name, t_ss.c_ym_set_id, t_ym.c_color/*1.17*/, t_ym.c_YM_set_name, ' +
    't_a.c_assortment_name, t_ym.c_head_class, ' +
    't_s.c_yarncnt, t_s.c_slip ' + //Nue:27.3.03
    'from t_style t_s, t_xml_ym_settings t_ym, t_assortment t_a, t_style_settings t_ss ';
  cStyleQueryWhere =
    ' AND (t_ss.c_YM_set_id=t_ym.c_YM_set_id) and (t_s.c_assortment_id=t_a.c_assortment_id) and (t_s.c_style_id=t_ss.c_style_id) ' +
    'and t_s.c_style_state<>2 '; // Style status: 1=active; 2=inactive
  cStyleQueryWhere1 =
    'and t_ym.c_head_class=:c_head_class ';
  cStyleQueryOrder =
    ' order by c_style_name ';
//------------------------------------------------------------------------------
  cOrderPositionQuery =
    'select t_op.c_order_position_id, t_op.c_order_position_name, t_ym.c_ym_set_id, t_s.c_color, t_ym.c_head_class ' +
    'from t_order_position t_op, t_style t_s, t_xml_ym_settings t_ym ' +
    'where t_op.c_style_id = t_s.c_style_id and t_ym.c_ym_set_id=t_op.c_ym_set_id ' +
    'order by c_order_position_name';

//------------------------------------------------------------------------------
  cMaxDateQuery =
    'select distinct max(c_fragshift_start) MaxDate from t_dw_prodgroup_shift';

//------------------------------------------------------------------------------

constructor TListCobChooser.Create(aOwner: TComponent; aSpindleRangeList: TSpindleRangeList);
var
  xMaxDate : TDate;
begin
  inherited Create(aOwner);
  HelpContext       := GetHelpContext('WindingMaster\Zuordnung\ZUO_DLG_Settingssource.htm');

  mSpindleRangeList := aSpindleRangeList;
  mSetToProdID      := 0;
  mYarnUnit         := TMMSettingsReader.Instance.Value[cYarnCntUnit];

  if Assigned(aSpindleRangeList) then
    acAssign.Visible := True
  else
    acAssign.Visible := False;

  //Create Filterform
  xMaxDate := Now;
  if NOT (csDesigning in ComponentState) then begin
    //Modifikation f�r DemoDB und Zeitumstellungen, wenn es auf einmal Daten in der Zukunft hat. Nue:7.10.03
    with dseQuery1 do try
      Close;
      CommandText := cMaxDateQuery;
      Open;
      if not EOF then begin // ADO Conform
        if (xMaxDate < FieldByName('MaxDate').AsDateTime) then begin
          xMaxDate := FieldByName('MaxDate').AsDateTime;
        end; //if
      end; //if
    except
    end;
  end; // if not csDesigning
  mFilterForm := TFilter.Create(Self, xMaxDate);

  if (mSpindleRangeList = nil) then begin //Aufruf �ber OLE-Server von Floor -> Set stHistory
    mActSource := stHistory;
    InitGrid(cProdGrpGrid);
  end; //IF

  //Per Default TKKlasse auf shc8x
  FActTKClass := shc8x;
end;

//------------------------------------------------------------------------------

destructor TListCobChooser.Destroy;
begin
{wss, Jan 2004: da diese Fenster entweder mit aOwner oder Self erstellt werden,
                m�ssen diese Fenster nicht manuell freigegeben werden.
  FreeAndNil(mFilterForm);
}
  inherited Destroy;

//  try
//    FreeAndNil(mFilterForm);
//    inherited Destroy;
//  except
//    on e: Exception do begin
//      raise Exception.Create('TListCobChooser.Destroy failed. ' + e.Message);
//    end;
//  end;
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.AssignTemplateItem(aTemplItem :TTemplateItem; aQuery: TmmADODataSet);
begin
  with aQuery do begin
    aTemplItem.Name := FieldByName('c_ym_set_name').AsString;
    aTemplItem.Color := FieldByName('c_color').AsInteger;
    aTemplItem.HeadClass := TSensingHeadClass(FieldByName('c_head_class').AsInteger); //Nue:13.08.01
  end; //with
end;

//------------------------------------------------------------------------------

procedure TListCobChooser.AssignStyleItem(aStyleItem :TStyleItem; aQuery: TmmADODataSet);
begin
  with aQuery do begin
    aStyleItem.Name := FieldByName('c_style_name').AsString;
    aStyleItem.Color := FieldByName('c_color').AsInteger;
    aStyleItem.YMSetName := FieldByName('c_YM_set_name').AsString;
    aStyleItem.AssortmentName := FieldByName('c_assortment_name').AsString;
    aStyleItem.HeadClass := TSensingHeadClass(FieldByName('c_head_class').AsInteger); //Nue:13.08.01
    aStyleItem.YarnCnt := YarnCountConvert(yuNm, mYarnUnit, FieldByName('c_yarncnt').AsFloat);
  //              aStyleItem.YarnCnt := FieldByName('c_yarncnt').AsFloat;
    aStyleItem.Slip := Format('%4.3f', [FieldByName('c_slip').AsFloat / 1000.0]);
  end; //with
end;

//------------------------------------------------------------------------------

function TListCobChooser.FillList(aSource: TSourceTypes): boolean;
var
  xSpindleLast: Integer;
  xSpindleFirst: Integer;
//  xMaItem: TPreselectItem;
  xProdGrpItem: TProdGrpItem;
  xStyleItem: TStyleItem;
  xOPItem: TOrderPositionItem;
  xTemplItem: TTemplateItem;
  x: Integer;
//  xProdGrpYMPara: TProdGrpYMParaRec; //Nue 21.06.01
//  xSettings: TYMSettingsByteArr;

//----LOCAL --------------------------------------------------------------------
  function SetHeadClassQuery(aQueryStr: string): string;
    var
      x: TSensingHeadClass;
      xAddOR: Boolean;
  begin
    Result := aQueryStr;
    xAddOR := False;
    for x:= LOW(TSensingHeadClass) to HIGH(TSensingHeadClass) do begin
      if x in mSpindleRangeList.AssMachine.MaConfigReader.AvailableSensingHeadClasses then begin
        if xAddOr then begin //Second time and further
          Result := Result+' OR '
        end
        else begin //First time
          Result := Result+' AND(';
        end;
        Result := Result + Format('c_head_class=%d',[ORD(x)]);
        xAddOR := True;
      end; //if x
    end; //for
    if xAddOr then  //Nach letztem gefundenen HeadClass Klammer schliessen
      Result := Result+') ';
  end;
//-END---LOCAL -----------------------------------------------------------------

begin
  EnterMethod('TListCobChooser.FillList');
  Result := False;

  if csDesigning in ComponentState then exit;
  if not Assigned(dseQuery1) then Exit;

  try
    //Neue Source f�r ListCobChooser und FilterForm zuweisen
    mActSource := aSource;
    if mFilterForm.ActSource <> mActSource then begin
      mFilterForm.InitBeforeShow(mActSource, mYarnUnit);
    end;

    if (mSpindleRangeList <> nil) {Aufruf nicht �ber OLE_Server von Floor (stHistory)} and (not Assigned(Settings1.SpdRangeList)) then begin
      mSpindleRangeList.Query := dseQuery1;
      Settings1.SpdRangeList := mSpindleRangeList;
    end;

    mExtStringGrid.ClearNormalCells;
    mExtStringGrid.RowCount := 3;
    mExtStringGrid.Visible := False;
    //Erste ist Sortierkolonne 
    mExtStringGrid.SortColumn := 1;
    mExtStringGrid.SortDirection := sdAscending;

    mNoItemFound := True;
///    cobChooser.Clear;
///MwComboControl.UnhookForm;

    with dseQuery1 do begin
      Close;
      ToolbarManager;
      case mActSource of
        stTemplate: begin
//Alt bis 23.4.02              if (NOT mSpindleRangeList.AssMachine.IsInProduction) //Nue:9.4.02 AND (fSettings.ActSensingHeadClass=hcTK8xx)
//                AND (NOT fSettings.CheckEqualTKClassInRange((mSpindleRangeList.ActSpdRange as TBaseSpindleRange).SpindleFirst,(mSpindleRangeList.ActSpdRange as TBaseSpindleRange).SpindleLast)) then begin    //Nue:6.3.02
//NUE1            if  (mSpindleRangeList.CallByMachine) or  //Nue:20.8.03 Anzeige aller TK-Klassen wenn Maschine selektiert wurde
//NUE1                ((not mSpindleRangeList.AssMachine.IsInProduction) and (fSettings.ActSensingHeadClass = shc8x)) then begin //Nue:6.3.02
//NUE1              //With this case, all styles will be showed in the cobChooser. This is used, because in the case of a machine
//NUE1              //is having several TK-Classes, but no TK8XX, no template will be displayed.
//NUE1              CommandText := cTemplateQuery + mFilterForm.GetWhere + cTemplateQueryWhere + cTemplateQueryOrder;
            if  (mSpindleRangeList.CallByMachine) or  //Nue:20.8.03 Anzeige aller TK-Klassen wenn Maschine selektiert wurde
                ((not mSpindleRangeList.AssMachine.IsInProduction) and (ActTKClass = shc8x)) then begin //Nue:6.3.02
              //With this case, all styles will be showed in the cobChooser. This is used, because in the case of a machine
              //is having several TK-Classes, but no TK8XX, no template will be displayed.
              CommandText := SetHeadClassQuery(cTemplateQuery + mFilterForm.GetWhere + cTemplateQueryWhere);
            end
            else begin
              CommandText := cTemplateQuery + mFilterForm.GetWhere + cTemplateQueryWhere + cTemplateQueryWhere1 + cTemplateQueryOrder;
              Parameters.ParamByName('c_head_class').Value := ORD(GetSensingHeadClass); //Nue:20.12.01
            end;
            Open;
//            xFirst := True;
            while not EOF do begin // ADO Conform

              //Check weil sich ExtGrid ohne Datens�tze komisch verhaltet
              if mNoItemFound then begin
                mNoItemFound := False;
                mExtStringGrid.RowCount := mExtStringGrid.RowCount - 1;
              end; //IF

              xTemplItem := TTemplateItem.Create(FieldByName('c_YM_set_id').AsInteger);
              AssignTemplateItem(xTemplItem,dseQuery1);
              mExtStringGrid.Cells[1, mExtStringGrid.RowCount - 1] := xTemplItem.Name;
              mExtStringGrid.Cells[2, mExtStringGrid.RowCount - 1] := cGUISensingHeadClassNames[xTemplItem.HeadClass];
              mExtStringGrid.Objects[3, mExtStringGrid.RowCount - 1] := xTemplItem;

              mExtStringGrid.InsertRows(mExtStringGrid.RowCount, 1);

              Next;
            end; // while not EOF
            mExtStringGrid.RowCount := mExtStringGrid.RowCount - 1;
          end;
        stOrderPosition: begin
            CommandText := cOrderPositionQuery;
            Open;
            while not EOF do begin // ADO Conform
              xOPItem := TOrderPositionItem.Create(FieldByName('c_YM_set_id').AsInteger, FieldByName('c_order_position_id').AsInteger);
              xOPItem.Name := FieldByName('c_order_position_name').AsString;
              xOPItem.Color := FieldByName('c_color').AsInteger;
              xOPItem.HeadClass := TSensingHeadClass(FieldByName('c_head_class').AsInteger); //Nue:13.08.01
//              cobChooser.Items.AddObject(xOPItem.Name, xOPItem);
              Next;
            end; // while not EOF
          end;

        stStyle: begin
//Alt bis 23.4.02              if (NOT mSpindleRangeList.AssMachine.IsInProduction) //Nue:9.4.02 AND (fSettings.ActSensingHeadClass=hcTK8xx)
//                AND (NOT fSettings.CheckEqualTKClassInRange((mSpindleRangeList.ActSpdRange as TBaseSpindleRange).SpindleFirst,(mSpindleRangeList.ActSpdRange as TBaseSpindleRange).SpindleLast)) then begin    //Nue:6.3.02
//            if (mSpindleRangeList.CallByMachine) or  //Nue:20.8.03 Anzeige aller TK-Klassen wenn Maschine selektiert wurde
//               ((not mSpindleRangeList.AssMachine.IsInProduction) and (fSettings.ActSensingHeadClass = shc8x)) then begin //Nue:6.3.02
//              //With this case, all styles will be showed in the cobChooser. This is used, because in the case of a machine
//              //is having several TK-Classes, but no TK8XX, no style will be displayed.
//              CommandText := cStyleQuery + mFilterForm.GetWhere + cStyleQueryWhere + cStyleQueryOrder;
            if (mSpindleRangeList.CallByMachine) or  //Nue:20.8.03 Anzeige aller TK-Klassen wenn Maschine selektiert wurde
               ((not mSpindleRangeList.AssMachine.IsInProduction) and (ActTKClass = shc8x)) then begin //Nue:6.3.02
              //With this case, all styles will be showed in the cobChooser. This is used, because in the case of a machine
              //is having several TK-Classes, but no TK8XX, no style will be displayed.
              CommandText := SetHeadClassQuery(cStyleQuery + mFilterForm.GetWhere + cStyleQueryWhere);
            end
            else begin
              CommandText := cStyleQuery + mFilterForm.GetWhere + cStyleQueryWhere + cStyleQueryWhere1 + cStyleQueryOrder;
              Parameters.ParamByName('c_head_class').Value := ORD(GetSensingHeadClass); //Nue:20.12.01
            end;
            Open;
//            xFirst := True;

            if not (TMMSettingsReader.Instance.IsComponentStyle and not TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable) then
              mExtStringGrid.HideColumn(1);

            while not EOF do begin // ADO Conform

              //Check weil sich ExtGrid ohne Datens�tze komisch verhaltet
              if mNoItemFound then begin
                mNoItemFound := False;
                mExtStringGrid.RowCount := mExtStringGrid.RowCount - 1;
              end; //IF

              xStyleItem:= TStyleItem.Create(FieldByName('c_YM_set_id').AsInteger, FieldByName('c_style_id').AsInteger);
              AssignStyleItem(xStyleItem,dseQuery1);
              mExtStringGrid.Cells[1, mExtStringGrid.RowCount - 1] := xStyleItem.Name;
              mExtStringGrid.Cells[2, mExtStringGrid.RowCount - 1] := xStyleItem.YMSetName;
              mExtStringGrid.Cells[3, mExtStringGrid.RowCount - 1] := cGUISensingHeadClassNames[xStyleItem.HeadClass];
              mExtStringGrid.Cells[4, mExtStringGrid.RowCount - 1] := xStyleItem.AssortmentName;
              // show yarncount in unit specified in MMConfiguration
              mExtStringGrid.Cells[5, mExtStringGrid.RowCount - 1] :=
                Format('%4.2f', [YarnCountConvert(yuNm, mYarnUnit, FieldByName('c_yarncnt').AsFloat)]); //Nue 21.12.06 Format von 4.1 auf 4.2
              mExtStringGrid.Cells[6, mExtStringGrid.RowCount - 1] := xStyleItem.Slip;
              mExtStringGrid.Objects[7, mExtStringGrid.RowCount - 1] := xStyleItem;
//              mExtStringGrid.Cells[7, mExtStringGrid.RowCount-1] := xStyleItem;
//              cobChooser.Items.AddObject(xStyleItem.Name, xStyleItem);

              mExtStringGrid.InsertRows(mExtStringGrid.RowCount, 1);

              Next;
            end; // while not EOF
            mExtStringGrid.RowCount := mExtStringGrid.RowCount - 1;
          end;
        stHistory: begin
            if mFilterForm.InitState then //Alle Gruppen in Prod. werden angezeigt!
              CommandText := cProdGrpTimedQuery + ' WHERE (t_p.c_prod_state in (1,3,5)) ' + cProdGrpTimedQueryWhere
            else
              CommandText := cProdGrpTimedQuery + mFilterForm.GetWhere + cProdGrpTimedQueryWhere;

           //LotStart ist Sortierkolonne (In FillList wird im Query nach ProdStart ASC sortiert das Grid gef�llt)
            mExtStringGrid.SortColumn := 9;
            mExtStringGrid.SortDirection := sdDescending;

            Open;

            if not (TMMSettingsReader.Instance.IsComponentStyle and not TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable) then
              mExtStringGrid.HideColumn(5);

            while not EOF do begin // ADO Conform

              //Check weil sich ExtGrid ohne Datens�tze komisch verhaltet
              if mNoItemFound then begin
                mNoItemFound := False;
                mExtStringGrid.RowCount := mExtStringGrid.RowCount - 1;
              end; //IF

              xProdGrpItem := TProdGrpItem.Create(FieldByName('c_YM_set_id').AsInteger,
                FieldByName('c_prod_id').AsInteger,
                FieldByName('c_order_position_id').AsInteger,
                FieldByName('c_style_id').AsInteger);
              xProdGrpItem.Name := FieldByName('c_prod_name').AsString;
              xProdGrpItem.Color := FieldByName('c_color').AsInteger;
              xProdGrpItem.MachineName := FieldByName('c_machine_name').AsString;
              xProdGrpItem.GrpNr := FieldByName('c_machineGroup').AsInteger;
              xProdGrpItem.StyleName := FieldByName('c_style_name').AsString;
//              xProdGrpItem.ProdStart := FormatDateTime('dd.mm.yy hh:mm', FieldByName('c_prod_start').AsDateTime);
//              xProdGrpItem.ProdEnd := FormatDateTime('dd.mm.yy hh:mm', FieldByName('c_prod_end').AsDateTime);
              xProdGrpItem.ProdStart := FormatDateTime('c', FieldByName('c_prod_start').AsDateTime);
              xProdGrpItem.ProdEnd := FormatDateTime('c', FieldByName('c_prod_end').AsDateTime);
              xProdGrpItem.SpindleFirst := FieldByName('c_spindle_first').AsString;
              xProdGrpItem.SpindleLast := FieldByName('c_spindle_last').AsString;
              xProdGrpItem.State := FieldByName('c_prod_state').AsInteger;
              xProdGrpItem.HeadClass := TSensingHeadClass(FieldByName('c_head_class').AsInteger);
              xProdGrpItem.YMSetName := FieldByName('c_ym_set_name').AsString;
//              xProdGrpItem.YarnCnt := FieldByName('c_act_yarncnt').AsFloat;
              xProdGrpItem.YarnCnt := YarnCountConvert(yuNm, mYarnUnit, FieldByName('c_act_yarncnt').AsFloat);
//              xProdGrpItem.Slip := FloatToStr(FieldByName('c_slip').AsFloat/1000.0);
              xProdGrpItem.Slip := Format('%4.3f', [FieldByName('c_slip').AsFloat / 1000.0]);
              xProdGrpItem.StartMode := FieldByName('c_start_mode').AsInteger;
              mExtStringGrid.Cells[1, mExtStringGrid.RowCount - 1] := xProdGrpItem.MachineName;
              mExtStringGrid.Cells[2, mExtStringGrid.RowCount - 1] := IntToStr(xProdGrpItem.GrpNr+1);
              mExtStringGrid.Cells[3, mExtStringGrid.RowCount - 1] := xProdGrpItem.SpindleFirst;
              mExtStringGrid.Cells[4, mExtStringGrid.RowCount - 1] := xProdGrpItem.SpindleLast;
              mExtStringGrid.Cells[5, mExtStringGrid.RowCount - 1] := xProdGrpItem.StyleName;
              mExtStringGrid.Cells[6, mExtStringGrid.RowCount - 1] := xProdGrpItem.Name;
              mExtStringGrid.Cells[7, mExtStringGrid.RowCount - 1] := xProdGrpItem.YMSetName;
              mExtStringGrid.Cells[8, mExtStringGrid.RowCount - 1] := cGUISensingHeadClassNames[xProdGrpItem.HeadClass];
              mExtStringGrid.Cells[9, mExtStringGrid.RowCount - 1] := xProdGrpItem.ProdStart;
///              mExtStringGrid.DatesTimes[9, mExtStringGrid.RowCount-1] := FormatDateTime('dd.mm.yy hh:mm', FieldByName('c_prod_start').AsDateTime);
              if FieldByName('c_prod_start').AsDateTime = FieldByName('c_prod_end').AsDateTime then begin
                mExtStringGrid.Cells[10, mExtStringGrid.RowCount - 1] := '-----';
                mExtStringGrid.Cells[11, mExtStringGrid.RowCount - 1] := 'A';
              end
              else begin
                mExtStringGrid.Cells[10, mExtStringGrid.RowCount - 1] := xProdGrpItem.ProdEnd;
///                mExtStringGrid.DatesTimes[10, mExtStringGrid.RowCount-1] := FormatDateTime('dd.mm.yy hh:mm', FieldByName('c_prod_end').AsDateTime);
                mExtStringGrid.Cells[11, mExtStringGrid.RowCount - 1] := '-';
              end; //else
              mExtStringGrid.Cells[12, mExtStringGrid.RowCount - 1] := cProdGrpStartModeArr[TProdGrpStartMode(xProdGrpItem.StartMode)];
              // show yarncount in unit specified in MMConfiguration
              mExtStringGrid.Cells[13, mExtStringGrid.RowCount - 1] :=
                Format('%4.2f', [YarnCountConvert(yuNm, mYarnUnit, FieldByName('c_act_yarncnt').AsFloat)]);  //Nue 21.12.06 Format von 4.1 auf 4.2
              mExtStringGrid.Cells[14, mExtStringGrid.RowCount - 1] := xProdGrpItem.Slip;
              mExtStringGrid.Objects[15, mExtStringGrid.RowCount - 1] := xProdGrpItem;
//              cobChooser.Items.AddObject(xProdGrpItem.Name, xProdGrpItem);

              if (mSpindleRangeList <> nil) {Aufruf nicht �ber OLE_Server von Floor (stHistory)} then begin
               if (mSpindleRangeList.ActSpdRange <> nil) then
                if AnsiSameText(mSpindleRangeList.ActSpdRange.MachineName, xProdGrpItem.MachineName) and mSpindleRangeList.FirstRange then begin
                  xSpindleFirst := StrToInt(xProdGrpItem.SpindleFirst);
                  xSpindleLast  := StrToInt(xProdGrpItem.SpindleLast);
                  with (mSpindleRangeList.ActSpdRange.MaConfigReader) do
                    for x:=0 to GroupCount-1 do
                      if ((xSpindleFirst >= GroupValueDef[x,cXPSpindleFromItem,1]) and
                          (xSpindleFirst <= GroupValueDef[x,cXPSpindleToItem,1])) AND
                         ((xSpindleLast >= GroupValueDef[x,cXPSpindleFromItem,1]) and
                          (xSpindleLast <= GroupValueDef[x,cXPSpindleToItem,1])) then begin
                        mExtStringGrid.Row := mExtStringGrid.RowCount - 1;
                        BREAK;
                      end; //IF
                end; // if AnsiSameText()
              end; //IF

              mExtStringGrid.InsertRows(mExtStringGrid.RowCount, 1);

              Next;
            end;
            mExtStringGrid.RowCount := mExtStringGrid.RowCount - 1;

            //Setzen des Zeitraums f�r Daten, wenn ganzer Zeitraum selektiert
            if (mFilterForm.cbTotTimeRange.Checked) and (not (mNoItemFound)) then begin
              mFilterForm.tpEnd.Date := StrToDateTime(mExtStringGrid.Cells[9, mExtStringGrid.FixedRows]);
              if mExtStringGrid.RowCount > mExtStringGrid.FixedCols then //Wurde �berhaupt Datensatz gefunden
                mFilterForm.tpStart.Date := StrToDateTime(mExtStringGrid.Cells[9, mExtStringGrid.RowCount - 1]);
            end;
          end;
      else // stMaMemory, stProdGrp
        // Wird nicht in ListCobChooser abgehandlet, sondern schiesst die Daten direkt in die ListBox cobChooser
        // Abhandlung in SettingsNav.FillList
      end;

      SetFormCaption;
      Result := True;
    end;

    mExtStringGrid.Visible := True;
    mStatusBar.SimpleText := Format('%s: %d', [cNrOfDatasets, mExtStringGrid.RowCount - mExtStringGrid.FixedRows]);
  except
    on e: Exception do begin
      raise Exception.Create('TListCobChooser.FillList failed. ' + e.Message);
    end;
  end;
end;

//------------------------------------------------------------------------------

procedure TListCobChooser.mExtStringGridClickCell(Sender: TObject; Arow,
  Acol: Integer);
begin
  if Arow > 0 then begin //Zelle im Grid wurde angeclickt (nicht Headerzeile)
    SetFormCaption;
  end;
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.mExtStringGridClickSort(Sender: TObject; aCol: Integer);
begin
  case mActSource of //Column header wurde angeklickt.
    stStyle: begin
        if aCol in [5, 6] then //For correct sorting of numeric column
          mExtStringGrid.SortAutoFormat := True
        else
          mExtStringGrid.SortAutoFormat := False;
      end;
    stHistory: begin
        if aCol in [2, 3, 4, 9, 10, 13, 14] then //For correct sorting of numeric column
          mExtStringGrid.SortAutoFormat := True
        else
          mExtStringGrid.SortAutoFormat := False;
      end;
  else // stTemplate ??
    mExtStringGrid.SortAutoFormat := False;
  end;

end;
//------------------------------------------------------------------------------

procedure TListCobChooser.mExtStringGridDblClickCell(Sender: TObject; Arow,
  Acol: Integer);
begin
//  mExtStringGrid.Row := Arow;  //Explizites setzen von Row, damit nachfolgende Zugriffe bereits auf die neue Selektion gehen.
//  if IsValidTKClass(Arow) then
  if Assigned(mSpindleRangeList) then  //Call von Assignment
    if (ARow > 0) and (ARow < mExtStringGrid.RowCount) //Wss
       and (mExtStringGrid.Cells[1, Arow]<>'') then begin
      if IsValidTKClass(Arow) then begin
        mTakeOverSettings := True;
        Close;
      end;
    end;
end;
//------------------------------------------------------------------------------
procedure TListCobChooser.mExtStringGridGetFormat(Sender: TObject;
  ACol: Integer; var AStyle: TSortStyle; var aPrefix, aSuffix: string);
begin
  inherited;
(*  if aCol = cStateCol then
    aStyle := ssAlphabetic;*)

end;
//------------------------------------------------------------------------------

procedure TListCobChooser.acFilterExecute(Sender: TObject);
begin
  mFilterForm.InitBeforeShow(mActSource, mYarnUnit); //Initialisieren des LookLike von mFilterForm
  if mFilterForm.ShowModal = mrOK then
    FillList(mActSource);
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.acLotParameterExecute(Sender: TObject);
var
  x: TProdGrpItem;
  xRow, xCol: Integer;
begin
  with TLotParameter.Create(Self) do
  try
    xCol := GridObjectCol;
    xRow := mExtStringGrid.Row;
    x := TProdGrpItem(mExtStringGrid.Objects[xCol, xRow]);
    ProdGrpID := x.ProdGrpID;
    if ShowModal = mrOK then begin
      mSetToProdID := x.ProdGrpID;
//      FormShow(nil);
      FillList(mActSource);
    end;
  finally
    Free;
  end; //with

end;
//------------------------------------------------------------------------------

procedure TListCobChooser.InitGrid(aColProperties: TColProperties);
var
  xCount: Integer;
  xStr: string;
begin
  with mExtStringGrid do begin
    xStr := '%s';
    mInfoGrid := aColProperties;
    //F�llen von mExtStringGrid
    Clear;
    UnhideColumnsAll;
    UnhideRowsAll;
    ColCount := 2;
    FixedCols := 1;
    RowCount := 2; //Unbedingt auf 2 belassen, weil in TSettingsNav.CobSelectionChanged auf Index 1 gecheckt wird (Index ist 1 die erste Zeile beieiner Fixzeile)
    FixedRows := 1;
    xCount := Low(aColProperties) + 1;
    while (xCount <= High(aColProperties)) do begin
      if (aColProperties[xCount].Width <> 0) then begin
        ColumnHeaders[xCount] := Translate(aColProperties[xCount].Name);
        ColWidths[xCount] := aColProperties[xCount].Width;
        ColCount := ColCount + 1;
        if (AnsiStrPos(PChar(ColumnHeaders[xCount]), PChar(xStr))<>NIL) then  //Ersetzen %s durch Garneinheit (Muss NACH translate erfolgen!!! Nue:26.8.03
          ColumnHeaders[xCount] := Format(ColumnHeaders[xCount], [cYarnUnitsStr[mYarnUnit]]);
      end; //if
      inc(xCount);
    end;
    ColCount := ColCount - 1;
    fGridObjectCol := ColCount - 1;
    HideColumn(ColCount - 1);
//      ShowColumnHeaders;
  end; //with
  mFilterForm.InitState := True;

  //Setzen des FabrikPattern
  Settings1.Model.SetFabFPattern;

  //Gef�llt wird das Grid �ber sp�teren FillList (Aufruf von SettingsNavFrame, oder von intern)
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.ToolbarManager;
begin
  acHelp.Visible := True;
  acFilter.Visible := True;
  acNoFilter.Visible := True;
  acPrint.Visible := True;
  acLotParameter.Visible := (mActSource = stHistory);
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, self.HelpContext);
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.acNoFilterExecute(Sender: TObject);
begin
  mFilterForm.NoFilter := True;
//  mInitRange := True;
  FillList(mActSource);
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.FormShow(Sender: TObject);
begin
  mMainPanel.ActivePageIndex := 0;
  if (Self.Owner.Owner is TForm) then begin
    Left := (Self.Owner.Owner as TForm).Left + (((Self.Owner.Owner as TForm).Width - (Self.Owner.Owner as TForm).ClientWidth) div 2);
    Top := (Self.Owner.Owner as TForm).Top + ((Self.Owner.Owner as TForm).Height - (Self.Owner.Owner as TForm).ClientHeight);
    Width := (Self.Owner.Owner as TForm).ClientWidth;
    Height := (Self.Owner.Owner as TForm).ClientHeight;
  end;
  mTakeOverSettings := False;

  //Tempor�r (solange Window showed) setzen des GUIMode, damit Settings richtig angezeigt werden
  mSaveGUIState := Settings1.GUIMode;
  Settings1.ReadOnly := True;
  Settings1.GUIMode := gmTemplate; //Damit die Settings (Siro usw.) richtig angezeigt werden ?? Nue:10.7.03

  if (mSpindleRangeList = nil) {Aufruf �ber OLE_Server von Floor (stHistory)} then
    FillList(mActSource);
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //Zur�cksetzen: Tempor�r (solange Window showed) setzen des GUIMode, damit Settings richtig angezeigt werden
  Settings1.GUIMode := mSaveGUIState; //Damit die Settings (Siro usw.) richtig angezeigt werden ?? Nue:10.7.03

  if (mTakeOverSettings) and (not (mNoItemFound)) then
    ModalResult := mrOk
  else
    ModalResult := mrCancel;
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.mExtStringGridRowChanging(Sender: TObject; OldRow, NewRow: Integer; var Allow: Boolean);
var
  xString: string;
begin
  case mActSource of
    stStyle: begin
        xString := Format('%s: %s', [cStyle, mExtStringGrid.Cells[1, NewRow]]);
      end;
    stHistory{stProdGrp}: begin
        xString := Format('%s: %s; %s: %s', [cMachine, mExtStringGrid.Cells[1, NewRow], cLotName, mExtStringGrid.Cells[5, NewRow]]);
      end;
  else
    Exit;
  end; //case
  mStatusBar.SimpleText := Format('%s: %d;  %s', [cNrOfDatasets, mExtStringGrid.RowCount - mExtStringGrid.FixedRows, xString]);
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.mExtStringGridGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var AAlignment: TAlignment);
begin
  //Das Alignment der Kolonnen muss �ber das Alignment der Zellen abgehandelt werden.
  if ARow >= mExtStringGrid.FixedRows then
    AAlignment := mInfoGrid[ACol].Align;
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.ToolButton13Click(Sender: TObject);
begin

end;
//------------------------------------------------------------------------------

procedure TListCobChooser.acCancelExecute(Sender: TObject);
begin
  Close
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.acAssignExecute(Sender: TObject);
begin
  if IsValidTKClass(mExtStringGrid.Row) then begin
    mTakeOverSettings := True;
    Close;
  end;
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.acPrintExecute(Sender: TObject);
begin
  if mMainPanel.ActivePageIndex = 0 then
    PrintList
  else
    PrintSettings;
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.tsSettingsShow(Sender: TObject);
begin
  try
    GetSettings(mExtStringGrid.Row);
  except
    on e: Exception do begin
      raise Exception.Create(Format('TListCobChooser.tsSettingsShow failed for SetID=%d. ',
        [(mExtStringGrid.Objects[GridObjectCol, mExtStringGrid.Row] as TBaseSetIDItem).SetID]) + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.SetFormCaption; //Setzt Fenstertitel mit selektiertem Itemname
begin
  with mExtStringGrid do begin
    if (Objects[GridObjectCol, Row] is TBaseItem) then begin
      case mActSource of
        stTemplate: begin
            Self.Caption := Format('%s: [%s]', [cTemplatelist, (Objects[GridObjectCol, Row] as TBaseItem).Name]);
          end;
        stOrderPosition, stStyle: begin
            Self.Caption := Format('%s: [%s]', [cStylelist, (Objects[GridObjectCol, Row] as TBaseItem).Name]);
          end;
        stHistory: begin
            Self.Caption := Format('%s: [%s]', [cLotlist, (Objects[GridObjectCol, Row] as TBaseItem).Name]);
          end;
      else // stMaMemory stProdGrp
        // Wird nicht in ListCobChooser abgehandlet, sondern schiesst die Daten direkt in die ListBox cobChooser
        // Abhandlung inSettingsNav.FillList
      end; // case
    end; // if
  end; // with
end;
//------------------------------------------------------------------------------
procedure TListCobChooser.PrintList;
var
  xTSG: TStringGrid;
  c, r: Integer;
  xText : String;
begin
  // da mExtStringGrid HiddenCols haben kann, m�ssen die "sichtbaren" Daten erst
  // in ein normales StringGrid umkopiert werden, bevor diese an die Printkomponente
  // �bergeben werden.
  xTSG := TStringGrid.Create(Self);
  with mExtStringGrid do begin
    xTSG.ColCount := ColCount;
    xTSG.FixedCols := FixedCols;
    xTSG.FixedRows := FixedRows;
    for c := 0 to ColCount - 1 do
      xTSG.ColWidths[c] := ColWidths[RealColIndex(c)];

    xTSG.RowCount := RowCount;
    for r := 0 to RowCount - 1 do begin
      for c := 0 to ColCount - 1 do begin
        xTSG.Rows[r].Add(Cells[RealColIndex(c), r]);
      end;
    end;
  end;

  with TfrmCobChooserPrint.Create(Self) do
  try

    with TfrmPrintSettings.Create(self) do begin

         if Pos( Translate(cStylelist), Self.Caption ) > 0 then
              mPrintSetup.Portrait := TRUE;

         if Pos( Translate(cLotlist), Self.Caption ) > 0 then
              mPrintSetup.Portrait := FALSE;

         if Pos( Translate(cTemplatelist), Self.Caption ) > 0 then
              mPrintSetup.Portrait := FALSE;


         //mPrintSetup.Portrait := FALSE;
         //mPrintSetup.OrientationEnable := FALSE;

         mPrintSetup.BlackWhiteEnable := FALSE;

         ShowClearerSettings := FALSE;  

         if ShowModal = mrOk then begin

            mQuickRep.UsePrinterIndex(mPrintSetup.PrinterIndex);

            //qlApplication.Caption := gApplicationName;
            mQuickRep.ReportTitle := Format('%s  (%s)',[gApplicationName,  Self.Caption]);

            //Texte aus Self.Caption fuer Report extrahieren
            xText := Copy(Self.Caption, 1, Pos(':',Self.Caption) );
            xText := StringReplace(xText , ':', '', [rfReplaceAll]);
            Trim(xText);
            qlApplication.Caption := xText;

            {
            xText := Copy(Self.Caption, Pos('[',Self.Caption), length(Self.Caption) );
            xText := StringReplace(xText , '[', '', [rfReplaceAll]);
            xText := StringReplace(xText , ']', '', [rfReplaceAll]);
            Trim(xText);
            qlTitle.Caption := xText;
            }
            QRPrintStringGrid.DataGrid := xTSG; 

            QRPrintStringGrid.Print;
         end;

         Free;
    end;
    //


{
    mQuickRep.Page.Orientation := poLandscape;
    qlApplication.Caption := gApplicationName;
    qlTitle.Caption := Self.Caption;
    QRPrintStringGrid.DataGrid := xTSG;
    QRPrintStringGrid.Print;
//    QRPrintStringGrid.Active := True;
//    mQuickRep.Print;
}
  finally
    Free;
    xTSG.Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TListCobChooser.PrintSettings;
var
//  xYMSettingsByteArr: TYMSettingsByteArr;
//  xLength: Word;
  xOldCursor: TCursor;
  xPrnFrm: TfrmPrint;
begin
  with TfrmPrintSettings.Create(self) do
  try
    if ShowModal = mrOK then begin
      xPrnFrm := TfrmPrint.Create(self);
      with xPrnFrm do
      try
        mQuickReport.UsePrinterIndex(mPrintSetup.PrinterIndex);
        mQuickReport.Page.Orientation :=  mPrintSetup.Orientation;

        //XML Daten ans Printout Model
        VCLXMLSettingsModel_Print.xmlAsString := Settings1.Model.xmlAsString;


        with qrmChannel do begin
          ChannelVisible := ShowClearerSettingsSet.ShowChannel;
          SpliceVisible := ShowClearerSettingsSet.ShowSplice;
          ClusterVisible := ShowClearerSettingsSet.ShowFFCluster;
          BlackWhite := ShowClearerSettingsSet.PrintBlackWhite;
        end;

        qrmSiro.BlackWhite := ShowClearerSettingsSet.PrintBlackWhite;


        //Print Settings anzeigen
        QRXMLSettings_Channel.Enabled := ShowClearerSettingsSet.ShowChannel;
        QRXMLSettings_Cluster.Enabled := ShowClearerSettingsSet.ShowCluster;
        QRXMLSettings_FCluster.Enabled := ShowClearerSettingsSet.ShowFFCluster;
        QRXMLSettings_SFI.Enabled := ShowClearerSettingsSet.ShowSFI;
        QRXMLSettings_Splice.Enabled := ShowClearerSettingsSet.ShowSplice;
        QRXMLSettings_YarnCount.Enabled := ShowClearerSettingsSet.ShowYarnCount;


        qcbTemplate.Enabled := False;
        qcbLot.Enabled := False;
        qcbStyle.Enabled := False;
        qcbOrderPos.Enabled := False;
        qcbMachine.Enabled := False;
       

        case mActSource of
          stTemplate: begin
              qcbTemplate.Enabled := True;
              with mExtStringGrid do begin
                qlTemplate1.Caption := Cells[RealColIndex(1), Row];
              end;
              QRXMLSettings_YarnCount.YarnCount := 0;
            end;
          stHistory: begin
              qcbLot.Enabled := True;
              with mExtStringGrid do begin
                qlMachine2.Caption := Cells[RealColIndex(1), Row];
                qlSpindleRange2.Caption := Cells[RealColIndex(3), Row] + '-' + Cells[RealColIndex(4), Row];
                qlStyle2.Caption := Cells[RealColIndex(5), Row];
                if TMMSettingsReader.Instance.IsComponentStyle and not TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable then begin
                  qlLot2.Caption := Cells[RealColIndex(6), Row];
                  qlTemplate2.Caption := Cells[RealColIndex(7), Row];
                  qlSlip2.Caption     := Cells[RealColIndex(14), Row];
                end else begin
                  qlLot2.Caption := Cells[RealColIndex(5), Row];
                  qlTemplate2.Caption := Cells[RealColIndex(6), Row];
                  //qlSlip2.Caption     := Cells[RealColIndex(13), Row];
                end;
              end;
            end;
          stStyle: begin
              qcbStyle.Enabled := True;
              with mExtStringGrid do begin
                qlStyle3.Caption := Cells[RealColIndex(1), Row];
                qlTemplate3.Caption := Cells[RealColIndex(2), Row];
                qlSlip3.Caption     := Cells[RealColIndex(6), Row];
              end;
            end;
        else
          Exit;
        end;


        xOldCursor := Screen.Cursor;
        with mQuickReport do try
          Screen.Cursor := crHourGlass;
          Prepare;
          Print;
        finally
          QRPrinter.Free;
          QRPrinter := nil;
          Screen.Cursor := xOldCursor;
        end; // try mQuickReport




//ab hier orginal (ohne XML)

{wss Settings �ber Model
        qrmChannel.QMatrix.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);

//NUE1        Settings1.GetScreenSettings(xYMSettingsByteArr, xLength);
        qrmChannel.QMatrix.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
        qrmChannel.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
{}
        //Nue: Added 25.11.02
//        with qrmChannel do begin
//
//          ChannelVisible := cbChannel.Checked;
//          SpliceVisible := cbSplice.Checked;
//          ClusterVisible := cbFFCluster.Checked;
//
//          BlackWhite := mPrintSetup.PrintBlackWhite;
//        end;
//
//{wss Settings �ber Model
//        qrmSiro.QMatrix.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
//        qrsSplice.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
//        qrsChannel1.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
//        qrsChannel2.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
//{}
//
//        with qrsChannel1 do begin
//          ChannelVisible := cbChannel.Checked;
//          YarnCountVisible := cbYarnCount.Checked;
//          FaultClusterVisible := cbCluster.Checked;
//        end;
//        with qrsChannel2 do begin
//          SFIVisible := cbSFI.Checked;
//          FFClusterVisible := cbFFCluster.Checked;
//        end;
//        with qrsSplice do begin
//          SpliceVisible := cbSplice.Checked;
//          Color := clWhite;
//        end;
//
//
//        qrmSiro.BlackWhite := mPrintSetup.PrintBlackWhite;
//
//
//        qcbTemplate.Enabled := False;
//        qcbLot.Enabled := False;
//        qcbStyle.Enabled := False;
//        qcbOrderPos.Enabled := False;
//        qcbMachine.Enabled := False;
//
//        case mActSource of
//          stTemplate: begin
//              qcbTemplate.Enabled := True;
//              with mExtStringGrid do begin
//                qlTemplate1.Caption := Cells[RealColIndex(1), Row];
//              end;
//              qrsChannel1.YarnCount := 0;
//            end;
//          stHistory: begin
//              qcbLot.Enabled := True;
//              with mExtStringGrid do begin
//                qlMachine2.Caption := Cells[RealColIndex(1), Row];
//                qlSpindleRange2.Caption := Cells[RealColIndex(3), Row] + '-' + Cells[RealColIndex(4), Row];
//                qlStyle2.Caption := Cells[RealColIndex(5), Row];
//                if TMMSettingsReader.Instance.IsComponentStyle and not TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable then begin
//                  qlLot2.Caption := Cells[RealColIndex(6), Row];
//                  qlTemplate2.Caption := Cells[RealColIndex(7), Row];
//                  qlSlip2.Caption     := Cells[RealColIndex(14), Row];
//                end else begin
//                  qlLot2.Caption := Cells[RealColIndex(5), Row];
//                  qlTemplate2.Caption := Cells[RealColIndex(6), Row];
//                  //qlSlip2.Caption     := Cells[RealColIndex(13), Row];
//                end;
//              end;
//            end;
//          stStyle: begin
//              qcbStyle.Enabled := True;
//              with mExtStringGrid do begin
//                qlStyle3.Caption := Cells[RealColIndex(1), Row];
//                qlTemplate3.Caption := Cells[RealColIndex(2), Row];
//                qlSlip3.Caption     := Cells[RealColIndex(6), Row];
//              end;
//            end;
//        else
//          Exit;
//        end;
//
//        xOldCursor := Screen.Cursor;
//        with mQuickReport do try
//          Screen.Cursor := crHourGlass;
//          Prepare;
//          Print;
//        finally
//          QRPrinter.Free;
//          QRPrinter := nil;
//          Screen.Cursor := xOldCursor;
//        end; // try mQuickReport
      finally
        Free;
      end; // try xPrnFrm
    end;
  finally
    Free;
  end; // try TfrmPrintSettings
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.GetSettings(aRow: Integer);
begin
  with  mExtStringGrid do begin
    try
      Settings1.LoadFromDB((Objects[GridObjectCol, aRow] as TBaseSetIDItem).SetID);

      if (mActSource=stHistory) then begin
        //Selektierter Spindlerange setzten um kein Template zu signalisieren Nue:9.5.01
{ DONE 1 -oNue -cXMLSettings : Checken wenn FPattern von LOK implementiert ist }
        Settings1.Model.FPHandler.SetSpindleRange(StrToInt(TProdGrpItem(Objects[GridObjectCol, aRow]).SpindleFirst),
                                                  StrToInt(TProdGrpItem(Objects[GridObjectCol, aRow]).SpindleLast), fpNone);
//NUE1        Settings1.SpindleFirst := StrToInt(TProdGrpItem(Objects[GridObjectCol, aRow]).SpindleFirst);
//NUE1        Settings1.SpindleLast := StrToInt(TProdGrpItem(Objects[GridObjectCol, aRow]).SpindleLast);
        Settings1.YarnCnt := Round(TProdGrpItem(Objects[GridObjectCol, aRow]).YarnCnt);
      end
      else if (mActSource=stStyle) then begin
        Settings1.YarnCnt := Round(TStyleItem(Objects[GridObjectCol, aRow]).YarnCnt);
      end
      else begin //StTemplate
        Settings1.YarnCnt := 0;
        Settings1.Speed := 0;
        Settings1.SpeedRamp := 0;
        Settings1.PilotSpindles := 0;
      end;//IF

    except
      on e: Exception do begin
        raise Exception.Create(Format('TListCobChooser.GetSettings failed for SetID=%d. ',
          [(Objects[GridObjectCol, Row] as TBaseSetIDItem).SetID]) + e.Message);
      end;
    end;
  end; //With
end;
//------------------------------------------------------------------------------

procedure TListCobChooser.acMoveUpDownExecute(Sender: TObject);
begin
  try
    mExtStringGrid.Row := mExtStringGrid.Row+TControl(Sender).Tag;
    GetSettings(mExtStringGrid.Row);
    SetFormCaption;
  except
    on e: Exception do begin
      raise Exception.Create(Format('TListCobChooser.acMoveUpDownExecute failed for SetID=%d. ',
        [TBaseSetIDItem(mExtStringGrid.Objects[GridObjectCol, mExtStringGrid.Row]).SetID]) + e.Message);
    end;
  end;
end;


//------------------------------------------------------------------------------
procedure TListCobChooser.mmActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin
  Handled := True;
  acMoveUp.Enabled       := (mExtStringGrid.Row > mExtStringGrid.FixedRows);
  acMoveDown.Enabled     := (mExtStringGrid.Row < (mExtStringGrid.RowCount-1));
  acAssign.Enabled       := (mExtStringGrid.Cells[1, mExtStringGrid.Row]<>'');
  acLotParameter.Enabled := (mExtStringGrid.Cells[1, mExtStringGrid.Row]<>'');
  tsSettings.TabVisible  := (mExtStringGrid.Cells[1, mExtStringGrid.Row]<>'');
end;

//------------------------------------------------------------------------------
function TListCobChooser.GetSensingHeadClass: TSensingHeadClass;
var
  xCount: integer;
begin
//  Result := fSettings.ActSensingHeadClass;
  Result := shcNone;
  if mSpindleRangeList.ActSpdRange is TBaseSpindleRange then begin
//Alt    for xCount:=0 to cZESpdGroupLimit-1 do begin
    with mSpindleRangeList.AssMachine.MaConfigReader do begin
      for xCount:=0 to GroupCount-1 do begin
        //Check richtiger Spindelbereich
        if (TBaseSpindleRange(mSpindleRangeList.ActSpdRange).SpindleFirst >= GroupValueDef[xCount, cXPSpindleFromItem, 1]) and
           (TBaseSpindleRange(mSpindleRangeList.ActSpdRange).SpindleLast  <= GroupValueDef[xCount, cXPSpindleToItem, 1]) then begin
//          Result := TYMMachineConfig.ConvertSensingHeadTypeToClass(GetSensingHeadValue(GroupValueDef[xCount, cXPSensingHeadItem,cSensingHeadNames[shTK830]]));
          Result := YMParaUtils.GetSensingHeadClass(GetSensingHeadValue(GroupValueDef[xCount, cXPSensingHeadItem,cSensingHeadNames[shTK830]]));
          BREAK; //!!!!!!!!!!!!!!!!!!!!!
        end; //if
      end; //for
    end; //with
  end;//if
end;

//------------------------------------------------------------------------------
function TListCobChooser.IsValidTKClass(aRow: Integer): Boolean;
begin
  if (TBaseSetIDItem(mExtStringGrid.Objects[fGridObjectCol,aRow]).HeadClass = ActTKClass) OR
      (ActTKClass=shc8x) {Notl�sung, falls in ListCobchooser ohne TKKlasse eingetreten wird} then begin
    Result := True
  end
  else begin
    WarningMsg(rsNotSameTKforProdGrp, Self);
    Result := False;
  end;//else
end;

//------------------------------------------------------------------------------
procedure TListCobChooser.mExtStringGridGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if Assigned(mExtStringGrid.Objects[fGridObjectCol,ARow]) then
    if NOT(((mExtStringGrid.Objects[fGridObjectCol,ARow] as TBaseSetIDItem).HeadClass = ActTKClass) OR
      (ActTKClass=shc8x)) {Notl�sung, falls in ListCobchooser ohne TKKlasse eingetreten wird} then begin
      if ACol=0 then
        ABrush.Color := clInfoBk
      else
        AFont.Color := clGrayText;
    end;
end;

//------------------------------------------------------------------------------

procedure TListCobChooser.CreateParams(var Params: TCreateParams);
begin
  inherited;

  // PPW 21/06/2017 : MM-13 Fix for modal dialogs so they stay in front on their parent
  Params.WndParent := TForm(Owner).Handle;
end;

end.

