inherited MachineSel: TMachineSel
  Left = 518
  Top = 185
  Caption = '(30)Maschinen'
  ClientHeight = 288
  ClientWidth = 204
  PixelsPerInch = 96
  TextHeight = 13
  inherited bOK: TmmButton
    Left = 12
    Top = 258
    OnClick = bOKClick
  end
  inherited bCancel: TmmButton
    Left = 104
    Top = 258
    Width = 91
  end
  object mmListBox1: TmmListBox
    Left = 8
    Top = 8
    Width = 185
    Height = 241
    Enabled = True
    ItemHeight = 13
    TabOrder = 2
    Visible = True
    OnDblClick = mmListBox1DblClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 176
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
end
