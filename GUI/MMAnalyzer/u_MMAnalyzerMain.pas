unit u_MMAnalyzerMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mmDataSource, DBTables, mmQuery, mmDatabase, ImgList, mmImageList,
  ActnList, mmActionList, ComCtrls, StdCtrls, mmRichEdit, ToolWin,
  mmToolBar, mmStatusBar, ExtCtrls, mmImage, mmPanel, Registry, Privileg,
  NTCommon, UserMan, TService, Grids, DBGrids;

type

  TMillMasterType = (mtClient, mtServer, mtLight);

  TMMFont = record
    FontName   : String;
    FontSize   : Integer;
    FontColor  : TColor;
    FontStyle  : TFontStyles;
  end;

  TAnalyer = class(TForm)
    StatusBar: TmmStatusBar;
    reAnalyseReport: TmmRichEdit;
    mmActionList1: TmmActionList;
    acExit: TAction;
    acPrint: TAction;
    acAnalyze: TAction;
    acEdit: TAction;
    mmImageList1: TmmImageList;
    acInfo: TAction;
    mmPanel1: TmmPanel;
    mmPanel2: TmmPanel;
    mmPanel3: TmmPanel;
    ToolBar: TmmToolBar;
    tbExit: TToolButton;
    tbPrint: TToolButton;
    tbAnalyze: TToolButton;
    tbSettings: TToolButton;
    tbInfo: TToolButton;
    ToolButton1: TToolButton;
    mDatabase: TmmDatabase;
    mmDataSource1: TmmDataSource;
    mmQuery: TmmQuery;
    NTUserMan: TNTUserMan;
    NTPrivilege: TNTPrivilege;
    NTService: TNTService;
    dbSQLServer: TmmDatabase;
    qSQLServer: TmmQuery;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acAnalyzeExecute(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
    mMMAnayzePath       : String;
    mMillMasterRootPath : String;
    mPreCheckTextList,
    mUsersGroups        : TStringList;


    fisNT, FUncomplete  : Boolean;
    fMillMasterType     : TMillMasterType;
    FErrorCounts        : Word;
    FComputerName       : String;
    FSQLServerReady                    : Boolean;
    FMMInstalled, FBDEInstalled        : Boolean;

    procedure AddErrorText(aText: String);
    procedure AddText(aText: String);
    procedure Analyze;
    procedure CreateAlias;
    procedure DeleteAlias;
    procedure Init;
    procedure SetReportHeader;
    function CheckKeyExists(aRootKey : HKEY; aKey : String):Boolean;
    procedure CreateTables;
    function ExistsTable(aPath : String ):Boolean;

    // MillMaster checks
    procedure CheckOS;                // is NT and SP >= 6 ?
    function CheckMMExists: Boolean;  // is MM installed?
    procedure GetMillMasterType;      // MM-Produkt (TMillMasterType)
    procedure CheckODBC;
    procedure CheckBDE;
    procedure CheckSQLServer;
    procedure CheckMMGroups;
    procedure CkeckUserRight;
    procedure CheckTexNet;
    procedure CheckDistinct; //WSC
    procedure CheckNTServicesDevices(aType: TEnumSevices);

    property isNT : Boolean read FisNT write FisNT;
    property MillMasterType : TMillMasterType read FMillMasterType write FMillMasterType;
    property Uncomplete : Boolean read FUncomplete write FUncomplete;
    property ErrorCounts : Word read FErrorCounts write FErrorCounts default 0;
    property ComputerName : String read FComputerName write FComputerName;
    property SQLServerReady: Boolean  read FSQLServerReady default FALSE;
    property MillMasterInstalled : Boolean read FMMInstalled write FMMInstalled default TRUE;
    property BDEInstalled : Boolean read FBDEInstalled write FBDEInstalled default TRUE;

  public
    { Public declarations }
  end;

var
  Analyer: TAnalyer;

const
      cMMTableFiles    = 'MMF.db';
      cDriver          = 'STANDARD';      //'MSACCESS';
      cNewAlias        = 'MMAnalyzer';
      cDBName          = 'MMAnalyzeDB';
      cApplCaption     = 'MMAnalyzer';

      cODBCName        = 'MM_WindingODBC';
      cBDENative       = 'MM_WindingNative';
      cMMDBName        = 'MM_Winding';
      cDBDriver        = 'sqlsrv32.dll';
      cMMWinding_Data  = 'MM_Winding_Data.MDF';
      cMMWinding_Log   = 'MM_Winding_Data.MDF';
      cSQLServerVers   = 7;
      cSQLServerSP     = 699; // ->  7.00.699 = SP1
      cSQLServerSPNr   = 1;

      cMMUserGroup     = 'MMUserGroup';
      cMMAdminGroup    = 'MMAdminGroup';
      cMMAdvancedGroup = 'MMAdvancedGroup';

      cMMSuppUser      = 'MMSupp';
      cMMSystemUser    = 'MMSystemUser';
      cMMAdminUser     = 'MMAdmin';

implementation // 15.07.2002 added mmMBCS to imported units

{$R *.DFM}

uses
  mmMBCS,
 u_AnalyzerEditForm, LoepfeGlobal, mmRegistry, u_PrintForm;

// Supportfunktionen
// +++++++++++++++++

//------------------------------------------------------------------------------
procedure TAnalyer.FormCreate(Sender: TObject);
begin
 Caption := cApplCaption;
 mPreCheckTextList := TStringList.Create;
 mUsersGroups      := TStringList.Create;

 Init;
 CheckBDE;

 if BDEInstalled then CreateAlias;
end;
//------------------------------------------------------------------------------
procedure TAnalyer.FormDestroy(Sender: TObject);
begin
  mPreCheckTextList.Free;
  mUsersGroups.Free;
  DeleteAlias;
end;
//------------------------------------------------------------------------------
procedure TAnalyer.CreateTables;
var xDBFile : String;
begin
  xDBFile :=  mMMAnayzePath + '\' + cMMTableFiles;
  if not ExistsTable(xDBFile) then
     with mmQuery do begin
          SQL.Clear;
          SQL.ADD('Create Table "' +  cMMTableFiles + '"(');
          SQL.ADD('Appkey CHAR(255), ');
          SQL.ADD('FileName CHAR(255), ');
          SQL.ADD('Path CHAR(255), ');
          SQL.ADD('Version CHAR(20), ');
          SQL.ADD('InstallType CHAR(5), ');
          SQL.ADD('Result BOOLEAN DEFAULT 1 )');
          ExecSQL;
     end;
end;
//------------------------------------------------------------------------------
function TAnalyer.ExistsTable(aPath: String): Boolean;
begin
  if not FileExists(aPath) then
     Result := FALSE
  else
     Result := TRUE;
end;
//------------------------------------------------------------------------------
procedure TAnalyer.CreateAlias;
var xList  : TStringList;
begin

  mDatabase.Connected  := FALSE;
  mmQuery.Active        := mDatabase.Connected;

  with Session do begin
    if not IsAlias(cNewAlias) then begin
       xList := TStringList.Create;
       try
         with xList do begin
             Add('Database name=' + mMMAnayzePath );
             AddAlias(cNewAlias, cDriver, xList);
             //SaveConfigFile;
         end;
       finally
         xList.Free;
       end;
       //SaveConfigFile;
    end else
      // ShowMessage( cNewAlias + ' does exists!');
    end;

    mDatabase.AliasName    := cNewAlias;
    mDatabase.DatabaseName := cDBName;
    mDatabase.DriverName   := cDriver;
    mDatabase.Params.Clear;
    mDatabase.Params.Add('PATH=' + mMMAnayzePath);

    mmQuery.DatabaseName   := mDatabase.DatabaseName;
    mmQuery.Active         := mDatabase.Connected;
    mDatabase.Connected    := TRUE;
end;
//------------------------------------------------------------------------------
procedure TAnalyer.DeleteAlias;
begin
  if not BDEInstalled then exit;
  Session.DeleteAlias(cNewAlias);
  //Session.SaveConfigFile;
end;
//------------------------------------------------------------------------------
procedure TAnalyer.Init;
var xOs : TOSVersionInfo;
    xDBFile : String;
    xName  : PChar;
    xsize  : DWord;
begin

  xsize:=255;
  xName:= StrAlloc(256);
  GetComputerName(xName, xsize);
  ComputerName :=  xName;
  StrDispose(xName);

  with reAnalyseReport do begin
   Clear;
   WantTabs  := TRUE;
   WantReturns:=FALSE;
   Paragraph.FirstIndent := 20;
   Paragraph.LeftIndent  := 120;
  end;

  xOs.dwOSVersionInfoSize:= sizeof(xOs);
  GetVersionEx(xOs);

  if xOs.dwPlatformId =  VER_PLATFORM_WIN32_NT then begin
     GetMillMasterType;
     case MillMasterType of
       mtClient  :  Caption := cApplCaption + '   [ Client version ]';
       mtServer  :  Caption := cApplCaption + '   [ Server version ]';
       mtLight   :  Caption := cApplCaption + '   [ Light version ]';
     end;
  end;

  mMMAnayzePath:=ExtractFilePath(Application.ExeName);

  // Paradox Tabellen check
  if BDEInstalled then CreateTables;
end;
//------------------------------------------------------------------------------
procedure TAnalyer.SetReportHeader;
var xText  : String;
    xLines : Byte;
begin

   reAnalyseReport.Clear;
   with reAnalyseReport.SelAttributes do begin
        Name:= 'ARIAL';
        Color:= clBlack;
        Size:= 10;
   end;
   xText:= 'Computername: ' + #9  + ComputerName;
   reAnalyseReport.Paragraph.FirstIndent:= 20;  // linker Rand in Pixel

   reAnalyseReport.Paragraph.TabCount:=5;
   reAnalyseReport.Paragraph.Tab[reAnalyseReport.Paragraph.TabCount - 1];


   reAnalyseReport.Paragraph.Numbering := TNumberingStyle(FALSE);
   reAnalyseReport.Lines.Add(xText);

   xText:= 'Date: ' + #9 + #9 + DateToStr(Date);
   reAnalyseReport.Paragraph.Numbering := TNumberingStyle(FALSE);
   reAnalyseReport.Lines.Add(xText);
   reAnalyseReport.Lines.Add('');

   //TabCtrl_SetItemSize( reAnalyseReport , 200, 100);


   with reAnalyseReport.SelAttributes do begin
        Color:= clBlack;
        Size:= 14;
        Style:= Style + [fsUnderline];
   end;
   reAnalyseReport.Lines.Add('Report');

   with reAnalyseReport.SelAttributes do begin
        Color:= clRed;
        Size:= 10;
        Style:= Style - [fsUnderline];;
   end;
   reAnalyseReport.Lines.Add('');

   reAnalyseReport.Paragraph.Numbering := TNumberingStyle(TRUE);


end;
//------------------------------------------------------------------------------
procedure TAnalyer.AddErrorText(aText: String);
begin
  reAnalyseReport.Paragraph.Numbering := TNumberingStyle(TRUE);
  reAnalyseReport.Lines.Add(aText);
  reAnalyseReport.Paragraph.Numbering := TNumberingStyle(FALSE);
  inc(FErrorCounts);
end;
//------------------------------------------------------------------------------
procedure TAnalyer.AddText(aText: String);
begin
  reAnalyseReport.Paragraph.Numbering := TNumberingStyle(TRUE);
  reAnalyseReport.Lines.Add(aText);
  reAnalyseReport.Paragraph.Numbering := TNumberingStyle(FALSE);
end;
//------------------------------------------------------------------------------
procedure TAnalyer.Analyze;
var x : Byte;
    xText : String;
begin

  tbAnalyze.Cursor       := crHourGlass;
  reAnalyseReport.Cursor := tbAnalyze.Cursor;
  ToolBar.Cursor         := tbAnalyze.Cursor;

  SetReportHeader;

  // Ueberpruefung aller MM-Typen
  CheckOS;

  // Procedures for analyzing
  if not isNT then begin
     AddErrorText('The MillMaster runs only with Windows NT.');
     exit;
  end;

  // Pre Check-Text ausgeben
  if mPreCheckTextList.Count > 0 then
    for x:= 0 to mPreCheckTextList.Count-1 do
        AddErrorText( mPreCheckTextList.Strings[x] );

  mPreCheckTextList.Clear;

  CheckODBC;

  MillMasterInstalled := CheckMMExists;

  if not MillMasterInstalled then AddErrorText('The MillMaster is not installed.');

   // Spezifische Ueberpruefung
  case MillMasterType of
       mtClient  :  begin
                     CkeckUserRight;
                    end;
       mtServer,
       mtLight   : begin
                     CheckSQLServer;

                     CheckMMGroups;
                     CkeckUserRight;

                     CheckTexNet;
                     CheckNTServicesDevices([PROCESS]);


                   end;
  end;



  if ErrorCounts = 0 then
     AddText('The MillMaster is correct installed.')
  else begin
    xText:= Format('%d Items are not installed or found.', [ErrorCounts]);
    StatusBar.SimpleText:= xText;
  end;

  reAnalyseReport.Cursor := crDefault;
  ToolBar.Cursor         := crDefault;
  tbAnalyze.Cursor       := crHandPoint;
end;
//------------------------------------------------------------------------------


// Actions
// +++++++

//------------------------------------------------------------------------------
procedure TAnalyer.acAnalyzeExecute(Sender: TObject);
begin
  Analyze;
end;
//------------------------------------------------------------------------------
procedure TAnalyer.acExitExecute(Sender: TObject);
begin
  Application.Terminate;
end;
//------------------------------------------------------------------------------
procedure TAnalyer.acPrintExecute(Sender: TObject);
var xPrintForm: TPrintForm;
    xStream :  TMemoryStream;
    x : String;
begin
 xStream:=  TMemoryStream.Create;
 reAnalyseReport.Lines.SaveToStream( xStream ) ;
 xStream.Position := 0;

 with TPrintForm.Create(Self) do begin
    qrRichText.Lines.LoadFromStream( xStream );
    mmQuickRep1.Prepare;
    //mmQuickRep1.Preview;
    //mmQuickRep1.PrinterSetup;
   //
    mmQuickRep1.Print;
    Free;
  end;

 xStream.Free;

end;
//------------------------------------------------------------------------------
procedure TAnalyer.acEditExecute(Sender: TObject);
begin
  // with TAnalyzeEditForm.Create(Self, mDatabase) do ShowModal;
end;
//------------------------------------------------------------------------------




// Procedure zur MM Ueberpruefung
// ++++++++++++++++++++++++++++++

//------------------------------------------------------------------------------
function TAnalyer.CheckMMExists: Boolean;
var
  xReg : TRegistry;
  xKey : String;
begin

  xKey:= cRegMillMasterPath;
  xReg := TRegistry.Create;
  try
    xReg.RootKey := HKEY_LOCAL_MACHINE;

    if xReg.KeyExists(xKey)then begin
       xReg.OpenKey(xKey, FALSE);
       mMillMasterRootPath := xReg.ReadString('MillMasterRootPath');
       if mMillMasterRootPath = '' then
          Result := FALSE
       else
          Result := TRUE;
    end else begin
       inc(FErrorCounts);
       Result := FALSE;
    end;
  finally
    xReg.CloseKey;
    xReg.Free;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Ermittelt das Betriebssystem mit SP
//******************************************************************************
procedure TAnalyer.CheckOS;
var xOs                : TOSVersionInfo;
    xVersion, xSPNrStr : String;
    xPlatformID        : DWORD;
    xSP                : PChar ;
    xSPNr              : Byte;
begin
  xOs.dwOSVersionInfoSize:= sizeof(xOs);
  GetVersionEx(xOs);

  xSp:= xOs.szCSDVersion;  // 'Service Pack 6'
  xSPNrStr:= Copy(xSp, Pos('k', xSp)+ 2 , 5);
  try
    xSPNr:=  StrToInt(xSPNrStr);
  except
    xSPNr:= 0;
  end;

  xPlatformID :=  xOs.dwPlatformId;

  if xPlatformID <> VER_PLATFORM_WIN32_NT then begin
     isNT:= FALSE;
     xVersion:= Format('Windows version %d.%d. Build : %d is installed. The MillMaster needs Windows NT.', [xOs.dwMajorVersion,xOs.dwMinorVersion, xos.dwBuildNumber]);
     AddErrorText(xVersion);
  end else isNT:= TRUE;

  if ( xPlatformID = VER_PLATFORM_WIN32_NT ) and  (xSPNr < 6 ) then begin
     xVersion:= Format('Windows NT %s is installed.', [xSp]);
     AddErrorText(xVersion);
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// SQL-Server
//******************************************************************************
procedure TAnalyer.CheckSQLServer;
var xKey, xPath, xVers, xNr ,xText : String;
    xReg : TRegistry;
    xVersNr : Integer;
begin
  // - Check SQL Server und SP >=7

  xReg := TRegistry.Create;
  try
    xReg.RootKey := HKEY_LOCAL_MACHINE;
    xKey:= '\SOFTWARE\Microsoft\MSSQLServer\Setup\';

    if xReg.KeyExists(xKey)then begin
       // SQL-Server
       xReg.OpenKey(xKey, FALSE);
       xPath := xReg.ReadString('SQLPath');
       if not FileExists(xPath + '\Binn\SQLServr.exe') then begin
          AddErrorText('The SQL Server is not installed.' );
          FSQLServerReady := FALSE;
       end else begin
          xKey:= '\SOFTWARE\Microsoft\MSSQLServer\MSSQLServer\CurrentVersion\';
          xReg.OpenKey(xKey, FALSE);
          xVers :=  xReg.ReadString('CurrentVersion');
          xNr:= copy( xVers, 0, Pos('.', xVers)-1 );
          try
            xVersNr:= StrToInt(xNr);
            if xVersNr < cSQLServerVers then begin
               xText:= Format('The SQL Server version is smaler than version %d .', [ cSQLServerVers]);
               AddErrorText(xText);
            end;
          except
          end;
       end;

       // SQL-Server SP  -> cSQLServerSP     = 699; // ->  7.00.699 = SP1
       xKey:= '\SOFTWARE\Microsoft\MSSQLServer\MSSQLServer\CurrentVersion\';

       if xReg.KeyExists(xKey)then begin
          xReg.OpenKey(xKey, FALSE);
          xVers := xReg.ReadString('CSDVersion');

          xNr:= copy( xVers, Pos('.', xVers)+1, length(xVers) );
          xNr:= copy( xNr, Pos('.', xNr)+1, length(xNr) );

          try
            xVersNr:= StrToInt(xNr);
            if xVersNr < cSQLServerSP then begin
               xText:= Format('The SQL Server Service pack is smaler than %d .', [ cSQLServerSPNr]);
               AddErrorText(xText);
            end;
          except
          end;

       end else begin
          AddErrorText('The SQL Server has no Service pack installed.' );

       end;
    end else begin
       AddErrorText('The MS-SQL-Server is not installed.' );
    end;

  finally
    xReg.Free;
  end;

  if not MillMasterInstalled then exit;

  // - Check MM-Database MM_Winding.MDF
  xPath:= mMillMasterRootPath + '\Data\';

  if not FileExists( xPath + cMMWinding_Data) then
     AddErrorText('The MillMaster Database does not exists.' );

  if not FileExists( xPath + cMMWinding_Data) then
     AddErrorText('The MillMaster DatabaseLog does not exists.' );

end;
//------------------------------------------------------------------------------
procedure TAnalyer.GetMillMasterType;
var xKey     : String;
    xProduct : String;
    xProcess : String;
    xReg     : TRegistry;
    xVal     : TStringList;
    x        : integer;
begin

  xReg:=TRegistry.Create;
  xVal:=TStringList.Create;
  try
    with xReg do begin
        RootKey := HKEY_LOCAL_MACHINE;
        OpenKeyReadOnly('SYSTEM\');
        GetKeyNames(xVal);
        for x:=0 to xVal.Count -1 do begin
          xKey:=  'SYSTEM\' + xVal.Strings[x]  + '\Control\ProductOptions';
          if CheckKeyExists(HKEY_LOCAL_MACHINE, xKey) then break;
        end;
       xProduct:= Trim(UpperCase( GetRegString(cRegLM, xKey, 'ProductType') ));
    end;
  finally
    xReg.Free;
    xVal.Free;
  end;

  if StrComp('WINNT',PChar(xProduct)) = 0 then begin  // 'WINNT', 'SERVERNT', 'LANMANNT', 'PRODUCTSUITE'
     xKey:=  mMillMasterRootPath + '\MMGuard';
     if CheckKeyExists(HKEY_LOCAL_MACHINE, xKey) then begin
        xProcess:= GetRegString(HKEY_LOCAL_MACHINE, xKey, 'Process1') ;
        // MM ist auf einem NT-WKS installiert und laeuft als MM-Light
        if xProcess <> '' then MillMasterType:= mtLight;

        // MM ist auf einem NT-WKS installiert
     end else MillMasterType:= mtClient;
  end else
    // MM ist auf einem NT-Server installiert
    MillMasterType:= mtServer;
end;
//------------------------------------------------------------------------------
function TAnalyer.CheckKeyExists(aRootKey: HKEY; aKey: String): Boolean;
begin
  Result:=FALSE;
  with TmmRegistry.Create do
  try
    RootKey := aRootKey;
    Access:=  KEY_READ;
    // Pfad existiert nicht
    Result:= KeyExists(aKey) ;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Check auf vorghanden sein von ODBC
//******************************************************************************
procedure TAnalyer.CheckODBC;
var
  xReg : TRegistry;
  xKey : String;
  xDataBaseName, xServer, xDriver: String;
begin

  // ODBC Datenquellen check (DNS Eintag)
  xReg := TRegistry.Create;
  try
    xReg.RootKey := HKEY_LOCAL_MACHINE;
    xKey:= '\SOFTWARE\ODBC\ODBC.INI\' + cODBCName ;

    if xReg.KeyExists(xKey)then begin
       xReg.OpenKey(xKey, FALSE);
       xDataBaseName := xReg.ReadString('Database');
       xServer       := xReg.ReadString('Server');
       xDriver       := ExtractFileName(xReg.ReadString('Driver'));

       if xDataBaseName = '' then
          AddErrorText('The ODBC has no default database.' )
       else if xDataBaseName <> cMMDBName then
            AddErrorText('The ODBC has a wrong default database.' )
            else FSQLServerReady:= TRUE;

       if xDriver = '' then
          AddErrorText('The ODBC has no SQLServer dirver installed.' )
       else if xDriver <> cDBDriver then
          AddErrorText('The ODBC has a wrong dirver.' );

       if xServer = '' then AddErrorText('The ODBC has no connect to a Server.');
    end else
       AddErrorText('The ODBC as ' + cODBCName + ' is not installed.' );
  finally
    xReg.CloseKey;
    xReg.Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TAnalyer.ToolButton1Click(Sender: TObject);
begin
  FSQLServerReady:= TRue;
  CheckTexNet;
end;
//------------------------------------------------------------------------------
procedure TAnalyer.CheckMMGroups;
var xList, xUsers, xGroups : TStringList;
    xID : Integer;
    xMsg : String;
begin
  xList   := TStringList.Create;
  xUsers  := TStringList.Create;
  xGroups := TStringList.Create;

  xMsg:= ' not found on ' +  NTUserMan.LocalComputer + '.' ;

 // Local MMGroups

  if MillMasterType =  mtServer then
     xList.Assign(NTUserMan.GlobalGroups);

  if MillMasterType =  mtLight then
     xList.Assign(NTUserMan.LocalGroups)
  else exit;


  xList.Assign(NTUserMan.LocalGroups);
  xList.Sort;

  if xList.Find(cMMUserGroup, xID) then
     xGroups.Add(xList.Strings[xID])
  else
     AddErrorText('Group : ' + cMMUserGroup + xMsg);

  if xList.Find(cMMAdminGroup, xID) then
     xGroups.Add(xList.Strings[xID])
  else
     AddErrorText('Group : ' + cMMAdminGroup + xMsg);


  // Local MMUser
  xList.Assign(NTUserMan.Users);
  xList.Sort;

  if xList.Find(cMMSuppUser, xID) then
     xUsers.Add(xList.Strings[xID])
  else
     AddErrorText('User : ' + cMMSuppUser + xMsg);

  if xList.Find(cMMSystemUser, xID) then
     xUsers.Add(xList.Strings[xID])
  else
     AddErrorText('User : ' + cMMSystemUser + xMsg);

  if xList.Find(cMMAdminUser, xID) then
     xUsers.Add(xList.Strings[xID])
  else
     AddErrorText('User : ' + cMMAdminUser + xMsg);

{
  mUsersGroups.AddStrings(xUsers);
  mUsersGroups.Assign(xGroups);
}

  xList.Free;
  xGroups.Free;
  xUsers.Free;

end;
//------------------------------------------------------------------------------
procedure TAnalyer.CkeckUserRight;
var xList, xUsers: TStringList;
    xID : Integer;
    xMsg, PCName : String;
begin
  xList   := TStringList.Create;
  xUsers  := TStringList.Create;

  PCName:=   GetPrimaryDomainName;
  xMsg:= ' has no user right as "Act as part of the operating system" on ' +  PCName + '.' ;

  //'SeTcbPrivilege'

  NTPrivilege.Privilege := 'SeTcbPrivilege';
  xList.Assign(NTPrivilege.Accounts);
  xList.Sort;

  if xList.Find(PCName + '\' + cMMUserGroup , xID) then
     xUsers.Add(xList.Strings[xID])
  else
     AddErrorText('The ' + cMMUserGroup + xMsg);

  if xList.Find(PCName + '\' + cMMAdminGroup, xID) then
     xUsers.Add(xList.Strings[xID])
  else
     AddErrorText('The ' + cMMAdminGroup + xMsg);


  if xList.Find(PCName + '\' + cMMSystemUser, xID) then
     xUsers.Add(xList.Strings[xID])
  else
     AddErrorText('The ' + cMMSystemUser + xMsg);

  xList.Free;
  xUsers.Free;
end;
//------------------------------------------------------------------------------
procedure TAnalyer.CheckDistinct;
begin

end;
//------------------------------------------------------------------------------
procedure TAnalyer.CheckTexNet;
begin
  if SQLServerReady then begin
     with qSQLServer do begin
        SQL.Clear;
        SQL.Add('Select m.c_Machine_id from  t_Machine m, t_net n where m.c_Machine_id > 0 ');
        SQL.Add('And m.c_net_id = n.c_net_id And n.c_net_name = ' + '''TEXNET''' );
        Open;

        // Eintraege vorhanden
        if not Eof then CheckNTServicesDevices([DRIVER]);
     end;

  end;

end;
//------------------------------------------------------------------------------
procedure TAnalyer.CheckNTServicesDevices(aType: TEnumSevices);
var
  i             : integer;
  xEnumList     : TEnumList;
  xMsg          : String;
  xTexNetExists : Boolean;
begin
  xEnumList := TEnumList.Create(Self);

  NTService.ActiveManager := false;

  NTService.MachineName := ComputerName;
  NTService.ManagerAccess := [M_CONNECT, M_ENUMERATE_SERVICE];
  if xEnumList <> nil then begin xEnumList.Free; xEnumList := nil; end;

  xEnumList := NTService.GetServiceList([STATE_ACTIVE, STATE_INACTIVE], aType);

  xMsg := 'The service %s stopped.';
  xTexNetExists:= FALSE;
  for i := 0 to xEnumList.Count - 1 do begin

      // Device check
      if aType = [DRIVER] then begin
        if Uppercase(xEnumList[i].ServiceName) = UpperCase('TexNet') then begin
           if xEnumList[i].CurrentState = STOPPED then
              AddErrorText(Format('The Device %s stopped.', [xEnumList[i].ServiceName])  );
           xTexNetExists:= TRUE;
        end;
      end;

      // Process check
      if aType = [PROCESS] then begin
        if Uppercase(xEnumList[i].ServiceName) = UpperCase('Loepfe MillMaster') then
           if xEnumList[i].CurrentState = STOPPED then
              AddErrorText(Format(xMsg, [xEnumList[i].ServiceName]));

        if Uppercase(xEnumList[i].ServiceName) = UpperCase('MSSQLServer') then
           if xEnumList[i].CurrentState = STOPPED then
              AddErrorText(Format(xMsg, [xEnumList[i].ServiceName]));
      end;
  end;

  if (aType = [DRIVER]) and (xTexNetExists = FALSE) then begin
     AddErrorText('The Texnet does not exists.' );
  end;

  NTService.ActiveManager := false;
  xEnumList.Free;
end;
//------------------------------------------------------------------------------
procedure TAnalyer.CheckBDE;
var
  xReg : TRegistry;
  xKey : String;
  xBDEPath, xBDEConfig, xNativeDB: String;
begin
   xReg := TRegistry.Create;
  try
    // BDE
    xReg.RootKey:=  HKEY_LOCAL_MACHINE;
    xKey:= '\SOFTWARE\Borland\Database Engine';
    if xReg.KeyExists(xKey)then begin
       xReg.OpenKey(xKey, FALSE);
       xBDEPath   := xReg.ReadString('DLLPATH');
       xBDEConfig := xReg.ReadString('CONFIGFILE01');

       if not FileExists(xBDEPath + '\bdeadmin.exe') then begin
          mPreCheckTextList.Add( 'The BDE is not installed.');
          BDEInstalled := FALSE;
       end;

       if not FileExists(xBDEConfig) and BDEInstalled then begin
          mPreCheckTextList.Add('The BDE-Config file (IDAPI32.CFG) does not exists.' );
          BDEInstalled := FALSE;
       end;

       // BDE Native check
       //if BDEInstalled then begin
       try
         if not Session.IsAlias(cBDENative) then AddErrorText('The Databse ' + cBDENative + ' is not in the BDE registered.' );
       finally
       end;

       try
         if not Session.IsAlias(cODBCName) then  AddErrorText('The Databse ' + cODBCName + ' is not in the BDE registered.' );
       finally
       end;
       //end;
    end else begin
              mPreCheckTextList.Add('The BDE is not installed.' );
              BDEInstalled := FALSE;
             end;
  finally
    xReg.CloseKey;
    xReg.Free;
  end;
end;

end.
