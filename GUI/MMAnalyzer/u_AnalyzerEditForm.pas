unit u_AnalyzerEditForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, Db, DBTables, mmQuery, mmDatabase, mmDataSource, Grids,
  DBGrids, mmDBGrid, DBCtrls, mmDBNavigator, StdCtrls, ExtCtrls,
  mmRadioGroup, mmPanel, ComCtrls, ToolWin, mmToolBar, IvMlDlgs,
  mmOpenDialog, ActnList, mmActionList, Buttons, mmSpeedButton, mmListBox;

type
  TAnalyzeEditForm = class(TForm)
    mmDataSource1: TmmDataSource;
    QuerryEditForm: TmmQuery;
    mmPanel1: TmmPanel;
    mmOpenDialog1: TmmOpenDialog;
    mmToolBar1: TmmToolBar;
    tbFile: TToolButton;
    tbRegistry: TToolButton;
    tbDatabase: TToolButton;
    ToolButton4: TToolButton;
    tbPick: TToolButton;
    mmActionList1: TmmActionList;
    acFile: TAction;
    acRegistry: TAction;
    acDatabase: TAction;
    acPickValues: TAction;
    mmPanel2: TmmPanel;
    mmDBNavigator1: TmmDBNavigator;
    mmDBGrid1: TmmDBGrid;
    mmPanel3: TmmPanel;
    mmListBox1: TmmListBox;
    sbAddToDB: TmmSpeedButton;
    procedure acFileExecute(Sender: TObject);
    procedure acRegistryExecute(Sender: TObject);
    procedure acDatabaseExecute(Sender: TObject);
    procedure acPickValuesExecute(Sender: TObject);
  private
    { Private declarations }
    FDatabase : TmmDatabase;
    procedure SetDatabase(const Value: TmmDatabase);

  protected
  public
    { Public declarations }
    constructor Create(AOwner: TComponent; aDB : TmmDatabase); overload;
  end;

var
  AnalyzeEditForm: TAnalyzeEditForm;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

{ TAnalyzeEditForm }

procedure TAnalyzeEditForm.SetDatabase(const Value: TmmDatabase);
begin
  FDatabase:= Value;
  QuerryEditForm.DatabaseName :=  Value.Name;
end;

procedure TAnalyzeEditForm.acFileExecute(Sender: TObject);
begin

  QuerryEditForm.SQL.Clear;
  QuerryEditForm.SQL.Add('Select * from t_File');
  FDatabase.Connected    := TRUE;
  QuerryEditForm.Open;
end;

procedure TAnalyzeEditForm.acRegistryExecute(Sender: TObject);
begin
  inherited;
  //
end;

procedure TAnalyzeEditForm.acDatabaseExecute(Sender: TObject);
begin
  inherited;
//
end;

procedure TAnalyzeEditForm.acPickValuesExecute(Sender: TObject);
begin
  mmOpenDialog1.Execute;
end;

constructor TAnalyzeEditForm.Create(AOwner: TComponent; aDB: TmmDatabase);
begin
  inherited Create(AOwner);
  FDatabase:=   aDB;
  QuerryEditForm.DatabaseName := FDatabase.DatabaseName;
  QuerryEditForm.RequestLive:= TRUE;
  tbFile.Down := TRUE;
  acFile.Execute;
end;

end.
