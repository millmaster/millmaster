object Form1: TForm1
  Left = 168
  Top = 135
  Width = 696
  Height = 480
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object mmDBGrid1: TmmDBGrid
    Left = 264
    Top = 104
    Width = 169
    Height = 145
    DataSource = mmDataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object mmButton1: TmmButton
    Left = 64
    Top = 128
    Width = 75
    Height = 25
    Caption = 'mmButton1'
    TabOrder = 1
    Visible = True
    OnClick = mmButton1Click
    AutoLabel.LabelPosition = lpLeft
  end
  object mDatabase: TmmDatabase
    LoginPrompt = False
    SessionName = 'Default'
    Left = 144
    Top = 72
  end
  object mmQuery: TmmQuery
    Left = 192
    Top = 72
  end
  object mmDataSource1: TmmDataSource
    DataSet = mmQuery
    Left = 264
    Top = 64
  end
end
