unit u_PrintForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QuickRpt, Qrctrls, mmQRRichText, mmQRBand, ExtCtrls, mmQuickRep,
  mmQRCompositeReport, mmQRSysData, mmQRLabel;

type
  TPrintForm = class(TForm)
    mmQuickRep1: TmmQuickRep;
    PageFooterBand1: TQRBand;
    mmQRSysData1: TmmQRSysData;
    mmQRLabel1: TmmQRLabel;
    DetailBand1: TQRBand;
    qrRichText: TmmQRRichText;
    TitleBand1: TQRBand;
    qlTitel: TmmQRLabel;
    procedure mmQuickRep1NeedData(Sender: TObject; var MoreData: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PrintForm: TPrintForm;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

procedure TPrintForm.mmQuickRep1NeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  ;
end;

end.
