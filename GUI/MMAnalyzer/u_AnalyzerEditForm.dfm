object AnalyzeEditForm: TAnalyzeEditForm
  Left = 155
  Top = 162
  Width = 879
  Height = 537
  Caption = 'MMAnalyzer values editor'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object mmPanel1: TmmPanel
    Left = 0
    Top = 33
    Width = 871
    Height = 477
    Align = alClient
    Caption = 'mmPanel1'
    TabOrder = 0
    object mmPanel2: TmmPanel
      Left = 251
      Top = 1
      Width = 619
      Height = 475
      Align = alClient
      Caption = 'mmPanel2'
      TabOrder = 0
      object mmDBNavigator1: TmmDBNavigator
        Left = 1
        Top = 1
        Width = 617
        Height = 25
        DataSource = mmDataSource1
        Align = alTop
        TabOrder = 0
      end
      object mmDBGrid1: TmmDBGrid
        Left = 1
        Top = 26
        Width = 617
        Height = 448
        Align = alClient
        DataSource = mmDataSource1
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object mmPanel3: TmmPanel
      Left = 1
      Top = 1
      Width = 250
      Height = 475
      Align = alLeft
      Caption = 'mmPanel3'
      TabOrder = 1
      object sbAddToDB: TmmSpeedButton
        Left = 8
        Top = 440
        Width = 225
        Height = 22
        Caption = 'Add to Database'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmListBox1: TmmListBox
        Left = 1
        Top = 1
        Width = 248
        Height = 424
        Align = alTop
        ItemHeight = 13
        TabOrder = 0
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
  end
  object mmToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 871
    Height = 33
    BorderWidth = 1
    ButtonHeight = 21
    ButtonWidth = 72
    Caption = 'mmToolBar1'
    EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
    EdgeOuter = esRaised
    ShowCaptions = True
    TabOrder = 1
    object tbFile: TToolButton
      Left = 0
      Top = 2
      Cursor = crHandPoint
      Action = acFile
      Grouped = True
      Style = tbsCheck
    end
    object tbRegistry: TToolButton
      Left = 72
      Top = 2
      Cursor = crHandPoint
      Action = acRegistry
      Grouped = True
      Style = tbsCheck
    end
    object tbDatabase: TToolButton
      Left = 144
      Top = 2
      Cursor = crHandPoint
      Action = acDatabase
      Grouped = True
      Style = tbsCheck
    end
    object ToolButton4: TToolButton
      Left = 216
      Top = 2
      Width = 29
      ImageIndex = 3
      Style = tbsDivider
    end
    object tbPick: TToolButton
      Left = 245
      Top = 2
      Cursor = crHandPoint
      Action = acPickValues
    end
  end
  object mmDataSource1: TmmDataSource
    DataSet = QuerryEditForm
    Left = 368
    Top = 48
  end
  object QuerryEditForm: TmmQuery
    Left = 336
    Top = 48
  end
  object mmOpenDialog1: TmmOpenDialog
    Positions = [ivdpCenter]
    Options = [ofReadOnly, ofHideReadOnly, ofAllowMultiSelect, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 400
    Top = 48
  end
  object mmActionList1: TmmActionList
    Left = 544
    Top = 49
    object acFile: TAction
      Caption = 'acFile'
      OnExecute = acFileExecute
    end
    object acRegistry: TAction
      Caption = 'acRegistry'
      Hint = 'ac'
      OnExecute = acRegistryExecute
    end
    object acDatabase: TAction
      Caption = 'acDatabase'
      OnExecute = acDatabaseExecute
    end
    object acPickValues: TAction
      Caption = 'acPickValues'
      OnExecute = acPickValuesExecute
    end
  end
end
