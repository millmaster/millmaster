program MMAnalyzer;
 // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  Forms,
  u_MMAnalyzerMain in '\\WETSRVBDE2\BDE_Dev\Delphi\Act\GUI\MMAnalyzer\u_MMAnalyzerMain.pas' {Analyer},
  u_AnalyzerEditForm in 'u_AnalyzerEditForm.pas' {AnalyzeEditForm},
  u_PrintForm in 'u_PrintForm.pas' {PrintForm};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TAnalyer, Analyer);
  Application.Run;
end.
