unit u_MSAccess_test;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, mmDBGrid, Db, DBTables, mmQuery, mmDatabase, mmDataSource,
  StdCtrls, mmButton;

type
  TForm1 = class(TForm)
    mDatabase: TmmDatabase;
    mmQuery: TmmQuery;
    mmDBGrid1: TmmDBGrid;
    mmDataSource1: TmmDataSource;
    mmButton1: TmmButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure mmButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure CreateAlias;
    procedure DeleteAlias;
  public
    { Public declarations }
  end;

const   cMMAnalyzerDB  = 'MMAnalyze.mdb';
        cDriver        = 'MSACCESS';
        cNewAlias      = 'MMAnalyzer';
        cDBName        = 'MMAnalyzeDB';




var
  Form1: TForm1;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

{ TForm1 }

procedure TForm1.CreateAlias;
var xList  : TStringList;
    xMMAnayzePath : String;
begin

  mDatabase.Connected  := FALSE;
  mmQuery.Active        := mDatabase.Connected;

  xMMAnayzePath:=ExtractFilePath(Application.ExeName) + cMMAnalyzerDB;


  with Session do begin
    if not IsAlias(cNewAlias) then begin
       xList := TStringList.Create;
       try
         with xList do begin
             Add('Database name=' + xMMAnayzePath );
             AddAlias(cNewAlias, cDriver, xList);
            // SaveConfigFile;
         end;
       finally
         xList.Free;
       end;
       SaveConfigFile;
    end else
       ShowMessage( cNewAlias + ' does exists!');
    end;

    mDatabase.AliasName    := cNewAlias;
    mDatabase.DatabaseName := cDBName;
    mmQuery.DatabaseName   := mDatabase.DatabaseName;


end;

procedure TForm1.DeleteAlias;
begin
  Session.DeleteAlias(cNewAlias);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  CreateAlias;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  DeleteAlias;
end;

procedure TForm1.mmButton1Click(Sender: TObject);
begin
 mDatabase.Connected    := TRUE;
 mmQuery.Active         := mDatabase.Connected;
end;

end.
