(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QOGUIShared.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Gemeinsame Klassen und Routinen f�r den QOfflimit (nur GUI)
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.04.2003  1.00  Lok | Datei erstellt
| 18.06.2004  1.01  SDO | Neuer Drucker-DLG in PrintQRWithSetupDialog() implementiert
| 30.05.2005        Lok | Korrektur Druckerdialog in PrintQRWithSetupDialog()
|=========================================================================================*)
unit QOGUIShared;

interface

uses
  VirtualTrees, sysutils, QOKernel, ClassDef,
  Windows, Messages, Classes, Graphics, Controls, Dialogs, QODef, LoepfeGlobal, stdctrls, mmQuickRep, 
  ActnList, mmSecurity, printers;

const
  cApplicationName    = 'MMQOfflimit';
  cDBConnectionFailed = 'Application can not start because of no connection with the database.';


type
  //1 wird gefeuert, wenn Elemente zu anderen Elementen hinzugef�gt werden sollen 
  TOnAddItems = procedure (Sender: TBaseVirtualTree) of object;
  PTreeData = ^TTreeData;

  (*: Klasse:        TTreeData
      Vorg�nger:     
      Kategorie:     record
      Kurzbeschrieb: Record f�r die TreeViews in der gesammten Anwendung 
      Beschreibung:  
                     In diesem Record werden die Objekte die an einem Knoten in einem
                     TreeView "angeh�ngt" sind verwaltet. *)
  TTreeData = record
    Item: TObject;
  end;
  
  (*: Klasse:        TQOAppSettings
      Vorg�nger:     TPersistent
      Kategorie:     Controller
      Kurzbeschrieb: Applikations Settings 
      Beschreibung:  
                     Diese Settings beeinflussen das GUI. *)
  TQOAppSettings = class(TPersistent)
  private
    FAlarmAssignMachinesAssignable: Boolean;
    FAlarmAssignMachinesPanelHeight: Integer;
    FAnalyzeRankingCount: Integer;
    FAssignMachSortCol: Integer;
    FAssignMachSortDir: TSortDirection;
    FAssignStyleSortCol: Integer;
    FAssignStyleSortDir: TSortDirection;
    FMainWindowLeftPanelWidth: Integer;
    FShiftCal: Integer;
    //1 L�dt die Benutzerspeziefischen Einstellungen 
    procedure LoadSettings;
    //1 Speichert die Benutzerspeziefischen Einstellungen 
    procedure SaveSettings;
  protected
    //1 Singleton 
    constructor CreateInstance;
    //1 Singleton 
    class function AccessInstance(Request: Integer): TQOAppSettings;
  public
    //1 Singleton 
    constructor Create;
    //1 Singleton 
    destructor Destroy; override;
    //1 Singleton 
    class function Instance: TQOAppSettings;
    //1 Singleton 
    class procedure ReleaseInstance;
    //1 True, wenn das Panel mit den Maschinen angezeigt werden soll (Assign Alarm) 
    property AlarmAssignMachinesAssignable: Boolean read FAlarmAssignMachinesAssignable write
      FAlarmAssignMachinesAssignable;
    //1 H�he des Panels das die Maschinen anzeigt (Alarm Zuordnung) 
    property AlarmAssignMachinesPanelHeight: Integer read FAlarmAssignMachinesPanelHeight write
      FAlarmAssignMachinesPanelHeight;
    //1 Anzahl der Alarme/Maschinen die im "Ranking" - Chart dargestellt werden 
    property AnalyzeRankingCount: Integer read FAnalyzeRankingCount write FAnalyzeRankingCount;
    //1 Spalte nach der das Tree mit den Maschinen sortiert wird 
    property AssignMachSortCol: Integer read FAssignMachSortCol write FAssignMachSortCol;
    //1 Richtung nach der das Tree mit den Maschinen sortiert wird 
    property AssignMachSortDir: TSortDirection read FAssignMachSortDir write FAssignMachSortDir;
    //1 Spalte nach der das Tree mit den Artikeln sortiert wird 
    property AssignStyleSortCol: Integer read FAssignStyleSortCol write FAssignStyleSortCol;
    //1 Richtung nach der das Tree mit den Artikeln sortiert wird 
    property AssignStyleSortDir: TSortDirection read FAssignStyleSortDir write FAssignStyleSortDir;
    //1 Position des vertikalen Splitters im Hauptfenster 
    property MainWindowLeftPanelWidth: Integer read FMainWindowLeftPanelWidth write FMainWindowLeftPanelWidth;
    //1 Zu verwendender Schichtkalender 
    property ShiftCal: Integer read FShiftCal write FShiftCal;
  end;
  
  (*: Klasse:        EQOLengthError
      Vorg�nger:     EQOException
      Kategorie:     No category
      Kurzbeschrieb: Fehler der geworfen wird, wenn ein GUI Element die definierte L�nge �berschreitet 
      Beschreibung:  
                     - *)
  EQOLengthError = class(EQOException)
  public
    //1 Erstellt die Fehlermeldung direkt. 
    constructor Create(aLength: integer); reintroduce; overload; virtual;
    //1 Implementierung der Abstrakten Methode 
    constructor CreateQO(aCaller: string); override;
  end;
  
  (*: Klasse:        ENoDataObject
      Vorg�nger:     EQOException
      Kategorie:     Exception
      Kurzbeschrieb: Exception, wenn keine Daten vorhanden sind 
      Beschreibung:  
                     Wird ausgel�st, wenn im Tree mit der Funktion
                     GetDataObject kein Obejkt gefunden werden kann. *)
  ENoDataObject = class(EQOException)
  public
    //1 Konstruktor 
    constructor CreateQO(aCaller: string); override;
  end;
  
  (*: Klasse:        EFactoryError
      Vorg�nger:     EQOException
      Kategorie:     Exception
      Kurzbeschrieb: Wird ausgel�st, wenn eine Factory Methode kein Objekt erzeugen kann 
      Beschreibung:  
                     - *)
  EFactoryError = class(EQOException)
  public
    //1 Konstruktor 
    constructor CreateQO(aCaller: string); override;
  end;
  


  // Gew�nschter Status eines Knotens nach dem Einf�gen im Tree
  TNodeState = (nsExpanded, nsSelected, nsFocussed);
  TNodeStateSet = Set of TNodeState;
const
  cDefaultNodeStates = [nsExpanded, nsSelected, nsFocussed];

const
  // Definiert die Farbe f�r die Kenzeichnung von "Selektierten" Datenpunkten in der Balkengrafik
  cSerieHighlightColor = $0000F2F9;
  // Farbe f�r die Alarme in der Rangliste
  cAlarmRankingColor   = clNavy;
  // Farbe f�r die Maschinen in der Rangliste
  cMachineRankingColor = clGreen;
  // Farbe f�r den l�ngsten Turn im AlarmView
  cLongestTurnColor    = clLime;
  
  

const
  // Definiert die Farbe f�r die Serien im Analyse-Chart  
  cMaxAnalyzeColor = 15;
  cAnalyzeChartColors: Array[0..cMaxAnalyzeColor - 1] of TColor = 
                       (clRed, 
                        clGreen, 
                        clBlue, 
                        clFuchsia, 
                        clNavy, 
                        clMaroon, 
                        clDkGray, 
                        clTeal, 
                        clGray, 
                        clLime, 
                        clOlive, 
                        clPurple, 
                        clSilver, 
                        clAqua, 
                        clBlack);// cAnalyzeChartColors

//  F�gt ein Objekt zu einem Tree hinzu
function AddVSTStructure(aVst: TBaseVirtualTree; aNode: PVirtualNode; aObject: TObject; const aNodeStates: 
  TNodeStateSet = cDefaultNodeStates): PVirtualNode;

// Setzt den Focus in einem TreeView auf ein bestimmtes Objekt
function SetFocusToObject(aVst: TBaseVirtualTree; aObject: TObject; aLevel: integer = -1): PVirtualNode;

// Gibt das selektierte Objekt und den selektierten Knoten zur�ck
function GetSelectedNode(aVst: TBaseVirtualTree; aLevel: integer; out aSelectedObject: TObject): PVirtualNode;

// Gibt das Objekt zu einem Knoten zur�ck
function GetNodeObject(aVst: TBaseVirtualTree; aNode: PVirtualNode): TObject;

// Selektiert und fokussiert einen Knoten der mit dem Objekt assoziiert ist
function SelectNodeFromObject(aVst: TBaseVirtualTree; aObject: TObject; aLevel: integer = -1): Boolean;

// Findet einen Knoten im Tree der auf das �bergebene Objekt zeigt
function GetNodeFromObject(aVst: TBaseVirtualTree; aObject: TObject; aRootNode: PVirtualNode): PVirtualNode;

// Selektiert den Knoten und setzt den Fokus darauf  
procedure SetFocusToNode(aVst: TBaseVirtualTree; aNode: PVirtualNode; aKeepSelection: boolean);

// Gibt den �bersetzten String f�r die entsprechende Benachrichtigungsmethode zur�ck  
function GetAdviceMethodString(aAdviceMethod: TAdviceMethod): String;

//  �bersetzt die Classdefects (sofern notwendig)
function TranslateDataItemDisplayName(aDataItem: string; aClassDefects: Integer): String; overload;

// Gibt zur�ck seit wievielen zusammenh�ngenden Schichten die angegebene
// Partie im Alarm ist. Ausgangspunkt ist die angegebene Schicht
function GetProdIDTurnCount(aShiftid: integer; aProdID: integer): Integer;

// �bersetzt die Tags (Cut, Uncut, Both) f�r die Analyse
function TranslateDataItemDisplayName(aDataItem: string): String; overload;

// Zeigt den Printersetup Dialog und druckt den Report aus, wenn auf OK gedr�ckt wurde
function PrintQRWithSetupDialog(aQR: TmmQuickRep; aWithSetup: boolean; aOrientation: TPrinterOrientation): Boolean;

// Fragt den Benutzer ob ein, oder mehrere Elemente gel�scht werden sollen
function AskForGUIDelete(aTree: TBaseVirtualTree; aSelection: TNodeArray; aParentLevels: integer = -1): Boolean; 
  overload;
// Fragt den Benutzer ob ein, oder mehrere Elemente gel�scht werden sollen
function AskForGUIDelete(aTree: TBaseVirtualTree; aSelection: TNodeArray; aPrompt: string; aParentLevels: integer = 
  -1): Boolean; overload;


// Druckt den Schichtbericht f�r die letzte abgeschlossene Schicht (Wird vom StorrageHandler �ber Com aufgerufen)
procedure PrintRecentShift;

//   Globale Funktion um die Security zu handeln
procedure EnableActionFromSecurity(aAction: TAction; aSecurity: TmmSecurityControl; aEnabled: boolean; aVisible: 
  boolean = true);

var
  // Der Index des zuletzt verwendeten Printers
  gGlobalPrinterIndex: integer = -1;
  
resourcestring
  // Text f�r das Incrementelle Suchen, wenn das Feld leer ist
  rsIncrementalSearchText = '(*)Filterkriterium eingeben';   //ivlm
  
  sAskForSave = '(*)Das Element "%s" wurde veraendert. Wollen Sie speichern?'; //ivlm
  cAskForDelete = '(*)Sollen folgende Objekte geloescht werden?'; //ivlm
  cAskForDeleteShiftFromArchiv = '(*)Sollen folgende Schichten aus dem Archiv entfernt werden?'; //ivlm
  
  // Benachrichtigungsmethoden
  rsAmMonitor = '(*)Mittels Bildschirm alarmieren';//ivlm
  rsAmPrinter = '(*)Mittels Drucker alarmieren';//ivlm
  rsAmPager   = '(*)Mittels Pager alarmieren';//ivlm
  rsAmEmail   = '(*)Mittels EMail alarmieren';//ivlm
  rsAmSMS     = '(*)Mittels SMS alarmieren';//ivlm
  
  // GUI
  rsIncludedAlarms       = '(*)Erfasste Alarme'; // ivlm
  rsAlarms               = '(*)Alarme'; // ivlm
  rsAutoGenerated        = '(*)Automatisch generiert'; // ivlm
  rsManualGenerated      = '(*)Manuell generierte'; //ivlm
  rsArchiv               = '(*)Archiv'; //ivlm
  rsNewAlarm             = '(*)Neuer Alarm'; //ivlm
  rsMachines             = '(30)Maschinen';//ivlm
  rsMachRanking          = '(*)Haeufigkeit der Maschinen';//ivlm
  rsAlarmRanking         = '(*)Haeufigkeit der Alarme';//ivlm
  rsStyle                = '(*)Artikel';//ivlm
  rsLimit                = '(*)Grenzwert';//ivlm
  rsLot                  = '(*)Partie';//ivlm
  rsNoStyleAssigned      = '(*)Kein Artikel zugewiesen';//ivlm
  rsStyleInProduction    = '(*)Der Artikel "%s" ist momentan zugewiesen';//ivlm
  rsStyleNotInProduction = '(*)Der Artikel "%s" ist momentan nicht zugewiesen';//ivlm
  rsMachInProduction     = '(*)Auf der Maschine "%s" wird momentan produziert';//ivlm
  rsMachNotInProduction  = '(*)Auf der Maschine "%s" wird momentan nicht produziert';//ivlm
  
  rsCallLabReportError = '(*)Fehler beim Aufruf des Labor Berichtes:'; //ivlm

const
  cAnalyzeRootNodeCount = 3;
  cAnalyzeRootNodeText: Array [0..cAnalyzeRootNodeCount -1] of string = (rsAutoGenerated, rsManualGenerated, rsArchiv);
    
const
// Tree View Konstanten
  // Breite der Methodenspalten
  cMethodColWith = 30;
  // Minimale Breite der ersten Spalte des SettingsTree (mSettingsTreeResize)
  cMinNameSettingsColWidth = 130;
  // Nummer der Spalte mit dem Namen des Alarms im Settings Tree
  cSettingsNameCol = 0;
  
  // Spalte in der der "Datensatzzeiger" angezeigt wird
  cAnalyzePointerCol = 1; 
  // Spalte in der der "Datensatzzeiger" angezeigt wird
  cSettingsPointerCol = 1; 
  
  // Spalte in der im AnalyzeTree der Butten f�r die Eingabe der zusatzinfos Angezeigt wirrd  
  cAnalyzeInfoCol = 2;
  
  // ImageIndex des Info Bitmaps
  cInfoBitmap = 28;

implementation
uses
  ActiveX, Forms, SettingsReader, registry, AdoDBAccess, mmcs, typinfo, ivdictio, mmDialogs, QOGUIClasses, 
  QOReportShiftList, QRPrntr, PrintSettingsTemplateForm;

const
  cQORegPath = cRegMMApplicationPath + '\' + cApplicationName;

  // ---------- Registry Values ------------
  cShiftCal                       = 'ShiftCalendar';
  
  // Main Window
  cMainWindowLeftPanelWidth       = 'MainWindowLeftPanelWidth';
  
  // Assign Frame
  cAlarmAssignMachinesAssignable  = 'AlarmAssignMachinesAssignable';
  cAlarmAssignMachinesPanelHeight = 'AlarmAssignMachinesPanelHeight';
  cAssignMachSortDir              = 'AssignMachSortDir';
  cAssignMachSortCol              = 'AssignMachSortCol';
  cAssignStyleSortDir             = 'AssignStyleSortDir';
  cAssignStyleSortCol             = 'AssignStyleSortCol';
  
  // Analyze Overview
  cAnalyzeRankingCount            = 'AnalyzeRankingCount';

(*---------------------------------------------------------
  F�gt ein Objekt zu einem Tree hinzu
----------------------------------------------------------*)
function AddVSTStructure(aVst: TBaseVirtualTree; aNode: PVirtualNode; aObject: TObject; const aNodeStates:
  TNodeStateSet = cDefaultNodeStates): PVirtualNode;
var
  Data: PTreeData;
begin
  result := nil;
  if assigned(aVst) then begin
    // Knoten hinzuf�gen
    Result := aVst.AddChild(aNode);
    
    // Jetz den Record abf�llen
    aVst.ValidateNode(Result, False);
    Data := aVst.GetNodeData(Result);
    if assigned(Data) then
      Data^.Item := aObject;
    
    // Neuen Knoten aufmachen und selektieren
    if nsExpanded in aNodeStates then
      aVst.FullExpand(aNode);
    if nsSelected in aNodeStates then
      aVst.FocusedNode := Result;
    if (nsFocussed in aNodeStates) and (aVst.Showing) then
      aVst.SetFocus;
  end;// if assigned(aVst) then begin
end;// function AddVSTStructure(aVst: TCustomVirtualStringTree; aNode: PVirtualNode; aDataItem: TObject): PVirtualNode;

(*-----------------------------------------------------------
  Setzt in einem TreeView den Focus auf den Knoten der das entsprechende Objekt beinhaltet
-------------------------------------------------------------*)
function SetFocusToObject(aVst: TBaseVirtualTree; aObject: TObject; aLevel: integer = -1): PVirtualNode;
begin
  result := nil;

  if assigned(aVst) then begin
    if assigned(aObject) then begin
      aVst.FocusedNode := nil;
  
      // Das Tree vom ersten Element an durchsuchen
      result := aVst.GetFirst;
      while (assigned(result)) and (not(assigned(aVst.FocusedNode))) do begin
        if GetNodeObject(aVst, result) = aObject then
          aVst.FocusedNode := result;
        result := aVst.GetNext(result);
      end;// while (assigned(result)) and (not(assigned(aVst.FocusedNode))) do begin
    end;// if assigned(aObject) then begin
    result := aVst.FocusedNode;
  
    if aLevel >= 0 then begin
      while integer(aVst.GetNodeLevel(result)) > aLevel do
        result := aVst.GetVisibleParent(result);
      aVst.FocusedNode := result;
    end;// if aLevel >= 0 then begin
  end;// if assigned(aVst) then begin
  
  // Wenn ein Knoten gefunden wurde, dann diesen Einblenden
  if assigned(result) then
    aVst.ScrollIntoView(result, true);

end;// function SetFocusToObject(aVst: TBaseVirtualTree; aObject: TObject; aLevel: integer = -1): PVirtualNode;

(*-----------------------------------------------------------
  Gibt den Knoten und das Objekt das gerade selektiert ist zur�ck.
  Wenn aLEvel >= 0 ist, dann wird der �bergeordnete Knoten auf dem
  entsprechenden Level zur�ckgegeben.
-------------------------------------------------------------*)
function GetSelectedNode(aVst: TBaseVirtualTree; aLevel: integer; out aSelectedObject: TObject): PVirtualNode;
begin
  aSelectedObject := nil;
  result := nil;
  
  if assigned(aVst) then begin
    // Aktuell selektierten Alarm sichern
    result := aVst.FocusedNode;
  
    if assigned(result) then begin
      // Wenn ein bestimmtes Level selektiert wird
      if aLevel >= 0 then begin
        while integer(aVst.GetNodeLevel(result)) > aLevel do
          result := aVst.GetVisibleParent(result);
      end;// if aLevel >= 0 then begin
  
      aSelectedObject := GetNodeObject(aVst, result);
    end;// if assigned(xNode) then begin
  end;// if assigned(aVst) then begin
end;// function GetSelectedNode(aVst: TBaseVirtualTree; aLevel: integer; out aSelectedObject: TObject): PVirtualNode;

(*-----------------------------------------------------------
  Findet einen Knoten im Tree der auf das �bergebene Objekt zeigt
-------------------------------------------------------------*)
function GetNodeFromObject(aVst: TBaseVirtualTree; aObject: TObject; aRootNode: PVirtualNode): PVirtualNode;
var
  xNode: PVirtualNode;
begin
  result := nil;

  if assigned(aObject) and assigned(aVst) then begin
    // Das Tree vom gew�nschten Element an durchsuchen
    if assigned(aRootNode) then
      xNode := aRootNode
    else
      xNode := aVst.GetFirst;
      
    while (assigned(xNode)) and(GetNodeObject(aVst, xNode) <> aObject) do 
      // Die Knoten m�ssen nicht initialisiert werden.
      xNode := aVst.GetNextNoInit(xNode);
      
    if (GetNodeObject(aVst, xNode) = aObject) then
      result := xNode;  
  end;// if assigned(aObject) then begin
end;// if assigned(aObject) and assigned(aVst) then begin

(*-----------------------------------------------------------
  Gibt das mit dem �bergebenen Knoten assoziierte Objekt zur�ck
-------------------------------------------------------------*)
function GetNodeObject(aVst: TBaseVirtualTree; aNode: PVirtualNode): TObject;
var
  xTreeData: PTreeData;

begin
  result := nil;
  if assigned(aVst) and assigned(aNode) then begin
    xTreeData := aVst.GetNodeData(aNode);
    if assigned(xTreeData) then
      result := xTreeData^.Item;
  end;// if assigned(aVst) and assigned(aNode) then begin
end;// function GetNodeObject(aVst: TBaseVirtualTree; aNode: PVirtualNode): TObject;

(*-----------------------------------------------------------
  Selektiert den Knoten und setzt den Fokus darauf  
-------------------------------------------------------------*)
procedure SetFocusToNode(aVst: TBaseVirtualTree; aNode: PVirtualNode; aKeepSelection: boolean);
begin
  if assigned(aVst)and assigned(aNode) then begin
    if not(aKeepSelection) then
      aVst.ClearSelection;
    aVst.Selected[aNode] := true;
    aVst.FocusedNode := aNode;
  end;// if assigned(aVst)and assigned(aNode) then begin
end;// procedure SetFocusToNode(aVst: TBaseVirtualTree; aNode: PVirtualNode);

(*-----------------------------------------------------------
  Selektiert ein Knoten in einem TreeView der mit dem entsprechenden Objekt assoziiert ist
-------------------------------------------------------------*)
function SelectNodeFromObject(aVst: TBaseVirtualTree; aObject: TObject; aLevel: integer = -1): Boolean;

  
begin
  result := false;
  if assigned(aVst)and assigned(aObject) then begin
    aVst.Selected[SetFocusToObject(aVst, aObject, aLevel)] := true;
    result := true;
  end;// if assigned(aVst)and assigned(aObject) then begin
end;// procedure SelectNodeFromObject(aVst: TBaseVirtualTree; aObject; aLevel: integer = -1);

(*-----------------------------------------------------------
  Gibt den �bersetzten String f�r die entsprechende Benachrichtigungsmethode zur�ck  
-------------------------------------------------------------*)
function GetAdviceMethodString(aAdviceMethod: TAdviceMethod): String;
begin
  result := '';
  case aAdviceMethod of
    amMonitor : result := rsAmMonitor;
    amPrinter : result := rsAmPrinter;
    amPager   : result := rsAmPager;
    amEmail   : result := rsAmEmail;
    amSMS     : result := rsAmSMS;
  else
    CodeSite.SendError('EFactoryError.CreateQO: ' + GetEnumName(TypeInfo(TAdviceMethod), ord(aAdviceMethod)));
  end;  
end;

{-----------------------------------------------------------
  �bersetzt die Classdefects (sofern notwendig).
  Ein DataItem kann z.B. 'A1 ((*)Beide)' heissen. Dann muss 
  der Teil in den Klammern �berstetzt werden.
-------------------------------------------------------------}
function TranslateDataItemDisplayName(aDataItem: string; aClassDefects: Integer): String;
begin
  result := Translate(aDataItem);
  if aClassDefects >= 0 then
    result := Format('%s (%s)', [result, Translate(GetDefectTypeText(TClassDefects(aClassDefects)))]);
end;// function TranslateDataItemDisplayName(aDataItem: string): String;

(*-----------------------------------------------------------
  �bersetzt die Tags (Cut, Uncut, Both) f�r die Analyse
-------------------------------------------------------------*)
function TranslateDataItemDisplayName(aDataItem: string): String;
var
  xBeginPos: integer;
  xLength: integer;
  xClassDefectsTagLength: integer;
  xClassDefectString: string;
  xClassDefectsTag: string;
  i: integer;
const
  cClassDefectsTag = '((*)';
  cClassDefectsTag2 = '((30)';
begin
  result := aDataItem;
  for i := 0 to 1 do begin
    if i = 0 then
      xClassDefectsTag := cClassDefectsTag
    else
      xClassDefectsTag := cClassDefectsTag2;
    
    xClassDefectsTagLength := Length(xClassDefectsTag);
    xBeginPos := AnsiPos(xClassDefectsTag, aDataItem);
    if xBeginPos > 0 then begin
      xLength := AnsiPos(')', copy(aDataItem, xBeginPos + xClassDefectsTagLength, MAXINT));
      if xLength > 0 then begin
        xLength := xLength + xClassDefectsTagLength;
        // Nur der innere Teil der Klammer ist zu �bersetzen
        xClassDefectString := copy(aDataItem, xBeginPos + 1, xLength - 2);
        result := Format(cDataItemNameFormat, [Trim(copy(aDataItem, 1, xBeginPos - 1)), Translate(xClassDefectString)])
      end;// if xEndPos > 0 then begin
    end;// if xBeginPos > 0 then begin
  end;// for i := 0 to 1 do begin
end;// function TranslateDataItemDisplayName(aDataItem: string): String;

(*-----------------------------------------------------------
  Gibt zur�ck seit wievielen zusammenh�ngenden Schichten die angegebene
  Partie im Alarm ist. Ausgangspunkt ist die angegebene Schicht
-------------------------------------------------------------*)
function GetProdIDTurnCount(aShiftid: integer; aProdID: integer): Integer;
const
  cGetProdIDTurn = 'SELECT Top 1 c_shift_count ' +
                   'FROM t_QO_Involved_Lot ' +
                   'WHERE c_prod_id = :c_prod_id ' +
                   '  AND c_shift_id = :c_shift_id';
begin
  result := 0;
  with TAdoDBAccess.Create(1) do try
    Init;
    with Query[0] do begin
      SQL.Text := cGetProdIDTurn;
      ParamByName('c_shift_id').AsInteger := aShiftid;
      ParamByName('c_prod_id').AsInteger := aProdID;
      open;
      if not(EOF) then
        result := FieldByName('c_shift_count').AsInteger;
    end;// with Query[0] do begin
  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try
end;// function GetProdIDTurnCount(aShiftid: integer; aProdID: integer): Integer;

(*-----------------------------------------------------------
  Zeigt den Printersetup Dialog und druckt den Report aus, wenn auf OK gedr�ckt wurde
-------------------------------------------------------------*)
function PrintQRWithSetupDialog(aQR: TmmQuickRep; aWithSetup: boolean; aOrientation: TPrinterOrientation): Boolean;
var
  xPrintAllowed: boolean;
begin
  result := false;
  
  if gGlobalPrinterIndex >= 0 then begin
    if Printer.Printers.Count > gGlobalPrinterIndex then 
      aQR.UsePrinterIndex(gGlobalPrinterIndex);
  end;// if gGlobalPrinterIndex >= 0 then begin
  
  // Hack da sonst der QR hin und wieder die Orientierung "verliert"
  Printer.Orientation := aOrientation;
  QRPrinter.Orientation := aOrientation;
  aQR.Page.Orientation := aOrientation;
  
  (* Initialisieren:
     - Wenn ohne Setup, dann immer drucken.
     - Soll das Setup angezeigt werden, dann h�ngt das Drucken davon ab, 
       ob OK oder Abbruch gedr�ckt wurde (siehe unten). *)
  xPrintAllowed := not(aWithSetup);

  if aWithSetup then begin
    with TfrmPrintSettings.Create(aQR.Owner) do try
      ShowClearerSettings := false;
      Printer.PrinterIndex := gGlobalPrinterIndex;
      PrinterSet.Orientation := aOrientation;
      PrinterSet.OrientationEnable := false;

      if ShowModal = mrOK then begin 
        xPrintAllowed := true;
        aQR.UsePrinterIndex(PrinterSet.PrinterIndex)
      end;// if ShowModal = mrOK then begin
    finally
      Free;
    end;// with TfrmPrintSettings.Create(aQR.Owner) do try
  end;// if aWithSetup then begin
 
  if xPrintAllowed then begin
    gGlobalPrinterIndex := aQR.PrinterSettings.PrinterIndex;
    if Printer.Printers.Count > 0 then begin
      aQR.Print;
      Result := true;
    end;// if Printer.Printers.Count > 0 then begin
  end;// if xPrintAllowed then begin
end;// function PrintQRWithSetupDialog(aQR: TmmQuickRep): Boolean;

(*-----------------------------------------------------------
  Fragt den Benutzer ob ein, oder mehrere Elemente gel�scht werden sollen
    aParentLevels bestimmt die Anzahl zus�tzlicher Parents die dem String hinzugef�gt werden sollen.
    Ist aParentLevels =  -1, dann werden alle Parent hinzugef�gt.-------------------------------------------------------------*)
function AskForGUIDelete(aTree: TBaseVirtualTree; aSelection: TNodeArray; aParentLevels: integer = -1): Boolean;
begin
  result := AskForGUIDelete(aTree, aSelection, cAskForDelete, aParentLevels);
end;// function AskForGUIDelete(aTree: TBaseVirtualTree; aSelection: TNodeArray): Boolean;

(*-----------------------------------------------------------
  Fragt den Benutzer ob ein, oder mehrere Elemente gel�scht werden sollen
    aParentLevels bestimmt die Anzahl zus�tzlicher Parents die dem String hinzugef�gt werden sollen.
    Ist aParentLevels =  -1, dann werden alle Parent hinzugef�gt.
-------------------------------------------------------------*)
function AskForGUIDelete(aTree: TBaseVirtualTree; aSelection: TNodeArray; aPrompt: string; aParentLevels: integer = -1): Boolean;
var
  xPrompt: string;
  i: integer;
  xNode: PVirtualNode;
  xParents: string;
  xCurrentLevel: integer;
  
  // Gibt die Beschriftung des Knotens zur�ck
  function GetCaption(aNode: PVirtualNode): string;
  var
    xQOInstance: TObject;
    xVisualizeInterface: IQOVisualize;
  begin
    result := '';
    xQOInstance := GetNodeObject(aTree, aNode);
    if assigned(xQOInstance) then begin
      if xQOInstance.GetInterface(IQOVisualize, xVisualizeInterface) then 
        result := xVisualizeInterface.Caption;
    end;// if assigned(xQOInstance) then begin
  end;// function GetCaption(aNode: PVirtualNode): string; 
begin
  result := false;
  
  xPrompt := '';
  for i := Low(aSelection) to High(aSelection) do begin
    // Zuerst den Namen des zu l�schenden Objektes holen
    xPrompt := xPrompt + #13#10 + GetCaption(aSelection[i]);
    
    // Nur weiter wenn mindestens ein zus�tliches Level angezeigt werden soll
    if (aParentLevels <> 0) then begin
      xParents := '';
      // ersten Parent holen
      xNode := aTree.NodeParent[aSelection[i]];
      // Feststellen wieviele zus�tliche Levels angezeigt werden sollen
      if aParentLevels = -1 then
        aParentLevels := aTree.GetNodeLevel(aSelection[i]); 
        
      xCurrentLevel := 0;
      // Solange weitermachen bis alle gew�nschten Parents abgefragt sind
      while (assigned(xNode)) and (xCurrentLevel < aParentLevels) do begin 
        xParents := ', ' + GetCaption(xNode) + xParents;
        xNode := aTree.NodeParent[xNode];
        inc(xCurrentLevel);
      end;//  while (assigned(xNode)) and (xCurrentLevel < aParentLevels) do begin 
      
      // Wenn Parents gefunden wurden, dann diesen Text zum Prompt hinzuf�gen
      if (xParents > '') then begin
        // F�hrendes Komma l�schen (mit Leerzeichen
        system.delete(xParents, 1, 2);
        xParents := ' [' + xParents + ']';
        xPrompt := xPrompt + xParents;
      end;// if (xParents > '') then begin
    end;// if (aParentLevels <> 0) then begin
  end;// for i := Low(aSelection) to High(aSelection) do begin
  
  if xPrompt > '' then begin
    xPrompt := aPrompt + #13#10 + xPrompt;  
    result := (mmMessageDlg(xPrompt, mtConfirmation, [mbYes, mbNo], 0, nil) = mrYes);
  end;// if xPrompt > '' then begin
end;// function AskForGUIDelete(aTree: TBaseVirtualTree; aSelection: TNodeArray): Boolean;

(*-----------------------------------------------------------
  Druckt den Schichtbericht f�r die letzte abgeschlossene Schicht (Wird vom StorrageHandler �ber Com aufgerufen)
-------------------------------------------------------------*)
procedure PrintRecentShift;
begin
  TQOShiftSet.Instance.LoadAlarmRootFromDB(1);
  
  with TfrmQOReportShiftList.create(nil) do try
    // Die Liste mit den Alarmen l�schen
    ClearShiftList;

    if TQOShiftSet.Instance.ShiftCount > 0 then begin
      AddShift(TQOShiftSet.Instance.Shifts[0]);
      
      // Nur Alarme drucken die als Benachrrichtigungsmethode 'Printer' definiert sind 
      IgnoreAdvice := false;
      // Schicht ausdrucken
      Print(false);
    end;// if TQOShiftSet.Instance.ShiftCount > 0 then begin
  finally
    free;
  end;// try finally
end;// procedure PrintRecentShift;

(*-----------------------------------------------------------
  Globale Funktion um die Security zu handeln
-------------------------------------------------------------*)
procedure EnableActionFromSecurity(aAction: TAction; aSecurity: TmmSecurityControl; aEnabled: boolean; aVisible: 
  boolean = true);
begin
  aAction.Enabled := aEnabled and aSecurity.CanEnabled(aAction);
  aAction.Visible := aVisible and aSecurity.CanVisible(aAction);
end;// procedure EnableActionFromSecurity(aAction: TAction ...)

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOAppSettings
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOAppSettings.Create;
begin
  inherited Create;

//: ----------------------------------------------
  raise Exception.CreateFmt('Access class %s through Instance only', [ClassName]);
end;// TQOAppSettings.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           CreateInstance
 *  Klasse:           TQOAppSettings
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOAppSettings.CreateInstance;
begin
  inherited Create;

//: ----------------------------------------------
  ShiftCal := 1;
  
  AlarmAssignMachinesAssignable  := false;
  AlarmAssignMachinesPanelHeight := 200;
  MainWindowLeftPanelWidth := 330;
  
  AnalyzeRankingCount := 4;

//: ----------------------------------------------
  LoadSettings;
end;// TQOAppSettings.CreateInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TQOAppSettings
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TQOAppSettings.Destroy;
begin
  if AccessInstance(0) = Self then AccessInstance(2);

//: ----------------------------------------------
  SaveSettings;

//: ----------------------------------------------
  inherited Destroy;
end;// TQOAppSettings.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           AccessInstance
 *  Klasse:           TQOAppSettings
 *  Kategorie:        No category 
 *  Argumente:        (Request)
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class function TQOAppSettings.AccessInstance(Request: Integer): TQOAppSettings;
  
  const FInstance: TQOAppSettings = nil;
  
begin
  case Request of
    0 : ;
    1 : if not Assigned(FInstance) then FInstance := CreateInstance;
    2 : FInstance := nil;
  else
    raise Exception.CreateFmt('Illegal request %d in AccessInstance', [Request]);
  end;
  Result := FInstance;
end;// TQOAppSettings.AccessInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           Instance
 *  Klasse:           TQOAppSettings
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class function TQOAppSettings.Instance: TQOAppSettings;
begin
  Result := AccessInstance(1);
end;// TQOAppSettings.Instance cat:No category

//:-------------------------------------------------------------------
(*: Member:           LoadSettings
 *  Klasse:           TQOAppSettings
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�dt die Benutzerspeziefischen Einstellungen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOAppSettings.LoadSettings;
begin
  with TRegistry.Create do try
    if OpenKey(cQORegPath, false) then begin

//: ----------------------------------------------
  // -------- Allgemein --------
      if ValueExists(cShiftCal) then
        FShiftCal := ReadInteger(cShiftCal);

//: ----------------------------------------------
  // -------- MainWindow --------
      if ValueExists(cMainWindowLeftPanelWidth) then
        FMainWindowLeftPanelWidth := ReadInteger(cMainWindowLeftPanelWidth);

//: ----------------------------------------------
  // -------- Assign Frame--------
      // Maschinen Ansicht
      if ValueExists(cAlarmAssignMachinesAssignable) then
        FAlarmAssignMachinesAssignable := ReadBool(cAlarmAssignMachinesAssignable);
      if ValueExists(cAlarmAssignMachinesPanelHeight) then
        FAlarmAssignMachinesPanelHeight := ReadInteger(cAlarmAssignMachinesPanelHeight);
  
      // Sortierung
      if ValueExists(cAssignMachSortDir) then
        FAssignMachSortDir := TSortDirection(ReadInteger(cAssignMachSortDir));
      if ValueExists(cAssignMachSortCol) then
        FAssignMachSortCol := ReadInteger(cAssignMachSortCol);
      if ValueExists(cAssignStyleSortDir) then
        FAssignStyleSortDir := TSortDirection(ReadInteger(cAssignStyleSortDir));
      if ValueExists(cAssignStyleSortCol) then
        FAssignStyleSortCol := ReadInteger(cAssignStyleSortCol);

//: ----------------------------------------------
  // ---------------- Analyze Overview Frame
      // Anzahl Alarme/Maschinen im Ranking
      if ValueExists(cAnalyzeRankingCount) then
        FAnalyzeRankingCount := ReadInteger(cAnalyzeRankingCount);

//: ----------------------------------------------
    end;// if OpenKey(cQORegPath, false) then begin
  finally
    Free;
  end;// with TRegistry.Create do try
end;// TQOAppSettings.LoadSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           ReleaseInstance
 *  Klasse:           TQOAppSettings
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class procedure TQOAppSettings.ReleaseInstance;
begin
  AccessInstance(0).Free;
end;// TQOAppSettings.ReleaseInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveSettings
 *  Klasse:           TQOAppSettings
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Speichert die Benutzerspeziefischen Einstellungen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOAppSettings.SaveSettings;
begin
  with TRegistry.Create do try
    if OpenKey(cQORegPath, true) then begin
    // -------- MainWindow --------
      WriteInteger(cMainWindowLeftPanelWidth, FMainWindowLeftPanelWidth) ;
  
    // -------- Assign Frame --------
      // Maschinen Ansicht
      WriteBool(cAlarmAssignMachinesAssignable, FAlarmAssignMachinesAssignable);
      WriteInteger(cAlarmAssignMachinesPanelHeight, FAlarmAssignMachinesPanelHeight) ;
  
      // Sortierung
      WriteInteger(cAssignMachSortDir, ord(AssignMachSortDir));
      WriteInteger(cAssignMachSortCol, AssignMachSortCol) ;
      WriteInteger(cAssignStyleSortDir, ord(AssignStyleSortDir)) ;
      WriteInteger(cAssignStyleSortCol, AssignStyleSortCol) ;
    end;// if OpenKey(cQORegPath, true) then begin
  finally
    Free;
  end;// with TRegistry.Create do try
end;// TQOAppSettings.SaveSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           EQOLengthError
 *  Kategorie:        No category 
 *  Argumente:        (aLength)
 *
 *  Kurzbeschreibung: Erstellt die Fehlermeldung direkt.
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor EQOLengthError.Create(aLength: integer);
begin
  create(Format(rsTextLength, [aLength]));
end;// EQOLengthError.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           CreateQO
 *  Klasse:           EQOLengthError
 *  Kategorie:        No category 
 *  Argumente:        (aCaller)
 *
 *  Kurzbeschreibung: Implementierung der Abstrakten Methode
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor EQOLengthError.CreateQO(aCaller: string);
begin
  if aCaller > '' then
    create(aCaller + ':' + rsLengthError)
  else
    create(rsLengthError);
end;// EQOLengthError.CreateQO cat:No category

//:-------------------------------------------------------------------
(*: Member:           CreateQO
 *  Klasse:           ENoDataObject
 *  Kategorie:        No category 
 *  Argumente:        (aCaller)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor ENoDataObject.CreateQO(aCaller: string);
begin
  if aCaller > '' then
    create(aCaller)
  else
    create('');
end;// ENoDataObject.CreateQO cat:No category

//:-------------------------------------------------------------------
(*: Member:           CreateQO
 *  Klasse:           EFactoryError
 *  Kategorie:        No category 
 *  Argumente:        (aCaller)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor EFactoryError.CreateQO(aCaller: string);
begin
  if aCaller > '' then
    create(aCaller + ':' + sEFactoryError)
  else
    create(sEFactoryError);
end;// EFactoryError.CreateQO cat:No category


initialization
finalization
  // Singletons wieder freigeben
  TQOAppSettings.ReleaseInstance;
  TMMSettingsReader.ReleaseInstance;
  
end.
