object frmAnalyzeChartFrame: TfrmAnalyzeChartFrame
  Left = 0
  Top = 0
  Width = 454
  Height = 364
  TabOrder = 0
  object mDataItemTabs: TmmTabControl
    Left = 0
    Top = 0
    Width = 454
    Height = 364
    Align = alClient
    TabOrder = 0
    Tabs.Strings = (
      ' ')
    TabIndex = 0
    OnChange = mDataItemTabsChange
    object mmSplitter1: TmmSplitter
      Left = 4
      Top = 174
      Width = 446
      Height = 6
      Cursor = crVSplit
      Align = alBottom
    end
    object mDataItemChart: TmmChart
      Left = 4
      Top = 24
      Width = 446
      Height = 150
      BackWall.Brush.Color = clWhite
      BackWall.Brush.Style = bsClear
      Title.Font.Charset = ANSI_CHARSET
      Title.Font.Color = clBlack
      Title.Font.Height = -13
      Title.Font.Name = 'Arial'
      Title.Font.Style = [fsBold]
      Title.Text.Strings = (
        'TmmChart')
      OnClickLegend = mDataItemChartClickLegend
      OnClickSeries = mDataItemChartClickSeries
      LeftAxis.Automatic = False
      LeftAxis.AutomaticMaximum = False
      LeftAxis.AutomaticMinimum = False
      LeftAxis.AxisValuesFormat = '#,##0.####'
      Legend.Alignment = laTop
      View3D = False
      Align = alClient
      BevelOuter = bvNone
      Color = clWhite
      TabOrder = 0
      MouseMode = mmNormal
    end
    object mBottomPanel: TmmPanel
      Left = 4
      Top = 180
      Width = 446
      Height = 180
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object mmLabel1: TmmLabel
        Left = 0
        Top = 0
        Width = 446
        Height = 13
        Align = alTop
        Caption = '(*)Einstellungen'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      inline mMethodContainer: TfrmMethodContainerFrame
        Top = 13
        Width = 446
        Height = 167
        inherited mMethods: TmmScrollBox
          Width = 446
          Height = 167
        end
        inherited mmTranslator1: TmmTranslator
          TargetsData = (
            1
            1
            (
              '*'
              'Caption'
              0))
        end
      end
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 340
    Top = 40
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Tabs'
        0)
      (
        '*'
        'Caption'
        0))
  end
  object slParameters: TmmVCLStringList
    Strings.Strings = (
      '[Basic]'
      'XAxis=time'
      'ZAxis=prod_name')
    Left = 24
    Top = 16
  end
end
