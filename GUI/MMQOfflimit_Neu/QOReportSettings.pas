(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QOReportSettings.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Bericht f�r die Alarm Settings
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.04.2003  1.00  Lok | Datei erstellt
| 18.06.2004  1.00  SDo | Layout Aenderung & Zuweisung in FormCreate
|=========================================================================================*)
unit QOReportSettings;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, BaseForm, Dialogs,
  QuickRpt, Qrctrls, mmQRLabel, ExtCtrls, mmQuickRep, IvDictio, IvMulti,
  IvEMulti, mmTranslator, IvAMulti, IvBinDic, mmDictionary, mmQRImage,
  mmQRSysData, mmQRShape, QOKernel, mmQRSubDetail, mmQRDBText, ImgList,
  mmImageList, mmQRMemo;

type
  (*: Klasse:        TfrmQOReportSettings
      Vorg�nger:     TmmForm
      Kategorie:     No category
      Kurzbeschrieb: Druckt einen Alarm aus 
      Beschreibung:  
                     - *)
  TfrmQOReportSettings = class (TmmForm)
    la: TmmQRLabel;
    laAdviceMethods: TmmQRLabel;
    laAlarmName: TmmQRLabel;
    laBoolOpAndAlarm: TmmQRLabel;
    laBoolOpAndDI: TmmQRLabel;
    laDataItemName: TmmQRLabel;
    laLimitValueAVG: TmmQRLabel;
    laLimitValueAVGUnit: TmmQRLabel;
    laLimitValueBound: TmmQRLabel;
    laLimitValueBountUnit: TmmQRLabel;
    laMethodNameAVG: TmmQRLabel;
    laMethodNameBound: TmmQRLabel;
    laMinProdYarnLen: TmmQRLabel;
    laMinProdYarnLenValue: TmmQRLabel;
    laShiftAVGCount: TmmQRLabel;
    laShiftCountAVG: TmmQRLabel;
    laShiftCountBound: TmmQRLabel;
    mAlarmBand: TQRBand;
    mAVGBand: TmmQRSubDetail;
    mDataItemBand: TmmQRSubDetail;
    memStyleAlarm: TmmQRMemo;
    mFixBoundBand: TmmQRSubDetail;
    mMethodImages: TmmImageList;
    mmQRLabel10: TmmQRLabel;
    mmQRLabel11: TmmQRLabel;
    mmQRLabel4: TmmQRLabel;
    mmQRLabel5: TmmQRLabel;
    mmQRLabel6: TmmQRLabel;
    mmQRLabel8: TmmQRLabel;
    mmQRLabel9: TmmQRLabel;
    mmQRShape1: TmmQRShape;
    mmQRShape2: TmmQRShape;
    mmQRSysData1: TmmQRSysData;
    mmQRSysData3: TmmQRSysData;
    mmTranslator1: TmmTranslator;
    mQRSettings: TmmQuickRep;
    PageHeaderBand1: TQRBand;
    qlDate: TmmQRLabel;
    qlTime: TmmQRLabel;
    qlTitle: TmmQRLabel;
    qsTitleLine1: TmmQRShape;
    qsTitleLine2: TmmQRShape;
    PageFooterBand1: TQRBand;
    mmQRImage1: TmmQRImage;
    qlPageValue: TmmQRSysData;
    mmQRSysData2: TmmQRSysData;
    qlCompanyName: TmmQRLabel;
    //1 Initialisieren 
    procedure FormCreate(Sender: TObject);
    //1 Mittelwert Methode ausdrucken 
    procedure mAVGBandNeedData(Sender: TObject; var MoreData: Boolean);
    //1 Daten aufbereiten (DataItems) 
    procedure mDataItemBandNeedData(Sender: TObject; var MoreData: Boolean);
    //1 Fix Bound Methode asudrucken 
    procedure mFixBoundBandNeedData(Sender: TObject; var MoreData: Boolean);
    //1 Daten aufbereiten (Alarm) 
    procedure mQRSettingsNeedData(Sender: TObject; var MoreData: Boolean);
  private
    mAlarmIndex: Integer;
    mAlarmList: TList;
    mAVGPrinted: Boolean;
    mBoundPrinted: Boolean;
    mDataItemIndex: Integer;
    //1 Anzahl Alarme die ausgedruckt werden sollen 
    function GetAlarmCount: Integer;
    //1 Ausdruck eines Alarms 
    function PrintAlarm(aQOAlarm: TQOAlarm): Boolean;
  public
    //1 Destruktor 
    destructor Destroy; override;
    //1 Fugt einen Alarm der ausgedruckt werden soll zur Liste hinzu 
    procedure AddAlarm(aAlarm:TQOAlarm);
    //1 L�scht die Liste mit den Alarmen die zum Ausdruck vorgesehen sind 
    procedure ClearAlarmList;
    //1 Druckt die Alarme aus die zuvor der Liste hinzugef�gt wurden (siehe AddAlarm()) 
    procedure Print(aWithSetup: boolean);
    //1 Anzahl Alarme die ausgedruckt werden sollen 
    property AlarmCount: Integer read GetAlarmCount;
  end;
  
var
  frmQOSettingsReport: TfrmQOReportSettings;

implementation

uses
  QOGUIShared, QOMethods, QODef, SettingsReader, MMUGlobal, CalcMethods, printers, QRPrntr;

resourcestring
  rsActive             = '(*)Aktiv';//ivlm
  rsInactive           = '(*)Inaktiv';//ivlm
  rsAlarmBoolOpAnd_And = '(*)Alle Datenelemente muessen im Alarm sein';//ivlm
  rsAlarmBoolOpAnd_Or  = '(*)Mindestens ein Datenelement muss im Alarm sein';//ivlm
  rsNoMethod           = '(*)keine';//ivlm
  rsDIBoolOpAnd_And    = '(*)Alle Bedingungen muessen zutreffen (+)';//ivlm
  rsDIBoolOpAnd_Or     = '(*)Mindestens eine Bedingung muss zutreffen (o)';//ivlm
  rsAllMachines        = '(*)Alle Maschinen';//ivlm

{$R *.DFM}

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmQOReportSettings
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmQOReportSettings.Destroy;
begin
  FreeAndNil(mAlarmList);

//: ----------------------------------------------
  inherited Destroy;
end;// TfrmQOReportSettings.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddAlarm
 *  Klasse:           TfrmQOReportSettings
 *  Kategorie:        No category 
 *  Argumente:        (aAlarm)
 *
 *  Kurzbeschreibung: Fugt einen Alarm der ausgedruckt werden soll zur Liste hinzu
 *  Beschreibung:     
                      Ist der Parameter 'aNew' = true, dann wird vor dem Hinzuf�gen die 
                      Liste gel�scht.
 --------------------------------------------------------------------*)
procedure TfrmQOReportSettings.AddAlarm(aAlarm:TQOAlarm);
begin
  // Alarm zur Liste hinzuf�gen
  if assigned(aAlarm) then
    mAlarmList.Add(aAlarm);
end;// TfrmQOReportSettings.AddAlarm cat:No category

//:-------------------------------------------------------------------
(*: Member:           ClearAlarmList
 *  Klasse:           TfrmQOReportSettings
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�scht die Liste mit den Alarmen die zum Ausdruck vorgesehen sind
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOReportSettings.ClearAlarmList;
begin
  mAlarmList.Clear;
end;// TfrmQOReportSettings.ClearAlarmList cat:No category

//:-------------------------------------------------------------------
(*: Member:           FormCreate
 *  Klasse:           TfrmQOReportSettings
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Initialisieren
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOReportSettings.FormCreate(Sender: TObject);
begin
  mAlarmIndex := -1;
  mAlarmList := TList.Create;

  try
    qlCompanyName.Caption := TMMSettingsReader.Instance.Value[cCompanyName];
  except
    qlCompanyName.Caption :='';
  end;

  qlPageValue.Text := Translate( qlPageValue.Text ) + ' ';
end;// TfrmQOReportSettings.FormCreate cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAlarmCount
 *  Klasse:           TfrmQOReportSettings
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Anzahl Alarme die ausgedruckt werden sollen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmQOReportSettings.GetAlarmCount: Integer;
begin
  result := 0;
  if assigned(mAlarmList) then
    result := mAlarmList.count;
end;// TfrmQOReportSettings.GetAlarmCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           mAVGBandNeedData
 *  Klasse:           TfrmQOReportSettings
 *  Kategorie:        No category 
 *  Argumente:        (Sender, MoreData)
 *
 *  Kurzbeschreibung: Mittelwert Methode ausdrucken
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOReportSettings.mAVGBandNeedData(Sender: TObject; var MoreData: Boolean);
var
  xQOAlarm: TQOAlarm;
  xQOItem: TQOItem;
  xQOAVG: TQOAvg;
  xUnit: String;
  xConversionFactor: Extended;
  xId: Integer;
begin
  MoreData := not(mAVGPrinted);
  mAVGPrinted := true;
  if MoreData then begin
    if  (mAlarmIndex < mAlarmList.Count) then begin
      xQOALarm := TQOAlarm(mAlarmList[mAlarmIndex]);
      if (mDataItemIndex < xQOAlarm.QOItems.ItemCount) then begin
        xQOItem := xQOAlarm.QOItems[mDataItemIndex];
        if (xQOItem.QOMethods.MethodByType(cmAVG) <> nil) then begin
          xQOAVG := TQOAVG(xQOItem.QOMethods.MethodByType(cmAvg));
          if xQOAVG.Active then begin
            with TMMSettingsReader.Instance do begin
              // Initialisieren und Error wenn fehlgeschlagen
              if not(init) then
                raise ESettingsreaderInit.CreateQO('TfrmMethodAVGFrame.Create');
              xUnit:= 'm';
  
              //Einheit ermitteln und Umrechungsfaktor setzen
              xId := Value[cStatisticLengthUnit];
              case xId of
                0: begin
                     xUnit:= 'km';
                     xConversionFactor := 1000;
                   end;// 0:
                1: begin
                     xUnit:= 'kyds';
                     xConversionFactor := cYdToM * 1000;
                   end;// 1:
                else
                  raise Exception.CreateFMT('TfrmQOReportSettings.mAVGBandNeedData: %s %s (id = %d)!',[sQOLengthUnit, sNotValid, xId]);
              end;// case xId of
            end;// with TMMSettingsReader.Instance do begin
  
            laMethodNameAVG.Caption := Translate(cCalculationMethods[cmAVG].Name);
            laShiftCountAVG.caption := IntToStr(xQOAVG.ShiftCount);
            laLimitValueAVG.Caption := FloatToStr(xQOAVG.Tolerance);
            laLimitValueAVGUnit.Left := laLimitValueAVG.Left + laLimitValueAVG.Width + 3;
            laShiftAVGCount.Caption := IntToStr(xQOAVG.ShiftCountAVG);
            laMinProdYarnLen.Caption := Format(sProdYarnDescr,[xUnit]);
            laMinProdYarnLenValue.Caption := FloatToStr(xQOAVG.MinProdYarnLength / xConversionFactor);
  
            laMinProdYarnLenValue.Left := laMinProdYarnLen.Left + laMinProdYarnLen.Width + 3;
          end else begin
            MoreData := false;
          end;// if xQOAVG.Active then begin
        end;// if (xQOItem.QOMethods.MethodByType(cmAVG) <> nil) then begin
      end;// if (mDataItemIndex < xQOAlarm.QOItems.ItemCount) then begin
    end;// if  (mAlarmIndex < mAlarmList.Count) then begin
  end;// if MoreData then begin
end;// TfrmQOReportSettings.mAVGBandNeedData cat:No category

//:-------------------------------------------------------------------
(*: Member:           mDataItemBandNeedData
 *  Klasse:           TfrmQOReportSettings
 *  Kategorie:        No category 
 *  Argumente:        (Sender, MoreData)
 *
 *  Kurzbeschreibung: Daten aufbereiten (DataItems)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOReportSettings.mDataItemBandNeedData(Sender: TObject; var MoreData: Boolean);
var
  xQOAlarm: TQOAlarm;
  xQOItem: TQOItem;
  xFound: Boolean;
begin
  xFound := false;
  MoreData := false;
  mBoundPrinted := false;
  mAVGPrinted := false;
  if mAlarmIndex < mAlarmList.Count then begin
   xQOALarm := TQOAlarm(mAlarmList[mAlarmIndex]);
   while not(xFound) do begin
      inc(mDataItemIndex);
      MoreData := (mDataItemIndex < xQOAlarm.QOItems.ItemCount);
      if MoreData then begin
        xQOItem := xQOAlarm.QOItems[mDataItemIndex];
        if xQOItem.Active then begin
          xFound := true;
          laDataItemName.Caption := xQOItem.QOVisualize.Caption;
  
  
  // ------------- BoolOpAnd  -------------
          laBoolOpAndDI.Caption := '';
          if  xQOItem.QOMethods.ActiveMethodCount > 1 then begin
            if xQOItem.BoolOpAnd then
              laBoolOpAndDI.Caption :=  StringReplace(rsDIBoolOpAnd_And, '(+)', '', [])
            else
              laBoolOpAndDI.Caption :=  StringReplace(rsDIBoolOpAnd_Or, '(o)', '', []);
           end;// if  xQOItem.Methods.ActiveMethodCount > 1 then begin
        end;// if xQOItem.Active then begin
      end else begin
         xFound := true;
      end;// if MoreData then begin
    end;//
  end;// if mAlarmIndex < mAlarmList.Count then begin
end;// TfrmQOReportSettings.mDataItemBandNeedData cat:No category

//:-------------------------------------------------------------------
(*: Member:           mFixBoundBandNeedData
 *  Klasse:           TfrmQOReportSettings
 *  Kategorie:        No category 
 *  Argumente:        (Sender, MoreData)
 *
 *  Kurzbeschreibung: Fix Bound Methode asudrucken
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOReportSettings.mFixBoundBandNeedData(Sender: TObject; var MoreData: Boolean);
var
  xQOAlarm: TQOAlarm;
  xQOItem: TQOItem;
  xQOBound: TQOFixBound;
begin
  MoreData := not(mBoundPrinted);
  mBoundPrinted := true;
  if MoreData then begin
    if mAlarmIndex < mAlarmList.Count then begin
     xQOALarm := TQOAlarm(mAlarmList[mAlarmIndex]);
      if (mDataItemIndex < xQOAlarm.QOItems.ItemCount) then begin
        xQOItem := xQOAlarm.QOItems[mDataItemIndex];
        if (xQOItem.QOMethods.MethodByType(cmFix) <> nil) then begin
          xQOBound := TQOFixBound(xQOItem.QOMethods.MethodByType(cmFix));
          if xQOBound.Active then begin
            laMethodNameBound.Caption := Translate(cCalculationMethods[cmFix].Name);
            laShiftCountBound.caption := IntToStr(xQOBound.ShiftCount);
            laLimitValueBound.Caption := FloatToStr(xQOBound.LimitValue);
            laLimitValueBountUnit.Caption :=  GetLenBaseString(xQOBound.LenBase);
            laLimitValueBountUnit.Left := laLimitValueBound.Left + laLimitValueBound.Width + 3;
          end else begin
            MoreData := false;
          end;// if xQOBound.Active then begin
        end;// if (xQOItem.QOMethods.MethodByType(cmFix) <> nil) then begin
      end;// if (mDataItemIndex < xQOAlarm.QOItems.ItemCount) then begin
    end;// if mAlarmIndex < mAlarmList.Count then begin
  end;// if MoreData then begin
end;// TfrmQOReportSettings.mFixBoundBandNeedData cat:No category

//:-------------------------------------------------------------------
(*: Member:           mQRSettingsNeedData
 *  Klasse:           TfrmQOReportSettings
 *  Kategorie:        No category 
 *  Argumente:        (Sender, MoreData)
 *
 *  Kurzbeschreibung: Daten aufbereiten (Alarm)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOReportSettings.mQRSettingsNeedData(Sender: TObject; var MoreData: Boolean);
var
  xQOAlarm: TQOAlarm;
begin
  mDataItemIndex := -1;
  inc(mAlarmIndex);
  MoreData := (mAlarmIndex < mAlarmList.Count);
  if MoreData then begin
    xQOALarm := TQOAlarm(mAlarmList[mAlarmIndex]);
    PrintAlarm(xQOAlarm)
  end;// if MoreDate then begin
end;// TfrmQOReportSettings.mQRSettingsNeedData cat:No category

//:-------------------------------------------------------------------
(*: Member:           Print
 *  Klasse:           TfrmQOReportSettings
 *  Kategorie:        No category 
 *  Argumente:        (aWithSetup)
 *
 *  Kurzbeschreibung: Druckt die Alarme aus die zuvor der Liste hinzugef�gt wurden (siehe AddAlarm())
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOReportSettings.Print(aWithSetup: boolean);
begin
  (*Printer.Orientation := poPortrait;
  QRPrinter.Orientation := poPortrait;
  mQRSettings.Page.Orientation := poPortrait;*)
  
  PrintQRWithSetupDialog(mQRSettings, aWithSetup, poPortrait);
end;// TfrmQOReportSettings.Print cat:No category

//:-------------------------------------------------------------------
(*: Member:           PrintAlarm
 *  Klasse:           TfrmQOReportSettings
 *  Kategorie:        No category 
 *  Argumente:        (aQOAlarm)
 *
 *  Kurzbeschreibung: Ausdruck eines Alarms
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmQOReportSettings.PrintAlarm(aQOAlarm: TQOAlarm): Boolean;
var
  i: TAdviceMethod;
  j: Integer;
  k: Integer;
  xAdviceMethodString: String;
  xAssignString: String;
  xAssigns: TQOAssigns;
begin
  result := assigned(aQOAlarm);
  if result then begin
  // ------------- Name -------------
    laAlarmName.Caption := aQOAlarm.QOVisualize.Caption;
  
  // ------------- Aktiv -------------
    if aQOAlarm.Active then
      laAlarmName.Caption := laAlarmName.Caption + Format(' (%s)', [Translate(rsActive)])
    else
      laAlarmName.Caption := laAlarmName.Caption + Format(' (%s)', [Translate(rsInactive)]);
  
  // ------------- Verkn�pfung -------------
    laBoolOpAndAlarm.Caption := '';
    if aQOAlarm.QOItems.ActiveDataItemCount > 1 then begin
      if aQOAlarm.BoolOpAnd then
        laBoolOpAndAlarm.Caption := rsAlarmBoolOpAnd_And
      else
        laBoolOpAndAlarm.Caption := rsAlarmBoolOpAnd_Or;
    end;// if aQOAlarm.QOItems.ActiveDataItemCount > 1 then begin
  // ------------- Benachrichtigung  -------------
    xAdviceMethodString := '';
    for i := Low(TAdviceMethod) to High(TAdviceMethod) do begin
      if (i in aQOAlarm.AdviceMethods) then
        xAdviceMethodString := xAdviceMethodString + ', ' + GetAdviceMethodString(i);
    end;// for i := Low(TAdviceMethods) to High(TAdviceMethods) do begin
  
      // F�rendes Komma l�schen, oder signalisieren, dass keine Benachrichtigungsmethoden def. sind
    if xAdviceMethodString > '' then
      system.delete(xAdviceMethodString, 1, 2)
    else
      xAdviceMethodString := rsNoMethod;
    laAdviceMethods.Caption := xAdviceMethodString;
  
    xAssignString := '';
    memStyleAlarm.Lines.Clear;
    xAssigns := aQOAlarm.QOAssigns;
    if assigned(xAssigns) then begin
      for j := 0 to xAssigns.AssignCount - 1 do begin
        xAssignString := xAssigns[j].QOVisualize.Caption + ' (';
        if xAssigns[j].QOAssigns.AssignCount > 0 then begin
          for k := 0 to xAssigns[j].QOAssigns.AssignCount - 1 do begin
            if k > 0 then
              xAssignString := xAssignString + ', ';
            xAssignString := xAssignString + xAssigns[j].QOAssigns[k].QOVisualize.Caption;
          end;// for k := 0 to xAssigns[j].QOAssigns.AssignCount - 1 do begin
        end else begin
          xAssignString := xAssignString + rsAllMachines;
        end;// if xAssigns[j].QOAssigns.AssignCount > 0 then begin
        xAssignString := xAssignString + ')';
        memStyleAlarm.Lines.Add(xAssignString);
        xAssignString := '';
      end;// for j := 0 to xAssigns.AssignCount - 1 do begin
    end;// if assigned(xAssigns) then begin
    if memStyleAlarm.Lines.Count = 0 then
      memStyleAlarm.Lines.Text := rsNoMethod;
  end;// if result then begin
end;// TfrmQOReportSettings.PrintAlarm cat:No category

end.




