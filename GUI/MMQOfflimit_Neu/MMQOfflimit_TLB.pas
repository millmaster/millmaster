unit MMQOfflimit_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88.1.0.1.0  $
// File generated on 18.12.2003 10:11:50 from Type Library described below.

// ************************************************************************ //
// Type Lib: W:\MillMaster\GUI\MMQOfflimit_Neu\MMQOfflimit.tlb (1)
// IID\LCID: {63AE5B2A-D32B-4539-9507-EBA80E1D93B6}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\System32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  MMQOfflimitMajorVersion = 1;
  MMQOfflimitMinorVersion = 0;

  LIBID_MMQOfflimit: TGUID = '{63AE5B2A-D32B-4539-9507-EBA80E1D93B6}';

  IID_IMMQOfflimitServer: TGUID = '{49992D64-FAC5-4D70-8D61-927D113C973B}';
  CLASS_MMQOfflimitServer: TGUID = '{79D2FBB1-8620-4E85-A509-D0B8386B6A68}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IMMQOfflimitServer = interface;
  IMMQOfflimitServerDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  MMQOfflimitServer = IMMQOfflimitServer;


// *********************************************************************//
// Interface: IMMQOfflimitServer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {49992D64-FAC5-4D70-8D61-927D113C973B}
// *********************************************************************//
  IMMQOfflimitServer = interface(IDispatch)
    ['{49992D64-FAC5-4D70-8D61-927D113C973B}']
    procedure Execute(const aParameter: WideString); safecall;
    procedure AnalyzeRecentShift(aSave: WordBool; out aAlarmCount: OleVariant); safecall;
  end;

// *********************************************************************//
// DispIntf:  IMMQOfflimitServerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {49992D64-FAC5-4D70-8D61-927D113C973B}
// *********************************************************************//
  IMMQOfflimitServerDisp = dispinterface
    ['{49992D64-FAC5-4D70-8D61-927D113C973B}']
    procedure Execute(const aParameter: WideString); dispid 1;
    procedure AnalyzeRecentShift(aSave: WordBool; out aAlarmCount: OleVariant); dispid 2;
  end;

// *********************************************************************//
// The Class CoMMQOfflimitServer provides a Create and CreateRemote method to          
// create instances of the default interface IMMQOfflimitServer exposed by              
// the CoClass MMQOfflimitServer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoMMQOfflimitServer = class
    class function Create: IMMQOfflimitServer;
    class function CreateRemote(const MachineName: string): IMMQOfflimitServer;
  end;

implementation

uses ComObj;

class function CoMMQOfflimitServer.Create: IMMQOfflimitServer;
begin
  Result := CreateComObject(CLASS_MMQOfflimitServer) as IMMQOfflimitServer;
end;

class function CoMMQOfflimitServer.CreateRemote(const MachineName: string): IMMQOfflimitServer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_MMQOfflimitServer) as IMMQOfflimitServer;
end;

end.
