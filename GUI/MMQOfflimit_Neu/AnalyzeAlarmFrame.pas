(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: AnalyzeAlarmFrame.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Frame f�r die �bersicht eines bestimmten Alarms (Auswertung)
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 26.08.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit AnalyzeAlarmFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeEngine, Series, ExtCtrls, TeeProcs, Chart, DBChart, mmDBChart, Db,
  ADODB, mmADODataSet, QOGUIClasses, mmDataSource, mmChart, StdCtrls,
  mmLabel, mmShape, QOChartSeries, IvDictio, IvMulti, IvEMulti,
  mmTranslator, mmImage, mmPanel;

type
  //1 wird ausgel�st, wenn im Alarm Histogramm auf eine Schicht geklickt wird
  //1 wird ausgel�st, wenn im Alarm Histogramm auf eine Schicht geklickt wird 
  TAlarmViewChartClickEvent = procedure (aDate: TDateTime; aAlarmName: string; aButton: TMouseButton; aShiftKey:
    TShiftState) of object;
  (*: Klasse:        TAlarmDataRec
      Vorg�nger:     
      Kategorie:     record
      Kurzbeschrieb: Record der Informationen zu einem Datenpunkt aufnehmen kann 
      Beschreibung:  
                     Dieser Record nimmt die Daten eines einzelnen Datenpunktes im Histogramm auf.
                     Diese Daten werden ben�tigt, um die "Ranking"- Serien anzuzeigen. *)
  TAlarmDataRec = record
    AlarmDataID: Integer;
    Value: Integer;
    Date: TDateTime;
    ShiftID: Integer;
    ShiftStart: TDateTime;
  end;
  
  PAlarmDataRec = ^TAlarmDataRec;

  (*: Klasse:        TfrmAnalyzeAlarmFrame
      Vorg�nger:     TFrame
      Kategorie:     No category
      Kurzbeschrieb: Frame der statistische Daten f�r den �berblick �ber den selektierten Alarm bereitstellt 
      Beschreibung:  
                     - *)
  TfrmAnalyzeAlarmFrame = class(TFrame)
    laAlarmTurn: TmmLabel;
    laAlarmTurnDate: TmmLabel;
    laLongestTurn: TmmLabel;
    laLongestTurnDate: TmmLabel;
    laTotalCount: TmmLabel;
    mHistogramChart: TmmChart;
    mInfoImage: TmmImage;
    mLegendLongestTurn: TmmShape;
    mmPanel1: TmmPanel;
    mmTranslator1: TmmTranslator;
    mRankingMachineChart: TmmChart;
    paExtendedInfo: TmmPanel;
    paInfo: TmmPanel;
    pSpacePanel: TmmPanel;
    //1 Wird aufgerufen, wenn die Maus �ber das Chart bewegt wird 
    procedure mChartMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    //1 Wird aufgerufen, wenn im Chart auf eine serie geklickt wird 
    procedure mHistogramChartClickSeries(Sender: TCustomChart; Series: TChartSeries; ValueIndex: Integer; Button:
      TMouseButton; Shift: TShiftState; X, Y: Integer);
    //1 �bernimmt alle �bersetungen die nicht automatisch "passieren" 
    procedure mmTranslator1AfterTranslate(translator: TIvCustomTranslator);
    //1 L�scht das Highlighten der selektierten Maschine 
    procedure mRankingMachineChartClickBackground(Sender: TCustomChart; Button: TMouseButton; Shift: TShiftState; X, Y:
      Integer);
    //1 Selektiert eine Maschine 
    procedure mRankingMachineChartClickSeries(Sender: TCustomChart; Series: TChartSeries; ValueIndex: Integer; Button:
      TMouseButton; Shift: TShiftState; X, Y: Integer);
  private
    FOnAlarmChartClick: TAlarmViewChartClickEvent;
    mAlarm: TQOAlarmView;
    mAlarmViewSeries: TSeriesList;
    //1 Baut die zu Filternde Serie auf 
    procedure SetHighlightSerie(aFilter: string; aColor: TColor = clBlack);
    //1 Setzt den Titel des Histogramms und zeigt die Anzahl der selektierten Alarme an 
    procedure SetHistogramTitle;
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Initialisiert alle verwendeten Serien 
    procedure CreateSeries;
    //1 Zeigt den Alarm im Frame an 
    procedure ShowAlarm(aAlarm: TQOAlarmView);
  published
    //1 wird aufgerufen, wenn im Histogramm auf einen Alarm geklickt wurde 
    property OnAlarmChartClick: TAlarmViewChartClickEvent read FOnAlarmChartClick write FOnAlarmChartClick;
  end;
  

resourcestring
  rsShift          = '(*)Schicht'; // ivlm
  rsShifts         = '(*)Schichten'; // ivlm
  rsAlarmSince     = '(*)Im Alarm seit'; // ivlm
  rsLongestTurn    = '(*)Laengste Alarmdauer'; //ivlm
  rsTotalAlamCount = '(*)Total ausgeloest:'; // ivlm
  rsHistogramm     = '(*)Histogramm'; //ivlm
  rsSelected       = '(*)Ausgewaehlt:'; //ivlm
// START resource string wizard section
resourcestring
  rsAlleAlarme = '(*)Alle Alarme';// ivlm
  rsSelektierteMaschine = '(*)Selektierte Maschine (%s)';// ivlm

// END resource string wizard section


implementation

uses
  adoint, AdoDBAccess, QOGUIShared, mmcs, QODef;

{$R *.DFM}

var
  lAlarmSerie:integer;
  lTurnSeries:integer;
  lRankingSerie: integer;
  lMachineRankingSerie: integer;

type
  (*: Klasse:        TTurn
      Vorg�nger:     TObject
      Kategorie:     No category
      Kurzbeschrieb: Hilfsklasse um zusammenh�ngende Alarme anzeigne zu k�nnen 
      Beschreibung:  
                     Zusammenh�ngende Alarme sind Alarme die in aufeinanderfolgenden Schichten
                     aufgetreten sind. *)
  TTurn = class(TObject)
  private
    FCount: Integer;
    FEndDate: TDateTime;
    FStartDate: TDateTime;
  public
    //1 Kopiert das Source Objekt in sich selber 
    procedure Assign(aSource: TTurn);
    //1 gibt den String mit den Daten des Turns 
    function GetDateString: String;
    //1 gibt den String mit der Anzahl Schichten 
    function GetShiftString: String;
    //1 Anzahl Schichten 
    property Count: Integer read FCount write FCount;
    //1 Datum an dem die Serie zu Ende war 
    property EndDate: TDateTime read FEndDate write FEndDate;
    //1 Datum an dem die Serie begann 
    property StartDate: TDateTime read FStartDate write FStartDate;
  end;
  
//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmAnalyzeAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmAnalyzeAlarmFrame.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

//: ----------------------------------------------
  mAlarmViewSeries := TSeriesList.Create;

//: ----------------------------------------------
  // Untere Achse des Chart definieren
  mHistogramChart.BottomAxis.DateTimeFormat := Format('%s %s', [ShortDateFormat, ShortTimeFormat]);
  mHistogramChart.BottomAxis.Increment := DateTimeStep[dtSixHours];
  mHistogramChart.BottomAxis.LabelsMultiLine := True;
end;// TfrmAnalyzeAlarmFrame.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmAnalyzeAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmAnalyzeAlarmFrame.Destroy;
begin
  mAlarmViewSeries.Clear;
  FreeAndNil(mAlarmViewSeries);

//: ----------------------------------------------
  inherited Destroy;
end;// TfrmAnalyzeAlarmFrame.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           CreateSeries
 *  Klasse:           TfrmAnalyzeAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Initialisiert alle verwendeten Serien
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeAlarmFrame.CreateSeries;
  
  function AddBarSerie(aColor: TColor): Integer;
  begin
    result := mAlarmViewSeries.Add(TQOBarSeries.Create(self));
    with TQOBarSeries(mAlarmViewSeries[result]) do begin
      Marks.Visible := false;
      BorderWidth := 0;
      ParentChart := mHistogramChart;
      XValues.DateTime := true;
  
      BarPen.Color := aColor;
      SeriesColor := aColor;
    end;// with TQOBarSeries(mAlarmViewSeries[result]) do begin
  end;// function AddSerie(aColor: TColor): Integer;
  
  function AddHorBarSerie(aColor: TColor): Integer;
  begin
    result := mAlarmViewSeries.Add(TQOHorizBarSeries.Create(self));
    with TQOHorizBarSeries(mAlarmViewSeries[result]) do begin
      Marks.Visible := false;
      BorderWidth := 0;
      ParentChart := mRankingMachineChart;
  
      BarPen.Color := aColor;
      SeriesColor := aColor;
      ItemOwned := false;
    end;// with TQOHorizBarSeries(mAlarmViewSeries[result]) do begin
  end;// procedure AddHorBarSerie(aColor: TColor);
  
begin
  mAlarmViewSeries.Clear;
  
  (* Doe Alarm Serie im Histogramm zeigt das Auftreten des Alarmes �ber
     einen l�ngeren Zeitraum, Quantisiert mit der Anzahl der beteiligten
     Maschinen *)
  lAlarmSerie := AddBarSerie(clRed);
  with TQOBarSeries(mAlarmViewSeries[lAlarmSerie]) do
    Title := Translate(rsAlleAlarme);
  
  // Serie die den l�ngsten zusammenh�ngenden Zeitraum das Alarms anzeigt
  lTurnSeries := AddBarSerie(cLongestTurnColor);
  
  // Anzeige der selektierten Maschine im Histogramm
  lRankingSerie := AddBarSerie(cSerieHighlightColor);
  
  // Serie im Chart f�r die H�ufigkeit mit der eine Maschine am Alarm beteiligt war
  lMachineRankingSerie := AddHorBarSerie(cMachineRankingColor);
end;// TfrmAnalyzeAlarmFrame.CreateSeries cat:No category

//:-------------------------------------------------------------------
(*: Member:           mChartMouseMove
 *  Klasse:           TfrmAnalyzeAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Shift, X, Y)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn die Maus �ber das Chart bewegt wird
 *  Beschreibung:     
                      Mithilfe dieses Events kann bestimmt werden, ob der Mauszeiger
                      �ber einem Balken ist und, wenn dieser klickbar ist, den Mauszeiger
                      als Finger darstellt.
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeAlarmFrame.mChartMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  xChart: TmmChart;
  i: Integer;
  xFound: Boolean;
begin
  // Initialisieren
  xFound := false;

//: ----------------------------------------------
  // Feststellen, ob auf eine Serie gecklickt wurde
  if (Sender is TmmChart) then begin
    xChart := TmmChart(Sender);
    i := 0;
    // Durch alle Serien durchgehn um festzustellen ob der Punkt zu einer Serie geh�rt
    while (i < xChart.SeriesCount) and (not(xFound)) do begin
      (* Stellt den Index des geklickten Datenpunktes fest
         Ein Index gr�sser -1 heisst, dass der gecklickte Punkt zur entsprechenden Serie geh�rt *)
      xFound := (xChart.Series[i].GetCursorValueIndex > -1);
      // Und die n�chste Serie pr�fen
      inc(i);
    end;// while (i < xChart.SeriesCount) and (not(xFound)) do begin
  end;// if (Sender is TmmChart) then begin

//: ----------------------------------------------
  // Wenn auf eine Serie geklickt wurde und ein Klck erlaubt ist, dann Cursor �ndern
  if (xFound) then
    Screen.Cursor := crHandPoint
  else
    Screen.Cursor := crDefault;
end;// TfrmAnalyzeAlarmFrame.mChartMouseMove cat:No category

//:-------------------------------------------------------------------
(*: Member:           mHistogramChartClickSeries
 *  Klasse:           TfrmAnalyzeAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Series, ValueIndex, Button, Shift, X, Y)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn im Chart auf eine serie geklickt wird
 *  Beschreibung:     
                      Dieses Ereignis wird vom Main Window genutzt um auf eine 
                      bestimmte Schicht zu fokussieren.
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeAlarmFrame.mHistogramChartClickSeries(Sender: TCustomChart; Series: TChartSeries; ValueIndex:
  Integer; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if assigned(FOnAlarmChartClick) then
    FOnAlarmChartClick(Series.XValues[ValueIndex], mAlarm.QOVisualize.Caption, Button, Shift);
end;// TfrmAnalyzeAlarmFrame.mHistogramChartClickSeries cat:No category

//:-------------------------------------------------------------------
(*: Member:           mmTranslator1AfterTranslate
 *  Klasse:           TfrmAnalyzeAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (translator)
 *
 *  Kurzbeschreibung: �bernimmt alle �bersetungen die nicht automatisch "passieren"
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeAlarmFrame.mmTranslator1AfterTranslate(translator: TIvCustomTranslator);
begin
  mRankingMachineChart.Title.Text.Text := rsMachRanking;
  SetHistogramTitle;
end;// TfrmAnalyzeAlarmFrame.mmTranslator1AfterTranslate cat:No category

//:-------------------------------------------------------------------
(*: Member:           mRankingMachineChartClickBackground
 *  Klasse:           TfrmAnalyzeAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Button, Shift, X, Y)
 *
 *  Kurzbeschreibung: L�scht das Highlighten der selektierten Maschine
 *  Beschreibung:     
                      Die Maschine die "ge-highlightet" ist, wird im Histogramm
                      hervorgehoben.
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeAlarmFrame.mRankingMachineChartClickBackground(Sender: TCustomChart; Button: TMouseButton; Shift:
  TShiftState; X, Y: Integer);
begin
  // Ganze Serie l�schen, damit das Histogramm wieder frisch ist
  mAlarmViewSeries[lRankingSerie].Clear;
  // Neuen Titel setzen (ohne Anzahl der Selektion)
  SetHistogramTitle;
  
  // Im Ranking Chart die Selektion l�schen
  ClearSerieColor(mAlarmViewSeries[lMachineRankingSerie]);
end;// TfrmAnalyzeAlarmFrame.mRankingMachineChartClickBackground cat:No category

//:-------------------------------------------------------------------
(*: Member:           mRankingMachineChartClickSeries
 *  Klasse:           TfrmAnalyzeAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Series, ValueIndex, Button, Shift, X, Y)
 *
 *  Kurzbeschreibung: Selektiert eine Maschine
 *  Beschreibung:     
                      Die Maschine die "ge-highlightet" ist, wird im Histogramm
                      hervorgehoben.
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeAlarmFrame.mRankingMachineChartClickSeries(Sender: TCustomChart; Series: TChartSeries; ValueIndex:
  Integer; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  // Hebt die Alarme hervor, an denen die angeklickte Maschine beteiligt war
  SetHighlightSerie(Series.XLabel[ValueIndex], cSerieHighlightColor);
  // Ganze Serie l�schen, damit das Histogramm wieder frisch ist
  ClearSerieColor(Series);
  // Selektierte Maschine anzeigen
  Series.ValueColor[ValueIndex] := cSerieHighlightColor;
end;// TfrmAnalyzeAlarmFrame.mRankingMachineChartClickSeries cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetHighlightSerie
 *  Klasse:           TfrmAnalyzeAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (aFilter, aColor)
 *
 *  Kurzbeschreibung: Baut die zu Filternde Serie auf
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeAlarmFrame.SetHighlightSerie(aFilter: string; aColor: TColor = clBlack);
var
  i: Integer;
  xRankingSerie: TQOBarSeries;
  xAlarmSerie: TQOBarSeries;
  
  const
    cGetMachine = 'SELECT ad.c_caught_alarm_id, ad.c_QO_alarm_data_id, im.c_machine_name FROM t_QO_Alarm_Data ad, t_QO_Involved_Machine im ' +
                  'WHERE  ad.c_QO_alarm_data_id = im.c_QO_alarm_data_id ' +
                  '   AND c_machine_name in (''%s'') ' +
                  '   AND ad.c_QO_Alarm_Name = ''%s''';
  
begin
  // Serie die im Histogramm die Alarme anzeigt an denen die entsprechende Maschine beteiligt ist
  xRankingSerie := TQOBarSeries(mAlarmViewSeries[lRankingSerie]);
  // Alarm Serie im Histogramm
  xAlarmSerie   := TQOBarSeries(mAlarmViewSeries[lAlarmSerie]);

//: ----------------------------------------------
  // Alle Alarme mit dem entsprechenden Maschinen Name abfrage
  with TAdoDBAccess.Create(1) do try
    Init;
    with Query[0] do begin
      // holt die Daten an denen die �bergebene Maschine an einem Alarm beteiligt war
      SQL.Text := Format(cGetMachine, [PrepareStringForSQL(aFilter, false), PrepareStringForSQL(mAlarm.QOVisualize.Caption, false)]);
      open;
  
      // Serie Konfigurieren
      xRankingSerie.Clear;
      xRankingSerie.BarPen.Color := aColor;
      xRankingSerie.SeriesColor := aColor;
      xRankingSerie.Title := Format(Translate(rsSelektierteMaschine), [aFilter]);
  
      (* Um die Datenpunkte die in die neue Serie geh�ren zu finden, wird der gesammte
         "Datenhaufen" des Histogramms gescannt(xAlarmSerie) um eine �bereinstimmung der Daten
         festzustellen.
         Ist eine �bereinstimmung festgestellt worden, dann wird der Datensatz mit der Maschine
         (xRankingSerie) in die Serie eingef�gt. *)
      for i:= 0 to xAlarmSerie.Count - 1 do begin
        // F�r jeden Alarm den entsprechenden Datensatz ausfiltern
        Filter('c_QO_alarm_data_id = ' + IntToStr(PAlarmDataRec(xAlarmSerie.Items[i])^.AlarmDataID));
        FindFirst;
        if not(EOF) then
          xRankingSerie.AddXY(xAlarmSerie.XValues[i], xAlarmSerie.YValues[i]);
      end;// for i:= 0 to xAlarmSerie.Count - 1 do begin
  
      // Anzeigen wieviele Daten selektiert sind
      SetHistogramTitle;
    end;// with Query[0] do begin
  
  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try
end;// TfrmAnalyzeAlarmFrame.SetHighlightSerie cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetHistogramTitle
 *  Klasse:           TfrmAnalyzeAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Setzt den Titel des Histogramms und zeigt die Anzahl der selektierten Alarme an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeAlarmFrame.SetHistogramTitle;
var
  xFilterSerie: TChartSeries;
begin
  xFilterSerie := nil;
  
  if assigned(mAlarmViewSeries) then
    if mAlarmViewSeries.SerieCount > lRankingSerie then
      xFilterSerie := mAlarmViewSeries[lRankingSerie];
  
  // Anzeigen wieviele Alarme selektiert sind
  if assigned(xFilterSerie) then begin
  
    // Anzeigen wieviele Daten selektiert sind
    if (xFilterSerie.Count = 0) then
      mHistogramChart.Title.Text.Text := rsHistogramm
    else
      mHistogramChart.Title.Text.Text := rsHistogramm + ' - ' + rsSelected + '' + IntToStr(xFilterSerie.Count);
  end else begin
    mHistogramChart.Title.Text.Text := rsHistogramm
  end;// if assigned(xFilterSerie) then begin
end;// TfrmAnalyzeAlarmFrame.SetHistogramTitle cat:No category

//:-------------------------------------------------------------------
(*: Member:           ShowAlarm
 *  Klasse:           TfrmAnalyzeAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (aAlarm)
 *
 *  Kurzbeschreibung: Zeigt den Alarm im Frame an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeAlarmFrame.ShowAlarm(aAlarm: TQOAlarmView);
var
  xOldestShiftID: Integer;
  xActualTurn: TTurn;
  xLongestTurn: TTurn;
  xTurn: TTurn;
  xTotalCount: Integer;
  i: Integer;
  xMachineCount: TNativeAdoQuery;
  xAlarmDataRec: PAlarmDataRec;
  xSerie: TQOBarSeries;
  xAlarmsFound: Boolean;
  
  const
    cGetAlarmShiftData = 'SELECT s.c_shift_id , ad.c_QO_Alarm_Name, ad.c_QO_alarm_data_id, s.c_shift_start, ca.c_date ' +
                         'FROM t_shift s ' +
                         '         LEFT OUTER JOIN t_QO_Caught_Alarm ca ON ca.c_shift_id = s.c_shift_id ' +
                         '                  LEFT OUTER JOIN t_QO_Alarm_Data ad ON ca.c_caught_alarm_id = ad.c_caught_alarm_id AND ad.c_QO_Alarm_Name = ''%s'' ' +
                         'WHERE  s.c_shift_id <= :AktShift ' +
                         '   AND s.c_shift_id >= :OldestShift ' +
                         '   AND s.c_shiftcal_id = :c_shiftcal_id ' +
                         'ORDER BY s.c_shift_id DESC ';
  
    cGetOldestShiftID  = 'SELECT Min(c_shift_id) ' +
                         'FROM t_QO_Caught_Alarm ' +
                         'WHERE c_shiftCal_id = :c_shiftCal_id';
  
    cGetMachineCount = 'SELECT c_QO_alarm_data_id, Count(*) as c_Count ' +
                       'FROM t_QO_Involved_Machine ' +
                       'Group By c_QO_alarm_data_id';
  
    cGetMachineCountChart = 'SELECT Count(*) as #, im.c_machine_name ' +
                            'FROM t_QO_Alarm_Data ad, t_QO_involved_machine im, t_QO_Caught_Alarm ca ' +
                            'WHERE  ad.c_QO_Alarm_Name = :c_QO_Alarm_Name ' +
                            '   AND ad.c_QO_Alarm_data_id = im.c_QO_Alarm_data_id ' +
                            '   AND ca.c_caught_alarm_id = ad.c_caught_alarm_id ' +
                            '   AND ca.c_date <= ''%s'' ' +
                            'GROUP BY im.c_machine_name';
  
  
  (*-----------------------------------------------------------
    Pr�ft ob die Turn-Objekte aktualisiert werden m�ssen
  -------------------------------------------------------------*)
  procedure CheckAndUpdateTurnCount;
  begin
    // Sobald der aktuelle Turn einmal zugewiesen wurde, hat sich die Sache erledigt
    if xActualTurn.Count = 0 then
      xActualTurn.Assign(xTurn);
  
    (* Der L�ngste Turn wird nur aktualisiert, wenn der tempor�re Turn(xTurn) mehr Alarme registriert hat
       Da in Richtung Vergangenheit durchsucht wird, wird immer derjenige Turn �bernommen
       der am n�chsten am aktuellen Datum liegt. *)
    if xTurn.Count > xLongestTurn.Count then begin
      xLongestTurn.Assign(xTurn);
      // Diese Anweisung wird erst ausgef�hrt, wenn mindestens ein Alarm gefunden wurde.
      xAlarmsFound := true;
    end;// if xTurn.Count > xLongestTurn.Count then begin
  end;// procedure CheckTurnCount;
  
  (*-----------------------------------------------------------
    F�gt einen Datenpunkt mit Zusatzeangaben in die Alarm Serie ein
  -------------------------------------------------------------*)
  procedure InsertAlarmRec(aShiftID: integer; aAlarmDataID: integer; aValue: integer; aShiftStart, aDate: TDateTime);
  begin
    // Neuen Record erzeugen (Wird von der Serie freigegeben)
    new(xAlarmDataRec);
  
    // Record abf�llen
    xAlarmDataRec^.ShiftID := aShiftID;
    xAlarmDataRec^.AlarmDataID := aAlarmDataID;
    xAlarmDataRec^.Value := aValue;
    xAlarmDataRec^.Date := aDate;
    xAlarmDataRec^.ShiftStart := aShiftStart;
  
    // Neuen Datenpunkt einf�gen
    TQOBarSeries(mAlarmViewSeries[lAlarmSerie]).AddXY(aDate, aValue, '', mAlarmViewSeries[lAlarmSerie].SeriesColor, xAlarmDataRec);
  end;// procedure FillAlarmRec(...
  
begin
  // Zoomfaktor wieder zur�ckstellen
  mRankingMachineChart.UndoZoom;
  mHistogramChart.UndoZoom;
  
  xAlarmsFound := false;
  try
    Screen.Cursor := crSQLWait;
    // Alarm f�r die sp�tere Verwendung sichern
    mAlarm := aAlarm;
  
    // Alle Serien Initialisieren
    CreateSeries;
  
    // Das TShape Objekt f�r die Legende des l�ngsten Turn einf�rben
    mLegendLongestTurn.Brush.Color := mAlarmViewSeries[lTurnSeries].SeriesColor;
  
    // Initialisieren
    xTotalCount := 0;

//: ----------------------------------------------
    (* Es werden 2 Queries gemeinsam gebraucht (Aanzahl beteiligter Maschinen pro Schicht und
       jewils ein Datensatz pro Schicht, egal ob Alarm oder nicht *)
    with TAdoDBAccess.Create(2) do try
      Init;

//: ----------------------------------------------
      // Zuerst die Anzahl beteiligter Maschinen pro Alarm ermitteln (ein DS pro Alarm)
      xMachineCount := Query[1];
      with xMachineCount do begin
        SQL.Text := cGetMachineCount;
        open;
      end;// with xMachineCount do begin
  
      // Turn der zu diesem Alarm gef�hrt hat
      xActualTurn  := TTurn.Create;
      // L�ngste zusammenh�ngende Periode seit der Alarm aufgetreten ist
      xLongestTurn := TTurn.Create;
      // Tempor�re Variable f�r den aktuellen Durchlauf (Schleife)
      xTurn        := TTurn.Create;
      with Query[0] do begin

//: ----------------------------------------------
        // �lteste Schicht ermitteln in der ein Alarm aufgetreten ist
        SQL.Text := cGetOldestShiftID;
        ParamByName('c_shiftcal_id').AsInteger := TQOAppSettings.Instance.ShiftCal;
        open;
        if not(EOF) then begin
          xOldestShiftID := Fields[0].AsInteger;
          (* F�r jede Schicht seit dem ersten Alarm wird ein DS zur�ckgegeben. Ist in der entsprechenden
             Schicht kein Alarm eingetreten, dann ist das Feld 'c_QO_Alarm_Name' mit dem Wert NULL gef�llt. *)
          SQL.Text := Format(cGetAlarmShiftData, [StringReplace(aAlarm.QOVisualize.Caption, '''', '''''', [rfReplaceAll])]);
          ParamByName('AktShift').AsInteger := aAlarm.ShiftID;
          ParamByName('OldestShift').AsInteger := xOldestShiftID;
          ParamByName('c_shiftcal_id').AsInteger := TQOAppSettings.Instance.ShiftCal;
  
          open;
          FindFirst;
  
          // Escape Taste "scharfstellen"
          GetAsyncKeyState(VK_ESCAPE);
  
          // Daten in der 'lAlarmSerie' eintragen. Die Schleife kann mit der ESCAPE Taste abgebrochen werden
          while (not(EOF)) and (GetAsyncKeyState(VK_ESCAPE) = 0) do begin
            // Pr�fen ob f�r die Schicht ein Alarm registriert ist
            if VarIsNull(FieldByName('c_QO_Alarm_Name').Value) then begin
              // In dieser Schicht ist kein Alarm des Typs 'aAlarm' eingetreten, deshalb wird ein 0-Wert eingef�gt
              InsertAlarmRec(FieldByName('c_shift_id').AsInteger, FieldByName('c_QO_alarm_data_id').AsInteger, 0, FieldByName('c_shift_start').AsDateTime, FieldByName('c_shift_start').AsDateTime);
              // �berpr�ft ob eine der TTurn Variablen aktualisiert werden muss
              CheckAndUpdateTurnCount;
              // ein neuer Turn kann erst wieder beginnen, wenn ein Alarm registriert wurd
              xTurn.count := 0;
            end else begin
              // Ein Alarm des Typs 'aAlarm' ist in dieser Schicht eingetreten, deshalb wird die Anzahl Maschinen eingetragen
              xMachineCount.Filter('c_QO_alarm_data_id = ' + FieldByName('c_QO_alarm_data_id').AsString);
              if not(xMachineCount.EOF) then
                // Anzahl Maschinen eintragen
                InsertAlarmRec(FieldByName('c_shift_id').AsInteger, FieldByName('c_QO_alarm_data_id').AsInteger, xMachineCount.FieldByName('c_Count').AsInteger, FieldByName('c_shift_start').AsDateTime, FieldByName('c_date').AsDateTime)
              else
                // Im Fehlerfall wenigstens eine Maschine eintragen (sollte nie eintreten)
                InsertAlarmRec(FieldByName('c_shift_id').AsInteger, FieldByName('c_QO_alarm_data_id').AsInteger, 1, FieldByName('c_shift_start').AsDateTime, FieldByName('c_date').AsDateTime);
  
              // Z�hler erh�hen (Z�hlt die gesammt Anzahl der Alarme)
              inc(xTotalCount);
  
              // Wenn ein neuer Turn beginnt, ...
              if xTurn.Count = 0 then
                // ... dann das Enddatum sichern (wird vom Aktuellen Datum in die Vergangenheit aufgerollt)
                xTurn.EndDate := FieldByName('c_date').AsDateTime;
              // Das Datum des aktuellen DS ist immer das Startdatum des Turns
              xTurn.StartDate := FieldByName('c_date').AsDateTime;
              // Anzahl Alarme in diesem Turn hochz�hlen
              xTurn.Count := xTurn.Count + 1;
            end;// if VarIsNull(FieldByName('c_QO_Alarm_Name').Value) then begin
            // N�chste Schicht
            next;
          end;// while not(EOF) do begin
          // �berpr�ft ob eine der TTurn Variablen aktualisiert werden muss
          CheckAndUpdateTurnCount;
        end;// while (not(EOF)) and (GetAsyncKeyState(VK_ESCAPE) = 0) do begin
  
        // Fragt die H�ufigkeit ab mit der die Maschinen am Alarm 'aAlarm' beteiligt waren
        SQL.Text := Format(cGetMachineCountChart, [StringReplace(FormatDateTime(cSQLDateTimeFormatString, aAlarm.AlarmDate), '''', '''''', [rfReplaceAll])]);
        ParamByName('c_QO_Alarm_Name').AsString := aAlarm.QOVisualize.Caption;
        open;
  
        // F�r jeden DS ein Punkt (horizontaler Balken) zum Chart hinzuf�gen
        while not(EOF) do begin
          mAlarmViewSeries[lMachineRankingSerie].Add(FieldByName('#').AsInteger, FieldByName('c_machine_name').AsString);
          next;
        end;// while not(EOF) do begin

//: ----------------------------------------------
      end;// with Query[0] do begin
  
      // Den l�ngsten Turn im Histogramm als neue Serie einf�gen
      for i := 0 to TQOBarSeries(mAlarmViewSeries[lAlarmSerie]).Count - 1 do begin
        // Pr�ft ob das Alarmdatum in den Bereich des l�ngsten Turns f�llt
        xSerie := TQOBarSeries(mAlarmViewSeries[lAlarmSerie]);
        if   (PAlarmDataRec(xSerie.Items[i])^.Date >= xLongestTurn.StartDate)
         and (PAlarmDataRec(xSerie.Items[i])^.Date <= xLongestTurn.EndDate) then
           mAlarmViewSeries[lTurnSeries].AddXY(TQOBarSeries(mAlarmViewSeries[lAlarmSerie]).XValues[i], TQOBarSeries(mAlarmViewSeries[lAlarmSerie]).YValues[i]);
      end;// for i := 0 to mAlarmViewSeries[lMachineRankingSerie].Count - 1 do begin
  
      // Statisik anzeigen f�r die Anzahl Schichten des Aktuellen Turns
      laAlarmTurn.Caption := rsAlarmSince + ' ' + xActualTurn.GetShiftString;
      laAlarmTurnDate.Caption := xActualTurn.GetDateString;
  
      // ... des l�ngsten Turns
      laLongestTurn.Caption := rsLongestTurn + ' ' + xLongestTurn.GetShiftString;
      laLongestTurnDate.Caption := xLongestTurn.GetDateString;
  
      // und der Anzahl der Total aufgetretenen Alarme
      laTotalCount.Caption := rsTotalAlamCount + ' ' + IntToStr(xTotalCount)
    finally
      // AdoDBccess
      Free;
      FreeAndNil(xActualTurn);
      FreeAndNil(xLongestTurn);
      FreeAndNil(xTurn);
    end;// with TAdoDBAccess.Create(1) do try
  
  finally
    Screen.Cursor := crDefault;
    CodeSite.SendString('Alarm Daten', aAlarm.AlarmData.AsXML);
    if (xAlarmsFound) then begin
      mRankingMachineChart.visible := true;
      mHistogramChart.visible := true;
      paExtendedInfo.visible := true;
      pSpacePanel.visible := false;
      paInfo.visible := false;
    end else begin
      mRankingMachineChart.visible := false;
      mHistogramChart.visible := false;
      paExtendedInfo.visible := false;
      pSpacePanel.visible := true;
      paInfo.visible := true;
    end;// if (xAlarmsFound) then begin
  end;// try finally
end;// TfrmAnalyzeAlarmFrame.ShowAlarm cat:No category


//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TTurn
 *  Kategorie:        No category 
 *  Argumente:        (aSource)
 *
 *  Kurzbeschreibung: Kopiert das Source Objekt in sich selber
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TTurn.Assign(aSource: TTurn);
begin
  FCount := aSource.Count;
  FStartDate := aSource.StartDate;
  FEndDate := aSource.EndDate;
end;// TTurn.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetDateString
 *  Klasse:           TTurn
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: gibt den String mit den Daten des Turns
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TTurn.GetDateString: String;
  
  function GetDateString(aDate: TDateTime): String;
  begin
    result := DateToStr(aDate) + FormatDateTime(Format(' hh%smm',[TimeSeparator]), aDate);
  end;
  
begin
  result := GetDateString(FStartDate);
  if Count > 1 then
    result := result + '  -  ' + GetDateString(FEndDate);
end;// TTurn.GetDateString cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetShiftString
 *  Klasse:           TTurn
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: gibt den String mit der Anzahl Schichten
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TTurn.GetShiftString: String;
begin
  result := IntToStr(FCount) + ' ';
  
  if Count = 1 then
    result := result + rsShift
  else
    result := result + rsShifts;
end;// TTurn.GetShiftString cat:No category

end.








