object frmAlarmAssignFrame: TfrmAlarmAssignFrame
  Left = 0
  Top = 0
  Width = 510
  Height = 373
  TabOrder = 0
  object mRightPanel: TmmPanel
    Left = 0
    Top = 0
    Width = 510
    Height = 373
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object mmPanel1: TmmPanel
      Left = 0
      Top = 0
      Width = 510
      Height = 373
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object mHorSplitter: TmmSplitter
        Left = 0
        Top = 188
        Width = 510
        Height = 9
        Cursor = crVSplit
        Align = alBottom
      end
      object mTopPanel: TmmPanel
        Left = 0
        Top = 0
        Width = 510
        Height = 188
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object mTopButtonPanel: TmmPanel
          Left = 0
          Top = 0
          Width = 47
          Height = 188
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object imMachCollapse: TmmImage
            Left = 0
            Top = 104
            Width = 32
            Height = 16
            Picture.Data = {
              07544269746D617076010000424D760100000000000076000000280000002000
              0000100000000100040000000000000100000000000000000000100000000000
              0000000000000000800000800000008080008000000080008000808000008080
              8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
              FF00D0000000DDDDDDDDDDDDDDDDDD0FDDDD788888870DDDDDDDDDDDDDDDD000
              FDDD788899877000DDDDDDDDDDDD00F00FDD788888007030D00DDDDDDDD00FDD
              00FD7888883300B303B0DDDDDDDDDDDDDDDD7888880B30F33B0DDDDDDDDDDD0F
              DDDD78888030BFBFB030DDDDDDDDD000FDDD7FFFF03BF000FB30DDDDDDDD00F0
              0FDD7777730F08F00300DDDDDDD00FDD00FD78888803B8F03B0DDDDDDDDDDDDD
              DDDD7FFFFF3B08F0030DDDDDDDDDDD0FDDDD777777737887D0DDDDDDDDDDD000
              FDDD7888888700DDDDDDDDDDDDDD00F00FDD7FFFFFF770DDDDDDDDDDDDD00FDD
              00FD7888888870DDDDDDDDDDDDDDDDDDDDDDD77777777DDDDDDDDDDDDDDDDDDD
              DDDD}
            Visible = False
            AutoLabel.LabelPosition = lpLeft
          end
          object imMachExpand: TmmImage
            Left = 0
            Top = 88
            Width = 32
            Height = 16
            Picture.Data = {
              07544269746D617076010000424D760100000000000076000000280000002000
              0000100000000100040000000000000100000000000000000000100000000000
              0000000000000000800000800000008080008000000080008000808000008080
              8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
              FF00D0000000DDDDDDDDDDDDDDDDDDDDDDDD788888870DDDDDDDDDDDDDDDDDDD
              DDDD788899877000DDDDDDDDDDD00FDD00FD788888007030D00DDDDDDDDD00F0
              0FDD7888883300B303B0DDDDDDDDD000FDDD7888880B30F33B0DDDDDDDDDDD0F
              DDDD78888030BFBFB030DDDDDDDDDDDDDDDD7FFFF03BF000FB30DDDDDDD00FDD
              00FD7777730F08F00300DDDDDDDD00F00FDD78888803B8F03B0DDDDDDDDDD000
              FDDD7FFFFF3B08F0030DDDDDDDDDDD0FDDDD777777737887D0DDDDDDDDDDDDDD
              DDDD7888888700DDDDDDDDDDDDD00FDD00FD7FFFFFF770DDDDDDDDDDDDDD00F0
              0FDD7888888870DDDDDDDDDDDDDDD000FDDDD77777777DDDDDDDDDDDDDDDDD0F
              DDDD}
            Visible = False
            AutoLabel.LabelPosition = lpLeft
          end
          object bAddStyle: TmmSpeedButton
            Left = 11
            Top = 40
            Width = 24
            Height = 24
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Glyph.Data = {
              92000000424D9200000000000000760000002800000007000000070000000100
              0400000000001C00000000000000000000001000000000000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888808808880
              08808800088080000880880008808880088088880880}
            ParentFont = False
            Visible = True
            OnClick = bAddStyleClick
            AutoLabel.LabelPosition = lpLeft
          end
          object bShowMachines: TmmSpeedButton
            Left = 0
            Top = 164
            Width = 47
            Height = 20
            Anchors = [akLeft, akBottom]
            Flat = True
            Visible = True
            OnClick = bShowMachinesClick
            AutoLabel.LabelPosition = lpLeft
          end
        end
        object mmPanel2: TmmPanel
          Left = 47
          Top = 0
          Width = 463
          Height = 188
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object mStyleList: TmmVirtualStringTree
            Left = 0
            Top = 21
            Width = 463
            Height = 167
            Align = alClient
            DragMode = dmAutomatic
            DragType = dtVCL
            Header.AutoSizeIndex = 0
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            Header.Options = [hoColumnResize, hoDrag, hoShowImages, hoShowSortGlyphs, hoVisible]
            Header.SortDirection = sdDescending
            Indent = 2
            TabOrder = 0
            TreeOptions.MiscOptions = [toAcceptOLEDrop, toFullRepaintOnResize, toGridExtensions, toInitOnSave, toToggleOnDblClick, toWheelPanning]
            TreeOptions.PaintOptions = [toHideFocusRect, toShowButtons, toShowDropmark, toShowRoot, toThemeAware, toUseBlendedImages]
            TreeOptions.SelectionOptions = [toFullRowSelect, toMultiSelect]
            OnCompareNodes = TreeCompareNodes
            OnFocusChanged = mStyleListFocusChanged
            OnFreeNode = TreeFreeNode
            OnGetCursor = mStyleListGetCursor
            OnGetText = mStyleListGetText
            OnGetImageIndex = mStyleListGetImageIndex
            OnHeaderClick = TreeHeaderClick
            OnResize = mStyleListResize
            Columns = <
              item
                Position = 1
                WideText = '(*)Artikel'
              end
              item
                Margin = 0
                Position = 0
                Spacing = 0
                Width = 40
              end>
          end
          object mIncrementalStylePanel: TmmPanel
            Left = 0
            Top = 0
            Width = 463
            Height = 21
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            OnResize = mIncrementalStylePanelResize
            object edIncrementalStyle: TmmEdit
              Left = 0
              Top = 0
              Width = 225
              Height = 21
              Color = clWindow
              TabOrder = 0
              Visible = True
              OnChange = edIncrementalStyleChange
              OnEnter = edIncrementalStyleEnter
              OnExit = edIncrementalStyleExit
              AutoLabel.LabelPosition = lpLeft
              ReadOnlyColor = clInfoBk
              ShowMode = smNormal
            end
          end
        end
      end
      object mBottomPanel: TmmPanel
        Left = 0
        Top = 197
        Width = 510
        Height = 176
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object mBottomButtonPanel: TmmPanel
          Left = 0
          Top = 0
          Width = 47
          Height = 176
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object bAddMachine: TmmSpeedButton
            Left = 11
            Top = 19
            Width = 24
            Height = 24
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Glyph.Data = {
              92000000424D9200000000000000760000002800000007000000070000000100
              0400000000001C00000000000000000000001000000000000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888808808880
              08808800088080000880880008808880088088880880}
            ParentFont = False
            Visible = True
            OnClick = bAddMachineClick
            AutoLabel.LabelPosition = lpLeft
          end
        end
        object mMachineList: TmmVirtualStringTree
          Left = 47
          Top = 0
          Width = 463
          Height = 176
          Align = alClient
          DragMode = dmAutomatic
          DragType = dtVCL
          Header.AutoSizeIndex = 0
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          Header.Options = [hoColumnResize, hoDrag, hoShowImages, hoShowSortGlyphs, hoVisible]
          Header.SortDirection = sdDescending
          Indent = 2
          TabOrder = 1
          TreeOptions.MiscOptions = [toAcceptOLEDrop, toFullRepaintOnResize, toGridExtensions, toInitOnSave, toToggleOnDblClick, toWheelPanning]
          TreeOptions.PaintOptions = [toHideFocusRect, toShowButtons, toShowDropmark, toShowRoot, toThemeAware, toUseBlendedImages]
          TreeOptions.SelectionOptions = [toFullRowSelect, toMultiSelect]
          OnCompareNodes = TreeCompareNodes
          OnFreeNode = TreeFreeNode
          OnGetText = mMachineListGetText
          OnGetImageIndex = mMachineListGetImageIndex
          OnHeaderClick = TreeHeaderClick
          OnResize = mMachineListResize
          Columns = <
            item
              Position = 1
              WideText = '(30)Maschinen'
            end
            item
              Margin = 2
              Position = 0
              Width = 40
            end>
        end
      end
    end
  end
  object mTranslator: TmmTranslator
    DictionaryName = 'MainDictionary'
    OnAfterTranslate = mTranslatorAfterTranslate
    Left = 361
    Top = 65
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        'TVirtualTreeColumn'
        'Text'
        0))
  end
end
