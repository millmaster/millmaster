(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: EventInfo
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Zusatzinformationen zu Schicht oder Alarm im Analyse Modus
|
| Info..........: 
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 06.01.2004  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit EventInfo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, MMSecurity, IvDictio, IvMulti, IvEMulti, mmTranslator,
  StdCtrls, ComCtrls, mmRichEdit, Buttons, ExtCtrls, ActnList, mmActionList,
  mmBitBtn, ImgList, mmImageList;

type
  TfrmEventInfo = class (TmmForm)
    acConfigSecurity: TAction;
    acSave: TAction;
    bCancel: TBitBtn;
    bOk: TBitBtn;
    bSecurityConfiguration: TmmBitBtn;
    memoUserInfo: TmmRichEdit;
    mmActionList1: TmmActionList;
    mmImageList: TmmImageList;
    mSecurityControl: TMMSecurityControl;
    mTranslator: TmmTranslator;
    Panel1: TPanel;
    Panel2: TPanel;
    //1 Bietet Zugriff auf die Security Settings 
    procedure acConfigSecurityExecute(Sender: TObject);
    //1 Diese Methode ist notwendig, da sonst die Action nicht Enabled werden kann 
    procedure acSaveExecute(Sender: TObject);
    //1 Initialisiert das Formular 
    procedure FormCreate(Sender: TObject);
  private
    //1 liefert die Zusatzinformation f�r die entsprechende Schicht oder den Alarm 
    function GetUserInfo: String;
    //1 Setzt die Zusatzinformation f�r die entsprechende Schicht oder den Alarm 
    procedure SetUserInfo(const Value: String);
  public
    //1 Zusatzinformation f�r die entsprechende Schicht oder den Alarm 
    property UserInfo: String read GetUserInfo write SetUserInfo;
  end;
  
var
  frmEventInfo: TfrmEventInfo;

implementation

{$R *.DFM}

//:-------------------------------------------------------------------
(*: Member:           acConfigSecurityExecute
 *  Klasse:           TfrmEventInfo
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Bietet Zugriff auf die Security Settings
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmEventInfo.acConfigSecurityExecute(Sender: TObject);
begin
  mSecurityControl.Configure;
end;// TfrmEventInfo.acConfigSecurityExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acSaveExecute
 *  Klasse:           TfrmEventInfo
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Diese Methode ist notwendig, da sonst die Action nicht Enabled werden kann
 *  Beschreibung:     
                      Die Zusatzinfos werden im Hauptformular gespeichert. Der entsprechende Button
                      liefert das Modalresult zur�ck.
 --------------------------------------------------------------------*)
procedure TfrmEventInfo.acSaveExecute(Sender: TObject);
begin
  inherited;
  // Notwendig, damit der Button Enabled ist
end;// TfrmEventInfo.acSaveExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           FormCreate
 *  Klasse:           TfrmEventInfo
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Initialisiert das Formular
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmEventInfo.FormCreate(Sender: TObject);
begin
  bSecurityConfiguration.Caption := '';
end;// TfrmEventInfo.FormCreate cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetUserInfo
 *  Klasse:           TfrmEventInfo
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: liefert die Zusatzinformation f�r die entsprechende Schicht oder den Alarm
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmEventInfo.GetUserInfo: String;
begin
  result := memoUserInfo.Text;
end;// TfrmEventInfo.GetUserInfo cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetUserInfo
 *  Klasse:           TfrmEventInfo
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Zusatzinformation f�r die entsprechende Schicht oder den Alarm
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmEventInfo.SetUserInfo(const Value: String);
begin
  memoUserInfo.Text := Value;
end;// TfrmEventInfo.SetUserInfo cat:No category

end.

