(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: SettingsAlarmFrame.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Frame f�r die Darstellung eines Alarm Settings
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.04.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit SettingsAlarmFrame;

interface

uses
  QOKernel, ClassDef, DataItemTree, QOGUIShared,
  Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, mmLabel, mmRadioGroup, mmListbox, VirtualTrees, ActiveX,
  mmPanel, mmEdit, ComCtrls, mmPageControl, ExtCtrls, StdCtrls, mmSplitter,
  Buttons, mmSpeedButton, mmImage, mmDialogs, ImgList, mmImageList,
  ActnList, mmActionList, ToolWin, mmToolBar, mmCheckBox, mmGroupBox,
  IvDictio, IvMulti, IvEMulti, mmTranslator, MMSecurity, ComObj, MMUGlobal, mmcs,
  mmMemo;

type
  TfrmSettingsAlarmFrame = class;
  //1 Wird gefeuert nachdem ein Alarm gesichert wurde 
  TOnAlarmSaved = procedure (Sender: TfrmSettingsAlarmFrame; aAlarm: TQOAlarm) of object;
  (*: Klasse:        TfrmSettingsAlarmFrame
      Vorg�nger:     TFrame
      Kategorie:     View
      Kurzbeschrieb: Stellt die Settings eines Alarms dar 
      Beschreibung:  
                     - *)
  TfrmSettingsAlarmFrame = class (TFrame)
    acCallClassdesigner: TAction;
    acSave: TAction;
    acUndo: TAction;
    bAddItem: TmmSpeedButton;
    cbActive: TmmCheckBox;
    cbEmail: TmmCheckBox;
    cbMonitor: TmmCheckBox;
    cbPager: TmmCheckBox;
    cbPrinter: TmmCheckBox;
    cbSMS: TmmCheckBox;
    edName: TmmEdit;
    laLastSave: TmmLabel;
    mButtonImages: TmmImageList;
    mButtonPanel: TmmPanel;
    mDataItemTree: TDataItemTree;
    mDescriptionSpacePanel: TmmPanel;
    mInfoImage: TmmImage;
    mInfoMemo: TmmMemo;
    mInfoPanel: TmmPanel;
    mmActionList1: TmmActionList;
    mmGroupBox1: TmmGroupBox;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmPanel1: TmmPanel;
    mmPanel2: TmmPanel;
    mmPanel3: TmmPanel;
    mmToolBar1: TmmToolBar;
    mmToolBar2: TmmToolBar;
    mSettingsPage: TmmPageControl;
    mTranslator: TmmTranslator;
    rgBoolOp: TmmRadioGroup;
    tabItems: TTabSheet;
    tabSettings: TTabSheet;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    procedure acCallClassdesignerExecute(Sender: TObject);
    //1 Speichert die ver�nderten Settings 
    procedure acSaveExecute(Sender: TObject);
    //1 L�dt die Settings wieder vom urspr�nglichen Alarm 
    procedure acUndoExecute(Sender: TObject);
    //1 f�gt die selektierten Data Items zum alarm hinzu 
    procedure bAddItemClick(Sender: TObject);
    //1 Erfasst ob �nderungen an den Settings gemacht wurden 
    procedure ChangeFromGUI(Sender: TObject);
    //1 L�ngenbegranzung des Names des Alarms 
    procedure edNameChange(Sender: TObject);
    //1 F�gt das DataItem auf das doppelt geklickt wurde zum Alarm hinzu 
    procedure mDataItemTreeDblClick(Sender: TObject);
    //1 wird aufgerufen, wenn das aktive Page �ndert 
    procedure mSettingsPageChange(Sender: TObject);
    //1 �bernimmt alle �bersetungen die nicht automatisch "passieren" 
    procedure mTranslatorAfterTranslate(translator: TIvCustomTranslator);
  private
    FCurrentUser: String;
    FOnAddItems: TOnAddItems;
    FOnOnAlarmSaved: TOnAlarmSaved;
    FQOAlarm: TQOAlarm;
    FSaveEnabled: Boolean;
    FSaveVisible: Boolean;
    //1 Gibt die ImageListe des DataItemTree zur�ck 
    function GetImages: TImageList;
    //1 Zugriffsmethode f�r CurrentUser 
    procedure SetCurrentUser(const Value: String);
    //1 Zugriffsmethode f�r QOAlarm 
    procedure SetQOAlarm(Value: TQOAlarm);
  protected
    //1 Wird aufgerufen, wenn ein Alarm gesichert wurde 
    procedure DoAlarmSaved(Sender: TfrmSettingsAlarmFrame; aAlarm: TQOAlarm);
    //1 Ruft den Ereignishandler auf, wenn ein DAtaItem zum Alarm hinzugef�gt werden soll 
    procedure DoOnAddItems(Sender: TBaseVirtualTree);
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Gibt die selektierten Benachrichtigungs Methoden als Set zur�ck 
    function GetAdviceMethods: TAdviceMethodSet;
    //1 Gibt True zur�ck, wenn das aktuelle Objekt auf der gespeichert ist 
    function saved: Boolean;
    //1 Speichert die Einstellungen im AlarmObjekt 
    function SaveSettings(aAskForPermission: boolean; aShowCancel: boolean): Word;
    //1 True, wenn die Settings noch denen des �bergebenen Alarms entsprechen 
    function SettingsChanged: Boolean;
    //1 Zeigt die Einstellungen des �bergebenen Alarms an 
    procedure ShowAlarm(aQOAlarm: TObject; aActivePage: TTabSheet = nil);
    //1 Aktueller Benutzer f�r das Speicherdatum 
    property CurrentUser: String read FCurrentUser write SetCurrentUser;
    //1 ImageListe des DataItemTree 
    property Images: TImageList read GetImages;
    //1 Anzuzeigender Alarm 
    property QOAlarm: TQOAlarm read FQOAlarm write SetQOAlarm;
    //1 True wenn die Action acSave Enabled sein darf 
    property SaveEnabled: Boolean read FSaveEnabled write FSaveEnabled;
    //1 True wenn die Action acSave sichtbar sein darf 
    property SaveVisible: Boolean read FSaveVisible write FSaveVisible;
  published
    //1 Event der gefeuert wird, wenn Data Items zum Alarm hinzugef�gt werden sollen 
    property OnAddItems: TOnAddItems read FOnAddItems write FOnAddItems;
    //1 Wird gefeuert, wenn der Alarm gesichert wurde 
    property OnOnAlarmSaved: TOnAlarmSaved read FOnOnAlarmSaved write FOnOnAlarmSaved;
  end;
  
implementation
{$R *.DFM}
uses
  mmVirtualStringTree, QODef;

resourcestring
  rsGetValidAlarmNameCaption = '(*)Erfassen'; //ivlm
  rsGetValidAlarmNamePrompt  = '(*)Bitte geben Sie einen eindeutigen Namen ein.'; //ivlm
  rsAlarmNotSaved            = '(*)Die Aenderungen konnten nicht gespeichert werden.'; //ivlm
  
//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmSettingsAlarmFrame.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;// TfrmSettingsAlarmFrame.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmSettingsAlarmFrame.Destroy;
begin
  inherited Destroy;
end;// TfrmSettingsAlarmFrame.Destroy cat:No category

//:-------------------------------------------------------------------
procedure TfrmSettingsAlarmFrame.acCallClassdesignerExecute(Sender: TObject);
var
  xIntf: OLEVariant;
begin
  try
    xIntf := CreateOLEObject(cClassDesignerServer);
    xIntf.Execute;
    xIntf := Unassigned;
    LoadDataItems;
    mDataItemTree.Refresh;
  except
    on e:Exception do begin
      CodeSite.SendError('TfrmSettingsAlarmFrame.acCallClassdesignerExecute: ' + e.Message);
    end;
  end;
end;// TfrmSettingsAlarmFrame.acCallClassdesignerExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acSaveExecute
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Speichert die ver�nderten Settings
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsAlarmFrame.acSaveExecute(Sender: TObject);
begin
  // Es wird nicht gefragt ob gespeichert werden soll
  SaveSettings(false, false);
end;// TfrmSettingsAlarmFrame.acSaveExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acUndoExecute
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: L�dt die Settings wieder vom urspr�nglichen Alarm
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsAlarmFrame.acUndoExecute(Sender: TObject);
var
  i: Integer;
begin
  // evt. gel�schte DataItems wieder herstellen
  for i := 0 to FQOAlarm.QOItems.ItemCount - 1 do
    if FQOAlarm.QOItems[i].DeleteState = dsPending then
      FQOAlarm.QOItems[i].DeleteState := dsValid;
  
  ShowAlarm(FQOAlarm, mSettingsPage.ActivePage);
end;// TfrmSettingsAlarmFrame.acUndoExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           bAddItemClick
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: f�gt die selektierten Data Items zum alarm hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsAlarmFrame.bAddItemClick(Sender: TObject);
begin
  DoOnAddItems(mDataItemTree.TreeView);
end;// TfrmSettingsAlarmFrame.bAddItemClick cat:No category

//:-------------------------------------------------------------------
(*: Member:           ChangeFromGUI
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Erfasst ob �nderungen an den Settings gemacht wurden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsAlarmFrame.ChangeFromGUI(Sender: TObject);
var
  xSettingsChanged: Boolean;
begin
  if assigned(QOAlarm) then begin
    xSettingsChanged := SettingsChanged;
  
    acSave.Enabled := (xSettingsChanged or (QOAlarm.ID = cINVALID_ID)) and FSaveEnabled;
    acSave.Visible := FSaveVisible;
  
    acUndo.Enabled := xSettingsChanged or (QOAlarm.ID = cINVALID_ID);
  
  (*  EnableActionFromSecurity(acSave, mSecurityControl, xSettingsChanged or (QOAlarm.ID = cINVALID_ID));
    EnableActionFromSecurity(acUndo, mSecurityControl, xSettingsChanged or (QOAlarm.ID = cINVALID_ID));*)
  end;// if assigned(QOAlarm) then begin
end;// TfrmSettingsAlarmFrame.ChangeFromGUI cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoAlarmSaved
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aAlarm)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn ein Alarm gesichert wurde
 *  Beschreibung:     
                      Die Anwendung kann hier die visuielle Darstellung aktualisieren.
 --------------------------------------------------------------------*)
procedure TfrmSettingsAlarmFrame.DoAlarmSaved(Sender: TfrmSettingsAlarmFrame; aAlarm: TQOAlarm);
begin
  if Assigned(FOnOnAlarmSaved) then FOnOnAlarmSaved(Sender, aAlarm);
end;// TfrmSettingsAlarmFrame.DoAlarmSaved cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoOnAddItems
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Ruft den Ereignishandler auf, wenn ein DAtaItem zum Alarm hinzugef�gt werden soll
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsAlarmFrame.DoOnAddItems(Sender: TBaseVirtualTree);
begin
  if Assigned(FOnAddItems) then FOnAddItems(Sender);
end;// TfrmSettingsAlarmFrame.DoOnAddItems cat:No category

//:-------------------------------------------------------------------
(*: Member:           edNameChange
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: L�ngenbegranzung des Names des Alarms
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsAlarmFrame.edNameChange(Sender: TObject);
begin
  if Length(edName.Text) > cAlarmNameLength then begin
    edName.Text := copy(edName.Text, 1, cAlarmNameLength);
    ChangeFromGUI(sender);
    raise EQOLengthError.Create(cAlarmNameLength);
  end;// if Length(edName.Text) > cAlarmNameLength then begin
  
  ChangeFromGUI(sender);
end;// TfrmSettingsAlarmFrame.edNameChange cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAdviceMethods
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die selektierten Benachrichtigungs Methoden als Set zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmSettingsAlarmFrame.GetAdviceMethods: TAdviceMethodSet;
begin
  result := [];
  
  if cbMonitor.Checked then include(result, amMonitor);
  if cbPrinter.Checked then include(result, amPrinter);
  if cbPager.Checked   then include(result, amPager);
  if cbEMail.Checked   then include(result, amEMail);
  if cbSMS.Checked     then include(result, amSMS);
end;// TfrmSettingsAlarmFrame.GetAdviceMethods cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetImages
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die ImageListe des DataItemTree zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmSettingsAlarmFrame.GetImages: TImageList;
begin
  result := mDataItemTree.mTreeImages;
end;// TfrmSettingsAlarmFrame.GetImages cat:No category

//:-------------------------------------------------------------------
(*: Member:           mDataItemTreeDblClick
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: F�gt das DataItem auf das doppelt geklickt wurde zum Alarm hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsAlarmFrame.mDataItemTreeDblClick(Sender: TObject);
begin
  DoOnAddItems(mDataItemTree.TreeView);
end;// TfrmSettingsAlarmFrame.mDataItemTreeDblClick cat:No category

//:-------------------------------------------------------------------
(*: Member:           mSettingsPageChange
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: wird aufgerufen, wenn das aktive Page �ndert
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsAlarmFrame.mSettingsPageChange(Sender: TObject);
begin
  if mSettingsPage.ActivePage = tabItems then begin
    mDataItemTree.ClassDefList := gQOItems;
    mDataItemTree.ExpandedGroups := [ctDataItem];
  end;// if mSettingsPage.ActivePage = tabItems then begin

//: ----------------------------------------------
  if mSettingsPage.ActivePage = tabSettings then begin
  end;
end;// TfrmSettingsAlarmFrame.mSettingsPageChange cat:No category

//:-------------------------------------------------------------------
(*: Member:           mTranslatorAfterTranslate
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (translator)
 *
 *  Kurzbeschreibung: �bernimmt alle �bersetungen die nicht automatisch "passieren"
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsAlarmFrame.mTranslatorAfterTranslate(translator: TIvCustomTranslator);
begin
  mInfoMemo.Text := rsNoStyleAssigned;
end;// TfrmSettingsAlarmFrame.mTranslatorAfterTranslate cat:No category

//:-------------------------------------------------------------------
(*: Member:           saved
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt True zur�ck, wenn das aktuelle Objekt auf der gespeichert ist
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmSettingsAlarmFrame.saved: Boolean;
begin
  result := not(acSave.Enabled);
end;// TfrmSettingsAlarmFrame.saved cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveSettings
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (aAskForPermission, aShowCancel)
 *
 *  Kurzbeschreibung: Speichert die Einstellungen im AlarmObjekt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmSettingsAlarmFrame.SaveSettings(aAskForPermission: boolean; aShowCancel: boolean): Word;
var
  xAllow: Boolean;
  xButtons: TMsgDlgButtons;
  xAlarmName: string;
begin
  result := mrOK;
  
  if aShowCancel then
    xButtons := [mbYes, mbNo, mbCancel]
  else
    xButtons := [mbYes, mbNo];
  
  
  // Nichts unternehmen, wenn kein Alarm zugeordnet ist
  if not(assigned(QOAlarm)) then
    exit;
  
  // Zuerst anhand der Security feststellen ob gespeichert werden darf
  xAllow := acSave.Visible and acSave.Enabled;
  
  // Nur weitermachen, wenn die Settings ge�ndert haben
  xAllow := xAllow and (SettingsChanged or (QOAlarm.ID = cINVALID_ID));
  
  // evt. Abfragen ob gespeichert werden soll
  if xAllow and aAskForPermission then begin
    result := mmMessageDlg(Format(sAskForSave,[QOAlarm.QOVisualize.Caption]), mtConfirmation, xButtons, 0, self);
    xAllow := (result = mrYes);
  end;// if xAllow and aAskForPermission then begin
  
  // Wenn die Settings ge�ndert haben, dann wird gespeichert
  if xAllow then begin
    xAlarmName := edName.Text;
    if (QOAlarm.QOVisualize.Caption <> xAlarmName) and (TQOAlarms.Instance.IndexOf(edName.Text) >= 0) then begin
      repeat
        xAllow := InputQuery(rsGetValidAlarmNameCaption, rsGetValidAlarmNamePrompt, xAlarmName);
      until ((TQOAlarms.Instance.IndexOf(xAlarmName) = -1) or (QOAlarm.QOVisualize.Caption = xAlarmName)) or not(xAllow);
    end;// if (QOAlarm.QOVisualize.Caption <> edName.Text) and (TQOAlarms.Instance.IndexOf(edName.Text) >= 0) then begin
  
    if xAllow then begin
      // Settings des Alarms
      QOAlarm.BoolOpAnd := (rgBoolOp.ItemIndex = 0);
      QOAlarm.QOVisualize.Caption := xAlarmName;
  
      QOAlarm.Active := cbActive.Checked;
  
      QOAlarm.AdviceMethods := GetAdviceMethods;
      QOAlarm.SavingUser := CurrentUser;
  
      // Settings wurden ge�ndert
      result := mrOK;
  
      // Auf der Datenbank speichern
      QOAlarm.SaveDefToDB;
  
      (* Die Daten neu anzeigen, da die DataItems Kopien sind
         und der Vergleich darum nicht klappen w�rde *)
      ShowAlarm(QOAlarm, mSettingsPage.ActivePage);
      // Buttons disablen
      ChangeFromGUI(nil);
  
      // Anwendung informieren
      DoAlarmSaved(self, QOAlarm);
    end else begin
      result := mrNo;
      acUndo.Execute;
      mmMessageDlg(rsAlarmNotSaved, mtInformation, [mbOK], 0, self);
    end;// if xAllow then begin
  end else begin
    if result = mrNo then
      acUndo.Execute;
  end;// if xAllow then begin
end;// TfrmSettingsAlarmFrame.SaveSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetCurrentUser
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r CurrentUser
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsAlarmFrame.SetCurrentUser(const Value: String);
begin
  FCurrentUser := Value;

//: ----------------------------------------------
  if FCurrentUser = '' then
    FCurrentUser := '-';
end;// TfrmSettingsAlarmFrame.SetCurrentUser cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetQOAlarm
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r QOAlarm
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsAlarmFrame.SetQOAlarm(Value: TQOAlarm);
begin
  FQOAlarm := Value;

//: ----------------------------------------------
  visible := FQOAlarm <> nil;
end;// TfrmSettingsAlarmFrame.SetQOAlarm cat:No category

//:-------------------------------------------------------------------
(*: Member:           SettingsChanged
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: True, wenn die Settings noch denen des �bergebenen Alarms entsprechen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmSettingsAlarmFrame.SettingsChanged: Boolean;
var
  xBoolOpAnd: Boolean;
begin
  result := false;
  
  // Nichts unternehmen, wenn kein Alarm zugeordnet ist
  if not(assigned(QOAlarm)) then
    exit;
  
  // Verkn�pfung der DataItems
  xBoolOpAnd := (rgBoolOp.ItemIndex = 0);
  
  // Alle Erkenntnisse zusammenziehen
  result := (
                 // Verkn�pfung der DataItems
                 (QOAlarm.BoolOpAnd <> xBoolOpAnd)
                 // Aktiv - Inaktiv
              or (QOAlarm.Active <> cbActive.Checked)
                 // Name
              or (QOAlarm.QOVisualize.Caption <> edName.Text)
                 // Benachrichtigungsmethode
              or (QOAlarm.AdviceMethods <> GetAdviceMethods)
             );
end;// TfrmSettingsAlarmFrame.SettingsChanged cat:No category

//:-------------------------------------------------------------------
(*: Member:           ShowAlarm
 *  Klasse:           TfrmSettingsAlarmFrame
 *  Kategorie:        No category 
 *  Argumente:        (aQOAlarm, aActivePage)
 *
 *  Kurzbeschreibung: Zeigt die Einstellungen des �bergebenen Alarms an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsAlarmFrame.ShowAlarm(aQOAlarm: TObject; aActivePage: TTabSheet = nil);
begin
  if aQOAlarm is TQOAlarm then begin
    FQOAlarm := TQOAlarm(aQOAlarm);

//: ----------------------------------------------
    // Alarm
    if QOAlarm.BoolOpAnd then
      rgBoolOp.ItemIndex := 0
    else
      rgBoolOp.ItemIndex := 1;
  
    cbActive.Checked := QOAlarm.Active;
  
    edName.Text := QOAlarm.QOVisualize.Caption;
  
    cbMonitor.Checked := amMonitor in QOAlarm.AdviceMethods;
    cbPrinter.Checked := amPrinter in QOAlarm.AdviceMethods;
    cbPager.Checked   := amPager in QOAlarm.AdviceMethods;
    cbEMail.Checked   := amEMail in QOAlarm.AdviceMethods;
    cbSMS.Checked     := amSMS in QOAlarm.AdviceMethods;
  
    if QOAlarm.SaveDate > 1 then
      laLastSave.Caption := DateTimeToStr(QOAlarm.SaveDate) + ' (' + QOAlarm.SavingUser + ')'
    else
      laLastSave.Caption := '-';
  
    // Frame
    if assigned(aActivePage) then
      mSettingsPage.ActivePage := aActivePage;
  
    mInfoPanel.Visible := QOAlarm.QOAssigns.AssignCount = 0;
    visible := true;
  
  end;// if aQOAlarm is TQOAlarm then begin
end;// TfrmSettingsAlarmFrame.ShowAlarm cat:No category

end.




































