if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_t_QO_involved_dataitem_t_QO_Alarm_Data]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[t_QO_involved_dataitem] DROP CONSTRAINT FK_t_QO_involved_dataitem_t_QO_Alarm_Data
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_t_QO_Involved_Lot_t_QO_Alarm_Data]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[t_QO_Involved_Lot] DROP CONSTRAINT FK_t_QO_Involved_Lot_t_QO_Alarm_Data
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_t_QO_Involved_Machine_t_QO_Alarm_Data]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[t_QO_Involved_Machine] DROP CONSTRAINT FK_t_QO_Involved_Machine_t_QO_Alarm_Data
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_t_QO_Involved_Style_t_QO_Alarm_Data]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[t_QO_Involved_Style] DROP CONSTRAINT FK_t_QO_Involved_Style_t_QO_Alarm_Data
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_t_QO_Alarm_Data_t_QO_Alarm_Setting]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[t_QO_Alarm_Data] DROP CONSTRAINT FK_t_QO_Alarm_Data_t_QO_Alarm_Setting
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_t_QO_Item_Setting_t_QO_Alarm_Setting]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[t_QO_Item_Setting] DROP CONSTRAINT FK_t_QO_Item_Setting_t_QO_Alarm_Setting
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_t_QO_Style_Setting_t_QO_Alarm_Setting]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[t_QO_Style_Setting] DROP CONSTRAINT FK_t_QO_Style_Setting_t_QO_Alarm_Setting
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_t_QO_Alarm_Data_t_QO_Caught_Alarm]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[t_QO_Alarm_Data] DROP CONSTRAINT FK_t_QO_Alarm_Data_t_QO_Caught_Alarm
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_t_QO_Method_Setting_t_QO_Item_Setting]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[t_QO_Method_Setting] DROP CONSTRAINT FK_t_QO_Method_Setting_t_QO_Item_Setting
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_t_QO_Machine_Setting_t_QO_Style_Setting]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[t_QO_Machine_Setting] DROP CONSTRAINT FK_t_QO_Machine_Setting_t_QO_Style_Setting
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_t_QO_involved_method_t_QO_involved_dataitem]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[t_QO_involved_method] DROP CONSTRAINT FK_t_QO_involved_method_t_QO_involved_dataitem
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_Alarm_Data]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_Alarm_Data]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_Alarm_Setting]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_Alarm_Setting]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_Caught_Alarm]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_Caught_Alarm]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_Involved_Lot]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_Involved_Lot]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_Involved_Machine]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_Involved_Machine]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_Involved_Style]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_Involved_Style]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_Item_Setting]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_Item_Setting]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_Machine_Setting]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_Machine_Setting]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_Method_Setting]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_Method_Setting]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_Style_Setting]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_Style_Setting]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_involved_dataitem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_involved_dataitem]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_involved_method]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_involved_method]
GO

CREATE TABLE [dbo].[t_QO_Alarm_Data] (
	[c_AdviceMethods] [smallint] NULL ,
	[c_SaveDate] [datetime] NULL ,
	[c_SavingUser] [varchar] (50) COLLATE Compatibility_52_409_30003 NULL ,
	[c_QO_alarm_data_id] [int] NOT NULL ,
	[c_caught_alarm_id] [int] NULL ,
	[c_QOAlarm_id] [int] NULL ,
	[c_QO_Alarm_Name] [varchar] (50) COLLATE Compatibility_52_409_30003 NULL ,
	[c_alarm_settings] [text] COLLATE Compatibility_52_409_30003 NULL ,
	[c_alarm_data] [text] COLLATE Compatibility_52_409_30003 NULL ,
	[c_BoolOpAnd] [smallint] NULL ,
	[c_info] [text] COLLATE Compatibility_52_409_30003 NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[t_QO_Alarm_Setting] (
	[c_QOAlarm_id] [int] NOT NULL ,
	[c_Name] [varchar] (50) COLLATE Compatibility_52_409_30003 NULL ,
	[c_BoolOpAnd] [smallint] NULL ,
	[c_AdviceMethods] [smallint] NULL ,
	[c_Active] [smallint] NULL ,
	[c_SaveDate] [datetime] NULL ,
	[c_SavingUser] [varchar] (50) COLLATE Compatibility_52_409_30003 NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[t_QO_Caught_Alarm] (
	[c_caught_alarm_id] [int] NOT NULL ,
	[c_shift_id] [int] NOT NULL ,
	[c_shiftcal_id] [tinyint] NOT NULL ,
	[c_date] [datetime] NULL ,
	[c_Persistent] [smallint] NULL ,
	[c_monitored_DataItems] [varchar] (2000) COLLATE Compatibility_52_409_30003 NULL ,
	[c_style_data] [text] COLLATE Compatibility_52_409_30003 NULL ,
	[c_info] [text] COLLATE Compatibility_52_409_30003 NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[t_QO_Involved_Lot] (
	[c_QO_Alarm_Data_id] [int] NOT NULL ,
	[c_prod_id] [int] NOT NULL ,
	[c_prod_name] [varchar] (50) COLLATE Compatibility_52_409_30003 NULL ,
	[c_Mach_id] [int] NULL ,
	[c_first_spd] [int] NULL ,
	[c_last_spd] [int] NULL ,
	[c_style_id] [int] NULL ,
	[c_shift_id] [int] NULL ,
	[c_shift_count] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[t_QO_Involved_Machine] (
	[c_QO_Alarm_data_id] [int] NOT NULL ,
	[c_machine_id] [smallint] NOT NULL ,
	[c_machine_name] [varchar] (50) COLLATE Compatibility_52_409_30003 NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[t_QO_Involved_Style] (
	[c_QO_Alarm_Data_id] [int] NOT NULL ,
	[c_style_id] [smallint] NOT NULL ,
	[c_style_name] [varchar] (50) COLLATE Compatibility_52_409_30003 NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[t_QO_Item_Setting] (
	[c_QOItem_id] [int] NOT NULL ,
	[c_QOAlarm_id] [int] NOT NULL ,
	[c_Name] [varchar] (50) COLLATE Compatibility_52_409_30003 NULL ,
	[c_class_defect] [int] NOT NULL ,
	[c_BoolOpAnd] [smallint] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[t_QO_Machine_Setting] (
	[c_QOMachine_id] [int] NOT NULL ,
	[c_QOStyle_id] [int] NULL ,
	[c_Machine_id] [smallint] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[t_QO_Method_Setting] (
	[c_QOMethod_id] [int] NOT NULL ,
	[c_QOItem_id] [int] NOT NULL ,
	[c_Type] [smallint] NULL ,
	[c_Settings] [text] COLLATE Compatibility_52_409_30003 NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[t_QO_Style_Setting] (
	[c_QOStyle_id] [int] NOT NULL ,
	[c_QOAlarm_id] [int] NULL ,
	[c_Style_id] [smallint] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[t_QO_involved_dataitem] (
	[c_involved_dataitem_id] [int] NOT NULL ,
	[c_QO_alarm_data_id] [int] NULL ,
	[c_dataitem_name] [varchar] (50) COLLATE Compatibility_52_409_30003 NULL ,
	[c_BoolOpAnd] [smallint] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[t_QO_involved_method] (
	[c_involved_dataitem_id] [int] NULL ,
	[c_method_type] [int] NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[t_QO_Alarm_Data] WITH NOCHECK ADD 
	CONSTRAINT [PK_t_QO_Alarm_Data] PRIMARY KEY  CLUSTERED 
	(
		[c_QO_alarm_data_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[t_QO_Alarm_Setting] WITH NOCHECK ADD 
	CONSTRAINT [PK_t_QO_Alarm_Setting] PRIMARY KEY  CLUSTERED 
	(
		[c_QOAlarm_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[t_QO_Caught_Alarm] WITH NOCHECK ADD 
	CONSTRAINT [PK_t_QO_Caught_Alarm] PRIMARY KEY  CLUSTERED 
	(
		[c_caught_alarm_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[t_QO_Involved_Lot] WITH NOCHECK ADD 
	CONSTRAINT [PK_t_QO_Involved_Lot] PRIMARY KEY  CLUSTERED 
	(
		[c_QO_Alarm_Data_id],
		[c_prod_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[t_QO_Involved_Machine] WITH NOCHECK ADD 
	CONSTRAINT [PK_t_QO_Involved_Machine] PRIMARY KEY  CLUSTERED 
	(
		[c_QO_Alarm_data_id],
		[c_machine_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[t_QO_Involved_Style] WITH NOCHECK ADD 
	CONSTRAINT [PK_t_QO_Involved_Style] PRIMARY KEY  CLUSTERED 
	(
		[c_QO_Alarm_Data_id],
		[c_style_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[t_QO_Item_Setting] WITH NOCHECK ADD 
	CONSTRAINT [PK_t_QO_Item_Setting] PRIMARY KEY  CLUSTERED 
	(
		[c_QOItem_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[t_QO_Machine_Setting] WITH NOCHECK ADD 
	CONSTRAINT [PK_t_QO_Machine_Setting] PRIMARY KEY  CLUSTERED 
	(
		[c_QOMachine_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[t_QO_Method_Setting] WITH NOCHECK ADD 
	CONSTRAINT [PK_t_QO_Method_Setting] PRIMARY KEY  CLUSTERED 
	(
		[c_QOMethod_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[t_QO_Style_Setting] WITH NOCHECK ADD 
	CONSTRAINT [PK_t_QO_Style_Setting] PRIMARY KEY  CLUSTERED 
	(
		[c_QOStyle_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[t_QO_involved_dataitem] WITH NOCHECK ADD 
	CONSTRAINT [PK_t_QO_involved_dataitem] PRIMARY KEY  CLUSTERED 
	(
		[c_involved_dataitem_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[t_QO_Alarm_Data] ADD 
	CONSTRAINT [FK_t_QO_Alarm_Data_t_QO_Alarm_Setting] FOREIGN KEY 
	(
		[c_QOAlarm_id]
	) REFERENCES [dbo].[t_QO_Alarm_Setting] (
		[c_QOAlarm_id]
	),
	CONSTRAINT [FK_t_QO_Alarm_Data_t_QO_Caught_Alarm] FOREIGN KEY 
	(
		[c_caught_alarm_id]
	) REFERENCES [dbo].[t_QO_Caught_Alarm] (
		[c_caught_alarm_id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

alter table [dbo].[t_QO_Alarm_Data] nocheck constraint [FK_t_QO_Alarm_Data_t_QO_Alarm_Setting]
GO

ALTER TABLE [dbo].[t_QO_Involved_Lot] ADD 
	CONSTRAINT [FK_t_QO_Involved_Lot_t_QO_Alarm_Data] FOREIGN KEY 
	(
		[c_QO_Alarm_Data_id]
	) REFERENCES [dbo].[t_QO_Alarm_Data] (
		[c_QO_alarm_data_id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[t_QO_Involved_Machine] ADD 
	CONSTRAINT [FK_t_QO_Involved_Machine_t_machine] FOREIGN KEY 
	(
		[c_machine_id]
	) REFERENCES [dbo].[t_machine] (
		[c_machine_id]
	) ON DELETE CASCADE  ON UPDATE CASCADE ,
	CONSTRAINT [FK_t_QO_Involved_Machine_t_QO_Alarm_Data] FOREIGN KEY 
	(
		[c_QO_Alarm_data_id]
	) REFERENCES [dbo].[t_QO_Alarm_Data] (
		[c_QO_alarm_data_id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[t_QO_Involved_Style] ADD 
	CONSTRAINT [FK_t_QO_Involved_Style_t_QO_Alarm_Data] FOREIGN KEY 
	(
		[c_QO_Alarm_Data_id]
	) REFERENCES [dbo].[t_QO_Alarm_Data] (
		[c_QO_alarm_data_id]
	) ON DELETE CASCADE  ON UPDATE CASCADE ,
	CONSTRAINT [FK_t_QO_Involved_Style_t_style] FOREIGN KEY 
	(
		[c_style_id]
	) REFERENCES [dbo].[t_style] (
		[c_style_id]
	)
GO

alter table [dbo].[t_QO_Involved_Style] nocheck constraint [FK_t_QO_Involved_Style_t_style]
GO

ALTER TABLE [dbo].[t_QO_Item_Setting] ADD 
	CONSTRAINT [FK_t_QO_Item_Setting_t_QO_Alarm_Setting] FOREIGN KEY 
	(
		[c_QOAlarm_id]
	) REFERENCES [dbo].[t_QO_Alarm_Setting] (
		[c_QOAlarm_id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[t_QO_Machine_Setting] ADD 
	CONSTRAINT [FK_t_QO_Machine_Setting_t_machine] FOREIGN KEY 
	(
		[c_Machine_id]
	) REFERENCES [dbo].[t_machine] (
		[c_machine_id]
	),
	CONSTRAINT [FK_t_QO_Machine_Setting_t_QO_Style_Setting] FOREIGN KEY 
	(
		[c_QOStyle_id]
	) REFERENCES [dbo].[t_QO_Style_Setting] (
		[c_QOStyle_id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

alter table [dbo].[t_QO_Machine_Setting] nocheck constraint [FK_t_QO_Machine_Setting_t_machine]
GO

ALTER TABLE [dbo].[t_QO_Method_Setting] ADD 
	CONSTRAINT [FK_t_QO_Method_Setting_t_QO_Item_Setting] FOREIGN KEY 
	(
		[c_QOItem_id]
	) REFERENCES [dbo].[t_QO_Item_Setting] (
		[c_QOItem_id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[t_QO_Style_Setting] ADD 
	CONSTRAINT [FK_t_QO_Style_Setting_t_QO_Alarm_Setting] FOREIGN KEY 
	(
		[c_QOAlarm_id]
	) REFERENCES [dbo].[t_QO_Alarm_Setting] (
		[c_QOAlarm_id]
	) ON DELETE CASCADE  ON UPDATE CASCADE ,
	CONSTRAINT [FK_t_QO_Style_Setting_t_style] FOREIGN KEY 
	(
		[c_Style_id]
	) REFERENCES [dbo].[t_style] (
		[c_style_id]
	)
GO

alter table [dbo].[t_QO_Style_Setting] nocheck constraint [FK_t_QO_Style_Setting_t_style]
GO

ALTER TABLE [dbo].[t_QO_involved_dataitem] ADD 
	CONSTRAINT [FK_t_QO_involved_dataitem_t_QO_Alarm_Data] FOREIGN KEY 
	(
		[c_QO_alarm_data_id]
	) REFERENCES [dbo].[t_QO_Alarm_Data] (
		[c_QO_alarm_data_id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[t_QO_involved_method] ADD 
	CONSTRAINT [FK_t_QO_involved_method_t_QO_involved_dataitem] FOREIGN KEY 
	(
		[c_involved_dataitem_id]
	) REFERENCES [dbo].[t_QO_involved_dataitem] (
		[c_involved_dataitem_id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

