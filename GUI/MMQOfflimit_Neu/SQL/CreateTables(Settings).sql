if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_t_QO_Alarm_Data_t_QO_Alarm_Setting]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[t_QO_Alarm_Data] DROP CONSTRAINT FK_t_QO_Alarm_Data_t_QO_Alarm_Setting
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_t_QO_Item_Setting_t_QO_Alarm_Setting]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[t_QO_Item_Setting] DROP CONSTRAINT FK_t_QO_Item_Setting_t_QO_Alarm_Setting
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_t_QO_Style_Setting_t_QO_Alarm_Setting]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[t_QO_Style_Setting] DROP CONSTRAINT FK_t_QO_Style_Setting_t_QO_Alarm_Setting
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_t_QO_Method_Setting_t_QO_Item_Setting]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[t_QO_Method_Setting] DROP CONSTRAINT FK_t_QO_Method_Setting_t_QO_Item_Setting
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_t_QO_Machine_Setting_t_QO_Style_Setting]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[t_QO_Machine_Setting] DROP CONSTRAINT FK_t_QO_Machine_Setting_t_QO_Style_Setting
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_Alarm_Setting]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_Alarm_Setting]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_Item_Setting]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_Item_Setting]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_Machine_Setting]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_Machine_Setting]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_Method_Setting]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_Method_Setting]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[t_QO_Style_Setting]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[t_QO_Style_Setting]
GO

CREATE TABLE [dbo].[t_QO_Alarm_Setting] (
	[c_QOAlarm_id] [int] NOT NULL ,
	[c_Name] [varchar] (50) COLLATE Compatibility_52_409_30003 NULL ,
	[c_BoolOpAnd] [smallint] NULL ,
	[c_AdviceMethods] [smallint] NULL ,
	[c_Active] [smallint] NULL ,
	[c_SaveDate] [datetime] NULL ,
	[c_SavingUser] [varchar] (50) COLLATE Compatibility_52_409_30003 NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[t_QO_Item_Setting] (
	[c_QOItem_id] [int] NOT NULL ,
	[c_QOAlarm_id] [int] NOT NULL ,
	[c_Name] [varchar] (50) COLLATE Compatibility_52_409_30003 NULL ,
	[c_class_defect] [int] NOT NULL ,
	[c_BoolOpAnd] [smallint] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[t_QO_Machine_Setting] (
	[c_QOMachine_id] [int] NOT NULL ,
	[c_QOStyle_id] [int] NULL ,
	[c_Machine_id] [smallint] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[t_QO_Method_Setting] (
	[c_QOMethod_id] [int] NOT NULL ,
	[c_QOItem_id] [int] NOT NULL ,
	[c_Type] [smallint] NULL ,
	[c_Settings] [text] COLLATE Compatibility_52_409_30003 NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[t_QO_Style_Setting] (
	[c_QOStyle_id] [int] NOT NULL ,
	[c_QOAlarm_id] [int] NULL ,
	[c_Style_id] [smallint] NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[t_QO_Alarm_Setting] WITH NOCHECK ADD 
	CONSTRAINT [PK_t_QO_Alarm_Setting] PRIMARY KEY  CLUSTERED 
	(
		[c_QOAlarm_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[t_QO_Item_Setting] WITH NOCHECK ADD 
	CONSTRAINT [PK_t_QO_Item_Setting] PRIMARY KEY  CLUSTERED 
	(
		[c_QOItem_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[t_QO_Machine_Setting] WITH NOCHECK ADD 
	CONSTRAINT [PK_t_QO_Machine_Setting] PRIMARY KEY  CLUSTERED 
	(
		[c_QOMachine_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[t_QO_Method_Setting] WITH NOCHECK ADD 
	CONSTRAINT [PK_t_QO_Method_Setting] PRIMARY KEY  CLUSTERED 
	(
		[c_QOMethod_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[t_QO_Style_Setting] WITH NOCHECK ADD 
	CONSTRAINT [PK_t_QO_Style_Setting] PRIMARY KEY  CLUSTERED 
	(
		[c_QOStyle_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[t_QO_Item_Setting] ADD 
	CONSTRAINT [FK_t_QO_Item_Setting_t_QO_Alarm_Setting] FOREIGN KEY 
	(
		[c_QOAlarm_id]
	) REFERENCES [dbo].[t_QO_Alarm_Setting] (
		[c_QOAlarm_id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[t_QO_Machine_Setting] ADD 
	CONSTRAINT [FK_t_QO_Machine_Setting_t_machine] FOREIGN KEY 
	(
		[c_Machine_id]
	) REFERENCES [dbo].[t_machine] (
		[c_machine_id]
	),
	CONSTRAINT [FK_t_QO_Machine_Setting_t_QO_Style_Setting] FOREIGN KEY 
	(
		[c_QOStyle_id]
	) REFERENCES [dbo].[t_QO_Style_Setting] (
		[c_QOStyle_id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

alter table [dbo].[t_QO_Machine_Setting] nocheck constraint [FK_t_QO_Machine_Setting_t_machine]
GO

ALTER TABLE [dbo].[t_QO_Method_Setting] ADD 
	CONSTRAINT [FK_t_QO_Method_Setting_t_QO_Item_Setting] FOREIGN KEY 
	(
		[c_QOItem_id]
	) REFERENCES [dbo].[t_QO_Item_Setting] (
		[c_QOItem_id]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[t_QO_Style_Setting] ADD 
	CONSTRAINT [FK_t_QO_Style_Setting_t_QO_Alarm_Setting] FOREIGN KEY 
	(
		[c_QOAlarm_id]
	) REFERENCES [dbo].[t_QO_Alarm_Setting] (
		[c_QOAlarm_id]
	) ON DELETE CASCADE  ON UPDATE CASCADE ,
	CONSTRAINT [FK_t_QO_Style_Setting_t_style] FOREIGN KEY 
	(
		[c_Style_id]
	) REFERENCES [dbo].[t_style] (
		[c_style_id]
	)
GO

alter table [dbo].[t_QO_Style_Setting] nocheck constraint [FK_t_QO_Style_Setting_t_style]
GO

