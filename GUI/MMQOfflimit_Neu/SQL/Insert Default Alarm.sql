/* Alarm */
INSERT INTO t_QO_Alarm_Setting(c_QOAlarm_id, c_Name, c_BoolOpAnd, c_AdviceMethods, c_Active, c_SaveDate, c_SavingUser)
VALUES     (1,'Loepfe Example',0,3,0,'2004-01-13 11:06:30.000','Loepfe')

/* Zugeordnete Artikel */
INSERT INTO t_QO_Style_Setting(c_QOStyle_id, c_QOAlarm_id, c_Style_id)
VALUES     (1,1,32767)

/* DataItems */
INSERT INTO t_QO_Item_Setting(c_QOItem_id, c_Name, c_QOAlarm_id, c_class_defect, c_BoolOpAnd)
VALUES     (1,'CYTot',1,0, 0)
INSERT INTO t_QO_Item_Setting(c_QOItem_id, c_Name, c_QOAlarm_id, c_class_defect, c_BoolOpAnd)
VALUES     (2,'CSIRO',1,0,1)

/* Methoden */
INSERT INTO t_QO_Method_Setting(c_QOMethod_id, c_QOItem_id, c_Type, c_Settings)
VALUES     (1,1,0,'<?xml version = "1.0"?>    <QOSettings>     <Method Type = "cmFix">        <MethodSettings Active = "True" ShowDetails = "msdFull" ShiftCount = "3" LengthUnit = "0" LenBase = "1" LimitValue = "10"/>     </Method>  </QOSettings>')

INSERT INTO t_QO_Method_Setting(c_QOMethod_id, c_QOItem_id, c_Type, c_Settings)
VALUES     (2,1,1,'<?xml version = "1.0"?>    <QOSettings>     <Method Type = "cmAvg">        <MethodSettings Active = "True" ShowDetails = "msdFull" ShiftCount = "3" LengthUnit = "0" LenBase = "1" Deviation = "10" MinProdYarnLength = "10000000" ShiftCompression = "3"/>     </Method>  </QOSettings>')

INSERT INTO t_QO_Method_Setting(c_QOMethod_id, c_QOItem_id, c_Type, c_Settings)
VALUES     (3,2,0,'<?xml version = "1.0"?>    <QOSettings>     <Method Type = "cmFix">        <MethodSettings Active = "True" ShowDetails = "msdFull" ShiftCount = "3" LengthUnit = "0" LenBase = "1" LimitValue = "5"/>     </Method>  </QOSettings>')

INSERT INTO t_QO_Method_Setting(c_QOMethod_id, c_QOItem_id, c_Type, c_Settings)
VALUES     (4,2,1,'<?xml version = "1.0"?>    <QOSettings>     <Method Type = "cmAvg">        <MethodSettings Active = "True" ShowDetails = "msdFull" ShiftCount = "3" LengthUnit = "0" LenBase = "1" Deviation = "10" MinProdYarnLength = "10000000" ShiftCompression = "2"/>     </Method>  </QOSettings>')

SELECT     c_QOAlarm_id, c_Name, c_BoolOpAnd, c_AdviceMethods, c_Active, c_SaveDate, c_SavingUser
FROM         t_QO_Alarm_Setting

SELECT     c_QOStyle_id, c_QOAlarm_id, c_Style_id
FROM         t_QO_Style_Setting

SELECT     c_QOItem_id, c_Name, c_QOAlarm_id, c_class_defect, c_BoolOpAnd
FROM         t_QO_Item_Setting

SELECT     c_QOMethod_id, c_QOItem_id, c_Type, c_Settings
FROM         t_QO_Method_Setting