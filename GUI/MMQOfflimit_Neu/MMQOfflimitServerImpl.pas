(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QOKernel.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Basisklassen f�r die Organisation der Alarme
|
| Info..........: 
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.04.2003  1.00  Lok | Datei erstellt
| 22.05.2004        Lok | Eintrag in die Alarmierungstabelle wenn mindestens 1 Alarm (Botto Alarm).
| 02.06.2005        Lok | Reihenfolge der Alarmierung ge�ndert (Print erst am Schluss, da sonst bei einem Fehler
|                       |   die Alarmierung am Bildschirm bis zu 3min dauern konnte.
| 08.09.2005        Lok | Benachrichtigung ins Eventlog wenn mindestens 1 Alarm eingetroffen ist
|                       |   Ausserdem ein Eintrag im Event Log, wenn eine Bildschirmbenacgrichtigung oder eine
|                       |   Printerbenachrichtigung stattfindet.
|=========================================================================================*)
unit MMQOfflimitServerImpl;

interface

uses
  ComObj, ActiveX, MMQOfflimit_TLB, StdVcl, LoepfePluginIMPL, LOEPFELIBRARY_TLB, Mailslot;

type
  TMMQOfflimitServer = class(TLoepfePlugin, IMMQOfflimitServer)
  private
    mBroadcaster: TBroadcaster;
  protected
    procedure Execute(const aParameter: WideString); safecall;
    // Aufruf erfolgt aus dem StorrageHandler jeweils am Schichtende
    procedure AnalyzeRecentShift(aSave: WordBool; out aAlarmCount: OleVariant);
      safecall;
  public
    // Aufruf erfolgt aus der Floor
    function ExecuteCommand: SYSINT; override;
    procedure Initialize; override;
  end;

implementation
{$R 'Toolbar.res'}
uses
  mmCS,
  Controls, ComServ, Forms, BaseForm, WrapperForm, MainWindow, IvDictio, QODef, QOKernel,
  sysutils, QOGUIShared, SettingsReader, BaseGlobal, windows, AdoDBAccess,
  MMEventLog;

type
  TCommandID = (ciQOSettings, ciQOAssign, ciQOAnalyze);

const
  cFloorMenuDef: Array[0..3] of TFloorMenuRec = (
    // --- Main Menu File ---
    (CommandID: 0; SubMenu: -1; MenuBmpName: '';
     MenuText: '(*)Q-Offlimit'; MenuHint: '(*)Q-Offlimit'; MenuStBar:'(*)Q-Offlimit'; //ivlm
     ToolbarText: '';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: Ord(ciQOSettings); SubMenu: 0; MenuBmpName: 'QOSETTINGS';
     MenuText: '(*)Erfassen'; MenuHint:'(*)Q-Offlimit Einstellungen'; MenuStBar: '(*)Q-Offlimit Einstellungen'; //ivlm
     ToolbarText: '(*)Erfassen';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: Ord(ciQOAssign); SubMenu: 0; MenuBmpName: 'QOSETTINGS';
     MenuText: '(*)Zuweisen'; MenuHint:'(*)Zuweisen eines Artikels zu einem Alarm'; MenuStBar: '(*)Artikel zuweisen'; //ivlm
     ToolbarText: '(*)Zuweisen';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: Ord(ciQOAnalyze); SubMenu: 0; MenuBmpName: 'QOSETTINGS';
     MenuText: '(14)Auswerten'; MenuHint:'(*)Q-Offlimit Auswertung'; MenuStBar: '(*)Q-Offlimit Auswertung'; //ivlm
     ToolbarText: '(14)Auswerten';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll)
  );

{ TMMQOfflimitServer }

(*---------------------------------------------------------
  Aufruf erfolgt aus der Floor
----------------------------------------------------------*)
function TMMQOfflimitServer.ExecuteCommand: SYSINT;
var
  xForm: TfrmMainWindow;
  xWrap: TmmForm;
begin
  Result := 0;

  if GetWrappedDataForm(TfrmMainWindow, TForm(xForm), xWrap) then begin

    case TCommandID(CommandIndex) of
      ciQOSettings: begin
        xForm.SetActiveUseCase(ucSettings);
        xForm.Show;
      end;// ciQOSettings
      ciQOAssign: begin
        xForm.SetActiveUseCase(ucAssign);
        xForm.Show;
      end;// ciQOAssign
      ciQOAnalyze: begin
        xForm.SetActiveUseCase(ucAnalyze);
        xForm.Show;
      end;// ciQOAnalyze
    else
    end;// case TCommandID(CommandIndex) of
    Result := xWrap.Handle;
  end;// if GetWrappedDataForm(TfrmMainWindow, TForm(xForm), xWrap) then begin
end;// function TMMQOfflimitServer.ExecuteCommand: SYSINT;


procedure TMMQOfflimitServer.Initialize;
begin
  inherited Initialize;
  InitMenu(cFloorMenuDef);
end;// procedure TMMQOfflimitServer.Initialize;

procedure TMMQOfflimitServer.Execute(const aParameter: WideString);
var
  xForm: TfrmMainWindow;
  xWrap: TmmForm;
begin
  if GetWrappedDataForm(TfrmMainWindow, TForm(xForm), xWrap) then begin
    xForm.Execute(aParameter);
  end;
end;// procedure TMMQOfflimitServer.Execute(const aParameter: WideString);

(*---------------------------------------------------------
  Aufruf erfolgt aus dem StorrageHandler jeweils am Schichtende
----------------------------------------------------------*)
procedure TMMQOfflimitServer.AnalyzeRecentShift(aSave: WordBool; out aAlarmCount: OleVariant);
var
  xMsg: TMMClientRec;
const
{  UPDATE t_MMAlerter SET c_ActualError = :aAlarmCount ' +
                      ' WHERE c_Relais_Nr = (SELECT Data ' +
                                            ' FROM t_MMUParm  ' +
                                            ' WHERE AppName = ''MMAlerter'' AND AppKey = ''QOfflimitRelais'')';
  cTableExists  = '               select * from dbo.sysobjects where id = object_id(N''[dbo].[t_MMAlerter]'') and OBJECTPROPERTY(id, N''IsUserTable'') = 1 ';}
  cUpdateAlarmTable = 'if exists (SELECT * FROM dbo.sysobjects where id = object_id(N''[dbo].[t_MMAlerter]'') AND OBJECTPROPERTY(id, N''IsUserTable'') = 1) ' +
                      '           UPDATE t_MMAlerter ' +
                      '           SET c_ActualError = :aAlarmCount ' +
                      '           WHERE c_Relais_Nr = (SELECT Data FROM t_MMUParm WHERE AppName = ''MMAlerter'' AND AppKey = ''QOfflimitRelais'')';
begin
  // Initialisiert die Umgebung (DataItems, Artikelliste, ...)
  if not(InitQOEnviroment) then
    raise Exception.Create(Translate(rsQOInitFailed));

  // Settings von der DB laden
  TQOAlarms.Instance.LoadDefFromDB;

  // Alarme berechnen
  TQOAlarms.Instance.Evaluate(aSave);
  // Anzahl der generierten Alarme zur�ckgeben
  aAlarmCount := TQOAlarms.Instance.OfflimitCount;

  // Benachrichtigen
  if aAlarmCount > 0 then begin
    WriteToEventLog('QOfflimit alarm(s) occured. Count: ' + IntToStr(aAlarmCount), '');
    // Wenn mindestens ein Alarm �ber den Bildschirm benachrichtigen soll ...
    if TQOAlarms.Instance.NeedToAdvice(amMonitor) then begin
      WriteToEventLog('QOfflimit broadcast to clients will be sent.', '');
      // Broadcast versenden (MMClient)
      mBroadcaster := TBroadcaster.Create(TMMSettingsReader.Instance.value[cDomainNames], cChannelNames[ttMMClientReader]);
      if not(mBroadcaster.Init) then
        raise Exception.Create('Init of Broadcaster failed.');

      FillChar(xMsg, sizeof(xMsg), 0);
      xMsg.MsgTyp := ccMMApplMsg;
      xMsg.ServerName := TMMSettingsReader.Instance.value[cMMHost];
      xMsg.ApplMsg.ApplMsgEvent := amQOfflimit;

      if not mBroadcaster.Write(@xMsg, sizeof(xMsg)) then
        raise Exception.Create('Broadcast.Write failed. ' + mBroadcaster.ErrorInformation);
    end;// if TQOAlarms.Instance.NeedToAdvice(amMonitor) then begin

    // Alarmierung mit Alarmlampe (Botto)
    with TAdoDBAccess.Create(1) do try
      Init;
      with Query[0] do begin
        SQL.Text := cUpdateAlarmTable;
        ParamByName('aAlarmCount').AsInteger := aAlarmCount;
        ExecSQL;
      end;// with Query[0] do begin
    finally
      Free;
    end;// with TAdoDBAccess.Create(1) do try

    // Wenn mindestens ein Alarm ausgedruckt werden muss ...
    if TQOAlarms.Instance.NeedToAdvice(amPrinter) then begin
      WriteToEventLog('QOfflimit alarm(s) will be printed on standad printer.', '');
      // ... automatischer Ausdruck auf dem Standard Drucker
      PrintRecentShift;
    end;//

  end;// if aAlarmCount > 0 then begin
end;// procedure TMMQOfflimitServer.AnalyzeRecentShift(aSave: WordBool;

initialization
  InitializeObjectFactory(TMMQOfflimitServer, Class_MMQOfflimitServer);
  
end.
