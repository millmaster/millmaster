(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QOMethods.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Einzelne Berechnungsmethoden
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.04.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit QOMethods;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, QOKernel, IcXMLParser, QODef, mmVCLStringList;


type
  (*: Klasse:        TQOAVG
      Vorg�nger:     TQOMethod
      Kategorie:     Kernel
      Kurzbeschrieb: Methode f�r die Mittelwertberechnung 
      Beschreibung:  
                     - *)
  TQOAVG = class(TQOMethod)
  private
    FMinProdYarnLength: Integer;
    FShiftCountAVG: Integer;
    FTolerance: Extended;
  protected
    //1 Vergleicht den Artikelwert mit der Berechnungsvorschrift 
    function GetIsInOfflimit(aValue: extended; aShiftToCalc, aShiftID, aStyleID: integer): Boolean; override;
    //1 Gibt die Anzahl der Schichten bekannt die f�r die Berechnung des Mittelwertes ben�tigt werden 
    function GetSampleShiftCount: Integer; override;
    //1 Wird von der Methode 'Evaluate' aufgerufen 
    procedure Internal_Evaluate; override;
  public
    //1 Konstruktor 
    constructor Create(aOwner: TQOItem); override;
    //1 Gibt den absoluten Grenzwert f�r die Bezeichnete Schicht und den bezeichneten Artikel zur�ck. 
    function CalculateLimitValue(aShiftNumber: integer; aShiftID: integer; aStyleID: integer): Extended; override;
    //1 Gibt zur�ck ob alle Parameter mit sinvollen Werten belegt sind 
    function MethodValid: Boolean; override;
    //1 Liest die Settings der Methode aus dem XML Stream 
    procedure ReadXMLMethodeSettings(aXMLItem: TIcXMLelement); override;
    //1 Gibt die Methoden Settings als XML formatierten String zur�ck 
    procedure WriteXMLMethodSettings(aXMLItem: TIcXMLElement); override;
    //1 Holt die Settings aller Methoden die vom Frame angezeigt werdenn. 
    function _GetMethodSettings(aSettings: TmmVCLStringList): String; override;
    //1 Minimal produzierte Garnl�nge ab Startdatum 
    property MinProdYarnLength: Integer read FMinProdYarnLength write FMinProdYarnLength;
    //1 Anzahl der produzierenden Schichten die f�r die Mittelwertbestimmung ber�cksichtigt werden 
    property ShiftCountAVG: Integer read FShiftCountAVG write FShiftCountAVG;
    //1 Erlaubte Abweichung zum Mittelwert in % 
    property Tolerance: Extended read FTolerance write FTolerance;
  end;
  
  (*: Klasse:        TQOFixBound
      Vorg�nger:     TQOMethod
      Kategorie:     Kernel
      Kurzbeschrieb: Methode f�r die Grenzwertberechnung 
      Beschreibung:  
                     - *)
  TQOFixBound = class(TQOMethod)
  private
    FLimitValue: Extended;
  public
    //1 Konstruktor 
    constructor Create(aOwner: TQOItem); override;
    //1 Gibt den berechneten, absoluten Grenzwert zur�ck 
    function CalculateLimitValue(aShiftNumber: integer; aShiftID: integer; aStyleID: integer): Extended; override;
    //1 Gibt zur�ck ob alle Parameter mit sinvollen Werten belegt sind 
    function MethodValid: Boolean; override;
    //1 Liest die Settings der Methode aus dem XML Stream 
    procedure ReadXMLMethodeSettings(aXMLItem: TIcXMLelement); override;
    //1 Gibt die Methoden Settings als XML formatierten String zur�ck 
    procedure WriteXMLMethodSettings(aXMLItem: TIcXMLElement); override;
    //1 Holt die Settings aller Methoden die vom Frame angezeigt werdenn. 
    function _GetMethodSettings(aSettings: TmmVCLStringList): String; override;
    //1 Grenzwert 
    property LimitValue: Extended read FLimitValue write FLimitValue;
  end;
  

const 
  cDefSchiftCountAVG = 5;
  
implementation

uses
  AdoDBAccess, ADODB_TLB, mmcs, QOValues, MMUGlobal, LoepfeGlobal, SettingsReader,
  CalcMethods;

resourcestring
  rsNoStyleData = '(*)Keine Daten fuer die Berechnung des Mittelwertes vorhanden (Artikel %d)'; // ivlm
  
//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOAVG
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOAVG.Create(aOwner: TQOItem);
begin
  inherited Create(aOwner);

//: ----------------------------------------------
  FMethodType := cmAVG;

//: ----------------------------------------------
  ShiftCountAVG := cDefSchiftCountAVG;

//: ----------------------------------------------
  FMinProdYarnLength := 0;
  // Einheiten bestimmen
  with TMMSettingsReader.Instance do begin
    // Initialisieren und Error wenn fehlgeschlagen
    if not(init) then
      raise ESettingsreaderInit.CreateQO('TQOAVG.Create');
    FMinProdYarnLength := Value[cMinStatisticLength];
  end;// with TMMSettingsReader.Instance do begin
end;// TQOAVG.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           CalculateLimitValue
 *  Klasse:           TQOAVG
 *  Kategorie:        No category 
 *  Argumente:        (aShiftNumber, aShiftID, aStyleID)
 *
 *  Kurzbeschreibung: Gibt den absoluten Grenzwert f�r die Bezeichnete Schicht und den bezeichneten Artikel zur�ck.
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAVG.CalculateLimitValue(aShiftNumber: integer; aShiftID: integer; aStyleID: integer): Extended;
var
  xLimitValues: TLimitValues;
  xLimitValueRec: TLimitValueRec;
begin
  (* Den Grenzwert nur neu berechnen, wenn aNew = true. Wenn f�r jede Schicht ein Grenzwert
     berechnet werden soll muss auch die Nummer der Schicht beachtet werden.
     Die ShiftID identifiziert die Schicht f�r die der Grenzwert berechnet werden soll. *)
  result := 0;
  
  xLimitValues := mLimitValueList.LimitValuesFromStyleID(aStyleID);
  
  if assigned(xLimitValues) then begin
    xLimitValueRec := xLimitValues.GetValue(aShiftNumber);
    if xLimitValueRec.Calculated then
      result := xLimitValueRec.Value;
  end;// if assigned(xLimitValues) then begin
  
  (*
  xStyleLimitIndex := mLimitValueList.IndexOfStyleID(aStyleID);
  if xStyleLimitIndex < 0 then
    raise Exception.CreateFMT(rsNoStyleData, [aStyleID]);
  
  xLimitValues := mLimitValueList.StyleLimits[xStyleLimitIndex];
  
  
  if (xLimitValues.LimitValues[aShiftNumber] < 0) then
    xLimitValues.GetMeanValue(aShiftID, aShiftNumber, LenBase);
  
  // berechneter Mittelwert
  result := xLimitValues.LimitValues[aShiftNumber];
  
  // relativen Wert berechnen
  result := result + (result * (Tolerance / 100));*)
end;// TQOAVG.CalculateLimitValue cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetIsInOfflimit
 *  Klasse:           TQOAVG
 *  Kategorie:        No category 
 *  Argumente:        (aValue, aShiftToCalc, aShiftID, aStyleID)
 *
 *  Kurzbeschreibung: Vergleicht den Artikelwert mit der Berechnungsvorschrift
 *  Beschreibung:     
                      Je nach Methode kann es sein, dass der Artikelwert in einem Intervall liegen
                      muss. Um diesen Vergleich zu entkoppeln, kann jede abgeleitete Methode
                      den Vergleich selber vornehmen.
 --------------------------------------------------------------------*)
function TQOAVG.GetIsInOfflimit(aValue: extended; aShiftToCalc, aShiftID, aStyleID: integer): Boolean;
begin
  Result := inherited GetIsInOfflimit(aValue, aShiftToCalc, aShiftID, aStyleID);
end;// TQOAVG.GetIsInOfflimit cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetSampleShiftCount
 *  Klasse:           TQOAVG
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die Anzahl der Schichten bekannt die f�r die Berechnung des Mittelwertes ben�tigt werden
 *  Beschreibung:     
                      Wird ein Mittelwert ben�tigt, dann kann eine Ableitung diese Methode
                      �berschreiben und den Umfang der Stichprobe bekannt geben.
 --------------------------------------------------------------------*)
function TQOAVG.GetSampleShiftCount: Integer;
begin
  result := ShiftCountAVG;
end;// TQOAVG.GetSampleShiftCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           Internal_Evaluate
 *  Klasse:           TQOAVG
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Wird von der Methode 'Evaluate' aufgerufen
 *  Beschreibung:     
                      Hier werden die Daten f�r die Mittelwertbestimmung gesammelt.
 --------------------------------------------------------------------*)
procedure TQOAVG.Internal_Evaluate;
var
  i: Integer;
  xActiveProdIDs: TNativeAdoQuery;
  xStyleID: Integer;
  xMinProdAsFloat: Extended;
  xStyleIDs: String;
  
  const
    cGetLenStyle = 'SELECT SUM(c_len) AS c_len ' +
                   'FROM   v_production_shift ' +
                   'WHERE  c_style_id = :c_style_id';
  
begin
  // Alle Artikel die noch nicht lange genug Produzieren entfernen (Min. produzierte L�nge)
  try
    xStyleIDs := '';
  
    xActiveProdIDs := TNativeAdoQuery.Create;
    xActiveProdIDs.Recordset := TQODataPool.Instance.ActiveProdIDs.Recordset.Clone(LockTypeEnum(adLockUnspecified));
    xActiveProdIDs.CreateFields;
  
    // Da die L�nge im GUI als Km eingegeben wird muss noch multipliziert werden
    xMinProdAsFloat := FMinProdYarnLength;
  
    with TAdoDBAccess.Create(1) do try
      Init;
      i := 0;
      while i < mProdIDList.Count do begin
        xActiveProdIDs.Filter('c_prod_id = ' + mProdIDList[i]);
        if not(xActiveProdIDs.EOF) then begin
          xStyleID := xActiveProdIDs.FieldByName('c_style_id').AsInteger;
  
          // Ermitteln wieviel der Artikel produziert hat
          with Query[0] do begin
            SQL.Text := cGetLenStyle;
            ParamByName('c_style_id').AsInteger := xStyleID;
            open;
  
            if not(EOF) then begin
              if FieldByName('c_len').AsFloat < xMinProdAsFloat then begin
                mProdIDList.Delete(i);
              end else begin
                inc(i);
                xStyleIDs := xStyleIDs + ',' + IntToStr(xStyleID);
              end;// if FieldByName('c_len').AsFloat < xMinProdAsFloat then begin
            end else begin
              mProdIDList.Delete(i);
            end;// if not(EOF) then begin
          end;// with Query[0] do begin
        end;// if not(xActiveProdIDs.EOF) then begin
      end;// while i < mProdIDList.Count do begin
    finally
      Free;
    end;// with TAdoDBAccess.Create(1) do try
  finally
    FreeAndNil(xActiveProdIDs);
  end;// try finally
  
  // f�hrendes Komma l�schen
  if xStyleIDs > '' then
    system.delete(xStyleIDs, 1, 1);

//: ----------------------------------------------
  inherited Internal_Evaluate;
end;// TQOAVG.Internal_Evaluate cat:No category

//:-------------------------------------------------------------------
(*: Member:           MethodValid
 *  Klasse:           TQOAVG
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt zur�ck ob alle Parameter mit sinvollen Werten belegt sind
 *  Beschreibung:     
                      Nat�rlich kann nicht bis ins letzte Detail bestimmt werden, ob 
                      Einstellungen sinnvoll sind. Hier ist vor allem daf�r zu sorgen,
                      dass die G�ltigkeit der Werte sichergestellt ist.
 --------------------------------------------------------------------*)
function TQOAVG.MethodValid: Boolean;
begin
  Result := inherited MethodValid;

//: ----------------------------------------------
  result := ShiftCount > 0;
  result := result and (MinProdYarnLength > 0);
  result := result and (ShiftCountAVG > 0);
  result := result and (Tolerance <> 0);
end;// TQOAVG.MethodValid cat:No category

//:-------------------------------------------------------------------
(*: Member:           ReadXMLMethodeSettings
 *  Klasse:           TQOAVG
 *  Kategorie:        No category 
 *  Argumente:        (aXMLItem)
 *
 *  Kurzbeschreibung: Liest die Settings der Methode aus dem XML Stream
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOAVG.ReadXMLMethodeSettings(aXMLItem: TIcXMLelement);
var
  xDeviation: string;
begin
  inherited ReadXMLMethodeSettings(aXMLItem);

//: ----------------------------------------------
  if assigned(aXMLItem) then begin
    xDeviation := GetAttrValDef(cAVGDeviation, aXMLItem, '0');
    Tolerance := MMStrToFloat(xDeviation);
    ShiftCountAVG := GetAttrValDef(cAVGShiftCompression, aXMLItem, cDefSchiftCountAVG);
    MinProdYarnLength := GetAttrValDef(cXMLMinProdYarnLength, aXMLItem, 0);
  end;// if assigned(aItem) then begin
end;// TQOAVG.ReadXMLMethodeSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           WriteXMLMethodSettings
 *  Klasse:           TQOAVG
 *  Kategorie:        No category 
 *  Argumente:        (aXMLItem)
 *
 *  Kurzbeschreibung: Gibt die Methoden Settings als XML formatierten String zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOAVG.WriteXMLMethodSettings(aXMLItem: TIcXMLElement);
begin
  inherited WriteXMLMethodSettings(aXMLItem);

//: ----------------------------------------------
  // Dann alle Eigenschaften in den Stream schreiben
  aXMLItem.SetAttribute(cAVGDeviation, MMFloatToStr(Tolerance));
  aXMLItem.SetAttribute(cXMLMinProdYarnLength, IntToStr(MinProdYarnLength));
  aXMLItem.SetAttribute(cAVGShiftCompression, IntToStr(ShiftCountAVG));
end;// TQOAVG.WriteXMLMethodSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           _GetMethodSettings
 *  Klasse:           TQOAVG
 *  Kategorie:        No category 
 *  Argumente:        (aSettings)
 *
 *  Kurzbeschreibung: Holt die Settings aller Methoden die vom Frame angezeigt werdenn.
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAVG._GetMethodSettings(aSettings: TmmVCLStringList): String;
begin
  Result := inherited _GetMethodSettings(aSettings);

//: ----------------------------------------------
  // Zus�tzliche Settings des Mittelwertes
  if assigned(aSettings) then begin
    aSettings.Strings.Values[cAVGDeviation]         := MMFloatToStr(Tolerance);
    aSettings.Strings.Values[cXMLMinProdYarnLength] := MinProdYarnLength;
    aSettings.Strings.Values[cAVGShiftCompression]  := ShiftCountAVG;
  end;// if assigned(aSettings) then begin
end;// TQOAVG._GetMethodSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOFixBound
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOFixBound.Create(aOwner: TQOItem);
begin
  inherited Create(aOwner);

//: ----------------------------------------------
  FMethodType := cmFix;
end;// TQOFixBound.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           CalculateLimitValue
 *  Klasse:           TQOFixBound
 *  Kategorie:        No category 
 *  Argumente:        (aShiftNumber, aShiftID, aStyleID)
 *
 *  Kurzbeschreibung: Gibt den berechneten, absoluten Grenzwert zur�ck
 *  Beschreibung:     
                      Mit den Parametern 'aNew, aShift' kann festgelegt werden ob ein Grenzwert in jedem Fall 
                      neu berechnet werden soll. 
                      Wird f�r jede Schicht ein eigener Grenzwert berechnet werden, dann muss sich die Methode 
                      im ersten Aufruf (siehe Evaluate) den ermittelten Grenzwert merken, so dass der 
                      Grenzwert f�r die bereits berechneten Schichten nur einmal berechnet werden muss.
 --------------------------------------------------------------------*)
function TQOFixBound.CalculateLimitValue(aShiftNumber: integer; aShiftID: integer; aStyleID: integer): Extended;
begin
  // Da der Grenzwert nicht berechnet werden muss, kann das Argument 'aNew' unbeachtet bleiben
  result := LimitValue;
end;// TQOFixBound.CalculateLimitValue cat:No category

//:-------------------------------------------------------------------
(*: Member:           MethodValid
 *  Klasse:           TQOFixBound
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt zur�ck ob alle Parameter mit sinvollen Werten belegt sind
 *  Beschreibung:     
                      Nat�rlich kann nicht bis ins letzte Detail bestimmt werden, ob 
                      Einstellungen sinnvoll sind. Hier ist vor allem daf�r zu sorgen,
                      dass die G�ltigkeit der Werte sichergestellt ist.
 --------------------------------------------------------------------*)
function TQOFixBound.MethodValid: Boolean;
begin
  Result := inherited MethodValid;

//: ----------------------------------------------
  (* Soll eine G�ltigkeitspr�fung durchgef�hrt werden,
     dann muss diese Methode �berschrieben werden *)
  result := ShiftCount > 0;
  result := result and (LimitValue >= 0);
end;// TQOFixBound.MethodValid cat:No category

//:-------------------------------------------------------------------
(*: Member:           ReadXMLMethodeSettings
 *  Klasse:           TQOFixBound
 *  Kategorie:        No category 
 *  Argumente:        (aXMLItem)
 *
 *  Kurzbeschreibung: Liest die Settings der Methode aus dem XML Stream
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOFixBound.ReadXMLMethodeSettings(aXMLItem: TIcXMLelement);
var
  xLimitValue: string;
begin
  inherited ReadXMLMethodeSettings(aXMLItem);

//: ----------------------------------------------
  if assigned(aXMLItem) then begin
    xLimitValue := GetAttrValDef(cFixBorderLimitValue, aXMLItem, 0);
    LimitValue := MMStrToFloat(xLimitValue);
  end;// if assigned(aItem) then begin
end;// TQOFixBound.ReadXMLMethodeSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           WriteXMLMethodSettings
 *  Klasse:           TQOFixBound
 *  Kategorie:        No category 
 *  Argumente:        (aXMLItem)
 *
 *  Kurzbeschreibung: Gibt die Methoden Settings als XML formatierten String zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOFixBound.WriteXMLMethodSettings(aXMLItem: TIcXMLElement);
begin
  inherited WriteXMLMethodSettings(aXMLItem);

//: ----------------------------------------------
  // Dann alle Eigenschaften in den Stream schreiben
  aXMLItem.SetAttribute(cFixBorderLimitValue, MMFloatToStr(LimitValue));
end;// TQOFixBound.WriteXMLMethodSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           _GetMethodSettings
 *  Klasse:           TQOFixBound
 *  Kategorie:        No category 
 *  Argumente:        (aSettings)
 *
 *  Kurzbeschreibung: Holt die Settings aller Methoden die vom Frame angezeigt werdenn.
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOFixBound._GetMethodSettings(aSettings: TmmVCLStringList): String;
begin
  Result := inherited _GetMethodSettings(aSettings);

//: ----------------------------------------------
  // Zus�tzliche Settings des FixBorders
  if assigned(aSettings) then begin
    aSettings.Strings.Values[cFixBorderLimitValue] := MMFloatToStr(LimitValue);
  end;// if assigned(aSettings) then begin
end;// TQOFixBound._GetMethodSettings cat:No category


end.
