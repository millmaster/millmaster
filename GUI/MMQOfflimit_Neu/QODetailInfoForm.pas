(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QODetailInfoForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Detail Ansicht f�r einen einzelnen Artikel
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 17.10.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit QODetailInfoForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, ComCtrls, mmTreeView, StdCtrls, mmLabel, VirtualTrees,
  ExtCtrls, mmPanel, TeEngine, IvDictio, IvMulti, IvEMulti,
  mmTranslator, mmTimer;

type
  (*: Klasse:        TfrmDetailInfo
      Vorg�nger:     TmmForm
      Kategorie:     No category
      Kurzbeschrieb: Zeigt Detail Infos f�r einen Datenpunkt im Cahrt an 
      Beschreibung:  
                     - *)
  TfrmDetailInfo = class(TmmForm)
    laShiftStart: TmmLabel;
    laStyle: TmmLabel;
    laTitle: TmmLabel;
    laValue: TmmLabel;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmLabel3: TmmLabel;
    mmTranslator1: TmmTranslator;
    pnMain: TmmPanel;
    timClose: TmmTimer;
    //1 Merkt sich wo im Formular gecklickt wurde um anhand dieser Daten das Form zu verschieben 
    procedure ControlsMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    //1 Verschiebt das Formular an die aktuelle Position der Maus 
    procedure ControlsMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    //1 Verbirgt die Detail Infos wieder 
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    //1 Sobald das Formular nicht mehr das aktive Form ist, wird es ausgeblendet 
    procedure FormDeactivate(Sender: TObject);
    //1 Schliesst das Formular nach einer bestimmten Zeit 
    procedure timCloseTimer(Sender: TObject);
  private
    mMousePoint: TPoint;
  public
    //1 Knstruktor 
    constructor Create(aOwner: TComponent); reintroduce; virtual;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Zeigt die Infos f�r den aktuellen Datenpunkt an 
    procedure ShowInfo(aClickedSeries: TChartSeries; X, Y, aValueIndex: Integer);
  end;
  
var
  frmDetailInfo: TfrmDetailInfo;

implementation
{$R *.DFM}
uses
  mmCS, LoepfeGlobal,
  LabMasterDef, QOGUIClasses, QOChartSeries, QOGUIShared, multimon;

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmDetailInfo
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Knstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmDetailInfo.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
end;// TfrmDetailInfo.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmDetailInfo
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmDetailInfo.Destroy;
begin
  inherited Destroy;
end;// TfrmDetailInfo.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           ControlsMouseDown
 *  Klasse:           TfrmDetailInfo
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Button, Shift, X, Y)
 *
 *  Kurzbeschreibung: Merkt sich wo im Formular gecklickt wurde um anhand dieser Daten das Form zu verschieben
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmDetailInfo.ControlsMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  mMousePoint := Point(X, Y);
end;// TfrmDetailInfo.ControlsMouseDown cat:No category

//:-------------------------------------------------------------------
(*: Member:           ControlsMouseMove
 *  Klasse:           TfrmDetailInfo
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Shift, X, Y)
 *
 *  Kurzbeschreibung: Verschiebt das Formular an die aktuelle Position der Maus
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmDetailInfo.ControlsMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if ssLeft in Shift then begin
    Left := Left + (X - mMousePoint.x);
    Top  := Top + (Y - mMousePoint.y);
    timClose.Restart;
  end;
end;// TfrmDetailInfo.ControlsMouseMove cat:No category

//:-------------------------------------------------------------------
(*: Member:           FormClose
 *  Klasse:           TfrmDetailInfo
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Action)
 *
 *  Kurzbeschreibung: Verbirgt die Detail Infos wieder
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmDetailInfo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caHide;
end;// TfrmDetailInfo.FormClose cat:No category

//:-------------------------------------------------------------------
(*: Member:           FormDeactivate
 *  Klasse:           TfrmDetailInfo
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Sobald das Formular nicht mehr das aktive Form ist, wird es ausgeblendet
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmDetailInfo.FormDeactivate(Sender: TObject);
begin
  Close;
end;// TfrmDetailInfo.FormDeactivate cat:No category

//:-------------------------------------------------------------------
(*: Member:           ShowInfo
 *  Klasse:           TfrmDetailInfo
 *  Kategorie:        No category 
 *  Argumente:        (aClickedSeries, X, Y, aValueIndex)
 *
 *  Kurzbeschreibung: Zeigt die Infos f�r den aktuellen Datenpunkt an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmDetailInfo.ShowInfo(aClickedSeries: TChartSeries; X, Y, aValueIndex: Integer);
var
  xSeriesDetailInfo: PSeriesDetailInfoRec;
  xRight: Integer;
  xBottom: Integer;
  i: Integer;
  xWidth: Integer;
  xLabel: TLabel;
  xCalcWorkareaSuccessful: Boolean;
  xClickPoint: TPoint;
  xMonitorInfo: tagMONITORINFOA;
  xMonitorHandle: THandle;
begin
  if (aClickedSeries is TQOLineSeries) then begin
    xSeriesDetailInfo := TQOLineSeries(aClickedSeries).Items[aValueIndex];
    if assigned(xSeriesDetailInfo) then begin
      laStyle.Caption := xSeriesDetailInfo^.StyleName;
      laShiftStart.Caption := DateTimeToStr(xSeriesDetailInfo^.ShiftStart);
      laValue.Caption := mmFormatFloatToStr(xSeriesDetailInfo^.Value) + xSeriesDetailInfo^.ValueUnit;
  
      laTitle.Caption := '';
      case xSeriesDetailInfo^.DetailInfoLevel of
        ilStyle      : laTitle.Caption := rsStyle;
        ilLot        : laTitle.Caption := rsLot;
        ilCalcMethod : laTitle.Caption := rsLimit;
      end;// case xSeriesDetailInfo^.DetailInfoLevel of
      laTitle.Caption := laTitle.Caption + ': ' + xSeriesDetailInfo^.SerieTitle;
  
      // Nun noch die Koordinaten setzen und Show aufrufen
      // Screensize ohne Taskbar (= Workarea) ermitteln
  
      xCalcWorkareaSuccessful := false;
  
      xClickPoint.x := X;
      xClickPoint.y := Y;
  
      xRight  := Width;
      xBottom := Height;
  
      xMonitorHandle := MonitorFromPoint(xClickPoint, MONITOR_DEFAULTTONEAREST);
      if not(xMonitorhandle = INVALID_HANDLE_VALUE) then begin
        xMonitorInfo.cbSize := sizeof(xMonitorInfo);
        if (GetMonitorInfo(xMonitorHandle, @xMonitorInfo)) then begin
          xRight := xMonitorInfo.rcWork.Right - Width;
          xBottom := xMonitorInfo.rcWork.Bottom - Height;
          xCalcWorkareaSuccessful := true;
        end;// if (GetMonitorInfo(xMonitorHandle, @xMonitorInfo)) then begin
      end;//if not(xMonitorhandle = INVALID_HANDLE_VALUE) then begin
  
      if not(xCalcWorkareaSuccessful) then begin
        xRight  := Screen.Width - Width;
        xBottom := Screen.Height - Height - 25;
      end;
  
      if X > xRight  then
        X := xRight;
      if Y > xBottom then
        Y := xBottom;
  
      Left := X+5;
      Top  := Y+5;
  
      xWidth := 0;
      for i := 0 to ComponentCount - 1 do begin
        if Components[i] is TLabel then begin
          xLabel := TLabel(Components[i]);
          if (xLabel.Left + xLabel.Width) > xWidth then
            xWidth := xLabel.Left + xLabel.Width;
        end;// if Components[i] is TLabel then begin
      end;// for i := 0 to ComponentCount - 1 do begin
      Width := xWidth + 8;
  
      // Titel zentrieren
      laTitle.Left := (Width - laTitle.Width) div 2;
  
      Show;
      timClose.Restart;
    end;// if assigned(xSeriesDetailInfo) then begin
  end;// if (aClickedSeries is TQOLineSeries) then begin
end;// TfrmDetailInfo.ShowInfo cat:No category

//:-------------------------------------------------------------------
(*: Member:           timCloseTimer
 *  Klasse:           TfrmDetailInfo
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Schliesst das Formular nach einer bestimmten Zeit
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmDetailInfo.timCloseTimer(Sender: TObject);
begin
  Close;
end;// TfrmDetailInfo.timCloseTimer cat:No category


end.






