(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QOAnalyzeChartReport.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Bericht f�r die Chartansicht (Artikel/Maschine)
|
| Info..........: 
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 17.10.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit QOAnalyzeChartReport;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, TeeProcs, TeEngine, Chart, DBChart, QrTee, QuickRpt, mmQRChart,
  ExtCtrls, mmQuickRep, QOGUIClasses, Qrctrls, mmQRImage, mmQRSysData,
  mmQRLabel, IvDictio, IvMulti, IvEMulti, mmTranslator;

type
  (*: Klasse:        ChartObjRec
      Vorg�nger:     
      Kategorie:     record
      Kurzbeschrieb: Record f�r die Verwaltung der Statistikdaten f�r die Alarme im AnalyzeTree (�bersicht und 
      AlarmView) 
      Beschreibung:  
                     - *)
  ChartObjRec = record
    Alarm: TQOAlarmView;
    Machine: TQOAssignNodeObject;
    Style: TQOAssignNodeObject;
  end;
  
  PChartObjRec = ^ChartObjRec;

  (*: Klasse:        TfrmQOAnalyzeChartReport
      Vorg�nger:     TmmForm
      Kategorie:     No category
      Kurzbeschrieb: Bericht f�r den gesammten Artikel oder f�r eine einzelne Maschine eines Alarms 
      Beschreibung:  
                     - *)
  TfrmQOAnalyzeChartReport = class (TmmForm)
    mAlarmCaption: TmmQRLabel;
    mAlarmName: TmmQRLabel;
    mAnalyzeChart: TmmQRChart;
    mChartReport: TmmQuickRep;
    mmQRImage1: TmmQRImage;
    mmTranslator1: TmmTranslator;
    mRootBand: TQRBand;
    mShiftListTitle: TQRBand;
    mShiftStart: TmmQRLabel;
    mShiftStartCaption: TmmQRLabel;
    mStyleCaption: TmmQRLabel;
    mStyleName: TmmQRLabel;
    PageFooterBand1: TQRBand;
    qlTitle: TmmQRLabel;
    QRDBChart1: TQRDBChart;
    qsysDateTime: TmmQRSysData;
    qsysPageNumber: TmmQRSysData;
    //1 Druckmethode 
    procedure mChartReportNeedData(Sender: TObject; var MoreData: Boolean);
  private
    mAssignIndex: Integer;
    mAssignList: TList;
    mDataItemIndex: Integer;
    mDataServer: TAnalyzeChartDataServer;
    mMoreDataItems: Boolean;
    //1 Anzahl der zu druckenden Schichten 
    function GetAssignCount: Integer;
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 F�gt eine Schicht die gedruckt werden soll zur Liste hinzu 
    procedure AddAssign(aAlarm: TQOAlarmView; aStyleObject, aMachineObject: TQOAssignNodeObject);
    //1 L�scht alle Eintrage der zu druckenden Artikel/Maschinen 
    procedure ClearAssignList;
    //1 Druckt die Schichten aus die zuvor der Liste hinzugef�gt wurden (siehe AddShift()) 
    procedure Print;
    //1 Anzahl der zu druckenden Schichten 
    property AssignCount: Integer read GetAssignCount;
  end;
  
var
  frmQOAnalyzeChartReport: TfrmQOAnalyzeChartReport;

implementation


{$R *.DFM}

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmQOAnalyzeChartReport
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmQOAnalyzeChartReport.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

//: ----------------------------------------------
  mAssignList := TList.Create;
end;// TfrmQOAnalyzeChartReport.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmQOAnalyzeChartReport
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmQOAnalyzeChartReport.Destroy;
begin
  FreeAndNil(mAssignList);
  FreeAndNil(mDataServer);

//: ----------------------------------------------
  inherited Destroy;
end;// TfrmQOAnalyzeChartReport.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddAssign
 *  Klasse:           TfrmQOAnalyzeChartReport
 *  Kategorie:        No category 
 *  Argumente:        (aAlarm, aStyleObject, aMachineObject)
 *
 *  Kurzbeschreibung: F�gt eine Schicht die gedruckt werden soll zur Liste hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOAnalyzeChartReport.AddAssign(aAlarm: TQOAlarmView; aStyleObject, aMachineObject: TQOAssignNodeObject);
var
  xChartObjRec: PChartObjRec;
begin
  // Assign Objekt zur Liste hinzuf�gen
  if assigned(mAssignList) then begin
    new(xChartObjRec);
  
    xChartObjRec.Alarm := aAlarm;
    xChartObjRec.Style := aStyleObject;
    xChartObjRec.Machine := aMachineObject;
  
    mAssignList.Add(xChartObjRec);
  end;// if assigned(mAssignList) then begin
end;// TfrmQOAnalyzeChartReport.AddAssign cat:No category

//:-------------------------------------------------------------------
(*: Member:           ClearAssignList
 *  Klasse:           TfrmQOAnalyzeChartReport
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�scht alle Eintrage der zu druckenden Artikel/Maschinen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOAnalyzeChartReport.ClearAssignList;
var
  i: Integer;
begin
  // Liste leeren
  if assigned(mAssignList) then begin
    // Speicherplatz f�r die Records freigeben
    for i := 0 to mAssignList.Count - 1 do
      Dispose(mAssignList[i]);
  
    mAssignList.Clear;
  end;// if assigned(mAssignList) then begin
end;// TfrmQOAnalyzeChartReport.ClearAssignList cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAssignCount
 *  Klasse:           TfrmQOAnalyzeChartReport
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Anzahl der zu druckenden Schichten
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmQOAnalyzeChartReport.GetAssignCount: Integer;
begin
  if assigned(mAssignList) then
    result := mAssignList.Count;
end;// TfrmQOAnalyzeChartReport.GetAssignCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           mChartReportNeedData
 *  Klasse:           TfrmQOAnalyzeChartReport
 *  Kategorie:        No category 
 *  Argumente:        (Sender, MoreData)
 *
 *  Kurzbeschreibung: Druckmethode
 *  Beschreibung:     
                      Diese Methode wird f�r jedes zu Druckende Chart aufgerufen. Die Verwaltung ob noch
                      mehr Charts zu Drucken sind ist in der Methode selbst implementiert (MoreData).
 --------------------------------------------------------------------*)
procedure TfrmQOAnalyzeChartReport.mChartReportNeedData(Sender: TObject; var MoreData: Boolean);
var
  xAssign: TQOAssignNodeObject;
  
  const
    cCaptionNameSpace = 4;
    cItemSpace = 20;
  
begin
  // Vom letzten mal freigeben;
  FreeAndNil(mDataServer);
  
  if mMoreDataItems then begin
    MoreData := (mAssignIndex < AssignCount);
  end else begin
    inc(mAssignIndex);
    MoreData := (mAssignIndex < AssignCount);
    mMoreDataItems := true;
    mDataItemIndex := -1;
  end;// if mMoreDataItems then begin
  
  if MoreData then begin
    mDataServer := TAnalyzeChartDataServer.Create(self);
    try
      with mDataServer do begin
        xAssign := mAssignList[mAssignIndex];
        Style := PChartObjRec(mAssignList[mAssignIndex])^.Style;
        // Aktuell selektierte Maschine
        Machine := PChartObjRec(mAssignList[mAssignIndex])^.Machine;
        if assigned(Machine) then begin
          // Chart ist im Maschinen Modus
          AssignShowMode := smMachine;
          Initialize(PChartObjRec(mAssignList[mAssignIndex])^.Alarm, IntToStr(Machine.LinkID));
        end else begin
          // Chart ist im Artikel Modus
          AssignShowMode := smStyle;
          // Initialisiert die Anzeige und ruft 'mDataItemTabsChange' auf
          Initialize(PChartObjRec(mAssignList[mAssignIndex])^.Alarm, '');
        end;// if assigned(Machine) then begin
  
        inc(mDataItemIndex);
        if mDataItemIndex < DataItemList.Count then begin
          ShowDataItem(DataItemList[mDataItemIndex], mAnalyzeChart.Chart);
  
          mAlarmName.Caption := PChartObjRec(mAssignList[mAssignIndex])^.Alarm.QOVisualize.Caption;
          mStyleName.Caption := PChartObjRec(mAssignList[mAssignIndex])^.Style.QOVisualize.Caption;
          mShiftStart.Caption := PChartObjRec(mAssignList[mAssignIndex])^.Alarm.GetShiftStart;
  
          mAlarmName.Left := mAlarmCaption.Left + mAlarmCaption.Width + cCaptionNameSpace;
          mShiftStartCaption.Left := mAlarmName.Left + mAlarmName.Width + cItemSpace;
          mShiftStart.Left := mShiftStartCaption.Left + mShiftStartCaption.Width + cCaptionNameSpace + 2;
          mStyleCaption.Left := mShiftStart.Left + mShiftStart.Width + cItemSpace;
          mStyleName.Left := mStyleCaption.Left + mStyleCaption.Width + cCaptionNameSpace;
  
        end;// if mDataItemIndex < DataItemList.Count then begin
        mMoreDataItems := (mDataItemIndex < (DataItemList.Count -1));
      end;// with DataServer do begin
    except
      FreeAndNil(mDataServer);
    end;// try except
  end;// if MoreData then begin
end;// TfrmQOAnalyzeChartReport.mChartReportNeedData cat:No category

//:-------------------------------------------------------------------
(*: Member:           Print
 *  Klasse:           TfrmQOAnalyzeChartReport
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Druckt die Schichten aus die zuvor der Liste hinzugef�gt wurden (siehe AddShift())
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOAnalyzeChartReport.Print;
begin
  mAssignIndex := -1;
  mDataItemIndex := -1;
  // Wenn ein Chart gedruckt wird ist auch am Anfang mindestens ein DataItem vorhanden
  mMoreDataItems := false;
  
  // Das Sammeln der Daten kann unter Umstanden (viele Schichten) l�nger dauern
  try
    Screen.Cursor := crHourGlass;
    GetAsyncKeyState(VK_ESCAPE);
    mChartReport.Print;
  finally
    Screen.Cursor := crDefault;
  end;// finally
end;// TfrmQOAnalyzeChartReport.Print cat:No category

end.




