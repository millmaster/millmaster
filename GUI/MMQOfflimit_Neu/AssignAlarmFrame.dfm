object frmAssignAlarmFrame: TfrmAssignAlarmFrame
  Left = 0
  Top = 0
  Width = 510
  Height = 505
  TabOrder = 0
  object mRightPanel: TmmPanel
    Left = 0
    Top = 0
    Width = 510
    Height = 505
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object mmPanel1: TmmPanel
      Left = 0
      Top = 0
      Width = 510
      Height = 505
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object mHorSplitter: TmmSplitter
        Left = 0
        Top = 320
        Width = 510
        Height = 9
        Cursor = crVSplit
        Align = alBottom
      end
      object mTopPanel: TmmPanel
        Left = 0
        Top = 0
        Width = 510
        Height = 320
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object mTopButtonPanel: TmmPanel
          Left = 0
          Top = 0
          Width = 49
          Height = 320
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object bAddStyle: TmmSpeedButton
            Left = 13
            Top = 40
            Width = 24
            Height = 24
            Hint = '(*)Artikel hinzufuegen'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Glyph.Data = {
              92000000424D9200000000000000760000002800000007000000070000000100
              0400000000001C00000000000000000000001000000000000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888808808880
              08808800088080000880880008808880088088880880}
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Visible = True
            OnClick = bAddStyleClick
            AutoLabel.LabelPosition = lpLeft
          end
          object bShowMachines: TmmSpeedButton
            Left = 0
            Top = 296
            Width = 48
            Height = 20
            Anchors = [akLeft, akBottom]
            Flat = True
            ParentShowHint = False
            ShowHint = True
            Visible = True
            OnClick = bShowMachinesClick
            AutoLabel.LabelPosition = lpLeft
          end
        end
        object mmPanel2: TmmPanel
          Left = 49
          Top = 0
          Width = 461
          Height = 320
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object mStyleList: TmmVirtualStringTree
            Left = 0
            Top = 21
            Width = 461
            Height = 281
            Align = alClient
            AutoScrollDelay = 10
            DragMode = dmAutomatic
            DragType = dtVCL
            Header.AutoSizeIndex = 0
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            Header.Options = [hoColumnResize, hoDrag, hoShowImages, hoShowSortGlyphs, hoVisible]
            Header.SortDirection = sdDescending
            Indent = 2
            TabOrder = 0
            TreeOptions.AutoOptions = [toAutoDropExpand, toAutoScroll, toAutoScrollOnExpand, toAutoTristateTracking, toAutoDeleteMovedNodes]
            TreeOptions.MiscOptions = [toAcceptOLEDrop, toFullRepaintOnResize, toGridExtensions, toInitOnSave, toToggleOnDblClick, toWheelPanning]
            TreeOptions.PaintOptions = [toHideFocusRect, toShowButtons, toShowDropmark, toShowRoot, toThemeAware, toUseBlendedImages]
            TreeOptions.SelectionOptions = [toFullRowSelect, toMultiSelect]
            OnChange = mStyleListChange
            OnCompareNodes = TreeCompareNodes
            OnDblClick = mStyleListDblClick
            OnFocusChanged = mStyleListFocusChanged
            OnFreeNode = TreeFreeNode
            OnGetCursor = mStyleListGetCursor
            OnGetText = mStyleListGetText
            OnGetImageIndex = mStyleListGetImageIndex
            OnHeaderClick = TreeHeaderClick
            OnResize = mStyleListResize
            Columns = <
              item
                Position = 1
                WideText = '(*)Artikel'
              end
              item
                Margin = 0
                Position = 0
                Spacing = 0
                Width = 40
              end>
          end
          object mIncrementalStylePanel: TmmPanel
            Left = 0
            Top = 0
            Width = 461
            Height = 21
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            OnResize = mIncrementalStylePanelResize
            object edIncrementalStyle: TmmEdit
              Left = 0
              Top = 0
              Width = 225
              Height = 21
              Color = clWindow
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGrayText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              Visible = True
              OnChange = edIncrementalStyleChange
              AutoLabel.LabelPosition = lpLeft
              InfoText = '(*)Filterkriterium eingeben'
              NumMode = False
              ReadOnlyColor = clInfoBk
              ShowMode = smNormal
            end
          end
          object mInfoPanelStyle: TmmPanel
            Left = 0
            Top = 302
            Width = 461
            Height = 18
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 2
            Visible = False
            object mmPanel4: TmmPanel
              Left = 0
              Top = 2
              Width = 21
              Height = 16
              Align = alLeft
              BevelOuter = bvNone
              Color = clInfoBk
              TabOrder = 0
              object mInfoImage: TmmImage
                Left = 2
                Top = 0
                Width = 16
                Height = 16
                Picture.Data = {
                  07544269746D617036030000424D360300000000000036000000280000001000
                  0000100000000100180000000000000300000000000000000000000000000000
                  0000E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF84695A8C695A
                  E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FF
                  FFE7FFFFE7FFFF94694ACE9E6BAD794A84695AE7FFFFE7FFFFE7FFFFE7FFFFE7
                  FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF94694ACEAE84FFF7C6946142
                  84695AE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF9471
                  638C614AE7CFADFFDFB5FFDFB5AD86635228086B413184695AE7FFFFE7FFFFE7
                  FFFFE7FFFFE7FFFFBDA69CBDA69CD6B69CE7CFADFFE7BDFFEFC6FFFFD6F7D7B5
                  DEBE94AD8E635A281084695AE7FFFFE7FFFFE7FFFFCEB6A5EFDFC6EFD7BDFFEF
                  CEFFD7B5C6714AEF9663DE8E5ADEA67BFFE7C6F7D7B5D6B6947341298C6963E7
                  FFFFCEBEA5EFDFC6EFDFC6FFEFD6FFEFCEFFFFE7B586637B00008C2800F7E7C6
                  FFEFCEFFE7C6F7D7B5DEBE9C5A2810E7FFFFC6B6A5EFDFC6FFF7D6FFEFD6FFE7
                  CEFFFFEFCEB6946B0000A55931FFFFF7FFE7CEFFE7C6FFEFCEFFE7C6AD8E6B94
                  716BCEBEADFFE7D6FFF7DEFFEFD6FFEFD6FFFFF7CEAE94730000A55131FFFFF7
                  FFEFCEFFE7CEFFEFCEFFEFD6E7CFAD8C695ACEC7B5FFEFE7FFF7E7FFEFDEFFF7
                  DEFFFFFFD6C7AD730000A55931FFFFFFFFEFD6FFEFD6FFEFD6FFF7DEEFD7C684
                  695AD6C7BDFFF7EFFFFFEFFFF7E7FFFFEFEFE7DE8C38186300008C3821FFFFFF
                  FFF7DEFFEFD6FFEFDEFFFFE7F7DFCE947973DED7CEF7F7EFFFFFFFFFF7EFFFFF
                  FFE7DFCEB59684BDA69CC6AEA5FFFFFFFFF7E7FFEFDEFFFFEFFFFFFFE7CFB594
                  7973E7FFFFE7E7E7FFFFFFFFFFFFFFFFF7FFFFFFFFFFFFFFCFA5FFFFEFFFFFFF
                  FFF7EFFFFFEFFFFFFFFFFFFFAD9684E7FFFFE7FFFFEFE7E7EFEFEFFFFFFFFFFF
                  FFFFFFFF8C514A5A00009C4129FFFFFFFFFFFFFFFFFFFFFFFFD6BEADE7FFFFE7
                  FFFFE7FFFFE7FFFFF7EFE7F7F7F7FFFFFFFFFFFFD6CFCE634142C6B6ADFFFFFF
                  FFFFFFFFFFFFEFDFD6E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFEFE7
                  DEF7F7EFFFFFFFFFFFFFFFFFFFFFFFF7DECFC6E7FFFFE7FFFFE7FFFFE7FFFFE7
                  FFFF}
                Transparent = True
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
            end
            object mInfoMemoStyle: TmmMemo
              Left = 21
              Top = 2
              Width = 440
              Height = 16
              Align = alClient
              BorderStyle = bsNone
              Color = clInfoBk
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object mDescriptionSpacePanel: TmmPanel
              Left = 0
              Top = 0
              Width = 461
              Height = 2
              Align = alTop
              BevelOuter = bvNone
              Color = clInfoBk
              TabOrder = 2
            end
          end
        end
      end
      object mBottomPanel: TmmPanel
        Left = 0
        Top = 329
        Width = 510
        Height = 176
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object mBottomButtonPanel: TmmPanel
          Left = 0
          Top = 0
          Width = 49
          Height = 176
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object bAddMachine: TmmSpeedButton
            Left = 13
            Top = 19
            Width = 24
            Height = 24
            Hint = '(*)Maschine hinzufuegen'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Glyph.Data = {
              92000000424D9200000000000000760000002800000007000000070000000100
              0400000000001C00000000000000000000001000000000000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888808808880
              08808800088080000880880008808880088088880880}
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Visible = True
            OnClick = bAddMachineClick
            AutoLabel.LabelPosition = lpLeft
          end
          object imMachCollapse: TmmImage
            Left = 8
            Top = 144
            Width = 32
            Height = 16
            Picture.Data = {
              07544269746D617076010000424D760100000000000076000000280000002000
              0000100000000100040000000000000100000000000000000000100000000000
              0000000000000000800000800000008080008000000080008000808000008080
              8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
              FF00D0000000DDDDDDDDDDDDDDDDDD0FDDDD788888870DDDDDDDDDDDDDDDD000
              FDDD788899877000DDDDDDDDDDDD00F00FDD788888007030D00DDDDDDDD00FDD
              00FD7888883300B303B0DDDDDDDDDDDDDDDD7888880B30F33B0DDDDDDDDDDD0F
              DDDD78888030BFBFB030DDDDDDDDD000FDDD7FFFF03BF000FB30DDDDDDDD00F0
              0FDD7777730F08F00300DDDDDDD00FDD00FD78888803B8F03B0DDDDDDDDDDDDD
              DDDD7FFFFF3B08F0030DDDDDDDDDDD0FDDDD777777737887D0DDDDDDDDDDD000
              FDDD7888888700DDDDDDDDDDDDDD00F00FDD7FFFFFF770DDDDDDDDDDDDD00FDD
              00FD7888888870DDDDDDDDDDDDDDDDDDDDDDD77777777DDDDDDDDDDDDDDDDDDD
              DDDD}
            Visible = False
            AutoLabel.LabelPosition = lpLeft
          end
          object imMachExpand: TmmImage
            Left = 8
            Top = 128
            Width = 32
            Height = 16
            Picture.Data = {
              07544269746D617076010000424D760100000000000076000000280000002000
              0000100000000100040000000000000100000000000000000000100000000000
              0000000000000000800000800000008080008000000080008000808000008080
              8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
              FF00D0000000DDDDDDDDDDDDDDDDDDDDDDDD788888870DDDDDDDDDDDDDDDDDDD
              DDDD788899877000DDDDDDDDDDD00FDD00FD788888007030D00DDDDDDDDD00F0
              0FDD7888883300B303B0DDDDDDDDD000FDDD7888880B30F33B0DDDDDDDDDDD0F
              DDDD78888030BFBFB030DDDDDDDDDDDDDDDD7FFFF03BF000FB30DDDDDDD00FDD
              00FD7777730F08F00300DDDDDDDD00F00FDD78888803B8F03B0DDDDDDDDDD000
              FDDD7FFFFF3B08F0030DDDDDDDDDDD0FDDDD777777737887D0DDDDDDDDDDDDDD
              DDDD7888888700DDDDDDDDDDDDD00FDD00FD7FFFFFF770DDDDDDDDDDDDDD00F0
              0FDD7888888870DDDDDDDDDDDDDDD000FDDDD77777777DDDDDDDDDDDDDDDDD0F
              DDDD}
            Visible = False
            AutoLabel.LabelPosition = lpLeft
          end
        end
        object mmPanel3: TmmPanel
          Left = 49
          Top = 0
          Width = 461
          Height = 176
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object mMachineList: TmmVirtualStringTree
            Left = 0
            Top = 0
            Width = 461
            Height = 158
            Align = alClient
            AutoScrollDelay = 10
            DragMode = dmAutomatic
            DragType = dtVCL
            Header.AutoSizeIndex = 0
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            Header.Options = [hoColumnResize, hoDrag, hoShowImages, hoShowSortGlyphs, hoVisible]
            Header.SortDirection = sdDescending
            Indent = 2
            TabOrder = 0
            TreeOptions.AutoOptions = [toAutoDropExpand, toAutoScroll, toAutoScrollOnExpand, toAutoTristateTracking, toAutoDeleteMovedNodes]
            TreeOptions.MiscOptions = [toAcceptOLEDrop, toFullRepaintOnResize, toGridExtensions, toInitOnSave, toToggleOnDblClick, toWheelPanning]
            TreeOptions.PaintOptions = [toHideFocusRect, toShowButtons, toShowDropmark, toShowRoot, toThemeAware, toUseBlendedImages]
            TreeOptions.SelectionOptions = [toFullRowSelect, toMultiSelect]
            OnCompareNodes = TreeCompareNodes
            OnDblClick = mMachineListDblClick
            OnFocusChanged = mMachineListFocusChanged
            OnFreeNode = TreeFreeNode
            OnGetText = mMachineListGetText
            OnGetImageIndex = mMachineListGetImageIndex
            OnHeaderClick = TreeHeaderClick
            OnResize = mMachineListResize
            Columns = <
              item
                Position = 1
                WideText = '(30)Maschinen'
              end
              item
                Margin = 2
                Position = 0
                Width = 40
              end>
          end
          object mInfoPanelMachine: TmmPanel
            Left = 0
            Top = 158
            Width = 461
            Height = 18
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            Visible = False
            object mmPanel6: TmmPanel
              Left = 0
              Top = 2
              Width = 21
              Height = 16
              Align = alLeft
              BevelOuter = bvNone
              Color = clInfoBk
              TabOrder = 0
              object mmImage1: TmmImage
                Left = 2
                Top = 0
                Width = 16
                Height = 16
                Picture.Data = {
                  07544269746D617036030000424D360300000000000036000000280000001000
                  0000100000000100180000000000000300000000000000000000000000000000
                  0000E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF84695A8C695A
                  E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FF
                  FFE7FFFFE7FFFF94694ACE9E6BAD794A84695AE7FFFFE7FFFFE7FFFFE7FFFFE7
                  FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF94694ACEAE84FFF7C6946142
                  84695AE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF9471
                  638C614AE7CFADFFDFB5FFDFB5AD86635228086B413184695AE7FFFFE7FFFFE7
                  FFFFE7FFFFE7FFFFBDA69CBDA69CD6B69CE7CFADFFE7BDFFEFC6FFFFD6F7D7B5
                  DEBE94AD8E635A281084695AE7FFFFE7FFFFE7FFFFCEB6A5EFDFC6EFD7BDFFEF
                  CEFFD7B5C6714AEF9663DE8E5ADEA67BFFE7C6F7D7B5D6B6947341298C6963E7
                  FFFFCEBEA5EFDFC6EFDFC6FFEFD6FFEFCEFFFFE7B586637B00008C2800F7E7C6
                  FFEFCEFFE7C6F7D7B5DEBE9C5A2810E7FFFFC6B6A5EFDFC6FFF7D6FFEFD6FFE7
                  CEFFFFEFCEB6946B0000A55931FFFFF7FFE7CEFFE7C6FFEFCEFFE7C6AD8E6B94
                  716BCEBEADFFE7D6FFF7DEFFEFD6FFEFD6FFFFF7CEAE94730000A55131FFFFF7
                  FFEFCEFFE7CEFFEFCEFFEFD6E7CFAD8C695ACEC7B5FFEFE7FFF7E7FFEFDEFFF7
                  DEFFFFFFD6C7AD730000A55931FFFFFFFFEFD6FFEFD6FFEFD6FFF7DEEFD7C684
                  695AD6C7BDFFF7EFFFFFEFFFF7E7FFFFEFEFE7DE8C38186300008C3821FFFFFF
                  FFF7DEFFEFD6FFEFDEFFFFE7F7DFCE947973DED7CEF7F7EFFFFFFFFFF7EFFFFF
                  FFE7DFCEB59684BDA69CC6AEA5FFFFFFFFF7E7FFEFDEFFFFEFFFFFFFE7CFB594
                  7973E7FFFFE7E7E7FFFFFFFFFFFFFFFFF7FFFFFFFFFFFFFFCFA5FFFFEFFFFFFF
                  FFF7EFFFFFEFFFFFFFFFFFFFAD9684E7FFFFE7FFFFEFE7E7EFEFEFFFFFFFFFFF
                  FFFFFFFF8C514A5A00009C4129FFFFFFFFFFFFFFFFFFFFFFFFD6BEADE7FFFFE7
                  FFFFE7FFFFE7FFFFF7EFE7F7F7F7FFFFFFFFFFFFD6CFCE634142C6B6ADFFFFFF
                  FFFFFFFFFFFFEFDFD6E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFEFE7
                  DEF7F7EFFFFFFFFFFFFFFFFFFFFFFFF7DECFC6E7FFFFE7FFFFE7FFFFE7FFFFE7
                  FFFF}
                Transparent = True
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
            end
            object mInfoMemoMachine: TmmMemo
              Left = 21
              Top = 2
              Width = 440
              Height = 16
              Align = alClient
              BorderStyle = bsNone
              Color = clInfoBk
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object mmPanel7: TmmPanel
              Left = 0
              Top = 0
              Width = 461
              Height = 2
              Align = alTop
              BevelOuter = bvNone
              Color = clInfoBk
              TabOrder = 2
            end
          end
        end
      end
    end
  end
  object mTranslator: TmmTranslator
    DictionaryName = 'MainDictionary'
    OnAfterTranslate = mTranslatorAfterTranslate
    Left = 361
    Top = 65
    TargetsData = (
      1
      4
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        'TVirtualTreeColumn'
        'Text'
        0)
      (
        'TmmMemo'
        'Text'
        0))
  end
end
