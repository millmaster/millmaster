(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QOGUIClasses.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: GUI Klassen f�r die Darstellung der Daten der bisherigen Alarme
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.07.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit QOGUIClasses;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, AdoDBAccess, QOKernel,
  ExtCtrls, TeeProcs, TeEngine, Chart, mmChart,
  QOChartSeries, LoepfeGlobal, IcXMLParser, CalcMethods;
  
type
  // Typ des TAssignNodeObjects
  TQONodeObjectType = (otStyle,
                       otMachine
                      );// TQONodeObjectType
                      
  // Level der DetailInfos zu jedem Datenpunkt in einer ChartSerie
  TDetailInfoLevel = (
    ilNone, 
    ilStyle, 
    ilLot, 
    ilCalcMethod
  );// TDetailInfoLevel = (
  
  // Art wie die Schicht erzeugt wurde
  TShiftAlarmType = (
    satAuto,
    satManual,
    satArchive
  );// TShiftAlarmType = (
  TShiftAlarmTypeSet = Set of TShiftAlarmType;
  
  TAssignShowMode = (smStyle, smMachine);

  TQOAlarmView = class;
  TQOShiftView = class;
  TQOShiftSet = class;
  TQOAssignNodeObject = class;
  TProdID = class;
  TProdIDList = class;
  (*: Klasse:        IQOUserInfo
      Vorg�nger:     IUnknown
      Kategorie:     interface
      Kurzbeschrieb: Stellt Funktionen zu den Benutzerinfos bereit 
      Beschreibung:  
                     - *)
  IQOUserInfo = interface(IUnknown)
    ['{5CF5F5D4-2DD3-4FC9-A17E-C1C027294AF4}']
    //1 Gibt die Zusatzinformationen zur Schicht zur�ck 
    function GetInfo: String; stdcall;
    //1 Speichert die Zusatzinforamtionen auf der DB 
    procedure SaveInfo;
    //1 Setzt die Zusatzinformationen zur Schicht 
    procedure SetInfo(Value: String); stdcall;
    //1 Zusatzinformationen zur Schicht 
    property Info: String read GetInfo write SetInfo;
  end;
  
  PSeriesDetailInfoRec = ^TSeriesDetailInfoRec;
    
  (*: Klasse:        TSeriesDetailInfoRec
      Vorg�nger:     
      Kategorie:     record
      Kurzbeschrieb: Der Record wird f�r jeden Datenpunkt im Chart erzeugt und mitgegeben (DetailInfoForm) 
      Beschreibung:  
                     - *)
  TSeriesDetailInfoRec = record
    DetailInfoLevel: TDetailInfoLevel;
    LotName: String;
    SerieTitle: String;
    ShiftStart: TDateTime;
    StyleName: String;
    Value: Extended;
    ValueUnit: String;
  end;
  
  (*: Klasse:        TAnalyzeChartDataServer
      Vorg�nger:     TObject
      Kategorie:     No category
      Kurzbeschrieb: Stellt die Daten f�r einen Style oder eine Maschine f�r ein einzelnes DataItem zusammen 
      Beschreibung:  
                     Zuerst muss die Methode 'Initialize' aufgerufen werden, um das Chart zu initialisieren.
                     Danach die Methode 'ShowDataItem'.
                     F�r Initialize muss der Alarm �bergeben werden. Optional kann auch die LikID einer Maschine 
                     �bergeben werden. In diesem Fall werden nur die Daten der entsprechenden Maschine gesammelt.
                     Ansonsten die Daten aller Lots auf allen beteiligten Maschinen dieses Alarms und Styles.
                     
                     Nach dem Aufruf von Initialize kann auf die Liste der DataItems zugegriffen werden.
                     
                     Mit ShowDataItem() k�nnen dann die Daten auf dem �bergebenen Chart angezeigt werden. 
                     Der DataServer wird f�r den AnalyzeChartFrame und den entsprechenden Report verwendet werden.
                     *)
  TAnalyzeChartDataServer = class(TObject)
  private
    FActiveDataItemIndex: Integer;
    FAssignShowMode: TAssignShowMode;
    FCalcMode: Integer;
    FClassDefects: Integer;
    FDataItemDisplayName: String;
    FDataItemItemName: String;
    FDataItemList: TStringList;
    FMachine: TQOAssignNodeObject;
    FOwner: TComponent;
    FStyle: TQOAssignNodeObject;
    FTimeFrom: TDateTime;
    FTimeTo: TDateTime;
    mAlarm: TQOAlarmView;
    mAlarmData: TNativeAdoQuery;
    mDataItemFieldName: String;
    mLenBase: TLenBase;
    mSeriesList: TSeriesList;
    //1 holt alle Informationen f�r ein DataItem aus dem Recordset 
    function GetDataItemFieldInformation(aDataItem: string): String;
    //1 Holt eine Liste aller beteiligten Methoden (Grenzwert, AVG ...) 
    procedure GetMethods(aDataItem: string; aMethodList: TList);
    //1 Setzt das anzuzeigende DataItem 
    procedure SetActiveDataItemIndex(Value: Integer);
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent); reintroduce; virtual;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Gibt die Kalkulationsbasis f�r die Methode zur�ck 
    function GetLenBaseFromDataItem(aDataItem: string): TLenBase;
    //1 Initialisiert die Anzeige 
    procedure Initialize(aAlarm: TQOAlarmView; aMachine: string);
    //1 zeigt ein DataItem im Chart an 
    procedure ShowDataItem(aDataItem: string; aChart: TCustomChart);
    //1 Index des aktuall angezeigten DataItems 
    property ActiveDataItemIndex: Integer read FActiveDataItemIndex write SetActiveDataItemIndex;
    //1 Merkt sich den Anzeige-Modus (gesammter Artikel oder nur eine einzelne Maschine) 
    property AssignShowMode: TAssignShowMode read FAssignShowMode write FAssignShowMode;
    //1 Kalkulationsmodus f�r das angezeigte DataItem 
    property CalcMode: Integer read FCalcMode write FCalcMode;
    //1 Typ der ClassDefects des DataItems (Cut, Uncut, beide, kein) 
    property ClassDefects: Integer read FClassDefects write FClassDefects;
    //1 Displayname des DataItems (ohne ClassDefects) 
    property DataItemDisplayName: String read FDataItemDisplayName write FDataItemDisplayName;
    //1 Itemname des DataItems im globalen Pool 
    property DataItemItemName: String read FDataItemItemName write FDataItemItemName;
    //1 Liste mit den anzuzeigenden DataItems 
    property DataItemList: TStringList read FDataItemList write FDataItemList;
    //1 Maschine deren Daten angezeigt werden soll 
    property Machine: TQOAssignNodeObject read FMachine write FMachine;
    //1 "Eigent�mer" des DataServers 
    property Owner: TComponent read FOwner write FOwner;
    //1 Artikel der Angezeigt wird oder der zur angezeigten Maschine geh�rt 
    property Style: TQOAssignNodeObject read FStyle write FStyle;
    //1 Zeit des ersten Datenpunktes 
    property TimeFrom: TDateTime read FTimeFrom write FTimeFrom;
    //1 Zeit des letzten Datenpunktes 
    property TimeTo: TDateTime read FTimeTo write FTimeTo;
  end;
  
  (*: Klasse:        TQOAlarmView
      Vorg�nger:     TQOIntetrfacedObject
      Kategorie:     GUI
      Kurzbeschrieb: Nimmt einen Alarm von der Datenbank auf 
      Beschreibung:  
                     Die Klasse entspricht einem einzelen Alarm. Das Objekt verf�gt �ber alle relevanten 
                     Daten eines einzelenen Alarms. Dies beinhaltet auch die Daten der Berechnungsmethoden. *)
  TQOAlarmView = class(TQOIntetrfacedObject, IQOVisualize, IQOUserInfo)
  private
    FAdviceMethods: TAdviceMethodSet;
    FAlarmData: TNativeAdoQuery;
    FAlarmDate: TDateTime;
    FAlarmSettings: String;
    FAlarmViews: TQOShiftView;
    FAllDataItems: TStringList;
    FAllMachines: TStringList;
    FAllStyles: TStringList;
    FID: Integer;
    FInfo: String;
    FInitialized: Boolean;
    FLinkAlarmID: Integer;
    FQOVisualize: TQOVisualizeImpl;
    FSaveDate: TDateTime;
    FSavingUser: String;
    FShiftID: Integer;
    //1 Initialisiert die Daten beim ersten Zugriff 
    function GetAlarmData: TNativeAdoQuery;
    //1 Zugriffsmethode f�r die Alarm Settings 
    function GetAlarmSettings: String;
    //1 Initialisiert die Liste mit den DataItems beim ersten Zufgriff 
    function GetAllDataItems: TStringList;
    //1 Initialisiert die Liste mit den Maschinen beim ersten Zufgriff 
    function GetAllMachines: TStringList;
    //1 Initialisiert die Liste mit den Artikel beim ersten Zufgriff 
    function GetAllStyles: TStringList;
    //1 Holt die Zusatzinformationen zum Alarm 
    function GetInfo: String; stdcall;
    //1 Setzt die Zusatzinformationen zum Alarm 
    procedure SetInfo(Value: String); stdcall;
  protected
    //1 Gibt den Index f�r den Alarm zur�ck (Darstellung im Tree) 
    procedure OnGetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer);
  public
    //1 Konstruktor 
    constructor Create;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Ermittelt die DataItems f�r den �bergebenen Artikel 
    function GetDataItemsFromStyle(aStyleName: string; aMachineID: string = ''): String;
    //1 Ermittelt die Namen einer Mashcine anhand der ID 
    function GetMachineNameFromID(aMachineID: integer): String;
    //1 Gibt eine Liste mit den Maschinen zur�ck die am Alarm f�r den �bergebenen Artikel beteiligt waren 
    procedure GetMachinesFromStyle(aMachineList: TStringList; aStyleName: string);
    //1 Liefert den Schichtstart des Alarmdatums als string 
    function GetShiftStart: String;
    //1 Initialisiert die Klasse 
    procedure Init;
    //1 Speichert die Zusatzinforamtionen auf der DB 
    procedure SaveInfo;
    //1 Benachrichtigungsmethoden 
    property AdviceMethods: TAdviceMethodSet read FAdviceMethods write FAdviceMethods;
    //1 Daten f�r alle Artikel, DataItems und Methoden die am Alarm beteiligt waren 
    property AlarmData: TNativeAdoQuery read GetAlarmData;
    //1 Datum an dem der Alarm aufgetreten ist 
    property AlarmDate: TDateTime read FAlarmDate write FAlarmDate;
    //1 Settings der Methoden als XML String 
    property AlarmSettings: String read GetAlarmSettings;
    //1 Schicht in der der Alarm aufgetren ist (Zugriff auf Artikeldaten) 
    property AlarmViews: TQOShiftView read FAlarmViews write FAlarmViews;
    //1 DataItems die am Alarm beteiligt sind 
    property AllDataItems: TStringList read GetAllDataItems;
    //1 Alle am Alarm beteiligten Maschinen 
    property AllMachines: TStringList read GetAllMachines;
    //1 Alle, am Alarm berteiliten Artikel 
    property AllStyles: TStringList read GetAllStyles;
    //1 ID f�r die Datenbank 
    property ID: Integer read FID write FID;
    //1 Zusatzinformationen zum Alarm 
    property Info: String read GetInfo write SetInfo;
    //1 True, wenn die dazugeh�rigen Alarme geladen wurden 
    property Initialized: Boolean read FInitialized write FInitialized;
    //1 Id der Alarm Settings auf der DB 
    property LinkAlarmID: Integer read FLinkAlarmID write FLinkAlarmID;
    //1 Implementiert die Funktionalit�t von IQOVisualize 
    property QOVisualize: TQOVisualizeImpl read FQOVisualize implements IQOVisualize;
    //1 Datum als die Settings des Alarms das letzte Mal gespeichert wurden 
    property SaveDate: TDateTime read FSaveDate write FSaveDate;
    //1 Benutzer der die Settings das letzte Mal ver�ndert hat 
    property SavingUser: String read FSavingUser write FSavingUser;
    //1 ID der Schicht an der der Alarm aufgetreten ist 
    property ShiftID: Integer read FShiftID write FShiftID;
  end;
  
  (*: Klasse:        TQOShiftView
      Vorg�nger:     TQOIntetrfacedObject
      Kategorie:     GUI
      Kurzbeschrieb: Liste der Alarme von der DB 
      Beschreibung:  
                     Diese Klasse entspricht der Klasse 'TQOAlarms' der Kernelklassen.
                     Hier werden die Alarme einer einzelnen Schicht zusammengefasst.
                     Die Daten werden erst von der DB geholt, wenn die Grafik angezeigt 
                     werden soll. *)
  TQOShiftView = class(TQOIntetrfacedObject, IQOVisualize, IQOUserInfo)
  private
    FAlarmDate: TDateTime;
    FAllStyles: TNativeAdoQuery;
    FID: Integer;
    FInfo: String;
    FInitialized: Boolean;
    FQOVisualize: TQOVisualizeImpl;
    FShiftAlarmType: TShiftAlarmType;
    FShiftID: Integer;
    //1 Recordset mit den Daten aller am Alarm beteiligten Artikel 
    function GetAllStyles: TNativeAdoQuery;
    //1 Holt die Zusatzinformationen zum Alarm 
    function GetInfo: String; stdcall;
    //1 Setzt die Zusatzinformationen zum Alarm 
    procedure SetInfo(Value: String); stdcall;
  protected
    mAlarmViews: TList;
    //1 ERmittelt die anzahl der Alarme 
    function GetAlarmViewCount: Integer;
    //1 Bietet den Zugriff auf einen einzelnen Alarm 
    function GetAlarmViews(aIndex: Integer): TQOAlarmView;
    //1 Gibt das Image f�r die Darstellung zur�ck 
    procedure OnGetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer);
  public
    //1 Konstruktor 
    constructor Create; virtual;
    //1 Destruktor 
    destructor Destroy; override;
    //1 F�gt einen neuen Alarm zur Liste hinzu 
    function Add(aAlarmView: TQOAlarmView): Integer; virtual;
    //1 Kopiert die Liste mit den Alarmen 
    procedure Assign(Source: TPersistent); overload; override;
    //1 Kopiert die Liste mit den Alarmen 
    procedure Assign(aSourceAlarm: TQOAlarms); reintroduce; overload; virtual;
    //1 L�scht alle Alarme aus der Lsite 
    procedure Clear;
    //1 L�scht einen Alarm aus der Liste 
    procedure Delete(aIndex: integer); virtual;
    //1 Kennzeichnet die Schicht wieder als automatisch generiert 
    procedure DeleteFromArchive;
    //1 Gibt die Style ID f�r den gew�nschten Artikelnamen zur�ck 
    function GetStyleIDByName(aStyleName: string; var aStyleID: integer): Boolean;
    //1 Gibt den Index eines bestimmten Alarmes in der Liste zur�ck 
    function IndexOf(aAlarmView: TQOAlarmView): Integer; virtual;
    //1 L�dt die Alarme von der Datenbank 
    procedure Init;
    //1 Kennzeichnet die Schicht als dem Archiv zugeh�rig 
    procedure SaveInArchive;
    //1 Speichert die Zusatzinforamtionen auf der DB 
    procedure SaveInfo;
    //1 Datum an dem der Alarm aufgetreten ist 
    property AlarmDate: TDateTime read FAlarmDate write FAlarmDate;
    //1 Anzahl der Alarme inm der Liste 
    property AlarmViewCount: Integer read GetAlarmViewCount;
    //1 Bietet den Zugriff auf einen einzelnen Alarm 
    property AlarmViews[aIndex: Integer]: TQOAlarmView read GetAlarmViews;
    //1 Recordset mit den Daten aller am Alarm beteiligten Artikel 
    property AllStyles: TNativeAdoQuery read GetAllStyles;
    //1 ID f�r die Datenbank 
    property ID: Integer read FID write FID;
    //1 Zusatzinformationen zur Schicht 
    property Info: String read GetInfo write SetInfo;
    //1 True, wenn die dazugeh�rigen Alarme geladen wurden 
    property Initialized: Boolean read FInitialized write FInitialized;
    //1 Implementiert die Funktionalit�t von IQOVisualize 
    property QOVisualize: TQOVisualizeImpl read FQOVisualize implements IQOVisualize;
    //1 Art wie die Alarme f�r die Schicht erzeugt wurden (automatisch, manuell, archiv) 
    property ShiftAlarmType: TShiftAlarmType read FShiftAlarmType write FShiftAlarmType;
    //1 ID der Schicht des Alarmdatums 
    property ShiftID: Integer read FShiftID write FShiftID;
  end;
  
  (*: Klasse:        TQOShiftSetObserver
      Vorg�nger:     TComponent
      Kategorie:     No category
      Kurzbeschrieb: Objekt f�r einen Observer der Klasse TQOAlarmSets 
      Beschreibung:  
                     �ber den Observer kann sich ein Objekt bei einem Objekt der Klasse 'TQOAlarmSets' registrieren.
                     Jeder registrierte Observer wird benachrichtigt, wenn ein Event auftritt. *)
  TQOShiftSetObserver = class(TComponent)
  private
    FEnabled: Boolean;
    FOnDeleteAlarm: TNotifyEvent;
    FOnShiftsLoaded: TNotifyEvent;
  public
    //1 Wird aufgerufen, wenn ein Alarm gel�scht wurde 
    procedure DeleteAlarm(aShift: TQOShiftView);
    //1 Wird aufgerufen, sobald die Schichten von der DB geladen wurden 
    procedure ShiftsLoaded;
  published
    //1 The Enabled property switches notification dispatching on and off. 
    property Enabled: Boolean read FEnabled write FEnabled;
    //1 Wird aufgerufen, wenn eine Schicht aus der Liste gel�scht wird 
    property OnDeleteAlarm: TNotifyEvent read FOnDeleteAlarm write FOnDeleteAlarm;
    //1 Wird aufgetrufen sobald die Schichten von der DB geladen wurden 
    property OnShiftsLoaded: TNotifyEvent read FOnShiftsLoaded write FOnShiftsLoaded;
  end;
  
  (*: Klasse:        TQOShiftSet
      Vorg�nger:     TPersistent
      Kategorie:     GUI
      Kurzbeschrieb: Liste aller Alarm Sets 
      Beschreibung:  
                     Pro Alarm Set k�nnen mehrere Alarme registriert sein.
                     Ein Alarm Set wird erstellt, wenn am Ende einer Schicht die 
                     Offlimits berechnet werden, oder wenn ein Offlimit von Hand
                     ausgel�st wird (Evalute). *)
  TQOShiftSet = class(TPersistent)
  private
    FObservers: TList;
    FOldestShiftID: Integer;
  protected
    mShifts: TList;
    //1 singleton 
    constructor CreateInstance;
    //1 singleton 
    class function AccessInstance(Request: Integer): TQOShiftSet;
    //1 Benachrichtigt alle registrierten Observer �ber das L�schen eines Alarms 
    procedure DeleteAlarm(aShift: TQOShiftView);
    //1 Zugriffsmethode um die Anzahl Schichten zu ermitteln 
    function GetShiftCount: Integer;
    //1 Zugriffsmethode um auf eine Schicht zuzugreifen 
    function GetShifts(aIndex: Integer): TQOShiftView;
  public
    //1 singleton 
    constructor Create;
    //1 Destruktor 
    destructor Destroy; override;
    //1 F�gt eine Schicht zur Liste hinzu 
    function Add(aShift: TQOShiftView): Integer; virtual;
    //1 Kopiert die gesammte Liste 
    procedure Assign(Source: TPersistent); override;
    //1 L�scht alle Schichten 
    procedure Clear(aShiftAlarmTypeSet: TShiftAlarmTypeSet = []);
    //1 L�scht eine Schicht 
    procedure Delete(aIndex: integer); virtual;
    //1 Gibt den Index einer Schicht zur�ck 
    function IndexOf(aShift: TQOShiftView): Integer; virtual;
    //1 F�gt eine Schicht an einem Index in dei Liste ein 
    procedure Insert(aIndex: integer; aShift: TQOShiftView);
    //1 singleton 
    class function Instance: TQOShiftSet;
    //1 L�dt erstmal nur den Inhalt der Tabelle 't_QO_Caught_Alarm' von der DB 
    procedure LoadAlarmRootFromDB(aCount: integer);
    //1 Registriert einen Observer f�r die Benachrichtigung eines Events 
    procedure RegisterObserver(Observer: TQOShiftSetObserver);
    //1 singleton 
    class procedure ReleaseInstance;
    //1 Entfernt den Observer wieder aus der Liste. Der Besitzer des Observers wird in Zukunft nicht mehr benachrichtigt
    procedure UnregisterObserver(Observer: TQOShiftSetObserver);
    //1 �lteste Schicht f�r die noch Daten vorhanden sein k�nnten 
    property OldestShiftID: Integer read FOldestShiftID write FOldestShiftID;
    //1 Anzahl Schichten 
    property ShiftCount: Integer read GetShiftCount;
    //1 Liste mit den Schichten die gespeichert sind 
    property Shifts[aIndex: Integer]: TQOShiftView read GetShifts;
  end;
  
  (*: Klasse:        TQONodeObject
      Vorg�nger:     TObject
      Kategorie:     GUI
      Kurzbeschrieb: Objekt das einen Knoten repr�sentiert der kein Objekt "hat" 
      Beschreibung:  
                     - *)
  TQONodeObject = class(TObject, IQOVisualize)
  private
    FQOVisualize: TQOVisualizeImpl;
  protected
    //1 Gibt den Imageindex zur�ck der f�r die Darstellung gebraucht wird (Tree) 
    procedure OnGetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer); virtual;
  public
    //1 Konstruktor 
    constructor Create; virtual;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Implementiert die Funktionalit�t von IQOVisualize 
    property QOVisualize: TQOVisualizeImpl read FQOVisualize implements IQOVisualize;
  end;
  
  (*: Klasse:        TQOAssignNodeObject
      Vorg�nger:     TQONodeObject
      Kategorie:     GUI
      Kurzbeschrieb: Objekt das einen Knoten repr�sentiert der kein eigenes Objekt "hat" (Artikel/Maschine) 
      Beschreibung:  
                     - *)
  TQOAssignNodeObject = class(TQONodeObject)
  private
    FAlarmView: TQOAlarmView;
    FNodeObjectType: TQONodeObjectType;
  protected
    FLinkID: Integer;
    FProdIDList: TProdIDList;
    //1 Gibt das Bitmap f�r den entsprechenden Typ zur�ck 
    procedure OnGetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer); override;
  public
    //1 Konstruktor 
    constructor Create; override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Erstellt die Liste mit den Produktionsgruppen 
    procedure BuildProdIDList(aStyleID: integer = -1);
    //1 Referenz auf den �Ubergeordneten Alarm 
    property AlarmView: TQOAlarmView read FAlarmView write FAlarmView;
    //1 ID des Original Artikels/Maschine auf der DB (t_style/t_machine) 
    property LinkID: Integer read FLinkID write FLinkID;
    //1 Txp des Objektes (Artikel oder Maschine) 
    property NodeObjectType: TQONodeObjectType read FNodeObjectType write FNodeObjectType;
    //1 Liste die die einzelnen ProdIDs enth�lt 
    property ProdIDList: TProdIDList read FProdIDList write FProdIDList;
  end;
  
  (*: Klasse:        TProdID
      Vorg�nger:     TPersistent
      Kategorie:     No category
      Kurzbeschrieb: Klasse f�r eine einzelnes Lot 
      Beschreibung:  
                     Hier werden die Informationen die die Produktionsgruppen
                     beschreiben. *)
  TProdID = class(TPersistent)
  private
    FFirstSpindle: Integer;
    FLastSpindle: Integer;
    FMachineID: Integer;
    FMachineName: String;
    FProdID: Integer;
    FProdName: String;
    FStyleID: Integer;
    FStyleName: String;
  public
    //1 Erste Spindel der Produktionsgruppe 
    property FirstSpindle: Integer read FFirstSpindle write FFirstSpindle;
    //1 Letzte Spindel der Produktionsgruppe 
    property LastSpindle: Integer read FLastSpindle write FLastSpindle;
    //1 ID der Maschine auf der die Produktionsgruppe lief 
    property MachineID: Integer read FMachineID write FMachineID;
    //1 Name der Maschine auf der die Produktionsgruppe lief 
    property MachineName: String read FMachineName write FMachineName;
    //1 ID des Lots 
    property ProdID: Integer read FProdID write FProdID;
    //1 Name des Lots 
    property ProdName: String read FProdName write FProdName;
    //1 ID des Artikels der mit diesem Lot produziert wurde 
    property StyleID: Integer read FStyleID write FStyleID;
    //1 Name des Artikels der mit diesem Lot produziert wurde 
    property StyleName: String read FStyleName write FStyleName;
  end;
  
  (*: Klasse:        TProdIDList
      Vorg�nger:     TPersistent
      Kategorie:     No category
      Kurzbeschrieb: Liste mit Produktionsgruppen f�r die Anzeige im Chart 
      Beschreibung:  
                     - *)
  TProdIDList = class(TPersistent)
  protected
    FOnDeleteProdID: TNotifyEvent;
    mProdIDs: TList;
    //1 Liefert die Anzahl der Listeneintr�gen 
    function GetProdIDCount: Integer;
    //1 Liefert die ProdID mit dem entsprechenden Index 
    function GetProdIDs(aIndex: Integer): TProdID;
  public
    //1 Konstruktor 
    constructor Create; virtual;
    //1 Destruktor 
    destructor Destroy; override;
    //1 F�gt eine ProdID zur Liste hinzu 
    function Add(aProdID: TProdID): Integer; virtual;
    //1 Kopiert die gesammte Liste (inkl. ProdIDs) 
    procedure Assign(Source: TPersistent); override;
    //1 L�scht die Liste (inkl. freigabe TProdID) 
    procedure Clear;
    //1 L�scht eine einzelne ProdID 
    procedure Delete(aIndex: integer); virtual;
    //1 Gibt den Index des Objektes mit der entsprechenden ProdID in der Liste 
    function IndexOf(aProdID: integer): Integer; overload; virtual;
    //1 Gibt den Index des Objektes in der Liste 
    function IndexOf(aProdID: TProdID): Integer; overload; virtual;
    //1 Wird aufgerufen wenn eine ProdID aus der Liste gel�scht wird 
    property OnDeleteProdID: TNotifyEvent read FOnDeleteProdID write FOnDeleteProdID;
    //1 Anzahl der ProdIDs 
    property ProdIDCount: Integer read GetProdIDCount;
    //1 Array-Zugriff auf die einzelnen ProdIDs 
    property ProdIDs[aIndex: Integer]: TProdID read GetProdIDs; default;
  end;
  

// Bereitet ein Datum zur Anzeige im Tree auf
function DateTimeToTreeCaption(aDate: TDateTime): String;

implementation
uses
  mmMBCS, mmcs, QODef, adoint, MMUGlobal, QOGUIShared, typinfo, IvDictio, Series;

(*-----------------------------------------------------------
  Gibt die Style ID f�r den gew�nschten Artikelnamen
-------------------------------------------------------------*)
resourcestring
  rsStyleNotExists = '(*)Keine Artikelinfos'; //ivlm
  rsMachineNotExists = '(*)Keine Maschineninfos'; //ivlm

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TAnalyzeChartDataServer
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TAnalyzeChartDataServer.Create(aOwner: TComponent);
begin
  inherited Create;

//: ----------------------------------------------
  FOwner := aOwner;
  
  mSeriesList := TSeriesList.Create;
  FDataItemList := TStringList.Create;
  mAlarmData := TNativeADOQuery.Create;
end;// TAnalyzeChartDataServer.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TAnalyzeChartDataServer
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TAnalyzeChartDataServer.Destroy;
begin
  FreeAndNil(FDataItemList);
  FreeAndNil(mAlarmData);
  FreeAndNil(mSeriesList);

//: ----------------------------------------------
  inherited Destroy;
end;// TAnalyzeChartDataServer.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetDataItemFieldInformation
 *  Klasse:           TAnalyzeChartDataServer
 *  Kategorie:        No category 
 *  Argumente:        (aDataItem)
 *
 *  Kurzbeschreibung: holt alle Informationen f�r ein DataItem aus dem Recordset
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TAnalyzeChartDataServer.GetDataItemFieldInformation(aDataItem: string): String;
begin
  result := '';
  mAlarmData.SendRecordsetToCodeSite('TAnalyzeChartDataServer.Alarm Data', 0);
  // Die Datenmenge nach dem DataItem filtern und dann aus dem Feld 'dfn' den Filedname herauslesen
  mAlarmData.Filter(Format('%s = ''%s''', [cDataItemDisplayName, StringReplace(aDataItem, '''', '''''', [rfReplaceAll])]));
  if not(mAlarmData.EOF) then begin
    result := mAlarmData.FieldByName(cDataItemFieldName).AsString;
    // Zusatzinformationen aus der Tabelle auslesesn die zum DataItem geh�ren
    FCalcMode           := mAlarmData.FieldByName(cCalcModeFieldName).AsInteger;
    mLenBase            := TLenBase(mAlarmData.FieldByName(cLenBaseFieldName).AsInteger);
    ClassDefects        := mAlarmData.FieldByName(cClassDefectsFieldName).AsInteger;
    DataItemItemName    := mAlarmData.FieldByName(cDataItemItemName).AsString;
    DataItemDisplayName := mAlarmData.FieldByName(cDataItemDisplayName).AsString;
  end;// if not(mAlarmData.EOF) then begin
end;// TAnalyzeChartDataServer.GetDataItemFieldInformation cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetLenBaseFromDataItem
 *  Klasse:           TAnalyzeChartDataServer
 *  Kategorie:        No category 
 *  Argumente:        (aDataItem)
 *
 *  Kurzbeschreibung: Gibt die Kalkulationsbasis f�r die Methode zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TAnalyzeChartDataServer.GetLenBaseFromDataItem(aDataItem: string): TLenBase;
begin
  result := lb100km;
  
  try
    with mAlarmData do begin
      // L�sst nur DS zur�ck die dem anzuzeigenden DataItem entsprechen
      Filter(Format('%s = ''%s''', [cDataItemDisplayName, PrepareStringForSQL(aDataItem, false)]));
      FindFirst;
      if not(EOF) then
        result := TLenBase(FieldByName(cLenBaseFieldName).AsInteger);
    end;// with mAlarmData do begin
  finally
    // Datenmenge wieder "zur�cksetzen"
    mAlarmData.Filter('');
  end;// try finally
end;// TAnalyzeChartDataServer.GetLenBaseFromDataItem cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMethods
 *  Klasse:           TAnalyzeChartDataServer
 *  Kategorie:        No category 
 *  Argumente:        (aDataItem, aMethodList)
 *
 *  Kurzbeschreibung: Holt eine Liste aller beteiligten Methoden (Grenzwert, AVG ...)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TAnalyzeChartDataServer.GetMethods(aDataItem: string; aMethodList: TList);
begin
  try
    with mAlarmData do begin
      // L�sst nur DS zur�ck die dem anzuzeigenden DataItem entsprechen
      Filter(Format('%s = ''%s''', [cDataItemDisplayName, PrepareStringForSQL(aDataItem, false)]));
      FindFirst;
      while not(EOF) do begin
        // Wenn die Methode noch nicht erfasst wurde Methode hinzuf�gen
        if aMethodList.IndexOf(Pointer(FieldByName(cMethodFieldName).AsInteger)) = -1 then
          aMethodList.Add(Pointer(FieldByName(cMethodFieldName).AsInteger));
        Next;
      end;// while not(EOF) do begin
    end;// with mAlarmData do begin
  finally
    // Datenmenge wieder "zur�cksetzen"
    mAlarmData.Filter('');
  end;// try finally
end;// TAnalyzeChartDataServer.GetMethods cat:No category

//:-------------------------------------------------------------------
(*: Member:           Initialize
 *  Klasse:           TAnalyzeChartDataServer
 *  Kategorie:        No category 
 *  Argumente:        (aAlarm, aMachine)
 *
 *  Kurzbeschreibung: Initialisiert die Anzeige
 *  Beschreibung:     
                      Hier wird zB. der Datenpool f�r die Anzeige zusammengestellt.
                      Ist die Artikelsicht gew�nscht, dann muss aMachine = '' sein. F�r die Maschinensicht
                      muss die Maschinen ID angegben werden.
 --------------------------------------------------------------------*)
procedure TAnalyzeChartDataServer.Initialize(aAlarm: TQOAlarmView; aMachine: string);
var
  xStyleID: Integer;
begin
  FDataItemList.Clear;

//: ----------------------------------------------
  // Alarm f�r die weitere Verwendung sichern
  mAlarm := aAlarm;
  
  // Die StyleID steht in jedem ProdID-Objekt zu Verf�gung
  if FStyle.ProdIDList.ProdIDCount > 0 then begin
    // Artikel ID abholen
    xStyleID := FStyle.ProdIDList[0].StyleID;
  
    // Je nach Anzeigemodus die Datenmege filtern
    case FAssignShowMode of
      smMachine: mAlarm.AlarmData.Filter(Format('%s = %d AND %s = %s', [cStyleFieldName, xStyleID, cMachineFieldName, aMachine]));
      smStyle  : mAlarm.AlarmData.Filter(Format('%s = %d', [cStyleFieldName, xStyleID]));
    end;// case mAssignShowMode of
  
    // Nur die gefilterten Datens�tze �bernehmen (so kann sp�ter besser gefiltert werden)
    mAlarmData.AsXML := mAlarm.AlarmData.AsXML;
    // Den Filter im Original DataSet wieder entfernen
    mAlarm.AlarmData.Filter('');
  
    // Ermittelt welche DataItems angezeigt werden m�ssen
    with FDataItemList do begin
      // DataItems abfragen ...
      CommaText := mAlarm.GetDataItemsFromStyle(FStyle.QOVisualize.Caption, aMachine);
      FActiveDataItemIndex := -1;
    end;// with mDataItemList do begin
  end;// if mStyle.ProdIDList.ProdIDCount > 0 then begin
end;// TAnalyzeChartDataServer.Initialize cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetActiveDataItemIndex
 *  Klasse:           TAnalyzeChartDataServer
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt das anzuzeigende DataItem
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TAnalyzeChartDataServer.SetActiveDataItemIndex(Value: Integer);
begin
end;// TAnalyzeChartDataServer.SetActiveDataItemIndex cat:No category

//:-------------------------------------------------------------------
(*: Member:           ShowDataItem
 *  Klasse:           TAnalyzeChartDataServer
 *  Kategorie:        No category 
 *  Argumente:        (aDataItem, aChart)
 *
 *  Kurzbeschreibung: zeigt ein DataItem im Chart an
 *  Beschreibung:     
                      Zuerst wird der Artikel eingef�gt, dann die Grenzwerte und erst am 
                      Schluss alle beteiligten Produktionsgruppen.
 --------------------------------------------------------------------*)
procedure TAnalyzeChartDataServer.ShowDataItem(aDataItem: string; aChart: TCustomChart);
var
  xMethodList: TList;
  xLineSerie: TQOLineSeries;
  i: Integer;
  xValue: Extended;
  xInitialSeriesCount: Integer;
  xProdIDList: TProdIDList;
  xLastShiftStart: TDateTime;
  xMinVal: extended;
  xMaxVal: extended;
  xDataItemUnit: string;
  xSeriesDetailInfo: PSeriesDetailInfoRec;
  
  (*-----------------------------------------------------------
    F�gt eine neue Serie zum Chart hinzu
  -------------------------------------------------------------*)
  function AddLineSerie (aName: string): TQOLineSeries;
  begin
    // Die neue Serie erzeugen
    result := TQOLineSeries.Create(aChart);
    // ... und zur Internen Liste hinzuf�gen
    mSeriesList.Add(result);
    with result do begin
      Title := aName;
      // Die Serie wird im DataItemChart dargestellt
      ParentChart := aChart;
      XValues.DateTime := true;
      Pointer.Visible := false;
      Pointer.Style := psRectangle;
      // Die Farbe der Serie h�ngt von der Position in der Liste ab
      SeriesColor := cAnalyzeChartColors[mSeriesList.IndexOf(result) mod cMaxAnalyzeColor];
  
      ChartItemType := citPointer;
      ItemOwned := true;
    end;// with result do begin
  end;// function AddLineSerie: TQOLineSeries;
  
begin
  // Untere Achse des Chart definieren
  aChart.BottomAxis.DateTimeFormat := Format('%s %s', [ShortDateFormat, ShortTimeFormat]);
  aChart.BottomAxis.Increment := DateTimeStep[dtSixHours];
  aChart.BottomAxis.LabelsMultiLine := True;

//: ----------------------------------------------
  // Initialisieren
  xMethodList := nil;
  
  try
    // Feldname f�r das Artikel-Recordset ermitteln (f�llt auch mCalcMode ab)
    mDataItemFieldName := GetDataItemFieldInformation(aDataItem);
    // Titel des Chart und Titel der Achse zeigen das DataItem
    aChart.Title.Text.Text := TranslateDataItemDisplayName(DataItemItemName, ClassDefects);
    // Jetzt kann auch der String berechnet werden, da das DataItem bekannt ist
    xDataItemUnit := GetLenBaseString(GetLenBaseFromDataItem(aDataItem), FCalcMode);
    if xDataItemUnit <> '' then
      // SFI hat zB. keine Einheit
      xDataItemUnit := ' [' + xDataItemUnit + ']';
    aChart.LeftAxis.Title.Caption := TranslateDataItemDisplayName(DataItemItemName, ClassDefects) + xDataItemUnit;
  
    // Alle Serien l�schen und neu aufbauen
    mSeriesList.Clear;

//: ----------------------------------------------
  // ---------------- Artikel --------------------
  
    // Nimmt den �ltesten Schichtstart auf (F�r den FixBorder ben�tigt - siehe unten)
    xLastShiftStart := 1;
  
    // F�gt eine neue Serie zum Chart hinzu
    xLineSerie := AddLineSerie(FStyle.QOVisualize.Caption);
  
    // Datenmenge nach dem Artikelname filtern
    mAlarm.AlarmViews.AllStyles.Filter(Format('c_style_name = ''%s''', [PrepareStringForSQL(FStyle.QOVisualize.Caption, false)]));
    FTimeFrom := now;
    FTimeTo := 5000;
    with mAlarm.AlarmViews.AllStyles do begin
      // F�r jede Schicht einen Datenpunkt zum Tree hinzuf�gen
      while not(EOF) do begin
        // Im Recordset sind Absolute Werte gespeichert
        xValue := calcAllValues(mLenBase,                                  // L�ngenbasis
                                FieldByName(mDataItemFieldName).AsFloat,   // zu rechnender Wert
                                FieldByName('c_len').AsFloat,              // Produzierte L�nge
                                FCalcMode,                                 // Kalkulationsmodus (aus dem Konstantenarray)
                                FieldByName('c_SFICnt').AsFloat,           // Anzahl der Werte auf denen der SFI beruht
                                FieldByName('c_AdjustBase').AsFloat,       // AdjustBase f�r SFI-Berechnungen
                                FieldByName('c_AdjustBaseCnt').AsFloat,    // Anzahl aufsummierter Werte
                                1);                                        // Basiert auf einem Wert (F�r QO unwichtig da Nutzeffekt)
        // DetailInfos
        try
          new(xSeriesDetailInfo);
          with xSeriesDetailInfo^ do begin
            DetailInfoLevel := ilStyle;
            StyleName       := FStyle.QOVisualize.Caption;
            ShiftStart      := FieldByName('c_shift_start').AsDateTime;
            Value           := xValue;
            ValueUnit       := xDataItemUnit;
            SerieTitle      := FStyle.QOVisualize.Caption;
          end;// with xSeriesDetailInfo do begin
          if FieldByName('c_shift_start').AsDateTime < FTimeFrom then
            FTimeFrom := FieldByName('c_shift_start').AsDateTime;
  
          if FieldByName('c_shift_start').AsDateTime > FTimeTo then
            FTimeTo := FieldByName('c_shift_start').AsDateTime;
  
          // Datenpunkt zur Serie hinzuf�gen
          xLineSerie.AddXY(FieldByName('c_shift_start').AsDateTime, xValue, '', clTeeColor, xSeriesDetailInfo);
        except
          on e: Exception do
            FreeAndNil(xSeriesDetailInfo);
        end;// try except
  
        // Speichert das �lteste Datum f�r das Artikeldaten vorhanden sind (siehe Grenzwert - cmFix)
        if (xLastShiftStart <= 1) or (xLastShiftStart > FieldByName('c_shift_start').AsDateTime) then
          xLastShiftStart := FieldByName('c_shift_start').AsDateTime;
        next;
      end;// while not(EOF) do begin
    end;// with mAlarm.AlarmViews.AllStyles do begin

//: ----------------------------------------------
  // ---------------- Methoden (Grenzwerte) --------------------
  
    // Liste die alle registrierten Grenzwerte f�r dieses DataItem aufnimmt
    xMethodList := TList.Create;
    // Ermittelt alle Methoden die im Recordset vorkommen
    GetMethods(aDataItem, xMethodList);
  
    // F�r jede Methode den Grenzwert einzeichnen
    for i := 0 to xMethodList.Count - 1 do begin
      // Neue Serie in das Chart einf�gen
      xLineSerie := AddLineSerie(Translate(cCalculationMethods[TCalculationMethod(Integer(xMethodList[i]))].Name));
      // Initialisieren
      xValue := 0;
      with mAlarmData do begin
        // Nach DataItem und Methode filtern
        Filter(Format('%s = ''%s'' AND %s = %d', [cDataItemFieldName, PrepareStringForSQL(mDataItemFieldName, false), cMethodFieldName, Integer(xMethodList[i])]));
        FindFirst;
        while not(EOF) do begin
          // Grenzwert abfragen ...
          xValue := FieldByName(cLimitValueFieldName).AsFloat;
  
          // DetailInfos
          try
            new(xSeriesDetailInfo);
            with xSeriesDetailInfo^ do begin
              StyleName       := FStyle.QOVisualize.Caption;
              DetailInfoLevel := ilCalcMethod;
              ShiftStart      := FieldByName(cShiftStartFieldName).AsDateTime;
              Value           := xValue;
              ValueUnit       := xDataItemUnit;
              SerieTitle      := xLineSerie.Title;
            end;// with xSeriesDetailInfo do begin
            // ... und eintragen
            xLineSerie.AddXY(FieldByName(cShiftStartFieldName).AsDateTime, xValue, '', clTeeColor, xSeriesDetailInfo);
          except
            on e: Exception do
              FreeAndNil(xSeriesDetailInfo);
          end;// try except
          next;
        end;// while not(EOF) do begin
      end;// with mAlarmData do begin
  
      // Der Grenzwert f�r den FixBorder wird �ber das gesammte Diagramm gezeichnet (erster Wert des Artikels)
      if TCalculationMethod(Integer(xMethodList[i])) = cmFix then begin
        if xLastShiftStart > 1 then
          xLineSerie.AddXY(xLastShiftStart, xValue, '', clTeeColor);
       end;// if TCalculationMethod(Integer(xMethodList[i]) = cmFix then begin
    end;// for i := 0 to xLotList.Count - 1 do begin

//: ----------------------------------------------
  // ---------------- Partien --------------------
    // Je nach Anzeigemodus die Liste des entsprechenden Objektes �bernehmen
    if FAssignShowMode = smMachine then
      xProdIDList := FMachine.ProdIDList
    else
      xProdIDList := FStyle.ProdIDList;
  
    // F�r die Bestimmung der Farbe der neuen Serien feststellen wieviele Serien existieren
    xInitialSeriesCount := mSeriesList.SerieCount;
  
    // F�r jede registrierte ProdID eine neue Serie erzeugen
    for i := 0 to xProdIDList.ProdIDCount - 1 do begin
      // Neu Serie im Chart darstellen
      xLineSerie := AddLineSerie(xProdIDList[i].ProdName);
  
      // Farbe der Serie bestimmen
      if assigned(FStyle) then begin
        (* Je nach Position in der Liste wird die Farbe bestimmt.
           Damit wird sichergestellt, dass auch bei mehreren Maschinen und mehreren
           ProdID's pro Maschine die Farbe der Produktionsgruppe immer dieselbe
           ist - Sowohl in der �bersicht (Artikel) sowie in der Detailansicht (Maschine) *)
        if FStyle.ProdIDList.IndexOf(xProdIDList[i].ProdID) >= 0 then
          xLineSerie.SeriesColor := cAnalyzeChartColors[(FStyle.ProdIDList.IndexOf(xProdIDList[i].ProdID) + xInitialSeriesCount) mod cMaxAnalyzeColor]
        else
          xLineSerie.SeriesColor := cAnalyzeChartColors[mSeriesList.SerieCount mod cMaxAnalyzeColor];
      end;// if assigned(mStyle) then begin
  
      // Auch dieese Serien werden etwas dicker dargestellt
      xLineSerie.LinePen.Width := 2;
      with mAlarmData do begin
        // Der Titel wird sp�ter bestimmt
        xLineSerie.Title := '';
        // Nach DataItem und nach ProdID filtern
        Filter(Format('%s = ''%s'' AND %s = %d', [cDataItemFieldName, PrepareStringForSQL(mDataItemFieldName, false), cProdGroupFieldName, xProdIDList[i].ProdID]));
        FindFirst;
        // Titel und Name der Serie feststellen
        if not(EOF) then begin
          xLineSerie.Title := Format('%s (%s [%d - %d] (#%d))', [// Name der Produktionsgruppe
                                                                FieldByName(cProdGroupNameFieldName).AsString,
                                                                // Name der Maschine
                                                                FieldByName(cMachineNameFieldName).AsString,
                                                                // Erste Spindel
                                                                FieldByName(cFirstSpdFieldName).AsInteger,
                                                                // Letzte Spindel
                                                                FieldByName(cLastSpindleFieldName).AsInteger,
                                                                // Anzahl zusammenh�ngender Schichten in der das Lot im Alarm war
                                                                GetProdIDTurnCount(mAlarmData.FieldByName(cShiftFieldName).AsInteger, xProdIDList[i].ProdId)]);
        end else begin
          // Wenn keine Daten vorhanden sind, dann muss die Serie nicht angezeigt werden
          xLineSerie.Active := false;
        end;// if not(EOF) then begin
        while not(EOF) do begin
          // Im Recordset werden absolute Werte gespeichert, deshalb die Werte zuerst berechnen ...
          xValue := calcAllValues(mLenBase,                                  // L�ngenbasis
                                  FieldByName(cValueFieldName).AsFloat,      // zu rechnender Wert
                                  FieldByName(cLenFieldName).AsFloat,        // Produzierte L�nge
                                  FCalcMode,                                 // Kalkulationsmodus (aus dem Konstantenarray)
                                  FieldByName(cSFICntFieldName).AsFloat,     // Anzahl der Werte auf denen der SFI beruht
                                  FieldByName(cAdjustBaseFieldName).AsFloat, // AdjustBase f�r SFI-Berechnungen
                                  FieldByName(cAdjustBaseCntFieldName).AsFloat, // Anzahl aufsummierter Werte
                                  1);                                        // Basiert auf einem Wert (F�r QO unwichtig da Nutzeffekt)
  
          // DetailInfos
          try
            new(xSeriesDetailInfo);
            with xSeriesDetailInfo^ do begin
              DetailInfoLevel := ilLot;
              StyleName       := FStyle.QOVisualize.Caption;
              ShiftStart      := FieldByName(cShiftStartFieldName).AsDateTime;
              Value           := xValue;
              LotName         := xLineSerie.Title;
              ValueUnit       := xDataItemUnit;
              SerieTitle      := xLineSerie.Title;
            end;// with xSeriesDetailInfo do begin
            // ... und dann zur Serie hinzuf�gen
            xLineSerie.AddXY(FieldByName(cShiftStartFieldName).AsDateTime, xValue, '', xLineSerie.SeriesColor, xSeriesDetailInfo);
          except
            on e: Exception do
              FreeAndNil(xSeriesDetailInfo);
          end;// try except
  
          next;
        end;// while not(EOF) do begin
      end;// with mAlarmData do begin
    end;// for i := 0 to xProdIDList.ProdIDCount - 1 do begin

//: ----------------------------------------------
  finally
    xMethodList.Free;
  end;

//: ----------------------------------------------
  xMinVal := mSeriesList.MinYValue;
  xMaxVal := mSeriesList.MaxYValue;
  xMinVal := xMinVal - ((xMaxVal - xMinVal) * 0.05);
  xMaxVal := xMaxVal + ((xMaxVal - xMinVal) * 0.05);
  
  // Neue Achsenbegrenzungen setzen
  aChart.LeftAxis.SetMinMax(xMinVal, xMaxVal);
end;// TAnalyzeChartDataServer.ShowDataItem cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOAlarmView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOAlarmView.Create;
begin
  inherited Create;

//: ----------------------------------------------

//: ----------------------------------------------
  FQOVisualize := TQOVisualizeImpl.Create;

//: ----------------------------------------------
  FAlarmData := TNativeAdoQuery.Create;
  FQOVisualize.OnGetImageIndex := OnGetImageIndex;
  FQOVisualize.EditorEnabled := true;
end;// TQOAlarmView.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TQOAlarmView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TQOAlarmView.Destroy;
begin
  FreeAndNil(FQOVisualize);

//: ----------------------------------------------
  FreeAndNil(FAlarmData);
  FreeAndNil(FAllStyles);
  FreeAndNil(FAllMachines);
  FreeAndNil(FAllDataItems);

//: ----------------------------------------------
  inherited Destroy;
end;// TQOAlarmView.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAlarmData
 *  Klasse:           TQOAlarmView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Initialisiert die Daten beim ersten Zugriff
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarmView.GetAlarmData: TNativeAdoQuery;
begin
  init;

//: ----------------------------------------------
  Result := FAlarmData;
end;// TQOAlarmView.GetAlarmData cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAlarmSettings
 *  Klasse:           TQOAlarmView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r die Alarm Settings
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarmView.GetAlarmSettings: String;
begin
  init;

//: ----------------------------------------------
  Result := FAlarmSettings;
end;// TQOAlarmView.GetAlarmSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAllDataItems
 *  Klasse:           TQOAlarmView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Initialisiert die Liste mit den DataItems beim ersten Zufgriff
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarmView.GetAllDataItems: TStringList;
var
  xDB: TAdoDBAccess;
  
  const
    cGetDataItems = 'SELECT c_dataitem_name ' +
                    'FROM t_QO_involved_dataitem ' +
                    'WHERE c_QO_alarm_data_id = :c_QO_alarm_data_id';
  
begin
  if not(assigned(FAllDataItems)) then begin
    FAllDataItems := TStringList.Create;
    FAllDataItems.Sorted := true;
    FAllDataItems.Duplicates := dupIgnore;
    xDB := TAdoDBAccess.Create(1);
    try
      xDB.Init;
      with xDB.Query[0] do begin
        SQL.Text := cGetDataItems;
        ParamByName('c_QO_alarm_data_id').AsInteger := FID;
        open;
        while not(EOF) do begin
          FAllDataItems.Add(Fields[0].AsString);
          next;
        end;// while not(EOF) do begin
      end;// with xDB.Query[0] do begin
    finally
      xDB.Free;
    end;// try finally
  end;// if not(assigned(FAllDataItems)) then begin

//: ----------------------------------------------
  Result := FAllDataItems;
end;// TQOAlarmView.GetAllDataItems cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAllMachines
 *  Klasse:           TQOAlarmView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Initialisiert die Liste mit den Maschinen beim ersten Zufgriff
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarmView.GetAllMachines: TStringList;
var
  xDB: TAdoDBAccess;
  xIndex: Integer;
  
  const
    cGetStyleName = 'SELECT c_machine_name ' +
                    'FROM t_machine ' +
                    'WHERE c_machine_id = :c_machine_id';
  
begin
  if not(assigned(FAllMachines)) then begin
    FAllMachines := TStringList.Create;
    FAllMachines.Sorted := true;
    FAllMachines.Duplicates := dupIgnore;
    with AlarmData do begin
      FindFirst;
      xDB := TAdoDBAccess.Create(1);
      try
        xDB.Init;
        xDB.Query[0].SQL.Text := cGetStyleName;
        while not(EOF) do begin
          xDB.Query[0].ParamByName('c_machine_id').AsInteger := FieldByName(cMachineFieldName).AsInteger;
          xDB.Query[0].open;
          if not(xDB.Query[0].EOF) then begin
            xIndex := FAllMachines.Add(xDB.Query[0].FieldByName('c_machine_name').AsString);
            FAllMachines.Objects[xIndex] := Pointer(FieldByName(cMachineFieldName).AsInteger);
          end else begin
            xIndex := FAllMachines.Add(rsMachineNotExists);
            FAllMachines.Objects[xIndex] := Pointer(-1);
          end;// if not(xDB.Query[0].EOF) then begin
          xDB.Query[0].close;
          next;
        end;// while not(EOF) do begin
      finally
        xDB.Free;
      end;// try finally
    end;// with AlarmData do begin
  end;// if not(assigned(FAllMachines)) then begin

//: ----------------------------------------------
  Result := FAllMachines;
end;// TQOAlarmView.GetAllMachines cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAllStyles
 *  Klasse:           TQOAlarmView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Initialisiert die Liste mit den Artikel beim ersten Zufgriff
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarmView.GetAllStyles: TStringList;
var
  xDB: TAdoDBAccess;
  xIndex: Integer;
  
  const
    cGetStyleName = 'SELECT c_style_name ' +
                    'FROM t_style ' +
                    'WHERE c_style_id = :c_style_id';
  
begin
  if not(assigned(FAllStyles)) then begin
    FAllStyles := TStringList.Create;
    FAllStyles.Sorted := true;
    FAllStyles.Duplicates := dupIgnore;
    with AlarmData do begin
      FindFirst;
      xDB := TAdoDBAccess.Create(1);
      try
        xDB.Init;
        xDB.Query[0].SQL.Text := cGetStyleName;
        while not(EOF) do begin
          xDB.Query[0].ParamByName('c_style_id').AsInteger := FieldByName(cStyleFieldName).AsInteger;
          xDB.Query[0].open;
          if not(xDB.Query[0].EOF) then begin
            xIndex := FAllStyles.Add(xDB.Query[0].FieldByName('c_style_name').AsString);
            FAllStyles.Objects[xIndex] := Pointer(FieldByName(cStyleFieldName).AsInteger);
          end else begin
            xIndex := FAllStyles.Add(rsStyleNotExists);
            FAllStyles.Objects[xIndex] := Pointer(-1);
          end;// if not(xDB.Query[0].EOF) then begin
          xDB.Query[0].close;
          next;
        end;// while not(EOF) do begin
      finally
        xDB.Free;
      end;// try finally
    end;// with AlarmData do begin
  end;// if not(assigned(FAllStyles)) then begin

//: ----------------------------------------------
  Result := FAllStyles;
end;// TQOAlarmView.GetAllStyles cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetDataItemsFromStyle
 *  Klasse:           TQOAlarmView
 *  Kategorie:        No category 
 *  Argumente:        (aStyleName, aMachineID)
 *
 *  Kurzbeschreibung: Ermittelt die DataItems f�r den �bergebenen Artikel
 *  Beschreibung:     
                      Wenn die Maschinen ID leer ist (aMachineID = '') dann wird nur auf den 
                      Artikel geschaut sonst muss der Artikel und die Maschine stimmen um ein DataItem 
                      in die Liste aufzunehmen.
 --------------------------------------------------------------------*)
function TQOAlarmView.GetDataItemsFromStyle(aStyleName: string; aMachineID: string = ''): String;
var
  xAlarmData: TNativeAdoQuery;
  xStyleId: Integer;
  xDataItemList: TStringList;
begin
  result := '';
  xAlarmData := nil;
  try
    xAlarmData := TNativeAdoQuery.Create;
    xAlarmData.Recordset := AlarmData.Recordset.Clone(LockTypeEnum(adLockUnspecified));
    xAlarmData.CreateFields;
  
    if AlarmViews.GetStyleIDByName(aStyleName, xStyleID) then begin
      if aMachineID > '' then
        xAlarmData.Filter(Format('%s = %d AND %s = %s', [cStyleFieldName, xStyleID, cMachineFieldName, aMachineID]))
      else
        xAlarmData.Filter(Format('%s = %d', [cStyleFieldName, xStyleId]));
  
      xDataItemList := TStringList.Create;
      xDataItemList.sorted := true;
      xDataItemList.Duplicates := dupIgnore;
      while not(xAlarmData.EOF) do begin
        xDataItemList.Add(xAlarmData.FieldByName(cDataItemDisplayName).AsString);
        xAlarmData.Next;
      end;// while not(xAlarmData.EOF) do begin
      result := xDataITemList.CommaText;
    end;// if GetStyleIDByName(aStyleName, xStyleID) then begin
  finally
    FreeAndNil(xAlarmData);
    FreeAndNil(xDataItemList);
  end;// try finally
end;// TQOAlarmView.GetDataItemsFromStyle cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetInfo
 *  Klasse:           TQOAlarmView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Holt die Zusatzinformationen zum Alarm
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarmView.GetInfo: String;
begin
  Result := FInfo;
end;// TQOAlarmView.GetInfo cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMachineNameFromID
 *  Klasse:           TQOAlarmView
 *  Kategorie:        No category 
 *  Argumente:        (aMachineID)
 *
 *  Kurzbeschreibung: Ermittelt die Namen einer Mashcine anhand der ID
 *  Beschreibung:     
                      Die ID ist als Integer in der Objects Eigenscahft der Stringliste gespeichert
 --------------------------------------------------------------------*)
function TQOAlarmView.GetMachineNameFromID(aMachineID: integer): String;
begin
  result := '';
  
  if AllMachines.IndexOfObject(pointer(aMachineID)) >= 0 then
    result := AllMachines[AllMachines.IndexOfObject(pointer(aMachineID))];
end;// TQOAlarmView.GetMachineNameFromID cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMachinesFromStyle
 *  Klasse:           TQOAlarmView
 *  Kategorie:        No category 
 *  Argumente:        (aMachineList, aStyleName)
 *
 *  Kurzbeschreibung: Gibt eine Liste mit den Maschinen zur�ck die am Alarm f�r den �bergebenen Artikel beteiligt
 waren
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOAlarmView.GetMachinesFromStyle(aMachineList: TStringList; aStyleName: string);
var
  xStyleID: Integer;
  xIndex: Integer;
  xMachineName: String;
begin
  if assigned(aMachineList) then begin
    if AllStyles.IndexOf(aStyleName) >= 0 then begin
      // Stellt sicher, dass die Maschinendaten geholt wurden
      GetAllMachines;
      xStyleID := Integer(AllStyles.Objects[AllStyles.IndexOf(aStyleName)]);
      if xStyleID > -1 then begin
        aMachineList.Clear;
        aMachineList.sorted := true;
        aMachineList.Duplicates := dupIgnore;
        try
          AlarmData.Recordset.Filter := Format('%s = %d', [cStyleFieldName, xStyleID]);
          AlarmData.FindFirst;
          while not(AlarmData.EOF) do begin
            xMachineName := GetMachineNameFromID(AlarmData.FieldByName(cMachineFieldName).AsInteger);
            xIndex := aMachineList.Add(xMachineName);
            aMachineList.Objects[xIndex] := Pointer(AlarmData.FieldByName(cMachineFieldName).AsInteger);
            AlarmData.next;
          end;// while not(AlarmData.EOF) do begin
        finally
          // sicherstellen, dass der Filter wieder entfernt wird
          AlarmData.Recordset.Filter := '';
        end;// try finally
      end;// if xStyleID > -1 then begin
    end;// if AllStyles.IndexOf(aStyleName) >= 0 then begin
  end;// if assigned(aMachineList) then begin
end;// TQOAlarmView.GetMachinesFromStyle cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetShiftStart
 *  Klasse:           TQOAlarmView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert den Schichtstart des Alarmdatums als string
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarmView.GetShiftStart: String;
var
  xFound: Boolean;
begin
  result := '';
  AlarmData.FindFirst;
  
  with FAlarmData do begin
    xFound := false;
    while (not(EOF)) and(not(xFound)) do begin
      if FieldByName(cShiftFieldName).AsInteger = ShiftID then begin
        xFound := true;
        result := FieldByName(cShiftStartFieldName).AsString;
      end;// if FieldByName(cShiftFieldName).AsInteger = ShiftID then begin
      next;
    end;// while (not(EOF)) and(not(xFound)) do begin
  end;// with FAlarmData do begin
end;// TQOAlarmView.GetShiftStart cat:No category

//:-------------------------------------------------------------------
(*: Member:           Init
 *  Klasse:           TQOAlarmView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Initialisiert die Klasse
 *  Beschreibung:     
                      Die Initialisierung ist erst notwendig, wenn die Artikel oder Maschinen angezeigt werden
                      sollen
 --------------------------------------------------------------------*)
procedure TQOAlarmView.Init;
  
  const
    cGetAlarmData = 'SELECT c_alarm_data, c_alarm_settings ' +
                    'FROM  t_QO_alarm_data ' +
                    'WHERE  c_QO_Alarm_data_id = :c_QO_Alarm_data_id';
  
begin
  if not(FInitialized) then begin
    with TAdoDBAccess.Create(1) do try
      Init;
      with Query[0] do begin
        SQL.Text := cGetAlarmData;
        ParamByName('c_QO_Alarm_data_id').AsInteger := FID;
        open;
        if not(EOF) then begin
          FAlarmSettings := FieldByName('c_alarm_settings').AsString;
          FAlarmData.AsXML := FieldByName('c_alarm_data').AsString;
        end;// if not(EOF) then begin
      end;// with Query[0] do begin
      FInitialized := true;
    finally
      Free;
    end;// with TAdoDBAccess.Create(1) do try
  end;// if not(FInitialized) then begin
end;// TQOAlarmView.Init cat:No category

//:-------------------------------------------------------------------
(*: Member:           OnGetImageIndex
 *  Klasse:           TQOAlarmView
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aImageKind, aImageIndex)
 *
 *  Kurzbeschreibung: Gibt den Index f�r den Alarm zur�ck (Darstellung im Tree)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOAlarmView.OnGetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer);
begin
  aImageIndex := TImageIndexHandler.Instance.AlarmAnalyze;
end;// TQOAlarmView.OnGetImageIndex cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveInfo
 *  Klasse:           TQOAlarmView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Speichert die Zusatzinforamtionen auf der DB
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOAlarmView.SaveInfo;
  
  const
    cSaveInfo = 'UPDATE t_QO_Alarm_Data ' +
                'SET c_info = :c_info ' +
                'WHERE c_QO_alarm_data_id = :c_QO_alarm_data_id';
  
begin
  if FID > cINVALID_ID then begin
    with TAdoDBAccess.Create(1) do try
      Init;
      with Query[0] do begin
        SQL.Text := cSaveInfo;
        ParamByName('c_info').AsString := FInfo;
        ParamByName('c_QO_alarm_data_id').AsInteger := FID;
        ExecSQL;
      end;// with Query[0] do begin
    finally
      Free;
    end;// with TAdoDBAccess.Create(1) do try
  end;// if FID > cINVALID_ID then begin
end;// TQOAlarmView.SaveInfo cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetInfo
 *  Klasse:           TQOAlarmView
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Zusatzinformationen zum Alarm
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOAlarmView.SetInfo(Value: String);
begin
  FInfo := Value;
end;// TQOAlarmView.SetInfo cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOShiftView
 *  Kategorie:        List 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOShiftView.Create;
begin
  inherited Create;

//: ----------------------------------------------

//: ----------------------------------------------
  FQOVisualize := TQOVisualizeImpl.Create;

//: ----------------------------------------------
  mAlarmViews := TList.Create;
  FAllStyles := TNativeAdoQuery.Create;
  FQOVisualize.OnGetImageIndex := OnGetImageIndex;
  FQOVisualize.EditorEnabled := true;
end;// TQOShiftView.Create cat:List

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TQOShiftView
 *  Kategorie:        List 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TQOShiftView.Destroy;
begin
  FreeAndNil(FQOVisualize);

//: ----------------------------------------------
  Clear;
  FreeAndNil(mAlarmViews);
  FreeAndNil(FAllStyles);

//: ----------------------------------------------
  inherited Destroy;
end;// TQOShiftView.Destroy cat:List

//:-------------------------------------------------------------------
(*: Member:           Add
 *  Klasse:           TQOShiftView
 *  Kategorie:        List 
 *  Argumente:        (aAlarmView)
 *
 *  Kurzbeschreibung: F�gt einen neuen Alarm zur Liste hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOShiftView.Add(aAlarmView: TQOAlarmView): Integer;
begin
  result := -1;
  if assigned(mAlarmViews) then
    mAlarmViews.Add(aAlarmView);
end;// TQOShiftView.Add cat:List

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TQOShiftView
 *  Kategorie:        List 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Kopiert die Liste mit den Alarmen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOShiftView.Assign(Source: TPersistent);
var
  xSource: TQOShiftView;
  xAlarmView: TQOAlarmView;
  i: Integer;
begin
  if Source is TQOShiftView then begin
    xSource := TQOShiftView(Source);
    Clear;
    // Alle Eigenschaften kopieren
    for i := 0 to xSource.AlarmViewCount - 1 do begin
      xAlarmView := TQOAlarmView.Create;
      xAlarmView.Assign(xSource.AlarmViews[i]);
      Add(xAlarmView);
    end;// for i := 0 to xSource.AlarmViewCount - 1 do begin
  
  end else begin
    // Weiterreichen
    inherited Assign(Source);
  end;// if Source is TQOShiftView then begin
end;// TQOShiftView.Assign cat:List

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TQOShiftView
 *  Kategorie:        List 
 *  Argumente:        (aSourceAlarm)
 *
 *  Kurzbeschreibung: Kopiert die Liste mit den Alarmen
 *  Beschreibung:     
                      Als Source ist 'TQOAlarms' angegeben. Das heisst, dass ein manuell generierter Alarm
                      direkt an eine 'TQOShiftView' �bergeben werden kann und dann fertig initialisiert im
                      GUI angezeigt werden kann, wie wenn er von der DB geladen worden w�re.
 --------------------------------------------------------------------*)
procedure TQOShiftView.Assign(aSourceAlarm: TQOAlarms);
var
  i: Integer;
  xQOAlarmView: TQOAlarmView;
  xXML: TIcXMLParser;
  xXMLDoc: TIcXMLDocument;
  xStream: TStringStream;
begin
  try
    // weil manuell generiert, ist keine ID auf der DB vorhanden
    ID := cINVALID_ID;
    AlarmDate := now;
    QOVisualize.Caption := DateTimeToTreeCaption(now);
    ShiftID := TQODataPool.Instance.RecentShiftID;
    // Artikeldaten holen
    FAllStyles.AsXML := TQODataPool.Instance.StyleData.AsXML;
    // Alarm wurde manuell generiert
    ShiftAlarmType := satManual;
  
    // Alle Alarme �bernehmen die gebissen haben
    for i := 0 to aSourceAlarm.AlarmCount - 1 do begin
      if aSourceAlarm.Alarms[i].Caught then begin
        // Ein neues Alarm Objekt erzeugen
        xQOAlarmView := TQOAlarmView.Create;
  
        // Auch der Alarm hat keine Entsprechung auf der DB
        xQOAlarmView.ID := cINVALID_ID;
        xQOAlarmView.QOVisualize.Caption := aSourceAlarm.Alarms[i].QOVisualize.Caption;
        xQOAlarmView.LinkAlarmID := cINVALID_ID;
        xQOAlarmView.AlarmDate := AlarmDate;
        xQOAlarmView.ShiftID := ShiftID;
        xQOAlarmView.AlarmViews := self;
        xQOAlarmView.SaveDate   := aSourceAlarm.Alarms[i].SaveDate;
        xQOAlarmView.SavingUser := aSourceAlarm.Alarms[i].SavingUser;
        xQOAlarmView.AdviceMethods := aSourceAlarm.Alarms[i].AdviceMethods;
  
        // Methoden Settings als XML schreiben
        xXMLDoc := nil;
        xXML    := nil;
        try
          // Der Parser ist zust�ndig f�r das erzeugen von XML
          xXML := TIcXMLParser.Create(nil);
          // "virtuelles" XML Dokument (wird sp�ter in einem Stream abgelegt)
          xXMLDoc := TIcXMLDocument.Create;
          xXMLDoc.AssignParser(xXML);
          // Alle Methoden in das XML-Dokument schreiben
          aSourceAlarm.Alarms[i].CollectMethodsAsXML(xXMLDoc);
          // Der Stream nimmt den XML Formatierten String auf
          xStream := TStringStream.Create('');
          try
            // XML String erzeugen
            xXMLDoc.Write(xStream);
            xQOAlarmView.FAlarmSettings := xStream.DataString;
          finally
            xStream.Free;
          end;// try finally
        finally
          FreeAndNil(xXMLDoc);
          FreeAndNil(xXML);
        end;// try finally
  
        // Die Daten des Alarmes �bernehmen
        xQOAlarmView.FAlarmData.AsXML := aSourceAlarm.Alarms[i].Values.AsXML;
        // Alle notwendigen Initialisierungen am Alarm sind gemacht
        xQOAlarmView.Initialized := true;
        // Zur Liste hinzuf�gen
        Add(xQOAlarmView);
      end;// if xSourceAlarms.Alarms[i].Caught then begin
    end;// for i := 0 to xSourceAlarm.AlarmCount - 1 do begin
  finally
    // Alle notwendigen Initialisierungen an der Schicht sind gemacht
    FInitialized := true;
  end;// try finally
end;// TQOShiftView.Assign cat:List

//:-------------------------------------------------------------------
(*: Member:           Clear
 *  Klasse:           TQOShiftView
 *  Kategorie:        List 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�scht alle Alarme aus der Lsite
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOShiftView.Clear;
begin
  while mAlarmViews.Count > 0 do
    delete(0);
end;// TQOShiftView.Clear cat:List

//:-------------------------------------------------------------------
(*: Member:           Delete
 *  Klasse:           TQOShiftView
 *  Kategorie:        List 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: L�scht einen Alarm aus der Liste
 *  Beschreibung:     
                      Mit dem L�schen wird auch das Objekt freigegeben.
 --------------------------------------------------------------------*)
procedure TQOShiftView.Delete(aIndex: integer);
begin
  if assigned(mAlarmViews) then begin
    if aIndex < mAlarmViews.Count then begin
      AlarmViews[aIndex].Free;
      mAlarmViews.delete(aIndex);
    end;// if aIndex < mAlarmViews.Count then begin
  end;// if assigned(mAlarmViews) then begin
end;// TQOShiftView.Delete cat:List

//:-------------------------------------------------------------------
(*: Member:           DeleteFromArchive
 *  Klasse:           TQOShiftView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Kennzeichnet die Schicht wieder als automatisch generiert
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOShiftView.DeleteFromArchive;
  
  const
    cDelFromArchive = 'UPDATE t_QO_Caught_Alarm ' +
                      'SET c_Persistent = :c_Persistent ' +
                      'WHERE c_caught_alarm_id = :c_caught_alarm_id';
  
begin
  with TAdoDBAccess.Create(1) do try
    Init;
    with Query[0] do begin
      SQL.Text := cDelFromArchive;
      ParamByName('c_Persistent').AsBoolean := false;
      ParamByName('c_caught_alarm_id').AsInteger := FID;
      ExecSQL;
    end;// with Query[0] do begin
  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try
end;// TQOShiftView.DeleteFromArchive cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAlarmViewCount
 *  Klasse:           TQOShiftView
 *  Kategorie:        List 
 *  Argumente:        
 *
 *  Kurzbeschreibung: ERmittelt die anzahl der Alarme
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOShiftView.GetAlarmViewCount: Integer;
begin
  result := 0;

//: ----------------------------------------------
  Init;

//: ----------------------------------------------
  if assigned(mAlarmViews) then
    result := mAlarmViews.Count;
end;// TQOShiftView.GetAlarmViewCount cat:List

//:-------------------------------------------------------------------
(*: Member:           GetAlarmViews
 *  Klasse:           TQOShiftView
 *  Kategorie:        List 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: Bietet den Zugriff auf einen einzelnen Alarm
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOShiftView.GetAlarmViews(aIndex: Integer): TQOAlarmView;
begin
  result := nil;
  if assigned(mAlarmViews) then
    if mAlarmViews.Count > aIndex then
      result := TQOAlarmView(mAlarmViews.Items[aIndex]);
end;// TQOShiftView.GetAlarmViews cat:List

//:-------------------------------------------------------------------
(*: Member:           GetAllStyles
 *  Klasse:           TQOShiftView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Recordset mit den Daten aller am Alarm beteiligten Artikel
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOShiftView.GetAllStyles: TNativeAdoQuery;
  
  const
    cGetAllStyleData = 'SELECT c_Style_data ' +
                       'FROM  t_QO_caught_alarm ' +
                       'WHERE  c_caught_Alarm_id = :c_caught_Alarm_id';
  
begin
  if not(FAllStyles.Active) then begin
    with TAdoDBAccess.Create(1) do try
      Init;
      with Query[0] do begin
        SQL.Text := cGetAllStyleData;
        ParamByName('c_caught_Alarm_id').AsInteger := FID;
        open;
        if not(EOF) then
          FAllStyles.AsXML := FieldByName('c_style_data').AsString;
      end;// with Query[0] do begin
    finally
      Free;
    end;// with TAdoDBAccess.Create(1) do try
  end;// if not(FAllStyles.Active) then begin

//: ----------------------------------------------
  Result := FAllStyles;
end;// TQOShiftView.GetAllStyles cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetInfo
 *  Klasse:           TQOShiftView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Holt die Zusatzinformationen zum Alarm
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOShiftView.GetInfo: String;
begin
  Result := FInfo;
end;// TQOShiftView.GetInfo cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetStyleIDByName
 *  Klasse:           TQOShiftView
 *  Kategorie:        No category 
 *  Argumente:        (aStyleName, aStyleID)
 *
 *  Kurzbeschreibung: Gibt die Style ID f�r den gew�nschten Artikelnamen zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOShiftView.GetStyleIDByName(aStyleName: string; var aStyleID: integer): Boolean;
begin
  result := false;
  
  with TNativeAdoQuery.Create do try
    Clone(AllStyles);
    CreateFields;
  
    Filter('c_style_name = ''' + aStyleName + '''');
    if not(EOF) then begin
      aStyleId := FieldByName('c_style_id').AsInteger;
      result := true;
    end;// if not(EOF) then begin
  finally
    free;
  end;// with TNativeAdoDataSet do try
end;// TQOShiftView.GetStyleIDByName cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOf
 *  Klasse:           TQOShiftView
 *  Kategorie:        List 
 *  Argumente:        (aAlarmView)
 *
 *  Kurzbeschreibung: Gibt den Index eines bestimmten Alarmes in der Liste zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOShiftView.IndexOf(aAlarmView: TQOAlarmView): Integer;
var
  i: Integer;
begin
  result := -1;
  
  i := 0;
  while (i < mAlarmViews.Count) and (result = -1) do begin
    if mAlarmViews[i] = aAlarmView then
      result := i;
    inc(i);
  end;// while (i < mAlarmViews.Count) and (result = -1) do begin
end;// TQOShiftView.IndexOf cat:List

//:-------------------------------------------------------------------
(*: Member:           Init
 *  Klasse:           TQOShiftView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�dt die Alarme von der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOShiftView.Init;
var
  xQOAlarmView: TQOAlarmView;
  
  const
    cGetAlarmData = 'SELECT c_QO_Alarm_data_id, c_QO_Alarm_name, c_QOAlarm_id, c_info, c_AdviceMethods, c_SaveDate, c_SavingUser ' +
                    'FROM t_QO_Alarm_data ' +
                    'WHERE c_caught_alarm_id = :c_caught_alarm_id ' +
                    'ORDER BY c_QO_Alarm_name';
  
begin
  if not(FInitialized) then begin
    with TAdoDBAccess.Create(1) do try
      Init;
      with Query[0] do begin
        SQL.Text := cGetAlarmData;
        ParamByName('c_caught_alarm_id').AsInteger := FID;
        open;
        while not(EOF) do begin
          xQOAlarmView := TQOAlarmView.Create;
          xQOAlarmView.ID                  := FieldByName('c_QO_Alarm_data_id').AsInteger;
          xQOAlarmView.QOVisualize.Caption := FieldByName('c_QO_Alarm_name').AsString;
          xQOAlarmView.LinkAlarmID         := FieldByName('c_QOAlarm_id').AsInteger;
          xQOAlarmView.AlarmDate           := AlarmDate;
          xQOAlarmView.ShiftID             := ShiftID;
          xQOAlarmView.AlarmViews          := self;
          xQOAlarmView.Info                := FieldByName('c_info').AsString;
          xQOAlarmView.SaveDate            := FieldByName('c_SaveDate').AsDateTime;
          xQOAlarmView.SavingUser          := FieldByName('c_SavingUser').AsString;
          // Typecast auf Word �ndern, wenn mehr als 8 Methoden definiert sind
          xQOAlarmView.AdviceMethods := TAdviceMethodSet(Byte(FieldByName('c_AdviceMethods').AsInteger));
  
          Add(xQOAlarmView);
          next;
        end;// while not(EOF) do begin
      end;// with Query[0] do begin
      FInitialized := true;
    finally
      Free;
    end;// with TAdoDBAccess.Create(1) do try
  end;// if not(mInitialized) then begin
end;// TQOShiftView.Init cat:No category

//:-------------------------------------------------------------------
(*: Member:           OnGetImageIndex
 *  Klasse:           TQOShiftView
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aImageKind, aImageIndex)
 *
 *  Kurzbeschreibung: Gibt das Image f�r die Darstellung zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOShiftView.OnGetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer);
begin
  if ShiftAlarmType = satArchive then
    aImageIndex := TImageIndexHandler.Instance.AlarmSetArchive
  else
    aImageIndex := TImageIndexHandler.Instance.AlarmSet;
end;// TQOShiftView.OnGetImageIndex cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveInArchive
 *  Klasse:           TQOShiftView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Kennzeichnet die Schicht als dem Archiv zugeh�rig
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOShiftView.SaveInArchive;
  
  const
    cSaveInArchive = 'UPDATE t_QO_Caught_Alarm ' +
                     'SET c_Persistent = :c_Persistent ' +
                     'WHERE c_caught_alarm_id = :c_caught_alarm_id';
  
begin
  with TAdoDBAccess.Create(1) do try
    Init;
    with Query[0] do begin
      SQL.Text := cSaveInArchive;
      ParamByName('c_Persistent').AsBoolean := true;
      ParamByName('c_caught_alarm_id').AsInteger := FID;
      ExecSQL;
    end;// with Query[0] do begin
  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try
end;// TQOShiftView.SaveInArchive cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveInfo
 *  Klasse:           TQOShiftView
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Speichert die Zusatzinforamtionen auf der DB
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOShiftView.SaveInfo;
  
  const
    cSaveInfo = 'UPDATE t_QO_Caught_Alarm ' +
                'SET c_info = :c_info ' +
                'WHERE c_caught_alarm_id = :c_caught_alarm_id';
  
begin
  if FID > cINVALID_ID then begin
    with TAdoDBAccess.Create(1) do try
      Init;
      with Query[0] do begin
        SQL.Text := cSaveInfo;
        ParamByName('c_info').AsString := FInfo;
        ParamByName('c_caught_alarm_id').AsInteger := FID;
        ExecSQL;
      end;// with Query[0] do begin
    finally
      Free;
    end;// with TAdoDBAccess.Create(1) do try
  end;// if FID > cINVALID_ID then begin
end;// TQOShiftView.SaveInfo cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetInfo
 *  Klasse:           TQOShiftView
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Zusatzinformationen zum Alarm
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOShiftView.SetInfo(Value: String);
begin
  FInfo := Value;
end;// TQOShiftView.SetInfo cat:No category

//:-------------------------------------------------------------------
(*: Member:           DeleteAlarm
 *  Klasse:           TQOShiftSetObserver
 *  Kategorie:        No category 
 *  Argumente:        (aShift)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn ein Alarm gel�scht wurde
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOShiftSetObserver.DeleteAlarm(aShift: TQOShiftView);
begin
  if Assigned (FOnDeleteAlarm) then
    FOnDeleteAlarm(aShift);
end;// TQOShiftSetObserver.DeleteAlarm cat:No category

//:-------------------------------------------------------------------
(*: Member:           ShiftsLoaded
 *  Klasse:           TQOShiftSetObserver
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Wird aufgerufen, sobald die Schichten von der DB geladen wurden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOShiftSetObserver.ShiftsLoaded;
begin
  If assigned(FOnShiftsLoaded) then
    FOnShiftsLoaded(self);
end;// TQOShiftSetObserver.ShiftsLoaded cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOShiftSet
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOShiftSet.Create;
begin
  inherited Create;

//: ----------------------------------------------
  raise Exception.CreateFmt('Access class %s through Instance only', [ClassName]);
end;// TQOShiftSet.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           CreateInstance
 *  Klasse:           TQOShiftSet
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOShiftSet.CreateInstance;
begin
  inherited Create;

//: ----------------------------------------------
  mShifts := TList.Create;
  FObservers := TList.Create;
  FOldestShiftID := 5000;
end;// TQOShiftSet.CreateInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TQOShiftSet
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TQOShiftSet.Destroy;
begin
  if AccessInstance(0) = Self then AccessInstance(2);

//: ----------------------------------------------
  Clear;
  FreeAndNil(mShifts);
  FreeAndNil(FObservers);

//: ----------------------------------------------
  inherited Destroy;
end;// TQOShiftSet.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           AccessInstance
 *  Klasse:           TQOShiftSet
 *  Kategorie:        No category 
 *  Argumente:        (Request)
 *
 *  Kurzbeschreibung: singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class function TQOShiftSet.AccessInstance(Request: Integer): TQOShiftSet;
  
  const FInstance: TQOShiftSet = nil;
  
begin
  case Request of
    0 : ;
    1 : if not Assigned(FInstance) then FInstance := CreateInstance;
    2 : FInstance := nil;
  else
    raise Exception.CreateFmt('Illegal request %d in AccessInstance', [Request]);
  end;
  Result := FInstance;
end;// TQOShiftSet.AccessInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           Add
 *  Klasse:           TQOShiftSet
 *  Kategorie:        List 
 *  Argumente:        (aShift)
 *
 *  Kurzbeschreibung: F�gt eine Schicht zur Liste hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOShiftSet.Add(aShift: TQOShiftView): Integer;
begin
  result := -1;
  if assigned(mShifts) then
    result := mShifts.Add(aShift);
end;// TQOShiftSet.Add cat:List

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TQOShiftSet
 *  Kategorie:        List 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Kopiert die gesammte Liste
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOShiftSet.Assign(Source: TPersistent);
var
  xSource: TQOShiftSet;
  xAlarm: TQOShiftView;
  i: Integer;
begin
  if Source is TQOShiftSet then begin
    xSource := TQOShiftSet(Source);
    Clear;
    // Alle Eigenschaften kopieren
    for i := 0 to xSource.ShiftCount - 1 do begin
      xAlarm := TQOShiftView.Create;
      xAlarm.Assign(xSource.Shifts[i]);
      Add(xAlarm);
    end;// for i := 0 to xSource.ShiftCount - 1 do begin
  
  end else begin
    // Weiterreichen
    inherited Assign(Source);
  end;// if Source is TQOAlarmPresentation then begin
end;// TQOShiftSet.Assign cat:List

//:-------------------------------------------------------------------
(*: Member:           Clear
 *  Klasse:           TQOShiftSet
 *  Kategorie:        List 
 *  Argumente:        (aShiftAlarmTypeSet)
 *
 *  Kurzbeschreibung: L�scht alle Schichten
 *  Beschreibung:     
                      Wird ein leeres Set �begeben, dann werden alle Schichten gel�scht - Ansonsten
                      nur diejenigen deren Typ im �bergebenen Set enthalten sind.
                      
                      F�r jede Schicht in der ein Alarm eingetreten ist, wird ein
                      Objekt der Klasse TQOShiftView verwaltet.
 --------------------------------------------------------------------*)
procedure TQOShiftSet.Clear(aShiftAlarmTypeSet: TShiftAlarmTypeSet = []);
var
  i: Integer;
begin
  i := 0;
  
  while i < mShifts.Count do begin
    if Shifts[i].ShiftAlarmType in aShiftAlarmTypeSet then
      delete(i)
    else
      inc(i);
  end;// while i < mAlarms.Count do begin
end;// TQOShiftSet.Clear cat:List

//:-------------------------------------------------------------------
(*: Member:           Delete
 *  Klasse:           TQOShiftSet
 *  Kategorie:        List 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: L�scht eine Schicht
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOShiftSet.Delete(aIndex: integer);
begin
  if assigned(mShifts) then begin
    if aIndex < mShifts.Count then begin
      DeleteAlarm(Shifts[aIndex]);
      Shifts[aIndex].Free;
      mShifts.delete(aIndex);
    end;// if aIndex < mAlarms.Count then begin
  end;// if assigned(mAlarms) then begin
end;// TQOShiftSet.Delete cat:List

//:-------------------------------------------------------------------
(*: Member:           DeleteAlarm
 *  Klasse:           TQOShiftSet
 *  Kategorie:        No category 
 *  Argumente:        (aShift)
 *
 *  Kurzbeschreibung: Benachrichtigt alle registrierten Observer �ber das L�schen eines Alarms
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOShiftSet.DeleteAlarm(aShift: TQOShiftView);
var
  i: Integer;
  xObs: TQOShiftSetObserver;
begin
  for i := 0 to FObservers.Count - 1 do begin
    xObs := FObservers[i];
    if xObs.Enabled then
      xObs.DeleteAlarm(aShift);
  end;// for I := 0 to FObservers.Count - 1 do begin
end;// TQOShiftSet.DeleteAlarm cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetShiftCount
 *  Klasse:           TQOShiftSet
 *  Kategorie:        List 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zugriffsmethode um die Anzahl Schichten zu ermitteln
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOShiftSet.GetShiftCount: Integer;
begin
  result := 0;
  
  if assigned(mShifts) then
    result := mShifts.Count;
end;// TQOShiftSet.GetShiftCount cat:List

//:-------------------------------------------------------------------
(*: Member:           GetShifts
 *  Klasse:           TQOShiftSet
 *  Kategorie:        List 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: Zugriffsmethode um auf eine Schicht zuzugreifen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOShiftSet.GetShifts(aIndex: Integer): TQOShiftView;
begin
  result := nil;
  if assigned(mShifts) then
    if mShifts.Count > aIndex then
      result := TQOShiftView(mShifts.Items[aIndex]);
end;// TQOShiftSet.GetShifts cat:List

//:-------------------------------------------------------------------
(*: Member:           IndexOf
 *  Klasse:           TQOShiftSet
 *  Kategorie:        List 
 *  Argumente:        (aShift)
 *
 *  Kurzbeschreibung: Gibt den Index einer Schicht zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOShiftSet.IndexOf(aShift: TQOShiftView): Integer;
var
  i: Integer;
begin
  result := -1;
  
  i := 0;
  while (i < mShifts.Count) and (result = -1) do begin
    if mShifts[i] = aShift then
      result := i;
    inc(i);
  end;// while (i < mShifts.Count) and (result = -1) do begin
end;// TQOShiftSet.IndexOf cat:List

//:-------------------------------------------------------------------
(*: Member:           Insert
 *  Klasse:           TQOShiftSet
 *  Kategorie:        No category 
 *  Argumente:        (aIndex, aShift)
 *
 *  Kurzbeschreibung: F�gt eine Schicht an einem Index in dei Liste ein
 *  Beschreibung:     
                      Die manuell generierten Alarme m�ssen in der Liste am Anfang eingef�gt werden,
                      da die Daten von der DB nach Datum sortiert sind, und die manuellen Alarme in 
                      der selben reihenfolge angezeigt werden sollen.
 --------------------------------------------------------------------*)
procedure TQOShiftSet.Insert(aIndex: integer; aShift: TQOShiftView);
begin
  if assigned(mShifts) then begin
    if aIndex >= ShiftCount then
      Add(aShift)
    else
      mShifts.Insert(aIndex, aShift);
  end;// if assigned(mShifts) then begin
end;// TQOShiftSet.Insert cat:No category

//:-------------------------------------------------------------------
(*: Member:           Instance
 *  Klasse:           TQOShiftSet
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class function TQOShiftSet.Instance: TQOShiftSet;
begin
  Result := AccessInstance(1);
end;// TQOShiftSet.Instance cat:No category

//:-------------------------------------------------------------------
(*: Member:           LoadAlarmRootFromDB
 *  Klasse:           TQOShiftSet
 *  Kategorie:        No category 
 *  Argumente:        (aCount)
 *
 *  Kurzbeschreibung: L�dt erstmal nur den Inhalt der Tabelle 't_QO_Caught_Alarm' von der DB
 *  Beschreibung:     
                      Es werden nicht alle Daten von der DB geladen, sondern nur das Datum an dem 
                      mindestens ein Alarm aufgetreten ist. Die Detaildatens�tzte werden erst geladen, 
                      wenn Informationen angezeigt werden sollen.
                      
                      F�r unterschiedliche Anwendungsf�lle kannn es notwendig sein nur einzelne Schichten zu laden.
                      Mit aCount kann angegeben werden wieviele Schichten geladen werden sollen (z.B. automatischer
                      Ausdruck am Schichtende).
 --------------------------------------------------------------------*)
procedure TQOShiftSet.LoadAlarmRootFromDB(aCount: integer);
var
  xQOShiftView: TQOShiftView;
  i: Integer;
  xObs: TQOShiftSetObserver;
  
  const
    cSelectQOAlarm  = 'SELECT c_caught_Alarm_id, c_date, c_shift_id, c_monitored_DataItems, c_Persistent, c_info ' +
                      'FROM t_QO_Caught_Alarm ' +
                      'ORDER BY c_date DESC';
  
    cGetOldestShift = 'SELECT MIN(c_shift_id) as c_shift_id FROM t_shift';
  
begin
  // Alle Automatisch generierten Alarme l�schen
  Clear([satAuto, satArchive]);

//: ----------------------------------------------
  with TAdoDBAccess.Create(1) do try
    Init;
    with Query[0] do begin
      SQL.Text := cGetOldestShift;
      open;
      if not(EOF) then
        OldestShiftID := FieldByName('c_shift_id').AsInteger;
  
      SQL.Text := cSelectQOAlarm;
      // immer alle Alarme laden
      open;
  
      // Wurde ALs Anzahl 0 oder -1 angegeben, dann alle DS lesen
      if aCount <= 0 then
        aCount := MAXINT;
  
      i := 0;
      // Klassenhirarchie aufbauen, solange Daten da sind, oder die Anzahl erreicht ist
      while (not(EOF) and (i < aCount)) do begin
        xQOShiftView := TQOShiftView.Create;
        xQOShiftView.ID                  := FieldByName('c_caught_alarm_id').AsInteger;
        xQOShiftView.AlarmDate           := VarFromDateTime(FieldByName('c_date').AsDateTime);
        xQOShiftView.QOVisualize.Caption := DateTimeToTreeCaption(FieldByName('c_date').AsDateTime);
        xQOShiftView.ShiftID             := FieldByName('c_shift_id').AsInteger;
        xQOShiftView.Info                := FieldByName('c_info').AsString;
        if FieldByName('c_Persistent').AsBoolean then
          xQOShiftView.ShiftAlarmType := satArchive
        else
          xQOShiftView.ShiftAlarmType := satAuto;
        Add(xQOShiftView);
        next;
      end;// while (not(EOF) and (i < aCount)) do begin
    end;// with Query[0] do begin
  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try

//: ----------------------------------------------
  for i := 0 to FObservers.Count - 1 do begin
    xObs := FObservers[i];
    if xObs.Enabled then
      xObs.ShiftsLoaded;
  end;// for i := 0 to FObservers.Count - 1 do begin
end;// TQOShiftSet.LoadAlarmRootFromDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           RegisterObserver
 *  Klasse:           TQOShiftSet
 *  Kategorie:        No category 
 *  Argumente:        (Observer)
 *
 *  Kurzbeschreibung: Registriert einen Observer f�r die Benachrichtigung eines Events
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOShiftSet.RegisterObserver(Observer: TQOShiftSetObserver);
begin
  if Assigned(FObservers) then

//: ----------------------------------------------
    if FObservers.IndexOf(Observer) = -1 then
      FObservers.Add(Observer);
end;// TQOShiftSet.RegisterObserver cat:No category

//:-------------------------------------------------------------------
(*: Member:           ReleaseInstance
 *  Klasse:           TQOShiftSet
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class procedure TQOShiftSet.ReleaseInstance;
begin
  AccessInstance(0).Free;
end;// TQOShiftSet.ReleaseInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           UnregisterObserver
 *  Klasse:           TQOShiftSet
 *  Kategorie:        No category 
 *  Argumente:        (Observer)
 *
 *  Kurzbeschreibung: Entfernt den Observer wieder aus der Liste. Der Besitzer des Observers wird in Zukunft nicht
 mehr benachrichtigt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOShiftSet.UnregisterObserver(Observer: TQOShiftSetObserver);
begin
  if assigned(FObservers) then

//: ----------------------------------------------
    FObservers.Remove(Observer);
end;// TQOShiftSet.UnregisterObserver cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQONodeObject
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQONodeObject.Create;
begin
  inherited Create;

//: ----------------------------------------------
  FQOVisualize := TQOVisualizeImpl.Create;

//: ----------------------------------------------
  FQOVisualize.OnGetImageIndex := OnGetImageIndex;
end;// TQONodeObject.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TQONodeObject
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TQONodeObject.Destroy;
begin
  FreeAndNil(FQOVisualize);

//: ----------------------------------------------

//: ----------------------------------------------
  inherited Destroy;
end;// TQONodeObject.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           OnGetImageIndex
 *  Klasse:           TQONodeObject
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aImageKind, aImageIndex)
 *
 *  Kurzbeschreibung: Gibt den Imageindex zur�ck der f�r die Darstellung gebraucht wird (Tree)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQONodeObject.OnGetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer);
begin
  aImageIndex := TImageIndexHandler.Instance.NodeObject;
end;// TQONodeObject.OnGetImageIndex cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOAssignNodeObject
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOAssignNodeObject.Create;
begin
  inherited Create;

//: ----------------------------------------------
  FProdIDList := TProdIDList.Create;
end;// TQOAssignNodeObject.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TQOAssignNodeObject
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TQOAssignNodeObject.Destroy;
begin
  FProdIDList.Clear;
  FreeAndNil(FProdIDList);

//: ----------------------------------------------
  inherited Destroy;
end;// TQOAssignNodeObject.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           BuildProdIDList
 *  Klasse:           TQOAssignNodeObject
 *  Kategorie:        No category 
 *  Argumente:        (aStyleID)
 *
 *  Kurzbeschreibung: Erstellt die Liste mit den Produktionsgruppen
 *  Beschreibung:     
                      Die Liste wird erstellt, um die Farben f�r die einzelnen Produktions-
                      gruppen eindeutig festlegen zu k�nnen.
 --------------------------------------------------------------------*)
procedure TQOAssignNodeObject.BuildProdIDList(aStyleID: integer = -1);
var
  xProdID: TProdID;
begin
  try
    if assigned(FAlarmView) then begin
      with FAlarmView.AlarmData do begin
        if aStyleID >= 0 then
          Filter(Format('%s = %d and %s = %d', [cStyleFieldName, aStyleID, cMachineFieldName, LinkID]))
        else
          Filter(Format('%s = %d', [cStyleFieldName, LinkID]));
        FindFirst;
        while not(EOF) do begin
          // Name und ID hinzuf�gen
          if FProdIDList.IndexOf(FieldByName(cProdGroupFieldName).AsInteger) < 0 then begin
            xProdID := TProdID.Create;
            xProdID.ProdID       := FieldByName(cProdGroupFieldName).AsInteger;
            xProdID.ProdName     := FieldByName(cProdGroupNameFieldName).AsString;
            xProdID.MachineID    := FieldByName(cMachineFieldName).AsInteger;
            xProdID.MachineName  := FieldByName(cMachineNameFieldName).AsString;
            xProdID.FirstSpindle := FieldByName(cFirstSpdFieldName).AsInteger;
            xProdID.LastSpindle  := FieldByName(cLastSpindleFieldName).AsInteger;
            xProdID.StyleID      := LinkID;
            xProdID.StyleName    := QOVisualize.Caption;
  
            FProdIDList.Add(xProdID);
          end;// if FProdIDList.IndexOf(FieldByName(cProdGroupFieldName)) < 0 then begin
          Next;
        end;// while not(EOF) do begin
      end;// with FAlarmView.AlarmData do begin
    end;// if assigned(FAlarmView) then begin
  finally
    if assigned(FAlarmView) then
      FAlarmView.AlarmData.Filter('');
  end;// try finally
end;// TQOAssignNodeObject.BuildProdIDList cat:No category

//:-------------------------------------------------------------------
(*: Member:           OnGetImageIndex
 *  Klasse:           TQOAssignNodeObject
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aImageKind, aImageIndex)
 *
 *  Kurzbeschreibung: Gibt das Bitmap f�r den entsprechenden Typ zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOAssignNodeObject.OnGetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer);
begin
  case NodeObjectType of
    otStyle   : aImageIndex := TImageIndexHandler.Instance.Style;
    otMachine : aImageIndex := TImageIndexHandler.Instance.Machine;
  end;// case NodeObjectType of
end;// TQOAssignNodeObject.OnGetImageIndex cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TProdIDList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TProdIDList.Create;
begin
  inherited;
  mProdIDs := TList.Create;
end;// TProdIDList.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TProdIDList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TProdIDList.Destroy;
begin
  Clear;
  FreeAndNil(mProdIDs);
  inherited;
end;// TProdIDList.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           Add
 *  Klasse:           TProdIDList
 *  Kategorie:        No category 
 *  Argumente:        (aProdID)
 *
 *  Kurzbeschreibung: F�gt eine ProdID zur Liste hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TProdIDList.Add(aProdID: TProdID): Integer;
begin
  result := -1;
  if assigned(mProdIDs) then
    result := mProdIDs.Add(aProdID);
end;// TProdIDList.Add cat:No category

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TProdIDList
 *  Kategorie:        No category 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Kopiert die gesammte Liste (inkl. ProdIDs)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TProdIDList.Assign(Source: TPersistent);
var
  xSource: TProdIDList;
  xProdID: TProdID;
  i: Integer;
begin
  if Source is TProdIDList then begin
    xSource := TProdIDList(Source);
    Clear;
    // Alle Eigenschaften kopieren
    for i := 0 to xSource.ProdIDCount - 1 do begin
      xProdID := TProdID.Create;
      xProdID.Assign(xSource.ProdIDs[i]);
      Add(xProdID);
    end;// for i := 0 to xSource.ProdIDCount - 1 do begin
  
  end else begin
    // Weiterreichen
    inherited Assign(Source);
  end;// if Source is TProdIDList then begin
end;// TProdIDList.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           Clear
 *  Klasse:           TProdIDList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�scht die Liste (inkl. freigabe TProdID)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TProdIDList.Clear;
begin
  while mProdIDs.Count > 0 do
    delete(0);
end;// TProdIDList.Clear cat:No category

//:-------------------------------------------------------------------
(*: Member:           Delete
 *  Klasse:           TProdIDList
 *  Kategorie:        No category 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: L�scht eine einzelne ProdID
 *  Beschreibung:     
                      Der Eintrag in der Lsite und das Objekt TProdID wird freigegeben.
 --------------------------------------------------------------------*)
procedure TProdIDList.Delete(aIndex: integer);
begin
  if assigned(mProdIDs) then begin
    if aIndex < mProdIDs.Count then begin
      if assigned(FOnDeleteProdID) then
        FOnDeleteProdID(ProdIDs[aIndex]);
      ProdIDs[aIndex].Free;
      mProdIDs.delete(aIndex);
    end;// if aIndex < mProdIDs.Count then begin
  end;// if assigned(mProdIDs) then begin
end;// TProdIDList.Delete cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetProdIDCount
 *  Klasse:           TProdIDList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert die Anzahl der Listeneintr�gen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TProdIDList.GetProdIDCount: Integer;
begin
  result := 0;
  
  if assigned(mProdIDs) then
    result := mProdIDs.Count;
end;// TProdIDList.GetProdIDCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetProdIDs
 *  Klasse:           TProdIDList
 *  Kategorie:        No category 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: Liefert die ProdID mit dem entsprechenden Index
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TProdIDList.GetProdIDs(aIndex: Integer): TProdID;
begin
  result := nil;
  if assigned(mProdIDs) then
    if mProdIDs.Count > aIndex then
      result := TProdID(mProdIDs.Items[aIndex]);
end;// TProdIDList.GetProdIDs cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOf
 *  Klasse:           TProdIDList
 *  Kategorie:        No category 
 *  Argumente:        (aProdID)
 *
 *  Kurzbeschreibung: Gibt den Index des Objektes mit der entsprechenden ProdID in der Liste
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TProdIDList.IndexOf(aProdID: integer): Integer;
var
  i: Integer;
begin
  result := -1;
  
  i := 0;
  while (i < mProdIDs.Count) and (result = -1) do begin
    if ProdIDs[i].ProdID = aProdID then
      result := i;
    inc(i);
  end;// while (i < mProdIDs.Count) and (result = -1) do begin
end;// TProdIDList.IndexOf cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOf
 *  Klasse:           TProdIDList
 *  Kategorie:        No category 
 *  Argumente:        (aProdID)
 *
 *  Kurzbeschreibung: Gibt den Index des Objektes in der Liste
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TProdIDList.IndexOf(aProdID: TProdID): Integer;
var
  i: Integer;
begin
  result := -1;
  
  i := 0;
  while (i < mProdIDs.Count) and (result = -1) do begin
    if mProdIDs[i] = aProdID then
      result := i;
    inc(i);
  end;// while (i < mProdIDs.Count) and (result = -1) do begin
end;// TProdIDList.IndexOf cat:No category


(*-----------------------------------------------------------
  Bereitet ein Datum zur Anzeige im Tree auf
-------------------------------------------------------------*)
function DateTimeToTreeCaption(aDate: TDateTime): String;
begin
  result := DateToStr(aDate) + FormatDateTime(' (hh:mm)', aDate);
end;// function TMainWindow.DateTimeToTreeCaption(aDate: TDateTime): string;

initialization

finalization
  TQOShiftSet.ReleaseInstance;

end.
