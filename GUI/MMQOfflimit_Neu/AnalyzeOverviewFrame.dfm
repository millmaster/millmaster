object frmAnalyzeOverviewFrame: TfrmAnalyzeOverviewFrame
  Left = 0
  Top = 0
  Width = 694
  Height = 586
  TabOrder = 0
  OnResize = FrameResize
  object mHistogramChart: TmmChart
    Left = 0
    Top = 161
    Width = 694
    Height = 425
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Font.Charset = DEFAULT_CHARSET
    Title.Font.Color = clBlack
    Title.Font.Height = -12
    Title.Font.Name = 'Arial'
    Title.Font.Style = []
    Title.Text.Strings = (
      '(*)Histogramm')
    OnClickSeries = mHistogramChartClickSeries
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMaximum = False
    LeftAxis.AutomaticMinimum = False
    LeftAxis.Title.Caption = '(*)Anzahl beteiligter Maschinen'
    Legend.Alignment = laTop
    View3D = False
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    OnMouseMove = mChartMouseMove
    MouseMode = mmNormal
  end
  object paRanking: TmmPanel
    Left = 0
    Top = 0
    Width = 694
    Height = 161
    Align = alTop
    BevelOuter = bvNone
    Caption = '(*)Keine automatisch generierten Alarme registriert'
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object mInfoImage: TmmImage
      Left = 10
      Top = 10
      Width = 16
      Height = 16
      Picture.Data = {
        07544269746D617036030000424D360300000000000036000000280000001000
        0000100000000100180000000000000300000000000000000000000000000000
        0000E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF84695A8C695A
        E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FF
        FFE7FFFFE7FFFF94694ACE9E6BAD794A84695AE7FFFFE7FFFFE7FFFFE7FFFFE7
        FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF94694ACEAE84FFF7C6946142
        84695AE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF9471
        638C614AE7CFADFFDFB5FFDFB5AD86635228086B413184695AE7FFFFE7FFFFE7
        FFFFE7FFFFE7FFFFBDA69CBDA69CD6B69CE7CFADFFE7BDFFEFC6FFFFD6F7D7B5
        DEBE94AD8E635A281084695AE7FFFFE7FFFFE7FFFFCEB6A5EFDFC6EFD7BDFFEF
        CEFFD7B5C6714AEF9663DE8E5ADEA67BFFE7C6F7D7B5D6B6947341298C6963E7
        FFFFCEBEA5EFDFC6EFDFC6FFEFD6FFEFCEFFFFE7B586637B00008C2800F7E7C6
        FFEFCEFFE7C6F7D7B5DEBE9C5A2810E7FFFFC6B6A5EFDFC6FFF7D6FFEFD6FFE7
        CEFFFFEFCEB6946B0000A55931FFFFF7FFE7CEFFE7C6FFEFCEFFE7C6AD8E6B94
        716BCEBEADFFE7D6FFF7DEFFEFD6FFEFD6FFFFF7CEAE94730000A55131FFFFF7
        FFEFCEFFE7CEFFEFCEFFEFD6E7CFAD8C695ACEC7B5FFEFE7FFF7E7FFEFDEFFF7
        DEFFFFFFD6C7AD730000A55931FFFFFFFFEFD6FFEFD6FFEFD6FFF7DEEFD7C684
        695AD6C7BDFFF7EFFFFFEFFFF7E7FFFFEFEFE7DE8C38186300008C3821FFFFFF
        FFF7DEFFEFD6FFEFDEFFFFE7F7DFCE947973DED7CEF7F7EFFFFFFFFFF7EFFFFF
        FFE7DFCEB59684BDA69CC6AEA5FFFFFFFFF7E7FFEFDEFFFFEFFFFFFFE7CFB594
        7973E7FFFFE7E7E7FFFFFFFFFFFFFFFFF7FFFFFFFFFFFFFFCFA5FFFFEFFFFFFF
        FFF7EFFFFFEFFFFFFFFFFFFFAD9684E7FFFFE7FFFFEFE7E7EFEFEFFFFFFFFFFF
        FFFFFFFF8C514A5A00009C4129FFFFFFFFFFFFFFFFFFFFFFFFD6BEADE7FFFFE7
        FFFFE7FFFFE7FFFFF7EFE7F7F7F7FFFFFFFFFFFFD6CFCE634142C6B6ADFFFFFF
        FFFFFFFFFFFFEFDFD6E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFEFE7
        DEF7F7EFFFFFFFFFFFFFFFFFFFFFFFF7DECFC6E7FFFFE7FFFFE7FFFFE7FFFFE7
        FFFF}
      Transparent = True
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mSpacerGraph: TmmPanel
      Left = 297
      Top = 0
      Width = 9
      Height = 161
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 2
    end
    object mRankingAlarmChart: TmmChart
      Left = 0
      Top = 0
      Width = 297
      Height = 161
      BackWall.Brush.Color = clWhite
      Title.Font.Charset = DEFAULT_CHARSET
      Title.Font.Color = clBlack
      Title.Font.Height = -12
      Title.Font.Name = 'Arial'
      Title.Font.Style = []
      Title.Text.Strings = (
        '(*)Haeufigkeit der Alarme')
      OnClickSeries = mRankingChartClickSeries
      OnClickBackground = RankingClickBackgraound
      View3D = False
      Align = alLeft
      BevelOuter = bvNone
      BevelWidth = 0
      TabOrder = 0
      OnMouseMove = mChartMouseMove
      MouseMode = mmNormal
    end
    object mRankingMachineChart: TmmChart
      Left = 306
      Top = 0
      Width = 388
      Height = 161
      BackWall.Brush.Color = clWhite
      Title.Font.Charset = DEFAULT_CHARSET
      Title.Font.Color = clBlack
      Title.Font.Height = -12
      Title.Font.Name = 'Arial'
      Title.Font.Style = []
      Title.Text.Strings = (
        '(*)Haeufigkeit der Maschinen')
      OnClickSeries = mRankingChartClickSeries
      OnClickBackground = RankingClickBackgraound
      View3D = False
      Align = alClient
      BevelOuter = bvNone
      BevelWidth = 0
      TabOrder = 1
      OnMouseMove = mChartMouseMove
      MouseMode = mmNormal
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    OnAfterTranslate = mmTranslator1AfterTranslate
    Left = 418
    Top = 72
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
end
