program MMQOfflimit;

uses
  MemCheck,
  Forms,
  LoepfeGlobal,
  ActiveX,
  Windows,
  sysutils,
  BASEFORM in '..\..\..\LoepfeShares\Templates\BASEFORM.pas' {mmForm},
  MainWindow in 'MainWindow.pas' {frmMainWindow},
  SettingsAlarmFrame in 'SettingsAlarmFrame.pas' {frmSettingsAlarmFrame: TFrame},
  SettingsItemFrame in 'SettingsItemFrame.pas' {frmSettingsItemFrame: TFrame},
  MethodBaseFrame in 'MethodBaseFrame.pas' {frmMethodBaseFrame: TFrame},
  MethodContainerFrame in 'MethodContainerFrame.pas' {frmMethodContainerFrame: TFrame},
  MethodAVGFrame in 'MethodAVGFrame.pas' {frmMethodAVGFrame: TFrame},
  AssignAlarmFrame in 'AssignAlarmFrame.pas' {frmAssignAlarmFrame: TFrame},
  QOGUIShared in 'QOGUIShared.pas',
  AssignStyleFrame in 'AssignStyleFrame.pas' {frmAssignStyleFrame: TFrame},
  AnalyzeOverviewFrame in 'AnalyzeOverviewFrame.pas' {frmAnalyzeOverviewFrame: TFrame},
  AnalyzeAlarmFrame in 'AnalyzeAlarmFrame.pas' {frmAnalyzeAlarmFrame: TFrame},
  AnalyzeChartFrame in 'AnalyzeChartFrame.pas' {frmAnalyzeChartFrame: TFrame},
  BASEAPPLMAIN in '..\..\..\LOEPFESHARES\TEMPLATES\BASEAPPLMAIN.pas' {BaseApplMainForm},
  MainMDIForm in 'MainMDIForm.pas' {frmMainMDIForm},
  MMQOfflimit_TLB in 'MMQOfflimit_TLB.pas',
  MMQOfflimitServerImpl in 'MMQOfflimitServerImpl.pas' {MMQOfflimitServer: CoClass},
  QOReportSettings in 'QOReportSettings.pas' {frmQOReportSettings},
  QOReportShiftList in 'QOReportShiftList.pas' {frmQOReportShiftList},
  QOReportAnalyzeChart in 'QOReportAnalyzeChart.pas' {frmQOAnalyzeChartReport},
  QODetailInfoForm in 'QODetailInfoForm.pas' {frmDetailInfo},
  QOKernel in 'QOKernel.PAS',
  QOGUIClasses in 'QOGUIClasses.pas',
  EventInfo in 'EventInfo.pas' {frmEventInfo},
  MethodFixFrame in 'MethodFixFrame.pas' {frmMethodFixFrame: TFrame},
  dialogs,
  AdoDBAccess,
  SettingsReader,
  mmcs,
  BASEDialog in '..\..\..\LoepfeShares\Templates\BASEDIALOG.pas' {mmDialog},
  BASEDialogBottom in '..\..\..\LoepfeShares\Templates\BASEDIALOGBOTTOM.pas' {DialogBottom},
  SelectDefectDialog in '..\..\Common\SelectDefectDialog.pas' {frmSelectDefects};

{$R *.TLB}

{$R *.RES}
{$R 'Version.res'}

function CheckDBVersion: boolean;
begin
  with TMMSettingsReader.Instance do
    result := DBVersionOK(gMinDBVersion, True);
end;// function CheckDBVersion: boolean

{$Hints off}
function CheckDBConnection: boolean;
begin
  result := false;
  with TAdoDBAccess.Create(1) do try
    result := Init;
  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try
end;// function CheckDBConnection: boolean;
{$Hints on}

var
    xResult: HResult;

begin
{$IFDEF MemCheck}
  MemChk(cApplicationName);
{$ENDIF}

  xResult := CoInitialize(nil);
  // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
  // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
  if ((xResult <> S_OK) and (xResult <> S_FALSE)) then
    raise Exception.Create('CoInitialize failed');

  try
    gApplicationName := cApplicationName;

    CodeSite.Enabled := CodeSiteEnabled;
    gMinDBVersion    := '4.02.00';

    if CheckDBConnection then begin
      if CheckDBVersion then begin
        Application.Initialize;
        InitQOEnviroment;
        Application.CreateForm(TfrmMainMDIForm, frmMainMDIForm);
  Application.Run;
      end;// if CheckDBVersion then begin
    end else begin
      showMessage(cDBConnectionFailed);
    end;// if CheckDBConnection then begin
  finally
    CoUninitialize;
  end;
end.
