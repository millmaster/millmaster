(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MethodBaseFrame.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Basis Frame f�r die Methoden
|                 F�r jede Methode muss ein Nachfahre von diesem Frame implementiert
|                 werden. Hier werden die Methoden speziefischen Settings erfasst
|                 und verwaltet.
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 08.05.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit MethodBaseFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, mmPanel, mmImage, StdCtrls, mmStaticText, mmCheckBox, QOKernel,
  IvDictio, IvMulti, IvEMulti, mmTranslator, CalcMethods, mmLabel, CalcMethodBaseFrame, mmStringList,
  mmMemo, mmBevel;

type
  TfrmMethodBaseFrame = class;
  //1 Leitet das Ereignis der Steuerelemente weiter sobald an einer Methode etwas ge�ndert wurde 
  TOnMethodChangeEvent = procedure (aSender: TFrame; aMethod: TQOMethod) of object;
  //1 Wird aufgerufen, wenn eine Methode auf- oder zugeklappt wird 
  TExpandCollapseMethod = procedure (Sender: TObject; aExpanded: boolean) of object;
  (*: Klasse:        TfrmMethodBaseFrame
      Vorg�nger:     TFrame
      Kategorie:     View
      Kurzbeschrieb: Dieser Frame dient als Ausgangspunkt f�r die Frames der verschiedenen Methoden 
      Beschreibung:  
                     Jede Berechnungs Methode muss f�r die Settings einen Nachfolger dieses Frames
                     implementieren. *)
  TfrmMethodBaseFrame = class(TFrame)
    bExpand: TmmImage;
    cbActivateMethod: TmmCheckBox;
    edMethodDescription: TmmMemo;
    laMethodName: TmmStaticText;
    mDescriptionSpacePanel: TmmPanel;
    mInfoImage: TmmImage;
    mInfoPanel: TmmPanel;
    mmBevel1: TmmBevel;
    mMethodImage: TmmImage;
    mMethodNamePanel: TmmPanel;
    mMethodPanel: TmmPanel;
    mMinusImage: TmmImage;
    mmPanel3: TmmPanel;
    mPlusImage: TmmImage;
    mTranslator: TmmTranslator;
    paCalcMethodFrame: TmmPanel;
    //1 �ffnet den Frame oder "klappt" ihn zusammen 
    procedure bExpandClick(Sender: TObject);
    //1 Aktiviert- oder deaktiviert die entsprechende Methode 
    procedure GUIChange(Sender: TObject); virtual;
    //1 Kennzeichent die Methode als verf�gbar (aktivierbar) 
    procedure SetMethodValid(Sender: TObject); virtual;
  private
    FImages: TImageList;
    FOnExpandCollapseMethod: TExpandCollapseMethod;
    FOnMethodChange: TOnMethodChangeEvent;
    FUpdateCnt: Integer;
    mMethodPanelHeight: Integer;
    mReadOnly: Boolean;
    //1 Setzt die Imageliste f�r das Bitmap im Header 
    procedure SetImages(Value: TImageList); virtual;
  protected
    FCalcMode: Integer;
    FMethod: TQOMethod;
    mCalcMethodFrame: TfrmCalcMethodBaseFrame;
    mMethodParam: TmmStringList;
    mMethodType: TCalculationMethod;
    //1 Wird aufgerufen, wenn im GUI des Berechnungsmethoden Frames etwas ge�ndert wurde 
    procedure CalcMethodChange(aSender: TfrmCalcMethodBaseFrame);
    //1 Feuert den Event, wenn auf das Plus- oder Minuszeichen geklickt wurde 
    procedure DoExpandCollapseMethod(Sender: TObject; aExpanded: boolean); virtual;
    //1 Ruft das EReignis OnMethodeChange auf. 
    procedure DoMethodChange(aSender: TFrame; aMethod: TQOMethod); virtual;
    //1 Update Pattern 
    function IsUpdating: Boolean;
    //1 Setzt ein Edit Control auf ReadOnly 
    procedure SetEditReadOnly(aEdit: TCustomEdit);
    //1 Zeigt die darzustellende Methode an 
    procedure SetMethod(Value: TQOMethod); virtual;
    //1 Update Pattern 
    procedure SetUpdating(Updating: Boolean);
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent); reintroduce; override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 �bernimmt alle Einstellungen in das �bergebene Objekt 
    function ApplySettings(aMethod: TQOMethod): Boolean; virtual;
    //1 Update Pattern 
    procedure BeginUpdate;
    //1 Klappt den Frame zusammen 
    procedure Collapse;
    //1 Update Pattern 
    procedure EndUpdate;
    //1 macht den Frame auf und zeigt die Kontrollelemente f�r die Settings 
    procedure Expand;
    //1 True, wenn die Methode ausgeklappt ist 
    function Expanded: Boolean;
    //1 Holt die Settings aller Methoden die vom Frame angezeigt werdenn. 
    function GetMethodSettings: String;
    //1 F�hrt G�ltigkeitspr�fungen durch 
    function GetValidity: Boolean; virtual;
    //1 True, wenn die Methode editiert wurde 
    function MethodChanged: Boolean; virtual;
    //1 Speichert die Einstellungen der zugeordneten Methode 
    function SaveSettings: Boolean;
    //1 F�llt alle Felder des GUI's mit den Werten der Methode 
    procedure SetGUIValues;
    //1 Versetzt den Frame in den ReadOnly Modus 
    procedure SetReadOnly; virtual;
    //1 F�llt alle Felder des GUI's mit den Werten der Methode 
    procedure _SetGUIValues; virtual; abstract;
    //1 Kalkulationsmodus f�r die L�ngenbasis (zB. Imperfektionen) 
    property CalcMode: Integer read FCalcMode write FCalcMode;
    //1 Liste mit den Images die verwendet werden um die Methode anzuzeigen 
    property Images: TImageList read FImages write SetImages;
    //1 Objekt das dargestellt wird 
    property Method: TQOMethod read FMethod write SetMethod;
    //1 Wird aufgerufen, wenn in einer Methode auf ein Steuerelement geklickt wird 
    property OnMethodChange: TOnMethodChangeEvent read FOnMethodChange write FOnMethodChange;
  published
    //1 Wird gefeuert, wenn auf das Plus- oder Minuszeichen geklickt wurde 
    property OnExpandCollapseMethod: TExpandCollapseMethod read FOnExpandCollapseMethod write FOnExpandCollapseMethod;
  end;
  
  TfrmBaseMethodFrameClass = class of TfrmMethodBaseFrame;

type
  (*: Klasse:        TMethodVisualStyleRec
      Vorg�nger:     
      Kategorie:     record
      Kurzbeschrieb: In diesem Record muss die entsprechende Berechnungsmethode ihre Daten hinterlegen 
      Beschreibung:  
                     Da die Berechnungs Methoden ausserhalb des Frames bekannt sein m�ssen 
                     (z.B im Tree f�r die Bitmaps) muss jede Berechnungs Methode den ImageIndex und
                     die zu erzeugende Klasse f�r den Frame hinterlegen. Diese Daten werden in einem 
                     Array erfasst.
                     Dieses Vorgehen ergibt sich daraus, dass die Klasse f�r die Berechnungsmethode
                     ihre Darstellung im GUI nicht kennt (nicht kennen darf, da sonst Verbindungen
                     zwischen Modell und GUI fest verdrahtet w�ren). *)
  TMethodVisualStyleRec = record
    ImageIndex: Integer;
    SettingsFrameClass: TfrmBaseMethodFrameClass;
  end;
  
  TMethodVisualStyleArray = array[TCalculationMethod] of TMethodVisualStyleRec;
const
  cCalcMethodImages: Array[TCalculationMethod]of integer = (0, 1);

var
  gMethodVisualStyles: TMethodVisualStyleArray;

implementation
uses
  MethodFixFrame, MethodAVGFrame, typInfo, mmNumCtrl, mmcs, AvgBorderFrame, FixBorderFrame;

{$R *.DFM}

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmMethodBaseFrame.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

//: ----------------------------------------------
  mMethodParam := TmmStringList.Create;
  
  mMethodPanelHeight := mMethodPanel.Height;
  laMethodName.Caption := Translate(cCalculationMethods[mMethodType].Name);

//: ----------------------------------------------
  // Frame f�r die Methoden Parameter erzeugen
  mCalcMethodFrame := CalcMethodFrameFactory(mMethodType, nil);
  
  if assigned(mCalcMethodFrame) then begin
    with mCalcMethodFrame do begin
      OnGUIMethodSettingsChanging := CalcMethodChange;
  
      edMethodDescription.Text := Description;
      paCalcMethodFrame.Height := Height;
      paCalcMethodFrame.Width := Width;
      parent := paCalcMethodFrame;
    end;// with mMethodFrame do begin
  end else begin
    mInfoPanel.visible := false;
  end;// if assigned(mCalcMethodFrame) then begin
end;// TfrmMethodBaseFrame.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmMethodBaseFrame.Destroy;
begin
  FreeAndNil(mMethodParam);
  FreeAndNil(mCalcMethodFrame);

//: ----------------------------------------------
  inherited Destroy;
end;// TfrmMethodBaseFrame.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           ApplySettings
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (aMethod)
 *
 *  Kurzbeschreibung: �bernimmt alle Einstellungen in das �bergebene Objekt
 *  Beschreibung:     
                      Nachfahren �berschreiben diese Methode.
                      Die Methode "SaveSettings" ruft die Methode "AplySettings" 
                      auf um die Einstellungen zu speichern. Die Methode wird 
                      ausserdem von der G�ligkeitspr�fung verwendet um die 
                      Plausibilit�t der eingegebenen Parameter zu �berpr�fen.
 --------------------------------------------------------------------*)
function TfrmMethodBaseFrame.ApplySettings(aMethod: TQOMethod): Boolean;
begin
  result := true;
  
  // Die Einstellungen des Frames holen
  mMethodParam.CommaText := mCalcMethodFrame.MethodSettings;
  
  aMethod.Active := cbActivateMethod.Checked;
  
  if Expanded then
    aMethod.ShowDetails := msdFull
  else
    aMethod.ShowDetails := msdNone;
end;// TfrmMethodBaseFrame.ApplySettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           BeginUpdate
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Update Pattern
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodBaseFrame.BeginUpdate;
begin
  Inc(FUpdateCnt);
  if FUpdateCnt = 1 then SetUpdating(True);
end;// TfrmMethodBaseFrame.BeginUpdate cat:No category

//:-------------------------------------------------------------------
(*: Member:           bExpandClick
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: �ffnet den Frame oder "klappt" ihn zusammen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodBaseFrame.bExpandClick(Sender: TObject);
begin
  if mMethodPanel.Visible then
    Collapse
  else
    Expand;
end;// TfrmMethodBaseFrame.bExpandClick cat:No category

//:-------------------------------------------------------------------
(*: Member:           CalcMethodChange
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (aSender)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn im GUI des Berechnungsmethoden Frames etwas ge�ndert wurde
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodBaseFrame.CalcMethodChange(aSender: TfrmCalcMethodBaseFrame);
begin
  GUIChange(self);
end;// TfrmMethodBaseFrame.CalcMethodChange cat:No category

//:-------------------------------------------------------------------
(*: Member:           Collapse
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Klappt den Frame zusammen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodBaseFrame.Collapse;
begin
  ClientHeight := mMethodNamePanel.Height;
  bExpand.Picture := mPlusImage.Picture;
  mMethodPanel.Visible := false;
  // Benachrichtigung
  DoExpandCollapseMethod(self, false);
end;// TfrmMethodBaseFrame.Collapse cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoExpandCollapseMethod
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aExpanded)
 *
 *  Kurzbeschreibung: Feuert den Event, wenn auf das Plus- oder Minuszeichen geklickt wurde
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodBaseFrame.DoExpandCollapseMethod(Sender: TObject; aExpanded: boolean);
begin
  if Assigned(FOnExpandCollapseMethod) then FOnExpandCollapseMethod(Sender, aExpanded);
end;// TfrmMethodBaseFrame.DoExpandCollapseMethod cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoMethodChange
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (aSender, aMethod)
 *
 *  Kurzbeschreibung: Ruft das EReignis OnMethodeChange auf.
 *  Beschreibung:     
                      Dieses Ereignis sollte immer bei einer GUI Aktion aufgerufen werden.
                      Als Folge wird gepr�ft ob die Settings der Methode ge�ndert haben und 
                      entsprechend werden die Buttons zum Speichern und Undo aktiviert.
 --------------------------------------------------------------------*)
procedure TfrmMethodBaseFrame.DoMethodChange(aSender: TFrame; aMethod: TQOMethod);
begin
  if Assigned(FOnMethodChange) then FOnMethodChange(aSender, aMethod);
end;// TfrmMethodBaseFrame.DoMethodChange cat:No category

//:-------------------------------------------------------------------
(*: Member:           EndUpdate
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Update Pattern
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodBaseFrame.EndUpdate;
begin
  Dec(FUpdateCnt);
  if FUpdateCnt = 0 then SetUpdating(False);
end;// TfrmMethodBaseFrame.EndUpdate cat:No category

//:-------------------------------------------------------------------
(*: Member:           Expand
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: macht den Frame auf und zeigt die Kontrollelemente f�r die Settings
 *  Beschreibung:     
                      Jede Berechnungs Methode hat unterschiedliche Settings und daher auch 
                      verschiedne GUI Elemente. Die H�he des Frames wird im Design Mode bestimmt 
                      und beim zusammenklappen gesichert.
 --------------------------------------------------------------------*)
procedure TfrmMethodBaseFrame.Expand;
begin
  mMethodPanel.Visible := true;
  ClientHeight := mMethodNamePanel.Height + mMethodPanelHeight;
  bExpand.Picture := mMinusImage.Picture;
  // Benachrichtigung
  DoExpandCollapseMethod(self, true);
end;// TfrmMethodBaseFrame.Expand cat:No category

//:-------------------------------------------------------------------
(*: Member:           Expanded
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: True, wenn die Methode ausgeklappt ist
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmMethodBaseFrame.Expanded: Boolean;
begin
  result := (mMethodPanel.Visible = true);
end;// TfrmMethodBaseFrame.Expanded cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMethodSettings
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Holt die Settings aller Methoden die vom Frame angezeigt werdenn.
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmMethodBaseFrame.GetMethodSettings: String;
begin
  result := '';
  if assigned(FMethod) then
    // Settings der Methode als String
    result := FMethod.GetMethodSettings;
end;// TfrmMethodBaseFrame.GetMethodSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetValidity
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: F�hrt G�ltigkeitspr�fungen durch
 *  Beschreibung:     
                      Da jede Methode andere Settings hat, muss die Plausibilit�tspr�fung in der
                      Klasse der Methode erfolgen. Hier wird lediglich der Aufruf an das entsprechende
                      Objekt weitergeleitet.
 --------------------------------------------------------------------*)
function TfrmMethodBaseFrame.GetValidity: Boolean;
var
  xMethod: TQOMethod;
begin
  result := false;
  if assigned(FMethod) then begin
    xMethod := TQOMethodClass(FMethod.ClassType).Create(nil);
    try
      ApplySettings(xMethod);
      result := xMethod.MethodValid;
    finally
      FreeAndNil(xMethod);
    end;// try finally
  end;// if assigned(FMethod) then begin
end;// TfrmMethodBaseFrame.GetValidity cat:No category

//:-------------------------------------------------------------------
(*: Member:           GUIChange
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Aktiviert- oder deaktiviert die entsprechende Methode
 *  Beschreibung:     
                      Das Ereignis wird nach "draussen" weitergeleitet
 --------------------------------------------------------------------*)
procedure TfrmMethodBaseFrame.GUIChange(Sender: TObject);
var
  xValid: Boolean;
begin
  DoMethodChange(self, FMethod);

//: ----------------------------------------------
  if not(IsUpdating) then begin
    xValid := GetValidity;
  
    (*if Sender <> cbActivateMethod then
      cbActivateMethod.Checked := xValid and (assigned(Method) and (Method.Active));*)
    cbActivateMethod.visible := xValid and (not(mReadOnly));
  end;// if not(IsUpdating) then begin
end;// TfrmMethodBaseFrame.GUIChange cat:No category

//:-------------------------------------------------------------------
(*: Member:           IsUpdating
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Update Pattern
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmMethodBaseFrame.IsUpdating: Boolean;
begin
  Result := FUpdateCnt > 0;
end;// TfrmMethodBaseFrame.IsUpdating cat:No category

//:-------------------------------------------------------------------
(*: Member:           MethodChanged
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: True, wenn die Methode editiert wurde
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmMethodBaseFrame.MethodChanged: Boolean;
var
  xShowDetail: TMethodShowDetail;
begin
  result := false;
  
  // Die Einstellungen des Frames holen
  mMethodParam.CommaText := mCalcMethodFrame.MethodSettings;
  
  if assigned(FMethod) then begin
    result := result or (FMethod.Active <> cbActivateMethod.checked);
  
    if Expanded then
      xShowDetail := msdFull
    else
      xShowDetail := msdNone;
  
    result := result or (FMethod.ShowDetails <> xShowDetail);
  end;// if assigned(FMethod) then begin
end;// TfrmMethodBaseFrame.MethodChanged cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveSettings
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Speichert die Einstellungen der zugeordneten Methode
 *  Beschreibung:     
                      Diese Methode muss in einem Nachfolger �berschrieben werden.
 --------------------------------------------------------------------*)
function TfrmMethodBaseFrame.SaveSettings: Boolean;
begin
  result := ApplySettings(FMethod);
end;// TfrmMethodBaseFrame.SaveSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetEditReadOnly
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (aEdit)
 *
 *  Kurzbeschreibung: Setzt ein Edit Control auf ReadOnly
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodBaseFrame.SetEditReadOnly(aEdit: TCustomEdit);
begin
  if aEdit is TEdit then begin
    TEdit(aEdit).ReadOnly := true;
    TEdit(aEdit).Color := clInfoBK;
  end else begin
    if aEdit is TmmNumEdit then begin
      TmmNumEdit(aEdit).ReadOnly := true;
      TmmNumEdit(aEdit).Color := clInfoBK;
    end;// if aEdit is TNumEdit then begin
  end;// if aEdit is TEdit then begin
end;// TfrmMethodBaseFrame.SetEditReadOnly cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetGUIValues
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: F�llt alle Felder des GUI's mit den Werten der Methode
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodBaseFrame.SetGUIValues;
begin
  try
    BeginUpdate;

//: ----------------------------------------------
    // Methode nur ausklappen wenn sie aktiviert ist
    if (cbActivateMethod.Checked and (FMethod.ShowDetails = msdDefault))
        or (FMethod.ShowDetails = msdFull) then
      Expand
    else
      Collapse;
  
    // Parameter f�r den CalcMethodFrame zusammensuchen
    mMethodParam.Values[cMethodType] := ord(mMethodType);
  
    mMethodParam.Values[cCalcMode]   := FCalcMode;
    mMethodParam.Values[cLenBase]    := FMethod.LenBase;
  
    _SetGUIValues;

//: ----------------------------------------------
  finally
    EndUpdate;
  end;// try finally
end;// TfrmMethodBaseFrame.SetGUIValues cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetImages
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Imageliste f�r das Bitmap im Header
 *  Beschreibung:     
                      Wird die Imageliste zugewiesen, dann muss das entsprechende 
                      Bitmap kopiert werden.
 --------------------------------------------------------------------*)
procedure TfrmMethodBaseFrame.SetImages(Value: TImageList);
var
  xBitmap: TBitmap;
begin
  FImages := Value;

//: ----------------------------------------------
  if assigned(Value) then begin
    xBitmap := TBitmap.create;
    try
      xBitmap.Width  := Images.Width;
      xBitmap.Height := Images.Height;
      Images.GetBitmap(gMethodVisualStyles[mMethodType].ImageIndex, xBitmap);
  
      mMethodImage.Picture.Bitmap := xBitmap;
    finally
      xBitmap.Free;
    end;// try finally
  end else begin
    mMethodImage.Picture := nil;
  end;// if assigned(Value) then begin
end;// TfrmMethodBaseFrame.SetImages cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetMethod
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zeigt die darzustellende Methode an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodBaseFrame.SetMethod(Value: TQOMethod);
var
  xValid: Boolean;
begin
  FMethod := Value;

//: ----------------------------------------------
  // In SetGUIValues k�nnen die Nachfahren die Settings in die StringList schreiben
  SetGUIValues;
  
  mCalcMethodFrame.MethodSettings := mMethodParam.CommaText;

//: ----------------------------------------------
  xValid := GetValidity;
  
  cbActivateMethod.Checked := xValid and (assigned(Method) and (Method.Active));
  cbActivateMethod.visible := xValid;
end;// TfrmMethodBaseFrame.SetMethod cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetMethodValid
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Kennzeichent die Methode als verf�gbar (aktivierbar)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodBaseFrame.SetMethodValid(Sender: TObject);
begin
  (*  mItem.SetValid1(mMethod);
    cbActivateMethod.Enabled := true;
    cbActivateMethod.visible := true;*)
end;// TfrmMethodBaseFrame.SetMethodValid cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetReadOnly
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Versetzt den Frame in den ReadOnly Modus
 *  Beschreibung:     
                      Der RO-Modus wird gebraucht, wenn die Settings lediglich
                      angezeigt werden (Analyse).
 --------------------------------------------------------------------*)
procedure TfrmMethodBaseFrame.SetReadOnly;
begin
  mReadOnly := true;
  cbActivateMethod.visible := false;
  mCalcMethodFrame.ReadOnly := true;
end;// TfrmMethodBaseFrame.SetReadOnly cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetUpdating
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (Updating)
 *
 *  Kurzbeschreibung: Update Pattern
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodBaseFrame.SetUpdating(Updating: Boolean);
begin
end;// TfrmMethodBaseFrame.SetUpdating cat:No category



var
  i: TCalculationMethod;

initialization
  // globales Array f�r die visualisierung der Methoden f�llen
  for i := Low(gMethodVisualStyles) to High(gMethodVisualStyles) do begin
    with gMethodVisualStyles[i] do begin
      ImageIndex := cCalcMethodImages[i];
      case i of
        cmFix : SettingsFrameClass := TfrmMethodFixFrame;
        cmAVG   : SettingsFrameClass := TfrmMethodAVGFrame;
      else
        raise Exception.create('MethodBaseFrame.initialization: CalcMethod not definend (' + GetEnumName(TypeInfo(TCalculationMethod),ord(i)) + ')');
      end;// case i of
    end;// with gMethodVisualStyles[i] do begin
  end;// for i := Low(gMethodVisualStyles) to High(gMethodVisualStyles) do begin

end.






