inherited frmMainMDIForm: TfrmMainMDIForm
  Left = 276
  Top = 178
  Width = 884
  Height = 752
  Caption = '(*)Q-Offlimit'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited plTopPanel: TmmPanel
    Width = 876
    inherited Panel2: TmmPanel
      Left = 812
    end
    inherited mToolBar: TmmToolBar
      Width = 812
    end
  end
  inherited mmStatusBar: TmmStatusBar
    Top = 688
    Width = 876
  end
  inherited mDictionary: TmmDictionary
    AutoConfig = True
    OnAfterLangChange = mDictionaryAfterLangChange
  end
  object mSecurityDB: TMMSecurityDB
    Active = True
    CheckApplStart = False
    Left = 384
    Top = 152
  end
  object MMHtmlHelp: TMMHtmlHelp
    AutoConfig = True
    MMHelp = mmpEasy
    Left = 448
    Top = 56
  end
end
