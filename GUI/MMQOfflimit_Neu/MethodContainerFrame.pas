(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MethodContainerFrame.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Container f�r die Methoden
|                 Hier werden die Einstellungsm�glichkeiten der einzelnen
|                 Methoden in einem gemeinsamen Container verwaltet.
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 08.05.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit MethodContainerFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, QOKernel,
  MethodBaseFrame, mmScrollBox, StdCtrls, mmLineLabel, IvDictio, IvMulti,
  IvEMulti, mmTranslator, CalcMethods;

type
  (*: Klasse:        TfrmMethodContainerFrame
      Vorg�nger:     TFrame
      Kategorie:     View
      Kurzbeschrieb: Container f�r die Darstellung der verf�gbaren Methoden 
      Beschreibung:  
                     Alle Methoden werden angezeigt. Sobald eine Methode mit g�ltigen Werten
                     "bef�llt" worden ist, kann die Methode aktiviert werden.
                     
                     Die Methoden werden in einer Scrollbox dargestellt. Dies f�r den Fall das 
                     mehr Platz ben�tigt wird als vorhanden ist. *)
  TfrmMethodContainerFrame = class (TFrame)
    mMethods: TmmScrollBox;
    mmTranslator1: TmmTranslator;
  private
    FImages: TImageList;
    FMethodCount: Integer;
    FOnExpandCollapseMethod: TExpandCollapseMethod;
    FOnMethodChange: TOnMethodChangeEvent;
    FQOItem: TQOItem;
    //1 Gibt die H�he aller Methoden zur�ck (Wenn eine Methode "zu" ist wird nur diese H�he verwendet) 
    function GetContainerHeight: Integer;
    //1 Gibt den Methoden Frame f�r die entsprechende Methode zur�ck 
    function GetMethodFrame(Index: Integer): TfrmMethodBaseFrame;
    //1 Gibt alle Methoden Frames frei. 
    procedure HideMethods;
  protected
    FCalcMode: Integer;
    //1 Ruft das EReignis OnMethodeChange auf. 
    procedure DoMethodChange(aSender: TFrame; aMethod: TQOMethod);
    //1 Wird gefeuert, wenn auf das Plus- oder Minuszeichen einer Methode geklickt wurde 
    procedure ExpandCollapseMethod(Sender: TObject; aExpanded: boolean);
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent); override;
    //1 Holt die Settings aller Methoden die vom Frame angezeigt werdenn. 
    function GetMethodSettings: String;
    //1 True, wenn eine der Methoden editiert wurde 
    function MethodsChanged: Boolean;
    //1 Speichert die Einstellungen aller Methoden Objekte 
    function SaveSettings: Boolean;
    //1 Zeigt die Methoden eines einzelnen Items an 
    procedure ShowItem(aQOItem: TQOItem; aImages: TImageList);
    //1 Erzeugt ein Frame f�r eine einzelne Methode 
    procedure ShowMethod(aMethod: TCalculationMethod);
    //1 Erzeugt ein Frame f�r eine einzelne Methode 
    procedure ShowMethodReadOnly(aMethod: TQOMethod);
    //1 Zeigt die Methoden die an einem Alarm beteiligt waren im Read Only Modus an an 
    procedure ShowMethodsReadOnly(aMethodList: TQOMethods; aImages: TImageList);
    //1 Kalkulationsmodus f�r die L�ngenbasis (zB. Imperfektionen) 
    property CalcMode: Integer read FCalcMode write FCalcMode;
    //1 Gibt die H�he aller Methoden zur�ck (Wenn eine Methode "zu" ist wird nur diese H�he verwendet) 
    property ContainerHeight: Integer read GetContainerHeight;
    //1 Liste mit den Images die verwendet werden um die Methode anzuzeigen 
    property Images: TImageList read FImages write FImages;
    //1 Anzahl der dargestellten Methoden 
    property MethodCount: Integer read FMethodCount write FMethodCount;
    //1 Gibt den Methoden Frame f�r die entsprechende Methode zur�ck 
    property MethodFrame[Index: Integer]: TfrmMethodBaseFrame read GetMethodFrame;
    //1 Wird aufgerufen, wenn eine Methode ge�ndert wurde 
    property OnMethodChange: TOnMethodChangeEvent read FOnMethodChange write FOnMethodChange;
    //1 Item dessen Methoden dargestellt werden sollen 
    property QOItem: TQOItem read FQOItem;
  published
    //1 Wird gefeuert, wenn auf das Plus- oder Minuszeichen einer Methode geklickt wurde 
    property OnExpandCollapseMethod: TExpandCollapseMethod read FOnExpandCollapseMethod write FOnExpandCollapseMethod;
  end;
  
implementation

uses
  MethodFixFrame, MethodAVGFrame, typInfo, mmcs;

{$R *.DFM}

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmMethodContainerFrame
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmMethodContainerFrame.Create(aOwner: TComponent);
begin
  inherited;
  HideMethods;
end;// TfrmMethodContainerFrame.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoMethodChange
 *  Klasse:           TfrmMethodContainerFrame
 *  Kategorie:        No category 
 *  Argumente:        (aSender, aMethod)
 *
 *  Kurzbeschreibung: Ruft das EReignis OnMethodeChange auf.
 *  Beschreibung:     
                      Dieses Ereignis sollte immer bei einer GUI Aktion aufgerufen werden.
                      Als Folge wird gepr�ft ob die Settings der Methode ge�ndert haben und 
                      entsprechend werden die Buttons zum Speichern und Undo aktiviert.
 --------------------------------------------------------------------*)
procedure TfrmMethodContainerFrame.DoMethodChange(aSender: TFrame; aMethod: TQOMethod);
begin
  if Assigned(FOnMethodChange) then FOnMethodChange(aSender, aMethod);
end;// TfrmMethodContainerFrame.DoMethodChange cat:No category

//:-------------------------------------------------------------------
(*: Member:           ExpandCollapseMethod
 *  Klasse:           TfrmMethodContainerFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aExpanded)
 *
 *  Kurzbeschreibung: Wird gefeuert, wenn auf das Plus- oder Minuszeichen einer Methode geklickt wurde
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodContainerFrame.ExpandCollapseMethod(Sender: TObject; aExpanded: boolean);
begin
  if assigned(FOnExpandCollapseMethod) then
    FonExpandCollapseMethod(Sender, aExpanded);
end;// TfrmMethodContainerFrame.ExpandCollapseMethod cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetContainerHeight
 *  Klasse:           TfrmMethodContainerFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die H�he aller Methoden zur�ck (Wenn eine Methode "zu" ist wird nur diese H�he verwendet)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmMethodContainerFrame.GetContainerHeight: Integer;
var
  i: Integer;
begin
  result := 0;
  for i := 0 to mMethods.ComponentCount - 1 do
    if mMethods.Components[i] is TFrame then
      result := result + TFrame(mMethods.Components[i]).Height;
end;// TfrmMethodContainerFrame.GetContainerHeight cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMethodFrame
 *  Klasse:           TfrmMethodContainerFrame
 *  Kategorie:        No category 
 *  Argumente:        (Index)
 *
 *  Kurzbeschreibung: Gibt den Methoden Frame f�r die entsprechende Methode zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmMethodContainerFrame.GetMethodFrame(Index: Integer): TfrmMethodBaseFrame;
var
  i: Integer;
  j: Integer;
begin
  result := nil;
  if Index < MethodCount then begin
    i := 0;
    j := 0;
    // Alle Komponenten die Methoden Frames sind untersuchen
    while (i <= Index) and (j < mMethods.ComponentCount) do begin
      if mMethods.Components[j] is TfrmMethodBaseFrame then begin
        // Solange weiter, bis der gew�nschte Frame gefunden ist
        if i = Index then
          result := TfrmMethodBaseFrame(mMethods.Components[j]);
        inc(i);
      end;
      inc(j);
    end;// while i < Index do begin
  end;// if Index < MethodCount then begin
end;// TfrmMethodContainerFrame.GetMethodFrame cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMethodSettings
 *  Klasse:           TfrmMethodContainerFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Holt die Settings aller Methoden die vom Frame angezeigt werdenn.
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmMethodContainerFrame.GetMethodSettings: String;
var
  xSettings: string;
  i: Integer;
begin
  result := '';

//: ----------------------------------------------
  with TStringList.Create do try
    // Von allen Methoden die Settings holen
    for i := 0 to mMethods.ComponentCount - 1 do begin
      if mMethods.Components[i] is TfrmMethodBaseFrame then begin
        // Settings vom Frame abfragen ...
        xSettings := TfrmMethodBaseFrame(mMethods.Components[i]).GetMethodSettings;
        if xSettings > '' then
          // ... und der Liste hinzuf�gen
          add(xSettings);
      end;// if mMethods.Components[i] is TMethodBaseFrame then begin
    end;// for i := 0 to mMethods.ComponentCount - 1 do begin
  
    // Liste mit den Settings f�r alle Methoden (kommasepariert)
    result := CommaText;
  finally
    Free;
  end;// with TmmStringList.Create do try
end;// TfrmMethodContainerFrame.GetMethodSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           HideMethods
 *  Klasse:           TfrmMethodContainerFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt alle Methoden Frames frei.
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodContainerFrame.HideMethods;
begin
  FMethodCount := 0;
  
  // Gibt alle Methoden frei
  while mMethods.ComponentCount > 0 do
    mMethods.Components[0].Free;
end;// TfrmMethodContainerFrame.HideMethods cat:No category

//:-------------------------------------------------------------------
(*: Member:           MethodsChanged
 *  Klasse:           TfrmMethodContainerFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: True, wenn eine der Methoden editiert wurde
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmMethodContainerFrame.MethodsChanged: Boolean;
var
  i: Integer;
begin
  result := false;
  
  i := 0;
  while (i < mMethods.ComponentCount) and(not(result)) do begin
    if mMethods.Components[i] is TfrmMethodBaseFrame then
      result := result or TfrmMethodBaseFrame(mMethods.Components[i]).MethodChanged;
    inc(i);
  end;// while (i < ComponentCount) and(not(result)) do begin
end;// TfrmMethodContainerFrame.MethodsChanged cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveSettings
 *  Klasse:           TfrmMethodContainerFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Speichert die Einstellungen aller Methoden Objekte
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmMethodContainerFrame.SaveSettings: Boolean;
var
  i: Integer;
begin
  result := false;

//: ----------------------------------------------
  for i := 0 to mMethods.ComponentCount - 1 do begin
    if mMethods.Components[i] is TfrmMethodBaseFrame then
      result := TfrmMethodBaseFrame(mMethods.Components[i]).SaveSettings and result;
  end;// for i := 0 to mMethods.ComponentCount - 1 do begin
end;// TfrmMethodContainerFrame.SaveSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           ShowItem
 *  Klasse:           TfrmMethodContainerFrame
 *  Kategorie:        No category 
 *  Argumente:        (aQOItem, aImages)
 *
 *  Kurzbeschreibung: Zeigt die Methoden eines einzelnen Items an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodContainerFrame.ShowItem(aQOItem: TQOItem; aImages: TImageList);
var
  xMethod: TCalculationMethod;
begin
  // DataItem sichern
  FQOItem := aQOItem;
  if assigned(FQOItem) then
    FCalcMode := FQOItem.CalcMode;
  
  // Allf�llig sichtbare Methoden erstmal entfernen
  HideMethods;
  
  // Bitmaps f�r die Methoden sind in dieser Liste gespeichert
  Images := aImages;

//: ----------------------------------------------
  if assigned(FQOItem) then begin
    for xMethod := High(TCalculationMethod) downto Low(TCalculationMethod) do
      ShowMethod(xMethod);
  end;// if assigned(FQOItem) then begin
end;// TfrmMethodContainerFrame.ShowItem cat:No category

//:-------------------------------------------------------------------
(*: Member:           ShowMethod
 *  Klasse:           TfrmMethodContainerFrame
 *  Kategorie:        No category 
 *  Argumente:        (aMethod)
 *
 *  Kurzbeschreibung: Erzeugt ein Frame f�r eine einzelne Methode
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodContainerFrame.ShowMethod(aMethod: TCalculationMethod);
begin
  with gMethodVisualStyles[aMethod].SettingsFrameClass.Create(mMethods) do begin
    Parent   := mMethods;
    Visible  := true;
    Align    := alTop;
    Images   := self.Images;
    CalcMode := FCalcMode;
    inc(FMethodCount);
    if assigned(QOItem) then begin
      Method  := QOItem.QOMethods.MethodByType(aMethod);
      OnMethodChange := DoMethodChange;
    end;// if assigned(QOItem) then begin
  end;// with gMethodVisualStyles[aMethod].SettingsFrameClass.Create(mMethods) do begin
end;// TfrmMethodContainerFrame.ShowMethod cat:No category

//:-------------------------------------------------------------------
(*: Member:           ShowMethodReadOnly
 *  Klasse:           TfrmMethodContainerFrame
 *  Kategorie:        No category 
 *  Argumente:        (aMethod)
 *
 *  Kurzbeschreibung: Erzeugt ein Frame f�r eine einzelne Methode
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodContainerFrame.ShowMethodReadOnly(aMethod: TQOMethod);
begin
  if assigned(aMethod) then begin
    with gMethodVisualStyles[aMethod.MethodType].SettingsFrameClass.Create(mMethods) do begin
      Parent   := mMethods;
      Visible  := true;
      Align    := alTop;
      Images   := self.Images;
      CalcMode := FCalcMode;
      // Zeigt die Settings an
      Method   := aMethod;
  
      SetReadOnly;
      OnExpandCollapseMethod := ExpandCollapseMethod;
      Collapse;
      inc(FMethodCount);
    end;// with gMethodVisualStyles[aMethod.MethodType].SettingsFrameClass.Create(mMethods) do begin
  end;// if assigned(aMethod) then begin
end;// TfrmMethodContainerFrame.ShowMethodReadOnly cat:No category

//:-------------------------------------------------------------------
(*: Member:           ShowMethodsReadOnly
 *  Klasse:           TfrmMethodContainerFrame
 *  Kategorie:        No category 
 *  Argumente:        (aMethodList, aImages)
 *
 *  Kurzbeschreibung: Zeigt die Methoden die an einem Alarm beteiligt waren im Read Only Modus an an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMethodContainerFrame.ShowMethodsReadOnly(aMethodList: TQOMethods; aImages: TImageList);
var
  i: Integer;
begin
  // Allf�llig sichtbare Methoden erstmal entsorgen
  HideMethods;
  
  // Bitmaps f�r die Methoden sind in dieser Liste gespeichert
  Images := aImages;

//: ----------------------------------------------
  for i := aMethodList.MethodCount - 1 downto 0 do
    ShowMethodReadOnly(aMethodList[i]);
end;// TfrmMethodContainerFrame.ShowMethodsReadOnly cat:No category


end.



