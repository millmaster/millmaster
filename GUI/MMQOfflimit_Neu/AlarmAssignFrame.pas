(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: AlarmAssignFrame.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Frame f�r die Zuordnung von Artikeln und Maschinen zu einem Alarm
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 17.07.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit AlarmAssignFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, mmSpeedButton, ExtCtrls, mmImage, StdCtrls, mmListBox, mmLabel,
  mmPanel, ImgList, mmImageList, ActnList, mmActionList, ComCtrls, ToolWin,
  mmToolBar, QOSettings, AdoDBAccess, mmBitBtn, mmStaticText, VirtualTrees,
  mmVirtualStringTree, mmSplitter, ActiveX, QOGroup, mmEdit, IvDictio,
  IvMulti, IvEMulti, mmTranslator, QOGUIShared;

type
  TfrmAlarmAssignFrame = class;
  (*: Klasse:        TfrmAlarmAssignFrame
      Vorg�nger:     TFrame
      Kategorie:     View
      Kurzbeschrieb: Frame f�r die Zuordnung eines Styles zu einem Alarm 
      Beschreibung:  
                     - *)
  TfrmAlarmAssignFrame = class (TFrame)
    bAddMachine: TmmSpeedButton;
    bAddStyle: TmmSpeedButton;
    bShowMachines: TmmSpeedButton;
    edIncrementalStyle: TmmEdit;
    imMachCollapse: TmmImage;
    imMachExpand: TmmImage;
    mBottomButtonPanel: TmmPanel;
    mBottomPanel: TmmPanel;
    mHorSplitter: TmmSplitter;
    mIncrementalStylePanel: TmmPanel;
    mMachineList: TmmVirtualStringTree;
    mmPanel1: TmmPanel;
    mmPanel2: TmmPanel;
    mRightPanel: TmmPanel;
    mStyleList: TmmVirtualStringTree;
    mTopButtonPanel: TmmPanel;
    mTopPanel: TmmPanel;
    mTranslator: TmmTranslator;
    //1 F�gt die selektierten Maschinen zu den selektierten Artikeln dazu 
    procedure bAddMachineClick(Sender: TObject);
    //1 F�gt die selektierten Artikel zum Assign Tree hinzu 
    procedure bAddStyleClick(Sender: TObject);
    //1 Zeigt oder verbirgt das Panel mit den Maschinen 
    procedure bShowMachinesClick(Sender: TObject);
    //1 Selektiert den Knoten der dem Text entspricht 
    procedure edIncrementalStyleChange(Sender: TObject);
    //1 Stellt das Editfeld auf Eingabe um 
    procedure edIncrementalStyleEnter(Sender: TObject);
    //1 Stellt das Editfeld auf Anzeige um 
    procedure edIncrementalStyleExit(Sender: TObject);
    //1 Passt die Gr�sse des Editfeldes an die neue Gr�sse an 
    procedure mIncrementalStylePanelResize(Sender: TObject);
    //1 Gibt den ImageIndex des selektierten Objektes zur�ck 
    procedure mMachineListGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: 
      TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
    //1 Gibt den Text des gew�nschten Knoten zur�ck 
    procedure mMachineListGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: 
      TVSTTextType; var CellText: WideString);
    //1 Berechnet die Breite f�r die erste Spalte 
    procedure mMachineListResize(Sender: TObject);
    //1 Setzt den neu selktierten Artikel und sortiert die Mashcinen liste entsprechend 
    procedure mStyleListFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);
    //1 Fragt jedesmal wenn der Mauscursor bewegt wird, welcher Cursor denn dargestellt werden soll 
    procedure mStyleListGetCursor(Sender: TBaseVirtualTree; var Cursor: TCursor);
    //1 Gibt den ImageIndex des selektierten Objektes zur�ck 
    procedure mStyleListGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: 
      TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
    //1 - 
    procedure mStyleListGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: 
      TVSTTextType; var CellText: WideString);
    //1 Berechnet die Breite f�r die erste Spalte 
    procedure mStyleListResize(Sender: TObject);
    //1 �bernimmt alle �bersetungen die nicht automatisch "passieren" 
    procedure mTranslatorAfterTranslate(translator: TIvCustomTranslator);
    //1 Sortierfunktion (Callback) f�r das TreeView 
    procedure TreeCompareNodes(Sender: TBaseVirtualTree; Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: 
      Integer);
    //1 Gibt das Eingebettete Objekt frei 
    procedure TreeFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    //1 Sortiert die Lsite nach dem entsprechenden Kriterium 
    procedure TreeHeaderClick(Sender: TVTHeader; Column: TColumnIndex; Button: TMouseButton; Shift: TShiftState; X, Y: 
      Integer);
  private
    FOnAddItems: TOnAddItems;
    FQOAlarm: TQOAlarm;
    FTreeImages: TImageList;
    mInitialized: Boolean;
    mSelectedStyle: TQOStyle;
    //1 Macht die Maschinen verf�gbar oder nicht 
    procedure EnableMachine(aEnable: boolean);
    //1 Zugriffsmethode f�r ExpandMachines 
    function GetExpandMachines: Boolean;
    //1 Zugriffsmethode f�r ExpandMachines 
    procedure SetExpandMachines(Value: Boolean);
    //1 Zugriffsmethode f�r TreeImages 
    procedure SetTreeImages(Value: TImageList);
  protected
    //1 ruft die Ereignisprozedur auf, wenn Artikel oder Maschinen hinzugef�gt werden sollen 
    procedure DoAddItems(Sender: TBaseVirtualTree);
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Gibt den Button f�r das Hinzuf�gen der Maschinen frei (oder sperrt) 
    procedure EnableAddMachine(aEnable: boolean);
    //1 Gibt den Button f�r das Hinzuf�gen der Artikel frei (oder sperrt) 
    procedure EnableAddStyle(aEnable: boolean);
    //1 Gibt den ImageIndex f�r das selektierte Objekt zur�ck 
    function GetAssignImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: integer): 
      Integer;
    //1 Initialisiert die Sicht (F�llt die Trees von der DB) 
    procedure Init;
    //1 Zeigt oder verbirgt dsa Panel mit den MAschinen 
    property ExpandMachines: Boolean read GetExpandMachines write SetExpandMachines;
    //1 Assiozierter Alarm der dargestellt wird 
    property QOAlarm: TQOAlarm read FQOAlarm write FQOAlarm;
    //1 Bilderliste aus der sich die Trees bedienen (von der Applikation zur Verf�gung gestellt) 
    property TreeImages: TImageList write SetTreeImages;
  published
    //1 Event der ausgel�st wird, wenn Artikel oder Maschinen zum selektierten Alarmn hizugef�gt werden sollen 
    property OnAddItems: TOnAddItems read FOnAddItems write FOnAddItems;
  end;
  

implementation
uses
  QODef, mmDialogs, mmcs;

{$R *.DFM}

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmAlarmAssignFrame.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

//: ----------------------------------------------

//: ----------------------------------------------
  mInitialized := false;
  
  // Die Trees initialisieren
  mStyleList.NodeDataSize := SizeOf(TTreeData);
  mMachineList.NodeDataSize := SizeOf(TTreeData);

//: ----------------------------------------------
  // Grundeinstellungen
  mBottomPanel.Height := TQOAppSettings.Instance.AlarmAssignMachinesPanelHeight;
  
  // Ob die Maschinen angezeigt werden sollen oder nicht, aus den Settings
  ExpandMachines := TQOAppSettings.Instance.AlarmAssignMachinesAssignable;

//: ----------------------------------------------
  SetIncSearchControlState(edIncrementalStyle);

//: ----------------------------------------------
  // Sortierreihenfolge
  mStyleList.Header.SortColumn := TQOAppSettings.Instance.AssignStyleSortCol;
  mStyleList.Header.SortDirection := TQOAppSettings.Instance.AssignStyleSortDir;
  
  mMachineList.Header.SortColumn := TQOAppSettings.Instance.AssignMachSortCol;
  mMachineList.Header.SortDirection := TQOAppSettings.Instance.AssignMachSortDir;
end;// TfrmAlarmAssignFrame.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmAlarmAssignFrame.Destroy;
begin

//: ----------------------------------------------
  TQOAppSettings.Instance.AlarmAssignMachinesPanelHeight := mBottomPanel.Height;

//: ----------------------------------------------
  mStyleList.Clear;
  mMachineList.Clear;

//: ----------------------------------------------
  inherited Destroy;
end;// TfrmAlarmAssignFrame.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           bAddMachineClick
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: F�gt die selektierten Maschinen zu den selektierten Artikeln dazu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.bAddMachineClick(Sender: TObject);
begin
  DoAddItems(mMachineList);
end;// TfrmAlarmAssignFrame.bAddMachineClick cat:No category

//:-------------------------------------------------------------------
(*: Member:           bAddStyleClick
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: F�gt die selektierten Artikel zum Assign Tree hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.bAddStyleClick(Sender: TObject);
begin
  DoAddItems(mStyleList);
  (*// Beinhaltet alle selektierten Artikel
  xSourceSelection := mStyleList.GetSortedSelection(false);
  
  // F�gt die Artikel zum Tree hinzu
  AddStyles(xSourceSelection);*)
end;// TfrmAlarmAssignFrame.bAddStyleClick cat:No category

//:-------------------------------------------------------------------
(*: Member:           bShowMachinesClick
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Zeigt oder verbirgt das Panel mit den Maschinen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.bShowMachinesClick(Sender: TObject);
begin
  ExpandMachines := not(ExpandMachines);
end;// TfrmAlarmAssignFrame.bShowMachinesClick cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoAddItems
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: ruft die Ereignisprozedur auf, wenn Artikel oder Maschinen hinzugef�gt werden sollen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.DoAddItems(Sender: TBaseVirtualTree);
begin
  if Assigned(FOnAddItems) then FOnAddItems(Sender);
end;// TfrmAlarmAssignFrame.DoAddItems cat:No category

//:-------------------------------------------------------------------
(*: Member:           edIncrementalStyleChange
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Selektiert den Knoten der dem Text entspricht
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.edIncrementalStyleChange(Sender: TObject);
begin
  mStyleList.FindString(edIncrementalStyle.Text, 0, [foPartial, foFirstNode, foFromBegin, foSelect]);
end;// TfrmAlarmAssignFrame.edIncrementalStyleChange cat:No category

//:-------------------------------------------------------------------
(*: Member:           edIncrementalStyleEnter
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Stellt das Editfeld auf Eingabe um
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.edIncrementalStyleEnter(Sender: TObject);
begin
  SetIncSearchControlState(edIncrementalStyle);
end;// TfrmAlarmAssignFrame.edIncrementalStyleEnter cat:No category

//:-------------------------------------------------------------------
(*: Member:           edIncrementalStyleExit
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Stellt das Editfeld auf Anzeige um
 *  Beschreibung:     
                      Das Editfeld zeigt einen Hilfetext an, wenn kein Suchtext 
                      eingegeben wurde und wenn das Editfeld nicht den Folus hat.
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.edIncrementalStyleExit(Sender: TObject);
begin
  SetIncSearchControlState(edIncrementalStyle);
end;// TfrmAlarmAssignFrame.edIncrementalStyleExit cat:No category

//:-------------------------------------------------------------------
(*: Member:           EnableAddMachine
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (aEnable)
 *
 *  Kurzbeschreibung: Gibt den Button f�r das Hinzuf�gen der Maschinen frei (oder sperrt)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.EnableAddMachine(aEnable: boolean);
begin
  bAddMachine.Enabled := aEnable;
end;// TfrmAlarmAssignFrame.EnableAddMachine cat:No category

//:-------------------------------------------------------------------
(*: Member:           EnableAddStyle
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (aEnable)
 *
 *  Kurzbeschreibung: Gibt den Button f�r das Hinzuf�gen der Artikel frei (oder sperrt)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.EnableAddStyle(aEnable: boolean);
begin
  bAddStyle.Enabled := aEnable;
end;// TfrmAlarmAssignFrame.EnableAddStyle cat:No category

//:-------------------------------------------------------------------
(*: Member:           EnableMachine
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (aEnable)
 *
 *  Kurzbeschreibung: Macht die Maschinen verf�gbar oder nicht
 *  Beschreibung:     
                      Die Maschinen werden nicht disabled damit mit Drag & Drop auch
                      dann MAschinen zugeordnet werden, wenn keine Maschine selektiert ist.
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.EnableMachine(aEnable: boolean);
begin
  bAddMachine.Enabled := aEnable;
  //laMachineList.Enabled := aEnable;
  //mMachineList.Enabled := aEnable;
end;// TfrmAlarmAssignFrame.EnableMachine cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAssignImageIndex
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Kind, Column)
 *
 *  Kurzbeschreibung: Gibt den ImageIndex f�r das selektierte Objekt zur�ck
 *  Beschreibung:     
                      Diese Funktion wird von allen drei Trees genutzt.
 --------------------------------------------------------------------*)
function TfrmAlarmAssignFrame.GetAssignImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; 
  Column: integer): Integer;
var
  xQOInstance: TObject;
  xVisualizeInterface: IQOVisualize;
  xAssign: TQOAssign;
begin
  result := -1;
  
  // Den ImageIndex �ber das Interface abfragen
  xQOInstance := GetNodeObject(Sender, Node);
  
  if Kind <> ikOverlay then begin
    case Column of
      // Kein Icon
      0: begin
      end;// 0: begin
      // Maschinen Icon (In Produktion Overlay)
      1: begin
        if assigned(xQOInstance) then begin
          // Jetzt das Interface abfragen
          if xQOInstance.GetInterface(IQOVisualize, xVisualizeInterface) then
            // ImageIndex wird vom Interface geliefert
            result := xVisualizeInterface.ImageIndex[ikAssignNormal];
        end;// if assigned(xQOInstance) then begin
      end;// 1: begin
    end;// case Column of
  end;// if Kind <> ikOverlay then begin
end;// TfrmAlarmAssignFrame.GetAssignImageIndex cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetExpandMachines
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r ExpandMachines
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmAlarmAssignFrame.GetExpandMachines: Boolean;
begin
  result := mBottomPanel.visible;
end;// TfrmAlarmAssignFrame.GetExpandMachines cat:No category

//:-------------------------------------------------------------------
(*: Member:           Init
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Initialisiert die Sicht (F�llt die Trees von der DB)
 *  Beschreibung:     
                      Die Initialisierung muss nach dem Create ausgef�hrt werden, wenn alle 
                      Komponenten komplet erzeugt sind.
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.Init;
var
  xQOStyle: TQOStyle;
  xQOMach: TQOMachine;
  i: Integer;
begin
  // Initialisieren
  mInitialized := false;
  mStyleList.clear;
  mMachineList.Clear;

//: ----------------------------------------------
  // Zuerst einmal "Alle Artikel" definieren
  xQOStyle := TQOAllStyles.Create;
  xQOStyle.LinkID := cAllStylesLinkID;
  AddVSTStructure(mStyleList,nil, xQOStyle, []);
  
  // Alle Artikel aus der Globalen Liste gAllStyles
  for i := 0 to gAllStyles.AssignCount - 1 do begin
    xQOStyle := TQOStyle.Create;
    xQOStyle.Assign(gAllStyles[i]);
    AddVSTStructure(mStyleList,nil, xQOStyle, []);
  end;// for i := 0 to gAllStyles.AssignCount - 1 do begin
  
  // Alle Maschinen aus der Liste gAllMachines
  for i := 0 to gAllMachines.AssignCount - 1 do begin
    xQOMach := TQOMachine.Create;
    xQOMach.Assign(gAllMachines[i]);
    AddVSTStructure(mMachineList,nil, xQOMach, []);
  end;// for i := 0 to gAllMachines.AssignCount - 1 do begin
  
  mInitialized := true;

//: ----------------------------------------------
  // Sortieren
  with mStyleList do
    SortTree(Header.SortColumn, Header.SortDirection, true);
  with mMachineList do
    SortTree(Header.SortColumn, Header.SortDirection, true);
end;// TfrmAlarmAssignFrame.Init cat:No category

//:-------------------------------------------------------------------
(*: Member:           mIncrementalStylePanelResize
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Passt die Gr�sse des Editfeldes an die neue Gr�sse an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.mIncrementalStylePanelResize(Sender: TObject);
begin
  edIncrementalStyle.Width  := mIncrementalStylePanel.ClientWidth;
  edIncrementalStyle.Height := mIncrementalStylePanel.ClientHeight;
end;// TfrmAlarmAssignFrame.mIncrementalStylePanelResize cat:No category

//:-------------------------------------------------------------------
(*: Member:           mMachineListGetImageIndex
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Kind, Column, Ghosted, ImageIndex)
 *
 *  Kurzbeschreibung: Gibt den ImageIndex des selektierten Objektes zur�ck
 *  Beschreibung:     
                      Die verwendete Imagelist wird von der Applikation �ber die Eigenschaft
                      "TreeImages" gesetzt.
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.mMachineListGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: 
  TVTImageKind; Column: TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
begin
  ImageIndex := GetAssignImageIndex(Sender, Node, Kind, Column);
end;// TfrmAlarmAssignFrame.mMachineListGetImageIndex cat:No category

//:-------------------------------------------------------------------
(*: Member:           mMachineListGetText
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Column, TextType, CellText)
 *
 *  Kurzbeschreibung: Gibt den Text des gew�nschten Knoten zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.mMachineListGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; 
  TextType: TVSTTextType; var CellText: WideString);
begin
  if assigned(GetNodeObject(mMachineList, Node)) and (Column = 0) then
    CellText := TQOMachine(GetNodeObject(mMachineList, Node)).QOVisualize.Caption
  else
    CellText := '';
end;// TfrmAlarmAssignFrame.mMachineListGetText cat:No category

//:-------------------------------------------------------------------
(*: Member:           mMachineListResize
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Berechnet die Breite f�r die erste Spalte
 *  Beschreibung:     
                      Die erste Spalte f�llt immer die Breite komplet aus.
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.mMachineListResize(Sender: TObject);
var
  xFirstColWidth: Integer;
  i: Integer;
begin
  // Breite der ersten Spalte bestimmen
  with mMachineList.Header do begin
    (* Die Breite der Spalte berechnet sich aus der Breite des Trees
       minus der Breite aller definierten Methoden Spalten *)
    xFirstColWidth := mMachineList.ClientWidth;
    for i := 1 to Columns.count - 1 do
       xFirstColWidth := xFirstColWidth - Columns[i].width;
  
    (* Minimale Breite der ersten Spalte einhalten.
       Wird diese Breite unterschritten, wird ein Scrollbalken erzeugt *)
    if xFirstColWidth > 100 then
      Columns[cSettingsNameCol].width := xFirstColWidth;
  end;// with mSettingsTree.Header do begin
end;// TfrmAlarmAssignFrame.mMachineListResize cat:No category

//:-------------------------------------------------------------------
(*: Member:           mStyleListFocusChanged
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Column)
 *
 *  Kurzbeschreibung: Setzt den neu selktierten Artikel und sortiert die Mashcinen liste entsprechend
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.mStyleListFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: 
  TColumnIndex);
begin
  // Assoziiertes Objekt holen
  if assigned(GetNodeObject(mStyleList, Node)) then
    mSelectedStyle := TQOStyle(GetNodeObject(mStyleList, Node));

//: ----------------------------------------------
  // Maschinenliste neu zeichnen damit die produzierenden Maschinen aktuell sind
  mMachineList.Invalidate;
  Application.ProcessMessages;
  
  with mMachineList do
    SortTree(Header.SortColumn, Header.SortDirection, true);
end;// TfrmAlarmAssignFrame.mStyleListFocusChanged cat:No category

//:-------------------------------------------------------------------
(*: Member:           mStyleListGetCursor
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Cursor)
 *
 *  Kurzbeschreibung: Fragt jedesmal wenn der Mauscursor bewegt wird, welcher Cursor denn dargestellt werden soll
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.mStyleListGetCursor(Sender: TBaseVirtualTree; var Cursor: TCursor);
var
  xHitInfo: THitInfo;
  xRelative: Boolean;
  xPoint: TPoint;
  xHitPositions: THitPositions;
  
  const
    cDragHitPositions = [hiOnStateIcon, hiOnNormalIcon, hiOnItemLabel];
  
begin
  Cursor := Cursor;
  (* Wird nicht realisiert, da es keinen Sinnvollen DEfaultCursor gibt
     um die M�glichkeit des Drag&Drop zu zeigen. *)
  {// Position des Mauscursors in Windows Koordinaten
  GetCursorPos(xPoint);
  // Windows Koordinaten in Koordinaten relativ zum Tree umwandeln
  xPoint := Sender.ScreenToClient(xPoint);
  
  // Abfragen �ber welchem Element der Cursor steht
  Sender.GetHitTestInfoAt(xPoint.x, xPoint.y, false, xHitInfo);
  
  xHitPositions := [];
  xHitPositions := xHitInfo.HitPositions;
  
  if (cDragHitPositions * xHitPositions <> []) then
    cursor := crHandpoint;
  //  CodeSite.SendNote('Hit');}
end;// TfrmAlarmAssignFrame.mStyleListGetCursor cat:No category

//:-------------------------------------------------------------------
(*: Member:           mStyleListGetImageIndex
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Kind, Column, Ghosted, ImageIndex)
 *
 *  Kurzbeschreibung: Gibt den ImageIndex des selektierten Objektes zur�ck
 *  Beschreibung:     
                      Die verwendete Imagelist wird von der Applikation �ber die Eigenschaft
                      "TreeImages" gesetzt.
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.mStyleListGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind;
  Column: TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
begin
  ImageIndex := GetAssignImageIndex(Sender, Node, Kind, Column);
end;// TfrmAlarmAssignFrame.mStyleListGetImageIndex cat:No category

//:-------------------------------------------------------------------
(*: Member:           mStyleListGetText
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Column, TextType, CellText)
 *
 *  Kurzbeschreibung: -
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.mStyleListGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; 
  TextType: TVSTTextType; var CellText: WideString);
begin
  if assigned(GetNodeObject(mMachineList, Node)) and (Column = 0) then
    CellText := TQOStyle(GetNodeObject(mStyleList, Node)).QOVisualize.Caption
  else
    CellText := '';
end;// TfrmAlarmAssignFrame.mStyleListGetText cat:No category

//:-------------------------------------------------------------------
(*: Member:           mStyleListResize
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Berechnet die Breite f�r die erste Spalte
 *  Beschreibung:     
                      Die erste Spalte f�llt immer die Breite komplet aus.
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.mStyleListResize(Sender: TObject);
var
  xFirstColWidth: Integer;
  i: Integer;
begin
  // Breite der ersten Spalte bestimmen
  with mStyleList.Header do begin
    (* Die Breite der Spalte berechnet sich aus der Breite des Trees
       minus der Breite aller anderen Spalten *)
    xFirstColWidth := mStyleList.ClientWidth;
    for i := 1 to Columns.count - 1 do
       xFirstColWidth := xFirstColWidth - Columns[i].width;
  
    (* Minimale Breite der ersten Spalte einhalten.
       Wird diese Breite unterschritten, wird ein Scrollbalken erzeugt *)
    if xFirstColWidth > 100 then
      Columns[cSettingsNameCol].width := xFirstColWidth;
  end;// with mSettingsTree.Header do begin
end;// TfrmAlarmAssignFrame.mStyleListResize cat:No category

//:-------------------------------------------------------------------
(*: Member:           mTranslatorAfterTranslate
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (translator)
 *
 *  Kurzbeschreibung: �bernimmt alle �bersetungen die nicht automatisch "passieren"
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.mTranslatorAfterTranslate(translator: TIvCustomTranslator);
begin
  mStyleList.Header.Columns[0].Text := rsStyle;
  mMachineList.Header.Columns[0].Text := rsMachines;
  SetIncSearchControlStateText(edIncrementalStyle);
end;// TfrmAlarmAssignFrame.mTranslatorAfterTranslate cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetExpandMachines
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r ExpandMachines
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.SetExpandMachines(Value: Boolean);
begin
  if Value then begin
    mBottomPanel.Height := TQOAppSettings.Instance.AlarmAssignMachinesPanelHeight;
    bShowMachines.Glyph := imMachCollapse.Picture.Bitmap;
    bShowMachines.NumGlyphs := 1;
    mBottomPanel.visible := true;
    mHorSplitter.Visible := true;
    TQOAppSettings.Instance.AlarmAssignMachinesAssignable := true;
  end else begin
    TQOAppSettings.Instance.AlarmAssignMachinesPanelHeight := mBottomPanel.Height;
    bShowMachines.Glyph := imMachExpand.Picture.Bitmap;
    bShowMachines.NumGlyphs := 1;
    mHorSplitter.Visible := false;
    mBottomPanel.visible := false;
    TQOAppSettings.Instance.AlarmAssignMachinesAssignable := false;
  end;// if xMachVisible then begin
end;// TfrmAlarmAssignFrame.SetExpandMachines cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetTreeImages
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r TreeImages
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.SetTreeImages(Value: TImageList);
begin
  FTreeImages := Value;

//: ----------------------------------------------
  // Trees
  mStyleList.Images := FTreeImages;
  mMachineList.Images := FTreeImages;
  
  // Header
  mStyleList.Header.Images := FTreeImages;
  mMachineList.Header.Images := FTreeImages;
end;// TfrmAlarmAssignFrame.SetTreeImages cat:No category

//:-------------------------------------------------------------------
(*: Member:           TreeCompareNodes
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node1, Node2, Column, Result)
 *
 *  Kurzbeschreibung: Sortierfunktion (Callback) f�r das TreeView
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.TreeCompareNodes(Sender: TBaseVirtualTree; Node1, Node2: PVirtualNode; Column: 
  TColumnIndex; var Result: Integer);
var
  xItem1: TQOAssign;
  xItem2: TQOAssign;
begin
  // Initialisieren
  xItem1 := nil;
  xItem2 := nil;
  
  // Die Zuordnungen Substituieren
  if assigned(GetNodeObject(Sender, Node1)) then
    xItem1 := TQOAssign(GetNodeObject(Sender, Node1));
  
  if assigned(GetNodeObject(Sender, Node2)) then
    xItem2 := TQOAssign(GetNodeObject(Sender, Node2));

//: ----------------------------------------------
  // Wenn eines der Elemente "Alle Artikel" ist, dann Sonderbehandlung
  if (xItem1 is TQOAllStyles) or (xItem2 is TQOAllStyles) then begin
    // "Alle Artiel ist immer an erster Stelle in der Liste
    if (xItem1 is TQOAllStyles) then
      result := 1;
  
    if (xItem2 is TQOAllStyles) then
      result := -1;
  
    // Je nach Sortierrichtung Resultat invertieren
    if TmmVirtualStringTree(Sender).Header.SortDirection = sdAscending then
      result := result * (-1);
  end else begin
    // Wenn beide Elemente "regul�re" Artikel / Maschinen sind
    if assigned(xItem1) and assigned(xItem2) then begin
  
      // Den �bergeordneten Artikel setzen (ohne Funktion, wenn die Items Artikel sind)
      if assigned(mSelectedStyle) then begin
        xItem1.ParentID := mSelectedStyle.LinkID;
        xItem2.ParentID := mSelectedStyle.LinkID;
      end;// if assigned(mSelectedStyle) then begin
  
      case Column of
        // Name
        0: begin
          result := ANSICompareText(xItem1.QOVisualize.Caption, xItem2.QOVisualize.Caption);
        end;// 0: begin
  
        // InProduktion
        1: begin
          if xItem1.InProduction and xItem2.InProduction then begin
            // Wenn beide in Produktion, dann nach dem Namen sortieren
            result := ANSICompareText(xItem1.QOVisualize.Caption, xItem2.QOVisualize.Caption);
          end else begin
            if xItem1.InProduction and not(xItem2.InProduction) then
              result := -1;
            if xItem2.InProduction and not(xItem1.InProduction) then
              result := 1;
          end;// if xItem1.InProduction and xItem2.InProduction then begin
        end;// 1: begin
  
      end;// case Column of
  
    end;// if assigned(xItem1) and assigned(xItem2) then begin
  end;// if (xItem1 is TQOAllStyles) or (xItem2 is TQOAllStyles) then begin
end;// TfrmAlarmAssignFrame.TreeCompareNodes cat:No category

//:-------------------------------------------------------------------
(*: Member:           TreeFreeNode
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node)
 *
 *  Kurzbeschreibung: Gibt das Eingebettete Objekt frei
 *  Beschreibung:     
                      Wenn der Knoten gel�scht wird, verschwindet auch das Objekt, da
                      die Objekte in der Selektionsphase im GUI verwaltet werden.
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.TreeFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  if assigned(GetNodeObject(Sender, Node)) then
    GetNodeObject(Sender, Node).Free;
end;// TfrmAlarmAssignFrame.TreeFreeNode cat:No category

//:-------------------------------------------------------------------
(*: Member:           TreeHeaderClick
 *  Klasse:           TfrmAlarmAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Column, Button, Shift, X, Y)
 *
 *  Kurzbeschreibung: Sortiert die Lsite nach dem entsprechenden Kriterium
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAlarmAssignFrame.TreeHeaderClick(Sender: TVTHeader; Column: TColumnIndex; Button: TMouseButton; Shift: 
  TShiftState; X, Y: Integer);
begin
  if Column = Sender.SortColumn then begin
    if sender.SortDirection = sdAscending then
      sender.SortDirection := sdDescending
    else
      Sender.SortDirection := sdAscending;
  end else begin
    sender.SortDirection := sdAscending
  end;// if Column = Sender.Header.SortColumn then begin
  
  Sender.SortColumn := Column;
  
  Sender.TreeView.SortTree(Column, Sender.SortDirection, true);

//: ----------------------------------------------
  // Sortierung sichern (Immer beide Trees sichern damit nicht unterschieden werden muss)
  TQOAppSettings.Instance.AssignStyleSortCol := mStyleList.Header.SortColumn;
  TQOAppSettings.Instance.AssignStyleSortDir := mStyleList.Header.SortDirection;
  
  TQOAppSettings.Instance.AssignMachSortCol := mMachineList.Header.SortColumn;
  TQOAppSettings.Instance.AssignMachSortDir := mMachineList.Header.SortDirection;
end;// TfrmAlarmAssignFrame.TreeHeaderClick cat:No category

end.































