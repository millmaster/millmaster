(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MainWindow.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Hauptformular f�r eine Instanz des Q-Offlimits
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.04.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit MainWindow;

interface

uses
  SettingsAlarmFrame, QOKernel, QOGroup, SettingsItemFrame, NCButton,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, VirtualTrees, mmVirtualStringTree, ToolWin, ComCtrls,
  mmToolBar, mmPageControl, ExtCtrls, mmSplitter, mmPanel, ActnList,
  mmActionList, ImgList, mmImageList, menus, mmDialogs, mmPopupMenu, AssignAlarmFrame,
  IvDictio, IvMulti, IvEMulti, mmTranslator, IvAMulti, IvBinDic,
  mmDictionary, Buttons, mmSpeedButton, StdCtrls, mmBitBtn, ActiveX,
  MMSecurity, AssignStyleFrame, QOGUIClasses, AnalyzeOverviewFrame, AnalyzeAlarmFrame, AnalyzeChartFrame,
  ComObj, mmButton;

type
  // Ansicht im Zuordnungs Tree (Alarm oder Artikel als Root)
  TAssignRootStyle = (
    rsStyle,
    rsAlarm
  );// TAssignRootStyle

  // Type der Objekte die per Drag&Drop hinzugef�gt werden
  TDDSourceType = (
    stNone,
    stAlarm,
    stStyle,
    stMachine
  );// TDDSourceType

  TQOUseCase = (ucSettings, ucAssign, ucAnalyze);

const
  cNoSource      = 0;
  cStyleSource   = 1;
  cMachineSource = 2;

  // H�he der Toolbars im MainWindow
  cToolbarHeight = 24;

const
  cOLELabReport = 'LOEPFE.LabReportServer';

type
  TButtonEditLink = class;
  //1 Wird aufgerufen, wenn EventInfos angezeigt werden sollen (Analyze) 
  TShowUserInfoEvent = procedure (Sender: TButtonEditLink) of object;
  (*: Klasse:        TTreeBitBtn
      Vorg�nger:     TmmBitBtn
      Kategorie:     No category
      Kurzbeschrieb: Button der im AnalyzeTree als Editor angezeigt wird 
      Beschreibung:  
                     Dieser Buttuon erweitert den Standard BitBtn um die M�glichkeit das Fokusrechteck
                     zu entfernen. Dies ist notwendig, da der Button nicht wesentlich gr�sser ist als
                     das Bitmap da angezeigt wird und deshalb das Recheck nur st�ren w�rde. *)
  TTreeBitBtn = class(TmmBitBtn)
  public
    //1 Entfernt das Focus Rechteck 
    procedure RemoveFocusRect;
  end;
  
  (*: Klasse:        TButtonEditLink
      Vorg�nger:     TInterfacedObject
      Kategorie:     No category
      Kurzbeschrieb: Editor f�r das TreeView (Button) 
      Beschreibung:  
                     Diese Klasse wird gebraucht um die Buttons anzuzeigen mithilfe derer
                     zu einer Schicht oder einem Alarm Zusatzinfos eingegeben oder gelesen werden
                     k�nnen. *)
  TButtonEditLink = class(TInterfacedObject, IVTEditLink)
  private
    FButton: TTreeBitBtn;
    FColumn: TColumnIndex;
    FGlyphSet: Boolean;
    FNode: PVirtualNode;
    FOnShowUserInfo: TShowUserInfoEvent;
    FStopping: Boolean;
    FTree: TCustomVirtualStringTree;
    //1 Setzt das Bitmap f�r den Button 
    procedure SetGlyph(Value: TBitmap);
  protected
    //1 Ein gew�nlicher Button der als Editor angezeigt wird 
    property Button: TTreeBitBtn read FButton;
  public
    //1 Konstruktor 
    constructor Create; overload;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Wird aufgerufen, wenn das Tree das Control zum Editieren anzeigen will 
    function BeginEdit: Boolean; stdcall;
    //1 Wird aufgerufen, wenn auf den Button geklickt wird. 
    procedure ButtonClick(Sender: TObject);
    //1 Wird aufgerufen, wenn das Tree das Editieren verwirft 
    function CancelEdit: Boolean; stdcall;
    //1 Wird aufgerufen, wenn das Tree das Editieren beendet 
    function EndEdit: Boolean; stdcall;
    //1 Wird aufgerufen, wenn das Control Resized wird 
    function GetBounds: TRect; stdcall;
    //1 Wird aufgerufen, nachdem der Editor erzeugt ist. 
    function PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean; stdcall;
    //1 Leitet die Messages an den Button weiter 
    procedure ProcessMessage(var Message: TMessage); stdcall;
    //1 Hier kann die Gr�sse des Buttons angepasst werden 
    procedure SetBounds(R: TRect); stdcall;
    //1 Spalte in der der Editor (Button) angezeigt wird 
    property Column: TColumnIndex read FColumn;
    //1 Bitmap das auf dem Button angezeigt wird 
    property Glyph: TBitmap write SetGlyph;
    //1 Knoten des Trees das den Editor anzeigt 
    property Node: PVirtualNode read FNode;
    //1 Backpointer auf das aufrufende Tree 
    property Tree: TCustomVirtualStringTree read FTree;
  published
    //1 Wird gefeuert, wenn auf den Button gecklickt wurde 
    property OnShowUserInfo: TShowUserInfoEvent read FOnShowUserInfo write FOnShowUserInfo;
  end;
  
  (*: Klasse:        TfrmMainWindow
      Vorg�nger:     TmmForm
      Kategorie:     View
      Kurzbeschrieb: Hauptfenster 
      Beschreibung:  
                     - *)
  TfrmMainWindow = class(TmmForm)
    acAssignAlarm: TAction;
    acAssignStyle: TAction;
    acCallLabReport: TAction;
    acCollapse: TAction;
    acConfigSecurity: TAction;
    acDeleteAssign: TAction;
    acDeleteFromArchive: TAction;
    acDeleteSettings: TAction;
    acEditAlarm: TAction;
    acEditDataItem: TAction;
    acEvaluate: TAction;
    acExit: TAction;
    acExpand: TAction;
    acHelpSettings: TAction;
    acNew: TAction;
    acPrintAnalyze: TAction;
    acPrintSettings: TAction;
    acRefreshData: TAction;
    acSaveData: TAction;
    Aktualisieren1: TMenuItem;
    Ausblenden1: TMenuItem;
    Ausblenden2: TMenuItem;
    Ausblenden3: TMenuItem;
    AusdemArchivlschen1: TMenuItem;
    bSecurity: TToolButton;
    Drucken1: TMenuItem;
    Drucken2: TMenuItem;
    Drucken3: TMenuItem;
    Einblenden1: TMenuItem;
    Einblenden2: TMenuItem;
    Einblenden3: TMenuItem;
    LaborBericht1: TMenuItem;
    Loeschen1: TMenuItem;
    lschen1: TMenuItem;
    lschen2: TMenuItem;
    mAnalyzeToolBar: TmmToolBar;
    mAnalyzeTree: TmmVirtualStringTree;
    mAppActions: TmmActionList;
    mAssignToolBar: TmmToolBar;
    mAssignTree: TmmVirtualStringTree;
    mButtonImages: TmmImageList;
    mLeftPanel: TmmPanel;
    mMethodImages: TmmImageList;
    mmPanel1: TmmPanel;
    mmPanel10: TmmPanel;
    mmPanel2: TmmPanel;
    mmPanel3: TmmPanel;
    mmPanel4: TmmPanel;
    mmPanel5: TmmPanel;
    mmPanel6: TmmPanel;
    mmPanel7: TmmPanel;
    mmPanel8: TmmPanel;
    mmPanel9: TmmPanel;
    mmToolBar1: TmmToolBar;
    mmToolBar2: TmmToolBar;
    mmToolBar3: TmmToolBar;
    mNCImages: TmmImageList;
    mPanel1: TmmPanel;
    mRightPanel: TmmPanel;
    mSecurityControl: TMMSecurityControl;
    mSettingsToolBar: TmmToolBar;
    mSettingsTree: TmmVirtualStringTree;
    mTranslator: TmmTranslator;
    mTreeImages: TmmImageList;
    mTreePageControl: TmmPageControl;
    mVertSplitter: TmmSplitter;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    pmAnalyzeTree: TmmPopupMenu;
    pmAssignTreeAlarm: TmmPopupMenu;
    pmSettingsTreeAlarm: TmmPopupMenu;
    QOfflimitsgenerieren1: TMenuItem;
    Speichern1: TMenuItem;
    tabAnalyze: TTabSheet;
    tabAssign: TTabSheet;
    tabSettings: TTabSheet;
    tbAnalyzeDeleteFromArchive: TToolButton;
    tbAnalyzePrint: TToolButton;
    tbSaveData: TToolButton;
    ToolButton1: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    ToolButton18: TToolButton;
    ToolButton19: TToolButton;
    ToolButton2: TToolButton;
    ToolButton20: TToolButton;
    ToolButton21: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    //1 Holt das Interface zum Labreport und ruft ihn mit den aktuellen Parametern auf 
    procedure acCallLabReportExecute(Sender: TObject);
    //1 Klappt das TreeView zusammen 
    procedure acCollapseExecute(Sender: TObject);
    procedure acConfigSecurityExecute(Sender: TObject);
    //1 l�scht eine Zuordnung (Artikel oder Maschine) 
    procedure acDeleteAssignExecute(Sender: TObject);
    //1 Entfernt den Datensatz aus dem Archiv 
    procedure acDeleteFromArchiveExecute(Sender: TObject);
    //1 L�scht die selektierten Data Items aus dem Tree und dem Modell 
    procedure acDeleteSettingsExecute(Sender: TObject);
    procedure acEditAlarmExecute(Sender: TObject);
    procedure acEditDataItemExecute(Sender: TObject);
    //1 Generiert tempor�r einen QOfflimit 
    procedure acEvaluateExecute(Sender: TObject);
    //1 Programmende 
    procedure acExitExecute(Sender: TObject);
    //1 Blendet alle Zweige des Trees ein 
    procedure acExpandExecute(Sender: TObject);
    //1 Zeigt die Hilfe an 
    procedure acHelpExecute(Sender: TObject);
    //1 Erzeugt einen neuen Alarm 
    procedure acNewExecute(Sender: TObject);
    //1 Druckt den Schichtbericht aus 
    procedure acPrintAnalyzeExecute(Sender: TObject);
    //1 - 
    procedure acPrintSettingsExecute(Sender: TObject);
    //1 L�dt die Alarmdaten neu von der Datenbank 
    procedure acRefreshDataExecute(Sender: TObject);
    //1 Markiert einen Datensatz auf der Datenbank als persistent 
    procedure acSaveDataExecute(Sender: TObject);
    //1 Programmende 
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    //1 Programmende 
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    //1 Initialieren 
    procedure FormCreate(Sender: TObject);
    //1 Malt die Fixe Spalte mit dem Datenzeiger 
    procedure mAnalyzeTreeAfterCellPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; Column:
      TColumnIndex; CellRect: TRect);
    //1 - 
    procedure mAnalyzeTreeChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
    //1 Erzeugt den Button f�r die Infos 
    procedure mAnalyzeTreeCreateEditor(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; out
      EditLink: IVTEditLink);
    //1 Bestimmt ob der Button angezeigt werden soll 
    procedure mAnalyzeTreeEditing(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; var Allowed:
      Boolean);
    //1 Setzt den Mauszeiger zur�ck 
    procedure mAnalyzeTreeExpanded(Sender: TBaseVirtualTree; Node: PVirtualNode);
    //1 Setzt den Mauszeiger auf "Besch�ftigt"  
    procedure mAnalyzeTreeExpanding(Sender: TBaseVirtualTree; Node: PVirtualNode; var Allowed: Boolean);
    //1 Wird aufgerufen, wenn ein neues Element ausgew�hlt wird 
    procedure mAnalyzeTreeFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);
    //1 Freigeben der Objekte die nicht in einer Liste verwaltet werden 
    procedure mAnalyzeTreeFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    //1 Wird aufgerufen, wenn das darzustellende Icon f�r den Node abgefragt wird 
    procedure mAnalyzeTreeGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column:
      TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
    //1 Wird aufgerufen um den Text des Nodes zu zeichnen 
    procedure mAnalyzeTreeGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType:
      TVSTTextType; var CellText: WideString);
    //1 Wird aufgerufen um die Childs des Knoten zu initialisieren 
    procedure mAnalyzeTreeInitChildren(Sender: TBaseVirtualTree; Node: PVirtualNode; var ChildCount: Cardinal);
    //1 Wird aufgerufen um zu erfahren ob der entsprechende Knoten Kinder besitzt 
    procedure mAnalyzeTreeInitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode; var InitialStates:
      TVirtualNodeInitStates);
    //1 Berechnet die Splatenbreite f�r die erste Spalte (Zuordnung) 
    procedure mAnalyzeTreeResize(Sender: TObject);
    procedure mAppActionsUpdate(Action: TBasicAction; var Handled: Boolean);
    //1 F�gt dem Alarm die selektierten Zuordnungen(Artikel, Maschinen) hinzu 
    procedure mAssignTreeDragDrop(Sender: TBaseVirtualTree; Source: TObject; DataObject: IDataObject; Formats:
      TFormatArray; Shift: TShiftState; Pt: TPoint; var Effect: Integer; Mode: TDropMode);
    //1 Pr�ft ob die Selektion auf dem entsprechenden Eintrag abgelegt werden darf. 
    procedure mAssignTreeDragOver(Sender: TBaseVirtualTree; Source: TObject; Shift: TShiftState; State: TDragState; Pt:
      TPoint; Mode: TDropMode; var Effect: Integer; var Accept: Boolean);
    //1 Je nach aktueller Selektion muss die Umgebung angepasst werden 
    procedure mAssignTreeFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);
    //1 Gibt das aktuelle Icon f�r den Knoten und die Spalte zur�ck (Zuordnung) 
    procedure mAssignTreeGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column:
      TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
    //1 Gibt an welches Popupmen� angezeigt werden soll (Zuordnung) 
    procedure mAssignTreeGetPopupMenu(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; const P:
      TPoint; var AskParent: Boolean; var PopupMenu: TPopupMenu);
    //1 Setzt den Text der angezeigt werden soll (Zuordnung) 
    procedure mAssignTreeGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType:
      TVSTTextType; var CellText: WideString);
    //1 Berechnet die Splatenbreite f�r die erste Spalte (Zuordnung) 
    procedure mAssignTreeResize(Sender: TObject);
    //1 sichert die aktuelle Position des vertikalen Splitters 
    procedure mLeftPanelResize(Sender: TObject);
    //1 Malt die Fixe Spalte mit dem Datenzeiger 
    procedure mSettingsTreeAfterCellPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; Column:
      TColumnIndex; CellRect: TRect);
    //1 f�gt dem Ziel(Alarm) die selektierten DataItems hinzu 
    procedure mSettingsTreeDragDrop(Sender: TBaseVirtualTree; Source: TObject; DataObject: IDataObject; Formats:
      TFormatArray; Shift: TShiftState; Pt: TPoint; var Effect: Integer; Mode: TDropMode);
    //1 Pr�ft ob die Selektion auf dem entsprechenden Eintrag abgelegt werden darf. 
    procedure mSettingsTreeDragOver(Sender: TBaseVirtualTree; Source: TObject; Shift: TShiftState; State: TDragState;
      Pt: TPoint; Mode: TDropMode; var Effect: Integer; var Accept: Boolean);
    //1 Wird aufgerufen, nachdem der aktive Knoten gewechselt hat (Settings) 
    procedure mSettingsTreeFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);
    //1 Wird aufgerufen, bevor der Knoten gewechselt wird (Settings) 
    procedure mSettingsTreeFocusChanging(Sender: TBaseVirtualTree; OldNode, NewNode: PVirtualNode; OldColumn,
      NewColumn: TColumnIndex; var Allowed: Boolean);
    //1 Gibt das aktuelle Icon f�r den Knoten und die Spalte zur�ck (Settings) 
    procedure mSettingsTreeGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column:
      TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
    //1 Gibt an welches Popupmen� angezeigt werden soll (Settings) 
    procedure mSettingsTreeGetPopupMenu(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; const P:
      TPoint; var AskParent: Boolean; var PopupMenu: TPopupMenu);
    //1 Setzt den Text der angezeigt werden soll (Settings) 
    procedure mSettingsTreeGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType:
      TVSTTextType; var CellText: WideString);
    //1 Tempor�r um eine Berechnung auszul�sen 
    procedure mSettingsTreeMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    //1 Berechnet die Splatenbreite f�r die erste Spalte (Settings) 
    procedure mSettingsTreeResize(Sender: TObject);
    //1 Alle Texte die nicht automatisch �bersetzt werden 
    procedure mTranslatorAfterTranslate(translator: TIvCustomTranslator);
    //1 Ereignis Handler f�r die Buttons in der rechten oberen Ecke des AssignTree 
    procedure RebuildAssignTreeClick(Sender: TObject);
    //1 Blendet die Analyze Frames aus 
    procedure tabAnalyzeHide(Sender: TObject);
    //1 Erzeugt, wenn notwendig den Inhalt des Trees 
    procedure tabAnalyzeShow(Sender: TObject);
    //1 Speichert die h�ngigen Zuordnungen und blendet die entsprechenden Frames aus 
    procedure tabAssignHide(Sender: TObject);
    //1 Zeigt die Seite mit den Alarm Zuordnungen an 
    procedure tabAssignShow(Sender: TObject);
    //1 Speichert die h�ngigen Settings und blendet die entsprechenden Frames aus 
    procedure tabSettingsHide(Sender: TObject);
    //1 Zeigt die Seite mit den Alarm Settings an 
    procedure tabSettingsShow(Sender: TObject);
    //1 Wird aufgerufen, wenn in einem TreeView eine Taste gedr�ckt wird (WM_KEYDOWN) 
    procedure TreeKeyAction(Sender: TBaseVirtualTree; var CharCode: Word; var Shift: TShiftState; var DoDefault:
      Boolean);
    //1 Sonderbehandlung einiger Tasten f�r das AnalyzeTree 
    procedure TreeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FUpdateCnt: Integer;
    mAlarmAssignFrame: TfrmAssignAlarmFrame;
    mAnalyzeAlarmFrame: TfrmAnalyzeAlarmFrame;
    mAnalyzeArchivNode: PVirtualNode;
    mAnalyzeAutoAlarmNode: PVirtualNode;
    mAnalyzeChartFrame: TfrmAnalyzeChartFrame;
    mAnalyzeManualAlarmNode: PVirtualNode;
    mAnalyzeOverviewFrame: TfrmAnalyzeOverviewFrame;
    mAssignStyle: TAssignRootStyle;
    mCreated: Boolean;
    mExpandByAction: Boolean;
    mFirstMethodCol: Integer;
    mLabReport: OLEVariant;
    mObserver: TQOShiftSetObserver;
    mSettingsAlarmFrame: TfrmSettingsAlarmFrame;
    mSettingsItemFrame: TfrmSettingsItemFrame;
    mStyleAssignFrame: TfrmAssignStyleFrame;
    mStyles: TQOAssigns;
    //1 F�gt die Atrikel den Alarmen hinzu 
    procedure AddAlarms(aSourceTree:TBaseVirtualTree; aSourceAlarms, aDestStyles: TNodeArray);
    //1 F�gt den Alarmen die Data Items hinzu 
    procedure AddItems(aSourceTree: TBaseVirtualTree; aSourceItems, aDestAlarms: TNodeArray);
    //1 F�gt den Artikeln die Maschinen hinzu 
    procedure AddMachines(aSourceTree:TBaseVirtualTree; aSourceMachines, aDestStyles: TNodeArray);
    //1 F�gt den Alarmen die Artikel hinzu 
    procedure AddStyles(aSourceTree:TBaseVirtualTree; aSourceStyles, aDestAlarms: TNodeArray);
    //1 Baut den Node mit den automatisch generierten Alarmen neu auf 
    procedure BuildAnalyzeAutoGenerated;
    //1 Baut den Tree mit den Schichten auf 
    procedure BuildAnalyzeTreeFromShiftSet;
    //1 Erzeugt den SettingsAlarmFrame 
    procedure CreateSettingsAlarmFrame;
    //1 Ereignis Handler der aufgerufen wird, wenn ein Alarm aus dem Tree entfernt wird 
    procedure DeleteAlarm(Sender: TObject);
    //1 Gibt eine Kommaseparierte Liste mit den selektierten Schichten zur�ck 
    function GetAnalyzeSelectedShifts: String;
    //1 Erneuert die Anzeige der Analyse Daten 
    procedure RefreshAnalyzeData;
    //1 Speichert das aktuelle Objekt 
    function SaveCurrentSettings: Word;
    //1 Wird aufgerufen wenn die Schichten neu geladen wurden 
    procedure ShiftsLoaded(Sender: TObject);
  protected
    //1 Leitet das Neuerfassen des Alarms im Tree ein 
    procedure AlarmOnAlarmSaved(Sender: TfrmSettingsAlarmFrame; aAlarm: TQOAlarm);
    //1 Wird aufgerufen, wenn Zusatzinfos zur Schicht oder zum Alarm angezeigt werden sollen (Analyze) 
    procedure AnalyzeShowEventInfo(Sender: TButtonEditLink);
    //1 Baut den Tree mit den Archivierten Alarmen auf 
    procedure BuildAnalyzeArchiv;
    //1 Baut den Tree mit dem manuell generierten Alarm auf 
    procedure BuildAnalyzeManualGenerated;
    //1 Update Patern 
    function IsUpdating: Boolean;
    //1 Wird aufgerufen, wenn im AlarmView auf eine Serie des Histogramms gecklickt wird 
    procedure OnAlarmviewChartClick(aDate: TDateTime; aAlarmName: string; aButton: TMouseButton; aShiftKey:
      TShiftState);
    //1 wird aufgerufen, wenn mit dem Button die selektierten Alarme hinzugef�gt werden 
    procedure OnAssignAddAlarms(Sender: TBaseVirtualTree);
    //1 wird aufgerufen, wenn mit dem Button die selektierten Artikel / Maschinen hinzugef�gt werden 
    procedure OnAssignAddItems(Sender: TBaseVirtualTree);
    //1 wird aufgerufen, wenn im Histogramm auf einen Alarm geklickt wurde 
    procedure OnOverviewChartClick(aDate: TDateTime; aButton: TMouseButton; aShiftKey: TShiftState);
    //1 wird aufgerufen, wenn mit dem Button die selektierten Data Items hinzugef�gt werden 
    procedure OnSettingsAddItems(Sender: TBaseVirtualTree);
    //1 Wird aufgerufen, wenn ein Data Item gespeichert wurde 
    procedure SettingsItemSaved(Sender: TfrmSettingsItemFrame; aItem: TQOItem);
    //1 Update Patern 
    procedure SetUpdating(Updating: Boolean);
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Update Patern 
    procedure BeginUpdate;
    //1 Erzeugt die DataItems eines Alarms im Assign Tree 
    procedure BuildAssignTreeAlarm(aAlarmNode: PVirtualNode; aAlarm: TQOAlarm);
    //1 Erzeugt die DataItems eines Artikels im Assign Tree 
    procedure BuildAssignTreeAssign(aAssignNode: PVirtualNode; aAssign: TQOAssign);
    //1 Zeigt die Liste mit den Alarmen oder Artikel an 
    procedure BuildAssignTreeFromAlarms(aAssignRootStyle: TAssignRootStyle = rsAlarm);
    //1 Erzeugt die DataItems eines Alarms im Assign Tree 
    procedure BuildAssignTreeStyle(aStyleNode: PVirtualNode; aStyle: TQOAssign);
    //1 Erzeugt die DataItems eines Alarms im Settings Tree 
    procedure BuildSettingsTreeAlarm(aAlarmNode: PVirtualNode; aAlarm: TQOAlarm);
    //1 Zeigt die Liste mit den Alarmen an 
    procedure BuildSettingsTreeFromAlarms;
    //1 Setzt eine Action unter Ber�cksichtigung der Security Settings 
    procedure EnableDisableAction(aAction: TAction; aEnabled: boolean; aVisible: boolean = true);
    //1 Update Patern 
    procedure EndUpdate;
    //1 COM Implementierung 
    procedure Execute(const aParameter: WideString);
    //1 Pr�ft ob der gegebene Mausbutton gedr�ckt ist 
    function GetMouseButtonPressed(aButton: TMouseButton): Boolean;
    //1 Initialisiert die Anwendung 
    procedure Init;
    //1 Setzt die �bergebene Funktion als aktiv 
    function SetActiveUseCase(aUseCase: TQOUseCase): Boolean;
  end;
  

resourcestring
  cAskAddAllStyle        = '(*)Es ist bereits mindestens ein Artikel definiert.'#13#10'Sollen trotzdem alle Artikel hinzugefuegt werden?'; //ivlm
  rsNotAllStylesAssigned = '(*)Ein- oder mehrere Artikel sind bereits zugeordnet';//ivlm
  rsNotAllMachsAssigned  = '(*)Eine- oder mehrere Maschinen sind bereits zugeordnet';//ivlm
  rsNotAllAlarmsAssigned = '(*)Ein- oder mehrere Alarme sind bereits zugeordnet';//ivlm
  rsNotAllItemsAssigned  = '(*)Ein- oder mehrere Datenelemente sind bereits zugewiesen';//ivlm

// START resource string wizard section
resourcestring
  rsErmittelteOfflimitVerletzungen = '(*)Anzahl Q-Offlimit Verletzungen:';//ivlm
  rsSpeichern = '(9)Speichern';//ivlm

// END resource string wizard section


implementation
uses
  DataItemTree, QOGUIShared, mmcs, MethodBaseFrame, AdoDBAccess, ClassDef, QODef, SelectDefectDialog, Commctrl,
  QOReportSettings, QOReportShiftList, QOReportAnalyzeChart, EventInfo, CalcMethods, LoepfeGlobal, MMHtmlHelp;

{$R *.DFM}
const
  // ImageIndex f�r die darstellung der Icons im Tree
  cAlarmImage     = 0;
  cItemImage      = 1;
  cPointImage     = 6;
  cPlusGreen      = 8;
  cPlus           = 9;
  cActiveImage    = 12;

  // ImageIndex f�r die Instanz der Objekte;
  cAlarmAssignImageIndex   = 0;
  cAlarmInactivemageIndex  = 15;
  cAlarmSettingsImageIndex = 0;
  cDataItemImageIndex      = 1;
  cMachineImageIndex       = 10;
  cMachineSWImageIndex     = 24;
  cStyleImageIndex         = 3;
  cStyleSWImageIndex       = 23;
  cStyleHalfImageIndex     = 29;
  cActiveOverlayImageIndex = 2;
  cAlarmAnalyzeImageIndex  = 0;
  cAlarmSetImageIndex      = 20;
  cNodeObjectImageIndex    = 21;
  cAlarmSetArchiveIndex    = 26;

  // Definition der Overlay  Indexe
  cPlusOverlay    = 1;
  cActiveOverlay  = cActiveOverlayImageIndex;
//:-------------------------------------------------------------------
(*: Member:           RemoveFocusRect
 *  Klasse:           TTreeBitBtn
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Entfernt das Focus Rechteck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TTreeBitBtn.RemoveFocusRect;
begin
  (* In dieser Prozedur wird das private Symbol 'IsFocused' von TBitBtn
     gesetzt. Diese Variable steuert im Paint Event daf�r ob das Fokus Rechteck
     gezeichnet wird, oder nicht *)
  SetButtonStyle(false);
end;// TTreeBitBtn.RemoveFocusRect cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TButtonEditLink
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      Hier wird der Button erstmal erzeugt um dann sp�ter vom
                      Tree angezeigt zu werden.
 --------------------------------------------------------------------*)
constructor TButtonEditLink.Create;
begin
  inherited;

//: ----------------------------------------------
  FGlyphSet := false;

//: ----------------------------------------------
  FButton := TTreeBitBtn.Create(nil);
  with FButton do begin
    Visible := False;
    OnClick := ButtonClick;
  end;// with FButton do begin
end;// TButtonEditLink.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TButtonEditLink
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TButtonEditLink.Destroy;
begin
  inherited;
  FreeAndNil(FButton);
end;// TButtonEditLink.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           BeginEdit
 *  Klasse:           TButtonEditLink
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn das Tree das Control zum Editieren anzeigen will
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TButtonEditLink.BeginEdit: Boolean;
begin
  Result := not FStopping;
  if Result then
    FButton.Show;
end;// TButtonEditLink.BeginEdit cat:No category

//:-------------------------------------------------------------------
(*: Member:           ButtonClick
 *  Klasse:           TButtonEditLink
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn auf den Button geklickt wird.
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TButtonEditLink.ButtonClick(Sender: TObject);
begin
  if assigned(FOnShowUserInfo) then
    FOnShowUserInfo(self);
end;// TButtonEditLink.ButtonClick cat:No category

//:-------------------------------------------------------------------
(*: Member:           CancelEdit
 *  Klasse:           TButtonEditLink
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn das Tree das Editieren verwirft
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TButtonEditLink.CancelEdit: Boolean;
begin
  Result := not FStopping;
  if Result then begin
    FStopping := True;
    FButton.Hide;
    FTree.CancelEditNode;
  end;
end;// TButtonEditLink.CancelEdit cat:No category

//:-------------------------------------------------------------------
(*: Member:           EndEdit
 *  Klasse:           TButtonEditLink
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn das Tree das Editieren beendet
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TButtonEditLink.EndEdit: Boolean;
begin
  Result := not FStopping;
  if Result then try
    FStopping := True;
    FButton.Hide;
  except
    FStopping := False;
    raise;
  end;
end;// TButtonEditLink.EndEdit cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetBounds
 *  Klasse:           TButtonEditLink
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn das Control Resized wird
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TButtonEditLink.GetBounds: TRect;
begin
  Result := FButton.BoundsRect;
end;// TButtonEditLink.GetBounds cat:No category

//:-------------------------------------------------------------------
(*: Member:           PrepareEdit
 *  Klasse:           TButtonEditLink
 *  Kategorie:        No category 
 *  Argumente:        (Tree, Node, Column)
 *
 *  Kurzbeschreibung: Wird aufgerufen, nachdem der Editor erzeugt ist.
 *  Beschreibung:     
                      Diese Funktion kann verwendet werden um den Styl des Buttons
                      zu definieren.
 --------------------------------------------------------------------*)
function TButtonEditLink.PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean;
begin
  Result := Tree is TCustomVirtualStringTree;
  if Result then
  begin
    FTree := Tree as TCustomVirtualStringTree;
    FNode := Node;
    FColumn := Column;
    FButton.Font.Color := clBlack;
    FButton.Font.Style := [fsBold];
    FButton.Parent := Tree;
    if not(FGlyphSet) then
      FButton.Caption := '...';
  end;
end;// TButtonEditLink.PrepareEdit cat:No category

//:-------------------------------------------------------------------
(*: Member:           ProcessMessage
 *  Klasse:           TButtonEditLink
 *  Kategorie:        No category 
 *  Argumente:        (Message)
 *
 *  Kurzbeschreibung: Leitet die Messages an den Button weiter
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TButtonEditLink.ProcessMessage(var Message: TMessage);
begin
  FButton.WindowProc(Message);
end;// TButtonEditLink.ProcessMessage cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetBounds
 *  Klasse:           TButtonEditLink
 *  Kategorie:        No category 
 *  Argumente:        (R)
 *
 *  Kurzbeschreibung: Hier kann die Gr�sse des Buttons angepasst werden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TButtonEditLink.SetBounds(R: TRect);
begin
  if not FStopping then begin
    with R do begin
      // Set the edit's bounds but make sure there's a minimum width and the right border does not
      // extend beyond the parent's left/right border.
      if Left < 0 then
        Left := 0;
      if Right - Left < 20 then
          Right := Left + 20;
      if Right > FTree.ClientWidth then
        Right := FTree.ClientWidth;
      FButton.BoundsRect := R;
    end;
  end;
end;// TButtonEditLink.SetBounds cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetGlyph
 *  Klasse:           TButtonEditLink
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt das Bitmap f�r den Button
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TButtonEditLink.SetGlyph(Value: TBitmap);
begin
  FButton.Glyph := Value;
  FGlyphSet := true;
end;// TButtonEditLink.SetGlyph cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmMainWindow.Create(aOwner: TComponent);
var
  xTreeCol: TVirtualTreeColumn;
  i: TCalculationMethod;
  xQOStyle: TQOStyle;
  xImage: TBitmap;
  xMask: TBitmap;
  xRect: TRect;
  il: TImageList;
  
  const
  
    // NCButtons (/Artikel oder Alarm Sicht)
    cButtonWidthHeight = 14;
    cButtonFrame       = 3;
    cButtonTop         = 2;
    cCollapseImageIndex = 3;
    cExpandImageIndex   = 2;
  
  function AppendImageToTreeImages(aIndex: integer): Integer;
  begin
    xImage.Canvas.FillRect(xRect);
    ImageList_Draw(il.Handle, aIndex, xImage.Canvas.Handle, 0, 0, ILD_NORMAL);
    xMask.Canvas.FillRect(xRect);
    ImageList_Draw(il.Handle, aIndex, xMask.Canvas.Handle, 0, 0, ILD_MASK);
    result := mTreeImages.Add(xImage, xMask);
  end;// procedure AppendImageToTreeImages;
  
begin
  inherited Create(aOwner);

//: ----------------------------------------------
  // ImageIndex zuweisen
  with TImageIndexHandler.Instance do begin
    ActiveOverlay   := cActiveOverlayImageIndex;
    AlarmAnalyze    := cAlarmAnalyzeImageIndex;
    AlarmAssign     := cAlarmAssignImageIndex;
    AlarmInactive   := cAlarmInactivemageIndex;
    AlarmSet        := cAlarmSetImageIndex;
    AlarmSettings   := cAlarmSettingsImageIndex;
    DataItem        := cDataItemImageIndex;
    Machine         := cMachineImageIndex;
    MachineSW       := cMachineSWImageIndex;
    NodeObject      := cNodeObjectImageIndex;
    Style           := cStyleImageIndex;
    StyleSW         := cStyleSWImageIndex;
    StyleHalf       := cStyleHalfImageIndex;
    AlarmSetArchive := cAlarmSetArchiveIndex;
  end;// with TImageIndexHandler.Instance do begin

//: ----------------------------------------------
  TQOAlarms.Instance.LoadDefFromDB;

//: ----------------------------------------------
  mObserver := TQOShiftSetObserver.Create(self);
  mObserver.OnDeleteAlarm := DeleteAlarm;
  mObserver.OnShiftsLoaded := ShiftsLoaded;
  mObserver.Enabled := true;
  
  TQOShiftSet.Instance.RegisterObserver(mObserver);
  
  mSettingsTree.NodeDataSize := SizeOf(TTreeData);
  mAssignTree.NodeDataSize := SizeOf(TTreeData);

//: ----------------------------------------------
  mAnalyzeTree.NodeDataSize := SizeOf(TTreeData);
  
  // Erste Spalte mit den Methoden merken (f�r GetImageIndex)
  mFirstMethodCol := mSettingsTree.Header.Columns.Count;
  
  // Spalten f�r das Tree erzeugen (eine Spalte pro Methode)
  for i := Low(TCalculationMethod) to High(TCalculationMethod) do begin
    xTreeCol := mSettingsTree.Header.Columns.Add;
    xTreeCol.Width := cMethodColWith;
    xTreeCol.ImageIndex := gMethodVisualStyles[i].ImageIndex;
  end;// for i := Low(TCalculationMethod) to High(TCalculationMethod) do begin
  
  // Overlay Images definieren
  mTreeImages.Overlay(cPlus, cPlusOverlay);
  mTreeImages.Overlay(cActiveImage, cActiveOverlay);

//: ----------------------------------------------
  // Im Normalfall wird die Zuodnung der Alarme zu den Styles angezeigt
  mAssignStyle := rsAlarm;

//: ----------------------------------------------
  // Diese Toolbuttons ohne Text
  tbAnalyzePrint.Caption := '';
  tbAnalyzeDeleteFromArchive.Caption := '';
  
  tbSaveData.Caption := '';
  // Settings
  mLeftPanel.Width := TQOAppSettings.Instance.MainWindowLeftPanelWidth;
  
  // Liest alle Artikel die auf der DB definiert sind in eine Liste ein
  mStyles := TQOAssigns.Create;
  
  // Zuerst einmal "Alle Artikel" definieren ...
  xQOStyle := TQOAllStyles.Create;
  xQOStyle.LinkID := cAllStylesLinkID;
  mStyles.add(xQOStyle);
  
  // ... dann alle Artikel von der DB holen
  with TAdoDBAccess.Create(1) do try
    Init;
    with Query[0] do begin
      SQL.Text := cGetAllStyles;
      open;
      while not(EOF) do begin
        xQOStyle := TQOStyle.Create;
        xQOStyle.LinkID := FieldByName('c_style_id').AsInteger;
        xQOStyle.QOVisualize.Caption := FieldByName('c_style_name').AsString;
        mStyles.add(xQOStyle);
        next;
      end;// while not(EOF) do begin
    end;// with Query[0] do begin
  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try
  
  (* Alarm Frame erzeugen damit der DataItemTree zu Verf�gung steht
     Vom DataItemTree werden die Bitmaps der DataItems in die eigene
     ImageList kopiert. *)
  
  CreateSettingsAlarmFrame;
  
  // ... Dann die Bitmaps herauskopieren
  if assigned(mSettingsAlarmFrame) then begin
    il := mSettingsAlarmFrame.Images;
    xRect := Rect(0, 0, mTreeImages.Width, mTreeImages.Height);
  
    xMask := nil;
    xImage := TBitmap.Create;
    try
      xImage.Height := mTreeImages.Height;
      xImage.Width  := mTreeImages.Width;
  
      xMask := TBitmap.Create;
      xMask.Monochrome := True;
      xMask.Height := mTreeImages.Height;
      xMask.Width  := mTreeImages.Width;
  
      with TImageIndexHandler.Instance do begin
        MatrixTrendImageIndex     := AppendImageToTreeImages(cMatrixTrend);
        MatrixTrendEditImageIndex := AppendImageToTreeImages(cMatrixTrendEdit);
        FunctionImageIndex        := AppendImageToTreeImages(cFunctionImage);
      end;// with TImageIndexHandler.Instance do begin
  
    finally
      FreeAndNil(xMask);
      FreeAndNil(xImage);
    end;// try finally
  end;// if assigned(mAlarmSettingsFrame) then begin

//: ----------------------------------------------
  (* ACHTUNG
     Dieser Abschnitt muss am Ende der Methode stehen *)
  mCreated := true;
  Init;
end;// TfrmMainWindow.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmMainWindow.Destroy;
begin
  FreeAndNil(mStyles);
  FreeAndNil(mObserver);

//: ----------------------------------------------
  inherited;
end;// TfrmMainWindow.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           acCallLabReportExecute
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Holt das Interface zum Labreport und ruft ihn mit den aktuellen Parametern auf
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.acCallLabReportExecute(Sender: TObject);
var
  xWindowHandle: THandle;
begin
  try
    try
      Screen.Cursor := crHourglass;
      if assigned(mAnalyzeChartFrame) then begin
        // Interface zum Labreport erzeugen
        mLabReport := CreateOLEObject(cOLELabReport);
  
        // Handel des Wraper Forms (COM) ermitteln
        xWindowHandle := 0;
        if assigned(WrapperForm) then
          xWindowHandle := WrapperForm.Handle;
  
        // Labreport starten
        mLabReport.OpenReport(mAnalyzeChartFrame.GetLabReportSettings(xWindowHandle));
        // Interface wieder freigeben, damit der Labreport unabh�ngig ist
        mLabReport := unassigned;
      end;// if assigned(mAnalyzeChartFrame) then begin
    finally
      Screen.Cursor := crDefault;
      // Interface wieder freigeben, damit der Labreport unabh�ngig ist
      mLabReport := unassigned;
    end;// try finally
  except
    on e:Exception do begin
      ShowMessage(rsCallLabReportError+ ccrlf + ccrlf + e.Message);
    end;
  end;// try except
end;// TfrmMainWindow.acCallLabReportExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acCollapseExecute
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Klappt das TreeView zusammen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.acCollapseExecute(Sender: TObject);
var
  xNodeArray: TNodeArray;
  i: Integer;
begin
  xNodeArray := nil;
  
  // Settings
  if mTreePageControl.ActivePage = tabSettings then
    mSettingsTree.FullCollapse;

//: ----------------------------------------------
  // Zuordnung
  if mTreePageControl.ActivePage = tabAssign then
    mAssignTree.FullCollapse;

//: ----------------------------------------------
  // Auswertung
  if mTreePageControl.ActivePage = tabAnalyze then begin
    // evt. sind mehrere Items selektiert
    xNodeArray := mAnalyzeTree.GetSortedSelection(false);
    // ... deshalb alle selektierten Nodes zuklappen
    for i := 0 to Length(xNodeArray) - 1 do
      // Entsprechenden Node zuklappen
      mAnalyzeTree.FullCollapse(xNodeArray[i]);
  
    if mAnalyzeTree.GetNodeLevel(mAnalyzeTree.FocusedNode) = 0 then
      mAnalyzeTree.Expanded[mAnalyzeTree.FocusedNode] := true;
  end;// if mTreePageControl.ActivePage = tabAnalyze then begin
end;// TfrmMainWindow.acCollapseExecute cat:No category

//:-------------------------------------------------------------------
procedure TfrmMainWindow.acConfigSecurityExecute(Sender: TObject);
begin
  mSecurityControl.Configure;
end;// TfrmMainWindow.acConfigSecurityExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acDeleteAssignExecute
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: l�scht eine Zuordnung (Artikel oder Maschine)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.acDeleteAssignExecute(Sender: TObject);
var
  xSelection: TNodeArray;
  i: Integer;
  xAssign: TQOAssign;
  xParent: TObject;
  xParentNode: PVirtualNode;
  xIndex: Integer;
  xAlarm: TQOAlarm;
  xAllow: Boolean;
begin
  // Beinhaltet alle selektierten Elemente
  xSelection := mAssignTree.GetSortedSelection(false);

//: ----------------------------------------------
  xAllow := AskForGUIDelete(mAssignTree, xSelection);
  if xAllow then begin
    case mAssignStyle of

//: ----------------------------------------------
      rsAlarm: begin
        for i := Low(xSelection) to High(xSelection) do begin
          xAssign := TQOAssign(GetNodeObject(mAssignTree, xSelection[i]));
          // Zuordnung auf der DB l�schen
          xAssign.DeleteState := dsPending;
          xAssign.SaveDefToDB;
  
          // �bergeordneten Knoten feststellen
          xParentNode := mAssignTree.NodeParent[xSelection[i]];
          if assigned(xParentNode) then begin
            xParent := GetNodeObject(mAssignTree, xParentNode);
            if assigned(xParent) then begin
  
              // xAssign ist ein Artikel
              if (xParent is TQOAlarm) then begin
                xIndex := TQOAlarm(xParent).QOAssigns.IndexOf(xAssign);
                if xIndex >= 0 then
                  TQOAlarm(xParent).QOAssigns.Delete(xIndex)
                else
                  xAssign.Free;
              end;//if (xParent is TQOAlarm) then begin
  
              // xAssign ist eine Maschine
              if (xParent is TQOAssign) then begin
                xIndex := TQOAssign(xParent).QOAssigns.IndexOf(xAssign);
                if xIndex >= 0 then
                  TQOAssign(xParent).QOAssigns.Delete(xIndex)
                else
                  xAssign.Free;
              end;// if (xParent is TQOAssign) then begin
            end;// if assigned(xParent) then begin
          end;// if assigned(xParentNode) then begin
  
          mAssignTree.DeleteNode(xSelection[i]);
        end;// for i := Low(xSelection) to High(xSelection) do begin
      end;// rsAlarm: begin

//: ----------------------------------------------
      rsStyle: begin
        for i := Low(xSelection) to High(xSelection) do begin
          xParentNode := mAssignTree.NodeParent[xSelection[i]];
          if assigned(xParentNode) then begin
            // Artikel der zum zu l�schenden Alarm geh�rt
            xAssign := nil;
            if assigned(GetNodeObject(mAssignTree, xParentNode)) then
              xAssign := TQOAssign(GetNodeObject(mAssignTree, xParentNode));
  
            xAlarm := nil;
            if assigned(GetNodeObject(mAssignTree, xSelection[i])) then
              xAlarm := TQOAlarm(GetNodeObject(mAssignTree, xSelection[i]));
  
            if assigned(xAlarm) and assigned(xAssign) then begin
              xIndex := xAlarm.QOAssigns.IndexOfLinkID(xAssign.LinkID);
              if xIndex >= 0 then begin
                xAssign := xAlarm.QOAssigns[xIndex];
  
                // Zuordnung auf der DB l�schen
                xAssign.DeleteState := dsPending;
                xAssign.SaveDefToDB;
  
                // Im Model l�schen
                xAlarm.QOAssigns.Delete(xIndex);
  
                // Im Tree l�schen
                mAssignTree.DeleteNode(xSelection[i]);
              end;// if xAlarm.QOAssigns.IndexOfLinkID(xAssign.LinkID) >= 0 then begin
            end;// if assigned(xAlarm) and assigned(xAssign) then begin
          end;// if assigned(xParentNode) then begin
        end;// for i := Low(xSelection) to High(xSelection) do begin
      end;// rsStyle: begin

//: ----------------------------------------------
    end;// case mAssignStyle of
  end;// if xAllow then begin
end;// TfrmMainWindow.acDeleteAssignExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acDeleteFromArchiveExecute
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Entfernt den Datensatz aus dem Archiv
 *  Beschreibung:     
                      Der Datensatz wird nicht gel�scht, sondern einfach wieder als 
                      automatisch generiert markiert.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.acDeleteFromArchiveExecute(Sender: TObject);
var
  xNodeArray: TNodeArray;
  i: Integer;
  xAllow: Boolean;
begin
  xNodeArray := nil;
  
  // Je nach angecklicktem Knoten wird auf der rechten Seite ein Frame angezeigt
  case mAnalyzeTree.GetNodeLevel(mAnalyzeTree.FocusedNode) of
    // Schicht Level
    1: begin
      // Alle selektierten Nodes holen
      xNodeArray := mAnalyzeTree.GetSortedSelection(false);
      xAllow := AskForGUIDelete(mAnalyzeTree, xNodeArray, cAskForDeleteShiftFromArchiv, 0);
      if xAllow then begin
        // ... und dann die selektierten Schichten speichern
        for i := 0 to Length(xNodeArray) - 1 do begin
          if (GetNodeObject(mAnalyzeTree, xNodeArray[i]) is TQOShiftView) then
            TQOShiftView(GetNodeObject(mAnalyzeTree, xNodeArray[i])).DeleteFromArchive;
        end;// for i := 0 to Length(xNodeArray) - 1 do begin
        RefreshAnalyzeData;
      end;// if xAllow then begin
    end;// 1: begin
  end;// case mAnalyzeTree.GetNodeLevel(mAnalyzeTree.FocusedNode) of
end;// TfrmMainWindow.acDeleteFromArchiveExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acDeleteSettingsExecute
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: L�scht die selektierten Data Items aus dem Tree und dem Modell
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.acDeleteSettingsExecute(Sender: TObject);
var
  i: Integer;
  xIndex: Integer;
  xNodeObject: TObject;
  xParent: TObject;
  xSelection: TNodeArray;
  xParentNode: PVirtualNode;
  xNodeToSelect: PvirtualNode;
  xAllow: Boolean;
  
  procedure DeleteAlarm(aAlarm: TQOAlarm);
  begin
    with TAdoDBAccess.Create(1) do try
      Init;
      aAlarm.DeleteFromDB(Query[0]);
      if assigned(mSettingsAlarmFrame) then begin
        if mSettingsAlarmFrame.QOAlarm = aAlarm then
          mSettingsAlarmFrame.QOAlarm := nil;
      end;// if assigned(mAlarmSettingsFrame) then begin
  
      // Wenn eines der DataItems des Alarms angezeigt wird, sicherstellen dass nichts passiert
      if assigned(mSettingsItemFrame) then begin
        if aAlarm.QOItems.IndexOf(mSettingsItemFrame.QOItem) >= 0 then
          mSettingsItemFrame.QOItem := nil;
      end;// if assigned(mAlarmSettingsFrame) then begin
  
      TQOAlarms.Instance.Delete(aAlarm);
    finally
      free;
    end;// with TAdoDBAccess.Create(1) do try
  end;// procedure DeleteAlarm(aAlarm: TQOAlarm);
  
  procedure DeleteItem(aQOItem: TQOItem; aNode: PVirtualNode);
  begin
    aQOItem.DeleteState := dsPending;
    aQOItem.SaveDefToDB;
    if assigned(mSettingsItemFrame) then begin
      if mSettingsItemFrame.QOItem = aQOItem then
        mSettingsItemFrame.QOItem := nil;
    end;// if assigned(mAlarmSettingsFrame) then begin
  
  end;// procedure DeleteItem(aQOItem: TQOItem);
  
  {$Hints off} {L�st einen Hint aus, da das Ergebnis direkt verrechnet wird}
  function CalculateNextNodeToSelect(aNode: PVirtualNode): PVirtualNode;
  begin
    result := nil;
  
    if assigned(mSettingsTree.GetNextSibling(aNode)) then begin
      Result :=  mSettingsTree.GetNextSibling(aNode)
    end else begin
      Result :=  mSettingsTree.GetPreviousSibling(aNode);
      if not(assigned(result)) then
        result := mSettingsTree.NodeParent[aNode];
    end;// if assigned(mSettingsTree.GetNextSibling(aNode)) then begin
    if result = mSettingsTree.RootNode then
      result := nil;
  end;// function CalculateNextNodeToSelect(aNode: PVirtualNode): PVirtualNode;
  {$Hints on}
  
begin
  try
    BeginUpdate;

//: ----------------------------------------------
    xNodeToSelect := nil;
  
    // Beinhaltet alle selektierten Elemente
    xSelection := mSettingsTree.GetSortedSelection(false);
  
    xAllow := AskForGUIDelete(mSettingsTree, xSelection);
    if xAllow then begin
      for i := Low(xSelection) to High(xSelection) do begin
        xNodeObject := GetNodeObject(mSettingsTree, xSelection[i]);
  
        // Objekte aus dem Modell l�schen
        if (xNodeObject is TQOItem) then
          DeleteItem(TQOItem(xNodeObject), xSelection[i]);
  
        if (xNodeObject is TQOAlarm) then
          DeleteAlarm(TQOAlarm(xNodeObject));
  
        // �bergeordneten Knoten feststellen
        xParentNode := mSettingsTree.NodeParent[xSelection[i]];
        if assigned(xParentNode) then begin
          xParent := GetNodeObject(mSettingsTree, xParentNode);
          if assigned(xParent) then begin
            // Parent ist der Alarm
            if (xParent is TQOAlarm) then begin
              xIndex := TQOAlarm(xParent).QOItems.IndexOf(TQOItem(xNodeObject));
              if xIndex >= 0 then
                TQOAlarm(xParent).QOItems.Delete(xIndex)
              else
                xNodeObject.Free;
            end;//if (xParent is TQOAlarm) then begin
  
          end;// if assigned(xParent) then begin
        end;// if assigned(xParentNode) then begin
        xNodeToSelect := CalculateNextNodeToSelect(xSelection[i]);
        mSettingsTree.DeleteNode(xSelection[i]);
  
      end;// for i := Low(xSelection) to High(xSelection) do begin
    end else begin
      // Der Knoten soll nicht gel�scht werden
      xNodeToSelect := mSettingsTree.FocusedNode;
    end;// if xAllow then begin

//: ----------------------------------------------
  finally
    EndUpdate;
  end;// try finally

//: ----------------------------------------------
  mSettingsTree.FocusedNode := xNodeToSelect;
  mSettingsTree.Selected[xNodeToSelect] := true;
end;// TfrmMainWindow.acDeleteSettingsExecute cat:No category

//:-------------------------------------------------------------------
procedure TfrmMainWindow.acEditAlarmExecute(Sender: TObject);
begin
  inherited;
  //
end;// TfrmMainWindow.acEditAlarmExecute cat:No category

//:-------------------------------------------------------------------
procedure TfrmMainWindow.acEditDataItemExecute(Sender: TObject);
begin
  inherited;
  //
end;// TfrmMainWindow.acEditDataItemExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acEvaluateExecute
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Generiert tempor�r einen QOfflimit
 *  Beschreibung:     
                      Die Berechnungen werden nicht auf der DB gesichert sondern nur
                      im entsprechenden Tree (Analyze) angezeigt.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.acEvaluateExecute(Sender: TObject);
var
  xQOShiftView: TQOShiftView;
begin
  try
    Screen.Cursor := crSQLWait;
    TQOAlarms.Instance.Evaluate(false);
    showMessage(Format(rsErmittelteOfflimitVerletzungen + ' %d', [TQOAlarms.Instance.OfflimitCount]));
  
    if TQOAlarms.Instance.OfflimitCount > 0 then begin
      xQOShiftView := TQOShiftView.Create;
      xQOShiftView.Assign(TQOAlarms.Instance);
      // Manuelle Alarme am Anfang der Liste einf�gen, so dass der neueste Alarm immer oben ist
      TQOShiftSet.Instance.Insert(0, xQOShiftView);
      RefreshAnalyzeData;
    end;// if TQOAlarms.Instance.OfflimitCount > 0 then begin
  finally
    Screen.Cursor := crDefault;
  end;// try finally
end;// TfrmMainWindow.acEvaluateExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acExitExecute
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Programmende
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.acExitExecute(Sender: TObject);
begin
  Close;
end;// TfrmMainWindow.acExitExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acExpandExecute
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Blendet alle Zweige des Trees ein
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.acExpandExecute(Sender: TObject);
var
  xLevel: Integer;
  xNode: PVirtualNode;
  xNodeArray: TNodeArray;
  i: Integer;
begin
  xNodeArray := nil;
  
  // Settings
  if mTreePageControl.ActivePage = tabSettings then
    mSettingsTree.FullExpand;

//: ----------------------------------------------
  // Zuordnung
  if mTreePageControl.ActivePage = tabAssign then
    mAssignTree.FullExpand;

//: ----------------------------------------------
  // Auswertung
  if mTreePageControl.ActivePage = tabAnalyze then begin
    xLevel := mAnalyzeTree.GetNodeLevel(mAnalyzeTree.FocusedNode);
    // Initialisiert die Behandlung f�r die ESCAPE Taste (Win API)
    GetAsyncKeyState(VK_ESCAPE);
  
    try
      (* Aufklappen k�nnte l�nger dauern, da unter Umst�nden
         relativ viele Daten ermittelt werden m�ssen *)
      Screen.Cursor := crHourglass;
      mExpandByAction := true;
      case xLevel of
        // Auf dem untersten Level werden alle Knoten bis auf das Alarm Level aufgeklappt
        0: begin
          // Zuerst alle Knoten bis auf Schichtlevel zuklappen
          acCollapse.Execute;
          // Dann alle Schichten durchlaufen und die Schichten aufmachen
          xNode := mAnalyzeTree.GetFirstChild(mAnalyzeTree.FocusedNode);
          // Die Funktion kann mit der ESCAPE Taste abgebrochen werden
          while (xNode <> nil) and (GetAsyncKeyState(VK_ESCAPE) = 0) do begin
            mAnalyzeTree.Expanded[xNode] := true;
            // N�chste Schicht
            xNode := mAnalyzeTree.GetNextSibling(xNode);
          end;// while xNode <> nil do begin
        end;// 0:
        else
          // evt. sind mehrere Items selektiert
          xNodeArray := mAnalyzeTree.GetSortedSelection(false);
          // ... deshalb alle selektierten Nodes aufklappen
          for i := 0 to Length(xNodeArray) - 1 do begin
            // Die Funktion kann mit der ESCAPE-Taste unterbrochen werden
            if (GetAsyncKeyState(VK_ESCAPE) <> 0) then
              break;
            // Entsprechenden Node aufklappen
            mAnalyzeTree.FullExpand(xNodeArray[i]);
          end;// for i := 0 to Length(xNodeArray) - 1 do begin
      end;// case xLevel of
    finally
      mExpandByAction := false;
      Screen.Cursor := crDefault;
      // Den Ursprungsknoten wieder anzeigen, sollte das Fenster gescrollt woeden sein
      mAnalyzeTree.ScrollIntoView(mAnalyzeTree.FocusedNode, false)
    end;// try finally
  end;// if mTreePageControl.ActivePage = tabAnalyze then begin
end;// TfrmMainWindow.acExpandExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acHelpExecute
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Zeigt die Hilfe an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;// TfrmMainWindow.acHelpExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acNewExecute
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Erzeugt einen neuen Alarm
 *  Beschreibung:     
                      Der Alarm wird der globalen Liste mit den Alarmen hinzugef�gt.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.acNewExecute(Sender: TObject);
var
  xQOAlarm: TQOAlarm;
  xUniqueName: String;
  
  // Gibt einen Eindeutigen Namen zur�ck
  function GetUniqueAlarmName(aName: string): String;
  var
    xVerified: boolean;
    i: integer;
  begin
    xVerified := false;
    Result := aName;
    i := 1;
    while not(xVerified) do begin
      xVerified := (TQOAlarms.Instance.IndexOf(result) = -1);
      if not(xVerified) then
        result := Format(aName + ' (%d)', [i]);
      inc(i);
    end;// while not(xVerified) do begin
  end;// function GetUniqueAlarmName(aName: string): String;
  
begin
  // zuerst einmal die aktuellen Settings speichern
  if not(SaveCurrentSettings = mrCancel) then begin
    xUniqueName := GetUniqueAlarmName(rsNewAlarm);
    // Neuen Alarm hinzuf�gen
    xQOAlarm := TQOAlarms.Instance.AddNewAlarm(xUniqueName);
    // ... und gleich speichern
    xQOAlarm.SaveDefToDB;
    // Baum neu anzeigen
    BuildSettingsTreeFromAlarms;
    // Alte Selektion l�schen
    mSettingsTree.ClearSelection;
    // Neuen Alarm selektieren
    SelectNodeFromObject(mSettingsTree, xQOAlarm);
  end;// if not(SaveCurrentSettings = mrCancel) then begin
end;// TfrmMainWindow.acNewExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acPrintAnalyzeExecute
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Druckt den Schichtbericht aus
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.acPrintAnalyzeExecute(Sender: TObject);
var
  xNodeArray: TNodeArray;
  i: Integer;
  xParentNode: PVirtualNode;
  xAssign: TQOAssignNodeObject;
begin
  xNodeArray := nil;
  
  // Je nach angecklicktem Knoten wird auf der rechten Seite ein Frame angezeigt
  case mAnalyzeTree.GetNodeLevel(mAnalyzeTree.FocusedNode) of
    // Schicht Level
    1: begin
      with TfrmQOReportShiftList.create(self) do try
        // Die Liste mit den Alarmen l�schen
        ClearShiftList;
  
        // Jetzt die selektierten Nodes holen
        xNodeArray := mAnalyzeTree.GetSortedSelection(false);
        // ... und dann alle Alarme der Liste des Reports hinzuf�gen
        for i := 0 to Length(xNodeArray) - 1 do begin
          if (GetNodeObject(mAnalyzeTree, xNodeArray[i]) is TQOShiftView) then
            AddShift(TQOShiftView(GetNodeObject(mAnalyzeTree, xNodeArray[i])));
        end;// for i := 0 to Length(xNodeArray) - 1 do begin
  
        // Wenn keine Schicht selektiert ist, dann alle Alarme ausdrucken
        if ShiftCount = 0 then begin
          if TQOShiftSet.Instance.ShiftCount > 0 then
            AddShift(TQOShiftSet.Instance.Shifts[0]);
        end;// if ShiftCount = 0 then begin
  
        // Alle selektierten Alarme ausdrucken
        Print(true);
      finally
        free;
      end;// try finally
    end;// 1: begin
  
    // Chart Level
    3,4:begin
      with TfrmQOReportAnalyzeChart.create(self) do try
        // Die Liste mit den Alarmen l�schen
        ClearAssignList;
  
        // Jetzt die selektierten Nodes holen
        xNodeArray := mAnalyzeTree.GetSortedSelection(false);
        // ... und dann alle Alarme der Liste des Reports hinzuf�gen
        for i := 0 to Length(xNodeArray) - 1 do begin
          if (GetNodeObject(mAnalyzeTree, xNodeArray[i]) is TQOAssignNodeObject) then begin
            xAssign := TQOAssignNodeObject(GetNodeObject(mAnalyzeTree, xNodeArray[i]));
            case xAssign.NodeObjectType of
              otStyle: AddAssign(TQOAlarmView(GetNodeObject(mAnalyzeTree, mAnalyzeTree.NodeParent[xNodeArray[i]])), xAssign, nil);
              otMachine: begin
                xParentNode := mAnalyzeTree.NodeParent[xNodeArray[i]];
                AddAssign(TQOAlarmView(GetNodeObject(mAnalyzeTree, mAnalyzeTree.NodeParent[xParentNode])),
                          TQOAssignNodeObject(GetNodeObject(mAnalyzeTree, xParentNode)),
                          xAssign);
              end;// otMachine: begin
            end;// case xAssign.NodeObjectType of
          end;// if (GetNodeObject(mAnalyzeTree, xNodeArray[i]) is TQOAssignNodeObject) then begin
        end;// for i := 0 to Length(xNodeArray) - 1 do begin
  
        if AssignCount > 0 then begin
          // Alle selektierten Charts ausdrucken
          Print(true);
        end;// if AssignCount = 0 then begin
  
      finally
        free;
      end;// try finally
    end;// 3,4:begin
  end;// case mAnalyzeTree.GetNodeLevel(mAnalyzeTree.FocusedNode) of
end;// TfrmMainWindow.acPrintAnalyzeExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acPrintSettingsExecute
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: -
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.acPrintSettingsExecute(Sender: TObject);
var
  xNodeArray: TNodeArray;
  i: Integer;
begin
  with TfrmQOReportSettings.create(self) do try
    // Die Liste mit den Alarmen l�schen
    ClearAlarmList;
  
    // Jetzt die selektierten Nodes holen
    xNodeArray := mSettingsTree.GetSortedSelection(false);
    // ... und dann alle Alarme der Liste des Reports hinzuf�gen
    for i := 0 to Length(xNodeArray) - 1 do begin
      if (GetNodeObject(mSettingsTree, xNodeArray[i]) is TQOAlarm) then
        AddAlarm(TQOAlarm(GetNodeObject(mSettingsTree, xNodeArray[i])));
    end;// for i := 0 to Length(xNodeArray) - 1 do begin
  
    // Wenn kein Alarm selektiert ist, dann alle Alarme ausdrucken
    if AlarmCount = 0 then begin
      for i := 0 to TQOAlarms.Instance.AlarmCount - 1 do
        AddAlarm(TQOAlarms.Instance.Alarms[i]);
    end;// if AlarmCount = 0 then begin
  
    // Alle selektierten Alarme ausdrucken
    Print(true);
  finally
    free;
  end;// try finally
end;// TfrmMainWindow.acPrintSettingsExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acRefreshDataExecute
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: L�dt die Alarmdaten neu von der Datenbank
 *  Beschreibung:     
                      Bereits geladene Alarme werden zuerst gel�scht
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.acRefreshDataExecute(Sender: TObject);
begin
  RefreshAnalyzeData;
end;// TfrmMainWindow.acRefreshDataExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acSaveDataExecute
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Markiert einen Datensatz auf der Datenbank als persistent
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.acSaveDataExecute(Sender: TObject);
var
  xNodeArray: TNodeArray;
  i: Integer;
begin
  xNodeArray := nil;
  
  // Je nach angecklicktem Knoten wird auf der rechten Seite ein Frame angezeigt
  case mAnalyzeTree.GetNodeLevel(mAnalyzeTree.FocusedNode) of
    // Schicht Level
    1: begin
      // Alle selektierten Nodes holen
      xNodeArray := mAnalyzeTree.GetSortedSelection(false);
      // ... und dann die selektierten Schichten speichern
      for i := 0 to Length(xNodeArray) - 1 do begin
        if (GetNodeObject(mAnalyzeTree, xNodeArray[i]) is TQOShiftView) then
          TQOShiftView(GetNodeObject(mAnalyzeTree, xNodeArray[i])).SaveInArchive;
      end;// for i := 0 to Length(xNodeArray) - 1 do begin
      RefreshAnalyzeData;
    end;// 1: begin
  end;// case mAnalyzeTree.GetNodeLevel(mAnalyzeTree.FocusedNode) of
end;// TfrmMainWindow.acSaveDataExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddAlarms
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (aSourceTree, aSourceAlarms, aDestStyles)
 *
 *  Kurzbeschreibung: F�gt die Atrikel den Alarmen hinzu
 *  Beschreibung:     
                      Diese Methode wird aufgerufen in der Style-Alarm Sicht. Visuell
                      wird also der Alarm dem Artikel zugeordnet. Im Modell ist allerdings immer
                      der Alarm oberste Instanz.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.AddAlarms(aSourceTree:TBaseVirtualTree; aSourceAlarms, aDestStyles: TNodeArray);
var
  i: Integer;
  j: Integer;
  xAlarm: TQOAlarm;
  xStyle: TQOStyle;
  xAssigned: Boolean;
  xSomeMachsNotAssigned: Boolean;
begin
  xSomeMachsNotAssigned := false;
  for i := Low(aDestStyles) to High(aDestStyles) do begin
    for j := Low(aSourceAlarms) to High(aSourceAlarms) do begin
      xAssigned := false;
  
      xStyle := TQOStyle.Create;
      if assigned(GetNodeObject(aSourceTree, aDestStyles[i]))then
        xStyle.Assign(TQOStyle(GetNodeObject(aSourceTree, aDestStyles[i])));
  
      if assigned(GetNodeObject(aSourceTree, aSourceAlarms[j])) then begin
        xAlarm := TQOAlarm(GetNodeObject(aSourceTree, aSourceAlarms[j]));
  
        if xAlarm.QOAssigns.IndexOfLinkID(xStyle.LinkID) < 0 then begin
          xAlarm.QOAssigns.Add(xStyle);
          xAssigned := true;
  
          // Zuerst auf der DB sichern
          xStyle.ParentID := xAlarm.ID;
          xStyle.SaveDefToDB;
          // Und auch noch dem Tree hinzuf�gen
          AddVSTStructure(mAssignTree, aDestStyles[i], xAlarm);
        end else begin
          xSomeMachsNotAssigned := true;
        end;// if xAlarm.QOAssigns.IndexOfLinkID(xStyle.LinkID) < 0 then begin
      end;// if assigned(GetNodeObject(aSourceTree, aDestAlarms[j])) then begin
  
      // Wenn der Artikel aus irgend einem Grund nicht zugeordnet werden kann, dann l�schen
      if not(xAssigned) then
        xStyle.Free;
    end;// for j := Low(aSourceAlarms) to High(aSourceAlarms) do begin
  end;// for i := Low(aDestStyles) to High(aDestStyles) do begin

//: ----------------------------------------------
  if xSomeMachsNotAssigned then
    mmMessageDlg(rsNotAllAlarmsAssigned, mtInformation, [mbOk], 0, nil);
end;// TfrmMainWindow.AddAlarms cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddItems
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (aSourceTree, aSourceItems, aDestAlarms)
 *
 *  Kurzbeschreibung: F�gt den Alarmen die Data Items hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.AddItems(aSourceTree: TBaseVirtualTree; aSourceItems, aDestAlarms: TNodeArray);
var
  i: Integer;
  j: Integer;
  xAssigned: Boolean;
  xQOItem: TQOItem;
  xAlarm: TQOAlarm;
  xAbort: Boolean;
  xSomeAlarmsNotAssigned: Boolean;
  xClassDefects: TClassDefectsSet;
begin
  xSomeAlarmsNotAssigned := false;
  
  try
    if not(SaveCurrentSettings = mrCancel) then begin
      BeginUpdate;
      for i := Low(aSourceItems) to High(aSourceItems) do begin
        for j := Low(aDestAlarms) to High(aDestAlarms) do begin
          xAssigned := false;
          xAbort := false;
  
          // Keine GruppierungsItems zugelassen
          if not(GetNodeObject(aSourceTree, aSourceItems[i]) is TGroupDataItem) then begin
            xQOItem := TQOItem.Create;
            if assigned(GetNodeObject(aSourceTree, aSourceItems[i]))then
              xQOItem.Assign(TQOItem(GetNodeObject(aSourceTree, aSourceItems[i])));
  
            if assigned(GetNodeObject(aSourceTree, aDestAlarms[j])) then begin
              xAlarm := TQOAlarm(GetNodeObject(aSourceTree, aDestAlarms[j]));
  
              // Wenn ein Matrix DataItem ausgew�hlt ist, die Art der Classdefects abfragen
              if xQOItem.IsMatrixDataItem then begin
                with TfrmSelectDefects.Create(self) do try
                  // Name des DataItems anzeigen
                  TitleInfo := xQOItem.DisplayName;
                  xClassDefects := xAlarm.QOItems.GetClassDefectsFromName(xQOItem.ItemName);
                  DisabledClassDefects := xClassDefects;
                  if not(xClassDefects = [cdCut, cdUncut, cdBoth]) then
                    ShowModal;
                  xAbort := ModalResult = mrCancel;
                  xQOItem.ClassDefects := DefectType;
                finally
                  Free;
                end;// with TAdoDBAccess.Create(1) do try
              end;// if xQOItem.IsMatrixDef then begin
  
              if not(xAlarm.QOItems.SameDataItemExists(xQOItem.ItemName, xQOItem.ClassDefects)) then begin
                if not(xAbort) then begin
                  xAlarm.QOItems.Add(xQOItem);
                  xAssigned := true;
  
                  // Zuerst auf der DB sichern
                  xQOItem.AlarmID := xAlarm.ID;
                  xQOItem.SaveDefToDB;
                  // Und auch noch dem Tree hinzuf�gen
                  AddVSTStructure(mSettingsTree, aDestAlarms[j], xQOItem);
                end;// if not(xAbort) then begin
              end else begin
                xSomeAlarmsNotAssigned := true;
              end;// if xAlarm.QOItems.IndexOfItemName(xQOItem.ItemName) < 0 then begin
            end;// if not(xAlarm.QOItems.SameDataItemExists(xQOItem.ItemName)) then begin
  
            // Wenn das DataItem aus irgend einem Grund nicht zugeordnet werden kann, dann l�schen
            if not(xAssigned) then
              xQOItem.Free;
          end;// if not(GetNodeObject(aSourceTree, aSourceItems[i]) is TGroupDataItem) then begin
        end;// for j := Low(aDestStyles) to High(aDestStyles) do begin
      end;// for i := Low(aSourceMachines) to High(aSourceMachines) do begin
    end;// if not(SaveCurrentSettings = mrCancel) then begin
  finally
    EndUpdate;
  end;// try finally

//: ----------------------------------------------
  // Knoten wieder slektieren
  if Length(aDestAlarms) > 0 then begin
    with mSettingsTree do begin
      FocusedNode := aDestAlarms[High(aDestAlarms)];
      if not(Selected[FocusedNode]) then begin
        ClearSelection;
        Selected[FocusedNode] := true;
      end;// if not(.Selected[.FocusedNode]) then begin
    end;// with mSettingsTree do begin
  end;// if Length(aDestAlarms) > 0 then begin

//: ----------------------------------------------
  if xSomeAlarmsNotAssigned then
    mmMessageDlg(rsNotAllItemsAssigned, mtInformation, [mbOk], 0, nil);
end;// TfrmMainWindow.AddItems cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddMachines
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (aSourceTree, aSourceMachines, aDestStyles)
 *
 *  Kurzbeschreibung: F�gt den Artikeln die Maschinen hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.AddMachines(aSourceTree:TBaseVirtualTree; aSourceMachines, aDestStyles: TNodeArray);
var
  i: Integer;
  j: Integer;
  xMachine: TQOMachine;
  xStyle: TQOStyle;
  xAssigned: Boolean;
  xSomeMachNotAssigned: Boolean;
begin
  xSomeMachNotAssigned := false;
  
  for i := Low(aSourceMachines) to High(aSourceMachines) do begin
    for j := Low(aDestStyles) to High(aDestStyles) do begin
      xAssigned := false;
      xMachine := TQOMachine.Create;
      if assigned(GetNodeObject(aSourceTree, aSourceMachines[i]))then
        xMachine.Assign(TQOMachine(GetNodeObject(aSourceTree, aSourceMachines[i])));
  
      if assigned(GetNodeObject(aSourceTree, aDestStyles[j])) then begin
        xStyle := TQOStyle(GetNodeObject(aSourceTree, aDestStyles[j]));
  
        // Pr�fen ob die Maschine bereits im entsprechenden Artikel erfasst ist
        if xStyle.QOAssigns.IndexOfLinkID(xMachine.LinkID) < 0 then begin
          // ... wenn nein, dann hinzuf�gen
          xStyle.QOAssigns.Add(xMachine);
          xAssigned := true;
  
          // Zuerst auf der DB sichern
          xMachine.ParentID := xStyle.ID;
          xMachine.SaveDefToDB;
          // Und auch noch dem Tree hinzuf�gen
          AddVSTStructure(mAssignTree, aDestStyles[j], xMachine,[nsExpanded]);
        end else begin
          xSomeMachNotAssigned := true;
        end;// if xAlarm.QOAssigns.IndexOfLinkID(xMachine.LinkID) < 0 then begin
      end;//if assigned(GetNodeObject(aSourceTree, aDestStyles[j])) then begin
  
      // Wenn der Artikel aus irgend einem Grund nicht zugeordnet werden kann, dann l�schen
      if not(xAssigned) then
        xMachine.Free;
    end;// for j := Low(aDestStyles) to High(aDestStyles) do begin
  end;// for i := Low(aSourceMachines) to High(aSourceMachines) do begin

//: ----------------------------------------------
  if xSomeMachNotAssigned then
    mmMessageDlg(rsNotAllMachsAssigned, mtInformation, [mbOk], 0, nil);
end;// TfrmMainWindow.AddMachines cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddStyles
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (aSourceTree, aSourceStyles, aDestAlarms)
 *
 *  Kurzbeschreibung: F�gt den Alarmen die Artikel hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.AddStyles(aSourceTree:TBaseVirtualTree; aSourceStyles, aDestAlarms: TNodeArray);
var
  i: Integer;
  j: Integer;
  xAlarm: TQOAlarm;
  xStyle: TQOStyle;
  xAssigned: Boolean;
  xAllow: Boolean;
  xSomeStylesNotAssigned: Boolean;
begin
  xSomeStylesNotAssigned := false;
  for i := Low(aSourceStyles) to High(aSourceStyles) do begin
    for j := Low(aDestAlarms) to High(aDestAlarms) do begin
      xAssigned := false;
  
      // Zuerst die beteiligten Objekte �bernehmen
      xStyle := nil;
      if assigned(GetNodeObject(aSourceTree, aSourceStyles[i]))then begin
        // Kopie mit dem richtigen Konstruktor erstellen (kann TQOStyle oder TQOAllStyles sein)
        xStyle := TQOStyleClass(GetNodeObject(aSourceTree, aSourceStyles[i]).ClassType).Create;
        // ... und dann kopieren
        xStyle.Assign(TQOStyle(GetNodeObject(aSourceTree, aSourceStyles[i])));
      end;// if assigned(GetNodeObject(aSourceTree, aSourceStyles[i]))then begin
  
      if assigned(xStyle) then begin
        if assigned(GetNodeObject(aSourceTree, aDestAlarms[j])) then begin
          xAlarm := TQOAlarm(GetNodeObject(aSourceTree, aDestAlarms[j]));
  
          // Pr�fen ob der Artikel bereits in der Liste ist, oder ob "Alle Artikel" definiert ist
          if (xAlarm.QOAssigns.IndexOfLinkID(xStyle.LinkID) < 0)
             and (xAlarm.QOAssigns.IndexOfLinkID(cAllStylesLinkID) < 0) then begin
            xAllow := true;
            // Wenn "Alle Artikel" definiert ist, evt. vorhandene Artikel l�schen
            if xStyle.LinkID = cAllStylesLinkID then begin
              if (xAlarm.QOAssigns.AssignCount > 0) then begin
                xAllow := (mmMessageDlg(cAskAddAllStyle, mtConfirmation, [mbYes, mbNo], 0, self) = mrYes);
                if xAllow then begin
                  // Zuerst im Tree l�schen ...
                  mAssignTree.DeleteChildren(aDestAlarms[j]);
                  // ... dann im Modell
                  xAlarm.QOAssigns.Clear;
                end;// if xAllow then begin
              end;// if (xAlarm.QOAssigns.AssignCount > 0) then begin
            end;// if xStyle.LinkID = cAllStylesLinkID then begin
  
            // Wenn erlaubt, dann den Artikel in die Liste des entsprechenden Alarms aufnehmen
            if xAllow then begin
              xAlarm.QOAssigns.Add(xStyle);
              xAssigned := true;
  
              // Zuerst auf der DB sichern
              xStyle.ParentID := xAlarm.ID;
              xStyle.SaveDefToDB;
              // Und auch noch dem Tree hinzuf�gen
              AddVSTStructure(mAssignTree, aDestAlarms[j], xStyle, []);
              mAssignTree.FullExpand(aDestAlarms[j]);
            end;// if xAllow then begin
          end else begin
            xSomeStylesNotAssigned := true;
          end;// if xAlarm.QOAssigns.IndexOfLinkID(xStyle.LinkID) < 0 ...
        end;// if assigned(GetNodeObject(aSourceTree, aDestAlarms[j])) then begin
      end;// if assigned(xStyle) then begin
  
      // Wenn der Artikel aus irgend einem Grund nicht zugeordnet werden kann, dann l�schen
      if not(xAssigned) then
        xStyle.Free;
    end;// for j := Low(aDestAlarms) to High(aDestAlarms) do begin
  end;// for i := Low(aSourceStyles) to High(aSourceStyles) do begin

//: ----------------------------------------------
  if xSomeStylesNotAssigned then
    mmMessageDlg(rsNotAllStylesAssigned, mtInformation, [mbOk], 0, nil);
end;// TfrmMainWindow.AddStyles cat:No category

//:-------------------------------------------------------------------
(*: Member:           AlarmOnAlarmSaved
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aAlarm)
 *
 *  Kurzbeschreibung: Leitet das Neuerfassen des Alarms im Tree ein
 *  Beschreibung:     
                      Wenn der Alarm gespeichert wurde. m�ssen auch die untergeordneten 
                      Elemente neu gezeichnet werden (DataItems).
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.AlarmOnAlarmSaved(Sender: TfrmSettingsAlarmFrame; aAlarm: TQOAlarm);
begin
  (* Wenn gespeichert wurde, dann muss das Tree neu gezeichnet werden, da die
     �nderungen unter Umst�nden visuelle Auswirkungen haben (Name, BoolOpAnd) *)
  mSettingsTree.Invalidate;
end;// TfrmMainWindow.AlarmOnAlarmSaved cat:No category

//:-------------------------------------------------------------------
(*: Member:           AnalyzeShowEventInfo
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn Zusatzinfos zur Schicht oder zum Alarm angezeigt werden sollen (Analyze)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.AnalyzeShowEventInfo(Sender: TButtonEditLink);
var
  xNodeObject: TObject;
  xUserInfo: IQOUserInfo;
begin
  xNodeObject := GetNodeObject(Sender.Tree, Sender.Node);
  Sender.Button.RemoveFocusRect;
  if assigned(xNodeObject) then begin
    if xNodeObject.GetInterface(IQOUserInfo, xUserInfo) then begin
      with TfrmEventInfo.Create(nil) do try
        UserInfo := xUserInfo.Info;
        if ShowModal = mrOK then begin
          xUserInfo.Info := UserInfo;
          xUserInfo.SaveInfo;
        end;// if ShowModal = mrOK then begin
      finally
        free;
      end;// with TfrmEventInfo.Create do try
    end;// if xNodeObject.GetInterface(IQOUserInfo, xUserInfo) then begin
  end;// if assigned(xNodeObject) then begin
end;// TfrmMainWindow.AnalyzeShowEventInfo cat:No category

//:-------------------------------------------------------------------
(*: Member:           BeginUpdate
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Update Patern
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.BeginUpdate;
begin
  Inc(FUpdateCnt);
  if FUpdateCnt = 1 then SetUpdating(True);
end;// TfrmMainWindow.BeginUpdate cat:No category

//:-------------------------------------------------------------------
(*: Member:           BuildAnalyzeArchiv
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Baut den Tree mit den Archivierten Alarmen auf
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.BuildAnalyzeArchiv;
var
  xArchiv: TQONodeObject;
  xShiftSet: TQOShiftSet;
  i: Integer;
begin
  xShiftSet := TQOShiftSet.Instance;
  
  if assigned(mAnalyzeArchivNode) then begin
    mAnalyzeTree.DeleteChildren(mAnalyzeArchivNode);
  end else begin
    xArchiv := TQONodeObject.Create;
    xArchiv.QOVisualize.Caption := rsArchiv;
    mAnalyzeArchivNode := AddVSTStructure(mAnalyzeTree, nil, xArchiv, [nsSelected, nsFocussed]);
  end;// if assigned(mAnalyzeArchivNode) then begin
  mAnalyzeTree.HasChildren[mAnalyzeArchivNode] := false;
  
  // Alle automatisch erzeugten Schichten laden
  for i := 0 to xShiftSet.ShiftCount - 1 do begin
    // Alle Archivierten Schichten herausholen
    if (xShiftSet.Shifts[i].ShiftAlarmType = satArchive) then
      AddVSTStructure(mAnalyzeTree, mAnalyzeArchivNode, xShiftSet.Shifts[i],[]);
  end;// for i := 0 to xShiftSet.AlarmCount - 1 do begin
  
  mAnalyzeTree.Expanded[mAnalyzeArchivNode] := true;
end;// TfrmMainWindow.BuildAnalyzeArchiv cat:No category

//:-------------------------------------------------------------------
(*: Member:           BuildAnalyzeAutoGenerated
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Baut den Node mit den automatisch generierten Alarmen neu auf
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.BuildAnalyzeAutoGenerated;
var
  xShiftSet: TQOShiftSet;
  i: Integer;
  xAutoAlarm: TQONodeObject;
begin
  xShiftSet := TQOShiftSet.Instance;
  
  if assigned(mAnalyzeAutoAlarmNode) then begin
    mAnalyzeTree.DeleteChildren(mAnalyzeAutoAlarmNode);
  end else begin
    xAutoAlarm := TQONodeObject.Create;
    xAutoAlarm.QOVisualize.Caption := rsAutoGenerated;
    mAnalyzeAutoAlarmNode := AddVSTStructure(mAnalyzeTree, nil, xAutoAlarm, [nsSelected, nsFocussed]);
  end;// if assigned(mAnalyzeAutoAlarmNode) then begin
  
  // Alle automatisch erzeugten Schichten laden
  for i := 0 to xShiftSet.ShiftCount - 1 do begin
    // automatisch generiert oder im Archiv aber noch im Datenfenster
    if (xShiftSet.Shifts[i].ShiftAlarmType = satAuto)
       or ((xShiftSet.Shifts[i].ShiftAlarmType = satArchive) and (xShiftSet.Shifts[i].ShiftID > xShiftSet.OldestShiftID)) then begin
      AddVSTStructure(mAnalyzeTree, mAnalyzeAutoAlarmNode, xShiftSet.Shifts[i],[]);
    end;// if (xShiftSet.Shifts[i].ShiftAlarmType = satAuto) ...
  end;// for i := 0 to xShiftSet.AlarmCount - 1 do begin
  
  mAnalyzeTree.Expanded[mAnalyzeAutoAlarmNode] := true;
end;// TfrmMainWindow.BuildAnalyzeAutoGenerated cat:No category

//:-------------------------------------------------------------------
(*: Member:           BuildAnalyzeManualGenerated
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Baut den Tree mit dem manuell generierten Alarm auf
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.BuildAnalyzeManualGenerated;
var
  xManualAlarm: TQONodeObject;
  i: Integer;
  xShiftSet: TQOShiftSet;
begin
  xShiftSet := TQOShiftSet.Instance;
  
  if assigned(mAnalyzeManualAlarmNode) then begin
    mAnalyzeTree.DeleteChildren(mAnalyzeManualAlarmNode);
  end else begin
    xManualAlarm := TQONodeObject.Create;
    xManualAlarm.QOVisualize.Caption := rsManualGenerated;
    mAnalyzeManualAlarmNode := AddVSTStructure(mAnalyzeTree, nil, xManualAlarm, [nsSelected, nsFocussed]);
  end;// if assigned(mAnalyzeManualAlarmNode) then begin
  mAnalyzeTree.HasChildren[mAnalyzeManualAlarmNode] := false;
  
  // Alle manuell erzeugten Schichten laden
  for i := 0 to xShiftSet.ShiftCount - 1 do begin
    if xShiftSet.Shifts[i].ShiftAlarmType = satManual then
      AddVSTStructure(mAnalyzeTree, mAnalyzeManualAlarmNode, xShiftSet.Shifts[i],[]);
  end;// for i := 0 to xShiftSet.AlarmCount - 1 do begin
  
  mAnalyzeTree.Expanded[mAnalyzeManualAlarmNode] := true;
end;// TfrmMainWindow.BuildAnalyzeManualGenerated cat:No category

//:-------------------------------------------------------------------
(*: Member:           BuildAnalyzeTreeFromShiftSet
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Baut den Tree mit den Schichten auf
 *  Beschreibung:     
                      Die weitere Hirarchie wird erst dann geladen, wenn der Alarm
                      betrachtet werden soll.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.BuildAnalyzeTreeFromShiftSet;
begin
  BuildAnalyzeAutoGenerated;
  BuildAnalyzeManualGenerated;
  BuildAnalyzeArchiv;
  
  mAnalyzeTree.ScrollIntoView(mAnalyzeAutoAlarmNode, false);
  mAnalyzeTree.FocusedNode := mAnalyzeAutoAlarmNode;
end;// TfrmMainWindow.BuildAnalyzeTreeFromShiftSet cat:No category

//:-------------------------------------------------------------------
(*: Member:           BuildAssignTreeAlarm
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (aAlarmNode, aAlarm)
 *
 *  Kurzbeschreibung: Erzeugt die DataItems eines Alarms im Assign Tree
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.BuildAssignTreeAlarm(aAlarmNode: PVirtualNode; aAlarm: TQOAlarm);
var
  i: Integer;
begin
  for i := 0 to aAlarm.QOAssigns.AssignCount - 1 do
    BuildAssignTreeAssign(AddVSTStructure(mAssignTree, aAlarmNode, aAlarm.QOAssigns[i], [nsExpanded]), aAlarm.QOAssigns[i]);
end;// TfrmMainWindow.BuildAssignTreeAlarm cat:Tree

//:-------------------------------------------------------------------
(*: Member:           BuildAssignTreeAssign
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (aAssignNode, aAssign)
 *
 *  Kurzbeschreibung: Erzeugt die DataItems eines Artikels im Assign Tree
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.BuildAssignTreeAssign(aAssignNode: PVirtualNode; aAssign: TQOAssign);
var
  i: Integer;
begin
  for i := 0 to aAssign.QOAssigns.AssignCount - 1 do
    mAssignTree.FocusedNode := AddVSTStructure(mAssignTree, aAssignNode, aAssign.QOAssigns[i], [nsExpanded]);
end;// TfrmMainWindow.BuildAssignTreeAssign cat:Tree

//:-------------------------------------------------------------------
(*: Member:           BuildAssignTreeFromAlarms
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (aAssignRootStyle)
 *
 *  Kurzbeschreibung: Zeigt die Liste mit den Alarmen oder Artikel an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.BuildAssignTreeFromAlarms(aAssignRootStyle: TAssignRootStyle = rsAlarm);
var
  xQOAlarms: TQOAlarms;
  i: Integer;
  xSelected: TObject;
begin
  xQOAlarms := TQOAlarms.Instance;

//: ----------------------------------------------
  GetSelectedNode(mAssignTree, 0, xSelected);
  
  mAssignTree.BeginUpdate;
  try
    mAssignTree.Clear;
  
    case aAssignRootStyle of
      rsAlarm: begin
        // Alle Alarme mit ihren DataItems darstellen
        for i := 0 to xQOAlarms.AlarmCount - 1 do
          BuildAssignTreeAlarm(AddVSTStructure(mAssignTree, nil, xQOAlarms[i]), xQOAlarms[i]);
      end;// rsAlarm: begin
      rsStyle: begin
        for i := 0 to mStyles.AssignCount - 1 do
          BuildAssignTreeStyle(AddVSTStructure(mAssignTree, nil, mStyles[i]), mStyles[i]);
      end;// rsStyle: begin
    end;// case aAssignRootStyle of
  
    // vorher selektierten Knoten wieder selektieren (auf Ebene 0)
    SelectNodeFromObject(mAssignTree, xSelected, 0);
  finally
    mAssignTree.EndUpdate;
  end;// try finally
end;// TfrmMainWindow.BuildAssignTreeFromAlarms cat:Tree

//:-------------------------------------------------------------------
(*: Member:           BuildAssignTreeStyle
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (aStyleNode, aStyle)
 *
 *  Kurzbeschreibung: Erzeugt die DataItems eines Alarms im Assign Tree
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.BuildAssignTreeStyle(aStyleNode: PVirtualNode; aStyle: TQOAssign);
var
  i: Integer;
  xQOAlarms: TQOAlarms;
begin
  xQOAlarms := TQOAlarms.Instance;
  
  for i := 0 to xQOAlarms.AlarmCount - 1 do begin
    if xQOAlarms[i].QOAssigns.IndexOfLinkID(aStyle.LinkID) >= 0 then
      AddVstStructure(mAssignTree, aStyleNode, xQOAlarms[i], [nsExpanded]);
  end;// for i := 0 to xQOAlarms.AlarmCount - 1 do begin
end;// TfrmMainWindow.BuildAssignTreeStyle cat:Tree

//:-------------------------------------------------------------------
(*: Member:           BuildSettingsTreeAlarm
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (aAlarmNode, aAlarm)
 *
 *  Kurzbeschreibung: Erzeugt die DataItems eines Alarms im Settings Tree
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.BuildSettingsTreeAlarm(aAlarmNode: PVirtualNode; aAlarm: TQOAlarm);
var
  i: Integer;
begin
  mSettingsTree.BeginUpdate;
  try
    for i := 0 to aAlarm.QOItems.ItemCount - 1 do begin
      if aAlarm.QOItems[i].DeleteState = dsValid then begin
        // Nur die Aktiven Alarme aufklappen
        AddVSTStructure(mSettingsTree, aAlarmNode, aAlarm.QOItems[i], []);
      end;// if aAlarm.QOItems[i].DeleteState = dsValid then begin
    end;// for i := 0 to aAlarm.QOItems.ItemCount - 1 do begin
  
    // Knoten aufklappen, wenn der Alarm aktiviert ist
    if aAlarm.Active then
      mSettingsTree.FullExpand(aAlarmNode);
  finally
    mSettingsTree.EndUpdate;
  end;// try finally
end;// TfrmMainWindow.BuildSettingsTreeAlarm cat:Tree

//:-------------------------------------------------------------------
(*: Member:           BuildSettingsTreeFromAlarms
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zeigt die Liste mit den Alarmen an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.BuildSettingsTreeFromAlarms;
var
  xQOAlarms: TQOAlarms;
  i: Integer;
  xSelected: TObject;
begin
  xQOAlarms := TQOAlarms.Instance;

//: ----------------------------------------------
  // Aktuell selektierten Index sichern
  GetSelectedNode(mSettingsTree, -1, xSelected);
  
  mSettingsTree.Clear;
  
  // Alle Alarme mit ihren DataItems darstellen
  for i := 0 to xQOAlarms.AlarmCount - 1 do
    BuildSettingsTreeAlarm(AddVSTStructure(mSettingsTree, nil, xQOAlarms[i], []), xQOAlarms[i]);
  
  // Alarm wieder selektieren
  // mSettingsTree.Selected[SetFocusToObject(mSettingsTree, xSelected)] := true;
  if not(SelectNodeFromObject(mSettingsTree, xSelected)) then
    SetFocusToNode(mSettingsTree, mSettingsTree.GetFirst, false);
  
  // ...und neu Anzeigen
  if assigned(mSettingsAlarmFrame) then
    mSettingsAlarmFrame.ShowAlarm(GetNodeObject(mSettingsTree, mSettingsTree.FocusedNode));
end;// TfrmMainWindow.BuildSettingsTreeFromAlarms cat:Tree

//:-------------------------------------------------------------------
(*: Member:           CreateSettingsAlarmFrame
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Erzeugt den SettingsAlarmFrame
 *  Beschreibung:     
                      Der Frame wird an zwei Orten gebraucht (Create, SettingsTreeFocusChanged)
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.CreateSettingsAlarmFrame;
begin
  if not(assigned(mSettingsAlarmFrame)) then begin
    mSettingsAlarmFrame := TfrmSettingsAlarmFrame.Create(self);
    with mSettingsAlarmFrame do begin
      // Die Einstellungen zum Alarm werden auf der rechten Seite angezeigt
      Parent         := mRightPanel;
      Align          := alClient;
      CurrentUser    := mSecurityControl.CurrentMMUser;
      OnOnAlarmSaved := AlarmOnAlarmSaved;
      OnAddItems     := OnSettingsAddItems;
    end;// with mAlarmSettingsFrame do begin
  end;// if not(assigned(mAlarmSettingsFrame)) then begin
end;// TfrmMainWindow.CreateSettingsAlarmFrame cat:No category

//:-------------------------------------------------------------------
(*: Member:           DeleteAlarm
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Ereignis Handler der aufgerufen wird, wenn ein Alarm aus dem Tree entfernt wird
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.DeleteAlarm(Sender: TObject);
var
  xNode: PVirtualNode;
  xPreviousNode: PVirtualNode;
begin
  // Beim Programmende kann der Tree bereits nil sein
  if assigned(mAnalyzeTree) then begin
    xNode := GetNodeFromObject(mAnalyzeTree, Sender, nil);
    while assigned(xNode) do begin
      // Neuer Startpunkt f�r die weitere Suche
      xPreviousNode := mAnalyzeTree.GetPrevious(xNode);
      // Knoten entfernen
      mAnalyzeTree.DeleteNode(xNode);
      // Nach weiterern "manifestationen" des entsprechenden Objektes suchen
      xNode := GetNodeFromObject(mAnalyzeTree, Sender, xPreviousNode);
    end;// while assigned(xNode) do begin
  end;// if assigned(mAnalyzeTree) then begin
end;// TfrmMainWindow.DeleteAlarm cat:No category

//:-------------------------------------------------------------------
(*: Member:           EnableDisableAction
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (aAction, aEnabled, aVisible)
 *
 *  Kurzbeschreibung: Setzt eine Action unter Ber�cksichtigung der Security Settings
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.EnableDisableAction(aAction: TAction; aEnabled: boolean; aVisible: boolean = true);
begin
  aAction.Enabled := aEnabled and mSecurityControl.CanEnabled(aAction);
  aAction.Visible := aVisible and mSecurityControl.CanVisible(aAction);
end;// TfrmMainWindow.EnableDisableAction cat:No category

//:-------------------------------------------------------------------
(*: Member:           EndUpdate
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Update Patern
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.EndUpdate;
begin
  Dec(FUpdateCnt);
  if FUpdateCnt = 0 then SetUpdating(False);
end;// TfrmMainWindow.EndUpdate cat:No category

//:-------------------------------------------------------------------
(*: Member:           Execute
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (aParameter)
 *
 *  Kurzbeschreibung: COM Implementierung
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.Execute(const aParameter: WideString);
begin
  // Vorbereitet auf den Aufruf vom LabReort
end;// TfrmMainWindow.Execute cat:No category

//:-------------------------------------------------------------------
(*: Member:           FormClose
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Action)
 *
 *  Kurzbeschreibung: Programmende
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  TQOShiftSet.Instance.UnRegisterObserver(mObserver);
  (*
  if assigned(mSettingsAlarmFrame)then
    mSettingsAlarmFrame.SaveSettings(false, false);
  
  if assigned(mSettingsItemFrame)then
    mSettingsItemFrame.SaveSettings(true, false);*)
  
  Action := caFree;
end;// TfrmMainWindow.FormClose cat:No category

//:-------------------------------------------------------------------
(*: Member:           FormCloseQuery
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender, CanClose)
 *
 *  Kurzbeschreibung: Programmende
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if assigned(mSettingsAlarmFrame)then
    CanClose := not(mSettingsAlarmFrame.SaveSettings(true, true) = mrCancel);
  
  if assigned(mSettingsItemFrame)then
    CanClose := not(mSettingsItemFrame.SaveSettings(true, true) = mrCancel);
  
  if CanClose then
    TQOShiftSet.Instance.UnRegisterObserver(mObserver);
end;// TfrmMainWindow.FormCloseQuery cat:No category

//:-------------------------------------------------------------------
(*: Member:           FormCreate
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Initialieren
 *  Beschreibung:     
                      Diese Funktion wird erst aufgerufen, wenn alle Komponenten
                      erzeugt sind (im Gegensatz zum Konstruktor). Deshalb k�nne hier 
                      Anpassungen an den Visuellen Komponenten angebracht werden.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.FormCreate(Sender: TObject);
begin
  inherited;

//: ----------------------------------------------
  // Init;
end;// TfrmMainWindow.FormCreate cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAnalyzeSelectedShifts
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt eine Kommaseparierte Liste mit den selektierten Schichten zur�ck
 *  Beschreibung:     
                      Die Schichten werden anhand des Datums und der Zeit ermittelt.
 --------------------------------------------------------------------*)
function TfrmMainWindow.GetAnalyzeSelectedShifts: String;
var
  i: Integer;
  xNodeArray: TNodeArray;
begin
  result := '';
  
  // Alle Selektierten Items abholen
  xNodeArray := mAnalyzeTree.GetSortedSelection(false);
  for i := 0 to mAnalyzeTree.SelectedCount - 1 do begin
    if mAnalyzeTree.GetNodeLevel(xNodeArray[i]) = 1 then
      result := result + ',"' + FloatToStr(TQOShiftView(GetNodeObject(mAnalyzeTree, xNodeArray[i])).AlarmDate) + '"';
  end;// for i := 0 to mAnalyzeTree.SelectedCount - 1 do begin
  
  if result > '' then
    system.delete(result, 1, 1);
end;// TfrmMainWindow.GetAnalyzeSelectedShifts cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMouseButtonPressed
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (aButton)
 *
 *  Kurzbeschreibung: Pr�ft ob der gegebene Mausbutton gedr�ckt ist
 *  Beschreibung:     
                      Da GetAsyncKeyState nicht pr�ft ob die Mausbuttons vertauscht sind,
                      muss dies �ber das API erledigt werden.
 --------------------------------------------------------------------*)
function TfrmMainWindow.GetMouseButtonPressed(aButton: TMouseButton): Boolean;
var
  xButton: Integer;
begin
  xButton := 0;
  
  // Je nach Maustaste unterscheiden
  case aButton of
    mbLeft: begin
      if GetSystemMetrics(SM_SWAPBUTTON) <> 0 then
        xButton := VK_RBUTTON
      else
        xButton := VK_LBUTTON;
    end;// mbLeft: begin
  
    mbMiddle: begin
      xButton := VK_MBUTTON;
    end;// mbMiddle: begin
  
    mbRight: begin
      if GetSystemMetrics(SM_SWAPBUTTON) <> 0 then
        xButton := VK_LBUTTON
      else
        xButton := VK_RBUTTON;
    end;// mbRight: begin
  end;// case aButton of
  
  // HSB gesetzt bedeutet die Taste ist gedr�ckt
  result := (GetAsyncKeyState(xButton) and ($8000) > 0);
end;// TfrmMainWindow.GetMouseButtonPressed cat:No category

//:-------------------------------------------------------------------
(*: Member:           Init
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Initialisiert die Anwendung
 *  Beschreibung:     
                      Die meisten Funktionen sind im Konstruktor untergebracht
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.Init;
var
  xFoundUseCase: Boolean;
  
  const
    cSwitchSeparator = [' ','-','/'];
  
begin
  xFoundUseCase := false;
  
  // Comand Line Switches
  if FindCmdLineSwitch(cCmdShowSettings, cSwitchSeparator, true) then
    xFoundUseCase := SetActiveUseCase(ucSettings);
  
  // Zeigt das Assign Tab an
  if FindCmdLineSwitch(cCmdShowAssign, cSwitchSeparator, true) then
    xFoundUseCase := SetActiveUseCase(ucAssign);
  
  // Zeigt das Analyze Tab an
  if FindCmdLineSwitch(cCmdShowAnalyze, cSwitchSeparator, true) then
    xFoundUseCase := SetActiveUseCase(ucAnalyze);
  
  // Default, wenn kein Parameter gesetzt ist
  if not(xFoundUseCase) then
    SetActiveUseCase(ucSettings);
  
  // Erm�glicht das neu Laden der Alarme von der Datenbank (Analyze)
  EnableDisableAction(acRefreshData, FindCmdLineSwitch(cCmdRefreshVisible, cSwitchSeparator, true), FindCmdLineSwitch(cCmdRefreshVisible, cSwitchSeparator, true));
end;// TfrmMainWindow.Init cat:No category

//:-------------------------------------------------------------------
(*: Member:           IsUpdating
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Update Patern
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmMainWindow.IsUpdating: Boolean;
begin
  Result := FUpdateCnt > 0;
end;// TfrmMainWindow.IsUpdating cat:No category

//:-------------------------------------------------------------------
(*: Member:           mAnalyzeTreeAfterCellPaint
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender, TargetCanvas, Node, Column, CellRect)
 *
 *  Kurzbeschreibung: Malt die Fixe Spalte mit dem Datenzeiger
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAnalyzeTreeAfterCellPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode;
  Column: TColumnIndex; CellRect: TRect);
begin
  if Column = cAnalyzePointerCol then begin
    with TargetCanvas do begin
      // Simuliert die Anzeige einer FixedRow wie in TStringGrid
      if toShowVertGridLines in mAnalyzeTree.TreeOptions.PaintOptions then
        Inc(CellRect.Right);
      if toShowHorzGridLines in mAnalyzeTree.TreeOptions.PaintOptions then
        Inc(CellRect.Bottom);
      // Zeichnet eine 3D Fl�che
      DrawEdge(Handle, CellRect, BDR_RAISEDINNER, BF_RECT or BF_MIDDLE);
      // Je nach dem auch noch den DS-Pointer zeichnen
      if Node = Sender.FocusedNode then
        mTreeImages.Draw(TargetCanvas, CellRect.Left, CellRect.Top, 27);
    end;// with TargetCanvas do begin
  end;// if Column = cAnalyzePointerCol then begin
end;// TfrmMainWindow.mAnalyzeTreeAfterCellPaint cat:No category

//:-------------------------------------------------------------------
(*: Member:           mAnalyzeTreeChange
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Node)
 *
 *  Kurzbeschreibung: -
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAnalyzeTreeChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  if Sender.GetNodeLevel(Sender.FocusedNode) <= 1 then
    mAnalyzeOverviewFrame.SetHighlightSerie(aftShift, GetAnalyzeSelectedShifts, cSerieHighlightColor);

//: ----------------------------------------------
  if assigned(node) then
    Sender.EditNode(Node, cAnalyzeInfoCol);
end;// TfrmMainWindow.mAnalyzeTreeChange cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mAnalyzeTreeCreateEditor
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Column, EditLink)
 *
 *  Kurzbeschreibung: Erzeugt den Button f�r die Infos
 *  Beschreibung:     
                      Im Event 'OnEditing' wird festgelegt, ob ein Steuerelement angezeigt werden darf.
                      Danach wird der Event 'OnCreateEditor' aufgeruffen. Hier erst wird dann das Steuerelement 
                      erzeugt. Da ein Interface auf das Objekt zur�ckgegeben wird, muss das Steuerelement nicht
                      zerst�rt werden (Referenzz�hlung).
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAnalyzeTreeCreateEditor(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  out EditLink: IVTEditLink);
var
  xButtonEditLink: TButtonEditLink;
  xBitmap: TBitmap;
  xNodeObject: TObject;
  xUserInfo: IQOUserInfo;
begin
  // Button erzeugen
  xButtonEditLink := TButtonEditLink.Create;
  xButtonEditLink.OnShowUserInfo := AnalyzeShowEventInfo;
  
  (* Anhand des verkn�pften Objektes herausfinden ob bereits Zusatzinfos
     eingegeben wurden. Ist dies der Fall, dann soll das Info-Bitmap auf
     dem Button angezeigt werden. *)
  xNodeObject := GetNodeObject(Sender, Node);
  if assigned(xNodeObject) then begin
    (* Im Interface 'IQOUserInfo' werden die Zusdatzinfos abgelegt.
       Nur einzelne Klassen implementieren dieses Interface (Schicht und Alarm) *)
    if xNodeObject.GetInterface(IQOUserInfo, xUserInfo) then begin
      // Wenn Zusatzinfos vorhanden sind, dann wird das Bitmap auf dem Button angezeigt
      if xUserInfo.Info > '' then begin
        xBitmap := TBitmap.create;
        try
          xBitmap.Width  := mTreeImages.Width;
          xBitmap.Height := mTreeImages.Height;
          mTreeImages.GetBitmap(cInfoBitmap, xBitmap);
  
          xButtonEditLink.Glyph := xBitmap;
        finally
          xBitmap.Free;
        end;// try finally
      end;// if xUserInfo.Info > '' then begin
    end;// if xNodeObject.GetInterface(IQOUserInfo, xUserInfo) then begin
  end;// if assigned(xNodeObject) then begin
  
  // Interface weitergeben
  EditLink := xButtonEditLink as IVTEditLink;
end;// TfrmMainWindow.mAnalyzeTreeCreateEditor cat:No category

//:-------------------------------------------------------------------
(*: Member:           mAnalyzeTreeEditing
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Column, Allowed)
 *
 *  Kurzbeschreibung: Bestimmt ob der Button angezeigt werden soll
 *  Beschreibung:     
                      Wird 'Allow' auf true gesetzt, ruft das TreeView den Event 'OnCreateEditor' auf um das
                      Steuerelement
                      zu erzeugen. Mit 'Allow' und 'Column' kann gesteuert werden, ob in der entsprechenden Spalte ein
                      Steuerelement angezeigt werden darf.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAnalyzeTreeEditing(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; var
  Allowed: Boolean);
var
  xQOInstance: TObject;
  xVisualizeInterface: IQOVisualize;
begin
  Allowed := false;
  
  // Nur in der hintersten Spalte kann der Button dargestellt werden
  if (Column = cAnalyzeInfoCol) then begin
    // �ber das Interface abfragen ob der Editor angezeigt werden darf
    xQOInstance := GetNodeObject(Sender, Node);
  
    if assigned(xQOInstance) then begin
      // Jetzt das Interface abfragen
      if xQOInstance.GetInterface(IQOVisualize, xVisualizeInterface) then begin
        // W�hrend der Selektion ist kein Editor erlaubt
        Allowed := (xVisualizeInterface.EditorEnabled) and (not(tsDrawSelecting in Sender.TreeStates));
      end;// if xQOInstance.GetInterface(IQOVisualize, xVisualizeInterface) then begin
    end;// if assigned(xQOInstance) then begin
  end else begin
    Sender.EditNode(Node, cAnalyzeInfoCol);
  end;// if (Column = cAnalyzeInfoCol) then begin
end;// TfrmMainWindow.mAnalyzeTreeEditing cat:No category

//:-------------------------------------------------------------------
(*: Member:           mAnalyzeTreeExpanded
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node)
 *
 *  Kurzbeschreibung: Setzt den Mauszeiger zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAnalyzeTreeExpanded(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  if not(mExpandByAction) then
    Screen.Cursor := crDefault;
end;// TfrmMainWindow.mAnalyzeTreeExpanded cat:No category

//:-------------------------------------------------------------------
(*: Member:           mAnalyzeTreeExpanding
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Allowed)
 *
 *  Kurzbeschreibung: Setzt den Mauszeiger auf "Besch�ftigt" 
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAnalyzeTreeExpanding(Sender: TBaseVirtualTree; Node: PVirtualNode; var Allowed: Boolean);
begin
  if not(mExpandByAction) then begin
    if Sender.HasChildren[Node] then
      Screen.Cursor := crHourglass;
  end;// if not(mExpandByAction) then begin
end;// TfrmMainWindow.mAnalyzeTreeExpanding cat:No category

//:-------------------------------------------------------------------
(*: Member:           mAnalyzeTreeFocusChanged
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Node, Column)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn ein neues Element ausgew�hlt wird
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAnalyzeTreeFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);
var
  xQOView: TQOAlarmView;
  xParentNode: PVirtualNode;
  xShiftAlarmType: TShiftAlarmType;
begin
  // Es k�nnen nur ganze Alarme gedruckt werden
  EnableDisableAction(acPrintAnalyze, (Sender.GetNodeLevel(Node) = 1) or (Sender.GetNodeLevel(Node) > 2));
  // Der Labreport kann nur f�r Artikel oder Maschinen aufgerufen werden
  EnableDisableAction(acCallLabReport, (Sender.GetNodeLevel(Node) > 2));
  // Expand Collapse nicht auf der obersten Ebene (unter Umst�nden Speicher und Rechenintensiv)
  EnableDisableAction(acExpand, (Sender.GetNodeLevel(Node) > 0));
  // Expand Collapse nicht auf der obersten Ebene (unter Umst�nden Speicher und Rechenintensiv)
  EnableDisableAction(acCollapse, (Sender.GetNodeLevel(Node) > 0));
  
  // Die Archiv Funktionen erst mal ausschalten
  EnableDisableAction(acSaveData, false);
  EnableDisableAction(acDeleteFromArchive, false);

//: ----------------------------------------------
  (* Alle verwendeten Frames ausblenden. Der gew�nschte Frame wird
     dann sp�ter wieder eingeblendet. *)
  if assigned(mAnalyzeOverviewFrame) then
    mAnalyzeOverviewFrame.Visible := false;
  
  if assigned(mAnalyzeAlarmFrame) then
    mAnalyzeAlarmFrame.Visible := false;
  
  if assigned(mAnalyzeChartFrame) then
    mAnalyzeChartFrame.Visible := false;
  
  // Je nach angecklicktem Knoten wird auf der rechten Seite ein Frame angezeigt
  case mAnalyzeTree.GetNodeLevel(Node) of

//: ----------------------------------------------
    // �bersicht, Schicht
    0, 1: begin
      (* Den Frame vor der ersten Verwendung erstmal erzeugen.
         �bersicht und Schichten verwenden den selben Frame *)
      if not(assigned(mAnalyzeOverviewFrame)) then begin
        mAnalyzeOverviewFrame := TfrmAnalyzeOverviewFrame.Create(self);
        mAnalyzeOverviewFrame.Parent := mRightPanel;
        mAnalyzeOverviewFrame.Align := alClient;
        // Wird ausgel�st, wenn im Histogramm auf eine schicht geklickt wird
        mAnalyzeOverviewFrame.OnOverviewChartClick := OnOverviewChartClick;
      end;// if not(assigned(frmAnalyzeOverview)) then begin
      // �bersicht anzeigen
      mAnalyzeOverviewFrame.ShowOverview;
      mAnalyzeOverviewFrame.Visible := true;
  
      if mAnalyzeTree.GetNodeLevel(Node) = 1 then begin
  
        if GetNodeObject(Sender, Node) is TQOShiftView then begin
          // Schichttyp bestimmen
          xShiftAlarmType := TQOShiftView(GetNodeObject(Sender, Node)).ShiftAlarmType;
          // Save wieder einschalten, wenn es sich um eine automatisch generierte Schicht handelt.
          EnableDisableAction(acSaveData, xShiftAlarmType = satAuto);
          // L�schen wieder einschalten, wenn es sich um eine archivierte Schicht handelt.
          EnableDisableAction(acDeleteFromArchive, xShiftAlarmType = satArchive);
        end;// if GetNodeObject(Sender, Node) is TQOShiftView then begin
  
        mAnalyzeOverviewFrame.SetHighlightSerie(aftShift, GetAnalyzeSelectedShifts, cSerieHighlightColor);
      end else begin
        mAnalyzeOverviewFrame.SetHighlightSerie(aftNone, '');
      end;// if mAnalyzeTree.GetNodeLevel(Node) = 1 then begin
    end;// 0, 1:

//: ----------------------------------------------
    // Alarm
    2: begin
      // Den Frame vor der ersten verwendung erstmal erzeugen
      if (GetNodeObject(Sender, Node) is TQOAlarmView) then begin
        xQOView := TQOAlarmView(GetNodeObject(Sender, Node));
        if not(assigned(mAnalyzeAlarmFrame)) then begin
          mAnalyzeAlarmFrame := TfrmAnalyzeAlarmFrame.Create(self);
          mAnalyzeAlarmFrame.Parent := mRightPanel;
          mAnalyzeAlarmFrame.Align := alClient;
          // Wird ausgel�st, wenn im Histogramm auf eine schicht geklickt wird
          mAnalyzeAlarmFrame.OnAlarmChartClick := OnAlarmviewChartClick;
        end;// if not(assigned(frmAnalyzeOverview)) then begin
        // Alarm �bersicht anzeigen
        mAnalyzeAlarmFrame.ShowAlarm(xQOView);
        mAnalyzeAlarmFrame.Visible := true;
      end;// if (GetNodeObject(Sender, Node) is TQOAlarmView) then begin
    end;// 2:

//: ----------------------------------------------
    // Artikel Maschine
    3,4: begin
      // Sicherstellen, dass ein Assignobjekt zum Node geh�rt
      if (GetNodeObject(Sender, Node) is TQOAssignNodeObject) then begin
         if not(assigned(mAnalyzeChartFrame)) then begin
          mAnalyzeChartFrame := TfrmAnalyzeChartFrame.Create(self);
          mAnalyzeChartFrame.Parent := mRightPanel;
          mAnalyzeChartFrame.Align := alClient;
          mAnalyzeChartFrame.MethodImages := mMethodImages;
        end;// if not(assigned(frmAnalyzeOverview)) then begin
        if mAnalyzeTree.GetNodeLevel(Node) = 3 then begin
          mAnalyzeChartFrame.ShowStyle(TQOAlarmView(GetNodeObject(Sender, Sender.NodeParent[Node])), TQOAssignNodeObject(GetNodeObject(Sender, Node)));
        end else begin
          xParentNode := Sender.NodeParent[Node];
          mAnalyzeChartFrame.ShowMachine(TQOAlarmView(GetNodeObject(Sender, Sender.NodeParent[xParentNode])),
                                         TQOAssignNodeObject(GetNodeObject(Sender, xParentNode)),
                                         TQOAssignNodeObject(GetNodeObject(Sender, Node)));
        end;// if mAnalyzeTree.GetNodeLevel(Node) = 3 then begin
        mAnalyzeChartFrame.Visible := true;
      end;// if (GetNodeObject(Sender, Node) is TQOAlarmView) then begin
    end;// 3, 4:

//: ----------------------------------------------
  end;// case mAnalyzeTree.GetNodeLevel(Node) of}
end;// TfrmMainWindow.mAnalyzeTreeFocusChanged cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mAnalyzeTreeFreeNode
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Node)
 *
 *  Kurzbeschreibung: Freigeben der Objekte die nicht in einer Liste verwaltet werden
 *  Beschreibung:     
                      Alle andern Objekte (Schichten, Alarme) werden von der Liste verwaltet.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAnalyzeTreeFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  xData: TObject;
begin
  //if Sender.GetNodeLevel(Node) = 0 then begin
  xData := GetNodeObject(Sender, Node);
  if assigned(xData) then begin
    if (xData is TQONodeObject) then begin
      xData.Free;
      PTreeData(Sender.GetNodeData(Node))^.Item := nil;
    end;// if (xData is TQONodeObject) then begin
  end;// if assigned(xData) then begin
  //end;// if Sender.GetNodeLevel(Sender, Node) = 0 then begin
end;// TfrmMainWindow.mAnalyzeTreeFreeNode cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mAnalyzeTreeGetImageIndex
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Node, Kind, Column, Ghosted, ImageIndex)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn das darzustellende Icon f�r den Node abgefragt wird
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAnalyzeTreeGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind;
  Column: TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
var
  xQOInstance: TObject;
  xVisualizeInterface: IQOVisualize;
  xUserInfo: IQOUserInfo;
begin
  // Den ImageIndex �ber das Interface abfragen
  xQOInstance := GetNodeObject(Sender, Node);
  
  case Column of
    0: begin
        if assigned(xQOInstance) then begin
          // Jetzt das Interface abfragen
          if xQOInstance.GetInterface(IQOVisualize, xVisualizeInterface) then begin
            if Kind <> ikOverlay then begin
              case Column of
                0: begin
                  // ImageIndex wird vom Interface geliefert
                  ImageIndex := xVisualizeInterface.ImageIndex[ikAnalyzeNormal];
                end;// 0: begin
              end;// case Column of
            end;// if Kind <> ikOverlay then begin
          end;// if xQOInstance.GetInterface(IQOVisualize, xVisualizeInterface) then begin
        end;// if assigned(xQOInstance) then begin
    end;// 0:
    2: begin
      if assigned(xQOInstance) and (Kind <> ikOverlay) and (Node <> Sender.FocusedNode) then begin
        if xQOInstance.GetInterface(IQOUserInfo, xUserInfo) then begin
          if xUserInfo.Info > '' then
            ImageIndex := cInfoBitmap;
        end;// if xNodeObject.GetInterface(IQOUserInfo, xUserInfo) then begin
      end;// if assigned(xQOInstance) and (Kind <> ikOverlay) and (Node <> Sender.FocusedNode) then begin
    end;// 2:
  end;// case Column of
end;// TfrmMainWindow.mAnalyzeTreeGetImageIndex cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mAnalyzeTreeGetText
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Node, Column, TextType, CellText)
 *
 *  Kurzbeschreibung: Wird aufgerufen um den Text des Nodes zu zeichnen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAnalyzeTreeGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType; var CellText: WideString);
var
  xVisualizeInterface: IQOVisualize;
  xQOInstance: TObject;
begin
  xQOInstance := nil;
  
  try
    // Zuerst das Objekt holen
    if Column >= 0 then
      xQOInstance := GetNodeObject(Sender, Node);
  
    case Column of
      0: begin
        if assigned(xQOInstance) then begin
          // Jetzt das Interface abfragen
          if xQOInstance.GetInterface(IQOVisualize, xVisualizeInterface) then
            // Anzeigename wird vom Interface geliefert
            CellText := xVisualizeInterface.Caption;
        end;// if assigned(xQOInstance) then begin
      end;// 0: begin
    else
      CellText := '';
    end;// case Column of
  except
    on e: ENoDataObject do
      ShowMessage(e.Message);
  end;// try except

//: ----------------------------------------------
  // Im obersten Level die Anzahl der untergeordneten Elemente anzeigen
  if (Sender.GetNodeLevel(Node) = 0) and (CellText > '') then
    CellText := CellText + Format(' (#%d)',[Sender.ChildCount[Node]]);
end;// TfrmMainWindow.mAnalyzeTreeGetText cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mAnalyzeTreeInitChildren
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Node, ChildCount)
 *
 *  Kurzbeschreibung: Wird aufgerufen um die Childs des Knoten zu initialisieren
 *  Beschreibung:     
                      Wenn ein Knoten expandiert wird, m�ssen die Kinder zuerst initialisiert werden,
                      da sonst der Text nicht angezeigt werden kann. 
                      Dies ist bei der Analyse notwendig, da nicht alle Daten bereits am Anfang
                      geladen werden um Zeit zu sparen, da im Normlafall kaum alle Alarme gesichtet
                      werden.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAnalyzeTreeInitChildren(Sender: TBaseVirtualTree; Node: PVirtualNode; var ChildCount:
  Cardinal);
var
  xQOInstance: TObject;
  i: Integer;
  xAssignNodeObject: TQOAssignNodeObject;
  xMachineList: TStringList;
begin
  xQOInstance := GetNodeObject(Sender, Node);
  if assigned(xQOInstance) then begin
  
    if (xQOInstance is TQOShiftView) then begin
      if ChildCount = 0 then begin
        for i := 0 to TQOShiftView(xQOInstance).AlarmViewCount - 1 do
          AddVSTStructure(Sender, Node, TQOShiftView(xQOInstance).AlarmViews[i], []);
        ChildCount := Sender.ChildCount[Node];
      end;// if ChildCount = 0 then begin
    end;// if (xQOInstance is TQOShiftView) then begin
  
  
    if (xQOInstance is TQOAlarmView) then begin
      if ChildCount = 0 then begin
        for i := 0 to TQOAlarmView(xQOInstance).AllStyles.Count - 1 do begin
          xAssignNodeObject := TQOAssignNodeObject.Create;
          xAssignNodeObject.AlarmView := TQOAlarmView(xQOInstance);
          xAssignNodeObject.NodeObjectType := otStyle;
          xAssignNodeObject.QOVisualize.Caption := xAssignNodeObject.AlarmView.AllStyles[i];
          xAssignNodeObject.LinkID := Integer(xAssignNodeObject.AlarmView.AllStyles.Objects[i]);
          xAssignNodeObject.BuildProdIDList;
          AddVSTStructure(Sender, Node, xAssignNodeObject, []);
        end;// for i := 0 to TQOAlarmView(xQOInstance).StyleCount - 1 do begin
        ChildCount := Sender.ChildCount[Node];
      end;// if ChildCount = 0 then begin
    end;// if (xQOInstance is TQOShiftView) then begin
  
    if (xQOInstance is TQOAssignNodeObject) then begin
      if ChildCount = 0 then begin
        xMachineList := TStringList.Create;
        try
          xAssignNodeObject := TQOAssignNodeObject(xQOInstance);
          xAssignNodeObject.AlarmView.GetMachinesFromStyle(xMachineList, xAssignNodeObject.QOVisualize.Caption);
          for i := 0 to xMachineList.Count - 1 do begin
            xAssignNodeObject := TQOAssignNodeObject.Create;
            xAssignNodeObject.AlarmView := TQOAssignNodeObject(xQOInstance).AlarmView;
            xAssignNodeObject.NodeObjectType := otMachine;
            xAssignNodeObject.QOVisualize.Caption := xMachineList[i];
            xAssignNodeObject.LinkID := Integer(xMachineList.Objects[i]);
            xAssignNodeObject.BuildProdIDList(TQOAssignNodeObject(xQOInstance).LinkID);
            AddVSTStructure(Sender, Node, xAssignNodeObject, []);
          end;// for i := 0 to TQOAlarmView(xQOInstance).StyleCount - 1 do begin
        finally
          xMachineList.Free;
        end;// try finally
        ChildCount := Sender.ChildCount[Node];
      end;// if ChildCount = 0 then begin
    end;// if (xQOInstance is TQOShiftView) then begin
  
  end;// if assigned(xQOInstance) then begin
end;// TfrmMainWindow.mAnalyzeTreeInitChildren cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mAnalyzeTreeInitNode
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, ParentNode, Node, InitialStates)
 *
 *  Kurzbeschreibung: Wird aufgerufen um zu erfahren ob der entsprechende Knoten Kinder besitzt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAnalyzeTreeInitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode; var
  InitialStates: TVirtualNodeInitStates);
begin
  if not((GetNodeObject(Sender, ParentNode) is TQOAssignNodeObject)) then begin
    include(InitialStates, ivsHasChildren);
  end;// if (GetNodeObject(Sender, Node) is TQOAssignNodeObject) then begin
end;// TfrmMainWindow.mAnalyzeTreeInitNode cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mAnalyzeTreeResize
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Berechnet die Splatenbreite f�r die erste Spalte (Zuordnung)
 *  Beschreibung:     
                      Die erste Spalte wird immer so breit. dass die Buttons am rechten Rand sind.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAnalyzeTreeResize(Sender: TObject);
var
  i: Integer;
  xTempColWidth: Integer;
begin
  // Breite der ersten Spalte bestimmen
  with mAnalyzeTree.Header do begin
    xTempColWidth := 0;
    for i := 1 to Columns.count - 1 do
      xTempColWidth := xTempColWidth + Columns[i].Width;
  //    Columns[0].width := Columns[0].width - Columns[i].Width;
    Columns[0].width := mAnalyzeTree.ClientWidth - xTempColWidth;
  end;// with mAnalyzeTree.Header do
end;// TfrmMainWindow.mAnalyzeTreeResize cat:Tree

//:-------------------------------------------------------------------
procedure TfrmMainWindow.mAppActionsUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  //
end;// TfrmMainWindow.mAppActionsUpdate cat:No category

//:-------------------------------------------------------------------
(*: Member:           mAssignTreeDragDrop
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Source, DataObject, Formats, Shift, Pt, Effect, Mode)
 *
 *  Kurzbeschreibung: F�gt dem Alarm die selektierten Zuordnungen(Artikel, Maschinen) hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAssignTreeDragDrop(Sender: TBaseVirtualTree; Source: TObject; DataObject: IDataObject;
  Formats: TFormatArray; Shift: TShiftState; Pt: TPoint; var Effect: Integer; Mode: TDropMode);
var
  xVst: TBaseVirtualTree;
  xDestNode: PVirtualNode;
  xNodeTop: Integer;
  xDestSelection: TNodeArray;
  xSourceType: TDDSourceType;
begin
  If Source is TmmVirtualStringTree then begin
    xVst := TmmVirtualStringTree(Source);
    xDestNode := Sender.GetNodeAt(pt.x, pt.y, true, xNodeTop);

//: ----------------------------------------------
    // Stellt den Typ der zu plazierenden Objekte fest
    xSourceType := stNone;
    if assigned(xVst.FocusedNode) then begin
      if assigned(GetNodeObject(xVst, xVst.FocusedNode)) then begin
        if GetNodeObject(xVst, xVst.FocusedNode) is TQOAlarm then
          xSourceType := stAlarm;
        if GetNodeObject(xVst, xVst.FocusedNode) is TQOStyle then
          xSourceType := stStyle;
        if GetNodeObject(xVst, xVst.FocusedNode) is TQOMachine then
          xSourceType := stMachine;
      end;// if GetNodeObject(xVst, xVst.FocusedNode) is TQOStyle then
    end;// if assigned(xVst.FocusedNode) then begin
  
    // Legt das Ziel fest (auf Selektion oder neben die Selektion gezogen)
    if mAssignTree.Selected[xDestNode] then begin
      xDestSelection := mAssignTree.GetSortedSelection(false)
    end else begin
      SetLength(xDestSelection, 1);
      xDestSelection[0] := xDestNode;
    end;// if mAssignTree.Selected[xDestNode] then begin

//: ----------------------------------------------
    (* werden die selektierten Alarme auf einen der selektierten Artikel
       gezogen, dann werden die Alarme jedem der selektierten Artikel
       zugeordnet.
       Werden die Alarme auf einen Knoten gezogen der nicht selektiert ist,
       dann werden die Alarme nur diesem Knoten zugeordnet (unabh�ngig von
       der aktuellen Selektion) *)
    if xSourceType = stAlarm then
      AddAlarms(xVst, xVst.GetSortedSelection(false), xDestSelection);

//: ----------------------------------------------
    (* werden die selektierten Artikel auf einen der selektierten Alarme
       gezogen, dann werden die Artikel jedem der selektierten Alarme
       zugeordnet.
       Werden die Artikel auf einen Knoten gezogen der nicht selektiert ist,
       dann werden die Artikel nur diesem Knoten zugeordnet (unabh�ngig von
       der aktuellen Selektion) *)
    if xSourceType = stStyle then
      AddStyles(xVst, xVst.GetSortedSelection(false), xDestSelection);

//: ----------------------------------------------
    (* werden die selektierten Maschinen auf einen der selektierten Artikel
       gezogen, dann werden die Maschinen jedem der selektierten Artikel
       zugeordnet.
       Werden die Maschinen auf einen Knoten gezogen der nicht selektiert ist,
       dann werden die Maschinen nur diesem Knoten zugeordnet (unabh�ngig von
       der aktuellen Selektion) *)
    if xSourceType = stMachine then
      AddMachines(xVst, xVst.GetSortedSelection(false), xDestSelection);

//: ----------------------------------------------
  end;// If Source is TmmVirtualStringTree then begin
end;// TfrmMainWindow.mAssignTreeDragDrop cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mAssignTreeDragOver
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Source, Shift, State, Pt, Mode, Effect, Accept)
 *
 *  Kurzbeschreibung: Pr�ft ob die Selektion auf dem entsprechenden Eintrag abgelegt werden darf.
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAssignTreeDragOver(Sender: TBaseVirtualTree; Source: TObject; Shift: TShiftState; State:
  TDragState; Pt: TPoint; Mode: TDropMode; var Effect: Integer; var Accept: Boolean);
var
  xVst: TBaseVirtualTree;
  xDestNode: PVirtualNode;
  xNodeTop: Integer;
  xSourceType: TDDSourceType;
begin
  If Source is TmmVirtualStringTree then begin
    xVst := TmmVirtualStringTree(Source);

//: ----------------------------------------------
    // Stellt den Typ der zu plazierenden Objekte fest
    xSourceType := stNone;
    if assigned(xVst.FocusedNode) then begin
      if assigned(GetNodeObject(xVst, xVst.FocusedNode)) then begin
        if GetNodeObject(xVst, xVst.FocusedNode) is TQOAlarm then
          xSourceType := stAlarm;
        if GetNodeObject(xVst, xVst.FocusedNode) is TQOStyle then
          xSourceType := stStyle;
        if GetNodeObject(xVst, xVst.FocusedNode) is TQOMachine then
          xSourceType := stMachine;
      end;// if GetNodeObject(xVst, xVst.FocusedNode) is TQOStyle then
    end;// if assigned(xVst.FocusedNode) then begin
  
    // Ziel der Aktion feststellen
    xDestNode := Sender.GetNodeAt(pt.x, pt.y, true, xNodeTop);

//: ----------------------------------------------
    if xSourceType = stAlarm then begin
      // Alarme k�nnen nicht Alarmen zugeordnet werden
      if assigned(xDestNode) then
        Accept := Sender.GetNodeLevel(xDestNode) = 0;
    end;// if xSourceType = stAlarm then begin

//: ----------------------------------------------
    if xSourceType = stStyle then begin
      // Artikel k�nnen nur einem Alarm zugeordnet werden
      if assigned(xDestNode) then
        Accept := Sender.GetNodeLevel(xDestNode) = 0;
    end;// if xSourceType = stStyle then begin

//: ----------------------------------------------
    if xSourceType = stMachine then begin
      // Maschinen k�nnen nur einem Artikel zugeordnet werden
      if assigned(xDestNode) then
        Accept := Sender.GetNodeLevel(xDestNode) = 1;
    end;// if xSourceType = stMachine then begin

//: ----------------------------------------------
  end;// If Source is TmmVirtualStringTree then begin
end;// TfrmMainWindow.mAssignTreeDragOver cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mAssignTreeFocusChanged
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Node, Column)
 *
 *  Kurzbeschreibung: Je nach aktueller Selektion muss die Umgebung angepasst werden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAssignTreeFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);
var
  xObject: TObject;
begin
  // Expand Collapse wieder einschalten (Analyse evt. ausgeschaltet)
  EnableDisableAction(acExpand, true);
  // Expand Collapse wieder einschalten (Analyse evt. ausgeschaltet)
  EnableDisableAction(acCollapse, true);

//: ----------------------------------------------
  if assigned(GetNodeObject(Sender, Node)) then begin
    xObject := GetNodeObject(Sender, Node);
  
    case mAssignStyle of
      rsAlarm: EnableDisableAction(acDeleteAssign, (xObject is TQOAssign));
      rsStyle: EnableDisableAction(acDeleteAssign, (xObject is TQOAlarm));
    else
      raise Exception.Create('TfrmMainWindow.mAssignTreeFocusChanged: AssignStyle not implemented');
    end;//   case mAssignStyle of
  
    if assigned(mAlarmAssignFrame) then begin
      mAlarmAssignFrame.EnableAddStyle(xObject is TQOAlarm);
      mAlarmAssignFrame.EnableAddMachine(xObject is TQOStyle);
    end;// if assigned(mAlarmAssignFrame) then begin
  
    if assigned(mStyleAssignFrame) then
      mStyleAssignFrame.EnableAddAlarm(xObject is TQOStyle);
  end;// if assigned(GetNodeObject(Sender, Node)) then begin
end;// TfrmMainWindow.mAssignTreeFocusChanged cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mAssignTreeGetImageIndex
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Node, Kind, Column, Ghosted, ImageIndex)
 *
 *  Kurzbeschreibung: Gibt das aktuelle Icon f�r den Knoten und die Spalte zur�ck (Zuordnung)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAssignTreeGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind;
  Column: TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
var
  xQOInstance: TObject;
  xVisualizeInterface: IQOVisualize;
  xAssign: TQOAssign;
  xStyle: TObject;
begin
  // Den ImageIndex �ber das Interface abfragen
  xQOInstance := GetNodeObject(Sender, Node);
  
  if assigned(xQOInstance) then begin
    if Kind <> ikOverlay then begin
      // Jetzt das Interface abfragen
      if xQOInstance.GetInterface(IQOVisualize, xVisualizeInterface) then begin
  
        // Zuerst muss festgestellt werden ob der Artikel/Maschine in Produktion ist
        if xQOInstance is TQOAssign then begin
          xAssign := TQOAssign(xQOInstance);
          if (xAssign is TQOMachine) then begin
            xStyle := GetNodeObject(Sender, Sender.NodeParent[Node]);
            if xStyle is TQOStyle then
              xAssign.ParentID := TQOStyle(xStyle).LinkID;
          end;// if (xAssign is TQOMachine) then begin
        end;// if xQOInstance is TQOAssign then begin
  
        // ImageIndex wird vom Interface geliefert
        ImageIndex := xVisualizeInterface.ImageIndex[ikAssignNormal];
      end;// if xQOInstance.GetInterface(IQOVisualize, xVisualizeInterface) then begin
    end;// if Kind <> ikOverlay then begin
  end;// if assigned(xQOInstance) then begin
end;// TfrmMainWindow.mAssignTreeGetImageIndex cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mAssignTreeGetPopupMenu
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Node, Column, P, AskParent, PopupMenu)
 *
 *  Kurzbeschreibung: Gibt an welches Popupmen� angezeigt werden soll (Zuordnung)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAssignTreeGetPopupMenu(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  const P: TPoint; var AskParent: Boolean; var PopupMenu: TPopupMenu);
begin
  inherited;
  {}
end;// TfrmMainWindow.mAssignTreeGetPopupMenu cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mAssignTreeGetText
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Node, Column, TextType, CellText)
 *
 *  Kurzbeschreibung: Setzt den Text der angezeigt werden soll (Zuordnung)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAssignTreeGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType; var CellText: WideString);
var
  xQOInstance: TObject;
  xVisualizeInterface: IQOVisualize;
begin
  try
    // Zuerst das Objekt holen
    xQOInstance := GetNodeObject(Sender, Node);
  
    if assigned(xQOInstance) then begin
      // Jetzt das Interface abfragen
      if xQOInstance.GetInterface(IQOVisualize, xVisualizeInterface) then
        // Anzeigename wird vom Interface geliefert
        CellText := xVisualizeInterface.Caption;
    end;// if assigned(xQOInstance) then begin
  except
    on e: ENoDataObject do
      ShowMessage(e.Message);
  end;// try except
end;// TfrmMainWindow.mAssignTreeGetText cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mAssignTreeResize
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Berechnet die Splatenbreite f�r die erste Spalte (Zuordnung)
 *  Beschreibung:     
                      Die erste Spalte wird immer so breit. dass die Buttons am rechten Rand sind.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mAssignTreeResize(Sender: TObject);
begin
  // Breite der ersten Spalte bestimmen
  with mAssignTree do
    Header.Columns[0].width := mAssignTree.ClientWidth;
end;// TfrmMainWindow.mAssignTreeResize cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mLeftPanelResize
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: sichert die aktuelle Position des vertikalen Splitters
 *  Beschreibung:     
                      Die Position darf erst gesichert werden, wenn der Konstruktor vorbei ist.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mLeftPanelResize(Sender: TObject);
begin
  if mCreated then
    TQOAppSettings.Instance.MainWindowLeftPanelWidth := mLeftPanel.Width;
end;// TfrmMainWindow.mLeftPanelResize cat:No category

//:-------------------------------------------------------------------
(*: Member:           mSettingsTreeAfterCellPaint
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender, TargetCanvas, Node, Column, CellRect)
 *
 *  Kurzbeschreibung: Malt die Fixe Spalte mit dem Datenzeiger
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mSettingsTreeAfterCellPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node:
  PVirtualNode; Column: TColumnIndex; CellRect: TRect);
begin
  if Column = cSettingsPointerCol then begin
    with TargetCanvas do begin
      // Simuliert die Anzeige einer FixedRow wie in TStringGrid
      if toShowVertGridLines in mSettingsTree.TreeOptions.PaintOptions then
        Inc(CellRect.Right);
      if toShowHorzGridLines in mSettingsTree.TreeOptions.PaintOptions then
        Inc(CellRect.Bottom);
      // Zeichnet eine 3D Fl�che
      DrawEdge(Handle, CellRect, BDR_RAISEDINNER, BF_RECT or BF_MIDDLE);
      // Je nach dem auch noch den DS-Pointer zeichnen
      if Node = Sender.FocusedNode then
        mTreeImages.Draw(TargetCanvas, CellRect.Left, CellRect.Top, 27);
    end;// with TargetCanvas do begin
  end;// if Column = cSettingsPointerCol then begin
end;// TfrmMainWindow.mSettingsTreeAfterCellPaint cat:No category

//:-------------------------------------------------------------------
(*: Member:           mSettingsTreeDragDrop
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Source, DataObject, Formats, Shift, Pt, Effect, Mode)
 *
 *  Kurzbeschreibung: f�gt dem Ziel(Alarm) die selektierten DataItems hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mSettingsTreeDragDrop(Sender: TBaseVirtualTree; Source: TObject; DataObject: IDataObject;
  Formats: TFormatArray; Shift: TShiftState; Pt: TPoint; var Effect: Integer; Mode: TDropMode);
var
  xVst: TBaseVirtualTree;
  xDestNode: PVirtualNode;
  xDestSelection: TNodeArray;
  xNodeTop: Integer;
begin
  If Source is TmmVirtualStringTree then begin
    xVst := TmmVirtualStringTree(Source);
    xDestNode := Sender.GetNodeAt(pt.x, pt.y, true, xNodeTop);
  
    // Legt das Ziel fest (auf Selektion oder neben die Selektion gezogen)
    if mSettingsTree.Selected[xDestNode] then begin
      xDestSelection := mSettingsTree.GetSortedSelection(false)
    end else begin
      SetLength(xDestSelection, 1);
      xDestSelection[0] := xDestNode;
    end;// if mSettingsTree.Selected[xDestNode] then begin
  
    (* werden die selektierten DataItems auf einen der selektierten Alarme
       gezogen, dann werden die DataItems jedem der selektierten Alarme
       zugeordnet.
       Werden die DataItems auf einen Knoten gezogen der nicht selektiert ist,
       dann werden die DataItems nur diesem Knoten zugeordnet (unabh�ngig von
       der aktuellen Selektion) *)
    AddItems(xVst, xVst.GetSortedSelection(false), xDestSelection);
  end;// If Source is TmmVirtualStringTree then begin
end;// TfrmMainWindow.mSettingsTreeDragDrop cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mSettingsTreeDragOver
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Source, Shift, State, Pt, Mode, Effect, Accept)
 *
 *  Kurzbeschreibung: Pr�ft ob die Selektion auf dem entsprechenden Eintrag abgelegt werden darf.
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mSettingsTreeDragOver(Sender: TBaseVirtualTree; Source: TObject; Shift: TShiftState; State:
  TDragState; Pt: TPoint; Mode: TDropMode; var Effect: Integer; var Accept: Boolean);
var
  xDestNode: PVirtualNode;
  xNodeTop: Integer;
begin
  If Source is TmmVirtualStringTree then begin
    // Ziel der Aktion feststellen
    xDestNode := Sender.GetNodeAt(pt.x, pt.y, true, xNodeTop);
  
    // DataItems k�nnen nur einem Alarm zugeordnet werden
    if assigned(xDestNode) then
      Accept := Sender.GetNodeLevel(xDestNode) = 0;
  end;// If Source is TmmVirtualStringTree then begin
end;// TfrmMainWindow.mSettingsTreeDragOver cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mSettingsTreeFocusChanged
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Node, Column)
 *
 *  Kurzbeschreibung: Wird aufgerufen, nachdem der aktive Knoten gewechselt hat (Settings)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mSettingsTreeFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);
var
  xObject: TObject;
  xParent: TObject;
begin
  // Expand Collapse wieder einschalten (Analyse evt. ausgeschaltet)
  EnableDisableAction(acExpand, true);
  // Expand Collapse wieder einschalten (Analyse evt. ausgeschaltet)
  EnableDisableAction(acCollapse, true);
  // Es k�nnen nur ganze Alarme gedruckt werden
  EnableDisableAction(acPrintSettings, Sender.GetNodeLevel(Node) = 0);

//: ----------------------------------------------
  // Zwischen BeginUpdate und EndUpdate keine Aktionen vornehmen
  if IsUpdating then
    exit;

//: ----------------------------------------------
  xObject := GetNodeObject(mSettingsTree, Node);
  
  // zuerst alle Frames ausblenden, so dass immer nur ein Frame visible ist
  if assigned(mSettingsAlarmFrame) then
    mSettingsAlarmFrame.Visible := false;
  
  if assigned(mSettingsItemFrame) then
    mSettingsItemFrame.Visible  := false;

//: ----------------------------------------------
  // Alarm Frame erzeugen, wenn er noch nicht gebraucht wurde
  if (xObject is TQOAlarm) and(not(assigned(mSettingsAlarmFrame))) then
    CreateSettingsAlarmFrame;
  
  // ---- HINWEIS ----
  // Der SettingsAlarmFrame wird ebenfalls im Konstruktor erzeugt um die TreeImages mit
  // den Bitmaps f�r die DataItems zu erweitern

//: ----------------------------------------------
  // Item Frame erzeugen, wenn er noch nicht gebraucht wurde
  if (xObject is TQOItem) and(not(assigned(mSettingsItemFrame))) then begin
    mSettingsItemFrame := TfrmSettingsItemFrame.Create(self);
    with mSettingsItemFrame do try
      // Die Einstellungen zum Alarm werden auf der rechten Seite angezeigt
      Parent := mRightPanel;
      Align  := alClient;
      Images := mMethodImages;
      OnItemSaved := SettingsItemSaved;
    except
      raise;
    end;// with mItemSettingsFrame do try
  end;// if (xObject is TQOAlarm) and(not(assigned(mAlarmSettingsFrame))) then begin

//: ----------------------------------------------
  // Alarm Einstellungen anzeigen
  if xObject is TQOAlarm then begin
    // Security f�r das Speichern der Alarmsettings
    mSettingsAlarmFrame.SaveEnabled := acEditAlarm.Enabled;
    mSettingsAlarmFrame.SaveVisible := acEditAlarm.Visible;
    // Anzeigen des Alarms
    mSettingsAlarmFrame.ShowAlarm(xObject);
  end;// if xObject is TQOAlarm then begin
  
  // Alarm Einstellungen anzeigen
  if xObject is TQOItem then begin
    // Security f�r das Speichern der DataItems
    mSettingsItemFrame.SaveEnabled := acEditDataItem.Enabled;
    mSettingsItemFrame.SaveVisible := acEditDataItem.Visible;
  
    xParent := nil;
    if assigned(mSettingsTree.NodeParent[Node]) then
      xParent := GetNodeObject(mSettingsTree, mSettingsTree.NodeParent[Node]);
  
    // Anzeigen des DataItems
    mSettingsItemFrame.ShowItem(xObject, xParent);
  end;// if xObject is TQOItem then begin
end;// TfrmMainWindow.mSettingsTreeFocusChanged cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mSettingsTreeFocusChanging
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, OldNode, NewNode, OldColumn, NewColumn, Allowed)
 *
 *  Kurzbeschreibung: Wird aufgerufen, bevor der Knoten gewechselt wird (Settings)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mSettingsTreeFocusChanging(Sender: TBaseVirtualTree; OldNode, NewNode: PVirtualNode; OldColumn,
  NewColumn: TColumnIndex; var Allowed: Boolean);
begin
  // Zwischen BeginUpdate und EndUpdate keine Aktionen vornehmen
  if not(IsUpdating) then
    // Speichert das selektierte Objekt
    Allowed := not(SaveCurrentSettings = mrCancel);
end;// TfrmMainWindow.mSettingsTreeFocusChanging cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mSettingsTreeGetImageIndex
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Node, Kind, Column, Ghosted, ImageIndex)
 *
 *  Kurzbeschreibung: Gibt das aktuelle Icon f�r den Knoten und die Spalte zur�ck (Settings)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mSettingsTreeGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind;
  Column: TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
var
  xMethodType: TCalculationMethod;
  xItem: TQOItem;
  xQOInstance: TObject;
  xVisualizeInterface: IQOVisualize;
begin
  // Den ImageIndex �ber das Interface abfragen
  xQOInstance := GetNodeObject(Sender, Node);
  
  if assigned(xQOInstance) then begin
    // Jetzt das Interface abfragen
    if xQOInstance.GetInterface(IQOVisualize, xVisualizeInterface) then begin
      if Kind <> ikOverlay then begin
        case Column of
          // Kein Bitmap f�r die Spalte -1
          -1:;
          // Tree Spalte
          0: begin
            // ImageIndex wird vom Interface geliefert
            ImageIndex := xVisualizeInterface.ImageIndex[ikSettingsNormal];
            ghosted := not(xVisualizeInterface.Active) and (xQOInstance is TQOItem);
          end;// 0: begin
          // Spalte mit dem Datenzeiger
          cSettingsPointerCol:;
        else
          // Wenn die Zeile Markiert ist, dann die Images aufhellen
          ghosted := (Kind = ikSelected);
          if mSettingsTree.GetNodeLevel(Node) = 1 then begin
            xMethodType := TCalculationMethod(Column - mFirstMethodCol);
            if assigned(GetNodeObject(Sender, Node)) then begin
              xItem := TQOItem(GetNodeObject(Sender, Node));
              if xItem.QOMethods.MethodByType(xMethodType).Active then begin
                ImageIndex := cPointImage;
                if xItem.BoolOpAnd then
                  ImageIndex := cPlusGreen;
              end;// if xItem.Methods.MethodByType(xMethodType).Active then begin
            end;// if assigned(GetNodeObject) then begin
          end;// if mSettingsTree.GetNodeLevel(Node) = 1 then begin
        end;// case Column of
      end else begin
        // Overlay Behandlung
        ImageIndex := -1;
        if Column = 0 then begin
          if Sender.GetNodeLevel(Node) = 1 then begin
            if assigned(GetNodeObject(Sender, Sender.NodeParent[Node])) then begin
              if TQOAlarm(GetNodeObject(Sender, Sender.NodeParent[Node])).BoolOpAnd then begin
                ImageIndex := cPlusOverlay;
                ghosted := not(xVisualizeInterface.Active);
              end;//
            end;// if assigned(GetNodeObject(Sender, Sender.NodeParent[Node])) then begin
          end;// if Sender.GetNodeLevel(Node) = 1 then begin
        end;// if Column = 0 then begin
      end;// if Kind <> ikOverlay then begin
    end;// if xQOInstance.GetInterface(IQOVisualize, xVisualizeInterface) then begin
  end;// if assigned(xQOInstance) then begin
end;// TfrmMainWindow.mSettingsTreeGetImageIndex cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mSettingsTreeGetPopupMenu
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Node, Column, P, AskParent, PopupMenu)
 *
 *  Kurzbeschreibung: Gibt an welches Popupmen� angezeigt werden soll (Settings)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mSettingsTreeGetPopupMenu(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  const P: TPoint; var AskParent: Boolean; var PopupMenu: TPopupMenu);
begin
  PopupMenu := pmSettingsTreeAlarm;
end;// TfrmMainWindow.mSettingsTreeGetPopupMenu cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mSettingsTreeGetText
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Node, Column, TextType, CellText)
 *
 *  Kurzbeschreibung: Setzt den Text der angezeigt werden soll (Settings)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mSettingsTreeGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType; var CellText: WideString);
var
  xVisualizeInterface: IQOVisualize;
  xQOInstance: TObject;
begin
  xQOInstance := nil;
  
  try
    // Zuerst das Objekt holen
    if Column >= 0 then
      xQOInstance := GetNodeObject(Sender, Node);
  
    case Column of
      0: begin
        if assigned(xQOInstance) then begin
          // Jetzt das Interface abfragen
          if xQOInstance.GetInterface(IQOVisualize, xVisualizeInterface) then
            // Anzeigename wird vom Interface geliefert
            CellText := xVisualizeInterface.Caption;
        end;// if assigned(xQOInstance) then begin
      end;// 0: begin
    else
      CellText := '';
    end;// case Column of
  except
    on e: ENoDataObject do
      ShowMessage(e.Message);
  end;// try except
end;// TfrmMainWindow.mSettingsTreeGetText cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mSettingsTreeMouseDown
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender, Button, Shift, X, Y)
 *
 *  Kurzbeschreibung: Tempor�r um eine Berechnung auszul�sen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mSettingsTreeMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y:
  Integer);
var
  xSave: Boolean;
  xQOShiftView: TQOShiftView;
  
  const
    cTasten = [ssShift, ssAlt, ssCtrl];
  
begin
  if (Button = mbRight) and ((cTasten * Shift) = cTasten)  then begin
    try
      xSave := MessageDlg(rsSpeichern, mtInformation, [mbYes,mbNo], 0) = mrYes;
      Screen.Cursor := crSQLWait;
      TQOAlarms.Instance.Evaluate(xSave);
      showMessage(Format(rsErmittelteOfflimitVerletzungen + ' %d', [TQOAlarms.Instance.OfflimitCount]));
  
      if TQOAlarms.Instance.OfflimitCount > 0 then  begin
        xQOShiftView := TQOShiftView.Create;
        xQOShiftView.Assign(TQOAlarms.Instance);
        TQOShiftSet.Instance.Insert(0, xQOShiftView);
        RefreshAnalyzeData;
      end;//   if TQOAlarms.Instance.OfflimitCount > 0 then begin
  
    finally
      Screen.Cursor := crDefault;
    end;// try finally
  end;// if (Button = mbRight) and (ssleft in Shift)  then begin
end;// TfrmMainWindow.mSettingsTreeMouseDown cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mSettingsTreeResize
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Berechnet die Splatenbreite f�r die erste Spalte (Settings)
 *  Beschreibung:     
                      Die erste Spalte wird immer so breit dass die Methoden 
                      Splalten am rechten Rand sind.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mSettingsTreeResize(Sender: TObject);
var
  xFirstColWidth: Integer;
  i: Integer;
begin
  // Breite der ersten Spalte bestimmen
  with mSettingsTree.Header do begin
    (* Die Breite der Spalte berechnet sich aus der Breite des Trees
       minus der Breite aller definierten Methoden Spalten *)
    xFirstColWidth := mSettingsTree.ClientWidth;
    for i := 0 to Columns.count - 1 do
      if i <> cSettingsNameCol then
        xFirstColWidth := xFirstColWidth - Columns[i].width;
  
    (* Minimale Breite der ersten Spalte einhalten.
       Wird diese Breite unterschritten, wird ein Scrollbalken erzeugt *)
    if xFirstColWidth > cMinNameSettingsColWidth then
      Columns[cSettingsNameCol].width := xFirstColWidth;
  end;// with mSettingsTree.Header do begin
end;// TfrmMainWindow.mSettingsTreeResize cat:Tree

//:-------------------------------------------------------------------
(*: Member:           mTranslatorAfterTranslate
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (translator)
 *
 *  Kurzbeschreibung: Alle Texte die nicht automatisch �bersetzt werden
 *  Beschreibung:     
                      Einige Texte werden nicht automatisch �bersetzt (z.B. Die Spalten�berschriften des
                      VirtualTreeView). Diese Teste m�ssen hier manuell �bersetzt werden.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.mTranslatorAfterTranslate(translator: TIvCustomTranslator);
var
  i: Integer;
  xNode: PVirtualNode;
  xVisualizeInterface: IQOVisualize;
  xQOInstance: TObject;
begin
  // Tree Header
  mSettingsTree.Header.Columns[0].Text := rsIncludedAlarms;
  mAssignTree.Header.Columns[0].Text := rsIncludedAlarms;
  mAnalyzeTree.Header.Columns[0].Text := rsAlarms;

//: ----------------------------------------------
  // Analyse Wurzel-Knoten
  xNode := mAnalyzeTree.GetFirst;
  if Assigned(xNode) then begin
    for i := 0 to cAnalyzeRootNodeCount - 1 do begin
      if Assigned(xNode) then begin
        xQOInstance := GetNodeObject(mAnalyzeTree, xNode);
        if assigned(xQOInstance) then begin
          // Jetzt das Interface abfragen
          if xQOInstance.GetInterface(IQOVisualize, xVisualizeInterface) then
            // Anzeigename wird vom Interface geliefert
            xVisualizeInterface.Caption := Translate(cAnalyzeRootNodeText[i]);
        end;// if assigned(xQOInstance) then begin
      end else begin
        Break;
      end;// if Assigned(xNode) then begin
      xNode := mAnalyzeTree.GetNextSibling(xNode);
    end;// for i := 0 to cAnalyzeRootNodeCount - 1 do begin
  end;// if Assigned(xNode) then begin
end;// TfrmMainWindow.mTranslatorAfterTranslate cat:No category

//:-------------------------------------------------------------------
(*: Member:           OnAlarmviewChartClick
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (aDate, aAlarmName, aButton, aShiftKey)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn im AlarmView auf eine Serie des Histogramms gecklickt wird
 *  Beschreibung:     
                      Bei einem Klick auf einen ALarm wird versucht die entsprechende
                      Schicht zu selektieren.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.OnAlarmviewChartClick(aDate: TDateTime; aAlarmName: string; aButton: TMouseButton; aShiftKey:
  TShiftState);
var
  xNode: PVirtualNode;
begin
  try
    Screen.Cursor := crSQLWait;
  
    // Selektiert den Knoten mit der Beschriftung die zum Datum passt
    xNode := mAnalyzeTree.FindString(DateTimeToTreeCaption(aDate), 0, [foFirstNode, foLevel], 1);
    xNode := mAnalyzeTree.FindString(aAlarmName, 0, [foFromBegin, foSelect, foDeliveredNode], 0, xNode);

//: ----------------------------------------------
    if assigned(xNode) then begin
      mAnalyzeTree.FullExpand(xNode);
      mAnalyzeTree.FocusedNode := xNode;
    end;// if assigned(xNode) then begin

//: ----------------------------------------------
  finally
    Screen.Cursor := crDefault;
  end;// try finally
end;// TfrmMainWindow.OnAlarmviewChartClick cat:No category

//:-------------------------------------------------------------------
(*: Member:           OnAssignAddAlarms
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: wird aufgerufen, wenn mit dem Button die selektierten Alarme hinzugef�gt werden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.OnAssignAddAlarms(Sender: TBaseVirtualTree);
var
  xDestSelection: TNodeArray;
begin
  // Legt das Ziel fest (auf Selektion oder neben die Selektion gezogen)
  xDestSelection := mAssignTree.GetSortedSelection(false);

//: ----------------------------------------------
  (* werden die selektierten Artikel auf einen der selektierten Alarme
     gezogen, dann werden die Artikel jedem der selektierten Alarme
     zugeordnet.
     Werden die Artikel auf einen Knoten gezogen der nicht selektiert ist,
     dann werden die Artikel nur diesem Knoten zugeordnet (unabh�ngig von
     der aktuellen Selektion) *)
  AddAlarms(Sender, Sender.GetSortedSelection(false), xDestSelection);
end;// TfrmMainWindow.OnAssignAddAlarms cat:No category

//:-------------------------------------------------------------------
(*: Member:           OnAssignAddItems
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: wird aufgerufen, wenn mit dem Button die selektierten Artikel / Maschinen hinzugef�gt werden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.OnAssignAddItems(Sender: TBaseVirtualTree);
var
  xDestSelection: TNodeArray;
  xSourceType: TDDSourceType;
begin
  // Stellt den Typ der zu plazierenden Objekte fest
  xSourceType := stNone;
  if assigned(Sender.FocusedNode) then begin
    if assigned(GetNodeObject(Sender, Sender.FocusedNode)) then begin
      if GetNodeObject(Sender, Sender.FocusedNode) is TQOStyle then
        xSourceType := stStyle
      else
        xSourceType := stMachine;
    end;// if GetNodeObject(xVst, xVst.FocusedNode) is TQOStyle then
  end;// if assigned(xVst.FocusedNode) then begin
  
  // Legt das Ziel fest (auf Selektion oder neben die Selektion gezogen)
  xDestSelection := mAssignTree.GetSortedSelection(false);

//: ----------------------------------------------
  (* werden die selektierten Artikel auf einen der selektierten Alarme
     gezogen, dann werden die Artikel jedem der selektierten Alarme
     zugeordnet.
     Werden die Artikel auf einen Knoten gezogen der nicht selektiert ist,
     dann werden die Artikel nur diesem Knoten zugeordnet (unabh�ngig von
     der aktuellen Selektion) *)
  if xSourceType = stStyle then
    AddStyles(Sender, Sender.GetSortedSelection(false), xDestSelection);

//: ----------------------------------------------
  (* werden die selektierten Maschinen auf einen der selektierten Artikel
     gezogen, dann werden die Maschinen jedem der selektierten Artikel
     zugeordnet.
     Werden die Maschinen auf einen Knoten gezogen der nicht selektiert ist,
     dann werden die Maschinen nur diesem Knoten zugeordnet (unabh�ngig von
     der aktuellen Selektion) *)
  if xSourceType = stMachine then
    AddMachines(Sender, Sender.GetSortedSelection(false), xDestSelection);
end;// TfrmMainWindow.OnAssignAddItems cat:No category

//:-------------------------------------------------------------------
(*: Member:           OnOverviewChartClick
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (aDate, aButton, aShiftKey)
 *
 *  Kurzbeschreibung: wird aufgerufen, wenn im Histogramm auf einen Alarm geklickt wurde
 *  Beschreibung:     
                      Bei einem Klick auf einen ALarm wird versucht die entsprechende
                      Schicht zu selektieren.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.OnOverviewChartClick(aDate: TDateTime; aButton: TMouseButton; aShiftKey: TShiftState);
var
  xNode: PVirtualNode;
begin
  try
    Screen.Cursor := crSQLWait;
    // Selektiert den Knoten mit der Beschriftung die zum Datum passt
    xNode := mAnalyzeTree.FindString(DateTimeToTreeCaption(aDate), 0, [foFirstNode, foFromBegin, foSelect]);

//: ----------------------------------------------
    if assigned(xNode) then begin
      mAnalyzeTree.FullExpand(xNode);
      mAnalyzeTree.FocusedNode := xNode;
    end;// if assigned(xNode) then begin

//: ----------------------------------------------
  finally
    Screen.Cursor := crDefault;
  end;// try finally
end;// TfrmMainWindow.OnOverviewChartClick cat:No category

//:-------------------------------------------------------------------
(*: Member:           OnSettingsAddItems
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: wird aufgerufen, wenn mit dem Button die selektierten Data Items hinzugef�gt werden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.OnSettingsAddItems(Sender: TBaseVirtualTree);
var
  xDestSelection: TNodeArray;
begin
  // Legt das Ziel fest (auf Selektion oder neben die Selektion gezogen)
  xDestSelection := mSettingsTree.GetSortedSelection(false);
  
  (* werden die selektierten DataItems auf einen der selektierten Alarme
     gezogen, dann werden die DataItems jedem der selektierten Alarme
     zugeordnet.
     Werden die DataItems auf einen Knoten gezogen der nicht selektiert ist,
     dann werden die DataItems nur diesem Knoten zugeordnet (unabh�ngig von
     der aktuellen Selektion) *)
  AddItems(Sender, Sender.GetSortedSelection(false), xDestSelection);
end;// TfrmMainWindow.OnSettingsAddItems cat:No category

//:-------------------------------------------------------------------
(*: Member:           RebuildAssignTreeClick
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Ereignis Handler f�r die Buttons in der rechten oberen Ecke des AssignTree
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.RebuildAssignTreeClick(Sender: TObject);
begin
  // Zuordnung Alarm - Style
  if Sender = acAssignAlarm then begin
    BuildAssignTreeFromAlarms(rsAlarm);
    mAssignStyle := rsAlarm;
    mAlarmAssignFrame.visible := true;
    mStyleAssignFrame.visible := false;
  end;// if Sender = acAssignAlarm then begin

//: ----------------------------------------------
  // Zuordnung Style - Alarm
  if Sender = acAssignStyle then begin
    BuildAssignTreeFromAlarms(rsStyle);
    mAssignStyle := rsStyle;
    mAlarmAssignFrame.visible := false;
    mStyleAssignFrame.visible := true;
  end;// if Sender = acAssignStyle then begin
end;// TfrmMainWindow.RebuildAssignTreeClick cat:No category

//:-------------------------------------------------------------------
(*: Member:           RefreshAnalyzeData
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Erneuert die Anzeige der Analyse Daten
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.RefreshAnalyzeData;
begin
  try
    Screen.Cursor := crSQLWait;
    TQOShiftSet.Instance.LoadAlarmRootFromDB(-1);
    if assigned(mAnalyzeOverviewFrame) then begin
      mAnalyzeOverviewFrame.UnInitialize;
      mAnalyzeOverviewFrame.ShowOverview;
    end;// if assigned(mAnalyzeOverviewFrame) then begin
  finally
    Screen.Cursor := crDefault;
  end;// try finally
end;// TfrmMainWindow.RefreshAnalyzeData cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveCurrentSettings
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        Tree 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Speichert das aktuelle Objekt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmMainWindow.SaveCurrentSettings: Word;
begin
  (* Wenn beide Frames nicht existieren, dann muss result mit "True"
     initialisiert werden, da ja keine Settings gespeichert werden m�ssen. *)
  if not((assigned(mSettingsAlarmFrame)) or (assigned(mSettingsItemFrame))) then
    result := mrOK
  else
    result := mrNo;
  
  // result := not((assigned(mSettingsAlarmFrame)) or (assigned(mSettingsItemFrame)));

//: ----------------------------------------------
  // Alarm
  if assigned(mSettingsAlarmFrame) then begin
    if mSettingsAlarmFrame.visible then begin
       result := mSettingsAlarmFrame.SaveSettings(true, true);
    end;// if mAlarmSettingsFrame.visible then begin
  end;// if not(assigned(mAlarmSettingsFrame)) then begin

//: ----------------------------------------------
  // Item
  if assigned(mSettingsItemFrame) then begin
    if mSettingsItemFrame.visible then begin
      result := mSettingsItemFrame.SaveSettings(true, true);
    end;// if mItemSettingsFrame.visible then begin
  end;// if not(assigned(mItemSettingsFrame)) then begin
end;// TfrmMainWindow.SaveCurrentSettings cat:Tree

//:-------------------------------------------------------------------
(*: Member:           SetActiveUseCase
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (aUseCase)
 *
 *  Kurzbeschreibung: Setzt die �bergebene Funktion als aktiv
 *  Beschreibung:     
                      Um einer �nderung der Implementierung entgegenzuwirken, wird diese 
                      Methode zur aktivierung der entsprechenden Funktion eingesetzt.
 --------------------------------------------------------------------*)
function TfrmMainWindow.SetActiveUseCase(aUseCase: TQOUseCase): Boolean;
begin
  result := false;
  
  case aUseCase of
    ucSettings: begin
      result := true;
      if mTreePageControl.ActivePage = tabSettings then
        tabSettingsShow(Self)
      else
        mTreePageControl.ActivePage := tabSettings;
    end;// ucSettings
    ucAssign: begin
      result := true;
      if mTreePageControl.ActivePage = tabAssign then
        tabSettingsShow(Self)
      else
        mTreePageControl.ActivePage := tabAssign;
    end;// ucAssign
    ucAnalyze: begin
      result := true;
      if mTreePageControl.ActivePage = tabAnalyze then
        tabSettingsShow(Self)
      else
        mTreePageControl.ActivePage := tabAnalyze;
    end;// ucAnalyze
  end;// case aUseCase of
  {  result := false;
  
    case aUseCase of
      ucSettings: begin
        result := true;
        mTreePageControl.ActivePage := tabSettings;
      end;// ucSettings
      ucAssign: begin
        result := true;
        mTreePageControl.ActivePage := tabAssign;
      end;// ucAssign
      ucAnalyze: begin
        result := true;
        mTreePageControl.ActivePage := tabAnalyze;
      end;// ucAnalyze
    end;// case aUseCase of}
end;// TfrmMainWindow.SetActiveUseCase cat:No category

//:-------------------------------------------------------------------
(*: Member:           SettingsItemSaved
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aItem)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn ein Data Item gespeichert wurde
 *  Beschreibung:     
                      Das SettingsTree muss neu angezeigt werden, da auch Informationen �ber 
                      die Methoden im Tree angezeigt werden.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.SettingsItemSaved(Sender: TfrmSettingsItemFrame; aItem: TQOItem);
begin
  // Tree neu zeichnen, damit die �nderungen an den Methoden angezeigt wird
  mSettingsTree.Invalidate;
end;// TfrmMainWindow.SettingsItemSaved cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetUpdating
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Updating)
 *
 *  Kurzbeschreibung: Update Patern
 *  Beschreibung:     
                      Hier k�nnten Operationen ausgef�hrt werden mit welchen die Umgebung
                      wieder den entsprechenden Anforderungen angepasst werden.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.SetUpdating(Updating: Boolean);
begin
end;// TfrmMainWindow.SetUpdating cat:No category

//:-------------------------------------------------------------------
(*: Member:           ShiftsLoaded
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Wird aufgerufen wenn die Schichten neu geladen wurden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.ShiftsLoaded(Sender: TObject);
begin
  BuildAnalyzeTreeFromShiftSet;
end;// TfrmMainWindow.ShiftsLoaded cat:No category

//:-------------------------------------------------------------------
(*: Member:           tabAnalyzeHide
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Blendet die Analyze Frames aus
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.tabAnalyzeHide(Sender: TObject);
begin
  // Darstellung der �bersicht
  if assigned(mAnalyzeOverviewFrame) then
    mAnalyzeOverviewFrame.Visible := false;

//: ----------------------------------------------
  // Darstellung des Alarms
  if assigned(mAnalyzeAlarmFrame)then
    mAnalyzeAlarmFrame.visible := false;

//: ----------------------------------------------
  // Darstellung des Charts
  if assigned(mAnalyzeChartFrame) then
    mAnalyzeChartFrame.Visible := false;
end;// TfrmMainWindow.tabAnalyzeHide cat:No category

//:-------------------------------------------------------------------
(*: Member:           tabAnalyzeShow
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Erzeugt, wenn notwendig den Inhalt des Trees
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.tabAnalyzeShow(Sender: TObject);
begin
  (* Die Daten nur dann neu von der DB laden, wenn dies nicht
     bereits mindestens einmal passiert ist *)
  if (mAnalyzeTree.ChildCount[nil] = 0) and (mCreated)then begin
    if (TQOShiftSet.Instance.ShiftCount = 0) then begin
      RefreshAnalyzeData;
    end else begin
      try
        Screen.Cursor := crSQLWait;
        BuildAnalyzeTreeFromShiftSet;
      finally
        Screen.Cursor := crDefault;
      end;// try finally
    end;//if (TQOShiftSet.Instance.AlarmCount = 0) then begin
  end else begin
    // Den Knoten wieder einblenden (F�r den Fall, dass er vorher asgeblendet wurde)
    if assigned(mAnalyzeTree.FocusedNode)then
      mAnalyzeTree.FullyVisible[mAnalyzeTree.FocusedNode] := true;
  
    // Knoten neu selektieren, damit der Frame wieder angezeigt wird
    mAnalyzeTree.ForceFocusChanged(mAnalyzeTree.FocusedNode);
  end;// if (mAnalyzeTree.ChildCount[nil] = 0) and (mCreated)then begin

//: ----------------------------------------------
  HelpContext := GetHelpContext('LabMaster\QOfflimit\QOFF_Fenster_Q-Offlimit_Auswerten.htm');
end;// TfrmMainWindow.tabAnalyzeShow cat:No category

//:-------------------------------------------------------------------
(*: Member:           tabAssignHide
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Speichert die h�ngigen Zuordnungen und blendet die entsprechenden Frames aus
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.tabAssignHide(Sender: TObject);
begin
  if assigned(mAlarmAssignFrame)then begin
    mAlarmAssignFrame.visible := false;
    mStyleAssignFrame.visible := false;
  end;// if assigned(mAlarmAssignFrame)then begin
end;// TfrmMainWindow.tabAssignHide cat:No category

//:-------------------------------------------------------------------
(*: Member:           tabAssignShow
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Zeigt die Seite mit den Alarm Zuordnungen an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.tabAssignShow(Sender: TObject);
begin
  // Ruft ein Resize auf um die NC-Buttons sicher am richtigen Ort zu zeichnen
  mAssignTreeResize(Sender);
  
  // Bei der Anzeige zuerst den Baum aufbauen
  if mCreated then
    BuildAssignTreeFromAlarms(mAssignStyle);

//: ----------------------------------------------
  // Artikel-Auswahl-Frame erzeugen, wenn er noch nicht gebraucht wurde
  if not(assigned(mAlarmAssignFrame)) then begin
    mAlarmAssignFrame := TfrmAssignAlarmFrame.Create(self);
    with mAlarmAssignFrame do begin
      // Die Einstellungen zum Alarm werden auf der rechten Seite angezeigt
      TreeImages    := mTreeImages;
      Parent        := self.mRightPanel;
      Align         := alClient;
      Visible       := false;
      (* Wird ausgef�hrt, wenn eine Selektion mit dem Button in das
         AssignTree �bertragen werden soll *)
      OnAddItems    := OnAssignAddItems;
      (* Beim Init werden die Daten von der DB geholt.
         Dies sind die verf�gbaren Artikel und Maschinen.
         Ausserdem wird ein Array mit den aktiven Produktionsgruppen
         von der DB geholt *)
      Init;
    end;// with mAlarmAssignFrame do begin
  end;// if not(assigned(mAlarmAssignFrame)) then begin

//: ----------------------------------------------
  // Alarm-Auswahl-Frame erzeugen, wenn er noch nicht gebraucht wurde
  if not(assigned(mStyleAssignFrame)) then begin
    mStyleAssignFrame := TfrmAssignStyleFrame.Create(self);
    with mStyleAssignFrame do begin
      // Die Einstellungen zum Alarm werden auf der rechten Seite angezeigt
      TreeImages    := mTreeImages;
      Parent        := self.mRightPanel;
      Align         := alClient;
      Visible       := false;
      (* Wird ausgef�hrt, wenn eine Selektion mit dem Button in das
         AssignTree �bertragen werden soll *)
      OnAddItems    := OnAssignAddAlarms;
      (* Beim Init werden die Daten von der DB geholt.
         Dies sind die verf�gbaren Artikel und Maschinen.
         Ausserdem wird ein Array mit den aktiven Produktionsgruppen
         von der DB geholt *)
      Init;
    end;// with mAlarmAssignFrame do begin
  end;// if not(assigned(mAlarmAssignFrame)) then begin
  
  case mAssignStyle of
    rsAlarm: mAlarmAssignFrame.visible := true;
    rsStyle: mStyleAssignFrame.visible := true;
    else
      raise Exception.Create('TfrmMainWindow.tabAssignShow: AssignStyle not implemented');
  end;// case mAssignStyle of

//: ----------------------------------------------
  HelpContext := GetHelpContext('LabMaster\QOfflimit\QOFF_Fenster_Q-Offlimit_Zuordnen.htm');
end;// TfrmMainWindow.tabAssignShow cat:No category

//:-------------------------------------------------------------------
(*: Member:           tabSettingsHide
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Speichert die h�ngigen Settings und blendet die entsprechenden Frames aus
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.tabSettingsHide(Sender: TObject);
begin
  if assigned(mSettingsAlarmFrame)then begin
    mSettingsAlarmFrame.SaveSettings(true, false);
    FreeAndNil(mSettingsAlarmFrame);
  end;// if assigned(mAlarmSettingsFrame)then begin

//: ----------------------------------------------
  if assigned(mSettingsItemFrame)then begin
    mSettingsItemFrame.SaveSettings(true, false);
    FreeAndNil(mSettingsItemFrame);
  end;// if assigned(mItemSettingsFrame)then begin

//: ----------------------------------------------
  EnableDisableAction(acDeleteSettings, false);
  EnableDisableAction(acNew, false);
end;// TfrmMainWindow.tabSettingsHide cat:No category

//:-------------------------------------------------------------------
(*: Member:           tabSettingsShow
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Zeigt die Seite mit den Alarm Settings an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.tabSettingsShow(Sender: TObject);
begin
  // Bei der Anzeige zuerst den Baum aufbauen
  if mCreated then
    BuildSettingsTreeFromAlarms;

//: ----------------------------------------------
  EnableDisableAction(acDeleteSettings, true);
  EnableDisableAction(acNew, true);

//: ----------------------------------------------
  HelpContext := GetHelpContext('LabMaster\QOfflimit\QOFF_Fenster_Q-Offlimit_erfassen.htm');
end;// TfrmMainWindow.tabSettingsShow cat:No category

//:-------------------------------------------------------------------
(*: Member:           TreeKeyAction
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender, CharCode, Shift, DoDefault)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn in einem TreeView eine Taste gedr�ckt wird (WM_KEYDOWN)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.TreeKeyAction(Sender: TBaseVirtualTree; var CharCode: Word; var Shift: TShiftState; var
  DoDefault: Boolean);
begin
  case CharCode of
  
    // L�schen - Taste
    VK_DELETE: begin
      if Shift = [] then begin
        if Sender = mSettingsTree then
          acDeleteSettings.Execute
        else if Sender = mAssignTree then
          acDeleteAssign.Execute
        else if Sender = mAnalyzeTree then
          acDeleteFromArchive.Execute;
      end;// if Shift = [] then begin
    end;// VK_DELETE: begin
  
    // Einf�gen - Taste
    VK_INSERT: begin
      if Shift = [] then begin
        if Sender = mSettingsTree then
          acNew.Execute;
      end;// if Shift = [] then begin
    end;// VK_INSERT: begin
  
  end;// case CharCode of
end;// TfrmMainWindow.TreeKeyAction cat:No category

//:-------------------------------------------------------------------
(*: Member:           TreeKeyDown
 *  Klasse:           TfrmMainWindow
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Key, Shift)
 *
 *  Kurzbeschreibung: Sonderbehandlung einiger Tasten f�r das AnalyzeTree
 *  Beschreibung:     
                      Im AnalyzeTree werden einige Tasten besonders behandelt:
                      ALT + UP, ALT + PAGE UP     ==> geht zum jeweils oberen sichtbaren Knoten auf gleicher Ebene.
                      ALT + DOWN, ALT + PAGE DOWN ==> geht zum jeweils unteren sichtbaren Knoten auf gleicher Ebene.
 --------------------------------------------------------------------*)
procedure TfrmMainWindow.TreeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  xNextNode: PVirtualNode;
  xKeepSelection: Boolean;
  xSender: TBaseVirtualTree;
  
  (*-----------------------------------------------------------
    Gibt den n�chsten Knoten auf dem selben Level zur�ck
  -------------------------------------------------------------*)
  function GetNextNodeSameLevel(aSender: TBaseVirtualTree; aUp: boolean; var aMultiselect: boolean): PVirtualNode;
  var
    xFound: Boolean;
    xIndexToSelect: Integer;
    xTempNode: PVirtualNode;
    i: Integer;
    xTreeSelection: TNodeArray;
  begin
    // Wird kein Knoten gefunden, dann wird der aktuelle Knoten zur�ckgegeben
    result := aSender.FocusedNode;
  
    // Alle selektierten Nodes holen
    xTreeSelection := aSender.GetSortedSelection(false);
    aMultiselect := (Length(xTreeSelection) > 1);
  
    if not(aMultiselect) then begin
      // Nur ein Node selektiert
      xTempNode := result;
      xFound := false;
      while (assigned(xTempNode)) and (not(xFound)) do begin
        // Je nachdem oben, oder unten suchen
        if aUp then
          xTempNode := aSender.GetPreviousNoInit(xTempNode)
        else
          xTempNode := aSender.GetNextNoInit(xTempNode);
        (* wurde ein Node gefunden, dann kontrollieren ob er auf dem selben Level liegt wie
           der urspr�nglich selektierte Node und ob er aufgeklappt ist (sichtbar) *)
        if assigned(xTempNode) then begin
          xFound :=     (aSender.GetNodeLevel(xTempNode) = aSender.GetNodeLevel(result))
                    and (aSender.FullyVisible[xTempNode]);
        end;// if assigned(xTempNode) then begin
      end;// while (assigned(xTempNode)) and (not(xFound)) do begin
  
      // Wurde ein passender Node gefunden, dann diesen selektieren
      if xFound then
        result := xTempNode;
    end else begin
      // Mehrere Nodes selelktiert
      xTempNode := nil;
      i := 0;
      // Zuerst einmal den fokussierten Node in der Selektion suchen
      while (i < Length(xTreeSelection)) and (not(assigned(xTempNode))) do begin
        if xTreeSelection[i] = aSender.FocusedNode then
          xTempNode := xTreeSelection[i]
        else
          inc(i);
      end;// while (i < Length(xTreeSelection)) and (not(assigned(xTempNode))) do begin
  
      // Nur weiter, wenn der aktuelle Node gefunden wurde
      if assigned(xTempNode) then begin
        // Je nach Suchrichtung nach oben oder unten suchen
        if aUp then
          xIndexToSelect := i - 1
        else
          xIndexToSelect := i + 1;
  
        // Ist in der gew�nschten Richtung ein Node vorhanden, diesen zur�ckgeben
        if (Length(xTreeSelection) > xIndexToSelect) and (xIndexToSelect >= 0) then
          result := xTreeSelection[xIndexToSelect]
      end;//
    end;// if not(aMultiselect) then begin
    SetLength(xTreeSelection, 0)
  end;// procedure GetNextNodeSameLevel(aNode: PVirtualNode; aUp: boolean);
  
begin
  if Sender is TBaseVirtualTree then begin
    xSender := TBaseVirtualTree(Sender);
    if ssAlt in Shift then begin
      case Key of
        // PageUp
        VK_PRIOR, VK_UP: begin
          // Sucht den n�chsten sichtbaren oberen Knoten ...
          xNextNode := GetNextNodeSameLevel(xSender, true, xKeepSelection);
          // ... und selektiert ihn dann
          SetFocusToNode(xSender, xNextNode, xKeepSelection);
          Key := 0;
        end;// VK_PRIOR: begin
        // Page Down
        VK_NEXT, VK_DOWN: begin
          // Sucht den n�chsten sichtbaren unteren Knoten ...
          xNextNode := GetNextNodeSameLevel(xSender, false, xKeepSelection);
          // ... und selektiert ihn dann
          SetFocusToNode(xSender, xNextNode, xKeepSelection);
          Key := 0;
        end;// VK_NEXT : begin
      end;// case Key of
    end;// if ssCtrl in Shift then begin
  end;// if Sender is TBaseVirtualTree then begin
end;// TfrmMainWindow.TreeKeyDown cat:No category

end.







































































