(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MethodFixFrame.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Frame f�r die Darstellung der Settings der "Absoluter Grenzwert" Methode
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 08.05.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit MethodFixFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MethodBaseFrame, StdCtrls, mmCheckBox, mmStaticText, ExtCtrls, mmImage,
  QOKernel, mmPanel, mmEdit, mmLabel, QOMethods, mmNumCtrl, IvDictio,
  IvMulti, IvEMulti, mmTranslator, QODef, CalcMethods, mmStringList, mmMemo,
  mmBevel;

type
  (*: Klasse:        TfrmMethodFixFrame
      Vorg�nger:     TfrmMethodBaseFrame
      Kategorie:     View
      Kurzbeschrieb: Frame f�r die Darstellung der Settings der "Absoluter Grenzwert" Methode 
      Beschreibung:  
                     - *)
  TfrmMethodFixFrame = class(TfrmMethodBaseFrame)
    edNumShifts: TmmEdit;
    mmLabel2: TmmLabel;
    mShiftDescr: TmmLabel;
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 �bernimmt alle Einstellungen in das �bergebene Objekt 
    function ApplySettings(aMethod: TQOMethod): Boolean; override;
    //1 Typecast f�r die zugewiesene Methode 
    function FixBoundMethod: TQOFixBound;
    //1 True, wenn die Methode editiert wurde 
    function MethodChanged: Boolean; override;
    //1 Versetzt den Frame in den ReadOnly Modus 
    procedure SetReadOnly; override;
    procedure _SetGUIValues; override;
  end;
  

implementation
uses
  SettingsReader, QOGUIShared, mmcs, JclMath, LoepfeGlobal;
{$R *.DFM}

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmMethodFixFrame
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmMethodFixFrame.Create(aOwner: TComponent);
begin
  (* Der Methodentyp muss vor dem inherited Aufruf definiert werden
     da anhand des Methodentyps der angezeigte Text bestimmt wird *)
  mMethodType := cmFix;

//: ----------------------------------------------
  inherited Create(aOwner);
end;// TfrmMethodFixFrame.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmMethodFixFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmMethodFixFrame.Destroy;
begin
  inherited Destroy;
end;// TfrmMethodFixFrame.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           ApplySettings
 *  Klasse:           TfrmMethodFixFrame
 *  Kategorie:        No category 
 *  Argumente:        (aMethod)
 *
 *  Kurzbeschreibung: �bernimmt alle Einstellungen in das �bergebene Objekt
 *  Beschreibung:     
                      Nachfahren �berschreiben diese Methode.
                      Die Methode "SaveSettings" ruft die Methode "AplySettings" 
                      auf um die Einstellungen zu speichern. Die Methode wird 
                      ausserdem von der G�ligkeitspr�fung verwendet um die 
                      Plausibilit�t der eingegebenen Parameter zu �berpr�fen.
 --------------------------------------------------------------------*)
function TfrmMethodFixFrame.ApplySettings(aMethod: TQOMethod): Boolean;
begin
  Result := inherited ApplySettings(aMethod);

//: ----------------------------------------------
  TQOFixBound(aMethod).ShiftCount := edNumShifts.AsInteger;
  //TQOFixBound(aMethod).LimitValue := edBorder.Value;
  
  try
    // Parameter f�r den CalcMethodFrame zusammensuchen
    with mMethodParam do
      TQOFixBound(aMethod).LimitValue := MMStrToFloat(ValueDef(cFixBorderLimitValue, 0));
  except
    // Exception abfangen, wenn nur ein '-' Zeichen eingegeben wurde
    TQOFixBound(aMethod).LimitValue := 0;
  end;//try except
end;// TfrmMethodFixFrame.ApplySettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           FixBoundMethod
 *  Klasse:           TfrmMethodFixFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Typecast f�r die zugewiesene Methode
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmMethodFixFrame.FixBoundMethod: TQOFixBound;
begin
  result := nil;
  
  if assigned(Method) then
    result := TQOFixBound(FMethod);
end;// TfrmMethodFixFrame.FixBoundMethod cat:No category

//:-------------------------------------------------------------------
(*: Member:           MethodChanged
 *  Klasse:           TfrmMethodFixFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: True, wenn die Methode editiert wurde
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmMethodFixFrame.MethodChanged: Boolean;
begin
  Result := inherited MethodChanged;

//: ----------------------------------------------
  result := result or (edNumShifts.AsInteger <> FixBoundMethod.ShiftCount);
  
  //result := result or (not(FloatsEqual(FixBoundMethod.LimitValue, edBorder.Value)));
  
  // Parameter f�r den CalcMethodFrame zusammensuchen
  with mMethodParam do begin
    try
      result := result or (not(FloatsEqual(FixBoundMethod.LimitValue, MMStrToFloat(ValueDef(cFixBorderLimitValue, 0)))));
    except
      // Exception abfangen, wenn nur ein '-' Zeichen eingegeben wurde
      result := result or (not(FloatsEqual(FixBoundMethod.LimitValue, 0)));
    end;//try except
  end;// with mMethodParam do begin
end;// TfrmMethodFixFrame.MethodChanged cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetReadOnly
 *  Klasse:           TfrmMethodFixFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Versetzt den Frame in den ReadOnly Modus
 *  Beschreibung:     
                      Der RO-Modus wird gebraucht, wenn die Settings lediglich
                      angezeigt werden (Analyse).
 --------------------------------------------------------------------*)
procedure TfrmMethodFixFrame.SetReadOnly;
begin
  inherited SetReadOnly;

//: ----------------------------------------------
  SetEditReadOnly(edNumShifts);
end;// TfrmMethodFixFrame.SetReadOnly cat:No category

//:-------------------------------------------------------------------
procedure TfrmMethodFixFrame._SetGUIValues;
begin
  edNumShifts.Value := FixBoundMethod.ShiftCount;
  //edBorder.Text     := FloatToStr(FixBoundMethod.LimitValue);
  
  //laLenBase.Caption := GetLenBaseString(FixBoundMethod.LenBase, FCalcMode);
  
  // Parameter f�r den CalcMethodFrame zusammensuchen
  mMethodParam.Values[cFixBorderLimitValue] := MMFloatToStr(FixBoundMethod.LimitValue);
end;// TfrmMethodFixFrame._SetGUIValues cat:No category


end.








