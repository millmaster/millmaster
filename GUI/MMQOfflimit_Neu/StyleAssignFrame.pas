(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: StyleAssignFrame.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Frame der verwendet wird um einem Artikel einen Alarm zuzuordnen. (Alternative
|                 Ansicht im Zuordnungstree.
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 17.07.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit StyleAssignFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmEdit, VirtualTrees, mmVirtualStringTree, mmStaticText,
  Buttons, mmSpeedButton, ExtCtrls, mmPanel, ComCtrls, mmPageControl, QOGUIShared,
  IvDictio, IvMulti, IvEMulti, mmTranslator;

type
  (*: Klasse:        TfrmStyleAssignFrame
      Vorg�nger:     TFrame
      Kategorie:     View
      Kurzbeschrieb: - 
      Beschreibung:  
                     - *)
  TfrmStyleAssignFrame = class (TFrame)
    bAddAlarm: TmmSpeedButton;
    edIncrementalAlarm: TmmEdit;
    mAlarmList: TmmVirtualStringTree;
    mButtonPanel: TmmPanel;
    mIncrementalAlarmPanel: TmmPanel;
    mmPanel2: TmmPanel;
    mmTranslator1: TmmTranslator;
    //1 F�gt die selektierten Alarme zu den selektierten Artikeln hinzu 
    procedure bAddAlarmClick(Sender: TObject);
    //1 Stellt das Editfeld auf Eingabe oder Anzeige um 
    procedure edIncrementalAlarmEnterExit(Sender: TObject);
    //1 Selektiert den Knoten der dem Text entspricht 
    procedure edIncrementalStyleChange(Sender: TObject);
    //1 Gibt den ImageIndex des selektierten Objektes zur�ck 
    procedure mAlarmListGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: 
      TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
    //1 - 
    procedure mAlarmListGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: 
      TVSTTextType; var CellText: WideString);
    //1 Passt die Breite der Spalten an die Breite des Trees an 
    procedure mAlarmListResize(Sender: TObject);
    //1 Passt die Gr�sse des Editfeldes an die neue Gr�sse an 
    procedure mIncrementalAlarmPanelResize(Sender: TObject);
    //1 �bernimmt alle �bersetungen die nicht automatisch "passieren" 
    procedure mmTranslator1AfterTranslate(translator: TIvCustomTranslator);
    //1 Sortierfunktion f�r das Tree (Callback) 
    procedure TreeCompareNodes(Sender: TBaseVirtualTree; Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: 
      Integer);
    //1 Sortiert nach der geklickten Spalte 
    procedure TreeHeaderClick(Sender: TVTHeader; Column: TColumnIndex; Button: TMouseButton; Shift: TShiftState; X, Y: 
      Integer);
  private
    FOnAddItems: TOnAddItems;
    FTreeImages: TImageList;
    mInitialized: Boolean;
    //1 Zugriffsmethode f�r TreeImages 
    procedure SetTreeImages(Value: TImageList);
  protected
    //1 Wird aufgerufen, wenn Alarme zugeordnet werden sollen 
    procedure DoAddItems(Sender: TBaseVirtualTree);
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Initialisiert die Sicht (F�llt die Trees von der DB) 
    procedure Init;
    //1 Bilderliste aus der sich die Trees bedienen (von der Applikation zur Verf�gung gestellt) 
    property TreeImages: TImageList write SetTreeImages;
  published
    //1 Wird gefeuert, wenn ein Alarm zugeordet werden soll 
    property OnAddItems: TOnAddItems read FOnAddItems write FOnAddItems;
  end;
  
implementation
{$R *.DFM}
uses
  QOSettings;

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmStyleAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmStyleAssignFrame.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

//: ----------------------------------------------
  SetIncSearchControlState(edIncrementalAlarm);

//: ----------------------------------------------
  mInitialized := false;
  
  // Die Trees initialisieren
  mAlarmList.NodeDataSize := SizeOf(TTreeData);
end;// TfrmStyleAssignFrame.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmStyleAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmStyleAssignFrame.Destroy;
begin
  inherited Destroy;
end;// TfrmStyleAssignFrame.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           bAddAlarmClick
 *  Klasse:           TfrmStyleAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: F�gt die selektierten Alarme zu den selektierten Artikeln hinzu
 *  Beschreibung:     
                      In Wahrheit werden nicht die Alarme den Artikeln zugewiesen, sondern die
                      Artikel den Alarmen.
 --------------------------------------------------------------------*)
procedure TfrmStyleAssignFrame.bAddAlarmClick(Sender: TObject);
begin
  DoAddItems(mAlarmList);
end;// TfrmStyleAssignFrame.bAddAlarmClick cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoAddItems
 *  Klasse:           TfrmStyleAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn Alarme zugeordnet werden sollen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmStyleAssignFrame.DoAddItems(Sender: TBaseVirtualTree);
begin
  if Assigned(FOnAddItems) then FOnAddItems(Sender);
end;// TfrmStyleAssignFrame.DoAddItems cat:No category

//:-------------------------------------------------------------------
(*: Member:           edIncrementalAlarmEnterExit
 *  Klasse:           TfrmStyleAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Stellt das Editfeld auf Eingabe oder Anzeige um
 *  Beschreibung:     
                      Das Editfeld zeigt einen Hilfetext an, wenn kein Suchtext 
                      eingegeben wurde und wenn das Editfeld nicht den Folus hat.
 --------------------------------------------------------------------*)
procedure TfrmStyleAssignFrame.edIncrementalAlarmEnterExit(Sender: TObject);
begin
  SetIncSearchControlState(edIncrementalAlarm);
end;// TfrmStyleAssignFrame.edIncrementalAlarmEnterExit cat:No category

//:-------------------------------------------------------------------
(*: Member:           edIncrementalStyleChange
 *  Klasse:           TfrmStyleAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Selektiert den Knoten der dem Text entspricht
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmStyleAssignFrame.edIncrementalStyleChange(Sender: TObject);
begin
  mAlarmList.FindString(edIncrementalAlarm.Text, 0, [foPartial, foFirstNode, foFromBegin, foSelect]);
end;// TfrmStyleAssignFrame.edIncrementalStyleChange cat:No category

//:-------------------------------------------------------------------
(*: Member:           Init
 *  Klasse:           TfrmStyleAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Initialisiert die Sicht (F�llt die Trees von der DB)
 *  Beschreibung:     
                      Die Initialisierung muss nach dem Create ausgef�hrt werden, wenn alle 
                      Komponenten komplet erzeugt sind.
 --------------------------------------------------------------------*)
procedure TfrmStyleAssignFrame.Init;
var
  i: Integer;
begin
  for i := 0 to TQOAlarms.Instance.AlarmCount - 1 do
    AddVSTStructure(mAlarmList, nil, TQOAlarms.Instance.Alarms[i], []);

//: ----------------------------------------------
  mInitialized := true;
end;// TfrmStyleAssignFrame.Init cat:No category

//:-------------------------------------------------------------------
(*: Member:           mAlarmListGetImageIndex
 *  Klasse:           TfrmStyleAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Kind, Column, Ghosted, ImageIndex)
 *
 *  Kurzbeschreibung: Gibt den ImageIndex des selektierten Objektes zur�ck
 *  Beschreibung:     
                      Die verwendete Imagelist wird von der Applikation �ber die Eigenschaft
                      "TreeImages" gesetzt.
 --------------------------------------------------------------------*)
procedure TfrmStyleAssignFrame.mAlarmListGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind;
  Column: TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
var
  xQOInstance: TObject;
  xVisualizeInterface: IQOVisualize;
begin
  // Den ImageIndex �ber das Interface abfragen
  xQOInstance := GetNodeObject(Sender, Node);
  
  if Kind <> ikOverlay then begin
    if assigned(xQOInstance) then begin
      // Jetzt das Interface abfragen
      if xQOInstance.GetInterface(IQOVisualize, xVisualizeInterface) then
        // ImageIndex wird vom Interface geliefert
        ImageIndex := xVisualizeInterface.ImageIndex[ikAssignNormal];
    end;// if assigned(xQOInstance) then begin
  end;// if Kind <> ikOverlay then begin
end;// TfrmStyleAssignFrame.mAlarmListGetImageIndex cat:No category

//:-------------------------------------------------------------------
(*: Member:           mAlarmListGetText
 *  Klasse:           TfrmStyleAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Column, TextType, CellText)
 *
 *  Kurzbeschreibung: -
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmStyleAssignFrame.mAlarmListGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; 
  TextType: TVSTTextType; var CellText: WideString);
begin
  if assigned(GetNodeObject(mAlarmList, Node)) and (Column = 0) then
    CellText := TQOAlarm(GetNodeObject(mAlarmList, Node)).QOVisualize.Caption
  else
    CellText := '';
end;// TfrmStyleAssignFrame.mAlarmListGetText cat:No category

//:-------------------------------------------------------------------
(*: Member:           mAlarmListResize
 *  Klasse:           TfrmStyleAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Passt die Breite der Spalten an die Breite des Trees an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmStyleAssignFrame.mAlarmListResize(Sender: TObject);
begin
  // Breite der ersten Spalte bestimmen
  mAlarmList.Header.Columns[0].width := mAlarmList.ClientWidth;
end;// TfrmStyleAssignFrame.mAlarmListResize cat:No category

//:-------------------------------------------------------------------
(*: Member:           mIncrementalAlarmPanelResize
 *  Klasse:           TfrmStyleAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Passt die Gr�sse des Editfeldes an die neue Gr�sse an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmStyleAssignFrame.mIncrementalAlarmPanelResize(Sender: TObject);
begin
  edIncrementalAlarm.Width  := mIncrementalAlarmPanel.ClientWidth;
  edIncrementalAlarm.Height := mIncrementalAlarmPanel.ClientHeight;
end;// TfrmStyleAssignFrame.mIncrementalAlarmPanelResize cat:No category

//:-------------------------------------------------------------------
(*: Member:           mmTranslator1AfterTranslate
 *  Klasse:           TfrmStyleAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (translator)
 *
 *  Kurzbeschreibung: �bernimmt alle �bersetungen die nicht automatisch "passieren"
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmStyleAssignFrame.mmTranslator1AfterTranslate(translator: TIvCustomTranslator);
begin
  mAlarmList.Header.Columns[0].Text := rsAlarms;
  SetIncSearchControlStateText(edIncrementalAlarm);
end;// TfrmStyleAssignFrame.mmTranslator1AfterTranslate cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetTreeImages
 *  Klasse:           TfrmStyleAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r TreeImages
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmStyleAssignFrame.SetTreeImages(Value: TImageList);
begin
  FTreeImages := Value;

//: ----------------------------------------------
  // Trees
  mAlarmList.Images := FTreeImages;
  
  // Header
  mAlarmList.Header.Images := FTreeImages;
end;// TfrmStyleAssignFrame.SetTreeImages cat:No category

//:-------------------------------------------------------------------
(*: Member:           TreeCompareNodes
 *  Klasse:           TfrmStyleAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node1, Node2, Column, Result)
 *
 *  Kurzbeschreibung: Sortierfunktion f�r das Tree (Callback)
 *  Beschreibung:     
                      Diese Funktion wird vom Tree aufgerufen, wenn eine Sortiuerung
                      stattfinded (Funktion Sort()).
 --------------------------------------------------------------------*)
procedure TfrmStyleAssignFrame.TreeCompareNodes(Sender: TBaseVirtualTree; Node1, Node2: PVirtualNode; Column: 
  TColumnIndex; var Result: Integer);
var
  xItem1: TQOAssign;
  xItem2: TQOAssign;
begin
  xItem1 := nil;
  xItem2 := nil;
  
  if assigned(GetNodeObject(Sender, Node1)) then
    xItem1 := TQOAssign(GetNodeObject(Sender, Node1));
  
  if assigned(GetNodeObject(Sender, Node2)) then
    xItem2 := TQOAssign(GetNodeObject(Sender, Node2));
  
  if assigned(xItem1) and assigned(xItem2) then begin
    case Column of
      // Name
      0: begin
        result := ANSICompareText(xItem1.QOVisualize.Caption, xItem2.QOVisualize.Caption);
      end;// 0: begin
    end;// case Column of
  end;// if assigned(xItem1) and assigned(xItem2) then begin
end;// TfrmStyleAssignFrame.TreeCompareNodes cat:No category

//:-------------------------------------------------------------------
(*: Member:           TreeHeaderClick
 *  Klasse:           TfrmStyleAssignFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Column, Button, Shift, X, Y)
 *
 *  Kurzbeschreibung: Sortiert nach der geklickten Spalte
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmStyleAssignFrame.TreeHeaderClick(Sender: TVTHeader; Column: TColumnIndex; Button: TMouseButton; Shift: 
  TShiftState; X, Y: Integer);
begin
  if Column = Sender.SortColumn then begin
    if sender.SortDirection = sdAscending then
      sender.SortDirection := sdDescending
    else
      Sender.SortDirection := sdAscending;
  end else begin
    sender.SortDirection := sdAscending
  end;// if Column = Sender.Header.SortColumn then begin
  
  Sender.SortColumn := Column;
  
  Sender.TreeView.SortTree(Column, Sender.SortDirection, true);
end;// TfrmStyleAssignFrame.TreeHeaderClick cat:No category


end.




