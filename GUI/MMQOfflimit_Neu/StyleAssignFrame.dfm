object frmStyleAssignFrame: TfrmStyleAssignFrame
  Left = 0
  Top = 0
  Width = 320
  Height = 240
  TabOrder = 0
  object mmPanel2: TmmPanel
    Left = 47
    Top = 0
    Width = 273
    Height = 240
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object mAlarmList: TmmVirtualStringTree
      Left = 0
      Top = 21
      Width = 273
      Height = 219
      Align = alClient
      DragMode = dmAutomatic
      DragType = dtVCL
      Header.AutoSizeIndex = 0
      Header.Font.Charset = DEFAULT_CHARSET
      Header.Font.Color = clWindowText
      Header.Font.Height = -11
      Header.Font.Name = 'MS Sans Serif'
      Header.Font.Style = []
      Header.Options = [hoColumnResize, hoDrag, hoShowImages, hoShowSortGlyphs, hoVisible]
      Header.SortDirection = sdDescending
      Indent = 2
      TabOrder = 0
      TreeOptions.MiscOptions = [toAcceptOLEDrop, toFullRepaintOnResize, toGridExtensions, toInitOnSave, toToggleOnDblClick, toWheelPanning]
      TreeOptions.PaintOptions = [toHideFocusRect, toShowButtons, toShowDropmark, toShowRoot, toThemeAware, toUseBlendedImages]
      TreeOptions.SelectionOptions = [toFullRowSelect, toMultiSelect]
      OnGetText = mAlarmListGetText
      OnGetImageIndex = mAlarmListGetImageIndex
      OnResize = mIncrementalAlarmPanelResize
      Columns = <
        item
          Position = 0
          WideText = '(*)Alarm'
        end>
    end
    object mIncrementalAlarmPanel: TmmPanel
      Left = 0
      Top = 0
      Width = 273
      Height = 21
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      OnResize = mAlarmListResize
      object edIncrementalAlarm: TmmEdit
        Left = 0
        Top = 0
        Width = 225
        Height = 21
        Color = clWindow
        TabOrder = 0
        Visible = True
        OnChange = edIncrementalStyleChange
        OnEnter = edIncrementalAlarmEnterExit
        OnExit = edIncrementalAlarmEnterExit
        AutoLabel.LabelPosition = lpLeft
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
    end
  end
  object mButtonPanel: TmmPanel
    Left = 0
    Top = 0
    Width = 47
    Height = 240
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    object bAddAlarm: TmmSpeedButton
      Left = 11
      Top = 40
      Width = 24
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        92000000424D9200000000000000760000002800000007000000070000000100
        0400000000001C00000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888808808880
        08808800088080000880880008808880088088880880}
      ParentFont = False
      Visible = True
      OnClick = bAddAlarmClick
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    OnAfterTranslate = mmTranslator1AfterTranslate
    Left = 231
    Top = 64
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
end
