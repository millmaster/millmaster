(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QOSettings.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Basisklassen f�r die Organisation der Alarme
|
| Info..........: 
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.04.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit QOSettings;

interface

uses
  AdoDBAccess, 
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, ClassDef, IcXMLParser,
  ActiveX, ADODB_TLB, QOValues, LoepfeGlobal;

type
  // Benachrichtigungs Methoden
  (* Beim Einf�gen einer neuen Methode auch die Namen anpassen
     siehe 'GetAdviceMethodString' in der Unit QOGUIShared' *)
  TAdviceMethod = (
    amMonitor, 
    amPrinter, 
    amPager, 
    amEmail, 
    amSMS
  );// TAdviceMethod
  TAdviceMethodSet = Set of TAdviceMethod;
  
  TMethodShowDetail = (
    msdDefault, 
    msdNone, 
    msdFull
  );// TMethodShowDetail 
  
  // Berechnungsmethoden
  TCalcMethod = (
    cmBound, 
    cmAVG
  );// TCalcMethod
resourcestring
  rsBoundName = '(*)Absoluter Grenzwert';//ivlm
  rsAVGName   = '(*)Prozentuale Abweichung';//ivlm
const
  (* ACHTUNG! Wenn auf die Strings aus dem Konstantenarray zugegriffen wird
              muss dies immer unter der Verwendung der funktion Translate()
              geschehen *)
  cCalcMethodNames: Array[TCalcMethod] of string = (rsBoundName, rsAVGName);
  
type  
  (* Array in das Grenzwerte abgelegt werden, damit sie nicht 
     f�r mehrere Zugriffe mehrfach beerechnet werden m�ssen.
     Es muss f�r jede Schicht ein eigener Grenzwert berechnet (ausser cmBound).*)
  TLimitValues = Array of extended;
  
  (* Array in das die einzelnen Werte der Stichprobe abgelegt werden. Da die
     Datenwerte in einem Array abgelegt sind, k�nnen in dieser Datenmenge relativ
     einfach einzelne Datenwerte von der weiteren Berechnung ausgeschlossen werden. *)
  TSampleArray     = Array of extended;
  // Die produzierte L�nge f�r jedes Sample 
  TLenArray        = Array of extended;
  // SFI Count f�r jedes Sample
  TSFICntArray     = Array of extended;
  // Adjust Base f�r jedes Sample
  TAdjBaseArray    = Array of extended;
  // Adjust Base Count f�r jedes Sample
  TAdjBaseCntArray = Array of extended;
const
  cMethodColWith = 30;
  
  
const
  // Minimale Breite der ersten Spalte des SettingsTree (mSettingsTreeResize)
  cMinNameSettingsColWidth = 130;
  
  // Nummer der Spalte mit dem Namen des Alarms im Settings Tree
  cSettingsNameCol = 0;
  
type
  TQOImageKind = (
    ikSettingsNormal,
    ikAssignNormal,
    ikAnalyzeNormal
  );// TQOImageKind
  
  // Array f�r die Aufnahme des Mappings Maschine - Artikel
  TActiveProdGroupArray = Array of Array [0..1] of integer;
const
  cMachineIDCol = 0;
  cStyleIDCol   = 1;

              
type  
  // L�schzustand eines Eintrages
  TDeleteState = (dsValid, dsPending, dsDeleted);
  
  // Klassentyp f�r die Methoden
  TQOMethodClass = Class of TQOMethod;
  TQOAssignClass = Class of TQOAssign;
  
  TQOVisualizeImpl = class;
  TQOMethods = class;
  TQOMethod = class;
  TQOItems = class;
  TQOItem = class;
  TQOAssigns = class;
  TQOAssign = class;
  TQOAlarms = class;
  TQOAlarm = class;
  TLimitValueList = class;
  TStyleLimit = class;
  (*: Klasse:        ISaveDefToDB
      Vorg�nger:     IUnknown
      Kategorie:     interface
      Kurzbeschrieb: Speichert ein Element auf der Datenbank 
      Beschreibung:  
                     - *)
  ISaveDefToDB = interface (IUnknown)
    ['{9888E2D3-A353-419D-BAD5-F3A20D2117EE}']
    //1 Speichert die Settings des Alarms auf der Datenbank 
    function SaveDefToDB: Boolean;
  end;
  
  (*: Klasse:        IQOVisualize
      Vorg�nger:     IUnknown
      Kategorie:     No category
      Kurzbeschrieb: Interface f�r die Visualisierung der Klassen in einem GUI Control 
      Beschreibung:  
                     Es werden die elementaren Properties die f�r die visualisierung
                     notwendig sind, zur Verf�gung gestellt. *)
  IQOVisualize = interface (IUnknown)
    ['{E7434EF3-741D-4F4B-AC0F-C27D3257A3D8}']
    //1 True, wenn das Objekt aktiv ist 
    function GetActive: Boolean; stdcall;
    //1 Zugriffsmethode f�r Caption 
    function GetCaption: String; stdcall;
    //1 Zugriffsmethode f�r ImageIndex 
    function GetImageIndex(aIndex: TQOImageKind): Integer; stdcall;
    //1 Zugriffsmethode f�r Caption 
    procedure SetCaption(const Value: String); stdcall;
    //1 True, wenn das Objekt aktiv ist 
    property Active: Boolean read GetActive;
    //1 Name unter dem ein Eintrag in einem GUI Control angezeigt wird 
    property Caption: String read GetCaption write SetCaption;
    //1 Index f�r das Icon f�r die Visualisierung 
    property ImageIndex[aIndex: TQOImageKind]: Integer read GetImageIndex;
  end;
  
  //1 Wird gefeuert, wenn eine String Eigenschaft ge�ndert wurde 
  TStringPropChangeEvent = procedure (Sender: TObject; aNewString :string; aOldString: string; aAllow: boolean) of 
    object;
  //1 Wird gefeuert, wenn eine Integer Eigenschaft �ndert 
  TGetImageIndexEvent = procedure (Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer) of object;
  //1 Wird aufgerufen, wenn die Caption abgefragt wird 
  TOnGetCaption = procedure (Sender: TObject; var aCaption: string) of object;
  //1 Abfrage ob ein Objekt aktiviert ist 
  TGetActive = function (Sender: TObject): Boolean of object;
  (*: Klasse:        TQOIntetrfacedObject
      Vorg�nger:     TPersistent
      Kategorie:     Controller
      Kurzbeschrieb: Implementiert nur IUnknown ohne Referenzz�hlung 
      Beschreibung:  
                     - *)
  TQOIntetrfacedObject = class (TPersistent, IUnknown)
  public
    //1 IUnknown 
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    //1 IUnknown 
    function _AddRef: Integer; stdcall;
    //1 IUnknown 
    function _Release: Integer; stdcall;
  end;
  
  (*: Klasse:        TImageIndexHandler
      Vorg�nger:     TObject
      Kategorie:     No category
      Kurzbeschrieb: Stellt die ImageIndexe f�r die Objekte global bereit 
      Beschreibung:  
                     Die Objekte fragen die ImageIndexe ab die von der Anwendung gesetzt werden. *)
  TImageIndexHandler = class (TObject)
  private
    FActiveOverlay: Integer;
    FAlarmAnalyze: Integer;
    FAlarmAssign: Integer;
    FAlarmInactive: Integer;
    FAlarmSet: Integer;
    FAlarmSettings: Integer;
    FDataItem: Integer;
    FFunctionImageIndex: Integer;
    FMachine: Integer;
    FMachineSW: Integer;
    FMatrixTrendEditImageIndex: Integer;
    FMatrixTrendImageIndex: Integer;
    FNodeObject: Integer;
    FStyle: Integer;
    FStyleSW: Integer;
  protected
    //1 singleton 
    constructor CreateInstance;
    //1 singleton 
    class function AccessInstance(Request: Integer): TImageIndexHandler;
  public
    //1 singleton 
    constructor Create;
    //1 singleton 
    destructor Destroy; override;
    //1 singleton 
    class function Instance: TImageIndexHandler;
    //1 singleton 
    class procedure ReleaseInstance;
    //1 Image das angibt, dass eine Zuordnung (Maschine oder Artikel) aktiv ist 
    property ActiveOverlay: Integer read FActiveOverlay write FActiveOverlay;
    //1 Icon f�r den Alarm im AnalyzeTree 
    property AlarmAnalyze: Integer read FAlarmAnalyze write FAlarmAnalyze;
    //1 ImageIndex f�r die Zuordnung(Alarm) 
    property AlarmAssign: Integer read FAlarmAssign write FAlarmAssign;
    //1 ImageIndex f�r einen Alarm ohne Benachrichtigungsmethoden 
    property AlarmInactive: Integer read FAlarmInactive write FAlarmInactive;
    //1 Image das f�pr eine Schicht im Analyze Tree benuzt wird 
    property AlarmSet: Integer read FAlarmSet write FAlarmSet;
    //1 ImageIndex f�r die Settings (Alarm) 
    property AlarmSettings: Integer read FAlarmSettings write FAlarmSettings;
    //1 ImageIndex f�r die DataItems 
    property DataItem: Integer read FDataItem write FDataItem;
    //1 ImageIndex f�r die Komposit DataItems 
    property FunctionImageIndex: Integer read FFunctionImageIndex write FFunctionImageIndex;
    //1 ImageIndex f�r die Maschine 
    property Machine: Integer read FMachine write FMachine;
    //1 ImageIndex f�r die Maschine wenn sie nicht in Produktion ist 
    property MachineSW: Integer read FMachineSW write FMachineSW;
    //1 ImageIndex f�r die selber definierten Matrix Klassen 
    property MatrixTrendEditImageIndex: Integer read FMatrixTrendEditImageIndex write FMatrixTrendEditImageIndex;
    //1 ImageIndex f�r die MatrixDataItems 
    property MatrixTrendImageIndex: Integer read FMatrixTrendImageIndex write FMatrixTrendImageIndex;
    //1 Image das f�r ein Artikel oder Maschine im AnalyzeTree angezeigt wird 
    property NodeObject: Integer read FNodeObject write FNodeObject;
    //1 ImageIndex f�r den Artikel 
    property Style: Integer read FStyle write FStyle;
    //1 ImageIndex f�r den Artikel wenn er nicht in Produktion ist 
    property StyleSW: Integer read FStyleSW write FStyleSW;
  end;
  
  (*: Klasse:        TQOVisualizeImpl
      Vorg�nger:     TPersistent
      Kategorie:     IQOVisualize
      Kurzbeschrieb: Implementiert das Interface IQOVisualize (ohne Referenzz�hlung) 
      Beschreibung:  
                     F�r jede Klasse die die Schnittstelle "IQOVisualize" verwenden will, kann ein Property vom Typ 
                     "TQOVisualizeImpl" erstellt werden.
                     �ber das Schl�sselwort "implements IQOVisualize" wird der Klasse mitgeteilt, dass alle Anfragen 
                     die das Interface betreffen �ber das aggregierte Objekt vom Typ "TQOVisualize" abgewicklt werden 
                     soll.
                     Der Vorteil liegt darin, dass die Funktionalit�t nur einmal implementiert werden muss. 
                     Das Interface kann nun von jeder Klasse verwendet werden.
                     
                         if xQOObject.GetInterface(IQOVisualize, xObject) then begin
                           .  // Verwenden der Eigenschaften von IQOVisualize
                           .
                         end;
                      *)
  TQOVisualizeImpl = class (TPersistent, IQOVisualize)
  private
    FCaption: String;
    FOnCaptionChange: TStringPropChangeEvent;
    FOnGetActive: TGetActive;
    FOnGetCaption: TOnGetCaption;
    FOnGetImageIndex: TGetImageIndexEvent;
  protected
    //1 Fragt die Caption beim "Eltern-Objekt" ab 
    procedure DoGetCaption(Sender: TObject; var aCaption: string);
  public
    //1 Eigenschaften �bernehmen 
    procedure Assign(Source: TPersistent); override;
    //1 Fragt das Implementierende Objekt ab ob es Aktiv ist oder nicht 
    function GetActive: Boolean; stdcall;
    //1 Zugriffsmethoden f�r Caption 
    function GetCaption: String; stdcall;
    //1 Zugriffsmethode f�r ImageIndex 
    function GetImageIndex(aIndex: TQOImageKind): Integer; stdcall;
    //1 Fragt ab ob ein bestimmtes Interface existiert (IUnknown) 
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    //1 Zugriffsmethode f�r Caption 
    procedure SetCaption(const Value: String); stdcall;
    //1 Referenzz�hlung (IUnknown) 
    function _AddRef: Integer; stdcall;
    //1 Refernzz�hlung (IUnknown) 
    function _Release: Integer; stdcall;
    //1 Wird f�r die Darstellung des Zustands des Implementierenedn Objektes gebraucht 
    property Active: Boolean read GetActive;
    //1 Name unter dem ein Eintrag in einem GUI Control angezeigt wird 
    property Caption: String read GetCaption write SetCaption;
    //1 Index f�r das Icon f�r die Visualisierung 
    property ImageIndex[aIndex: TQOImageKind]: Integer read GetImageIndex;
  published
    //1 Wird gefeuert, wenn die Caption g�ndert wurde 
    property OnCaptionChange: TStringPropChangeEvent read FOnCaptionChange write FOnCaptionChange;
    //1 Fragt nach ob ein Objekt aktiviert ist 
    property OnGetActive: TGetActive read FOnGetActive write FOnGetActive;
    //1 Wird gefeuert, wenn lesend auf die Caption zuggegriffen wird 
    property OnGetCaption: TOnGetCaption read FOnGetCaption write FOnGetCaption;
    //1 Wird gefeuert, wenn lesend auf den ImageIndex zugegriffen wird 
    property OnGetImageIndex: TGetImageIndexEvent read FOnGetImageIndex write FOnGetImageIndex;
  end;
  
  (*: Klasse:        TQOMethods
      Vorg�nger:     TPersistent
      Kategorie:     Kernel
      Kurzbeschrieb: Container f�r Objekte der Klasse TQOMethods 
      Beschreibung:  
                     Liste mit den Methoden f�r die Berechnungsmethoden. Die Liste stellt grunds�tzlich alle verf�gbaren
                     Berechnungsmethoden zur Verf�gung. Die Methoden k�nnen einzeln aktiviert werden. Nicht aktivierte 
                     Methoden
                     werden aber nicht gel�scht, sondern stehen auch bei einem Neustart der Anwendung zur Verf�gung, da 
                     die 
                     Settings in jedem Fall gesichert werden.
                     �ber die Methode MethodByType() wird eine Art Factory verwendet, die die gew�nschte Methode 
                     zur�ckgibt. *)
  TQOMethods = class (TPersistent)
  private
    FFieldName: String;
    FItemId: Integer;
    FProdIDsInOfflimit: TStringList;
    mDataItem: TQOItem;
    //1 Gibt die Anzahl aktivierter Methoden zur�ck 
    function GetActiveMethodCount: Integer;
    //1 �Zugriffsmethode f�r DeleteState 
    procedure SetDeleteState(Value: TDeleteState);
    //1 Zugriffsmethode f�r FieldName 
    procedure SetFieldName(const Value: String);
    //1 ID des zugeh�rigen ITemsf�r die Datenbank 
    procedure SetItemId(Value: Integer);
  protected
    mMethods: TList;
    //1 Anzahl Elemente in der Liste 
    function GetMethodCount: Integer;
    //1 Zugriffsmethode auf ein Element aus der Liste 
    function GetMethods(aIndex: Integer): TQOMethod;
  public
    //1 Konstruktor, wenn kein DataItem mit der Methode assoziiert ist 
    constructor Create; overload; virtual;
    //1 Konstruktor 
    constructor Create(aOwner: TQOItem); reintroduce; overload; virtual;
    //1 Destruktor 
    destructor Destroy; override;
    //1 F�gt ein Element zur Liste hinzu 
    function Add(aMethod: TQOMethod): Integer; virtual;
    //1 Eigenschaften �bernehmen 
    procedure Assign(Source: TPersistent); override;
    //1 L�scht alle Elemente aus der Liste 
    procedure Clear;
    //1 L�scht ein Element aus der Liste 
    procedure Delete(aIndex: integer); virtual;
    //1 L�scht den Datensatz von der Datenbank 
    function DeleteFromDB(aQuery: TNativeAdoQuery): Boolean;
    //1 Wird aufgerufen, wenn ein Alarm berechnet werden soll 
    procedure Evaluate(aBoolOpAnd: boolean);
    //1 Gibt ein Objekt f�r die entsprechende Methode zur�ck 
    function FactoryGetMethod(aMethod: TCalcMethod): TQOMethod;
    //1 Fragt die Methoden ab, ob eine davon im Offlimit ist 
    function GetCaught: Boolean;
    //1 Sammelt von allen Methoden die Anzahl der Schichten 
    function GetMaxShiftCount: Integer;
    //1 Ermittelt die Datenreihe f�r alle Produktionsgruppen die im Offlimit sind. 
    procedure GetOfflimitData(aAlarm: TQOAlarm);
    //1 Gibt die Methode im Container zur�ck 
    function MethodByType(aType: TCalcMethod): TQOMethod;
    //1 Speichert die Settings auf der Datenbank 
    function SaveDefToDB(aItemID: integer): Boolean;
    //1 Gibt die Anzahl aktivierter Methoden zur�ck 
    property ActiveMethodCount: Integer read GetActiveMethodCount;
    //1 Zeigt an ob ein DataItem gel�scht werden soll 
    property DeleteState: TDeleteState write SetDeleteState;
    //1 Name des Datenbank Feldes das die Daten f�r die Methode enth�lt 
    property FieldName: String read FFieldName write SetFieldName;
    //1 ID des zugeh�rigen Items f�r die Datenbank 
    property ItemId: Integer read FItemId write SetItemId;
    //1 Anzahl der Elemente in der Liste 
    property MethodCount: Integer read GetMethodCount;
    //1 Alle Elemente als Array mit indiziertem Zugriff 
    property Methods[aIndex: Integer]: TQOMethod read GetMethods; default;
    //1 ProdIDs die im Offlimit sind 
    property ProdIDsInOfflimit: TStringList read FProdIDsInOfflimit write FProdIDsInOfflimit;
  end;
  
  (*: Klasse:        TQOMethod
      Vorg�nger:     TQOIntetrfacedObject
      Kategorie:     Kernel
      Kurzbeschrieb: Eine Berechnungs Methode  
      Beschreibung:  
                     Die Berechnungmethode verwendet das Startegie Muster um den aktuell g�ltigen
                     Grenzwert zu bestimmen.
                     Die Methode CalculateLimitValue() ist abstrakt definiert und wird in den konkreten
                     Nachfahren implementiert. Das Ermitteln ob ein Wert im Offlimit ist, h�ngt im
                     Wesentlichen davon ab wie der Grenzwert berechnet wird. Deshalb kann diese 
                     Berechnung in die konkrete Klasse verlegt werden. Der Grundalgoritmus ist in
                     der Basisklasse implementiert. *)
  TQOMethod = class (TQOIntetrfacedObject, IQOVisualize, ISaveDefToDB)
  private
    FActive: Boolean;
    FCaught: Boolean;
    FDeleteState: TDeleteState;
    FFieldName: String;
    FGreaterThan: Boolean;
    FID: Integer;
    FItemId: Integer;
    FLenBase: TLenBase;
    FProdIDsInOfflimit: TStringList;
    FQOAlarm: Integer;
    FQOVisualize: TQOVisualizeImpl;
  protected
    FMethodType: TCalcMethod;
    FShiftCount: Integer;
    FShowDetails: TMethodShowDetail;
    mDataItem: TQOItem;
    mLimitValueList: TLimitValueList;
    mProdIDList: TStringList;
    //1 Gibt einen Wert aus dem XML-String zur�ck. 
    function GetAttrValDef(aName: string; aXMLItem: TIcXMLElement; aDef: variant): Variant; virtual;
    //1 Vergleicht den Artikelwert mit der Berechnungsvorschrift 
    function GetIsInOfflimit(aValue: extended; aShiftToCalc, aShiftID, aStyleID: integer): Boolean; virtual;
    //1 Gibt die global eingestellte L�ngenbasis zur�ck 
    function GetLenBase: TLenBase;
    //1 Gibt die Anzahl der Schichten bekannt die f�r die Berechnung des Mittelwertes ben�tigt werden 
    function GetSampleShiftCount: Integer; virtual;
    //1 Wird intern aufgerufen, wenn ein Alarm berechnet werden soll 
    procedure Internal_Evaluate; virtual;
  public
    //1 Konstruktor 
    constructor Create(aOwner: TQOItem); virtual;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Eigenschaften �bernehmen 
    procedure Assign(Source: TPersistent); override;
    //1 Ermittelt den Absoluten Grenzwert von der entsprechenden Methode 
    function CalculateLimitValue(aShiftNumber: integer; aShiftID: integer; aStyleID: integer): Extended; virtual; 
      abstract;
    //1 L�scht den Datensatz von der Datenbank 
    function DeleteFromDB(aQuery: TNativeAdoQuery): Boolean; virtual;
    //1 Wird aufgerufen, wenn ein Alarm berechnet werden soll 
    procedure Evaluate;
    //1 Ermittelt die Datenreihe f�r alle Produktionsgruppen die im Offlimit sind. 
    procedure GetOfflimitData(aAlarm: TQOAlarm); virtual;
    //1 Gibt zur�ck ob alle Parameter mit sinvollen Werten belegt sind 
    function MethodValid: Boolean; virtual;
    //1 Liest die Settings der Methode aus dem XML Stream 
    procedure ReadXMLMethodeSettings(aXMLItem: TIcXMLelement); virtual;
    //1 Speichert die Settings der Methode auf der Datenbank 
    function SaveDefToDB: Boolean;
    //1 Gibt die Methoden Settings als XML formatierten String zur�ck 
    procedure WriteXMLMethodSettings(aXMLItem: TIcXMLElement); virtual;
    //1 True, wenn die Methode aktiviert ist 
    property Active: Boolean read FActive write FActive;
    //1 True, wenn dies Methode im Offlimit ist (wird gesetzt in 'GetOfflimitData') 
    property Caught: Boolean read FCaught write FCaught;
    //1 Zeigt an ob ein DataItem gel�scht werden soll 
    property DeleteState: TDeleteState read FDeleteState write FDeleteState;
    //1 Name des Datenbank Feldes das die Daten f�r die Methode enth�lt 
    property FieldName: String read FFieldName write FFieldName;
    //1 Definiert ob die Verletzung auftritt, wennd er Wert gr�sser ist oder wenn er kleiner ist 
    property GreaterThan: Boolean read FGreaterThan write FGreaterThan;
    //1 ID f�r die Datenbank 
    property ID: Integer read FID write FID;
    //1 ID des zugeh�rigen Items f�r die Datenbank 
    property ItemId: Integer read FItemId write FItemId;
    //1 L�ngen Basis f�r die Berechnung des Grenzwertes 
    property LenBase: TLenBase read FLenBase write FLenBase;
    //1 Typ der Methode (ReadOnly) 
    property MethodType: TCalcMethod read FMethodType;
    //1 ProdIDs die im Offlimit sind 
    property ProdIDsInOfflimit: TStringList read FProdIDsInOfflimit write FProdIDsInOfflimit;
    //1 Alarm der diese Methode (indirekt) beherbergt 
    property QOAlarm: Integer read FQOAlarm write FQOAlarm;
    //1 Implementiert die Funktionalit�t von IQOVisualize 
    property QOVisualize: TQOVisualizeImpl read FQOVisualize implements IQOVisualize;
    //1 Anzahl der Schichten die f�r eine Verletzung ausschlaggebend sind 
    property ShiftCount: Integer read FShiftCount write FShiftCount;
  published
    //1 True, wenn die Methode mit allen Dateils angezeigt werden soll 
    property ShowDetails: TMethodShowDetail read FShowDetails write FShowDetails;
  end;
  
  (*: Klasse:        TQOItems
      Vorg�nger:     TClassDefContainer
      Kategorie:     Kernel
      Kurzbeschrieb: Liste mit den beteiligten Dataitems 
      Beschreibung:  
                     Diese Klasse erbt von TClassDefContainer (Superklassen).
                     TClassDefContainer bietet eine Schnittstelle um 
                     Schnittdaten von der Datenbank zu ermitteln. *)
  TQOItems = class (TClassDefContainer)
  private
    FAlarmId: Integer;
    FItemsLoaded: Boolean;
    FProdIDs: String;
    FProdIDsInOfflimit: TStringList;
    //1 Gibt die Anzahl aktivierter DataItems zur�ck 
    function GetActiveDataItemCount: Integer;
    //1 ID des zugeh�rigen Alarms f�r die Datenbank 
    procedure SetAlarmId(Value: Integer);
    //1 Verteilt die zugewiesenen ProdIDs an die Items 
    procedure SetProdIDs(const Value: String);
    //1 Verteilt Alarm an die Items 
    procedure SetQOAlarm(Value: TQOAlarm);
  protected
    //1 Anzahl der Elemente in der Liste 
    function GetItemCount: Integer;
    //1 Zugriffsmethode auf ein Element aus der Liste 
    function GetItems(aIndex: Integer): TQOItem;
  public
    //1 Konstruktor 
    constructor Create(aPersistent: boolean = false); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 L�scht den Datensatz von der Datenbank 
    function DeleteFromDB(aQuery: TNativeAdoQuery): Boolean;
    //1 Wird aufgerufen, wenn ein Alarm berechnet werden soll 
    procedure Evaluate(aBoolOpAnd: boolean);
    //1 Sammelt von allen Methoden die Anzahl der Schichten 
    function GetMaxShiftCount: Integer;
    //1 Ermittelt die Datenreihe f�r alle Produktionsgruppen die im Offlimit sind. 
    procedure GetOfflimitData(aAlarm: TQOAlarm);
    //1 Gibt die Position des DataItems anhand der �berschrift an 
    function IndexOfCaption(aCaption: string): Integer;
    //1 Liefert den Index des entsprechenden Items 
    function IndexOfItem(aQOItem: TQOItem): Integer;
    //1 L�dt die DataItems von der Datenbank 
    procedure LoadFromDatabase(aDataItemDecoratorClass: TDataItemDecoratorClass = nil); override;
    //1 Gibt die Namen der DataItems als Kommasaparierten String zur�ck 
    function NamesAsString: String;
    //1 Speichert die Settings auf der Datenbank 
    function SaveDefToDB(aAlarmID: integer): Boolean;
    //1 Gibt die Anzahl aktivierter DataItems zur�ck 
    property ActiveDataItemCount: Integer read GetActiveDataItemCount;
    //1 ID des zugeh�rigen Alarms f�r die Datenbank 
    property AlarmId: Integer read FAlarmId write SetAlarmId;
    //1 Anzahl der Elemente in der Liste 
    property ItemCount: Integer read GetItemCount;
    //1 Alle Elemente als Array mit indiziertem Zugriff 
    property Items[aIndex: Integer]: TQOItem read GetItems; default;
    //1 True, wenn die DataItems von der Datenbank geladen wurden 
    property ItemsLoaded: Boolean read FItemsLoaded write FItemsLoaded;
    //1 Produktions IDs die f�r den �bergeordneten Alarm relevant sind 
    property ProdIDs: String read FProdIDs write SetProdIDs;
    //1 ProdIDs die im Offlimit sind 
    property ProdIDsInOfflimit: TStringList read FProdIDsInOfflimit write FProdIDsInOfflimit;
    //1 Alarm der die Items beherbergt 
    property QOAlarm: TQOAlarm write SetQOAlarm;
  end;
  
  (*: Klasse:        TQOItem
      Vorg�nger:     TDataItemDecorator
      Kategorie:     Kernel
      Kurzbeschrieb: Ein einzelnes DataItem 
      Beschreibung:  
                     Ein DataItem repr�sentiert einen einzelnen Qualit�tsaspekt einer Partie. 
                     Das DataItem (z.B. CYTot) kann f�r einzelne Artikel und sogar MAschinen
                     �berwacht werden. Die �berwachungsmethode kann dabei bestimmt werden. Auch
                     Kombinationen von �berwachungsmethoden k�nnen defineirt werden, sowie deren 
                     logischen Verkn�pfungen. So ist es m�glich einen fixen Grenzwert und einen
                     prozentualen Grenzwert mit AND zu verkn�pfen, so dass ein Alarm nur ausgel�st
                     wird, wenn beide Berechnungen eine Verletzung ihrer Grenzwerte ergeben. *)
  TQOItem = class (TDataItemDecorator, ISaveDefToDB, IUnknown, IQOVisualize)
  private
    FAlarmId: Integer;
    FBoolOpAnd: Boolean;
    FCalcMode: Integer;
    FCaught: Boolean;
    FDeleteState: TDeleteState;
    FID: Integer;
    FProdIDs: String;
    FProdIDsInOfflimit: TStringList;
    FQOAlarm: TQOAlarm;
    FQOMethods: TQOMethods;
    FQOVisualize: TQOVisualizeImpl;
    //1 Gibt True zur�ck, wenn mindestens eine Methode aktiviert ist 
    function GetActive: Boolean;
    //1 �Zugriffsmethode f�r DeleteState 
    procedure SetDeleteState(Value: TDeleteState);
    //1 Setzt die ID (Datenbank) des DataItems 
    procedure SetID(Value: Integer);
  protected
    //1 Fragt den ImageIndex f�r die Anzeige ab 
    procedure GetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer);
    //1 Gibt True zur�ck, wenn das Objekt aktiviert ist 
    function OnGetActive(Sender: TObject): Boolean;
    //1 Ver�ndert f�r Matrixklassen den Namen der Angezeigt wird 
    procedure OnGetCaption(Sender: TObject; var aCaption: string);
  public
    //1 Konstruktor 
    constructor Create; override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Eigenschaften �bernehmen 
    procedure Assign(Source: TPersistent); override;
    //1 L�scht den Datensatz von der Datenbank 
    function DeleteFromDB(aQuery: TNativeAdoQuery): Boolean;
    //1 Wird aufgerufen, wenn ein Alarm berechnet werden soll 
    procedure Evaluate;
    //1 Sammelt von allen Methoden die Anzahl der Schichten 
    function GetMaxShiftCount: Integer;
    //1 Ermittelt die Datenreihe f�r alle Produktionsgruppen die im Offlimit sind. 
    procedure GetOfflimitData(aAlarm: TQOAlarm);
    //1 IUnknown 
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    //1 Speichert die Settings des DataItems auf der Datenbank 
    function SaveDefToDB: Boolean;
    //1 IUnknown 
    function _AddRef: Integer; stdcall;
    //1 IUnknown 
    function _Release: Integer; stdcall;
    //1 True, wenn das Item aktiviert ist (Mindestens eine aktivierte Methode) 
    property Active: Boolean read GetActive;
    //1 ID des zugeh�rigen Alarms f�r die Datenbank 
    property AlarmId: Integer read FAlarmId write FAlarmId;
    //1 True, wenn die Methoden AND verkn�pft werden sollen 
    property BoolOpAnd: Boolean read FBoolOpAnd write FBoolOpAnd;
    //1 Kalkulationsmodus f�r das entsprechende DataItem f�r CalcValue() 
    property CalcMode: Integer read FCalcMode write FCalcMode;
    //1 True, wenn dieses DataItem im Offlimit ist 
    property Caught: Boolean read FCaught write FCaught;
    //1 Zeigt an ob ein DataItem gel�scht werden soll 
    property DeleteState: TDeleteState read FDeleteState write SetDeleteState;
    //1 ID f�r die Datenbank 
    property ID: Integer read FID write SetID;
    //1 Produktions IDs die f�r den �bergeordneten Alarm relevant sind 
    property ProdIDs: String read FProdIDs write FProdIDs;
    //1 ProdIDs die im Offlimit sind 
    property ProdIDsInOfflimit: TStringList read FProdIDsInOfflimit write FProdIDsInOfflimit;
    //1 Alarm der das Item beherbergt 
    property QOAlarm: TQOAlarm read FQOAlarm write FQOAlarm;
    //1 Liste mit den definierten Methoden 
    property QOMethods: TQOMethods read FQOMethods;
    //1 Implementiert die Funktionalit�t von IQOVisualize 
    property QOVisualize: TQOVisualizeImpl read FQOVisualize implements IQOVisualize;
  end;
  
  (*: Klasse:        TQODataItem
      Vorg�nger:     TDataItem
      Kategorie:     No category
      Kurzbeschrieb: QOfflimit DataItem f�r die Standard Items 
      Beschreibung:  
                     Die Standard Items werden mit einer eigenen Klasse implementiert. Diese
                     Data Items werden dann mit dem entsprechenden Decorator dekoriert (siehe  Superklassen). *)
  TQODataItem = class (TDataItem)
  protected
    //1 Gibt immer utNone zur�ck da diese Felder in der View definiert sind 
    function GetTableType: TUsedTablesSet; override;
  public
    //1 Gibt die ben�tigten Felddefinitionen f�r das entsprechende DataItem zur�ck 
    function GetSQLSelectFields(aWithSum: boolean = true; aWithAlias:boolean = true): String; override;
  end;
  
  (*: Klasse:        TQOAssigns
      Vorg�nger:     TPersistent
      Kategorie:     Kernel
      Kurzbeschrieb: Liste f�r Gruppierungs Items 
      Beschreibung:  
                     GruppierungsItems k�nnen Artikel oder Maschinen sein. *)
  TQOAssigns = class (TPersistent)
  private
    FParentID: Integer;
    //1 Gibt die Zuordnung (LinkID) als Komma String zur�ck 
    function GetCommaTextByLinkID: String;
    //1 Zugriffsmethode f�r DeleteState 
    procedure SetDeleteState(Value: TDeleteState);
    //1 ID des zugeh�rigen Alarms f�r die Datenbank 
    procedure SetParentID(Value: Integer);
  protected
    mAssigns: TList;
    //1 Anzahl Elemente in der Liste 
    function GetAssignCount: Integer;
    //1 Zugriffsmethode auf ein Element aus der Liste 
    function GetAssigns(aIndex: Integer): TQOAssign;
  public
    //1 Konstruktor 
    constructor Create; virtual;
    //1 Destruktor 
    destructor Destroy; override;
    //1 F�gt ein Element zur Liste hinzu 
    function Add(aAssign: TQOAssign): Integer; virtual;
    //1 Eigenschaften �bernehmen 
    procedure Assign(Source: TPersistent); override;
    //1 L�scht alle Elemente aus der Liste 
    procedure Clear;
    //1 L�scht ein Element aus der Liste 
    procedure Delete(aIndex: integer); virtual;
    //1 L�scht den Datensatz von der Datenbank 
    function DeleteFromDB(aQuery: TNativeAdoQuery): Boolean;
    //1 Pr�ft ob eine Zuordnung in der Liste erfasst ist 
    function IndexOf(aQOAssign: TQOAssign): Integer; virtual;
    //1 Sucht einen Artikel in der Liste anhand der ID 
    function IndexOfLinkID(aLinkID: integer): Integer;
    //1 True, wenn die beinhaltenden Elemente identisch sind 
    function IsSameList(aSource: TQOAssigns): Boolean;
    //1 Speichert die Settings auf der Datenbank 
    function SaveDefToDB(aParentID: integer): Boolean;
    //1 Anzahl der Elemente in der Liste 
    property AssignCount: Integer read GetAssignCount;
    //1 Alle Elemente als Array mit indiziertem Zugriff 
    property Assigns[aIndex: Integer]: TQOAssign read GetAssigns; default;
    //1 Gibt die Zuordnung (LinkID) als Komma String zur�ck 
    property CommaTextByLinkID: String read GetCommaTextByLinkID;
    //1 Zeigt an ob ein DataItem gel�scht werden soll 
    property DeleteState: TDeleteState write SetDeleteState;
    //1 ID des �bergeordneten Elementes (Alarm oder Style) f�r die Datenbank 
    property ParentID: Integer read FParentID write SetParentID;
  end;
  
  (*: Klasse:        TQOAssign
      Vorg�nger:     TQOIntetrfacedObject
      Kategorie:     Kernel
      Kurzbeschrieb: Basisklasse f�r Gruppierungs Items 
      Beschreibung:  
                     Diese Klasse repr�sentiert immer einen Artikel oder eine Maschiene.
                     Einem Alarm wid ein Artikel zugeordnet um zu kennzeichnen, dass dieser
                     Artikel vom entsprechenden Alarm �berwacht werden soll. Die �berwachung
                     kann noch weiter eingeschr�nkt werden in dem eine oder mehrere Maschinen 
                     einem Artikel zugeordnet wird. Ist keine Maschine zugeordnet, werden 
                     automatisch alle Maschinen �berwacht. Ist mindesten eine Maschine 
                     zugeordnet, dann werden nur die zugeordneten Maschinen �berwacht - sofern
                     mindestens eine Partie des �bergeordneten Artikels auf dieser Maschine
                     produziert. *)
  TQOAssign = class (TQOIntetrfacedObject, IQOVisualize, ISaveDefToDB)
  protected
    FDeleteState: TDeleteState;
    FID: Integer;
    FLinkID: Integer;
    FParentID: Integer;
    FQOAssigns: TQOAssigns;
    FQOVisualize: TQOVisualizeImpl;
    //1 Gibt die Zuordnung (LinkID) als Komma String zur�ck 
    function GetCommaTextByLinkID: String; virtual;
    //1 Zugriffsmethode f�r InProduction 
    function GetInProduction: Boolean; virtual; abstract;
    //1 Zugriffsmethode f�r DeleteState 
    procedure SetDeleteState(Value: TDeleteState); virtual;
  public
    //1 Konstruktor 
    constructor Create; virtual;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Eigenschaften �bernehmen 
    procedure Assign(Source: TPersistent); override;
    //1 L�scht den Datensatz von der Datenbank 
    function DeleteFromDB(aQuery: TNativeAdoQuery): Boolean; virtual;
    //1 Speichert die Settings des DataItems auf der Datenbank 
    function SaveDefToDB: Boolean; virtual; abstract;
    //1 Gibt die Zuordnung (LinkID) als Komma String zur�ck 
    property CommaTextByLinkID: String read GetCommaTextByLinkID;
    //1 Zeigt an ob ein DataItem gel�scht werden soll 
    property DeleteState: TDeleteState read FDeleteState write SetDeleteState;
    //1 ID des Artikels auf der Datenbank (QO_Settings) 
    property ID: Integer read FID write FID;
    //1 True, wenn der entsprechende Artikel / Maschine in Produktion sind 
    property InProduction: Boolean read GetInProduction;
    //1 ID des Original Artikels auf der DB (t_style) 
    property LinkID: Integer read FLinkID write FLinkID;
    //1 ID des �bergeordneten Elementes (Alarm oder Style) f�r die Datenbank 
    property ParentID: Integer read FParentID write FParentID;
    //1 Untergeordnete Elemente (Bei TQOStyle sind das TQOMachine) 
    property QOAssigns: TQOAssigns read FQOAssigns write FQOAssigns;
    //1 Implementiert die Funktionalit�t von IQOVisualize 
    property QOVisualize: TQOVisualizeImpl read FQOVisualize implements IQOVisualize;
  end;
  
  (*: Klasse:        TQOAlarms
      Vorg�nger:     TPersistent
      Kategorie:     Kernel
      Kurzbeschrieb: Liste aller definierten Alarme 
      Beschreibung:  
                     Diese Klasse ist die Wurzel einer Objekthirarchie zum Erfassen und Auswerten von QOfflimit 
                     Alarmen. 
                     Die Klasse verwaltet eine Menge von Alarm-Objekten. 
                     
                     �ber die Objekthirarchie kann auf jedes Detail der erfassten Alarme, DataItems, Zuordnungen und 
                     Berechnungsmethoden 
                     zugegriffen werden. Auch die Berechnung von QOfflimit-Alarmen haben als Ausgangspunkt diese Klasse 
                     (Evaluate()). 
                     Die Aufgabe wird dann allerdings durch die gesammte Objekt-Hirarchie an die Berechnungsmethoden 
                     delegiert.
                     
                     Die Klasse �bernimmt dann auch das Speichern der Settings und der ermittelten Alarme. Sie 
                     delegiert die Aufgaben, 
                     wo n�tig, an die untergeordneten Objekte. *)
  TQOAlarms = class (TPersistent)
  private
    mLoaded: Boolean;
    //1 Gibt die Anzahl der Alarme im Offlimit zur�ck 
    function GetOfflimitCount: Integer;
  protected
    mAlarms: TList;
    //1 Singleton 
    constructor CreateInstance; virtual;
    //1 singleton 
    class function AccessInstance(Request: Integer): TQOAlarms;
    //1 Anzahl Elemente in der Liste 
    function GetAlarmCount: Integer;
    //1 Zugriffsmethode auf ein Element aus der Liste 
    function GetAlarms(aIndex: Integer): TQOAlarm;
  public
    //1 Konstruktor 
    constructor Create;
    //1 Destruktor 
    destructor Destroy; override;
    //1 F�gt ein Element zur Liste hinzu 
    function Add(aAlarm: TQOAlarm): Integer; virtual;
    //1 F�gt einen neuen Alarm zur Liste hinzu und gibt den Alarm zur�ck 
    function AddNewAlarm(aName: string): TQOAlarm;
    //1 Gibt einen Alarm anhand seiner ID zur�ck 
    function AlarmByID(aID: integer): TQOAlarm;
    //1 L�scht alle Elemente aus der Liste 
    procedure Clear;
    //1 Gibt die Namen der DataItems als Kommasaparierten String zur�ck 
    function DataItemNamesAsString(aOnlyCaught: boolean): String;
    //1 L�scht ein Element aus der Liste 
    procedure Delete(aIndex: integer); overload; virtual;
    //1 L�scht ein Alarm aus der Liste 
    procedure Delete(aAlarm: TQOAlarm); overload; virtual;
    //1 Startet die Analyse der Daten 
    procedure Evaluate(aSave: boolean);
    //1 Sammelt von allen Methoden die Anzahl der Schichten 
    function GetMaxShiftCount: Integer;
    //1 Gibt alle beteiligten ProdID's als kommaseparierten String zur�ck 
    function GetProdIDs: String;
    //1 Gibt den Index des �bergebenen Alarms zur�ck 
    function IndexOf(aAlarm: TQOAlarm): Integer; virtual;
    //1 singleton 
    class function Instance: TQOAlarms;
    //1 L�dt alle Definitionen von der Datenbank 
    function LoadDefFromDB: Boolean;
    //1 singleton 
    class procedure ReleaseInstance;
    //1 Speichert die ermittelten Daten in die Datenbank 
    procedure SaveDataToDB(aPersistent: boolean);
    //1 Speichert die Settings auf der Datenbank 
    function SaveDefToDB: Boolean;
    //1 Anzahl der Elemente in der Liste 
    property AlarmCount: Integer read GetAlarmCount;
    //1 Alle Elemente als Array mit indiziertem Zugriff 
    property Alarms[aIndex: Integer]: TQOAlarm read GetAlarms; default;
    //1 Gibt die Anzahl der Alarme im Offlimit zur�ck 
    property OfflimitCount: Integer read GetOfflimitCount;
  end;
  
  (*: Klasse:        TQOAlarm
      Vorg�nger:     TQOIntetrfacedObject
      Kategorie:     Kernel
      Kurzbeschrieb: Ein einzelnes Alarm Set 
      Beschreibung:  
                     Ein Alarm besteht aus �berwachten DataItems und den Artiklen/Maschinen die dem
                     Alarm zugeordnet sind. Ein Alarm beschreibt wie eine �berwachung auszusehen hat.
                     Es werden alle definierten DataItems der zugeordneten Artikel �berwacht. Die
                     �berwachung erfolgt nicht online sondern wird von aussen getriggert (von Hand 
                     oder vom StorrageHandler bei Schichtende). Wird die �berwachung ausgef�hrt, werden 
                     die Schichtdaten der entsprechenden Artikel f�r jedes DataItem gesammelt.
                     
                     Die eigentliche Berechnung wird an die einzelnen Items und deren Methoden delegiert.
                     
                     Am Schluss wird f�r jedes DataItem gepr�ft ob die logische Verkn�pfung der Lots
                     im Alarm zutrifft und entsprechend die Daten verworfen oder gesichert. *)
  TQOAlarm = class (TQOIntetrfacedObject, IQOVisualize, ISaveDefToDB)
  private
    FActive: Boolean;
    FAdviceMethods: TAdviceMethodSet;
    FBoolOpAnd: Boolean;
    FCaught: Boolean;
    FID: Integer;
    FInvolvedProdIDs: String;
    FProdIDsInOfflimit: TStringList;
    FQOAssigns: TQOAssigns;
    FQOItems: TQOItems;
    FQOVisualize: TQOVisualizeImpl;
    FSaveDate: TDateTime;
    FSavingUser: String;
    FValues: TQOValues;
    //1 Berechnet die Produktions IDs die f�r diesen Alarm relevant sind 
    function GetInvolvedProdIDs: String;
  protected
    //1 Fragt den ImageIndex f�r die Anzeige ab 
    procedure GetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer);
    //1 True, wenn der Alarm aktiviert ist 
    function OnGetActive(Sender: TObject): Boolean;
  public
    //1 Konstruktor 
    constructor Create; virtual;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Gibt ein DataItem anhand des Namens zur�ck 
    function DataItemByName(aName: string; aDataItemNameType: TDataItemNameType = ntItem): Integer;
    //1 Gibt die Namen der DataItems als Kommasaparierten String zur�ck 
    function DataItemNamesAsString(aOnlyCaught: boolean): String;
    //1 L�scht den Datensatz von der Datenbank 
    function DeleteFromDB(aQuery: TNativeAdoQuery): Boolean;
    //1 Wird aufgerufen, wenn ein Alarm berechnet werden soll 
    procedure Evaluate;
    //1 Sammelt von allen Methoden die Anzahl der Schichten 
    function GetMaxShiftCount: Integer;
    //1 Liefert den Index des entsprechenden Items 
    function IndexOfItem(aQOItem: TQOItem): Integer;
    //1 Pr�ft ob ein Item mit dem entsprechenden Namen bereits existiert 
    function ItemExists(aName: string; aDataItemNameType: TDataItemNameType = ntItem): Boolean;
    //1 Setzt den Alarm zur�ck 
    procedure Reset;
    //1 Holt die Settings der bDataItems/Methoden di im Offlimit sind 
    procedure SaveDataToDB(aQuery: TNativeAdoQuery; aAlarmDataID: integer; aPersistent: boolean);
    //1 Speichert die Settings des Alarms auf der Datenbank 
    function SaveDefToDB: Boolean;
    //1 True, wenn der Alarm "scharf" ist 
    property Active: Boolean read FActive write FActive;
    //1 Benachrichtigungsmethoden 
    property AdviceMethods: TAdviceMethodSet read FAdviceMethods write FAdviceMethods;
    //1 True, wenn die DataItems AND verkn�pft werden sollen 
    property BoolOpAnd: Boolean read FBoolOpAnd write FBoolOpAnd;
    //1 True, wenn ein Alarm im Offlimit ist 
    property Caught: Boolean read FCaught write FCaught;
    //1 ID f�r die Datenbank 
    property ID: Integer read FID write FID;
    //1 Berechnet die Produktions IDs die f�r diesen Alarm relevant sind 
    property InvolvedProdIDs: String read GetInvolvedProdIDs;
    //1 ProdIDs die im Offlimit sind 
    property ProdIDsInOfflimit: TStringList read FProdIDsInOfflimit write FProdIDsInOfflimit;
    //1 Liste mit den Gruppierungs Items 
    property QOAssigns: TQOAssigns read FQOAssigns;
    //1 Liste mit den beteiligten DataItems 
    property QOItems: TQOItems read FQOItems;
    //1 Implementiert die Funktionalit�t von IQOVisualize 
    property QOVisualize: TQOVisualizeImpl read FQOVisualize implements IQOVisualize;
    //1 Datum als die Settings des Alarms das letzte Mal gespeichert wurden 
    property SaveDate: TDateTime read FSaveDate write FSaveDate;
    //1 Benutzer der die Settings das letzte Mal ver�ndert hat 
    property SavingUser: String read FSavingUser write FSavingUser;
    //1 Recordset mit allen Daten f�r den entsprechenden Alarm 
    property Values: TQOValues read FValues write FValues;
  end;
  
  (*: Klasse:        TActiveMachines
      Vorg�nger:     TPersistent
      Kategorie:     Helper
      Kurzbeschrieb: Kapselt ein 2 - Dimensionales Array mit den produzierenden Maschinen und den entprechenden 
      Artikeln 
      Beschreibung:  
                     Diese Klasse ist als Singleton ausgelegt. *)
  TActiveMachines = class (TPersistent)
  private
    FInitialized: Boolean;
    mActiveProdGroups: TActiveProdGroupArray;
    //1 Zugriffsmethode f�r "RowCount" 
    function GetRowCount: Integer;
  protected
    //1 Singleton 
    constructor CreateInstance;
    //1 Singleton 
    class function AccessInstance(Request: Integer): TActiveMachines;
  public
    //1 Singleton 
    constructor Create;
    //1 Singleton 
    destructor Destroy; override;
    //1 Holt alle aktiven Produktionsgruppen aus der DB 
    function GetActiveProdGroups: Boolean;
    //1 Initialisiert Das Objekt 
    procedure Init;
    //1 Singleton 
    class function Instance: TActiveMachines;
    //1 Fragt ab ob auf einer Maschine ein bestimmter Artikel produziert wird 
    function MachInProd(aMachID: integer; aStyleID: integer): Boolean;
    //1 Singleton 
    class procedure ReleaseInstance;
    //1 Gibt zur�ck ob ein Artikel auf irgendeiner Maschine produziert wird 
    function StyleInProd(aStyleID: integer): Boolean;
    //1 True, wenn die Daten von der DB geholt wurden 
    property Initialized: Boolean read FInitialized write FInitialized;
    //1 Gibt die Anzahl Eintr�ge zur�ck 
    property RowCount: Integer read GetRowCount;
  end;
  
  (*: Klasse:        TQODataPool
      Vorg�nger:     TObject
      Kategorie:     Helper
      Kurzbeschrieb: H�lt die Daten die f�r die Berechnung des Alarms notwendig sind 
      Beschreibung:  
                     - *)
  TQODataPool = class (TObject)
  private
    FActiveProdIDs: TNativeAdoQuery;
    FAllShifts: TNativeAdoQuery;
    FDataPool: TNativeAdoQuery;
    FInitialized: Boolean;
    FInitializing: Boolean;
    FRecentShiftID: Integer;
    FRecentShiftStart: TDateTime;
    FSelectPast: Boolean;
    FShiftCalID: Integer;
    FStyleData: TNativeAdoQuery;
    mDB: TAdoDBAccess;
    mOldestShift: TDateTime;
    mSQLFromTables: String;
    mSQLSelectFields: String;
    mSQLWhere: String;
    //1 Holt die aktuelle Schicht aus dem Query mit allen Schichten 
    function GetRecentShiftID: Integer;
    //1 Holt den Start der aktuellen Schicht aus dem Query mit allen Schichten 
    function GetRecentShiftStart: TDateTime;
    //1 Setzt Initialized auf false, wenn vergangene Daten abgefragt werden sollen 
    procedure SetSelectPast(Value: Boolean);
  protected
    //1 Singleton 
    constructor CreateInstance;
    //1 Singleton 
    class function AccessInstance(Request: Integer): TQODataPool;
  public
    //1 Singleton 
    constructor Create;
    //1 Singleton 
    destructor Destroy; override;
    //1 Gibt zur�ck wieviele zusammenh�ngende Schichten das Lot im Alarm war 
    function GetActualTurnFromProdID(aProdId: integer): Integer;
    //1 Holt die Artikeldaten f�r die Artikel die im Alarm sind 
    procedure GetStyleData(aStyles: string);
    //1 Initialisiert die Querys und stellt den Daten Pool zusammen 
    procedure Init;
    //1 Singleton 
    class function Instance: TQODataPool;
    //1 Singleton 
    class procedure ReleaseInstance;
    //1 Dataset mit den aktuell produzierenden Partien 
    property ActiveProdIDs: TNativeAdoQuery read FActiveProdIDs write FActiveProdIDs;
    //1 Dataset mit allen SchichtID's und den Startdaten 
    property AllShifts: TNativeAdoQuery read FAllShifts write FAllShifts;
    //1 Globaler Datenpool f�r alle Methoden 
    property DataPool: TNativeAdoQuery read FDataPool write FDataPool;
    //1 True, wenn die Initialisation abgeschlossen ist. (Erneut Initialisieren mit Initialized := false) 
    property Initialized: Boolean read FInitialized write FInitialized;
    //1 True, w�hrend die Initialisierung l�uft 
    property Initializing: Boolean read FInitializing write FInitializing;
    //1 Letzte abgeschlossene Schicht 
    property RecentShiftID: Integer read GetRecentShiftID;
    //1 Holt den Start der aktuellen Schicht aus dem Query mit allen Schichten 
    property RecentShiftStart: TDateTime read GetRecentShiftStart;
    //1 True, wenn nicht das aktuelle Datum abgefragt wird, sondern r�ckwirkende QOfflimits erstellt werden sollen 
    property SelectPast: Boolean read FSelectPast write SetSelectPast;
    //1 Selektierter Schichtkalender 
    property ShiftCalID: Integer read FShiftCalID write FShiftCalID;
    //1 Beinhaltet die Daten aller an den ALarmen betiligter Artikel 
    property StyleData: TNativeAdoQuery read FStyleData write FStyleData;
  end;
  
  (*: Klasse:        TLimitValueList
      Vorg�nger:     TObject
      Kategorie:     No category
      Kurzbeschrieb: Liste die eine Menge von Grenzwerten verwaltet 
      Beschreibung:  
                     Pro Grenzwert gibt es einen Eintrag in der Liste.
                     Jeder Eintrag repr�sentiert ein Array von Grenzwerten
                     f�r den entzsprechenden Artikel und eine gewisse Anzahl Schichten. *)
  TLimitValueList = class (TObject)
  private
    FDataItem: TQOItem;
    FLimitValueCount: Integer;
    FSampleShiftCount: Integer;
  protected
    FOnDeleteStyleLimit: TNotifyEvent;
    mStyleLimits: TList;
    //1 F�gt ein neues Grenzwert-Array in die Liste ein 
    function AddAndInit(aStyleLimit: TStyleLimit): Integer; overload; virtual;
    //1 Liefert zur�ck f�r wieviele Artikel die Grenzwerte gespeichert sind 
    function GetStyleLimitCount: Integer;
    //1 Liefert ein "Grenzwert-Objekt" f�r einen Artikel 
    function GetStyleLimits(aIndex: Integer): TStyleLimit;
  public
    //1 Konstruktor 
    constructor Create; virtual;
    //1 Destruktor 
    destructor Destroy; override;
    //1 F�gt ein neues Grenzwert-Array in die Liste ein 
    function Add(aStyleID: integer): Integer; overload; virtual;
    //1 L�scht alle Elemente 
    procedure Clear;
    //1 L�scht ein einzelnes Element 
    procedure Delete(aIndex: integer); virtual;
    //1 Holt alle Artikeldaten von der DB die f�r die Berechnung notwendig sind 
    procedure GetStyleData(aStyleIDs: string);
    //1 Sucht ein Element in der Liste und gibt den Index zur�ck 
    function IndexOf(aStyleLimit: TStyleLimit): Integer; virtual;
    //1 Sucht ein Element in der Liste und gibt den Index zur�ck 
    function IndexOfStyleID(aStyleID: integer): Integer; virtual;
    //1 DataItem f�r das Grenzwerte gesammelt werden 
    property DataItem: TQOItem read FDataItem write FDataItem;
    //1 Anzahl der zu speichernden Grenzwerte (Anzahl angezeigter Schichten) 
    property LimitValueCount: Integer read FLimitValueCount write FLimitValueCount;
    //1 Wird ausgef�hrt, wenn ein Element gel�scht wird 
    property OnDeleteStyleLimit: TNotifyEvent read FOnDeleteStyleLimit write FOnDeleteStyleLimit;
    //1 Anzahl Schichten die f�r den Mittelwert herangezogen werden 
    property SampleShiftCount: Integer read FSampleShiftCount write FSampleShiftCount;
    //1 Anzahl der gespeicherten Artikel 
    property StyleLimitCount: Integer read GetStyleLimitCount;
    //1 Zugriff auf Grenzwerte einzelner Artikel 
    property StyleLimits[aIndex: Integer]: TStyleLimit read GetStyleLimits; default;
  end;
  
  (*: Klasse:        TStyleLimit
      Vorg�nger:     TObject
      Kategorie:     No category
      Kurzbeschrieb: Repr�sentiert eine Liste von Grenzwerten f�r einen bestimmten Artikel 
      Beschreibung:  
                     F�r jeden Artikel werden die Grenzwerte f�r
                     jede interessierende Schicht berechnet. *)
  TStyleLimit = class (TObject)
  private
    FCalcMode: Integer;
    FDataItemFieldName: String;
    FLimitValues: TLimitValues;
    FSampleShiftCount: Integer;
    FStyleData: TNativeAdoQuery;
    FStyleID: Integer;
    //1 Liefert das Recordset mit den Artikeldaten 
    function GetStyleData: TNativeAdoQuery;
    //1 Kopiert das �bergebene Recordset 
    procedure SetStyleData(aRecordset: TNativeAdoQuery);
  public
    //1 Destruktor 
    destructor Destroy; override;
    //1 Berechnet den Grenzwert (ohne Ausreisserbehandlung) 
    procedure GetMeanValue(aShiftID: integer; aShiftNumber: integer; aLenBase: TLenBase); virtual;
    //1 Initialisiert das Array mit der gew�nschten Anzahl Elementen 
    procedure InitLimitValues(aCount: integer);
    //1 Kalkulationsmodus f�r das DataItem 
    property CalcMode: Integer read FCalcMode write FCalcMode;
    //1 Name der Spalte �ber die auf die Artikeldaten zugegriffen werden soll 
    property DataItemFieldName: String read FDataItemFieldName write FDataItemFieldName;
    //1 Array mit einem Grenzwert f�r jede Schicht 
    property LimitValues: TLimitValues read FLimitValues write FLimitValues;
    //1 Anzahl Schichten die f�r den Mittelwert herangezogen werden 
    property SampleShiftCount: Integer read FSampleShiftCount write FSampleShiftCount;
    //1 Nimmt die Daten des Artikels f�r den entsprechenden Grenzwert auf 
    property StyleData: TNativeAdoQuery read GetStyleData write SetStyleData;
    //1 Artikel ID f�r die die Grenzwerte verwaltet werden 
    property StyleID: Integer read FStyleID write FStyleID;
  end;
  

// Initialisiert die Umgebung (z.B. Definition der DataItems laden)   
function InitQOEnviroment: Boolean;

procedure LoadDataItems;

const
  // Ung�ltige ID bedeutet, dass der Datensatz noch nicht auf der DB existiert
  cINVALID_ID = -1;
  
  // Diese ID identifiziert einen Eintrag in der DB als "Alle Artikel"
  cAllStylesLinkID = 32767;
  
  // Zeichen das untergeordnete Maschinen kennzeichnet (CommaTextByLinkID)
  cAdditionalMachines = '+';
  
var
  // DataItem Container mit den definierten (DB) DataItems
  gQOItems    : TQOItems = nil;
  
  // Alle Artikel die auf der DB derfiniert sind
  gAllStyles  : TQOAssigns = nil;
  
  // Alle Maschinen die auf der DB definiert sind
  gAllMachines: TQOAssigns = nil;

implementation
uses
  mmcs, QOMethods, QOGroup, typInfo, QODef, LabMasterDef, SettingsReader, MMUGlobal, ivdictio;
  
const
  // Default Benachrichtigungs Methoden
  cDefaultAdviceMethodeSet = [amMonitor, amPrinter];

(*-----------------------------------------------------------
  F�gt die Applikations-Abh�ngigen DataItems hinzu
-------------------------------------------------------------*)
procedure SetApplicationDataItems; forward;

//:-------------------------------------------------------------------
(*: Member:           QueryInterface
 *  Klasse:           TQOIntetrfacedObject
 *  Kategorie:        No category 
 *  Argumente:        (IID, Obj)
 *
 *  Kurzbeschreibung: IUnknown
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOIntetrfacedObject.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := S_OK
  else
    Result := E_NOINTERFACE;
end;// TQOIntetrfacedObject.QueryInterface cat:No category

//:-------------------------------------------------------------------
(*: Member:           _AddRef
 *  Klasse:           TQOIntetrfacedObject
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: IUnknown
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOIntetrfacedObject._AddRef: Integer;
begin
  Result := -1;
  // non-refcounted memorymanagement
end;// TQOIntetrfacedObject._AddRef cat:No category

//:-------------------------------------------------------------------
(*: Member:           _Release
 *  Klasse:           TQOIntetrfacedObject
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: IUnknown
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOIntetrfacedObject._Release: Integer;
begin
  Result := -1;
  // non-refcounted memorymanagement
end;// TQOIntetrfacedObject._Release cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TImageIndexHandler
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TImageIndexHandler.Create;
begin
  inherited Create;

//: ----------------------------------------------
  raise Exception.CreateFmt('Access class %s through Instance only', [ClassName]);
end;// TImageIndexHandler.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           CreateInstance
 *  Klasse:           TImageIndexHandler
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TImageIndexHandler.CreateInstance;
begin
  inherited Create;
end;// TImageIndexHandler.CreateInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TImageIndexHandler
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TImageIndexHandler.Destroy;
begin
  if AccessInstance(0) = Self then AccessInstance(2);

//: ----------------------------------------------
  inherited Destroy;
end;// TImageIndexHandler.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           AccessInstance
 *  Klasse:           TImageIndexHandler
 *  Kategorie:        No category 
 *  Argumente:        (Request)
 *
 *  Kurzbeschreibung: singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class function TImageIndexHandler.AccessInstance(Request: Integer): TImageIndexHandler;
  
  const FInstance: TImageIndexHandler = nil;
  
begin
  case Request of
    0 : ;
    1 : if not Assigned(FInstance) then FInstance := CreateInstance;
    2 : FInstance := nil;
  else
    raise Exception.CreateFmt('Illegal request %d in AccessInstance', [Request]);
  end;
  Result := FInstance;
end;// TImageIndexHandler.AccessInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           Instance
 *  Klasse:           TImageIndexHandler
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class function TImageIndexHandler.Instance: TImageIndexHandler;
begin
  Result := AccessInstance(1);
end;// TImageIndexHandler.Instance cat:No category

//:-------------------------------------------------------------------
(*: Member:           ReleaseInstance
 *  Klasse:           TImageIndexHandler
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class procedure TImageIndexHandler.ReleaseInstance;
begin
  AccessInstance(0).Free;
end;// TImageIndexHandler.ReleaseInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TQOVisualizeImpl
 *  Kategorie:        No category 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Eigenschaften �bernehmen
 *  Beschreibung:     
                      Dient zum kopieren von Objekten
 --------------------------------------------------------------------*)
procedure TQOVisualizeImpl.Assign(Source: TPersistent);
var
  xSource: TQOVisualizeImpl;
begin
  if Source is TQOVisualizeImpl then begin
    xSource := TQOVisualizeImpl(Source);
    // Alle Eigenschaften kopieren
    Caption         := xSource.Caption;
  //  OnCaptionChange := xSource.OnCaptionChange;
  end else begin
    // Weiterreichen
    inherited Assign(Source);
  end;// if Source is TQOItem then begin
end;// TQOVisualizeImpl.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoGetCaption
 *  Klasse:           TQOVisualizeImpl
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aCaption)
 *
 *  Kurzbeschreibung: Fragt die Caption beim "Eltern-Objekt" ab
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOVisualizeImpl.DoGetCaption(Sender: TObject; var aCaption: string);
begin
  if Assigned(FOnGetCaption) then FOnGetCaption(Sender, aCaption);
end;// TQOVisualizeImpl.DoGetCaption cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetActive
 *  Klasse:           TQOVisualizeImpl
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Fragt das Implementierende Objekt ab ob es Aktiv ist oder nicht
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOVisualizeImpl.GetActive: Boolean;
begin
  result := false;
  
  if assigned(FOnGetActive) then
    result := FOnGetActive(self);
end;// TQOVisualizeImpl.GetActive cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetCaption
 *  Klasse:           TQOVisualizeImpl
 *  Kategorie:        IQOVisualize 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zugriffsmethoden f�r Caption
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOVisualizeImpl.GetCaption: String;
begin
  Result := FCaption;

//: ----------------------------------------------
  DoGetCaption(self, result);
end;// TQOVisualizeImpl.GetCaption cat:IQOVisualize

//:-------------------------------------------------------------------
(*: Member:           GetImageIndex
 *  Klasse:           TQOVisualizeImpl
 *  Kategorie:        No category 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r ImageIndex
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOVisualizeImpl.GetImageIndex(aIndex: TQOImageKind): Integer;
begin
  result := -1;
  
  if assigned(FOnGetImageIndex) then
    FOnGetImageIndex(self, aIndex, result);
end;// TQOVisualizeImpl.GetImageIndex cat:No category

//:-------------------------------------------------------------------
(*: Member:           QueryInterface
 *  Klasse:           TQOVisualizeImpl
 *  Kategorie:        IUnknown 
 *  Argumente:        (IID, Obj)
 *
 *  Kurzbeschreibung: Fragt ab ob ein bestimmtes Interface existiert (IUnknown)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOVisualizeImpl.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := S_OK
  else
    Result := E_NOINTERFACE;
end;// TQOVisualizeImpl.QueryInterface cat:IUnknown

//:-------------------------------------------------------------------
(*: Member:           SetCaption
 *  Klasse:           TQOVisualizeImpl
 *  Kategorie:        IQOVisualize 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r Caption
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOVisualizeImpl.SetCaption(const Value: String);
var
  xAllow: Boolean;
begin
  if FCaption <> Value then begin
    // Defaultm�ssig darf die Caption ge�ndert werden
    xAllow := true;
  
    // Event feuern
    if assigned(FOnCaptionChange) then
      FOnCaptionChange(self, FCaption, Value, xAllow);
  
    // Wenn keine Gegenstimmen, dann Caption �ndern
    if xAllow then
      FCaption := Value;
  end;// if FCaption <> Value then begin
end;// TQOVisualizeImpl.SetCaption cat:IQOVisualize

//:-------------------------------------------------------------------
(*: Member:           _AddRef
 *  Klasse:           TQOVisualizeImpl
 *  Kategorie:        IUnknown 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Referenzz�hlung (IUnknown)
 *  Beschreibung:     
                      Die Referenzz�hlung ist ausgeschaltet.
 --------------------------------------------------------------------*)
function TQOVisualizeImpl._AddRef: Integer;
begin
  Result := -1;
  // non-refcounted memorymanagement
end;// TQOVisualizeImpl._AddRef cat:IUnknown

//:-------------------------------------------------------------------
(*: Member:           _Release
 *  Klasse:           TQOVisualizeImpl
 *  Kategorie:        IUnknown 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Refernzz�hlung (IUnknown)
 *  Beschreibung:     
                      Die Referenzz�lung ist ausgeschaltet.
 --------------------------------------------------------------------*)
function TQOVisualizeImpl._Release: Integer;
begin
  Result := -1;
  // non-refcounted memorymanagement
end;// TQOVisualizeImpl._Release cat:IUnknown

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOMethods
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor, wenn kein DataItem mit der Methode assoziiert ist
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOMethods.Create;
begin
  inherited Create;
  mMethods := TList.Create;
  mDataItem := nil;
end;// TQOMethods.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOMethods
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      Das entsprechende ITem muss in der Methode bekannt sein - sonst
                      hat die Methode nicht genug Informationen um die Daten berechnen
                      zu k�nnen.
 --------------------------------------------------------------------*)
constructor TQOMethods.Create(aOwner: TQOItem);
var
  i: TCalcMethod;
begin
  inherited Create;
  mMethods := TList.Create;

//: ----------------------------------------------
  ItemId := cINVALID_ID;
  mDataItem := aOwner;
  
  // Sammelt die Produktionsgruppen die im Offlimit sind
  FProdIDsInOfflimit := TStringList.Create;
  FProdIDsInOfflimit.Duplicates := dupIgnore;

//: ----------------------------------------------
  (* Da die Methoden erfasst werden k�nnen ohne dass sie
     aktiv sind, muss jedes Item ein kompletes Set an Methoden
     bekommen. *)
  for i := Low(TCalcMethod) to High(TCalcMethod) do
    Add(FactoryGetMethod(i));
end;// TQOMethods.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TQOMethods
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TQOMethods.Destroy;
begin
  Clear;
  FreeAndNil(mMethods);
  FreeAndNil(FProdIDsInOfflimit);

//: ----------------------------------------------
  inherited;
end;// TQOMethods.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           Add
 *  Klasse:           TQOMethods
 *  Kategorie:        List 
 *  Argumente:        (aMethod)
 *
 *  Kurzbeschreibung: F�gt ein Element zur Liste hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOMethods.Add(aMethod: TQOMethod): Integer;
begin
  result := -1;
  if assigned(mMethods) then
    result := mMethods.Add(aMethod);
end;// TQOMethods.Add cat:List

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TQOMethods
 *  Kategorie:        No category 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Eigenschaften �bernehmen
 *  Beschreibung:     
                      Dient zum kopieren von Objekten
 --------------------------------------------------------------------*)
procedure TQOMethods.Assign(Source: TPersistent);
var
  xSource: TQOMethods;
  i: Integer;
  xMethod: TQOMethod;
begin
  if Source is TQOMethods then begin
    xSource := TQOMethods(Source);
    Clear;
  
    // Methoden kopieren
    for i := 0 to xSource.MethodCount - 1 do begin
      xMethod := TQOMethodClass(xSource.Methods[i].ClassType).Create(mDataItem);
      xMethod.Assign(xSource.Methods[i]);
      Add(xMethod);
    end;// for i := 0 to xSource.MethodCount - 1 do begin
  
    ItemID := xSource.ItemID;
  end else begin
    // Weiterreichen
    inherited Assign(Source);
  end;// if Source is TQOItem then begin
end;// TQOMethods.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           Clear
 *  Klasse:           TQOMethods
 *  Kategorie:        List 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�scht alle Elemente aus der Liste
 *  Beschreibung:     
                      Die Elemente werden freigegeben (Komposition).
 --------------------------------------------------------------------*)
procedure TQOMethods.Clear;
begin
  while MethodCount > 0 do
    delete(0);
end;// TQOMethods.Clear cat:List

//:-------------------------------------------------------------------
(*: Member:           Delete
 *  Klasse:           TQOMethods
 *  Kategorie:        List 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: L�scht ein Element aus der Liste
 *  Beschreibung:     
                      Das Element wird freigegeben (Komposition)
 --------------------------------------------------------------------*)
procedure TQOMethods.Delete(aIndex: integer);
begin
  if assigned(mMethods) then begin
    if (aIndex < mMethods.Count) and (aIndex >= 0) then begin
      Methods[aIndex].Free;
      mMethods.delete(aIndex);
    end;// (aIndex < mMethods.Count) and (aIndex >= 0)
  end;// if assigned(mMethods) then begin
end;// TQOMethods.Delete cat:List

//:-------------------------------------------------------------------
(*: Member:           DeleteFromDB
 *  Klasse:           TQOMethods
 *  Kategorie:        No category 
 *  Argumente:        (aQuery)
 *
 *  Kurzbeschreibung: L�scht den Datensatz von der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOMethods.DeleteFromDB(aQuery: TNativeAdoQuery): Boolean;
var
  i: Integer;
  xDeleted: Boolean;
begin
  // Result soll mit True initialisiert werden, wenn Items definiert sind
  result := (MethodCount = 0);
  
  // Alle Items vond er DB l�schen
  for i := 0 to MethodCount - 1 do begin
    xDeleted := Methods[i].DeleteFromDB(aQuery);
    if i > 0 then
      result := xDeleted and result
    else
      result := xDeleted;
  end;// for i := 0 to MethodCount - 1 do
end;// TQOMethods.DeleteFromDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           Evaluate
 *  Klasse:           TQOMethods
 *  Kategorie:        No category 
 *  Argumente:        (aBoolOpAnd)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn ein Alarm berechnet werden soll
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOMethods.Evaluate(aBoolOpAnd: boolean);
var
  i: Integer;
  xFirstActiveListAssigned: Boolean;
  j: Integer;
begin
  FProdIDsInOfflimit.Clear;

//: ----------------------------------------------
  for i := 0 to MethodCount - 1 do begin
    if Methods[i].Active then begin
      Methods[i].Evaluate;
    end;// if Methods[i].Active then begin
  end;// for i := 0 to MethodCount - 1 do begin

//: ----------------------------------------------
  // Feststellen welche Produktionsgruppen dem Kriterium 'aBoolOpAnd' entsprechen
  if aBoolOpAnd then begin
    xFirstActiveListAssigned := false;
    // Alle aktiven Methoden m�ssen im Alarm sein
    for i := 0 to MethodCount - 1 do begin
      if Methods[i].Active then begin
        if not(xFirstActiveListAssigned) then begin
          // Die erste Liste komplet �bernehmen
          FProdIDsInOfflimit.CommaText := Methods[i].ProdIDsInOfflimit.CommaText;
          xFirstActiveListAssigned := true;
        end else begin
          // Alle weiteren Listen vergleichen
          j := 0;
          while (j < FProdIDsInOfflimit.Count) do begin
            // Wenn eine ProdID in der Vergleichsliste nicht vorkommt, dann diesen Eintrag entfernen
            if Methods[i].ProdIDsInOfflimit.IndexOf(FProdIDsInOfflimit[j]) = -1 then
              FProdIDsInOfflimit.Delete(j)
            else
              inc(j);
          end;// while (j < FProdIDsInOfflimit.Count) do begin
        end;// if FProdIDsInOfflimit.Count = 0 then begin
      end;// if Methods[i].Active then begin
    end;// for i := 0 to MethodCount - 1 do begin
  end else begin
    // Mindestens eine aktive Methode muss im Alarm sein
    for i := 0 to MethodCount - 1 do begin
      if Methods[i].Active then begin
        for j := 0 to Methods[i].ProdIDsInOfflimit.Count -1 do
          FProdIDsInOfflimit.Add(Methods[i].ProdIDsInOfflimit[j]);
      end;// if Methods[i].Active then begin
    end;// for i := 0 to MethodCount - 1 do begin
  end;// if aBoolOpAnd then begin
end;// TQOMethods.Evaluate cat:No category

//:-------------------------------------------------------------------
(*: Member:           FactoryGetMethod
 *  Klasse:           TQOMethods
 *  Kategorie:        No category 
 *  Argumente:        (aMethod)
 *
 *  Kurzbeschreibung: Gibt ein Objekt f�r die entsprechende Methode zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOMethods.FactoryGetMethod(aMethod: TCalcMethod): TQOMethod;
begin
  result := nil;
  
  case aMethod of
    cmBound : result := TQOBound.Create(mDataItem);
    cmAVG   : result := TQOAVG.Create(mDataItem);
  else
    raise Exception.create('MethodBaseFrame.initialization: CalcMethod not definend (' + GetEnumName(TypeInfo(TCalcMethod),ord(aMethod)) + ')');
  end;// case aMethod of

//: ----------------------------------------------
  if not(assigned(result)) then
    raise Exception.Create('TQOMethods.FactoryGetMethod');
end;// TQOMethods.FactoryGetMethod cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetActiveMethodCount
 *  Klasse:           TQOMethods
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die Anzahl aktivierter Methoden zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOMethods.GetActiveMethodCount: Integer;
var
  i: Integer;
begin
  result := 0;
  for i := 0 to MethodCount - 1 do begin
    if Methods[i].Active then
      inc(result);
  end;// for i := 0 to MethodCount - 1 do begin
end;// TQOMethods.GetActiveMethodCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetCaught
 *  Klasse:           TQOMethods
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Fragt die Methoden ab, ob eine davon im Offlimit ist
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOMethods.GetCaught: Boolean;
var
  i: Integer;
begin
  result := false;
  for i := 0 to MethodCount - 1 do
    result := result or Methods[i].Caught;
end;// TQOMethods.GetCaught cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMaxShiftCount
 *  Klasse:           TQOMethods
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Sammelt von allen Methoden die Anzahl der Schichten
 *  Beschreibung:     
                      Jede Methode definiert eine gewisse Anzahl Schichten die
                      mindestens an einer Verletzung beteiligt sein m�ssen um
                      einen Alarm zu erzeugen. 
                      Diese Funktion ermittelt die Methode (resp. Anzahl Schichten)
                      in der die meisten Schichten definiert sind. 
                      Heraus kommt die Anzahl Schichten die mindestens im globalen
                      "Datenhaufen" vorhanden sein m�ssen um alle Methoden
                      auswerten zu k�nnen.
 --------------------------------------------------------------------*)
function TQOMethods.GetMaxShiftCount: Integer;
var
  i: Integer;
  xShiftCount: Integer;
begin
  result := 0;

//: ----------------------------------------------
  for i := 0 to MethodCount - 1 do begin
    if Methods[i].Active then begin
      xShiftCount := Methods[i].ShiftCount;
      if xShiftCount > result then
        result := xShiftCount;
    end;// if Methods[i].Active then begin
  end;// for i := 0 to MethodCount - 1 do begin
end;// TQOMethods.GetMaxShiftCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMethodCount
 *  Klasse:           TQOMethods
 *  Kategorie:        List 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Anzahl Elemente in der Liste
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOMethods.GetMethodCount: Integer;
begin
  result := 0;
  
  if assigned(mMethods) then
    result := mMethods.Count;
end;// TQOMethods.GetMethodCount cat:List

//:-------------------------------------------------------------------
(*: Member:           GetMethods
 *  Klasse:           TQOMethods
 *  Kategorie:        List 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: Zugriffsmethode auf ein Element aus der Liste
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOMethods.GetMethods(aIndex: Integer): TQOMethod;
begin
  result := nil;
  if assigned(mMethods) then
    if mMethods.Count > aIndex then
      result := TQOMethod(mMethods.Items[aIndex]);
end;// TQOMethods.GetMethods cat:List

//:-------------------------------------------------------------------
(*: Member:           GetOfflimitData
 *  Klasse:           TQOMethods
 *  Kategorie:        No category 
 *  Argumente:        (aAlarm)
 *
 *  Kurzbeschreibung: Ermittelt die Datenreihe f�r alle Produktionsgruppen die im Offlimit sind.
 *  Beschreibung:     
                      Zuerst wird mit der Methode 'Evaluate' festgestellt ob- und
                      welche Produktionsgruppen im Offlimit sind. Danach werden alle 
                      Methoden nach der Verkn�pfungsvorschrift verkn�pft. 
                      Dieser Vorgang wird dann f�r den Alarm und die DataItems wiederholt. 
                      Auch hier k�nnen Produktionsgruppen entfallen. 
                      Erst wenn klar ist, welche Produktionsgruppen f�r einen Alarm im Offlimit
                      sind, werden die Daten �ber die gew�hlte Anzahl Schichten abgefragt.
                      Dabei k�nnten Produktionsgruppen entfallen (z.B. Zwei Methoden 
                      definiert aber die Produktionsgruppe ist nur in einer Methode im Offlimit).
 --------------------------------------------------------------------*)
procedure TQOMethods.GetOfflimitData(aAlarm: TQOAlarm);
var
  i: Integer;
begin
  for i := 0 to Methodcount - 1 do
    Methods[i].GetOfflimitData(aAlarm);
end;// TQOMethods.GetOfflimitData cat:No category

//:-------------------------------------------------------------------
(*: Member:           MethodByType
 *  Klasse:           TQOMethods
 *  Kategorie:        No category 
 *  Argumente:        (aType)
 *
 *  Kurzbeschreibung: Gibt die Methode im Container zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOMethods.MethodByType(aType: TCalcMethod): TQOMethod;
var
  i: Integer;
begin
  result := nil;

//: ----------------------------------------------
  i := 0;
  
  while (i < MethodCount) and(not(assigned(result))) do begin
    if Methods[i].MethodType = aType then
      result := Methods[i];
    inc(i);
  end;// while (i < MethodCount) and(not(assigned(result))) do begin
end;// TQOMethods.MethodByType cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveDefToDB
 *  Klasse:           TQOMethods
 *  Kategorie:        No category 
 *  Argumente:        (aItemID)
 *
 *  Kurzbeschreibung: Speichert die Settings auf der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOMethods.SaveDefToDB(aItemID: integer): Boolean;
var
  xSaved: Boolean;
  i: Integer;
begin
  result := true;
  // Die ID des zugeh�rigen Items f�r jede Methode setzen
  ItemID := aItemID;

//: ----------------------------------------------
  // Jede Methode ist selber f�r seine Speicherung zust�ndig (Pro Methode ein DS)
  for i := 0 to MethodCount - 1 do begin
    xSaved := Methods[i].SaveDefToDB;
    result := result and xSaved;
  end;// for i := 0 to MethodCount do begin
end;// TQOMethods.SaveDefToDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetDeleteState
 *  Klasse:           TQOMethods
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: �Zugriffsmethode f�r DeleteState
 *  Beschreibung:     
                      Der L�sch Status muss auch an die Methoden weitergegeben werden.
 --------------------------------------------------------------------*)
procedure TQOMethods.SetDeleteState(Value: TDeleteState);
var
  i: Integer;
begin
  for i := 0 to MethodCount - 1 do
    Methods[i].DeleteState := Value;
end;// TQOMethods.SetDeleteState cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetFieldName
 *  Klasse:           TQOMethods
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r FieldName
 *  Beschreibung:     
                      Gibt den FieldName an jede Methode in der Liste weiter.
 --------------------------------------------------------------------*)
procedure TQOMethods.SetFieldName(const Value: String);
var
  i: Integer;
begin
  FFieldName := Value;

//: ----------------------------------------------
  for i := 0 to MethodCount - 1 do
    Methods[i].FieldName := Value
end;// TQOMethods.SetFieldName cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetItemId
 *  Klasse:           TQOMethods
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: ID des zugeh�rigen ITemsf�r die Datenbank
 *  Beschreibung:     
                      Die ID muss f�r alle Methoden gesetzt werden.
 --------------------------------------------------------------------*)
procedure TQOMethods.SetItemId(Value: Integer);
var
  i: Integer;
begin
  FItemId := Value;

//: ----------------------------------------------
  for i := 0 to MethodCount - 1 do
    Methods[i].ItemID := FItemId;
end;// TQOMethods.SetItemId cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOMethod
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      Das DataItem muss bekannt sein, um die Daten f�r die Berechnung 
                      zusammenzufinden.
 --------------------------------------------------------------------*)
constructor TQOMethod.Create(aOwner: TQOItem);
begin
  inherited Create;

//: ----------------------------------------------
  FQOVisualize := TQOVisualizeImpl.Create;

//: ----------------------------------------------
  FShowDetails := msdDefault;
  FGreaterThan := true;
  FLenBase := GetLenBase;
  
  mDataItem := aOwner;
  
  (* Initialisieren, damit der Datensatz gegebenfalls in der DB
     Eingef�gt werden kann (Unterscheidung INSERT oder UPDATE. *)
  FID := cINVALID_ID;
  
  // Sammelt die Produktionsgruppen die im Offlimit sind
  FProdIDsInOfflimit := TStringList.Create;
  FProdIDsInOfflimit.Duplicates := dupIgnore;
  
  mLimitValueList := TLimitValueList.Create;
end;// TQOMethod.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TQOMethod
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TQOMethod.Destroy;
begin
  FreeAndNil(FQOVisualize);

//: ----------------------------------------------
  FreeAndNil(FProdIDsInOfflimit);
  FreeAndNil(mLimitValueList);

//: ----------------------------------------------
  inherited Destroy;
end;// TQOMethod.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TQOMethod
 *  Kategorie:        No category 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Eigenschaften �bernehmen
 *  Beschreibung:     
                      Dient zum kopieren von Objekten
 --------------------------------------------------------------------*)
procedure TQOMethod.Assign(Source: TPersistent);
var
  xSource: TQOMethod;
begin
  if Source is TQOMethod then begin
    xSource := TQOMethod(Source);
    // Alle Eigenschaften kopieren
    Active      := xSource.Active;
    ItemId      := xSource.ItemId;
    ID          := xSource.ID;
    ShiftCount  := xSource.ShiftCount;
    ShowDetails := xSource.ShowDetails;
  
    FQOVisualize.Assign(xSource.QOVisualize);
  
  end else begin
    // Weiterreichen
    inherited Assign(Source);
  end;// if Source is TQOItem then begin
end;// TQOMethod.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           DeleteFromDB
 *  Klasse:           TQOMethod
 *  Kategorie:        No category 
 *  Argumente:        (aQuery)
 *
 *  Kurzbeschreibung: L�scht den Datensatz von der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOMethod.DeleteFromDB(aQuery: TNativeAdoQuery): Boolean;
  
  const
    cDeleteMethod = 'DELETE FROM t_QO_Method_Setting WHERE c_QOMethod_id = :c_QOMethod_id';
  
begin
  result := false;
  
  aQuery.SQL.Text := cDeleteMethod;
  aQuery.ParamByName('c_QOMethod_id').AsInteger := FID;
  
  // Datensatz gel�scht, wenn ein Datensatz betroffen ist
  result := (aQuery.ExecSQL = 1);
end;// TQOMethod.DeleteFromDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           Evaluate
 *  Klasse:           TQOMethod
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn ein Alarm berechnet werden soll
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOMethod.Evaluate;
begin
  try
    (* Diese Liste kann von einer Methode noch eingeschr�nkt
       werden, wenn zB. nur Artikel betrarchtet werden sollen,
       die seit einer gewissen Zeit produzieren oder aus irgend einem
       anderen Grund von der Berechnung ausgeschlossen werden sollen *)
    mProdIDList := TStringList.Create;
    mProdIDList.CommaText := mDataItem.ProdIDs;

//: ----------------------------------------------
    // Alle bisherigen Offlimits l�schen
    FProdIDsInOfflimit.Clear;
  
    (* Methode ist nicht im Offlimit
      (wird erst gesetzt in 'GetOfflimitData'. Das Bedeutet n�mlich, dass
      Daten vorhanden sind und deshalb auch, dass diese Methode am Alarm beteiligt ist) *)
    Caught := false;
  
    // F�r jede Schicht ein Grenzwert
    mLimitValueList.Clear;
    mLimitValueList.DataItem := mDataItem;
    mLimitValueList.LimitValueCount := ShiftCount + TMMSettingsReader.Instance.Value[cGraphicShiftDepth] - 1;
    mLimitValueList.SampleShiftCount := GetSampleShiftCount;

//: ----------------------------------------------
    // Berechnung ausf�hren
    if Active then
      Internal_Evaluate;

//: ----------------------------------------------
  finally
    FreeAndNil(mProdIDList);
  end;// try finally
end;// TQOMethod.Evaluate cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAttrValDef
 *  Klasse:           TQOMethod
 *  Kategorie:        No category 
 *  Argumente:        (aName, aXMLItem, aDef)
 *
 *  Kurzbeschreibung: Gibt einen Wert aus dem XML-String zur�ck.
 *  Beschreibung:     
                      Wird der Wert nicht im Stream gefunden wird das Defaultvalue zur�ckgegeben.
 --------------------------------------------------------------------*)
function TQOMethod.GetAttrValDef(aName: string; aXMLItem: TIcXMLElement; aDef: variant): Variant;
var
  xAttribute: TIcXMLAttr;
begin
  result := aDef;
  xAttribute := aXMLItem.GetAttributeNode(aName);
  if assigned(xAttribute) then
    result := xAttribute.Value;
end;// TQOMethod.GetAttrValDef cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetIsInOfflimit
 *  Klasse:           TQOMethod
 *  Kategorie:        No category 
 *  Argumente:        (aValue, aShiftToCalc, aShiftID, aStyleID)
 *
 *  Kurzbeschreibung: Vergleicht den Artikelwert mit der Berechnungsvorschrift
 *  Beschreibung:     
                      Je nach Methode kann es sein, dass der Artikelwert in einem Intervall liegen
                      muss. Um diesen Vergleich zu entkoppeln, kann jede abgeleitete Methode
                      den Vergleich selber vornehmen.
 --------------------------------------------------------------------*)
function TQOMethod.GetIsInOfflimit(aValue: extended; aShiftToCalc, aShiftID, aStyleID: integer): Boolean;
var
  xLimitValue: extended;
begin
  result := false;
  
  if FGreaterThan then
    result := aValue > CalculateLimitValue(aShiftToCalc, aShiftID, aStyleID)
  else
    result := aValue < CalculateLimitValue(aShiftToCalc, aShiftID, aStyleID);
end;// TQOMethod.GetIsInOfflimit cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetLenBase
 *  Klasse:           TQOMethod
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die global eingestellte L�ngenbasis zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOMethod.GetLenBase: TLenBase;
begin
  result := lb100KM;
  
  with TMMSettingsReader.Instance do
    result := TLenBase(Value[cMMUnit]);
end;// TQOMethod.GetLenBase cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetOfflimitData
 *  Klasse:           TQOMethod
 *  Kategorie:        No category 
 *  Argumente:        (aAlarm)
 *
 *  Kurzbeschreibung: Ermittelt die Datenreihe f�r alle Produktionsgruppen die im Offlimit sind.
 *  Beschreibung:     
                      Zuerst wird mit der Methode 'Evaluate' festgestellt ob- und
                      welche Produktionsgruppen im Offlimit sind. Danach werden alle 
                      Methoden nach der Verkn�pfungsvorschrift verkn�pft. 
                      Dieser Vorgang wird dann f�r den Alarm und die DataItems wiederholt. 
                      Auch hier k�nnen Produktionsgruppen entfallen. 
                      Erst wenn klar ist, welche Produktionsgruppen f�r einen Alarm im Offlimit
                      sind, werden die Daten �ber die gew�hlte Anzahl Schichten abgefragt.
                      Dabei k�nnten Produktionsgruppen entfallen (z.B. Zwei Methoden 
                      definiert aber die Produktionsgruppe ist nur in einer Methode im Offlimit).
 --------------------------------------------------------------------*)
procedure TQOMethod.GetOfflimitData(aAlarm: TQOAlarm);
var
  xValue: Extended;
  i: Integer;
  xDataPool: TNativeAdoQuery;
  xAllShifts: TNativeAdoQuery;
  xShiftCount: Integer;
  xShiftToCalc: Integer;
  xClassDefects: Integer;
begin
  xDataPool   := nil;
  xAllShifts  := nil;
  
  xDataPool   := TNativeAdoQuery.Create;
  xAllShifts  := TNativeAdoQuery.Create;

//: ----------------------------------------------
  try
    if assigned(mDataItem) then begin

//: ----------------------------------------------
      // Alle ProdIDs die nicht im Alarm registriert sind entfernen
      i := 0;
      while (i < FProdIDsInOfflimit.Count) do begin
        if aAlarm.ProdIDsInOfflimit.IndexOf(FProdIDsInOfflimit[i]) = -1 then
          FProdIDsInOfflimit.Delete(i)
        else
          inc(i);
      end;// while (i < FProdIDsInOfflimit.Count) do begin

//: ----------------------------------------------
      if FProdIDsInOfflimit.Count > 0 then begin
        // Original Klonen (CreateFields muss aufgerufen werden, da sonst im Query die Felder nicht bekannt sind)
        xDataPool.Recordset := TQODataPool.Instance.DataPool.Recordset.Clone(LockTypeEnum(adLockUnspecified));
        xDataPool.CreateFields;
        xAllShifts.Recordset := TQODataPool.Instance.AllShifts.Recordset.Clone(LockTypeEnum(adLockUnspecified));
        xAllShifts.CreateFields;

//: ----------------------------------------------
        // Anzahl Schichten f�r die Grafik
        xShiftCount := mLimitValueList.LimitValueCount;
  //      xShiftCount := ShiftCount + TMMSettingsReader.Instance.Value[cGraphicShiftDepth];
        for i := 0 to FProdIDsInOfflimit.Count - 1 do begin
          // Cursor auf die j�ngste Schicht
          xAllShifts.FindFirst;
          xShiftToCalc := 0;
          repeat
            xValue := 0;
            if not(xAllShifts.EOF) then begin
              // F�r jede ProdID nur die Daten der letzten Schicht anzeigen
              xDataPool.Recordset.Filter := Format('c_prod_id = %s AND c_shift_id = %s',[FProdIDsInOfflimit[i], xAllShifts.FieldByName('c_shift_id').AsString]);
              xDataPool.FindFirst;
              (* Wenn f�r die entsprechende ProdID ein Eintrag exisitert, dann kann der
                 Wert mit dem Limit verglichen werden. *)
              xValue := 0;
              if not(xDataPool.EOF) then begin
                // Hier ist klar, dass die Methode am Alarm beteiligt ist
                caught := true;
  (*              xValue := calcValue(FLenBase,                                          // Globale L�ngenbasis
                                    xDataPool.FieldByName(mDataItem.FieldName).AsFloat,  // zu rechnender Wert
                                    xDataPool.FieldByName('c_len').AsFloat,              // Produzierte L�nge
                                    mDataItem.CalcMode);                                 // Kalkulationsmodus (aus dem Konstantenarray)*)
                // ------------------ Daten speichern -------------------
                if (aAlarm.Values.FieldCount = 0) then begin
                  // Das erste Mal initialisieren und Felder erzeugen
                  aAlarm.Values.AddFields([cValueFieldName, cLimitValueFieldName, cShiftFieldName, cProdGroupFieldName, cProdGroupNameFieldName, cLenFieldName, cShiftStartFieldName, cFirstSpdFieldName, cLastSpindleFieldName, cMachineFieldName, cMachineNameFieldName, cStyleFieldName, cDataItemItemName, cDataItemFieldName, cClassDefectsFieldName, cMethodFieldName, cCalcModeFieldName, cLenBaseFieldName, cSFICntFieldName, cAdjustBaseFieldName, cAdjustBaseCntFieldName],
                                          [adDouble,        adDouble,             adInteger,       adInteger,           adVarChar,               adInteger,     adDate              , adInteger,          adInteger,             adInteger,         adVarChar,             adInteger,       adVarChar,         adVarChar,          adInteger,              adInteger,        adInteger,          adInteger,         adDouble,         adDouble            , adDouble]);
                  aAlarm.Values.Open;
                end;// if (aAlarm.Values.FieldCount = 0) then begin
  
                // Bei Matrix Items die Art abspeichern (-1 = ung�ltig)
                xClassDefects := -1;
                if mDataItem.IsMatrixDataItem then
                  xClassDefects := ord(mDataItem.ClassDefects);
  
                aAlarm.Values.Add([xDataPool.FieldByName(mDataItem.FieldName).AsFloat, // Wert (absolute Werte)
                                   CalculateLimitValue(xShiftToCalc, xAllShifts.FieldByName('c_shift_id').AsInteger, xDataPool.FieldByName('c_style_id').AsInteger),  // Grenzwert f�r die aktuelle Schicht
                                   xAllShifts.FieldByName('c_shift_id').AsInteger,     // Schicht ID
                                   StrToIntDef(FProdIDsInOfflimit[i], 0),              // Produktionsgruppe
                                   xDataPool.FieldByName('c_prod_name').AsString,      // Name der Produktionsgruppe
                                   xDataPool.FieldByName('c_len').AsFloat,             // Produzierte L�nge in Meter
                                   xDataPool.FieldByName('c_shift_start').AsDateTime,  // Schicht Start
                                   xDataPool.FieldByName('c_spindle_first').AsInteger, // Erste Spindel
                                   xDataPool.FieldByName('c_spindle_last').AsInteger,  // Letzte Spindel
                                   xDataPool.FieldByName('c_machine_id').AsInteger,    // Maschinen ID
                                   xDataPool.FieldByName('c_machine_name').AsString,   // Name der Maschine
                                   xDataPool.FieldByName('c_style_id').AsInteger,      // Artikel ID
                                   mDataItem.QOVisualize.Caption,                      // Name des DataItem
                                   mDataItem.FieldName,                                // Feldname in der Datenmenge
                                   xClassDefects,                                      // Art des DataItems (cut, uncut, both)
                                   ord(MethodType),                                    // Typ der Methode
                                   mDataItem.CalcMode,                                 // Berechnungsmethode f�r den Wert per Einheit (zB /100km)
                                   ord(FLenBase),                                      // L�ngenbasis mit denen die Werte ermittelt wurden
                                   xDataPool.FieldByName('c_SFICnt').AsFloat,          // Anzahl SFI Werte
                                   xDataPool.FieldByName('c_AdjustBase').AsFloat,      // Wird f�r die Berechnung der SFI Werte gebraucht
                                   xDataPool.FieldByName('c_AdjustBaseCnt').AsFloat    // Anzahl der 'AdjustBase-Werte'
                                   ]);
              end;// if not(xDataPool.EOF) then begin
            end;// if not(xDataPool.EOF) do begin
            inc(xShiftToCalc);
            xAllShifts.Next;
          until (xShiftToCalc = xShiftCount);
        end;// for j := 0 to FProdIDsInOfflimit.Count - 1 do begin

//: ----------------------------------------------
      end;// if FProdIDsInOfflimit.Count > 0 then begin

//: ----------------------------------------------
    end;// if assigned(mDataItem) then begin
  
  finally
    FreeAndNil(xDataPool);
    FreeAndNil(xAllShifts);
  end;// try finally
end;// TQOMethod.GetOfflimitData cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetSampleShiftCount
 *  Klasse:           TQOMethod
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die Anzahl der Schichten bekannt die f�r die Berechnung des Mittelwertes ben�tigt werden
 *  Beschreibung:     
                      Wird ein Mittelwert ben�tigt, dann kann eine Ableitung diese Methode
                      �berschreiben und den Umfang der Stichprobe bekannt geben.
 --------------------------------------------------------------------*)
function TQOMethod.GetSampleShiftCount: Integer;
begin
  result := 0;
end;// TQOMethod.GetSampleShiftCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           Internal_Evaluate
 *  Klasse:           TQOMethod
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Wird intern aufgerufen, wenn ein Alarm berechnet werden soll
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOMethod.Internal_Evaluate;
var
  xValue: extended;
  i: Integer;
  xDataPool: TNativeAdoQuery;
  xAllShifts: TNativeAdoQuery;
  xShiftToCalc: Integer;
  
  function IsProdGroupInOfflimit(aIndex: integer): Boolean;
  begin
    result := Boolean(mProdIDList.Objects[aIndex])
  end;// function ProdGroupInOfflimit(aIndex: integer):boolean
  
  procedure SetProdGroupOfflimit(aIndex: integer; aValue: boolean);
  begin
    mProdIDList.Objects[aIndex] := Pointer(aValue);
  end;// procedure SetProdGroupOfflimit(aIndex: integer; aValue: boolean);
  
begin
  xDataPool   := nil;
  xAllShifts  := nil;
  
  xDataPool   := TNativeAdoQuery.Create;
  xAllShifts  := TNativeAdoQuery.Create;

//: ----------------------------------------------
  try
    if assigned(mDataItem) then begin

//: ----------------------------------------------
      // Original Klonen (CreateFields muss aufgerufen werden, da sonst im Query die Felder nicht bekannt sind)
      xDataPool.Recordset := TQODataPool.Instance.DataPool.Recordset.Clone(LockTypeEnum(adLockUnspecified));
      xDataPool.CreateFields;
      xAllShifts.Recordset := TQODataPool.Instance.AllShifts.Recordset.Clone(LockTypeEnum(adLockUnspecified));
      xAllShifts.CreateFields;
  
      // Nur weiter, wenn auch relevante Produktions Gruppen existieren (ohne Alarm macht es keinen grossen Sinn)
      if (Active) and (mDataItem.ProdIDs > '') then begin
        // Liste mit den ProdID's abf�llen
        xDataPool.Recordset.Sort := 'c_shift_id, c_style_id, c_machine_id';
  
        //-------------------  Feststellen ob im Offlimit ----------------------
  
  
        (* Zuerst feststellen ob �berhaupt eine Maschine in der letzten Schicht im Offlimit ist.
           Prozedere f�r jede ProdID durchf�hren *)
        // F�r jede relevante ProdID
        for i := 0 to mProdIDList.Count - 1 do begin
          // Initialisieren.
          SetProdGroupOfflimit(i, false);
          // Cursor auf die j�ngste Schicht
          xAllShifts.FindFirst;
          xShiftToCalc := 0;
          repeat
            if not(xAllShifts.EOF) then begin
              // F�r jede ProdID nur die Daten der letzten Schicht anzeigen
              xDataPool.Recordset.Filter := Format('c_prod_id = %s AND c_shift_id = %s',[mProdIDList[i], xAllShifts.FieldByName('c_shift_id').AsString]);
              xDataPool.FindFirst;
              (* Wenn f�r die entsprechende ProdID ein Eintrag exisitert, dann kann der
                 Wert mit dem Limit verglichen werden. Ist der Grenzwert �berschritten, dann wird ein "Flag" gesetzt *)
              if not(xDataPool.EOF) then begin
                xValue := calcAllValues(FLenBase,                                            // L�ngenbasis der Methode
                                        xDataPool.FieldByName(mDataItem.FieldName).AsFloat,  // zu rechnender Wert
                                        xDataPool.FieldByName('c_len').AsFloat,              // Produzierte L�nge
                                        mDataItem.CalcMode,                                  // Kalkulationsmodus (aus dem Konstantenarray)
                                        xDataPool.FieldByName('c_SFICnt').AsFloat,           // Anzahl SFI Werte
                                        xDataPool.FieldByName('c_AdjustBase').AsFloat,      // Wird f�r die Berechnung der SFI Werte gebraucht
                                        xDataPool.FieldByName('c_AdjustBaseCnt').AsFloat);   // Anzahl aufsummierter Werte
  
                (* Der Grenzwert wird erst jetzt berechnet, damit die unter Umst�nden aufwendige
                   Berechnung erst dann ausgef�hrt wird, wenn der Grenzwert auch ganz sicher
                   ben�tigt wird.
                   Das Argument (i=0) bewirkt, dass der Grenzwert nur einmal Berechnet werden muss (H�ngt
                   von der Implementierung der Methode ab). *)
                // Offlimit nur wenn alle Schichten (ShiftCount) im Ofllimit sind
                SetProdGroupOfflimit(i, GetIsInOfflimit(xValue, xShiftToCalc, xAllShifts.FieldByName('c_shift_id').AsInteger, xDataPool.FieldByName('c_style_id').AsInteger));
  //              SetProdGroupOfflimit(i, xValue > CalculateLimitValue(xShiftToCalc, xAllShifts.FieldByName('c_shift_id').AsInteger, xDataPool.FieldByName('c_style_id').AsInteger));
                inc(xShiftToCalc);
                xAllShifts.Next;
              end else begin
                SetProdGroupOfflimit(i, false);
              end;// if not(xAllShifts.EOF) then begin
            end else begin
              mProdIDList.Objects[i] := Pointer(false);
            end;// if not(xDataPool.EOF) do begin
          until ((xShiftToCalc = ShiftCount) or not(IsProdGroupInOfflimit(i)));
          if IsProdGroupInOfflimit(i) then begin
            // Zum Offlimit hinzuf�gen
            if FProdIDsInOfflimit.IndexOf(mProdIDList[i]) < 0 then
              FProdIDsInOfflimit.Add(mProdIDList[i]);
          end;// if IsProdGroupInOfflimit(i) then begin
        end;// for j := 0 to xProdIDList.Count - 1 do begin
  
      end;//  if (Active) and (xProdIDs > '') then begin

//: ----------------------------------------------
    end;// if assigned(mDataItem) then begin
  
  finally
    FreeAndNil(xDataPool);
    FreeAndNil(xAllShifts);
  end;// try finally
end;// TQOMethod.Internal_Evaluate cat:No category

//:-------------------------------------------------------------------
(*: Member:           MethodValid
 *  Klasse:           TQOMethod
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt zur�ck ob alle Parameter mit sinvollen Werten belegt sind
 *  Beschreibung:     
                      Nat�rlich kann nicht bis ins letzte Detail bestimmt werden, ob 
                      Einstellungen sinnvoll sind. Hier ist vor allem daf�r zu sorgen,
                      dass die G�ltigkeit der Werte sichergestellt ist.
 --------------------------------------------------------------------*)
function TQOMethod.MethodValid: Boolean;
begin
  (* Soll eine G�ltigkeitspr�fung durchgef�hrt werden,
     dann muss diese Methode �berschrieben werden *)
  result := true;
end;// TQOMethod.MethodValid cat:No category

//:-------------------------------------------------------------------
(*: Member:           ReadXMLMethodeSettings
 *  Klasse:           TQOMethod
 *  Kategorie:        No category 
 *  Argumente:        (aXMLItem)
 *
 *  Kurzbeschreibung: Liest die Settings der Methode aus dem XML Stream
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOMethod.ReadXMLMethodeSettings(aXMLItem: TIcXMLelement);
var
  xValue: String;
  
  function GetBoolAttr(aAttr: string): Boolean;
  begin
    result := AnsiSameText(aAttr, 'true');
  end;// function GetBoolAttr(aAttr: string): Boolean;
  
begin
  if assigned(aXMLItem) then begin
    Active := GetBoolAttr(GetAttrValDef('Active', aXMLItem, false));
  
    // ! A CH T U N G ! ShowDetails muss Published definiert sein (RTTI)
    xValue := GetAttrValDef('ShowDetails', aXMLItem, 0);
    try
      SetEnumProp(self, 'ShowDetails', xValue );
    except
      CodeSite.SendError('TQOMethod.ReadXMLMethodeSettings: type ShowDetails(' + xValue + ') not found.');
    end;// try except
    ShiftCount := GetAttrValDef('ShiftCount', aXMLItem, cDefaultShiftCount);
  end;// if assigned(aItem) then begin
end;// TQOMethod.ReadXMLMethodeSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveDefToDB
 *  Klasse:           TQOMethod
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Speichert die Settings der Methode auf der Datenbank
 *  Beschreibung:     
                      Die Nachfolger m�ssen die Methoden "WriteXMLSettings" und "ReadXMLSettings" 
                      �berschreiben um ihre Settings auf die Datenbank abzubilden. Die zus�tzlichen
                      Daten finden sich dann im XML Stream wieder und k�nnen von da wieder gelesen werden.
 --------------------------------------------------------------------*)
function TQOMethod.SaveDefToDB: Boolean;
var
  mDB: TAdoDBAccess;
  
  const
    cInsertMethod = 'INSERT INTO t_QO_Method_Setting ' +
                    '(c_QOMethod_id, c_QOItem_id, c_Type, c_Settings) ' +
                    'VALUES (:c_QOMethod_id, :c_QOItem_id, :c_Type, :c_Settings)';
  
    cUpdateMethod = 'UPDATE t_QO_Method_Setting ' +
                    'SET c_QOItem_id = :c_QOItem_id, c_Type = :c_Type, c_Settings = :c_Settings ' +
                    'WHERE c_QOMethod_id = :c_QOMethod_id';
  
    cDeleteMethod = 'DELETE t_QO_Method_Setting ' +
                    'WHERE c_QOMethod_id = :c_QOMethod_id';
  
  (*-----------------------------------------------------------
    Setzt die ADO Parameter
  -------------------------------------------------------------*)
  procedure SetParameter;
  var
    xXMLDoc: TIcXMLDocument;
    xXML: TIcXMLParser;
    xXMLQO: TIcXMLElement;
    xXMLMethod: TIcXMLElement;
    xXMLSettingsNode: TIcXMLElement;
    xStream: TStringStream;
  begin
    // Initialisieren
    xXMLDoc := nil;
    xXML    := nil;
  
    try
      // Der Parser ist zust�ndig f�r das erzeugen von XML
      xXML := TIcXMLParser.Create(nil);
      // "virtuelles" XML Dokument (wird sp�ter in einen Stream erzeugt)
      xXMLDoc := TIcXMLDocument.Create;
      xXMLDoc.AssignParser(xXML);
  
      if assigned(xXMLDoc) then begin
        // Q-Offlimit Settings
        xXMLQO := xXMLDoc.CreateElement(cXMLQOSettings);
        xXMLDoc.SetDocumentElement(xXMLQO);
        // Methode
        xXMLMethod := xXMLDoc.CreateElement(cXMLMethodID);
        if assigned(xXMLMethod) then begin
          xXMLMethod.SetAttribute(cXMLMethodType, GetEnumName(TypeInfo(TCalcMethod),ord(MethodType)));
          // Methode zum Dokument hinzuf�gen
          xXMLQO.AppendChild(xXMLMethod);
          // Erstmal den Knoten f�r die Settings erzeugen
          xXMLSettingsNode := xXMLDoc.CreateElement(cXMLMethodSettingsID);
          xXMLMethod.AppendChild(xXMLSettingsNode);
          // Settings speichern
          WriteXMLMethodSettings(xXMLSettingsNode);
        end;// if assigned(xXMLMethod) then begin
      end;// if assigned(xXMLDoc) then begin
  
      // Der Stream nimmt den XML Formatierten String auf
      xStream := TStringStream.Create('');
      try
        // XML String erzeugen
        xXMLDoc.Write(xStream);
        // ... und dem Parameter zuweisen
        mDB.Query[0].ParamByName('c_Settings').AsString  := xStream.DataString;
      finally
        xStream.Free;
      end;// try finally
    finally
      FreeAndNil(xXMLDoc);
      FreeAndNil(xXML);
    end;// try finally
  
  
    mDB.Query[0].ParamByName('c_Type').AsInteger := ord(FMethodType);
    mDB.Query[0].ParamByName('c_QOItem_id').AsInteger := integer(ItemID);
  
    // Die ID wird nur f�r UPDATE ben�tigt
    if ID <> cINVALID_ID then
      mDB.Query[0].ParamByName('c_QOMethod_id').AsInteger := ID;
  end;// procedure SetParameter;
  
  (*-----------------------------------------------------------
    F�gt einen neuen Datensatz in die Tabelle ein
  -------------------------------------------------------------*)
  procedure InsertMethod;
  begin
    mDB.Query[0].SQL.Text := cInsertMethod;
    SetParameter;
    // Neuen Datensatz einf�gen und die ID merken
    ID := mDB.Query[0].InsertSQL('t_QO_Method_Setting', 'c_QOMethod_id', MAXINT, 1);
  end;// procedure InsertMethod;
  
  (*-----------------------------------------------------------
    Ver�ndert einen bestehenden Datensatz
  -------------------------------------------------------------*)
  procedure UpdateMethod;
  begin
    mDB.Query[0].SQL.Text := cUpdateMethod;
    SetParameter;
    // Datensatz Updaten
    if mDB.Query[0].ExecSQL = 0 then begin
      // Wenn kein Datensatz betroffen war, dann den Datensatz neu erzeugen
      ID := cINVALID_ID;
      InsertMethod;
    end;// if mDB.Query[0].ExecSQL = 0 then begin
  end;// procedure UpdateMethod;
  
  (*-----------------------------------------------------------
    L�scht einen Datensatz
  -------------------------------------------------------------*)
  procedure DeleteMethod;
  begin
    mDB.Query[0].SQL.Text := cDeleteMethod;
    mDB.Query[0].ParamByName('c_QOMethod_id').AsInteger := ID;
  
    mDB.Query[0].ExecSQL;
    FDeleteState := dsDeleted;
  end;// procedure DeleteMethod;
  
begin
  result := false;
  
  mDB := TAdoDBAccess.Create(1);
  try
    mDB.Init;
  
    if FID = cINVALID_ID then begin
      if FDeleteState = dsValid then
        InsertMethod
      else
        FDeleteState := dsDeleted;
    end else begin
      if FDeleteState = dsPending then begin
        DeleteMethod;
      end else begin
        if FDeleteState = dsValid then
          UpdateMethod;
      end;// if FDeleteState = dsPending then begin
    end;// if FID = cINVALID_ID then begin
  
  
  
  (*  // Je nachdem einen neuen DS erzeugen oder einen bestehenden aktualisieren
    if FID = cINVALID_ID then
      InsertMethod
    else
      UpdateMethod;*)
  
    // Erfolgreich
    result := true;
  finally
    FreeAndNil(mDB);
  end;// with TAdoDBAccess.Create(1) do try
end;// TQOMethod.SaveDefToDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           WriteXMLMethodSettings
 *  Klasse:           TQOMethod
 *  Kategorie:        No category 
 *  Argumente:        (aXMLItem)
 *
 *  Kurzbeschreibung: Gibt die Methoden Settings als XML formatierten String zur�ck
 *  Beschreibung:     
                      Abgeleitete Klassen m�ssend diese Methode �berschreiben um 
                      die Settings als XML String auf der Datenbank abzulegen.
 --------------------------------------------------------------------*)
procedure TQOMethod.WriteXMLMethodSettings(aXMLItem: TIcXMLElement);
begin
  // Dann alle Eigenschaften in den Stream schreiben
  if Active then
    aXMLItem.SetAttribute('Active', 'True')
  else
    aXMLItem.SetAttribute('Active', 'False');
  
  aXMLItem.SetAttribute('ShowDetails', GetEnumName(TypeInfo(TMethodShowDetail), ord(ShowDetails)));
  aXMLItem.SetAttribute('ShiftCount', IntToStr(ShiftCount));
end;// TQOMethod.WriteXMLMethodSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOItems
 *  Kategorie:        No category 
 *  Argumente:        (aPersistent)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOItems.Create(aPersistent: boolean = false);
begin
  inherited Create(aPersistent);

//: ----------------------------------------------
  ItemsLoaded := false;
  
  // Sammelt die Produktionsgruppen die im Offlimit sind
  FProdIDsInOfflimit := TStringList.Create;
  FProdIDsInOfflimit.Duplicates := dupIgnore;
end;// TQOItems.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TQOItems
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TQOItems.Destroy;
begin
  FreeAndNil(FProdIDsInOfflimit);

//: ----------------------------------------------
  inherited Destroy;
end;// TQOItems.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           DeleteFromDB
 *  Klasse:           TQOItems
 *  Kategorie:        No category 
 *  Argumente:        (aQuery)
 *
 *  Kurzbeschreibung: L�scht den Datensatz von der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOItems.DeleteFromDB(aQuery: TNativeAdoQuery): Boolean;
var
  i: Integer;
  xDeleted: Boolean;
begin
  // Result soll mit True initialisiert werden, wenn Items definiert sind
  result := (ItemCount = 0);
  
  // Alle Items vond er DB l�schen
  for i := 0 to ItemCount - 1 do begin
    xDeleted := Items[i].DeleteFromDB(aQuery);
    if i > 0 then
      result := xDeleted and result
    else
      result := xDeleted;
  end;// for i := 0 to ItemCount - 1 do begin
end;// TQOItems.DeleteFromDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           Evaluate
 *  Klasse:           TQOItems
 *  Kategorie:        No category 
 *  Argumente:        (aBoolOpAnd)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn ein Alarm berechnet werden soll
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOItems.Evaluate(aBoolOpAnd: boolean);
var
  i: Integer;
  xFirstActiveListAssigned: boolean;
  j: Integer;
begin
  FProdIDsInOfflimit.Clear;

//: ----------------------------------------------
  for i := 0 to ItemCount - 1 do
    Items[i].Evaluate;

//: ----------------------------------------------
  // Feststellen welche Produktionsgruppen dem Kriterium 'aBoolOpAnd' entsprechen
  if aBoolOpAnd then begin
    xFirstActiveListAssigned := false;
    // Alle Items m�ssen im Alarm sein
    for i := 0 to ItemCount - 1 do begin
      if Items[i].Active then begin
        if not(xFirstActiveListAssigned) then begin
          // Die erste Liste komplet �bernehmen
          FProdIDsInOfflimit.CommaText := Items[i].ProdIDsInOfflimit.CommaText;
          xFirstActiveListAssigned := true;
        end else begin
          // Alle weiteren Listen vergleichen
          j := 0;
          while (j < FProdIDsInOfflimit.Count) do begin
            // Wenn eine ProdID in der Vergleichsliste nicht vorkommt, dann diesen Eintrag entfernen
            if Items[i].ProdIDsInOfflimit.IndexOf(FProdIDsInOfflimit[j]) = -1 then
              FProdIDsInOfflimit.Delete(j)
            else
              inc(j);
          end;// while (j < FProdIDsInOfflimit.Count) do begin
        end;// if FProdIDsInOfflimit.Count = 0 then begin
      end;// for i := 0 to ItemCount - 1 do begin
    end;// for i := 0 to ItemCount - 1 do begin
  end else begin
    // Mindestens ein Item muss im Alarm sein
    for i := 0 to ItemCount - 1 do begin
      if Items[i].Active then
        for j := 0 to Items[i].ProdIDsInOfflimit.Count -1 do
          FProdIDsInOfflimit.Add(Items[i].ProdIDsInOfflimit[j]);
    end;// for i := 0 to ItemCount - 1 do begin
  end;// if aBoolOpAnd then begin
end;// TQOItems.Evaluate cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetActiveDataItemCount
 *  Klasse:           TQOItems
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die Anzahl aktivierter DataItems zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOItems.GetActiveDataItemCount: Integer;
var
  i: Integer;
begin
  result := 0;
  for i := 0 to ItemCount - 1 do begin
    if Items[i].Active then
      inc(result);
  end;// for i := 0 to ItemCount - 1 do begin
end;// TQOItems.GetActiveDataItemCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetItemCount
 *  Klasse:           TQOItems
 *  Kategorie:        List 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Anzahl der Elemente in der Liste
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOItems.GetItemCount: Integer;
begin
  result := DataItemCount;
end;// TQOItems.GetItemCount cat:List

//:-------------------------------------------------------------------
(*: Member:           GetItems
 *  Klasse:           TQOItems
 *  Kategorie:        List 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: Zugriffsmethode auf ein Element aus der Liste
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOItems.GetItems(aIndex: Integer): TQOItem;
begin
  result := TQOItem(DataItems[aIndex]);
end;// TQOItems.GetItems cat:List

//:-------------------------------------------------------------------
(*: Member:           GetMaxShiftCount
 *  Klasse:           TQOItems
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Sammelt von allen Methoden die Anzahl der Schichten
 *  Beschreibung:     
                      Jede Methode definiert eine gewisse Anzahl Schichten die
                      mindestens an einer Verletzung beteiligt sein m�ssen um
                      einen Alarm zu erzeugen. 
                      Diese Funktion ermittelt die Methode (resp. Anzahl Schichten)
                      in der die meisten Schichten definiert sind. 
                      Heraus kommt die Anzahl Schichten die mindestens im globalen
                      "Datenhaufen" vorhanden sein m�ssen um alle Methoden
                      auswerten zu k�nnen.
 --------------------------------------------------------------------*)
function TQOItems.GetMaxShiftCount: Integer;
var
  i: Integer;
  xShiftCount: Integer;
begin
  result := 0;

//: ----------------------------------------------
  for i := 0 to ItemCount - 1 do begin
    xShiftCount := Items[i].GetMaxShiftCount;
    if xShiftCount > result then
      result := xShiftCount;
  end;// for i := 0 to ItemCount - 1 do begin
end;// TQOItems.GetMaxShiftCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetOfflimitData
 *  Klasse:           TQOItems
 *  Kategorie:        No category 
 *  Argumente:        (aAlarm)
 *
 *  Kurzbeschreibung: Ermittelt die Datenreihe f�r alle Produktionsgruppen die im Offlimit sind.
 *  Beschreibung:     
                      Zuerst wird mit der Methode 'Evaluate' festgestellt ob- und
                      welche Produktionsgruppen im Offlimit sind. Danach werden alle 
                      Methoden nach der Verkn�pfungsvorschrift verkn�pft. 
                      Dieser Vorgang wird dann f�r den Alarm und die DataItems wiederholt. 
                      Auch hier k�nnen Produktionsgruppen entfallen. 
                      Erst wenn klar ist, welche Produktionsgruppen f�r einen Alarm im Offlimit
                      sind, werden die Daten �ber die gew�hlte Anzahl Schichten abgefragt.
                      Dabei k�nnten Produktionsgruppen entfallen (z.B. Zwei Methoden 
                      definiert aber die Produktionsgruppe ist nur in einer Methode im Offlimit).
 --------------------------------------------------------------------*)
procedure TQOItems.GetOfflimitData(aAlarm: TQOAlarm);
var
  i: Integer;
begin
  for i := 0 to Itemcount - 1 do
    Items[i].GetOfflimitData(aAlarm);
end;// TQOItems.GetOfflimitData cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOfCaption
 *  Klasse:           TQOItems
 *  Kategorie:        No category 
 *  Argumente:        (aCaption)
 *
 *  Kurzbeschreibung: Gibt die Position des DataItems anhand der �berschrift an
 *  Beschreibung:     
                      Die �berschrift beinhaltet f�r Matrix Items auch die Information ob
                      die geschnittenen, die ungeschnittenen oder beide Daten gew�nscht werden.
 --------------------------------------------------------------------*)
function TQOItems.IndexOfCaption(aCaption: string): Integer;
var
  xIndex: Integer;
  xFound: Boolean;
begin
  result := -1;

//: ----------------------------------------------
  xIndex := 0;
  xFound := false;
  
  while (xIndex < ItemCount) and (not(xFound)) do begin
    xFound := AnsiSameText(aCaption, Items[xIndex].QOVisualize.Caption);
    // Weitersuchen, oder Resultat sichern
    if not(xFound) then
      inc(xIndex)
    else
      result := xIndex;
  end;// while (xIndex < ItemCount) and (not(xFound)) do begin
end;// TQOItems.IndexOfCaption cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOfItem
 *  Klasse:           TQOItems
 *  Kategorie:        No category 
 *  Argumente:        (aQOItem)
 *
 *  Kurzbeschreibung: Liefert den Index des entsprechenden Items
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOItems.IndexOfItem(aQOItem: TQOItem): Integer;
begin
  result := IndexOf(aQOItem);
end;// TQOItems.IndexOfItem cat:No category

//:-------------------------------------------------------------------
(*: Member:           LoadFromDatabase
 *  Klasse:           TQOItems
 *  Kategorie:        No category 
 *  Argumente:        (aDataItemDecoratorClass)
 *
 *  Kurzbeschreibung: L�dt die DataItems von der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOItems.LoadFromDatabase(aDataItemDecoratorClass: TDataItemDecoratorClass = nil);
var
  i: Integer;
begin
  inherited LoadFromDatabase(aDataItemDecoratorClass);

//: ----------------------------------------------
  for i := 0 to DataItemCount - 1 do
    TQOItem(DataItems[i]).QOVisualize.Caption := TQOItem(DataItems[i]).DisplayName;

//: ----------------------------------------------
  ItemsLoaded := true;
end;// TQOItems.LoadFromDatabase cat:No category

//:-------------------------------------------------------------------
(*: Member:           NamesAsString
 *  Klasse:           TQOItems
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die Namen der DataItems als Kommasaparierten String zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOItems.NamesAsString: String;
var
  i: Integer;
begin
  result := '';
  
  for i := 0 to ItemCount - 1 do
    if Items[i].Active then
      result := result + ',' + '"' + Items[i].QOVisualize.Caption + '"';
  
  if result > '' then
    system.delete(result , 1, 1);
end;// TQOItems.NamesAsString cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveDefToDB
 *  Klasse:           TQOItems
 *  Kategorie:        No category 
 *  Argumente:        (aAlarmID)
 *
 *  Kurzbeschreibung: Speichert die Settings auf der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOItems.SaveDefToDB(aAlarmID: integer): Boolean;
var
  xSaved: Boolean;
  i: Integer;
begin
  result := true;
  // Die ID des zugeh�rigen Alarms f�r jedes Item setzen
  AlarmID := aAlarmID;

//: ----------------------------------------------
  // Jedes Item ist selber f�r seine Speicherung zust�ndig (Pro Item ein DS)
  for i := 0 to ItemCount - 1 do begin
    xSaved := Items[i].SaveDefToDB;
    result := result and xSaved;
  end;// for i := 0 to ItemCount do begin

//: ----------------------------------------------
  // Items die auf der Datenbank gel�scht sind, auch im Alarm l�schen
  i := 0;
  while i < ItemCount do begin
    if Items[i].DeleteState = dsDeleted then
      Delete(i)
    else
      inc(i);
  end;// while i < ItemCount do begin
end;// TQOItems.SaveDefToDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetAlarmId
 *  Klasse:           TQOItems
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: ID des zugeh�rigen Alarms f�r die Datenbank
 *  Beschreibung:     
                      Die ID muss f�r alle Items gesetzt werden.
 --------------------------------------------------------------------*)
procedure TQOItems.SetAlarmId(Value: Integer);
var
  i: Integer;
begin
  FAlarmId := Value;

//: ----------------------------------------------
  for i := 0 to ItemCount - 1 do
    Items[i].AlarmID := FAlarmID;
end;// TQOItems.SetAlarmId cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetProdIDs
 *  Klasse:           TQOItems
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Verteilt die zugewiesenen ProdIDs an die Items
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOItems.SetProdIDs(const Value: String);
var
  i: Integer;
begin
  for i := 0 to ItemCount - 1 do
    Items[i].ProdIDs := Value;
end;// TQOItems.SetProdIDs cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetQOAlarm
 *  Klasse:           TQOItems
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Verteilt Alarm an die Items
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOItems.SetQOAlarm(Value: TQOAlarm);
var
  i: Integer;
begin
  for i := 0 to ItemCount - 1 do
    Items[i].QOAlarm := Value;
end;// TQOItems.SetQOAlarm cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOItem.Create;
begin
  inherited Create;

//: ----------------------------------------------
  FQOMethods := TQOMethods.Create(self);
  FQOVisualize := TQOVisualizeImpl.Create;

//: ----------------------------------------------
  // Standardm�ssig eine ung�ltige ID zuweisen
  ID := cINVALID_ID;
  
  // L�schstatus initialisieren
  FDeleteState := dsValid;
  
  // Sammelt die Produktionsgruppen die im Offlimit sind
  FProdIDsInOfflimit := TStringList.Create;
  FProdIDsInOfflimit.Duplicates := dupIgnore;

//: ----------------------------------------------
  // Interface Initialisieren
  FQOVisualize.OnGetImageIndex := GetImageIndex;
  FQOVisualize.OnGetActive := OnGetActive;
  FQOVisualize.OnGetCaption := OnGetCaption;
end;// TQOItem.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TQOItem.Destroy;
begin
  FreeAndNil(FProdIDsInOfflimit);

//: ----------------------------------------------
  FreeAndNil(FQOMethods);
  FreeAndNil(FQOVisualize);

//: ----------------------------------------------
  inherited Destroy;
end;// TQOItem.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Eigenschaften �bernehmen
 *  Beschreibung:     
                      Dient zum kopieren von Objekten
 --------------------------------------------------------------------*)
procedure TQOItem.Assign(Source: TPersistent);
var
  xSource: TQOItem;
begin
  if Source is TQOItem then begin
    xSource := TQOItem(Source);
    // Alle Eigenschaften kopieren
    AlarmID     := xSource.AlarmID;
    BoolOpAnd   := xSource.BoolOpAnd;
    CalcMode    := xSource.CalcMode;
    DeleteState := xSource.DeleteState;
    ID          := xSource.ID;
    ProdIDs     := xSource.ProdIDs;
    QOAlarm     := xSource.QOAlarm;
  
    FQOMethods.Assign(xSource.QOMethods);
    FQOVisualize.Assign(xSource.QOVisualize);
  
    (* Modified darf erst am Ende gesetzt werden, da das Flag bei jeder
       Zuweisung gesetzt wird.
       Dies muss auch in einem Nachfolger geschehen*)
    inherited Assign(Source);
  
  end else begin
    // Weiterreichen
    inherited Assign(Source);
  end;// if Source is TQOItem then begin
end;// TQOItem.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           DeleteFromDB
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        (aQuery)
 *
 *  Kurzbeschreibung: L�scht den Datensatz von der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOItem.DeleteFromDB(aQuery: TNativeAdoQuery): Boolean;
  
  const
    cDeleteItem = 'DELETE FROM t_QO_Item_Setting WHERE c_QOItem_id = :c_QOItem_id';
  
begin
  result := false;
  
  if QOMethods.DeleteFromDB(aQuery) then begin
    aQuery.SQL.Text := cDeleteItem;
    aQuery.ParamByName('c_QOItem_id').AsInteger := FID;
  
    // Datensatz gel�scht, wenn ein Datensatz betroffen ist
    result := (aQuery.ExecSQL = 1);
  
    // Die Datens�tze der Methoden l�schen
    QOMethods.DeleteFromDB(aQuery);
  end;// if QOMethods.DeleteFromDB(aQuery) then begin
end;// TQOItem.DeleteFromDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           Evaluate
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn ein Alarm berechnet werden soll
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOItem.Evaluate;
begin
  FProdIDsInOfflimit.Clear;

//: ----------------------------------------------
  QOMethods.Evaluate(BoolOpAnd);

//: ----------------------------------------------
  // ProdID's die im Offlimit sind merken
  FProdIDsInOfflimit.Assign(QOMethods.ProdIDsInOfflimit);
end;// TQOItem.Evaluate cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetActive
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt True zur�ck, wenn mindestens eine Methode aktiviert ist
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOItem.GetActive: Boolean;
var
  i: Integer;
begin
  i := 0;
  result := false;

//: ----------------------------------------------
  // Nach mindestens einer Methode suchen die aktiviert ist
  while (i < QOMethods.MethodCount) and(not(result)) do begin
    if QOMethods[i].Active then
      result := true;
    inc(i);
  end;// while (i < QOMethods.MethodCount) and(not(result)) do begin
end;// TQOItem.GetActive cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetImageIndex
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aImageKind, aImageIndex)
 *
 *  Kurzbeschreibung: Fragt den ImageIndex f�r die Anzeige ab
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOItem.GetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer);
begin
  // Default immer als normales DataItem
  aImageIndex := TImageIndexHandler.Instance.DataItem;
  
  (* Es kann nicht mit dem Operator 'is' gearbeitet werden, da
     'DataItemType' eine Klassenreferenz ist *)
  if DataItemType.InheritsFrom(TClassDef) then begin
    if Predefined then begin
      aImageIndex := TImageIndexHandler.Instance.MatrixTrendImageIndex
    end else begin
      aImageIndex := TImageIndexHandler.Instance.MatrixTrendEditImageIndex;
    end;// if aDataItem.Predefined then begin
    if DataItemType.InheritsFrom(TCompositClassDef) then
      aImageIndex := TImageIndexHandler.Instance.FunctionImageIndex;
  end;// if DataItemType.InheritsFrom(TClassDef) then begin
end;// TQOItem.GetImageIndex cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMaxShiftCount
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Sammelt von allen Methoden die Anzahl der Schichten
 *  Beschreibung:     
                      Jede Methode definiert eine gewisse Anzahl Schichten die
                      mindestens an einer Verletzung beteiligt sein m�ssen um
                      einen Alarm zu erzeugen. 
                      Diese Funktion ermittelt die Methode (resp. Anzahl Schichten)
                      in der die meisten Schichten definiert sind. 
                      Heraus kommt die Anzahl Schichten die mindestens im globalen
                      "Datenhaufen" vorhanden sein m�ssen um alle Methoden
                      auswerten zu k�nnen.
 --------------------------------------------------------------------*)
function TQOItem.GetMaxShiftCount: Integer;
begin
  result := QOMethods.GetMaxShiftCount;
end;// TQOItem.GetMaxShiftCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetOfflimitData
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        (aAlarm)
 *
 *  Kurzbeschreibung: Ermittelt die Datenreihe f�r alle Produktionsgruppen die im Offlimit sind.
 *  Beschreibung:     
                      Zuerst wird mit der Methode 'Evaluate' festgestellt ob- und
                      welche Produktionsgruppen im Offlimit sind. Danach werden alle 
                      Methoden nach der Verkn�pfungsvorschrift verkn�pft. 
                      Dieser Vorgang wird dann f�r den Alarm und die DataItems wiederholt. 
                      Auch hier k�nnen Produktionsgruppen entfallen. 
                      Erst wenn klar ist, welche Produktionsgruppen f�r einen Alarm im Offlimit
                      sind, werden die Daten �ber die gew�hlte Anzahl Schichten abgefragt.
                      Dabei k�nnten Produktionsgruppen entfallen (z.B. Zwei Methoden 
                      definiert aber die Produktionsgruppe ist nur in einer Methode im Offlimit).
 --------------------------------------------------------------------*)
procedure TQOItem.GetOfflimitData(aAlarm: TQOAlarm);
begin
  QOMethods.GetOfflimitData(aAlarm);

//: ----------------------------------------------
  // True, wenn mindestens eine Methode am Alarm beteiligt war
  Caught := QOMethods.GetCaught;
end;// TQOItem.GetOfflimitData cat:No category

//:-------------------------------------------------------------------
(*: Member:           OnGetActive
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Gibt True zur�ck, wenn das Objekt aktiviert ist
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOItem.OnGetActive(Sender: TObject): Boolean;
begin
  result := Active;
end;// TQOItem.OnGetActive cat:No category

//:-------------------------------------------------------------------
(*: Member:           OnGetCaption
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aCaption)
 *
 *  Kurzbeschreibung: Ver�ndert f�r Matrixklassen den Namen der Angezeigt wird
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOItem.OnGetCaption(Sender: TObject; var aCaption: string);
begin
  if IsMatrixDataItem then
    aCaption := Format(cDataItemNameFormat, [Translate(DisplayName), Translate(GetDefectTypeText(ClassDefects))])
  else
    aCaption := Translate(DisplayName);
end;// TQOItem.OnGetCaption cat:No category

//:-------------------------------------------------------------------
(*: Member:           QueryInterface
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        (IID, Obj)
 *
 *  Kurzbeschreibung: IUnknown
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOItem.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := S_OK
  else
    Result := E_NOINTERFACE;
end;// TQOItem.QueryInterface cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveDefToDB
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Speichert die Settings des DataItems auf der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOItem.SaveDefToDB: Boolean;
var
  mDB: TAdoDBAccess;
  
  const
    cInsertItem = 'INSERT INTO t_QO_Item_Setting ' +
                   '(c_QOItem_id, c_QOAlarm_id, c_Name, c_BoolOpAnd, c_class_defect) ' +
                   'VALUES (:c_QOItem_id, :c_QOAlarm_id, :c_Name, :c_BoolOpAnd, :c_class_defect)';
  
    cUpdateItem = 'UPDATE t_QO_Item_Setting ' +
                   'SET c_Name = :c_Name, c_BoolOpAnd = :c_BoolOpAnd, c_QOAlarm_id = :c_QOAlarm_id, c_class_defect = :c_class_defect ' +
                   'WHERE c_QOItem_id = :c_QOItem_id';
  
    cDeleteItem = 'DELETE t_QO_Item_Setting ' +
                  'WHERE c_QOItem_id = :c_QOItem_id';
  
  (*-----------------------------------------------------------
    Setzt die ADO Parameter
  -------------------------------------------------------------*)
  procedure SetParameter;
  begin
    mDB.Query[0].ParamByName('c_Name').AsString  := ItemName;
    mDB.Query[0].ParamByName('c_BoolOpAnd').AsInteger := integer(BoolOpAnd);
    mDB.Query[0].ParamByName('c_QOAlarm_id').AsInteger := integer(AlarmID);
    mDB.Query[0].ParamByName('c_class_defect').AsInteger := ord(ClassDefects);
  
    // Die ID wird nur f�r UPDATE ben�tigt
    if ID <> cINVALID_ID then
      mDB.Query[0].ParamByName('c_QOItem_id').AsInteger := ID;
  end;// procedure SetParameter;
  
  (*-----------------------------------------------------------
    F�gt einen neuen Datensatz in die Tabelle ein
  -------------------------------------------------------------*)
  procedure InsertItem;
  begin
    mDB.Query[0].SQL.Text := cInsertItem;
    SetParameter;
    // Neuen Datensatz einf�gen und die ID merken
    ID := mDB.Query[0].InsertSQL('t_QO_Item_Setting', 'c_QOItem_id', MAXINT, 1);
  end;// procedure InsertItem;
  
  (*-----------------------------------------------------------
    Ver�ndert einen bestehenden Datensatz
  -------------------------------------------------------------*)
  procedure UpdateItem;
  begin
    mDB.Query[0].SQL.Text := cUpdateItem;
    SetParameter;
    // Datensatz Updaten
    if mDB.Query[0].ExecSQL = 0 then begin
      // Wenn kein Datensatz betroffen war, dann den Datensatz neu erzeugen
      ID := cINVALID_ID;
      InsertItem;
    end;// if mDB.Query[0].ExecSQL = 0 then begin
  end;// procedure UpdateItem;
  
  procedure DeleteItem;
  begin
    mDB.Query[0].SQL.Text := cDeleteItem;
    mDB.Query[0].ParamByName('c_QOItem_id').AsInteger := ID;
  
    mDB.Query[0].ExecSQL;
    FDeleteState := dsDeleted;
  end;// procedure DeleteItem;
  
begin
  result := false;
  
  mDB := TAdoDBAccess.Create(1);
  try
    mDB.Init;
    // Je nachdem einen neuen DS erzeugen oder einen bestehenden aktualisieren
    if FID = cINVALID_ID then begin
      if FDeleteState = dsValid then
        InsertItem
      else
        FDeleteState := dsDeleted;
    end else begin
      if FDeleteState = dsPending then begin
        DeleteItem;
      end else begin
        if FDeleteState = dsValid then
          UpdateItem;
      end;// if FDeleteState = dsPending then begin
    end;// if FID = cINVALID_ID then begin
  
    // Methoden speichern
    if FID > cINVALID_ID then
      QOMethods.SaveDefToDB(FID);
  
    // Erfolgreich
    result := true;
  finally
    FreeAndNil(mDB);
  end;// with TAdoDBAccess.Create(1) do try
end;// TQOItem.SaveDefToDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetDeleteState
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: �Zugriffsmethode f�r DeleteState
 *  Beschreibung:     
                      Der L�sch Status muss auch an die Methode weitergegeben werden.
 --------------------------------------------------------------------*)
procedure TQOItem.SetDeleteState(Value: TDeleteState);
begin
  FDeleteState := Value;
  QOMethods.DeleteState := FDeleteState;
end;// TQOItem.SetDeleteState cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetID
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die ID (Datenbank) des DataItems
 *  Beschreibung:     
                      Die Id muss an die MEthoden weitergegeben werden.
 --------------------------------------------------------------------*)
procedure TQOItem.SetID(Value: Integer);
begin
  FID := Value;

//: ----------------------------------------------
  // ID weitergeben
  QOMethods.ItemId := FID;
end;// TQOItem.SetID cat:No category

//:-------------------------------------------------------------------
(*: Member:           _AddRef
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: IUnknown
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOItem._AddRef: Integer;
begin
  Result := -1;
  // non-refcounted memorymanagement
end;// TQOItem._AddRef cat:No category

//:-------------------------------------------------------------------
(*: Member:           _Release
 *  Klasse:           TQOItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: IUnknown
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOItem._Release: Integer;
begin
  Result := -1;
  // non-refcounted memorymanagement
end;// TQOItem._Release cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetSQLSelectFields
 *  Klasse:           TQODataItem
 *  Kategorie:        No category 
 *  Argumente:        (aWithSum, aWithAlias)
 *
 *  Kurzbeschreibung: Gibt die ben�tigten Felddefinitionen f�r das entsprechende DataItem zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQODataItem.GetSQLSelectFields(aWithSum: boolean = true; aWithAlias:boolean = true): String;
var
  i: TUsedTable;
  xTable: TUsedTable;
  xFound: Boolean;
  xFieldName: string;
begin
  Result := inherited GetSQLSelectFields(aWithSum, aWithAlias);

//: ----------------------------------------------
  result := '';
  
  xFieldName := FieldName;
  
  i := Low(TUsedTable);
  xFound := false;
  while (i < High(TUsedTable)) and(not(xFound)) do begin
    if (i in TableType) then begin
      xFound := true;
      xTable := i;
    end;// if (i in TableType) then
    inc(i);
  end;// while (i < High(TUsedTable)) and(not(xFound)) do begin
  
  if xFound then begin
    // Wird die komplete Definition verlangt, dann f�r die Summe vorbereiten
    if aWithSum then begin
      if AnsiSameText(FieldName, 'c_SFI_D') then begin
        result := LinkAlias + '.' + 'c_SFI';
      end else begin
        result := LinkAlias + '.' + xFieldName;
      end;// if AnsiSameText(FieldName, 'c_SFI_D') then begin
    end;// if aWithSum then begin
  
    // Je nachdem was gew�nscht ist, das komplete Statement zussammensetzen
    if aWithSum then begin
      if aWithAlias then
        // zb: SUM(cd_tmp.c_CU13 * 1.0 +cd_tmp.c_CU14 * 1.0 ) as "A3"
        result := Format (' SUM(%s) as "%s"', [result, xFieldName])
      else
        // zb: SUM(cd_tmp.c_CU13 * 1.0 +cd_tmp.c_CU14 * 1.0 )
        result := Format (' SUM(%s)', [result]);
    end;// if aWithSum then begin
  end;// if xFound then begin
end;// TQODataItem.GetSQLSelectFields cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetTableType
 *  Klasse:           TQODataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt immer utNone zur�ck da diese Felder in der View definiert sind
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQODataItem.GetTableType: TUsedTablesSet;
begin
  result := [utNone];
end;// TQODataItem.GetTableType cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOAssigns
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOAssigns.Create;
begin
  inherited;
  mAssigns := TList.Create;
end;// TQOAssigns.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TQOAssigns
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TQOAssigns.Destroy;
begin
  Clear;
  FreeAndNil(mAssigns);
  inherited;
end;// TQOAssigns.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           Add
 *  Klasse:           TQOAssigns
 *  Kategorie:        List 
 *  Argumente:        (aAssign)
 *
 *  Kurzbeschreibung: F�gt ein Element zur Liste hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAssigns.Add(aAssign: TQOAssign): Integer;
begin
  result := -1;
  if assigned(mAssigns) then
    mAssigns.Add(aAssign);
end;// TQOAssigns.Add cat:List

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TQOAssigns
 *  Kategorie:        No category 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Eigenschaften �bernehmen
 *  Beschreibung:     
                      Dient zum kopieren von Objekten
 --------------------------------------------------------------------*)
procedure TQOAssigns.Assign(Source: TPersistent);
var
  xSource: TQOAssigns;
  xAssign: TQOAssign;
  i: Integer;
begin
  if Source is TQOAssigns then begin
    xSource := TQOAssigns(Source);
    Clear;
    // Alle Eigenschaften kopieren
    for i := 0 to xSource.AssignCount - 1 do begin
      xAssign := TQOAssignClass(xSource.Assigns[i].ClassType).Create;
      xAssign.Assign(xSource.Assigns[i]);
      Add(xAssign);
    end;// for i := 0 to xSource.AssignCount - 1 do begin
  
  end else begin
    // Weiterreichen
    inherited Assign(Source);
  end;// if Source is TQOAssigns then begin
end;// TQOAssigns.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           Clear
 *  Klasse:           TQOAssigns
 *  Kategorie:        List 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�scht alle Elemente aus der Liste
 *  Beschreibung:     
                      Die Elemente werden freigegeben (Komposition).
 --------------------------------------------------------------------*)
procedure TQOAssigns.Clear;
begin
  while AssignCount > 0 do
    delete(0);
end;// TQOAssigns.Clear cat:List

//:-------------------------------------------------------------------
(*: Member:           Delete
 *  Klasse:           TQOAssigns
 *  Kategorie:        List 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: L�scht ein Element aus der Liste
 *  Beschreibung:     
                      Das Element wird freigegeben (Komposition)
 --------------------------------------------------------------------*)
procedure TQOAssigns.Delete(aIndex: integer);
begin
  if assigned(mAssigns) then begin
    if (aIndex < mAssigns.Count) and (aIndex >= 0) then begin
      Assigns[aIndex].Free;
      mAssigns.delete(aIndex);
    end;// if (aIndex < mAssigns.Count)  and (aIndex > 0) then begin
  end;// if assigned(mAssigns) then begin
end;// TQOAssigns.Delete cat:List

//:-------------------------------------------------------------------
(*: Member:           DeleteFromDB
 *  Klasse:           TQOAssigns
 *  Kategorie:        No category 
 *  Argumente:        (aQuery)
 *
 *  Kurzbeschreibung: L�scht den Datensatz von der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAssigns.DeleteFromDB(aQuery: TNativeAdoQuery): Boolean;
var
  i: Integer;
  xDeleted: Boolean;
begin
  // Result soll mit True initialisiert werden, wenn Items definiert sind
  result := (AssignCount = 0);
  
  // Alle Zuordnungen von der DB l�schen
  for i := 0 to AssignCount - 1 do begin
    xDeleted := Assigns[i].DeleteFromDB(aQuery);
    if i > 0 then
      result := xDeleted and result
    else
      result := xDeleted;
  end;// for i := 0 to AssignCount - 1 do begin
end;// TQOAssigns.DeleteFromDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAssignCount
 *  Klasse:           TQOAssigns
 *  Kategorie:        List 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Anzahl Elemente in der Liste
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAssigns.GetAssignCount: Integer;
begin
  result := 0;
  
  if assigned(mAssigns) then
    result := mAssigns.Count;
end;// TQOAssigns.GetAssignCount cat:List

//:-------------------------------------------------------------------
(*: Member:           GetAssigns
 *  Klasse:           TQOAssigns
 *  Kategorie:        List 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: Zugriffsmethode auf ein Element aus der Liste
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAssigns.GetAssigns(aIndex: Integer): TQOAssign;
begin
  result := nil;
  if assigned(mAssigns) then
    if mAssigns.Count > aIndex then
      result := TQOAssign(mAssigns.Items[aIndex]);
end;// TQOAssigns.GetAssigns cat:List

//:-------------------------------------------------------------------
(*: Member:           GetCommaTextByLinkID
 *  Klasse:           TQOAssigns
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die Zuordnung (LinkID) als Komma String zur�ck
 *  Beschreibung:     
                      Die LinkID ist die ID der entsprechenden Zuordnung auf der DB. Im Fall eines 
                      Artikels ist dies die Spalte "c_style_id" in der Tabelle "t_style".
 --------------------------------------------------------------------*)
function TQOAssigns.GetCommaTextByLinkID: String;
var
  i: Integer;
begin
  result := '';

//: ----------------------------------------------
  for i := 0 to AssignCount - 1 do
    result := result + ',' + Assigns[i].CommaTextByLinkID;
  
  if result <> '' then
    system.delete(result, 1, 1);
end;// TQOAssigns.GetCommaTextByLinkID cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOf
 *  Klasse:           TQOAssigns
 *  Kategorie:        No category 
 *  Argumente:        (aQOAssign)
 *
 *  Kurzbeschreibung: Pr�ft ob eine Zuordnung in der Liste erfasst ist
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAssigns.IndexOf(aQOAssign: TQOAssign): Integer;
var
  i: Integer;
begin
  result := -1;
  
  i := 0;
  while (i < mAssigns.Count) and (result = -1) do begin
    if mAssigns[i] = aQOAssign then
      result := i;
    inc(i);
  end;// while (i < mAssigns.Count) and (result = -1) do begin
end;// TQOAssigns.IndexOf cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOfLinkID
 *  Klasse:           TQOAssigns
 *  Kategorie:        No category 
 *  Argumente:        (aLinkID)
 *
 *  Kurzbeschreibung: Sucht einen Artikel in der Liste anhand der ID
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAssigns.IndexOfLinkID(aLinkID: integer): Integer;
var
  i: Integer;
begin
  result := -1;

//: ----------------------------------------------
  i := 0;
  while (i < mAssigns.Count) and (result = -1) do begin
    if Assigns[i].LinkID = aLinkID then
      result := i;
    inc(i);
  end;// while (i < mAssigns.Count) and (result = -1) do begin
end;// TQOAssigns.IndexOfLinkID cat:No category

//:-------------------------------------------------------------------
(*: Member:           IsSameList
 *  Klasse:           TQOAssigns
 *  Kategorie:        No category 
 *  Argumente:        (aSource)
 *
 *  Kurzbeschreibung: True, wenn die beinhaltenden Elemente identisch sind
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAssigns.IsSameList(aSource: TQOAssigns): Boolean;
var
  i: Integer;
begin
  result := true;

//: ----------------------------------------------
  i := 0;
  
  if AssignCount = aSource.AssignCount then begin
    while (i < AssignCount) and(result) do begin
      if (aSource[i].ID <> Assigns[i].ID)
         or (aSource[i].DeleteState <> Assigns[i].DeleteState) then
        result := false
      else
        result := Assigns[i].QOAssigns.IsSameList(aSource[i].QOAssigns);
  
      // Wenn noch nicht gespeichert, dann sowiso nicht identisch
      if aSource[i].ID = cINVALID_ID then
        result := false;
  
      inc(i);
    end;// while (i < AssignCount) and(result) do begin
  end else begin
    // Wenn die Listen nicht die gleiche Anzahl Items haben, dann sind sie nicht identisch
    result := false;
  end;// if AssignCount = aSource.AssignCount then begin
end;// TQOAssigns.IsSameList cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveDefToDB
 *  Klasse:           TQOAssigns
 *  Kategorie:        No category 
 *  Argumente:        (aParentID)
 *
 *  Kurzbeschreibung: Speichert die Settings auf der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAssigns.SaveDefToDB(aParentID: integer): Boolean;
var
  xSaved: Boolean;
  i: Integer;
begin
  result := true;
  // Die ID des zugeh�rigen Alarms f�r jedes Item setzen
  ParentID := aParentID;

//: ----------------------------------------------
  // Jedes Zuordnungs Element ist selber f�r seine Speicherung zust�ndig (Pro TAssign ein DS)
  for i := 0 to AssignCount - 1 do begin
    xSaved := Assigns[i].SaveDefToDB;
    result := result and xSaved;
  end;// for i := 0 to AssignCount do begin

//: ----------------------------------------------
  // Items die auf der Datenbank gel�scht sind, auch in der Zuweisungsliste l�schen
  i := 0;
  while i < AssignCount do begin
    if Assigns[i].DeleteState = dsDeleted then
      Delete(i)
    else
      inc(i);
  end;// while i < AssignCount do begin
end;// TQOAssigns.SaveDefToDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetDeleteState
 *  Klasse:           TQOAssigns
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r DeleteState
 *  Beschreibung:     
                      Der L�sch Status muss auch an die Methoden weitergegeben werden.
 --------------------------------------------------------------------*)
procedure TQOAssigns.SetDeleteState(Value: TDeleteState);
var
  i: Integer;
begin
  for i := 0 to AssignCount - 1 do
    Assigns[i].DeleteState := Value;
end;// TQOAssigns.SetDeleteState cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetParentID
 *  Klasse:           TQOAssigns
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: ID des zugeh�rigen Alarms f�r die Datenbank
 *  Beschreibung:     
                      Die ID muss f�r alle Zuordnungen gesetzt werden.
 --------------------------------------------------------------------*)
procedure TQOAssigns.SetParentID(Value: Integer);
var
  i: Integer;
begin
  FParentID := Value;

//: ----------------------------------------------
  for i := 0 to AssignCount - 1 do
    Assigns[i].ParentID := FParentID;
end;// TQOAssigns.SetParentID cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOAssign
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOAssign.Create;
begin
  inherited Create;

//: ----------------------------------------------
  FQOAssigns := TQOAssigns.Create;
  FQOVisualize := TQOVisualizeImpl.Create;

//: ----------------------------------------------
  // Zuordnung ist noch nicht auf der DB gespeichert
  FID := cINVALID_ID;
  
  // L�schstatus initialisieren
  FDeleteState := dsValid;
end;// TQOAssign.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TQOAssign
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TQOAssign.Destroy;
begin
  FreeAndNil(FQOAssigns);
  FreeAndNil(FQOVisualize);

//: ----------------------------------------------
  inherited Destroy;
end;// TQOAssign.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TQOAssign
 *  Kategorie:        No category 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Eigenschaften �bernehmen
 *  Beschreibung:     
                      Dient zum kopieren von Objekten
 --------------------------------------------------------------------*)
procedure TQOAssign.Assign(Source: TPersistent);
var
  xSource: TQOAssign;
begin
  if Source is TQOAssign then begin
    xSource := TQOAssign(Source);
    // Alle Eigenschaften kopieren
    ID           := xSource.ID;
    LinkId       := xSource.LinkId;
    DeleteState  := xSource.DeleteState;
  
    FQOAssigns.Assign(xSource.QOAssigns);
    FQOVisualize.Assign(xSource.QOVisualize);
  
  end else begin
    // Weiterreichen
    inherited Assign(Source);
  end;// if Source is TQOAssign then begin
end;// TQOAssign.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           DeleteFromDB
 *  Klasse:           TQOAssign
 *  Kategorie:        No category 
 *  Argumente:        (aQuery)
 *
 *  Kurzbeschreibung: L�scht den Datensatz von der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAssign.DeleteFromDB(aQuery: TNativeAdoQuery): Boolean;
var
  i: Integer;
begin
  result := (QOAssigns.AssignCount = 0);
  
  // Alle untergeordneten Zuordnungen l�schen
  for i := 0 to QOAssigns.AssignCount - 1 do
    result := QOAssigns[i].DeleteFromDB(aQuery) and result;
end;// TQOAssign.DeleteFromDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetCommaTextByLinkID
 *  Klasse:           TQOAssign
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die Zuordnung (LinkID) als Komma String zur�ck
 *  Beschreibung:     
                      Die LinkID ist die ID der entsprechenden Zuordnung auf der DB. Im Fall eines 
                      Artikels ist dies die Spalte "c_style_id" in der Tabelle "t_style".
 --------------------------------------------------------------------*)
function TQOAssign.GetCommaTextByLinkID: String;
begin
  result := IntToStr(LinkID);

//: ----------------------------------------------
  if QOAssigns.AssignCount > 0 then begin
    result := result + cAdditionalMachines + QOAssigns.CommaTextByLinkID;  // Artikel
    result := StringReplace(result, ',', cAdditionalMachines, [rfReplaceAll]);
  end;// if AssignCount > 0 then begin
end;// TQOAssign.GetCommaTextByLinkID cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetDeleteState
 *  Klasse:           TQOAssign
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r DeleteState
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOAssign.SetDeleteState(Value: TDeleteState);
begin
  FDeleteState := Value;
  QOAssigns.DeleteState := FDeleteState;
end;// TQOAssign.SetDeleteState cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOAlarms.Create;
begin
  inherited Create;

//: ----------------------------------------------
  raise Exception.CreateFmt('Access class %s through Instance only', [ClassName]);
end;// TQOAlarms.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           CreateInstance
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOAlarms.CreateInstance;
begin
  inherited Create;

//: ----------------------------------------------
  // zuerst die Liste f�r die Alarme erzeugen
  mAlarms := TList.Create;
  mLoaded := false;
end;// TQOAlarms.CreateInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TQOAlarms.Destroy;
begin
  Clear;
  FreeAndNil(mAlarms);

//: ----------------------------------------------
  if AccessInstance(0) = Self then AccessInstance(2);

//: ----------------------------------------------
  inherited Destroy;
end;// TQOAlarms.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           AccessInstance
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        (Request)
 *
 *  Kurzbeschreibung: singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class function TQOAlarms.AccessInstance(Request: Integer): TQOAlarms;
  
  const FInstance: TQOAlarms = nil;
  
begin
  case Request of
    0 : ;
    1 : if not Assigned(FInstance) then FInstance := CreateInstance;
    2 : FInstance := nil;
  else
    raise Exception.CreateFmt('Illegal request %d in AccessInstance', [Request]);
  end;
  Result := FInstance;
end;// TQOAlarms.AccessInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           Add
 *  Klasse:           TQOAlarms
 *  Kategorie:        List 
 *  Argumente:        (aAlarm)
 *
 *  Kurzbeschreibung: F�gt ein Element zur Liste hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarms.Add(aAlarm: TQOAlarm): Integer;
begin
  result := -1;
  if assigned(mAlarms) then
    mAlarms.Add(aAlarm);
end;// TQOAlarms.Add cat:List

//:-------------------------------------------------------------------
(*: Member:           AddNewAlarm
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        (aName)
 *
 *  Kurzbeschreibung: F�gt einen neuen Alarm zur Liste hinzu und gibt den Alarm zur�ck
 *  Beschreibung:     
                      Wenn der Alarm nicht erzeugt werden konnte, dann wird eine
                      Exception ausgel�st.
 --------------------------------------------------------------------*)
function TQOAlarms.AddNewAlarm(aName: string): TQOAlarm;
begin
  result := TQOAlarm.Create;
  IQOVisualize(result).Caption := aName;
  Add(result);
end;// TQOAlarms.AddNewAlarm cat:No category

//:-------------------------------------------------------------------
(*: Member:           AlarmByID
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        (aID)
 *
 *  Kurzbeschreibung: Gibt einen Alarm anhand seiner ID zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarms.AlarmByID(aID: integer): TQOAlarm;
var
  i: Integer;
begin
  // Initialisieren
  result := nil;
  i := 0;

//: ----------------------------------------------
  // Suchen bis der entsprechende Alarm gefunden wurde
  while (i < AlarmCount) and (result = nil) do begin
    if Alarms[i].ID = aID then
      result := Alarms[i];
    inc(i);
  end;// while (i < AlarmCount) and (result = nil) do begin
end;// TQOAlarms.AlarmByID cat:No category

//:-------------------------------------------------------------------
(*: Member:           Clear
 *  Klasse:           TQOAlarms
 *  Kategorie:        List 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�scht alle Elemente aus der Liste
 *  Beschreibung:     
                      Die Elemente werden freigegeben (Komposition).
 --------------------------------------------------------------------*)
procedure TQOAlarms.Clear;
begin
  while AlarmCount > 0 do
    delete(0);
end;// TQOAlarms.Clear cat:List

//:-------------------------------------------------------------------
(*: Member:           DataItemNamesAsString
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        (aOnlyCaught)
 *
 *  Kurzbeschreibung: Gibt die Namen der DataItems als Kommasaparierten String zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarms.DataItemNamesAsString(aOnlyCaught: boolean): String;
var
  i: Integer;
  xTempDataItems: String;
begin
  result := '';
  
  for i := 0 to AlarmCount - 1 do begin
    if Alarms[i].Active then begin
      xTempDataItems := Alarms[i].DataItemNamesAsString(aOnlyCaught);
      if xTempDataItems > '' then
        result := result + ',' + xTempDataItems;
    end;// if Alarms[i].Active then begin
  end;// for i := 0 to AlarmCount - 1 do begin
  
  if result > '' then
    system.delete(result, 1, 1);

//: ----------------------------------------------
  with TStringList.Create do try
    sorted := true;
    Duplicates := dupIgnore;
    CommaText := result;
    result := CommaText;
  finally
    Free;
  end;// with TStringList.Create do try
end;// TQOAlarms.DataItemNamesAsString cat:No category

//:-------------------------------------------------------------------
(*: Member:           Delete
 *  Klasse:           TQOAlarms
 *  Kategorie:        List 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: L�scht ein Element aus der Liste
 *  Beschreibung:     
                      Das Element wird freigegeben (Komposition)
 --------------------------------------------------------------------*)
procedure TQOAlarms.Delete(aIndex: integer);
begin
  if assigned(mAlarms) then begin
    if (aIndex < mAlarms.Count) and (aIndex >= 0) then begin
      Alarms[aIndex].Free;
      mAlarms.delete(aIndex);
    end;// if aIndex < mAlarms.Count then begin
  end;// if assigned(mAlarms) then begin
end;// TQOAlarms.Delete cat:List

//:-------------------------------------------------------------------
(*: Member:           Delete
 *  Klasse:           TQOAlarms
 *  Kategorie:        List 
 *  Argumente:        (aAlarm)
 *
 *  Kurzbeschreibung: L�scht ein Alarm aus der Liste
 *  Beschreibung:     
                      Das Element wird freigegeben (Komposition)
 --------------------------------------------------------------------*)
procedure TQOAlarms.Delete(aAlarm: TQOAlarm);
begin
  if assigned(mAlarms) then
    Delete(IndexOf(aAlarm));
end;// TQOAlarms.Delete cat:List

//:-------------------------------------------------------------------
(*: Member:           Evaluate
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        (aSave)
 *
 *  Kurzbeschreibung: Startet die Analyse der Daten
 *  Beschreibung:     
                      f�r jeden Alarm wird die Methode Analyze aufgerufen. Die Daten werden dann
                      hierarchisch berechnet.
 --------------------------------------------------------------------*)
procedure TQOAlarms.Evaluate(aSave: boolean);
var
  i: Integer;
  xStyles: string;
begin
  // Alle Alarme zur�cksetzen
  for i := 0 to AlarmCount - 1 do
    Alarms[i].reset;
  
  // Der Daten Pool muss f�r jedes Evaluate neu aufgebaut werden
  TQODataPool.Instance.Initialized := false;
  TQODataPool.Instance.Init;

//: ----------------------------------------------
  (* Untersucht ersteinmal welche Produktionsgruppen relevant
     sein k�nnten (�berwachte Artikel/Maschinen) *)
  if GetProdIDs > '' then begin

//: ----------------------------------------------
    // Alarm "berechnen" (wird weitergereicht bis zu den einzelnen Berechnungsmethoden)
    for i := 0 to AlarmCount - 1 do begin
      // Noch hat kein Alarm "zugeschlagen"
      Alarms[i].Caught := false;
      // Alle aktiven Alarme untersuchen
      if Alarms[i].Active then
        Alarms[i].Evaluate;
    end;// for i := 0 to AlarmCount - 1 do begin

//: ----------------------------------------------
    // Daten der Artikel holen die an mindestens einem Alarm beteiligt sind
    with TStringList.Create do try
      sorted := true;
      Duplicates := dupIgnore;
    
      // Zuerst alle beteiligten ProdIDs die im Alarm sind holen
      for i := 0 to AlarmCount - 1 do begin
        if Alarms[i].Caught then begin
          if Count = 0 then begin
            CommaText := Alarms[i].ProdIDsInOfflimit.CommaText;
          end else begin
            if Alarms[i].ProdIDsInOfflimit.Count > 0 then
              CommaText := CommaText + ',' + Alarms[i].ProdIDsInOfflimit.CommaText;
          end;// if Count = 0 then begin
        end;// if Alarms[i].Cought then begin
      end;// for i := 0 to AlarmCount - 1 do begin
    
      // Jetzt sind alle verletzten ProdIDs in der StringListe versammelt
      for i := 0 to Count - 1 do begin
        TQODataPool.Instance.DataPool.Recordset.Filter := 'c_prod_id = ' + Strings[i];
        if not(TQODataPool.Instance.DataPool.EOF) then
          xStyles := xStyles + ',' + TQODataPool.Instance.DataPool.FieldByName('c_style_id').AsString;
      end;// for i := 0 to Count - 1 do begin
    
      // F�hrendes Komma l�schen
      if xStyles > '' then
        system.Delete(xStyles, 1, 1);
    
      // Die ProdIDs werden nicht mehr ben�tigt
      Clear;
    
      // Daf�r die doppelten StyleIDs entfernen
      CommaText := xStyles;
    
      // Jetzt die Artikeldaten holen
      TQODataPool.Instance.GetStyleData(CommaText);
    finally
      Free;
    end;// with TStringList.Create do try

//: ----------------------------------------------
    // Speichern
    if aSave then
      SaveDataToDB(false);

//: ----------------------------------------------
  end;// if GetProdIDs > '' then begin
end;// TQOAlarms.Evaluate cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAlarmCount
 *  Klasse:           TQOAlarms
 *  Kategorie:        List 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Anzahl Elemente in der Liste
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarms.GetAlarmCount: Integer;
begin
  result := 0;
  
  if assigned(mAlarms) then
    result := mAlarms.Count;
end;// TQOAlarms.GetAlarmCount cat:List

//:-------------------------------------------------------------------
(*: Member:           GetAlarms
 *  Klasse:           TQOAlarms
 *  Kategorie:        List 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: Zugriffsmethode auf ein Element aus der Liste
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarms.GetAlarms(aIndex: Integer): TQOAlarm;
begin
  result := nil;
  if assigned(mAlarms) then
    if mAlarms.Count > aIndex then
      result := TQOAlarm(mAlarms.Items[aIndex]);
end;// TQOAlarms.GetAlarms cat:List

//:-------------------------------------------------------------------
(*: Member:           GetMaxShiftCount
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Sammelt von allen Methoden die Anzahl der Schichten
 *  Beschreibung:     
                      Jede Methode definiert eine gewisse Anzahl Schichten die
                      mindestens an einer Verletzung beteiligt sein m�ssen um
                      einen Alarm zu erzeugen. 
                      Diese Funktion ermittelt die Methode (resp. Anzahl Schichten)
                      in der die meisten Schichten definiert sind. 
                      Heraus kommt die Anzahl Schichten die mindestens im globalen
                      "Datenhaufen" vorhanden sein m�ssen um alle Methoden
                      auswerten zu k�nnen.
 --------------------------------------------------------------------*)
function TQOAlarms.GetMaxShiftCount: Integer;
var
  i: Integer;
  xShiftCount: Integer;
begin
  result := 0;

//: ----------------------------------------------
  for i := 0 to AlarmCount - 1 do begin
    if Alarms[i].Active then begin
      xShiftCount := Alarms[i].GetMaxShiftCount;
      if xShiftCount > result then
        result := xShiftCount;
    end;// if Alarms[i].Active then begin
  end;// for i := 0 to AlarmCount - 1 do begin
end;// TQOAlarms.GetMaxShiftCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetOfflimitCount
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die Anzahl der Alarme im Offlimit zur�ck
 *  Beschreibung:     
                      Die Funktion l�st keine Berechnung aus. Um einen sinnvollen Wert 
                      zu erhalten muss zuerst die Funktion 'Evaluate' aufgerufen werden.
 --------------------------------------------------------------------*)
function TQOAlarms.GetOfflimitCount: Integer;
var
  i: Integer;
begin
  result := 0;
  
  // Z�hlen welche Alarme "zugeschlagen" haben
  for i := 0 to AlarmCount - 1 do
    if Alarms[i].Caught then
      inc(result);
end;// TQOAlarms.GetOfflimitCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetProdIDs
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt alle beteiligten ProdID's als kommaseparierten String zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarms.GetProdIDs: String;
var
  i: Integer;
begin
  result := '';
  
  // Alle Alarme nach deren ProdID's fragen
  for i := 0 to AlarmCount - 1 do begin
    // Nur wenn der Alarm aktiviert ist
    if Alarms[i].Active then begin
      // Es k�nnte f�r einen Alarm auch keine ProdID's relevant sein
      if Alarms[i].GetInvolvedProdIDs > '' then
        // ProdID's zur Liste (Kommaseparierter String) hinzuf�gen
        result := result + ',' + Alarms[i].GetInvolvedProdIDs
    end;// if Alarms[i].Active then begin
  end;// for i := 0 to AlarmCount - 1 do begin
  
  // F�hrendes Komma l�schen
  if result > '' then
    system.delete(result, 1, 1);
end;// TQOAlarms.GetProdIDs cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOf
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        (aAlarm)
 *
 *  Kurzbeschreibung: Gibt den Index des �bergebenen Alarms zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarms.IndexOf(aAlarm: TQOAlarm): Integer;
begin
  result := -1;
  if assigned(mAlarms) then
    result := mAlarms.IndexOf(aAlarm);
end;// TQOAlarms.IndexOf cat:No category

//:-------------------------------------------------------------------
(*: Member:           Instance
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class function TQOAlarms.Instance: TQOAlarms;
begin
  Result := AccessInstance(1);
end;// TQOAlarms.Instance cat:No category

//:-------------------------------------------------------------------
(*: Member:           LoadDefFromDB
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�dt alle Definitionen von der Datenbank
 *  Beschreibung:     
                      Ist aReload = true dann werden die Alarme neu geladen - Auch wenn sie
                      bereits geladen wurden.
 --------------------------------------------------------------------*)
function TQOAlarms.LoadDefFromDB: Boolean;
var
  xDB: TAdoDBAccess;
  
  const
    cAlarmsFromDB  = 'SELECT c_QOAlarm_id, c_Name, c_BoolOpAnd, c_AdviceMethods, c_Active, c_SaveDate, c_SavingUser ' +
                     'FROM t_QO_Alarm_Setting ';
  
    cItemsFromDB   = 'SELECT c_QOItem_id, c_QOAlarm_id, c_Name, c_BoolOpAnd, c_class_defect ' +
                     'FROM t_QO_Item_Setting ' +
                     'ORDER BY  c_QOAlarm_id, c_Name';
  
    cMethodsFromDB = 'SELECT c_QOMethod_id, c_QOItem_id, c_Type, c_Settings ' +
                     'FROM t_QO_Method_Setting ' +
                     'ORDER BY c_QOItem_id';
  
    cStylesFromDB  = 'SELECT c_QOStyle_id, c_QOAlarm_id, c_Style_id ' +
                     'FROM t_QO_Style_Setting ' +
                     'ORDER BY c_QOAlarm_id';
  
    cMachineFromDB = 'SELECT c_QOMachine_id, c_QOStyle_id, c_Machine_id ' +
                     'FROM t_QO_Machine_Setting ' +
                     'ORDER BY c_QOStyle_id';
  
    cGetAdditionalStyleInfo = 'SELECT c_style_name ' +
                              'FROM t_style ' +
                              'WHERE c_style_id = :c_style_id';
  
    cGetAdditionalMachInfo  = 'SELECT c_machine_name ' +
                              'FROM t_machine ' +
                              'WHERE c_machine_id = :c_machine_id';
  
  (*-----------------------------------------------------------
    Holt alle Alarme von der DB
  -------------------------------------------------------------*)
  procedure GetAlarms (aQuery: TNativeAdoQuery);
  var
    xQOAlarm: TQOAlarm;
  begin
    with aQuery do begin
      SQL.Text := cAlarmsFromDB;
      // Alle definierten Alarme von der Datenbank holen
      Open;
      while not(EOF) do begin
        // F�r jeden Datensatz einen neuen Alarm erzeugen
        xQOAlarm := TQOAlarm.Create;
  
        // Eigenschaften abf�llen
        xQOAlarm.BoolOpAnd           := FieldByName('c_BoolOpAnd').asBoolean;
        xQOAlarm.Active              := FieldByName('c_Active').asBoolean;
        xQOAlarm.ID                  := FieldByName('c_QOAlarm_id').asInteger;
        xQOAlarm.SaveDate            := FieldByName('c_SaveDate').AsDateTime;
        xQOAlarm.SavingUser          := FieldByName('c_SavingUser').AsString;
        xQOAlarm.QOVisualize.Caption := FieldByName('c_Name').asString;
        // Typecast auf Word �ndern, wenn mehr als 8 Methoden definiert sind
        xQOAlarm.AdviceMethods := TAdviceMethodSet(Byte(FieldByName('c_AdviceMethods').AsInteger));
  
        // Neuen Alarm zur Liste hinzuf�gen
        Add(xQOAlarm);
  
        // N�chster Datensatz
        Next;
      end;// while not(EOF) do begin
    end;// with aQuery do begin
  end;// procedure GetAlarms (aQuery: TNativeAdoQuery);
  
  (*-----------------------------------------------------------
    Holt alle Items von der DB.
    Die Items werden alle mit einem Zugriff geholt. Die Datenmenge
    wird dann nach den Alarmen gefiltert.
    Die Items werden anhand ihres Namens aus dem globalen Container
    kopiert (Klassendefinitionen).
  -------------------------------------------------------------*)
  procedure GetItems(aQuery: TNativeAdoQuery);
  var
    xItemIndex: integer;
    i: integer;
  begin
    with aQuery do begin
      SQL.Text := cItemsFromDB;
      // Alle definierten Items von der Datenbank holen
      Open;
      // Die Items sind jetzt nach der Zugeh�rigkeit zu den Alarmen geordnet
      for i := 0 to AlarmCount - 1 do begin
        // Nur die Datens�tze die zum Alarm geh�ren
        Recordset.Filter := 'c_QOAlarm_id = ' + intToStr(Alarms[i].ID);
        // An den Anfang der Datenmenge
        FindFirst;
        while not(EOF) do begin
          xItemIndex := Alarms[i].QOItems.AddCopy(FieldByName('c_Name').asString, gQOItems);
          // Wenn das DataItem gefunden wurde, dann Werte zuweisen
          if xItemIndex >= 0 then begin
            Alarms[i].QOItems[xItemIndex].ID := FieldByName('c_QOItem_id').AsInteger;
            Alarms[i].QOItems[xItemIndex].BoolOpAnd := FieldByName('c_BoolOpAnd').asBoolean;
            Alarms[i].QOItems[xItemIndex].QOVisualize.Caption := FieldByName('c_Name').asString;
            Alarms[i].QOItems[xItemIndex].AlarmID   := Alarms[i].ID;
            Alarms[i].QOItems[xItemIndex].ClassDefects := TClassDefects(FieldByName('c_class_defect').AsInteger);
          end;// if xItemIndex >= 0 then begin
          // N�chster Datensatz
          next;
        end;// while not(EOF) do begin
      end;// for i := 0 to AlarmCount - 1 do begin
    end;// with aQuery do begin
  end;// procedure GetItems(aQuery: TNativeAdoQuery);
  
  (*-----------------------------------------------------------
    Holt alle Methoden von der DB.
    Die Methoden werden in einem Zugriff von der DB geholt. Anschliessend
    wird f�r jedes DataItem der Filter gesetzt und die Methoden dem ent-
    prechenden DataItem zugeordnet.
  -------------------------------------------------------------*)
  procedure GetMethods(aQuery: TNativeAdoQuery);
  var
    xStream     : TStringStream;
    xDoc        : TIcXMLDocument;
    xParser     : TIcXMLParser;
    xXMLElement : TIcXMLelement;
    xXMLMethods : TIcXMLelement;
    i,j         : integer;
    xQOMethod   : TQOMethod;
  begin
    // intialisieren
    xDoc := nil;
    xParser := nil;
    try
      // PArser und ein "virtuelles" Dokument erzeugen
      xParser := TIcXMLParser.Create(nil);
      xDoc := TIcXMLDocument.Create;
      xDoc.AssignParser(xParser);
  
      with aQuery do begin
        // Holt alle Methoden von der DB
        SQL.Text := cMethodsFromDB;
        // Alle definierten Items von der Datenbank holen
        Open;
        // Alle Alarme(i) und deren DataItems(j) durchgehen
        for i := 0 to AlarmCount - 1 do begin
          for j := 0 to Alarms[i].QOItems.ItemCount - 1 do begin
            // Nur die Datens�tze die zum DataItem geh�ren
            Recordset.Filter := 'c_QOItem_id = ' + intToStr(Alarms[i].QOItems[j].ID);
            // An den Anfang der Datenmenge
            FindFirst;
  
            // F�r jede gefundenen Methode weitermachen
            while not(EOF) do begin
              xQOMethod := nil;
              // Holt die entsprechende Methode des DataItems anhand des Typs
              xQOMethod := Alarms[i].QOItems[j].QOMethods.MethodByType(TCalcMethod(FieldByName('c_Type').AsInteger));
  
              // Wenn die Methode vorhanden ist (sollte immer so sein), dann die Settings laden
              if assigned(xQOMethod) then begin
                xQOMethod.ID := FieldByName('c_QOMethod_id').AsInteger;
                xQOMethod.ItemId := Alarms[i].QOItems[j].ID;
  
                // XML String in einen Stream laden
                xStream := TStringStream.Create(FieldByName('c_Settings').AsString);
                try
                  // Parser starten. Das Ergebnis wird im Dokument abgelegt
                  if xParser.Parse(xStream, xDoc) then begin
                    xXMLMethods := xDoc.GetDocumentElement.GetFirstChild;
                    // Pr�fen ob das Element die Methoden bezeichnet
                    if assigned(xXMLMethods) then begin
                      // Methoden Settings
                      if xXMLMethods.GetName = cXMLMethodID then begin
                        xXMLElement := xXMLMethods.GetFirstChild;
                        if assigned(xXMLElement) then
                          // leist die Settings in die entsprechende Methode
                          xQOMethod.ReadXMLMethodeSettings(xXMLElement);
                      end;// if xMethods.GetName = cXMLMethodID then begin
                    end;// if assigned(xXMLMethods) then begin
                  end;// if xParser.Parse(xStream, xDoc) then begin
                finally
                  xStream.Free;
                end;// try finally
              end;// if assigned(xQOMethod) then begin
              // N�chster Datensatz
              next;
            end;// while not(EOF) do begin
          end;// for j := 0 to Alarms[i].QOItems.ItemCount - 1 do begin
        end;// for i := 0 to AlarmCount - 1 do begin
      end;// with aQuery do begin
    finally
      FreeAndNil(xDoc);
      FreeAndNil(xParser);
    end;// try finally
  end;// procedure GetMethods(aQuery: TNativeAdoQuery);
  
  (*-----------------------------------------------------------
    Holt alle Artikel (QO-Artikel) von der DB.
    Die Artikel werden alle mit einem Zugriff geholt. Die Datenmenge
    wird dann nach den Alarmen gefiltert.
  -------------------------------------------------------------*)
  procedure GetStyles(aDB: TAdoDBAccess);
  var
    xQOStyle: TQOStyle;
    xQOMachine: TQOMachine;
    i: integer;
  const
    cStyleQuery      = 0;
    cMachQuery       = 1;
    cStyleAdditional = 2;
    cMachAdditional  = 3;
  begin
    aDB.Query[cStyleQuery].SQL.Text := cStylesFromDB;
    aDB.Query[cMachQuery].SQL.Text := cMachineFromDB;
  
    aDB.Query[cStyleAdditional].SQL.Text := cGetAdditionalStyleInfo;
    aDB.Query[cMachAdditional].SQL.Text := cGetAdditionalMachInfo;
  
    (* Alle Maschinen-Definitionen holen (werden sp�ter gebraucht um die
       Maschinen den Artikeln zuordnen zu k�nnen *)
    aDB.Query[cMachQuery].Open;
  
    // Alle definierten Items von der Datenbank holen
    aDB.Query[cStyleQuery].Open;
    // Die Items sind jetzt nach der Zugeh�rigkeit zu den Alarmen geordnet
    for i := 0 to AlarmCount - 1 do begin
      // Nur die Datens�tze die zum Alarm geh�ren
      aDB.Query[cStyleQuery].Recordset.Filter := 'c_QOAlarm_id = ' + intToStr(Alarms[i].ID);
      // An den Anfang der Datenmenge
      aDB.Query[cStyleQuery].FindFirst;
      while not(aDB.Query[cStyleQuery].EOF) do begin
        // F�r jeden Datensatz einen neuen Alarm erzeugen
        if aDB.Query[cStyleQuery].FieldByName('c_Style_id').asInteger = cAllStylesLinkID then
          xQOStyle := TQOAllStyles.Create
        else
          xQOStyle := TQOStyle.Create;
  
        // Eigenschaften abf�llen
        xQOStyle.ID       := aDB.Query[cStyleQuery].FieldByName('c_QOStyle_id').asInteger;
        xQOStyle.ParentID := Alarms[i].ID;
        xQOStyle.LinkID   := aDB.Query[cStyleQuery].FieldByName('c_Style_id').asInteger;
  
        aDB.Query[cStyleAdditional].ParamByName('c_style_id').AsInteger := xQOStyle.LinkID;
        aDB.Query[cStyleAdditional].Open;
        if not(aDB.Query[cStyleAdditional].EOF) then
          xQOStyle.QOVisualize.Caption := aDB.Query[cStyleAdditional].FieldByName('c_style_name').asString;
  
        // Den Filter setzten, dass jetzt nur noch die Maschinen zum Style zu sehen sind
        aDB.Query[cMachQuery].Recordset.Filter := 'c_QOStyle_id = ' + intToStr(xQOStyle.ID);
        // An den Anfang der Datenmenge
        aDB.Query[cMachQuery].FindFirst;
        while not(aDB.Query[cMachQuery].EOF) do begin
          xQOMachine := TQOMachine.Create;
  
          xQOMachine.ID       := aDB.Query[cMachQuery].FieldByName('c_QOMachine_id').asInteger;
          xQOMachine.ParentID := xQOStyle.ID;
          xQOMachine.LinkID   := aDB.Query[cMachQuery].FieldByName('c_Machine_id').asInteger;
  
          xQOStyle.QOAssigns.Add(xQOMachine);
  
          aDB.Query[cMachAdditional].ParamByName('c_machine_id').AsInteger := xQOMachine.LinkID;
          aDB.Query[cMachAdditional].Open;
          if not(aDB.Query[cMachAdditional].EOF) then
            xQOMachine.QOVisualize.Caption := aDB.Query[cMachAdditional].FieldByName('c_machine_name').asString;
  
          // N�chste Maschine
          aDB.Query[cMachQuery].Next;
        end;// while not(aDB.Query[cMachQuery].EOF) do begin
  
        // Neuen Alarm zur Liste hinzuf�gen
        Alarms[i].QOAssigns.Add(xQOStyle);
        // N�chster Artikel
        aDB.Query[cStyleQuery].Next;
      end;// while not(EOF) do begin
    end;// for i := 0 to AlarmCount - 1 do begin
  end;// procedure GetItems(aQuery: TNativeAdoQuery);
  
begin
  result := false;

//: ----------------------------------------------
  if not(mLoaded) then begin
    xDB := TAdoDBAccess.Create(4);
    with xDB do try
      try
        init;
        // Alarme laden
        GetAlarms(Query[0]);
        // DateItems zu den Alarmen laden
        GetItems(Query[0]);
        // Methoden laden
        GetMethods(Query[0]);
        // Artikel zu den Alarmen laden
        GetStyles(xDB);
  
        result := true;
      except
        on e:Exception do begin
          Clear;
          CodeSite.SendError('TQOAlarms.LoadDefFromDB: ' + e.message);
        end;//
      end;// try except
    finally
      free;
      mLoaded := result;
    end;// with xDB do try
  end else begin
    result := true;
  end;// if not(mLoaded) then begin
end;// TQOAlarms.LoadDefFromDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           ReleaseInstance
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class procedure TQOAlarms.ReleaseInstance;
begin
  AccessInstance(0).Free;
end;// TQOAlarms.ReleaseInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveDataToDB
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        (aPersistent)
 *
 *  Kurzbeschreibung: Speichert die ermittelten Daten in die Datenbank
 *  Beschreibung:     
                      Die Daten m�ssen vorher mit "Evaluate" ermittelt werden.
                      F�r jeden Alarm existiert ein Recordset mit den beteiligten Maschinen.
                      Die Artikel Daten der beteiligten Artikel sind in 'TQODataPool.StyleData'
                      abgelegt.
                      
                      Ein "Alarm-Satz" entspricht allen gefangenen Alarmen (Daten und Settings).
                      Auf der Basis dieser Informationen wird dann auch die Grafik aufgebaut.
 --------------------------------------------------------------------*)
procedure TQOAlarms.SaveDataToDB(aPersistent: boolean);
var
  xDB: TAdoDBAccess;
  xID: Integer;
  i: Integer;
  
  const
    cInsertCaughtAlarms = 'INSERT INTO t_QO_Caught_Alarm ' +
                          '(c_caught_alarm_id, c_shift_id, c_shiftcal_id, c_date, c_style_data, c_monitored_DataItems) ' +
                          'VALUES  ' +
                          '(:c_caught_alarm_id, :c_shift_id, :c_shiftcal_id, :c_date, :c_style_data, :c_monitored_DataItems)';
  
begin
  xDB := TAdoDBAccess.Create(1);

//: ----------------------------------------------
  if OfflimitCount > 0 then begin

//: ----------------------------------------------
    try
      if xDB.init then begin
        // Alarm
        with xDB.Query[0] do begin
          SQL.Text := cInsertCaughtAlarms;
          ParamByName('c_date').AsDateTime := VarFromDateTime(now);
          ParamByName('c_style_data').AsString := TQODataPool.Instance.StyleData.AsXML;
          ParamByName('c_shift_id').AsInteger := TQODataPool.Instance.RecentShiftID;
          ParamByName('c_shiftcal_id').AsInteger := TQODataPool.Instance.ShiftCalID;
          ParamByName('c_monitored_DataItems').AsString := DataItemNamesAsString(false);
  
          CodeSite.SendIntegerEx(csmWarning, 'RecentShiftID', TQODataPool.Instance.RecentShiftID);
          xID := InsertSQL('t_QO_Caught_Alarm', 'c_caught_alarm_id', MAXINT, 1);
        end;// with xDB.Query[0] do begin

//: ----------------------------------------------
        // Alarm Daten
        for i := 0 to AlarmCount - 1 do begin
          if Alarms[i].Caught then
            Alarms[i].SaveDataToDB(xDB.Query[0], xID, aPersistent);
        end;// for i := 0 to AlarmCount - 1 do begin

//: ----------------------------------------------
      end;// if xDB.init then begin
    finally
      xDB.Free;
    end;// try finally

//: ----------------------------------------------
  end;// if OfflimitCount > 0 then begin
end;// TQOAlarms.SaveDataToDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveDefToDB
 *  Klasse:           TQOAlarms
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Speichert die Settings auf der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarms.SaveDefToDB: Boolean;
var
  xSaved: Boolean;
  i: Integer;
begin
  result := true;
  
  // Jeder Alarm ist selber f�r seine Speicherung zust�ndig (Pro Alarm ein DS)
  for i := 0 to AlarmCount - 1 do begin
    xSaved := Alarms[i].SaveDefToDB;
    result := result and xSaved;
  end;// for i := 0 to AlarmCount do begin
end;// TQOAlarms.SaveDefToDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOAlarm
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOAlarm.Create;
begin
  inherited Create;

//: ----------------------------------------------
  FQOAssigns := TQOAssigns.Create;
  FQOItems := TQOItems.Create;
  FQOVisualize := TQOVisualizeImpl.Create;
  FValues := TQOValues.Create;

//: ----------------------------------------------
  FAdviceMethods := cDefaultAdviceMethodeSet;
  // Ung�ltige ID (Bedeutet dass der Datensatz neu ist)
  FID := cINVALID_ID;

//: ----------------------------------------------
  // Interface Initialisieren
  FQOVisualize.OnGetImageIndex := GetImageIndex;
  FQOVisualize.OnGetActive := OnGetActive;

//: ----------------------------------------------
  // Sammelt die Produktionsgruppen die im Offlimit sind
  FProdIDsInOfflimit := TStringList.Create;
  FProdIDsInOfflimit.Duplicates := dupIgnore;
end;// TQOAlarm.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TQOAlarm
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TQOAlarm.Destroy;
begin
  FreeAndNil(FQOAssigns);
  FreeAndNil(FQOItems);
  FreeAndNil(FQOVisualize);
  FreeAndNil(FValues);

//: ----------------------------------------------
  FreeAndNil(FProdIDsInOfflimit);

//: ----------------------------------------------
  inherited Destroy;
end;// TQOAlarm.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           DataItemByName
 *  Klasse:           TQOAlarm
 *  Kategorie:        No category 
 *  Argumente:        (aName, aDataItemNameType)
 *
 *  Kurzbeschreibung: Gibt ein DataItem anhand des Namens zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarm.DataItemByName(aName: string; aDataItemNameType: TDataItemNameType = ntItem): Integer;
var
  xName: string;
  i: Integer;
begin
  result := -1;
  i := 0;
  
  // Alle DataItems durchsuchen bis eine �bereinstimmung gefunden wird
  while (i < QOItems.ItemCount) and (result < 0) do begin
    case aDataItemNameType of
      ntDisplay: xName := QOItems[i].DisplayName;
      ntItem:    xName := QOItems[i].ItemName;
      ntField:   xName := QOItems[i].FieldName;
    end;// case aDataItemNameType of
  
    // Fertig, wenn der Text identisch ist
    if AnsiSameText(xName, aName) then
      result := i;
    inc(i);
  end;// while (i < QOItems.ItemCount) and (result < 0) do begin
end;// TQOAlarm.DataItemByName cat:No category

//:-------------------------------------------------------------------
(*: Member:           DataItemNamesAsString
 *  Klasse:           TQOAlarm
 *  Kategorie:        No category 
 *  Argumente:        (aOnlyCaught)
 *
 *  Kurzbeschreibung: Gibt die Namen der DataItems als Kommasaparierten String zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarm.DataItemNamesAsString(aOnlyCaught: boolean): String;
var
  xOldDataItem: String;
  xTempBoolOpAnd: String;
  xDataItem: TQOItem;
  xTempMethods: String;
  i: Integer;
  xIndex: Integer;
begin
  result := '';
  
  if aOnlyCaught then begin
    with Values do begin
      if Active then begin
        Recordset.Filter := '';
        Recordset.Sort := cDataItemItemName;
        FindFirst;
        xOldDataItem := '';
        while not(EOF) do begin
          if FieldByName(cDataItemItemName).AsString <> xOldDataItem then begin
            xOldDataItem := FieldByName(cDataItemItemName).AsString;
            xIndex := QOItems.IndexOfCaption(xOldDataItem);
            if (xIndex >= 0) then begin
              xDataItem := QOItems[xIndex];
              xTempBoolOpAnd := '0' + cFieldSeparator; // false
  
              // Feststellen wie die Methoden verkn�pft sind
              if xDataItem.BoolOpAnd then
                xTempBoolOpAnd := '1' + cFieldSeparator;
  
              // Sammeln der Methoden die "angeschlagen" haben
              xTempMethods := '';
              for i := 0 to xDataItem.QOMethods.MethodCount - 1 do begin
                if xDataItem.QOMethods[i].Caught then
                  xTempMethods := xTempMethods + cFieldSeparator + intToStr(ord(xDataItem.QOMethods[i].MethodType));
              end;// for i := 0 to xDataItem.QOMethods.MethodCount - 1 do begin
              if xTempMethods > '' then
                system.delete(xTempMethods, 1, 1);
  
              (* Am Anfang steht das DataItem. Danach durch Steuerzeichen getrennt
                 die Verkn�pfung und die beteiligten Methoden *)
              result := result + ',' + '"' + xDataItem.QOVisualize.Caption + cFieldSeparator + xTempBoolOpAnd + xTempMethods + '"';
            end;// if (QOItems.IndexOfDisplayName(xOldDataItem) >= 0) then begin
          end;// if FieldByName(cDataItemItemName).AsString <> xOldDataItem then begin
          next;
        end;// while not(EOF) do begin
      end;// if Active then begin
    end;// with Values do begin
    if result > '' then
      system.delete(result, 1, 1);
  
  end else begin
    result := QOItems.NamesAsString;
  end;// if aOnlyCaught then begin
end;// TQOAlarm.DataItemNamesAsString cat:No category

//:-------------------------------------------------------------------
(*: Member:           DeleteFromDB
 *  Klasse:           TQOAlarm
 *  Kategorie:        No category 
 *  Argumente:        (aQuery)
 *
 *  Kurzbeschreibung: L�scht den Datensatz von der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarm.DeleteFromDB(aQuery: TNativeAdoQuery): Boolean;
  
  const
    cDeleteAlarm = 'DELETE FROM t_QO_Alarm_Setting WHERE c_QOAlarm_id = :c_QOAlarm_id';
  
begin
  result := false;
  
  if QOItems.DeleteFromDB(aQuery) then begin
    if QOAssigns.DeleteFromDB(aQuery) then begin
      // Dann den Alarm
      aQuery.SQL.Text := cDeleteAlarm;
      aQuery.ParamByName('c_QOAlarm_id').AsInteger := FID;
    end;// if QOAssigns.DeleteFromDB(aQuery) then begin
  end;// if QOItems.DeleteFromDB(aQuery) then begin
  
  // Datensatz gel�scht, wenn ein Datensatz betroffen ist
  result := (aQuery.ExecSQL = 1);
  
  if result then
    FID := cINVALID_ID;
end;// TQOAlarm.DeleteFromDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           Evaluate
 *  Klasse:           TQOAlarm
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn ein Alarm berechnet werden soll
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOAlarm.Evaluate;
begin
  // Zuerst mal alle bis jetzt gesammelten Daten verwerfen
  Values.Close;

//: ----------------------------------------------
  if Active then begin
    // Nur ausf�hren, wenn auch ProdIDs beteiligt sind
    if GetInvolvedProdIDs > '' then begin
      QOItems.ProdIDs := FInvolvedProdIDs;
      QOItems.QOAlarm := self;
      QOItems.Evaluate(BoolOpAnd);
  
      // ProdID's die im Offlimit sind merken
      FProdIDsInOfflimit.Assign(QOItems.ProdIDsInOfflimit);
      // Der Alarm ist im Alarm, wenn mindestens eine ProdID im Alarm ist
      Caught := (FProdIDsInOfflimit.Count > 0);
  
      // Daten abfragen
      QOItems.GetOfflimitData(self);
    end;// if GetProdIDs > '' then begin
  end;// if Active then begin
end;// TQOAlarm.Evaluate cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetImageIndex
 *  Klasse:           TQOAlarm
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aImageKind, aImageIndex)
 *
 *  Kurzbeschreibung: Fragt den ImageIndex f�r die Anzeige ab
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOAlarm.GetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer);
begin
  case aImageKind of
    ikSettingsNormal : begin
      if Active then
        aImageIndex := TImageIndexHandler.Instance.AlarmSettings
      else
        aImageIndex := TImageIndexHandler.Instance.AlarmInactive;
    end;// ikSettingsNormal : begin
    ikAssignNormal   : begin
      if Active then
        aImageIndex := TImageIndexHandler.Instance.AlarmAssign
      else
        aImageIndex := TImageIndexHandler.Instance.AlarmInactive;
    end;// ikAssignNormal   : begin
  end;// case aImageKind of
end;// TQOAlarm.GetImageIndex cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetInvolvedProdIDs
 *  Klasse:           TQOAlarm
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Berechnet die Produktions IDs die f�r diesen Alarm relevant sind
 *  Beschreibung:     
                      Die Produktions ID's werden aus dem globalen "Datenhaufen" extrahiert indem 
                      jeder Datensatz der produzierenden Partien mit den Assign-Objekten des Alarms verglichen
                      werden.
 --------------------------------------------------------------------*)
function TQOAlarm.GetInvolvedProdIDs: String;
var
  xStyleIndex: Integer;
begin
  if Active then begin

//: ----------------------------------------------
    if (FInvolvedProdIDs = '') and (assigned(TQODataPool.Instance.ActiveProdIDs)) then begin
      // Nur neu berechnen, wenn noch nicht berechnet
      with TQODataPool.Instance.ActiveProdIDs do begin
        if Active then begin
          // An den Anfang der Datenmenge
          FindFirst;
          // Alle Datens�tze durchsuchen
          while not(EOF) do begin
            // Pr�fen ob der Artikel beteiligt ist
            xStyleIndex := QOAssigns.IndexOfLinkID(FieldByName('c_style_id').AsInteger);
            // Es k�nnte sich auch um einen "Alle Artikel" handeln
            if xStyleIndex = -1 then
              xStyleIndex := QOAssigns.IndexOfLinkID(cAllStylesLinkID);
  
            if (xStyleIndex >= 0) then begin
              // Wenn der Artikel �berwacht wird, dann pr�fen ob alle oder nur einzelne Maschinen
              if QOAssigns[xStyleIndex].QOAssigns.AssignCount > 0 then begin
                // Bei einzelnen Maschinen pr�fen ob die Kombination Artikel - Maschine stimmt
                if QOAssigns[xStyleIndex].QOAssigns.IndexOfLinkID(FieldByName('c_machine_id').AsInteger) >= 0 then
                  // ProdID hinzuf�gen
                  FInvolvedProdIDs := FInvolvedProdIDs + ',' + FieldByName('c_prod_id').AsString;
              end else begin
                // ProdID hinzuf�gen, wenn alle Maschinen des Artikels �berwacht werden
                FInvolvedProdIDs := FInvolvedProdIDs + ',' + FieldByName('c_prod_id').AsString;
              end;// if QOAssigns[xStyleIndex].QOAssigns.AssignCount > 0 then begin
            end;// if xStyleIndex >= 0 then begin
            next;
          end;// while not(EOF) do begin
        end;// if Active then begin
      end;// with TQODataPool.Instance.ActiveProdIDs do begin
  
      // F�hrendes Komma entfernen
      if FInvolvedProdIDs > '' then
        system.delete(FInvolvedProdIDs, 1, 1);
    end;// if (FInvolvedProdIDs = '') and (assigned(TQODataPool.Instance.ActiveProdIDs)) then begin

//: ----------------------------------------------
  end else begin
    // Wenn der Alarm inaktiv ist, dann gibt es auch keine ProdIDs
    FInvolvedProdIDs := '';
  end;// if Active then begin

//: ----------------------------------------------
  result := FInvolvedProdIDs;
end;// TQOAlarm.GetInvolvedProdIDs cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMaxShiftCount
 *  Klasse:           TQOAlarm
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Sammelt von allen Methoden die Anzahl der Schichten
 *  Beschreibung:     
                      Jede Methode definiert eine gewisse Anzahl Schichten die
                      mindestens an einer Verletzung beteiligt sein m�ssen um
                      einen Alarm zu erzeugen. 
                      Diese Funktion ermittelt die Methode (resp. Anzahl Schichten)
                      in der die meisten Schichten definiert sind. 
                      Heraus kommt die Anzahl Schichten die mindestens im globalen
                      "Datenhaufen" vorhanden sein m�ssen um alle Methoden
                      auswerten zu k�nnen.
 --------------------------------------------------------------------*)
function TQOAlarm.GetMaxShiftCount: Integer;
begin
  result := QOItems.GetMaxShiftCount;
end;// TQOAlarm.GetMaxShiftCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOfItem
 *  Klasse:           TQOAlarm
 *  Kategorie:        No category 
 *  Argumente:        (aQOItem)
 *
 *  Kurzbeschreibung: Liefert den Index des entsprechenden Items
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarm.IndexOfItem(aQOItem: TQOItem): Integer;
begin
  result := QOItems.IndexOfItem(aQOItem);
end;// TQOAlarm.IndexOfItem cat:No category

//:-------------------------------------------------------------------
(*: Member:           ItemExists
 *  Klasse:           TQOAlarm
 *  Kategorie:        No category 
 *  Argumente:        (aName, aDataItemNameType)
 *
 *  Kurzbeschreibung: Pr�ft ob ein Item mit dem entsprechenden Namen bereits existiert
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarm.ItemExists(aName: string; aDataItemNameType: TDataItemNameType = ntItem): Boolean;
begin
  result := (DataItemByName(aName, aDataItemNameType) >= 0);
end;// TQOAlarm.ItemExists cat:No category

//:-------------------------------------------------------------------
(*: Member:           OnGetActive
 *  Klasse:           TQOAlarm
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: True, wenn der Alarm aktiviert ist
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarm.OnGetActive(Sender: TObject): Boolean;
begin
  result := Active;
end;// TQOAlarm.OnGetActive cat:No category

//:-------------------------------------------------------------------
(*: Member:           Reset
 *  Klasse:           TQOAlarm
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Setzt den Alarm zur�ck
 *  Beschreibung:     
                      Wenn ein Alarm neu berechnet werden soll, m�ssen einige Daten 
                      neu initialisiert werden.
 --------------------------------------------------------------------*)
procedure TQOAlarm.Reset;
begin
  Caught := false;
  FInvolvedProdIDs := '';
  FProdIDsInOfflimit.Clear;
  Values.Close;
end;// TQOAlarm.Reset cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveDataToDB
 *  Klasse:           TQOAlarm
 *  Kategorie:        No category 
 *  Argumente:        (aQuery, aAlarmDataID, aPersistent)
 *
 *  Kurzbeschreibung: Holt die Settings der bDataItems/Methoden di im Offlimit sind
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOAlarm.SaveDataToDB(aQuery: TNativeAdoQuery; aAlarmDataID: integer; aPersistent: boolean);
var
  xXML: TIcXMLParser;
  xXMLDoc: TIcXMLDocument;
  xXMLQO: TIcXMLElement;
  xXMLMethod: TIcXMLElement;
  xXMLItem: TIcXMLElement;
  xXMLSettingsNode: TIcXMLElement;
  xStream: TStringStream;
  i: Integer;
  j: Integer;
  xID: Integer;
  xAssignList: TStringList;
  xValueData: String;
  xDataItemID: Integer;
  xMethodType: Integer;
  xDataItemText: String;
  
  const
    cInsertAlarmData = 'INSERT INTO t_QO_Alarm_Data ' +
                       ' (c_QO_Alarm_Data_id, c_caught_alarm_id, c_QOAlarm_id, c_QO_Alarm_Name, c_Persistent, c_Alarm_Settings, c_Alarm_Data, c_boolopand) ' +
                       ' VALUES (:c_QO_Alarm_Data_id, :c_caught_alarm_id, :c_QOAlarm_id, :c_QO_Alarm_Name, :c_Persistent, :c_Alarm_Settings, :c_Alarm_Data, :c_boolopand)';
  
    cInsertStyle     = 'INSERT INTO t_QO_involved_style ' +
                       '       (c_QO_Alarm_data_id, c_style_id, c_style_name) ' +
                       'VALUES (:c_QO_Alarm_data_id, :c_style_id, :c_style_name)';
  
    cInsertMachine   = 'INSERT INTO t_QO_involved_machine ' +
                       '       (c_QO_Alarm_data_id, c_machine_id, c_machine_name)' +
                       'VALUES (:c_QO_Alarm_data_id, :c_machine_id, :c_machine_name)';
  
    cInsertDataItem  = 'INSERT INTO t_QO_involved_dataitem ' +
                       '       (c_involved_dataitem_id, c_QO_Alarm_data_id, c_dataitem_name, c_boolopand)' +
                       'VALUES (:c_involved_dataitem_id, :c_QO_Alarm_data_id, :c_dataitem_name, :c_boolopand)';
  
    cInsertMethod    = 'INSERT INTO t_QO_involved_method ' +
                       '       (c_involved_dataitem_id, c_method_type)' +
                       'VALUES (:c_involved_dataitem_id, :c_method_type)';
  
  
    cInsertLot       = 'INSERT INTO t_QO_Involved_Lot ' +
                       '       (c_QO_Alarm_Data_id, c_prod_id, c_prod_name, c_Mach_id, c_first_spd, c_last_spd, c_style_id, c_shift_count, c_shift_id) ' +
                       'VALUES (:c_QO_Alarm_Data_id, :c_prod_id, :c_prod_name, :c_Mach_id, :c_first_spd, :c_last_spd, :c_style_id, :c_shift_count, :c_shift_id)';
  
   cGetStyleName     = 'SELECT c_style_name ' +
                       'FROM t_style ' +
                       'WHERE c_style_id = :c_style_id';
  
begin
  // Initialisieren
  xXMLDoc := nil;
  xXML    := nil;
  
  try
    // Der Parser ist zust�ndig f�r das erzeugen von XML
    xXML := TIcXMLParser.Create(nil);
    // "virtuelles" XML Dokument (wird sp�ter in einem Stream abgelegt)
    xXMLDoc := TIcXMLDocument.Create;
    xXMLDoc.AssignParser(xXML);
  
    // Root Node erzeugen und zuordnen
    xXMLQO := xXMLDoc.CreateElement(cXMLQOSettings);
    xXMLDoc.SetDocumentElement(xXMLQO);

//: ----------------------------------------------
    // Die Settings aller beteiligten Methoden ermitteln
    for i := 0 to QOItems.ItemCount - 1 do begin
      if QOItems[i].Caught then begin
        xXMLItem := xXMLDoc.CreateElement(cXMLQOItem);
        // Item Name schreiben (nur Info)
        xXMLItem.SetAttribute(cXMQOName, QOItems[i].QOVisualize.Caption);
        // Item hinzuf�gen
        xXMLQO.AppendChild(xXMLItem);
  
        (* Jetzt alle Methoden durchlaufen
           Es werden nur die Settings der Methoden geschrieben, die zum Offlimit begetragen haben *)
        for j := 0 to QOItems[i].QOMethods.MethodCount - 1 do begin
          if QOItems[i].QOMethods[j].Caught then begin
            // Methode
            xXMLMethod := xXMLDoc.CreateElement(cXMLMethodID);
  
            // Typ der Methode setzen
            with QOItems[i].QOMethods[j] do
              xXMLMethod.SetAttribute(cXMLMethodType, GetEnumName(TypeInfo(TCalcMethod),ord(MethodType)));
            // Methode zum Dokument hinzuf�gen
            xXMLItem.AppendChild(xXMLMethod);
            // Erstmal den Knoten f�r die Settings erzeugen und der Methode hinzuf�gen
            xXMLSettingsNode := xXMLDoc.CreateElement(cXMLMethodSettingsID);
            xXMLMethod.AppendChild(xXMLSettingsNode);
  
            // Settings speichern
            with QOItems[i].QOMethods[j] do
              WriteXMLMethodSettings(xXMLSettingsNode);
          end;// if QOItems[i].QOMethods[j].Caught then begin
        end;// for j := 0 to QOItems[i].QOMethods.MethodCount - 1 do begin
      end;// if QOItems[i].Caught then begin
    end;// for i := 0 to QOItems.ItemCount - 1 do begin

//: ----------------------------------------------
    // Der Stream nimmt den XML Formatierten String auf
    xStream := TStringStream.Create('');
    try
      // XML String erzeugen
      xXMLDoc.Write(xStream);
  
      with aQuery do begin
        SQL.Text := cInsertAlarmData;
  
        xValueData := Values.AsXML;
        ParamByName('c_caught_alarm_id').AsInteger := aAlarmDataID;
        ParamByName('c_Alarm_Settings').AsString := xStream.DataString;
        ParamByName('c_Alarm_Data').AsString := xValueData;
        ParamByName('c_QOAlarm_id').AsInteger := FID;
        ParamByName('c_QO_Alarm_Name').AsString := QOVisualize.Caption;
        ParamByName('c_Persistent').AsBoolean := aPersistent;
        ParamByName('c_boolopand').AsBoolean := BoolOpAnd;
  
        // Alarm nur schreiben, wenn auch Daten vorhanden sind
        if xValueData > '' then
          xID := InsertSQL('t_QO_Alarm_Data', 'c_QO_Alarm_Data_id', MAXINT, 1);
      end;// with aQuery do begin
    finally
      xStream.Free;
    end;// try finally

//: ----------------------------------------------
  finally
    FreeAndNil(xXMLDoc);
    FreeAndNil(xXML);
  end;// try finally

//: ----------------------------------------------
  // Artikel und Maschinen die beteiligt sind
  if FInvolvedProdIDs > '' then begin
    // xAssignList nimmt zuerst alle beteiligten Artikel und nachher alle beteiligten Maschinen auf
    xAssignList := TStringlist.Create;
    try
  // --------------- Artikel --------------
      // Recordset initialisieren
      Values.Recordset.Filter := '';
      Values.FindFirst;
      // Und jeden gefundenen Artikel zur Liste hinzuf�gen
      while not(Values.EOF) do begin
        if xAssignList.IndexOfObject(pointer(Values.FieldByName(cStyleFieldName).AsInteger)) < 0 then
          xAssignList.AddObject('', pointer(Values.FieldByName(cStyleFieldName).AsInteger));
        Values.next;
      end;// while not(Values.EOF) do begin
  
      // Jetzt die Namen der ermittelten Artikel holen
      aQuery.SQL.Text := cGetStyleName;
      for i := 0 to xAssignList.Count - 1 do begin
        aQuery.ParamByName('c_style_id').AsInteger := Integer(xAssignList.Objects[i]);
        aQuery.Open;
        if not(aQuery.EOF) then
          xAssignList[i] := aQuery.FieldByName('c_style_name').AsString;
      end;// for i := 0 to xAssignList.Count - 1 do begin
  
  
      // F�r jeden gefundenen Artikel einen Datensatz erzeugen
      aQuery.SQL.Text := cInsertStyle;
  
      for i := 0 to xAssignList.Count - 1 do begin
        try
          aQuery.ParamByName('c_QO_Alarm_data_id').AsInteger := xID;
          aQuery.ParamByName('c_style_name').AsString := xAssignList.Strings[i];
          aQuery.ParamByName('c_style_id').AsInteger := Integer(xAssignList.Objects[i]);
          aQuery.ExecSQL;
        except
          on e: EConvertError do;
        end;// try except
      end;// for i := 0 to aAssignList.Count - 1 do begin
  
  // --------------- Maschinen --------------
      // Sorgt daf�r, dass jeder Artikel/Maschine nur einmal eingetragen wird
      xAssignList.sorted := true;
      xAssignList.Duplicates := dupIgnore;
  
      // Die Liste mit den Artikeln wieder leeren um f�r die Maschinen Platz zu machen
      xAssignList.Clear;
      // Und jede gefundene Maschine zur Liste hinzuf�gen
      Values.FindFirst;
      while not(Values.EOF) do begin
        xAssignList.AddObject(Values.FieldByName(cMachineNameFieldName).AsString, pointer(Values.FieldByName(cMachineFieldName).AsInteger));
        Values.next;
      end;// while not(EOF) do begin
      // F�r jede gefundene Maschine einen Datensatz erzeugen
      aQuery.SQL.Text := cInsertMachine;
  
      for i := 0 to xAssignList.Count - 1 do begin
        try
          aQuery.ParamByName('c_QO_Alarm_data_id').AsInteger := xID;
          aQuery.ParamByName('c_machine_name').AsString := xAssignList.Strings[i];
          aQuery.ParamByName('c_machine_id').AsInteger := Integer(xAssignList.Objects[i]);
          aQuery.ExecSQL;
        except
          on e: EConvertError do;
        end;// try except
      end;// for i := 0 to xAssignList.Count - 1 do begin
  
  // --------------- DataItems/Methoden --------------
  
      aQuery.SQL.Text := cInsertDataItem;
      xAssignList.Clear;
      xAssignList.CommaText := DataItemNamesAsString(true);
      for i := 0 to xAssignList.Count - 1 do begin
        // Jedes mal neu setzten, da der Text beim Speichern der Methode ver�ndert wird
        aQuery.SQL.Text := cInsertDataItem;
        xDataItemText := xAssignList[i];
        aQuery.ParamByName('c_QO_Alarm_data_id').AsInteger := xID;
        aQuery.ParamByName('c_dataitem_name').AsString := CopyLeft(xDataItemText, cFieldSeparator, true);
  
        if AnsiSameText(CopyLeft(xDataItemText, cFieldSeparator, true), '0') then
          aQuery.ParamByName('c_boolopand').AsBoolean := false
        else
          aQuery.ParamByName('c_boolopand').AsBoolean := true;
  
        xDataItemID := aQuery.InsertSQL('t_QO_involved_dataitem', 'c_involved_dataitem_id', MAXINT, 1);
        aQuery.SQL.Text := cInsertMethod;
        while xDataItemText > '' do begin
          aQuery.ParamByName('c_involved_dataitem_id').AsInteger := xDataItemID;
          xMethodType := StrToIntDef(CopyLeft(xDataItemText, cFieldSeparator, true), -1);
          if xMethodType >= 0 then begin
            aQuery.ParamByName('c_method_type').AsInteger := xMethodType;
            aQuery.ExecSQL;
          end;// if xMethodType >= 0 then begin
        end;// while xDataItemText > '' do begin
      end;// for i := 0 to xAssignList.Count - 1 do begin
  
  // --------------- Partien --------------
      try
        aQuery.SQL.Text := cInsertLot;
        // Die betroffenen Lots liegen bereits als Liste vor
        for i := 0 to ProdIDsInOfflimit.Count - 1 do begin
          Values.Filter(cProdGroupFieldName + ' = ' + ProdIDsInOfflimit[i]);
          if not(Values.EOF) then begin
            aQuery.ParamByName('c_QO_Alarm_Data_id').AsInteger := xID;
            aQuery.ParamByName('c_prod_id').AsInteger          := Values.FieldByName(cProdGroupFieldName).AsInteger;
            aQuery.ParamByName('c_prod_name').AsString         := Values.FieldByName(cProdGroupNameFieldName).AsString;
            aQuery.ParamByName('c_Mach_id').AsInteger          := Values.FieldByName(cMachineFieldName).AsInteger;
            aQuery.ParamByName('c_first_spd').AsInteger        := Values.FieldByName(cFirstSpdFieldName).AsInteger;
            aQuery.ParamByName('c_last_spd').AsInteger         := Values.FieldByName(cLastSpindleFieldName).AsInteger;
            aQuery.ParamByName('c_style_id').AsInteger         := Values.FieldByName(cStyleFieldName).AsInteger;
            aQuery.ParamByName('c_shift_id').AsInteger         := TQODataPool.Instance.RecentShiftID;
            aQuery.ParamByName('c_shift_count').AsInteger      := TQODataPool.Instance.GetActualTurnFromProdID(Values.FieldByName(cProdGroupFieldName).AsInteger);
            aQuery.ExecSQL;
          end;// if not(Values.EOF) then begin
        end;// for i := 0 to ProdIDsInOfflimit.Count - 1 do begin
      finally
        Values.Filter('');
      end;// try finally
    finally
      xAssignList.Free;
    end;// try finally
  end;// if FInvolvedProdIDs > '' then begin
end;// TQOAlarm.SaveDataToDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveDefToDB
 *  Klasse:           TQOAlarm
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Speichert die Settings des Alarms auf der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAlarm.SaveDefToDB: Boolean;
var
  mDB: TAdoDBAccess;
  
  const
    cInsertAlarm = 'INSERT t_QO_Alarm_Setting ' +
                   '(c_QOAlarm_id, c_Name, c_BoolOpAnd, c_AdviceMethods, c_Active, c_SaveDate, c_SavingUser) ' +
                   'VALUES (:c_QOAlarm_id, :c_Name, :c_BoolOpAnd, :c_AdviceMethods, :c_Active, :c_SaveDate, :c_SavingUser)';
  
    cUpdateAlarm = 'UPDATE t_QO_Alarm_Setting ' +
                   'SET c_Name = :c_Name, c_BoolOpAnd = :c_BoolOpAnd, c_AdviceMethods = :c_AdviceMethods, ' +
                   '    c_Active = :c_Active, c_SaveDate = :c_SaveDate , c_SavingUser = :c_SavingUser ' +
                   'WHERE c_QOAlarm_id = :c_QOAlarm_id';
  
  (*-----------------------------------------------------------
    Setzt die ADO Parameter
  -------------------------------------------------------------*)
  procedure SetParameter;
  begin
    mDB.Query[0].ParamByName('c_Name').AsString  := QOVisualize.Caption;
    mDB.Query[0].ParamByName('c_BoolOpAnd').AsInteger := integer(BoolOpAnd);
    // Typecast auf Word �ndern, wenn mehr als 8 Methoden definiert sind
    mDB.Query[0].ParamByName('c_AdviceMethods').AsInteger := Byte(FAdviceMethods);
    mDB.Query[0].ParamByName('c_Active').AsInteger  := integer(FActive);
    mDB.Query[0].ParamByName('c_SaveDate').AsFloat  := now;
    mDB.Query[0].ParamByName('c_SavingUser').AsString  := SavingUser;
  
    // Letztes Speicherdatum setzen
    SaveDate := now;
    // Die ID wird nur f�r UPDATE ben�tigt
    if FID <> cINVALID_ID then
      mDB.Query[0].ParamByName('c_QOAlarm_id').AsInteger := FID;
  end;// procedure SetParameter;
  
  (*-----------------------------------------------------------
    F�gt einen neuen DAtensatz in die Tabelle ein
  -------------------------------------------------------------*)
  procedure InsertAlarm;
  begin
    mDB.Query[0].SQL.Text := cInsertAlarm;
    SetParameter;
    // Neuen Datensatz einf�gen und die ID merken
    FID := mDB.Query[0].InsertSQL('t_QO_Alarm_Setting', 'c_QOAlarm_id', MAXINT, 1);
  end;// procedure InsertAlarm;
  
  (*-----------------------------------------------------------
    Ver�ndert einen bestehenden Datensatz
  -------------------------------------------------------------*)
  procedure UpdateAlarm;
  begin
    mDB.Query[0].SQL.Text := cUpdateAlarm;
    SetParameter;
    // Datensatz Updaten
    if mDB.Query[0].ExecSQL = 0 then begin
      // Wenn kein Datensatz betroffen war, dann den Datensatz neu erzeugen
      FID := cINVALID_ID;
      InsertAlarm;
    end;// if mDB.Query[0].ExecSQL = 0 then begin
  end;// procedure UpdateAlarm;
  
begin
  result := false;
  
  mDB := TAdoDBAccess.Create(1);
  try
    mDB.Init;
    // Je nachdem einen neuen DS erzeugen oder einen bestehenden aktualisieren
    if FID = cINVALID_ID then
      InsertAlarm
    else
      UpdateAlarm;
  
    if FID > cINVALID_ID then begin
      // Die DataItems auch in der Datenbank speichern
      QOItems.SaveDefToDB(FID);
  
      // Die Zuordnung auch in der Datenbank speichern
      QOAssigns.SaveDefToDB(FID);
    end;// if FID > cINVALID_ID then begin
  
    // Erfolgreich
    result := true;
  finally
    FreeAndNil(mDB);
  end;// with TAdoDBAccess.Create(1) do try
end;// TQOAlarm.SaveDefToDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TActiveMachines
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TActiveMachines.Create;
begin
  inherited Create;

//: ----------------------------------------------
  raise Exception.CreateFmt('Access class %s through Instance only', [ClassName]);
end;// TActiveMachines.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           CreateInstance
 *  Klasse:           TActiveMachines
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TActiveMachines.CreateInstance;
begin
  inherited Create;

//: ----------------------------------------------
  Init;
end;// TActiveMachines.CreateInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TActiveMachines
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TActiveMachines.Destroy;
begin
  SetLength(mActiveProdGroups,0);

//: ----------------------------------------------
  if AccessInstance(0) = Self then AccessInstance(2);

//: ----------------------------------------------
  inherited Destroy;
end;// TActiveMachines.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           AccessInstance
 *  Klasse:           TActiveMachines
 *  Kategorie:        No category 
 *  Argumente:        (Request)
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class function TActiveMachines.AccessInstance(Request: Integer): TActiveMachines;
  
  const FInstance: TActiveMachines = nil;
  
begin
  case Request of
    0 : ;
    1 : if not Assigned(FInstance) then FInstance := CreateInstance;
    2 : FInstance := nil;
  else
    raise Exception.CreateFmt('Illegal request %d in AccessInstance', [Request]);
  end;
  Result := FInstance;

//: ----------------------------------------------
  if assigned(result) then begin
    if not result.Initialized then
      result.Init;
  end;// if assigned(result) then begin
end;// TActiveMachines.AccessInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetActiveProdGroups
 *  Klasse:           TActiveMachines
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Holt alle aktiven Produktionsgruppen aus der DB
 *  Beschreibung:     
                      Die Paare der Maschinen und Artikel werden im Array gespeichert.
                      Die Liste wird nur einmal pro Applikation geladen.
 --------------------------------------------------------------------*)
function TActiveMachines.GetActiveProdGroups: Boolean;
var
  xIndex: Integer;
  xRecordCount: Integer;
  
  const
    cGetInProd = 'SELECT DISTINCT c_machine_id, c_style_id ' +
                 'FROM t_prodgroup_state ';
  
begin
  result := false;

//: ----------------------------------------------
  with TAdoDBAccess.Create(1) do try
    result := init;
    if result then begin
      with Query[0] do begin
        SQL.Text := cGetInProd;
        // Fragt alle Maschinen ab, die momentan Produzieren
        open;
  
  
        xRecordCount := RecordSet.RecordCount;
        // -1, wenn das Recordset die Funktion Recordcount nicht unterst�tzt
        if xRecordCount < 0 then begin
          FindFirst;
          xRecordCount := 0;
          while not(EOF) do begin
            inc(xRecordCount);
            next;
          end;// while not(EOF) do begin
        end;// if xRecordCount < 0 then begin
  
        SetLength(mActiveProdGroups, xRecordCount);
        xIndex := 0;
  
        FindFirst;
        while not(EOF) do begin
          mActiveProdGroups[xIndex, cMachineIDCol] := FieldByName('c_machine_id').AsInteger;
          mActiveProdGroups[xIndex, cStyleIDCol]   := FieldByName('c_style_id').AsInteger;
          inc(xIndex);
          next;
        end;// while not(EOF) do begin
      end;// with Query[0] do begin
    end;// if result then begin
  finally
    free;
  end;// with TAdoDBAccess.Create(1) do try
end;// TActiveMachines.GetActiveProdGroups cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetRowCount
 *  Klasse:           TActiveMachines
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r "RowCount"
 *  Beschreibung:     
                      Die Anzahl Eintr�ge sagen etwas dar�ber aus ob �berhaupt Produziert wird.
 --------------------------------------------------------------------*)
function TActiveMachines.GetRowCount: Integer;
begin
  result := 0;
  
  // Anzahl Datens�tze
  if assigned(mActiveProdGroups) then
    result := Length(mActiveProdGroups)
end;// TActiveMachines.GetRowCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           Init
 *  Klasse:           TActiveMachines
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Initialisiert Das Objekt
 *  Beschreibung:     
                      Da nur ein Objekt dieser Klasse existiert, wird "Init" aus 
                      "AccessInstance" aufgerufen
 --------------------------------------------------------------------*)
procedure TActiveMachines.Init;
begin
  Initialized := GetActiveProdGroups;
end;// TActiveMachines.Init cat:No category

//:-------------------------------------------------------------------
(*: Member:           Instance
 *  Klasse:           TActiveMachines
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class function TActiveMachines.Instance: TActiveMachines;
begin
  Result := AccessInstance(1);
end;// TActiveMachines.Instance cat:No category

//:-------------------------------------------------------------------
(*: Member:           MachInProd
 *  Klasse:           TActiveMachines
 *  Kategorie:        No category 
 *  Argumente:        (aMachID, aStyleID)
 *
 *  Kurzbeschreibung: Fragt ab ob auf einer Maschine ein bestimmter Artikel produziert wird
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TActiveMachines.MachInProd(aMachID: integer; aStyleID: integer): Boolean;
var
  i: Integer;
begin
  result := false;
  i := Low(mActiveProdGroups);

//: ----------------------------------------------
  while (i <= High(mActiveProdGroups)) and(not(result)) do begin
    result := (mActiveProdGroups[i, cMachineIDCol] = aMachId);
    if aStyleID >= 0 then
      result := result and (mActiveProdGroups[i, cStyleIDCol] = aStyleID);
    inc(i);
  end;// while (i <= High(mActiveProdGroups)) and(not(result)) do begin
end;// TActiveMachines.MachInProd cat:No category

//:-------------------------------------------------------------------
(*: Member:           ReleaseInstance
 *  Klasse:           TActiveMachines
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class procedure TActiveMachines.ReleaseInstance;
begin
  AccessInstance(0).Free;
end;// TActiveMachines.ReleaseInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           StyleInProd
 *  Klasse:           TActiveMachines
 *  Kategorie:        No category 
 *  Argumente:        (aStyleID)
 *
 *  Kurzbeschreibung: Gibt zur�ck ob ein Artikel auf irgendeiner Maschine produziert wird
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TActiveMachines.StyleInProd(aStyleID: integer): Boolean;
var
  i: Integer;
begin
  result := false;
  i := Low(mActiveProdGroups);

//: ----------------------------------------------
  while (i <= High(mActiveProdGroups)) and(not(result)) do begin
    result := (mActiveProdGroups[i, cStyleIDCol] = aStyleId);
    inc(i);
  end;// while (i <= High(mActiveProdGroups)) and(not(result)) do begin
end;// TActiveMachines.StyleInProd cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQODataPool
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQODataPool.Create;
begin
  inherited Create;

//: ----------------------------------------------
  raise Exception.CreateFmt('Access class %s through Instance only', [ClassName]);
end;// TQODataPool.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           CreateInstance
 *  Klasse:           TQODataPool
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQODataPool.CreateInstance;
  
  const
    cdpProdGroup = 0;
    cdpDataPool  = 1;
    cdpAllShifts = 2;
    cdpStyleData = 3;
    cdpCount     = 4; // Nummer immer eins h�her als letes Query
  
begin
  inherited Create;

//: ----------------------------------------------
  FRecentShiftStart := 1;

//: ----------------------------------------------
  mDB := TAdoDBAccess.Create(cdpCount);
  if not(mDB.Init) then
    raise Exception('TQODataPool.CreateInstance :' + sNoDBInit);
  
  ActiveProdIDs := mDB.Query[cdpProdGroup];
  AllShifts     := mDB.Query[cdpAllShifts];
  DataPool      := mDB.Query[cdpDataPool];
  StyleData     := mDB.Query[cdpStyleData];
  
  // Standardm�ssig der erste Schicht Kalender
  FShiftCalID   := 1;
end;// TQODataPool.CreateInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TQODataPool
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TQODataPool.Destroy;
begin
  if AccessInstance(0) = Self then AccessInstance(2);

//: ----------------------------------------------
  inherited Destroy;
end;// TQODataPool.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           AccessInstance
 *  Klasse:           TQODataPool
 *  Kategorie:        No category 
 *  Argumente:        (Request)
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class function TQODataPool.AccessInstance(Request: Integer): TQODataPool;
  
  const FInstance: TQODataPool = nil;
  
begin
  case Request of
    0 : ;
    1 : if not Assigned(FInstance) then FInstance := CreateInstance;
    2 : FInstance := nil;
  else
    raise Exception.CreateFmt('Illegal request %d in AccessInstance', [Request]);
  end;
  Result := FInstance;

//: ----------------------------------------------
  // Beim ersten Zugriff alle Daten zusammenstellen
  if assigned(result)then
    result.init;
end;// TQODataPool.AccessInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetActualTurnFromProdID
 *  Klasse:           TQODataPool
 *  Kategorie:        No category 
 *  Argumente:        (aProdId)
 *
 *  Kurzbeschreibung: Gibt zur�ck wieviele zusammenh�ngende Schichten das Lot im Alarm war
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQODataPool.GetActualTurnFromProdID(aProdId: integer): Integer;
var
  xNoMatch: boolean;
  
  const
    cGetProdIDAlarms = 'SELECT c_shift_id ' +
                       'FROM t_QO_Involved_Lot ' +
                       'WHERE c_prod_id = :c_prod_id ' +
                       'ORDER BY c_shift_id DESC';
  
begin
  result := 0;
  with TAdoDBAccess.Create(1) do try
    Init;
    with Query[0] do begin
      SQL.Text := cGetProdIDAlarms;
      ParamByNAme('c_prod_id').AsInteger := aProdId;
      open;
      xNoMatch := false;
      AllShifts.FindFirst;
      if not(AllShifts.EOF) then
        AllShifts.Next;
      while (not(AllShifts.EOF)) and (not(xNoMatch)) do begin
        Filter('c_shift_id = ' + AllShifts.FieldByName('c_shift_id').AsString);
        if not(EOF) then
          inc(result)
        else
          xNoMatch := true;
        AllShifts.next;
      end;// while (not(AllShifts.EOF)) and (not(xNoMatch)) do begin
    end;// with Query[0] do begin
  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try
end;// TQODataPool.GetActualTurnFromProdID cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetRecentShiftID
 *  Klasse:           TQODataPool
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Holt die aktuelle Schicht aus dem Query mit allen Schichten
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQODataPool.GetRecentShiftID: Integer;
begin
  if (FRecentShiftID = 0) and (AllShifts.Active) then begin
    with AllShifts do begin
      FindFirst;
      if not(EOF) then
        FRecentShiftID := FieldByName('c_shift_id').AsInteger;
    end;// with AllShifts do begin
  end;// if (FRecentShiftID = 0) and (AllShifts.Active) then begin

//: ----------------------------------------------
  result := FRecentShiftID;
end;// TQODataPool.GetRecentShiftID cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetRecentShiftStart
 *  Klasse:           TQODataPool
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Holt den Start der aktuellen Schicht aus dem Query mit allen Schichten
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQODataPool.GetRecentShiftStart: TDateTime;
begin
  if (FRecentShiftStart <= 1) and (AllShifts.Active) then begin
    with AllShifts do begin
      FindFirst;
      if not(EOF) then
        FRecentShiftStart := FieldByName('c_shift_start').AsDateTime;
    end;// with AllShifts do begin
  end;// if (FRecentShiftStart <= 1) and (AllShifts.Active) then begin

//: ----------------------------------------------
  result := FRecentShiftStart;
end;// TQODataPool.GetRecentShiftStart cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetStyleData
 *  Klasse:           TQODataPool
 *  Kategorie:        No category 
 *  Argumente:        (aStyles)
 *
 *  Kurzbeschreibung: Holt die Artikeldaten f�r die Artikel die im Alarm sind
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQODataPool.GetStyleData(aStyles: string);
var
  xStyleDataQuery: String;
begin
  CodeSite.SendString('Beteiligte Artikel', aStyles);

//: ----------------------------------------------
  if aStyles > '' then begin
    // Query zusammenstellen
    xStyleDataQuery := 'SELECT ' + cFixStyleDataFields + ', SUM(v.c_len) as c_len, SUM(v.c_SFICnt) as c_SFICnt,SUM(v.c_AdjustBase) as c_AdjustBase, SUM(v.c_AdjustBaseCnt) as c_AdjustBaseCnt, ';
    xStyleDataQuery := xStyleDataQuery + mSQLSelectFields + #13#10;
    xStyleDataQuery := xStyleDataQuery + ' FROM ' + mSQLFromTables + 'v_production_shift v' + #13#10;
    xStyleDataQuery := xStyleDataQuery + ' WHERE  c_style_id in (' + aStyles + ') ' + mSQLWhere + #13#10;
    xStyleDataQuery := xStyleDataQuery + '        AND c_shift_start >= ''' + FormatDateTime('yyyy-mm-dd hh:nn:ss', mOldestShift) + '''' + #13#10;
    xStyleDataQuery := xStyleDataQuery + '        AND c_shift_start <= ''' + FormatDateTime('yyyy-mm-dd hh:nn:ss', FRecentShiftStart) + ''' ' + #13#10;
    xStyleDataQuery := xStyleDataQuery + ' GROUP BY ' +  cFixStyleDataFields + #13#10;
    xStyleDataQuery := xStyleDataQuery + ' ORDER BY c_shift_id, c_style_id' + #13#10;
  
    CodeSite.SendString('StyleData Query', xStyleDataQuery);
  
    with FStyleData do begin
      SQL.Text := xStyleDataQuery;
      Open;
    end;// with FStyleData do begin
  end;// if aStyles > '' then begin
end;// TQODataPool.GetStyleData cat:No category

//:-------------------------------------------------------------------
(*: Member:           Init
 *  Klasse:           TQODataPool
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Initialisiert die Querys und stellt den Daten Pool zusammen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQODataPool.Init;
var
  xShiftCountToObserve: Integer;
  i: Integer;
  xQOAlarm: TQOAlarm;
  xDataPoolQuery: String;
  xTempString: string;
  xWhere: String;
  xUsedTables: TUsedTablesSet;
  
  const
    cSelectProdGroups = 'SELECT     c_prod_id, c_prod_state, c_style_id, c_machine_id, ' +
                        '           c_act_yarncnt, c_yarncnt_unit, c_prod_start, c_prod_end, c_prod_name ' +
                        'FROM       t_prodgroup ' +
                        'WHERE      c_prod_state = 1 /* 1 = laufende Produktions Gruppe */  ' +
                        '       AND c_prod_start <= :c_prod_start ' +
                        'ORDER BY   c_style_id, c_machine_id ';
  
  
    cSelectProdGroupsPast = 'SELECT     c_prod_id, c_prod_state, c_style_id, c_machine_id, ' +
                            '           c_act_yarncnt, c_yarncnt_unit, c_prod_start, c_prod_end, c_prod_name ' +
                            'FROM       t_prodgroup ' +
                            'WHERE      (c_prod_start <= :c_prod_start ' +
                            '            AND (c_prod_end > dateadd(minute, 2, :c_prod_start))) ' +
                            '       OR  (c_prod_state = 1 /* 1 = laufende Produktions Gruppe */ ' +
                            '            AND c_prod_start <= :c_prod_start) ' +
                            'ORDER BY   c_style_id, c_machine_id';
  
    cSelectAllShifts     = 'SELECT c_shift_id, c_shift_start ' +
                           'FROM  t_shift ' +
                           'WHERE  (DATEADD(minute, c_shift_length, c_shift_start) < ''%s'') ' +
                           '    AND c_shiftcal_id = :c_shiftcal_id ' +
                           'ORDER BY c_shift_start desc';
  
    // Alias f�r die View (Wird f�r den WHERE Teil gebraucht)
    cLinkAlias = 'v';
  
begin
  if not(FInitialized) then begin
    if not(FInitializing) then begin

//: ----------------------------------------------
      try
        FInitializing := true;
        FRecentShiftID := 0;
        FRecentShiftStart := 1;

//: ----------------------------------------------
  // Alle Startdaten der Schichten in der Vergangenheit
        with AllShifts do begin
          close;
          SQL.Text := format(cSelectAllShifts, [FormatDateTime('yyyy-mm-dd hh:nn:ss', now)]);
          ParamByName('c_shiftcal_id').AsInteger := FShiftCalID;
          open;
        end;// with AllShifts do begin
  
  // Alle produzierenden Partien
        with ActiveProdIDs do begin
          if SelectPast then
            SQL.Text := cSelectProdGroupsPast
          else
            SQL.Text := cSelectProdGroups;
          ParamByName('c_prod_start').AsDateTime := VarFromDateTime(RecentShiftStart);
          Open;
        end;// with ActiveProdIDs do begin

//: ----------------------------------------------
  // Datenpool (beinhaltet alle Daten aller produzierender Partien alelr betieligter DataItems)
        xShiftCountToObserve := TQOAlarms.Instance.GetMaxShiftCount;
        if xShiftCountToObserve > 0 then begin
          // Anzahl anzuzeigende Schichten auslesen
          xShiftCountToObserve := xShiftCountToObserve + TMMSettingsReader.Instance.Value[cGraphicShiftDepth];
  
          // Teile des Query's zusammenstellen (Items aus allen Alarmen sammeln)
          mSQLSelectFields := '';
          mSQLFromTables   := '';
          mSQLWhere        := '';
          xUsedTables      := [];
          for i := 0 to TQOAlarms.Instance.AlarmCount - 1 do begin
            xQOAlarm := TQOAlarms.Instance.Alarms[i];
            if xQOAlarm.Active then begin
              xQOAlarm.QOItems.QueryParameters.TimeMode := tmShift;
              xQOAlarm.QOItems.LinkAlias := cLinkAlias;
  
              // Container Initialisieren
              xQOAlarm.QOItems.PrepareTable;
              xTempString := xQOAlarm.QOItems.GetSQLSelectFields;
              if xTempString > '' then
                mSQLSelectFields := mSQLSelectFields + ',' + xTempString;
              // Zusammensuchen, welche Tabellen beteiligt sind
              xUsedTables := xUsedTables + xQOAlarm.QOItems.UsedTables;
            end;// if xQOAlarm.Active then begin
          end;// for i := 0 to TQOAlarms.Instance.AlarmCount do begin
          // Jeweils das f�hrende Komma l�schen
          if mSQLSelectFields > '' then
            system.delete(mSQLSelectFields, 1, 1);
          mSQLFromTables := GetSQLFromTableGlobal(xUsedTables, '');
          mSQLWhere := GetSQLWhereGlobal(xUsedTables, cLinkAlias);
  
          (* �lteste relevante Schicht f�r den DataPool ermitteln.
             Diese Anzahl berechnet sich aus der Anzahl Schichten in denen eine Methode
             im Alarm ist und der Anzahl Schichten die im Chart angezeigt werden *)
          mOldestShift := now;
          with FAllShifts do begin
            FindFirst;
            if not(EOF) then begin
              Recordset.Move(xShiftCountToObserve, EmptyParam);
              if not(EOF) then begin
                mOldestShift := FieldByName('c_shift_start').AsDateTime;
              end else begin
                if FindLast then
                  mOldestShift := FieldByName('c_shift_start').AsDateTime;
              end;// if not(EOF) then begin
            end;// if not(EOF) then begin
          end;// with FAllShifts do begin
  
          // Da sowiso immer die View hinten dran kommt, muss noch ein Komma angef�gt werden
          if mSQLFromTables > '' then
            mSQLFromTables := mSQLFromTables + ', ';
  
          // Where Klausel zusammensetzen
          xWhere := TQOAlarms.Instance.GetProdIDs;
          if xWhere > '' then
            xWhere := ' AND c_prod_id in (' + xWhere + ') '+ #13#10;
          xWhere := ' WHERE c_shift_start >= ''' + FormatDateTime('yyyy-mm-dd hh:nn:ss', mOldestShift) + ''' ' +
                    ' AND c_shift_start <= ''' + FormatDateTime('yyyy-mm-dd hh:nn:ss', FRecentShiftStart) + ''' ' +
                    #13#10 + xWhere;
          xWhere := xWhere + mSQLWhere + #13#10;
  
          xDataPoolQuery := xDataPoolQuery + ' WHERE  c_prod_id in (' + TQOAlarms.Instance.GetProdIDs + ') ' + mSQLWhere + #13#10;
          xDataPoolQuery := xDataPoolQuery + '        AND c_shift_start >= ''' + FormatDateTime('yyyy-mm-dd hh:nn:ss', mOldestShift) + '''' + #13#10;
  
          // Query zusammenstellen
          xDataPoolQuery := 'SELECT ' + cFixDataPoolFields + ', SUM(v.c_len) as c_len, SUM(v.c_SFICnt) as c_SFICnt, SUM(v.c_AdjustBase) as c_AdjustBase, SUM(v.c_AdjustBaseCnt) as c_AdjustBaseCnt, ';
          xDataPoolQuery := xDataPoolQuery + mSQLSelectFields + #13#10;
          xDataPoolQuery := xDataPoolQuery + ' FROM ' + mSQLFromTables + 'v_production_shift v' + #13#10;
          xDataPoolQuery := xDataPoolQuery + xWhere;
          xDataPoolQuery := xDataPoolQuery + ' GROUP BY ' +  cFixDataPoolFields + #13#10;
          xDataPoolQuery := xDataPoolQuery + ' ORDER BY c_shift_id, c_style_id, c_prod_id, c_Machine_id' + #13#10;
  
          CodeSite.SendString('DataPool Query', xDataPoolQuery);
  
          with FDataPool do begin
            SQL.Text := xDataPoolQuery;
            Open;
          end;// with FDataPool do begin
        end;// if xShiftCountToObserve > 0 then begin

//: ----------------------------------------------
        FInitialized := true;

//: ----------------------------------------------
      finally
        FInitializing := false;
      end;// try finally

//: ----------------------------------------------
    end;// if not(FInitialized) then begin
  end;//if not(FInitializing) then begin
end;// TQODataPool.Init cat:No category

//:-------------------------------------------------------------------
(*: Member:           Instance
 *  Klasse:           TQODataPool
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class function TQODataPool.Instance: TQODataPool;
begin
  Result := AccessInstance(1);
end;// TQODataPool.Instance cat:No category

//:-------------------------------------------------------------------
(*: Member:           ReleaseInstance
 *  Klasse:           TQODataPool
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Singleton
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class procedure TQODataPool.ReleaseInstance;
begin
  AccessInstance(0).Free;
end;// TQODataPool.ReleaseInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetSelectPast
 *  Klasse:           TQODataPool
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt Initialized auf false, wenn vergangene Daten abgefragt werden sollen
 *  Beschreibung:     
                      Dadurch wird das Objekt beim n�chsten Zugriff mit dem aktuellen Datum neu initialisiert
 --------------------------------------------------------------------*)
procedure TQODataPool.SetSelectPast(Value: Boolean);
begin
  if FSelectPast <> Value then
  begin

//: ----------------------------------------------
  FSelectPast := Value;

//: ----------------------------------------------
  Finitialized := false;

//: ----------------------------------------------
  end;
end;// TQODataPool.SetSelectPast cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TLimitValueList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TLimitValueList.Create;
begin
  inherited;
  mStyleLimits := TList.Create;
end;// TLimitValueList.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TLimitValueList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TLimitValueList.Destroy;
begin
  Clear;
  FreeAndNil(mStyleLimits);
  inherited;
end;// TLimitValueList.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           Add
 *  Klasse:           TLimitValueList
 *  Kategorie:        No category 
 *  Argumente:        (aStyleID)
 *
 *  Kurzbeschreibung: F�gt ein neues Grenzwert-Array in die Liste ein
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TLimitValueList.Add(aStyleID: integer): Integer;
var
  xStyleLimit: TStyleLimit;
begin
  result := -1;
  
  xStyleLimit := TStyleLimit.Create;
  // Artikel zuweisen
  xStyleLimit.StyleID := aStyleID;
  xStyleLimit.DataItemFieldName := FDataItem.FieldName;
  xStyleLimit.SampleShiftCount := FSampleShiftCount;
  xStyleLimit.CalcMode := FDataItem.CalcMode;
  
  // Grenzwert zur Grenzwertliste hinzuf�gen
  result := AddAndInit(xStyleLimit);
end;// TLimitValueList.Add cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddAndInit
 *  Klasse:           TLimitValueList
 *  Kategorie:        No category 
 *  Argumente:        (aStyleLimit)
 *
 *  Kurzbeschreibung: F�gt ein neues Grenzwert-Array in die Liste ein
 *  Beschreibung:     
                      Das Grenzwert Array wird gleich initialisiert
 --------------------------------------------------------------------*)
function TLimitValueList.AddAndInit(aStyleLimit: TStyleLimit): Integer;
begin
  result := -1;
  if assigned(mStyleLimits) then begin
    aStyleLimit.InitLimitValues(LimitValueCount);
  
    result := mStyleLimits.Add(aStyleLimit);
  end;// if assigned(mStyleLimits) then begin
end;// TLimitValueList.AddAndInit cat:No category

//:-------------------------------------------------------------------
(*: Member:           Clear
 *  Klasse:           TLimitValueList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�scht alle Elemente
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLimitValueList.Clear;
begin
  while mStyleLimits.Count > 0 do
    delete(0);
end;// TLimitValueList.Clear cat:No category

//:-------------------------------------------------------------------
(*: Member:           Delete
 *  Klasse:           TLimitValueList
 *  Kategorie:        No category 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: L�scht ein einzelnes Element
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLimitValueList.Delete(aIndex: integer);
begin
  if assigned(mStyleLimits) then begin
    if aIndex < mStyleLimits.Count then begin
      if assigned(FOnDeleteStyleLimit) then
        FOnDeleteStyleLimit(StyleLimits[aIndex]);
      StyleLimits[aIndex].Free;
      mStyleLimits.delete(aIndex);
    end;// if aIndex < mStyleLimits.Count then begin
  end;// if assigned(mStyleLimits) then begin
end;// TLimitValueList.Delete cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetStyleData
 *  Klasse:           TLimitValueList
 *  Kategorie:        No category 
 *  Argumente:        (aStyleIDs)
 *
 *  Kurzbeschreibung: Holt alle Artikeldaten von der DB die f�r die Berechnung notwendig sind
 *  Beschreibung:     
                      Es wird f�r jeden Artikel ein TStyleLimit-Objekt erstellt. Dem StyleLimit-Objekt wird ein 
                      kompletes
                      Recordset mit den Daten des Artikels in der entsprechenden Tiefe zur Verf�gung gestellt. 
                      Das Recordset wird hier ermittelt und in das StyleLimit-Objekt kopiert. Dabei wird nicht das 
                      Recordset-Objekt sondern die Datens�tze kopiert.
                      Die Eigentliche Berechnung des Mittelwertes erfolgt dann in der Funktion GetMeanValue() in
                      der f�r jede Schicht (sofern notwendig) der Mittelwert f�r die �bergebene Schicht berechnet wird.
 --------------------------------------------------------------------*)
procedure TLimitValueList.GetStyleData(aStyleIDs: string);
var
  xStyleIDs: TStringList;
  xStyleLimitIndex: Integer;
  i: Integer;
  xFromTable: String;
  xWhere: string;
  
  const
    // Der erste Datensatz ist die neuste Schicht
    cGetStyleData = 'SELECT TOP %d c_shift_id, SUM(c_Len) as c_len, c_style_id, SUM(v.c_SFICnt) as c_SFICnt, SUM(v.c_AdjustBase) as c_AdjustBase, SUM(v.c_AdjustBaseCnt) as c_AdjustBaseCnt, %s ' +
                    'FROM         %s v_production_shift v ' +
                    'WHERE        c_shift_id <= :c_shift_id AND c_style_id = :c_style_id %s ' +
                    'GROUP BY     c_shift_id, c_style_id ' +
                    'HAVING       SUM(c_len) > 0 ' +
                    'ORDER BY     c_shift_id DESC';
  
begin
  try
    xStyleIDs := TStringList.Create;
    xStyleIDs.Sorted := true;
    xStyleIDs.Duplicates := dupIgnore;
    xStyleIDs.CommaText := aStyleIDs;
  
    DataItem.TimeMode := tmShift;
    DataItem.LinkAlias := 'v';
  
    with TAdoDBAccess.Create(1) do try
      Init;
      with Query[0] do begin
        xFromTable := GetSQLFromTableGlobal(DataItem.TableType, '');
        if xFromTable > '' then
          xFromTable := xFromTable + ',';
        xWhere := GetSQLWhereGlobal(DataItem.TableType, 'v');
  
        SQL.Text := Format(cGetStyleData, [SampleShiftCount + LimitValueCount, DataItem.GetSQLSelectFields, xFromTable, xWhere]);
        ParamByName('c_shift_id').AsInteger := TQODataPool.Instance.RecentShiftID;
        for i := 0 to xStyleIDs.Count - 1 do begin
          ParamByName('c_style_id').AsInteger := StrToIntDef(xStyleIDs[i], 0);
          open;
          if not(EOF) then begin
            xStyleLimitIndex := IndexOfStyleID(StrToIntDef(xStyleIDs[i], 0));
            // F�gt f�r diesen Artikel die Daten hinzu
            if xStyleLimitIndex < 0 then
              xStyleLimitIndex := Add(StrToIntDef(xStyleIDs[i], 0));
  
            StyleLimits[xStyleLimitIndex].StyleData := Query[0];
          end;// if not(EOF) then begin
        end;// for i := 0 to xStyleIDs.Count - 1 do begin
      end;// with Query[0] do begin
    finally
      Free;
    end;// with TAdoDBAccess.Create(1) do try
  finally
    FreeAndNil(xStyleIDs);
  end;// try finally
end;// TLimitValueList.GetStyleData cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetStyleLimitCount
 *  Klasse:           TLimitValueList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert zur�ck f�r wieviele Artikel die Grenzwerte gespeichert sind
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TLimitValueList.GetStyleLimitCount: Integer;
begin
  result := 0;
  
  if assigned(mStyleLimits) then
    result := mStyleLimits.Count;
end;// TLimitValueList.GetStyleLimitCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetStyleLimits
 *  Klasse:           TLimitValueList
 *  Kategorie:        No category 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: Liefert ein "Grenzwert-Objekt" f�r einen Artikel
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TLimitValueList.GetStyleLimits(aIndex: Integer): TStyleLimit;
begin
  result := nil;
  if assigned(mStyleLimits) then
    if mStyleLimits.Count > aIndex then
      result := TStyleLimit(mStyleLimits.Items[aIndex]);
end;// TLimitValueList.GetStyleLimits cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOf
 *  Klasse:           TLimitValueList
 *  Kategorie:        No category 
 *  Argumente:        (aStyleLimit)
 *
 *  Kurzbeschreibung: Sucht ein Element in der Liste und gibt den Index zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TLimitValueList.IndexOf(aStyleLimit: TStyleLimit): Integer;
var
  i: Integer;
begin
  result := -1;
  
  i := 0;
  while (i < mStyleLimits.Count) and (result = -1) do begin
    if mStyleLimits[i] = aStyleLimit then
      result := i;
    inc(i);
  end;// while (i < mStyleLimits.Count) and (result = -1) do begin
end;// TLimitValueList.IndexOf cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOfStyleID
 *  Klasse:           TLimitValueList
 *  Kategorie:        No category 
 *  Argumente:        (aStyleID)
 *
 *  Kurzbeschreibung: Sucht ein Element in der Liste und gibt den Index zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TLimitValueList.IndexOfStyleID(aStyleID: integer): Integer;
var
  i: Integer;
begin
  result := -1;
  
  i := 0;
  while (i < mStyleLimits.Count) and (result = -1) do begin
    if StyleLimits[i].StyleID = aStyleID then
      result := i;
    inc(i);
  end;// while (i < mStyleLimits.Count) and (result = -1) do begin
end;// TLimitValueList.IndexOfStyleID cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TStyleLimit
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TStyleLimit.Destroy;
begin
  inherited Destroy;

//: ----------------------------------------------
  // Array wieder freigeben
  SetLength(FLimitValues, 0);
  FreeAndNil(FStyleData);
end;// TStyleLimit.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMeanValue
 *  Klasse:           TStyleLimit
 *  Kategorie:        No category 
 *  Argumente:        (aShiftID, aShiftNumber, aLenBase)
 *
 *  Kurzbeschreibung: Berechnet den Grenzwert (ohne Ausreisserbehandlung)
 *  Beschreibung:     
                      Der Grenzwert wird berechnet indem die gew�nschte Anzahl Schichten summiert, und dann
                      durch die Anzahl der Grenzwerte dividiert werden. Dabei muss beachtet werden, dass nur 
                      produzierende Schichten im Recordset zur Verf�gung stehen. 
                      Ist die gew�nschte Schicht nicht vorhanden, dann wird die neueste vorhandene Schicht als
                      Ausgangspunkt f�r die Berechnung herangezogen. Wenn mehrere Schichten nicht produziert wurde, 
                      dann wird der ermittelte Grenzwert immer der selbe sein. 
 --------------------------------------------------------------------*)
procedure TStyleLimit.GetMeanValue(aShiftID: integer; aShiftNumber: integer; aLenBase: TLenBase);
var
  xFound: Boolean;
  xSampleCount: Integer;
  i: Integer;
  xSample: TSampleArray;
  xLenArray: TLenArray;
  xSFICntArray: TSFICntArray;
  xAdjBaseArray: TAdjBaseArray;
  xAdjBaseCntArray: TAdjBaseCntArray;
  xSampleSum: Extended;
  xLenSum: Extended;
  xSFICntSum: Extended;
  xAdjBaseSum: Extended;
  xAdjBaseCntSum: Extended;
begin
  // Initialisieren
  LimitValues[aShiftNumber] := 0;
  
  with StyleData do begin
  // Zuerst einmal die Datenmenge wieder auf Anfang setzen.
    StyleData.SendRecordsetToCodeSite('StyleData', 0);
    FindFirst;
  
    (* Zuerst die gew�nschte Schicht f�r den Anfang der Mittelwertbildung finden.
       Es ist zwar die Position der Schicht in der Bearbeitung bekannt (aShiftNumber).
       Nat�rlich ist nicht sichergestellt, dass w�hrend der ganzen Zeit produziert
       wurde - Deshalb kann nicht einfach der z.B. 5 te Eintrag als Startpunkt
       f�r die Berechnung verwendet werden, sondern die erste produzierende
       Schicht vor der Schicht mit der gew�nschten Schicht ID.*)
    xFound := false;
    // Solange suchen bis die gew�nschte Schicht, oder die erste davor gefunden wurde
    while (not(xFound)) and (not(EOF)) do begin
      if FieldByName('c_shift_id').AsInteger <= aShiftID then
        xFound := true
      else
        next;
    end;// while (not(xFound)) and (not(EOF)) do begin
  
  // Jetzt sollte der Datenzeiger auf dem ersten Datensatz der Stichprobe stehen
    try
      SetLength(xSample, FSampleShiftCount);
      SetLength(xLenArray, FSampleShiftCount);
      SetLength(xSFICntArray, FSampleShiftCount);
      SetLength(xAdjBaseArray, FSampleShiftCount);
      SetLength(xAdjBaseCntArray, FSampleShiftCount);
  
      xSampleCount := 0;
  
      while (not(EOF)) and (xSampleCount < FSampleShiftCount) do begin
        (* Datenwerte in die Arrays eintragen
           !ACHTUNG!
           Die Arrays m�ssen immer synchron gehalten werden. *)
        xSample[xSampleCount] := FieldByName(DataItemFieldName).AsFloat;
        xLenArray[xSampleCount] := FieldByName('c_len').AsFloat;
        xSFICntArray[xSampleCount] := FieldByName('c_SFICnt').AsFloat;
        xAdjBaseArray[xSampleCount] := FieldByName('c_AdjustBase').AsFloat;
        xAdjBaseCntArray[xSampleCount] := FieldByName('c_AdjustBaseCnt').AsFloat;
  
  (*      xSample[xSampleCount] := calcAllValues(GetLenBase,                                // Globale L�ngenbasis
                                               FieldByName(DataItemFieldName).AsFloat,    // zu rechnender Wert
                                               FieldByName('c_len').AsFloat,              // Produzierte L�nge
                                               FCalcMode,                                 // Kalkulationsmodus (aus dem Konstantenarray)
                                               FieldByName('c_SFICnt').AsFloat,           // Anzahl der Werte auf denen der SFI beruht
                                               FieldByName('c_AdjustBase').AsFloat,       // AdjustBase f�r SFI-Berechnungen
                                               FieldByName('c_AdjustBaseCnt').AsFloat);   // Anzahl aufsummierter Werte*)
        // Und wieder ein Stichprobenwert eingetragen
        inc(xSampleCount);
        next;
      end;// while (not(EOF)) and (xSampleCount < FShiftCount) do begin
      // Arrays auf die ben�tigte L�nge k�rzen
      SetLength(xSample, xSampleCount);
      SetLength(xLenArray, xSampleCount);
      SetLength(xSFICntArray, xSampleCount);
      SetLength(xAdjBaseArray, xSampleCount);
      SetLength(xAdjBaseCntArray, xSampleCount);
  
      // Jetzt ist die gesammte Stichprobe in den Arrays gesichert und die Berechnung kann beginnen
      xSampleSum     := 0;
      xLenSum        := 0;
      xSFICntSum     := 0;
      xAdjBaseSum    := 0;
      xAdjBaseCntSum := 0;
      // Addieren aller Werte
      for i := 0 to xSampleCount -1 do begin
        xSampleSum     := xSampleSum + xSample[i];
        xLenSum        := xLenSum + xLenArray[i];
        xSFICntSum     := xSFICntSum + xSFICntArray[i];
        xAdjBaseSum    := xAdjBaseSum + xAdjBaseArray[i];
        xAdjBaseCntSum := xAdjBaseCntSum + xAdjBaseCntArray[i];
  
      end;// for i := 0 to xSampleCount -1 do begin
      xSampleSum := calcAllValues(aLenBase,         // L�ngenbasis der aufrufenden Methode
                                  xSampleSum,       // zu rechnender Daten Wert
                                  xLenSum,          // Produzierte L�nge
                                  FCalcMode,        // Kalkulationsmodus (aus dem Konstantenarray pro DataItem)
                                  xSFICntSum,       // Anzahl der Werte auf denen der SFI beruht
                                  xAdjBaseSum,      // AdjustBase f�r SFI-Berechnungen
                                  xAdjBaseCntSum);  // Anzahl aufsummierter AdjustBase-Werte
  
      // Mittelwert bestimmen und im "Mittelwert Array " sichern
      LimitValues[aShiftNumber] := xSampleSum / xSampleCount;
    finally
      // Speicher wieder freigeben
      SetLength(xSample, 0);
      SetLength(xLenArray, 0);
      SetLength(xSFICntArray, 0);
      SetLength(xAdjBaseArray, 0);
      SetLength(xAdjBaseCntArray, 0);
    end;// try finally
  end;// with StyleData do begin
end;// TStyleLimit.GetMeanValue cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetStyleData
 *  Klasse:           TStyleLimit
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert das Recordset mit den Artikeldaten
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TStyleLimit.GetStyleData: TNativeAdoQuery;
begin
  if not(assigned(FStyleData)) then
    FStyleData := TNativeADOQuery.Create;

//: ----------------------------------------------
  Result := FStyleData;
end;// TStyleLimit.GetStyleData cat:No category

//:-------------------------------------------------------------------
(*: Member:           InitLimitValues
 *  Klasse:           TStyleLimit
 *  Kategorie:        No category 
 *  Argumente:        (aCount)
 *
 *  Kurzbeschreibung: Initialisiert das Array mit der gew�nschten Anzahl Elementen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TStyleLimit.InitLimitValues(aCount: integer);
var
  i: Integer;
begin
  // Array mit den Grenzwerten initialisieren
  SetLength(FLimitValues, aCount);
  // Array initialisieren
  for i := 0 to Length(FLimitValues) - 1 do
    // Da ein Grenzwert immer positiv zu sein hat, gilt -1 als Initialisierung
    FLimitValues[i] := -1;
end;// TStyleLimit.InitLimitValues cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetStyleData
 *  Klasse:           TStyleLimit
 *  Kategorie:        No category 
 *  Argumente:        (aRecordset)
 *
 *  Kurzbeschreibung: Kopiert das �bergebene Recordset
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TStyleLimit.SetStyleData(aRecordset: TNativeAdoQuery);
begin
  StyleData.AsXML := aRecordset.AsXML;
end;// TStyleLimit.SetStyleData cat:No category


procedure LoadDataItems;
begin
// ------------------------------------------------------------
  // DataItems
  if not(assigned(gQOItems)) then
    gQOItems := TQOItems.Create;
  // Data Items laden
  gQOItems.LoadFromDataBase(TQOItem);
  // Zus�tzliche DataItems f�r den Container hinzuf�gen
  SetApplicationDataItems;
end;// procedure InitDataItems;

(*-----------------------------------------------------------
  Initialisiert die Umgebung (z.B. Definition der DataItems laden)   
-------------------------------------------------------------*)
function InitQOEnviroment: boolean;
var
  xQOStyle: TQOStyle;
  xQOMachine: TQOMachine;
begin
  result := false;
  
  // DataItems von der DB laden
  LoadDataItems;
  
// ------------------------------------------------------------
  // Artikel und Maschinen von der DB laden  
  if not(assigned(gAllStyles)) then
    gAllStyles := TQOAssigns.Create
  else
    gAllStyles.Clear;
 
  if not(assigned(gAllMachines)) then
    gAllMachines := TQOAssigns.Create
  else
    gAllMachines.Clear;
  
  // Alle Artikel von der DB laden
  with TAdoDBAccess.Create(1) do try
    Init;
    // Artikel
  
    // Einzelne Artikel von der DB laden
    with Query[0] do begin
      SQL.Text := cGetAllStyles;
      Open;
      // Artikel in einer Stringlist speichern
      while not(EOF) do begin
        xQOStyle := TQOStyle.Create;
        xQOStyle.LinkID := FieldByName('c_style_id').AsInteger;
        xQOStyle.QOVisualize.Caption := FieldByName('c_style_name').AsString;
        gAllStyles.Add(xQOStyle);
        Next;
      end;// while not(EOF) do begin
    end;// with Query[0] do begin
  
    // Maschinen
    with Query[0] do begin
      SQL.Text := cGetAllMachs;
      Open;
      // Artikel in einer Stringlist speichern
      while not(EOF) do begin
        xQOMachine := TQOMachine.Create;
        xQOMachine.LinkID := FieldByName('c_machine_id').AsInteger;
        xQOMachine.QOVisualize.Caption := FieldByName('c_machine_name').AsString;
        gAllMachines.Add(xQOMachine);
        Next;
      end;// while not(EOF) do begin
    end;// with Query[0] do begin(**)
  
    result := true;
  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try
end;// function InitQOEnviroment: boolean;

(*-----------------------------------------------------------
  F�gt die Applikations-Abh�ngigen DataItems hinzu
-------------------------------------------------------------*)
procedure SetApplicationDataItems;
var
  i: integer;
  xIndex: integer;
begin
  for i := Low(cDataItemArray) to High(cDataItemArray) do begin
    if cDataItemArray[i].QODataItem then begin
      (* Neues DataItem mit der gew�nschten Klasse erzeugen und anhand
         des zur�ckgegebenen Index auf das neue Item zuggreiffen*)
      xIndex := gQOItems.CreateAndAddNewDataItem(TQODataItem, TQOItem);
      // Eigenschaften des neuen Items setzen
      if ord(cDataItemArray[i].ClassTypeGroup) = 0 then
        gQOItems[xIndex].ClassTypeGroup   := ctDataItem
      else 
        gQOItems[xIndex].ClassTypeGroup   := cDataItemArray[i].ClassTypeGroup;
        
      gQOItems[xIndex].Description      := '';
      gQOItems[xIndex].ItemName         := cDataItemArray[i].Field;
      gQOItems[xIndex].FieldName        := 'c_' + cDataItemArray[i].Field;
      gQOItems[xIndex].DisplayName      := cDataItemArray[i].DisplayName;
      gQOItems[xIndex].OrderPos         := i;
      gQOItems[xIndex].Predefined       := true;
      // Name auch f�r die Anzeige im TreeView setzen (nur wenn das DataItem neu hinzugef�gt wird)
      TQOItem(gQOItems[xIndex]).QOVisualize.Caption := cDataItemArray[i].DisplayName;
      TQOItem(gQOItems[xIndex]).CalcMode := cDataItemArray[i].CalcMode;
    end;// if cDataItemArray[i].QODataItem then begin  
  end;// for i := Low(cDataItemArray) to High(cDataItemArray) do begin
end;// procedure SetApplicationDataItems 

initialization

finalization
  // Container f�r die DataItems wieder freigeben
  FreeAndNil(gQOItems);
  FreeAndNil(gAllStyles);
  FreeAndNil(gAllMachines);

  // Liste mit den Alarmen freigeben
  TQOAlarms.ReleaseInstance;
  
  TImageIndexHandler.ReleaseInstance;
  TActiveMachines.ReleaseInstance;
  TQODataPool.ReleaseInstance;
end.
