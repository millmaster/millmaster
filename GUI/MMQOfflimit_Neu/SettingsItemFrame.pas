(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: SettingsItemFrame
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Frame f�r die Darstellung eines Item Settings
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 06.05.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit SettingsItemFrame;

interface

uses
  QOKernel, QOGUIShared,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, mmRadioGroup, StdCtrls, mmCheckBox,
  mmGroupBox, mmPanel,ComCtrls, ImgList, mmImageList, ActnList,
  mmActionList, ToolWin, mmToolBar, mmPageControl, mmDialogs,
  MethodContainerFrame, mmLineLabel, IvDictio, IvMulti, IvEMulti,
  mmTranslator, MMSecurity;

type
  TfrmSettingsItemFrame = class;

  //1 Wird gefeuert nachdem ein DataItem gesichert wurde 
  TOnItemSaved = procedure (Sender: TfrmSettingsItemFrame; aItem: TQOItem) of object;
  (*: Klasse:        TfrmSettingsItemFrame
      Vorg�nger:     TFrame
      Kategorie:     View
      Kurzbeschrieb: Stellt die Settings eines Data Items dar 
      Beschreibung:  
                     - *)
  TfrmSettingsItemFrame = class (TFrame)
    acSave: TAction;
    acUndo: TAction;
    mButtonImages: TmmImageList;
    mClassDefSpacePanel: TmmPanel;
    mItemSettingsPanel: TmmPanel;
    mmActionList1: TmmActionList;
    mMethodContainer: TfrmMethodContainerFrame;
    mMethodHeader: TmmLineLabel;
    mmPageControl1: TmmPageControl;
    mmPanel1: TmmPanel;
    mmPanel2: TmmPanel;
    mmToolBar1: TmmToolBar;
    mTranslator: TmmTranslator;
    rgBoolOp: TmmRadioGroup;
    rgClassDefects: TmmRadioGroup;
    tabItemSettings: TTabSheet;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    //1 Speichert die ver�nderten Settings 
    procedure acSaveExecute(Sender: TObject);
    //1 L�dt die Settings wieder vom urspr�nglichen Alarm 
    procedure acUndoExecute(Sender: TObject);
    //1 Erfasst ob �nderungen an den Settings gemacht wurden 
    procedure ChangeFromGUI(Sender: TObject);
  private
    FImages: TImageList;
    FOnItemSaved: TOnItemSaved;
    FQOItem: TQOItem;
    FSaveEnabled: Boolean;
    FSaveVisible: Boolean;
    mQOAlarm: TQOAlarm;
    //1 Zugriffsmethode f�r QOItem 
    procedure SetQOItem(Value: TQOItem);
  protected
    //1 Dispatch Methode f�r den Event OnItemSaved 
    procedure DoItemSaved(Sender: TfrmSettingsItemFrame; aItem: TQOItem); virtual;
    //1 Wird aufgerufen, wenn an einer Methode etwas ge�ndert wurde 
    procedure MethodChange(aSender: TFrame; aMethod: TQOMethod);
  public
    //1 Gibt True zur�ck, wenn das aktuelle Objekt auf der gespeichert ist 
    function saved: Boolean;
    //1 Speichert die Einstellungen im AlarmObjekt 
    function SaveSettings(aAskForPermission: boolean; aShowCancel: boolean): Word;
    //1 True, wenn die Settings noch denen des �bergebenen Items entsprechen 
    function SettingsChanged: Boolean;
    //1 Zeigt die Einstellungen des �bergebenen DataItems an 
    procedure ShowItem(aQOItem: TObject; aQOAlarm: TObject);
    //1 Liste mit den Images die verwendet werden um die Methode anzuzeigen 
    property Images: TImageList read FImages write FImages;
    //1 Angezeigtes DataItem 
    property QOItem: TQOItem read FQOItem write SetQOItem;
    //1 True wenn die Action acSave Enabled sein darf 
    property SaveEnabled: Boolean read FSaveEnabled write FSaveEnabled;
    //1 True wenn die Action acSave sichtbar sein darf 
    property SaveVisible: Boolean read FSaveVisible write FSaveVisible;
  published
    //1 Wird gefeuert, wenn das DataItem gesichert wurde 
    property OnItemSaved: TOnItemSaved read FOnItemSaved write FOnItemSaved;
  end;
  
implementation

uses ClassDef;

{$R *.DFM}

//:-------------------------------------------------------------------
(*: Member:           acSaveExecute
 *  Klasse:           TfrmSettingsItemFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Speichert die ver�nderten Settings
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsItemFrame.acSaveExecute(Sender: TObject);
begin
  // Es wird nicht gefragt ob gespeichert werden soll
  SaveSettings(false, false);
end;// TfrmSettingsItemFrame.acSaveExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acUndoExecute
 *  Klasse:           TfrmSettingsItemFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: L�dt die Settings wieder vom urspr�nglichen Alarm
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsItemFrame.acUndoExecute(Sender: TObject);
begin
  // Das �rspr�ngliche DataItem wird einfach neu angezeigt
  ShowItem(FQOItem, mQOAlarm);
  ChangeFromGUI(Sender);
end;// TfrmSettingsItemFrame.acUndoExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           ChangeFromGUI
 *  Klasse:           TfrmSettingsItemFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Erfasst ob �nderungen an den Settings gemacht wurden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsItemFrame.ChangeFromGUI(Sender: TObject);
var
  xSettingsChanged: Boolean;
begin
  if assigned(QOItem) then begin
    xSettingsChanged := SettingsChanged;
  
    acSave.Enabled := (xSettingsChanged) and FSaveEnabled;
    acSave.Visible := FSaveVisible;
  
    acUndo.Enabled := xSettingsChanged;
  end;// if assigned(QOItem) then begin
end;// TfrmSettingsItemFrame.ChangeFromGUI cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoItemSaved
 *  Klasse:           TfrmSettingsItemFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aItem)
 *
 *  Kurzbeschreibung: Dispatch Methode f�r den Event OnItemSaved
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsItemFrame.DoItemSaved(Sender: TfrmSettingsItemFrame; aItem: TQOItem);
begin
  if Assigned(FOnItemSaved) then FOnItemSaved(Sender, aItem);
end;// TfrmSettingsItemFrame.DoItemSaved cat:No category

//:-------------------------------------------------------------------
(*: Member:           MethodChange
 *  Klasse:           TfrmSettingsItemFrame
 *  Kategorie:        No category 
 *  Argumente:        (aSender, aMethod)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn an einer Methode etwas ge�ndert wurde
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsItemFrame.MethodChange(aSender: TFrame; aMethod: TQOMethod);
begin
  ChangeFromGUI(nil);
end;// TfrmSettingsItemFrame.MethodChange cat:No category

//:-------------------------------------------------------------------
(*: Member:           saved
 *  Klasse:           TfrmSettingsItemFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt True zur�ck, wenn das aktuelle Objekt auf der gespeichert ist
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmSettingsItemFrame.saved: Boolean;
begin
  result := not(acSave.Enabled);
end;// TfrmSettingsItemFrame.saved cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveSettings
 *  Klasse:           TfrmSettingsItemFrame
 *  Kategorie:        No category 
 *  Argumente:        (aAskForPermission, aShowCancel)
 *
 *  Kurzbeschreibung: Speichert die Einstellungen im AlarmObjekt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmSettingsItemFrame.SaveSettings(aAskForPermission: boolean; aShowCancel: boolean): Word;
var
  xAllow: Boolean;
  xButtons: TMsgDlgButtons;
begin
  result := mrOK;
  
  if aShowCancel then
    xButtons := [mbYes, mbNo, mbCancel]
  else
    xButtons := [mbYes, mbNo];
  
  
  // Nichts unternehmen wenn kein Item zugewiesen (evt. gel�scht)
  if not(assigned(FQOItem)) then
    exit;

//: ----------------------------------------------
  // Zuerst anhand der Security feststellen ob gespeichert werden darf
  xAllow := acSave.Visible and acSave.Enabled;
  
  // Nur weitermachen, wenn die Settings ge�ndert haben
  xAllow := xAllow and SettingsChanged;
  
  // evt. Abfragen ob gespeichert werden soll
  if xAllow and aAskForPermission then begin
    result := mmMessageDlg(Format(sAskForSave,[QOItem.QOVisualize.Caption]), mtConfirmation, xButtons, 0, self);
    xAllow := (result = mrYes);
  end;// if xAllow and aAskForPermission then begin
  
  // Wenn die Settings ge�ndert haben, dann wird gespeichert
  if xAllow then begin

//: ----------------------------------------------
    FQOItem.BoolOpAnd := (rgBoolOp.ItemIndex = 0);
    FQOItem.ClassDefects := TClassDefects(rgClassDefects.ItemIndex);
  
    // Settings wurden ge�ndert
    result := mrOK;
  
    (* ------------- �nderungen �bernehmen ------------- *)
    // Zuerst die Settings der Methoden abf�llen
    mMethodContainer.SaveSettings;
    // Auf der Datenbank speichern
    QOItem.SaveDefToDB;
  
    (* Die Daten neu anzeigen, da die Methoden Kopien sind
       und der Vergleich darum nicht klappen w�rde *)
    ShowItem(QOItem, mQOAlarm);
    // Buttons disablen
    ChangeFromGUI(nil);
  
    // Anwendung informieren
    DoItemSaved(self, QOItem);

//: ----------------------------------------------
  end;// if xAllow then begin
end;// TfrmSettingsItemFrame.SaveSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetQOItem
 *  Klasse:           TfrmSettingsItemFrame
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r QOItem
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsItemFrame.SetQOItem(Value: TQOItem);
begin
  FQOItem := Value;

//: ----------------------------------------------
  visible := FQOItem <> nil;
end;// TfrmSettingsItemFrame.SetQOItem cat:No category

//:-------------------------------------------------------------------
(*: Member:           SettingsChanged
 *  Klasse:           TfrmSettingsItemFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: True, wenn die Settings noch denen des �bergebenen Items entsprechen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmSettingsItemFrame.SettingsChanged: Boolean;
var
  xBoolOpAnd: Boolean;
  xMethodsChanged: Boolean;
begin
  result := false;
  
  // Nichts unternehmen wenn kein Item zugewiesen (evt. gel�scht)
  if not(assigned(FQOItem)) then
    exit;

//: ----------------------------------------------
  // Verkn�pfung der DataItems
  xBoolOpAnd := (rgBoolOp.ItemIndex = 0);
  
  xMethodsChanged := mMethodContainer.MethodsChanged;

//: ----------------------------------------------
  // Alle Erkenntnisse zusammenziehen
  result := (
                 // Verkn�pfung der DataItems
                 (QOItem.BoolOpAnd <> xBoolOpAnd)
                 // Methoden
               or (xMethodsChanged)
               // Art der �berwachung (nur relevant f�r Matrix DataItem)
               or (FQOItem.ClassDefects <> TClassDefects(rgClassDefects.ItemIndex))
  
             );
end;// TfrmSettingsItemFrame.SettingsChanged cat:No category

//:-------------------------------------------------------------------
(*: Member:           ShowItem
 *  Klasse:           TfrmSettingsItemFrame
 *  Kategorie:        No category 
 *  Argumente:        (aQOItem, aQOAlarm)
 *
 *  Kurzbeschreibung: Zeigt die Einstellungen des �bergebenen DataItems an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmSettingsItemFrame.ShowItem(aQOItem: TObject; aQOAlarm: TObject);
var
  xDisabledClassDefects: TClassDefectsSet;
  
  procedure DisableExistingClassDefects(aSelected: TClassDefects; aDisabledClassDefects: TClassDefectsSet);
  var
    xClassDefect: TClassDefects;
  begin
    // Die gew�nschten RadioButtons auf disabled stellen
    for xClassDefect := Low(TClassDefects) to High(TClassDefects) do begin
      if rgClassDefects.Components[ord(xClassDefect)] is TWinControl then begin
        if xClassDefect <> aSelected then begin
          TWinControl(rgClassDefects.Components[ord(xClassDefect)]).Enabled := not((xClassDefect in aDisabledClassDefects));
        end else begin
          TWinControl(rgClassDefects.Components[ord(xClassDefect)]).Enabled := true;
          rgClassDefects.ItemIndex := ord(aSelected);
        end;// if xClassDefects <> aSelected then begin
      end;// if rgClassDefects.Components[ord(xClassDefect)] is TWinControl then begin
    end;// for xClassDefect := Low(TClassDefects) to High(TClassDefects) do begin
  end;// procedure DisableExistingClassDefects(aStillEnabled: TClassDefects);
  
begin
  if aQOItem is TQOItem then begin
    FQOItem := TQOItem(aQOItem);
  
    if aQOAlarm is TQOAlarm then
      mQOAlarm := TQOAlarm(aQOAlarm);

//: ----------------------------------------------
    if FQOItem.BoolOpAnd then
      rgBoolOp.ItemIndex := 0
    else
      rgBoolOp.ItemIndex := 1;
  
    xDisabledClassDefects := [];
    if assigned(mQOAlarm) then
      xDisabledClassDefects := mQOAlarm.QOItems.GetClassDefectsFromName(FQOItem.ItemName);
  
    DisableExistingClassDefects(FQOItem.ClassDefects, xDisabledClassDefects);
  //  rgClassDefects.ItemIndex := ord(FQOItem.ClassDefects);
  
    rgClassDefects.visible := FQOItem.IsMatrixDataItem;
    mClassDefSpacePanel.visible := rgClassDefects.visible;
  
    // Das Tab mit dem NAmen des Items beschriften
    tabItemSettings.Caption := FQOItem.QOVisualize.Caption;
  
    // Frame anzeigen
    Visible := true;

//: ----------------------------------------------
    // Methoden anzeigen
    mMethodContainer.OnMethodChange := nil;
    mMethodContainer.ShowItem(FQOItem, Images);
    mMethodContainer.OnMethodChange := MethodChange;

//: ----------------------------------------------
  end;// if aQOItem is TQOItem then begin
end;// TfrmSettingsItemFrame.ShowItem cat:No category

end.












