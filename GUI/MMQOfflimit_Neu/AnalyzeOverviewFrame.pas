(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: AnalyzeOverviewFrame.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Frame f�r die �bersicht �ber die registrierten Alarme (Auswertung)
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 22.08.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit AnalyzeOverviewFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, TeeProcs, TeEngine, Chart, mmChart, Series, StdCtrls,
  mmRadioGroup, mmSeries, QOChartSeries, mmPanel, Db, mmDataSource, ADODB,
  mmADODataSet, DBCtrls, mmDBListBox, mmADOConnection, Grids, DBGrids,
  mmDBGrid, mmLabel, ComCtrls, mmPageControl, DBChart, mmDBChart, Spin,
  mmSpinEdit, IvDictio, IvMulti, IvEMulti, mmTranslator, mmImage;

type
  TOverviewAlarmFilterType = (aftNone, aftAlarm, aftMachine, aftShift);

  //1 Wird ausgel�st, wenn in ein Diagramm geklickt wurde 
  TOverviewChartClickEvent = procedure (aDate: TDateTime; aButton: TMouseButton; aShiftKey: TShiftState) of object;
  (*: Klasse:        TOverviewDataRec
      Vorg�nger:     
      Kategorie:     record
      Kurzbeschrieb: Record f�r die Datenhaltung im Chart 
      Beschreibung:  
                     - *)
  TOverviewDataRec = record
    AlarmDataID: Integer;
    Value: Integer;
    Date: TDateTime;
  end;
  
  POverviewDataRec = ^TOverviewDataRec;

  (*: Klasse:        TOverviewBarSeries
      Vorg�nger:     TQOBarSeries
      Kategorie:     No category
      Kurzbeschrieb: Frame zur Analyse der eingetroffenen Alarme 
      Beschreibung:  
                     Der Frame bietet eine �bersicht �ber alle registrierten Alarme *)
  TOverviewBarSeries = class (TQOBarSeries)
  public
    //1 F�gt ein Punktepaar zur Serie hinzu 
    function AddXY(AOverviewDataRec: POverviewDataRec; const ALabel: String; AColor: TColor): LongInt; reintroduce; 
      overload; virtual;
  end;
  
  (*: Klasse:        TfrmAnalyzeOverviewFrame
      Vorg�nger:     TFrame
      Kategorie:     No category
      Kurzbeschrieb: Frame der statistische Daten f�r den �berblick �ber die registrierten Alarme bereitstellt 
      Beschreibung:  
                     - *)
  TfrmAnalyzeOverviewFrame = class (TFrame)
    mHistogramChart: TmmChart;
    mInfoImage: TmmImage;
    mmTranslator1: TmmTranslator;
    mRankingAlarmChart: TmmChart;
    mRankingMachineChart: TmmChart;
    mSpacerGraph: TmmPanel;
    paRanking: TmmPanel;
    //1 Passt die Gr�sse der Charts an, damit beide Charts gleich gross sind 
    procedure FrameResize(Sender: TObject);
    //1 Wird aufgerufen, wenn die Maus �ber das Chart bewegt wird 
    procedure mChartMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    //1 Wird aufgerufen, wenn im Histogramm ein Alarm angeklickt wurde 
    procedure mHistogramChartClickSeries(Sender: TCustomChart; Series: TChartSeries; ValueIndex: Integer; Button: 
      TMouseButton; Shift: TShiftState; X, Y: Integer);
    //1 �bernimmt alle �bersetungen die nicht automatisch "passieren" 
    procedure mmTranslator1AfterTranslate(translator: TIvCustomTranslator);
    //1 Wird ausgel�st, wenn im Alarm Ranking Chart auf einen Datenpunkt (Balken) geklickt wird 
    procedure mRankingChartClickSeries(Sender: TCustomChart; Series: TChartSeries; ValueIndex: Integer; Button: 
      TMouseButton; Shift: TShiftState; X, Y: Integer);
    //1 Wird ausgel�st, wenn auf den Hintergrund eines Charts geklickt wird 
    procedure RankingClickBackgraound(Sender: TCustomChart; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  private
    FOnOverviewChartClick: TOverviewChartClickEvent;
    mInitialized: Boolean;
    mOverviewSeries: TSeriesList;
    //1 Pr�ft eine Serie ob ein entsprechender Punkt Klickbar ist 
    function GetSerieClickable(aSerie: TChartSeries; aPointIndex: integer): Boolean;
    //1 Aktualisiert den Titel des Histogramms (wird nicht automatishc �bersetzt) 
    procedure SetHistogramTitle;
  protected
    //1 Setzt alle Balken der Serie wieder auf die Ursprungsfarbe 
    procedure ClearRankingColor(aSeries: TChartSeries);
    //1 Erzeugt alle benotwendigten Serien in diesem Frame 
    procedure CreateSeries;
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Zeigt ein Detailierungsgrad an 
    procedure SetHighlightSerie(aType: TOverviewAlarmFilterType; aFilter: string; aColor: TColor = clBlack);
    //1 Zeigt den Frame an und ermittelt die Daten f�r die �bersicht 
    procedure ShowOverview;
    //1 Setzt das Flag mInitialized zur�ck, damit die �bersicht neu berechnet werden kann 
    procedure UnInitialize;
    //1 Wird ausgel�st, wenn im Histogramm auf einen Seriepunkt geklickt wird 
    property OnOverviewChartClick: TOverviewChartClickEvent read FOnOverviewChartClick write FOnOverviewChartClick;
  end;
  

implementation
uses
  AdoDBAccess, mmcs, QOGUIShared, QODef;

resourcestring
// START resource string wizard section
  rsAlleAlarme = '(*)Alle Alarme';//ivlm
  rsSelektierteSchichten = '(*)Selektierte Schichten';//ivlm

// END resource string wizard section

  rsOther           = '(*)Andere'; //ivlm
  rsHistogramm      = '(*)Histogramm'; //ivlm
  rsSelected        = '(*)Ausgewaehlt:'; //ivlm
  rsSelectedAlarm   = '(*)Selektierter Alarm (%s)'; //ivlm
  rsSelectedMachine = '(*)Selektierte Maschine (%s)'; //ivlm

var
  lAlarmSerie:integer;
  lRankingSerie: integer;
  lAlarmRankingSerie:integer;
  lMachineRankingSerie: integer;

{$R *.DFM}

//:-------------------------------------------------------------------
(*: Member:           AddXY
 *  Klasse:           TOverviewBarSeries
 *  Kategorie:        No category 
 *  Argumente:        (AOverviewDataRec, ALabel, AColor)
 *
 *  Kurzbeschreibung: F�gt ein Punktepaar zur Serie hinzu
 *  Beschreibung:     
                      Als erster Parameter wird ein typisiertetr Pointer mit zusatzdaten zum Punktepaar
                      �bergeben. Der Pointer kann �ber das Array 'Items[n]' abgefragt werden.
 --------------------------------------------------------------------*)
function TOverviewBarSeries.AddXY(AOverviewDataRec: POverviewDataRec; const ALabel: String; AColor: TColor): LongInt;
begin
  result := AddXY(AOverviewDataRec^.Date, AOverviewDataRec^.Value, ALabel, AColor, AOverviewDataRec);
end;// TOverviewBarSeries.AddXY cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmAnalyzeOverviewFrame
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmAnalyzeOverviewFrame.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

//: ----------------------------------------------
  if not(assigned(mOverviewSeries)) then
    mOverviewSeries := TSeriesList.Create;

//: ----------------------------------------------
  // Untere Achse des Chart definieren
  mHistogramChart.BottomAxis.DateTimeFormat := Format('%s %s', [ShortDateFormat, ShortTimeFormat]);
  mHistogramChart.BottomAxis.Increment := DateTimeStep[dtSixHours];
  mHistogramChart.BottomAxis.LabelsMultiLine := True;
end;// TfrmAnalyzeOverviewFrame.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmAnalyzeOverviewFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmAnalyzeOverviewFrame.Destroy;
begin
  mOverviewSeries.Clear;
  FreeAndNil(mOverviewSeries);

//: ----------------------------------------------
  inherited Destroy;
end;// TfrmAnalyzeOverviewFrame.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           ClearRankingColor
 *  Klasse:           TfrmAnalyzeOverviewFrame
 *  Kategorie:        No category 
 *  Argumente:        (aSeries)
 *
 *  Kurzbeschreibung: Setzt alle Balken der Serie wieder auf die Ursprungsfarbe
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeOverviewFrame.ClearRankingColor(aSeries: TChartSeries);
var
  i: Integer;
begin
  for i := 0 to aSeries.Count - 1 do
    aSeries.ValueColor[i] := aSeries.SeriesColor;
end;// TfrmAnalyzeOverviewFrame.ClearRankingColor cat:No category

//:-------------------------------------------------------------------
(*: Member:           CreateSeries
 *  Klasse:           TfrmAnalyzeOverviewFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Erzeugt alle benotwendigten Serien in diesem Frame
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeOverviewFrame.CreateSeries;
  
  function AddBarSerie(aColor: TColor): Integer;
  begin
    result := mOverviewSeries.Add(TOverviewBarSeries.Create(self));
    with TOverviewBarSeries(mOverviewSeries[result]) do begin
      Marks.Visible := false;
      BorderWidth := 0;
      ParentChart := mHistogramChart;
      XValues.DateTime := true;
  
      BarPen.Color := aColor;
      SeriesColor := aColor;
    end;// with TOverviewBarSeries(mOverviewBarSeries[result]) do begin
  end;// function AddSerie(aColor: TColor): Integer;
  
  function AddHorBarSerie(aColor: TColor; aChart: TChart): Integer;
  begin
    result := mOverviewSeries.Add(TQOHorizBarSeries.Create(self));
    with TQOHorizBarSeries(mOverviewSeries[result]) do begin
      Marks.Visible := false;
      BorderWidth := 0;
      ParentChart := aChart;
  
      BarPen.Color := aColor;
      SeriesColor := aColor;
      ItemOwned := false;
    end;// with TQOHorizBarSeries(mOverviewBarSeries[result]) do begin
  end;// procedure AddHorBarSerie(aColor: TColor);
  
begin
  mOverviewSeries.Clear;
  
  // Eigentliche Serie
  lAlarmSerie := AddBarSerie(clRed);
  with TOverviewBarSeries(mOverviewSeries[lAlarmSerie]) do begin
    ShowInLegend := true;
    Title := Translate(rsAlleAlarme);
  end;// with TOverviewBarSeries(mOverviewSeries[lAlarmSerie]) do begin
  
  // Ranking Serie
  lRankingSerie := AddBarSerie(cSerieHighlightColor);
  with TOverviewBarSeries(mOverviewSeries[lRankingSerie]) do
    ShowInLegend := false;
  
  // Alarm Ranking
  lAlarmRankingSerie := AddHorBarSerie(cAlarmRankingColor, mRankingAlarmChart);
  with TQOHorizBarSeries(mOverviewSeries[lAlarmRankingSerie]) do
    ShowInLegend := false;
  
  // Maschine Ranking
  lMachineRankingSerie := AddHorBarSerie(cMachineRankingColor, mRankingMachineChart);
  with TQOHorizBarSeries(mOverviewSeries[lMachineRankingSerie]) do
    ShowInLegend := false;
end;// TfrmAnalyzeOverviewFrame.CreateSeries cat:No category

//:-------------------------------------------------------------------
(*: Member:           FrameResize
 *  Klasse:           TfrmAnalyzeOverviewFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Passt die Gr�sse der Charts an, damit beide Charts gleich gross sind
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeOverviewFrame.FrameResize(Sender: TObject);
begin
  mRankingAlarmChart.Width := (paRanking.ClientWidth - mSpacerGraph.Width) div 2
end;// TfrmAnalyzeOverviewFrame.FrameResize cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetSerieClickable
 *  Klasse:           TfrmAnalyzeOverviewFrame
 *  Kategorie:        No category 
 *  Argumente:        (aSerie, aPointIndex)
 *
 *  Kurzbeschreibung: Pr�ft eine Serie ob ein entsprechender Punkt Klickbar ist
 *  Beschreibung:     
                      Beim Klicken auf einen Balken, kann je nach Chart eine Aktion
                      erfolgen.
 --------------------------------------------------------------------*)
function TfrmAnalyzeOverviewFrame.GetSerieClickable(aSerie: TChartSeries; aPointIndex: integer): Boolean;
begin
  result := true;
  (* bei den beiden Ranking Charts wird der Pointer als boolean Wert missbraucht um
     festzustellen ob auf den Datenpunkt geklickt werden darf *)
  try
    if (aSerie is TQOHorizBarSeries) then
      result := boolean(TQOHorizBarSeries(aSerie).Items[aPointIndex])
  except
    result := false;
  end;// try except
end;// TfrmAnalyzeOverviewFrame.GetSerieClickable cat:No category

//:-------------------------------------------------------------------
(*: Member:           mChartMouseMove
 *  Klasse:           TfrmAnalyzeOverviewFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Shift, X, Y)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn die Maus �ber das Chart bewegt wird
 *  Beschreibung:     
                      Mithilfe dieses Events kann bestimmt werden, ob der Mauszeiger
                      �ber einem Balken ist und, wenn dieser klickbar ist, den Mauszeiger
                      als Finger darstellt.
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeOverviewFrame.mChartMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  xChart: TmmChart;
  i: Integer;
  xFound: Boolean;
  xIndex: Integer;
  xClickable: Boolean;
begin
  // Initialisieren
  xFound := false;
  xClickable := false;

//: ----------------------------------------------
  // Feststellen, ob auf eine Serie gecklickt wurde
  if (Sender is TmmChart) then begin
    xChart := TmmChart(Sender);
    i := 0;
    // Durch alle Serien durchgehn um festzustellen ob der Punkt zu einer Serie geh�rt
    while (i < xChart.SeriesCount) and (not(xFound)) do begin
      // Stellt den Index des geklickten Datenpunktes fest
      xIndex := xChart.Series[i].GetCursorValueIndex;
      // Ein Index gr�sser -1 heisst, dass der gecklickte Punkt zur entsprechenden Serie geh�rt
      xFound := (xIndex > -1);
      // Jetzt noch feststellen ob der Punkt angeklickt werden darf...
      if (xFound) then
        xClickable := GetSerieClickable(xChart.Series[i], xIndex);
      // Und die n�chste Serie pr�fen
      inc(i);
    end;// while (i < xChart.SeriesCount) and (not(xFound)) do begin
  end;// if (Sender is TmmChart) then begin

//: ----------------------------------------------
  // Wenn auf eine Serie geklickt wurde und ein Klck erlaubt ist, dann Cursor �ndern
  if (xFound) and (xClickable) then
    Screen.Cursor := crHandPoint
  else
    Screen.Cursor := crDefault;
end;// TfrmAnalyzeOverviewFrame.mChartMouseMove cat:No category

//:-------------------------------------------------------------------
(*: Member:           mHistogramChartClickSeries
 *  Klasse:           TfrmAnalyzeOverviewFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Series, ValueIndex, Button, Shift, X, Y)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn im Histogramm ein Alarm angeklickt wurde
 *  Beschreibung:     
                      Im Hauptformular wird versucht auf die entsprechende Schicht zu springen.
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeOverviewFrame.mHistogramChartClickSeries(Sender: TCustomChart; Series: TChartSeries; ValueIndex: 
  Integer; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if assigned(FOnOverviewChartClick) then
    FOnOverviewChartClick(Series.XValues[ValueIndex], Button, Shift);
end;// TfrmAnalyzeOverviewFrame.mHistogramChartClickSeries cat:No category

//:-------------------------------------------------------------------
(*: Member:           mmTranslator1AfterTranslate
 *  Klasse:           TfrmAnalyzeOverviewFrame
 *  Kategorie:        No category 
 *  Argumente:        (translator)
 *
 *  Kurzbeschreibung: �bernimmt alle �bersetungen die nicht automatisch "passieren"
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeOverviewFrame.mmTranslator1AfterTranslate(translator: TIvCustomTranslator);
begin
  mRankingAlarmChart.Title.Text.Text := rsAlarmRanking;
  mRankingMachineChart.Title.Text.Text := rsMachRanking;
  SetHistogramTitle;
end;// TfrmAnalyzeOverviewFrame.mmTranslator1AfterTranslate cat:No category

//:-------------------------------------------------------------------
(*: Member:           mRankingChartClickSeries
 *  Klasse:           TfrmAnalyzeOverviewFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Series, ValueIndex, Button, Shift, X, Y)
 *
 *  Kurzbeschreibung: Wird ausgel�st, wenn im Alarm Ranking Chart auf einen Datenpunkt (Balken) geklickt wird
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeOverviewFrame.mRankingChartClickSeries(Sender: TCustomChart; Series: TChartSeries; ValueIndex: 
  Integer; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  xType: TOverviewAlarmFilterType;
begin
  if GetSerieClickable(Series, ValueIndex) then begin
    if Sender = mRankingAlarmChart then
      xType := aftAlarm
    else
      xType := aftMachine;
    SetHighlightSerie(xType, Series.XLabel[ValueIndex], cSerieHighlightColor);
  
    ClearSerieColor(Series);
    Series.ValueColor[ValueIndex] := cSerieHighlightColor;
  end else begin
    SetHighlightSerie(aftNone, '');
  end;// if GetSerieClickable(Series, ValueIndex) then begin
end;// TfrmAnalyzeOverviewFrame.mRankingChartClickSeries cat:No category

//:-------------------------------------------------------------------
(*: Member:           RankingClickBackgraound
 *  Klasse:           TfrmAnalyzeOverviewFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Button, Shift, X, Y)
 *
 *  Kurzbeschreibung: Wird ausgel�st, wenn auf den Hintergrund eines Charts geklickt wird
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeOverviewFrame.RankingClickBackgraound(Sender: TCustomChart; Button: TMouseButton; Shift: 
  TShiftState; X, Y: Integer);
begin
  SetHighlightSerie(aftNone, '');
end;// TfrmAnalyzeOverviewFrame.RankingClickBackgraound cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetHighlightSerie
 *  Klasse:           TfrmAnalyzeOverviewFrame
 *  Kategorie:        No category 
 *  Argumente:        (aType, aFilter, aColor)
 *
 *  Kurzbeschreibung: Zeigt ein Detailierungsgrad an
 *  Beschreibung:     
                      Detaillierungsgrade sind Alarme, Schichten und Maschinen. Z.b. kann das Auf-
                      treten eines einzelnen Alarms visualisiert werden.
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeOverviewFrame.SetHighlightSerie(aType: TOverviewAlarmFilterType; aFilter: string; aColor: TColor = 
  clBlack);
var
  i: Integer;
  xAlarmSerie: TOverviewBarSeries;
  xFilterSerie: TOverviewBarSeries;
  xDateIndex: Integer;
  
  const
    cGetAlarms = 'SELECT  c_caught_alarm_id, c_QO_Alarm_Name ' +
                 'FROM    t_QO_Alarm_Data ' +
                 'WHERE   c_QO_Alarm_Name in (''%s'')';
  
    cGetMachine = 'SELECT ad.c_caught_alarm_id, ad.c_QO_alarm_data_id, im.c_machine_name FROM t_QO_Alarm_Data ad, t_QO_Involved_Machine im ' +
                  'WHERE  ad.c_QO_alarm_data_id = im.c_QO_alarm_data_id ' +
                  '   AND c_machine_name in (''%s'')';
  
begin
  // Substituierte Variablen
  xAlarmSerie := TOverviewBarSeries(mOverviewSeries[lAlarmSerie]);
  xFilterSerie := TOverviewBarSeries(mOverviewSeries[lRankingSerie]);
  
  // Je nachdem was gefiltert werden soll andere Behandlung
  case aType of
    // Filter entfernen
    aftNone: begin
      // Serie aus der Legende  ...
      xFilterSerie.ShowInLegend := false;
      // .. und dann die Daten l�schen
      xFilterSerie.Clear;
      // Default-Farben wiederherstellen
      ClearSerieColor(mOverviewSeries[lAlarmRankingSerie]);
      ClearSerieColor(mOverviewSeries[lMachineRankingSerie]);
    end;// aftNone: begin
  
    // Ein Alarm soll herausgehoben werden
    aftAlarm: begin
      // Zuerst die evt. vorhandene Einf�rbung eines vorherigen Filters l�schen
      ClearSerieColor(mOverviewSeries[lMachineRankingSerie]);
      // Daten von der Datenbank holen
      with TAdoDBAccess.Create(1) do try
        Init;
        with Query[0] do begin
          // Holt alle Alarme mit dem selben Namen
          SQL.Text := Format(cGetAlarms,[PrepareStringForSQL(aFilter, false)]);
          open;
  
          // Initialisieren
          xFilterSerie.Clear;
          xFilterSerie.BarPen.Color := aColor;
          xFilterSerie.SeriesColor := aColor;
  
          // Neue Serie betiteln
          xFilterSerie.Title := Format(Translate(rsSelectedAlarm), [aFilter]);
          // Wieder in der Legende Anzeigen
          xFilterSerie.ShowInLegend := true;
          // Durch die Alarmdaten hindurchgehen und die Daten der Alarme mit den Daten des selektierten Alarms vergleichen
          for i:= 0 to xAlarmSerie.Count - 1 do begin
            (* Da in jedem Datenpunkt auch die AlarmDataID enthalten ist, kann einfach diese ID (eindeutig) verglichen werden. *)
            Filter('c_caught_alarm_id = ' + IntToStr(POverviewDataRec(xAlarmSerie.Items[i])^.AlarmDataID));
            FindFirst;
            // Wenn der Alarm �bereinstimmt, diesen Datenpunkt in die selektierte Serie kopieren.
            if not(EOF) then
              xFilterSerie.AddXY(xAlarmSerie.XValues[i], POverviewDataRec(xAlarmSerie.Items[i])^.Value);
          end;// for i:= 0 to xAlarmSerie.Count - 1 do begin
        end;// with Query[0] do begin
      finally
        Free;
      end;// with TAdoDBAccess.Create(1) do try
    end;// aftAlarm: begin
  
    aftMachine: begin
      ClearSerieColor(mOverviewSeries[lAlarmRankingSerie]);
      with TAdoDBAccess.Create(1) do try
        Init;
        with Query[0] do begin
          SQL.Text := Format(cGetMachine,[StringReplace(StringReplace(aFilter, '''', '''''', [rfReplaceAll]), ',', '''', [rfReplaceAll])]);
          open;
          xFilterSerie.Clear;
          xFilterSerie.BarPen.Color := aColor;
          xFilterSerie.SeriesColor := aColor;
          xFilterSerie.Title := Format(Translate(rsSelectedMachine), [aFilter]);
          xFilterSerie.ShowInLegend := true;
          for i:= 0 to xAlarmSerie.Count - 1 do begin
            Filter('c_caught_alarm_id = ' + IntToStr(POverviewDataRec(xAlarmSerie.Items[i])^.AlarmDataID));
            FindFirst;
            if not(EOF) then
              xFilterSerie.AddXY(xAlarmSerie.XValues[i], POverviewDataRec(xAlarmSerie.Items[i])^.Value);
          end;//
        end;// with Query[0] do begin
      finally
        Free;
      end;// with TAdoDBAccess.Create(1) do try
    end;// aftMachine: begin
  
    aftShift: begin
      ClearSerieColor(mOverviewSeries[lAlarmRankingSerie]);
      ClearSerieColor(mOverviewSeries[lMachineRankingSerie]);
      xFilterSerie.Clear;
      xFilterSerie.BarPen.Color := aColor;
      xFilterSerie.SeriesColor := aColor;
      xFilterSerie.Title := Translate(rsSelektierteSchichten);
      xFilterSerie.ShowInLegend := true;
      with TStringList.Create do try
        CommaText := aFilter;
        for i:= 0 to xAlarmSerie.Count - 1 do begin
          xDateIndex := IndexOf(FloatToStr(xAlarmSerie.XValues[i]));
          if xDateIndex >= 0 then
            xFilterSerie.AddXY(xAlarmSerie.XValues[i], POverviewDataRec(xAlarmSerie.Items[i])^.Value);
        end;// for i:= 0 to xAlarmSerie.Count - 1 do begin
      finally
        Free;
      end;// with TStringList.Create do try
    end;// aftShift: begin
  end;// case aType of
  
  // Anzeigen wieviele Alarme selektiert sind
  SetHistogramTitle;
end;// TfrmAnalyzeOverviewFrame.SetHighlightSerie cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetHistogramTitle
 *  Klasse:           TfrmAnalyzeOverviewFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Aktualisiert den Titel des Histogramms (wird nicht automatishc �bersetzt)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeOverviewFrame.SetHistogramTitle;
var
  xFilterSerie: TChartSeries;
begin
  xFilterSerie := nil;
  
  if assigned(mOverviewSeries) then
    if mOverviewSeries.SerieCount > lRankingSerie then
      xFilterSerie := mOverviewSeries[lRankingSerie];
  
  // Anzeigen wieviele Alarme selektiert sind
  if assigned(xFilterSerie) then begin
    if (xFilterSerie.Count = 0) then
      mHistogramChart.Title.Text.Text := rsHistogramm;
  (*  else
      mHistogramChart.Title.Text.Text := rsHistogramm + ' - ' + rsSelected + '' + IntToStr(xFilterSerie.Count);*)
  end else begin
    mHistogramChart.Title.Text.Text := rsHistogramm
  end;// if assigned(xFilterSerie) then begin
end;// TfrmAnalyzeOverviewFrame.SetHistogramTitle cat:No category

//:-------------------------------------------------------------------
(*: Member:           ShowOverview
 *  Klasse:           TfrmAnalyzeOverviewFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zeigt den Frame an und ermittelt die Daten f�r die �bersicht
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeOverviewFrame.ShowOverview;
var
  xRankingCount: Integer;
  xOverviewDataRec: POverviewDataRec;
  xAlarmsFound: Boolean;
  
  const
    cGetMachineCountByShift = 'SELECT    ca.c_caught_alarm_id, ca.c_date as c_date, Count(m.c_machine_id) as c_machine_count ' +
                              'FROM      t_QO_Involved_Machine m, t_QO_Caught_alarm ca, t_QO_alarm_data ad ' +
                              'WHERE     m.c_QO_Alarm_data_id = ad.c_QO_Alarm_data_id ' +
                              '      AND ad.c_caught_alarm_id = ca.c_caught_alarm_id ' +
                              '      AND ca.c_shiftcal_id = :c_shiftcal_id ' +
                              'GROUP BY  ca.c_caught_alarm_id, ca.c_date';
  
    cGetAlarmCharts   = 'SELECT   TOP %d Count(*) as #, c_QO_Alarm_name as Name ' +
                        'FROM     t_QO_Alarm_Data ' +
                        'Group By c_QO_Alarm_name ' +
                        'ORDER BY # DESC';
  
    cGetOtherAlarmCount =  'SELECT (SELECT COUNT(*) FROM t_QO_Alarm_Data) - SUM(#) FROM (' + cGetAlarmCharts + ') as x';
  
    cGetMachineCharts = 'SELECT   TOP %d Count(*) as #, m.c_machine_name as Name ' +
                        'FROM     t_QO_Involved_Machine im, t_machine m  ' +
                        'WHERE    m.c_machine_id = im.c_machine_id ' +
                        'Group By m.c_machine_id, m.c_machine_name ' +
                        'ORDER BY # DESC';
  
    cGetOtherMachCount =  'SELECT (SELECT Count(*) FROM t_QO_Involved_Machine) - SUM(#) FROM (' + cGetMachineCharts + ') as x';
  
    //  cGetOtherAlarmCount =  'SELECT (SELECT COUNT(*) FROM t_QO_Alarm_Data) - SUM(#) FROM (' + cGetAlarmCharts + ') as x'
  
begin
  // Zoomfaktor wieder zur�ckstellen
  mRankingAlarmChart.UndoZoom;
  mRankingMachineChart.UndoZoom;
  mHistogramChart.UndoZoom;
  
  (* Anzahl Eintrage in den Ranking Charts erfragen.
     Zus�tzlich wird jeweils ein Eintrag f�r alle anderen Alarme/Maschinen
     hinzugef�gt (Ausser es gibt keine anderen Alarme/Maschinen) *)
  xRankingCount := TQOAppSettings.Instance.AnalyzeRankingCount;
  
  //Nur einmal Initialisiern, da auch die einzelnen Schichten diesen Frame verwenden
  if not(mInitialized) then begin
    xAlarmsFound := false;
    // Zuerst einmal alle Serien dieses Frames erzeugen
    CreateSeries;
    // Die verschiedenen Daten abfragen
    with TAdoDBAccess.Create(1) do try
      Init;
      with Query[0] do begin

//: ----------------------------------------------
    // Histogramm
        (* Holt die Anzahl der Maschinen die w�hrend einer gemeinsamen Schicht (Alarmdatum)
           an einen Alarm beteiligt waren. Dabei werden Maschinen die an zwei Alarmen
           beteiligt sind auch doppelt gez�hlt *)
        SQL.Text := cGetMachineCountByShift;
        ParamByName('c_shiftcal_id').AsInteger := TQOAppSettings.Instance.ShiftCal;
        open;
  
        while not(EOF) do begin
          xAlarmsFound := true;
          // Neuer Datenrecord f�r den neuen Punkt erzeugen (wird von der Serie freigegeben)
          new(xOverviewDataRec);
          // Record abf�llen
          xOverviewDataRec^.AlarmDataID := FieldByName('c_caught_alarm_id').AsInteger;
          xOverviewDataRec^.Value := FieldByName('c_machine_count').AsInteger;
          xOverviewDataRec^.Date := FieldByName('c_date').AsDateTime;
  
          // Punkt hinzuf�gen
          TOverviewBarSeries(mOverviewSeries[lAlarmSerie]).AddXY(xOverviewDataRec, '', clTeeColor);
          next;
        end;// while not(EOF) do begin
  
        // Maximum + 1 damit ein gewisser header zu sehen ist
        mHistogramChart.LeftAxis.SetMinMax(0, TOverviewBarSeries(mOverviewSeries[lAlarmSerie]).MaxYValue + 1);

//: ----------------------------------------------
    // Ranking der Alarme
        TQOHorizBarSeries(mOverviewSeries[lAlarmRankingSerie]).Clear;
        // Die "Anderen" Alarme ermitteln
        SQL.Text := Format(cGetOtherAlarmCount, [xRankingCount]);
        open;
        // Wenn noch andere Alarme existieren, dann einen Datenpunkt eintragen
        if not(EOF) then
          if Fields[0].AsFloat > 0 then
            // Der letzte Parameter beschreibt ob der Datenpunkt angeklickt werden darf
            TQOHorizBarSeries(mOverviewSeries[lAlarmRankingSerie]).Add(Fields[0].AsFloat, rsOther, pointer(false));
  
        // Die h�ufigsten Alarme ermitteln
        SQL.Text := Format(cGetAlarmCharts, [xRankingCount]);
        open;
        // Umsortieren, damit der H�ufigste Alarm oben ist
        Sort('# ASC');
        FindFirst;
        // F�r jeden Datensatz einen Punkt hinzuf�gen
        while (not(EOF)) do begin
          // Der letzte Parameter beschreibt ob der Datenpunkt angeklickt werden darf
          TQOHorizBarSeries(mOverviewSeries[lAlarmRankingSerie]).Add(FieldByName('#').AsFloat, FieldByName('Name').AsString, pointer(true));
          next;
        end;// while (not(EOF)) do begin
        close;

//: ----------------------------------------------
    // Ranking der Maschinen
        TQOHorizBarSeries(mOverviewSeries[lMachineRankingSerie]).Clear;
        // Die "Anderen" Maschinen ermitteln
        SQL.Text := Format(cGetOtherMachCount, [xRankingCount]);
        open;
        // Wenn noch andere Maschinen existieren, dann einen Datenpunkt eintragen
        if not(EOF) then
          if Fields[0].AsFloat > 0 then
            // Der letzte Parameter beschreibt ob der Datenpunkt angeklickt werden darf
            TQOHorizBarSeries(mOverviewSeries[lMachineRankingSerie]).Add(Fields[0].AsFloat, rsOther, pointer(false));
  
        // Die h�ufigsten Maschinen ermitteln
        SQL.Text := Format(cGetMachineCharts, [xRankingCount]);
        open;
        // Umsortieren, damit die H�ufigste Alarm oben ist
        Sort('# ASC');
        FindFirst;
        // F�r jeden Datensatz einen Punkt hinzuf�gen
        while (not(EOF)) do begin
          // Der letzte Parameter beschreibt ob der Datenpunkt angeklickt werden darf
          TQOHorizBarSeries(mOverviewSeries[lMachineRankingSerie]).Add(FieldByName('#').AsFloat, FieldByName('Name').AsString, pointer(true));
          next;
        end;// while (not(EOF)) do begin
        close;

//: ----------------------------------------------
      end;// with Query[0] do begin
      // Wenn alles durchgelaufen ist, dann ist die Initialisierung OK
      mInitialized := true;
    finally
      Free;
      if xAlarmsFound then begin
        mHistogramChart.visible := true;
        mRankingAlarmChart.visible := true;
        mSpacerGraph.visible := true;
        mRankingMachineChart.visible := true;
      end else begin
        mHistogramChart.visible := false;
        mRankingAlarmChart.visible := false;
        mRankingMachineChart.visible := false;
        mSpacerGraph.visible := false;
      end;// if xAlarmsFound then begin
    end;// with TAdoDBAccess.Create(1) do try
  end;// if not(mInitialized) then begin
end;// TfrmAnalyzeOverviewFrame.ShowOverview cat:No category

//:-------------------------------------------------------------------
(*: Member:           UnInitialize
 *  Klasse:           TfrmAnalyzeOverviewFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Setzt das Flag mInitialized zur�ck, damit die �bersicht neu berechnet werden kann
 *  Beschreibung:     
                      Diese Neuberechnung kann notweendig werden, wenn mauelle Alarme erzeugt wurden, 
                      oder ein Refresh durchgef�hrt wird.
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeOverviewFrame.UnInitialize;
begin
  mInitialized := false;
end;// TfrmAnalyzeOverviewFrame.UnInitialize cat:No category



end.

















