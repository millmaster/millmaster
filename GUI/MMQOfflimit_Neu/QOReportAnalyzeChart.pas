(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QOReportAnalyzeChart.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Bericht f�r die Chartansicht (Artikel/Maschine)
|
| Info..........: 
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 17.10.2003  1.00  Lok | Datei erstellt
| 18.06.2004  1.00  SDo | Layout Aenderung & Zuweisung in Create
|=========================================================================================*)
unit QOReportAnalyzeChart;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, TeeProcs, TeEngine, Chart, DBChart, QrTee, QuickRpt, mmQRChart,
  ExtCtrls, mmQuickRep, QOGUIClasses, Qrctrls, mmQRImage, mmQRSysData,
  mmQRLabel, IvDictio, IvMulti, IvEMulti, mmTranslator, QOGUIShared;

type
  (*: Klasse:        ChartObjRec
      Vorg�nger:     
      Kategorie:     record
      Kurzbeschrieb: Record f�r die Verwaltung der Statistikdaten f�r die Alarme im AnalyzeTree (�bersicht und 
      AlarmView) 
      Beschreibung:  
                     - *)
  ChartObjRec = record
    Alarm: TQOAlarmView;
    Machine: TQOAssignNodeObject;
    Style: TQOAssignNodeObject;
  end;
  
  PChartObjRec = ^ChartObjRec;

  (*: Klasse:        TfrmQOReportAnalyzeChart
      Vorg�nger:     TmmForm
      Kategorie:     No category
      Kurzbeschrieb: Bericht f�r den gesammten Artikel oder f�r eine einzelne Maschine eines Alarms 
      Beschreibung:  
                     - *)
  TfrmQOReportAnalyzeChart = class (TmmForm)
    mAlarmCaption: TmmQRLabel;
    mAlarmName: TmmQRLabel;
    mAnalyzeChart: TmmQRChart;
    mChartReport: TmmQuickRep;
    mmTranslator1: TmmTranslator;
    mRootBand: TQRBand;
    mShiftListTitle: TQRBand;
    mShiftStart: TmmQRLabel;
    mShiftStartCaption: TmmQRLabel;
    mStyleCaption: TmmQRLabel;
    mStyleName: TmmQRLabel;
    qlTitle: TmmQRLabel;
    QRDBChart1: TQRDBChart;
    qlCompanyName: TmmQRLabel;
    PageFooterBand1: TQRBand;
    mmQRImage1: TmmQRImage;
    qlPageValue: TmmQRSysData;
    mmQRSysData2: TmmQRSysData;
    //1 Druckmethode 
    procedure mChartReportNeedData(Sender: TObject; var MoreData: Boolean);
  private
    mAssignIndex: Integer;
    mAssignList: TList;
    mDataItemIndex: Integer;
    mDataServer: TAnalyzeChartDataServer;
    mMoreDataItems: Boolean;
    //1 Anzahl der zu druckenden Schichten 
    function GetAssignCount: Integer;
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 F�gt eine Schicht die gedruckt werden soll zur Liste hinzu 
    procedure AddAssign(aAlarm: TQOAlarmView; aStyleObject, aMachineObject: TQOAssignNodeObject);
    //1 L�scht alle Eintrage der zu druckenden Artikel/Maschinen 
    procedure ClearAssignList;
    //1 Druckt die Schichten aus die zuvor der Liste hinzugef�gt wurden (siehe AddShift()) 
    procedure Print(aWithSetup: boolean);
    //1 Anzahl der zu druckenden Schichten 
    property AssignCount: Integer read GetAssignCount;
  end;
  
var
  frmQOAnalyzeChartReport: TfrmQOReportAnalyzeChart;

implementation

uses printers, QRPrntr, SettingsReader;


{$R *.DFM}

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmQOReportAnalyzeChart
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmQOReportAnalyzeChart.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

//: ----------------------------------------------
  mAssignList := TList.Create;

  try
    qlCompanyName.Caption := TMMSettingsReader.Instance.Value[cCompanyName];
  except
    qlCompanyName.Caption :='';
  end;

  qlPageValue.Text := Translate( qlPageValue.Text ) + ' ';

end;// TfrmQOReportAnalyzeChart.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmQOReportAnalyzeChart
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmQOReportAnalyzeChart.Destroy;
begin
  FreeAndNil(mAssignList);
  FreeAndNil(mDataServer);

//: ----------------------------------------------
  inherited Destroy;
end;// TfrmQOReportAnalyzeChart.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddAssign
 *  Klasse:           TfrmQOReportAnalyzeChart
 *  Kategorie:        No category 
 *  Argumente:        (aAlarm, aStyleObject, aMachineObject)
 *
 *  Kurzbeschreibung: F�gt eine Schicht die gedruckt werden soll zur Liste hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOReportAnalyzeChart.AddAssign(aAlarm: TQOAlarmView; aStyleObject, aMachineObject: TQOAssignNodeObject);
var
  xChartObjRec: PChartObjRec;
begin
  // Assign Objekt zur Liste hinzuf�gen
  if assigned(mAssignList) then begin
    new(xChartObjRec);
  
    xChartObjRec.Alarm := aAlarm;
    xChartObjRec.Style := aStyleObject;
    xChartObjRec.Machine := aMachineObject;
  
    mAssignList.Add(xChartObjRec);
  end;// if assigned(mAssignList) then begin
end;// TfrmQOReportAnalyzeChart.AddAssign cat:No category

//:-------------------------------------------------------------------
(*: Member:           ClearAssignList
 *  Klasse:           TfrmQOReportAnalyzeChart
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�scht alle Eintrage der zu druckenden Artikel/Maschinen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOReportAnalyzeChart.ClearAssignList;
var
  i: Integer;
begin
  // Liste leeren
  if assigned(mAssignList) then begin
    // Speicherplatz f�r die Records freigeben
    for i := 0 to mAssignList.Count - 1 do
      Dispose(mAssignList[i]);
  
    mAssignList.Clear;
  end;// if assigned(mAssignList) then begin
end;// TfrmQOReportAnalyzeChart.ClearAssignList cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAssignCount
 *  Klasse:           TfrmQOReportAnalyzeChart
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Anzahl der zu druckenden Schichten
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmQOReportAnalyzeChart.GetAssignCount: Integer;
begin
  result := 0;
  if assigned(mAssignList) then
    result := mAssignList.Count;
end;// TfrmQOReportAnalyzeChart.GetAssignCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           mChartReportNeedData
 *  Klasse:           TfrmQOReportAnalyzeChart
 *  Kategorie:        No category 
 *  Argumente:        (Sender, MoreData)
 *
 *  Kurzbeschreibung: Druckmethode
 *  Beschreibung:     
                      Diese Methode wird f�r jedes zu Druckende Chart aufgerufen. Die Verwaltung ob noch
                      mehr Charts zu Drucken sind ist in der Methode selbst implementiert (MoreData).
 --------------------------------------------------------------------*)
procedure TfrmQOReportAnalyzeChart.mChartReportNeedData(Sender: TObject; var MoreData: Boolean);
  
  const
    cCaptionNameSpace = 4;
    cItemSpace = 20;
  
begin
  // Vom letzten mal freigeben;
  FreeAndNil(mDataServer);
  
  if mMoreDataItems then begin
    MoreData := (mAssignIndex < AssignCount);
  end else begin
    inc(mAssignIndex);
    MoreData := (mAssignIndex < AssignCount);
    mMoreDataItems := true;
    mDataItemIndex := -1;
  end;// if mMoreDataItems then begin
  
  if MoreData then begin
    mDataServer := TAnalyzeChartDataServer.Create(self);
    try
      with mDataServer do begin
        Style := PChartObjRec(mAssignList[mAssignIndex])^.Style;
        // Aktuell selektierte Maschine
        Machine := PChartObjRec(mAssignList[mAssignIndex])^.Machine;
        if assigned(Machine) then begin
          // Chart ist im Maschinen Modus
          AssignShowMode := smMachine;
          Initialize(PChartObjRec(mAssignList[mAssignIndex])^.Alarm, IntToStr(Machine.LinkID));
        end else begin
          // Chart ist im Artikel Modus
          AssignShowMode := smStyle;
          // Initialisiert die Anzeige und ruft 'mDataItemTabsChange' auf
          Initialize(PChartObjRec(mAssignList[mAssignIndex])^.Alarm, '');
        end;// if assigned(Machine) then begin
  
        inc(mDataItemIndex);
        if mDataItemIndex < DataItemList.Count then begin
          ShowDataItem(DataItemList[mDataItemIndex], mAnalyzeChart.Chart);
  
          mAlarmName.Caption := PChartObjRec(mAssignList[mAssignIndex])^.Alarm.QOVisualize.Caption;
          mStyleName.Caption := PChartObjRec(mAssignList[mAssignIndex])^.Style.QOVisualize.Caption;
          mShiftStart.Caption := PChartObjRec(mAssignList[mAssignIndex])^.Alarm.GetShiftStart;
  
          mAlarmName.Left := mAlarmCaption.Left + mAlarmCaption.Width + cCaptionNameSpace;
          mShiftStartCaption.Left := mAlarmName.Left + mAlarmName.Width + cItemSpace;
          mShiftStart.Left := mShiftStartCaption.Left + mShiftStartCaption.Width + cCaptionNameSpace + 2;
          mStyleCaption.Left := mShiftStart.Left + mShiftStart.Width + cItemSpace;
          mStyleName.Left := mStyleCaption.Left + mStyleCaption.Width + cCaptionNameSpace;
  
        end;// if mDataItemIndex < DataItemList.Count then begin
        mMoreDataItems := (mDataItemIndex < (DataItemList.Count -1));
      end;// with DataServer do begin
    except
      FreeAndNil(mDataServer);
    end;// try except
  end;// if MoreData then begin
end;// TfrmQOReportAnalyzeChart.mChartReportNeedData cat:No category

//:-------------------------------------------------------------------
(*: Member:           Print
 *  Klasse:           TfrmQOReportAnalyzeChart
 *  Kategorie:        No category 
 *  Argumente:        (aWithSetup)
 *
 *  Kurzbeschreibung: Druckt die Schichten aus die zuvor der Liste hinzugef�gt wurden (siehe AddShift())
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOReportAnalyzeChart.Print(aWithSetup: boolean);
begin
  (*Printer.Orientation := poPortrait;
  QRPrinter.Orientation := poPortrait;
  mChartReport.Page.Orientation := poPortrait;*)
  
  mAssignIndex := -1;
  mDataItemIndex := -1;
  // Wenn ein Chart gedruckt wird ist auch am Anfang mindestens ein DataItem vorhanden
  mMoreDataItems := false;
  
  // Das Sammeln der Daten kann unter Umstanden (viele Schichten) l�nger dauern
  try
    Screen.Cursor := crHourGlass;
    GetAsyncKeyState(VK_ESCAPE);
    PrintQRWithSetupDialog(mChartReport, aWithSetup, poPortrait);
  finally
    Screen.Cursor := crDefault;
  end;// finally
end;// TfrmQOReportAnalyzeChart.Print cat:No category

end.





