(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QOGroup.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Objekte die f�r das Level der Gruppierung stehen (Artikel, Maschinen)
|
| Info..........: 
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.04.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit QOGroup;

interface

uses
  AdoDBAccess,
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, QOKernel;

type
  (*: Klasse:        TQOStyle
      Vorg�nger:     TQOAssign
      Kategorie:     Kernel
      Kurzbeschrieb: Einzelner Artikel 
      Beschreibung:  
                     - *)
  TQOStyle = class (TQOAssign)
  protected
    //1 Fragt den ImageIndex f�r die Anzeige ab 
    procedure GetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer); virtual;
    //1 Fragt nach ob der Artikel in Produktion ist 
    function GetInProduction: Boolean; override;
  public
    //1 Konstruktor 
    constructor Create; override;
    //1 Eigenschaften �bernehmen 
    procedure Assign(Source: TPersistent); override;
    //1 L�scht den Datensatz von der Datenbank 
    function DeleteFromDB(aQuery: TNativeAdoQuery): Boolean; override;
    //1 Speichert die Settings des DataItems auf der Datenbank 
    function SaveDefToDB: Boolean; override;
  end;
  
  (*: Klasse:        TQOMachine
      Vorg�nger:     TQOAssign
      Kategorie:     Kernel
      Kurzbeschrieb: Zuordnungseinheit Maschine 
      Beschreibung:  
                     In dieser Klasse werden die Daten gehlaten dei der QOfflimit
                     von einer Maschine ben�tigt. *)
  TQOMachine = class (TQOAssign)
  private
    FAssociatedStyles: String;
  protected
    //1 Fragt bei der Anwendung nach dem ImageIndex 
    procedure GetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer);
    //1 Fragt nach ob die Maschine in Produktion ist 
    function GetInProduction: Boolean; override;
  public
    //1 Konnstruktor 
    constructor Create; override;
    //1 Kopieren 
    procedure Assign(Source: TPersistent); override;
    //1 L�scht die maschine von der DB (nur QO Zuordnung) 
    function DeleteFromDB(aQuery: TNativeAdoQuery): Boolean; override;
    //1 Speichert die Daten zur Maschine auf der DB (QO Tabellen) 
    function SaveDefToDB: Boolean; override;
    //1 Eine Maschine kann mehreren Styles zugeh�rig sein, wenn festgestellt werden soll ob ein Style einer Gruppe auf dieser Maschine produziert 
    property AssociatedStyles: String read FAssociatedStyles write FAssociatedStyles;
  end;
  
  (*: Klasse:        TQOAllStyles
      Vorg�nger:     TQOStyle
      Kategorie:     Kernel
      Kurzbeschrieb: Repr�sentiert alle Artikel 
      Beschreibung:  
                     Dieser "Artikel" wird verwendet um alle Artikel die 
                     erfasst sind zu repr�sentieren. 
                     Einige Methoden erfordern hier eine andere
                     Vorgehensweise im Sammeln der Daten *)
  TQOAllStyles = class (TQOStyle)
  protected
    //1 Wird aufgerufen, wenn die Caption ge�ndert werden soll 
    procedure GetCaption(Sender: TObject; var aCaption :string);
    //1 Gibt die Zuordnung (LinkID) als Komma String zur�ck 
    function GetCommaTextByLinkID: String; override;
    //1 Fragt den ImageIndex f�r die Anzeige ab 
    procedure GetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer); override;
    //1 Fragt ab, ob �berhaupt irgend ein Artikel in Produktion ist 
    function GetInProduction: Boolean; override;
  public
    //1 Konstruktor 
    constructor Create; override;
    //1 Speichert das Objekt auf der DB 
    function SaveDefToDB: Boolean; override;
  end;
  
  
  TQOStyleClass = class of TQOStyle;

implementation

uses QODef;

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOStyle
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOStyle.Create;
begin
  inherited Create;

//: ----------------------------------------------
  // Interface Initialisieren
  FQOVisualize.OnGetImageIndex := GetImageIndex;
end;// TQOStyle.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TQOStyle
 *  Kategorie:        No category 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Eigenschaften �bernehmen
 *  Beschreibung:     
                      Dient zum kopieren von Objekten
 --------------------------------------------------------------------*)
procedure TQOStyle.Assign(Source: TPersistent);
begin
  if Source is TQOStyle then begin
    // Alle Eigenschaften kopieren
  end;// if Source is TQOStyle then begin

//: ----------------------------------------------
  // Weiterreichen
  inherited Assign(Source);
end;// TQOStyle.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           DeleteFromDB
 *  Klasse:           TQOStyle
 *  Kategorie:        No category 
 *  Argumente:        (aQuery)
 *
 *  Kurzbeschreibung: L�scht den Datensatz von der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOStyle.DeleteFromDB(aQuery: TNativeAdoQuery): Boolean;
  
  const
    cDeleteStyle = 'DELETE FROM t_QO_Style_Setting WHERE c_QOStyle_id = :c_QOStyle_id';
  
begin
  Result := inherited DeleteFromDB(aQuery);

//: ----------------------------------------------
  aQuery.SQL.Text := cDeleteStyle;
  aQuery.ParamByName('c_QOStyle_id').AsInteger := FID;
  
  // Datensatz gel�scht, wenn ein Datensatz betroffen ist
  result := (aQuery.ExecSQL = 1);
end;// TQOStyle.DeleteFromDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetImageIndex
 *  Klasse:           TQOStyle
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aImageKind, aImageIndex)
 *
 *  Kurzbeschreibung: Fragt den ImageIndex f�r die Anzeige ab
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOStyle.GetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer);
begin
  if InProduction then
    aImageIndex := TImageIndexHandler.Instance.Style
  else
    aImageIndex := TImageIndexHandler.Instance.StyleSW;
end;// TQOStyle.GetImageIndex cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetInProduction
 *  Klasse:           TQOStyle
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Fragt nach ob der Artikel in Produktion ist
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOStyle.GetInProduction: Boolean;
begin
  result := TActiveMachines.Instance.StyleInProd(FLinkID);
end;// TQOStyle.GetInProduction cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveDefToDB
 *  Klasse:           TQOStyle
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Speichert die Settings des DataItems auf der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOStyle.SaveDefToDB: Boolean;
var
  mDB: TAdoDBAccess;
  
  const
    cInsertStyle = 'INSERT INTO t_QO_Style_Setting ' +
                   '(c_QOStyle_id, c_QOAlarm_id, c_style_id) ' +
                   'VALUES (:c_QOStyle_id, :c_QOAlarm_id, :c_style_id)';
  
    cUpdateStyle = 'UPDATE t_QO_Style_Setting ' +
                   'SET c_QOAlarm_id = :c_QOAlarm_id , c_style_id = :c_style_id ' +
                   'WHERE c_QOStyle_id = :c_QOStyle_id';
  
    cDeleteStyle = 'DELETE t_QO_Style_Setting ' +
                   'WHERE c_QOStyle_id = :c_QOStyle_id';
  
  (*-----------------------------------------------------------
    Setzt die ADO Parameter
  -------------------------------------------------------------*)
  procedure SetParameter;
  begin
    mDB.Query[0].ParamByName('c_style_id').AsInteger := integer(LinkID);
    mDB.Query[0].ParamByName('c_QOAlarm_id').AsInteger := integer(ParentID);
  
    // Die ID wird nur f�r UPDATE ben�tigt
    if ID <> cINVALID_ID then
      mDB.Query[0].ParamByName('c_QOStyle_id').AsInteger := ID;
  end;// procedure SetParameter;
  
  (*-----------------------------------------------------------
    F�gt einen neuen Datensatz in die Tabelle ein
  -------------------------------------------------------------*)
  procedure InsertStyle;
  begin
    mDB.Query[0].SQL.Text := cInsertStyle;
    SetParameter;
    // Neuen Datensatz einf�gen und die ID merken
    ID := mDB.Query[0].InsertSQL('t_QO_Style_Setting', 'c_QOStyle_id', MAXINT, 1);
  end;// procedure InsertStyle;
  
  (*-----------------------------------------------------------
    Ver�ndert einen bestehenden Datensatz
  -------------------------------------------------------------*)
  procedure UpdateStyle;
  begin
    mDB.Query[0].SQL.Text := cUpdateStyle;
    SetParameter;
    // Datensatz Updaten
    if mDB.Query[0].ExecSQL = 0 then begin
      // Wenn kein Datensatz betroffen war, dann den Datensatz neu erzeugen
      ID := cINVALID_ID;
      InsertStyle;
    end;// if mDB.Query[0].ExecSQL = 0 then begin
  end;// procedure UpdateStyle;
  
  (*-----------------------------------------------------------
    L�scht einen bestehenden Datensatz
  -------------------------------------------------------------*)
  procedure DeleteStyle;
  begin
    mDB.Query[0].SQL.Text := cDeleteStyle;
    mDB.Query[0].ParamByName('c_QOStyle_id').AsInteger := ID;
  
    mDB.Query[0].ExecSQL;
    FDeleteState := dsDeleted;
  end;// procedure DeleteStyle;
  
begin
  result := false;
  
  mDB := TAdoDBAccess.Create(1);
  try
    mDB.Init;
  
    if FID = cINVALID_ID then begin
      if FDeleteState = dsValid then
        InsertStyle
      else
        FDeleteState := dsDeleted;
    end else begin
      if FDeleteState = dsPending then begin
        DeleteStyle;
      end else begin
        if FDeleteState = dsValid then
          UpdateStyle;
      end;// if FDeleteState = dsPending then begin
    end;// if FID = cINVALID_ID then begin
  
    // Untergeordnete Items speichern (zugeordnete Maschinen)
    if FID > cINVALID_ID then
      QOAssigns.SaveDefToDB(FID);
  
    // Erfolgreich
    result := true;
  finally
    FreeAndNil(mDB);
  end;// with TAdoDBAccess.Create(1) do try
end;// TQOStyle.SaveDefToDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOMachine
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konnstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOMachine.Create;
begin
  inherited Create;
  // Interface Initialisieren
  FQOVisualize.OnGetImageIndex := GetImageIndex;
end;// TQOMachine.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TQOMachine
 *  Kategorie:        No category 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Kopieren
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOMachine.Assign(Source: TPersistent);
begin
  if Source is TQOMachine then begin
    // Alle Eigenschaften kopieren
    (* !!! ACHTUNG !!!
       AssociatedStyles darf nicht kopiert werden, da es sich um eine tempor�re
       Eigenschaft handelt *)
    AssociatedStyles := '';
  end;// if Source is TQOMachine then begin
  // Weiterreichen
  inherited Assign(Source);
end;// TQOMachine.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           DeleteFromDB
 *  Klasse:           TQOMachine
 *  Kategorie:        No category 
 *  Argumente:        (aQuery)
 *
 *  Kurzbeschreibung: L�scht die maschine von der DB (nur QO Zuordnung)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOMachine.DeleteFromDB(aQuery: TNativeAdoQuery): Boolean;
  
  const
    cDeleteMachine = 'DELETE FROM t_QO_Machine_Setting WHERE c_QOMachine_id = :c_QOMachine_id';
  
begin
  Result := inherited DeleteFromDB(aQuery);
  aQuery.SQL.Text := cDeleteMachine;
  aQuery.ParamByName('c_QOMachine_id').AsInteger := FID;
  
  // Datensatz gel�scht, wenn ein Datensatz betroffen ist
  result := (aQuery.ExecSQL = 1);
end;// TQOMachine.DeleteFromDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetImageIndex
 *  Klasse:           TQOMachine
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aImageKind, aImageIndex)
 *
 *  Kurzbeschreibung: Fragt bei der Anwendung nach dem ImageIndex
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TQOMachine.GetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer);
begin
  if InProduction then
    aImageIndex := TImageIndexHandler.Instance.Machine
  else
    aImageIndex := TImageIndexHandler.Instance.MachineSW;
end;// TQOMachine.GetImageIndex cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetInProduction
 *  Klasse:           TQOMachine
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Fragt nach ob die Maschine in Produktion ist
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOMachine.GetInProduction: Boolean;
var
  i: Integer;
begin
  result := false;
  if (AssociatedStyles > '') then begin
    with TStringList.Create do try
      (* Wenn die Maschine mit mindestens einem der Styles produziert, dann ist sie f�r alle
         in AssociatedStyles aufgef�hrten Styles in produktion *)
      CommaText := AssociatedStyles;
      for i := 0 to Count - 1 do begin
        result := result or TActiveMachines.Instance.MachInProd(FLinkID, StrTointDef(Strings[i], 0));
      end;
    finally
      free;
    end;// with TStringList.Create do try
  end else begin
    result := TActiveMachines.Instance.MachInProd(FLinkID, ParentID);
  end;// if AssociatedStyles > '' then begin
end;// TQOMachine.GetInProduction cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveDefToDB
 *  Klasse:           TQOMachine
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Speichert die Daten zur Maschine auf der DB (QO Tabellen)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOMachine.SaveDefToDB: Boolean;
var
  mDB: TAdoDBAccess;
  
  const
    cInsertMachine = 'INSERT INTO t_QO_Machine_Setting ' +
                   '(c_QOMachine_id, c_QOStyle_id, c_Machine_id) ' +
                   'VALUES (:c_QOMachine_id, :c_QOStyle_id, :c_Machine_id)';
  
    cUpdateMachine = 'UPDATE t_QO_Machine_Setting ' +
                     'SET c_QOStyle_id = :c_QOStyle_id, c_Machine_id = :c_Machine_id ' +
                     'WHERE c_QOMachine_id = :c_QOMachine_id';
  
    cDeleteMachine = 'DELETE t_QO_Machine_Setting ' +
                     'WHERE c_QOMachine_id = :c_QOMachine_id';
  
  
  (*-----------------------------------------------------------
    Setzt die ADO Parameter
  -------------------------------------------------------------*)
  procedure SetParameter;
  begin
    mDB.Query[0].ParamByName('c_QOStyle_id').AsInteger := integer(ParentID);
    mDB.Query[0].ParamByName('c_Machine_id').AsInteger := integer(LinkID);
  
    // Die ID wird nur f�r UPDATE ben�tigt
    if ID <> cINVALID_ID then
      mDB.Query[0].ParamByName('c_QOMachine_id').AsInteger := ID;
  end;// procedure SetParameter;
  
  (*-----------------------------------------------------------
    F�gt einen neuen Datensatz in die Tabelle ein
  -------------------------------------------------------------*)
  procedure InsertMachine;
  begin
    mDB.Query[0].SQL.Text := cInsertMachine;
    SetParameter;
    // Neuen Datensatz einf�gen und die ID merken
    ID := mDB.Query[0].InsertSQL('t_QO_Machine_Setting', 'c_QOMachine_id', MAXINT, 1);
  end;// procedure InsertMachine;
  
  (*-----------------------------------------------------------
    Ver�ndert einen bestehenden Datensatz
  -------------------------------------------------------------*)
  procedure UpdateMachine;
  begin
    mDB.Query[0].SQL.Text := cUpdateMachine;
    SetParameter;
    // Datensatz Updaten
    if mDB.Query[0].ExecSQL = 0 then begin
      // Wenn kein Datensatz betroffen war, dann den Datensatz neu erzeugen
      ID := cINVALID_ID;
      InsertMachine;
    end;// if mDB.Query[0].ExecSQL = 0 then begin
  end;// procedure UpdateMachine;
  
  (*-----------------------------------------------------------
    L�scht einen bestehenden Datensatz
  -------------------------------------------------------------*)
  procedure DeleteMachine;
  begin
    mDB.Query[0].SQL.Text := cDeleteMachine;
    mDB.Query[0].ParamByName('c_QOMachine_id').AsInteger := ID;
  
    mDB.Query[0].ExecSQL;
    FDeleteState := dsDeleted;
  end;// procedure DeleteMachine;
  
begin
  result := false;
  
  mDB := TAdoDBAccess.Create(1);
  try
    mDB.Init;
  
    // Je nachdem einen neuen DS erzeugen oder einen bestehenden aktualisieren
    if FID = cINVALID_ID then begin
      if FDeleteState = dsValid then
        InsertMachine
      else
        FDeleteState := dsDeleted;
    end else begin
      if FDeleteState = dsPending then begin
        DeleteMachine;
      end else begin
        if FDeleteState = dsValid then
          UpdateMachine;
      end;// if FDeleteState = dsPending then begin
    end;// if FID = cINVALID_ID then begin
  
    // Erfolgreich
    result := true;
  finally
    FreeAndNil(mDB);
  end;// with TAdoDBAccess.Create(1) do try
end;// TQOMachine.SaveDefToDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TQOAllStyles
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TQOAllStyles.Create;
begin
  inherited Create;

//: ----------------------------------------------
  QOVisualize.Caption := '*';
  
  QOVisualize.OnGetCaption := GetCaption;
end;// TQOAllStyles.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetCaption
 *  Klasse:           TQOAllStyles
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aCaption)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn die Caption ge�ndert werden soll
 *  Beschreibung:     
                      Die Caption darf f�r dieses Objekt nicht ge�ndert wird.
 --------------------------------------------------------------------*)
procedure TQOAllStyles.GetCaption(Sender: TObject; var aCaption :string);
begin
  // Immer den selben String zur�ckgeben
  aCaption := sAllStyles;
end;// TQOAllStyles.GetCaption cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetCommaTextByLinkID
 *  Klasse:           TQOAllStyles
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die Zuordnung (LinkID) als Komma String zur�ck
 *  Beschreibung:     
                      Die LinkID ist die ID der entsprechenden Artikel auf der DB. Dies ist die Spalte "c_style_id" 
                      in der Tabelle "t_style".
 --------------------------------------------------------------------*)
function TQOAllStyles.GetCommaTextByLinkID: String;
var
  i: Integer;
begin
  result := '';
  
  if assigned(gAllStyles) then
    result := gAllStyles.CommaTextByLinkID;

//: ----------------------------------------------
  if QOAssigns.AssignCount > 0 then begin
    with TStringList.Create do try
      CommaText := result;
      for i := 0 to Count - 1 do begin
        Strings[i] := Strings[i] + cAdditionalMachines + QOAssigns.CommaTextByLinkID;
        Strings[i] := StringReplace(Strings[i], ',', cAdditionalMachines, [rfReplaceAll]);
      end;// for i := 0 to Count - 1 do begin
      result := CommaText;
    finally
      free;
    end;// with TStringList.Create do try
  end;// if AssignCount > 0 then begin
end;// TQOAllStyles.GetCommaTextByLinkID cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetImageIndex
 *  Klasse:           TQOAllStyles
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aImageKind, aImageIndex)
 *
 *  Kurzbeschreibung: Fragt den ImageIndex f�r die Anzeige ab
 *  Beschreibung:     
                      Wenn nicht alle Artikel in Produktion sind, dann muss ein "halber" Style angezeigt werden.
                      Da hier bereits bekannt ist, dass mindestens ein Artikel produziert, muss nun noch
                      festgestellt werden, ob alle Artikel produzieren.
                      --> Darstellung:
                          Alle Artikel produzieren:   Symbol farbig
                          Kein Artikel produziert:    Symbol Schwarz-Weiss
                          Einige Artikel produzieren: Symbol Halb fabig, halb Schwarz-Weiss
 --------------------------------------------------------------------*)
procedure TQOAllStyles.GetImageIndex(Sender: TObject; aImageKind: TQOImageKind; var aImageIndex: integer);
var
  xNoProduction: Boolean;
  i: Integer;
begin
  inherited GetImageIndex(Sender, aImageKind, aImageIndex);

//: ----------------------------------------------
  if InProduction then begin
    // Wenn nicht alle Artikel in Produktion sind, dann muss ein "halber" Style angezeigt werden.
    xNoProduction := false;
    i := 0;
    while (i < gAllStyles.AssignCount) and(not(xNoProduction)) do begin
      xNoProduction := not(TActiveMachines.Instance.StyleInProd(gAllStyles[i].LinkID));
      inc(i);
    end;// while (i < gAllStyles.AssignCCount) and(not(xNoProduction)) do begin
  
    // Wenn mindestens ein Artikel nicht am produzieren ist, dann Halb anzeigen
    if xNoProduction then
      aImageIndex := TImageIndexHandler.Instance.StyleHalf;
  end;// if InProduction then begin
end;// TQOAllStyles.GetImageIndex cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetInProduction
 *  Klasse:           TQOAllStyles
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Fragt ab, ob �berhaupt irgend ein Artikel in Produktion ist
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TQOAllStyles.GetInProduction: Boolean;
begin
  result := (TActiveMachines.Instance.RowCount > 0);
end;// TQOAllStyles.GetInProduction cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveDefToDB
 *  Klasse:           TQOAllStyles
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Speichert das Objekt auf der DB
 *  Beschreibung:     
                      Da diese Klasse alle Artikel repr�sentiert, gibt es keine LinkID
                      da in der DB kein Datensatz f�r "Alle Artikel" existiert.
                      Deshalb wird eine "Macic Number" als Identifikation verwendet
 --------------------------------------------------------------------*)
function TQOAllStyles.SaveDefToDB: Boolean;
begin
  // "Macic Number" zuweisen
  fLinkID := cAllStylesLinkID;

//: ----------------------------------------------
  Result := inherited SaveDefToDB;
end;// TQOAllStyles.SaveDefToDB cat:No category


end.
