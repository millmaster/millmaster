object frmMethodBaseFrame: TfrmMethodBaseFrame
  Left = 0
  Top = 0
  Width = 443
  Height = 81
  Align = alTop
  TabOrder = 0
  object mMethodNamePanel: TmmPanel
    Left = 0
    Top = 0
    Width = 443
    Height = 18
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvNone
    Color = clInfoBk
    TabOrder = 0
    object mMinusImage: TmmImage
      Left = 432
      Top = 0
      Width = 9
      Height = 9
      Picture.Data = {
        07544269746D617032010000424D320100000000000036000000280000000900
        0000090000000100180000000000FC000000120B0000120B0000000000000000
        0000D3C2B0B59878B59878B59878B59878B59878B59878B59878D3C2B000B598
        78BFCCD2AEBEC6A8B8C2A7B8C1A7B8C1A6B7C0AABAC3B5987800B59878D9E1E4
        CFD8DCC9D3D8C7D2D7C6D1D6C0CCD2BBC8CFB5987800B59878EEF2F2ECF0F0E7
        EDEDE6EBECE3E9EAD9E0E3CCD6DBB5987800B59878F1F5F50000000000000000
        00000000000000D2DBDFB5987800B59878F5F7F7F5F7F7F4F7F7F4F7F7F4F6F6
        EBF0F1DAE1E5B5987800B59878FBFCFCFBFDFDFBFDFDFBFDFDFBFCFCFAFCFCF3
        F6F7B5987800B59878FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB598
        7800D3C2B0B59878B59878B59878B59878B59878B59878B59878D3C2B000}
      Visible = False
      AutoLabel.LabelPosition = lpLeft
    end
    object bExpand: TmmImage
      Left = 17
      Top = 3
      Width = 12
      Height = 12
      Picture.Data = {
        07544269746D617032010000424D320100000000000036000000280000000900
        0000090000000100180000000000FC000000120B0000120B0000000000000000
        0000D3C2B0B59878B59878B59878B59878B59878B59878B59878D3C2B000B598
        78BFCCD2AEBEC6A8B8C2A7B8C1A7B8C1A6B7C0AABAC3B5987800B59878D9E1E4
        CFD8DCC9D3D8000000C6D1D6C0CCD2BBC8CFB5987800B59878EEF2F2ECF0F0E7
        EDED000000E3E9EAD9E0E3CCD6DBB5987800B59878F1F5F50000000000000000
        00000000000000D2DBDFB5987800B59878F5F7F7F5F7F7F4F7F7000000F4F6F6
        EBF0F1DAE1E5B5987800B59878FBFCFCFBFDFDFBFDFD000000FBFCFCFAFCFCF3
        F6F7B5987800B59878FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB598
        7800D3C2B0B59878B59878B59878B59878B59878B59878B59878D3C2B000}
      Stretch = True
      Visible = True
      OnClick = bExpandClick
      AutoLabel.LabelPosition = lpLeft
    end
    object mPlusImage: TmmImage
      Left = 424
      Top = 0
      Width = 9
      Height = 9
      Picture.Data = {
        07544269746D617032010000424D320100000000000036000000280000000900
        0000090000000100180000000000FC000000120B0000120B0000000000000000
        0000D3C2B0B59878B59878B59878B59878B59878B59878B59878D3C2B000B598
        78BFCCD2AEBEC6A8B8C2A7B8C1A7B8C1A6B7C0AABAC3B5987800B59878D9E1E4
        CFD8DCC9D3D8000000C6D1D6C0CCD2BBC8CFB5987800B59878EEF2F2ECF0F0E7
        EDED000000E3E9EAD9E0E3CCD6DBB5987800B59878F1F5F50000000000000000
        00000000000000D2DBDFB5987800B59878F5F7F7F5F7F7F4F7F7000000F4F6F6
        EBF0F1DAE1E5B5987800B59878FBFCFCFBFDFDFBFDFD000000FBFCFCFAFCFCF3
        F6F7B5987800B59878FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB598
        7800D3C2B0B59878B59878B59878B59878B59878B59878B59878D3C2B000}
      Visible = False
      AutoLabel.LabelPosition = lpLeft
    end
    object mMethodImage: TmmImage
      Left = 32
      Top = 1
      Width = 16
      Height = 16
      Transparent = True
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laMethodName: TmmStaticText
      Left = 75
      Top = 3
      Width = 22
      Height = 17
      Caption = '123'
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object cbActivateMethod: TmmCheckBox
      Left = 2
      Top = 1
      Width = 15
      Height = 17
      TabOrder = 1
      Visible = False
      OnClick = GUIChange
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mMethodPanel: TmmPanel
    Left = 0
    Top = 18
    Width = 443
    Height = 63
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 1
    Visible = False
    object mmBevel1: TmmBevel
      Left = 8
      Top = 53
      Width = 218
      Height = 2
    end
    object paCalcMethodFrame: TmmPanel
      Left = 0
      Top = 0
      Width = 185
      Height = 41
      Alignment = taLeftJustify
      BevelOuter = bvNone
      TabOrder = 0
    end
    object mInfoPanel: TmmPanel
      Left = 296
      Top = 1
      Width = 146
      Height = 61
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object mmPanel3: TmmPanel
        Left = 0
        Top = 2
        Width = 21
        Height = 59
        Align = alLeft
        BevelOuter = bvNone
        Color = clInfoBk
        TabOrder = 0
        object mInfoImage: TmmImage
          Left = 2
          Top = 0
          Width = 16
          Height = 16
          Picture.Data = {
            07544269746D617036030000424D360300000000000036000000280000001000
            0000100000000100180000000000000300000000000000000000000000000000
            0000E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF84695A8C695A
            E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FF
            FFE7FFFFE7FFFF94694ACE9E6BAD794A84695AE7FFFFE7FFFFE7FFFFE7FFFFE7
            FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF94694ACEAE84FFF7C6946142
            84695AE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF9471
            638C614AE7CFADFFDFB5FFDFB5AD86635228086B413184695AE7FFFFE7FFFFE7
            FFFFE7FFFFE7FFFFBDA69CBDA69CD6B69CE7CFADFFE7BDFFEFC6FFFFD6F7D7B5
            DEBE94AD8E635A281084695AE7FFFFE7FFFFE7FFFFCEB6A5EFDFC6EFD7BDFFEF
            CEFFD7B5C6714AEF9663DE8E5ADEA67BFFE7C6F7D7B5D6B6947341298C6963E7
            FFFFCEBEA5EFDFC6EFDFC6FFEFD6FFEFCEFFFFE7B586637B00008C2800F7E7C6
            FFEFCEFFE7C6F7D7B5DEBE9C5A2810E7FFFFC6B6A5EFDFC6FFF7D6FFEFD6FFE7
            CEFFFFEFCEB6946B0000A55931FFFFF7FFE7CEFFE7C6FFEFCEFFE7C6AD8E6B94
            716BCEBEADFFE7D6FFF7DEFFEFD6FFEFD6FFFFF7CEAE94730000A55131FFFFF7
            FFEFCEFFE7CEFFEFCEFFEFD6E7CFAD8C695ACEC7B5FFEFE7FFF7E7FFEFDEFFF7
            DEFFFFFFD6C7AD730000A55931FFFFFFFFEFD6FFEFD6FFEFD6FFF7DEEFD7C684
            695AD6C7BDFFF7EFFFFFEFFFF7E7FFFFEFEFE7DE8C38186300008C3821FFFFFF
            FFF7DEFFEFD6FFEFDEFFFFE7F7DFCE947973DED7CEF7F7EFFFFFFFFFF7EFFFFF
            FFE7DFCEB59684BDA69CC6AEA5FFFFFFFFF7E7FFEFDEFFFFEFFFFFFFE7CFB594
            7973E7FFFFE7E7E7FFFFFFFFFFFFFFFFF7FFFFFFFFFFFFFFCFA5FFFFEFFFFFFF
            FFF7EFFFFFEFFFFFFFFFFFFFAD9684E7FFFFE7FFFFEFE7E7EFEFEFFFFFFFFFFF
            FFFFFFFF8C514A5A00009C4129FFFFFFFFFFFFFFFFFFFFFFFFD6BEADE7FFFFE7
            FFFFE7FFFFE7FFFFF7EFE7F7F7F7FFFFFFFFFFFFD6CFCE634142C6B6ADFFFFFF
            FFFFFFFFFFFFEFDFD6E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFEFE7
            DEF7F7EFFFFFFFFFFFFFFFFFFFFFFFF7DECFC6E7FFFFE7FFFFE7FFFFE7FFFFE7
            FFFF}
          Transparent = True
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
      end
      object edMethodDescription: TmmMemo
        Left = 21
        Top = 2
        Width = 125
        Height = 59
        TabStop = False
        Align = alClient
        BorderStyle = bsNone
        Color = clInfoBk
        Ctl3D = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mDescriptionSpacePanel: TmmPanel
        Left = 0
        Top = 0
        Width = 146
        Height = 2
        Align = alTop
        BevelOuter = bvNone
        Color = clInfoBk
        TabOrder = 2
      end
    end
  end
  object mTranslator: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 200
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        'TmmMemo'
        'Text'
        0))
  end
end
