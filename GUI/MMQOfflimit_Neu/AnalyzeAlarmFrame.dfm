object frmAnalyzeAlarmFrame: TfrmAnalyzeAlarmFrame
  Left = 0
  Top = 0
  Width = 476
  Height = 580
  TabOrder = 0
  object mHistogramChart: TmmChart
    Left = 0
    Top = 241
    Width = 476
    Height = 249
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    MarginBottom = 5
    Title.Font.Charset = DEFAULT_CHARSET
    Title.Font.Color = clBlack
    Title.Font.Height = -12
    Title.Font.Name = 'Arial'
    Title.Font.Style = []
    Title.Text.Strings = (
      '(*)Histogramm')
    OnClickSeries = mHistogramChartClickSeries
    BottomAxis.DateTimeFormat = 'dd.MM.yyyy'
    LeftAxis.Title.Caption = '(*)Anzahl beteiligter Maschinen'
    Legend.Visible = False
    View3D = False
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    OnMouseMove = mChartMouseMove
    MouseMode = mmNormal
  end
  object paExtendedInfo: TmmPanel
    Left = 0
    Top = 490
    Width = 476
    Height = 90
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object laAlarmTurn: TmmLabel
      Left = 31
      Top = 8
      Width = 90
      Height = 13
      Caption = '(*)Im Alarm seit '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = True
      AutoLabel.LabelPosition = lpRight
    end
    object laLongestTurn: TmmLabel
      Left = 31
      Top = 24
      Width = 133
      Height = 13
      Caption = '(*)Laengste Alarmdauer'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laAlarmTurnDate: TmmLabel
      Left = 312
      Top = 8
      Width = 3
      Height = 13
      Visible = True
      AutoLabel.Distance = 4
      AutoLabel.LabelPosition = lpLeft
    end
    object laLongestTurnDate: TmmLabel
      Left = 312
      Top = 24
      Width = 3
      Height = 13
      Visible = True
      AutoLabel.Distance = 4
      AutoLabel.LabelPosition = lpLeft
    end
    object mLegendLongestTurn: TmmShape
      Left = 272
      Top = 24
      Width = 33
      Height = 13
      Brush.Color = 62201
      Pen.Style = psInsideFrame
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laTotalCount: TmmLabel
      Left = 31
      Top = 40
      Width = 22
      Height = 13
      Caption = '123'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = True
      AutoLabel.Distance = 4
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 476
    Height = 241
    Align = alTop
    BevelOuter = bvNone
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object paInfo: TmmPanel
      Left = 0
      Top = 0
      Width = 476
      Height = 161
      Align = alClient
      BevelOuter = bvNone
      Caption = '(*)Keine automatisch generierten Alarme registriert'
      Color = clInfoBk
      TabOrder = 2
      object mInfoImage: TmmImage
        Left = 10
        Top = 10
        Width = 16
        Height = 16
        Picture.Data = {
          07544269746D617036030000424D360300000000000036000000280000001000
          0000100000000100180000000000000300000000000000000000000000000000
          0000E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF84695A8C695A
          E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FF
          FFE7FFFFE7FFFF94694ACE9E6BAD794A84695AE7FFFFE7FFFFE7FFFFE7FFFFE7
          FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF94694ACEAE84FFF7C6946142
          84695AE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF9471
          638C614AE7CFADFFDFB5FFDFB5AD86635228086B413184695AE7FFFFE7FFFFE7
          FFFFE7FFFFE7FFFFBDA69CBDA69CD6B69CE7CFADFFE7BDFFEFC6FFFFD6F7D7B5
          DEBE94AD8E635A281084695AE7FFFFE7FFFFE7FFFFCEB6A5EFDFC6EFD7BDFFEF
          CEFFD7B5C6714AEF9663DE8E5ADEA67BFFE7C6F7D7B5D6B6947341298C6963E7
          FFFFCEBEA5EFDFC6EFDFC6FFEFD6FFEFCEFFFFE7B586637B00008C2800F7E7C6
          FFEFCEFFE7C6F7D7B5DEBE9C5A2810E7FFFFC6B6A5EFDFC6FFF7D6FFEFD6FFE7
          CEFFFFEFCEB6946B0000A55931FFFFF7FFE7CEFFE7C6FFEFCEFFE7C6AD8E6B94
          716BCEBEADFFE7D6FFF7DEFFEFD6FFEFD6FFFFF7CEAE94730000A55131FFFFF7
          FFEFCEFFE7CEFFEFCEFFEFD6E7CFAD8C695ACEC7B5FFEFE7FFF7E7FFEFDEFFF7
          DEFFFFFFD6C7AD730000A55931FFFFFFFFEFD6FFEFD6FFEFD6FFF7DEEFD7C684
          695AD6C7BDFFF7EFFFFFEFFFF7E7FFFFEFEFE7DE8C38186300008C3821FFFFFF
          FFF7DEFFEFD6FFEFDEFFFFE7F7DFCE947973DED7CEF7F7EFFFFFFFFFF7EFFFFF
          FFE7DFCEB59684BDA69CC6AEA5FFFFFFFFF7E7FFEFDEFFFFEFFFFFFFE7CFB594
          7973E7FFFFE7E7E7FFFFFFFFFFFFFFFFF7FFFFFFFFFFFFFFCFA5FFFFEFFFFFFF
          FFF7EFFFFFEFFFFFFFFFFFFFAD9684E7FFFFE7FFFFEFE7E7EFEFEFFFFFFFFFFF
          FFFFFFFF8C514A5A00009C4129FFFFFFFFFFFFFFFFFFFFFFFFD6BEADE7FFFFE7
          FFFFE7FFFFE7FFFFF7EFE7F7F7F7FFFFFFFFFFFFD6CFCE634142C6B6ADFFFFFF
          FFFFFFFFFFFFEFDFD6E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFEFE7
          DEF7F7EFFFFFFFFFFFFFFFFFFFFFFFF7DECFC6E7FFFFE7FFFFE7FFFFE7FFFFE7
          FFFF}
        Transparent = True
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object mRankingMachineChart: TmmChart
      Left = 0
      Top = 0
      Width = 476
      Height = 161
      BackWall.Brush.Color = clWhite
      Title.Font.Charset = DEFAULT_CHARSET
      Title.Font.Color = clBlack
      Title.Font.Height = -12
      Title.Font.Name = 'Arial'
      Title.Font.Style = []
      Title.Text.Strings = (
        '(*)Haeufigkeit der Maschinen')
      OnClickSeries = mRankingMachineChartClickSeries
      OnClickBackground = mRankingMachineChartClickBackground
      BottomAxis.MinorTickLength = 0
      LeftAxis.LabelsSeparation = 0
      Legend.Visible = False
      View3D = False
      Align = alClient
      BevelOuter = bvNone
      BevelWidth = 0
      TabOrder = 0
      OnMouseMove = mChartMouseMove
      MouseMode = mmNormal
    end
    object pSpacePanel: TmmPanel
      Left = 0
      Top = 161
      Width = 476
      Height = 80
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    OnAfterTranslate = mmTranslator1AfterTranslate
    Left = 376
    Top = 16
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
end
