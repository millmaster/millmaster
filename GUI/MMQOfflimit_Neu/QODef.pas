(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QODef.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Definitionen des QOfflimits
|
| Info..........: 
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 17.07.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit QODef;

interface

uses
  SysUtils, ADOInt, LoepfeGlobal, SettingsReader;

type
  (*: Klasse:        EQOException
      Vorg�nger:     Exception
      Kategorie:     Exception
      Kurzbeschrieb: Konstruktor 
      Beschreibung:  
                     - *)
  EQOException = class(Exception)
  public
    //1 Konstruktor f�r einen Defaultstring 
    constructor CreateQO(aCaller: string); virtual; abstract;
  end;
  
  (*: Klasse:        ESettingsreaderInit
      Vorg�nger:     EQOException
      Kategorie:     Exception
      Kurzbeschrieb: F�r Settingsreader Init 
      Beschreibung:  
                     Dieser Error wird ausgel�st, wenn 
                     TSettingsreader.Init nicht erfoplgreich ist, 
                     ohne dass eine Exception eingetreten ist. *)
  ESettingsreaderInit = class(EQOException)
  public
    //1 Konstruktor f�r einen Defaultstring 
    constructor CreateQO(aCaller: string); override;
  end;
  

// Bereitet einen String so auf, dass er einem SQL Statement �bergeben werden kann
function PrepareStringForSQL(aText: string; aCommaSep: boolean): String;

// Liefert die globale L�ngenbasis als String
function GetLenBaseString: String; overload;

// Liefert die gew�nschte L�ngenbasis als String
function GetLenBaseString(aLenBase: TLenBase): String; overload;

// Liefert die globale L�ngenbasis
function GetLenBase: TLenBase;

// Liefert den String f�r die L�ngenbasis f�r jedes DataItem
function GetLenBaseString(aLenBase: TLenBase; aCalcMode: integer): String; overload;

// Liefert die L�ngenbasis anhand des CalcMode f�r unregelm�ssige DataItems
function GetLenBaseString(aCalcMode: integer): String; overload;

resourcestring
  // Beschreibung f�r das GUI
  sProdYarnDescr  = '(*)Minimale produzierte Garnlaenge in %s.';  //ivlm
  sProdYarnDescr1 = '(*)Dieser Wert bezieht sich auf alle Maschinen eines Artikels seit Beginn des Kalkulationsdatums.'; //ivlm
  sQOLengthUnit   = '(*)Laengeneinheit'; //ivlm
  sNotValid       = '(*)ungueltig'; //ivlm

  // Fehlermeldungen
  sESettingsreaderInit = '(*)Die Einstellungen konnten nicht gelesen werden.'; //ivlm
  sEFactoryError       = '(*)Das Objekt konnte nicht erstellt werden'; //ivlm
  rsLengthError        = '(*)Beschraenkung der Laenge';//ivlm
  rsTextLength         = '(*)Der Text darf nur %d Zeichen lang sein';//ivlm
  rsQOInitFailed       = '(*)Die Initialisierung des Q-Offlimits ist fehlgeschlagen'; //ivlm
  
  sAbsolute     = '(30)Absolut';       //ivlm
  
  // Beschriftung f�r das GUI
  sAllStyles    = '(*)Alle Artikel';   //ivlm
  
  // Fehlermeldung, wenn die Datenbank nicht initialisiert werden kann
  sNoDBInit = '(*)Keine Verbindung zur Datenbank.'; // ivlm 
  
  // Undefinierte L�ngenbasis
  srUndefined = '(*)Undefiniert'; // ivlm 
  
const
//  GUI
  // L�nge des Names f�r einen Alarm
  cAlarmNameLength = 25;
  // Formatstring f�r die Matrix - DataItems (Cut,Uncout, Both)
  cDataItemNameFormat ='%s (%s)';
  
// Kommandozeile
  // Zeigt das Settings Tab an  
  cCmdShowSettings     = 'ShowSetting'; 
  // Zeigt das Assign Tab an
  cCmdShowAssign       = 'ShowAssign'; 
  // Zeigt das Analyze Tab an
  cCmdShowAnalyze      = 'ShowAnalyze'; 
  // Erm�glicht das neu Laden der Alarme von der Datenbank (Analyze)
  cCmdRefreshVisible   = 'RefreshVisible';
 
// Tags
  cXMLQOItem           = 'QOItem'; 
  cXMQOName            = 'Name';  
  cXMLQOSettings       = 'QOSettings';
  cXMLMethodID         = 'Method';
  cXMLMethodSettingsID = 'MethodSettings';
  
  // Basis Methoden Parameter Tags
    cXMLActive = 'Active';
    cXMLShowDetails = 'ShowDetails';
    cXMLShiftCount = 'ShiftCount';
    cXMLLengthUnit = 'LengthUnit';
    cXMLLenBase = 'LenBase';
  
  // FixBorder Parameter Tags (Keine zus�tzlichen Tags)
  
  // AVGBorder Parameter Tags
    cXMLMinProdYarnLength = 'MinProdYarnLength';
      
 // XML Attribute
  // Tag f�r den Methodentyp
  cXMLMethodType       = 'Type';
  
  // Diverse
  cDefaultShiftCount  = 3;
  cFieldSeparator = #255;

  (* 
     Die Feldnamen m�glichst kurz halten, damit der Overhead im XMLString m�glichst klein ist *)
  cValueFieldName         = 'val';
  cLimitValueFieldName    = 'lim';
  cShiftFieldName         = 'sid';
  cProdGroupFieldName     = 'pid';
  cProdGroupNameFieldName = 'pn';
  cLenFieldName           = 'len';
  cShiftStartFieldName    = 'sst';
  cFirstSpdFieldName      = 'fsp';
  cLastSpindleFieldName   = 'lsp';
  cMachineFieldName       = 'mid'; 
  cMachineNameFieldName   = 'man';
  cStyleFieldName         = 'stid';
  cDataItemItemName       = 'din';
  cDataItemDisplayName    = 'ddn';
  cDataItemFieldName      = 'dfn';
  cClassDefectsFieldName  = 'cdf';
  cMethodFieldName        = 'men';
  cCalcModeFieldName      = 'cmd';
  cLenBaseFieldName       = 'lbn';
  cSFICntFieldName        = 'sfic';
  cAdjustBaseFieldName    = 'adjb';
  cAdjustBaseCntFieldName = 'adjc';


  cFieldCount = 21;
  
// Queries  
  cSQLDateTimeFormatString = 'yyyy''-''mm''-''dd hh'':''nn'':''ss';
  cGetAllStyles = 'SELECT c_style_id, c_style_name ' +
                  'FROM t_style ' +
                  'ORDER BY c_style_name';
                  
  cGetAllMachs  = 'SELECT c_machine_id, c_machine_name ' +
                  'FROM t_machine ' +
                  'ORDER BY c_machine_name';
                  
  cGetInProdFromStyle = 'SELECT DISTINCT c_machine_id ' +
                        'FROM t_prodgroup_state ' +
                        'WHERE c_style_id = :c_style_id ' +
                        'ORDER BY c_machine_id';
  
  cFixDataPoolFields  = 'c_shift_id, c_style_id, c_prod_id, c_Machine_id, c_shift_start, c_style_name, c_machine_name, c_prod_name, c_spindle_first, c_spindle_last';
  cFixStyleDataFields = 'c_shift_id, c_style_id, c_shift_start, c_style_name';
                  
implementation

uses Classes, mmuGlobal;

(*-----------------------------------------------------------
  Bereitet einen String so auf, dass er einem SQL Statement �bergeben werden kann
-------------------------------------------------------------*)
function PrepareStringForSQL(aText: string; aCommaSep: boolean): String;
var
  i: Integer;

const 
  cSemicolon    = #19#19;
  cDoubleQuotes = #20#20;
  
begin

(* Auszug aus dem ADO SDK:

  Note   To include single quotation marks (') in the filter Value, use two single 
  quotation marks to represent one. For example, to filter on O'Malley, the criteria 
  string should be "col1 = 'O''Malley'". To include single quotation marks at both 
  the beginning and the end of the filter value, enclose the string with pound signs (#). 
  For example, to filter on '1', the criteria string should be "col1 = #'1'#". 
  
  *)  
  
  // Erstmal die Hochkommas durch 2 Hochkommas ersetzen
  result := StringReplace(aText, '''', '''''', [rfReplaceAll]);
  
  if aCommaSep then begin
    // result := "Stri,ng 1","Stri""ng 2","String 3","Stri,ng4",String5
    // Zuerst die Kommas in den einzelnen Strings mit zwei Steurzeichen ersetzen
    with TStringList.Create do try
      CommaText := result;
      for i := 0 to Count - 1 do begin
        // Ersetzt alle Kommas mit zwei Steuerzeichen
        Strings[i] := StringReplace(Strings[i],',', cSemicolon, [rfReplaceAll]);  
      end;// for i := 0 to Count - 1 do begin
      result := commatext;
    finally 
      Free;
    end;// with TStringList.Create do try
    // result := "Stri#19#19ng 1","Stri""ng 2","String 3","Stri#19#19ng4",String5
    
    // Zuerst doppelte Anf�hrungszeichen mit zwei Steuerzeichen ersetzen
    result := StringReplace(result, '""', cDoubleQuotes, [rfReplaceAll]); 
    // result := "Stri#19#19ng 1","Stri#20#20ng 2","String 3","Stri#19#19ng4",String5
    
    // Jetzt die Anf�hrungszeichen l�schen
    result := StringReplace(result, '"', '', [rfReplaceAll]); 
    // TStrings.CommaText := Stri#19#19ng 1,Stri#20#20ng 2,String 3,Stri#19#19ng4,String5

    // Jetzt f�r jedes Komma die Anf�hrungszeichen einsetzen
    result := '''' + StringReplace(result, ',', ''',''', [rfReplaceAll]) + ''''; 
    // TStrings.CommaText := 'Stri#19#19ng 1','Stri#20#20ng 2','String 3','Stri#19#19ng4','String5'
    
    // Jetzt die Steuerzeichen wieder durch die gew�nschten Zeichen ersetzen
    result := StringReplace(result, cDoubleQuotes, '"', [rfReplaceAll]); 
    result := StringReplace(result, cSemicolon, ',', [rfReplaceAll]); 
    // TStrings.CommaText := 'Stri,ng 1','Stri"ng 2','String 3','Stri,ng4','String5'
  end;// if aCommaSep then begin
end;// function PrepareStringForSQL(aText: string; aCommaSep: boolean): String;

(*-----------------------------------------------------------
  Liefert die globale L�ngenbasis als String
-------------------------------------------------------------*)
function GetLenBaseString: String;
begin
  Result := GetLenBaseString(GetLenBase);
end;// function GetLenBaseString: TLenBase;

(*-----------------------------------------------------------
  Liefert die gew�nschte L�ngenbasis als String
-------------------------------------------------------------*)
function GetLenBaseString(aLenBase: TLenBase): String;
begin
  Result := '';
  case aLenBase of
    lbAbsolute : Result := sAbsolute;
    lb100KM    : Result := '/100km';
    lb1000KM   : Result := '/1000km';
    lb100000Y  : Result := '/100000 yards';
    lb1000000Y : Result := '/1000000 yards';
  end;// case GetLenBase of
end;// function GetLenBaseString: TLenBase;

(*-----------------------------------------------------------
  Liefert die globale L�ngenbasis
-------------------------------------------------------------*)
function GetLenBase: TLenBase;
begin
  result := lb100KM;

  with TMMSettingsReader.Instance do begin
    // Initialisieren soweit notwendig
    if init then
      result := TLenBase(Value[cMMUnit]);
  end;// with TMMSettingsReader.Instance do begin
end;// function GetLenBase: TLenBase;

(*-----------------------------------------------------------
  Liefert die L�ngenbasis anhand des CalcMode f�r unregelm�ssige DataItems
-------------------------------------------------------------*)
function GetLenBaseString(aCalcMode: integer): String;
begin
  case aCalcMode of
    1     : result := '/m';
    1000  : result := '/1km';
    -2, -3: result := '';
  else
    result := srUndefined;
  end;// case aCalcMode of
end;// function GetLenBaseString(aCalcMode: integer): String;

(*-----------------------------------------------------------
  Liefert den String f�r die L�ngenbasis f�r jedes DataItem
-------------------------------------------------------------*)
function GetLenBaseString(aLenBase: TLenBase; aCalcMode: integer): String;
begin
  case aCalcMode of
    // normal len based value
    0: result := GetLenBaseString(aLenBase);
    else
      //Imperfection or at fixed meter count or SFI
      Result := GetLenBaseString(aCalcMode);
  end;//case aCalcMode of
end;// function GetLenBaseString(aLenBase: TLenBase; aCalcMode: integer): String;

const
  cLoepfeDecimalSeparator = '.';

//:-------------------------------------------------------------------
(*: Member:           CreateQO
 *  Klasse:           ESettingsreaderInit
 *  Kategorie:        No category 
 *  Argumente:        (aCaller)
 *
 *  Kurzbeschreibung: Konstruktor f�r einen Defaultstring
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor ESettingsreaderInit.CreateQO(aCaller: string);
begin
  if aCaller > '' then
    create(aCaller + ':' + sESettingsreaderInit)
  else
    create(sESettingsreaderInit);
end;// ESettingsreaderInit.CreateQO cat:No category


end.
