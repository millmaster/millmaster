(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QOShiftListReport
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Bericht f�r die registrierten Alarme pro Schicht (Liste)
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.04.2003  1.00  Lok | Datei erstellt
| 18.06.2004  1.00  SDo | Layout Aenderung & Zuweisung in FormCreate
|=========================================================================================*)
unit QOReportShiftList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, QuickRpt, Qrctrls, mmQRLabel, ExtCtrls, mmQuickRep, mmQRBand,
  mmQRImage, mmQRSysData, mmQRShape, mmQRMemo, mmQRSubDetail, QOGUIClasses,
  IvDictio, IvMulti, IvEMulti, mmTranslator, mmQRDBText;

type
  (*: Klasse:        TfrmQOReportShiftList
      Vorg�nger:     TmmForm
      Kategorie:     No category
      Kurzbeschrieb: Liste mit den Schichten die gedruckt werden sollen 
      Beschreibung:  
                     - *)
  TfrmQOReportShiftList = class(TmmForm)
    laActValue: TmmQRLabel;
    laActValueAlarm: TmmQRLabel;
    laAlarmAlarm: TmmQRLabel;
    laAlarmCount: TmmQRLabel;
    laAvgValueAlarm: TmmQRLabel;
    laBoundAlarm: TmmQRLabel;
    laDIAlarm: TmmQRLabel;
    laLotAlarm: TmmQRLabel;
    laMethodAlarm: TmmQRLabel;
    laShiftDate: TmmQRLabel;
    laStyleAlarm: TmmQRLabel;
    memMethodNr: TmmQRMemo;
    memMethods: TmmQRMemo;
    memoAlarmEventInfo: TmmQRMemo;
    memoShiftEventInfo: TmmQRMemo;
    mLineShape: TmmQRShape;
    mmQRBand2: TmmQRBand;
    mmQRImage1: TmmQRImage;
    mmQRLabel1: TmmQRLabel;
    mmQRLabel2: TmmQRLabel;
    mmQRLabel3: TmmQRLabel;
    mmQRLabel4: TmmQRLabel;
    mmQRLabel5: TmmQRLabel;
    mmQRLabel6: TmmQRLabel;
    mmQRLabel7: TmmQRLabel;
    mmQRLabel8: TmmQRLabel;
    mmQRSysData1: TmmQRSysData;
    mmQRSysData2: TmmQRSysData;
    mmQRSysData3: TmmQRSysData;
    mmTranslator1: TmmTranslator;
    mQRShift: TmmQuickRep;
    mRootBand: TQRBand;
    mShiftBand: TmmQRSubDetail;
    mShiftListTitle: TQRBand;
    PageFooterBand1: TQRBand;
    qlCompanyName: TmmQRLabel;
    qlDate: TmmQRLabel;
    qlMeth: TmmQRLabel;
    qlPageValue: TmmQRSysData;
    qlParam: TmmQRLabel;
    qlTime: TmmQRLabel;
    qlTitle: TmmQRLabel;
    qsTitleLine1: TmmQRShape;
    qsTitleLine2: TmmQRShape;
    procedure FormCreate(Sender: TObject);
    //1 Druckt alle Schichten die in der Liste mShiftViewList definiert sind 
    procedure mQRShiftNeedData(Sender: TObject; var MoreData: Boolean);
    //1 Diese Prozedur wird immer aufgerufen, wenn eine Schicht zu Drucken ist 
    procedure mShiftBandNeedData(Sender: TObject; var MoreData: Boolean);
    //1 Wird aufgerufen um den Titelbereich zu initialisieren (Liste der verf�gbaren Methoden) 
    procedure mShiftListTitleBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
  private
    FIgnoreAdvice: Boolean;
    mActualAlarmView: TQOAlarmView;
    mActualShiftView: TQOShiftView;
    mAlarmIndex: Integer;
    mDataItemList: TStringList;
    mDIIndex: Integer;
    mLinesPrinted: Integer;
    mLotIndex: Integer;
    mMethodIndex: Integer;
    mMethodList: TStringList;
    mMoreDataItems: Boolean;
    mMoreLots: Boolean;
    mMoreMethods: Boolean;
    mMoreStyles: Boolean;
    mNormalShiftBandHeight: Integer;
    mShiftIndex: Integer;
    mShiftViewList: TList;
    mStyle: TQOAssignNodeObject;
    mStyleIndex: Integer;
    //1 Liefert den Namen des DataItem 
    function GetDataItemData: Boolean;
    //1 Liefert die Lotdaten (Maschine und Spindelbereich) 
    procedure GetLotData;
    //1 Liefert die Daten der Methode und des Artikels f�r die Spalte 'Wert' 
    function GetMethodData: Boolean;
    //1 Anzahl der zu druckenden Schichten 
    function GetShiftCount: Integer;
    //1 Holt den Artikelnamen um ihn im entsprechenden Feld anzuzeigen 
    procedure GetStyleData;
    //1 Schreibt das DataItem in den Report 
    procedure SetDIText;
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 F�gt eine Schicht die gedruckt werden soll zur Liste hinzu 
    procedure AddShift(aShiftView: TQOShiftView);
    //1 L�scht die Liste mit den zu druckenden Schichten 
    procedure ClearShiftList;
    //1 Druckt die Schichten aus die zuvor der Liste hinzugef�gt wurden (siehe AddShift()) 
    procedure Print(aWithSetup: boolean);
    //1 Bestimmt ob ein Alarm ausgedruckt werden soll 
    property IgnoreAdvice: Boolean read FIgnoreAdvice write FIgnoreAdvice;
    //1 Anzahl der zu druckenden Schichten 
    property ShiftCount: Integer read GetShiftCount;
  end;
  
var
  frmQOShiftListReport: TfrmQOReportShiftList;

implementation
uses printers, QODef, mmcs, QOKernel, MMUGlobal, mmDialogs, QOGUIShared,
     LoepfeGlobal, CalcMethods, QRPrntr, SettingsReader;

resourcestring
  rsPrintAborted = '(*)Das Drucken wurde abgebrochen'; //ivlm
  rsAlarmCount   = '(*)Anzahl Alarme in dieser Schicht'; //ivlm
  rsAlarmHidden  = '(15)Gedruckt'; //ivlm

{$R *.DFM}

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmQOReportShiftList
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmQOReportShiftList.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

//: ----------------------------------------------
  // Verschiedene Listen die f�r die Verwaltung ben�tigt werden
  mShiftViewList := TList.Create;
  mDataItemList := TStringList.Create;
  mMethodList := TStringList.Create;
  
  // Da sich der Report die Orientierung nicht merken kann
  mQRShift.page.orientation := poLandscape;
  
  // Im Normalfall alle Alarme drucken
  IgnoreAdvice := true;
end;// TfrmQOReportShiftList.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmQOReportShiftList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmQOReportShiftList.Destroy;
begin
  FreeAndNil(mShiftViewList);
  FreeAndNil(mDataItemList);
  FreeAndnil(mMethodList);
  FreeAndNil(mStyle);

//: ----------------------------------------------
  inherited Destroy;
end;// TfrmQOReportShiftList.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddShift
 *  Klasse:           TfrmQOReportShiftList
 *  Kategorie:        No category 
 *  Argumente:        (aShiftView)
 *
 *  Kurzbeschreibung: F�gt eine Schicht die gedruckt werden soll zur Liste hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOReportShiftList.AddShift(aShiftView: TQOShiftView);
begin
  // Schicht zur Liste hinzuf�gen
  if (assigned(mShiftViewList))and (assigned(aShiftView)) then
    mShiftViewList.Add(aShiftView);
end;// TfrmQOReportShiftList.AddShift cat:No category

//:-------------------------------------------------------------------
(*: Member:           ClearShiftList
 *  Klasse:           TfrmQOReportShiftList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�scht die Liste mit den zu druckenden Schichten
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOReportShiftList.ClearShiftList;
begin
  // Liste leeren
  if assigned(mShiftViewList) then
    mShiftViewList.Clear;
end;// TfrmQOReportShiftList.ClearShiftList cat:No category

//:-------------------------------------------------------------------
procedure TfrmQOReportShiftList.FormCreate(Sender: TObject);
begin
  inherited;
  try
    qlCompanyName.Caption := TMMSettingsReader.Instance.Value[cCompanyName];
  except
    qlCompanyName.Caption :='';
  end;
  
  qlPageValue.Text := Translate( qlPageValue.Text ) + ' ';
end;// TfrmQOReportShiftList.FormCreate cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetDataItemData
 *  Klasse:           TfrmQOReportShiftList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert den Namen des DataItem
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmQOReportShiftList.GetDataItemData: Boolean;
var
  xFilterString: String;
  xIndex: Integer;
begin
  result := false;
  // Da hier ein neues DataItem ermittelt wird m�ssen auch die Methoden neu ermittelt werden
  mMethodList.clear;

//: ----------------------------------------------
  // Nach einem Datensatz suchen der mit den notwendigen Kriterien auftritt
  while (not(result)) and (mDIIndex < mDataItemList.Count) do begin
    // Zuerst einmal den Index f�r das DataItem erh�hen (wird mit -1 initialisiert)
    inc(mDIIndex);
    // Wenn �berhaupt noch Daten vorhanden sind
    if (mDIIndex < mDataItemList.Count) then begin
      with mStyle.ProdIDList[mLotIndex] do
        xFilterString := Format('%s = %d and %s = ''%s''', [cProdGroupFieldName, ProdID, cDataItemDisplayName, PrepareStringForSQL(mDataItemList[mDIIndex], false)]);
  
      // Artikeldaten filtern
      mActualAlarmView.AlarmData.Filter(xFilterString);
      mActualAlarmView.AlarmData.FindFirst;
      // Wenn ein Datensatz vorhanden ist, dann ist die Suche beendet
      result := not(mActualAlarmView.AlarmData.EOF);
      if (result) then begin
        // Namen des DataItems eintragen
        SetDIText;
  
        // Jetzt muss die Liste mit den Berechnungsmethoden f�r dieses DataItem ermittelt werden
        with mActualAlarmView.AlarmData do begin
          // Nach den Methoden sortieren, damit sie nacher auch sortiert in der Liste sind
          Sort(cMethodFieldName);
          // Da das Recordset bereits gefiltert ist, kann jeder DS untersucht werden
          FindFirst;
          while not(EOF) do begin
            // Pr�fen ob die Methode bereits in der Liste vorhanden ist - Ansonsten hinzuf�gen
            xIndex := mMethodList.IndexOf(FieldByName(cMethodFieldName).AsString);
            if xIndex < 0 then
              mMethodList.Add(FieldByName(cMethodFieldName).AsString);
            // N�chsten DS untersuchen
            next;
          end;// while not(EOF) do begin
        end;// with mActualAlarmView.AlarmData do begin
      end;// if (result) then begin
    end;// if (mDIIndex < mDataItemList.Count) then begin
  end;// while (not(result)) and (mDIIndex < mDataItemList.Count) do begin
  
  mActualAlarmView.AlarmData.Filter('');
end;// TfrmQOReportShiftList.GetDataItemData cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetLotData
 *  Klasse:           TfrmQOReportShiftList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert die Lotdaten (Maschine und Spindelbereich)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOReportShiftList.GetLotData;
var
  i: Integer;
  xFilterString: String;
begin
  with mStyle.ProdIDList[mLotIndex] do begin
    laLotAlarm.Caption := Format('%s: %d - %d (#%d)', [// Name der Maschine
                                                      MachineName,
                                                      // Erste Spindel
                                                      FirstSpindle,
                                                      // Letzte Spindel
                                                      LastSpindle,
                                                      // Anzahl der Schichten seit das Lot ununterbrochen im Alarm ist
                                                      GetProdIDTurnCount(mActualShiftView.ShiftID, ProdID)]);
  end;// with mStyle.ProdIDList[mLotIndex] do begin

//: ----------------------------------------------
  // Liste mit den beteiligten DataItems pro Alarm
  mDataItemList.CommaText := mActualAlarmView.GetDataItemsFromStyle(mStyle.QOVisualize.Caption);
  
  i := 0;
  while (i < mDataItemList.Count) do begin
    // Nur die DataItems in der Liste haben die f�r dieses Lot massgebend sind
    with mStyle.ProdIDList[mLotIndex] do
      xFilterString := Format('%s = %d and %s = ''%s''', [cProdGroupFieldName, ProdID, cDataItemDisplayName, PrepareStringForSQL(mDataItemList[i], false)]);
  
    // Artikeldaten filtern
    mActualAlarmView.AlarmData.Filter(xFilterString);
    mActualAlarmView.AlarmData.FindFirst;
    // Wenn ein Datensatz vorhanden ist, dann ist die Suche beendet
    if (mActualAlarmView.AlarmData.EOF) then
      mDataItemList.Delete(i)
    else
      inc(i);
  end;// while (i < mDataItemList.Count) do begin
  
  mActualAlarmView.AlarmData.Filter('');
end;// TfrmQOReportShiftList.GetLotData cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMethodData
 *  Klasse:           TfrmQOReportShiftList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert die Daten der Methode und des Artikels f�r die Spalte 'Wert'
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmQOReportShiftList.GetMethodData: Boolean;
var
  xFilterString: String;
  xValue: Extended;
  xStyleValue: Extended;
  xStyleLen: Extended;
  xDataItemName: String;
  
  const
    _cFormatString = '%.5g';
  
begin
  result := false;

//: ----------------------------------------------
  // Zuerst den Filterstring bis und mit DataItem ermitteln
  with mStyle.ProdIDList[mLotIndex] do
    xFilterString := Format('%s = %d and %s = %d and %s = ''%s''', [cProdGroupFieldName, ProdID, cStyleFieldName, StyleID, cDataItemDisplayName, PrepareStringForSQL(mDataItemList[mDIIndex], false)]);
  
  // Zus�tzlich nach der Methode filtern
  mActualAlarmView.AlarmData.Filter(Format('%s and %s = %d', [xFilterString, cMethodFieldName, StrToIntDef(mMethodList[mMethodIndex], 0)]));
  // Die neueste Schicht als ersten Datensatz (Alle Angaben auf dem Bericht beziehen sich auf die neueste Schicht)
  mActualAlarmView.AlarmData.Sort(Format('%s DESC', [cShiftFieldName]));
  mActualAlarmView.AlarmData.FindFirst;
  
  // Die Daten sind verf�gbar, wenn jetzt noch mindestens ein DS �brig ist
  result := not(mActualAlarmView.AlarmData.EOF);
  
  if (result) then begin
    // Die Nummer der Methode erh�hen, damit nicht 0 als Methode im Bericht steht (siehe ...BeforePrint())
    laMethodAlarm.Caption := IntToStr(StrToIntDef(mMethodList[mMethodIndex], 0) + 1);
  
    with mActualAlarmView.AlarmData do begin
      // Den aktuellen Wert berechnen (normiert auf LenBase)
      xValue := calcAllValues(TLenBase(FieldByName(cLenBaseFieldName).AsInteger),
                              FieldByName(cValueFieldName).AsFloat,
                              FieldByName(cLenFieldName).AsFloat,
                              FieldByName(cCalcModeFieldName).AsInteger,
                              FieldByName(cSFICntFieldName).AsFloat,      // Anzahl der Werte auf denen der SFI beruht
                              FieldByName(cAdjustBaseFieldName).AsFloat,  // AdjustBase f�r SFI-Berechnungen
                              FieldByName(cAdjustBaseCntFieldName).AsFloat,   // Anzahl aufsummierter Werte
                              1);                                         // Basiert auf einem Wert (F�r QO unwichtig da Nutzeffekt)
      laActValueAlarm.Caption := mmFormatFloatToStr(xValue);
  
      // Der Grenzwert muss nicht berechnet werden
      laBoundAlarm.Caption := mmFormatFloatToStr(FieldByName(cLimitValueFieldName).AsFloat);
  
      // Filtert die Daten nach der neuesten Schicht und dem entsprechenden Artikel
      mActualShiftView.AllStyles.Filter(Format('c_shift_id = %d and c_style_id = %d', [FieldByName(cShiftFieldName).AsInteger, FieldByName(cStyleFieldName).AsInteger]));
      mActualShiftView.AllStyles.FindFirst;
  
      // Name des Feldes in dem die Daten des entsprechenden DateItems abgelegt sind, und dann den Wert abfragen
      xDataItemName := FieldByName(cDataItemFieldName).AsString;
      xStyleValue := mActualShiftView.AllStyles.FieldByName(xDataItemName).AsFloat;
      // Die produzierte L�nge abfragen
      xStyleLen := mActualShiftView.AllStyles.FieldByName('c_len').AsFloat;
      // ... und den definitiven Wert berechnen
      xValue := calcAllValues(TLenBase(FieldByName(cLenBaseFieldName).AsInteger),
                              xStyleValue,
                              xStyleLen,
                              FieldByName(cCalcModeFieldName).AsInteger,
                              FieldByName(cSFICntFieldName).AsFloat,        // Anzahl der Werte auf denen der SFI beruht
                              FieldByName(cAdjustBaseFieldName).AsFloat,    // AdjustBase f�r SFI-Berechnungen
                              FieldByName(cAdjustBaseCntFieldName).AsFloat, // Anzahl aufsummierter Werte
                              1);                                           // Basiert auf einem Wert (F�r QO unwichtig da Nutzeffekt)
      laAvgValueAlarm.Caption := mmFormatFloatToStr(xValue);
  
      // Filter f�r die weitere Verwendung entfernen
      mActualShiftView.AllStyles.Filter('');
    end;// with mActualAlarmView.AlarmData do begin
  end;// if (xMethodFound) then begin
  // Filter f�r die weitere Verwendung entfernen
  mActualAlarmView.AlarmData.Filter('');
end;// TfrmQOReportShiftList.GetMethodData cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetShiftCount
 *  Klasse:           TfrmQOReportShiftList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Anzahl der zu druckenden Schichten
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmQOReportShiftList.GetShiftCount: Integer;
begin
  result := 0;
  
  if assigned(mShiftViewList) then
    result := mShiftViewList.Count;
end;// TfrmQOReportShiftList.GetShiftCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetStyleData
 *  Klasse:           TfrmQOReportShiftList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Holt den Artikelnamen um ihn im entsprechenden Feld anzuzeigen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOReportShiftList.GetStyleData;
begin
  // Erst mal den "alten" Style freigeben
  FreeAndNil(mStyle);
  
  // Neuen Style erzeugen
  mStyle := TQOAssignNodeObject.Create;
  // Den Alarm zuweisen (Wird f�r gebraucht um auf die Daten zuzugreifen)
  mStyle.AlarmView := mActualAlarmView;
  mStyle.NodeObjectType := otStyle;
  mStyle.QOVisualize.Caption := mActualAlarmView.AllStyles[mStyleIndex];
  // StyleID aus 't_style'
  mStyle.LinkID := Integer(mActualAlarmView.AllStyles.Objects[mStyleIndex]);
  // ERzeugt eine Liste von Produktionsgruppen f�r den Style
  mStyle.BuildProdIDList;
  laStyleAlarm.Caption := mStyle.QOVisualize.Caption;
end;// TfrmQOReportShiftList.GetStyleData cat:No category

//:-------------------------------------------------------------------
(*: Member:           mQRShiftNeedData
 *  Klasse:           TfrmQOReportShiftList
 *  Kategorie:        No category 
 *  Argumente:        (Sender, MoreData)
 *
 *  Kurzbeschreibung: Druckt alle Schichten die in der Liste mShiftViewList definiert sind
 *  Beschreibung:     
                      Diese Methode wird vom QR f�r jede Schicht aufgerufen.
 --------------------------------------------------------------------*)
procedure TfrmQOReportShiftList.mQRShiftNeedData(Sender: TObject; var MoreData: Boolean);
var
  i: Integer;
  xAlarmsToHide: Integer;
begin
  // F�r jede Schicht neu initialisieren
  mDIIndex     := -1;
  mLotIndex    := -1;
  mMethodIndex := -1;
  mStyleIndex  := -1;
  mAlarmIndex  := -1;
  
  // "Zeiger" auf die n�chste Schicht
  inc(mShiftIndex);
  MoreData := (mShiftIndex < mShiftViewList.Count);
  if MoreData then begin
    // zu druckende Schicht sichern. Sie dient als Ausgangspunkt f�r die weiteren Operationen
    mActualShiftView := TQOShiftView(mShiftViewList[mShiftIndex]);
  
    // Findet die Startzeit der betrefenden Schicht und tr�gt diesen als Titel ein.
    mActualShiftView.AllStyles.Filter('c_shift_id = ' + IntToStr(mActualShiftView.ShiftID));
    if not(mActualShiftView.AllStyles.EOF) then
      laShiftDate.Caption := DateTimeToTreeCaption(mActualShiftView.AllStyles.FieldByName('c_shift_start').AsDateTime)  + ' - ' + DateTimeToTreeCaption(mActualShiftView.AlarmDate)
    else
      laShiftDate.Caption := DateTimeToTreeCaption(mActualShiftView.AlarmDate);
  
    // F�r den Fall dass nicht alle Alarme ausgedruckt werden, soll die Anzahl der Verborgenen Alarme angezeigt werden
    if not(IgnoreAdvice) then begin
      laAlarmCount.Visible := true;
      xAlarmsToHide := 0;
      for i := 0 to mActualShiftView.AlarmViewCount - 1 do begin
        if not(amPrinter in mActualShiftView.AlarmViews[i].AdviceMethods) then
          inc(xAlarmsToHide);
      end;// for i := 0 to mActualShiftView.AlarmViewCount - 1 do begin
  
      // Anzahl der verborgenen Alarme anzeigen
      laAlarmCount.Caption := Format('%s: %d - %s: %d',[rsAlarmCount, mActualShiftView.AlarmViewCount,
                                                        rsAlarmHidden, xAlarmsToHide]);
    end else begin
      // Wenn sowiso alle Alarme angezeigt werden, wird dieses Label nicht ben�tigt
      laAlarmCount.Visible := false;
      laAlarmCount.Caption := '';
    end;// if not(IgnoreAdvice) then begin
  
    (* Kommentar ins Memo schreiben. Das Band vergr�ssert sich vertikal automatisch
       wenn mehrere Zeilen geschrieben werden m�ssen. *)
    memoShiftEventInfo.Lines.Text := mActualShiftView.Info;
    // Band verkleinern, wenn es keinen Kommentar gibt
    if mActualShiftView.Info = '' then
      mRootBand.Height := memoShiftEventInfo.Top;
  
    mActualShiftView.AllStyles.Filter('');
  end;// if MoreData then begin
end;// TfrmQOReportShiftList.mQRShiftNeedData cat:No category

//:-------------------------------------------------------------------
(*: Member:           mShiftBandNeedData
 *  Klasse:           TfrmQOReportShiftList
 *  Kategorie:        No category 
 *  Argumente:        (Sender, MoreData)
 *
 *  Kurzbeschreibung: Diese Prozedur wird immer aufgerufen, wenn eine Schicht zu Drucken ist
 *  Beschreibung:     
                      MoreData wird erst false, wenn alle Alarme einer Schicht gedruckt sind.
                      Pro Alarm werden je nach Detailinformationen mehrere B�nder gedruckt. Es 
                      werden nur die neuen Daten gedruckt. So entsteht eine Art Tree.
 --------------------------------------------------------------------*)
procedure TfrmQOReportShiftList.mShiftBandNeedData(Sender: TObject; var MoreData: Boolean);
var
  xFound: Boolean;
  
  function GetMoreShiftBandData: Boolean;
  begin
    Result := (mAlarmIndex < mActualShiftView.AlarmViewCount) or (mMoreStyles or mMoreLots or mMoreDataItems or mMoreMethods);
  end;
  
begin
    // Zuerst feststellen, ob �berhaupt noch Daten vorhanden sind
  MoreData := GetMoreShiftBandData;
  if MoreData then begin
    mLineShape.enabled := odd(mLinesPrinted);
    inc(mLinesPrinted);
    try

//: ----------------------------------------------
    // Alarm
      if (mMoreStyles or mMoreLots or mMoreDataItems or mMoreMethods) then begin
          // Einen Leerstring ausgeben, wenn noch untergeordnete Daten zu drucken sind.
        laAlarmAlarm.Caption := '';
          // Grenzlinie ausblenden
        mShiftBand.Frame.DrawTop := false;
      end else begin
          // Grenzlinie einblenden
        mShiftBand.Frame.DrawTop := true;
  
        xFound := false;
        mActualAlarmView := nil;
        repeat
            // "Zeiger" auf den n�chsten Alarm
          inc(mAlarmIndex);
          if (mAlarmIndex < mActualShiftView.AlarmViewCount) then begin
              // zu druckenden Alarm f�r die sp�tere Verwendung sichern
            mActualAlarmView := mActualShiftView.AlarmViews[mAlarmIndex];
  
            if (IgnoreAdvice) or (amPrinter in mActualAlarmView.AdviceMethods) then
              xFound := true;
          end else begin
            mActualAlarmView := nil;
          end;// if (mAlarmIndex < mActualShiftView.AlarmViewCount) then begin
        until (mAlarmIndex > mActualShiftView.AlarmViewCount) or xFound;
  
        if assigned(mActualAlarmView) then begin
            // Wenn ein neuer Alarm gedruckt werden soll, m�ssen alle untergeordneten Daten gedruckt werden
          mMoreStyles    := false;
          mMoreLots      := false;
          mMoreDataItems := false;
          mMoreMethods   := false;
  
          mStyleIndex  := -1;
          mLotIndex    := -1;
          mDIIndex     := -1;
          mMethodIndex := -1;
          mDataItemList.Clear;
  
    //        mActualAlarmView := mActualShiftView.AlarmViews[mAlarmIndex];
  
          laAlarmAlarm.Caption := mActualAlarmView.QOVisualize.Caption;
        end else begin
          mActualAlarmView := nil;
        end;// if assigned(mActualAlarmView) then begin
      end;// if (xMoreDataItemsMirror or xMoreStylesMirror or xMoreLotsMirror or xMoreMethodsMirror) then begin
  
      if assigned(mActualAlarmView) then begin
  
    // Artikel
        if (mMoreLots or mMoreDataItems or mMoreMethods) then begin
            // Einen Leerstring ausgeben, wenn noch untergeordnete Daten zu drucken sind.
          laStyleAlarm.Caption := '';
        end else begin
            // Bei einem neuen Artikel werden die untergeordnetetn Daten neu aufbereitet
          mMoreLots      := false;
          mMoreDataItems := false;
          mMoreMethods   := false;
  
          mLotIndex    := -1;
          mDIIndex     := -1;
          mMethodIndex := -1;
  
            // "Zeiger" auf den n�chsten Artikel
          inc(mStyleIndex);
          if (mStyleIndex < mActualAlarmView.AllStyles.Count)then begin
            GetStyleData;
            mMoreStyles := (mStyleIndex < (mActualAlarmView.AllStyles.Count - 1));
          end;// if (mStyleIndex < mActualAlarmView.AllStyles.Count)then begin
        end;// if (xMoreLotsMirror or xMoreDataItemsMirror or xMoreMethodsMirror) then begin

//: ----------------------------------------------
    // Lot
        if (mMoreDataItems or mMoreMethods) then begin
            // Einen Leerstring ausgeben, wenn noch untergeordnete Daten zu drucken sind.
          laLotAlarm.Caption := '';
        end else begin
            // "Zeiger" auf die n�chste Partie
          inc(mLotIndex);
          if (mLotIndex < mStyle.ProdIDList.ProdIDCount) then begin
              // DataItems und Methoden werden neu ermittelt
            mMoreDataItems := false;
            mMoreMethods   := false;
  
            mDIIndex     := -1;
            mMethodIndex := -1;
            mDataItemList.Clear;
  
              // Stellt die Maschine mit dem Spindelbereich dar
            GetLotData;
          end;// if (mLotIndex < mStyle.ProdIDList.ProdIDCount) then begin
        end;// if (xMoreDataItemsMirror or xMoreMethodsMirror) then begin
        mMoreLots := (mLotIndex < (mStyle.ProdIDList.ProdIDCount - 1));

//: ----------------------------------------------
    // DataItem
        if (mMoreMethods) then begin
            // Einen Leerstring ausgeben, wenn noch untergeordnete Daten zu drucken sind.
    //        laDIAlarm.Caption := '';
          SetDIText;
        end else begin
            // Sucht das DataItem und gibt es aus
          if (GetDataItemData) then begin
              // Die Methoden m�ssen neu ermittelt werden
            mMoreMethods   := false;
  
            mMethodIndex := -1;
          end;// if (xDataItemFound) then begin
        end;// if (xMoreMethodsMirror) then begin
        mMoreDataItems := (mDIIndex < mDataItemList.Count - 1);

//: ----------------------------------------------
    // Method
          // "Zeiger" auf die n�chste Methode
        inc(mMethodIndex);
        if (mMethodIndex < mMethodList.Count) then begin
            // Stellt die Daten der Methode dar
          GetMethodData
        end;// if (mMethodIndex < mMethodList.Count) then begin
        mMoreMethods := (mMethodIndex < (mMethodList.Count - 1));

//: ----------------------------------------------
  // ZusatzInfo Alarm
  
        // Bei einem neuen Alarm ein bisschen Platz lassen
        memoAlarmEventInfo.Lines.Text := '';
        if (not(mMoreStyles or mMoreLots or mMoreDataItems or mMoreMethods))then begin
            (* Kommentar ins Memo schreiben. Das Band vergr�ssert sich vertikal automatisch
               wenn mehrere Zeilen geschrieben werden m�ssen. *)
          if (mActualAlarmView.Info > '') then begin
              if (mShiftIndex < (mShiftViewList.Count)) or (mAlarmIndex < (mActualShiftView.AlarmViewCount - 1)) then
                memoAlarmEventInfo.Lines.Text := mActualAlarmView.Info + cCrlf + '  '
              else
                memoAlarmEventInfo.Lines.Text := mActualAlarmView.Info;
          end else begin
              // Band verkleinern, wenn es keinen Kommentar gibt
            mShiftBand.Height := memoAlarmEventInfo.Top;
          end;// if (mActualAlarmView.Info > '') then begin
        end else begin
          memoAlarmEventInfo.Caption := '';
          mShiftBand.Height := memoAlarmEventInfo.Top;
        end;// if (not(mMoreStyles or mMoreLots or mMoreDataItems or mMoreMethods))then begin
  
      end else begin
          // Bei einem Fehler die Ausgabe stoppen
        MoreData := false;
      end;// if assigned(mActualAlarmView) then begin

//: ----------------------------------------------
    except
      MoreData := false;
    end;// try except

//: ----------------------------------------------
  end else begin
    mMoreDataItems := false;
    mMoreStyles    := false;
    mMoreLots      := false;
    mMoreMethods   := false;
  end;// if MoreData then begin
  
    // Den Ausdruck abbrechen, wenn die ESCAPE-Taste gedr�ckt wurde.
  if (MoreData) and (GetAsyncKeyState(VK_ESCAPE) <> 0) then begin
    MoreData := false;
    mQRShift.Cancel;
    mmMessageDlg(rsPrintAborted, mtInformation, [mbOK], 0, self);
  end;// if (MoreData) and (GetAsyncKeyState(VK_ESCAPE) <> 0) then begin

//: ----------------------------------------------
    (*// Bei einem neuen Alarm ein bisschen Platz lassen
    if (mMoreStyles or mMoreLots or mMoreDataItems or mMoreMethods) then
      mShiftBand.Height := mNormalShiftBandHeigth
    else
      mShiftBand.Height := mNormalShiftBandHeigth + 10;*)
end;// TfrmQOReportShiftList.mShiftBandNeedData cat:No category

//:-------------------------------------------------------------------
(*: Member:           mShiftListTitleBeforePrint
 *  Klasse:           TfrmQOReportShiftList
 *  Kategorie:        No category 
 *  Argumente:        (Sender, PrintBand)
 *
 *  Kurzbeschreibung: Wird aufgerufen um den Titelbereich zu initialisieren (Liste der verf�gbaren Methoden)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOReportShiftList.mShiftListTitleBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
var
  i: TCalculationMethod;
begin
  // Initialisieren
  memMethods.Lines.Clear;
  memMethodNr.Lines.Clear;
  
  // Im Titel wird jede Methode als Legende eingetragen
  for i := Low(TCalculationMethod) to High(TCalculationMethod) do begin
    memMethodNr.Lines.Add(IntToStr(ord(i) + 1));
    memMethods.Lines.Add('= ' + Translate(cCalculationMethods[i].Name));
  end;// for i := Low(TCalculationMethod) to High(TCalculationMethod) do begin
end;// TfrmQOReportShiftList.mShiftListTitleBeforePrint cat:No category

//:-------------------------------------------------------------------
(*: Member:           Print
 *  Klasse:           TfrmQOReportShiftList
 *  Kategorie:        No category 
 *  Argumente:        (aWithSetup)
 *
 *  Kurzbeschreibung: Druckt die Schichten aus die zuvor der Liste hinzugef�gt wurden (siehe AddShift())
 *  Beschreibung:     
                      Mit dem Property 'IgnoreAdvice' kann festgelegt werden ob ein Alarm auch dann ausgedruckt
                      werden soll,
                      wenn es in den Settings nicht definiert ist (AdviceMethod).
 --------------------------------------------------------------------*)
procedure TfrmQOReportShiftList.Print(aWithSetup: boolean);
begin
  (*Printer.Orientation := poLandscape;
  QRPrinter.Orientation := poLandscape;
  mQRShift.Page.Orientation := poLandscape;*)
  
  // Initialisieren
  mDIIndex     := -1;
  mLotIndex    := -1;
  mMethodIndex := -1;
  mShiftIndex  := -1;
  mStyleIndex  := -1;
  mDataItemList.Clear;
  
  mNormalShiftBandHeight := mShiftBand.Height;
  
  // Das Sammeln der Daten kann unter Umstanden (viele Schichten) l�nger dauern
  try
    Screen.Cursor := crHourGlass;
    GetAsyncKeyState(VK_ESCAPE);
    PrintQRWithSetupDialog(mQRShift, aWithSetup, poLandscape);
  finally
    Screen.Cursor := crDefault;
  end;// finally
end;// TfrmQOReportShiftList.Print cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetDIText
 *  Klasse:           TfrmQOReportShiftList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Schreibt das DataItem in den Report
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmQOReportShiftList.SetDIText;
begin
  laDIAlarm.Caption := TranslateDataItemDisplayName(mDataItemList[mDIIndex]);
end;// TfrmQOReportShiftList.SetDIText cat:No category

end.




























