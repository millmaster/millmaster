inherited frmMethodAVGFrame: TfrmMethodAVGFrame
  Height = 170
  inherited mMethodNamePanel: TmmPanel
    inherited laMethodName: TmmStaticText
      Width = 132
      Caption = '(*)Prozentuale Abweichung'
    end
  end
  inherited mMethodPanel: TmmPanel
    Height = 152
    inherited mmBevel1: TmmBevel
      Top = 76
    end
    object mmLabel1: TmmLabel [1]
      Left = 73
      Top = 87
      Width = 93
      Height = 13
      Caption = '(*)Anzahl Schichten'
      FocusControl = edNumShifts
      Visible = True
      AutoLabel.Distance = 4
      AutoLabel.LabelPosition = lpLeft
    end
    object laUnit: TmmLabel [2]
      Left = 232
      Top = 120
      Width = 6
      Height = 13
      Caption = '0'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel2: TmmLabel [3]
      Left = 18
      Top = 119
      Width = 146
      Height = 13
      Caption = '(30)minimal produzierte Laenge'
      FocusControl = edProdYarnLength
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    inherited paCalcMethodFrame: TmmPanel
      Left = 40
      Top = 1
      Width = 200
      Height = 72
    end
    inherited mInfoPanel: TmmPanel
      Height = 150
      inherited mmPanel3: TmmPanel
        Height = 148
      end
      inherited edMethodDescription: TmmMemo
        Height = 148
      end
    end
    object edNumShifts: TmmEdit
      Left = 168
      Top = 83
      Width = 57
      Height = 21
      Color = clWindow
      TabOrder = 2
      Visible = True
      OnChange = GUIChange
      Alignment = taRightJustify
      AutoLabel.Control = mmLabel1
      AutoLabel.LabelPosition = lpLeft
      NumMode = True
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
    end
    object edProdYarnLength: TmmEdit
      Left = 168
      Top = 115
      Width = 57
      Height = 21
      Color = clWindow
      TabOrder = 3
      Visible = True
      OnChange = GUIChange
      Alignment = taRightJustify
      AutoLabel.Control = mmLabel2
      AutoLabel.Distance = 4
      AutoLabel.LabelPosition = lpLeft
      NumMode = True
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
    end
  end
  inherited mTranslator: TmmTranslator
    Left = 288
  end
end
