(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: AnalyzeChartFrame
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Frame f�r die DArstellungs eines Alarms (nur ein Artikel)
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 16.09.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit AnalyzeChartFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, QOGUIClasses,
  ExtCtrls, TeeProcs, TeEngine, Chart, mmChart, ComCtrls, mmTabControl, AdoDBAccess,
  QOChartSeries, LoepfeGlobal, mmcs, MethodContainerFrame, mmSplitter, QOKernel,
  mmPanel, StdCtrls, mmLabel, QODetailInfoForm, IvDictio, IvMulti,
  IvEMulti, mmTranslator, mmVCLStringList, ComObj, Buttons, mmBitBtn,
  ImgList, mmImageList, ActnList, mmActionList;

type
  (*: Klasse:        TfrmAnalyzeChartFrame
      Vorg�nger:     TFrame
      Kategorie:     No category
      Kurzbeschrieb: Frame f�r die visualisierung der Alarm Daten eines Artikels oder einer Maschine 
      Beschreibung:  
                     - *)
  TfrmAnalyzeChartFrame = class(TFrame)
    mBottomPanel: TmmPanel;
    mDataItemChart: TmmChart;
    mDataItemTabs: TmmTabControl;
    mMethodContainer: TfrmMethodContainerFrame;
    mmLabel1: TmmLabel;
    mmSplitter1: TmmSplitter;
    mmTranslator1: TmmTranslator;
    slParameters: TmmVCLStringList;
    //1 Schlatet die vergr�sserten Datenpunkte der entsprechenden Serie ein 
    procedure mDataItemChartClickLegend(Sender: TCustomChart; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    //1 Zeigt Detail Infos des angeklickten Datenpunktes an 
    procedure mDataItemChartClickSeries(Sender: TCustomChart; Series: TChartSeries; ValueIndex: Integer; Button:
      TMouseButton; Shift: TShiftState; X, Y: Integer);
    //1 Initialisiert die Anzeige eines neuen DataItems 
    procedure mDataItemTabsChange(Sender: TObject);
  private
    FMethodImages: TImageList;
    mAlarm: TQOAlarmView;
    mDataItemList: TStringList;
    mDataServer: TAnalyzeChartDataServer;
    mDetailInfo: TfrmDetailInfo;
    mMethods: TQOMethods;
  protected
    //1 Wird aufgerufen, wenn das DetailInfoForm geschlossen wird 
    procedure DetailInfoHide(Sender: TObject);
    //1 Wird aufgerufen, wenn eine Methode (Settings) auf- oder zugeklappt wird 
    procedure ExpandCollapseMethod(Sender: TObject; aExpanded: boolean);
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Liefert die Settings f�r die Schnittstelle zum Labreport 
    function GetLabReportSettings(aWindowHandle: THandle): String;
    //1 Initialisiert das Chart 
    procedure InitChart(aSelectedTabName: string);
    //1 Wird von "aussen" aufgerufen wenn eine Maschine visualisiert werden soll 
    procedure ShowMachine(aAlarm: TQOAlarmView; aAssignObject, aMachineObject: TQOAssignNodeObject);
    //1 Wird von "aussen" aufgerufen wenn ein Artikel visualisiert werden soll 
    procedure ShowStyle(aAlarm: TQOAlarmView; aAssignObject: TQOAssignNodeObject);
    //1 ImageList mit den Bitmaps f�r die Methoden (Aus dem MainWindow) 
    property MethodImages: TImageList read FMethodImages write FMethodImages;
  end;
  
implementation

uses
  QODef, SettingsReader, MMUGlobal, QOGUIShared, typinfo, IcXMLParser, mmSeries, series, 
  ClassDef, CalcMethods;

{$R *.DFM}

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmAnalyzeChartFrame
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmAnalyzeChartFrame.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

//: ----------------------------------------------
  mDataServer := TAnalyzeChartDataServer.Create(self);
  mMethods := TQOMethods.Create;
  mDataItemList := TStringList.Create;
end;// TfrmAnalyzeChartFrame.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmAnalyzeChartFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmAnalyzeChartFrame.Destroy;
begin
  FreeAndNil(mDataServer);
  
  // Methoden
  mMethods.Clear;
  FreeAndNil(mMethods);
  FreeAndNil(mDetailInfo);
  FreeAndNil(mDataItemList);

//: ----------------------------------------------
  inherited Destroy;
end;// TfrmAnalyzeChartFrame.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           DetailInfoHide
 *  Klasse:           TfrmAnalyzeChartFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn das DetailInfoForm geschlossen wird
 *  Beschreibung:     
                      Gleichzeitig mit dem Schliessen des DatailInfoForm muss auch die 
                      Markierung des Datenpunktes gel�scht werden.
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeChartFrame.DetailInfoHide(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to mDataItemChart.SeriesCount - 1 do
    mDataItemChart.SeriesEx[i].DrawClickPoint;
end;// TfrmAnalyzeChartFrame.DetailInfoHide cat:No category

//:-------------------------------------------------------------------
(*: Member:           ExpandCollapseMethod
 *  Klasse:           TfrmAnalyzeChartFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aExpanded)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn eine Methode (Settings) auf- oder zugeklappt wird
 *  Beschreibung:     
                      Die H�he des Panels wird an den Platzbedarf angepasst.
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeChartFrame.ExpandCollapseMethod(Sender: TObject; aExpanded: boolean);
var
  xAdditionalRange: Integer;
  xHeight: Integer;
begin
  // Zus�tzliche Steuerelemente mitberechnen
  xAdditionalRange := mBottomPanel.ClientHeight - mMethodContainer.Height;
  
  // Die ben�tigte H�he f�r die Darstellung aller Methoden berechnen
  xHeight := xAdditionalRange + mMethodContainer.ContainerHeight;
  // Begranzen, wenn die H�he mehr als 50% des Charts beansprucht
  if xHeight > mDataItemTabs.ClientHeight div 2 then
    xHeight := mDataItemTabs.ClientHeight div 2;
  
  // Neue H�he zuweisen
  mBottomPanel.Height := xHeight;
end;// TfrmAnalyzeChartFrame.ExpandCollapseMethod cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetLabReportSettings
 *  Klasse:           TfrmAnalyzeChartFrame
 *  Kategorie:        No category 
 *  Argumente:        (aWindowHandle)
 *
 *  Kurzbeschreibung: Liefert die Settings f�r die Schnittstelle zum Labreport
 *  Beschreibung:     
                      Die Settings werden in einer Stringliste gespeichert die wie ein INI-File aufgebaut ist.
                      So kann der LabReport diese Liste in ein MemIniFile �bernehmen, und die Settings lesen.
                      Die Struktur ist im Dokument "Schnittstelle QOfflimit - LabReport.doc" beschrieben.
                      
                      Um die Daten der DataItems (inkl. Methoden Settings) zu erhalten, wird jedes DataItem einmal
                      angezeigt um den DataServer zu Initialisieren und die XML Daten der Methoden Settings zu lesen.
                      Die eigentlichen Daten werden dann von den Methoden abgefragt (�ber die Frames). Dadurch flackert
                      der
                      Bildschirm kurz, da ja die Daten angezeigt werden.
                      
                      ---------------------------------------------------------
                      Beispiel f�r die Zusammensetung eines Strings:
                      [Basic]
                      DataItems=bA1,cA1,uA1,xCN,xI20_70,xISmall,xIThin
                      Active=bA1
                      XAxis=time
                      ZAxis=prod_name
                      StyleID=119
                      TimeFrom=38000.9166666667
                      TimeTo=38008.75
                      EventTime=38004.8333333333
                      LotID=-2147481492
                      WindowHandle=...
                      [bA1]
                      ClassDefects=2
                      MethodCount=1
                      Method0=Method=0,BorderCondition=1,LengthUnit=0,LenBase=2,LimitValue=0.000000001
                      [cA1]
                      ClassDefects=0
                      MethodCount=1
                      Method0=Method=0,BorderCondition=1,LengthUnit=0,LenBase=2,LimitValue=0.000001
                      [uA1]
                      ClassDefects=1
                      MethodCount=1
                      Method0=Method=0,BorderCondition=1,LengthUnit=0,LenBase=2,LimitValue=0.0000000001
                      [xCN]
                      MethodCount=1
                      Method0=Method=1,BorderCondition=1,LengthUnit=0,LenBase=2,Deviation=-10,MinProdYarnLength=105000,
                      ShiftCompression=2
                      [xI20_70]
                      MethodCount=1
                      Method0=Method=0,BorderCondition=1,LengthUnit=0,LenBase=2,LimitValue=0.001
                      [xISmall]
                      MethodCount=1
                      Method0=Method=0,BorderCondition=1,LengthUnit=0,LenBase=2,LimitValue=1
                      [xIThin]
                      MethodCount=1
                      Method0=Method=0,BorderCondition=1,LengthUnit=0,LenBase=2,LimitValue=1
 --------------------------------------------------------------------*)
function TfrmAnalyzeChartFrame.GetLabReportSettings(aWindowHandle: THandle): String;
var
  xProdIDList: TProdIDList;
  xLotString: String;
  i: Integer;
  j: Integer;
  xTimeTo: TDateTime;
  xOldDataItemIndex: Integer;
  xDataItems: TStringList;
  
  function GetDataItemName(aDataServer: TAnalyzeChartDataServer): String;
  begin
    Result := aDataServer.DataItemItemName;
    case TClassDefects(mDataServer.ClassDefects) of
      cdCut   : result := 'c' + result;
      cdUncut : result := 'u' + result;
      cdBoth  : result := 'b' + result;
    else
      result := 'x' + result;
    end;// case TClassDefects(mDataServer.ClassDefects) of
  end;// function GetDataItemName(aDataServer: TAnalyzeChartDataServer): String;
  
begin
  (* Da die DataItems nur mit dem DisplayName zur Verf�gung stehen, muss eine Liste angelegt werden
     um die ItemNames der DataItems zu sammeln sobald sie angezeigt werden *)
  xDataItems := TStringList.Create;
  try

//: ----------------------------------------------
    // Platz machen f�r die neuen Settings
    slParameters.Strings.Clear;
  
    // Erste Sektion und fixe Parameter schreiben
    slParameters.Strings.Add('[Basic]');
    slParameters.Strings.Add('XAxis=time');
    slParameters.Strings.Add('ZAxis=prod_name');
    slParameters.Strings.Add('WindowHandle=' + IntToStr(aWindowHandle));

//: ----------------------------------------------
    // Artikel der dargestellt wird (Wird im LR nur f�r die Berechnung ben�tigt und nicht wird dargestellt)
    slParameters.Strings.Values['StyleID'] := mDataServer.Style.LinkID;
    // Anfang des Datenfesnsters
    slParameters.Strings.Values['TimeFrom']  := MMFloatToStr(mDataServer.TimeFrom);
    // Ende des Datanfensters (Alarm liegt in der Mitte des Datenfensters)
    xTimeTo := mDataServer.TimeTo + mDataServer.TimeTo - mDataServer.TimeFrom;
    slParameters.Strings.Values['TimeTo']    := MMFloatToStr(xTimeTo);
    // Datum des Alarms
    slParameters.Strings.Values['EventTime'] := MMFloatToStr(mDataServer.TimeTo);

//: ----------------------------------------------
    // Die Liste der ProdID's muss je nach Anzeigemodus von der Maschine oder vom Artikel verwendet werden
    if mDataServer.AssignShowMode = smMachine then
      xProdIDList := mDataServer.Machine.ProdIDList
    else
      xProdIDList := mDataServer.Style.ProdIDList;
  
    // Alle ProdIDs ermitteln
    xLotString := '';
    for i := 0 to xProdIDList.ProdIDCount - 1 do
      xLotString := xLotString + ',' + IntToStr(xProdIDList[i].ProdID);
    if xLotString > '' then
      system.delete(xLotString, 1, 1);
    // Prod IDs �bergeben
    slParameters.Strings.Values['LotID']     := xLotString;

//: ----------------------------------------------
    // Index des aktiven Tabs merken
    xOldDataItemIndex := mDataItemTabs.TabIndex;
    // Nacheinander alle Tabs anw�hlen, damit die Daten aufbereitet werden
    for i := 0 to mDataServer.DataItemList.Count - 1 do begin
      mDataItemTabs.TabIndex := i;
      // Immer auch ein Change von Hand ausl�sen (wird vom TabIndex nicht ausgel�st)
      mDataItemTabsChange(self);
  
      // Name des DataItems f�r den Namen der Sektion
      slParameters.Strings.Add('[' + GetDataItemName(mDataServer) + ']');
      // Name des DataItems f�r die kommaseparierte Liste
      xDataItems.Add(GetDataItemName(mDataServer));
      // Nur bei Matrix Items werden die ClassDefects angegeben
      if mDataServer.ClassDefects >= 0 then
        slParameters.Strings.Add('ClassDefects=' + IntToStr(mDataServer.ClassDefects));
  
      // Zuerst muss mal bekannt sein, wieviele Methoden vorhanden sind
      slParameters.Strings.Add('MethodCount=' + IntToStr(mMethodContainer.MethodCount));
      // ... dann muss f�r jede Methode ein eigener Settingsstring zusammengesetz werden
      for j := 0 to mMethodContainer.MethodCount - 1 do
        slParameters.Strings.Add(Format('Method%d=%s', [j, mMethodContainer.MethodFrame[j].GetMethodSettings]));
    end;// for i := 0 to mDataServer.DataItemList.Count - 1 do begin
  
    // Alten Zustand wieder herstellen
    mDataItemTabs.TabIndex := xOldDataItemIndex;
    mDataItemTabsChange(self);
  
    // Die Liste mit allen DataItems
    slParameters.Strings.Insert(1, 'DataItems=' + xDataItems.CommaText);
    // Das aktive DataItem angeben (Dieses DataItem wird im LybReport angezeigt)
    slParameters.Strings.Insert(2, 'Active=' + xDataItems[mDataItemTabs.TabIndex]);

//: ----------------------------------------------
  finally
    FreeAndNil(xDataItems);
  end;// try finally
  
  // Settings zur�ckgeben
  result := slParameters.CommaText;
  
  CodeSite.SendStringList('Lab Report Settings', slParameters.Strings);
end;// TfrmAnalyzeChartFrame.GetLabReportSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           InitChart
 *  Klasse:           TfrmAnalyzeChartFrame
 *  Kategorie:        No category 
 *  Argumente:        (aSelectedTabName)
 *
 *  Kurzbeschreibung: Initialisiert das Chart
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeChartFrame.InitChart(aSelectedTabName: string);
var
  xOldTabIndex: Integer;
  i: Integer;
begin
  // Zoomfaktor wieder zur�ckstellen
  mDataItemChart.UndoZoom;
  // Aktuell selektierter Index
  xOldTabIndex := mDataItemTabs.TabIndex;
  
  // ... und f�r jedes DataItem ein Tab bereitstellen
  mDataItemList.Assign(mDataServer.DataItemList);
  mDataItemTabs.Tabs.Assign(mDataServer.DataItemList);
  
  for i := 0 to mDataItemTabs.Tabs.Count - 1 do
    mDataItemTabs.Tabs[i] := TranslateDataItemDisplayName(mDataItemTabs.Tabs[i]);
  
  (* vorher selektiertes Tab wiederherstellen
     (Wenn weniger Tabs existieren wird das erste Tab gew�hlt) *)
  if mDataItemTabs.Tabs.IndexOf(aSelectedTabName) >= 0 then
    // Selektiert das Tab mit dem selben Namen des vorher selektierten DataITems
    mDataItemTabs.TabIndex := mDataItemTabs.Tabs.IndexOf(aSelectedTabName)
  else
    // Selektiert das selbe TAb das vorher selektiert war
    mDataItemTabs.TabIndex := xOldTabIndex;
  
  // Anzeige ausl�sen
  mDataItemTabsChange(self);
end;// TfrmAnalyzeChartFrame.InitChart cat:No category

//:-------------------------------------------------------------------
(*: Member:           mDataItemChartClickLegend
 *  Klasse:           TfrmAnalyzeChartFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Button, Shift, X, Y)
 *
 *  Kurzbeschreibung: Schlatet die vergr�sserten Datenpunkte der entsprechenden Serie ein
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeChartFrame.mDataItemChartClickLegend(Sender: TCustomChart; Button: TMouseButton; Shift:
  TShiftState; X, Y: Integer);
var
  xIndex: Integer;
  i: Integer;
begin
  // Index der angeklickten Serie erfragen
  xIndex := Sender.Legend.Clicked(X, Y);
  if (xIndex >= 0) and (xIndex < Sender.SeriesCount) then begin
    if (Sender.Series[xIndex] is TmmLineSeries) then begin
      // Alle Markierungen l�schen
      for i := 0 to Sender.SeriesCount - 1 do
        TmmChart(Sender).SeriesEx[i].ShowClickPoint :=  false;
      // Angeklickte Serie markieren
      TmmChart(Sender).SeriesEx[xIndex].ShowClickPoint :=  true;
    end;// if (Sender.Series[xIndex] is TmmLineSeries) then begin
  end;// if (xIndex >= 0) and (xIndex < Sender.SeriesCount) then begin
end;// TfrmAnalyzeChartFrame.mDataItemChartClickLegend cat:No category

//:-------------------------------------------------------------------
(*: Member:           mDataItemChartClickSeries
 *  Klasse:           TfrmAnalyzeChartFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Series, ValueIndex, Button, Shift, X, Y)
 *
 *  Kurzbeschreibung: Zeigt Detail Infos des angeklickten Datenpunktes an
 *  Beschreibung:     
                      Die Detail Infos werden erst angezeigt, wenn die Serie in der Legende angecklickt 
                      wurde und die Markierungen sichtbar sind.
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeChartFrame.mDataItemChartClickSeries(Sender: TCustomChart; Series: TChartSeries; ValueIndex:
  Integer; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if not(assigned(mDetailInfo)) then begin
    mDetailInfo := TfrmDetailInfo.Create(self);
    mDetailInfo.OnHide := DetailInfoHide;
  end;// if not(assigned(mDetailInfo)) then begin
  
  if assigned(mDetailInfo) then begin
    if Series is TmmLineSeries then begin
      if TmmLineSeries(Series).Pointer.Visible then begin
        with mDataItemChart.ClientToScreen(Point(X, Y)) do
          mDetailInfo.ShowInfo(Series, X, Y, ValueIndex);
        mDataItemChart.SeriesEx[mDataItemChart.SeriesList.IndexOf(Series)].DrawClickPoint;
      end;// if TmmLineSeries(Series).Pointer.Visible then begin
    end;// if Series is TmmLineSeries then begin
  end;// if assigned(mDetailInfo) then begin
end;// TfrmAnalyzeChartFrame.mDataItemChartClickSeries cat:No category

//:-------------------------------------------------------------------
(*: Member:           mDataItemTabsChange
 *  Klasse:           TfrmAnalyzeChartFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Initialisiert die Anzeige eines neuen DataItems
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeChartFrame.mDataItemTabsChange(Sender: TObject);
var
  xStream: TStringStream;
  xDoc: TIcXMLDocument;
  xParser: TIcXMLParser;
  xXMLMethod: TIcXMLelement;
  xXMLMethodSettings: TIcXMLelement;
  xXMLItem: TIcXMLelement;
  xAttribute: TIcXMLAttr;
  xMethodName: String;
  xMethodIndex: Integer;
  xDataItemFound: Boolean;
begin
  try
    Screen.Cursor := crHourglass;
    // DataItem anzeigen
    mDataServer.ShowDataItem(mDataItemList[mDataItemTabs.TabIndex], mDataItemChart);
    //ShowDataItem(mDataItemList[mDataItemTabs.TabIndex]);
  
    // Settings
    if assigned(mAlarm) then begin
    { Beispiel:
          <?xml version = "1.0"?>
          <QOSettings>
             <QOItem Name = "A3 ((*)Beide)">
                <Method Type = "cmFix">
                   <MethodSettings Active = "True" ShowDetails = "msdFull" ShiftCount = "1" LimitValue = "3"/>
                </Method>
                <Method Type = "cmAVG">
                   <MethodSettings Active = "True" ShowDetails = "msdFull" ShiftCount = "1" Tolerance = "-80" MinProdYarnLength = "1000" ShiftCountAVG = "2"/>
                </Method>
             </QOItem>
          </QOSettings>
    }
      // intialisieren
      xDoc := nil;
      xParser := nil;
      mMethods.Clear;
      try
        // Parser und ein "virtuelles" Dokument erzeugen
        xParser := TIcXMLParser.Create(nil);
        xDoc := TIcXMLDocument.Create;
        xDoc.AssignParser(xParser);
  
        // XML String in einen Stream laden
        xStream := TStringStream.Create(mAlarm.AlarmSettings);
        try
            // Parser starten. Das Ergebnis wird im Dokument abgelegt
          if xParser.Parse(xStream, xDoc) then begin
  
            // DataItem im XML Stream suchen (bei mehreren DataItems f�r einen Artikel)
            xDataItemFound := false;
            // Item Knoten bestimmen (zB: '<QOItem Name = "A3 ((*)Beide)">')
            xXMLItem := xDoc.GetDocumentElement.GetFirstChild;
            while (assigned(xXMLItem)) and (not(xDataItemFound)) do begin
              xAttribute := xXMLItem.GetAttributeNode(cXMQOName);
              if assigned(xAttribute) then
                xDataItemFound := (xAttribute.Value = mDataItemList[mDataItemTabs.TabIndex]);
              if not(xDataItemFound) then
                xXMLItem := xXMLItem.NextSibling;
            end;// while (assigned(xXMLItem)) and (not(xDataItemFound)) do begin
  
            if assigned(xXMLItem) and (xDataItemFound) then begin
                // Ersten Knoten f�r eine Methode bestimmen (zB: '<Method Type = "cmFix">')
              xXMLMethod := xXMLItem.GetFirstChild;
              while assigned(xXMLMethod) do begin
                if assigned(xXMLMethod) then begin
                    // Typ der Methode bestimmen (Attribut der Methode zB: 'Type = "cmFix"')
                  xAttribute := xXMLMethod.GetAttributeNode(cXMLMethodType);
                  xMethodName := '';
                    // Methodenname bestimmen (Name der Enummeration)
                  if assigned(xAttribute) then
                    xMethodName := xAttribute.Value;
                  if xMethodName > '' then begin
                    try
                        // gew�nschte Methode zur Liste hinzuf�gen
                      xMethodIndex := mMethods.Add(mMethods.FactoryGetMethod(TCalculationMethod(GetEnumValue(TypeInfo(TCalculationMethod), xMethodName))));
                        // Und die Methoden Settings suchen (zB: '<MethodSettings Active = "True" ShowDetails = "msdFull" ShiftCount = "1" LimitValue = "3"/>')
                      xXMLMethodSettings := xXMLMethod.GetFirstChild;
                      if assigned(xXMLMethodSettings) then begin
                          // Die Settings an die jeweilige Methode weitergeben
                        mMethods[xMethodIndex].ReadXMLMethodeSettings(xXMLMethodSettings);
                      end;// if assigned(xXMLMethodSettings) then begin
                    except
                      CodeSite.SendError('TfrmAnalyzeChartFrame.mDataItemTabsChange: type Method(' + xMethodName + ') not found.');
                    end;// try except
                  end;// if xMethodName > '' then begin
                end;// if assigned(xXMLMethod) then begin
                  // Wenn mehrere Methoden beteiligt waren, dann die N�chste Methode suchen
                xXMLMethod := xXMLMethod.NextSibling;
              end;// while assigned(xXMLMethod) do begin
            end;// if assigned(xXMLItem) and (xDataItemFound) then begin
          end;// if xParser.Parse(xStream, xDoc) then begin
        finally
          xStream.Free;
        end;// try finally
      finally
        FreeAndNil(xDoc);
        FreeAndNil(xParser);
      end;// try finally
      mMethodContainer.OnExpandCollapseMethod := ExpandCollapseMethod;
      mMethodContainer.CalcMode := mDataServer.CalcMode;
      mMethodContainer.ShowMethodsReadOnly(mMethods, FMethodImages);
    end;// if assigned(mAlarm) then begin
  finally
    Screen.Cursor := crDefault;
  end;// try finally
end;// TfrmAnalyzeChartFrame.mDataItemTabsChange cat:No category

//:-------------------------------------------------------------------
(*: Member:           ShowMachine
 *  Klasse:           TfrmAnalyzeChartFrame
 *  Kategorie:        No category 
 *  Argumente:        (aAlarm, aAssignObject, aMachineObject)
 *
 *  Kurzbeschreibung: Wird von "aussen" aufgerufen wenn eine Maschine visualisiert werden soll
 *  Beschreibung:     
                      Bei einer einzelnen Maschine werden mehr zus�tzliche Informationen ben�tigt als
                      beim Artikel. So muss die Farbe f�r jede Produktionsgruppe bestimmt werden. Dies 
                      damit in der Artikel Grafik und in der Maschinen Grafik die jeweilige Produktionsgruppe 
                      dieselbe Farbe hat. Ausserdem muss nat�rlich der Artikel bekannt sein (�bersicht).
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeChartFrame.ShowMachine(aAlarm: TQOAlarmView; aAssignObject, aMachineObject: TQOAssignNodeObject);
var
  xOldTabName: String;
begin
  mAlarm := aAlarm;
  
  xOldTabName := mDataItemTabs.Tabs[mDataItemTabs.TabIndex];
  
  with mDataServer do begin
    Style := aAssignObject;
    // Aktuell selektierte Maschine
    Machine := aMachineObject;
    // Chart ist im Maschinen Modus
    AssignShowMode := smMachine;
  
    // Initialisiert die Anzeige und ruft 'mDataItemTabsChange' auf
    Initialize(aAlarm, IntToStr(aMachineObject.LinkID));
  end;// with DataServer do begin
  
  InitChart(xOldTabName);
end;// TfrmAnalyzeChartFrame.ShowMachine cat:No category

//:-------------------------------------------------------------------
(*: Member:           ShowStyle
 *  Klasse:           TfrmAnalyzeChartFrame
 *  Kategorie:        No category 
 *  Argumente:        (aAlarm, aAssignObject)
 *
 *  Kurzbeschreibung: Wird von "aussen" aufgerufen wenn ein Artikel visualisiert werden soll
 *  Beschreibung:     
                      Wird der Artikel angezeigt werden weniger zus�tzliche Informationen ben�tigt als
                      bei einer einzelnen Maschine.
 --------------------------------------------------------------------*)
procedure TfrmAnalyzeChartFrame.ShowStyle(aAlarm: TQOAlarmView; aAssignObject: TQOAssignNodeObject);
var
  xOldTabName: String;
begin
  mAlarm := aAlarm;
  
  xOldTabName := mDataItemTabs.Tabs[mDataItemTabs.TabIndex];
  
  with mDataServer do begin
    Style := aAssignObject;
    // Modus festlegen
    AssignShowMode := smStyle;
  
    // Initialisiert die Anzeige und ruft 'mDataItemTabsChange' auf
    Initialize(aAlarm, '');
  end;// with DataServer do begin
  
  InitChart(xOldTabName);
end;// TfrmAnalyzeChartFrame.ShowStyle cat:No category

end.









