unit MainWindow;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton, mmLabel, ExtCtrls, mmRadioGroup;

type
  TfrmMainWindow = class(TForm)
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmButton1: TmmButton;
    gbAnalyzeRecentShiftSave: TmmRadioGroup;
    procedure mmButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMainWindow: TfrmMainWindow;

implementation

uses
  comobj;

const
  cOLEQOfflimit = 'LOEPFE.MMQOfflimitServer';

{$R *.DFM}

procedure TfrmMainWindow.mmButton1Click(Sender: TObject);
var
  xQOfflimit: OleVariant;
  xAlarmCount: OleVariant;
begin
  try
    Screen.Cursor := crHourglass;
    try
      // Interface zum Labreport erzeugen
      xQOfflimit := CreateOLEObject(cOLEQOfflimit);
      // Labreport starten
      xQOfflimit.AnalyzeRecentShift((gbAnalyzeRecentShiftSave.ItemIndex = 0), xAlarmCount);
      ShowMessage('Anzahl Alarme = ' + IntToStr(xAlarmCount));
    except
      on e:Exception do begin
        showMessage(e.Message);
      end;
    end;// try except
  finally
    Screen.Cursor := crDefault;
    xQOfflimit := Unassigned;
  end;// try finally
end;

end.
