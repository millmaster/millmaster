object frmMainWindow: TfrmMainWindow
  Left = 245
  Top = 138
  Width = 454
  Height = 287
  Caption = 'QO Starter'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object mmLabel1: TmmLabel
    Left = 24
    Top = 16
    Width = 44
    Height = 13
    Caption = 'Funktion:'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel2: TmmLabel
    Left = 184
    Top = 16
    Width = 48
    Height = 13
    Caption = 'Parameter'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmButton1: TmmButton
    Left = 16
    Top = 48
    Width = 137
    Height = 25
    Caption = 'AnalyzeRecentShift'
    TabOrder = 0
    Visible = True
    OnClick = mmButton1Click
    AutoLabel.LabelPosition = lpLeft
  end
  object gbAnalyzeRecentShiftSave: TmmRadioGroup
    Left = 184
    Top = 40
    Width = 209
    Height = 41
    Caption = 'Ergebnisse'
    Columns = 2
    Items.Strings = (
      'Speichern'
      'Nicht Speichern')
    TabOrder = 1
  end
end
