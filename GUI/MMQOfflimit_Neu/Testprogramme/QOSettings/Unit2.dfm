object Form2: TForm2
  Left = 102
  Top = 210
  Width = 311
  Height = 485
  Caption = 'Form2'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object mmButton1: TmmButton
    Left = 208
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Alarm'
    TabOrder = 0
    Visible = True
    OnClick = mmButton1Click
    AutoLabel.LabelPosition = lpLeft
  end
  object mmButton2: TmmButton
    Left = 208
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Artikel'
    TabOrder = 1
    Visible = True
    OnClick = mmButton2Click
    AutoLabel.LabelPosition = lpLeft
  end
  object mmButton3: TmmButton
    Left = 208
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Methode'
    TabOrder = 2
    Visible = True
    OnClick = mmButton3Click
    AutoLabel.LabelPosition = lpLeft
  end
  object mTree: TmmVirtualStringTree
    Left = 0
    Top = 0
    Width = 200
    Height = 458
    Align = alLeft
    Header.AutoSizeIndex = 0
    Header.Font.Charset = DEFAULT_CHARSET
    Header.Font.Color = clWindowText
    Header.Font.Height = -11
    Header.Font.Name = 'MS Sans Serif'
    Header.Font.Style = []
    Header.MainColumn = -1
    Header.Options = [hoColumnResize, hoDrag]
    TabOrder = 3
    OnFreeNode = FreeNode
    OnGetText = TreeGetText
    OnGetImageIndex = GetImageIndex
    Columns = <>
  end
  object mmButton4: TmmButton
    Left = 208
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Items'
    TabOrder = 4
    Visible = True
    OnClick = mmButton4Click
    AutoLabel.LabelPosition = lpLeft
  end
end
