unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  VirtualTrees, mmVirtualStringTree, StdCtrls, mmButton;

type
  TForm2 = class(TForm)
    mmButton1: TmmButton;
    mmButton2: TmmButton;
    mmButton3: TmmButton;
    mTree: TmmVirtualStringTree;
    mmButton4: TmmButton;
    function AddVSTStructure(aVst: TmmVirtualStringTree;
      aNode: PVirtualNode; aObject: TObject): PVirtualNode;
    procedure FreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure GetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Kind: TVTImageKind; Column: TColumnIndex; var Ghosted: Boolean;
      var ImageIndex: Integer);
    procedure TreeGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure FormCreate(Sender: TObject);
    procedure mmButton1Click(Sender: TObject);
    procedure mmButton2Click(Sender: TObject);
    procedure mmButton4Click(Sender: TObject);
    procedure mmButton3Click(Sender: TObject);
  private
    function GetNodeData(aVst: TBaseVirtualTree;
      aNode: PVirtualNode): TObject;
  public
  end;

var
  Form2: TForm2;

implementation

uses
  QOSettings;

{$R *.DFM}

type
  PTreeData = ^TTreeData;
  TTreeData = record
    Item : TObject;
  end;// TTreeData = record

(*---------------------------------------------------------
  F�gt ein Objekt zum Tree hinzu
----------------------------------------------------------*)
function TForm2.AddVSTStructure(aVst: TmmVirtualStringTree; aNode: PVirtualNode; aObject: TObject): PVirtualNode;
var
  Data: PTreeData;
begin
  // Knoten hinzuf�gen
  Result := aVst.AddChild(aNode);

  aVst.FullExpand(aNode);
  aVst.FocusedNode := Result;
  aVst.SetFocus;

  // Jetz den Record abf�llen
  aVst.ValidateNode(Result, False);
  Data := aVst.GetNodeData(Result);
  if assigned(Data) then
    Data^.Item := aObject;
end;// function TForm2.AddVSTStructure(aVst: TmmVirtualStringTree; aNode: PVirtualNode; aDataItem: TObject): PVirtualNode;

(*---------------------------------------------------------
  Gibt das Icon f�r einen Knoten zur�ck
----------------------------------------------------------*)
procedure TForm2.GetImageIndex(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex;
  var Ghosted: Boolean; var ImageIndex: Integer);
begin
  if Kind <> ikOverlay then begin
    case Column of
      0: begin
      end;// 0
    else ;
    end;// case Column of
  end else begin
    // Overlay Behandlung
    case Column of
      0: begin
      end;// 0
    else ;
    end;// case Column of
  end;// if Kind <> ikOverlay then begin
end;// procedure TForm2.GetImageIndex(...)

(*---------------------------------------------------------
  Gibt das Objekt von TTreeData zur�ck
----------------------------------------------------------*)
function TForm2.GetNodeData(aVst: TBaseVirtualTree; aNode:PVirtualNode): TObject;
var
  xTreeData: PTreeData;
begin
  result := nil;
  xTreeData := aVst.GetNodeData(aNode);
  if assigned(xTreeData) then
    result := xTreeData^.Item;
end;

(*---------------------------------------------------------
  Gibt den Text f�r einen Knoten zur�ck
----------------------------------------------------------*)
procedure TForm2.TreeGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
var
  xObject: IQOVisualize;
  xQOObject: TObject;
begin
  xQOObject := GetNodeData(Sender, Node);
  if assigned(xQOObject) then begin
    if xQOObject.GetInterface(IQOVisualize, xObject) then begin
      CellText := xObject.Caption;
    end;// if xQOObject.GetInterface(IQOVisualize, xObject) then begin
  end;// if assigned(xQOObject) then begin
end;// procedure TForm2.TreeGetText(...)
(*---------------------------------------------------------*)

(*---------------------------------------------------------
  Gibt Speicher der nicht vom Record stammt zur�ck (strings, Objekte, Arrays, ...)
----------------------------------------------------------*)
procedure TForm2.FreeNode(
  Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  Data: PTreeData;
begin
  Data := Sender.GetNodeData(Node);
  if Assigned(Data) then
    ;
end;// procedure TForm2.FreeNode(...)

procedure TForm2.FormCreate(Sender: TObject);
begin
  mTree.NodeDataSize := SizeOf(TTreeData);
end;

procedure TForm2.mmButton1Click(Sender: TObject);
const
  cCounter: integer = 0;
var
  xQOAlarm: TQOAlarm;
begin
  xQOAlarm := TQOAlarm.Create;
  IQOVisualize(xQOAlarm).Caption := 'Alarm ' + IntToStr(cCounter);
  inc(cCounter);
  AddVSTStructure(mTree, mTree.FocusedNode, xQOAlarm);
end;

procedure TForm2.mmButton2Click(Sender: TObject);
const
  cCounter: integer = 0;
var
  xQOGroup: TQOGroup;
begin
  xQOGroup := TQOGroup.Create;
  IQOVisualize(xQOGroup).Caption := 'Artikel ' + IntToStr(cCounter);
  inc(cCounter);
  AddVSTStructure(mTree, mTree.FocusedNode, xQOGroup);
end;

procedure TForm2.mmButton4Click(Sender: TObject);
const
  cCounter: integer = 0;
var
  xQOItem: TQOItem;
begin
  xQOItem := TQOItem.Create;
  IQOVisualize(xQOItem).Caption := 'Item ' + IntToStr(cCounter);
  inc(cCounter);
  AddVSTStructure(mTree, mTree.FocusedNode, xQOItem);
end;

procedure TForm2.mmButton3Click(Sender: TObject);
const
  cCounter: integer = 0;
var
  xQOMethod: TQOMethod;
begin
  xQOMethod := TQOMethod.Create;
  IQOVisualize(xQOMethod).Caption := 'Methode ' + IntToStr(cCounter);
  inc(cCounter);
  AddVSTStructure(mTree, mTree.FocusedNode, xQOMethod);
end;

end.
