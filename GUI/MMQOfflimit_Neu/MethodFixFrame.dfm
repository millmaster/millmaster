inherited frmMethodFixFrame: TfrmMethodFixFrame
  Height = 169
  inherited mMethodNamePanel: TmmPanel
    inherited laMethodName: TmmStaticText
      Width = 109
      Caption = '(*)Absoluter Grenzwert'
    end
  end
  inherited mMethodPanel: TmmPanel
    Height = 151
    inherited mmBevel1: TmmBevel
      Top = 55
    end
    object mmLabel2: TmmLabel [1]
      Left = 71
      Top = 68
      Width = 93
      Height = 13
      Caption = '(*)Anzahl Schichten'
      FocusControl = edNumShifts
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mShiftDescr: TmmLabel [2]
      Left = 8
      Top = 88
      Width = 216
      Height = 57
      AutoSize = False
      BiDiMode = bdLeftToRight
      Caption = 
        '(*)Ueber diese Zahl der Schichten muss eine Grenzwertverletzung ' +
        'vorhanden sein, damit die Produktionseinheit im Q-Offlimit ausge' +
        'wiesen wird.'
      Color = clMenu
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentBiDiMode = False
      ParentColor = False
      ParentFont = False
      Visible = True
      WordWrap = True
      AutoLabel.LabelPosition = lpLeft
    end
    inherited paCalcMethodFrame: TmmPanel
      Left = 40
      Top = 1
      Width = 240
      Height = 40
    end
    inherited mInfoPanel: TmmPanel
      Height = 149
      inherited mmPanel3: TmmPanel
        Height = 147
      end
      inherited edMethodDescription: TmmMemo
        Height = 147
      end
    end
    object edNumShifts: TmmEdit
      Left = 168
      Top = 64
      Width = 57
      Height = 21
      Color = clWindow
      TabOrder = 2
      Visible = True
      OnChange = GUIChange
      Alignment = taRightJustify
      AutoLabel.Control = mmLabel2
      AutoLabel.Distance = 4
      AutoLabel.LabelPosition = lpLeft
      NumMode = True
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
    end
  end
end
