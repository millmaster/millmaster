unit MainMDIForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEAPPLMAIN, Menus, mmMainMenu, IvDictio, IvAMulti, IvBinDic,
  mmDictionary, IvMulti, IvEMulti, mmTranslator, mmLogin, mmPopupMenu,
  mmOpenDialog, IvMlDlgs, mmSaveDialog, mmPrintDialog,
  mmPrinterSetupDialog, StdActns, ActnList, mmActionList, ImgList,
  mmImageList, ComCtrls, mmStatusBar, ToolWin, mmToolBar, mmAnimate,
  ExtCtrls, mmPanel, MainWindow, QOKernel, QOGUIClasses, MMSecurity,
  MMHtmlHelp;

type
  TfrmMainMDIForm = class(TBaseApplMainForm)
    mSecurityDB: TMMSecurityDB;
    MMHtmlHelp: TMMHtmlHelp;
    procedure acNewExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure mDictionaryAfterLangChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMainMDIForm: TfrmMainMDIForm;

implementation
uses
  LoepfeGlobal;

{$R *.DFM}

procedure TfrmMainMDIForm.acNewExecute(Sender: TObject);
begin
  inherited;
  with TfrmMainWindow.Create(self) do begin
    WindowState := wsMaximized;
  end;
end;

procedure TfrmMainMDIForm.FormShow(Sender: TObject);
begin
  inherited;
  Caption := Format('%s (%s - %s)', [Caption, gSQLServerName, gDBName]);
  acNew.Execute;
end;

procedure TfrmMainMDIForm.mDictionaryAfterLangChange(Sender: TObject);
begin
  MMHtmlHelp.LoepfeIndex := mDictionary.LoepfeIndex;
end;

initialization

finalization
  TQOShiftSet.Instance.Clear;


end.
