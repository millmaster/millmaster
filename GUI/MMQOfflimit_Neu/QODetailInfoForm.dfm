inherited frmDetailInfo: TfrmDetailInfo
  Left = 624
  Top = 255
  BorderStyle = bsNone
  ClientHeight = 83
  ClientWidth = 304
  OnClose = FormClose
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  object pnMain: TmmPanel
    Left = 0
    Top = 0
    Width = 304
    Height = 83
    Align = alClient
    Color = clInfoBk
    TabOrder = 0
    OnMouseDown = ControlsMouseDown
    OnMouseMove = ControlsMouseMove
    object mmLabel1: TmmLabel
      Left = 8
      Top = 48
      Width = 50
      Height = 13
      Caption = '(*)Artikel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = True
      OnMouseDown = ControlsMouseDown
      OnMouseMove = ControlsMouseMove
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel2: TmmLabel
      Left = 8
      Top = 64
      Width = 82
      Height = 13
      Caption = '(*)Schichtstart'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = True
      OnMouseDown = ControlsMouseDown
      OnMouseMove = ControlsMouseMove
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel3: TmmLabel
      Left = 8
      Top = 32
      Width = 41
      Height = 13
      Caption = '(*)Wert'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = True
      OnMouseDown = ControlsMouseDown
      OnMouseMove = ControlsMouseMove
      AutoLabel.LabelPosition = lpLeft
    end
    object laStyle: TmmLabel
      Left = 128
      Top = 48
      Width = 6
      Height = 13
      Caption = '0'
      Visible = True
      OnMouseDown = ControlsMouseDown
      OnMouseMove = ControlsMouseMove
      AutoLabel.LabelPosition = lpLeft
    end
    object laShiftStart: TmmLabel
      Left = 128
      Top = 64
      Width = 6
      Height = 13
      Caption = '0'
      Visible = True
      OnMouseDown = ControlsMouseDown
      OnMouseMove = ControlsMouseMove
      AutoLabel.LabelPosition = lpLeft
    end
    object laValue: TmmLabel
      Left = 128
      Top = 32
      Width = 6
      Height = 13
      Caption = '0'
      Visible = True
      OnMouseDown = ControlsMouseDown
      OnMouseMove = ControlsMouseMove
      AutoLabel.LabelPosition = lpLeft
    end
    object laTitle: TmmLabel
      Left = 16
      Top = 8
      Width = 9
      Height = 16
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      Visible = True
      OnMouseDown = ControlsMouseDown
      OnMouseMove = ControlsMouseMove
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 144
    Top = 40
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
  object timClose: TmmTimer
    Enabled = False
    Interval = 120000
    OnTimer = timCloseTimer
    Left = 176
    Top = 40
  end
end
