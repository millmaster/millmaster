(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: ItemSettingsFrame.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Frame f�r die Darstellung eines Item Settings
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 06.05.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit ItemSettingsFrame;

interface

uses
  QOSettings, QOGUIShared,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, mmRadioGroup, StdCtrls, mmCheckBox,
  mmGroupBox, mmPanel,ComCtrls, ImgList, mmImageList, ActnList,
  mmActionList, ToolWin, mmToolBar, mmPageControl, mmDialogs,
  MethodContainerFrame, mmLineLabel, IvDictio, IvMulti, IvEMulti,
  mmTranslator;

type
  TfrmItemSettingsFrame = class;

  //1 Wird gefeuert nachdem ein DataItem gesichert wurde 
  TOnItemSaved = procedure (Sender: TfrmItemSettingsFrame; aItem: TQOItem) of object;
  (*: Klasse:        TfrmItemSettingsFrame
      Vorg�nger:     TFrame
      Kategorie:     View
      Kurzbeschrieb: Stellt die Settings eines Data Items dar 
      Beschreibung:  
                     - *)
  TfrmItemSettingsFrame = class (TFrame)
    acSave: TAction;
    acUndo: TAction;
    mButtonImages: TmmImageList;
    mClassDefSpacePanel: TmmPanel;
    mItemSettingsPanel: TmmPanel;
    mmActionList1: TmmActionList;
    mMethodContainer: TfrmMethodContainerFrame;
    mMethodHeader: TmmLineLabel;
    mmPageControl1: TmmPageControl;
    mmPanel1: TmmPanel;
    mmPanel2: TmmPanel;
    mmToolBar1: TmmToolBar;
    mTranslator: TmmTranslator;
    rgBoolOp: TmmRadioGroup;
    rgClassDefects: TmmRadioGroup;
    tabItemSettings: TTabSheet;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    //1 Speichert die ver�nderten Settings 
    procedure acSaveExecute(Sender: TObject);
    //1 L�dt die Settings wieder vom urspr�nglichen Alarm 
    procedure acUndoExecute(Sender: TObject);
    //1 Erfasst ob �nderungen an den Settings gemacht wurden 
    procedure ChangeFromGUI(Sender: TObject);
  private
    FImages: TImageList;
    FOnItemSaved: TOnItemSaved;
    FQOItem: TQOItem;
    //1 Zugriffsmethode f�r QOItem 
    procedure SetQOItem(Value: TQOItem);
  protected
    //1 Dispatch Methode f�r den Event OnItemSaved 
    procedure DoItemSaved(Sender: TfrmItemSettingsFrame; aItem: TQOItem); virtual;
    //1 Wird aufgerufen, wenn an einer Methode etwas ge�ndert wurde 
    procedure MethodChange(aSender: TFrame; aMethod: TQOMethod);
  public
    //1 Gibt True zur�ck, wenn das aktuelle Objekt auf der gespeichert ist 
    function saved: Boolean;
    //1 Speichert die Einstellungen im AlarmObjekt 
    function SaveSettings(aAskForPermission: boolean): Boolean;
    //1 True, wenn die Settings noch denen des �bergebenen Items entsprechen 
    function SettingsChanged: Boolean;
    //1 Zeigt die Einstellungen des �bergebenen DataItems an 
    procedure ShowItem(aQOItem: TObject);
    //1 Liste mit den Images die verwendet werden um die Methode anzuzeigen 
    property Images: TImageList read FImages write FImages;
    //1 Angezeigtes DataItem 
    property QOItem: TQOItem read FQOItem write SetQOItem;
  published
    //1 Wird gefeuert, wenn das DataItem gesichert wurde 
    property OnItemSaved: TOnItemSaved read FOnItemSaved write FOnItemSaved;
  end;
  
implementation

uses ClassDef;

{$R *.DFM}

//:-------------------------------------------------------------------
(*: Member:           acSaveExecute
 *  Klasse:           TfrmItemSettingsFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Speichert die ver�nderten Settings
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmItemSettingsFrame.acSaveExecute(Sender: TObject);
begin
  // Es wird nicht gefragt ob gespeichert werden soll
  SaveSettings(false);
end;// TfrmItemSettingsFrame.acSaveExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           acUndoExecute
 *  Klasse:           TfrmItemSettingsFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: L�dt die Settings wieder vom urspr�nglichen Alarm
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmItemSettingsFrame.acUndoExecute(Sender: TObject);
begin
  // Das �rspr�ngliche DataItem wird einfach neu angezeigt
  ShowItem(FQOItem);
  ChangeFromGUI(Sender);
end;// TfrmItemSettingsFrame.acUndoExecute cat:No category

//:-------------------------------------------------------------------
(*: Member:           ChangeFromGUI
 *  Klasse:           TfrmItemSettingsFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Erfasst ob �nderungen an den Settings gemacht wurden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmItemSettingsFrame.ChangeFromGUI(Sender: TObject);
begin
  acSave.Enabled := SettingsChanged;
  acUndo.Enabled := acSave.Enabled;
end;// TfrmItemSettingsFrame.ChangeFromGUI cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoItemSaved
 *  Klasse:           TfrmItemSettingsFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aItem)
 *
 *  Kurzbeschreibung: Dispatch Methode f�r den Event OnItemSaved
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmItemSettingsFrame.DoItemSaved(Sender: TfrmItemSettingsFrame; aItem: TQOItem);
begin
  if Assigned(FOnItemSaved) then FOnItemSaved(Sender, aItem);
end;// TfrmItemSettingsFrame.DoItemSaved cat:No category

//:-------------------------------------------------------------------
(*: Member:           MethodChange
 *  Klasse:           TfrmItemSettingsFrame
 *  Kategorie:        No category 
 *  Argumente:        (aSender, aMethod)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn an einer Methode etwas ge�ndert wurde
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmItemSettingsFrame.MethodChange(aSender: TFrame; aMethod: TQOMethod);
begin
  ChangeFromGUI(nil);
end;// TfrmItemSettingsFrame.MethodChange cat:No category

//:-------------------------------------------------------------------
(*: Member:           saved
 *  Klasse:           TfrmItemSettingsFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt True zur�ck, wenn das aktuelle Objekt auf der gespeichert ist
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmItemSettingsFrame.saved: Boolean;
begin
  result := not(acSave.Enabled);
end;// TfrmItemSettingsFrame.saved cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveSettings
 *  Klasse:           TfrmItemSettingsFrame
 *  Kategorie:        No category 
 *  Argumente:        (aAskForPermission)
 *
 *  Kurzbeschreibung: Speichert die Einstellungen im AlarmObjekt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmItemSettingsFrame.SaveSettings(aAskForPermission: boolean): Boolean;
var
  xAllow: Boolean;
begin
  result := (not(assigned(FQOItem)));
  
  // Nichts unternehmen wenn kein Item zugewiesen (evt. gel�scht)
  if not(assigned(FQOItem)) then
    exit;

//: ----------------------------------------------
  // Nur weitermachen, wenn die Settings ge�ndert haben
  xAllow := SettingsChanged;
  
  // evt. Abfragen ob gespeichert werden soll
  if xAllow and aAskForPermission then
    xAllow := (mmMessageDlg(Format(sAskForSave,[QOItem.QOVisualize.Caption]), mtConfirmation, [mbYes, mbNo], 0, self) = mrYes);
  
  // Wenn die Settings ge�ndert haben, dann wird gespeichert
  if xAllow then begin

//: ----------------------------------------------
    FQOItem.BoolOpAnd := (rgBoolOp.ItemIndex = 0);
    FQOItem.ClassDefects := TClassDefects(rgClassDefects.ItemIndex);
  
    // Settings wurden ge�ndert
    result := true;
  
    (* ------------- �nderungen �bernehmen ------------- *)
    // Zuerst die Settings der Methoden abf�llen
    mMethodContainer.SaveSettings;
    // Auf der Datenbank speichern
    QOItem.SaveDefToDB;
  
    (* Die Daten neu anzeigen, da die Methoden Kopien sind
       und der Vergleich darum nicht klappen w�rde *)
    ShowItem(QOItem);
    // Buttons disablen
    ChangeFromGUI(nil);
  
    // Anwendung informieren
    DoItemSaved(self, QOItem);

//: ----------------------------------------------
  end;// if xAllow then begin
end;// TfrmItemSettingsFrame.SaveSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetQOItem
 *  Klasse:           TfrmItemSettingsFrame
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r QOItem
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmItemSettingsFrame.SetQOItem(Value: TQOItem);
begin
  FQOItem := Value;

//: ----------------------------------------------
  visible := FQOItem <> nil;
end;// TfrmItemSettingsFrame.SetQOItem cat:No category

//:-------------------------------------------------------------------
(*: Member:           SettingsChanged
 *  Klasse:           TfrmItemSettingsFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: True, wenn die Settings noch denen des �bergebenen Items entsprechen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmItemSettingsFrame.SettingsChanged: Boolean;
var
  xBoolOpAnd: Boolean;
  xMethodsChanged: Boolean;
begin
  result := false;
  
  // Nichts unternehmen wenn kein Item zugewiesen (evt. gel�scht)
  if not(assigned(FQOItem)) then
    exit;

//: ----------------------------------------------
  // Verkn�pfung der DataItems
  xBoolOpAnd := (rgBoolOp.ItemIndex = 0);
  
  xMethodsChanged := mMethodContainer.MethodsChanged;

//: ----------------------------------------------
  // Alle Erkenntnisse zusammenziehen
  result := (
                 // Verkn�pfung der DataItems
                 (QOItem.BoolOpAnd <> xBoolOpAnd)
                 // Methoden
               or (xMethodsChanged)
               // Art der �berwachung (nur relevant f�r Matrix DataItem)
               or (FQOItem.ClassDefects <> TClassDefects(rgClassDefects.ItemIndex))
  
             );
end;// TfrmItemSettingsFrame.SettingsChanged cat:No category

//:-------------------------------------------------------------------
(*: Member:           ShowItem
 *  Klasse:           TfrmItemSettingsFrame
 *  Kategorie:        No category 
 *  Argumente:        (aQOItem)
 *
 *  Kurzbeschreibung: Zeigt die Einstellungen des �bergebenen DataItems an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmItemSettingsFrame.ShowItem(aQOItem: TObject);
begin
  if aQOItem is TQOItem then begin
    FQOItem := TQOItem(aQOItem);

//: ----------------------------------------------
    if FQOItem.BoolOpAnd then
      rgBoolOp.ItemIndex := 0
    else
      rgBoolOp.ItemIndex := 1;
  
    rgClassDefects.ItemIndex := ord(FQOItem.ClassDefects);
    rgClassDefects.visible := FQOItem.IsMatrixDataItem;
    mClassDefSpacePanel.visible := rgClassDefects.visible;
  
    // Das Tab mit dem NAmen des Items beschriften
    tabItemSettings.Caption := FQOItem.QOVisualize.Caption;
  
    // Frame anzeigen
    Visible := true;

//: ----------------------------------------------
    // Methoden anzeigen
    mMethodContainer.OnMethodChange := nil;
    mMethodContainer.ShowItem(FQOItem, Images);
    mMethodContainer.OnMethodChange := MethodChange;

//: ----------------------------------------------
  end;// if aQOItem is TQOItem then begin
end;// TfrmItemSettingsFrame.ShowItem cat:No category

end.







