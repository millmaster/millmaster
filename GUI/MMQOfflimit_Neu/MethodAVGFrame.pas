(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MethodAVGFrame.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Frame f�r die Darstellung der Settings der Mittelwert Methode
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 08.05.2003  1.00  Lok | Datei erstellt
|=========================================================================================*)
unit MethodAVGFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MethodBaseFrame, StdCtrls, mmCheckBox, mmStaticText, ExtCtrls, mmImage,
  mmPanel, mmEdit, ComCtrls, mmDateTimePicker, mmBevel, mmLabel,
  QOKernel, mmGroupBox, QOMethods, mmNumCtrl, IvDictio, IvMulti,
  IvEMulti, mmTranslator, QODef, CalcMethods, mmStringList, mmMemo;

type
  (*: Klasse:        TfrmMethodAVGFrame
      Vorg�nger:     TfrmMethodBaseFrame
      Kategorie:     View
      Kurzbeschrieb: Frame f�r die Darstellung der Settings der Mittelwert Methode 
      Beschreibung:  
                     - *)
  TfrmMethodAVGFrame = class(TfrmMethodBaseFrame)
    edNumShifts: TmmEdit;
    edProdYarnLength: TmmEdit;
    laUnit: TmmLabel;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 �bernimmt alle Einstellungen in das �bergebene Objekt 
    function ApplySettings(aMethod: TQOMethod): Boolean; override;
    //1 Typecast der zugewiesenen Methode 
    function AVGMethod: TQOAVG;
    //1 True, wenn die Methode editiert wurde 
    function MethodChanged: Boolean; override;
    //1 Versetzt den Frame in den ReadOnly Modus 
    procedure SetReadOnly; override;
    procedure _SetGUIValues; override;
  end;
  
var
  frmMethodAVGFrame: TfrmMethodAVGFrame;

implementation
uses
  SettingsReader, MMUGlobal, QOGUIShared, mmcs, JCLMath, LoepfeGlobal;

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmMethodAVGFrame
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmMethodAVGFrame.Create(aOwner: TComponent);
begin
  (* Der Methodentyp muss vor dem inherited Aufruf definiert werden
     da anhand des Methodentyps der angezeigte Text bestimmt wird *)
  mMethodType := cmAVG;

//: ----------------------------------------------
  inherited Create(aOwner);
end;// TfrmMethodAVGFrame.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmMethodAVGFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmMethodAVGFrame.Destroy;
begin
  inherited Destroy;
end;// TfrmMethodAVGFrame.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           ApplySettings
 *  Klasse:           TfrmMethodAVGFrame
 *  Kategorie:        No category 
 *  Argumente:        (aMethod)
 *
 *  Kurzbeschreibung: �bernimmt alle Einstellungen in das �bergebene Objekt
 *  Beschreibung:     
                      Nachfahren �berschreiben diese Methode.
                      Die Methode "SaveSettings" ruft die Methode "AplySettings" 
                      auf um die Einstellungen zu speichern. Die Methode wird 
                      ausserdem von der G�ligkeitspr�fung verwendet um die 
                      Plausibilit�t der eingegebenen Parameter zu �berpr�fen.
 --------------------------------------------------------------------*)
function TfrmMethodAVGFrame.ApplySettings(aMethod: TQOMethod): Boolean;
var
  xAVG: TQOAVG;
begin
  Result := inherited ApplySettings(aMethod);

//: ----------------------------------------------
  xAVG := TQOAVG(aMethod);
  
  // L�nge in Metern
  xAVG.MinProdYarnLength := Trunc(edProdYarnLength.AsFloat * xAvg.GetConversionFactor);
  xAVG.ShiftCount        := Trunc(edNumShifts.AsInteger);
  
  // Parameter f�r den CalcMethodFrame zusammensuchen
  with mMethodParam do begin
    try
      xAVG.Tolerance := MMStrToFloat(ValueDef(cAVGDeviation, 0));
    except
      // Exception abfangen, wenn nur ein '-' Zeichen eingegeben wurde
      xAVG.Tolerance := 0;
    end;//try except
  
    try
      xAVG.ShiftCountAVG := ValueDef(cAVGShiftCompression, cDefSchiftCountAVG);
    except
      // Exception abfangen, wenn nur ein '-' Zeichen eingegeben wurde
      xAVG.ShiftCountAVG := cDefSchiftCountAVG;
    end;//try except
  end;// with TmmStringList.Create do begin
end;// TfrmMethodAVGFrame.ApplySettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           AVGMethod
 *  Klasse:           TfrmMethodAVGFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Typecast der zugewiesenen Methode
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmMethodAVGFrame.AVGMethod: TQOAVG;
begin
  result := nil;
  
  if assigned(Method) then
    result := TQOAVG(FMethod);
end;// TfrmMethodAVGFrame.AVGMethod cat:No category

//:-------------------------------------------------------------------
(*: Member:           MethodChanged
 *  Klasse:           TfrmMethodAVGFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: True, wenn die Methode editiert wurde
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmMethodAVGFrame.MethodChanged: Boolean;
begin
  Result := inherited MethodChanged;

//: ----------------------------------------------
  result := result or (AVGMethod.MinProdYarnLength <> Trunc(edProdYarnLength.AsFloat * AVGMethod.GetConversionFactor));
  result := result or (AVGMethod.ShiftCount <> Trunc(edNumShifts.AsFloat));
  
  // Parameter f�r den CalcMethodFrame zusammensuchen
  with mMethodParam do begin
    try
      result := result or (not(FloatsEqual(AVGMethod.Tolerance, MMStrToFloat(ValueDef(cAVGDeviation, 0)))));
    except
      // Exception abfangen, wenn nur ein '-' Zeichen eingegeben wurde
      result := result or (not(FloatsEqual(AVGMethod.Tolerance, 0)));
    end;//try except
  
    try
      result := result or (AVGMethod.ShiftCountAVG <> ValueDef(cAVGShiftCompression, cDefSchiftCountAVG));
    except
      // Exception abfangen, wenn nur ein '-' Zeichen eingegeben wurde
      result := result or (AVGMethod.ShiftCountAVG <> cDefSchiftCountAVG);
    end;//try except
  end;// with TmmStringList.Create do begin
end;// TfrmMethodAVGFrame.MethodChanged cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetReadOnly
 *  Klasse:           TfrmMethodAVGFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Versetzt den Frame in den ReadOnly Modus
 *  Beschreibung:     
                      Der RO-Modus wird gebraucht, wenn die Settings lediglich
                      angezeigt werden (Analyse).
 --------------------------------------------------------------------*)
procedure TfrmMethodAVGFrame.SetReadOnly;
begin
  inherited SetReadOnly;

//: ----------------------------------------------
  SetEditReadOnly(edProdYarnLength);
  SetEditReadOnly(edNumShifts);
end;// TfrmMethodAVGFrame.SetReadOnly cat:No category

//:-------------------------------------------------------------------
procedure TfrmMethodAVGFrame._SetGUIValues;
var
  xUnit: String;
begin
  edProdYarnLength.AsFloat := AVGMethod.MinProdYarnLength / AVGMethod.GetConversionFactor;
  edNumShifts.AsInteger    := AVGMethod.ShiftCount;
  
  case AVGMethod.LengthUnit of
    luMeter : xUnit := 'km';
    luYard  : xUnit := 'kyds';
    else
      raise Exception.CreateFMT('TfrmMethodAVGFrame.Create: %s %s (id = %d)!',[sQOLengthUnit, sNotValid, ord(AVGMethod.LengthUnit)]);
  end;// case LengthUnit of
  
  laUnit.Caption := xUnit;
  
  // Parameter f�r den CalcMethodFrame zusammensuchen
  with mMethodParam do begin
    Values[cAVGShiftCompression] := AVGMethod.ShiftCountAVG;
    Values[cAVGDeviation]        := MMFloatToStr(AVGMethod.Tolerance);
  end;// with TmmStringList.Create do begin
end;// TfrmMethodAVGFrame._SetGUIValues cat:No category

{$R *.DFM}

end.










