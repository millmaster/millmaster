(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_MMSecurity_MMGroups.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Zeigt die MM-Security der Benutzer
| Info..........: TMMSecurityMMGroups wird im MMConfiguration.dpr erstellt
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 14.10.2002  1.00  SDo | Report erstellt
|=============================================================================*)

unit u_MMSecurity_MMGroups;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids, AdvGrid, mmExtStringGrid, mmComboBox, mmLabel, ExtCtrls,
  mmPanel,
  u_MMConfigSecurity, IvDictio, IvMulti, IvEMulti, mmTranslator, fcLabel,
  mmGroupBox;

type
  TMMSecurityMMGroups = class(TFrame)
    paTop: TmmPanel;
    mmLabel1: TmmLabel;
    cbUserGroups: TmmComboBox;
    esgGroupSecurity: TmmExtStringGrid;
    mmTranslator1: TmmTranslator;
    gbRemark: TmmGroupBox;
    lbRemark: TmmLabel;
    mLabel: TmmLabel;
    procedure esgGroupSecurityRadioClick(Sender: TObject; aCol, aRow,
      aIdx: Integer);
    procedure esgGroupSecurityClick(Sender: TObject);
    procedure esgGroupSecurityGetCellColor(Sender: TObject; ARow,
      ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure cbUserGroupsChange(Sender: TObject);
    procedure mmTranslator1LanguageChange(Sender: TObject);
    procedure FrameResize(Sender: TObject);
  private
    { Private declarations }
    fMMSecurityClass : TBaseMMSecurity;
    fSecurityChanged : Boolean;
    procedure SetActiveSecurityClass(const Value: TBaseMMSecurity);
    function GetSecurityChanged: Boolean;
    procedure SetSecurityChanged(const Value: Boolean);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    property ActiveSecurityClass: TBaseMMSecurity  write SetActiveSecurityClass;
    property SecurityChanged : Boolean read  GetSecurityChanged write SetSecurityChanged;
  end;

var  MMSecurityMMGroups : TMMSecurityMMGroups;

implementation
{$R *.DFM}
uses
  MMSetupDataMudule, mmSetupMain, mmCS;

{ TMMSecurityMMGroups }
//------------------------------------------------------------------------------
constructor TMMSecurityMMGroups.Create(AOwner: TComponent);
begin
EnterMethod('TMMSecurityMMGroups.Create');
  inherited Create(AOwner);
  fMMSecurityClass:= NIL;
  InitSecurityGrid(esgGroupSecurity);
  esgGroupSecurity.ColWidths[0]:= 250;
  esgGroupSecurity.ColWidths[1]:= 210;
  fSecurityChanged := FALSE;
  mLabel.Caption := Trim(cDefaultMarker);
  CodeSite.SendMsg('TMMSecurityMMGroups created');
end;
//------------------------------------------------------------------------------
procedure TMMSecurityMMGroups.esgGroupSecurityRadioClick(Sender: TObject;
  aCol, aRow, aIdx: Integer);
var x, xSecurLevel: Integer;
    xAppl, xForm, xFunction, xUserGroup: String;
begin
  xSecurLevel := aIdx;

  xAppl       := esgGroupSecurity.Cells[3, aRow];
  xForm       := esgGroupSecurity.Cells[4, aRow];
  xFunction   := esgGroupSecurity.Cells[5, aRow];
  xUserGroup  := esgGroupSecurity.Cells[6, aRow];

  with fMMSecurityClass do
      for x:= 0 to SecurityItem.Count-1 do
          if (SecurityItem.Items[x].Appl = xAppl) and
             (SecurityItem.Items[x].ApplForm = xForm) and
             (SecurityItem.Items[x].ApplFunction = xFunction) and
             (SecurityItem.Items[x].SecurityGroup = xUserGroup) then
             begin
                SecurityItem.Items[x].SecurityLevel := xSecurLevel;
                break;
             end;

  fSecurityChanged:= TRUE;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityMMGroups.esgGroupSecurityClick(Sender: TObject);
begin
  esgGroupSecurity.SetFocus;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityMMGroups.esgGroupSecurityGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush;
  AFont: TFont);
begin
  if (ACol = 2) and (ARow > 0 )then begin
      AFont.Name:= 'Courier';
      AFont.Size:= 6;
      AFont.Color:= clRed;
  end else AFont.Color:= clWindowText;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityMMGroups.cbUserGroupsChange(Sender: TObject);
begin
  fMMSecurityClass.SecurityGrid:= esgGroupSecurity;
  fMMSecurityClass.ShowData( cbUserGroups.text);
  esgGroupSecurity.SetFocus;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityMMGroups.SetActiveSecurityClass(
  const Value: TBaseMMSecurity);
begin
 fMMSecurityClass :=Value;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityMMGroups.mmTranslator1LanguageChange(Sender: TObject);
var x : integer;
    xText : String;
begin
// for x:=1 to esgGroupSecurity.RowCount-1 do
//     Translate( esgGroupSecurity.cells[1,x]);

   EnterMethod('TMMSecurityMMGroups.mmTranslator1LanguageChange');

   xText := Translate(rsRemarkText);
   xText := Format('%s ''%s''.', [rsRemarkText, cDefaultUserGroup]);

   MMSecurityMMGroups.lbRemark.Caption := xText;

   fMMSecurityClass.Translate;
   TranslateHeader(esgGroupSecurity);
   fMMSecurityClass.SecurityGrid:= esgGroupSecurity;
   fMMSecurityClass.ShowData(cbUserGroups.Text);

   //InitSecurityGrid(esgGroupSecurity);
end;
//------------------------------------------------------------------------------
function TMMSecurityMMGroups.GetSecurityChanged: Boolean;
begin
  result := fSecurityChanged;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityMMGroups.SetSecurityChanged(const Value: Boolean);
begin
  fSecurityChanged:= Value;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityMMGroups.FrameResize(Sender: TObject);
begin
  gbRemark.Width :=   Width - gbRemark.Left -17;
  lbRemark.Width :=   gbRemark.Width - lbRemark.Left - 4 ;
end;
//------------------------------------------------------------------------------
end.
