(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_MMSecurity_Appl.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Zeigt die MM-Security der Applikationen & Forms
| Info..........: TMMSecurityAppl wird im MMConfiguration.dpr erstellt
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 14.10.2002  1.00  SDo | Report erstellt
|                       | Appls. werden von Func. TSetupMain.ShowSequrity()
|                       | in cbAppls abgefuellt
|=============================================================================*)
unit u_MMSecurity_Appl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmComboBox, ExtCtrls, mmPanel, Grids, AdvGrid, mmExtStringGrid,
  mmLabel,
  u_MMConfigSecurity, IvDictio, IvMulti, IvEMulti, mmTranslator, fcLabel,
  mmGroupBox;

type

  TApplName = class(TObject)
  private
    mApplNameOrg: String;
    mApplNameTranslate: String;

    function GetApplNameOrg: String;
    function GetApplNameTranslate: String;
    procedure SetApplNameOrg(const Value: String);
    procedure SetApplNameTranslate(const Value: String);
  public
    property ApplNameOrg : String read GetApplNameOrg write SetApplNameOrg;
    property ApplNameTranslate : String  read GetApplNameTranslate write SetApplNameTranslate;
  end;

  TMMSecurityAppl = class(TFrame)
    esgApplSecurity: TmmExtStringGrid;
    paTop: TmmPanel;
    cbAppls: TmmComboBox;
    cbForms: TmmComboBox;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmTranslator1: TmmTranslator;
    gbRemark: TmmGroupBox;
    lbRemark: TmmLabel;
    mLabel: TmmLabel;
    procedure esgApplSecurityClick(Sender: TObject);
    procedure esgApplSecurityGetCellColor(Sender: TObject; ARow,
      ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure cbApplsChange(Sender: TObject);
    procedure cbFormsChange(Sender: TObject);
    procedure esgApplSecurityRadioClick(Sender: TObject; aCol, aRow,
      aIdx: Integer);
    procedure mmTranslator1LanguageChange(Sender: TObject);
    procedure FrameResize(Sender: TObject);
  private
    { Private declarations }
    fMMSecurityClass: TBaseMMSecurity;
    fSecurityChanged: Boolean;
    mApplList : TStringList;

    procedure ShowData;
    procedure SetActiveSecurityClass(const Value: TBaseMMSecurity);
    function GetSecurityChanged: Boolean;
    procedure SetSecurityChanged(const Value: Boolean);
    procedure SetApplList(const Value: TStringList);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property ActiveSecurityClass: TBaseMMSecurity write SetActiveSecurityClass;
    property SecurityChanged: Boolean read GetSecurityChanged write SetSecurityChanged;
    property ApplList : TStringList write SetApplList;
  end;

var
  MMSecurityAppl: TMMSecurityAppl;
implementation
{$R *.DFM}
uses
  MMSetupDataMudule, mmSetupMain, mmCS;
//------------------------------------------------------------------------------
constructor TMMSecurityAppl.Create(AOwner: TComponent);
var
  x: Integer;
  xRbIndex: Integer;
begin
EnterMethod('TMMSecurityAppl.Create');
  inherited Create(AOwner);;
  fMMSecurityClass := nil;

  mApplList := TStringList.Create;
  mApplList.Sorted := TRUE;
  mApplList.Duplicates := dupIgnore;

  cbAppls.Clear;
  cbForms.Clear;

  InitSecurityGrid(esgApplSecurity);

  esgApplSecurity.ColWidths[0] := 280;
  esgApplSecurity.ColWidths[1] := 210;
  mLabel.Caption := Trim(cDefaultMarker);
  fSecurityChanged := False;

  CodeSite.SendMsg('TMMSecurityAppl created');

end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.esgApplSecurityClick(Sender: TObject);
begin
  esgApplSecurity.SetFocus;
  esgApplSecurity.ScrollBars := ssVertical;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.esgApplSecurityGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush;
  AFont: TFont);
begin
  if (ACol = 2) and (ARow > 0) then begin
    AFont.Name := 'Courier';
    AFont.Size := 6;
    AFont.Color := clRed;
  end
  else
    AFont.Color := clWindowText;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.cbApplsChange(Sender: TObject);
var x: integer;
    xObj : TApplName;
    xText : String;
begin

  x := cbAppls.Items.IndexOf(cbAppls.Text);
  xObj := TApplName(cbAppls.Items.Objects[x]);
  xText :=  xObj.ApplNameOrg;

//  cbForms.Items.CommaText := fMMSecurityClass.GetApplicationForms(cbAppls.Text);
  cbForms.Items.CommaText := fMMSecurityClass.GetApplicationForms(xText);
  cbForms.ItemIndex := 0;
  ShowData;
  {
  for x:= 0 to cbForms.Items.Count-1 do
     cbForms.items.Strings[x] :=  Translate(cbForms.items.Strings[x]);
  }
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.ShowData;
var x: integer;
    xObj : TApplName;
    xText : String;
begin
  x := cbAppls.Items.IndexOf(cbAppls.Text);
  xObj := TApplName(cbAppls.Items.Objects[x]);
  xText :=  xObj.ApplNameOrg;

  fMMSecurityClass.SecurityGrid := esgApplSecurity;
//  fMMSecurityClass.ShowData(cbAppls.text, cbForms.text);
  fMMSecurityClass.ShowData(cbAppls.Text, cbForms.text);
  esgApplSecurity.SetFocus;
  esgApplSecurity.Click;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.cbFormsChange(Sender: TObject);
begin
  ShowData;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.esgApplSecurityRadioClick(Sender: TObject; aCol,
  aRow, aIdx: Integer);
var x, xSecurLevel: Integer;
  xAppl, xForm, xFunction, xUserGroup: string;

begin

  xSecurLevel := aIdx;

  xAppl := esgApplSecurity.Cells[3, aRow];
  xForm := esgApplSecurity.Cells[4, aRow];
  xFunction := esgApplSecurity.Cells[5, aRow];
  xUserGroup := esgApplSecurity.Cells[6, aRow];

  with fMMSecurityClass do
    for x := 0 to SecurityItem.Count - 1 do
      if (SecurityItem.Items[x].Appl = xAppl) and
        (SecurityItem.Items[x].ApplForm = xForm) and
        (SecurityItem.Items[x].ApplFunction = xFunction) and
        (SecurityItem.Items[x].SecurityGroup = xUserGroup) then begin
        SecurityItem.Items[x].SecurityLevel := xSecurLevel;
        break;
      end;
  fSecurityChanged := True;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.SetActiveSecurityClass(
  const Value: TBaseMMSecurity);
begin
  fMMSecurityClass := Value;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.mmTranslator1LanguageChange(Sender: TObject);
var xText, xOrgApplName: string;
    x, xIndex: integer;
    xObj : TApplName;
begin

  EnterMethod('TMMSecurityAppl.mmTranslator1LanguageChange');

  xText := Translate(rsRemarkText);
  xText := Format('%s ''%s''.', [rsRemarkText, cDefaultUserGroup]);

  MMSecurityAppl.lbRemark.Caption := xText;

  TranslateHeader(esgApplSecurity);

  if mApplList.Count < 1 then exit;

  //Orginal Appl.Name ermitteln
  xIndex := cbAppls.Items.IndexOf(cbAppls.Text);
  if xIndex >= 0 then begin
     xObj := TApplName(cbAppls.Items.Objects[xIndex]);
     xOrgApplName :=  xObj.ApplNameOrg;
  end;

  //Item Obj. loeschen
  for x:= 0 to cbAppls.Items.Count-1 do begin
      xObj := TApplName(cbAppls.Items.Objects[x]);
      xObj.Free;
  end;

  cbAppls.Clear;
  for x:= 0 to mApplList.Count - 1 do begin
      xText := Translate('(*)' + mApplList.Strings[x]);
      xText := StringReplace(xText, '(*)', '', [rfReplaceAll]);

      xObj := TApplName.Create;
      xObj.ApplNameOrg       := mApplList.Strings[x];
      xObj.ApplNameTranslate := xText;

      cbAppls.Items.AddObject(xText, xObj);

      //cbAppls.Items.Add( xText );
  end;

  xIndex := -1;

  //CB.Index auf org. Appl setzen (bei Uebersetzung ist der Appl. CB.Index unterschiedlich)
  for x:= 0 to mApplList.Count - 1 do begin
     xObj := TApplName(cbAppls.Items.Objects[x]);
     if xObj.ApplNameOrg = xOrgApplName then begin
        xIndex := x;
        break;
     end;
  end;


  if xIndex <0 then xIndex:= 0;
  cbAppls.ItemIndex := xIndex;

  xObj := TApplName(cbAppls.Items.Objects[xIndex]);
  xText :=  xObj.ApplNameOrg;

  fMMSecurityClass.Translate;
//  cbForms.Items.CommaText := fMMSecurityClass.GetApplicationForms(cbAppls.Text);
  cbForms.Items.CommaText := fMMSecurityClass.GetApplicationForms(xText);
  cbForms.ItemIndex := 0;


  //TranslateHeader(esgApplSecurity);
  fMMSecurityClass.SecurityGrid := esgApplSecurity;
  fMMSecurityClass.ShowData(cbAppls.Text, cbForms.text);
  //fMMSecurityClass.ShowData(cbAppls.text, cbForms.text);


 //InitSecurityGrid(esgApplSecurity);

end;
//------------------------------------------------------------------------------
function TMMSecurityAppl.GetSecurityChanged: Boolean;
begin
  Result := fSecurityChanged;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.SetSecurityChanged(const Value: Boolean);
begin
  fSecurityChanged := Value;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.FrameResize(Sender: TObject);
begin
  gbRemark.Width := Width - gbRemark.Left - 17;
  lbRemark.Width := gbRemark.Width - lbRemark.Left - 4;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.SetApplList(const Value: TStringList);
begin
  mApplList.CommaText := Value.CommaText;
  mmTranslator1.Translate;
end;
//------------------------------------------------------------------------------
destructor TMMSecurityAppl.Destroy;
var xObj : TApplName;
    x : Integer;
begin
  for x:= 0 to cbAppls.Items.Count-1 do begin
      xObj := TApplName(cbAppls.Items.Objects[x]);
      xObj.Free;
  end;
  mApplList.Free;
  inherited;
end;
//------------------------------------------------------------------------------



{ TApplName }

function TApplName.GetApplNameOrg: String;
begin
  Result := mApplNameOrg;
end;
//------------------------------------------------------------------------------
function TApplName.GetApplNameTranslate: String;
begin
  Result := mApplNameTranslate;
end;
//------------------------------------------------------------------------------
procedure TApplName.SetApplNameOrg(const Value: String);
begin
  mApplNameOrg := Value;
end;
//------------------------------------------------------------------------------
procedure TApplName.SetApplNameTranslate(const Value: String);
begin
  mApplNameTranslate := Value;
end;
//------------------------------------------------------------------------------
end.

