object MMSecurityAppl: TMMSecurityAppl
  Left = 0
  Top = 0
  Width = 764
  Height = 383
  TabOrder = 0
  OnResize = FrameResize
  object esgApplSecurity: TmmExtStringGrid
    Left = 0
    Top = 57
    Width = 764
    Height = 326
    Align = alClient
    ColCount = 3
    DefaultColWidth = 130
    DefaultRowHeight = 21
    FixedCols = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goColSizing]
    TabOrder = 0
    Visible = True
    OnClick = esgApplSecurityClick
    AutoNumAlign = False
    AutoSize = False
    VAlignment = vtaTop
    EnhTextSize = False
    EnhRowColMove = False
    SortFixedCols = False
    SizeWithForm = False
    Multilinecells = False
    OnGetCellColor = esgApplSecurityGetCellColor
    SortDirection = sdAscending
    OnRadioClick = esgApplSecurityRadioClick
    SortFull = False
    SortAutoFormat = True
    SortShow = False
    EnableGraphics = False
    SortColumn = 5
    HintColor = clInfoBk
    SelectionColor = clHighlight
    SelectionTextColor = clHighlightText
    SelectionRectangle = False
    SelectionRTFKeep = False
    HintShowCells = False
    OleDropTarget = False
    OleDropSource = False
    OleDropRTF = False
    PrintSettings.FooterSize = 0
    PrintSettings.HeaderSize = 0
    PrintSettings.Time = ppNone
    PrintSettings.Date = ppNone
    PrintSettings.DateFormat = 'dd/mm/yyyy'
    PrintSettings.PageNr = ppNone
    PrintSettings.Title = ppNone
    PrintSettings.Font.Charset = DEFAULT_CHARSET
    PrintSettings.Font.Color = clWindowText
    PrintSettings.Font.Height = -11
    PrintSettings.Font.Name = 'MS Sans Serif'
    PrintSettings.Font.Style = []
    PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
    PrintSettings.HeaderFont.Color = clWindowText
    PrintSettings.HeaderFont.Height = -11
    PrintSettings.HeaderFont.Name = 'MS Sans Serif'
    PrintSettings.HeaderFont.Style = []
    PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
    PrintSettings.FooterFont.Color = clWindowText
    PrintSettings.FooterFont.Height = -11
    PrintSettings.FooterFont.Name = 'MS Sans Serif'
    PrintSettings.FooterFont.Style = []
    PrintSettings.Borders = pbNoborder
    PrintSettings.BorderStyle = psSolid
    PrintSettings.Centered = False
    PrintSettings.RepeatFixedRows = False
    PrintSettings.RepeatFixedCols = False
    PrintSettings.LeftSize = 0
    PrintSettings.RightSize = 0
    PrintSettings.ColumnSpacing = 0
    PrintSettings.RowSpacing = 0
    PrintSettings.TitleSpacing = 0
    PrintSettings.Orientation = poPortrait
    PrintSettings.FixedWidth = 0
    PrintSettings.FixedHeight = 0
    PrintSettings.UseFixedHeight = False
    PrintSettings.UseFixedWidth = False
    PrintSettings.FitToPage = fpNever
    PrintSettings.PageNumSep = '/'
    PrintSettings.NoAutoSize = False
    PrintSettings.PrintGraphics = False
    HTMLSettings.Width = 100
    Navigation.AllowInsertRow = False
    Navigation.AllowDeleteRow = False
    Navigation.AdvanceOnEnter = False
    Navigation.AdvanceInsert = False
    Navigation.AutoGotoWhenSorted = False
    Navigation.AutoGotoIncremental = False
    Navigation.AutoComboDropSize = False
    Navigation.AdvanceDirection = adLeftRight
    Navigation.AllowClipboardShortCuts = False
    Navigation.AllowSmartClipboard = False
    Navigation.AllowRTFClipboard = False
    Navigation.AdvanceAuto = False
    Navigation.InsertPosition = pInsertBefore
    Navigation.CursorWalkEditor = False
    Navigation.MoveRowOnSort = False
    Navigation.ImproveMaskSel = False
    Navigation.AlwaysEdit = False
    ColumnSize.Save = False
    ColumnSize.Stretch = True
    ColumnSize.Location = clRegistry
    CellNode.Color = clSilver
    CellNode.NodeType = cnFlat
    CellNode.NodeColor = clBlack
    SizeWhileTyping.Height = False
    SizeWhileTyping.Width = False
    MouseActions.AllSelect = False
    MouseActions.ColSelect = False
    MouseActions.RowSelect = False
    MouseActions.DirectEdit = False
    MouseActions.DisjunctRowSelect = False
    MouseActions.AllColumnSize = False
    MouseActions.CaretPositioning = False
    IntelliPan = ipVertical
    URLColor = clBlack
    URLShow = False
    URLFull = False
    URLEdit = False
    ScrollType = ssNormal
    ScrollColor = clNone
    ScrollWidth = 16
    ScrollProportional = False
    ScrollHints = shNone
    OemConvert = False
    FixedFooters = 0
    FixedRightCols = 0
    FixedColWidth = 130
    FixedRowHeight = 21
    FixedFont.Charset = DEFAULT_CHARSET
    FixedFont.Color = clWindowText
    FixedFont.Height = -11
    FixedFont.Name = 'MS Sans Serif'
    FixedFont.Style = []
    WordWrap = False
    Lookup = False
    LookupCaseSensitive = False
    LookupHistory = False
    BackGround.Top = 0
    BackGround.Left = 0
    BackGround.Display = bdTile
    Hovering = False
    Filter = <>
    FilterActive = False
    AutoLabel.LabelPosition = lpLeft
    ColWidths = (
      130
      130
      497)
    RowHeights = (
      21
      21
      21
      21
      21)
  end
  object paTop: TmmPanel
    Left = 0
    Top = 0
    Width = 764
    Height = 57
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object mmLabel1: TmmLabel
      Left = 5
      Top = 5
      Width = 63
      Height = 13
      Caption = '(*)Programme'
      FocusControl = cbAppls
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel2: TmmLabel
      Left = 230
      Top = 5
      Width = 45
      Height = 13
      Caption = '(*)Fenster'
      FocusControl = cbForms
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object cbAppls: TmmComboBox
      Left = 5
      Top = 20
      Width = 200
      Height = 21
      Style = csDropDownList
      Color = clWindow
      ItemHeight = 13
      Sorted = True
      TabOrder = 0
      Visible = True
      OnChange = cbApplsChange
      AutoLabel.Control = mmLabel1
      AutoLabel.LabelPosition = lpTop
      Edit = True
      ReadOnlyColor = clInfoBk
      ShowMode = smExtended
    end
    object cbForms: TmmComboBox
      Left = 230
      Top = 20
      Width = 320
      Height = 21
      Style = csDropDownList
      Color = clWindow
      ItemHeight = 13
      Sorted = True
      TabOrder = 1
      Visible = True
      OnChange = cbFormsChange
      AutoLabel.Control = mmLabel2
      AutoLabel.LabelPosition = lpTop
      Edit = True
      ReadOnlyColor = clInfoBk
      ShowMode = smExtended
    end
    object gbRemark: TmmGroupBox
      Left = 573
      Top = 8
      Width = 183
      Height = 41
      Caption = '(*)Bemerkung'
      TabOrder = 2
      CaptionSpace = True
      object lbRemark: TmmLabel
        Left = 26
        Top = 15
        Width = 6
        Height = 13
        Caption = '0'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mLabel: TmmLabel
        Left = 8
        Top = 16
        Width = 7
        Height = 16
        Caption = '0'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    OnLanguageChange = mmTranslator1LanguageChange
    Left = 24
    Top = 192
    TargetsData = (
      1
      8
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Cells'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0)
      (
        'TmmExtStringGrid'
        'Cells'
        0)
      (
        'TmmLabel'
        '*'
        0)
      (
        'TmmGroupBox'
        '*'
        0)
      (
        'TmmComboBox'
        '*'
        0))
  end
end
