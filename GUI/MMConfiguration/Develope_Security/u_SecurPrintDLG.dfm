inherited PrintDialog: TPrintDialog
  Left = 353
  Top = 237
  Caption = 'PrintDialog'
  ClientHeight = 291
  ClientWidth = 518
  PixelsPerInch = 96
  TextHeight = 13
  inherited bOK: TmmButton
    Left = 326
    Top = 261
    TabOrder = 4
    OnClick = bOKClick
  end
  inherited bCancel: TmmButton
    Left = 426
    Top = 261
    TabOrder = 5
  end
  object mmGroupBox1: TmmGroupBox
    Left = 0
    Top = 0
    Width = 345
    Height = 130
    Caption = '(*)Drucker'
    TabOrder = 0
    TabStop = True
    CaptionSpace = True
    object laStatusLabel: TmmLabel
      Left = 20
      Top = 55
      Width = 43
      Height = 13
      Caption = '(*)Status:'
      Visible = True
      AutoLabel.Distance = 30
      AutoLabel.LabelPosition = lpRight
    end
    object laStatus: TmmLabel
      Left = 100
      Top = 56
      Width = 6
      Height = 13
      Caption = '0'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laTypeLabel: TmmLabel
      Left = 20
      Top = 80
      Width = 37
      Height = 13
      Caption = '(*)Type:'
      Visible = True
      AutoLabel.Distance = 30
      AutoLabel.LabelPosition = lpRight
    end
    object laType: TmmLabel
      Left = 100
      Top = 80
      Width = 6
      Height = 13
      Caption = '0'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laOrtLabel: TmmLabel
      Left = 20
      Top = 105
      Width = 27
      Height = 13
      Caption = '(*)Ort:'
      Visible = True
      AutoLabel.Distance = 30
      AutoLabel.LabelPosition = lpRight
    end
    object laOrt: TmmLabel
      Left = 100
      Top = 105
      Width = 6
      Height = 13
      Caption = '0'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmSpeedButton1: TmmSpeedButton
      Left = 300
      Top = 21
      Width = 24
      Height = 24
      Hint = '(*)Drucker Eigenschaften'
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00666666666666
        6666666600008F8F8F8F8F8F8F8F8F8F0000F800000000000008F8F800008787
        7777777777708F8F0000F0F888888888888708F8000080F9988888888887708F
        0000F7FFFFFFFFFFFFF770F800008F0888888888877F708F0000F8F087777777
        7777F0F800008F8F000000000000088F0000F8F8F8F8F8F8F6EEE0F800008F87
        000000000E60078F0000F8F0E6EEEEEEEE08F8F800008F87000000000E60078F
        0000F8F8F8F8F8F876EEE0F800008F8F8F8F8F8F8700078F0000F8F8F8F8F8F8
        F8F8F8F80000000000000000000000000000F0CCCCCCCCCCCCCC0F0F00007777
        77777777777777770000}
      ParentShowHint = False
      ShowHint = True
      Spacing = 0
      Transparent = False
      Visible = True
      OnClick = mmSpeedButton1Click
      AutoLabel.LabelPosition = lpLeft
    end
    object cbPrinters: TmmComboBox
      Left = 20
      Top = 24
      Width = 269
      Height = 21
      Color = clWindow
      ItemHeight = 13
      TabOrder = 0
      Text = '0'
      Visible = True
      OnChange = cbPrintersChange
      AutoLabel.LabelPosition = lpLeft
      Edit = True
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
    end
  end
  object rgSelectPrintDocu: TmmRadioGroup
    Left = 0
    Top = 136
    Width = 515
    Height = 120
    Caption = '(*)Auswahl Reprot'
    ItemIndex = 1
    Items.Strings = (
      '1'
      '2'
      '3'
      '4')
    TabOrder = 3
    TabStop = True
    OnClick = rgSelectPrintDocuClick
    CaptionSpace = True
  end
  object mmGroupBox2: TmmGroupBox
    Left = 350
    Top = 0
    Width = 165
    Height = 130
    Caption = '(*)Exemplare'
    TabOrder = 2
    TabStop = True
    CaptionSpace = True
    object mmLabel1: TmmLabel
      Left = 10
      Top = 107
      Width = 45
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = '(*)Anzahl:'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object nePageCopies: TmmNumEdit
      Left = 88
      Top = 100
      Width = 57
      Height = 21
      Decimals = 0
      Digits = 2
      Masks.PositiveMask = '#,##0'
      Max = 99
      Min = 1
      NumericType = ntGeneral
      TabOrder = 0
      UseRounding = True
      Validate = True
      ValidateString = 
        '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
        's %s'
    end
  end
  object pSelectItems: TmmPanel
    Left = 260
    Top = 150
    Width = 249
    Height = 21
    Anchors = []
    BevelOuter = bvNone
    TabOrder = 6
    object clItemSelect: TMMCheckListEdit
      Left = 5
      Top = 0
      Width = 238
      Height = 21
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      DropWidth = 220
      DropHeight = 80
      DropColumns = 0
      DropColor = clWindow
      DropFlat = True
      DropFont.Charset = DEFAULT_CHARSET
      DropFont.Color = clWindowText
      DropFont.Height = -11
      DropFont.Name = 'MS Sans Serif'
      DropFont.Style = []
      DropDirection = ddDown
      DropSorted = False
      TextDelimiter = ','
      TextEndChar = ']'
      TextStartChar = '['
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmGroupBox3: TmmGroupBox
    Left = 334
    Top = 16
    Width = 165
    Height = 81
    Caption = '(*)Druckbereich'
    TabOrder = 1
    TabStop = True
    Visible = False
    CaptionSpace = True
    object lbFrom: TmmLabel
      Left = 8
      Top = 54
      Width = 28
      Height = 13
      Caption = '(*)von'
      Enabled = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object lbTo: TmmLabel
      Left = 96
      Top = 54
      Width = 23
      Height = 13
      Caption = '(*)bis'
      Enabled = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object neFrom: TmmNumEdit
      Left = 48
      Top = 52
      Width = 30
      Height = 21
      Color = clBtnFace
      Decimals = 0
      Digits = 3
      Enabled = False
      Masks.PositiveMask = '#,##0'
      Max = 300
      Min = 1
      NumericType = ntGeneral
      TabOrder = 2
      UseRounding = True
      Validate = True
      ValidateString = 
        '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
        's %s'
    end
    object neTo: TmmNumEdit
      Left = 128
      Top = 52
      Width = 30
      Height = 21
      Color = clBtnFace
      Decimals = 0
      Digits = 3
      Enabled = False
      Masks.PositiveMask = '#,##0'
      Masks.ZeroMask = '###'
      Max = 300
      Min = 1
      NumericType = ntGeneral
      TabOrder = 3
      UseRounding = True
      Validate = True
      ValidateString = 
        '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
        's %s'
    end
    object rbPrintAll: TRadioButton
      Left = 8
      Top = 15
      Width = 113
      Height = 17
      Caption = '(*)Alles'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = rbPrintAllClick
    end
    object rbPrintPages: TRadioButton
      Left = 8
      Top = 34
      Width = 113
      Height = 17
      Caption = '(*)Seiten'
      TabOrder = 1
      TabStop = True
      OnClick = rbPrintAllClick
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 224
    Top = 88
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
  object PrinterSetupDialog1: TmmPrinterSetupDialog
    Left = 264
    Top = 88
  end
end
