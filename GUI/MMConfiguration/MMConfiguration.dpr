program MMConfiguration;
uses
  MemCheck,
  mmMBCS,
  LoepfeGlobal,
  Forms,
  Windows,
  SysUtils,
  RzCSIntf,
  BaseApplAboutBox in '..\..\..\LoepfeShares\Templates\BASEAPPLABOUTBOX.pas' {BaseApplAboutBoxForm},
  BASEAPPLMAIN in '..\..\..\LoepfeShares\Templates\BASEAPPLMAIN.pas' {BaseApplMainForm},
  mmSetupReport in 'mmSetupReport.pas' {SetupReport},
  mmGroupSelection in 'mmGroupSelection.pas' {GroupSelectionDLG},
  mmSpalsh in 'mmSpalsh.pas' {fSplash},
  mmSetupMain in 'mmSetupMain.pas' {SetupMain},
  MMSetupDataMudule in 'MMSetupDataMudule.pas' {MMConfigDataModule: TDataModule},
  u_MMSecurity_Appl in 'u_MMSecurity_Appl.pas' {MMSecurityAppl: TFrame},
  u_MMSecurity_MMGroups in 'u_MMSecurity_MMGroups.pas' {MMSecurityMMGroups: TFrame},
  u_MMConfigSecurity in 'u_MMConfigSecurity.pas',
  BaseForm in '..\..\..\LOEPFESHARES\TEMPLATES\BASEFORM.pas' {mmForm},
  BASEDialog in '..\..\..\LOEPFESHARES\TEMPLATES\BASEDIALOG.pas' {mmDialog},
  BASEDialogBottom in '..\..\..\LOEPFESHARES\TEMPLATES\BASEDIALOGBOTTOM.pas' {DialogBottom},
  u_SecurPrintDLG in 'u_SecurPrintDLG.pas' {PrintDialog},
  u_SecurityReport in 'u_SecurityReport.pas' {SecurityReport},
  u_FileVersion in 'u_FileVersion.pas';

{$R *.RES}

var
  xSplash : TfSplash;
  i: integer;
  xShowSplash : boolean;
  xTime : TDateTime;
begin
  gApplicationName := 'MMConfiguration';
{$IFDEF MemCheck}
  MemChk(gApplicationName);
{$ENDIF}

  xSplash     := Nil;
  xShowSplash := True;
  for i := 1 to ParamCount do begin
    if LowerCase(ParamStr(i)) = 'nv' then begin
      xShowSplash := False;
      break;
    end;
  end;

  if xShowSplash then begin
    xSplash:= TfSplash.Create(NIL);
    xSplash.mmAnimate1.Active := True;
    xSplash.Show;
    xSplash.Update;
  end;


  xTime := Time;
  Application.Initialize;
  Application.CreateForm(TMMConfigDataModule, MMConfigDataModule);
  if MMConfigDataModule.mError then begin
    CodeSite.SendMsg(gApplicationName  + ' will terminate.');
    FreeAndNil(xSplash);
    Application.Terminate;
  end else begin
    Application.CreateForm(TMMSecurityAppl, MMSecurityAppl);
    Application.CreateForm(TMMSecurityMMGroups, MMSecurityMMGroups);
    CodeSite.SendMsg('TMMSecurityAppl & TMMSecurityMMGroups created');
    Application.CreateForm(TSetupMain, SetupMain);
    FreeAndNil(xSplash);

    CodeSite.SendDateTime('Time : ' , Now);
    CodeSite.SendDateTime('Create time : ' , Now - xTime);

    Application.Run;
  end;
end.
