unit mmGroupSelection;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SELECTIONDIALOG, ActnList, mmActionList, Buttons, mmSpeedButton,
  StdCtrls, mmListBox, mmLabel, mmButton, ExtCtrls, mmPanel, IvDictio,
  IvMulti, IvEMulti, mmTranslator;

type
  TGroupSelectionDLG = class(TfrmSelectionDialog)
    Translator: TmmTranslator;
    mmPanel5: TmmPanel;
    bClear: TmmButton;
    acClear: TAction;
    procedure acClearExecute(Sender: TObject);
  private
    procedure SetDictionary(const Value: TIvDictionary);
    function GetSelectedGroups: TStrings;
  public
    procedure Init(aDomainList, aSelectList: TStrings);
    property SelectedGroups : TStrings read GetSelectedGroups;
    property DictionaryName : TIvDictionary write SetDictionary;

  end;

var
  GroupSelectionDLG: TGroupSelectionDLG;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

{ TGroupSelectionDLG }

//------------------------------------------------------------------------------
procedure TGroupSelectionDLG.SetDictionary(const Value: TIvDictionary);
begin
  Translator.Dictionary:= Value;
end;
//------------------------------------------------------------------------------
procedure TGroupSelectionDLG.acClearExecute(Sender: TObject);
begin
  inherited;
  lbSource.Items.AddStrings(lbDest.Items);
  lbDest.Clear;
end;
//------------------------------------------------------------------------------
function TGroupSelectionDLG.GetSelectedGroups: TStrings;
begin
  Result := lbDest.Items;
end;
//------------------------------------------------------------------------------
procedure TGroupSelectionDLG.Init(aDomainList, aSelectList: TStrings);
var
  i: Integer;
  xIndex: Integer;
begin

  lbSource.Items.Assign(aDomainList);
  lbDest.Items.Assign(aSelectList);

  for i:=0 to lbDest.Items.Count-1 do begin
    xIndex := lbSource.Items.IndexOf(lbDest.Items.Strings[i]);
    if xIndex >= 0 then
      lbSource.Items.Delete(xIndex);
  end;
end;
//------------------------------------------------------------------------------


end.
