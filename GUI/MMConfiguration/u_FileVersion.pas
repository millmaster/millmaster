(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_FileVersion.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: - Ermittelt MM-Files mit Version und Size
| Info..........:
| Develop.system: W2k
| Target.system.: W2k, XP, W2k3
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 05.04.2005  1.00  SDo | Report erstellt
|=============================================================================*)
unit u_FileVersion;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, comctrls, MMList;



type

  TFileRec = record
    Path: string;
    MMFile: string;
    Version: string;
    Size: DWORD; // KB
  end;
  pFileRec = ^TFileRec;


  TMMFiles = class(TObject)
  private
    FMMFileCount: Word;
    mFileList: TMMList;
    procedure DeleteListItems;
    procedure FilterMMFiles(aFileList:TStringList; var aResultList: TStringList);
    function GetMMFileRec(Index: Integer): TFileRec;
  public
    constructor Create;
    destructor Destroy; virtual;
    procedure FetchData;
    procedure GetAllFiles(aPath :string; var aFileList : TStringList);
    function GetFileDate(aFile : String): string;
    procedure PrepareFileList(aFileList:TStringList);
    property MMFileCount: Word read FMMFileCount;
    property MMFileRec[Index: Integer]: TFileRec read GetMMFileRec;
  end;

  function SortMMFileItem(aItem1, aItem2: Pointer): Integer;


const cLoepfe = 'LOEPFE';
      cMM     = 'MILLMASTER';
      cBarco  = 'BARCO';
      cBarcoV = 'BARCOVISION';

implementation

uses LoepfeGlobal, Registry, VersInfo, JCLFileUtils;


//******************************************************************************
//MMFile-Liste sortiern
//******************************************************************************
function SortMMFileItem(aItem1, aItem2: Pointer): Integer;
begin
  // 2 Spalten sortiern (Zuerst nach Key und dann nach ValueName sortiern)
  // Gross-Kleinbuchstaben werden beim Sortiern nicht berücksichtigt
  if UpperCase(TFileRec(aItem1^).Path) = UpperCase(TFileRec(aItem2^).Path)  then begin

        if UpperCase(TFileRec(aItem1^).MMFile) < UpperCase(TFileRec(aItem2^).MMFile ) then
           result := -1;

        if UpperCase(TFileRec(aItem1^).MMFile) > UpperCase(TFileRec(aItem2^).MMFile ) then
           result := 1;

        if UpperCase(TFileRec(aItem1^).MMFile) = UpperCase(TFileRec(aItem2^).MMFile ) then
           result := 0;
  end else begin
        if UpperCase(TFileRec(aItem1^).Path) < UpperCase(TFileRec(aItem2^).Path ) then
           result := -1;

        if UpperCase(TFileRec(aItem1^).Path) > UpperCase(TFileRec(aItem2^).Path ) then
           result := 1;
  end;
end;

//------------------------------------------------------------------------------





{ TMMFiles }
//------------------------------------------------------------------------------
constructor TMMFiles.Create;
begin
  inherited Create;
  mFileList := TMMList.Create;
end;
//------------------------------------------------------------------------------
destructor TMMFiles.Destroy;
begin
  DeleteListItems;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TMMFiles.DeleteListItems;
begin
  while mFileList.count > 0 do
    mFileList.delete(0);
end;
//------------------------------------------------------------------------------
procedure TMMFiles.FetchData;
var
  xFileList, xFileFoundList: TStringList;
  xPath, xFile, xRoot, xText, xVers: string;
  x: Integer;
  xFileRec: pFileRec;
begin

  DeleteListItems;

  fMMFileCount := 0;

  xRoot := GetRegString(cRegLM, cRegMillMasterPath, 'MILLMASTERROOTPATH', '');

  if xRoot = '' then exit;

  xFileList := TStringList.Create;
  xFileList.Duplicates := dupIgnore;
  xFileList.Sorted:= TRUE;

  xFileFoundList := TStringList.Create;
  xFileFoundList.Duplicates := dupIgnore;
  xFileFoundList.Sorted:= TRUE;

  //Alle MM & Barco EXE's
  xFile := '*.exe';
  xPath:= Format('%s\%s',[xRoot, xFile]);
  GetAllFiles(xPath ,  xFileList );
  FilterMMFiles(xFileList, xFileFoundList);
  xFileList.Clear;


  //Alle MM & Barco DLL's
  xFile := '*.dll';
  xPath:= Format('%s\%s',[xRoot, xFile]);
  GetAllFiles(xPath ,  xFileList );
  FilterMMFiles(xFileList, xFileFoundList);
  xFileList.Clear;

  //Nur MMGlossary.mld
  xFile := 'MMGlossary.mld';
  xFile := '*.mld';
  xPath:= Format('%s\%s',[xRoot, xFile]);
  GetAllFiles(xPath ,  xFileList );
  xFileFoundList.AddStrings(xFileList);
  xFileList.Clear;

  //Alle CHM's
  xFile := '*.chm';
  xPath:= Format('%s\%s',[xRoot, xFile]);
  GetAllFiles(xPath ,  xFileList );
  xFileFoundList.AddStrings(xFileList);
  xFileList.Clear;

  //Alle RPI's  -> Floor
  xFile := '*.rpi';
  xPath:= Format('%s\%s',[xRoot, xFile]);
  GetAllFiles(xPath ,  xFileList );
  xFileFoundList.AddStrings(xFileList);
  xFileList.Clear;


  //Fuellt gefundene Dateien in mFileList ab
  PrepareFileList(xFileFoundList);

  if mFileList.Count = 0 then begin
      New(xFileRec);

      xFileRec^.Path    := xRoot;
      xFileRec^.MMFile  := 'No files found';
      xFileRec^.Version := '';

      mFileList.Add(xFileRec);
  end else mFileList.sort( SortMMFileItem );

  fMMFileCount := mFileList.Count;

  xFileList.Free;
  xFileFoundList.Free;
end;
//------------------------------------------------------------------------------
procedure TMMFiles.FilterMMFiles(aFileList:TStringList; var aResultList:
        TStringList);
var
  xFileList, xFileFoundList: TStringList;
  xPath, xFile, xText, xVers: string;
  x: Integer;
  xVersionInfo: TdfsVersionInfoResource;
begin
  try
   xVersionInfo := TdfsVersionInfoResource.Create(Nil);
   for x:= 0  to  aFileList.Count-1 do begin
      try
        xFile := StringReplace(aFileList.Strings[x],'"', '', [rfReplaceAll]);
        if FileExists(xFile) then begin
           xVersionInfo.Filename :=  xFile;
           xText := UpperCase( xVersionInfo.CompanyName );

           if xVersionInfo.FileVersion.Major <> 0 then
              xVers := Format( ',%d.%d.%d',[ xVersionInfo.FileVersion.Major,
                                             xVersionInfo.FileVersion.Minor,
                                             xVersionInfo.FileVersion.Release  ] )
           else
              xVers := '';

           aResultList.Add(aFileList.Strings[x] + xVers);  
           {
           //Loepfe, Barco, MillMaster oder BarcoVisons Files
           if (Pos(cLoepfe, xText ) > 0 ) or (Pos(cMM, xText ) > 0 ) or
              (Pos(cBarco, xText  ) > 0 ) or (Pos(cBarcoV, xText ) > 0 ) then
              if Pos('MillMaster\Floor\Language', aFileList.Strings[x] ) = 0 then
                 aResultList.Add(aFileList.Strings[x] + xVers);
            }
          end;
      except
      end;
   end;
  finally
   xVersionInfo.Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMFiles.GetAllFiles(aPath :string; var aFileList : TStringList);
var
  xsr: TSearchRec;
  xFileAttrs: Integer;
  xDir, xFind: string;
begin
  xDir  := ExtractFilePath(aPath);
  xFind := ExtractFileName(aPath);

  if xDir [length(xDir)] <> '\' then xDir:= xDir + '\';

  xFileAttrs:=  faHidden or faArchive  or faReadOnly;

  //Alle Dateien suchen
  if FindFirst(xDir + xfind, xFileAttrs	, xsr) = 0 then repeat
     aFileList.Add('"' + xDir + xsr.Name + '"');
  until FindNext(xsr) <> 0;
  Sysutils.FindClose(xsr);

  //Unterverzeichnisse durchsuchen
  if FindFirst(xDir + '*.*', faDirectory, xsr) = 0 then repeat
     if ((xSr.Attr and faDirectory) = faDirectory) and  (xSr.name[1] <> '.' ) then
         GetAllFiles( xDir + xSr.name + '\' + xfind, aFileList);
  until FindNext(xsr) <> 0;
  Sysutils.FindClose(xsr);
end;
//------------------------------------------------------------------------------
function TMMFiles.GetFileDate(aFile : String): string;
begin
  //Datum
  try
    Result := DateToSTr(FileDateToDateTime( FileAge(aFile) ));
  except
    Result := '';
  end;
end;
//------------------------------------------------------------------------------
function TMMFiles.GetMMFileRec(Index: Integer): TFileRec;
var
  xFileRec: pFileRec;
begin
  xFileRec := mFileList.Items[ Index ];
  Result := xFileRec^;
end;
//------------------------------------------------------------------------------
procedure TMMFiles.PrepareFileList(aFileList:TStringList);
var
  x, n: Integer;
  xTempList: TStringList;
  xFile, xVers: string;
  xFileRec: pFileRec;
begin
  xTempList := TStringList.Create;

  for x:= 0 to aFileList.Count-1 do begin
      xTempList.CommaText := aFileList.Strings[x];
      xVers := '' ;
      for n:= 0 to xTempList.Count -1 do begin
          if n= 0 then xFile :=  xTempList.Strings[n]; //Path & Filename
          if n= 1 then xVers :=  xTempList.Strings[n]; //Version
      end;

      if xVers = '' then xVers := GetFileDate(xFile);

      New(xFileRec);

      xFileRec^.Path    := ExtractFilePath(xFile);
      xFileRec^.MMFile  := ExtractFileName(xFile);
      xFileRec^.Version := xVers;

      xFileRec^.Size := FileGetSize(xFile);

      mFileList.Add(xFileRec);
  end;
end;
//------------------------------------------------------------------------------
end.
