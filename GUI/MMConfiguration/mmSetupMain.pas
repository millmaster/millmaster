{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: mmSetupMain.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Hauptprogramm der MM-Setups|
| Info..........: Wichtig: Wenn im Test zwischen Client oder Server unterschieden
|                          werden muss, so ist in der Funktion GetNTProduct
|                          (Unit BaseSetup) und FormCreate (Unit mmSetupMain )
|                          der Wert Result := SERVER_NT oder WORKSTATION_NT zu
|                          setzen. -> suche nach ' // Test NTProduct '
|
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 21.07.1999  1.00  SDo | File created
| 11.02.2000  1.01  SDo | Programm wird beendet, wenn der User nicht in der DB
|                       | eingetragen ist. -> Implementation in TMMSetupModul
| 01.03.2000  1.02  SDo | Fileversion erweitert; Wenn keine Version, dann Datum
| 02.21.2001  1.03  SDo | Neuer Folder Artikel implementiert (nur mit Lab- ,
|                       | DispoMaster und Server sichtbar)
| 02.23.2001  1.04  SDo | Umbau der Proc. PreparePrintList
| 03.27.2001  1.05  SDo | Neue CB mit 'Standard Kalender' Auswahl
|                       | (aus Konst.Array + TShift)
| 02.05.2001  1.05  SDo | TabSheet nicht sichtbar bei WindingMaster
| 15.05.2001  1.05  SDo | Bemerkung: Der Job DelShiftByTime wird mit dem LabMaster
                                     auf 400 Tg gesetzt. Das bedeutet, dass
                                     nach DB-Daten ins Datawarehouse kopiert werden.
                                     Bei Kunden ohne LongTerm werden diese Daten geloescht.
                                     Bei Kunden, die ein LongTerm bestellt haben,
                                     duerfen diese Daten nicht geloescht werden,
                                     auch wenn das Datawarehouse noch nicht fertig ist.
                                     Der 400 Tg. Coundown laeuft ab der 1. LabMaster inst.

| 07.10.2002  1.06  SDo | Umstellung auf ADO
| 14.10.2002  1.07  SDo | MM-Security eingebaut (Security-Panel & Report)
| 27.01.2004  1.08  SDO | MM-Security Filter fuer Pro, Standard, EASY eingebaut
|                       | Ubersetzung von MM-Security-Appls
|                       | Aenderungen : ShowSequrity()
| 13.04.2004  1.08  Wss | Kleinere Textkorrekturen
| 05.04.2004  1.09  SDo | File-Version wird direkt eiglesen (nicht mehr ueber FileVersion.txt)
|                       | -> siehe u_FileVersion.pas
                        | LoadFileVersion() wird nicht mehr gebracht
| 07.04.2004  1.09  SDo | TInitializingThread fuer Initialisierung -> Aufstart etwas schneller
| 21.04.2005  1.10  SDo | Func. InsertProcessDLG() erweitert; Doppelte Prozesse (Handler) k�nnen nicht mehr eingegeben werden
|                       | Anpassung in Func. SetProcess() FileExists
| 23.06.2005  1.10  Wss | Label f�r Firmenname auf AutoSize da f�r Italienisch zuwenig Platz
| 15.11.2006  1.11  SDo | Erweiterung: Domeanen & PC-Namen ermitteln (ToolBox-Fenster); siehe
|                       | Unit u_PCNames
|                       | Umgestaltung Register 'MM System'; neuer Label-Text
|                       | RegDomainNames & RegDomainController
|                       | -> Const.Array cMMSetupDefaults in Unit SettingsReader
|                       | Neue Bedeutung Reg.Key :
|                       | DomainNames -> MM-Client (WKS): Computername vom MM-Host, eigener PC-Name
|                       | DomainNames -> MM-Host (SRV): Allle Computername auf denen das MM installiert ist
|                       | DomainController -> Computername auf denen die MM-User & MM-Gruppen installiert sind
===============================================================================}
unit mmSetupMain;

interface

uses

  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseForm, ExtCtrls, mmPanel, ComCtrls, mmStatusBar, ToolWin, mmToolBar,
  mmAnimate, IvDictio, IvAMulti, IvBinDic, mmDictionary, IvMulti, IvEMulti,
  mmTranslator, mmLogin, MMSecurity, Menus, mmPopupMenu, mmOpenDialog,
  IvMlDlgs, mmSaveDialog, mmPrintDialog, mmPrinterSetupDialog, mmMainMenu,
  StdActns, ActnList, mmActionList, ImgList, mmImageList, BaseApplAboutBox,
  MMMessages, mmEventLog, StdCtrls, mmLabel, mmRadioGroup,
  mmButton, BASEAPPLMAIN, AdvGrid,
  Spin, mmSpinEdit, mmGroupBox,
  mmPageControl, mmCheckBox, Mask, mmMaskEdit, mmListBox, mmComboBox,
  NTCommon, UserMan, mmList, CheckLst, mmCheckListBox, mmBevel,
  mmEdit, Buttons, mmBitBtn, DBVisualBox,
  mmDataSource, BaseSetup, MMUGlobal,
  BaseGlobal, DBCtrls, mmDBText,
  Grids, mmExtStringGrid, NumCtrl,
  Db, mmSetupModul, SetupComponents, {DBTables, mmQuery, mmDatabase, } ADODB,
  mmADOQuery, mmADOConnection, SettingsReader, fcStatusBar, fcLabel,
  u_MMConfigSecurity, XMLDef, u_FileVersion, mmThread, mmSpeedButton,
  u_PCNames;


type

  TSetupMain = class ;



  TInitializingThread = class(TmmThread)
  private
    mForm : TSetupMain;
    procedure Initializing;
  protected
  public
    constructor Create(aForm : TSetupMain); virtual;
    procedure Execute; override;
    destructor Destroy; override;
  end;

  TSetupValues = procedure of object;

  TExportConstArrayRec = record
    UniqueValueName: ShortString;
    Location: TLocation; // in BaseSetup
    Value: ShortString;
  end;

  TExportDBRec = record
    AppKey: string;
    Data: string;
  end;

  TPrintRec = record
    Group: string;
    Name: string;
    Value: string;
    ValueUnit: string;
    SortOrder: string;
  end;
  pPrintRec = ^TPrintRec;

  TYanCntUnit = class(TObject)
  private
    fYanUnit: TYarnUnit;
  public
    property YanUnit: TYarnUnit read fYanUnit write fYanUnit;
  end;

  TOldUserGroup = class(TObject)
  private
    fChecked: Boolean;
    fState: TCheckBoxState;
  public
    property Checked: Boolean read fChecked write fChecked;
    property State: TCheckBoxState read fState write fState;
  end;

  TSetupMain = class(TBaseApplMainForm)
    GetDefault1: TMenuItem;
    SaveSettings1: TMenuItem;
    acGetDefaults: TAction;
    acSaveActivePageSettings: TAction;
    SetupPageControl: TmmPageControl;
    tsJob: TTabSheet;
    tsMMGuard: TTabSheet;
    gbCleanUpEvent: TmmGroupBox;
    GetValue1: TMenuItem;
    acGetValue: TAction;
    mmGroupBox5: TmmGroupBox;
    mmGroupBox6: TmmGroupBox;
    gbDelShiftByTime: TmmGroupBox;
    mmGroupBox7: TmmGroupBox;
    mmLabel8: TmmLabel;
    mmGroupBox8: TmmGroupBox;
    mmGroupBox9: TmmGroupBox;
    sgProcesses: TmmExtStringGrid; //TmmExtStringGrid
    mmLabel9: TmmLabel;
    mmLabel10: TmmLabel;
    mmLabel11: TmmLabel;
    acSaveAllSettings: TAction;
    tsSystem: TTabSheet;
    MMSetupModul1: TMMSetupModul;
    sseCleanUpEvent: TSetupSpinEdit;
    sseDelShiftByTime: TSetupSpinEdit;
    SetupSpinEdit5: TSetupSpinEdit;
    SetupSpinEdit7: TSetupSpinEdit;
    SetupCheckBox1: TSetupCheckBox;
    SetupLabel3: TSetupLabel;
    SetupLabel4: TSetupLabel;
    SetupLabel5: TSetupLabel;
    SetupLabel7: TSetupLabel;
    SetupLabel8: TSetupLabel;
    SetupLabel9: TSetupLabel;
    mmLabel4: TmmLabel;
    mmLabel7: TmmLabel;
    mmGroupBox10: TmmGroupBox;
    SystemPageControl: TmmPageControl;
    tsSystemCommon: TTabSheet;
    tsSystemServer: TTabSheet;
    miImport: TMenuItem;
    miExport: TMenuItem;
    acImport: TAction;
    acExport: TAction;
    tsFileVersion: TTabSheet;
    sgFileVersion: TmmExtStringGrid;
    mmDataSource1: TmmDataSource;
    SetupSpinEdit6: TSetupSpinEdit;
    SetupCheckBox2: TSetupCheckBox;
    SetupLabel6: TSetupLabel;
    SetupLabel10: TSetupLabel;
    mmGroupBox11: TmmGroupBox;
    SetupCheckBox3: TSetupCheckBox;
    SetupLabel11: TSetupLabel;
    mmLabel3: TmmLabel;
    MMSecurityDB1: TMMSecurityDB;
    MMSecurityControl1: TMMSecurityControl;
    mmLabel21: TmmLabel;
    mmLabel22: TmmLabel;
    mmLabel23: TmmLabel;
    acSettings: TAction;
    acSetSecuritiy1: TMenuItem;
    N3: TMenuItem;
    mmGroupBox4: TmmGroupBox;
    SetupLabel1: TSetupLabel;
    mmLabel1: TmmLabel;
    mmLabel5: TmmLabel;
    SetupSpinEdit1: TSetupSpinEdit;
    mmGroupBox2: TmmGroupBox;
    SetupLabel2: TSetupLabel;
    mmLabel2: TmmLabel;
    mmLabel6: TmmLabel;
    SetupSpinEdit2: TSetupSpinEdit;
    mmLabel25: TmmLabel;
    scbYarnCntUnit: TSetupComboBox;
    edHelp: TSetupEdit;
    mmLabel27: TmmLabel;
    SpeedButton2: TSpeedButton;
    gbDefaultRepSettings: TmmGroupBox;
    mmLabel30: TmmLabel;
    scbMMUnits: TSetupComboBox;
    mmBevel4: TmmBevel;
    laDataBaseVers: TmmLabel;
    lDBVersValue: TmmDBText;
    tsZEConfig: TTabSheet;
    tsFloor: TTabSheet;
    mmLabel28: TmmLabel;
    scbFloorTrendSelect: TSetupComboBox;
    mmLabel29: TmmLabel;
    SetupSpinEdit9: TSetupSpinEdit;
    GroupBox1: TmmGroupBox;
    mmLabel31: TmmLabel;
    mmLabel26: TmmLabel;
    mmLabel24: TmmLabel;
    scbMMLenghtMode: TSetupComboBox;
    scbYMLenWindow: TSetupComboBox;
    sePilotSpindles: TSetupSpinEdit;
    tsOfflimit: TTabSheet;
    mmGroupBox13: TmmGroupBox;
    mmGroupBox14: TmmGroupBox;
    sedQOfflimitLen: TSetupEdit;
    SetupLabel13: TSetupLabel;
    SetupLabel14: TSetupLabel;
    nedQOfflimitLen: TNumEdit;
    srgQOfflimitUnit: TSetupRadioGroup;
    mmLabel33: TmmLabel;
    SetupSpinEdit8: TSetupSpinEdit;
    bvCommonVert: TmmBevel;
    dsDBVers: TmmDataSource;
    lbPilotSpindles: TmmLabel;
    mmLabel35: TmmLabel;
    tsStyle: TTabSheet;
    mmPanel4: TmmPanel;
    rgSetPartieName: TSetupRadioGroup;
    mmGroupBox12: TmmGroupBox;
    SetupCheckBox4: TSetupCheckBox;
    seUserPartieName: TSetupEdit;
    speClassMatrixValue: TSetupEdit;
    mmLabel36: TmmLabel;
    NTUserMan1: TNTUserMan;
    SetupLabel12: TSetupLabel;
    scbTemplateSetsAvailable: TSetupCheckBox;
    scbShifts: TSetupComboBox;
    mmLabel34: TmmLabel;
    mmADOQuery: TmmADOQuery;
    qADODBVers: TmmADOQuery;
    tsUserAdmin: TTabSheet;
    mmPanel5: TmmPanel;
    laDomain: TmmLabel;
    laDomainServerName: TmmLabel;
    lbDomainWorkGroup: TmmLabel;
    laDomainServerNameLabel: TmmLabel;
    ToolBar1: TToolBar;
    tbLoadSecurityDef: TToolButton;
    tbLoadSecurityBackup: TToolButton;
    ToolButton3: TToolButton;
    tbSaveSecurity: TToolButton;
    tbSaveSecurityBackup: TToolButton;
    tbPrintSecurity: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    tbAddUserGroup: TToolButton;
    tbShowUserManage: TToolButton;
    acPrintSecurity: TAction;
    acSaveSecurity: TAction;
    acSaveSecurityBackup: TAction;
    acLoadDefault: TAction;
    acLoadBackup: TAction;
    acShowUserManager: TAction;
    acAddUsergroup: TAction;
    acShowMMSecurityProg: TAction;
    acShowMMSecurityUserGroup: TAction;
    acPrintSecurityAll: TAction;
    acPrintSecurityProg: TAction;
    fcStatusBar1: TfcStatusBar;
    pcSecurity: TmmPageControl;
    tsSecurityAppl: TTabSheet;
    tsSecurityGroup: TTabSheet;
    acLoadWork: TAction;
    tbLoadSecurityCustom: TToolButton;
    pSystemHeader: TPanel;
    pFileVersionHeader: TPanel;
    lbPCNameLabel: TmmLabel;
    lbPCName: TmmLabel;
    lbTypeLabel: TmmLabel;
    lbType: TmmLabel;
    laAdo: TmmLabel;
    laADOVers: TmmLabel;
    tsMMSystem: TTabSheet;
    mmLabel19: TmmLabel;
    SetupEdit1: TSetupEdit;
    mmLabel20: TmmLabel;
    SetupEdit2: TSetupEdit;
    mmLabel32: TmmLabel;
    SetupEdit3: TSetupEdit;
    edCompanyName: TSetupEdit;
    SetupLabel15: TSetupLabel;
    SetupSpinEdit3: TSetupSpinEdit;
    mmLabel12: TmmLabel;
    sbGetMMHost1: TmmSpeedButton;
    sbGetMMHost2: TmmSpeedButton;
    sbGetMMHost3: TmmSpeedButton;
    slDescription_RegMMHost: TSetupLabel;
    slDescription_RegDomainNames: TSetupLabel;
    slDescription_RegDomainController: TSetupLabel;

    procedure DisplayHint(Sender: TObject); override;

    // Actions
    procedure acGetDefaultsExecute(Sender: TObject); //
    procedure acGetValueExecute(Sender: TObject); // aus Reg. oder DB
    procedure acSaveActivePageSettingsExecute(Sender: TObject);
    procedure acSaveAllSettingsExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acExportExecute(Sender: TObject);
    procedure acImportExecute(Sender: TObject);
    // [UserAdmin]
//    procedure acSaveApplExecute(Sender: TObject);
//    procedure acMMGroupChoiceExecute(Sender: TObject);
//    procedure acMMGroupUsersExecute(Sender: TObject);

    // MainForm
    procedure FormActivate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean); // ListBox
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);

    // Tabsheeds
    procedure tsUserAdminEnter(Sender: TObject);
    procedure tsUserAdminExit(Sender: TObject);

    // Grid von MM Gaurd
    procedure sgProcessesClickCell(Sender: TObject; Arow, Acol: Integer);
    procedure sgProcessesCellValidate(Sender: TObject; Col, Row: Integer;
      var Value: string; var Valid: Boolean);
    procedure sgProcessesDblClickCell(Sender: TObject; Arow,
      Acol: Integer);
    procedure sgProcessesSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure sgProcessesKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sgProcessesMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);

    // UserAdmin
//    procedure lbGroupsKeyUp(Sender: TObject; var Key: Word;
//      Shift: TShiftState);
//    procedure cbApplChange(Sender: TObject);
//    procedure lbFormsClick(Sender: TObject);
//    procedure sgCompDescriptionClickCell(Sender: TObject; Arow, Acol: Integer);
//    procedure sgCompDescriptionSelectCell(Sender: TObject; ACol, ARow: Integer;
//      var CanSelect: Boolean);
//    procedure lbGroupsClick(Sender: TObject);
    procedure SetupPageControlChange(Sender: TObject);
    procedure acSettingsExecute(Sender: TObject);
   // procedure scbYMLenWindowKeyPress(Sender: TObject; var Key: Char);
    procedure scbYarnCntUnitSave(Sender: TObject);
    procedure scbYarnCntUnitGetValue(Sender: TObject);
    procedure scbYMLenWindowGetValue(Sender: TObject);
    procedure scbYMLenWindowSave(Sender: TObject);
    procedure scbYarnCntUnitGetDefault(Sender: TObject);
    procedure scbYMLenWindowGetDefault(Sender: TObject);
    procedure sgProcessesRightClickCell(Sender: TObject; Arow,
      Acol: Integer);
    procedure SpeedButton2Click(Sender: TObject);
    procedure sgCompDescriptionEnter(Sender: TObject);
    procedure mTranslatorLanguageChange(Sender: TObject);
    procedure scbMMUnitsGetDefault(Sender: TObject);
    procedure scbMMUnitsSave(Sender: TObject);
    procedure scbMMUnitsGetValue(Sender: TObject);
    procedure scbMMLenghtModeGetValue(Sender: TObject);
    procedure scbMMLenghtModeGetDefault(Sender: TObject);
    procedure scbMMLenghtModeSave(Sender: TObject);
    procedure rgSetPartieNameClick(Sender: TObject);
    procedure sedQOfflimitLenChange(Sender: TObject);
    procedure nedQOfflimitLenChange(Sender: TObject);
    procedure srgQOfflimitUnitClick(Sender: TObject);
    procedure scbShiftsSave(Sender: TObject);
    procedure scbShiftsGetValue(Sender: TObject);
    procedure scbShiftsGetDefault(Sender: TObject);
    procedure nedQOfflimitLenKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure nedQOfflimitLenKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MMSecurityControl1AfterSecurityCheck(Sender: TObject);
    procedure acLanguageExecute(Sender: TObject);
    procedure scbTemplateSetsAvailableClick(Sender: TObject);
    procedure acAddUsergroupExecute(Sender: TObject);
    procedure pcSecurityChange(Sender: TObject);
    procedure acSaveSecurityExecute(Sender: TObject);
    procedure acLoadDefaultExecute(Sender: TObject);
    procedure tsSecurityApplEnter(Sender: TObject);
    procedure tsSecurityGroupEnter(Sender: TObject);
    procedure acLoadWorkExecute(Sender: TObject);
    procedure acLoadBackupExecute(Sender: TObject);
    procedure acShowUserManagerExecute(Sender: TObject);
    procedure acSaveSecurityBackupExecute(Sender: TObject);
    procedure mmActionListMainExecute(Action: TBasicAction;
      var Handled: Boolean);
    procedure tsSystemCommonResize(Sender: TObject);
    procedure acPrintSecurityExecute(Sender: TObject);
    procedure mmActionListMainUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure tsSecurityGroupContextPopup(Sender: TObject;
      MousePos: TPoint; var Handled: Boolean);
    procedure acLogin1Execute(Sender: TObject);
    procedure mmLabel20Click(Sender: TObject);
    procedure tsFileVersionEnter(Sender: TObject);

    procedure sbGetMMHost1Click(Sender: TObject);
    procedure sbGetMMHost2Click(Sender: TObject);
    procedure sbGetMMHost3Click(Sender: TObject);
    procedure tsSystemCommonContextPopup(Sender: TObject; MousePos: TPoint; var
        Handled: Boolean);
  private
    fIsWorkGroup: Boolean;
    fPDCName: string;
    fOsType: string;
    fPCName: string;
    fLogonNet: string;
    fOsTypeID: Word;
    fISMMEasy: Boolean;

  private
    mValueHasChanged: Boolean;
    mCol, mRow: Word; // Grid MMGuard
    mNTProduct: Word; // NTProdukt
    mSetupCaption: string;
    mRegistratedOrg: string; // Company
    mFormHide: Boolean;
    mButton: TMouseButton;
    mDomainGroups: TStringList;
    mSettings: TBaseSettings;
    mEventLog: TEventLogWriter;
    mQOfflimitLenText: string;

   // fMMLight: Boolean;
   // fISLabMaster: Boolean;
   // fISDispoMaster: Boolean;
    fIsLocalVersion: Boolean;
    mSecurityChanged: Boolean;

    fISStandard : Boolean;
    fISPro      : Boolean;

    mMMFiles : TMMFiles;

    mInitializingThread : TInitializingThread; //Thread zum Initialisieren

    mDomainNamesThread : TDomainsThread; //Ermittelt die DomainName im Thread



    procedure CallActivePageSetupCompProc(aComp: TComponent; aProcName: string);
    procedure CallSetupCompProc(aProcName: string);
    function CheckBeforeFormClose: Boolean;
    function BooleanToText(aValue: Boolean): string;
    procedure GetDomainGroups;
    procedure GetGroupUser(aGroups: TStringList);
    procedure GetProcess;
    function ExecuteSQL(aSQLText: string): Boolean;
    function OpenSQL(aSQLText: string): TmmADOQuery;
    function PreparePrintList(aPrintList: TmmList): TmmList;
    procedure ReadSetupValuesFromFile(aFileName: string);
    function SetProcess: Boolean;
    // Setzt das Layout fuer Client oder Server (visible TabSheets & Comps)
    procedure SetNTProductLayout(aNTProduct: WORD);
    procedure WriteSetupValuesInToFile(aFileName: string);
    procedure PrintReport;
//    procedure LoadFileVersion;
//    procedure GetMMGroupState(aRow: Word);
    procedure FillUpCombobox;
    procedure FillUpMMUnitsCB;
    procedure FillUpMMLenghtModeCB;
    procedure FillUpMMShifts;
    function GetYanCntUnit(aStrList: TStringList): TStringList;
    procedure InsertProcessDLG(aRow, aCol: Integer);
    procedure CheckRegistry;
//    procedure SetSecuritiy;
    function CheckMMLight: Boolean;
    procedure UpDateData;
    procedure SetPritableConfigItems;
    procedure SetMeterToUserUnit;
    procedure SetUserUnitToMeter;
    function MeterToYds(aValue: Single): Single;
    function YdsToMeter(aValue: Single): Single;
    procedure SettingsLabDispoMaster;
    procedure GetNetAmbient; //Ermittelt PC-Name, Domain, PC-Typ, PDC-Name

    //MM-Security
    //--------------------------------------------------------------------------
    procedure CreateSecurityClasses;
    procedure DestroySecurityClasses;
    procedure ShowSequrity(var aSecurityClass: TBaseMMSecurity);
    procedure SetDefaultSecurityGroup_Into_SecurityClass;
    function CallWinUserMgr(aMrg: string): Boolean;
    procedure PrepareStatusbar_for_Security(aText: string);
    procedure ClearSecurityCBs();
    procedure ShowActualApplsGroups(aAppl, aForm, aMMGroup: string);

    function IsSecurityOk: Boolean;
    //--------------------------------------------------------------------------

//    property ISMMLight: Boolean read fMMLight write fMMLight default False;
//    property ISLabMaster: Boolean read fISLabMaster write fISLabMaster default False;
//    property ISDispoMaster: Boolean read fISDispoMaster write fISDispoMaster default False;
    property IsLocalVersion: Boolean read fIsLocalVersion write fIsLocalVersion default False;

    property ISMMEasy: Boolean read fISMMEasy write fISMMEasy default False;
    property ISStandard: Boolean read fISStandard write fISStandard default False;
    property ISPro: Boolean read fISPro write fISPro default False;


    property PCName: string read fPCName write fPCName;
    property OsType: string read fOsType write fOsType;
    property OsTypeID: Word read fOsTypeID write fOsTypeID;
    property PDCName: string read fPDCName write fPDCName;
    property IsWorkGroup: Boolean read fIsWorkGroup write fIsWorkGroup;
    property LogonNet: string read fLogonNet write fLogonNet; //Domain- oder Workgropname

  protected
    procedure Loaded; override;
  public
    procedure AfterConstruction; override;

  end;

const
  cProcessValueName = 'Process';
  cMMNeedRestart = '(*)MinValue = %d, MaxValue = %d'; //ivlm

  cProcessCheck = '(*)Ueberpruefen Sie die Prozesse im Register MMGuard !'; //ivlm
  cValueChange0 = '(*)Sie haben Werte geaendert.'; //ivlm
  cValueChange1 = '(*)Wollen Sie diese speichern?'; //ivlm
//       cValueChange      =  cValueChange0 + #13#10 + cValueChange1;
  cSetupCaption = '(*)MillMaster Konfiguration auf'; //ivlm
  cNo = '(*)Nein'; //ivlm
  cYes = '(*)Ja'; //ivlm
  cGroupUsers = '(*)Gruppenangehoerige'; //ivlm
  cProductName = '(*)Millmaster Konfiguration'; //ivlm
  cLabelProductName = '(*)Produktname'; //ivlm
  cProductVersion = '(*)Version'; //ivlm
  cAboutProduct = '(*)Ueber MillMaster Konfiguration'; //ivlm
  cChoiceDir = '(*)Verzeichnis waehlen'; //ivlm
//  cMMLight = '(*)MillMaster light Konfiguration'; //ivlm
  cMMEasy  = '(*)MillMaster EASY Konfiguration'; //ivlm

  cKFaktor = '(*)Korrekturfaktor'; //ivlm

       //Provider=SQLOLEDB.1;Data Source=wetsrvmm;Initial Catalog=MM_Winding;Password=netpmek32;User ID=MMSystemSQL

resourcestring
  rsDatei = '(*)Dateiname'; //ivlm
  rsFileDatum = '(*)Erstellungsdatum'; //ivlm
  rsLoadActualSecur = '(*)Benutzer Sicherheitseinstellungen geladen'; //ivlm
  rsLoadDefaultSecur = '(*)Standard Sicherheitseinstellungen geladen'; //ivlm
  rsLoadBackupSecur = '(*)Backup Sicherheitseinstellungen geladen'; //ivlm
  rsNoUserGroups = '(*)Es sind keine Benutzergruppen geladen'; //ivlm

  cNoPrinterInstalled = '(*)Kein Drucker installiert'; //ivlm
  cAskLoadsecurityDefault = '(*)Wollen Sie die aktuellen Sicherheitseinstellungen mit ''Standard'' ueberschreiben ?'; //ivlm
  cAskLoadsecurityBackup = '(*)Wollen Sie die aktuellen Sicherheitseinstellungen mit ''Backup'' ueberschreiben ?'; //ivlm

  rsOpenProcess = '(*)Prozesse oeffnen'; //ivlm
  rsProcessPath = '(*)Prozess-Pfad'; //ivlm
  rsPrograms = '(*)Programme'; //ivlm
  rsProgrammExists = '(*)Programm existiert bereits im %s!'; //ivlm
  rsFileNotExists = '(*)Datei %s nicht vorhanden!'; //ivlm

var
  SetupMain: TSetupMain;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS,
  LoepfeGlobal, registry, MMSecurityConfig,
  NT_vs_95, mmSetupReport, printers, mmGroupSelection,
  IvMlUtil, FileCtrl, MMSetupDataMudule, 
  u_MMSecurity_Appl, u_MMSecurity_MMGroups, ShellAPI, NetWare,
  u_SecurPrintDLG, AdoDbAccess;

{$R *.DFM}
{$R VERSION.res}

{ TSetupMain }

// MainForm
//+++++++++

procedure TSetupMain.FormActivate(Sender: TObject);
var xText: string;
begin
  inherited;

  xText := Translate(cMMNeedRestart);

  // Zusaetzliche Labels
  with mSettings do begin

   // RegMaxInterval
    mmlabel5.caption := Format(xText, [SetupSpinEdit1.MinValue,
      SetupSpinEdit1.MaxValue, BooleanToText(SetupSpinEdit1.NeedRestart)]);
   //RegIntervalLen
    mmlabel6.caption := Format(xText, [SetupSpinEdit2.MinValue,
      SetupSpinEdit2.MaxValue, BooleanToText(SetupSpinEdit2.NeedRestart)]);
   // tsJob
   // RegCUpTimePeriod
    mmlabel7.caption := Format(xText, [sseCleanUpEvent.MinValue,
      sseCleanUpEvent.MaxValue, BooleanToText(sseCleanUpEvent.NeedRestart)]);
   // RegDaysForDelShift
    mmlabel8.caption := Format(xText, [sseDelShiftByTime.MinValue,
      sseDelShiftByTime.MaxValue, BooleanToText(sseDelShiftByTime.NeedRestart)]);

   // tsMMGuard
   // RegErrResetTime
    mmlabel21.caption := Format(xText, [SetupSpinEdit5.MinValue,
      SetupSpinEdit5.MaxValue, BooleanToText(SetupSpinEdit5.NeedRestart)]);

    mmlabel22.caption := Format(xText, [SetupSpinEdit6.MinValue,
      SetupSpinEdit6.MaxValue, BooleanToText(SetupSpinEdit6.NeedRestart)]);

    mmlabel23.caption := Format(xText, [SetupSpinEdit7.MinValue,
      SetupSpinEdit7.MaxValue, BooleanToText(SetupSpinEdit7.NeedRestart)]);

    mmLabel33.caption := Format(xText, [SetupSpinEdit8.MinValue,
      SetupSpinEdit8.MaxValue, BooleanToText(SetupSpinEdit8.NeedRestart)]);

  end;

  //Wertebereich anzeigen fuer SetupSpinedit
  lbPilotSpindles.Caption := Format('[%d..%d]', [sePilotSpindles.MinValue,
    sePilotSpindles.MaxValue]);

  mmLabel35.Caption := Format(xText, [SetupSpinEdit9.MinValue,
    SetupSpinEdit9.MaxValue]);

  SettingsLabDispoMaster;

  CodeSite.SendMsg('TSetupMain.FormActivate activated');
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Speichert alle neuen Eingaben bei bedarf, wenn das Prog. beendet wird
//******************************************************************************

//------------------------------------------------------------------------------
procedure TSetupMain.FormCreate(Sender: TObject);
var
  x, I: integer;
  xComponent: TComponent;
  xMenuItem: TMenuItem;
  xAction: TAction;
  xNV, xLF, xWF, xUp: Boolean; // Non Visible, Load file, Write file, UpDate
  xParam, xFrag, xFile: string;
  xSetting: Boolean;
begin
EnterMethod('FormCreate');
  inherited;


  ProductName := cProductName;

  mEventLog := TEventLogWriter.Create(cEventLogClassForSubSystems,
    cEventLogServerNameForSubSystems,
    ssApplication, 'MMSetup: ', True);

  try
    mSettings := MMSetupModul1.mSettings;
  except
    on E: Exception do begin
      mEventLog.Write(etError, 'Error in TBaseSettings. Can not initiate MMSettings. [FormCreate]  ' + e.Message);
      MessageBeep(MB_ICONERROR);
    end;
  end;

  mFormHide := False;

 // mNTProduct:= GetNTProduct;
 // IsMMLight := CheckMMLight;

//******************************************************************************
//*** For Test only  ***********************************************************
//******************************************************************************
  {
  //IsMMLight:= TRUE;
  mNTProduct:= SERVER_NT; // SERVER_NT , WORKSTATION_NT Test NTProduct
  {}
//******************************************************************************


  if ISMMEasy then
     //Uebersteuern, wenn EASY -> WKS oder SRV
     MMSetupModul1.NTProduct := SERVER_NT
  else
     MMSetupModul1.NTProduct := mNTProduct;


  SetupPageControl.Align := alClient;

{
 // Reg. Eintraege
 try
  case mNTProduct of
    SERVER_NT,
    DOMAIN_CONTROLLER_NT,
    ENTERPRISE_EDITION_NT : begin
                             // MMSetupModul1.MakeEntries(csServer);
                             // MMSetupModul1.MakeEntries(csClient);
                             // MMSetupModul1.MakeEntries(csClientServer);
                              // Prozesse einlesen
                              GetProcess;
                            end;
    WORKSTATION_NT        : begin
                             // MMSetupModul1.MakeEntries(csClient);
                             // MMSetupModul1.MakeEntries(csClientServer);
                            end;
  end;
 except
   on E: Exception do begin
         mEventLog.Write(etError,'Error in MMSetupModul.MakeEntries. ' +
                        'Can not write the registy items. Please check your user right. ' +
                        'Admin right only. [FormCreate]  ' + e.Message);
         MessageBeep(MB_ICONERROR);
         end;
 end;
 }

//  WORKSTATION_NT

  mRegistratedOrg := GetCompanyName;
  GetProcess;  // Active Prozsse ermitteln & Max. Handlers -> sgProcesses.RowCount
  // Programm Parameter check
  xNV := False;
  xLF := False;
  xWF := False;
  xUp := False;

  for i := 1 to ParamCount do begin
    xParam := LowerCase(ParamStr(i));
    if xParam = 'nv' then
      xNV := True; // non visible    i
    xFrag := Copy(xParam, 0, 2);
    if xFrag = 'lf' then xLF := True; // Load file
    if xFrag = 'wf' then xWF := True; // Write file
    if xFrag = 'up' then xUp := True; // UpDate

    if xLF or xWF then xFile := Copy(xParam, Pos('=', xParam) + 1, length(xParam));

    // -> DB eintraege speichern
  end;

  xSetting := True;
  // Einstellungen fuer das sichtbare Form

  // Delete all MenuItems and Menu there have the Tag = 2
  for x := 0 to ComponentCount - 1 do begin
    xComponent := Components[x];
      // MenuItems
    if (xComponent is TMenuItem) then begin
      xMenuItem := Components[x] as TMenuItem;
      if xMenuItem.Tag = 2 then
        xMenuItem.Visible := False;
    end;
      // Actions
    if (xComponent is TAction) then begin
      xAction := Components[x] as TAction;
      if xAction.Category = 'CategoryNotUsed' then begin
        xAction.Visible := False;
        xAction.Caption := '';
        xAction.Hint := '';
      end;
    end;
  end;

  SetupPageControl.TabStop := False;
  mValueHasChanged := False;

  mCol := 1;
  mRow := 1;
  mmOpenDialog.InitialDir := '';

  FillUpMMUnitsCB;
  FillUpMMLenghtModeCB;

  if scbMMLenghtMode.Text = '' then scbMMLenghtMode.GetDefault;

  SetMeterToUserUnit;
  FillUpMMShifts;

  // PrimaryDomainServerName zuweisen;
  try
    NTUserMan1.MachineName := NTUserMan1.GetPrimaryDomainServerName;
    IsLocalVersion := False;
  except
    NTUserMan1.MachineName := NTUserMan1.LocalComputer;
    IsLocalVersion := True;
  end;

  // Domain Gruppen auslesen
  try
    GetDomainGroups;
  except
    on E: Exception do begin
      mEventLog.Write(etError, 'Error in  GetDomainGroups. [FormCreate] ' + e.Message);
    end;
  end;

  with mmStatusBar do begin
    SimplePanel := False;
    ShowHint := False;
    ParentShowHint := False;
    AutoHint := False;
    Panels.Add;
    Panels[0].Width := 250;
    Panels[1].Text := '';
  end;

  ShowStatusbarHint := True;

  qADODBVers.Active := True;

  if (xUp and xNV) then begin
    WindowState := wsMinimized;
    Application.Minimize;
    mFormHide := True;
    Visible := False;
    UpDateData;
    Application.Terminate;
  end
  else if (xNV or (xNV and xWF)) and not xUp then begin // Daten-File schreiben
    mFormHide := True;
    Visible := False;
    WindowState := wsMinimized;

    WriteSetupValuesInToFile(xFile);

    acSaveAllSettings.Execute;
    Application.Terminate;
  end;

  // Daten-File laden
  if xNV and xLF then begin
    Visible := False;
    mFormHide := True;
    WindowState := wsMinimized;

    ReadSetupValuesFromFile(xFile);
    acSaveAllSettings.Execute;
    Application.Terminate;
  end;

{SDO 05.04.2005
  try
    LoadFileVersion;
  except
    on E: Exception do
      mEventLog.Write(etError, e.Message);
  end;
}
  mTranslatorLanguageChange(nil);

  SetNTProductLayout(mNTProduct);

  laADOVers.Caption := '';
  with TmmADOConnection.Create(nil) do
  try
    laADOVers.Caption := Version;
  finally
    Free;
  end;
  CodeSite.SendMsg('ADO vers. : ' + laADOVers.Caption);

  mMMFiles := NIL;

end;
//------------------------------------------------------------------------------
procedure TSetupMain.FormDestroy(Sender: TObject);
begin
  mSettings := nil;
 // mDatabase.Connected:=FALSE;
  mEventLog.Free;
  RevertToSelf; //Impersonates Token zurueck geben
  CodeSite.SendMsg('TSetupMain.FormDestroy');
  DestroySecurityClasses;
  mMMFiles.Free;

  if not mInitializingThread.Terminated then begin
     mInitializingThread.Terminate;
     mInitializingThread.WaitFor;
  end;
        //Create(True)
  try
    if mDomainNamesThread.Suspended = False then begin
       mDomainNamesThread.Suspended := True;
       mDomainNamesThread.Terminate;
       mDomainNamesThread.WaitFor;
    end;

  finally
  end;

  inherited;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  inherited;
  CanClose := CheckBeforeFormClose;
end;
//------------------------------------------------------------------------------

// Actions
// +++++++

//------------------------------------------------------------------------------
procedure TSetupMain.acGetValueExecute(Sender: TObject);
var x: integer;
  xComponent: TComponent;
begin
 // inherited;
  for x := 0 to ComponentCount - 1 do begin
    xComponent := Components[x];
    CallActivePageSetupCompProc(xComponent, 'GetValue');
  end;
  //if mNTProduct <> WORKSTATION_NT then GetProcess;
  GetProcess;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.acSaveActivePageSettingsExecute(Sender: TObject);
var x: integer;
  xComponent: TComponent;
  xEdit: TSetupEdit;
  xNTProd : Word;
begin
 // inherited;
  try
    //xNTProd := mNTProduct;

    //Uebersteuern, wenn EASY -> WKS oder SRV
    //if ISMMEasy then  MMSetupModul1.NTProduct := SERVER_NT;

    SetupPageControl.SetFocus;
    SetUserUnitToMeter;

    CheckRegistry;

    mValueHasChanged := False;
    Caption := mSetupCaption;

    for x := 0 to ComponentCount - 1 do begin
      xComponent := Components[x];

      if xComponent is TSetupEdit then begin
        xEdit := (xComponent as TSetupEdit);
           //Userpartiename darf nicht uebreschreiben werden, wenn
        if xEdit.Name = 'seUserPartieName' then
          if rgSetPartieName.ItemIndex = 4 then
            CallActivePageSetupCompProc(xComponent, 'Save')
          else
        else
          CallActivePageSetupCompProc(xComponent, 'Save');
      end
      else
        CallActivePageSetupCompProc(xComponent, 'Save');
    end;
  except
    on e: exception do
      mEventLog.Write(etError, 'Error in TSetupMain.acSaveActivePageSettings.Execute : Error msg: ' + e.Message);
  end;

  try
    if SetupPageControl.ActivePage = tsMMGuard then begin
      if not SetProcess then begin
        GetProcess;
         //IvMessageBox(cProcessCheck, Translation('Warnung'), mtWarning,
         //              [mbYes], 0, Dictionary );
        MessageDlg(mDictionary.Translate(cProcessCheck), mtWarning, [mbOk], 0);

        exit;
      end;
    end;
    GetProcess;
  except
    on e: exception do
      mEventLog.Write(etError, 'Error in TSetupMain.acSaveActivePageSettings.Execute. SetupPageControl.ActivePage : Error msg: ' + e.Message);
  end;



//  MMSetupModul1.NTProduct := xNTProd;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.acSaveAllSettingsExecute(Sender: TObject);
var x: integer;
  xComponent: TComponent;
  xValue: TSetupValues;
begin
  //inherited;
  CheckRegistry;

  SetUserUnitToMeter;

  // Setup-Komp. Werte in Reg. schreiben
  for x := 0 to Self.ComponentCount - 1 do begin
    xComponent := Components[x];

    TMethod(xValue).code := xComponent.MethodAddress('Save');
    // Code und Daten der Setupkomponente zuweisen
    if Assigned(TMethod(xValue).Code) then begin
      TMethod(xValue).Data := xComponent;
      xValue;
    end;
  end;

  // Processe in Reg schreiben  (nur Server)
  if (mNTProduct > WORKSTATION_NT) or (ISMMEasy) then SetProcess;

  acSaveSecurity.Execute;

end;
//------------------------------------------------------------------------------
procedure TSetupMain.acGetDefaultsExecute(Sender: TObject);
var x: integer;
  xComponent: TComponent;
  xHelpPath: string;
begin
 // inherited;
  for x := 0 to ComponentCount - 1 do begin
    xComponent := Components[x];
      //Help Pfad nicht ueberschreiben
    if xComponent <> TComponent(edHelp) then
      CallActivePageSetupCompProc(xComponent, 'GetDefault');
  end;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.acPrintExecute(Sender: TObject);
begin
  printer.Refresh;
  if printer.Printers.Count < 1 then begin
    MessageDlg(cNoPrinterInstalled, mtWarning, [mbOK], 0);
    exit;
  end;
  if mmPrinterSetupDialog.execute then PrintReport;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.acExitExecute(Sender: TObject);
var xCanClose: boolean;
begin
  SetupPageControl.SetFocus;
  Close;
{
  self.OnCloseQuery(Sender, xCanClose);
  if xCanClose then Application.Terminate;
{}
end;
//------------------------------------------------------------------------------
procedure TSetupMain.acExportExecute(Sender: TObject);
begin
  if mmSaveDialog.Execute then
    WriteSetupValuesInToFile(mmSaveDialog.FileName);
end;
//------------------------------------------------------------------------------
procedure TSetupMain.acImportExecute(Sender: TObject);
begin
  //inherited;
  with mmOpenDialog do begin
    Filter := 'MM Setup (*.dat) |*.dat';
    Title := 'Import MM Settings';
    DefaultExt := 'dat';
    if Execute then ReadSetupValuesFromFile(FileName);
  end;
end;
//------------------------------------------------------------------------------

// Tab.Sheeds
//+++++++++++

//------------------------------------------------------------------------------
procedure TSetupMain.SetupPageControlChange(Sender: TObject);
begin
  if (SetupPageControl.ActivePage = tsUserAdmin) or
    (SetupPageControl.ActivePage = tsFileVersion) or
    not SetupPageControl.ActivePage.Enabled then begin
    acGetDefaults.Enabled := False;
    acGetValue.Enabled := False;
    acSaveActivePageSettings.Enabled := False;
  end
  else begin
    acGetDefaults.Enabled := MMSecurityControl1.CanEnabled(acGetDefaults);
    acGetValue.Enabled := MMSecurityControl1.CanEnabled(acGetValue);
    acSaveActivePageSettings.Enabled := MMSecurityControl1.CanEnabled(acSaveActivePageSettings);
  end;

  tsMMGuard.Visible := True;
  tsMMGuard.Repaint;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.tsUserAdminEnter(Sender: TObject);
begin
  acLoadWork.Enabled := gActiveSecurityClass.HasSecurityEntries;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.tsUserAdminExit(Sender: TObject);
var xText: string;
begin
{
 if acSaveAppl.Enabled then begin
   xText:= Format('%s' + cCRLF + '%s', [Translate(cValueChange0), Translate(cValueChange1)]);
   if MessageDlg(xText, mtInformation,
                  [mbYes, mbNo], 0) = mrYes then acSaveAppl.Execute;
 end;
 mmADOQuery.Active:=FALSE;
}
end;
//------------------------------------------------------------------------------

// MMGuard Grid
//+++++++++++++

//------------------------------------------------------------------------------
procedure TSetupMain.sgProcessesCellValidate(Sender: TObject; Col,
  Row: Integer; var Value: string; var Valid: Boolean);
begin
  //inherited;
  mValueHasChanged := Valid;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.sgProcessesSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  //inherited;
  mCol := ACol;
  mRow := ARow;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.sgProcessesClickCell(Sender: TObject; Arow,
  Acol: Integer);
begin
  //inherited;
  mRow := aRow;
  mCol := aCol;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.sgProcessesMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  //inherited;
  mButton := Button;
  if Button = mbRight then sgProcessesDblClickCell(Sender, mRow, mCol);
end;
//------------------------------------------------------------------------------
procedure TSetupMain.sgProcessesKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //inherited;
  case Key of
    VK_Delete, VK_BACK: sgProcesses.Cells[mCol, mRow] := '';
  end;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.sgProcessesDblClickCell(Sender: TObject; Arow,
  Acol: Integer);
begin
 // inherited;
  if (Acol = 0) or (Arow = 0) then exit;
  if (Acol = 0) and (Arow = 0) then exit;
  InsertProcessDLG(Arow, Acol);
end;
//------------------------------------------------------------------------------

// Eigene Proc.'s
//++++++++++++++++

//------------------------------------------------------------------------------
procedure TSetupMain.DisplayHint(Sender: TObject);
begin
  if ShowStatusbarHint then mmStatusBar.Panels[0].Text := GetLongHint(Application.Hint);
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Register: MMGuard
// Liest die ProzessPfade fuer den MMGuard in das Grid ein
//******************************************************************************
procedure TSetupMain.GetProcess;
var x, xCounter: integer;
  LocationName, xValueName: string;
  LocationKey: DWord;
  xMaxValue: Integer;
begin
  //x:= Ord(TRegMMValueNamesEnum(vnNumOfProcess));
  x := mSettings.GetID(cNumOfProcess);
  LocationKey := mSettings.LocationKey[x];
  LocationName := mSettings.LocationName[x];
  xValueName := cProcessValueName;
  xMaxValue := mSettings.MaxValue[x];

  xCounter := 0;
  // Grid Labels
  with sgProcesses do begin
    //Cells[1, 0] := rsProcessPath;
    ClearNormalCells;
    RowCount := xMaxValue + 1;
    for x := 1 to xMaxValue do begin
      Cells[0, x] := inttostr(x);
        // Aus Reg Lesen
      Cells[1, x] := GetRegString(LocationKey, LocationName, xValueName + IntToStr(x));
      if Cells[1, x] = '' then inc(xCounter);
    end;
    row:=1;

  //  if xMaxValue-xCounter < 5 then   // MaxValue - Leer
  //     SetupPageControl.ActivePage:= tsMMGuard;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Register: MMGuard
// Speichert die gestezten Prozesse fuer den MMGuard in die Registy
//******************************************************************************
function TSetupMain.SetProcess: Boolean;
var xPos, xCount: integer;
  LocationName, xValueName : string;
  LocationKey: DWord;
  xList: TStringList;
  xDefaultValue: Integer;
begin
  Result := True;
  xList := TStringList.Create;
  xList.Sorted := TRUE;
  xList.Duplicates := dupIgnore;

  xPos := mSettings.GetID(cNumOfProcess); // Anz. Process
  LocationKey := mSettings.LocationKey[xPos]; // LocalMachine
  LocationName := mSettings.LocationName[xPos]; // Pfad der Reg.
  xDefaultValue := StrToInt(mSettings.DefaultValue[xPos]);
  xValueName := Translate(cProcessValueName); // Process

  // Eintraege ohne Luecke zusammenfassen und Reg. leeren
  with sgProcesses do
    //Alle Prozesse in Reg. leeren
    for xCount := 1 to RowCount - 1 do begin
      //if Cells[1, xCount] <> '' then
        if FileExists(Cells[1, xCount]) then
           xList.Add(Cells[1, xCount])
        else
           if ExtractFileExt( Cells[1, xCount] ) <> '' then begin
              MessageDLG(Format( rsFileNotExists, [ Cells[1, xCount] ] ), mtWarning, [mbOk], 0);
              Result := False;
           end;

      SetRegString(LocationKey, LocationName, xValueName + IntToStr(xCount), '');
    end;

  // Prozesse in Reg. eintragen
  for xCount := 0 to xList.Count - 1 do begin
    SetRegString(LocationKey, LocationName, xValueName + IntToStr(xCount + 1), xList.Strings[xCount]);
  end;

  // Anz. Prozesse in Reg. und Const-Array schreiben  (NumOfProzess)
  mSettings.UserValue[xPos] := xList.Count;

  mSettings.NativeValue[xPos] := mSettings.UserValue[xPos];
  xValueName := mSettings.ValueName[xPos];
  SetRegString(LocationKey, LocationName, xValueName, mSettings.UserValue[xPos]);

  if xList.Count < xDefaultValue then Result := False;
  xList.Free;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Register: Alle
// Ermittelt alle SetupKomp. auf der aktiven Page und ruft fuer diese
// Komp. eine Funktion (GetDefault, GetRegValue, Save ) auf.
// Achtung: Diese Komp.Funktionen (GetDefault, etc.) muessen als published
// in der entsprechenden Setup-Komponete deklariert werden.
//******************************************************************************
procedure TSetupMain.CallActivePageSetupCompProc(aComp: TComponent;
  aProcName: string);
var xValue: TSetupValues;
  xComp: TComponent;
  x, y: string;
  xTabSheet: TTabSheet;
begin

  xComp := aComp;

 // Alle Setup-Komps. speichern, welche sich auf dem aktiven TabSheets des
 // SystemPageControl befinden
  if SetupPageControl.ActivePage = tsSystem then begin
    repeat
      xComp := xComp.GetParentComponent;
    until (xComp = SystemPageControl.ActivePage) or (xComp = nil);

    if (xComp = SystemPageControl.ActivePage) then begin
      TMethod(xValue).code := aComp.MethodAddress(aProcName);
       // Code und Daten der Setupkomponente zuweisen
      if Assigned(TMethod(xValue).Code) then begin
        TMethod(xValue).Data := aComp;
        xValue;
          // x:=aComp.Name;
      end;
    end;
  end
  else begin
    // Alle Setup-Komps. speichern, welche sich auf dem aktiven TabSheets des
    // SetupPageControl befinden

    // Alle 'TComponent.Parents' auf der ActivePage ermitteln
    repeat
      xComp := xComp.GetParentComponent;
    until (xComp = SetupPageControl.ActivePage) or (xComp = nil);

    // Eigene SetupKomp. Methode der gefundenen Setupkomponenten aufrufen
    // ( z.B. 'GetDefault' )
    if (xComp = SetupPageControl.ActivePage) then begin

      TMethod(xValue).code := aComp.MethodAddress(aProcName);
        // Code und Daten der Setupkomponente zuweisen
      if Assigned(TMethod(xValue).Code) then begin
        TMethod(xValue).Data := aComp;
        xValue;
      end;
    end;
  end
end;
//------------------------------------------------------------------------------

// *****************************************************************************
// Register: Alle
// Konvertiert einen Booleanwert in einen Text um
//******************************************************************************
function TSetupMain.BooleanToText(aValue: Boolean): string;
begin
  case aValue of
    True: Result := Translate(cYes);
    False: Result := Translate(cNo);
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Register:  User Administartion
// Ermittelt die Domaingruppen
//******************************************************************************
procedure TSetupMain.GetDomainGroups;
var xList: TStringList;
begin
 // try

  xList := NTUserMan1.GetLocalGroups('');
  xList.Duplicates := dupIgnore;
  xList.Sorted := True;

  if not IsLocalVersion then begin
      //Globale Gruppen
    mDomainGroups := NTUserMan1.GetGlobalGroups(NTUserMan1.MachineName);
    mDomainGroups.AddStrings(xList);
  end
  else begin
      //Locale Gruppen
    mDomainGroups := NTUserMan1.GetLocalGroups(NTUserMan1.MachineName);
    mDomainGroups.AddStrings(xList);
  end;

   //Doppelte Eintraege eliminieren
  xList.Clear;
  xList.AddStrings(mDomainGroups);
  xList.Sort;
  mDomainGroups.Clear;
  mDomainGroups.AddStrings(xList);
   // xList is created in GetLocalGroups method
  xList.Free;

{
   xList :=  NTUserMan1.GetGlobalGroups(NTUserMan1.MachineName);
   with lbGroups do begin
        Clear;
        Items.Assign(xList);
   end;
  except
    on E: Exception do begin
         xList.Free;
         showMessage (E.Message);
       end;
  end;
  if xList <> NIL then xList.Free;
}
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Register:  User Administartion
// Ermittelt alle User einer bestimmten Gruppenzugehoerigkeit und zeigt diese an
//******************************************************************************
procedure TSetupMain.GetGroupUser(aGroups: TStringList);
var xDomainUserList,
  xMemberOfGlob: TStrings;
  xUserInGroup: TStringList;
  x, xMemberOfCount,
    xUser: Integer;
begin
  Screen.Cursor := crHourGlass;

  xUserInGroup := TStringList.Create;
  xUserInGroup.Duplicates := dupIgnore;
  xUserInGroup.Sorted := True;

  if not IsLocalVersion then begin
     // Primary Domain festlegen
    NTUserMan1.MachineName := NTUserMan1.GetPrimaryDomainServerName;

     // Alle DomainUser in Liste schreiben
    xDomainUserList := NTUserMan1.Users;

     // Alle Domain-User nach Gruppenname durchsuchen
    for x := 0 to xDomainUserList.Count - 1 do begin
         // Zu untersuchende User zuweisen
      NTUserMan1.UserName := xDomainUserList.Strings[x];

         // Gruppen-Zugehoerigkeit auslesen
      xMemberOfGlob := NTUserMan1.MemberOfGlob;

         // User der gesuchten Gruppe auslesen
      xMemberOfCount := 0;
      while xMemberOfCount < xMemberOfGlob.Count do begin
        if aGroups.Find(xMemberOfGlob.Strings[xMemberOfCount], xUser) then
          xUserInGroup.Add(NTUserMan1.UserName);
        inc(xMemberOfCount);
      end;
    end;
  end
  else begin
    NTUserMan1.MachineName := '\\' + NTUserMan1.LocalComputer;
     // Alle LocalUser in Liste schreiben
    xDomainUserList := NTUserMan1.Users;

     // Alle LocalUser-User nach Gruppenname durchsuchen
    for x := 0 to xDomainUserList.Count - 1 do begin
         // Zu untersuchende User zuweisen
      NTUserMan1.UserName := xDomainUserList.Strings[x];

         // Gruppen-Zugehoerigkeit auslesen
      xMemberOfGlob := NTUserMan1.MemberOfLocal;

         // User der gesuchten Gruppe auslesen
      xMemberOfCount := 0;
      while xMemberOfCount < xMemberOfGlob.Count do begin
        if aGroups.Find(xMemberOfGlob.Strings[xMemberOfCount], xUser) then
          xUserInGroup.Add(NTUserMan1.UserName);
        inc(xMemberOfCount);
      end;
    end;
  end;

  xUserInGroup.Free;
  Screen.Cursor := crDefault;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Register : Alle
// Zeigt ein Resultat eines SQL-Statements an
//******************************************************************************
function TSetupMain.OpenSQL(aSQLText: string): TmmADOQuery;
begin
  Result := nil;
  try
    with mmADOQuery do begin
      Close;
      SQL.Clear;
      Parameters.Clear;
//     Params.Clear;

      SQL.Add(aSQLText);
      Open;
    end;
    Result := mmADOQuery;
  except
    on e: Exception do
      WriteLog(etError, 'DB Error in TSetupMain.OpenSQL. ' + e.message);
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Register : Alle
// Fuehrt ein SQL-Statements aus
//******************************************************************************
function TSetupMain.ExecuteSQL(aSQLText: string): Boolean;
begin
  Result := True;
  try
    with mmADOQuery do begin
      Close;
      SQL.Clear;
     //Params.Clear;
      Parameters.Clear;
      SQL.Add(aSQLText);
      ExecSQL;
    end;
  except
    on e: Exception do
      WriteLog(etError, 'DB Error in TSetupMain.ExecuteSQL. ' + e.message);
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Register : keines (Main)
// Liest ein SetupFile mit Werten ein und setzt diese ind die SetupKomp.
//******************************************************************************
procedure TSetupMain.ReadSetupValuesFromFile(aFileName: string);
var xDaten: TExportConstArrayRec;
  xCounter: Word;
  xFile: TFileStream;
  xBuffer: array[0..255] of char;
  xText: PChar;
begin
  // File auslesen

  xFile := TFileStream.Create(aFileName, fmOpenRead);
  try
  // 1) FirmenName aus Datei lesen
    FillChar(xBuffer, SizeOf(xBuffer), 0);
    xFile.ReadBuffer(xBuffer, sizeof(xBuffer));
    xText := xBuffer;
    Caption := mSetupCaption + '  [ Registrated organization = ' + Trim(xText) + ' ]';

  // 2) Datum aus Datei lesen
    FillChar(xBuffer, SizeOf(xBuffer), 0);
    xFile.ReadBuffer(xBuffer, sizeof(xBuffer));
    xText := xBuffer;
  // Dateiname und Datum auf Statuszeile ausgeben
    mmStatusBar.Panels[1].Text := Format(rsDatei + ' = %s ' + rsFileDatum + ' = %s ', [aFileName, xText]);

  // 3) Werte aus Datei lesen und in Konst.Array schreiben
    xCounter := 0;

    while xCounter < mSettings.NumberOfValues - 1 do begin
      FillChar(xDaten, SizeOf(xDaten), 0);
      xFile.ReadBuffer(xDaten, SizeOf(xDaten));
      cMMSetupDefaults[xCounter].UserValue := Trim(xDaten.Value);
      inc(xCounter);
    end;

  // Daten aus Konst. Array auslesen und Komp. zuweisen
    CallSetupCompProc('GetArrayValue');

  // 4) Prozesse Werte aus Datei lesen
    xCounter := 1;
    repeat
      FillChar(xBuffer, SizeOf(xBuffer), 0);
      xFile.Read(xBuffer, SizeOf(xBuffer));
      sgProcesses.Cells[1, xCounter] := '';
      xText := xBuffer;
      if Length(xText) > 0 then // 1 ter Buffer ist immer leer ?
        sgProcesses.Cells[1, xCounter] := xText;
      inc(xCounter);
    until (xCounter = sgProcesses.RowCount);

  //Weitere Eintraege auslesen ....
  finally
    xFile.Free;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Register : keines (Main)
// Schreibt die Werte aus den SetupKomp. in ein File
//******************************************************************************
procedure TSetupMain.WriteSetupValuesInToFile(aFileName: string);
var xFileName: string;
  xDaten: TExportConstArrayRec;
  xCounter: Word;
  xFile: TFileStream;
  xBuffer: array[0..255] of char;
begin

  // Alle Settings speichern (ausser Security)
  acSaveAllSettings.Execute;
  xFileName := aFileName;

  // Datei erstellen
  if ExtractFileExt(xFileName) = '' then
    xFileName := xFileName + mmSaveDialog.DefaultExt;
  try
    xFile := TFileStream.Create(xFileName, fmCreate or fmOpenWrite);

  //Daten einlesen fuer den Export
  //******************************

    FillChar(xBuffer, SizeOf(xBuffer), 0);
    StrCopy(xBuffer, PChar(mRegistratedOrg));

  // 1) Firmenname in Datei schreiben
    xFile.WriteBuffer(xBuffer, SizeOf(xBuffer));

  // 2) Datum in Datei schreiben
    FillChar(xBuffer, SizeOf(xBuffer), 0);
    StrCopy(xBuffer, PChar(DateToStr(Date)));
    xFile.WriteBuffer(xBuffer, SizeOf(xBuffer));

  // 3) Konst.Array in Datei schreiben
    xCounter := 0;
    repeat
      FillChar(xDaten, SizeOf(xDaten), 0);
    // Data
      xDaten.UniqueValueName := cMMSetupDefaults[xCounter].UniqueValueName;
      xDaten.Location := cMMSetupDefaults[xCounter].Location;
      xDaten.Value := cMMSetupDefaults[xCounter].UserValue;
      xFile.WriteBuffer(xDaten, SizeOf(xDaten));
      inc(xCounter);
    until xCounter = (mSettings.NumberOfValues - 1);

  // 4) Prozesse in Datei schreiben
  //sgProcesses.SaveToStream(xFile); // Achtung! funktioniert nur als einzel Datei-Eintrag
    xCounter := 1;
    repeat
      FillChar(xBuffer, SizeOf(xBuffer), 0);
      StrCopy(xBuffer, PChar(Trim(sgProcesses.Cells[1, xCounter])));
      xFile.Write(xBuffer, SizeOf(xBuffer));
      inc(xCounter);
    until (xCounter = sgProcesses.RowCount);

{
  // 5) Group Securitylevels in Datei schreiben
  try
    xSQLText:= Format('select * from t_MMUParm where AppName = ''%s'' ' ,['Security']);
    xQuery:=OpenSQL(xSQLText);
  except
    WriteLog(etError,'Can not read table t_MMUParm in TSetupMain.WriteSetupValuesInToFile');
  end;

  while not xQuery.Eof do begin
     FillChar(xDBDaten, SizeOf(xDBDaten), 0);
     xDBDaten.AppKey:=  xQuery.FieldValues['AppKey'];
     xDBDaten.Data:=    xQuery.FieldValues['Data'];
     xFile.Write(xDBDaten, SizeOf(xDBDaten));
     xQuery.next;
  end;
  //xQuery.Free;
}
  finally
    xFile.Free;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Register : keines
// Setzt das Setup-Layout anhand des NT-Produkts
//******************************************************************************
procedure TSetupMain.SetNTProductLayout(aNTProduct: WORD);
var xSetting: Boolean;
begin
aNTProduct := Server_NT;
  // Action und Tabsheed Einstellungen
  case aNTProduct of
    WORKSTATION_NT: begin
        xSetting := False;
      end;

    SERVER_NT,
      DOMAIN_CONTROLLER_NT,
      ENTERPRISE_EDITION_NT: begin
        xSetting := True;
      end;
  end;

  if ISMMEasy = True then xSetting := True;

  SetupPageControl.ActivePage := tsSystem;
  SystemPageControl.ActivePage := tsSystemCommon;

  // immer sichtbar
  tsSystem.TabVisible := True;
  tsSystemCommon.TabVisible := True;
  //tsUserAdmin.TabVisible     := TRUE;
  tsFileVersion.TabVisible := True;
  tsZEConfig.TabVisible := True;
  tsFloor.TabVisible := True;
  //tsJob.TabVisible           := TRUE;

  tsMMGuard.TabVisible := xSetting;
  tsSystemServer.TabVisible := xSetting;
  tsJob.TabVisible := xSetting;


  tsMMGuard.Update;
  tsSystemServer.Update;
  tsJob.Update;

  tsOfflimit.TabVisible := False;
  // Nur auf Server sichtbar und wenn LabMaster oder DispoMaster installiert ist
  tsStyle.TabVisible := False;   

  //if xSetting then
  //Auf WKS & SRV
  if ISPro then begin
     tsStyle.TabVisible     := TRUE;
     tsOfflimit.TabVisible  := TRUE;
  end else
     if ISStandard then begin
        tsStyle.TabVisible  := TRUE;
     end;

  {
  // Server
  if xSetting then begin
     if ISLabMaster or ISDispoMaster then begin
        tsStyle.TabVisible       := TRUE;
     end else
       tsStyle.TabVisible := FALSE;
  end;
  }

  sseDelShiftByTime.enabled := False;
  SettingsLabDispoMaster;

  //gbCleanUpEvent.Visible:= FALSE; // Wird nicht mehr angezeigt  ??
  gbCleanUpEvent.Visible := True; // Wird nicht mehr angezeigt  ??
  gbCleanUpEvent.Repaint;
  UpDate;
  Repaint;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Register : keines
// Bereitet den Druck vor. Erstellt also eine Liste mit den zu druckenden Werten
//******************************************************************************
function TSetupMain.PreparePrintList(aPrintList: TmmList): TmmList;
var xData: pPrintRec;
  xCounter, x: integer;
  xMin, xMax, xText: string;
//    xText1               : String;
  xTempValue, xValue,
    xFactor: Variant;
  xDataType: TDataType;
  xCompType: TComponentType;
  xCoputerType: TClientServerType;
  xDefPartieNamecNum,
    xDefPartieIndexcNum,
    xDefPartieIndex: Integer;
  xPrintSetPartieName: Boolean;
  xDefaultPartieText: string;
begin

  // Schreibt alle Reg.eintraege ins UserValue des Konst.Arrays
  MMSetupModul1.WriteRegToUserValue;
  x := 0;

  case mNTProduct of
    SERVER_NT,
      DOMAIN_CONTROLLER_NT,
      ENTERPRISE_EDITION_NT: xCoputerType := csServer;
    WORKSTATION_NT: xCoputerType := csClient;
  end;

  xPrintSetPartieName := False;
  xCounter := -1;

  // Konst.Array lesen und in Printliste schreiben
  repeat
    inc(xCounter);
    if cMMSetupDefaults[xCounter].Printable then begin
      Application.ProcessMessages;
      try
        new(xData);
          // Data
   {Spalte1} xData.Group := Translate(cMMSetupDefaults[xCounter].Group);
    {Spalte2} xData.Name := Translate(cMMSetupDefaults[xCounter].ValueName);
   {Spalte3} xData.Value := Translate(cMMSetupDefaults[xCounter].UserValue);
{Spalte4} xData.ValueUnit := Translate(cMMSetupDefaults[xCounter].ValueText);
        xFactor := cMMSetupDefaults[xCounter].Factor;
        xMin := cMMSetupDefaults[xCounter].MinValue;
        xMax := cMMSetupDefaults[xCounter].MaxValue;
        xDataType := cMMSetupDefaults[xCounter].ValueType;
        xCompType := cMMSetupDefaults[xCounter].ComponentType;

        inc(x);

        case xCompType of
                         // Text Konvertierung fuer Checkboxen (Yes/No)
          ctCheckBox: begin
              if xData.Value = '1' then
                xData.Value := Translate(cYes)
              else
                xData.Value := Translate(cNo);

              if cMMSetupDefaults[xCounter].UniqueValueName = cCorrectionFactorImp then begin
                xData.Name := xData.ValueUnit;
                xData.ValueUnit := xData.Value;
                xData.Value := '';
              end;
            end;
          ctComboBox: begin
              if cMMSetupDefaults[xCounter].UniqueValueName = 'SQLYarnCntUnit' then
                xData.Value := scbYarnCntUnit.Text;

              if cMMSetupDefaults[xCounter].UniqueValueName = 'SQLPilotSpindles' then
                xData.Value := IntToStr(sePilotSpindles.Value);

              if cMMSetupDefaults[xCounter].UniqueValueName = 'SQLYMLenWindow' then
                xData.Value := scbYMLenWindow.Text;

              if cMMSetupDefaults[xCounter].UniqueValueName = 'SQLMMUnits' then
                xData.Value := scbMMUnits.Text;

              if cMMSetupDefaults[xCounter].UniqueValueName = 'SQLMMUnits' then
                xData.Value := scbMMUnits.Text;

              if cMMSetupDefaults[xCounter].UniqueValueName = 'SQLMMLenghtMode' then
                xData.Value := scbMMLenghtMode.Text;

              if cMMSetupDefaults[xCounter].UniqueValueName = 'FloorTrendSelection' then
                xData.Value := scbFloorTrendSelect.Text;

            end;
          ctRadioGroup: begin
              if cMMSetupDefaults[xCounter].UniqueValueName = cDefaultPartieIndex then begin
                              // Siehe auch Spez. Abhandlung;
                if StrToInt(xData.Value) = 4 then
                  xPrintSetPartieName := True;
                xData.Name := xData.ValueUnit;
                xData.ValueUnit := rgSetPartieName.Items.Strings[StrToInt(xData.Value)];
                xData.Value := '';
                xDefaultPartieText := xData.ValueUnit;
              end;
            end;
        end; //case

          // Spez. Abhandlung; HelpPath hat keine Einheit
        if cMMSetupDefaults[xCounter].UniqueValueName = 'HelpFilePath' then
          xData.ValueUnit := '';

          // Angezeigte Werte ausgeben
        if (xMin <> '') and (xMax <> '') and (xData.Value <> '') then begin
          xValue := xData.Value;
          try
            case xDataType of
              dtWord,
                dtDWord,
                dtInteger: xTempValue := Integer(xValue) / Integer(xFactor);
              dtFloat: xTempValue := xValue / xFactor;
            else
              xTempValue := xValue;
            end;
          except
            xTempValue := xValue;
          end;
          xData.Value := xTempValue;
        end;

          // Spez. Abhandlung;
        if (cMMSetupDefaults[xCounter].UniqueValueName = 'SQLDefaultPartieNameSet') then
             //Default Partiename drucken, wenn Default PartieIndex = 4
          if xPrintSetPartieName then begin
            xData.Name := xDefaultPartieText;
            xData.ValueUnit := '';
            xData.SortOrder := xData.Group + xData.Name;
            aPrintList.Add(xData);
          end
          else
                //Default Partiename nicht drucken, wenn Default PartieIndex < 4
            Dispose(xData)
        else begin
              // Alle andern drucken
          xData.SortOrder := xData.Group + xData.Name;
          aPrintList.Add(xData);
        end;
      except
        on E: Exception do begin
          xText := Format('TSetupMain.PreparePrintList (cMMSetupDefaults[%d ] ). Error msg: %s ', [xCounter, e.Message]);
          CodeSite.SendError(xText);
        end;
      end;
    end;
  until xCounter = mSettings.NumberOfValues - 1;

  // Processe lesen und in Printliste schreiben
  xCounter := 1;
  if (xCoputerType = csServer) or (xCoputerType = csClientServer) or (ISMMEasy) then
    repeat
      if sgProcesses.Cells[1, xCounter] <> '' then begin
        new(xData);
        xData.Group := 'MMGuard';
        xData.Name := 'Process' + IntToStr(xCounter);
        xData.Value := Trim(sgProcesses.Cells[1, xCounter]);
        xData.ValueUnit := '';

        xData.SortOrder := xData.Group + xData.Name;
        aPrintList.Add(xData);
      end;
      inc(xCounter);
    until (xCounter = sgProcesses.RowCount);

  // Fileversion lesen
  xCounter := 1;
  xText := GetTabSheet(sgFileVersion);
  repeat
   // Nur Files mit Fileversion auslesen
    if sgFileVersion.Cells[1, xCounter] <> '' then begin
      new(xData);
      xData.Group := xText;
      xData.Name := ExtractFileName(Trim(sgFileVersion.Cells[0, xCounter]));
      xData.Value := '';
      xData.ValueUnit := Trim(sgFileVersion.Cells[1, xCounter]);

      xData.SortOrder := xData.Group + xData.Name;
      aPrintList.Add(xData);
      //Dispose(xData);
    end;
    inc(xCounter);
  until (xCounter = sgFileVersion.RowCount);

  Result := aPrintList;

end;
//------------------------------------------------------------------------------

//******************************************************************************
// Register : keines
// Ueberprueft vor dem verlassen des Programms, ob alle geaenderten Daten
// gespeichert wurden.
//******************************************************************************
function TSetupMain.CheckBeforeFormClose: Boolean;
var x: integer;
  xChange: Boolean;
  xSeltext, xText: string;
  xRes: Word;
begin
  Result := True;
  // Hat sich etwas im sgProcess-Grid geaendert ?
  // -> sgProcess-Grid aendern mit anschliessenden exit
  xSeltext := sgProcesses.SelectedText;
  if not mValueHasChanged then
    if (sgProcesses.OriginalCellValue <> xSeltext) and
      (sgProcesses.OriginalCellValue <> '') then mValueHasChanged := True;
  x := -1;
  // Uberpruefen, ob es noch nicht gespeicherte Werte hat
  repeat
    inc(x);
    xChange := mSettings.ValueChange[x];
  until xChange or (mSettings.NumberOfValues - 1 = x);

  xText := Format('%s' + cCRLF + '%s', [Translate(cValueChange0), Translate(cValueChange1)]);
  if mValueHasChanged or xChange or mSecurityChanged then begin
    xRes := MessageDlg(xText, mtConfirmation,
      [mbYes, mbNo, mbCancel], 0);

    if xRes = mrYes then
      acSaveAllSettings.Execute;
    Result := xRes in [mrYes, mrNo];
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Register : keines
// Druckt die Werte aus
//******************************************************************************
procedure TSetupMain.PrintReport;
var xName: PChar;
  xsize: DWord;
  xProd: string;
  xOrgDateFormat: string;
  xPrintList: TmmList;
  xDrucker: string;
begin
 //inherited;

  acSaveAllSettings.Execute;

  Application.ProcessMessages;

  Screen.Cursor := crHourGlass;

  // Printliste erstellen und auffuellen
  xPrintList := TmmList.Create;
  xPrintList := PreparePrintList(xPrintList);
  if xPrintList.Count <= 0 then begin
    Screen.Cursor := crDefault;
    exit;
  end;

  Application.ProcessMessages;

  xsize := 255;
  xName := StrAlloc(256);
  with TSetupReport.Create(self, xPrintList) do begin
     // Header settings
    qlCompany.Caption := mRegistratedOrg; // Firmenname
    xProd := GetNTProduct(mNTProduct);
    qlComputer.Caption := xProd; // NT-Produkt
    GetComputerName(xName, xsize);
    qlComputername.Caption := Trim(xName); // Computername

     // Datum temp. auf long setzen
    ShortDateFormat := ShortDateFormat;
    ShortDateFormat := LongDateFormat;
    qlDate.Caption := DateTimeToStr(Now); // langes Datum
    ShortDateFormat := ShortDateFormat;

     // Aktuelle Printersettings dem QuickRep1 ueberweisen
    QuickRep1.Page.Orientation := Printer.Orientation;
    QuickRep1.ReportTitle := 'MillMaster settings'; // Header Titel
    QuickRep1.PrinterSettings.Title := QuickRep1.ReportTitle;
    QuickRep1.PrinterSettings.PrinterIndex := Printer.PrinterIndex;
    QuickRep1.ShowProgress := True;

     //Print Report
    mmStatusBar.Panels[1].Text := Translate('(*)Ausgabe auf:') +
      ' ' + Printer.Printers.Strings[Printer.PrinterIndex];
     //QuickRep1.Print;
    Application.ProcessMessages;

    QuickRep1.Preview;
    Free;
  end;
  StrDispose(xName);

  xPrintList.Clear;
  xPrintList.Free;
  Sleep(5000);
  Screen.Cursor := crDefault;
  mmStatusBar.Panels[1].Text := '';
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Register : keines
// Ruft alle Setupkomponenten auf und fuehrt eine angegebene Funktion aus
//******************************************************************************
procedure TSetupMain.CallSetupCompProc(aProcName: string);
var i: integer;
  xValue: TSetupValues;
  xComponent: TComponent;
begin

  for i := 0 to ComponentCount - 1 do begin
    xComponent := Components[i];
    TMethod(xValue).code := xComponent.MethodAddress(aProcName);
      // Code und Daten der Setupkomponente zuweisen
    if Assigned(TMethod(xValue).Code) then begin
      TMethod(xValue).Data := xComponent;
      xValue;
    end;
      //CallActivePageSetupCompProc(xComponent, 'GetValue');
  end;

// xComp:= aComp;
 {
 // Alle 'TComponent.Parents' auf der ActivePage ermitteln
 repeat
    xComp := xComp.GetParentComponent;
 until (xComp = SetupPageControl.ActivePage) or  (xComp = NIL);

 // Eigene SetupKomp. Methode der gefundenen Setupkomponenten aufrufen
 // ( z.B. 'GetDefault' )
// if (xComp =  SetupPageControl.ActivePage)  then begin
    TMethod(xValue).code := aComp.MethodAddress(aProcName);
    // Code und Daten der Setupkomponente zuweisen
    if Assigned(TMethod(xValue).Code) then begin
       TMethod(xValue).Data := aComp;
       xValue;
    end;
 end;
}
end;
//------------------------------------------------------------------------------

(*
//******************************************************************************
// Register : Datei Version
// List die Dateiversion aus einer Exe- oder DLL-Datei und zeigt diese an.
// Die auszulsesenden Millmasterdateien befinden sich in einem File, welches
// vom InstallShield erstellt wurde.
//******************************************************************************
procedure TSetupMain.LoadFileVersion;
var xFile: TFileStream;
  xFileName,
    xDir, x: string;
  xData: PChar;
  xText: PChar;
  xSize: DWord;
  i, xpos: integer;
  xRow: integer;
  xItem: string;
  xFileDate: integer;
begin

  xFileName := 'FileVersion.txt';
  xFileName := GetRegString(HKEY_LOCAL_MACHINE, '\SOFTWARE\Loepfe\MillMaster', 'FileList', xFileName);

  if xFileName = '' then exit;

  try
    xFile := TFileStream.Create(xFileName, fmOpenRead);
  except
    xDir := ExtractFilePath(xFileName);
    try
      xFile := TFileStream.Create(xFileName, fmCreate);
    except
      xFileName := ExtractFileName(xFileName);
      xFile := TFileStream.Create(xFileName, fmCreate);
    end;
  end;

  xSize := xFile.Size;

  xFile.Position := 0;
  GetMem(xData, xFile.Size);
  xFile.Read(xData^, xFile.Size);

  xRow := 1;
  sgFileVersion.RowCount := xRow + 1;
  xText := xData;
  xpos := 0;
  for i := 0 to xSize - 1 do begin
     // Scannen bis #13 (ENTER) und #10 (LineFeed) oder letztes Zeichen, dann Ausgabe
    if (xText^ = #13) and ((xText + 1)^ = #10) or (i = xFile.Size - 1) then begin
        //Letzter Eintrag
      if (i = xFile.Size - 1) then
        xItem := copy(StrPas(xData), 0, Pos('.', StrPas(xData)) + 3)
      else
          // 1. bis zweitletzter Eintrag
        xItem := copy(StrPas(xData), 0, pos(#$D, StrPas(xData)) - 1);

      Trim(xItem);
      sgFileVersion.Cells[0, xRow] := xItem;
      sgFileVersion.Cells[1, xRow] := GetFileVersion(xItem);
      if sgFileVersion.Cells[1, xRow] = '' then begin
        xFileDate := FileAge(xItem);
        if xFileDate > 0 then
          sgFileVersion.Cells[1, xRow] := DateToSTr(FileDateToDateTime(xFileDate))
        else
          sgFileVersion.Cells[1, xRow] := 'File not exists';
      end;
      xData := xText + 2; // zwei Zeichen ENTER + LINEFEED

        // Nur Zellen mit Daten zeigen
      if (sgFileVersion.Cells[0, xRow] <> '') or
        (length(sgFileVersion.Cells[0, xRow]) > 5) then begin
        inc(xRow);
        sgFileVersion.RowCount := xRow;
      end;
    end;
    inc(xText);
  end;

  xRow := sgFileVersion.RowCount;
  if length(sgFileVersion.Cells[0, xRow]) < 5 then dec(xRow);
  sgFileVersion.RowCount := xRow;

  xFile.Free;
end;
//------------------------------------------------------------------------------
*)
//------------------------------------------------------------------------------
procedure TSetupMain.acSettingsExecute(Sender: TObject);
var x: integer;
  xAppFound: Boolean;
  xForm: string;
begin
  MMSecurityControl1.Configure;
  Application.ProcessMessages;
 {
  xAppFound:=FALSE;

  // Sucht nach
  for x:= 0 to cbAppl.Items.Count-1 do
     if cbAppl.Items.Strings[x] = 'mmSetup' then begin
        cbAppl.ItemIndex:=x;
        xAppFound:=TRUE;
        break;
     end;

  if (cbAppl.Text = 'mmSetup') or not xAppFound then begin
     MMSecurityDB1.LoadSecurityInfo;

     // mmSetup in CB vorhanden
     if xAppFound then begin
        cbApplChange( Sender );
        lbForms.ItemIndex:=0;
        lbFormsClick( Sender);
     end else begin
        // mmSetup nicht in CB vorhanden
        tsUserAdminExit( Sender );
        tsUserAdminEnter( Sender );
        // mmSetup ist jetzt in CB vorhanden
        for x:= 0 to cbAppl.Items.Count-1 do
           if cbAppl.Items.Strings[x] = 'mmSetup' then begin
              cbAppl.ItemIndex:=x;
              cbApplChange( Sender );
              lbForms.ItemIndex:=0;
              lbFormsClick( Sender);
              break;
           end;
     end;
     // Speichern
     acSaveAppl.Execute;
  end;
 }

  mSecurityChanged := MMSecurityControl1.ControllsHasChanged;

  MMSecurityDB1.LoadSecurityInfo;

  gActiveSecurityClass := (gActualSecurity as TBaseMMSecurity);
  gActiveSecurityClass.Init;
  ShowSequrity(gActiveSecurityClass);

end;
//------------------------------------------------------------------------------

function TSetupMain.GetYanCntUnit(aStrList: TStringList): TStringList;
var x: TYarnUnit;
  xYanUnitObj: TYanCntUnit;
begin
  for x := yuNm to high(x) do begin
    xYanUnitObj := TYanCntUnit.Create;
    xYanUnitObj.YanUnit := x;
    aStrList.AddObject(cYarnUnitsStr[x], xYanUnitObj);
  end;
  Result := aStrList;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.scbYarnCntUnitSave(Sender: TObject);
var xText: string;
  xYarnUnits: TYarnUnit;
  xYanUnitObj: TYanCntUnit;
  x: integer;
begin
  x := scbYarnCntUnit.ItemIndex;
  xYanUnitObj := TYanCntUnit(scbYarnCntUnit.Items.Objects[x]);
  xYarnUnits := xYanUnitObj.YanUnit;
  x := ord(xYarnUnits);
  mSettings.DBValue[mSettings.GetID(scbYarnCntUnit.ValueName)] := InttoStr(x);
end;
//------------------------------------------------------------------------------
procedure TSetupMain.FillUpCombobox;
var xStringList: TStringList;
begin
  // CB mit Werten abfuellen
  if scbYarnCntUnit.Items.Count <= 0 then begin
    xStringList := TStringList.Create;
    xStringList := GetYanCntUnit(xStringList);
    scbYarnCntUnit.Clear;
    scbYarnCntUnit.Items.AddStrings(xStringList);
    xStringList.Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.scbYarnCntUnitGetValue(Sender: TObject);
var xText: string;
  xYarnUnits: TYarnUnit;
  xYanUnitObj: TYanCntUnit;
  x, xValue: integer;
  xID: Word;
begin

  xID := MMSetupModul1.mSettings.GetID(scbYarnCntUnit.ValueName);
  xText := MMSetupModul1.mSettings.DBValue[xID];

  try
    if xText <> '' then
     // Value aus DB
      xValue := StrToInt(xText)
    else begin
     // Default Value
      xValue := StrToInt(MMSetupModul1.mSettings.DefaultValue[xID]);
     // Default Value auf DE
      MMSetupModul1.mSettings.DBValue[xID] := InttoStr(xValue);
    end;
  except
   // Default Value
    xValue := StrToInt(MMSetupModul1.mSettings.DefaultValue[xID]);
  end;

  xYarnUnits := TYarnUnit(xValue);
  if scbYarnCntUnit.Items.Count <= 0 then FillUpCombobox;

  scbYarnCntUnit.ItemIndex := 1;
  for x := 0 to scbYarnCntUnit.Items.Count - 1 do begin
    xYanUnitObj := TYanCntUnit(scbYarnCntUnit.Items.Objects[x]);
    if xYarnUnits = xYanUnitObj.YanUnit then begin
      scbYarnCntUnit.ItemIndex := x;
      break;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.scbYMLenWindowGetValue(Sender: TObject);
var xValue: string;
  x: integer;
  xID: Word;
  xData: string;
begin

  xID := MMSetupModul1.mSettings.GetID(scbYMLenWindow.ValueName);
  xValue := MMSetupModul1.mSettings.DBValue[xID];

  try
    if xValue = '' then begin
      xValue := MMSetupModul1.mSettings.DefaultValue[xID];
     // Default Value auf DB
      MMSetupModul1.mSettings.DBValue[xID] := xValue;
    end;
  except
   // Default Value
    xValue := MMSetupModul1.mSettings.DefaultValue[xID];
  end;

 //xYarnUnits:=  TYarnUnit (xValue);
  if scbYMLenWindow.Items.Count <= 0 then begin
    xData := MMSetupModul1.mSettings.Data[xID];

  end;
 //FillUpCombobox;

  scbYMLenWindow.ItemIndex := 1;

  for x := 0 to scbYMLenWindow.Items.Count - 1 do
    if xValue = scbYMLenWindow.Items.Strings[x] then begin
      scbYMLenWindow.ItemIndex := x;
      MMSetupModul1.mSettings.UserValue[xID] := xValue;
      break;
    end;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.scbYMLenWindowSave(Sender: TObject);
begin
  mSettings.DBValue[mSettings.GetID(scbYMLenWindow.ValueName)] := scbYMLenWindow.Text
end;
//------------------------------------------------------------------------------
procedure TSetupMain.scbYarnCntUnitGetDefault(Sender: TObject);
var xText: string;
  xYarnUnits: TYarnUnit;
  xYanUnitObj: TYanCntUnit;
  x: integer;
  xID: Word;
begin

  xID := MMSetupModul1.mSettings.GetID(scbYarnCntUnit.ValueName);
  xText := MMSetupModul1.mSettings.DefaultValue[xID];

  xYarnUnits := TYarnUnit(StrToInt(xText));
  if scbYarnCntUnit.Items.Count <= 0 then FillUpCombobox;

  scbYarnCntUnit.ItemIndex := 1;
  for x := 0 to scbYarnCntUnit.Items.Count - 1 do begin
    xYanUnitObj := TYanCntUnit(scbYarnCntUnit.Items.Objects[x]);
    if xYarnUnits = xYanUnitObj.YanUnit then begin
      scbYarnCntUnit.ItemIndex := x;
      break;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.scbYMLenWindowGetDefault(Sender: TObject);
var xValue: string;
  x: integer;
  xID: Word;
begin

  xID := MMSetupModul1.mSettings.GetID(scbYMLenWindow.ValueName);
  xValue := MMSetupModul1.mSettings.DefaultValue[xID];

 //xYarnUnits:=  TYarnUnit (xValue);
  if scbYMLenWindow.Items.Count <= 0 then FillUpCombobox;

  scbYMLenWindow.ItemIndex := 1;
  for x := 0 to scbYMLenWindow.Items.Count - 1 do
    if xValue = scbYMLenWindow.Items.Strings[x] then begin
      scbYMLenWindow.ItemIndex := x;
      break;
    end;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.AfterConstruction;
var xMask: string;
  xDecSep: string;
  x: integer;
begin
  inherited;
  //CodeSite.SendMsg('Begin TSetupMain.AfterConstruction');
  EnterMethod('TSetupMain.AfterConstruction');
  CodeSite.SendDateTime('Time : ' , Now);

  SetPritableConfigItems;

  MMSecurityControl1.FormCaption := cProductName;

  acShowMMSecurityProg.Execute;
  acShowMMSecurityUserGroup.Execute;

  //Werte changer
  for x:= 0 to mSettings.NumberOfValues -1 do
      mSettings.ValueChange[x]:= FALSE;

  CodeSite.SendMsg('Set all properties ValueChange in mSettings to FALSE; Count of mSettings : ' + IntToStr(mSettings.NumberOfValues) );

  mSecurityChanged := FALSE;
  mValueHasChanged := FALSE;

  CodeSite.SendDateTime('Time : ' , Now);  
  //CodeSite.SendMsg('End TSetupMain.AfterConstruction');
end;
//------------------------------------------------------------------------------
procedure TSetupMain.sgProcessesRightClickCell(Sender: TObject; Arow,
  Acol: Integer);
begin
  InsertProcessDLG(Arow, Acol);
end;
//------------------------------------------------------------------------------
procedure TSetupMain.InsertProcessDLG(aRow, aCol: Integer);
var x: integer;
    xHandlerList : TStringList;
begin

  xHandlerList := TStringList.Create;
  xHandlerList.Sorted := TRUE;
  xHandlerList.Duplicates :=  dupIgnore;

  x := 0;
  if mmOpenDialog.InitialDir <> '' then begin
    repeat
      inc(x);
    until (sgProcesses.Cells[1, x] <> '') or (x = sgProcesses.RowCount);

    if sgProcesses.Cells[1, x] <> '' then
      mmOpenDialog.InitialDir := ExtractFileDir(sgProcesses.Cells[1, x]);
  end;

  for x:= 1 to sgProcesses.RowCount do
     if sgProcesses.Cells[1, x] <> '' then
        xHandlerList.Add(sgProcesses.Cells[1, x]);

  mmOpenDialog.Filter := Format('%s (*.exe) |*.exe',[rsPrograms]);
  mmOpenDialog.Title := rsOpenProcess;
  mmOpenDialog.DefaultExt := 'exe';
  if mmOpenDialog.Execute then begin

    if xHandlerList.IndexOf( mmOpenDialog.FileName ) = -1 then begin
       //Neuer Process
       mValueHasChanged := True;
       sgProcesses.Cells[Acol, Arow] := mmOpenDialog.FileName;
    end else begin
       MessageDlg( Format(rsProgrammExists ,[rsProcessPath]) , mtWarning, [mbOk], 0);
       sgProcesses.Cells[Acol, Arow] := '';
    end;

    mmOpenDialog.InitialDir := ExtractFileDir(mmOpenDialog.FileName);
  end;

  xHandlerList.Free;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.SpeedButton2Click(Sender: TObject);
var
  xDir: string;
begin
  xDir := edHelp.text;
  if not DirectoryExists(xDir) then xDir := '';
  if SelectDirectory(Translate(cChoiceDir), '', xDir) then edHelp.text := xDir;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.CheckRegistry;
begin
  // Reg. Eintraege
  try
    case mNTProduct of
      SERVER_NT,
        DOMAIN_CONTROLLER_NT,
        ENTERPRISE_EDITION_NT: begin
          MMSetupModul1.MakeEntries(csServer);
          MMSetupModul1.MakeEntries(csClient);
          MMSetupModul1.MakeEntries(csClientServer);
                              // Prozesse einlesen
                              //GetProcess;
        end;
      WORKSTATION_NT: begin
          MMSetupModul1.MakeEntries(csClient);
          MMSetupModul1.MakeEntries(csClientServer);
          if ISMMEasy then MMSetupModul1.MakeEntries(csServer);
        end;
    end;

  except
    on E: Exception do begin
      mEventLog.Write(etError, 'Error in MMSetupModul.MakeEntries. ' +
        'Can not write the registy items. Please check your user right. ' +
        'Admin right only. [FormCreate]  ' + e.Message);
      MessageBeep(MB_ICONERROR);
    end;
  end;
end;
//------------------------------------------------------------------------------
{
procedure TSetupMain.SetSecuritiy;
var xCompName, xGroup,
    xForm              : String;
    x, i, xItemSelect  : Integer;
    xState             : TSecurityState;
begin

  xItemSelect:= sgCompDescription.RowSelectCount;

  for i:= 1 to sgCompDescription.rowcount do
      if sgCompDescription.IsSelected(0,i) then begin
         xCompName := sgCompDescription.Cells[0, i];
         for x:=0 to lbGroups.Items.Count-1 do begin
             xGroup := lbGroups.Items.Strings[x];
             case lbGroups.State[x] of
                  cbUnchecked: xState := ssHide;     // 0
                  cbGrayed:    xState := ssVisible;  // 1
                  cbChecked:   xState := ssEnabled;  // 2
             else
                  xState := ssHide;     // 0
             end; //END Case
             scUserAdmin.SetGroupState(xCompName, xGroup, xState);
         end; // end for
      end; // end if
  xForm:= lbForms.Items.Strings[lbForms.itemindex];
  scUserAdmin.SaveForm( xForm );
end;
//------------------------------------------------------------------------------
}
//******************************************************************************
// Gruppenrechte abspeichern
//******************************************************************************
procedure TSetupMain.sgCompDescriptionEnter(Sender: TObject);
begin
  //sdbUserAdmin.SaveAppl( cbAppl.Text );
  //SetSecuritiy;

  //if cbAppl.Text = 'mmSetup' then begin
  //   MMSecurityDB1.LoadSecurityInfo;
  //   MMSecurityControl1.LoadForm('SetupMain');
  //end;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.mTranslatorLanguageChange(Sender: TObject);
var x: integer;
  xComponent: TComponent;
  xValue: TSetupValues;
begin
  inherited;

  MMSecurityMMGroups.mmTranslator1.Translate;
  MMSecurityAppl.mmTranslator1.Translate;

   //gActiveSecurityClass.Translate;
  // Translate

  {
  if (sgCompDescription.rowcount > 1) and (sgCompDescription.Cells[0,1] <> '') then
  for x:= 0 to scUserAdmin.Count-1 do begin
      sgCompDescription.Cells[0, x+1]:= scUserAdmin.Name[x];
      sgCompDescription.Cells[1, x+1]:= Translate(scUserAdmin.Comment[x]);
  end;

  }

  if not IsMMEasy then begin
    mSetupCaption := Format('%s %s', [Translate(cSetupCaption), GetNTProduct(mNTProduct)]);
  end
  else begin
    mSetupCaption := Translate(cMMEasy);
  end;

  {
  with TAdoDBAccess.Create(1, false) do begin
        Init;
        mSetupCaption :=  Format('%s     SQL-Server host : %s',[mSetupCaption, HostName]);
        Free;
  end;
  }

  Caption := mSetupCaption;

  mmGroupBox12.Caption := Translate(cKFaktor);

  for x := 0 to ComponentCount - 1 do begin
    xComponent := Components[x];

    TMethod(xValue).code := xComponent.MethodAddress('Translation');
      // Code und Daten der Setupkomponente zuweisen
    if Assigned(TMethod(xValue).Code) then begin
      TMethod(xValue).Data := xComponent;
      xValue;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.FillUpMMUnitsCB;
var x, xID: Word;
  xText, xData: string;
begin
  scbMMUnits.Clear;

  for x := 0 to integer(High(cLenBaseTxt)) do begin
    xID := Integer(cLenBaseTxt[x].ID);
    scbMMUnits.items.Insert(xID, Translate(cLenBaseTxt[x].Text));
    if x = 0 then
      xData := '"' + cLenBaseTxt[x].Text + '",'
    else
      xData := xData + '"' + cLenBaseTxt[x].Text + '",';
  end;

  delete(xData, length(xData), 1);
  scbMMUnits.NativeData := xData;

  scbMMUnits.GetValue;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.scbMMUnitsGetDefault(Sender: TObject);
var x, xID: Word;
  xText: string;
begin
  xID := MMSetupModul1.mSettings.GetID(scbMMUnits.ValueName);
  xText := MMSetupModul1.mSettings.DefaultValue[xID];

  try
    x := StrToInt(xText);
  except
    x := 1;
  end;
  scbMMUnits.ItemIndex := x;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.scbMMUnitsSave(Sender: TObject);
var x: integer;
begin
  x := scbMMUnits.ItemIndex;
  mSettings.DBValue[mSettings.GetID(scbMMUnits.ValueName)] := InttoStr(x);
end;
//------------------------------------------------------------------------------
procedure TSetupMain.scbMMUnitsGetValue(Sender: TObject);
var xText: string;
  x, xValue: integer;
  xID: Word;
begin

  xID := MMSetupModul1.mSettings.GetID(scbMMUnits.ValueName);
  xText := MMSetupModul1.mSettings.DBValue[xID];

  try
    if xText <> '' then
     // Value aus DB
      xValue := StrToInt(xText)
    else begin
     // Default Value
      xValue := StrToInt(MMSetupModul1.mSettings.DefaultValue[xID]);
     // Default Value auf DB
      MMSetupModul1.mSettings.DBValue[xID] := InttoStr(xValue);
    end;
  except
   // Default Value
    xValue := StrToInt(MMSetupModul1.mSettings.DefaultValue[xID]);
  end;

  scbMMUnits.ItemIndex := abs(xValue);
end;
//------------------------------------------------------------------------------
procedure TSetupMain.FillUpMMLenghtModeCB;
var x, xID: Word;
  xText, xData: string;

begin
  xID := MMSetupModul1.mSettings.GetID(scbMMUnits.ValueName);

  scbMMLenghtMode.Clear;
  for x := 0 to 2 do begin
    scbMMLenghtMode.items.Add(Translate(cLengthMode[x].Text));
    if x = 0 then
      xData := cLengthMode[x].Text + ','
    else
      xData := xData + cLengthMode[x].Text + ',';
  end;

  delete(xData, length(xData), 1);
  scbMMLenghtMode.NativeData := xData;
  scbMMLenghtMode.GetValue;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.scbMMLenghtModeGetValue(Sender: TObject);
var xText: string;
  x, xValue: integer;
  xID: Word;
begin

  xID := MMSetupModul1.mSettings.GetID(scbMMLenghtMode.ValueName);
  xText := MMSetupModul1.mSettings.DBValue[xID];

  try
    if xText <> '' then
     // Value aus DB
      xValue := StrToInt(xText)
    else begin
     // Default Value
      xValue := StrToInt(MMSetupModul1.mSettings.DefaultValue[xID]);
     // Default Value auf DE
      MMSetupModul1.mSettings.DBValue[xID] := InttoStr(xValue);
    end;
  except
   // Default Value
    xValue := StrToInt(MMSetupModul1.mSettings.DefaultValue[xID]);
  end;

  for x := 0 to 2 do begin
    if cLengthMode[x].Id = xValue then xValue := x;
  end;

  scbMMLenghtMode.ItemIndex := xValue;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.scbMMLenghtModeGetDefault(Sender: TObject);
var x, xID, xValue: Word;
  xText: string;
begin

  xID := MMSetupModul1.mSettings.GetID(scbMMLenghtMode.ValueName);
  xText := MMSetupModul1.mSettings.DefaultValue[xID];

  try
    xValue := StrToInt(xText);
  except
    xValue := 1;
  end;

  for x := 0 to integer(High(cLengthMode)) do begin
    if cLengthMode[x].Id = xValue then xValue := x;
  end;

 //scbMMLenghtMode.Text:=  cLenghtMode[xValue].Text;
  scbMMLenghtMode.ItemIndex := xValue;

  if scbMMLenghtMode.Text = '' then scbMMLenghtMode.ItemIndex := 1; // letzte
end;
//------------------------------------------------------------------------------
procedure TSetupMain.scbMMLenghtModeSave(Sender: TObject);
var x: integer;
begin
  x := cLengthMode[scbMMLenghtMode.ItemIndex].Id;
  mSettings.DBValue[mSettings.GetID(scbMMLenghtMode.ValueName)] := InttoStr(x);
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Ueberprueft ob ein MMLight installiert ist
//******************************************************************************
function TSetupMain.CheckMMLight: Boolean;
var xKey, xProduct, xProductNrStr, xMMPath: string;
  xReg: TRegistry;
  xVal: TStringList;
  x: Integer;
begin
  xReg := TRegistry.Create;
  xVal := TStringList.Create;
  xKey := 'Software\Microsoft\Windows\CurrentVersion\Uninstall';
  try
    with xReg do begin
      RootKey := cRegLM;
      OpenKeyReadOnly(xKey);
      GetKeyNames(xVal);
      xVal.Sorted := True;
      xVal.Sort;
      x := -1;
      xVal.Find('Microsoft SQL Server', x);
      xProduct := xVal.Strings[x];
      xProductNrStr := Trim(UpperCase(GetRegString(cRegLM, xKey + '\' +
        xProduct, 'DisplayName')));

      OpenKeyReadOnly(cRegMillMasterPath + '\Data');
      xMMPath := ReadString('MillMasterRootPath');

      Result := False;
      if (mNTProduct = WORKSTATION_NT) and (xProductNrStr <> '') and
        (FileExists(xMMPath + '\MM_Winding_Data.MDF')) then begin
        mNTProduct := SERVER_NT;
        Result := True;
      end;
    end;
  finally
    xReg.Free;
    xVal.Free;
  end;
end;
//******************************************************************************
// UpDate von Reg.- und DB-Eintraegen
//******************************************************************************
procedure TSetupMain.UpDateData;
begin
  acGetValue.Execute;
  acSaveAllSettings.Execute;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.Loaded;
var
  xSettings: TMMSettings;
  xText: string;
begin
  inherited Loaded;

EnterMethod('TSetupMain.Loaded');
CodeSite.SendDateTime('Time : ' , Now);


   mDomainNamesThread := TDomainsThread.Create;
   if mDomainNamesThread.Suspended then
      mDomainNamesThread.Resume;

   if mDomainNamesThread.Suspended then
     CodeSite.SendMsg('DomainNamesThread is not running')
   else
     CodeSite.SendMsg('DomainNamesThread is running');


  mInitializingThread := TInitializingThread.Create(Self);
 // mInitializingThread.
  if mInitializingThread.Suspended then
     CodeSite.SendMsg('InitializingThread is not running')
  else
     CodeSite.SendMsg('InitializingThread is running');

//----------
  //TODO: hier wird Zeit ben�tigt -> kann man das noch optimieren?
  CreateSecurityClasses;

  //Sequrity-Frames auf Security-TabSheet plazieren
  with MMSecurityAppl do begin
    Align := alClient;
    ParentWindow := tsSecurityAppl.Handle;
    Parent := tsSecurityAppl;
    Visible := True;

    with gbRemark do begin
      Left := MMSecurityAppl.Width - 220;
      Align := alNone;
      Width := 217;
      Top := 0;
      Height := paTop.Height - 1;
      ShowHint := False;
    end;

    with mLabel do begin
      Left := 5;
      Top := 13;
      AutoSize := True;
      Font.Color := clRed;
      Font.Style := [fsBold];
    end;

    with lbRemark do begin
      Align := alNone;
      Top := 13;
      Left := mLabel.Left + mLabel.Width + 5;
      Width := gbRemark.Width - Left - 5;
      Height := gbRemark.Height - Top - 5;

      AutoSize := False;
      WordWrap := True;
          //Caption:= xText;
    end;
  end;

  with MMSecurityMMGroups do begin
    Align := alClient;
    ParentWindow := tsSecurityGroup.Handle;
    Parent := tsSecurityGroup;
    Visible := True;

    with gbRemark do begin
      Left := MMSecurityAppl.Width - 220;
      Align := alNone;
      Width := 217;
      Top := 0;
      Height := paTop.Height - 1;
      ShowHint := False;
    end;

    with mLabel do begin
      Left := 5;
      Top := 13;
      AutoSize := True;
      Font.Color := clRed;
      Font.Style := [fsBold];
    end;

    with lbRemark do begin
      Align := alNone;
      Top := 13;
      Left := mLabel.Left + mLabel.Width + 5;
      Width := gbRemark.Width - Left - 5;
      Height := gbRemark.Height - Top - 5;

      AutoSize := False;
      WordWrap := True;
          //Caption:= xText;
    end;

  end;

  //Customer-Security anzeigen
  mSecurityChanged := False;
  gActiveSecurityClass := (gActualSecurity as TBaseMMSecurity);
  ShowSequrity(gActiveSecurityClass);
  acSaveSecurity.Execute;



//-------

{

  sgProcesses.ColCount := 2;
  //sgFileVersion.ColCount := 3;
  mNTProduct := GetNTProduct;

  xSettings := TMMSettings.Create;

  xSettings.Init;
  ISMMEasy := xSettings.IsPackageEasy;

  ISPro      := xSettings.IsPackagePro;
  ISStandard := xSettings.IsPackageStandard;

  xSettings.Free;

  //TODO: hier wird Zeit ben�tigt -> kann man das noch optimieren?
  CreateSecurityClasses;


  //Sequrity-Frames auf Security-TabSheet plazieren
  with MMSecurityAppl do begin
    Align := alClient;
    ParentWindow := tsSecurityAppl.Handle;
    Parent := tsSecurityAppl;
    Visible := True;

    with gbRemark do begin
      Left := MMSecurityAppl.Width - 220;
      Align := alNone;
      Width := 217;
      Top := 0;
      Height := paTop.Height - 1;
      ShowHint := False;
    end;

    with mLabel do begin
      Left := 5;
      Top := 13;
      AutoSize := True;
      Font.Color := clRed;
      Font.Style := [fsBold];
    end;

    with lbRemark do begin
      Align := alNone;
      Top := 13;
      Left := mLabel.Left + mLabel.Width + 5;
      Width := gbRemark.Width - Left - 5;
      Height := gbRemark.Height - Top - 5;

      AutoSize := False;
      WordWrap := True;
          //Caption:= xText;
    end;
  end;

  with MMSecurityMMGroups do begin
    Align := alClient;
    ParentWindow := tsSecurityGroup.Handle;
    Parent := tsSecurityGroup;
    Visible := True;

    with gbRemark do begin
      Left := MMSecurityAppl.Width - 220;
      Align := alNone;
      Width := 217;
      Top := 0;
      Height := paTop.Height - 1;
      ShowHint := False;
    end;

    with mLabel do begin
      Left := 5;
      Top := 13;
      AutoSize := True;
      Font.Color := clRed;
      Font.Style := [fsBold];
    end;

    with lbRemark do begin
      Align := alNone;
      Top := 13;
      Left := mLabel.Left + mLabel.Width + 5;
      Width := gbRemark.Width - Left - 5;
      Height := gbRemark.Height - Top - 5;

      AutoSize := False;
      WordWrap := True;
          //Caption:= xText;
    end;

  end;

  //Customer-Security anzeigen
  mSecurityChanged := False;
  gActiveSecurityClass := (gActualSecurity as TBaseMMSecurity);
  ShowSequrity(gActiveSecurityClass);
  acSaveSecurity.Execute;

  GetNetAmbient;
}
  CodeSite.SendDateTime('Time : ' , Now);
end;

//------------------------------------------------------------------------------
procedure TSetupMain.rgSetPartieNameClick(Sender: TObject);
begin
  inherited;
  if rgSetPartieName.ItemIndex <> rgSetPartieName.Items.Count - 1 then begin
    seUserPartieName.Enabled := False;
    seUserPartieName.Color := clSilver;
  end
  else begin
    seUserPartieName.Enabled := True;
    seUserPartieName.Color := clWindow;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Benachrichtig alle sichtbaren Komponenten und setzt das Flag Printable.
// Ist das Flag Printable TRUE, so kann die Kom.Wert ausgedruckt werden.
//******************************************************************************
procedure TSetupMain.SetPritableConfigItems;
var x, n: integer;
  xTabSheet: TTabSheet;
  xComp: TComponent;
  xComponent: TComponent;
  xValue: TSetupValues;
  xText: string;
begin
  for x := low(cMMSetupDefaults) to high(cMMSetupDefaults) do
    cMMSetupDefaults[x].Printable := False;

  for x := 0 to ComponentCount - 1 do begin
    xComp := Components[x];
    xComponent := Components[x];
      // Alle 'TComponent.Parents' auf den TabSheets ermitteln
    repeat
      xComp := xComp.GetParentComponent;
      if (xComp is TTabSheet) then
        xTabSheet := (xComp as TTabSheet);
    until (xComp is TTabSheet) or (xComp = nil);

    if xComp <> nil then
      if xTabSheet.TabVisible then begin
        TMethod(xValue).code := xComponent.MethodAddress('Printable');
           // Code und Daten der Setupkomponente zuweisen
        if Assigned(TMethod(xValue).Code) then begin
          TMethod(xValue).Data := xComponent;
          xValue;
          xText := xComponent.Name;
        end;
      end;
  end;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.sedQOfflimitLenChange(Sender: TObject);
begin
  inherited;
  SetMeterToUserUnit;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.nedQOfflimitLenChange(Sender: TObject);
begin
end;
//------------------------------------------------------------------------------
function TSetupMain.MeterToYds(aValue: Single): Single;
begin
  Result := aValue / 0.9144;
end;
//------------------------------------------------------------------------------
function TSetupMain.YdsToMeter(aValue: Single): Single;
begin
  Result := aValue * 0.9144;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.srgQOfflimitUnitClick(Sender: TObject);
var xInValue, xOutValue: Single;
begin
  inherited;

  mQOfflimitLenText := nedQOfflimitLen.AsString;

  xInValue := nedQOfflimitLen.Value;

  case srgQOfflimitUnit.ItemIndex of
    0: xOutValue := YdsToMeter(xInValue); // m
    1: xOutValue := MeterToYds(xInValue); // yds
  else
    xOutValue := xInValue;
  end;
  nedQOfflimitLen.Value := xOutValue;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.SetMeterToUserUnit;
var xInValue, xOutValue: Single;
begin
  if sedQOfflimitLen.Text = '' then sedQOfflimitLen.Text := '1';
  xInValue := StrToFloat(sedQOfflimitLen.Text);

  case srgQOfflimitUnit.ItemIndex of
    0: xOutValue := xInValue; // m
    1:
      xOutValue := MeterToYds(xInValue) // yds
  else
    xOutValue := xInValue;
  end;
  nedQOfflimitLen.Value := Round(xOutValue);
end;
//------------------------------------------------------------------------------
procedure TSetupMain.SetUserUnitToMeter;
var xInValue, xOutValue: Single;
begin

  xInValue := nedQOfflimitLen.Value;

  case srgQOfflimitUnit.ItemIndex of
    0: xOutValue := xInValue; // m
    1:
      xOutValue := YdsToMeter(xInValue) // yds
  else
    xOutValue := xInValue;
  end;
  sedQOfflimitLen.Text := Format('%0.0f', [xOutValue]);
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Schreibt alle Shifts aus t_shift in CB und setzt Benutzereistellung aus
// Konst.Array
//******************************************************************************
procedure TSetupMain.FillUpMMShifts;
var xID, x: Integer;
  xShift: string;
  xSettings: TMMSettingsReader;
begin
  scbShifts.Clear;


  xSettings := TMMSettingsReader.Instance;
  xSettings.Init;

  xID := StrToInt(xSettings.Value[cDefaultShiftCalID]);

  MMConfigDataModule.FetchAllShifts;

  with MMConfigDataModule.qShifts do begin
    First;
    while not EOF do begin
      scbShifts.Items.Add(FieldByName('c_Shiftcal_Name').AsString);
      x := FieldByName('c_shiftcal_id').AsInteger;
      if xID = x then
        xShift := FieldByName('c_Shiftcal_Name').AsString;
      Next;
    end;
  end;
  scbShifts.ItemIndex := scbShifts.Items.IndexOf(xShift);

  //Nur sichtbar, wenn mehr als 1 Schichtkal.
  if scbShifts.Items.Count <= 1 then scbShifts.Visible := False;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Speichert die ShiftsID aus CB auf DB
//******************************************************************************
procedure TSetupMain.scbShiftsSave(Sender: TObject);
var xID: integer;
  xText: string;
  xDBText: string;
begin
  xID := -1;
  xText := scbShifts.Text;

  with MMConfigDataModule.qShifts do begin
    Open;
    First;
    repeat
      xDBText := FieldByName('c_Shiftcal_Name').AsString;
      if CompareText(xText, xDBText) = 0 then
        xID := FieldByName('c_shiftcal_id').AsInteger;
      Next;
    until EOF or (xID > 0)
  end;
  if xID <= 0 then xID := 1;
  mSettings.DBValue[mSettings.GetID(scbShifts.ValueName)] := InttoStr(xID);
end;
//------------------------------------------------------------------------------
procedure TSetupMain.scbShiftsGetValue(Sender: TObject);
var x, xID, xShiftID: Integer;
  xShift: string;
  xSettings: TMMSettingsReader;
begin

  xSettings := TMMSettingsReader.Instance;

  xSettings.Init;
  try
    x := StrToInt(xSettings.Value[cDefaultShiftCalID]);
  except
    x := 1;
  end;

  with MMConfigDataModule.qShifts do begin
    Open;
    First;
    while not EOF do begin

      xShiftID := FieldByName('c_shiftcal_id').AsInteger;
      if x = xShiftID then begin
        xShift := FieldByName('c_Shiftcal_Name').AsString;
        break;
      end; //else
        //xShift := FieldByName('c_Shiftcal_Name').AsString;
      Next;
    end;
  end;
  scbShifts.ItemIndex := scbShifts.Items.IndexOf(xShift);
end;
//------------------------------------------------------------------------------
procedure TSetupMain.scbShiftsGetDefault(Sender: TObject);
var x, xID, xShiftID: Integer;
  xShift: string;
begin
  xID := MMSetupModul1.mSettings.GetID(scbShifts.ValueName);
  try
    x := StrToInt(MMSetupModul1.mSettings.DefaultValue[xID]);
  except
    x := 1;
  end;
  xShiftID := -1;

  with MMConfigDataModule.qShifts do begin
    Open;
    First;
    while not EOF do begin
      xShiftID := FieldByName('c_shiftcal_id').AsInteger;
      if x = xShiftID then begin
        xShift := FieldByName('c_Shiftcal_Name').AsString;
        break;
      end
      else
        xShift := FieldByName('c_Shiftcal_Name').AsString;
      Next;
    end;
  end;
  exit;
  if scbShifts.items.Count <= 1 then begin
    scbShifts.ItemIndex := 0;
    //MMSetupModul1.mSettings.ValueChange[xID]:= TRUE;
  end
  else
    scbShifts.ItemIndex := scbShifts.Items.IndexOf(xShift);

end;
//------------------------------------------------------------------------------
procedure TSetupMain.nedQOfflimitLenKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var xText: string;
  xID: Integer;
begin
  inherited;
  xText := nedQOfflimitLen.AsString;
  if mQOfflimitLenText <> xText then begin
    xID := MMSetupModul1.mSettings.GetID(scbShifts.ValueName);
    MMSetupModul1.mSettings.ValueChange[xID] := True;
//    FBaseSetup.ValueChange[mAbsValueNameID]:= mUserValueChange;
    beep;
    beep;
  end;
  mQOfflimitLenText := nedQOfflimitLen.AsString;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.nedQOfflimitLenKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  mQOfflimitLenText := nedQOfflimitLen.AsString;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.SettingsLabDispoMaster;
begin 
  if ISPro then begin
    tsOfflimit.TabVisible := True;
    tsStyle.TabVisible := True;
    sseDelShiftByTime.enabled := True;
    SetupPageControl.Repaint;
  end;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.MMSecurityControl1AfterSecurityCheck(Sender: TObject);
var xVisible: Boolean;
begin
  inherited;

  if mNTProduct = WORKSTATION_NT then begin
    xVisible := False;
    if ISMMEasy then xVisible := True;
  end
  else begin
    xVisible := True;
  end;

  tsMMGuard.TabVisible := xVisible;
  tsSystemServer.TabVisible := xVisible;
  tsJob.TabVisible := xVisible;

  tsMMGuard.Update;
  tsSystemServer.Update;
  tsJob.Update;

  {
  if not tsUserAdmin.TabVisible then
     tsUserAdmin.TabVisible := IsSecurityOk;
  }
  tsUserAdmin.Update;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.acLanguageExecute(Sender: TObject);
var x: integer;
  xComponent: TComponent;
  xValue: TSetupValues;
begin

  try
    inherited;
  except
    //Error mit TNTUserMan und Translator, wenn uebersetzt wird
  end;

  if not IsMMEASY then begin
    mSetupCaption := Format('%s %s', [Translate(cSetupCaption), GetNTProduct(mNTProduct)]);
    Caption := mSetupCaption;
  end
  else begin
    mSetupCaption := Translate(cMMEasy);
    Caption := mSetupCaption;
  end;

  for x := 0 to ComponentCount - 1 do begin
    xComponent := Components[x];

    TMethod(xValue).code := xComponent.MethodAddress('Translation');
      // Code und Daten der Setupkomponente zuweisen
    if Assigned(TMethod(xValue).Code) then begin
      TMethod(xValue).Data := xComponent;
      xValue;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.scbTemplateSetsAvailableClick(Sender: TObject);
begin
  inherited;
  rgSetPartieName.Enabled := not scbTemplateSetsAvailable.Checked;
  seUserPartieName.Enabled := not scbTemplateSetsAvailable.Checked;

  if not seUserPartieName.Enabled then
    seUserPartieName.Color := clSilver
  else
    seUserPartieName.Color := clWindow;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.acAddUsergroupExecute(Sender: TObject);
var x, n: integer;
  xBool: Boolean;
  xNewGroups, xDeletedGroups, xOldGroups: TStringList;
  xGroup: string;
begin
  inherited;

  xNewGroups := TStringList.Create;
  xDeletedGroups := TStringList.Create;
  xOldGroups := TStringList.Create;

  xNewGroups.Duplicates := dupIgnore;
  xNewGroups.Sorted := True;

  xDeletedGroups.Duplicates := dupIgnore;
  xDeletedGroups.Sorted := True;

  xOldGroups.Duplicates := dupIgnore;
  xOldGroups.Sorted := True;

  //Gruppen auswaehlen
  with TGroupSelectionDLG.Create(Self) do begin
    DictionaryName := mTranslator.Dictionary;

    xOldGroups.CommaText := gActiveSecurityClass.GetUserGroups;
    xNewGroups.Assign(xOldGroups);

    Init(mDomainGroups, xNewGroups);

    if ShowModal = mrOK then begin
          //ausgewaehlte Gruppen uebergeben
      xNewGroups.Assign(SelectedGroups);

          //Bestehende Gruppen filtrieren oder als geloescht deklarieren
      for x := 0 to xOldGroups.Count - 1 do begin
        if xNewGroups.Find(xOldGroups.Strings[x], n) then
          xNewGroups.Delete(n)
        else
          xDeletedGroups.Add(xOldGroups.Strings[x]);
      end;
    end
    else
      xNewGroups.Clear;
    Free;
  end;

  Application.ProcessMessages;
  //Neue Gruppen in SecurityList mit dem MMUsergroup-Recht einfuegen
  for n := 0 to xNewGroups.Count - 1 do begin
    gActiveSecurityClass.AddUserGroup(xNewGroups.Strings[n]);
    Application.ProcessMessages;
  end;

  Application.ProcessMessages;
  //Geloeschte Gruppen aus SecurityList entfernen
  for n := 0 to xDeletedGroups.Count - 1 do begin
    gActiveSecurityClass.DeleteGroup(xDeletedGroups.Strings[n]);
    Application.ProcessMessages;
  end;

  xNewGroups.Free;
  xDeletedGroups.Free;
  xOldGroups.Free;

 //Neue Gruppen in Combobox abfuellen
  with MMSecurityMMGroups do begin
    xGroup := cbUserGroups.Text;
    cbUserGroups.Items.CommaText := gActiveSecurityClass.GetUserGroups;
    cbUserGroups.ItemIndex := cbUserGroups.Items.IndexOf(xGroup);

    if cbUserGroups.Text = '' then cbUserGroups.ItemIndex := 0;
  end;

  SetDefaultSecurityGroup_Into_SecurityClass;
  pcSecurity.OnChange(nil);
end;

//******************************************************************************
// Initialisiert die TActualSecurity, TBackupSecurity & TDefaultSecurity Klassen
// TMMSecurityAppl- & TMMSecurityMMGroups-Frame m�ssen schon erstellt worden sein
// -> siehe MMConfiguration.dpr
// CreateSecurityClasses wird in TSetupMain.Loaded aufgerufen
// Die Security-Classen sind in u_MMConfigSecurity definiert
//******************************************************************************
procedure TSetupMain.CreateSecurityClasses;
var xAppl, xForm, xFunction, xGroup: string;
  xSeclevel, x, n: Integer;
begin

  EnterMethod('TSetupMain.CreateSecurityClasses');
  CodeSite.SendDateTime('Time : ' , Now);

  gActiveSecurityClass := nil;
  gActualSecurity := nil;
  gDefaultSecurity := nil;
  gBackupSecurity := nil;

  //Default Security
  CodeSite.SendMsg('Default Security');
  gDefaultSecurity := TDefaultSecurity.Create;
  with gDefaultSecurity do begin
    ADOConnection := MMConfigDataModule.MMADOConnection;
    Init;
    acLoadDefault.Enabled := HasSecurityEntries;

    for x := 0 to gDefaultSecurity.Count - 1 do begin
      SecurityItem.Items[x].DefaultLevel := 0;
      if xGroup = cDefaultUserGroup then
        SecurityItem.Items[x].DefaultLevel :=
          SecurityItem.Items[x].SecurityLevel;
    end;
  end;
  CodeSite.SendDateTime('Time : ' , Now);
  CodeSite.SendMsg('TDefaultSecurity created');

  //Actual Security
  gActualSecurity := TActualSecurity.Create;
  with gActualSecurity do begin
    ADOConnection := MMConfigDataModule.MMADOConnection;
    Init; //
  end;
  CodeSite.SendDateTime('Time : ' , Now);
  CodeSite.SendMsg('TActualSecurity created');

  //Backup Security
  gBackupSecurity := TBackupSecurity.Create;
  with gBackupSecurity do begin
    ADOConnection := MMConfigDataModule.MMADOConnection;
    Init;
    acLoadBackup.Enabled := HasSecurityEntries;
  end;
  CodeSite.SendDateTime('Time : ' , Now);
  CodeSite.SendMsg('TBackupSecurity created');

  SetDefaultSecurityGroup_Into_SecurityClass;
  CodeSite.SendDateTime('Time : ' , Now);
  CodeSite.SendMsg('SetDefaultSecurityGroup_Into_SecurityClass done');

 {
  for x:= 0 to gDefaultSecurity.Count-1 do begin
           xGroup     := gDefaultSecurity.SecurityItem.Items[x].SecurityGroup;
           if  xGroup = cDefaultUserGroup then begin
               xAppl      := gDefaultSecurity.SecurityItem.Items[x].Appl;
               xForm      := gDefaultSecurity.SecurityItem.Items[x].ApplForm;
               xFunction  := gDefaultSecurity.SecurityItem.Items[x].ApplFunction;
               xSeclevel  := gDefaultSecurity.SecurityItem.Items[x].SecurityLevel;

               for n:= 0 to gActualSecurity.Count-1 do
                   if  (gActualSecurity.SecurityItem.Items[n].Appl = xAppl) and
                       (gActualSecurity.SecurityItem.Items[n].ApplForm = xForm) and
                       (gActualSecurity.SecurityItem.Items[n].ApplFunction = xFunction) then
                        gActualSecurity.SecurityItem.Items[n].DefaultLevel := xSeclevel;

               for n:= 0 to gBackupSecurity.Count-1 do
                   if  (gBackupSecurity.SecurityItem.Items[n].Appl = xAppl) and
                       (gBackupSecurity.SecurityItem.Items[n].ApplForm = xForm) and
                       (gBackupSecurity.SecurityItem.Items[n].ApplFunction = xFunction) then
                        gBackupSecurity.SecurityItem.Items[n].DefaultLevel := xSeclevel;

               for n:= 0 to gDefaultSecurity.Count-1 do
                   if  (gDefaultSecurity.SecurityItem.Items[n].Appl = xAppl) and
                       (gDefaultSecurity.SecurityItem.Items[n].ApplForm = xForm) and
                       (gDefaultSecurity.SecurityItem.Items[n].ApplFunction = xFunction) then
                        gDefaultSecurity.SecurityItem.Items[n].DefaultLevel := xSeclevel;
           end;
  end;
  }
end;
//------------------------------------------------------------------------------
procedure TSetupMain.DestroySecurityClasses;
begin
  gActualSecurity.Free;
  gDefaultSecurity.Free;
  gBackupSecurity.Free;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.tsSecurityApplEnter(Sender: TObject);
begin
  inherited;
  gActiveSecurityClass.SecurityGrid := MMSecurityAppl.esgApplSecurity;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.tsSecurityGroupEnter(Sender: TObject);
begin
  inherited;
  gActiveSecurityClass.SecurityGrid := MMSecurityMMGroups.esgGroupSecurity;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Security im Grid  MMSecurityAppl.esgApplSecurity oder
// MSecurityMMGroups.esgGroupSecurity anzeigen
//******************************************************************************
procedure TSetupMain.pcSecurityChange(Sender: TObject);
begin
  inherited;
  //Security anzeigen

  if (pcSecurity.ActivePage = tsSecurityAppl) then begin
    gActiveSecurityClass.SecurityGrid := MMSecurityAppl.esgApplSecurity;
    gActiveSecurityClass.ShowData(MMSecurityAppl.cbAppls.text,
      MMSecurityAppl.cbForms.text);
  end
  else begin
    gActiveSecurityClass.SecurityGrid := MMSecurityMMGroups.esgGroupSecurity;
    gActiveSecurityClass.ShowData(MMSecurityMMGroups.cbUserGroups.text);
  end;

  if MMSecurityMMGroups.cbUserGroups.Items.Count <= 0 then
    PrepareStatusbar_for_Security(rsNoUserGroups);

    
    //MMSecurityMMGroups.esgGroupSecurity.EditorMode:= FALSE;
    //MMSecurityAppl.esgApplSecurity.EditorMode:= FALSE;

  {
  MMSecurityMMGroups.esgGroupSecurity.Options := MMSecurityMMGroups.esgGroupSecurity.Options - [goEditing, goRowSelect] ;
  MMSecurityAppl.esgApplSecurity.Options := MMSecurityAppl.esgApplSecurity.Options -  [goEditing, goRowSelect] ;
  MMSecurityAppl.esgApplSecurity.SelectCol(2);
  }
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Speichert die Security Actual in Tab. t_Security
//******************************************************************************
procedure TSetupMain.acSaveSecurityExecute(Sender: TObject);
var xAppl, xForm, xGroup: string;
begin
  inherited;

  screen.Cursor := crHourGlass;

  xAppl := MMSecurityAppl.cbAppls.Text;
  xForm := MMSecurityAppl.cbForms.Text;
  xGroup := MMSecurityMMGroups.cbUserGroups.Text;

  gActiveSecurityClass.SaveAsActual;

  gActiveSecurityClass := (gActualSecurity as TBaseMMSecurity); //27.01.2004
  gActiveSecurityClass.Init;
  gActiveSecurityClass.Load; // nochmals von DB einlesen

  SetDefaultSecurityGroup_Into_SecurityClass;

  ShowSequrity(gActiveSecurityClass);

  PrepareStatusbar_for_Security('');
  mSecurityChanged := False;

  MMSecurityMMGroups.SecurityChanged := mSecurityChanged;
  MMSecurityAppl.SecurityChanged := mSecurityChanged;

  xAppl := MMSecurityAppl.cbAppls.Text;
  xForm := MMSecurityAppl.cbForms.Text;
  xGroup := MMSecurityMMGroups.cbUserGroups.Text;


  gActiveSecurityClass.ShowData(xAppl, xForm);
  gActiveSecurityClass.ShowData(xGroup);            

  screen.Cursor := crDefault;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Default Security laden -> Tab. t_Security_Default
//******************************************************************************
procedure TSetupMain.acLoadDefaultExecute(Sender: TObject);
var
  xAppl, xForm, xGroup: string;
begin
  if IvMessageBox(cAskLoadsecurityDefault, '', mtConfirmation,
    [mbYes, mbCancel], 0, mTranslator.Dictionary) = mrCancel then
    Exit;

  screen.Cursor := crHourGlass;

  xAppl := MMSecurityAppl.cbAppls.Text;
  xForm := MMSecurityAppl.cbForms.Text;
  xGroup := MMSecurityMMGroups.cbUserGroups.Text;

  ClearSecurityCBs;
  gActiveSecurityClass := (gDefaultSecurity as TBaseMMSecurity);
  gActiveSecurityClass.Init;
  gActiveSecurityClass.Load; // nochmals von DB einlesen

  SetDefaultSecurityGroup_Into_SecurityClass;

  ShowSequrity(gActiveSecurityClass);
  mSecurityChanged := True;

  ShowActualApplsGroups(xAppl, xForm, xGroup);

  screen.Cursor := crDefault;

end;
//------------------------------------------------------------------------------

//******************************************************************************
// Ladet die Arbeits-Security -> Tab. t_Security
//******************************************************************************
procedure TSetupMain.acLoadWorkExecute(Sender: TObject);
var
  xAppl, xForm, xGroup: string;
begin
  screen.Cursor := crHourGlass;

  xAppl := MMSecurityAppl.cbAppls.Text;
  xForm := MMSecurityAppl.cbForms.Text;
  xGroup := MMSecurityMMGroups.cbUserGroups.Text;

  ClearSecurityCBs;
  gActiveSecurityClass := (gActualSecurity as TBaseMMSecurity);

  gActiveSecurityClass.Init;
  gActiveSecurityClass.Load; // nochmals von DB einlesen

  SetDefaultSecurityGroup_Into_SecurityClass;

  ShowSequrity(gActiveSecurityClass);

  mSecurityChanged := True;

  ShowActualApplsGroups(xAppl, xForm, xGroup);

  screen.Cursor := crDefault;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Ladet die Backup-Security  -> Tab. t_Security_Customer
//******************************************************************************
procedure TSetupMain.acLoadBackupExecute(Sender: TObject);
var
  xAppl, xForm, xGroup: string;
begin
  if IvMessageBox(cAskLoadsecurityBackup, '', mtConfirmation,
    [mbYes, mbCancel], 0, mTranslator.Dictionary) = mrCancel then
    Exit;

  screen.Cursor := crHourGlass;

  xAppl := MMSecurityAppl.cbAppls.Text;
  xForm := MMSecurityAppl.cbForms.Text;
  xGroup := MMSecurityMMGroups.cbUserGroups.Text;

  ClearSecurityCBs;
  gActiveSecurityClass := (gBackupSecurity as TBaseMMSecurity);
  gActiveSecurityClass.Init;
  gActiveSecurityClass.Load; // nochmals von DB einlesen
  SetDefaultSecurityGroup_Into_SecurityClass;
  ShowSequrity(gActiveSecurityClass);
  mSecurityChanged := True;

  ShowActualApplsGroups(xAppl, xForm, xGroup);

  screen.Cursor := crDefault;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ruft den Win-User Manger auf (nur auf Server)
//******************************************************************************
procedure TSetupMain.acShowUserManagerExecute(Sender: TObject);
var xProg: string;
begin
  inherited;
  Screen.Cursor := crHourGlass;

  //Win2k
  if GetOSMajorVersion >= 5 then begin
    if HasActiveDirectory then
      xProg := 'dsa.msc' //PDC
    else
      xProg := 'lusrmgr.msc'; //WKS, Standalone, server

  end
  else
    xProg := 'usrmgr.exe'; //NT4

  CallWinUserMgr(xProg);
  Screen.Cursor := crDefault;
end;

//******************************************************************************
//Started den Win-User Manger
//******************************************************************************
function TSetupMain.CallWinUserMgr(aMrg: string): Boolean;
var xShellInfo: TShellExecuteInfo;
  xResult: Boolean;
  xCommand, xDir, xProg: string;
  xSource, xSysDir: PChar;
begin

  //Get System32 Dir
  GetMem(xSysDir, 255);
  GetSystemDirectory(xSysDir, 255);
  xProg := StrPas(xSysDir);
  FreeMem(xSysDir, 255);

  xProg := xProg + '\' + aMrg;
  xCommand := xProg;
  xDir := ExtractFileDir(xProg);
  xSource := '';

  Randomize;
  FillCHar(xShellInfo, SizeOf(xShellInfo), 0);

  with xShellInfo do begin
    cbSize := SizeOf(xShellInfo);
    Wnd := Self.Handle;
    lpVerb := 'Open';
    lpFile := PChar(xCommand);
    lpParameters := xSource;
    lpDirectory := PChar(xDir);
    nShow := SW_SHOW;
    fMask := SEE_MASK_FLAG_DDEWAIT or
      SEE_MASK_FLAG_NO_UI or
      SEE_MASK_NOCLOSEPROCESS;
    lpClass := 'MMConfig';
    hProcess := Random(10000);
  end;

  try
    Application.ProcessMessages;
    xResult := ShellExecuteEx(@xShellInfo);
    Application.ProcessMessages;
  except
    on e: Exception do begin
      MessageDlg(xProg + #10#13 + 'Error : ' + e.Message, mtError, [mbOk], 0);
      xResult := False;
    end;
  end;
  Result := xResult;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Zeigt die gewaehlte Security im Grid an
//******************************************************************************
procedure TSetupMain.ShowSequrity(var aSecurityClass: TBaseMMSecurity);
var xAppl, xForm, xGroup : string;
    xIndex               : Integer;
    xSettings            : TMMSettings;
begin
  xAppl := MMSecurityAppl.cbAppls.Text;
  xForm := MMSecurityAppl.cbForms.Text;
  xGroup := MMSecurityMMGroups.cbUserGroups.Text;

  //Package auslesen
  xSettings := TMMSettings.Create;
  xSettings.Init;

  if xSettings.IsPackageStandard then
      aSecurityClass.MMPackage := mmpStandard
  else
     if xSettings.IsPackageEasy then
        aSecurityClass.MMPackage := mmpEasy
     else
        aSecurityClass.MMPackage:= mmpPro;

  xSettings.Free;

  //Acrive Security-Class den Farmes uebergeben
  MMSecurityAppl.ActiveSecurityClass := aSecurityClass;
  MMSecurityMMGroups.ActiveSecurityClass := aSecurityClass;

  //Daten in CB Applikation und Appl.-Forms schreiben
  aSecurityClass.Translate;
 // MMSecurityAppl.cbAppls.Items.CommaText := aSecurityClass.MMApplications;
  MMSecurityAppl.cbAppls.Items.CommaText :=  aSecurityClass.MMApplicationsTranslated;


  if xAppl = '' then
    xIndex := 0
  else
    xIndex := MMSecurityAppl.cbAppls.Items.IndexOf(xAppl);
  if xIndex < 0 then xIndex := 0;
  MMSecurityAppl.cbAppls.ItemIndex := xIndex;

  MMSecurityAppl.cbForms.Items.CommaText :=
    aSecurityClass.GetApplicationForms(MMSecurityAppl.cbAppls.Text);
  if xForm = '' then
    xIndex := 0
  else
    xIndex := MMSecurityAppl.cbForms.Items.IndexOf(xForm);
  if xIndex < 0 then xIndex := 0;
  MMSecurityAppl.cbForms.ItemIndex := xIndex;

  MMSecurityMMGroups.cbUserGroups.Items.CommaText := aSecurityClass.GetUserGroups;
  if xGroup = '' then
    xIndex := 0
  else
    xIndex := MMSecurityMMGroups.cbUserGroups.Items.IndexOf(xGroup);
  if xIndex < 0 then xIndex := 0;
  MMSecurityMMGroups.cbUserGroups.ItemIndex := xIndex;

  //Security in Grids schreiben
  aSecurityClass.SecurityGrid := MMSecurityAppl.esgApplSecurity;
  aSecurityClass.ShowData(MMSecurityAppl.cbAppls.text,
    MMSecurityAppl.cbForms.text);

  aSecurityClass.SecurityGrid := MMSecurityMMGroups.esgGroupSecurity;
  aSecurityClass.ShowData(MMSecurityMMGroups.cbUserGroups.text);

end;
//------------------------------------------------------------------------------

//******************************************************************************
// Speichert die Security als Backup in Tab. t_Security_Customer
//******************************************************************************
procedure TSetupMain.acSaveSecurityBackupExecute(Sender: TObject);
var xAppl, xForm, xGroup: string;
begin
  inherited;
  screen.Cursor := crHourGlass;

  gActiveSecurityClass.SaveAsBackup;

  gActiveSecurityClass.Init;
  gActiveSecurityClass.Load; // nochmals von DB einlesen

  SetDefaultSecurityGroup_Into_SecurityClass;
  PrepareStatusbar_for_Security('');

  acLoadBackup.Enabled := True;

  screen.Cursor := crDefault;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.mmActionListMainExecute(Action: TBasicAction;
  var Handled: Boolean);
begin
  inherited;

  if MMSecurityMMGroups.SecurityChanged or MMSecurityAppl.SecurityChanged then
    mSecurityChanged := True;

  if Action = acLoadWork then
    PrepareStatusbar_for_Security(rsLoadActualSecur);

  if Action = acLoadDefault then
    PrepareStatusbar_for_Security(rsLoadDefaultSecur);

  if Action = acLoadBackup then
    PrepareStatusbar_for_Security(rsLoadBackupSecur);

  if Action = acSaveSecurity then
    PrepareStatusbar_for_Security('');

  if Action = acSaveSecurityBackup then
    PrepareStatusbar_for_Security('');
end;
//------------------------------------------------------------------------------
procedure TSetupMain.PrepareStatusbar_for_Security(aText: string);
begin
  if aText = rsNoUserGroups then begin
    fcStatusBar1.Panels[0].Font.Color := clRed;
    fcStatusBar1.Panels[0].Font.Style := fcStatusBar1.Panels[0].Font.Style + [fsBold];
  end
  else begin
    fcStatusBar1.Panels[0].Font.Color := clWindowText;
    fcStatusBar1.Panels[0].Font.Style := fcStatusBar1.Panels[0].Font.Style - [fsBold];
  end;

  fcStatusBar1.Panels[0].Text := aText;

end;
//------------------------------------------------------------------------------
procedure TSetupMain.SetDefaultSecurityGroup_Into_SecurityClass;
var xAppl, xForm, xFunction, xGroup: string;
  xSeclevel, x, n: Integer;
begin

  exit;

  for x := 0 to gDefaultSecurity.Count - 1 do begin
    xGroup := gDefaultSecurity.SecurityItem.Items[x].SecurityGroup;
    if xGroup = cDefaultUserGroup then begin
      xAppl := gDefaultSecurity.SecurityItem.Items[x].Appl;
      xForm := gDefaultSecurity.SecurityItem.Items[x].ApplForm;
      xFunction := gDefaultSecurity.SecurityItem.Items[x].ApplFunction;
      xSeclevel := gDefaultSecurity.SecurityItem.Items[x].SecurityLevel;

      for n := 0 to gActualSecurity.Count - 1 do
        if (gActualSecurity.SecurityItem.Items[n].Appl = xAppl) and
          (gActualSecurity.SecurityItem.Items[n].ApplForm = xForm) and
          (gActualSecurity.SecurityItem.Items[n].ApplFunction = xFunction) then
          gActualSecurity.SecurityItem.Items[n].DefaultLevel := xSeclevel;

      for n := 0 to gBackupSecurity.Count - 1 do
        if (gBackupSecurity.SecurityItem.Items[n].Appl = xAppl) and
          (gBackupSecurity.SecurityItem.Items[n].ApplForm = xForm) and
          (gBackupSecurity.SecurityItem.Items[n].ApplFunction = xFunction) then
          gBackupSecurity.SecurityItem.Items[n].DefaultLevel := xSeclevel;

      for n := 0 to gDefaultSecurity.Count - 1 do
        if (gDefaultSecurity.SecurityItem.Items[n].Appl = xAppl) and
          (gDefaultSecurity.SecurityItem.Items[n].ApplForm = xForm) and
          (gDefaultSecurity.SecurityItem.Items[n].ApplFunction = xFunction) then
          gDefaultSecurity.SecurityItem.Items[n].DefaultLevel := xSeclevel;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.ClearSecurityCBs;
begin
  MMSecurityAppl.cbAppls.Clear;
  MMSecurityAppl.cbForms.Clear;
  MMSecurityMMGroups.cbUserGroups.Clear;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.tsSystemCommonResize(Sender: TObject);
begin
  inherited;
  bvCommonVert.Height := tsSystemCommon.Height - bvCommonVert.top - 10;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.GetNetAmbient;
var xPDC, xDomain, xType: string;
begin

EnterMethod('TSetupMain.GetNetAmbient');
CodeSite.SendDateTime('Time : ' , Now);

  IsWorkGroup := False;
  LogonNet := GetActualDomainName;
  CodeSite.SendMsg('GetActualDomainName : ' + LogonNet);
CodeSite.SendDateTime('Time : ' , Now);

  PDCName := GetDomainServerName;
  CodeSite.SendMsg('GetDomainServerName : ' + PDCName);
CodeSite.SendDateTime('Time : ' , Now);

  PCName := GetMachineName;
  CodeSite.SendMsg('GetMachineName : ' + PCName);
CodeSite.SendDateTime('Time : ' , Now);

  OsTypeID := GetNTProduct(PCName);

  case OsTypeID of
    cWorkstation: OsType := 'Member Workstation';
    cServer, cAddServer: OsType := 'Member Server';
    cBackupdomaincontroller: OsType := 'Backup Server';
    cPrimarydomaincontroller: OsType := 'Domain Controller';
    cStandaloneServer: begin
        OsType := 'Standalone Server';
        IsWorkGroup := True;
      end;
    cStandaloneWKS: begin
        OsType := 'Standalone Workstation';
        IsWorkGroup := True;
      end;
  else
    OsType := '';
  end;

  CodeSite.SendMsg('OS Type : ' + OsType);


  laDomain.Caption := LogonNet;
  lbPCName.Caption := PCName;

  if PDCName = '' then begin
    laDomainServerNameLabel.Caption := '';
    laDomainServerName.Caption := '';
  end
  else
    laDomainServerName.Caption := PDCName;

  lbType.Caption := OsType;

CodeSite.SendDateTime('Time : ' , Now);
end;
//------------------------------------------------------------------------------
procedure TSetupMain.acPrintSecurityExecute(Sender: TObject);
begin
  inherited;
  screen.Cursor := crHourGlass;

  CodeSite.SendMsg('TSetupMain.acPrintSecurityExecute() begin');

  acSaveSecurity.Execute;

  try
    printer.Refresh;
    if printer.Printers.Count < 1 then begin
      MessageDlg(cNoPrinterInstalled, mtWarning, [mbOK], 0);
      CodeSite.SendMsg('TSetupMain.acPrintSecurityExecute() no printer installed');
      exit;
    end;

    with TPrintDialog.Create(Self, gActiveSecurityClass, pcSecurity.ActivePage,
      MMSecurityAppl.cbAppls.text,
      MMSecurityAppl.cbForms.text,
      MMSecurityMMGroups.cbUserGroups.text) do begin

      MachineName := PCName;
      WinType := OsType;
      Company := mRegistratedOrg;

      screen.Cursor := crDefault;

      ShowModal;
      Free;
    end
  except
  end;
  screen.Cursor := crDefault;
  CodeSite.SendMsg('TSetupMain.acPrintSecurityExecute() end');
end;
//------------------------------------------------------------------------------
procedure TSetupMain.ShowActualApplsGroups(aAppl, aForm, aMMGroup: string);
var xAppl, xForm, xGroup: string;
  x: Integer;
begin
  if (pcSecurity.ActivePage = tsSecurityAppl) then
    with MMSecurityAppl do begin
      x := cbAppls.Items.IndexOf(aAppl);
      if x > -1 then begin
        cbAppls.ItemIndex := x;
        cbApplsChange(Self);
        x := cbForms.Items.IndexOf(aForm);
        if x < 0 then x := 0;
        cbForms.ItemIndex := x;
        cbFormsChange(self);
      end
      else begin
        cbAppls.ItemIndex := 0;
        cbForms.ItemIndex := 0;
        cbFormsChange(self);
      end;
    end
  else
    with MMSecurityMMGroups do begin
      x := cbUserGroups.Items.IndexOf(aMMGroup);
      if x < 0 then x := 0;
      cbUserGroups.ItemIndex := x;
      cbUserGroupsChange(self);
    end;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.mmActionListMainUpdate(Action: TBasicAction;
  var Handled: Boolean);
var xBool: Boolean;
begin
  inherited;
  if MMSecurityMMGroups.cbUserGroups.Items.Count > 0 then
    xBool := True
  else
    xBool := False;

  acSaveSecurity.Enabled := xBool;
  acSaveSecurityBackup.Enabled := xBool;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.tsSecurityGroupContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
  inherited;

end;
//------------------------------------------------------------------------------

//Zeigt MMSecurity, wenn als MMSupp, MMassign oder MMAdmin eingeloggt
function TSetupMain.IsSecurityOk: Boolean;
var xLogUser, xMMUser: string;
    //xIsLogonUserOK, xIsMMUserOk : Boolean;
  x: Integer;
const cMMSupp = 'MMSUPP';
  cMMAssign = 'MMASSIGN';
  cMMAdmin = 'MMAdmin';
begin

  xLogUser := GetLogonUserName;
  x := Pos('\', xLogUser);
  Delete(xLogUser, 1, x);

  xMMUser := '';
  xMMUser := GetRegString(cRegCU, cRegMMSecurityPath, cRegLogonUserName, xMMUser);

  xLogUser := UpperCase(xLogUser);
  xMMUser := UpperCase(xMMUser);

  Result := False;

  Result := AnsiSameStr(xMMUser, cMMSupp);
  if Result then exit;

  Result := AnsiSameStr(xLogUser, cMMSupp);
  if Result then exit;

  Result := AnsiSameStr(xMMUser, cMMAssign);
  if Result then exit;

  Result := AnsiSameStr(xLogUser, cMMAssign);

  Result := AnsiSameStr(xMMUser, cMMAdmin);
  if Result then exit;

  Result := AnsiSameStr(xLogUser, cMMAdmin);
end;
//------------------------------------------------------------------------------
procedure TSetupMain.acLogin1Execute(Sender: TObject);
begin
  inherited;
  {
  if not tsUserAdmin.TabVisible then
     tsUserAdmin.TabVisible:= IsSecurityOk;   //-> siehe SetNTProductLayout();
  }
  UpDate;
  Repaint;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//MM-Dateien mit Vers. aus MMFile-Liste ins Grid schreiben
//******************************************************************************
procedure TSetupMain.tsFileVersionEnter(Sender: TObject);
var xRow : Integer;
    xSite : Real;
begin
  inherited;

  try
      if not Assigned(mMMFiles) then
          mMMFiles := TMMFiles.Create;

      //MM-Dateien einlesen & ausgeben (nur 1 mal)     
      if (mMMFiles.MMFileCount <= 0) then begin
         Screen.Cursor := crHourGlass;

         Application.ProcessMessages;
         mMMFiles.FetchData;

         xRow :=0;
         sgFileVersion.RowCount := 2;

         repeat
           with mMMFiles.MMFileRec[xRow] do begin
                 sgFileVersion.Cells[0, xRow + 1] := StringReplace( Format('%s\%s',[Path, MMFile] ), '\\', '\',[rfReplaceAll] );
                 sgFileVersion.Cells[1, xRow + 1] := Version;
                 xSite := Size Div 1024;

                 if xSite >= 1000 then begin
                    xSite := xSite / 1024;
                    sgFileVersion.Cells[2, xRow + 1] := Format('%2f MB',[xSite] );     //  Format('%8.2f'
                 end else
                    sgFileVersion.Cells[2, xRow + 1] := Format('%d kB',[Round(xSite)] );


                 sgFileVersion.RowCount :=  sgFileVersion.RowCount + 1;
                 inc(xRow);

                 if xRow MOD 10 = 0 then
                    Application.ProcessMessages;
           end;
         until xRow = mMMFiles.MMFileCount-1;

         sgFileVersion.RowCount :=  sgFileVersion.RowCount - 1;
      end;
      Application.ProcessMessages;
  finally
    Screen.Cursor :=  crDefault;
  end;
end;
//------------------------------------------------------------------------------



{ TInitializingThread }

constructor TInitializingThread.Create(aForm: TSetupMain);
begin
  mForm := aForm;
  inherited Create(FALSE);
  FreeOnTerminate := True;

end;
//------------------------------------------------------------------------------
destructor TInitializingThread.Destroy;
begin
  inherited;

end;
//------------------------------------------------------------------------------
procedure TInitializingThread.Execute;
begin
  Initializing;
end;
//------------------------------------------------------------------------------
procedure TInitializingThread.Initializing;
var
  xSettings: TMMSettings;
  xText: string;
begin

EnterMethod('TInitializingThread.Initializing');

  with mForm do begin

      sgProcesses.ColCount := 2;
      //sgFileVersion.ColCount := 3;
      mNTProduct := GetNTProduct;

      xSettings := TMMSettings.Create;

      xSettings.Init;
      ISMMEasy := xSettings.IsPackageEasy;

      ISPro      := xSettings.IsPackagePro;
      ISStandard := xSettings.IsPackageStandard;

      xSettings.Free;
{
      //TODO: hier wird Zeit ben�tigt -> kann man das noch optimieren?
    / CreateSecurityClasses;


      //Sequrity-Frames auf Security-TabSheet plazieren
      with MMSecurityAppl do begin
        Align := alClient;
        ParentWindow := tsSecurityAppl.Handle;
        Parent := tsSecurityAppl;
        Visible := True;

        with gbRemark do begin
          Left := MMSecurityAppl.Width - 220;
          Align := alNone;
          Width := 217;
          Top := 0;
          Height := paTop.Height - 1;
          ShowHint := False;
        end;

        with mLabel do begin
          Left := 5;
          Top := 13;
          AutoSize := True;
          Font.Color := clRed;
          Font.Style := [fsBold];
        end;

        with lbRemark do begin
          Align := alNone;
          Top := 13;
          Left := mLabel.Left + mLabel.Width + 5;
          Width := gbRemark.Width - Left - 5;
          Height := gbRemark.Height - Top - 5;

          AutoSize := False;
          WordWrap := True;
              //Caption:= xText;
        end;
      end;

      with MMSecurityMMGroups do begin
        Align := alClient;
        ParentWindow := tsSecurityGroup.Handle;
        Parent := tsSecurityGroup;
        Visible := True;

        with gbRemark do begin
          Left := MMSecurityAppl.Width - 220;
          Align := alNone;
          Width := 217;
          Top := 0;
          Height := paTop.Height - 1;
          ShowHint := False;
        end;

        with mLabel do begin
          Left := 5;
          Top := 13;
          AutoSize := True;
          Font.Color := clRed;
          Font.Style := [fsBold];
        end;

        with lbRemark do begin
          Align := alNone;
          Top := 13;
          Left := mLabel.Left + mLabel.Width + 5;
          Width := gbRemark.Width - Left - 5;
          Height := gbRemark.Height - Top - 5;

          AutoSize := False;
          WordWrap := True;
              //Caption:= xText;
        end;

      end;

      //Customer-Security anzeigen
      mSecurityChanged := False;
      gActiveSecurityClass := (gActualSecurity as TBaseMMSecurity);
      ShowSequrity(gActiveSecurityClass);
      acSaveSecurity.Execute;
}
      GetNetAmbient;
  end;

  Terminate;
  CodeSite.SendMsg('TInitializingThread  terminate');
end;

procedure TSetupMain.mmLabel20Click(Sender: TObject);
begin
  inherited;
end;

//------------------------------------------------------------------------------
procedure TSetupMain.sbGetMMHost1Click(Sender: TObject);
begin
  inherited;
  try
    with TfMMPCNames.Create (Self, mDomainNamesThread, SetupEdit2.Text, ctMMComputers ) do begin
      if ShowModal = mrOK then
         SetupEdit2.Text := MMPCNames;
      Free;
    end;
  except
  end;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.sbGetMMHost2Click(Sender: TObject);
begin
  inherited;
  try
    with TfMMPCNames.Create (Self, mDomainNamesThread, SetupEdit3.Text, ctMMPDC ) do begin
      if ShowModal = mrOK then
         SetupEdit3.Text := MMPCNames;
      Free;
    end;
  except
  end;
end;
//------------------------------------------------------------------------------
procedure TSetupMain.sbGetMMHost3Click(Sender: TObject);
begin
  inherited;
  try
    with TfMMPCNames.Create (Self, mDomainNamesThread, SetupEdit1.Text, ctMMHost ) do begin
      if ShowModal = mrOK then
         SetupEdit1.Text := MMPCNames;
      Free;
    end;
  except
  end;
end;

procedure TSetupMain.tsSystemCommonContextPopup(Sender: TObject; MousePos:
    TPoint; var Handled: Boolean);
begin
  inherited;
end;

//------------------------------------------------------------------------------
initialization
  CodeSite.Enabled := GetRegBoolean(cRegLM, cRegMMDebug, 'MMConfiguration', False);
end.

