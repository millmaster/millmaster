object fSplash: TfSplash
  Left = 405
  Top = 183
  Cursor = crHourGlass
  BorderStyle = bsNone
  ClientHeight = 133
  ClientWidth = 292
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 292
    Height = 133
    Cursor = crHourGlass
    Align = alClient
    BevelInner = bvLowered
    BevelWidth = 2
    TabOrder = 0
    object mmLabel1: TmmLabel
      Left = 4
      Top = 4
      Width = 284
      Height = 13
      Align = alTop
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel3: TmmLabel
      Left = 4
      Top = 17
      Width = 284
      Height = 40
      Cursor = crHourGlass
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = 'Millmaster configuration'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -24
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel2: TmmLabel
      Left = 4
      Top = 57
      Width = 284
      Height = 22
      Align = alClient
      Alignment = taCenter
      AutoSize = False
      Caption = 'Initializing ...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmAnimate1: TmmAnimate
      Left = 4
      Top = 79
      Width = 284
      Height = 50
      Align = alBottom
      Active = False
      StopFrame = 23
    end
  end
end
