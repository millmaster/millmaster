object MMSecurityMMGroups: TMMSecurityMMGroups
  Left = 0
  Top = 0
  Width = 680
  Height = 351
  TabOrder = 0
  OnResize = FrameResize
  object paTop: TmmPanel
    Left = 0
    Top = 0
    Width = 680
    Height = 57
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object mmLabel1: TmmLabel
      Left = 5
      Top = 5
      Width = 91
      Height = 13
      Caption = '(*)Benutzergruppen'
      FocusControl = cbUserGroups
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object cbUserGroups: TmmComboBox
      Left = 5
      Top = 20
      Width = 200
      Height = 21
      Style = csDropDownList
      Color = clWindow
      ItemHeight = 13
      Sorted = True
      TabOrder = 0
      Visible = True
      OnChange = cbUserGroupsChange
      AutoLabel.Control = mmLabel1
      AutoLabel.LabelPosition = lpTop
      Edit = True
      ReadOnlyColor = clInfoBk
      ShowMode = smExtended
    end
    object gbRemark: TmmGroupBox
      Left = 504
      Top = 8
      Width = 129
      Height = 41
      Caption = '(*)Bemerkung'
      TabOrder = 1
      CaptionSpace = True
      object lbRemark: TmmLabel
        Left = 34
        Top = 15
        Width = 6
        Height = 13
        Caption = '0'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mLabel: TmmLabel
        Left = 8
        Top = 16
        Width = 7
        Height = 16
        Caption = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
  end
  object esgGroupSecurity: TmmExtStringGrid
    Left = 0
    Top = 57
    Width = 680
    Height = 294
    Align = alClient
    ColCount = 3
    DefaultColWidth = 130
    DefaultRowHeight = 21
    FixedCols = 0
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goColSizing, goRowSelect]
    ParentFont = False
    TabOrder = 1
    Visible = True
    OnClick = esgGroupSecurityClick
    AutoNumAlign = False
    AutoSize = False
    VAlignment = vtaTop
    EnhTextSize = True
    EnhRowColMove = False
    SortFixedCols = False
    SizeWithForm = True
    Multilinecells = True
    OnGetCellColor = esgGroupSecurityGetCellColor
    SortDirection = sdAscending
    OnRadioClick = esgGroupSecurityRadioClick
    SortFull = True
    SortAutoFormat = True
    SortShow = False
    EnableGraphics = True
    SortColumn = 0
    HintColor = clYellow
    SelectionColor = clHighlight
    SelectionTextColor = clHighlightText
    SelectionRectangle = True
    SelectionRTFKeep = False
    HintShowCells = False
    OleDropTarget = False
    OleDropSource = False
    OleDropRTF = False
    PrintSettings.FooterSize = 0
    PrintSettings.HeaderSize = 0
    PrintSettings.Time = ppNone
    PrintSettings.Date = ppNone
    PrintSettings.DateFormat = 'dd/mm/yyyy'
    PrintSettings.PageNr = ppNone
    PrintSettings.Title = ppNone
    PrintSettings.Font.Charset = DEFAULT_CHARSET
    PrintSettings.Font.Color = clWindowText
    PrintSettings.Font.Height = -11
    PrintSettings.Font.Name = 'MS Sans Serif'
    PrintSettings.Font.Style = []
    PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
    PrintSettings.HeaderFont.Color = clWindowText
    PrintSettings.HeaderFont.Height = -11
    PrintSettings.HeaderFont.Name = 'MS Sans Serif'
    PrintSettings.HeaderFont.Style = []
    PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
    PrintSettings.FooterFont.Color = clWindowText
    PrintSettings.FooterFont.Height = -11
    PrintSettings.FooterFont.Name = 'MS Sans Serif'
    PrintSettings.FooterFont.Style = []
    PrintSettings.Borders = pbNoborder
    PrintSettings.BorderStyle = psSolid
    PrintSettings.Centered = False
    PrintSettings.RepeatFixedRows = False
    PrintSettings.RepeatFixedCols = False
    PrintSettings.LeftSize = 0
    PrintSettings.RightSize = 0
    PrintSettings.ColumnSpacing = 0
    PrintSettings.RowSpacing = 0
    PrintSettings.TitleSpacing = 0
    PrintSettings.Orientation = poPortrait
    PrintSettings.FixedWidth = 0
    PrintSettings.FixedHeight = 0
    PrintSettings.UseFixedHeight = False
    PrintSettings.UseFixedWidth = False
    PrintSettings.FitToPage = fpNever
    PrintSettings.PageNumSep = '/'
    PrintSettings.NoAutoSize = False
    PrintSettings.PrintGraphics = False
    HTMLSettings.Width = 100
    Navigation.AllowInsertRow = False
    Navigation.AllowDeleteRow = False
    Navigation.AdvanceOnEnter = True
    Navigation.AdvanceInsert = False
    Navigation.AutoGotoWhenSorted = False
    Navigation.AutoGotoIncremental = False
    Navigation.AutoComboDropSize = False
    Navigation.AdvanceDirection = adLeftRight
    Navigation.AllowClipboardShortCuts = False
    Navigation.AllowSmartClipboard = False
    Navigation.AllowRTFClipboard = False
    Navigation.AdvanceAuto = False
    Navigation.InsertPosition = pInsertBefore
    Navigation.CursorWalkEditor = False
    Navigation.MoveRowOnSort = False
    Navigation.ImproveMaskSel = False
    Navigation.AlwaysEdit = False
    ColumnSize.Save = False
    ColumnSize.Stretch = True
    ColumnSize.Location = clRegistry
    CellNode.Color = clSilver
    CellNode.NodeType = cnFlat
    CellNode.NodeColor = clBlack
    SizeWhileTyping.Height = False
    SizeWhileTyping.Width = False
    MouseActions.AllSelect = False
    MouseActions.ColSelect = False
    MouseActions.RowSelect = True
    MouseActions.DirectEdit = False
    MouseActions.DisjunctRowSelect = False
    MouseActions.AllColumnSize = False
    MouseActions.CaretPositioning = False
    IntelliPan = ipVertical
    URLColor = clBlack
    URLShow = False
    URLFull = False
    URLEdit = False
    ScrollType = ssNormal
    ScrollColor = clNone
    ScrollWidth = 16
    ScrollProportional = False
    ScrollHints = shNone
    OemConvert = False
    FixedFooters = 0
    FixedRightCols = 0
    FixedColWidth = 228
    FixedRowHeight = 30
    FixedFont.Charset = DEFAULT_CHARSET
    FixedFont.Color = clWindowText
    FixedFont.Height = -11
    FixedFont.Name = 'MS Sans Serif'
    FixedFont.Style = []
    WordWrap = False
    Lookup = False
    LookupCaseSensitive = False
    LookupHistory = False
    BackGround.Top = 0
    BackGround.Left = 0
    BackGround.Display = bdTile
    Hovering = False
    Filter = <>
    FilterActive = False
    AutoLabel.LabelPosition = lpLeft
    ColWidths = (
      228
      184
      261)
    RowHeights = (
      30
      21
      21
      21
      21)
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    OnLanguageChange = mmTranslator1LanguageChange
    Left = 288
    Top = 16
    TargetsData = (
      1
      7
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Cells'
        0)
      (
        'TmmExtStringGrid'
        '*'
        0)
      (
        'TmmGroupbox'
        '*'
        0)
      (
        'TmmLabel'
        '*'
        0))
  end
end
