inherited SetupMain: TSetupMain
  Tag = 1
  Left = 351
  Top = 257
  Width = 829
  Height = 619
  Caption = 'Millmaster settings  (ADO vers.)'
  Constraints.MinHeight = 600
  Constraints.MinWidth = 805
  FormStyle = fsNormal
  OnActivate = FormActivate
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited plTopPanel: TmmPanel
    Width = 821
    inherited Panel2: TmmPanel
      Left = 757
    end
    inherited mToolBar: TmmToolBar
      Width = 757
    end
  end
  inherited mmStatusBar: TmmStatusBar
    Top = 532
    Width = 821
    Height = 21
    Panels = <
      item
        Width = 50
      end>
    ParentShowHint = False
    SimplePanel = True
    Visible = False
  end
  object SetupPageControl: TmmPageControl [2]
    Left = 0
    Top = 30
    Width = 821
    Height = 502
    ActivePage = tsSystem
    Align = alClient
    TabOrder = 2
    TabStop = False
    OnChange = SetupPageControlChange
    object tsSystem: TTabSheet
      Caption = '(*)System'
      ImageIndex = 3
      object SystemPageControl: TmmPageControl
        Left = 0
        Top = 25
        Width = 813
        Height = 449
        ActivePage = tsSystemCommon
        Align = alClient
        TabOrder = 0
        object tsSystemCommon: TTabSheet
          Caption = '(*)Allgemein'
          OnContextPopup = tsSystemCommonContextPopup
          OnResize = tsSystemCommonResize
          object mmLabel25: TmmLabel
            Left = 20
            Top = 65
            Width = 105
            Height = 13
            Caption = '(*)Einheit Garnnummer'
            FocusControl = scbYarnCntUnit
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel27: TmmLabel
            Left = 20
            Top = 15
            Width = 151
            Height = 13
            Caption = '(*)MillMaster Pfad fuer Helpdatei'
            FocusControl = edHelp
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object SpeedButton2: TSpeedButton
            Left = 420
            Top = 30
            Width = 21
            Height = 21
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = SpeedButton2Click
          end
          object bvCommonVert: TmmBevel
            Left = 456
            Top = 10
            Width = 3
            Height = 405
            Visible = False
          end
          object mmLabel34: TmmLabel
            Left = 20
            Top = 365
            Width = 133
            Height = 13
            Caption = '(*)Standard Schichtkalender'
            FocusControl = scbShifts
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object SetupLabel15: TSetupLabel
            Left = 20
            Top = 315
            Width = 293
            Height = 13
            Hint = '(*)Firmenname'
            AutoSize = False
            Caption = '(*)Firmenname'
            Constraints.MinWidth = 30
            FocusControl = edCompanyName
            WordWrap = True
            ValueName = 'SQLCompanyName'
            SetupModul = MMSetupModul1
          end
          object scbYarnCntUnit: TSetupComboBox
            Left = 20
            Top = 80
            Width = 160
            Height = 21
            Hint = '(*)Einheit der Garnnummer'
            Style = csDropDownList
            Color = clWindow
            ItemHeight = 13
            Sorted = True
            TabOrder = 0
            Visible = True
            AutoLabel.Control = mmLabel25
            AutoLabel.LabelPosition = lpTop
            Edit = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
            OnSave = scbYarnCntUnitSave
            OnGetValue = scbYarnCntUnitGetValue
            OnGetDefault = scbYarnCntUnitGetDefault
            ValueName = 'SQLYarnCntUnit'
            SetupModul = MMSetupModul1
          end
          object edHelp: TSetupEdit
            Left = 20
            Top = 30
            Width = 390
            Height = 21
            Hint = '(*)MillMaster Pfad fuer Helpdatei'
            TabOrder = 1
            Visible = True
            AutoLabel.Control = mmLabel27
            AutoLabel.LabelPosition = lpTop
            ValueName = 'HelpFilePath'
            SetupModul = MMSetupModul1
          end
          object gbDefaultRepSettings: TmmGroupBox
            Left = 20
            Top = 120
            Width = 357
            Height = 170
            Caption = '(*)Standardeinstellungen fuer die Datenauswertung'
            TabOrder = 2
            CaptionSpace = True
            object mmLabel30: TmmLabel
              Left = 20
              Top = 20
              Width = 83
              Height = 13
              Caption = '(*)Laengeneinheit'
              FocusControl = scbMMUnits
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object mmLabel36: TmmLabel
              Left = 20
              Top = 70
              Width = 190
              Height = 13
              Caption = '(*)Unterdrueckungswert fuer Klassierung'
              FocusControl = speClassMatrixValue
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object mmLabel12: TmmLabel
              Left = 20
              Top = 120
              Width = 161
              Height = 13
              Caption = '(*)Zeitvorgabe fuer Spindelbericht.'
              FocusControl = SetupSpinEdit3
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object scbMMUnits: TSetupComboBox
              Left = 20
              Top = 35
              Width = 110
              Height = 21
              Hint = '(*)Laengeneinheit fuer den Bericht'
              Style = csDropDownList
              Color = clWindow
              ItemHeight = 13
              TabOrder = 0
              Visible = True
              AutoLabel.Control = mmLabel30
              AutoLabel.LabelPosition = lpTop
              Edit = True
              ReadOnlyColor = clInfoBk
              ShowMode = smNormal
              OnSave = scbMMUnitsSave
              OnGetValue = scbMMUnitsGetValue
              OnGetDefault = scbMMUnitsGetDefault
              ValueName = 'SQLMMUnits'
              SetupModul = MMSetupModul1
            end
            object speClassMatrixValue: TSetupEdit
              Left = 20
              Top = 85
              Width = 41
              Height = 21
              Hint = '(*)Unterdrueckungswert fuer Nullbereiche in Klassiermatrix'
              EditMask = '#.##;1;_'
              MaxLength = 4
              TabOrder = 1
              Text = ' .  '
              Visible = True
              AutoLabel.Control = mmLabel36
              AutoLabel.LabelPosition = lpTop
              ValueName = 'SQLQMatrixZeroLimit'
              SetupModul = MMSetupModul1
            end
            object SetupSpinEdit3: TSetupSpinEdit
              Left = 20
              Top = 135
              Width = 50
              Height = 22
              Hint = '(*)Zeitvorgabe fuer Spindelbericht.'
              MaxValue = 30
              MinValue = 1
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              Value = 1
              Visible = True
              AutoLabel.Control = mmLabel12
              AutoLabel.LabelPosition = lpTop
              ValueName = 'DefaultSpindleReportTimeRange'
              SetupModul = MMSetupModul1
            end
          end
          object scbShifts: TSetupComboBox
            Left = 20
            Top = 380
            Width = 225
            Height = 21
            Hint = '(*)Standard Schichtkalender fuer Berichte und MMAlarm.'
            Style = csDropDownList
            Color = clWindow
            ItemHeight = 13
            TabOrder = 3
            Visible = True
            AutoLabel.Control = mmLabel34
            AutoLabel.LabelPosition = lpTop
            Edit = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
            OnSave = scbShiftsSave
            OnGetValue = scbShiftsGetValue
            OnGetDefault = scbShiftsGetDefault
            ValueName = 'DefaultShiftCalID'
            SetupModul = MMSetupModul1
          end
          object edCompanyName: TSetupEdit
            Left = 20
            Top = 330
            Width = 390
            Height = 21
            Hint = '(*)Firmenname'
            TabOrder = 4
            Visible = True
            AutoLabel.Control = SetupLabel15
            AutoLabel.LabelPosition = lpTop
            ValueName = 'SQLCompanyName'
            SetupModul = MMSetupModul1
          end
        end
        object tsZEConfig: TTabSheet
          Caption = '(*)ZE Konfiguration'
          ImageIndex = 2
          object GroupBox1: TmmGroupBox
            Left = 20
            Top = 20
            Width = 253
            Height = 169
            Caption = '(*)Daten Konfiguration ZE'
            TabOrder = 0
            CaptionSpace = True
            object mmLabel31: TmmLabel
              Left = 20
              Top = 65
              Width = 83
              Height = 13
              Caption = '(*)Laengenmodus'
              FocusControl = scbMMLenghtMode
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object mmLabel26: TmmLabel
              Left = 20
              Top = 115
              Width = 162
              Height = 13
              Caption = '(*)Laengenfenster fuer YarnMaster'
              FocusControl = scbYMLenWindow
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object mmLabel24: TmmLabel
              Left = 20
              Top = 20
              Width = 119
              Height = 13
              Caption = '(*)Anzahl Pilot Spulstellen'
              FocusControl = sePilotSpindles
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object lbPilotSpindles: TmmLabel
              Left = 104
              Top = 40
              Width = 15
              Height = 13
              Caption = 'xxx'
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object scbMMLenghtMode: TSetupComboBox
              Left = 20
              Top = 80
              Width = 150
              Height = 21
              Hint = '(*)Laengenmodus'
              Style = csDropDownList
              Color = clWindow
              ItemHeight = 0
              TabOrder = 0
              Visible = True
              AutoLabel.Control = mmLabel31
              AutoLabel.LabelPosition = lpTop
              Edit = True
              ReadOnlyColor = clInfoBk
              ShowMode = smNormal
              OnSave = scbMMLenghtModeSave
              OnGetValue = scbMMLenghtModeGetValue
              OnGetDefault = scbMMLenghtModeGetDefault
              ValueName = 'SQLMMLenghtMode'
              SetupModul = MMSetupModul1
            end
            object scbYMLenWindow: TSetupComboBox
              Left = 20
              Top = 130
              Width = 160
              Height = 21
              Hint = '(*)Laengenfenster fuer YarnMaster'
              Style = csDropDownList
              Color = clWindow
              ItemHeight = 13
              Sorted = True
              TabOrder = 1
              Visible = True
              Items.Strings = (
                '10'
                '100'
                '1000')
              AutoLabel.Control = mmLabel26
              AutoLabel.LabelPosition = lpTop
              Edit = True
              ReadOnlyColor = clInfoBk
              ShowMode = smNormal
              OnSave = scbYMLenWindowSave
              OnGetValue = scbYMLenWindowGetValue
              OnGetDefault = scbYMLenWindowGetDefault
              ValueName = 'SQLYMLenWindow'
              SetupModul = MMSetupModul1
            end
            object sePilotSpindles: TSetupSpinEdit
              Left = 20
              Top = 35
              Width = 70
              Height = 22
              Hint = '(*)Anzahl Pilot Spulstellen'
              MaxValue = 80
              MinValue = 1
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              Value = 1
              Visible = True
              AutoLabel.Control = mmLabel24
              AutoLabel.LabelPosition = lpTop
              ValueName = 'SQLPilotSpindles'
              SetupModul = MMSetupModul1
            end
          end
        end
        object tsFloor: TTabSheet
          Caption = '(*)Floor'
          ImageIndex = 3
          object mmLabel28: TmmLabel
            Left = 20
            Top = 15
            Width = 186
            Height = 13
            Caption = '(*)Default Zeitselektion fuer Trenddaten'
            FocusControl = scbFloorTrendSelect
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel29: TmmLabel
            Left = 20
            Top = 65
            Width = 162
            Height = 13
            Caption = '(*)Zeitbereich fuer aktuelle Ansicht'
            FocusControl = SetupSpinEdit9
            Visible = True
            AutoLabel.LabelPosition = lpTop
          end
          object mmLabel35: TmmLabel
            Left = 104
            Top = 85
            Width = 15
            Height = 13
            Caption = 'xxx'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object scbFloorTrendSelect: TSetupComboBox
            Left = 20
            Top = 30
            Width = 185
            Height = 21
            Hint = '(*)Default Zeitselektion fuer Trenddaten'
            Style = csDropDownList
            Color = clWindow
            ItemHeight = 0
            TabOrder = 0
            Visible = True
            AutoLabel.Control = mmLabel28
            AutoLabel.LabelPosition = lpTop
            Edit = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
            ValueName = 'FloorTrendSelection'
            SetupModul = MMSetupModul1
          end
          object SetupSpinEdit9: TSetupSpinEdit
            Left = 20
            Top = 80
            Width = 70
            Height = 22
            Hint = '(*)Zeitbereich fuer aktuelle Ansicht'
            MaxValue = 20
            MinValue = 1
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            Value = 1
            Visible = True
            AutoLabel.Control = mmLabel29
            AutoLabel.LabelPosition = lpTop
            ValueName = 'IntervalTimeRange'
            SetupModul = MMSetupModul1
          end
        end
        object tsSystemServer: TTabSheet
          Caption = '(*)Server'
          ImageIndex = 1
          object mmGroupBox4: TmmGroupBox
            Left = 20
            Top = 10
            Width = 744
            Height = 100
            Caption = 'DelInt Event'
            TabOrder = 0
            CaptionSpace = True
            object SetupLabel1: TSetupLabel
              Left = 20
              Top = 20
              Width = 700
              Height = 35
              Hint = '(*)Anzahl Intervalle'
              AutoSize = False
              Caption = 
                '(*)Loescht die aeltesten Intervalle und deren abhaengige Daten a' +
                'us den Tabellen.'
              Color = clInfoBk
              Constraints.MinWidth = 30
              ParentColor = False
              WordWrap = True
              ValueName = 'RegMaxInterval'
              SetupModul = MMSetupModul1
            end
            object mmLabel1: TmmLabel
              Left = 95
              Top = 69
              Width = 88
              Height = 13
              Caption = '(*)Anzahl Intervalle'
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object mmLabel5: TmmLabel
              Left = 250
              Top = 70
              Width = 159
              Height = 13
              Caption = '(*)MinValue = %d, MaxValue = %d'
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object SetupSpinEdit1: TSetupSpinEdit
              Left = 20
              Top = 65
              Width = 65
              Height = 22
              MaxValue = 50
              MinValue = 1
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              Value = 1
              Visible = True
              AutoLabel.LabelPosition = lpLeft
              ValueName = 'RegMaxInterval'
              SetupModul = MMSetupModul1
            end
          end
          object mmGroupBox2: TmmGroupBox
            Left = 20
            Top = 120
            Width = 744
            Height = 100
            Caption = 'Interval length'
            TabOrder = 1
            CaptionSpace = True
            object SetupLabel2: TSetupLabel
              Left = 20
              Top = 20
              Width = 700
              Height = 35
              Hint = '(*)DelInt Event'
              AutoSize = False
              Caption = '(*)Laenge der Intervalle'
              Color = clInfoBk
              Constraints.MinWidth = 30
              ParentColor = False
              WordWrap = True
              ValueName = 'RegIntervalLen'
              SetupModul = MMSetupModul1
            end
            object mmLabel2: TmmLabel
              Left = 95
              Top = 69
              Width = 30
              Height = 13
              Caption = '(*)Min.'
              FocusControl = SetupSpinEdit2
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object mmLabel6: TmmLabel
              Left = 250
              Top = 70
              Width = 159
              Height = 13
              Caption = '(*)MinValue = %d, MaxValue = %d'
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object SetupSpinEdit2: TSetupSpinEdit
              Left = 20
              Top = 65
              Width = 65
              Height = 22
              Hint = '(*)DelInt Event'
              MaxValue = 70
              MinValue = 1
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              Value = 1
              Visible = True
              AutoLabel.Control = mmLabel2
              AutoLabel.Distance = 10
              AutoLabel.LabelPosition = lpRight
              ValueName = 'RegIntervalLen'
              SetupModul = MMSetupModul1
            end
          end
        end
        object tsOfflimit: TTabSheet
          Caption = '(*)Q-Offlimit'
          ImageIndex = 4
          object mmGroupBox13: TmmGroupBox
            Left = 30
            Top = 168
            Width = 320
            Height = 113
            Caption = '(*)Minimale produzierte Laenge fuer den statistischen Mittelwert'
            TabOrder = 0
            CaptionSpace = True
            object SetupLabel14: TSetupLabel
              Left = 20
              Top = 25
              Width = 290
              Height = 33
              Hint = 
                '(*)Minimale produzierte Laenge pro Schicht, ab welcher die Daten' +
                ' in den Mittelwert einbezogen werden.'
              AutoSize = False
              Caption = 
                '(*)Minimale produzierte Laenge pro Schicht, ab welcher die Daten' +
                ' in den Mittelwert einbezogen werden.'
              Color = clInfoBk
              Constraints.MinWidth = 30
              ParentColor = False
              WordWrap = True
              ValueName = 'SQLStatisticLenght'
              SetupModul = MMSetupModul1
            end
            object sedQOfflimitLen: TSetupEdit
              Left = 20
              Top = 70
              Width = 125
              Height = 21
              TabOrder = 0
              Visible = True
              OnChange = sedQOfflimitLenChange
              AutoLabel.LabelPosition = lpLeft
              ValueName = 'SQLStatisticLenght'
              SetupModul = MMSetupModul1
            end
            object nedQOfflimitLen: TNumEdit
              Left = 20
              Top = 70
              Width = 125
              Height = 21
              Decimals = 2
              Digits = 12
              Masks.PositiveMask = '#,##0'
              Min = 1
              NumericType = ntGeneral
              OnChange = nedQOfflimitLenChange
              OnKeyDown = nedQOfflimitLenKeyDown
              OnKeyUp = nedQOfflimitLenKeyUp
              TabOrder = 1
              UseRounding = True
              Validate = False
              ValidateString = 
                'Wert ist nicht im erlaubten Eingabebereich.'#10#13'Bereich von %s bis ' +
                '%s'
            end
            object srgQOfflimitUnit: TSetupRadioGroup
              Left = 190
              Top = 60
              Width = 121
              Height = 33
              Hint = '(*)Laengeneinheit fuer die QOfflimit Mittelwertbildung.'
              Caption = '(*)Laengeneinheit'
              Columns = 2
              Items.Strings = (
                'm'
                'yds')
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = srgQOfflimitUnitClick
              ValueName = 'SQLStatisticLengthUnit'
              SetupModul = MMSetupModul1
            end
          end
          object mmGroupBox14: TmmGroupBox
            Left = 30
            Top = 24
            Width = 320
            Height = 113
            Caption = '(*)Schichttiefe fuer die QOfflimitgrafik'
            TabOrder = 1
            CaptionSpace = True
            object SetupLabel13: TSetupLabel
              Left = 20
              Top = 25
              Width = 290
              Height = 40
              Hint = 
                '(*)Anzahl vergangener Schichten, welche fuer eine aussagekraefti' +
                'ge QOfflimitgrafik benoetigt werden.'
              AutoSize = False
              Caption = 
                '(*)Anzahl vergangener Schichten, welche fuer eine aussagekraefti' +
                'ge QOfflimitgrafik benoetigt werden.'
              Color = clInfoBk
              Constraints.MinWidth = 30
              ParentColor = False
              WordWrap = True
              ValueName = 'SQLGraphicShiftdepth'
              SetupModul = MMSetupModul1
            end
            object mmLabel33: TmmLabel
              Left = 90
              Top = 85
              Width = 15
              Height = 13
              Caption = 'xxx'
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object SetupSpinEdit8: TSetupSpinEdit
              Left = 20
              Top = 80
              Width = 50
              Height = 22
              Hint = 
                '(*)Anzahl vergangener Schichten, welche fuer eine aussagekraefti' +
                'ge QOfflimitgrafik benoetigt werden.'
              MaxValue = 20
              MinValue = 5
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              Value = 5
              Visible = True
              AutoLabel.LabelPosition = lpLeft
              ValueName = 'SQLGraphicShiftdepth'
              SetupModul = MMSetupModul1
            end
          end
        end
        object tsStyle: TTabSheet
          Caption = '(*)Artikel'
          ImageIndex = 5
          object mmPanel4: TmmPanel
            Left = 10
            Top = 16
            Width = 520
            Height = 323
            BevelInner = bvRaised
            BevelOuter = bvLowered
            TabOrder = 0
            object SetupLabel12: TSetupLabel
              Left = 20
              Top = 78
              Width = 478
              Height = 37
              Hint = '(*)Definition des Standard-Partienamens'
              AutoSize = False
              Caption = 
                '(*)Definition des Standard-Partienamens, welcher beim Start eine' +
                'r neuen Partie angezeigt werden soll.'
              Color = clInfoBk
              Constraints.MinWidth = 30
              ParentColor = False
              ParentShowHint = False
              ShowHint = False
              WordWrap = True
              ValueName = 'SQLDefaultPartieIndexSet'
              SetupModul = MMSetupModul1
            end
            object rgSetPartieName: TSetupRadioGroup
              Left = 20
              Top = 123
              Width = 481
              Height = 169
              Hint = '(*)Definition des Standard-Partienamens'
              Caption = '(*)Definition des Standard-Partienamens'
              Items.Strings = (
                '(*)Keine Eingabe'
                '(*)Artikelname und ID'
                '(*)Artikelname und Datum'
                '(*)Artikelname'
                '(*)Individueller Name')
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = rgSetPartieNameClick
              CaptionSpace = True
              ValueName = 'SQLDefaultPartieIndexSet'
              SetupModul = MMSetupModul1
            end
            object seUserPartieName: TSetupEdit
              Left = 173
              Top = 260
              Width = 300
              Height = 21
              Hint = '(*)Definition des Standard-Partienamens'
              Enabled = False
              TabOrder = 1
              Visible = True
              AutoLabel.LabelPosition = lpLeft
              ValueName = 'SQLDefaultPartieNameSet'
              SetupModul = MMSetupModul1
            end
            object scbTemplateSetsAvailable: TSetupCheckBox
              Left = 20
              Top = 20
              Width = 475
              Height = 17
              Hint = '(*)Vorlagen nur ab Vorlagenverwaltung moeglich'
              Caption = '(*)Vorlagen nur ab Vorlagenverwaltung moeglich'
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              Visible = True
              OnClick = scbTemplateSetsAvailableClick
              AutoLabel.LabelPosition = lpLeft
              ValueName = 'SQLTemplateSetsAvailable'
              SetupModul = MMSetupModul1
            end
          end
          object mmGroupBox12: TmmGroupBox
            Left = 10
            Top = 355
            Width = 520
            Height = 57
            Caption = '(*)Korrekturfaktor'
            TabOrder = 1
            CaptionSpace = True
            object SetupCheckBox4: TSetupCheckBox
              Left = 30
              Top = 20
              Width = 475
              Height = 25
              Hint = '(*)Korrekturfaktor fuer die Imperfektion zulassen.'
              Caption = '(*)Korrekturfaktor fuer die Imperfektion zulassen.'
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              Visible = True
              AutoLabel.LabelPosition = lpLeft
              ValueName = 'SQLCorrectionFactorImp'
              SetupModul = MMSetupModul1
            end
          end
        end
        object tsMMSystem: TTabSheet
          Caption = '(*)MM System'
          ImageIndex = 6
          object mmLabel19: TmmLabel
            Left = 50
            Top = 13
            Width = 79
            Height = 13
            Caption = '(*)MM Hostname'
            FocusControl = SetupEdit1
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel20: TmmLabel
            Left = 50
            Top = 121
            Width = 400
            Height = 13
            Hint = '(*)Namen aller MillMaster Computer'
            AutoSize = False
            Caption = '(*)Namen aller MillMaster Computer'
            FocusControl = SetupEdit2
            Visible = True
            WordWrap = True
            OnClick = mmLabel20Click
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel32: TmmLabel
            Left = 50
            Top = 269
            Width = 425
            Height = 26
            AutoSize = False
            Caption = '(*)MillMaster User Host'
            FocusControl = SetupEdit3
            Layout = tlBottom
            Visible = True
            WordWrap = True
            AutoLabel.LabelPosition = lpTop
          end
          object sbGetMMHost1: TmmSpeedButton
            Left = 15
            Top = 136
            Width = 24
            Height = 22
            Glyph.Data = {
              36050000424D3605000000000000360400002800000010000000100000000100
              0800000000000001000000000000000000000001000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
              A6000020400000206000002080000020A0000020C0000020E000004000000040
              20000040400000406000004080000040A0000040C0000040E000006000000060
              20000060400000606000006080000060A0000060C0000060E000008000000080
              20000080400000806000008080000080A0000080C0000080E00000A0000000A0
              200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
              200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
              200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
              20004000400040006000400080004000A0004000C0004000E000402000004020
              20004020400040206000402080004020A0004020C0004020E000404000004040
              20004040400040406000404080004040A0004040C0004040E000406000004060
              20004060400040606000406080004060A0004060C0004060E000408000004080
              20004080400040806000408080004080A0004080C0004080E00040A0000040A0
              200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
              200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
              200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
              20008000400080006000800080008000A0008000C0008000E000802000008020
              20008020400080206000802080008020A0008020C0008020E000804000008040
              20008040400080406000804080008040A0008040C0008040E000806000008060
              20008060400080606000806080008060A0008060C0008060E000808000008080
              20008080400080806000808080008080A0008080C0008080E00080A0000080A0
              200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
              200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
              200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
              2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
              2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
              2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
              2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
              2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
              2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
              2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FD0000000000
              0000000000000000FDFDA4A4A4A4A4A4A4A4A4A4A4A4A4A400FDA4FF07FA0707
              07070707A40707A4A400A4FF0707070707000000000007A4A400A4FFFFFFFFFF
              FFFFFFFFFFFFFFA4A400FD000000000000000000000000A4A400FDA4A4A4A4A4
              A4A4A4A4A4A4A400A4FDFDA4FF070707070707070707A4A400FDFDA4FF0700FC
              FCFCFCFCFC07A4A400FDFDA4FF0700FCFCFCFCFCFC07A4A400FDFDA4FF0700FC
              FCFCFCFCFC07A4A400FDFDA4FF0700FCFCFCFCFCFC07A4A400FDFDA4FF070000
              000000000007A4A400FDFDA4FFFFFFFFFFFFFFFFFFFFA4A400FDFDFDA4070707
              070707070707FFA400FDFDFDFDA4A4A4A4A4A4A4A4A4A4A4FDFD}
            Visible = True
            OnClick = sbGetMMHost1Click
            AutoLabel.LabelPosition = lpLeft
          end
          object sbGetMMHost2: TmmSpeedButton
            Left = 15
            Top = 297
            Width = 24
            Height = 22
            Glyph.Data = {
              36050000424D3605000000000000360400002800000010000000100000000100
              0800000000000001000000000000000000000001000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
              A6000020400000206000002080000020A0000020C0000020E000004000000040
              20000040400000406000004080000040A0000040C0000040E000006000000060
              20000060400000606000006080000060A0000060C0000060E000008000000080
              20000080400000806000008080000080A0000080C0000080E00000A0000000A0
              200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
              200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
              200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
              20004000400040006000400080004000A0004000C0004000E000402000004020
              20004020400040206000402080004020A0004020C0004020E000404000004040
              20004040400040406000404080004040A0004040C0004040E000406000004060
              20004060400040606000406080004060A0004060C0004060E000408000004080
              20004080400040806000408080004080A0004080C0004080E00040A0000040A0
              200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
              200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
              200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
              20008000400080006000800080008000A0008000C0008000E000802000008020
              20008020400080206000802080008020A0008020C0008020E000804000008040
              20008040400080406000804080008040A0008040C0008040E000806000008060
              20008060400080606000806080008060A0008060C0008060E000808000008080
              20008080400080806000808080008080A0008080C0008080E00080A0000080A0
              200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
              200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
              200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
              2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
              2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
              2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
              2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
              2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
              2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
              2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FD0000000000
              0000000000000000FDFDA4A4A4A4A4A4A4A4A4A4A4A4A4A400FDA4FF07FA0707
              07070707A40707A4A400A4FF0707070707000000000007A4A400A4FFFFFFFFFF
              FFFFFFFFFFFFFFA4A400FD000000000000000000000000A4A400FDA4A4A4A4A4
              A4A4A4A4A4A4A400A4FDFDA4FF070707070707070707A4A400FDFDA4FF0700FC
              FCFCFCFCFC07A4A400FDFDA4FF0700FCFCFCFCFCFC07A4A400FDFDA4FF0700FC
              FCFCFCFCFC07A4A400FDFDA4FF0700FCFCFCFCFCFC07A4A400FDFDA4FF070000
              000000000007A4A400FDFDA4FFFFFFFFFFFFFFFFFFFFA4A400FDFDFDA4070707
              070707070707FFA400FDFDFDFDA4A4A4A4A4A4A4A4A4A4A4FDFD}
            Visible = True
            OnClick = sbGetMMHost2Click
            AutoLabel.LabelPosition = lpLeft
          end
          object sbGetMMHost3: TmmSpeedButton
            Left = 15
            Top = 28
            Width = 24
            Height = 22
            Glyph.Data = {
              36050000424D3605000000000000360400002800000010000000100000000100
              0800000000000001000000000000000000000001000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
              A6000020400000206000002080000020A0000020C0000020E000004000000040
              20000040400000406000004080000040A0000040C0000040E000006000000060
              20000060400000606000006080000060A0000060C0000060E000008000000080
              20000080400000806000008080000080A0000080C0000080E00000A0000000A0
              200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
              200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
              200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
              20004000400040006000400080004000A0004000C0004000E000402000004020
              20004020400040206000402080004020A0004020C0004020E000404000004040
              20004040400040406000404080004040A0004040C0004040E000406000004060
              20004060400040606000406080004060A0004060C0004060E000408000004080
              20004080400040806000408080004080A0004080C0004080E00040A0000040A0
              200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
              200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
              200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
              20008000400080006000800080008000A0008000C0008000E000802000008020
              20008020400080206000802080008020A0008020C0008020E000804000008040
              20008040400080406000804080008040A0008040C0008040E000806000008060
              20008060400080606000806080008060A0008060C0008060E000808000008080
              20008080400080806000808080008080A0008080C0008080E00080A0000080A0
              200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
              200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
              200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
              2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
              2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
              2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
              2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
              2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
              2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
              2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FD0000000000
              0000000000000000FDFDA4A4A4A4A4A4A4A4A4A4A4A4A4A400FDA4FF07FA0707
              07070707A40707A4A400A4FF0707070707000000000007A4A400A4FFFFFFFFFF
              FFFFFFFFFFFFFFA4A400FD000000000000000000000000A4A400FDA4A4A4A4A4
              A4A4A4A4A4A4A400A4FDFDA4FF070707070707070707A4A400FDFDA4FF0700FC
              FCFCFCFCFC07A4A400FDFDA4FF0700FCFCFCFCFCFC07A4A400FDFDA4FF0700FC
              FCFCFCFCFC07A4A400FDFDA4FF0700FCFCFCFCFCFC07A4A400FDFDA4FF070000
              000000000007A4A400FDFDA4FFFFFFFFFFFFFFFFFFFFA4A400FDFDFDA4070707
              070707070707FFA400FDFDFDFDA4A4A4A4A4A4A4A4A4A4A4FDFD}
            Visible = True
            OnClick = sbGetMMHost3Click
            AutoLabel.LabelPosition = lpLeft
          end
          object slDescription_RegMMHost: TSetupLabel
            Left = 470
            Top = 28
            Width = 320
            Height = 39
            Hint = '(*)MM Hostname'
            AutoSize = False
            Caption = '(*)MM Hostname'
            Color = clInfoBk
            Constraints.MinWidth = 30
            ParentColor = False
            WordWrap = True
            ValueName = 'RegMMHost'
            SetupModul = MMSetupModul1
          end
          object slDescription_RegDomainNames: TSetupLabel
            Left = 470
            Top = 136
            Width = 320
            Height = 89
            AutoSize = False
            Color = clInfoBk
            Constraints.MinWidth = 30
            ParentColor = False
            WordWrap = True
            ValueName = 'RegDomainNames'
            SetupModul = MMSetupModul1
          end
          object slDescription_RegDomainController: TSetupLabel
            Left = 467
            Top = 297
            Width = 320
            Height = 52
            AutoSize = False
            Color = clInfoBk
            Constraints.MinWidth = 30
            ParentColor = False
            WordWrap = True
            ValueName = 'RegDomainController'
            SetupModul = MMSetupModul1
          end
          object SetupEdit1: TSetupEdit
            Left = 50
            Top = 28
            Width = 400
            Height = 21
            Hint = '(*)MM Hostname'
            BiDiMode = bdLeftToRight
            ParentBiDiMode = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            Visible = True
            AutoLabel.Control = mmLabel19
            AutoLabel.LabelPosition = lpTop
            ValueName = 'RegMMHost'
            SetupModul = MMSetupModul1
          end
          object SetupEdit2: TSetupEdit
            Left = 50
            Top = 136
            Width = 400
            Height = 21
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            Visible = True
            AutoLabel.Control = mmLabel20
            AutoLabel.LabelPosition = lpTop
            ValueName = 'RegDomainNames'
            SetupModul = MMSetupModul1
          end
          object SetupEdit3: TSetupEdit
            Left = 50
            Top = 297
            Width = 400
            Height = 21
            Hint = '(*)MillMaster User Host'
            TabOrder = 2
            Visible = True
            AutoLabel.Control = mmLabel32
            AutoLabel.LabelPosition = lpTop
            ValueName = 'RegDomainController'
            SetupModul = MMSetupModul1
          end
        end
      end
      object pSystemHeader: TPanel
        Left = 0
        Top = 0
        Width = 813
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
      end
    end
    object tsJob: TTabSheet
      Caption = '(*)Jobs'
      object gbCleanUpEvent: TmmGroupBox
        Left = 20
        Top = 120
        Width = 744
        Height = 100
        Caption = 'CleanUp Event'
        TabOrder = 0
        CaptionSpace = True
        object SetupLabel3: TSetupLabel
          Left = 20
          Top = 20
          Width = 700
          Height = 35
          Hint = '(*)CleanUp Event'
          AutoSize = False
          Caption = 
            '(*)Loescht Daten aus dem Datenpool, die aelter sind als die eing' +
            'estellte Zeit.'
          Color = clInfoBk
          Constraints.MinWidth = 30
          ParentColor = False
          WordWrap = True
          ValueName = 'RegCUpTimePeriod'
          SetupModul = MMSetupModul1
        end
        object mmLabel7: TmmLabel
          Left = 250
          Top = 65
          Width = 159
          Height = 13
          Caption = '(*)MinValue = %d, MaxValue = %d'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel3: TmmLabel
          Left = 95
          Top = 69
          Width = 30
          Height = 13
          Caption = '(*)Min.'
          FocusControl = sseCleanUpEvent
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object sseCleanUpEvent: TSetupSpinEdit
          Left = 20
          Top = 65
          Width = 65
          Height = 22
          Hint = '(*)CleanUp Event'
          MaxValue = 1440
          MinValue = 30
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Value = 30
          Visible = True
          AutoLabel.Control = mmLabel3
          AutoLabel.Distance = 10
          AutoLabel.LabelPosition = lpRight
          ValueName = 'RegCUpTimePeriod'
          SetupModul = MMSetupModul1
        end
      end
      object gbDelShiftByTime: TmmGroupBox
        Left = 20
        Top = 10
        Width = 744
        Height = 100
        Caption = 'DelShiftByTime Event'
        TabOrder = 1
        CaptionSpace = True
        object mmLabel8: TmmLabel
          Tag = 10
          Left = 250
          Top = 70
          Width = 470
          Height = 24
          AutoSize = False
          Caption = '(*)MinValue = %d, MaxValue = %d'
          Color = clBtnFace
          ParentColor = False
          Visible = True
          WordWrap = True
          AutoLabel.LabelPosition = lpLeft
        end
        object SetupLabel4: TSetupLabel
          Left = 20
          Top = 20
          Width = 700
          Height = 35
          Hint = '(*)DelShiftByTime Event'
          AutoSize = False
          Caption = 
            '(*)Loescht Daten aus Tabellen, die aelter sind als die eingestel' +
            'lten Tage.'
          Color = clInfoBk
          Constraints.MinWidth = 30
          ParentColor = False
          WordWrap = True
          ValueName = 'RegDaysForDelShift'
          SetupModul = MMSetupModul1
        end
        object mmLabel4: TmmLabel
          Left = 95
          Top = 69
          Width = 70
          Height = 13
          Caption = '(*)Anzahl Tage'
          FocusControl = sseDelShiftByTime
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object sseDelShiftByTime: TSetupSpinEdit
          Left = 20
          Top = 65
          Width = 65
          Height = 22
          Hint = '(*)DelShiftByTime Event'
          MaxValue = 400
          MinValue = 60
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Value = 60
          Visible = True
          AutoLabel.Control = mmLabel4
          AutoLabel.Distance = 10
          AutoLabel.LabelPosition = lpRight
          ValueName = 'RegDaysForDelShift'
          SetupModul = MMSetupModul1
        end
      end
    end
    object tsMMGuard: TTabSheet
      Caption = '(*)MMGuard'
      ImageIndex = 1
      object mmGroupBox5: TmmGroupBox
        Left = 20
        Top = 10
        Width = 362
        Height = 100
        Caption = 'Error Reset Time'
        TabOrder = 0
        TabStop = True
        CaptionSpace = True
        object mmLabel9: TmmLabel
          Left = 95
          Top = 69
          Width = 30
          Height = 13
          Caption = '(*)Min.'
          FocusControl = SetupSpinEdit5
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object SetupLabel5: TSetupLabel
          Left = 20
          Top = 20
          Width = 320
          Height = 35
          Hint = '(*)Fehlerreset Zeit'
          AutoSize = False
          Caption = 
            '(*)Nach dieser Zeit vergisst das System die vorangegangenen Rest' +
            'arts.'
          Color = clInfoBk
          Constraints.MinWidth = 30
          ParentColor = False
          WordWrap = True
          ValueName = 'RegErrResetTime'
          SetupModul = MMSetupModul1
        end
        object mmLabel21: TmmLabel
          Left = 150
          Top = 70
          Width = 159
          Height = 13
          Caption = '(*)MinValue = %d, MaxValue = %d'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object SetupSpinEdit5: TSetupSpinEdit
          Left = 20
          Top = 65
          Width = 65
          Height = 22
          Hint = '(*)Fehlerreset Zeit'
          MaxValue = 600
          MinValue = 10
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Value = 10
          Visible = True
          AutoLabel.Control = mmLabel9
          AutoLabel.Distance = 10
          AutoLabel.LabelPosition = lpRight
          ValueName = 'RegErrResetTime'
          SetupModul = MMSetupModul1
        end
      end
      object mmGroupBox6: TmmGroupBox
        Left = 20
        Top = 125
        Width = 362
        Height = 100
        Caption = 'Alert Time'
        TabOrder = 1
        TabStop = True
        CaptionSpace = True
        object mmLabel10: TmmLabel
          Left = 95
          Top = 69
          Width = 32
          Height = 13
          Caption = '(*)Sek.'
          FocusControl = SetupSpinEdit6
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object SetupLabel6: TSetupLabel
          Left = 20
          Top = 20
          Width = 320
          Height = 35
          Hint = 
            '(*)Intervall des Alarmes falls das MillMaster nicht gestartet we' +
            'rden kann.'
          AutoSize = False
          Caption = 
            '(*)Intervall des Alarmes falls das MillMaster nicht gestartet we' +
            'rden kann.'
          Color = clInfoBk
          Constraints.MinWidth = 30
          ParentColor = False
          WordWrap = True
          ValueName = 'RegAlertTime'
          SetupModul = MMSetupModul1
        end
        object mmLabel22: TmmLabel
          Left = 150
          Top = 70
          Width = 159
          Height = 13
          Caption = '(*)MinValue = %d, MaxValue = %d'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object SetupSpinEdit6: TSetupSpinEdit
          Left = 20
          Top = 65
          Width = 65
          Height = 22
          Hint = 
            '(*)Intervall des Alarmes falls das MillMaster nicht gestartet we' +
            'rden kann.'
          MaxValue = 600
          MinValue = 10
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Value = 10
          Visible = True
          AutoLabel.Control = mmLabel10
          AutoLabel.Distance = 10
          AutoLabel.LabelPosition = lpRight
          ValueName = 'RegAlertTime'
          SetupModul = MMSetupModul1
        end
      end
      object mmGroupBox7: TmmGroupBox
        Left = 20
        Top = 240
        Width = 362
        Height = 100
        Caption = 'Retry Time'
        TabOrder = 2
        TabStop = True
        CaptionSpace = True
        object mmLabel11: TmmLabel
          Left = 95
          Top = 69
          Width = 30
          Height = 13
          Caption = '(*)Min.'
          FocusControl = SetupSpinEdit7
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object SetupLabel7: TSetupLabel
          Left = 20
          Top = 20
          Width = 320
          Height = 35
          Hint = '(*)Erneuter Startversuch bei Alarm'
          AutoSize = False
          Caption = 
            '(*)MillMaster wird nach der eingegebenen Zeit automatisch aus de' +
            'm Alarmzustand neu aufgestartet.'
          Color = clInfoBk
          Constraints.MinWidth = 30
          ParentColor = False
          WordWrap = True
          ValueName = 'RegRetrayTime'
          SetupModul = MMSetupModul1
        end
        object mmLabel23: TmmLabel
          Left = 150
          Top = 70
          Width = 159
          Height = 13
          Caption = '(*)MinValue = %d, MaxValue = %d'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object SetupSpinEdit7: TSetupSpinEdit
          Left = 20
          Top = 65
          Width = 65
          Height = 22
          Hint = '(*)Erneuter Startversuch bei Alarm'
          MaxValue = 1440
          MinValue = 10
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Value = 15
          Visible = True
          AutoLabel.Control = mmLabel11
          AutoLabel.Distance = 10
          AutoLabel.LabelPosition = lpRight
          ValueName = 'RegRetrayTime'
          SetupModul = MMSetupModul1
        end
      end
      object mmGroupBox8: TmmGroupBox
        Left = 20
        Top = 355
        Width = 362
        Height = 100
        Caption = 'Start MillMaster immediately'
        TabOrder = 3
        TabStop = True
        CaptionSpace = True
        object SetupLabel8: TSetupLabel
          Left = 20
          Top = 20
          Width = 320
          Height = 35
          Hint = 
            '(*)Automatischer Start des MillMasters beim Aufstarten des Serve' +
            'rs'
          AutoSize = False
          Caption = 
            '(*)Automatischer Start des MillMasters beim Aufstarten des Serve' +
            'rs.'
          Color = clInfoBk
          Constraints.MinWidth = 30
          ParentColor = False
          WordWrap = True
          ValueName = 'RegStartMMIm'
          SetupModul = MMSetupModul1
        end
        object SetupCheckBox1: TSetupCheckBox
          Left = 20
          Top = 65
          Width = 333
          Height = 17
          Hint = 
            '(*)Automatischer Start des MillMasters beim Aufstarten des Serve' +
            'rs'
          Caption = '(*)MillMaster Autostart (ja/nein)'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Visible = True
          AutoLabel.LabelPosition = lpRight
          ValueName = 'RegStartMMIm'
          SetupModul = MMSetupModul1
        end
      end
      object mmGroupBox9: TmmGroupBox
        Left = 400
        Top = 10
        Width = 362
        Height = 255
        Caption = 'Process'
        TabOrder = 4
        TabStop = True
        CaptionSpace = True
        object SetupLabel9: TSetupLabel
          Left = 20
          Top = 24
          Width = 320
          Height = 17
          Hint = '(*)Anzahl der Prozesse'
          AutoSize = False
          Caption = '(*)Anzahl der Prozesse.'
          Color = clInfoBk
          Constraints.MinWidth = 30
          ParentColor = False
          WordWrap = True
          ValueName = 'RegNumOfProcess'
          SetupModul = MMSetupModul1
        end
        object sgProcesses: TmmExtStringGrid
          Left = 20
          Top = 55
          Width = 321
          Height = 185
          ColCount = 2
          DefaultColWidth = 20
          DefaultRowHeight = 21
          RowCount = 8
          Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goEditing, goTabs]
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Visible = True
          OnKeyUp = sgProcessesKeyUp
          OnMouseUp = sgProcessesMouseUp
          OnSelectCell = sgProcessesSelectCell
          AutoNumAlign = True
          AutoSize = False
          VAlignment = vtaCenter
          EnhTextSize = False
          EnhRowColMove = False
          SortFixedCols = False
          SizeWithForm = False
          Multilinecells = False
          OnClickCell = sgProcessesClickCell
          OnRightClickCell = sgProcessesRightClickCell
          OnDblClickCell = sgProcessesDblClickCell
          OnCellValidate = sgProcessesCellValidate
          SortDirection = sdAscending
          SortFull = False
          SortAutoFormat = True
          SortShow = False
          EnableGraphics = False
          SortColumn = 0
          HintColor = clAqua
          SelectionColor = clHighlight
          SelectionTextColor = clHighlightText
          SelectionRectangle = False
          SelectionRTFKeep = False
          HintShowCells = True
          OleDropTarget = False
          OleDropSource = False
          OleDropRTF = False
          PrintSettings.FooterSize = 0
          PrintSettings.HeaderSize = 0
          PrintSettings.Time = ppNone
          PrintSettings.Date = ppNone
          PrintSettings.DateFormat = 'dd/mm/yyyy'
          PrintSettings.PageNr = ppNone
          PrintSettings.Title = ppNone
          PrintSettings.Font.Charset = DEFAULT_CHARSET
          PrintSettings.Font.Color = clWindowText
          PrintSettings.Font.Height = -11
          PrintSettings.Font.Name = 'MS Sans Serif'
          PrintSettings.Font.Style = []
          PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
          PrintSettings.HeaderFont.Color = clWindowText
          PrintSettings.HeaderFont.Height = -11
          PrintSettings.HeaderFont.Name = 'MS Sans Serif'
          PrintSettings.HeaderFont.Style = []
          PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
          PrintSettings.FooterFont.Color = clWindowText
          PrintSettings.FooterFont.Height = -11
          PrintSettings.FooterFont.Name = 'MS Sans Serif'
          PrintSettings.FooterFont.Style = []
          PrintSettings.Borders = pbNoborder
          PrintSettings.BorderStyle = psSolid
          PrintSettings.Centered = False
          PrintSettings.RepeatFixedRows = False
          PrintSettings.RepeatFixedCols = False
          PrintSettings.LeftSize = 0
          PrintSettings.RightSize = 0
          PrintSettings.ColumnSpacing = 0
          PrintSettings.RowSpacing = 0
          PrintSettings.TitleSpacing = 0
          PrintSettings.Orientation = poPortrait
          PrintSettings.FixedWidth = 0
          PrintSettings.FixedHeight = 0
          PrintSettings.UseFixedHeight = False
          PrintSettings.UseFixedWidth = False
          PrintSettings.FitToPage = fpNever
          PrintSettings.PageNumSep = '/'
          PrintSettings.NoAutoSize = False
          PrintSettings.PrintGraphics = False
          HTMLSettings.Width = 100
          Navigation.AllowInsertRow = False
          Navigation.AllowDeleteRow = False
          Navigation.AdvanceOnEnter = False
          Navigation.AdvanceInsert = False
          Navigation.AutoGotoWhenSorted = False
          Navigation.AutoGotoIncremental = False
          Navigation.AutoComboDropSize = False
          Navigation.AdvanceDirection = adLeftRight
          Navigation.AllowClipboardShortCuts = False
          Navigation.AllowSmartClipboard = False
          Navigation.AllowRTFClipboard = False
          Navigation.AdvanceAuto = False
          Navigation.InsertPosition = pInsertBefore
          Navigation.CursorWalkEditor = False
          Navigation.MoveRowOnSort = False
          Navigation.ImproveMaskSel = False
          Navigation.AlwaysEdit = False
          ColumnSize.Save = False
          ColumnSize.Stretch = True
          ColumnSize.Location = clRegistry
          CellNode.Color = clSilver
          CellNode.NodeType = cnFlat
          CellNode.NodeColor = clBlack
          SizeWhileTyping.Height = False
          SizeWhileTyping.Width = False
          MouseActions.AllSelect = False
          MouseActions.ColSelect = False
          MouseActions.RowSelect = True
          MouseActions.DirectEdit = False
          MouseActions.DisjunctRowSelect = False
          MouseActions.AllColumnSize = False
          MouseActions.CaretPositioning = False
          IntelliPan = ipVertical
          URLColor = clBlack
          URLShow = False
          URLFull = False
          URLEdit = False
          ScrollType = ssNormal
          ScrollColor = clNone
          ScrollWidth = 16
          ScrollProportional = False
          ScrollHints = shNone
          OemConvert = False
          FixedFooters = 0
          FixedRightCols = 0
          FixedColWidth = 20
          FixedRowHeight = 21
          FixedFont.Charset = DEFAULT_CHARSET
          FixedFont.Color = clWindowText
          FixedFont.Height = -11
          FixedFont.Name = 'MS Sans Serif'
          FixedFont.Style = []
          WordWrap = False
          ColumnHeaders.Strings = (
            ''
            '(*)Prozess-Pfad')
          Lookup = False
          LookupCaseSensitive = False
          LookupHistory = False
          BackGround.Top = 0
          BackGround.Left = 0
          BackGround.Display = bdTile
          Hovering = False
          Filter = <>
          FilterActive = False
          AutoLabel.LabelPosition = lpLeft
          ColWidths = (
            20
            279)
          RowHeights = (
            21
            21
            21
            21
            21
            21
            21
            21)
        end
      end
      object mmGroupBox10: TmmGroupBox
        Left = 400
        Top = 270
        Width = 362
        Height = 90
        Caption = 'Alert Sound'
        TabOrder = 5
        TabStop = True
        CaptionSpace = True
        object SetupLabel10: TSetupLabel
          Left = 20
          Top = 20
          Width = 320
          Height = 35
          Hint = 
            '(*)Soll ein Akustisches Signal ausgegeben werden, wenn das MillM' +
            'aster nicht gestartet werden kann?'
          AutoSize = False
          Caption = 
            '(*)Soll ein Akustisches Signal ausgegeben werden, wenn das MillM' +
            'aster nicht gestartet werden kann?'
          Color = clInfoBk
          Constraints.MinWidth = 30
          ParentColor = False
          WordWrap = True
          ValueName = 'RegAlertSound'
          SetupModul = MMSetupModul1
        end
        object SetupCheckBox2: TSetupCheckBox
          Left = 20
          Top = 65
          Width = 320
          Height = 17
          Hint = 
            '(*)Soll ein Akustisches Signal ausgegeben werden, wenn das MillM' +
            'aster nicht gestartet werden kann?'
          Caption = '(*)Alarmklang (ja/nein)'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Visible = True
          AutoLabel.LabelPosition = lpLeft
          ValueName = 'RegAlertSound'
          SetupModul = MMSetupModul1
        end
      end
      object mmGroupBox11: TmmGroupBox
        Left = 400
        Top = 365
        Width = 362
        Height = 90
        Caption = 'SQL-Server restart'
        TabOrder = 6
        CaptionSpace = True
        object SetupLabel11: TSetupLabel
          Left = 16
          Top = 20
          Width = 320
          Height = 33
          Hint = '(*)MM Primary Domain Controller'
          AutoSize = False
          Caption = '(*)MM Primary Domain Controller'
          Color = clInfoBk
          Constraints.MinWidth = 30
          ParentColor = False
          WordWrap = True
          ValueName = 'RegDomainController'
          SetupModul = MMSetupModul1
        end
        object SetupCheckBox3: TSetupCheckBox
          Left = 16
          Top = 65
          Width = 320
          Height = 17
          Hint = 
            '(*)Soll der SQL-Server beim Neustart des MillMasters ebenfalls n' +
            'eu gestartet werden?'
          Caption = '(*)Neustart von SQL-Server'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Visible = True
          AutoLabel.LabelPosition = lpLeft
          ValueName = 'RegRebootSQLServer'
          SetupModul = MMSetupModul1
        end
      end
    end
    object tsUserAdmin: TTabSheet
      Caption = '(*)MM Security'
      ImageIndex = 3
      OnEnter = tsUserAdminEnter
      OnExit = tsUserAdminExit
      object mmPanel5: TmmPanel
        Left = 0
        Top = 0
        Width = 813
        Height = 40
        Align = alTop
        BorderWidth = 1
        Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        object laDomain: TmmLabel
          Left = 83
          Top = 12
          Width = 8
          Height = 13
          Caption = '0'
          FocusControl = lbDomainWorkGroup
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laDomainServerName: TmmLabel
          Left = 691
          Top = 12
          Width = 8
          Height = 13
          Caption = '0'
          FocusControl = laDomainServerNameLabel
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = True
          AutoLabel.Distance = 10
          AutoLabel.LabelPosition = lpLeft
        end
        object lbDomainWorkGroup: TmmLabel
          Left = 10
          Top = 6
          Width = 63
          Height = 26
          Caption = 'Domain / Workgroup'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = True
          WordWrap = True
          AutoLabel.Control = laDomain
          AutoLabel.Distance = 10
          AutoLabel.LabelPosition = lpRight
        end
        object laDomainServerNameLabel: TmmLabel
          Left = 530
          Top = 12
          Width = 156
          Height = 13
          Caption = 'Name of Domain Controller:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = True
          AutoLabel.Control = laDomainServerName
          AutoLabel.Distance = 5
          AutoLabel.LabelPosition = lpRight
        end
        object lbPCNameLabel: TmmLabel
          Left = 250
          Top = 5
          Width = 101
          Height = 13
          Caption = '(*)Computername:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = True
          AutoLabel.Control = lbPCName
          AutoLabel.Distance = 5
          AutoLabel.LabelPosition = lpRight
        end
        object lbPCName: TmmLabel
          Left = 356
          Top = 5
          Width = 8
          Height = 13
          Caption = '0'
          FocusControl = lbPCNameLabel
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object lbTypeLabel: TmmLabel
          Left = 250
          Top = 20
          Width = 39
          Height = 13
          Caption = '(*)Typ:'
          Visible = True
          AutoLabel.Control = lbType
          AutoLabel.Distance = 5
          AutoLabel.LabelPosition = lpRight
        end
        object lbType: TmmLabel
          Left = 294
          Top = 20
          Width = 8
          Height = 13
          Caption = '0'
          FocusControl = lbTypeLabel
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
      end
      object ToolBar1: TToolBar
        Left = 0
        Top = 40
        Width = 813
        Height = 33
        Caption = 'ToolBar1'
        Images = ImageList16x16
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        object tbPrintSecurity: TToolButton
          Left = 0
          Top = 2
          Action = acPrintSecurity
        end
        object ToolButton7: TToolButton
          Left = 23
          Top = 2
          Width = 23
          ImageIndex = 5
          Style = tbsDivider
        end
        object tbLoadSecurityDef: TToolButton
          Left = 46
          Top = 2
          Action = acLoadDefault
        end
        object tbLoadSecurityBackup: TToolButton
          Left = 69
          Top = 2
          Action = acLoadBackup
        end
        object ToolButton3: TToolButton
          Left = 92
          Top = 2
          Width = 23
          ImageIndex = 2
          Style = tbsDivider
        end
        object tbSaveSecurity: TToolButton
          Left = 115
          Top = 2
          Action = acSaveSecurity
        end
        object tbSaveSecurityBackup: TToolButton
          Left = 138
          Top = 2
          Action = acSaveSecurityBackup
        end
        object tbLoadSecurityCustom: TToolButton
          Left = 161
          Top = 2
          Action = acLoadWork
        end
        object ToolButton8: TToolButton
          Left = 184
          Top = 2
          Width = 23
          ImageIndex = 5
          Style = tbsDivider
        end
        object tbAddUserGroup: TToolButton
          Left = 207
          Top = 2
          Action = acAddUsergroup
        end
        object tbShowUserManage: TToolButton
          Left = 230
          Top = 2
          Action = acShowUserManager
        end
      end
      object pcSecurity: TmmPageControl
        Left = 0
        Top = 73
        Width = 813
        Height = 401
        ActivePage = tsSecurityAppl
        Align = alClient
        TabOrder = 2
        OnChange = pcSecurityChange
        object tsSecurityAppl: TTabSheet
          Caption = '(*)Sicherheitseinstellung nach MM Programmen'
          OnEnter = tsSecurityApplEnter
        end
        object tsSecurityGroup: TTabSheet
          Caption = '(*)Sicherheitseinstellung nach MM Gruppen'
          ImageIndex = 1
          OnContextPopup = tsSecurityGroupContextPopup
          OnEnter = tsSecurityGroupEnter
        end
      end
    end
    object tsFileVersion: TTabSheet
      Caption = '(*)Datei Version'
      ImageIndex = 4
      OnEnter = tsFileVersionEnter
      object mmBevel4: TmmBevel
        Left = 0
        Top = 0
        Width = 813
        Height = 30
        Align = alTop
      end
      object laDataBaseVers: TmmLabel
        Left = 12
        Top = 8
        Width = 98
        Height = 13
        Caption = '(*)DataBase Version:'
        Visible = True
        AutoLabel.Distance = 60
        AutoLabel.LabelPosition = lpRight
      end
      object lDBVersValue: TmmDBText
        Left = 150
        Top = 8
        Width = 65
        Height = 17
        DataField = 'Data'
        DataSource = dsDBVers
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laAdo: TmmLabel
        Left = 656
        Top = 8
        Width = 74
        Height = 13
        Anchors = [akTop, akRight]
        Caption = '(*)ADO Version:'
        FocusControl = laADOVers
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laADOVers: TmmLabel
        Left = 750
        Top = 8
        Width = 6
        Height = 13
        Anchors = [akTop, akRight]
        Caption = '0'
        Visible = True
        AutoLabel.Control = laAdo
        AutoLabel.Distance = 20
        AutoLabel.LabelPosition = lpLeft
      end
      object sgFileVersion: TmmExtStringGrid
        Left = 0
        Top = 40
        Width = 813
        Height = 434
        Align = alClient
        Color = clInfoBk
        ColCount = 3
        DefaultRowHeight = 21
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goColSizing, goRowSelect]
        ParentShowHint = False
        ShowHint = False
        TabOrder = 0
        Visible = True
        AutoNumAlign = False
        AutoSize = False
        VAlignment = vtaTop
        EnhTextSize = False
        EnhRowColMove = False
        SortFixedCols = False
        SizeWithForm = False
        Multilinecells = False
        SortDirection = sdAscending
        SortFull = True
        SortAutoFormat = True
        SortShow = False
        EnableGraphics = False
        SortColumn = 0
        HintColor = clYellow
        SelectionColor = clHighlight
        SelectionTextColor = clCaptionText
        SelectionRectangle = False
        SelectionRTFKeep = False
        HintShowCells = False
        OleDropTarget = False
        OleDropSource = False
        OleDropRTF = False
        PrintSettings.FooterSize = 0
        PrintSettings.HeaderSize = 0
        PrintSettings.Time = ppNone
        PrintSettings.Date = ppNone
        PrintSettings.DateFormat = 'dd/mm/yyyy'
        PrintSettings.PageNr = ppNone
        PrintSettings.Title = ppNone
        PrintSettings.Font.Charset = DEFAULT_CHARSET
        PrintSettings.Font.Color = clWindowText
        PrintSettings.Font.Height = -11
        PrintSettings.Font.Name = 'MS Sans Serif'
        PrintSettings.Font.Style = []
        PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
        PrintSettings.HeaderFont.Color = clWindowText
        PrintSettings.HeaderFont.Height = -11
        PrintSettings.HeaderFont.Name = 'MS Sans Serif'
        PrintSettings.HeaderFont.Style = []
        PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
        PrintSettings.FooterFont.Color = clWindowText
        PrintSettings.FooterFont.Height = -11
        PrintSettings.FooterFont.Name = 'MS Sans Serif'
        PrintSettings.FooterFont.Style = []
        PrintSettings.Borders = pbNoborder
        PrintSettings.BorderStyle = psSolid
        PrintSettings.Centered = False
        PrintSettings.RepeatFixedRows = False
        PrintSettings.RepeatFixedCols = False
        PrintSettings.LeftSize = 0
        PrintSettings.RightSize = 0
        PrintSettings.ColumnSpacing = 0
        PrintSettings.RowSpacing = 0
        PrintSettings.TitleSpacing = 0
        PrintSettings.Orientation = poPortrait
        PrintSettings.FixedWidth = 0
        PrintSettings.FixedHeight = 0
        PrintSettings.UseFixedHeight = False
        PrintSettings.UseFixedWidth = False
        PrintSettings.FitToPage = fpNever
        PrintSettings.PageNumSep = '/'
        PrintSettings.NoAutoSize = False
        PrintSettings.PrintGraphics = False
        HTMLSettings.Width = 100
        Navigation.AllowInsertRow = False
        Navigation.AllowDeleteRow = False
        Navigation.AdvanceOnEnter = False
        Navigation.AdvanceInsert = False
        Navigation.AutoGotoWhenSorted = False
        Navigation.AutoGotoIncremental = False
        Navigation.AutoComboDropSize = False
        Navigation.AdvanceDirection = adLeftRight
        Navigation.AllowClipboardShortCuts = False
        Navigation.AllowSmartClipboard = False
        Navigation.AllowRTFClipboard = False
        Navigation.AdvanceAuto = False
        Navigation.InsertPosition = pInsertBefore
        Navigation.CursorWalkEditor = False
        Navigation.MoveRowOnSort = False
        Navigation.ImproveMaskSel = False
        Navigation.AlwaysEdit = False
        ColumnSize.Save = False
        ColumnSize.Stretch = True
        ColumnSize.Location = clRegistry
        CellNode.Color = clSilver
        CellNode.NodeType = cnFlat
        CellNode.NodeColor = clBlack
        SizeWhileTyping.Height = False
        SizeWhileTyping.Width = False
        MouseActions.AllSelect = False
        MouseActions.ColSelect = False
        MouseActions.RowSelect = False
        MouseActions.DirectEdit = False
        MouseActions.DisjunctRowSelect = False
        MouseActions.AllColumnSize = False
        MouseActions.CaretPositioning = False
        IntelliPan = ipVertical
        URLColor = clBlack
        URLShow = False
        URLFull = False
        URLEdit = False
        ScrollType = ssNormal
        ScrollColor = clNone
        ScrollWidth = 16
        ScrollProportional = False
        ScrollHints = shNone
        OemConvert = False
        FixedFooters = 0
        FixedRightCols = 0
        FixedColWidth = 507
        FixedRowHeight = 21
        FixedFont.Charset = DEFAULT_CHARSET
        FixedFont.Color = clWindowText
        FixedFont.Height = -11
        FixedFont.Name = 'MS Sans Serif'
        FixedFont.Style = []
        WordWrap = False
        ColumnHeaders.Strings = (
          '(*)Datei'
          '(*)Version'
          '(*)Dateigroesse')
        Lookup = False
        LookupCaseSensitive = False
        LookupHistory = False
        BackGround.Top = 0
        BackGround.Left = 0
        BackGround.Display = bdTile
        Hovering = False
        Filter = <>
        FilterActive = False
        AutoLabel.LabelPosition = lpLeft
        ColWidths = (
          507
          125
          158)
        RowHeights = (
          21
          21)
      end
      object pFileVersionHeader: TPanel
        Left = 0
        Top = 30
        Width = 813
        Height = 10
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
      end
    end
  end
  object fcStatusBar1: TfcStatusBar [3]
    Left = 0
    Top = 553
    Width = 821
    Height = 20
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Panels = <
      item
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Name = 'stpSecurity'
        Tag = 0
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaVCenter
        Width = '50'
      end>
    ParentFont = False
    SimplePanel = False
    StatusBarText.CapsLock = 'Caps'
    StatusBarText.Overwrite = 'Overwrite'
    StatusBarText.NumLock = 'Num'
    StatusBarText.ScrollLock = 'Scroll'
  end
  inherited ImageList16x16: TmmImageList
    Left = 408
    Top = 0
    Bitmap = {
      494C010129002C00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000B0000000010020000000000000B0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000008400
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000008400000084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000000000
      0000000000000000000000000000000000000000000000000000840000008400
      0000840000008400000084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000000000
      0000000000000000000000000000000000000000000000000000840000008400
      0000840000008400000084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000008400
      0000000000000000000000000000000000008400000084000000840000000000
      0000000000008400000084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000084000000840000008400000084000000000000000000
      0000000000000000000084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C6000000000000FFFF0000FFFF0000FFFF00000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C6C60000FFFF0000000000848484008484840084848400848484008484
      8400848484008484840084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C6C6000000000000FFFF0000FFFF0000FFFF000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000848400C6C6
      C60000FFFF00FFFFFF0000000000848484008484840084848400848484008484
      8400848484008484840084848400000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000000
      000000000000000000000000000000000000000000008484840000848400C6C6
      C60000FFFF000000000000FFFF0000FFFF000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000000
      00000000000000000000000000000000000000000000848484000084840000FF
      FF00FFFFFF0000FFFF000000000000000000FFFFFF0000000000848484008484
      8400848484008484840084848400000000000000000000FFFF00000000000084
      8400008484000084840000848400008484000084840000848400008484000084
      84000000000000000000000000000000000000000000848484000084840000FF
      FF00FFFFFF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF00000000000084
      8400008484000084840000848400008484000084840000848400008484000084
      84000000000000000000000000000000000000000000848484000084840000FF
      FF0000FFFF00FFFFFF0000000000000000000000000000000000000000000000
      00008484000084840000848400000000000000000000FFFFFF00848484000000
      0000008484000084840000848400008484000084840000848400008484000084
      84000084840000000000000000000000000000000000848484000084840000FF
      FF0000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000000000FFFFFF00848484000000
      0000008484000084840000848400008484000084840000848400008484000084
      84000084840000000000000000000000000000000000848484000084840000FF
      FF00FFFFFF0000FFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084840000FFFF000084840000000000000000000000FFFF00848484000084
      8400000000000084840000848400008484000084840000848400008484000084
      84000084840000848400000000000000000000000000848484000084840000FF
      FF00FFFFFF000000000000FFFF0000FFFF0000FFFF00000000000000000000FF
      FF0000FFFF0000FFFF0000000000000000000000000000FFFF00848484000084
      8400000000000084840000848400008484000084840000848400008484000084
      84000084840000848400000000000000000000000000848484000084840000FF
      FF0000FFFF00FFFFFF0000000000000000000000000000000000000000008484
      0000FFFF0000FFFF0000848400000000000000000000FFFFFF00848484000084
      840000FFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848484000084840000FF
      FF0000FFFF00FFFFFF000000000000FFFF0000FFFF00000000000000000000FF
      FF0000FFFF0000FFFF00000000000000000000000000FFFFFF00848484000084
      840000FFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848484000084840000FF
      FF00FFFFFF0000FFFF0084000000000000000000000000000000000000008484
      0000FFFF0000FFFF000084840000000000000000000000FFFF00848484000084
      840000FFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF000000000000000000000000000000000000000000848484000084840000FF
      FF00FFFFFF00FFFFFF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF00848484000084
      840000FFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF000000000000000000000000000000000000000000848484000084840000FF
      FF0000FFFF00FFFFFF0000000000FFFF00000000000000000000000000008484
      0000FFFF0000FFFF0000848400000000000000000000FFFFFF00848484000084
      840000FFFF000000FF000000FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
      FF000000000000000000000000000000000000000000848484000084840000FF
      FF0000FFFF00FFFFFF0000FFFF000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000000000FFFFFF00848484000084
      8400008484000000FF000000FF00008484000084840000FFFF0000FFFF00FFFF
      FF000000000000000000000000000000000000000000848484000084840000FF
      FF00FFFFFF0000000000FFFF0000FFFF00000000000084840000848400008484
      0000FFFF0000FFFF000084840000000000000000000000000000848484000084
      840000FFFF000000FF000000FF000000FF0000FFFF0000FFFF000000FF000000
      FF000000FF000000FF000000FF000000000000000000848484000084840000FF
      FF00FFFFFF00FFFFFF0000FFFF00FFFFFF00000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000848484000084
      8400008484000000FF000000FF000000FF000084840000FFFF000000FF000000
      FF000000FF000000FF000000FF000000000000000000848484000084840000FF
      FF0000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF000084840000000000000000000000000000848484000084
      8400FFFFFF0000FFFF000000FF000000FF0000FFFF00FFFFFF0000FFFF000000
      FF000000FF000000FF000000FF00000000000000000084848400008484000084
      84000084840000848400008484000084840000848400FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000848484000084
      8400FFFFFF0000FFFF000000FF000000FF0000FFFF00FFFFFF0000FFFF000000
      FF000000FF000000FF000000FF000000000000000000848484000084840000FF
      FF0084840000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000848400000000000000000000000000000000000084848400FFFF
      FF0000FFFF00FFFFFF000000FF000000FF000000FF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000000000000000848484000084840000FF
      FF0000FFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000848400008484000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF0000FFFF00FFFFFF000000FF000000FF000000FF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF00000000000000000084848400FFFFFF0000FF
      FF00FFFFFF0084840000FFFF0000FFFF00008400000084840000848400008484
      0000848400000000000000000000000000000000000000000000000000008484
      8400FFFFFF0000FFFF00FFFFFF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000084848400FFFFFF0000FF
      FF00FFFFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400FFFFFF0000FFFF00FFFFFF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000000000000000000084848400FFFF
      FF0000FFFF00FFFFFF0084840000FFFF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484008484840084848400848484000000FF000000FF000000FF000000
      FF0000000000000000000000FF0000000000000000000000000084848400FFFF
      FF0000FFFF00FFFFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484008484840084848400848484000000FF000000FF000000FF000000
      FF0000000000000000000000FF00000000000000000000000000000000000084
      8400008484000084840000848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484008484840084848400848484008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000084840000848400008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400008484000084840000848400000000008484840084848400848484008484
      8400848484008484840084848400848484000084840000848400000000008484
      8400848484000000000000000000000000008484840084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000848400FFFFFF00FFFFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000848400008484000000000084848400FFFFFF00C6C6C60000FF
      0000C6C6C600C6C6C6008400000000FFFF00FFFFFF0000848400000000008400
      00008400000084000000840000000000000084848400FFFFFF00C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600848484008484840000000000000000000000000000000000000000000000
      0000C6C6C60000FFFF0000000000848484008484840084848400848484008484
      84008484840084848400848484000000000000848400FFFFFF00FFFFFF000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400C6C6C60000848400008484000000000084848400FFFFFF00C6C6C600C6C6
      C600C6C6C6008400000000FFFF00FFFFFF00008484000000000084000000C6C6
      C60084000000FF000000840000008400000084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00840000008400000084000000840000008400000084000000FFFF
      FF0084848400848484000000000000000000000000008484840000848400C6C6
      C60000FFFF00FFFFFF0000000000848484008484840084848400848484008484
      840084848400FFFF0000848484000000000000848400FFFFFF00FFFFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000848400008484000000000084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000FFFF00FFFFFF000084840000000000C6C6C600FFFFFF00C6C6
      C600848484008400000084000000000000000000000000000000000000000000
      00008400000084000000FF000000FF0000008400000084000000840000008400
      00008400000084000000000000000000000000000000848484000084840000FF
      FF00FFFFFF0000FFFF000000000000000000FFFFFF0000000000848484008484
      8400FFFF0000FFFF0000FFFF00000000000000848400FFFFFF00FFFFFF000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400C6C6C6000084840000848400000000000000000000000000000000000000
      000000FFFF00FFFFFF00008484000000000084848400FFFFFF00FFFFFF008484
      8400000000008400000000000000000000008484840084848400848484008484
      8400848484008400000084000000C6C6C600FFFFFF0084848400840000008400
      00008400000084000000840000000000000000000000848484000084840000FF
      FF0000FFFF00FFFFFF000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF000000848400FFFFFF00FFFFFF000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400C6C6C60000848400008484000000000000000000848484008484840000FF
      FF00FFFFFF0000848400000000000000000000000000FFFFFF00C6C6C600C6C6
      C600C6C6C600C6C6C600000000000000000084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000084848400FFFFFF00C6C6C6000000000084000000FFFF
      FF008484840084000000840000000000000000000000848484000084840000FF
      FF00FFFFFF0000FFFF0000000000FFFFFF00FFFFFF00FFFFFF0084840000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF000000848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000084840000848400000000000000000084848400000084008484
      8400008484000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000084848400FFFFFF0084000000FF00
      0000000000000000000000000000C6C6C600FFFFFF00FFFFFF0000000000C6C6
      C6000000000084000000000000000000000000000000848484000084840000FF
      FF0000FFFF00FFFFFF0000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF000000848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000084840000848400000000000000000000008400000000000000
      8400000000000000000000000000000000000000000084848400FFFFFF00FFFF
      FF00FFFFFF00C6C6C600000000000000000084848400FFFFFF0084000000FF00
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF0000000000000000000000000000000000848484000084840000FF
      FF00FFFFFF0000FFFF00FFFFFF00FFFFFF0000FFFF0000000000000000008484
      0000FFFF0000FFFF000084840000848400000000000000000000000000000084
      8400000000000084840000848400008484000084840000848400008484000084
      8400000000000084840000848400000000000000840000000000000084000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C6C6000000000084848400FFFFFF0084000000FF00
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      8400FFFFFF00FFFFFF00000000000000000000000000848484000084840000FF
      FF0000FFFF00FFFFFF00FFFFFF0000FFFF00FFFFFF0000000000000000008484
      0000FFFF0000FFFF000084840000000000000000000000000000000000000084
      8400000000000084840000848400008484000084840000848400008484000084
      840000000000008484000084840000000000000000000000840000000000C6C6
      C600000000000000000000000000000000000000000084848400FFFFFF00FFFF
      FF00C6C6C600FF000000C6C6C6000000000084848400FFFFFF0084000000FF00
      000000000000000000000000000000000000C6C6C600FFFFFF00840000000000
      0000FFFFFF00FFFFFF00848484000000000000000000848484000084840000FF
      FF00FFFFFF0000FFFF0084840000848400008484000084840000848400008484
      0000FFFF0000FFFF000084840000000000000000000000000000000000000084
      8400000000000000000000000000000000000000000000000000000000000084
      8400000000000000000000000000000000000000000084848400FFFFFF00C6C6
      C60000000000000000000000000000000000000000000000000000000000C6C6
      C600FFFFFF0084848400000000000000000084848400FFFFFF00840000008400
      00000000000000000000000000000000000000000000C6C6C600FFFFFF000000
      0000FFFFFF0084000000000000000000000000000000848484000084840000FF
      FF00FFFFFF0000FFFF0084840000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF000084840000000000000000000000000000000000000084
      8400000000000000000000000000000000000000000000000000000000000084
      8400000000000000000000000000000000000000000084848400FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00C6C6C6000000000084848400FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000848484000000
      000000000000FFFFFF000000000000000000000000008484840000848400FFFF
      FF0000FFFF00FFFFFF0084840000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF00008484000000000000000000000000000000000000000000000084
      8400C6C6C6000000000000000000000000000000000000848400008484000084
      840000000000000000000000000000000000000000000000000084848400C6C6
      C600C6C6C600C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400C6C6C600C6C6
      C600C6C6C6000000000000000000000000000000000000000000000000000000
      0000000000008484840000000000000000000000000084848400FFFFFF0000FF
      FF00FFFFFF00FFFFFF00FFFFFF00848400008400000084840000848400008484
      0000848400000000000000000000000000000000000000000000000000000000
      000000848400C6C6C600C6C6C600000000000000000000848400008484000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484008484840084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF0000FFFF00FFFFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000848400008484000084840000848400008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      8400008484000084840000848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C6C6008484840084848400848484008484
      8400000000000000000000000000000000000000000000000000C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C60000000000848484000000000000000000000000000084
      84000084840000000000000000000000000000000000C6C6C600C6C6C6000000
      0000008484000084840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C6C6008484840000000000FFFFFF0084848400C6C6C60084848400C6C6
      C600000000000000000000000000000000000000000000000000FFFFFF0000FF
      000000FF0000FFFFFF00C6C6C600000000000000000000000000000000000000
      0000C6C6C600C6C6C60000000000848484000000000000000000000000000084
      84000084840000000000000000000000000000000000C6C6C600C6C6C6000000
      0000008484000084840000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000084848400C6C6C60000000000FFFFFF0084848400C6C6C60084848400C6C6
      C600000000000000000000000000000000000000000000000000C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600848484008484840084848400848484008484
      8400C6C6C600C6C6C60000000000848484000000000000000000000000000084
      8400008484000000000000000000000000000000000000000000000000000000
      0000008484000084840000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000C6C6C6008484840000000000FFFFFF0084848400C6C6C60084848400C6C6
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484000000000000000000000000000084
      84000084840000848400008484000084840000848400FFFF0000008484000084
      8400008484000084840000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600C6C6C600C6C6C600C6C6C60084848400C6C6
      C600C6C6C600C6C6C600C6C6C600000000000000000000000000000000000000
      000084848400C6C6C60000000000FFFFFF0084848400C6C6C60084848400C6C6
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C60000000000848484000000000000000000000000000084
      840000000000000000000000000000000000FFFF0000FFFF0000FFFF00000000
      0000000000000084840000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600000000000000000000000000848484000000
      00000000000000000000C6C6C600000000000000000000000000000000000000
      0000C6C6C600C6C6C60000000000FFFFFF0084848400C6C6C60084848400C6C6
      C600000000000000000000000000000000000000000000000000848484000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000084
      840000000000C6C6C60000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000000000000084840000000000000000000000000000000000000000000000
      0000008484000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000C6C6C6008484840000000000FFFFFF00C6C6C600C6C6C600C6C6C600C6C6
      C600000000000000000000000000000000000000000000000000C6C6C6000000
      0000840000008400000084000000840000008400000084000000840000008400
      0000000000008484840000000000000000000000000000000000000000000084
      84000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF000000000000000000000000000000000000000000000000000000FF
      FF000084840000FFFF0000000000000000008484840000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000C6C6C6008484840000000000FFFFFF00008400000000000000840000C6C6
      C600000000000000000000000000000000000000000000000000848484000000
      000000000000FFFF0000FFFF0000FFFF0000FF000000FF000000FF0000000000
      000000000000C6C6C60084848400000000000000000000000000000000000084
      840000000000C6C6C600C6C6C600C6C6C600FFFF0000FFFF0000FFFF0000FFFF
      0000C6C6C600C6C6C6000000000000000000000000000000000000FFFF000000
      0000000000000000000000FFFF000000000000000000C6C6C60084848400FFFF
      FF00FFFFFF00FFFFFF00C6C6C600000000000000000000000000000000000000
      000084848400C6C6C60000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6C6
      C600000000000000000000000000000000000000000000000000C6C6C6000000
      000000000000FF000000FF000000FF000000FF000000FF000000FF0000000000
      000000000000C6C6C60084848400000000000000000000000000000000000084
      840000000000000000000000000084000000FFFF0000FFFF0000FFFF00000000
      0000000000000084840000000000000000000000000000FFFF00000000000000
      000000FFFF00000000000000000000FFFF0000000000FFFFFF00848484000000
      00000000000000000000C6C6C600000000000000000000000000000000000000
      0000C6C6C6008484840000000000FFFFFF000000FF000000FF000000FF00C6C6
      C600000000000000000000000000000000000000000000000000848484000000
      000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF000000FF0000000000
      000000000000C6C6C600C6C6C600000000000000000000000000000000000084
      840000848400008484000084840084000000FFFF000000000000FFFF00000000
      0000008484000084840000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000000000FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000084848400C6C6C60000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000C6C6C6000000
      000000000000FF000000FF000000FF000000FF000000FF000000FF0000000000
      000000000000C6C6C60084848400000000000000000000000000000000000000
      0000000000000000000084000000000000000000000000000000848484000000
      0000000000000000000000000000000000000000000000FFFF00848484000000
      000000FFFF00000000008484840000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C6C6008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C600C6C6C600000000000000000000000000000000000000
      00000000000084000000000000000000000000000000FFFF0000840000008400
      0000000000000000000000000000000000000000000000FFFF00000000000000
      000000FFFF00000000000000000000FFFF000000000084848400848484008484
      8400848484008484840084848400848484000000000000000000000000000000
      0000C6C6C60000000000C6C6C600FFFFFF00C6C6C600FFFFFF00C6C6C6000000
      0000000000000000000000000000000000000000000000000000C6C6C6000000
      000084848400FFFFFF0084848400C6C6C60084848400C6C6C60084848400FFFF
      FF0084848400C6C6C60084848400000000000000000084000000840000008400
      00008400000084000000FF00000000000000FFFFFF0084000000840000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      000000FFFF000000000000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840000000000000000000000000000000000840000008484
      84008484840084848400FFFFFF00FFFFFF008400000084000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
      00000000000000000000000000000000000084840000FFFF0000FFFF00008484
      0000FFFF00000000000084840000000000000000000084008400008484000000
      0000FF00FF000000000084008400000000000000000000000000000000000000
      0000008484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000084840000000000000000000000000000000000C6C6C600C6C6C6000084
      840000000000000000000000000000000000000000000000000000000000FFFF
      FF0000000000FFFFFF00FFFFFF0084848400FFFFFF00FFFFFF00000000000000
      000000000000000000000000000000000000848400008484000084840000FFFF
      0000FFFF00000000000000000000FFFF00000000000000FFFF00008484008484
      8400000000000000000000000000000000000000000000000000000000000084
      8400008484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000084840000000000000000000000000000000000C6C6C600C6C6C6000084
      8400000000000000000000000000000000000000000000000000000000008484
      840084848400FFFFFF00FFFFFF00FFFFFF008484840084848400000000000084
      840000000000000000000000000000000000000000000000000084840000FFFF
      0000000000008484840084848400000000008484840000FFFF00C6C6C600FFFF
      FF00000000000000000000000000000000000000000000000000008484000084
      8400008484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008484000000000000000000000000000000000000000000000000000084
      840000000000000000000000000000000000000000000000000000000000FFFF
      FF008484840084848400FFFFFF00FFFFFF00FFFFFF008484840000000000FFFF
      FF00000000000000000000000000000000000000000000000000848400000000
      000084848400848484008484840000000000C6C6C60000848400848484000000
      0000000000000000000000000000000000000000000000000000008484000084
      84000084840000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008484000000000000000000000000000000000000000000000000000084
      840000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000008484
      8400848484008484840084848400848484008484840084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000008484000084
      84000000000000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      000000848400C6C6C600C6C6C600C6C6C60000000000C6C6C600C6C6C6000084
      840000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF0084848400FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000FFFF00000000000000000000000000000000000000
      0000848484008484840084848400848484008484840000000000FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000008484000084
      84000084840000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000840000008400000000000000000000000000000000000000000000000000
      000000848400C6C6C600C6C6C600FFFF0000FFFF000000000000C6C6C6000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484008484840084848400848484008484840000000000FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000008484000084
      84000084840000000000FFFFFF00FFFFFF000000000000000000000000008400
      0000840000008400000084000000840000000000000000000000000000000000
      000000848400C6C6C60000000000FFFF0000FFFF0000FFFF0000000000000084
      8400000000000000000000000000000000000000000000000000000000000000
      00008484840084848400848484008484840084848400FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484008484840084848400848484008484840000000000C6C6C600FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000008484000084
      84000084840000000000FFFFFF00FFFFFF000000000000000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000008484008484840084848400FFFF0000FFFF0000FFFF0000848484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000848484008484840084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000848484008484840084848400000000008484840000000000FFFFFF00FFFF
      FF00000000008484840000000000000000000000000000000000008484000084
      84000084840000000000FFFFFF00FFFFFF000000000000000000000000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000FFFF0000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008484840084848400848484008484840084848400FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840084848400000000000000000000FFFF00C6C6C600FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000008484000084
      840000000000FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840084848400848484000000000000FFFF0000FFFF000084
      8400008484000084840000FFFF00000000000000000000000000008484000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      00000000000084000000FF000000FFFF0000FFFF0000FFFF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      00008400000084000000FFFFFF0084000000FF00000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF000000FF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF00000084000000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF000000FF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000084000000840000008400000084
      0000008400000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000084000000840000008400000084
      000000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000000000840000008400000084000000FF
      FF00008400000000FF000000FF0000FFFF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF00FFFF
      FF00FFFFFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000084000000840000008400000084
      00000000FF00FFFFFF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF00FFFFFF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000084000000840000008400000084
      00000000FF00FFFFFF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF00FFFFFF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000000000000000FF000000FF000000FF000000
      FF000000FF0000FFFF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000000000840000008400000084000000FF
      FF000084000000FFFF000000FF0000FFFF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF00FFFFFF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000000000000000FF000000FF0000FFFF0000FF
      FF000000FF000000FF0000FFFF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000084000000840000008400000084
      000000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF00FFFF
      FF00FFFFFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000FF000000FF0000FFFF0000FF
      FF000000FF000000FF0000FFFF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000084000000840000008400000084
      0000008400000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000FF000000FF000000FF000000
      FF000000FF0000FFFF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000084000000840000008400000084
      0000008400000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF000000FF000000FF00
      0000FF000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF000000FF00FFFFFF00FF00
      0000FF000000FF000000FF0000000000FF00FFFFFF00FF000000FF000000FF00
      0000FF0000000000FF00FFFFFF0000000000FF000000FF000000FF000000FF00
      0000FF000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF00000000000084000000840000008400000084
      000000840000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FF000000FF000000FFFFFF000000
      FF00FFFFFF00FF000000FF0000000000FF00FFFFFF00FF000000FF000000FFFF
      FF000000FF00FF000000FF00000000000000FF000000FF000000FF000000FF00
      0000FF000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF00000000000084000000840000008400000084
      000000840000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FF000000FF000000FF000000FF00
      0000FFFFFF00FFFFFF00FF0000000000FF00FFFFFF00FFFFFF000000FF00FFFF
      FF00FF000000FF000000FF00000000000000FF000000FF000000FF000000FF00
      0000FF000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF00000000000084000000840000008400000084
      000000840000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF00C6C6C60000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FF000000FF000000FF000000FF00
      0000FF000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF00000000000084000000840000008400000084
      000000840000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF000000000000FFFF0000FFFF0000FFFF000084
      84000084840000008400FFFFFF00FFFFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FF000000FF000000FF000000FF00
      0000FF000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF00000000000084000000840000008400000084
      000000840000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF000000000000FFFF0000FFFF0000FFFF000084
      840000FFFF000000840000FFFF00FFFFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000FF000000FF000000FF000000FF00
      0000FFFFFF000000FF00FFFFFF000000FF00FFFFFF000000FF00FFFFFF00FF00
      0000FF000000FF000000FF00000000000000FF000000FF000000FF000000FF00
      0000FF000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF00000000000084000000840000008400000084
      000000840000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000FF000000FF000000FF0000000000
      FF00FFFFFF00FF000000FF0000000000FF00FFFFFF00FF000000FFFFFF000000
      FF00FFFFFF00FF000000FF00000000000000FF000000FF000000FF000000FF00
      0000FF000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF00000000000084000000840000008400000084
      000000840000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FF000000FFFFFF000000FF00FF00
      0000FF000000FF000000FF0000000000FF00FFFFFF00FF000000FF000000FF00
      0000FFFFFF000000FF00FFFFFF0000000000FF000000FF000000FF000000FF00
      0000FF000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF00000000000084000000840000008400000084
      000000840000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF00FFFFFF00FF000000FF00
      0000FF000000FF000000FF0000000000FF00FFFFFF00FF000000FF000000FF00
      0000FF000000FF000000FFFFFF0000000000FF000000FF000000FF000000FF00
      0000FF000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF00000000000084000000840000008400000084
      000000840000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000000000000000
      FF00000084000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      0000848484008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400000000000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      FF00000084000000FF0000000000008484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6000000
      0000C6C6C600C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      FF00000084000000FF0000848400008484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C600C6C6
      C600C6C6C600C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      FF00000084000000FF0000848400008484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      FF00000084000000FF0000848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF000000000084848400000000000000000084848400000000000000
      000000000000FFFFFF00000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000008484
      84000000000000000000000000008484840000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000848484000000
      0000FFFFFF000000000084848400000000000000000000000000000084000000
      84000000840000008400000084000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C600FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00848484000000000000000000000000000000
      0000000000008484840000000000000000000000000000848400008484000084
      84000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000C6C6C60000000000FF000000FF000000FF00
      00000000FF00FF000000FF000000000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00848484000000000000000000000000000000
      0000000000000000000000000000000000000084840000848400008484000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      FF000000FF000000FF0000000000000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C600FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000008484000084840000848400000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      00000000000000000000FFFFFF0000000000FFFFFF00000000000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000848400008484000084840000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000084840000848400008484000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000848400000000000000000000000000000000008484
      84000000000084848400000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF000000000000000000000000000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      84000000000084848400000000000000000000000000FFFFFF0000000000C6C6
      C600FFFFFF0000000000FFFFFF00000000000000000000000000848484000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000848484000000000000000000000000008484
      84000000000084848400000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484008400840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000008400000084000000840000008400000084000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400C6C6C60000000000C6C6C60084008400000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000008400
      000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008400
      0000840000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      000000FFFF0000FFFF0000000000000000000000000000000000000000008484
      8400C6C6C600000000000000000084848400C6C6C600C6C6C600840084000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000008400
      000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008400
      0000840000008400000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF00FFFF00000000000000000000000000000000
      000000FFFF0000FFFF0000000000000000000000000084848400C6C6C6000000
      00000000000084848400848484000000000084848400C6C6C600C6C6C6008400
      84000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000008400000084000000FFFFFF00FFFFFF00FFFFFF00840000008400
      0000840000008400000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF00FFFF00000000000000000000000000000000
      000000FFFF0000FFFF0000000000000000008400840084848400000000008484
      8400848484000000000000000000840084000000000084848400C6C6C600C6C6
      C6008400840000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFF00000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000008400000084000000FFFFFF00FFFFFF00FFFFFF00840000008400
      0000840000008400000084000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      000000FFFF0000FFFF0000000000000000008400840084848400848484000000
      000000000000840084008400840084008400840084000000000084848400C6C6
      C600C6C6C60084008400000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008400000084000000840000008400
      0000840000008400000084000000FFFFFF00FFFFFF00FFFFFF00840000008400
      00008400000084000000840000000000000000FFFF0000FFFF00000000000000
      00000000000000FFFF0000FFFF000000000000FFFF0000FFFF00FFFF00000000
      000000FFFF0000FFFF0000000000000000008400840000000000000000008400
      8400840084008400840084008400840084008400840084008400000000008484
      8400C6C6C600C6C6C600840084000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFF000000000000000000000000
      0000000000000000000000000000000000008400000084000000840000008400
      0000840000008400000084000000FFFFFF00FFFFFF00FFFFFF00840000008400
      00008400000084000000840000000000000000FFFF0000FFFF00000000000000
      00000000000000FFFF0000FFFF000000000000FFFF0000FFFF00FFFF000000FF
      FF0000FFFF0000FFFF0000000000000000008400840084848400840084008400
      8400840084008400840000FFFF00008484008400840084008400840084000000
      000084848400C6C6C600000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFF0000FFFF0000000000000000
      0000000000000000000000000000000000008400000084000000840000008400
      0000840000008400000084000000FFFFFF00FFFFFF00FFFFFF00840000008400
      00008400000084000000840000000000000000FFFF0000FFFF00000000000000
      0000000000000000000000FFFF000000000000FFFF0000FFFF00FFFF000000FF
      FF0000FFFF0000FFFF0000000000000000000000000084008400848484008400
      8400840084008400840084008400C6C6C60000FFFF0000FFFF00840084008400
      84000000000084848400000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000008400000084000000840000008400
      0000840000008400000084000000FFFFFF00FFFFFF00FFFFFF00840000008400
      00008400000084000000840000000000000000FFFF0000FFFF00000000000000
      000000FFFF0000FFFF00000000000000000000FFFF0000FFFF00FFFF00000000
      000000FFFF0000FFFF0000000000000000000000000000000000840084008484
      8400840084008400840000848400008484008400840000FFFF0000FFFF008400
      84008400840000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00840000008400
      00008400000084000000840000000000000000FFFF0000FFFF000000000000FF
      FF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF00FFFF00000000
      0000000000000000000000000000000000000000000000000000000000008400
      840084848400840084008400840000FFFF0000FFFF0000FFFF00008484008400
      84008400840084008400000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000084000000840000000000000000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000FFFF0000FFFF0000FFFF0000FFFF00FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840084008484840084008400840084008400840084008400840084008400
      84008400840000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      00008400000084000000FFFFFF00FFFFFF00FFFFFF0084000000840000008400
      00008400000084000000000000000000000000FFFF0000FFFF0000FFFF000000
      0000FFFF0000000000000000000000FFFF0000FFFF0000FFFF00FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400840084848400840084008400840084008400840084000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000008400
      00008400000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00840000008400
      00008400000000000000000000000000000000FFFF0000FFFF0000000000FFFF
      00000000000000000000000000000000000000FFFF0000FFFF00FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084008400848484008400840000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      00008400000084000000FFFFFF00FFFFFF00FFFFFF0084000000840000008400
      00000000000000000000000000000000000000000000FFFF0000000000000000
      00000000000000000000000000000000000000000000FFFF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840084000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008400000000000000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008400000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF000084840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF000084840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF000084840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000FFFFFF00FFFFFF0084000000840000008400000084000000840000008400
      00008400000084000000FFFFFF00840000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008400000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF000084840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000FFFFFF00FFFFFF0084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000FFFFFF008400000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF000084840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00840000000000000000000000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008400000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000848400000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000000000000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF00008484000000
      0000000000000000000000000000000000000000000084000000FFFFFF008400
      000084000000840000008400000084000000840000008400000084000000FFFF
      FF00840000000000000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400000000000000000000000000000000000000000000000000
      000000FFFF000084840000000000000000000000000000FFFF0000FFFF000084
      8400000000000000000000000000000000000000000084000000FFFFFF008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000000000000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008400000000000000000000000000000000000000000000000000
      000000FFFF00008484000000000000000000000000000000000000FFFF000084
      8400000000000000000000000000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00840000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008400000000000000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008400000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000848400000000000000000000FFFF0000FFFF000084
      8400000000000000000000000000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000FFFFFF008400000000000000000000000000000084000000840000008400
      00008400000084000000FFFFFF00840000008400000084000000840000008400
      0000FFFFFF008400000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00008484000000
      0000000000000000000000000000000000000000000084000000840000008400
      00008400000084000000840000008400000084000000FFFFFF00840000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008400000000000000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000FFFF00000000000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000000000FFFFFF0000FFFF000000
      0000008484000084840000848400008484000084840000848400008484000084
      8400008484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000FFFF00FFFFFF0000FF
      FF00000000000084840000848400008484000084840000848400008484000084
      8400008484000084840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      840000848400008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      000000848400008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF00000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000FFFF00FFFFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000B00000000100010000000000800500000000000000000000
      000000000000000000000000FFFFFF00FFFF000000000000FFFF000000000000
      FFFF000000000000FFFF000000000000C3FF0000000000008F81000000000000
      9FC10000000000009FE10000000000009FC10000000000008F19000000000000
      C03D000000000000E0FF000000000000FFFF000000000000FFFF000000000000
      FFFF000000000000FFFF000000000000FFFFFFFFFC00FFFFF000FFFFF000FFFF
      C000001FC000001F8000000F8000000F80000007800000078000000380000003
      8000000180000001800000008000000080610007800000078061000780000007
      80010001800000018001C0018001C0018003C001800FC0018007E001800FE001
      C07FF00DC01FF00DE07FFFFFE03FFFFF800180038007FFFF000000010003F000
      000000010001C000000000000001800000000001800180000000800100018000
      000080010001800000008001000380000000A00100038020E001400000018021
      E001800000018001E7E7800100018001E7E7800000018003E187C00080008007
      F00FE000C000C07FF81FFF01FF81E07FF8078001C001F800F0078000C001F800
      E0078000C001F800E0078000C001F800E007C000C001F800E007E000C001E000
      E007C007C001C000E007C003C0018000E007C001C0010000E007C001C0110000
      E007C001C0510000E007C001C1D10000E007C001FB8F0000E00FC001811F007F
      E01FC003C03F80FFFFFFFFFFE0FFC1FFFBFFFFFF801FFFFFF3FFF00780170000
      E3FFE007800F0003C3FFE007C00580070007E007C007C003807FE007C003C001
      807BE007C00080018073E007E00080018060E007E00300018040E007E0030003
      8060E047E00300018073FFFFE01F0001807BF83FF01F0001807FE07F007F8003
      807FF1FF007FC0FFFFFFFFFF83FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF862FF00FFFFFFFF80E0FF00FFFF
      FFFF01E0FF00FFFFFFFF01E0FF000000FFE731E100000000FF8131C100000000
      C001C181000000008000C30700000000000CFE17002300000001CC3700010000
      FD01A87700000000FF8340F700230000FFFF01E300630000FFFFC1E300C3FFFF
      FFFFC0E30107FFFFFFFFC83F03FFFFFFFFFFFE7FFFFFFC3FFFFFF83F000CE00F
      FFFFE11F0008C007F0F1860F00018003F0F0180700638001F0B0200300C30001
      1030000101EB000000000000016B000000000000002300000000800100670000
      0000C001000F00010010E001000F00010018F001000F8001001FF807005FC003
      061FFC1F003FE0070F1FFE7F007FF01FFFFFFC00FFFFFFFFFE7FFC0080038003
      FC3FFC0080038003FC3FFC0080038003FE7FE00080038003FC3FE00080038003
      FC3FE00080038003FC3FE00780038003FC1F800780038003F20F800780038003
      E107800780038003E187801F80038003E007801F80038003F00F801F80038003
      F81F801FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFC007C001
      C007001FBFEB8031C007000F00058031C00700077E318031C00700037E358001
      C007000100068001C00700007FEA8001C007001F80148FF1C007001FC00A8FF1
      C007001FE0018FF1C0078FF1E0078FF1C00FFFF9F0078FF1C01FFF75F0038FF5
      C03FFF8FF8038001FFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  inherited mmActionListMain: TmmActionList
    OnExecute = mmActionListMainExecute
    OnUpdate = mmActionListMainUpdate
    Left = 376
    Top = 0
    object acPrintSecurity: TAction [0]
      Category = 'MMSecurity'
      Hint = '(*)Einstellungen drucken'
      ImageIndex = 2
      OnExecute = acPrintSecurityExecute
    end
    object acImport: TAction [2]
      Category = 'CategoryNotUsed'
      Caption = '(*)&Import'
      Enabled = False
      Hint = '(*)Importiert eine Setupdatei'
      ImageIndex = 1
      ShortCut = 16457
      Visible = False
      OnExecute = acImportExecute
    end
    object acExport: TAction [5]
      Category = 'CategoryNotUsed'
      Caption = '(*)&Export'
      Enabled = False
      Hint = '(*)Exportiert eine Setupdatei'
      ImageIndex = 14
      ShortCut = 16453
      Visible = False
      OnExecute = acExportExecute
    end
    inherited acNew: TAction
      Category = 'CategoryNotUsed'
      ImageIndex = -1
    end
    inherited acOpen: TAction
      Category = 'CategoryNotUsed'
      Hint = ''
      ImageIndex = -1
      ShortCut = 16457
    end
    inherited acSave: TAction
      Category = 'CategoryNotUsed'
      ImageIndex = -1
    end
    inherited acSaveas: TAction
      Category = 'CategoryNotUsed'
      Hint = '(*)Speichern unte&r...'
      ImageIndex = -1
      ShortCut = 16453
    end
    inherited acPreView: TAction
      Category = 'CategoryNotUsed'
      ImageIndex = -1
    end
    inherited acPrinterSetup: TAction
      Category = 'CategoryNotUsed'
      ImageIndex = -1
    end
    object acGetValue: TAction [16]
      Category = 'Settings'
      Caption = '(*)&Benutzerwerte setzen'
      Hint = '(*)Benutzerwerte setzen'
      ImageIndex = 26
      OnExecute = acGetValueExecute
    end
    object acGetDefaults: TAction [17]
      Category = 'Settings'
      Caption = '(*)Stan&dardwerte setzen'
      Hint = '(*)Standardwerte setzen'
      ImageIndex = 31
      OnExecute = acGetDefaultsExecute
    end
    object acSaveActivePageSettings: TAction [18]
      Category = 'Settings'
      Caption = '(*)Werte &speichern'
      Hint = '(*)Werte speichern'
      ImageIndex = 3
      OnExecute = acSaveActivePageSettingsExecute
    end
    inherited acPrint: TAction
      Hint = '(*)Druckt die Seup Einstellungen'
    end
    inherited acWindowCascade: TWindowCascade
      Category = 'CategoryNotUsed'
    end
    inherited acWindowTileVertical: TWindowTileVertical
      Category = 'CategoryNotUsed'
    end
    inherited acTileHorizontally: TWindowTileHorizontal
      Category = 'CategoryNotUsed'
    end
    inherited acHelp: TAction
      Category = 'CategoryNotUsed'
    end
    object acSaveAllSettings: TAction
      Category = 'CategoryNotUsed'
      OnExecute = acSaveAllSettingsExecute
    end
    object acSettings: TAction
      Category = 'Security'
      Caption = '(*)Zugriffkontrolle'
      Hint = '(*)Zugriffkontrolle konfigurieren'
      ImageIndex = 32
      OnExecute = acSettingsExecute
    end
    object acSaveSecurity: TAction
      Category = 'MMSecurity'
      Hint = '(*)Einstellungen speichern'
      ImageIndex = 3
      OnExecute = acSaveSecurityExecute
    end
    object acSaveSecurityBackup: TAction
      Category = 'MMSecurity'
      Hint = '(*)Backup erstellen'
      ImageIndex = 35
      OnExecute = acSaveSecurityBackupExecute
    end
    object acLoadWork: TAction
      Category = 'MMSecurity'
      Enabled = False
      Hint = '(*)Widerrufen'
      ImageIndex = 40
      OnExecute = acLoadWorkExecute
    end
    object acLoadDefault: TAction
      Category = 'MMSecurity'
      Hint = '(*)Standard laden'
      ImageIndex = 38
      OnExecute = acLoadDefaultExecute
    end
    object acLoadBackup: TAction
      Category = 'MMSecurity'
      Hint = '(*)Backup laden'
      ImageIndex = 36
      OnExecute = acLoadBackupExecute
    end
    object acShowUserManager: TAction
      Category = 'MMSecurity'
      Hint = '(*)Benutzer Manager zeigen'
      ImageIndex = 34
      OnExecute = acShowUserManagerExecute
    end
    object acAddUsergroup: TAction
      Category = 'MMSecurity'
      Hint = '(*)Gruppen auswaehlen und zufuegen'
      ImageIndex = 33
      OnExecute = acAddUsergroupExecute
    end
    object acShowMMSecurityProg: TAction
      Category = 'MMSecurity'
      Hint = '(*)Sicherheitseinstellung nach MM Programme'
    end
    object acShowMMSecurityUserGroup: TAction
      Category = 'MMSecurity'
      Hint = '(*)Sicherheitseinstellung nach MM Gruppen'
    end
    object acPrintSecurityAll: TAction
      Category = 'MMSecurity'
      Hint = '(*)Sicherheits Einstellungen drucken'
    end
    object acPrintSecurityProg: TAction
      Category = 'MMSecurity'
      Hint = '(*)Programm Sicherheitseinstellungen drucken'
    end
  end
  inherited mmPrinterSetupDialog: TmmPrinterSetupDialog
    DictionaryName = 'MainDictionary'
    Left = 600
    Top = 0
  end
  inherited mmPrintDialog: TmmPrintDialog
    DictionaryName = 'MainDictionary'
    Left = 568
    Top = 0
  end
  inherited mmSaveDialog: TmmSaveDialog
    DictionaryName = 'MainDictionary'
    DefaultExt = 'dat'
    Filter = 'MM Setup (*.dat) |*.dat'
    Title = 'Export MM Settings'
    Left = 536
    Top = 0
  end
  inherited mmOpenDialog: TmmOpenDialog
    Parent = SetupPageControl
    DictionaryName = 'MainDictionary'
    Filter = 'Programme (*.exe) |*.exe'
    Options = [ofHideReadOnly, ofNoChangeDir, ofPathMustExist, ofFileMustExist]
    Title = 'Prozesse oeffnen'
    Left = 504
    Top = 0
  end
  inherited pmToolbar: TmmPopupMenu
    Left = 464
    Top = 48
  end
  inherited MMLogin: TMMLogin
    Left = 472
    Top = 0
  end
  inherited mTranslator: TmmTranslator
    OnLanguageChange = mTranslatorLanguageChange
    Options = [ivtoAutoTranslate, ivtoCheckFont, ivtoScaleMultiByte, ivtoMirrorBiDirectional, ivtoChangeFontCharset, ivtoUpdateLocaleProperty, ivtoAutoOpen, ivtoTranslateSystemMenu]
    Left = 344
    Top = 0
    TargetsData = (
      1
      19
      (
        '*'
        'Text'
        0)
      (
        'TNTUserMan'
        '*'
        1)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'SimpleText'
        0)
      (
        '*'
        'Title'
        0)
      (
        'TSetupLabel'
        '*'
        0)
      (
        'TSetupSpinEdit'
        '*'
        0)
      (
        'TSetupCheckBox'
        '*'
        0)
      (
        'TmmMainMenu'
        '*'
        0)
      (
        'TMenuItem'
        '*'
        0)
      (
        'TMMLogin'
        'Caption'
        0)
      (
        '*'
        'Filter'
        0)
      (
        'TmmExtStringGrid'
        '*'
        0)
      (
        'TmmExtStringGrid'
        'ColumnHeader'
        0)
      (
        'TSetupComboBox'
        '*'
        0)
      (
        'TSetupEdit'
        '*'
        0)
      (
        'TTabSheed'
        '*'
        0)
      (
        'TmmLabel'
        '*'
        0)
      (
        'TmmPageControl'
        '*'
        0))
  end
  inherited mDictionary: TmmDictionary
    PrimaryLanguage = 7
    SubLanguage = 1
    AutoConfig = True
    Left = 312
    Top = 0
  end
  inherited mMainMenu: TmmMainMenu
    Left = 440
    Top = 0
    inherited miDatei: TMenuItem
      object miImport: TMenuItem [0]
        Action = acImport
      end
      object miExport: TMenuItem [1]
        Action = acExport
      end
      inherited miNew: TMenuItem
        Tag = 2
        Action = nil
        ImageIndex = -1
      end
      inherited N11: TMenuItem
        Tag = 2
      end
      inherited miOpen: TMenuItem
        Tag = 2
      end
      inherited N21: TMenuItem
        Tag = 2
      end
      inherited miSave: TMenuItem
        Tag = 2
        Action = nil
        ImageIndex = -1
        ShortCut = 0
      end
      inherited miSaveAs: TMenuItem
        Tag = 2
      end
      inherited N31: TMenuItem
        Tag = 2
      end
      inherited miPrinterSetup: TMenuItem
        Tag = 2
      end
      inherited miPreview: TMenuItem
        Tag = 2
      end
    end
    inherited miBearbeiten: TMenuItem
      object GetDefault1: TMenuItem
        Action = acGetDefaults
      end
      object GetValue1: TMenuItem
        Action = acGetValue
      end
      object SaveSettings1: TMenuItem
        Action = acSaveActivePageSettings
      end
    end
    inherited miExtras: TMenuItem
      object acSetSecuritiy1: TMenuItem [0]
        Action = acSettings
      end
      object N3: TMenuItem [1]
        Caption = '-'
      end
    end
    inherited miFenster: TMenuItem
      Tag = 2
      inherited miCascade: TMenuItem
        Tag = 2
      end
      inherited miTileHorizontally: TMenuItem
        Tag = 2
      end
      inherited miTileVertically: TMenuItem
        Tag = 2
      end
    end
    inherited miHilfe: TMenuItem
      Caption = '(*)Hilfe'
      inherited miMMHilfe: TMenuItem
        Tag = 2
        Enabled = False
      end
      inherited N5: TMenuItem
        Tag = 2
      end
    end
  end
  object MMSetupModul1: TMMSetupModul
    UseApplName = True
    Left = 556
    Top = 46
  end
  object mmDataSource1: TmmDataSource
    DataSet = mmADOQuery
    Left = 48
    Top = 373
  end
  object MMSecurityDB1: TMMSecurityDB
    Active = True
    CheckApplStart = False
    Left = 668
    Top = 4
  end
  object MMSecurityControl1: TMMSecurityControl
    FormCaption = 'Millmaster settings  (ADO vers.)'
    Active = True
    MMSecurityName = 'MMSecurityDB1'
    OnAfterSecurityCheck = MMSecurityControl1AfterSecurityCheck
    Left = 700
    Top = 4
  end
  object dsDBVers: TmmDataSource
    DataSet = qADODBVers
    Left = 46
    Top = 419
  end
  object NTUserMan1: TNTUserMan
    Left = 28
    Top = 65532
  end
  object mmADOQuery: TmmADOQuery
    Connection = MMConfigDataModule.MMADOConnection
    Parameters = <>
    SQL.Strings = (
      'Select c_Applicationname from t_Security')
    Left = 97
    Top = 377
  end
  object qADODBVers: TmmADOQuery
    Connection = MMConfigDataModule.MMADOConnection
    Parameters = <>
    SQL.Strings = (
      'select Data from t_mmuparm where AppKey = '#39'ActualVersionUpdate'#39)
    Left = 88
    Top = 424
  end
end
