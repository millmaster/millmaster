unit mmSpalsh;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, mmProgressBar, mmAnimate, StdCtrls, mmLabel, ExtCtrls;

type
  TfSplash = class(TForm)
    Panel1: TPanel;
    mmLabel1: TmmLabel;
    mmLabel3: TmmLabel;
    mmLabel2: TmmLabel;
    mmAnimate1: TmmAnimate;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fSplash: TfSplash;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R MMConfig.res}
{$R *.DFM}

procedure TfSplash.FormCreate(Sender: TObject);
begin
   mmAnimate1.Height:= 40;
   mmAnimate1.FileName:='';
   mmAnimate1.ResName := 'FindFile';
   mmAnimate1.Active:= TRUE;
end;

procedure TfSplash.FormActivate(Sender: TObject);
begin
  mmAnimate1.Active:= TRUE;
end;

end.
