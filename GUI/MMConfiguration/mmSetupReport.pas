unit mmSetupReport;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QuickRpt, ExtCtrls, Qrctrls, BaseForm, mmList, mmSetupMain, mmQRLabel,
  mmQRShape, mmQRSubDetail, mmQRGroup, mmQRBand, mmQRSysData;

type

  TListSortCompare = function (Item1, Item2: Pointer): Integer;  // Sort TList

  TSetupReport = class(TmmForm)
    QuickRep1: TQuickRep;
    PageHeaderBand1: TQRBand;
    mmQRBand1: TmmQRBand;
    mmQRSysData1: TmmQRSysData;
    qrColumnHeader: TmmQRBand;
    qrDetail: TmmQRBand;
    mmQRLabel1: TmmQRLabel;
    mmQRLabel2: TmmQRLabel;
    mmQRLabel3: TmmQRLabel;
    mmQRLabel4: TmmQRLabel;
    qrPage: TmmQRLabel;
    mmQRLabel5: TmmQRLabel;
    mmQRLabel6: TmmQRLabel;
    mmQRLabel7: TmmQRLabel;
    mmQRLabel8: TmmQRLabel;
    mmQRShape2: TmmQRShape;
    QRSysData1: TQRSysData;
    mmQRShape1: TmmQRShape;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel5: TQRLabel;
    qlCompany: TQRLabel;
    qlComputer: TQRLabel;
    qlDate: TQRLabel;
    QRLabel3: TQRLabel;
    qlComputername: TQRLabel;
    QRImage1: TQRImage;
    QRSysData2: TQRSysData;
    procedure QuickRep1NeedData(Sender: TObject; var MoreData: Boolean);
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRGroupBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    mPrintRec: TmmList;
    CurrentItem : Integer;
    mGroup      : String;
    x: integer;

  public
    { Public declarations }
    constructor Create(aOwner: TComponent; aPrintRec: TmmList); virtual;
  end;

  function SortByGroup(Item1, Item2: Pointer): Integer;

var
  SetupReport: TSetupReport;

implementation // 15.07.2002 added mmMBCS to imported units

{$R *.DFM}
uses
  mmMBCS,
 printers;

{ TSetupReport }

//------------------------------------------------------------------------------
constructor TSetupReport.Create(aOwner: TComponent; aPrintRec: TmmList);
begin
  inherited Create(aOwner);
  mPrintRec:= aPrintRec;
  mPrintRec.Sort(SortByGroup);
end;
//------------------------------------------------------------------------------
procedure TSetupReport.QuickRep1NeedData(Sender: TObject; var MoreData: Boolean);
  var xPrintRec : pPrintRec;
      xName, xGroup, xValue, xValueUnit: String;
begin
  if CurrentItem <  mPrintRec.Count then begin

     xPrintRec:= mPrintRec.Items[CurrentItem];
     xValue     := xPrintRec.Value;
     xValueUnit := xPrintRec.ValueUnit;
     xName      := xPrintRec.Name;
     xGroup     := xPrintRec.Group;

     if mGroup <> xGroup then
       mmQRLabel5.Caption := xGroup
     else
       mmQRLabel5.Caption := '';

     mmQRLabel6.Caption:= xName;
     mmQRLabel7.Caption:= xValue;
     mmQRLabel8.Caption:= xValueUnit;

     mGroup:= xGroup;
  end;
  Inc(CurrentItem);
  MoreData := CurrentItem <= mPrintRec.Count;
  screen.Cursor:=  crDefault;
end;
//------------------------------------------------------------------------------
procedure TSetupReport.QuickRep1BeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  CurrentItem := 0;
  PrintReport := mPrintRec.Count > 0;
end;
//------------------------------------------------------------------------------
procedure TSetupReport.QRGroupBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if Sender.ControlCount = 0 then
    with qrDetail do
      While ControlCount > 0 do
        Controls[0].Parent := Sender;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Sortiert nach Gruppen
//******************************************************************************
function SortByGroup(Item1, Item2: Pointer): Integer;
begin
 result:=CompareText(pPrintRec(Item1)^.SortOrder, pPrintRec(Item2)^.SortOrder);
end;
//------------------------------------------------------------------------------

end.
