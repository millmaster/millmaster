(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_MMSecurity_Appl.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Zeigt die MM-Security der Applikationen & Forms
| Info..........: TMMSecurityAppl wird im MMConfiguration.dpr erstellt
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 14.10.2002  1.00  SDo | Report erstellt
| 27.01.2004  1.01  SDo | Appl. Uebersetzung,
|                       | Aenderungen: esgApplSecurityRadio(), Translator1.LanguageChange()
|=============================================================================*)
unit u_MMSecurity_Appl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmComboBox, ExtCtrls, mmPanel, Grids, AdvGrid, mmExtStringGrid,
  mmLabel,
  u_MMConfigSecurity, IvDictio, IvMulti, IvEMulti, mmTranslator, fcLabel,
  mmGroupBox;

type

  TMMSecurityAppl = class(TFrame)
    esgApplSecurity: TmmExtStringGrid;
    paTop: TmmPanel;
    cbAppls: TmmComboBox;
    cbForms: TmmComboBox;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmTranslator1: TmmTranslator;
    gbRemark: TmmGroupBox;
    lbRemark: TmmLabel;
    mLabel: TmmLabel;
    procedure esgApplSecurityClick(Sender: TObject);
    procedure esgApplSecurityGetCellColor(Sender: TObject; ARow,
      ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure cbApplsChange(Sender: TObject);
    procedure cbFormsChange(Sender: TObject);
    procedure esgApplSecurityRadioClick(Sender: TObject; aCol, aRow,
      aIdx: Integer);
    procedure mmTranslator1LanguageChange(Sender: TObject);
    procedure FrameResize(Sender: TObject);
  private
    { Private declarations }
    fMMSecurityClass: TBaseMMSecurity;
    fSecurityChanged: Boolean;

    procedure ShowData;
    procedure SetActiveSecurityClass(const Value: TBaseMMSecurity);
    function GetSecurityChanged: Boolean;
    procedure SetSecurityChanged(const Value: Boolean);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    property ActiveSecurityClass: TBaseMMSecurity write SetActiveSecurityClass;
    property SecurityChanged: Boolean read GetSecurityChanged write SetSecurityChanged;
  end;

var
  MMSecurityAppl: TMMSecurityAppl;
implementation
{$R *.DFM}
uses
  MMSetupDataMudule, mmSetupMain, mmCS;
//------------------------------------------------------------------------------
constructor TMMSecurityAppl.Create(AOwner: TComponent);
var
  x: Integer;
  xRbIndex: Integer;
begin
EnterMethod('TMMSecurityAppl.Create');
  inherited Create(AOwner);;
  fMMSecurityClass := nil;

  cbAppls.Clear;
  cbForms.Clear;

  InitSecurityGrid(esgApplSecurity);

  esgApplSecurity.ColWidths[0] := 280;
  esgApplSecurity.ColWidths[1] := 210;
  mLabel.Caption := Trim(cDefaultMarker);
  fSecurityChanged := False;

  CodeSite.SendMsg('TMMSecurityAppl created');

end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.esgApplSecurityClick(Sender: TObject);
begin
  esgApplSecurity.SetFocus;
  esgApplSecurity.ScrollBars := ssVertical;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.esgApplSecurityGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush;
  AFont: TFont);
begin
  if (ACol = 2) and (ARow > 0) then begin
    AFont.Name := 'Courier';
    AFont.Size := 6;
    AFont.Color := clRed;
  end
  else
    AFont.Color := clWindowText;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.cbApplsChange(Sender: TObject);
begin
  cbForms.Items.CommaText := fMMSecurityClass.GetApplicationForms(cbAppls.Text);
  cbForms.ItemIndex := 0;
  ShowData;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.ShowData;
begin
  fMMSecurityClass.SecurityGrid := esgApplSecurity;
  fMMSecurityClass.ShowData(cbAppls.text, cbForms.text);
  esgApplSecurity.SetFocus;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.cbFormsChange(Sender: TObject);
begin
  ShowData;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.esgApplSecurityRadioClick(Sender: TObject; aCol,
  aRow, aIdx: Integer);
var x, xSecurLevel: Integer;
  xAppl, xForm, xFunction, xUserGroup: string;
begin

  xSecurLevel := aIdx;

  xAppl := esgApplSecurity.Cells[3, aRow];
  xForm := esgApplSecurity.Cells[4, aRow];
  xFunction := esgApplSecurity.Cells[5, aRow];
  xUserGroup := esgApplSecurity.Cells[6, aRow];

  with fMMSecurityClass do
    for x := 0 to SecurityItem.Count - 1 do
      if (SecurityItem.Items[x].Appl = xAppl) and
        (SecurityItem.Items[x].ApplForm = xForm) and
        (SecurityItem.Items[x].ApplFunction = xFunction) and
        (SecurityItem.Items[x].SecurityGroup = xUserGroup) then begin
        SecurityItem.Items[x].SecurityLevel := xSecurLevel;
        break;
      end else if (SecurityItem.Items[x].TranslAppl = xAppl) and
        (SecurityItem.Items[x].TranslFormCaption = xForm) and
        (SecurityItem.Items[x].ApplFunction = xFunction) and
        (SecurityItem.Items[x].SecurityGroup = xUserGroup) then begin
              SecurityItem.Items[x].SecurityLevel := xSecurLevel;
              break;
         end;
  fSecurityChanged := True;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.SetActiveSecurityClass(
  const Value: TBaseMMSecurity);
begin
  fMMSecurityClass := Value;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.mmTranslator1LanguageChange(Sender: TObject);
var xText: string;
    x, xIndex: integer;
    xOrgTxt, xTranslTxt :String;
begin

  xOrgTxt := cbAppls.Text;
  xTranslTxt := xOrgTxt;

  for x:= 0 to  fMMSecurityClass.Count - 1 do begin
      if fMMSecurityClass.SecurityItem.Items[x].TranslAppl = xOrgTxt then begin
         xTranslTxt := Translate('(*)' + fMMSecurityClass.SecurityItem.Items[x].Appl) ;
         xTranslTxt := StringReplace(xTranslTxt, '(*)', '', [rfReplaceAll]);
         break;
      end else if fMMSecurityClass.SecurityItem.Items[x].Appl = xOrgTxt then begin
         xTranslTxt := Translate('(*)' + fMMSecurityClass.SecurityItem.Items[x].Appl);
         xTranslTxt := StringReplace(xTranslTxt, '(*)', '', [rfReplaceAll]);
         break;
      end;
  end;

  xText := Translate( rsRemarkText );
  xText := Format('%s ''%s''.', [rsRemarkText, cDefaultUserGroup]);

  MMSecurityAppl.lbRemark.Caption := xText;

  x := cbForms.ItemIndex;

  fMMSecurityClass.Translate;

  cbAppls.Items.CommaText := fMMSecurityClass.MMApplicationsTranslated;

  xIndex:= cbAppls.Items.IndexOf(xTranslTxt);
  if xIndex < 0 then xIndex:=0;
  cbAppls.ItemIndex := xIndex; 

  cbForms.Items.CommaText := fMMSecurityClass.GetApplicationForms(cbAppls.Text);
  cbForms.ItemIndex := x;

  TranslateHeader(esgApplSecurity);
  fMMSecurityClass.SecurityGrid := esgApplSecurity;
  fMMSecurityClass.ShowData(cbAppls.text, cbForms.text);
end;
//------------------------------------------------------------------------------
function TMMSecurityAppl.GetSecurityChanged: Boolean;
begin
  Result := fSecurityChanged;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.SetSecurityChanged(const Value: Boolean);
begin
  fSecurityChanged := Value;
end;
//------------------------------------------------------------------------------
procedure TMMSecurityAppl.FrameResize(Sender: TObject);
begin
  gbRemark.Width := Width - gbRemark.Left - 17;
  lbRemark.Width := gbRemark.Width - lbRemark.Left - 4;
end;
//------------------------------------------------------------------------------
end.

