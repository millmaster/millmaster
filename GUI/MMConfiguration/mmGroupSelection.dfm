inherited GroupSelectionDLG: TGroupSelectionDLG
  Left = 394
  Top = 271
  Caption = '(*)Zu kontrollierende Benutzergruppen auswaehlen'
  ClientHeight = 378
  PixelsPerInch = 96
  TextHeight = 16
  inherited mmPanel4: TmmPanel
    Top = 338
  end
  inherited gbSelectionList: TmmPanel
    Height = 338
    inherited mmPanel1: TmmPanel
      Height = 334
      inherited mmLabel2: TmmLabel
        Width = 115
        Caption = '(*)Domain Gruppen'
      end
      inherited lbSource: TmmListBox
        Height = 310
        Align = alNone
      end
    end
    inherited mmPanel2: TmmPanel
      Height = 334
    end
    inherited mmPanel3: TmmPanel
      Height = 334
      inherited mmLabel3: TmmLabel
        Width = 153
        Caption = '(*)Ausgewaehlte Gruppen'
      end
      inherited lbDest: TmmListBox
        Height = 284
      end
      object mmPanel5: TmmPanel
        Left = 0
        Top = 304
        Width = 200
        Height = 30
        Align = alBottom
        BevelOuter = bvNone
        BorderWidth = 5
        TabOrder = 1
        object bClear: TmmButton
          Left = 2
          Top = 2
          Width = 196
          Height = 25
          Action = acClear
          TabOrder = 0
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
      end
    end
  end
  inherited mmActionList: TmmActionList
    object acClear: TAction
      Caption = '(*)Alles loeschen'
      OnExecute = acClearExecute
    end
  end
  object Translator: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 62
    Top = 62
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
end
