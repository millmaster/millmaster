{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_PCNames.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Ermittelt alle Domains im Netz und von einer gewaehlten
|                 Domain / Workgroup alle dazugehoerigen Computernamen.
|                 Rueckgabe: Ausgewaehlte Computernamen
|
| Info..........: Wichtig: Beim Aufstart des MMConfigurations wird der Thread
|                          mDomainNamesThread (TDomainsThread) erstellt, welcher
|                          die Domainen einliest. Bei einer grossen Anzahl von
|                          Domains wird viel Zeit benoetigt (70 Domains -> 45 Sec.).
|                          Ebenfalls beim Auslesen der PC's ( 3300 PC's -> 60 Sec.).
|                          Die Ermittlung des MM-Hosts benoetigt noch mehr Zeit.
|                          Darum wurde darauf verzichtet.
|                          -> Pro SQL-Abfrage auf die Existsns der DB MM_Winding
|                             kann mehr als 60 sec. ins Land ziehen;
|                             z.B. kein Zugriff oder Timeouts!!
|                             (Abfage nur auf SRV mit SQL-SRV)
|
|                          Der Thread wird nach dem Einlesen gestoppt und mit
|                          dem Button-Click ( DomainRefresch &  PCNames) gestartet
|
| Develop.system: Win-XP
| Target.system.: W2k, Win-XP, W2k3
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 15.11.2006  1.00  SDo | File created
| 17.01.2007  1.01  Nue | Uebersetzte Strings (//ivlm) unter Resourcestring statt const.
===============================================================================}
unit u_PCNames;
                    
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, mmThread, BaseForm, ComCtrls, mmSpeedButton, mmListBox,
  mmLabel, ExtCtrls, mmRadioGroup, mmLineLabel,
  NetApi32, mmProgressBar, fcStatusBar, mmTimer, ActnList, mmActionList,
  AdoDbAccess, IvDictio, IvMulti, IvEMulti, mmTranslator, mmPanel, ActiveX;

type

  TMMComputerType = (ctMMHost, ctMMComputers, ctMMPDC);


  TDomainsThread = class(TmmThread)
  private
    { Private declarations }
    mDomains : TStringList;
    mMachines : TStringList;
    mDomainName : string;
    mItsGetCoputerNames :Boolean;

    function GetDomains : TStringList;
    function GetMachines(aDomainName: string): TStringList;
    function GetComputerNames: TStringList;
    procedure ReadComputerNames;

  protected
    procedure Execute; override;
  public
    destructor Destroy; override;
    constructor Create; virtual;
    property Domains: TStringList read GetDomains;
    property ComputerNames: TStringList read GetComputerNames;

  end;


  TfMMPCNames = class(TmmForm)
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    cbbDomain: TComboBox;
    lbl2: TLabel;
    status1: TfcStatusBar;
    ProgressBar1: TmmProgressBar;
    Timerimer1: TmmTimer;
    grp1: TGroupBox;
    rgFilter1: TmmRadioGroup;
    lblPCNames: TLabel;
    lbDomainComputerNames  : TmmListBox;
    lbSelectedComputers: TmmListBox;
    lbSelectedPC1: TmmLabel;
    btToChoosenPCs: TSpeedButton;
    btFromChoosenPCs: TSpeedButton;
    btnDeletePC: TSpeedButton;
    btnClearAllSelectedPCs: TSpeedButton;
    btPCNames: TSpeedButton;
    mmTranslator1: TmmTranslator;
    mmPanel1: TmmPanel;
    btnDomainRefresh: TSpeedButton;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btPCNamesClick(Sender: TObject);
    procedure btnDomainRefreshClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timerimer1Timer(Sender: TObject);
    procedure btToChoosenPCsClick(Sender: TObject);
    procedure btFromChoosenPCsClick(Sender: TObject);
    procedure btnClearAllSelectedPCsClick(Sender: TObject);
    procedure rgFilter1Click(Sender: TObject);
    procedure btnDeletePCClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure lbSelectedComputersClick(Sender: TObject);

  private
    FMMPCNames : string;
    FIsMMWorkStation : Boolean;
    //FPCName : String;
    mDomainList : TStringList;
    mPCList : TStringList;
    mPCFilterList : TStringList;

    mDomainThread : TDomainsThread;
    mLocalMachineName : string;
    mActualDomainWorkGroupName : String;
    mSelectedDomainWorkGroupName : String;
    mPreSelectedMachines : String; //PC(s) von Edit-Box auf Regsiter MM-System
    mMultipleComputersPermited : Boolean;
    mFormCreated : Boolean;

    mComputerType : TMMComputerType;

    procedure WorkInProgress(aStatus : Boolean);
    procedure FilterComputerTyp( const aPCListIn : TStringList; var aPCFilterListOut : TStringList );
    function ExtractComputerInfo(aMachineName : String): string;


  protected
    procedure WndProc(var aMessage : TMessage); override;

    { Private declarations }
  public
    { Public declarations }
    procedure Loaded; override;
    destructor Destroy; override;

    constructor Create( aOwner : TComponent; aDomainThread : TDomainsThread; aMachines: string = ''; aComputerType : TMMComputerType = ctMMHost); overload;
    property MMPCNames : string read FMMPCNames;
  end;


  function GetAllDomains(var aList: TStringList):Boolean;
  function GetServers(aDomain: String; aServerType: Word; aList : TStringList): Boolean;
  function GetServerInfo(aMachineName: String ): DWORD;


  // Sendet eine Meldung vom Thread zum PCName-Fenster -> Daten abholbereit
  function MMBroadCastMessage(aMessageID: THandle; WParam: Integer; LParam: Integer = 0):Boolean;

Resourcestring
      cDeterminePCsMsg = '(*)Computer werden ermittelt. Benoetigt einige Zeit!';  //ivlm
      cDetermineDomainssMsg = '(*)Domaenen werden ermittelt. Benoetigt einige Zeit!';  //ivlm
      cDeletePCNameMsg = '(*)Dieser Computer gehoert nicht zur Domaene / Arbeitsgruppe %s. %s Wollen Sie den selektierten Computer %s aus der Liste entfernen?'; //ivlm
      cLabelDomainWorkgroup = '(*)Domaene / Arbeitsgruppe : %s'; //ivlm
      cDeleteSelPCName = '(*)Wollen Sie den Computer %s aus der Liste entfernen?';  //ivlm
      cDeleteAllPCNames = '(*)Wollen Sie alle ausgewaehlten Computer aus der Liste entfernen?';  //ivlm

      cFormCaptionMMHost = '(*)Waehlen Sie den MillMaster Host Computer aus. (SQL Server mit der MillMaster Datenbank)'; //ivlm
      cFormCaptionMMPCs  = '(*)Waehlen Sie alle MillMaster Computer aus.'; //ivlm
      cFormCaptionMMPDC  = '(*)Waehlen Sie den Computer aus, auf welchem die MillMaster-Benutzer und -Gruppen installiert sind.'; //ivlm

      cMsgNoPCsSelected = '(*)Sie haben keine Computer ausgewaehlt!'; //ivlm
      cMsgNoPCSelected = '(*)Sie haben keinen Computer ausgewaehlt!'; //ivlm
      cMsgOnlyOnePCSelect = '(*)Sie duerfen nur einen Computer ausgewaehlen!'; //ivlm

const cThisPC =  'This PC' ; //ivlm
      cPDC    =  'DC';
      cMMHost =  'MM Host';


      //Adresse  (RegisterWindowMessage)
      cMsgMachine  = 'MachineManagemant';

      // Meldungstyp (SendMessages)
      WM_DOMAINNAMES_FINISH  = WM_USER + 1;
      WM_PCNAMES_FINISH      = WM_USER + 2;

      WM_START_PROGRESSBAR   = WM_USER + 3;
      WM_STOP_PROGRESSBAR    = WM_USER + 4;
var
  fMMPCNames: TfMMPCNames;

  gMachineManagementID : THandle;


implementation

{$R *.DFM}
uses mmMBCS, Netware, mmCS, LoepfeGlobal, NTException;


//------------------------------------------------------------------------------
function MMBroadCastMessage(aMessageID: THandle; WParam: Integer; LParam: Integer = 0):Boolean;
var xRecp: DWord;
begin
  xRecp := BSM_APPLICATIONS;
  Result := (BroadcastSystemMessage(BSF_POSTMESSAGE, @xRecp,
                                    aMessageID, WParam, LParam) > 0);
end;
//------------------------------------------------------------------------------


//******************************************************************************
//Alle PC's in der Domaine ermitteln
//******************************************************************************
function GetServers(aDomain: String; aServerType : Word ; aList : TStringList): Boolean;
var
  xPWCh: array[0..255] of WideChar;
  xRes:   NET_API_STATUS;
  xbufptr:  PServer_Info_101_Arr;
  xentriesread: DWORD;
  xtotalentries: DWORD;
  xresume_handle: DWORD;
  x: integer;
  xDomain  : array[0..255] of WideChar;
  xDomName : PChar;
  xMachineName, xErrorMSG: String;
  xMachType : string;
begin

 try

  Result := False;
  if not Assigned(aList) then
    Exit;

  aList.Clear;

  xDomName := PChar(aDomain);

  xresume_handle := 0;
  StringToWideChar('', @xPWCh, SizeOf(xPWCh));
  StringToWideChar(xDomName, @xDomain, SizeOf(xDomain));

  FillChar(xbufptr,Sizeof(xbufptr),0);

  Application.ProcessMessages;

  if aDomain <> '' then begin
     xRes :=  NetServerEnum(@xPWCh,
                            101,
                            Pointer(xbufptr),
                            $FFFFFFFF,
                            xentriesread,
                            xtotalentries,
                            aServerType,
                            @xDomain,
                            xresume_handle)
  end else
     xRes :=  NetServerEnum(@xPWCh,
                            101,
                            Pointer(xbufptr),
                            $FFFFFFFF,
                            xentriesread,
                            xtotalentries,
                            aServerType,
                            NIL,
                            xresume_handle);

  if xRes = 0 then
     for x := 0 to xentriesread - 1 do begin
         xtotalentries :=  xBufPtr^[x].sv101_type ;
         xMachineName  := WideCharToString(xBufPtr^[x].sv101_name);
         aList.AddObject(xMachineName, TObject(xBufPtr^[x].sv101_type) );
         Forms.Application.ProcessMessages;
     end;
  if xBufPtr <> nil then NetApiBufferFree(xBufPtr);
  Result := True;
 except
   on E: Exception do begin
       xErrorMSG := Format('Error in func. GetServers( %s, %d). Msg: %s',[aDomain, aServerType, e.message]);
       MessageDlg(xErrorMSG, mtError, [mbOk], 0);
       Result := False;
   end;
 end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Alle Domain-Namen ermitteln
//******************************************************************************
function GetAllDomains(var aList: TStringList):Boolean;
var
  xPWCh: array[0..255] of WideChar;
  xRes:   NET_API_STATUS;
  xbufptr:  PServer_Info_101_Arr;
  xentriesread: DWORD;
  xtotalentries: DWORD;
  xresume_handle: DWORD;
  x: integer;
  xErrorMSG : String;
begin
  result := False;

  if not Assigned(aList) then
    Exit;

  try
      aList.Clear;

      StringToWideChar('', @xPWCh, SizeOf(xPWCh));

      xresume_handle := 0;
      FillChar(xbufptr,Sizeof(xbufptr),0);
      xRes :=  NetServerEnum(@xPWCh,
                             101,
                             Pointer(xbufptr),
                             $FFFFFFFF,
                             xentriesread,
                             xtotalentries,
                             SV_TYPE_DOMAIN_ENUM,
                             NIL,
                             xresume_handle);

      if xRes = 0 then
         for x := 0 to xentriesread - 1 do begin
             xtotalentries :=  xBufPtr^[x].sv101_type ;
             aList.Add(WideCharToString(xBufPtr^[x].sv101_name));
             forms.Application.ProcessMessages;
         end;

    if xBufPtr <> nil then NetApiBufferFree(xBufPtr);
    result := TRUE;
  except
     on E: Exception do begin
         xErrorMSG := Format('Error in func. GetAllDomains(). Msg: %s',[e.message]);
         MessageDlg(xErrorMSG, mtError, [mbOk], 0);
         Result := False;
     end;
  end;
end;
//------------------------------------------------------------------------------
function GetServerInfo(aMachineName: String): DWORD;
var
  xPWCh: array[0..255] of WideChar;
  xRes:   NET_API_STATUS;
  xbufptr:  PServer_Info_101_Arr;
  xentriesread: DWORD;
  xtotalentries: DWORD;
  xresume_handle: DWORD;
  x: integer;
  xDomain  : array[0..255] of WideChar;
  xDomName : PChar;
  xMachineName, xErrorMSG: String;
  xMachType, xActDomain : string;
begin

 try

  Result := 0;

  xDomName := PChar(GetDomainName(aMachineName)) ;
  xActDomain := GetActualDomainName;

  if CompareText(xDomName, xActDomain) = 0 then
     xDomName := '';

  xMachineName :=StringReplace(aMachineName, '\\' , '', [rfReplaceAll]);
  xMachineName := '\\' + xMachineName ;

  xresume_handle := 0;
  StringToWideChar(xMachineName, @xPWCh, SizeOf(xPWCh));
  StringToWideChar(xDomName, @xDomain, SizeOf(xDomain));

  FillChar(xbufptr,Sizeof(xbufptr),0);

  Application.ProcessMessages;

  if StrLen(xDomName) > 0 then begin
     xRes :=  NetServerEnum(@xPWCh,
                            101,
                            Pointer(xbufptr),
                            $FFFFFFFF,
                            xentriesread,
                            xtotalentries,
                            SV_TYPE_NT	,
                            @xDomain,
                            xresume_handle)
  end else
     xRes :=  NetServerEnum(@xPWCh,
                            101,
                            Pointer(xbufptr),
                            $FFFFFFFF,
                            xentriesread,
                            xtotalentries,
                            SV_TYPE_NT,
                            NIL,
                            xresume_handle);

  if xRes = 0 then
     for x := 0 to xentriesread - 1 do begin
         Result := xBufPtr^[x].sv101_type ;
         Forms.Application.ProcessMessages;
     end;
     //else
     //  ShowMessage(GetLastErrorString(xRes));

  if xBufPtr <> nil then NetApiBufferFree(xBufPtr);
 except
   on E: Exception do begin
       xErrorMSG := Format('Error in func. GetServerInfo( %s). Msg: %s',[aMachineName, e.message]);
       MessageDlg(xErrorMSG, mtError, [mbOk], 0);
       Result := 0;
   end;
 end;
end;

//------------------------------------------------------------------------------
constructor TDomainsThread.Create;
begin
  inherited Create(FALSE);

  EnterMethod('TDomainsThread.Create');

  FreeOnTerminate := True;

  mDomains:= nil;
  mItsGetCoputerNames := False;
  mMachines := TStringList.Create;
  mMachines.Clear;
  mMachines.Sorted := True;
  mMachines.Duplicates := dupIgnore;

  mDomains := TStringList.Create;
  mDomains.Sorted := True;
  mDomains.Duplicates := dupIgnore;
end;
//------------------------------------------------------------------------------
destructor TDomainsThread.Destroy;
begin
 EnterMethod('TDomainsThread.Destroy');
 try
    if Assigned(mDomains) then begin
        mDomains.Free;
    end; 

  finally
    mMachines.Free;
    mDomains := nil;
  end;

  inherited;
end;
//------------------------------------------------------------------------------
procedure TDomainsThread.Execute;
begin
 { Place thread code here }
 try

   while (not Terminated) do begin
      //Domains auslesen
      if (mDomains.Count = 0) then begin
        CodeSite.SendMsg('TTDomainsThread.Execute-> GetAllDomains() ');
        MMBroadCastMessage(gMachineManagementID, WM_START_PROGRESSBAR);
        GetAllDomains(mDomains) ;

        if (mDomains.Count <=0) then
          mDomains.Add ( GetDomainName('.') ); //Workgroup

        if not Suspended then begin
          CodeSite.SendMsg('TDomainsThread.Execute -> Domain read Thread stopped ');
          MMBroadCastMessage(gMachineManagementID, WM_DOMAINNAMES_FINISH);
          MMBroadCastMessage(gMachineManagementID, WM_STOP_PROGRESSBAR);
          Suspended := True;
        end;
      end else
        //PC's auslesen
        if mItsGetCoputerNames then begin
           ReadComputerNames;
           if not Suspended then begin
             CodeSite.SendMsg('TDomainsThread.Execute -> PC read Thread stopped ');
             MMBroadCastMessage(gMachineManagementID, WM_PCNAMES_FINISH);
             MMBroadCastMessage(gMachineManagementID, WM_STOP_PROGRESSBAR);
             Suspended := True;
           end;
        end else
          Suspended := True;
   end;
 finally
 end;
end;
//------------------------------------------------------------------------------
procedure TfMMPCNames.FormCreate(Sender: TObject);
var xList : TStringList;
    x : Integer;
    xTxt : string;
    xPCType : DWORD;
begin

  case GetNTProduct( mLocalMachineName ) of
    cWorkstation,
    cStandaloneWKS  : FIsMMWorkStation := True;

    else
      FIsMMWorkStation := FALSE;
  end;

  mmPanel1.Caption := '';
  case mComputerType of
       ctMMHost      :  begin
                          mMultipleComputersPermited := FALSE;
                          mmPanel1.Caption := Format('   %s',[cFormCaptionMMHost] );
                          rgFilter1.ItemIndex := 1;
                        end;
       ctMMComputers :  begin
                          mMultipleComputersPermited := TRUE;
                          mmPanel1.Caption := Format('   %s',[cFormCaptionMMPCs] );

                          if not FIsMMWorkStation then
                            rgFilter1.ItemIndex := 2
                          else
                            rgFilter1.ItemIndex := 1;
                        end;

       ctMMPDC       :  begin
                          mMultipleComputersPermited := FALSE;
                          mmPanel1.Caption := Format('   %s',[cFormCaptionMMPDC] );
                          rgFilter1.ItemIndex := 0;
                        end;
  end;

  cbbDomain.Clear;
  cbbDomain.Sorted := True;

  mActualDomainWorkGroupName := GetDomainName('.');

  WorkInProgress(TRUE);

  xList := TStringList.Create;
  xList.Sorted := True;
  xList.CommaText := mPreSelectedMachines;

  lbSelectedComputers.Clear;
  for x:= 0 to xList.Count -1 do begin
     xTxt := xList.Strings[x];
     //This PC
     if CompareText(mLocalMachineName, xList.Strings[x] ) = 0 then
       xTxt := Format('%s [%s]', [ xList.Strings[x], cThisPC] )
     else  begin
       xPCType := GetServerInfo( xList.Strings[x] ) ;
       //DC
       if (xPCType and SV_TYPE_DOMAIN_CTRL = SV_TYPE_DOMAIN_CTRL ) then
          xTxt := Format('%s [%s]', [ xList.Strings[x], cPDC] );
     end;
     lbSelectedComputers.Items.Add(xTxt);
  end;
  xList.Free;

  grp1.caption := Format(cLabelDomainWorkgroup, [''] );


  lbDomainComputerNames.MultiSelect := mMultipleComputersPermited;

  mFormCreated := True;
end;
//------------------------------------------------------------------------------
procedure TfMMPCNames.Loaded;
begin
  inherited Loaded;

  mDomainList := nil;

  EnterMethod('TfMMPCNames.Loaded');

  try
    mDomainList := TStringList.Create;
    mDomainList.Sorted := True;
  finally
  end;
end;
//------------------------------------------------------------------------------
procedure TfMMPCNames.FormDestroy(Sender: TObject);
begin
  mDomainThread.Suspended := TRUE;
end;
//------------------------------------------------------------------------------
constructor TfMMPCNames.Create(aOwner : TComponent; aDomainThread: TDomainsThread;
            aMachines: string = ''; aComputerType : TMMComputerType = ctMMHost );
begin
  EnterMethod('TfMMPCNames.Create() ');

  inherited Create(aOwner);

  //Liste aller PCs von einer Domain
  mPCList := TStringList.Create;
  mPCList.Sorted := True;
  mPCList.Duplicates := dupIgnore;

  mPCFilterList := TStringList.Create;
  mPCFilterList.Sorted := True;
  mPCFilterList.Duplicates := dupIgnore;


  mFormCreated := False;
  Timerimer1.Enabled := FALSE;

  mComputerType := aComputerType;
  mPreSelectedMachines := aMachines;
  mLocalMachineName := GetMachineName;

  mSelectedDomainWorkGroupName := ' ';

  try
    if Assigned(aDomainThread) then begin
      mDomainThread := aDomainThread;
      if mDomainThread.Suspended then begin
         mDomainThread.Resume;
      end;
      Timerimer1.Enabled := True;

      if mDomainThread.mDomains.Count > 0 then begin
         MMBroadCastMessage(gMachineManagementID, WM_DOMAINNAMES_FINISH);
         MMBroadCastMessage(gMachineManagementID, WM_STOP_PROGRESSBAR);
      end else begin
        status1.Panels[0].Font.Color:= clRed;
        status1.Panels[0].Text := cDetermineDomainssMsg ;
     end;
    end else
     mDomainThread := nil;

  except
    mDomainThread := nil;
  end;
end;
//------------------------------------------------------------------------------
function TDomainsThread.GetComputerNames: TStringList;
begin
   Result := mMachines;
end;
//------------------------------------------------------------------------------
function TDomainsThread.GetDomains: TStringList;
begin
  if Assigned(mDomains) then
     Result := mDomains
  else
     Result := nil;
end;
//------------------------------------------------------------------------------
procedure TfMMPCNames.WndProc(var aMessage: TMessage);
begin

   //Domainname oder Computernamen vom Thread ausgeben, wenn fertig eingelesen
   if aMessage.Msg = gMachineManagementID then begin
      case (aMessage.WParam) of

        WM_DOMAINNAMES_FINISH : begin
                                 //Domain-Namen wurden ermittelt
                                 //Jetzt ausgeben
                                 mDomainList.AddStrings( mDomainThread.Domains );
                                 cbbDomain.Items.Clear;
                                 cbbDomain.Items.AddStrings( mDomainThread.Domains );
                                 cbbDomain.Sorted := True;
                                 cbbDomain.ItemIndex := cbbDomain.Items.IndexOf(mActualDomainWorkGroupName);

                                 if ( (mSelectedDomainWorkGroupName = cbbDomain.Text) and
                                      (mPCList.Count > 0 )) then
                                     rgFilter1.OnClick(nil);

                                 grp1.caption := Format(cLabelDomainWorkgroup, [cbbDomain.Text] );

                                 WorkInProgress(FALSE);
                                 CodeSite.SendMsg('TfMMPCNames.WndProc() -> WM_DOMAINNAMES_FINISH');
                                end;

           WM_PCNAMES_FINISH  : begin
                                 //Computer-Namen wurden ermittelt
                                 //Jetzt ausgeben
                                 mPCList.Clear;
                                 mPCList.AddStrings( mDomainThread.ComputerNames );
                                 rgFilter1.OnClick(nil);

                                 WorkInProgress(FALSE);
                                 status1.Panels[0].Text := '';
                                 CodeSite.SendMsg('TfMMPCNames.WndProc() -> WM_PCNAMES_FINISH');
                                end;


        WM_START_PROGRESSBAR  : begin
                                  Timerimer1.Enabled := True;
                                  ProgressBar1.Position := ProgressBar1.Min;
                                  CodeSite.SendMsg('TfMMPCNames.WndProc() -> WM_START_PROGRESSBAR');
                                end;

        WM_STOP_PROGRESSBAR   : begin
                                  Timerimer1.Enabled := FALSE;
                                  ProgressBar1.Position := ProgressBar1.Min;
                                  status1.Panels[0].Text := '';
                                  WorkInProgress(FALSE);

                                  btnDomainRefresh.Down := False;
                                  CodeSite.SendMsg('TfMMPCNames.WndProc() -> WM_STOP_PROGRESSBAR');
                                end;

      end;//END Case

   end else begin

     if mFormCreated and not Timerimer1.Enabled and Assigned(mPCList) then begin

       //ListBox mit den PC-Namen ist leer
       if (lbDomainComputerNames.Items.Count <=0) then begin
           btToChoosenPCs.Enabled := False;
       end else begin
          //Multiselect ist nicht erlaubt (MMHost, Domain Controller resp. PC mit Usernamen)
          if (not mMultipleComputersPermited and (lbSelectedComputers.Items.Count >=1) ) then
            btToChoosenPCs.Enabled := False
          else
            btToChoosenPCs.Enabled := True;
       end;

       //ListBox mit den ausgewaehlten PC-Namen ist leer
       if lbSelectedComputers.Items.Count <=0 then begin
          btFromChoosenPCs.Enabled := False;
          btnDeletePC.Enabled := False;
          btnClearAllSelectedPCs.Enabled := False;
       end else begin

          //PCNamen wurden noch nicht ermittelt (zuerst Button PC Name clicken)
          if (mPCList.Count <=0) then
            btFromChoosenPCs.Enabled := False
          else
            btFromChoosenPCs.Enabled := True;

          btnDeletePC.Enabled := True;

          if lbSelectedComputers.Items.Count > 1 then
             btnClearAllSelectedPCs.Enabled := True
          else
            btnClearAllSelectedPCs.Enabled := False;
       end;
     end;
   end;
   inherited  WndProc(aMessage);
end;
//------------------------------------------------------------------------------
function TDomainsThread.GetMachines(aDomainName: string): TStringList;
begin
  mMachines.Clear;

  //Thread starten und Machinen auslesen
  if aDomainName <> '' then Begin
     mMachines.Clear;
     mDomainName := aDomainName;
     mItsGetCoputerNames := True;

     if Suspended then
        Resume;
  end;

  Result := mMachines;
end;
//------------------------------------------------------------------------------
procedure TfMMPCNames.btPCNamesClick(Sender: TObject);
var  x, xIndex : Integer;
begin

  if Assigned(mDomainThread) then begin
     if mSelectedDomainWorkGroupName <> cbbDomain.Text then begin
        if mDomainThread.Suspended then begin
          CodeSite.SendMsg('TfMMPCNames.btPCNamesClick -> start mDomainThread');
          mDomainThread.Resume;
        end;

        CodeSite.SendMsg('TfMMPCNames.btPCNamesClick -> PC-Refresh');
        mPCList.Clear;
        WorkInProgress(TRUE);

        if not Timerimer1.Enabled then
           MMBroadCastMessage(gMachineManagementID, WM_START_PROGRESSBAR);
        
        status1.Panels[0].Font.Color:= clRed;
        status1.Panels[0].Text := cDeterminePCsMsg;
        mPCList.AddStrings( mDomainThread.GetMachines( cbbDomain.Text) );
        mSelectedDomainWorkGroupName := cbbDomain.Text;
     end else
        CodeSite.SendMsg('TfMMPCNames.btPCNamesClick -> Domain name not changed');
  end;

  grp1.caption := Format(cLabelDomainWorkgroup, [cbbDomain.Text] );

  //Alle PC's von mPCList in mPCFilterList nach Typ filtrieren
  rgFilter1.OnClick(Sender);

  Sleep(200);
end;
//------------------------------------------------------------------------------
procedure TfMMPCNames.btnDomainRefreshClick(Sender: TObject);
begin
  CodeSite.SendMsg('TfMMPCNames.btnDomainRefreshClick -> Domain-Refresh');
  WorkInProgress(TRUE);
  mDomainThread.mDomains.Clear;

  if not Timerimer1.Enabled then
     MMBroadCastMessage(gMachineManagementID, WM_START_PROGRESSBAR);

  if mDomainThread.Suspended then begin
     //Thread starten
     CodeSite.SendMsg('TfMMPCNames.btnDomainRefreshClick -> start mDomainThread');
     mDomainThread.Resume;
  end;
  Sleep(200);
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Steuert die Buttons Stati, wenn die Computernamen oder Domais eingelesen werden
//******************************************************************************
procedure TfMMPCNames.WorkInProgress(aStatus: Boolean);
var xSetStatus : Boolean;
begin
  xSetStatus  := not aStatus;
  btPCNames.Enabled := xSetStatus;
  btnDomainRefresh.Enabled := xSetStatus;
  cbbDomain.Enabled  := xSetStatus;

  btToChoosenPCs.Enabled   := xSetStatus;
  btFromChoosenPCs.Enabled := xSetStatus;
  btnClearAllSelectedPCs.Enabled := xSetStatus;
  btnDeletePC.Enabled := xSetStatus;

  if btnDomainRefresh.Enabled then
     btnDomainRefresh.Down := False;

  Application.ProcessMessages;
end;
//------------------------------------------------------------------------------
procedure TfMMPCNames.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   mDomainThread.Suspend;
   mDomainThread.mMachines.Clear;
   mDomainThread.mItsGetCoputerNames := False;
end;
//------------------------------------------------------------------------------
procedure TfMMPCNames.Timerimer1Timer(Sender: TObject);
begin
  ProgressBar1.StepIt;
  if ProgressBar1.Position = ProgressBar1.Max then
     ProgressBar1.Position := ProgressBar1.Min;
end;
//------------------------------------------------------------------------------
procedure TfMMPCNames.btToChoosenPCsClick(Sender: TObject);
 var x : Integer;
begin
  x:=0;
  //Selectierte Items von Domain-Computerauswahl in Box lbSelectedComputers
  while x <= lbDomainComputerNames.Items.Count -1 do begin
    if lbDomainComputerNames.Selected[x] then begin
      lbSelectedComputers.Items.Add ( lbDomainComputerNames.Items.Strings[x] );
      lbDomainComputerNames.Items.Delete(x);
      Dec(x);
    end;
    inc(x);
  end;
end;
//------------------------------------------------------------------------------
procedure TfMMPCNames.btFromChoosenPCsClick(Sender: TObject);
var x, xIndex : Integer;
    xMsgTxt, xPCName : string;
begin
  x:=0;
  //Selectierte Items aus lbSelectedComputers zurueck in Domain-Computerauswahl Box
  while x <= lbSelectedComputers.Items.Count -1 do begin
    if lbSelectedComputers.Selected[x] then begin

      xPCName := ExtractComputerInfo(lbSelectedComputers.Items.Strings[x]);

      //pruefen ob selek. PC in mPCList und lbDomainComputerNames vorhanden ist
      xIndex := mPCList.IndexOf( xPCName );
      if (xIndex >=0 ) and
         (lbDomainComputerNames.Items.IndexOf( xPCName ) <0 ) then begin

          xPCName := StringReplace( lbSelectedComputers.Items.Strings[x],
                                   xPCName,
                                   mPCList.Strings[xIndex], [rfReplaceAll] );

          lbDomainComputerNames.Items.Add ( xPCName );
          lbSelectedComputers.Items.Delete(x);
          Dec(x);
      end else begin
        xMsgTxt := Format( cDeletePCNameMsg, [ cbbDomain.Text, cCRLF, xPCName ]);
        if MessageDlg(xMsgTxt,  mtWarning, [mbYes, mbNo], 0) = mrYes then  begin
          lbSelectedComputers.Items.Delete(x);
          Dec(x)
        end;
      end;
    end;
    inc(x);
  end;//End While
end;
//------------------------------------------------------------------------------
procedure TfMMPCNames.btnClearAllSelectedPCsClick(Sender: TObject);
begin
 if MessageDlg(cDeleteAllPCNames,  mtWarning, [mbYes, mbNo], 0) = mrYes then begin
    lbSelectedComputers.Clear;
    rgFilter1.OnClick(nil);
 end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Computer nach Typ und SQL filtrieren (SRV, WKS, SRV & SQL, WKS & SQL)
//In Liste aPCListIn befindeden sich alle PC's von einer Domaine.
//******************************************************************************
procedure TfMMPCNames.FilterComputerTyp(const aPCListIn: TStringList;
  var aPCFilterListOut: TStringList);
var x       : Integer;
    xText   : string;
    xPCType : DWORD;
    xInfoTxt: string;
begin

  aPCFilterListOut.Clear;

  if not rgFilter1.Enabled then begin
    aPCFilterListOut.AddStrings(aPCListIn);
    Exit;
  end;

  for x:= 0 to  mPCList.Count-1 do begin

    xPCType  := DWORD(aPCListIn.Objects[x]);
    xText := '';
    xInfoTxt := '';

    if CompareText(mLocalMachineName, aPCListIn.Strings[x] ) = 0 then
      xInfoTxt := cThisPC;

    if rgFilter1.ItemIndex = 0 then begin
       //Server
       if (xPCType and SV_TYPE_DOMAIN_CTRL = SV_TYPE_DOMAIN_CTRL ) then
          //DC
          xText := Format('%s [%s]', [ aPCListIn.Strings[x], cPDC] )
       else if (xPCType and SV_TYPE_DOMAIN_BAKCTRL = SV_TYPE_DOMAIN_BAKCTRL ) then
          //Backup Controller
          xText := Format('%s', [ aPCListIn.Strings[x]] )
       else if (xPCType and SV_TYPE_SERVER_NT = SV_TYPE_SERVER_NT ) then
          //ADD Server
          if xInfoTxt <> '' then
            xText := Format('%s [%s]', [ aPCListIn.Strings[x], xInfoTxt ] )
          else
            xText := Format('%s', [ aPCListIn.Strings[x] ] );

       if xText <> '' then
         aPCFilterListOut.Add(xText);
    end;

    if rgFilter1.ItemIndex = 1 then begin
    //Server & SQL
       if (xPCType and SV_TYPE_DOMAIN_CTRL = SV_TYPE_DOMAIN_CTRL ) then begin
          //DC
          xText := Format('%s  [%s]', [ aPCListIn.Strings[x], cPDC ] );
          if (xInfoTxt = '') then  xInfoTxt := cPDC;
       end else if (xPCType and SV_TYPE_DOMAIN_BAKCTRL = SV_TYPE_DOMAIN_BAKCTRL ) then
          //Backup Controller
          xText := Format('%s  [%s]', [ aPCListIn.Strings[x], 'Backup DC' ] )
       else if (xPCType and SV_TYPE_SERVER_NT = SV_TYPE_SERVER_NT ) then
          //ADD Server
          xText := Format('%s', [ aPCListIn.Strings[x]] );

      if xText <> '' then
        if (xPCType and SV_TYPE_SQLSERVER  = SV_TYPE_SQLSERVER ) then begin
          //Server mit SQL
          if xInfoTxt <> '' then
            xText := Format('%s [%s]', [ aPCListIn.Strings[x], xInfoTxt ] )
          else
            xText := Format('%s', [ aPCListIn.Strings[x] ] );

          aPCFilterListOut.Add(xText);
        end;
    end;

    if rgFilter1.ItemIndex = 2 then begin
       //WKS
       if ( ( xPCType and SV_TYPE_SERVER_NT  <> SV_TYPE_SERVER_NT ) and
            ( xPCType and SV_TYPE_DOMAIN_CTRL	  <> SV_TYPE_DOMAIN_CTRL ) )then

          if (xPCType and SV_TYPE_WORKSTATION = SV_TYPE_WORKSTATION  ) then begin
            xText := Format('%s', [ aPCListIn.Strings[x] ] ) ;
            if xInfoTxt <> '' then
               xText := Format('%s [%s]', [ aPCListIn.Strings[x], xInfoTxt ] )
            else
              xText := Format('%s', [ aPCListIn.Strings[x] ] );
            end;

      if xText <> '' then
         aPCFilterListOut.Add(xText);
    end;

{
    if rgFilter1.ItemIndex = 3 then begin
    //WKS  & SQL
       if ( ( xPCType and SV_TYPE_SERVER_NT  <> SV_TYPE_SERVER_NT ) and
            ( xPCType and SV_TYPE_DOMAIN_CTRL	  <> SV_TYPE_DOMAIN_CTRL ) )then
          if (xPCType and SV_TYPE_WORKSTATION = SV_TYPE_WORKSTATION  ) then
             if (xPCType and SV_TYPE_SQLSERVER  = SV_TYPE_SQLSERVER ) then
                xText := Format('%s  [%s]  ', [ aPCListIn.Strings[x], 'WKS & SQL' ] ) ;

      if xText <> '' then
         aPCFilterListOut.Add(xText);
    end;
}

  end;
end;
//------------------------------------------------------------------------------
procedure TfMMPCNames.rgFilter1Click(Sender: TObject);
var x, xIndex: Integer;
    xPCName : string;
    xTempList, xPCFilterList : TStringList;
begin
  xTempList := TStringList.Create;
  xTempList.Sorted := True;

  xPCFilterList := TStringList.Create;
  xPCFilterList.Sorted := True;

  FilterComputerTyp(mPCList, mPCFilterList);
  lbDomainComputerNames.Clear;

  xPCFilterList.AddStrings(mPCFilterList);

  for x:=0 to mPCFilterList.Count -1 do begin
      xPCName := UpperCase(ExtractComputerInfo( mPCFilterList.strings[x])) ;
      xTempList.Add( xPCName ) ;
  end;

  //Check: PCNamen duerfen nur einmal in beiden ListBoxen vorkommen !
  x:= 0;
  if lbSelectedComputers.Items.Count > 0 then
     while x <= lbSelectedComputers.Items.Count -1 do begin
       xPCName := UpperCase(ExtractComputerInfo(lbSelectedComputers.Items.Strings[x]) );
       xIndex := xTempList.IndexOf(xPCName);

       if ( xIndex >=0 ) then begin
            xPCFilterList.Delete(xIndex);
       end;
       Inc(x);
     end; //END While

  lbDomainComputerNames.Items.AddStrings(xPCFilterList);

  xPCFilterList.Free;
  xTempList.Free;
end;
//------------------------------------------------------------------------------
procedure TfMMPCNames.btnDeletePCClick(Sender: TObject);
var xMsgTxt : string;
begin
 xMsgTxt := Format( cDeleteSelPCName,
                   [ ExtractComputerInfo(lbSelectedComputers.Items.Strings[ lbSelectedComputers.ItemIndex ]) ]);
// xMsgTxt := Format( Translate(cDeleteSelPCName),
//                   [ ExtractComputerInfo(lbSelectedComputers.Items.Strings[ lbSelectedComputers.ItemIndex ]) ]);
 if MessageDlg(xMsgTxt,  mtWarning, [mbYes, mbNo], 0) = mrYes then begin
    lbSelectedComputers.Items.Delete( lbSelectedComputers.ItemIndex);
    rgFilter1.OnClick(nil);
 end;

end;
//------------------------------------------------------------------------------
procedure TfMMPCNames.btnOKClick(Sender: TObject);
var xList : TStringList;
    x: integer;
begin

  if lbSelectedComputers.Items.Count <= 0 then begin
     MessageDlg(cMsgNoPCSelected,  mtWarning, [mbOK], 0);
     ModalResult := mrNone;
     Exit;
  end;

  if mMultipleComputersPermited then begin

    if lbSelectedComputers.Items.Count <= 0 then begin
      MessageDlg(cMsgNoPCsSelected ,  mtWarning, [mbOK], 0);
      ModalResult := mrNone;
    end else
      FMMPCNames := lbSelectedComputers.Items.CommaText;

  end else begin

    if lbSelectedComputers.Items.Count <= 0 then begin
      MessageDlg(cMsgNoPCSelected ,  mtWarning, [mbOK], 0);
      ModalResult := mrNone;
    end else begin
      if lbSelectedComputers.Items.Count > 1 then begin
        MessageDlg(cMsgOnlyOnePCSelect ,  mtWarning, [mbOK], 0);
        ModalResult := mrNone;
      end else
        FMMPCNames := lbSelectedComputers.Items.CommaText;
    end;
  end;

  xList := TStringList.Create;
  xList.CommaText :=  FMMPCNames;

  for x:= 0 to  xList.Count -1 do begin
    FMMPCNames := ExtractComputerInfo( xList.Strings[x] );
    xList.Strings[x] := FMMPCNames;
  end;

  FMMPCNames := xList.CommaText ;

  xList.Free;
end;
//------------------------------------------------------------------------------
procedure TfMMPCNames.lbSelectedComputersClick(Sender: TObject);
var xPCType : DWORD;
    xText, xPCName, xSQLTxt : string;
    xIsDC, xIsThisPC, xIsMMHost, xIsSQl : Boolean;
    xIndex : Integer;
    xDBAccess : TAdoDBAccess;
begin


  exit;
{

  xIsDC     := False;
  xIsThisPC := False;
  xIsMMHost := False;
  xIsSQl    := False;


  xIndex   := lbSelectedComputers.ItemIndex;
  xPCName  := lbSelectedComputers.Items.Strings[xIndex];

  xPCType  := GetServerInfo(xPCName);

//  xPCType  := DWORD(lbSelectedComputers.Items.Objects[xIndex]);


  //DC Server
  if (xPCType and SV_TYPE_DOMAIN_CTRL = SV_TYPE_DOMAIN_CTRL ) then xIsDC := True;

  //SQL Server
  if (xPCType and SV_TYPE_SQLSERVER = SV_TYPE_SQLSERVER ) then xIsSQl := True;


  //Dieser PC
  if mLocalMachineName = xPCName then xIsThisPC := True;

  try
     if xIsSQl then  begin
        //ActiveX.CoInitialize(nil);
        xDBAccess :=  TAdoDBAccess.Create(1);
        with xDBAccess do begin

          try
            Query[cPrimaryQuery].Close;

            HostName := xPCName;
            DBName   := 'master';

            if Init then begin

              xSQLTxt := 'select * FROM master.dbo.sysdatabases where name like ''MM_Winding'' ';
              Query[cPrimaryQuery].SQL.Text:= xSQLTxt;

              Query[cPrimaryQuery].Open;

              Query[cPrimaryQuery].First;

              xText := Query[cPrimaryQuery].FieldByName('name').AsString;
              if CompareText(xText, 'MM_winding') = 0 then
                 xIsMMHost := True;

              Query[cPrimaryQuery].Close;
            end;

          finally
            Free;
          end;
        end;
        //ActiveX.CoUninitialize;
     end;

  finally
  end;



  xText := '';
  if xIsThisPC and not xIsDC and not xIsMMHost then
     xText := Format('%s : [%s]', [xPCName, cThisPC ] );

  if not xIsThisPC and xIsDC and not xIsMMHost then
     xText := Format('%s : [%s]', [xPCName, cPDC ] );

  if not xIsThisPC and not xIsDC and xIsMMHost then
     xText := Format('%s : [%s]', [xPCName, cMMHost ] );



  if xIsThisPC and xIsDC and not xIsMMHost then
     xText := Format('%s : [%s, %s]', [xPCName, cThisPC, cPDC ] );

  if xIsThisPC and not xIsDC and xIsMMHost then
     xText := Format('%s : [%s, %s]', [xPCName, cThisPC, cMMHost ] );

  if not xIsThisPC and xIsDC and xIsMMHost then
     xText := Format('%s : [%s, %s]', [xPCName, cPDC, cMMHost ] );


  if xIsThisPC and xIsDC and xIsMMHost then
     xText := Format('%s : [ %s, %s, %s]',  [ xPCName, cThisPC, cPDC, cMMHost ] );

  status1.Panels[0].Font.Color:= clWindowText;
  status1.Panels[0].Text := xText;

  }

end;
//------------------------------------------------------------------------------
procedure TDomainsThread.ReadComputerNames;
begin
  MMBroadCastMessage(gMachineManagementID, WM_START_PROGRESSBAR);
  GetServers(mDomainName, SV_TYPE_NT, mMachines);
  mItsGetCoputerNames := false;
end;
//------------------------------------------------------------------------------
function TfMMPCNames.ExtractComputerInfo(aMachineName: String): string;
var xPCName :string;
begin
  xPCName := aMachineName;

  xPCName := StringReplace( xPCName, cThisPC, '', [rfReplaceAll] );
  xPCName := StringReplace( xPCName, cPDC, '', [rfReplaceAll] );
  xPCName := StringReplace( xPCName, cMMHost, '', [rfReplaceAll] );
  xPCName := StringReplace( xPCName, '[', '', [rfReplaceAll] );
  xPCName := StringReplace( xPCName, ']', '', [rfReplaceAll] );

  Result := Trim(xPCName);
end;
//------------------------------------------------------------------------------
destructor TfMMPCNames.Destroy;
begin
  EnterMethod('TfMMPCNames.Destroy() ');

  mDomainList.Free;
  mDomainList := nil;

  mPCFilterList.Free;
  mPCFilterList := nil;

  mPCList.Free;
  mPCList := nil;

  inherited;
end;

//------------------------------------------------------------------------------
initialization
  gMachineManagementID := RegisterWindowMessage(cMsgMachine);

end.
