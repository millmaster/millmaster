(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: BaseForm.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 14.10.2002  1.00  SDo | Report erstellt
| 12.02.2003  1.00  Wss | Loepfe Logo ersetzt
| 08.06.2004  1.01  SDO | Print-Layout angepasst  -> Seitenumbruch sollte nochmals überarbeitet werden !
|=============================================================================*)
unit u_SecurityReport;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, QuickRpt, ExtCtrls, mmQuickRep, Qrctrls, mmQRShape, mmQRBand,
  mmQRLabel, mmQRSubDetail, mmQRGroup, u_MMConfigSecurity, mmQRSysData,
  IvDictio, IvMulti, IvEMulti, mmTranslator, mmQRImage;

type
  TSecurityReport = class(TmmForm)
    qrSecurityreport: TmmQuickRep;
    qbDetail_Header: TmmQRBand;
    qlDetailItem: TmmQRLabel;
    qlFunctionLabel: TmmQRLabel;
    qlHeaderDetailAccess1: TmmQRLabel;
    qlGroupItem: TmmQRLabel;
    qbSubDetail: TmmQRSubDetail;
    qbPageHeader: TmmQRBand;
    qlTitle1: TQRSysData;
    qlViewText: TmmQRLabel;
    QRShapeGray: TQRShape;
    qlFunction: TmmQRLabel;
    qlAccess: TmmQRLabel;
    qlDefaultAccess: TmmQRLabel;
    qlDefault: TmmQRLabel;
    qlActualAccess: TmmQRLabel;
    qlHeaderDetailAccess2: TmmQRLabel;
    PageFooterBand1: TQRBand;
    qlDetailItemAdditional: TmmQRLabel;
    mmTranslator1: TmmTranslator;
    mmQRImage1: TmmQRImage;
    qlPageValue: TmmQRSysData;
    mmQRSysData1: TmmQRSysData;
    qlCompanyName1: TmmQRLabel;
    procedure qrSecurityreportNeedData(Sender: TObject;
      var MoreData: Boolean);
    procedure qbDetail_HeaderBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure qbPageHeaderBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure qrSecurityreportEndPage(Sender: TCustomQuickRep);
    procedure qrSecurityreportStartPage(Sender: TCustomQuickRep);
    procedure qbSubDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure qrSecurityreportBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    fShowProgramms : Boolean;

    mPage, {mTotalPage,} mTotalPageGroups : Integer;
    mCounter : Integer;
    mDetailCounter  : Word;
    mPrintDetailHeader : Boolean;
    mNewPage, mMaxPageCounted  : Boolean;
    mData: TSecurityList;
    mUserGroupCount : Integer;
    mGroupItem : String;

    mGroupPos : TStringList;
    mGrpHeight   : Integer;
    mReportGroupCounter : Integer;
    mSpaceFirstPage : Word;
    mSpaceSecPage   : Word;


    procedure SetShowProgramms(const Value: Boolean);
    procedure Init;
    procedure PreparePage;
    function ForceNewPage(aPos, aGroupNr:Integer ):Boolean;

  public
    { Public declarations }
    constructor Create(aOwner: TComponent; aData : TSecurityList); overload;
    property ShowProgramms : Boolean Read fShowProgramms write SetShowProgramms;

  end;

var
  SecurityReport: TSecurityReport;


Resourcestring
     rsFunction  = '(*)Funktion';  //ivlm
     rsGroups    = '(*)Benutzergruppen';  //ivlm


implementation

{$R *.DFM}
uses SettingsReader;


constructor TSecurityReport.Create(aOwner: TComponent;
  aData: TSecurityList);
begin
  inherited Create(aOwner);
  mData := NIL;
  mData       := aData;
  mUserGroupCount := mData.GroupCount;
  if mUserGroupCount <=0 then close;
  fShowProgramms := FALSE;

  //mTotalPage:= 1;
  mMaxPageCounted:= FALSE;
  mGroupPos := TStringList.Create;
  mGrpHeight   := 0;

  init;
end;
//------------------------------------------------------------------------------
procedure TSecurityReport.qrSecurityreportNeedData(Sender: TObject;
  var MoreData: Boolean);
var xText, xText1, xGroupItem : String;
    //xPage : Integer;
begin
  inherited;

  if (mCounter >= mData.Count) or
     (mData = NIL) or (mData.Count = 0) then begin
     MoreData := FALSE;
     exit;
  end else
     MoreData := TRUE;


  if mCounter = 0 then begin
     mPrintDetailHeader := TRUE
  end else begin
     mPrintDetailHeader := FALSE;
  end;

  qlDetailItemAdditional.Caption := '';


  //Liste Programme
  if fShowProgramms then begin

     qlFunctionLabel.Caption :=  rsGroups;

     xGroupItem             :=  mData.Items[mCounter].Appl +  ' / ' +
                                mData.Items[mCounter].FormCaption;

     qlGroupItem.Caption    :=  xGroupItem;

     qlFunction.Caption     :=  mData.Items[mCounter].SecurityGroup;
     qlDetailItem.Caption   :=  mData.Items[mCounter].Info;

     //Print DetailBand
     if (mDetailCounter = mUserGroupCount) then begin
         mPrintDetailHeader := TRUE;
         mDetailCounter     := 0;
         if mGroupItem = xGroupItem then
            qlGroupItem.Caption  := '';
     end;
     mGroupItem := xGroupItem;
  end else begin
     //Liste MMGruppen

     //mTotalPage:= mTotalPageGroups;

     qlFunctionLabel.Caption :=  rsFunction;

     xGroupItem             :=  mData.Items[mCounter].SecurityGroup;
     qlGroupItem.Caption    :=  xGroupItem;

     qlFunction.Caption     :=  mData.Items[mCounter].Info;

     qlDetailItem.Caption   :=  mData.Items[mCounter].Appl +  ' / ';


     if mCounter = 0  then
        qlDetailItemAdditional.Caption := mData.Items[mCounter].FormCaption;


     if mCounter >= 1 then begin

        xText  := mData.Items[mCounter].Appl +  ' / ' + mData.Items[mCounter].FormCaption;
        xText1 := mData.Items[mCounter-1].Appl +  ' / ' + mData.Items[mCounter-1].FormCaption;

        UpperCase(xText);
        UpperCase(xText1);

        //Print DetailBand
        if (xText <> xText1) then begin
            qlDetailItemAdditional.Caption := mData.Items[mCounter].FormCaption;
            if mGroupItem = xGroupItem then qlGroupItem.Caption  := '';

            mPrintDetailHeader := TRUE;

            mDetailCounter     := 0;
        end;
     end;

     mGroupItem := xGroupItem;
  end;

     case mData.Items[mCounter].SecurityLevel of
       0: xText := rsNoAccess;
       1: xText := rsVisAccess;
       2: xText := rsFullAccess;
     else
       xText := rsNoAccess;
     end;
     qlAccess.Caption := xText;

     case mData.Items[mCounter].DefaultLevel of
       0: xText := rsNoAccess;
       1: xText := rsVisAccess;
       2: xText := rsFullAccess;
     else
       xText := rsNoAccess;
     end;
     qlDefaultAccess.Caption := xText;

 // qrTotalPage.Caption:= IntToStr( mTotalPage );

  inc(mCounter);
  inc(mDetailCounter);

end;
//------------------------------------------------------------------------------
procedure TSecurityReport.qrSecurityreportStartPage(
  Sender: TCustomQuickRep);
begin
  inherited;
end;
//------------------------------------------------------------------------------
procedure TSecurityReport.qrSecurityreportEndPage(Sender: TCustomQuickRep);
begin
  inherited;
  inc(mPage);
  if not mMaxPageCounted then begin
     //inc(mTotalPage);
     mTotalPageGroups := mPage -1;
  end;
//  qlTitle1.Text := qlTitle.Text;
 // qlViewText1.Caption := qlViewText.Caption;
end;
//------------------------------------------------------------------------------
procedure TSecurityReport.qbPageHeaderBeforePrint(Sender: TQRCustomBand;
 var PrintBand: Boolean);
begin
  inherited;
 {
  if mPage > 1 then
     PrintBand := TRUE
  else
     PrintBand := FALSE;
 }
end;
//------------------------------------------------------------------------------
procedure TSecurityReport.qbDetail_HeaderBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  if mPrintDetailHeader then begin

     PrintBand := TRUE;

     if mMaxPageCounted = FALSE  then begin
        mGroupPos.Add( IntToStr(mGrpHeight) );
     end else
        //Neue Seite, wenn Gruppe nicht mehr auf Seite platz hat

        if fShowProgramms then
           if ForceNewPage(mGrpHeight, mReportGroupCounter ) then begin
              inc(mReportGroupCounter);
              qrSecurityreport.NewPage;
           end;

     Inc(mGrpHeight, qbDetail_Header.Height);
     inc(mReportGroupCounter);

     //Inc(mGrpHeight);
  end else
     PrintBand := FALSE;     
end;
//------------------------------------------------------------------------------
procedure TSecurityReport.qbSubDetailBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;

  with QRShapeGray.Brush do
    if Color = $00F0F0F0 then
       Color := $00E0E0E0
    else
       Color := $00F0F0F0;

  Inc(mGrpHeight, qbSubDetail.Height + 2) ;
end;
//------------------------------------------------------------------------------
procedure TSecurityReport.SetShowProgramms(const Value: Boolean);
begin
  FShowProgramms := Value;
end;
//------------------------------------------------------------------------------
procedure TSecurityReport.qrSecurityreportBeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
var y: String;
    xPages : TStringList;
    x, xValue, xNewValue, xMaxPages, xTempValue, n : Integer;
    xTxt :String;
begin
  inherited;

  //Anhand der Gruppen Position(Pixel) auf einer Seite wird der Seitenumbruch
  //berechnet. Eine Gruppe = qbDetail_Header-Band +  x qbSubDetail-Baender
  //-> in mGroupPos steht die Position in Pixel des letzten qbSubDetail
  //-> Richtger Seitenumbruch ist nur bei den Programmen. Bei den Gruppen
  //   ist es Zufall wenn diese stimmmt. (Orakeln mit Pos. Offset ??!!!) Darum
  //   wird beim Seitenausdruck der Gruppen die Func. ForceNewPage nicht aufgerufen

//  if mTotalPage > 1 then begin
  if mPage > 1 then begin
     mMaxPageCounted:= TRUE;
     mGroupPos.Add( IntToStr(mGrpHeight) );

     xPages   := TStringList.Create;
     xNewValue := mSpaceFirstPage;
     xTxt :=  mGroupPos.CommaText;


     for x:= 0 to mGroupPos.count -1 do begin
         xValue :=  StrToInt( mGroupPos.Strings[x] );

         if xValue >= xNewValue then begin

            xPages.Add( IntToStr(x-1) );
            xNewValue := StrToInt( mGroupPos.Strings[x-1] ) + mSpaceSecPage;

         end;
     end;

     //xPages.Add( IntToStr( mGroupPos.Count + 1) );


     //Korrektur letzete Seite
     xMaxPages :=1;
     xTempValue:= StrToInt(mGroupPos.Strings[mGroupPos.count -1]);



     if xTempValue MOD mSpaceFirstPage > 0 then
        xMaxPages :=  xTempValue DIV mSpaceFirstPage
     else
        xMaxPages := xTempValue MOD mSpaceFirstPage ;


     inc(xMaxPages);

    // mTotalPage := xMaxPages;

    // qrTotalPage.Caption :=  IntToStr(mTotalPage);

     mGroupPos.CommaText := xPages.CommaText;
     mGroupPos.Sort;
     xPages.free;
  end else PreparePage;
  Init;


  try
   qlCompanyName1.Caption := TMMSettingsReader.Instance.Value[cCompanyName];
  except
   qlCompanyName1.Caption :='';
  end;


end;
//------------------------------------------------------------------------------
procedure TSecurityReport.Init;
begin
  mPage := 1;
  mCounter:=0;
  mDetailCounter:=0;
  mReportGroupCounter :=0;

  mPrintDetailHeader := TRUE;
  mNewPage:= FALSE;
  mGroupItem:= '';
  mGrpHeight :=0;
end;
//------------------------------------------------------------------------------
procedure TSecurityReport.PreparePage;
var xWidth : Integer;
begin

  xWidth := qbSubDetail.Width;

//  mmQRShape1.Width  := xWidth;
//  mmQRShape2.Width  := xWidth;
//  mmQRShape2.Width  := xWidth;
  //mmQRImage1.Left     := xWidth - QRImage1.Width - 10;
  //mmQRImage1.Left     := xWidth - QRImage2.Width - 10;
//  QRShapeGray.Width := xWidth - QRShapeGray.Left;
  QRShapeGray.Left := qlFunction.Left;
  QRShapeGray.Width :=  xWidth - QRShapeGray.Left;

  //Seiten Nummerierung
//  qlActualPage.Left := xWidth - 20;

(*
  mSpaceFirstPage := qrSecurityreport.Height  {qrbTitle.Height -}
                     {qbGroupHeader.Height} - 85 - PageFooterBand1.Height;

  mSpaceSecPage   := qrSecurityreport.Height - qbPageHeader.Height -
                     PageFooterBand1.Height ;

*)

  mSpaceFirstPage := qrSecurityreport.Height - PageFooterBand1.Height - 50;

  mSpaceSecPage :=  mSpaceFirstPage;

  mmTranslator1.Translate;
end;
//------------------------------------------------------------------------------
procedure TSecurityReport.FormDestroy(Sender: TObject);
begin
  inherited;
  mGroupPos.free;
end;
//------------------------------------------------------------------------------
function TSecurityReport.ForceNewPage(aPos, aGroupNr:Integer ):Boolean;
var xPageSpace, xDiff, xGroupPos, x, xValue : Integer;
begin
  Result := FALSE;

  if not mMaxPageCounted then exit;

  if mGroupPos.IndexOf( IntToStr( aGroupNr ) ) > -1 then
     Result := TRUE;

end;
//------------------------------------------------------------------------------
procedure TSecurityReport.FormCreate(Sender: TObject);
begin
  inherited;
  qlPageValue.Text := Translate(qlPageValue.Text) + ' ' ;
end;



end.
