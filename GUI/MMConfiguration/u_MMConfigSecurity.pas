{===============================================================================
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: 
| Info..........: 
|
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 12.09.2002  1.00  SDo | File created
| 27.01.2004  1.01  SDO | Ubersetzung von MM-Security-Appls
|                       | TMMSecurityRec mit TranslAppl erweitert;
|                       | Aenderungen: FillSecurityList(),  ShowData(),
|                       |              GetApplicationForms(), Translate()
|                       | Neu: SetMMPackage(), GetApplicationsTranslated()
| 06.07.2006  1.02  SDO | Fehler : Loeschen der Appl. 'MMMaConfig' aus den
|                       |          Tabellen t_security_Work, t_security_xy
|                       |          fuer das Package EASY behoben.
|                       |          -> wird jetzt nicht mehr geloescht.
|                       |          siehe Func. SetMMPackage()
===============================================================================}

unit u_MMConfigSecurity;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, mmlist, StdCtrls, 
  Grids, AdvGrid, mmExtStringGrid,  
  mmADOConnection, mmADOQuery, ADODB, mmEventLog, SettingsReader;

type

  TMMSecurityRec = record
     Appl              : String;  // Programm
     ApplForm          : String;  // Form-Name
     ApplFunction      : String;  // Action-Name
     FormCaption       : String;  // Caption einer Form (Native)
     Info              : String;  // Beschreibung der Funktion (Native)
     SecurityGroup     : String;  // Security Gruppen
     SecurityLevel     : Integer; // Security Level (0..2)  
     DefaultLevel      : Integer; // Default Security Level (0..2)
     DBRecordMarker    : Boolean; // Markiert einen DB-Record; Darf nicht geloescht werden
     SecurityString    : String;  // Security Gruppe mit Security Level
     TranslFormCaption : String;  // Caption einer Form (Taranslate)
     TranslInfo        : String;  // Beschreibung der Funktion (Taranslate)
     TranslAppl        : String;  // Uebersetzung der Appl.
  end;
  pSecurityRec = ^TMMSecurityRec;
 
  TMMFormRec = record
    NativeCaption     : String;  
    TranslateCaption  : String;   
  end;
  pFormCaptionRec = ^TMMFormRec;
   
 
  TSecurityList = class (TMMList)
  private
    mGroupCount: Integer;
    function GetGroupCount: Integer;
    function GetGroups: String;
    function GetItems(aIndex: Integer): pSecurityRec;
    procedure SetItems(aIndex: Integer; Value: pSecurityRec);
  public
    procedure Clear; override;
    procedure Free; virtual;
    property GroupCount: Integer read GetGroupCount;
    property Groups: String read GetGroups;
    property Items[aIndex: Integer]: pSecurityRec read GetItems write SetItems;
  end;


  TBaseMMSecurity = class (TObject)
  private
    FCount: Integer;
    FHasSecurityEntries: Boolean;
    FIsAdoConnected: Boolean;
    FMMApplications: String;
    FSecurityList: TSecurityList;
    mPackage : TMMPackage;

    function GetADOConnection: TmmADOConnection;
    function GetDefaultLevel(aAppl, aForm, aFunction : String): Integer;
    function GetSecurityGrid: TmmExtStringGrid;
    procedure Save(aTable:String);
    procedure Save_notused;
    procedure SetADOConnection(Value: TmmADOConnection);
    procedure SetSecurityGrid(Value: TmmExtStringGrid);
    procedure SetSetMMUsergroup(Value: String);
    procedure SetMMPackage(const Value: TMMPackage);
    function GetApplicationsTranslated: String;
  protected
    mActualSecurityTab: String;
    mADOQuery: TmmADOQuery;
    mBackupSecurityTab: String;
    mDefaultSecurityTab: String;
    mEventLog: TEventLogWriter;
    mSecurityGrid: TmmExtStringGrid;
    mTable: String;
    constructor Create(aTable : String); overload; virtual;
    procedure FillSecurityList; virtual;
    procedure Insert(aTable, aAppl, aForm, aFunction, aCaption, aInfo, aSecurity: String); virtual;
    procedure Update(aAppl, aForm, aFunction, aSecurity : String); virtual;
  public
    destructor Destroy; override;
    procedure AddUserGroup(aGroup : String); virtual;
    procedure DeleteGroup(aGroup : String); virtual;
    function GetApplicationForms(aApplication:String): String; virtual;
    function GetUserGroups: String; virtual;
    function Init: Boolean; virtual;
    procedure Load; virtual;
    procedure SaveAsActual; virtual;
    procedure SaveAsBackup; virtual;
    procedure SaveAsDefault; virtual;
    procedure ShowData(aUserGroup : String); overload; virtual;
    procedure ShowData(aApplication, aForm : String); overload; virtual;
    procedure Translate; virtual;
    property ADOConnection: TmmADOConnection read GetADOConnection write SetADOConnection;
    // 1 Item Z�hler der aufbereiteten Records aus dem Query f�r das Grid 
    property Count: Integer read FCount default 0;
    property HasSecurityEntries: Boolean read FHasSecurityEntries;
    // 1 Datenbank Verbindungsabfrage 
    property IsAdoConnected: Boolean read FIsAdoConnected;
    // 1 Gibt alle Applikationen als CommaText zur�ck 
    property MMApplications: String read FMMApplications;
    property MMApplicationsTranslated: String read GetApplicationsTranslated;
    property SecurityGrid: TmmExtStringGrid read GetSecurityGrid write SetSecurityGrid;
    property SecurityItem: TSecurityList read FSecurityList write FSecurityList;
    property SetMMUsergroup: String write SetSetMMUsergroup;

    property MMPackage : TMMPackage write  SetMMPackage;
  end;
  

  TDefaultSecurity = class (TBaseMMSecurity)
  public
    constructor Create; overload; virtual;
    function Init: Boolean; override;
  end;
  

  TBackupSecurity = class (TBaseMMSecurity)
  public
    constructor Create; overload; virtual;
    function Init: Boolean; override;
  end;
  

  TActualSecurity = class (TBaseMMSecurity)
  public
    constructor Create; overload; virtual;
    function Init: Boolean; override;
  end;




procedure InitSecurityGrid(var aGrid: TmmExtStringGrid);
procedure TranslateHeader(var aGrid: TmmExtStringGrid);

//Sortiert die TSecurityList nach Function -> c_info und Benutzergruppem -> SecurityGroup
function Sort_Appl_View(Item1, Item2: Pointer): Integer;
function Sort_Group_View(Item1, Item2: Pointer): Integer;

//Security Report
function  Sort_Appls(Item1, Item2: Pointer): Integer;
function  Sort_Groups(Item1, Item2: Pointer): Integer;

//procedure WriteSecurityLevelToList(aSecurLevel : Integer;  );
procedure InitSecurity;

var
  gSecurityHeader, gSecurityMMGroups: String;
  gCellRadioButton:TStringlist;
  gBmp : TBitmap;
  
  gActiveSecurityClass : TBaseMMSecurity;
  gActualSecurity      : TActualSecurity;
  gDefaultSecurity     : TDefaultSecurity;
  gBackupSecurity      : TBackupSecurity;
  
Resourcestring

     //Security Header-Text
     rsSecurityFunction  = '(*)Funktion';  //ivlm
     rsSecurityMMGroups0 = '(*)Programm';  //ivlm
     rsSecurityMMGroups1 = '(*)Fenster';   //ivlm
     rsSecurityMMAppl = '(*)Benutzergruppen';   //ivlm

     rsAccess     = '(*)Zugriff';  //ivlm
     rsNoAccess   = '(*)Kein';     //ivlm
     rsVisAccess  = '(*)Sichtbar'; //ivlm
     rsFullAccess = '(*)Voll';     //ivlm
     
     rsRemarkText       = '(*)Standard Sicherheitseinstellung der MillMaster-Gruppe'; //ivlm
     
const
   cSecurityTable = 't_security_Work';    
   cSecurityDefaultTable = 't_security_default';
   cSecurityCustomerTable = 't_security_customer';

   cGetAllData   = 'Select * from %s';
   cGetApplData  = 'Select * from %s where where c_Appl = %s';
   cGetGroupData = 'Select * from t_security_default where c_Security like ''%s%'' ';
   cGetAllAppl   = 'Select distinct(c_Appl) from %s';
   cGetApplForms = 'Select distinct(c_Caption) from %s where c_Appl = ''%s'' ';

   cDeleteTable  = 'delete %s ';

   cUpdate       = 'update %s set c_Security = ''%s'' ' +
                   'where c_Appl = ''%s'' and c_Form = ''%s'' and c_Function = ''%s'' ';

   cInsert       = 'Insert %s (c_Appl, c_Form, c_Function, c_Caption, c_Info, c_Security ) ' +
                   'Values (''%s'', ''%s'', ''%s'', ''%s'', ''%s'', ''%s'') ';

   cGetDefaultSecur = 'Select c_security from t_security_default ' +
                      'where c_Appl=''%s'' and c_Form=''%s'' and c_Function=''%s'' ';

   cGetDefaultSecur1= 'Select Distinct(c_security) from t_security_Work ' +
                      'where c_Appl=''%s'' and c_Form=''%s'' and c_Function=''%s'' ';

   cDefaultUserGroup = 'MMUserGroup';
   cDefaultMarker   =' *';
   
implementation

uses LoepfeGlobal, RzCSIntf, IvDictio, IvMulti, IvEMulti, mmTranslator, BaseGlobal;


//******************************************************************************
// Initialisiert die SequrityGrids (esgGroupSecurity & esgApplSecurity)
//******************************************************************************
procedure InitSecurityGrid(var aGrid: TmmExtStringGrid);
var x, xRbIndex, xYHeaderOffset: Integer;
begin
  with aGrid do begin
       Multilinecells:= TRUE;
       FixedRowHeight := 30;
       EnableGraphics := TRUE;
       Options:= Options + [goRowSelect, goColSizing, goDrawFocusSelected] -
                 [goRangeSelect];

       ScrollBars:= (ssVertical);
              
       ColCount:= 8;
       HideColumn(3); //Appl
       HideColumn(4); //ApplForm
       HideColumn(5); //ApplFunction
       HideColumn(6); //SecurityGroup
       HideColumn(7); //FormCaption
       HideColumn(8); //TranslateAppl
  end;  
end;
//------------------------------------------------------------------------------
procedure TranslateHeader(var aGrid: TmmExtStringGrid);
var x, xRbIndex, xYHeaderOffset: Integer;
begin
  gSecurityMMGroups := Translate(rsSecurityMMGroups0) + ' /' + #13 + ' ' +
                       Translate(rsSecurityMMGroups1);
       
   if aGrid.Name = 'esgApplSecurity' then begin
      aGrid.Cells[0,0]:= Translate(rsSecurityFunction);
      aGrid.Cells[1,0]:= Translate(rsSecurityMMAppl);
   end;

   if aGrid.Name = 'esgGroupSecurity' then begin
      aGrid.Cells[0,0]:= Translate(gSecurityMMGroups);
      aGrid.Cells[1,0]:= Translate(rsSecurityFunction);
   end;

  xRbIndex:=-1;
  xYHeaderOffset := 14;
  
  gBmp := TBitmap.Create;
      
  with gBmp do begin
       gBmp.FreeImage;
       Canvas.Brush.Color := aGrid.FixedColor;
       
       Height := aGrid.FixedRowHeight-1;
       Width  := aGrid.ColWidths[2]-1;
       
       Canvas.Font:= aGrid.Font;
       Canvas.TextOut(  2,0, Translate(rsAccess));

       Canvas.TextOut(  2, xYHeaderOffset, Translate(rsNoAccess));

       x:= length(gCellRadioButton.Strings[0]) * aGrid.Font.Size + 13 ;
       Canvas.TextOut( x, xYHeaderOffset, Translate(rsVisAccess));

       x:= length(gCellRadioButton.Strings[1]) * aGrid.Font.Size + x + 13 ;
       Canvas.TextOut(x, xYHeaderOffset, Translate(rsFullAccess));
         
       aGrid.RemoveBitmap(2,0);
       aGrid.AddBitmap(2,0, gBmp, TRUE, haLeft, vaTop);

       for x:=1 to aGrid.Rowcount-1 do
           aGrid.Addradio(2, x, 1, xRbIndex, gCellRadioButton);
  end; //END With
end;


//******************************************************************************
// Sortiert die TSecurityList nach Function -> c_info und 
// Benutzergruppen -> SecurityGroup
//******************************************************************************
function  Sort_Appl_View(Item1, Item2: Pointer): Integer;
begin

  // 2 Spalten sortiern
  if TMMSecurityRec(Item1^).ApplFunction = TMMSecurityRec(Item2^).ApplFunction  then begin

      if TMMSecurityRec(Item1^).SecurityGroup < TMMSecurityRec(Item2^).SecurityGroup  then
         result := -1;

      if TMMSecurityRec(Item1^).SecurityGroup > TMMSecurityRec(Item2^).SecurityGroup  then
         result := 1;

      if TMMSecurityRec(Item1^).SecurityGroup = TMMSecurityRec(Item2^).SecurityGroup  then
         result := 0;
  end else begin
      if TMMSecurityRec(Item1^).ApplFunction < TMMSecurityRec(Item2^).ApplFunction  then
         result := -1;

      if TMMSecurityRec(Item1^).ApplFunction > TMMSecurityRec(Item2^).ApplFunction  then
         result := 1;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Sortiert die TSecurityList nach Programm -> c_Appl, 
// FormCaption -> c_Caption und Function -> c_Info
//******************************************************************************
function Sort_Group_View(Item1, Item2: Pointer): Integer;
begin

  // 3 Spalten sortiern
  //Programm sort ok
  if TMMSecurityRec(Item1^).Appl = TMMSecurityRec(Item2^).Appl  then begin

      if TMMSecurityRec(Item1^).FormCaption < TMMSecurityRec(Item2^).FormCaption  then
         result := -1;

      if TMMSecurityRec(Item1^).FormCaption > TMMSecurityRec(Item2^).FormCaption  then
         result := 1;

      //Programm & FormaCaption sort ok         
      if TMMSecurityRec(Item1^).FormCaption = TMMSecurityRec(Item2^).FormCaption  then begin

         //Programm & FormaCaption & Info sort ok
         if TMMSecurityRec(Item1^).Info = TMMSecurityRec(Item2^).Info  then
            result := 0
         else begin
            if TMMSecurityRec(Item1^).Info < TMMSecurityRec(Item2^).Info  then
               result := -1;
               
            if TMMSecurityRec(Item1^).Info > TMMSecurityRec(Item2^).Info  then
               result := 1;         
         end;   
      end;
  end else begin
      if TMMSecurityRec(Item1^).Appl < TMMSecurityRec(Item2^).Appl  then
         result := -1;

      if TMMSecurityRec(Item1^).Appl > TMMSecurityRec(Item2^).Appl  then
         result := 1;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Sortiert die TSecurityList nach Appl., Function  & Gruppen 
//******************************************************************************
function  Sort_Appls(Item1, Item2: Pointer): Integer;
begin
  //Appl sortiert
  if TMMSecurityRec(Item1^).Appl = TMMSecurityRec(Item2^).Appl then begin
     //Function sortiert
     if TMMSecurityRec(Item1^).Info = TMMSecurityRec(Item2^).Info  then begin
        //Group sortiert
        if TMMSecurityRec(Item1^).SecurityGroup = TMMSecurityRec(Item2^).SecurityGroup  then
           result := 0 
        else 
           if TMMSecurityRec(Item1^).SecurityGroup < TMMSecurityRec(Item2^).SecurityGroup  then
              result := -1
           else   
              if TMMSecurityRec(Item1^).SecurityGroup > TMMSecurityRec(Item2^).SecurityGroup  then
                 result := 1;

     end else
        if TMMSecurityRec(Item1^).Info < TMMSecurityRec(Item2^).Info  then
           result := -1 else
           if TMMSecurityRec(Item1^).Info > TMMSecurityRec(Item2^).Info  then
              result := 1;
  
  end else begin
      if TMMSecurityRec(Item1^).Appl < TMMSecurityRec(Item2^).Appl  then
         result := -1 else
         if TMMSecurityRec(Item1^).Appl > TMMSecurityRec(Item2^).Appl  then
            result := 1;  
  end;
  Application.ProcessMessages;
end;
//------------------------------------------------------------------------------


//******************************************************************************
// Sortiert die TSecurityList nach Gruppen, Appl., FormaCaptions & Function
//******************************************************************************
function  Sort_Groups(Item1, Item2: Pointer): Integer;
begin
  //Gruppe sortiert
  if TMMSecurityRec(Item1^).SecurityGroup = TMMSecurityRec(Item2^).SecurityGroup then begin
     //Appl sortiert
     if TMMSecurityRec(Item1^).Appl = TMMSecurityRec(Item2^).Appl  then begin
        //FormCaption sortiert
        if TMMSecurityRec(Item1^).FormCaption = TMMSecurityRec(Item2^).FormCaption  then begin
           //Function sortiert
           if TMMSecurityRec(Item1^).Info = TMMSecurityRec(Item2^).Info  then
              result := 0
           else
              if TMMSecurityRec(Item1^).Info < TMMSecurityRec(Item2^).Info  then
                 result := -1 else
                 if TMMSecurityRec(Item1^).Info > TMMSecurityRec(Item2^).Info  then
                    result := 1;         
                  
        end else        
            if TMMSecurityRec(Item1^).FormCaption < TMMSecurityRec(Item2^).FormCaption  then
               result := -1 else
               if TMMSecurityRec(Item1^).FormCaption > TMMSecurityRec(Item2^).FormCaption  then
                  result := 1;
        
     end else 
        if TMMSecurityRec(Item1^).Appl < TMMSecurityRec(Item2^).Appl  then
           result := -1 else
           if TMMSecurityRec(Item1^).Appl > TMMSecurityRec(Item2^).Appl  then
              result := 1;
  
  end else begin
      if TMMSecurityRec(Item1^).SecurityGroup < TMMSecurityRec(Item2^).SecurityGroup  then
         result := -1 else
         if TMMSecurityRec(Item1^).SecurityGroup > TMMSecurityRec(Item2^).SecurityGroup  then
            result := 1;  
  end;
  Application.ProcessMessages;
end;
//------------------------------------------------------------------------------
procedure InitSecurity;
var xOffset, x : integer;
    xSpace : String;
begin

  //Text fuer Header in MMSecurityAppl & MMSecurityMMGroups
  gSecurityMMGroups := Format('%s \' + cCRLF + '%s', [rsSecurityMMGroups0, rsSecurityMMGroups1]);
//  gSecurityMMGroups := rsSecurityMMGroups0 + ' \' + #13 + ' ' + rsSecurityMMGroups1;

  xOffset := 12;
  gCellRadioButton := TStringlist.Create;

  //Die Spaces d�rfen nicht gleich lang sein, weil bei einem
  //Grid-Radiobutton-Click alle drei Rb's markiert werden ???
  xSpace:= '';
  for x:= 0 to xOffset   do
      xSpace := xSpace + ' ';
  gCellRadioButton.Add(xSpace);

  xSpace:= '';
  for x:= 0 to xOffset-1   do
      xSpace := xSpace + ' ';
  gCellRadioButton.Add(xSpace);

  xSpace:= '';
  for x:= 0 to xOffset-2   do
      xSpace := xSpace + ' ';
  gCellRadioButton.Add(xSpace);
end;

//------------------------------------------------------------------------------
procedure TSecurityList.Clear;
begin
  while Count > 0 do begin
      Dispose(Items[0]);
      Delete(0);
  end;
  inherited Clear;
end;// TSecurityList.Clear cat:Undefined

//------------------------------------------------------------------------------
procedure TSecurityList.Free;
begin
  inherited Free;
end;// TSecurityList.Free cat:Undefined

//------------------------------------------------------------------------------
function TSecurityList.GetGroupCount: Integer;
begin
  Groups;
  Result := mGroupCount;
end;// TSecurityList.GetGroupCount cat:Undefined

//------------------------------------------------------------------------------
function TSecurityList.GetGroups: String;
  
  var x     : Integer;
      xList : TStringList;
  
begin
  xList := TStringList.create;
  xList.Sorted:= TRUE;
  xList.Duplicates:= dupIgnore;
  
  for x:= 0 to Count-1 do
      xList.Add(Items[x].SecurityGroup);
  
  mGroupCount := xList.count;
  Result := xList.CommaText;
  
  xList.Free;
end;// TSecurityList.GetGroups cat:Undefined

//------------------------------------------------------------------------------
function TSecurityList.GetItems(aIndex: Integer): pSecurityRec;
begin
  Result := inherited Items[aIndex];
end;// TSecurityList.GetItems cat:Undefined

//------------------------------------------------------------------------------
procedure TSecurityList.SetItems(aIndex: Integer; Value: pSecurityRec);
begin
  //Iems[aIndex].Appl := 'xx';
end;// TSecurityList.SetItems cat:Undefined


//------------------------------------------------------------------------------
constructor TBaseMMSecurity.Create(aTable : String);
begin
  inherited Create;

{----------------------------------------------}
  mEventLog := TEventLogWriter.Create(cEventLogClassForSubSystems,
                                      cEventLogServerNameForSubSystems,
                                      ssApplication, 'MMConfiguration: ', True);
  
  mActualSecurityTab   := cSecurityTable;
  mDefaultSecurityTab  := cSecurityDefaultTable;
  mBackupSecurityTab   := cSecurityCustomerTable;
  
  FSecurityList := TSecurityList.Create;
  mTable := aTable;
  mADOQuery := TmmADOQuery.Create(NIL);
  FCount:=0;
  FIsAdoConnected := FALSE;
  FHasSecurityEntries := FALSE;
  mSecurityGrid := NIL;
end;// TBaseMMSecurity.Create cat:Undefined

//------------------------------------------------------------------------------
destructor TBaseMMSecurity.Destroy;
begin
  FSecurityList.Free;
  mADOQuery.Active:= FALSE;
  mADOQuery.Free;
  mEventLog.Free;

{----------------------------------------------}
  inherited Destroy;
end;// TBaseMMSecurity.Destroy cat:Undefined

//------------------------------------------------------------------------------
procedure TBaseMMSecurity.AddUserGroup(aGroup : String);

  var xRec         : pSecurityRec;
      xCount, x, n : Integer;
      xGroupList   : TStringlist;
      xTemp        : String;
      xSecurLevel  : Integer;

      xNewGroups : TSecurityList;
      xAppl, xForm, xFunction, xCaption, xInfo, xSecur, xSecurTemp : String;
      xADOQuery : TmmADOQuery;

      xFound : Boolean;

begin
    if aGroup = '' then exit;

    xNewGroups := TSecurityList.create;
    xADOQuery  := TmmADOQuery.create(nil);

    xADOQuery.Connection := mADOQuery.Connection;

    //List-Record loeschen, welcher keine Security-Gruppe besitzt
    //-> Minimale Anzahl List-Records = Anzahl DB-Records
    x:=0;
    while x < FSecurityList.count do begin
        if FSecurityList.Items[x].SecurityGroup = '' then
           if FSecurityList.Items[x].DBRecordMarker = FALSE then begin
              xRec :=  FSecurityList.Items[x];
              Dispose( xRec );
              FSecurityList.Delete(x);
              x:= 0;
            end else begin
              FSecurityList.Items[x].SecurityGroup := '';
              FSecurityList.Items[x].SecurityLevel := -1;
            end;
        inc(x);
    end;

    //Security Daten von DB einlesen
    with mADOQuery do begin
         Close;
         SQL.text:= Format(cGetAllData, [mTable]);
         Open;
         First;

         xGroupList := TStringList.create;
         xGroupList.Sorted := TRUE;

         for xCount := 0 to RecordCount-1 do begin
             xFound    := FALSE;
             if EOF then break;

             xAppl     := FieldByName('c_Appl').asString;
             xForm     := FieldByName('c_Form').asString;
             xFunction := FieldByName('c_Function').asString;
             xCaption  := FieldByName('c_Caption').asString;
             xInfo     := FieldByName('c_Info').asString;
             xSecur    := FieldByName('c_Security').asString;

             //Standard-Wert von MMUsergroup der neuen Gruppe zuweisen
             xADOQuery.Close;
             xADOQuery.SQL.Text := Format(cGetDefaultSecur, [xAppl, xForm, xFunction]);
             xADOQuery.Open;
             xADOQuery.First;
             xGroupList.commatext :=  xADOQuery.FieldByName('c_Security').asString;

             //Securitylevel von 'MMUserGroup' der neuen Gruppe uebergeben
             for x:= 0 to xGroupList.count -1 do
                 if Pos('MMUserGroup', xGroupList.Strings[x]) > 0 then begin
                    n:= Pos('=', xGroupList.Strings[x]);
                    xTemp:= copy(xGroupList.Strings[x], n+1, 5);
                    try
                      xSecurLevel := StrToInt(xTemp);
                    except
                      xSecurLevel := 0;
                    end;
                 end;

             for x:= 0 to FSecurityList.Count-1 do
                 if (FSecurityList.Items[x].Appl = xAppl) and
                    (FSecurityList.Items[x].ApplForm = xForm) and
                    (FSecurityList.Items[x].ApplFunction = xFunction) and
                    (FSecurityList.Items[x].SecurityGroup = '') then begin
                     FSecurityList.Items[x].SecurityGroup := aGroup;
                     FSecurityList.Items[x].SecurityLevel := xSecurLevel;
                     xFound := TRUE;
                     break;
                  end;

             if not xFound then begin
                new(xRec);
                xRec.Appl           := xAppl;
                xRec.ApplForm       := xForm;
                xRec.ApplFunction   := xFunction;
                xRec.FormCaption    := xCaption;
                xRec.Info           := xInfo;
                xRec.SecurityGroup  := aGroup;
                xRec.SecurityLevel  := xSecurLevel;
                xRec.DBRecordMarker := FALSE;
                FSecurityList.Add(xRec);
             end;

             mADOQuery.Next;
         end;
    end;
    FCount  :=  FSecurityList.count;

    FHasSecurityEntries:=TRUE;

    xNewGroups.free;
    xADOQuery.free;
    xGroupList.Free;
end;// TBaseMMSecurity.AddUserGroup cat:Undefined

//------------------------------------------------------------------------------
(**********************************************************************
 *  Member:           DeleteGroup
 *  Klasse:           TBaseMMSecurity
 *  Kategorie:        Undefined
 *  Argumente:        (aGroup)
 *
 *  Kurzbeschreibung:
 *  Beschreibung:     Loescht eine Gruppe aus der Security-List
                      mit den dazugehoerigen Funktionen
 ************************************************************************)
procedure TBaseMMSecurity.DeleteGroup(aGroup : String);

  var x : Integer;
      xRec : pSecurityRec;
      xBool : Boolean;
  
begin
  x:= 0;
  xBool:= FALSE;
  while x < FSecurityList.count do begin
    if FSecurityList.Items[x].SecurityGroup = aGroup then begin
       if FSecurityList.Items[x].DBRecordMarker = FALSE then begin
          xRec :=  FSecurityList.Items[x];
          Dispose( xRec );
          FSecurityList.Delete(x);
          x:= 0;
       end else begin
          FSecurityList.Items[x].SecurityGroup:= '';
          FSecurityList.Items[x].SecurityLevel:= -1;
       end;
     end;
  
    if FSecurityList.Items[x].SecurityGroup <> '' then xBool := TRUE;
    inc(x);
  end;
  FCount  := FSecurityList.count;
  
  FHasSecurityEntries:=xBool;
end;// TBaseMMSecurity.DeleteGroup cat:Undefined

//------------------------------------------------------------------------------
(**********************************************************************
 *  Member:           FillSecurityList
 *  Klasse:           TBaseMMSecurity
 *  Kategorie:        Undefined 
 *  Argumente:        
 *
 *  Kurzbeschreibung: 
 *  Beschreibung:     Liest Daten aus der DB und fuellt diese in die TSecurityliste ab.
                      (Die Liste hat pro MMUsergroup einen Record)
                      Das Feld DBRecordMarker = TRUE bedeutet, dass dieser Record einem 
                      DB-Record entspricht. -> ohne c_Security
 ************************************************************************)
procedure TBaseMMSecurity.FillSecurityList;
  
  var xList : pSecurityRec;
      xCount, x, xRecID: Integer;
      xGroupList : TStringlist;
      xGroup, xTemp, xFunctionText: String;
      xSecurLevel, xDefaultLevel : Integer;
      xAppl, xForm, xFunction : String;
  
begin
  with mADOQuery do begin
       Close;
       SQL.text:= Format(cGetAllData, [mTable]);
       Open;
       First;
  
       xFunctionText:= '';
       xGroupList := TStringList.create;
  
       FSecurityList.Clear;
       for xCount := 0 to RecordCount-1 do begin
           if EOF then break; 
           //Default security-level ermitteln
           xDefaultLevel := GetDefaultLevel (FieldByName('c_Appl').asString,
                                             FieldByName('c_Form').asString,
                                             FieldByName('c_Function').asString
                                             );
  
           //Security-Gruppen auslesen
           xGroupList.commatext := FieldByName('c_Security').asString;
  
           if xGroupList.Count > 0 then begin
              //Leereintraege loeschen
              x := 0;
              while x <= xGroupList.Count-1 do begin
                  xGroup := xGroupList.Strings[x];
                  xGroup := StringReplace( xGroup, ',', '', [rfReplaceAll] );
                  xGroup   := StringReplace( xGroup, '=-1', '', [rfReplaceAll] );
                  xGroupList.Strings[x] := xGroup;
                  if xGroup = '' then begin
                     xGroupList.delete(x);
                     x:= 0;
                  end else inc( x );
              end;
           end else begin
               //Keine Usergroups in 'c_Security' vorhanden
               //Vorhandene Usergruppen einsetzen
               xGroupList.commatext := GetUserGroups;
  
               for x:= 0 to xGroupList.count-1 do
                   xGroupList.strings[x] := Format('%s=%d',[xGroupList.strings[x], 0]);
           end;
  
  
           for x:= 0 to xGroupList.count-1 do begin
               //Gruppen extrahieren
               xGroup := Copy(xGroupList.strings[x],
                              0,
                              Pos('=', xGroupList.strings[x])-1
                              );
  
               xTemp  := Copy(xGroupList.strings[x],
                              Pos('=', xGroupList.strings[x]) + 1,
                                  length(xGroupList.strings[x])
                              );
               try
                   xSecurLevel := StrToInt(xTemp);
  
                   new(xList);
                   xList.Appl         := FieldByName('c_Appl').asString;
                   xList.ApplForm     := FieldByName('c_Form').asString;
                   xList.ApplFunction := FieldByName('c_Function').asString;
                   xList.FormCaption  := FieldByName('c_Caption').asString;
                   xList.Info         := FieldByName('c_Info').asString;
                   xList.TranslFormCaption := IvDictio.Translate(FieldByName('c_Caption').asString);
                   xList.TranslInfo   := IvDictio.Translate(FieldByName('c_Info').asString);

                   xTemp              := IvDictio.Translate('(*)' + xList.Appl);
                   xTemp              := StringReplace(xTemp, '(*)', '', [rfReplaceAll]);
                   xList.TranslAppl   := xTemp;

                   xList.SecurityGroup:= xGroup;
                   xList.SecurityLevel:= xSecurLevel;
                   xList.DefaultLevel := xDefaultLevel;
  
                   if xGroup <> '' then FHasSecurityEntries:= TRUE;
  
                   if x = 0 then
                      xList.DBRecordMarker:= TRUE
                   else
                      xList.DBRecordMarker:= FALSE;
  
                   FSecurityList.Add(xList);
               except
               end;
           end;
  
           //Kein Defaultwert gefunden
           if xGroupList.count = 0 then begin
              new(xList);
  
              xList.Appl         := FieldByName('c_Appl').asString;
              xList.ApplForm     := FieldByName('c_Form').asString;
              xList.ApplFunction := FieldByName('c_Function').asString;
              xList.FormCaption  := FieldByName('c_Caption').asString;
              xList.Info         := FieldByName('c_Info').asString;
              xList.TranslFormCaption := IvDictio.Translate(FieldByName('c_Caption').asString);
              xList.TranslInfo   := IvDictio.Translate( FieldByName('c_Info').asString);
  
              xList.SecurityGroup:= '';
              xList.SecurityLevel:= 0;
              xList.DefaultLevel := xDefaultLevel;
              xList.DBRecordMarker:= TRUE;
  
              FSecurityList.Add(xList);
           end;
           Next;
       end;
  
       FSecurityList.sort(Sort_Appl_View);
       FCount := FSecurityList.count;
       xGroupList.free;
  end;
end;// TBaseMMSecurity.FillSecurityList cat:Undefined

//------------------------------------------------------------------------------
function TBaseMMSecurity.GetADOConnection: TmmADOConnection;
begin
end;// TBaseMMSecurity.GetADOConnection cat:Undefined

//------------------------------------------------------------------------------
function TBaseMMSecurity.GetApplicationForms(aApplication:String): String;

  var xFormList : TStringList;
      xCount : Integer;
      xText :String;

begin
  if aApplication = '' then begin
     Result := '';
     exit;
  end;
  {
  with mADOQuery do begin
    Close;
    SQL.text:= Format(cGetApplForms, [mTable, aApplication ]);
    Open;
  
    xFormList := TStringList.Create;
    xFormList.Sorted:= TRUE;
    //Alle Forms einer Applikation auslesen
    First;
    for xCount := 0 to RecordCount-1 do begin
        xText:= FieldByName('c_Caption').asString;
        xText := IvDictio.Translate(xText);
        xFormList.add( xText );
        Next;
    end;
    Result := xFormList.commatext;
    xFormList.Free;
   }
    xFormList := TStringList.Create;
    xFormList.Sorted:= TRUE;
    xFormList.Duplicates := dupIgnore;
    for xCount := 0 to FSecurityList.count-1 do
        if FSecurityList.Items[xCount].Appl = aApplication then begin
           xText := FSecurityList.Items[xCount].TranslFormCaption;
           xFormList.add( xText );
        end else if FSecurityList.Items[xCount].TranslAppl = aApplication then begin
           xText := FSecurityList.Items[xCount].TranslFormCaption;
           xFormList.add( xText );
        end;

    Result := xFormList.commatext;
    xFormList.Free;
end;// TBaseMMSecurity.GetApplicationForms cat:Undefined

//------------------------------------------------------------------------------
(**********************************************************************
 *  Member:           GetDefaultLevel
 *  Klasse:           TBaseMMSecurity
 *  Kategorie:        Undefined 
 *  Argumente:        (aAppl, aForm, aFunction)
 *
 *  Kurzbeschreibung: 
 *  Beschreibung:     Ermitelt den Securitylevel aus Tab. t_security_work
 ************************************************************************)
function TBaseMMSecurity.GetDefaultLevel(aAppl, aForm, aFunction : String): Integer;
  
  var xADOQuery : TmmADOQuery;
  
      xGroupList : TStringlist;
      xGroup, xTemp: String;
      xDefaultLevel, x : Integer;
  
  const cNonDefineDefaultSec = -1;
  
begin
  xDefaultLevel := cNonDefineDefaultSec;
  xADOQuery := TmmADOQuery.Create(NIL);
  xADOQuery.Connection := mADOQuery.Connection;
  
  with xADOQuery do begin
       Close;
       SQL.text:= Format(cGetDefaultSecur, [aAppl, aForm, aFunction]);
       SQL.text:= Format(cGetDefaultSecur1,[aAppl, aForm, aFunction]);
       Open;
       First;
  
       xGroupList := TStringList.create;
       xGroupList.commatext := FieldByName('c_Security').asString;
  
       for x:= 0 to xGroupList.Count-1 do begin
           xGroup := Copy(xGroupList.strings[x],
                          0,
                          Pos('=', xGroupList.strings[x])-1
                          );
  
            xTemp  := Copy(xGroupList.strings[x],
                           Pos('=', xGroupList.strings[x]) + 1,
                           length(xGroupList.strings[x])
                           );
  
            if xGroup = cDefaultUserGroup then begin
                try
                  xDefaultLevel := StrToInt(xTemp);
                except
                  xDefaultLevel :=cNonDefineDefaultSec;
                end;
                   break;
               end;
       end;
       xGroupList.Free;
       free;
  end;
  result := xDefaultLevel;
end;// TBaseMMSecurity.GetDefaultLevel cat:Undefined

//------------------------------------------------------------------------------
function TBaseMMSecurity.GetSecurityGrid: TmmExtStringGrid;
begin
  result := mSecurityGrid;
end;// TBaseMMSecurity.GetSecurityGrid cat:Undefined

//------------------------------------------------------------------------------
(**********************************************************************
 *  Member:           GetUserGroups
 *  Klasse:           TBaseMMSecurity
 *  Kategorie:        Undefined 
 *  Argumente:        
 *
 *  Kurzbeschreibung: 
 *  Beschreibung:     Alle m�glichen Benutzergruppen auslesen
 ************************************************************************)
function TBaseMMSecurity.GetUserGroups: String;
  
  var x: Integer;
      xGroupList, xGroups : TStringList;
      xADOQuery : TmmADOQuery;
      xText: String;
  
begin
  xGroupList := TStringList.create;
  xGroupList.Sorted:= TRUE;
  xGroupList.Duplicates:= dupIgnore;
  
  xGroups := TStringList.create;
  xGroups.Sorted:= TRUE;
  xGroups.Duplicates:= dupIgnore;
  
  for x:= 0 to fSecurityList.count-1 do begin
      if fSecurityList.Items[x].SecurityGroup <> '' then
         xGroupList.Add( fSecurityList.Items[x].SecurityGroup );
  end;
  
  if xGroupList.CommaText = '' then begin
     xADOQuery := TmmADOQuery.Create(NIL);
     xADOQuery.Connection := mADOQuery.Connection;
     with xADOQuery do begin
          Close;
          SQL.text:= Format(cGetAllData, [mTable]);
          Open;
          First;
          while not EOF do begin
             xGroups.commatext := FieldByName('c_Security').asString;
             xGroupList.AddStrings(xGroups);
             next;
          end;
          Close;
  
          xGroups.Clear;
          xGroupList.Sorted:= FALSE;
          for x:= 0 to xGroupList.count-1 do begin
              xText := xGroupList.Strings[x];
  
              System.Delete(xText,
                            Pos('=', xText) ,
                            3);
  
              xGroups.Add(xText);
          end;
          Free;
     end;
     xGroups.Sort;
     Result := xGroups.CommaText;
  end else Result := xGroupList.CommaText;
  
  xGroupList.free;
  xGroups.Free;
end;// TBaseMMSecurity.GetUserGroups cat:Undefined

//------------------------------------------------------------------------------
function TBaseMMSecurity.Init: Boolean;
  
  var xCount: Integer;
      xApplList: TStringList;
  
begin
  try
  
      if mADOQuery = NIL then mADOQuery:= TmmADOQuery.Create(NIL);
  
      with mADOQuery do begin
        Close;
        SQL.text:= Format(cGetAllAppl, [mTable]);
        Active := TRUE;
        FIsAdoConnected := Active;
  
        xApplList := TStringList.Create;
        xApplList.Sorted:= TRUE;
  
        //Alle Applikationen auslesen
        First;
        for xCount := 0 to RecordCount-1 do begin
            xApplList.add(FieldByName('c_Appl').asString );
            Next;
        end;
        FMMApplications := xApplList.commatext;
  
        FillSecurityList; //Ganze Tabelle in security list schreiben
  
        xApplList.free;
      end;
      Result := FIsAdoConnected;
  
  except
     on E: Exception do
           CodeSite.SendError( 'Error in ' + ClassName + '.Init. Error msg : ' + e.message) ;
  end;
end;// TBaseMMSecurity.Init cat:Undefined

//------------------------------------------------------------------------------
procedure TBaseMMSecurity.Insert(aTable, aAppl, aForm, aFunction, aCaption, aInfo, aSecurity: String);
begin
  with mADOQuery do begin
       Close;
       SQL.text:= Format(cInsert, [aTable, aAppl, aForm, aFunction, aCaption, aInfo, aSecurity]);
       ExecSQL;
  end;
end;// TBaseMMSecurity.Insert cat:Undefined

//------------------------------------------------------------------------------
(**********************************************************************
 *  Member:           Load
 *  Klasse:           TBaseMMSecurity
 *  Kategorie:        Undefined 
 *  Argumente:        
 *
 *  Kurzbeschreibung: 
 *  Beschreibung:     Liest die Daten neu aus der Tab. in FSecurityList ein
 ************************************************************************)
procedure TBaseMMSecurity.Load;
begin
  FillSecurityList;
end;// TBaseMMSecurity.Load cat:Undefined

//------------------------------------------------------------------------------
(**********************************************************************
 *  Member:           Save
 *  Klasse:           TBaseMMSecurity
 *  Kategorie:        Undefined 
 *  Argumente:        (aTable)
 *
 *  Kurzbeschreibung: 
 *  Beschreibung:     Speichert alle Daten aus FSecurityList in Tab.t_security_xy 
                      Vorhandene Eintraege werden zuerst geloescht und anschliessend eingefuegt
 ************************************************************************)
procedure TBaseMMSecurity.Save(aTable:String);
  
  var xRec : pSecurityRec;
      xCount, x: Integer;
      xSecurityList : TSecurityList;
      xAppl, xForm, xFunction, xCaption, xInfo, xSecurity, xSecurityString : String;
      xFound :Boolean;
  
begin
  xSecurityList   := TSecurityList.Create;
  
  FSecurityList.sort(Sort_Appl_View);
  
  for xCount := 0 to FSecurityList.count-1 do begin
  
       xFound:= FALSE;
       for x:= 0 to xSecurityList.count -1 do
           if (xSecurityList.Items[x].Appl = FSecurityList.Items[xCount].Appl) and
              (xSecurityList.Items[x].ApplForm = FSecurityList.Items[xCount].ApplForm) and
              (xSecurityList.Items[x].ApplFunction =  FSecurityList.Items[xCount].ApplFunction) then begin
  
               xSecurityString := Format('%s,%s=%d',[xSecurityList.Items[x].SecurityString,
                                                     FSecurityList.Items[xCount].SecurityGroup,
                                                     FSecurityList.Items[xCount].SecurityLevel]);
               FSecurityList.Items[xCount].SecurityString:= xSecurityString;
               xSecurityList.Items[x].SecurityString:= xSecurityString;
  
               xFound:= TRUE;
               break;
             end;
  
       if not xFound then begin
          new(xRec);
          xRec.Appl         := FSecurityList.Items[xCount].Appl;
          xRec.ApplForm     := FSecurityList.Items[xCount].ApplForm;
          xRec.ApplFunction := FSecurityList.Items[xCount].ApplFunction;
          xRec.FormCaption  := FSecurityList.Items[xCount].FormCaption;
          xRec.Info         := FSecurityList.Items[xCount].Info;
          xRec.SecurityGroup:= FSecurityList.Items[xCount].SecurityGroup;
          xRec.SecurityLevel:= FSecurityList.Items[xCount].SecurityLevel;
          xRec.SecurityString:= xRec.SecurityGroup + '=' + IntToStr(xRec.SecurityLevel);
  
          xRec.DBRecordMarker:= FSecurityList.Items[xCount].DBRecordMarker;
          xSecurityList.Add(xRec);
       end;
  end;
  
  
  with mADOQuery do begin
       Close;
       SQL.text:= Format(cDeleteTable, [aTable]);
       ExecSQL;
  end;
  
  for xCount := 0 to xSecurityList.count-1 do begin
      xAppl           := xSecurityList.Items[xCount].Appl;
      xForm           := xSecurityList.Items[xCount].ApplForm;
      xFunction       := xSecurityList.Items[xCount].ApplFunction;
      xCaption        := xSecurityList.Items[xCount].FormCaption;
      xInfo           := xSecurityList.Items[xCount].Info;
      xSecurityString := xSecurityList.Items[xCount].SecurityString;
  
      xSecurityString:= StringReplace( xSecurityString, '=-1', '', [rfReplaceAll] );
      Insert(aTable, xAppl, xForm, xFunction, xCaption, xInfo, xSecurityString);
  end;
  
  x:= xSecurityList.Count;
  xSecurityList.free;
end;// TBaseMMSecurity.Save cat:Undefined

//------------------------------------------------------------------------------
(**********************************************************************
 *  Member:           SaveAsActual
 *  Klasse:           TBaseMMSecurity
 *  Kategorie:        Undefined 
 *  Argumente:        
 *
 *  Kurzbeschreibung: 
 *  Beschreibung:     Speichert alle Daten aus FSecurityList in Tab. t_security_work
 ************************************************************************)
procedure TBaseMMSecurity.SaveAsActual;
begin
  Save(mActualSecurityTab);
end;// TBaseMMSecurity.SaveAsActual cat:Undefined

//------------------------------------------------------------------------------
(**********************************************************************
 *  Member:           SaveAsBackup
 *  Klasse:           TBaseMMSecurity
 *  Kategorie:        Undefined 
 *  Argumente:        
 *
 *  Kurzbeschreibung: 
 *  Beschreibung:     Speichert alle Daten aus FSecurityList in Tab. t_security_customer
 ************************************************************************)
procedure TBaseMMSecurity.SaveAsBackup;
begin
  Save(mBackupSecurityTab);
end;// TBaseMMSecurity.SaveAsBackup cat:Undefined

//------------------------------------------------------------------------------
(**********************************************************************
 *  Member:           SaveAsDefault
 *  Klasse:           TBaseMMSecurity
 *  Kategorie:        Undefined 
 *  Argumente:        
 *
 *  Kurzbeschreibung: 
 *  Beschreibung:     Speichert alle Daten aus FSecurityList in Tab. t_security_default
 ************************************************************************)
procedure TBaseMMSecurity.SaveAsDefault;
begin
  Save(mDefaultSecurityTab);
end;// TBaseMMSecurity.SaveAsDefault cat:Undefined

//------------------------------------------------------------------------------
(**********************************************************************
 *  Member:           Save_notused
 *  Klasse:           TBaseMMSecurity
 *  Kategorie:        Undefined 
 *  Argumente:        
 *
 *  Kurzbeschreibung: 
 *  Beschreibung:     Speichert Security-Daten aus FSecurityList in die Tabelle (mTable)
 ************************************************************************)
procedure TBaseMMSecurity.Save_notused;
  
    var xList : pSecurityRec;
        xRec : pSecurityRec;
        xCount, x: Integer;
  
        xGroup, xTemp, xFunctionText: String;
        xSecurLevel : Integer;
  
        xGroupList : TStringlist;
        xAppl, xForm, xFunction, xCaption, xInfo, xSecurity :String;
  
        xTempStringList : TStringList;
  
        xSecurityList : TSecurityList;
  
begin
    xSecurityList   := TSecurityList.Create;
  
    xTempStringList := TStringList.Create;
    xTempStringList.Duplicates:= dupIgnore;
    xTempStringList.Sorted:= TRUE;
  
    //Basis DB-Record erstellen
    for xCount := 0 to FSecurityList.count-1 do
        if FSecurityList.Items[xCount].DBRecordMarker = TRUE then begin
           new(xRec);
  
           xRec.Appl         := FSecurityList.Items[xCount].Appl;
           xRec.ApplForm     := FSecurityList.Items[xCount].ApplForm;
           xRec.ApplFunction := FSecurityList.Items[xCount].ApplFunction;
           xRec.FormCaption  := FSecurityList.Items[xCount].FormCaption;
           xRec.Info         := ''; // Hier wird die zusammengesetzte Security geschrieben
           xRec.SecurityGroup:= FSecurityList.Items[xCount].SecurityGroup;
           xRec.SecurityLevel:= FSecurityList.Items[xCount].SecurityLevel;
           xRec.DBRecordMarker:= FSecurityList.Items[xCount].DBRecordMarker;
  
           xSecurityList.Add(xRec);
       end;
  
    for xCount := 0 to FSecurityList.count-1 do
      for x := 0 to xSecurityList.count-1 do
          if (xSecurityList.Items[x].Appl = FSecurityList.Items[xCount].Appl) and
             (xSecurityList.Items[x].ApplForm = FSecurityList.Items[xCount].ApplForm) and
             (xSecurityList.Items[x].ApplFunction =  FSecurityList.Items[xCount].ApplFunction) then begin
             //Security zusammensetzen
             if Pos( FSecurityList.Items[xCount].SecurityGroup, xSecurityList.Items[x].info) = 0 then begin
                if xSecurityList.Items[x].Info <> '' then
                   xSecurity := Format('%s,%s=%d',[xSecurityList.Items[x].Info,
                                                FSecurityList.Items[xCount].SecurityGroup,
                                                FSecurityList.Items[xCount].SecurityLevel])
                else
                   xSecurity := Format('%s=%d',[FSecurityList.Items[xCount].SecurityGroup,
                                                FSecurityList.Items[xCount].SecurityLevel]);
  
                xSecurityList.Items[x].Info := xSecurity;
             end;
          end;
  
    for xCount := 0 to xSecurityList.count-1 do begin
       //Update Query  -> mTable
  
       xAppl:= xSecurityList.Items[xCount].Appl;
       xForm:= xSecurityList.Items[xCount].ApplForm;
       xFunction:= xSecurityList.Items[xCount].ApplFunction;
       xSecurity:= xSecurityList.Items[xCount].Info;  //Actuelle Security
  
       xTempStringList.CommaText :=  xSecurity;
       xTempStringList.sorted:= FALSE;
  
       //Leereintraege loeschen
       x := 0;
       while x <= xTempStringList.Count-1 do begin
           xSecurity := xTempStringList.Strings[x];
           xSecurity := StringReplace( xSecurity, ',', '', [rfReplaceAll] );
           xSecurity := StringReplace( xSecurity, '=-1', '', [rfReplaceAll] );
           xTempStringList.Strings[x] := xSecurity;
                 if xSecurity = '' then begin
                    xTempStringList.delete(x);
                    x:= 0;
                 end else inc( x );
           end;
       xTempStringList.sorted:= TRUE;
       xTempStringList.Sort;
  
       Update(xAppl, xForm, xFunction, xTempStringList.CommaText);
    end;
  
    xSecurityList.Free;
    xTempStringList.Free;
end;// TBaseMMSecurity.Save_notused cat:Undefined

//------------------------------------------------------------------------------
procedure TBaseMMSecurity.SetADOConnection(Value: TmmADOConnection);
begin
  if mADOQuery <> NIL then begin
     mADOQuery.Connection := value;
     mADOQuery.Active:= FALSE;
     FIsAdoConnected:= mADOQuery.Active;
  end;
end;// TBaseMMSecurity.SetADOConnection cat:Undefined

//------------------------------------------------------------------------------
procedure TBaseMMSecurity.SetSecurityGrid(Value: TmmExtStringGrid);
begin
  if Value <> NIL then
    mSecurityGrid := Value;
end;// TBaseMMSecurity.SetSecurityGrid cat:Undefined

//------------------------------------------------------------------------------
procedure TBaseMMSecurity.SetSetMMUsergroup(Value: String);
begin
end;// TBaseMMSecurity.SetSetMMUsergroup cat:Undefined

//------------------------------------------------------------------------------
(**********************************************************************
 *  Member:           ShowData
 *  Klasse:           TBaseMMSecurity
 *  Kategorie:        Undefined 
 *  Argumente:        (aUserGroup)
 *
 *  Kurzbeschreibung: 
 *  Beschreibung:     Zeigt die Appls. & Fenster & Security-Funktionen einer gewaehlten MMUser-Gruppe
 ************************************************************************)
procedure TBaseMMSecurity.ShowData(aUserGroup : String);
  
  var x, xCount, xSecLevel: integer;
      xProg : String;
      xSecurLabel: TStringlist;
  
begin
  mSecurityGrid.ClearNormalCells;
  mSecurityGrid.RowCount:=2;
  
  if aUsergroup = '' then exit;
  
  FSecurityList.sort(Sort_Group_View);
  
  xCount := 1;
  for x:= 0 to FSecurityList.count-1 do
      if FSecurityList.Items[x].SecurityGroup = aUsergroup then begin
         mSecurityGrid.RowCount:= xCount +1;
  
         //xProg := FSecurityList.Items[x].Appl + ' / ' +
         //         IvDictio.Translate(FSecurityList.Items[x].FormCaption);

//         xProg := FSecurityList.Items[x].Appl + ' / ' +
//                  FSecurityList.Items[x].TranslFormCaption;


         xProg := FSecurityList.Items[x].TranslAppl + ' / ' +
                  FSecurityList.Items[x].TranslFormCaption;


         //Text (Programm / Fenster) ausblenden, wenn auf vorhergehender Zeile
         //der xPorg-Text vorhanden ist.
         if xCount > 1 then
//            if xProg = mSecurityGrid.cells[3, xCount-1 ]  + ' / ' +
            if xProg = mSecurityGrid.cells[8, xCount-1 ]  + ' / ' +
                       mSecurityGrid.cells[7, xCount-1] then
                       xProg:= ''
            else if xProg = mSecurityGrid.cells[3, xCount-1 ]  + ' / ' +
                    mSecurityGrid.cells[7, xCount-1] then
                    xProg:= '';

         mSecurityGrid.cells[0, xCount]:= xProg;
         //mSecurityGrid.cells[1, xCount]:= IvDictio.Translate(FSecurityList.Items[x].Info);
         mSecurityGrid.cells[1, xCount]:= FSecurityList.Items[x].TranslInfo;
  
         //Markierung fuer Default MMUserGroup-Level erstellen
         xSecurLabel:= TStringlist.Create;
         xSecurLabel.CommaText  := gCellRadioButton.CommaText;

         xSecLevel := FSecurityList.Items[x].DefaultLevel; //Default level von MMUserGroup
         if xSecLevel >-1 then begin
            xSecurLabel.Strings[xSecLevel] := cDefaultMarker +  xSecurLabel.Strings[xSecLevel] ;
            xProg := xSecurLabel.Strings[xSecLevel];
            Delete( xProg ,3, 2);
            xSecurLabel.Strings[xSecLevel]:= xProg;
         end;
  
         mSecurityGrid.Addradio(2, xCount, 1, SecurityItem.Items[x].SecurityLevel, xSecurLabel);
  
         //Search key
         mSecurityGrid.cells[3, xCount]:= FSecurityList.Items[x].Appl;
         mSecurityGrid.cells[4, xCount]:= FSecurityList.Items[x].ApplForm;
         mSecurityGrid.cells[5, xCount]:= FSecurityList.Items[x].ApplFunction;
         mSecurityGrid.cells[6, xCount]:= FSecurityList.Items[x].SecurityGroup;
  //       mSecurityGrid.cells[7, xCount]:= IvDictio.Translate(FSecurityList.Items[x].FormCaption);
         mSecurityGrid.cells[7, xCount]:= FSecurityList.Items[x].TranslFormCaption;
         mSecurityGrid.cells[8, xCount]:= FSecurityList.Items[x].TranslAppl;
         inc(xCount);
      end;
end;// TBaseMMSecurity.ShowData cat:Undefined

//------------------------------------------------------------------------------
(**********************************************************************
 *  Member:           ShowData
 *  Klasse:           TBaseMMSecurity
 *  Kategorie:        Undefined 
 *  Argumente:        (aApplication, aForm)
 *
 *  Kurzbeschreibung: 
 *  Beschreibung:     Zeigt die Security-Funktionen und MMUser-Gruppen
                      einer gewaehlten Appl. und Fenster
 ************************************************************************)
procedure TBaseMMSecurity.ShowData(aApplication, aForm : String);
  
  var x, xCount, xSecLevel : integer;
      xInfo : String;
      xSecurLabel: TStringlist;
  
begin
  if mSecurityGrid = NIL then exit;
  
  mSecurityGrid.ClearNormalCells;
  mSecurityGrid.RowCount:=2;
  
  if aApplication = '' then exit;
  
  FSecurityList.sort(Sort_Appl_View);
  
  xCount := 1;
  for x:= 0 to FSecurityList.count-1 do
      if (FSecurityList.Items[x].Appl= aApplication) or
         (FSecurityList.Items[x].TranslAppl = aApplication) then
         //if (FSecurityList.Items[x].FormCaption = aForm ) or
         if (FSecurityList.Items[x].TranslFormCaption = aForm ) or
            (aForm  = '') then begin
             mSecurityGrid.RowCount:= xCount + 1;
             //xInfo := Translate(FSecurityList.Items[x].Info);
             xInfo := FSecurityList.Items[x].TranslInfo;

             //Text (Funktion) ausblenden, wenn auf vorhergehender Zeile
             //der xInfo-Text vorhanden ist.
             if xCount > 1 then
                if FSecurityList.Items[x].ApplFunction =
                               mSecurityGrid.cells[5, xCount-1] then  xInfo:= '';
  
             mSecurityGrid.cells[0, xCount]:= xInfo;
             mSecurityGrid.cells[1, xCount]:= FSecurityList.Items[x].SecurityGroup;
  
  
             //Markierung fuer Default MMUserGroup-Level erstellen
             xSecurLabel:= TStringlist.Create;
             xSecurLabel.CommaText  := gCellRadioButton.CommaText;
  
             xSecLevel := FSecurityList.Items[x].DefaultLevel; //Default level von MMUserGroup
  
             if xSecLevel > -1 then begin
                xSecurLabel.Strings[xSecLevel] := cDefaultMarker +  xSecurLabel.Strings[xSecLevel] ;
                xInfo := xSecurLabel.Strings[xSecLevel];
                Delete( xInfo ,3, 2);
                xSecurLabel.Strings[xSecLevel]:= xInfo;
             end;

             // mSecurityGrid.Addradio(2, xCount, 1, SecurityItem.Items[x].SecurityLevel, gCellRadioButton);
             mSecurityGrid.Addradio(2, xCount, 1, SecurityItem.Items[x].SecurityLevel, xSecurLabel);
  
             //Search key
             mSecurityGrid.cells[3, xCount]:= FSecurityList.Items[x].Appl;
             mSecurityGrid.cells[4, xCount]:= FSecurityList.Items[x].ApplForm;
             mSecurityGrid.cells[5, xCount]:= FSecurityList.Items[x].ApplFunction;
             mSecurityGrid.cells[6, xCount]:= FSecurityList.Items[x].SecurityGroup;
  //           mSecurityGrid.cells[7, xCount]:= Translate(FSecurityList.Items[x].FormCaption);
             mSecurityGrid.cells[7, xCount]:= FSecurityList.Items[x].TranslFormCaption;
             mSecurityGrid.cells[8, xCount]:= FSecurityList.Items[x].TranslAppl;
             inc(xCount);
          end;
end;// TBaseMMSecurity.ShowData cat:Undefined

//------------------------------------------------------------------------------
(**********************************************************************
 *  Member:           Translate
 *  Klasse:           TBaseMMSecurity
 *  Kategorie:        Undefined 
 *  Argumente:        
 *
 *  Kurzbeschreibung: 
 *  Beschreibung:     Texte werden uebersetzt
                      FormCaption (native) -> TranslFormCaption
                      Info (native) -> TranslInfo
 ************************************************************************)
procedure TBaseMMSecurity.Translate;
  
  var x: integer;
      xTemp : String;

begin
  for x:= 0 to FSecurityList.count-1 do begin
      FSecurityList.Items[x].TranslFormCaption := IvDictio.Translate(FSecurityList.Items[x].FormCaption);
      FSecurityList.Items[x].TranslInfo := IvDictio.Translate(FSecurityList.Items[x].Info);

      xTemp                             := IvDictio.Translate('(*)' + FSecurityList.Items[x].Appl);
      xTemp                             := StringReplace(xTemp, '(*)', '', [rfReplaceAll]);
      FSecurityList.Items[x].TranslAppl :=  xTemp;
  end;
end;// TBaseMMSecurity.Translate cat:Undefined

//------------------------------------------------------------------------------
procedure TBaseMMSecurity.Update(aAppl, aForm, aFunction, aSecurity : String);
begin
  with mADOQuery do begin
       Close;
       if aSecurity = '=-1' then aSecurity:= '';
       SQL.text:= Format(cUpdate, [mTable, aSecurity, aAppl, aForm, aFunction ]);
       ExecSQL;
  end;
end;// TBaseMMSecurity.Update cat:Undefined


//------------------------------------------------------------------------------
constructor TDefaultSecurity.Create;
begin
  try
     inherited Create (cSecurityDefaultTable);
  except
     on E: Exception do
           CodeSite.SendError( 'Error in ' + ClassName + '.Create. Error msg : ' + e.message) ;
  end;
end;// TDefaultSecurity.Create cat:Undefined

//------------------------------------------------------------------------------
function TDefaultSecurity.Init: Boolean;
  
  var xText :String;
  
begin
  try
     Result := inherited Init;
  except
     on E: Exception do begin
           xText := Format('Error in %s.Init. Error msg : %s',[ClassName,e.message]);
           CodeSite.SendError( xText ) ;
  
           xText := ExtractFileName(Application.Name);
           xText := Format('Error in %s.%s.Init. Error msg : %s',[xText, ClassName,e.message]);
           mEventLog.Write(etError, xText);
           Beep(500,100);
     end;
  end;
end;// TDefaultSecurity.Init cat:Undefined


//------------------------------------------------------------------------------
constructor TBackupSecurity.Create;
begin
  try
     inherited Create (cSecurityCustomerTable);
  except
     on E: Exception do
           CodeSite.SendError( 'Error in ' + ClassName + '.Create. Error msg : ' + e.message) ;
  end;
end;// TBackupSecurity.Create cat:Undefined

//------------------------------------------------------------------------------
function TBackupSecurity.Init: Boolean;
  
  var xText :String;
  
begin
  try
     Result := inherited Init;
  except
     on E: Exception do begin
           xText := Format('Error in %s.Init. Error msg : %s',[ClassName,e.message]);
           CodeSite.SendError( xText ) ;
  
           xText := ExtractFileName(Application.Name);
           xText := Format('Error in %s.%s.Init. Error msg : %s',[xText, ClassName,e.message]);
           mEventLog.Write(etError, xText);
           Beep(500,100);
     end;
  end;
end;// TBackupSecurity.Init cat:Undefined


//------------------------------------------------------------------------------
constructor TActualSecurity.Create;
begin
  try
     inherited Create (cSecurityTable);
  except
     on E: Exception do
        CodeSite.SendError( 'Error in ' + ClassName + '.Create. Error msg : ' + e.message) ;
  end;
end;// TActualSecurity.Create cat:Undefined

//------------------------------------------------------------------------------
function TActualSecurity.Init: Boolean;
  
  var xText :String;
  
begin
  try
     Result := inherited Init;
  except
     on E: Exception do begin
           xText := Format('Error in %s.Init. Error msg : %s',[ClassName,e.message]);
           CodeSite.SendError( xText ) ;
  
           xText := ExtractFileName(Application.Name);
           xText := Format('Error in %s.%s.Init. Error msg : %s',[xText, ClassName,e.message]);
           mEventLog.Write(etError, xText);
           Beep(500,100);
     end;
  end;
end;// TActualSecurity.Init cat:Undefined
//------------------------------------------------------------------------------
procedure TBaseMMSecurity.SetMMPackage(const Value: TMMPackage);
var x, xIndex : Integer;
     xDeleteItem : Boolean;
    xSearchAppl, xPackage : String;
    xItemRec: pSecurityRec;
begin

  mPackage := Value;

  xDeleteItem := FALSE;

  if mPackage <> mmpPro then begin
     x:= 0;
     repeat
        xDeleteItem := FALSE;
        xItemRec := FSecurityList.Items[x];
        //xIndex := Integer( xDeleteItems.Objects[x] );

        if (mPackage = mmpEasy) or (mPackage = mmpStandard) then begin

           //Programme, welche nicht im Stadard vorhanden sind
           // ohne MMQOfflimit, ClassDesigner, (LabReport hat keine Secuity!)
           xSearchAppl := 'MMQOfflimit';
           if xItemRec^.Appl = xSearchAppl then
              xDeleteItem := TRUE;

           xSearchAppl := 'ClassDesigner';
           if xItemRec^.Appl = xSearchAppl then
              xDeleteItem := TRUE;

           xPackage := 'Standard package';

           if (mPackage = mmpEasy) then begin
              //Programme, welche nicht im EASY vorhanden sind
              // ohne MMMaConfig, MMStyle, MMClearerAssistant

//            Die Appl. 'MMMaConfig' darf nicht aus den t_security Tabellen geloescht werden,
//            da 'MMMaConfig' bestandteil aller Packgages ist !
//            Fehler behoben am 06.07.2006; ab Vers. 5.03
//            xSearchAppl := 'MMMaConfig';
//            if xItemRec^.Appl = xSearchAppl then
//               xDeleteItem := TRUE;

              xSearchAppl := 'MMStyle';
              if xItemRec^.Appl = xSearchAppl then
                 xDeleteItem := TRUE;

              xSearchAppl := 'MMClearerAssistant';
              if xItemRec^.Appl = xSearchAppl then
                 xDeleteItem := TRUE;

              xPackage := 'EASY package';
           end;

           //DataItems loeschen
           if xDeleteItem = TRUE then begin
              Dispose(xItemRec);
              FSecurityList.Delete(x);
              x:=-1;
           end;
        end;

        inc(x);

//     until (xDeleteItem = FALSE);
     until (x >= FSecurityList.Count-1 );
  end else
      xPackage := 'Pro package';

  CodeSite.SendMsg(xPackage);

end;
//------------------------------------------------------------------------------
function TBaseMMSecurity.GetApplicationsTranslated: String;
var x: integer;
    xText : String;
    xList : TStringList;
begin
   xList := TStringList.Create;
   xList.CommaText :=  FMMApplications;

   for x:=0 to xList.Count-1 do begin
       xText := '(*)' + xList.Strings[x];
       xText :=  IvDictio.Translate( xText );
       xText := StringReplace(xText, '(*)', '', [rfReplaceAll]);
       xList.Strings[x] := xText;
   end;
   Result:= xList.CommaText;
   xList.Free;
end;
//------------------------------------------------------------------------------


initialization
  InitSecurity; 
finalization
  gBMP.free;
  gCellRadioButton.free;
end.
