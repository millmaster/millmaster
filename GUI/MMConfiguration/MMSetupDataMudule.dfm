object MMConfigDataModule: TMMConfigDataModule
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 295
  Top = 248
  Height = 489
  Width = 858
  object dbWork: TmmADOConnection
    CommandTimeout = 3
    ConnectionString = 
      'Provider=SQLOLEDB.1;Data Source=WETSRVMM5;Initial Catalog=MM_Win' +
      'ding;Password=netpmek32;User ID=MMSystemSQL;Auto Translate=False'
    ConnectionTimeout = 3
    CursorLocation = clUseServer
    DefaultDatabase = 'MM_Winding'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 32
    Top = 16
  end
  object qShifts: TmmADOQuery
    Connection = dbWork
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM t_shiftcal ORDER BY c_Shiftcal_Name')
    Left = 32
    Top = 72
  end
  object qMMSequity: TmmADOQuery
    Connection = dbWork
    Parameters = <>
    Left = 48
    Top = 184
  end
  object qMMSecurityDefault: TmmADOQuery
    Connection = dbWork
    Parameters = <>
    SQL.Strings = (
      'select * from t_security_default')
    Left = 48
    Top = 240
  end
  object qMMSecurityCustomer: TmmADOQuery
    Connection = dbWork
    Parameters = <>
    Left = 48
    Top = 296
  end
  object qMMAppls: TmmADOQuery
    Connection = dbWork
    Parameters = <>
    SQL.Strings = (
      'select distinct(c_Appl ) from t_security_Temp')
    Left = 176
    Top = 304
  end
  object qMMApplForms: TmmADOQuery
    Connection = dbWork
    Parameters = <
      item
        Name = 'Appl'
        DataType = ftString
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select c_Caption from t_security_Temp where c_Appl = %Appl')
    Left = 176
    Top = 248
  end
  object MMADOConnection: TmmADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Data Source=WETSRVMM5;Initial Catalog=MM_Win' +
      'ding;Password=netpmek32;User ID=MMSystemSQL;Auto Translate=False'
    DefaultDatabase = 'MM_Winding'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 164
    Top = 14
  end
end
