{===============================================================================
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: 
| Info..........: 
|
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 12.09.2002  1.00  SDo | File created
| 27.01.2004  1.01  SDO | Ubersetzung von MM-Security-Appls
|                       | Aenderungen in : bOKClick
| 08.06.2004  1.01  SDO | Print-Layout angepasst 
===============================================================================}
unit u_SecurPrintDLG;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, ExtCtrls, mmRadioGroup, StdCtrls, mmComboBox, Buttons,
  mmSpeedButton, mmLabel, mmGroupBox, mmButton, comctrls,
  u_MMConfigSecurity, clisted, mmPanel, IvDictio, IvMulti, IvEMulti,
  mmTranslator, Spin, mmNumCtrl, IvMlDlgs, mmPrinterSetupDialog,
  mmCheckListEdit ;

type
  TPrintDialog = class(TDialogBottom)
    mmGroupBox1: TmmGroupBox;
    laStatusLabel: TmmLabel;
    laStatus: TmmLabel;
    laTypeLabel: TmmLabel;
    laType: TmmLabel;
    laOrtLabel: TmmLabel;
    laOrt: TmmLabel;
    mmSpeedButton1: TmmSpeedButton;
    cbPrinters: TmmComboBox;
    rgSelectPrintDocu: TmmRadioGroup;
    mmTranslator1: TmmTranslator;
    mmGroupBox2: TmmGroupBox;
    mmLabel1: TmmLabel;
    nePageCopies: TmmNumEdit;
    pSelectItems: TmmPanel;
    clItemSelect: TMMCheckListEdit;
    mmGroupBox3: TmmGroupBox;
    lbFrom: TmmLabel;
    lbTo: TmmLabel;
    neFrom: TmmNumEdit;
    PrinterSetupDialog1: TmmPrinterSetupDialog;
    neTo: TmmNumEdit;
    rbPrintAll: TRadioButton;
    rbPrintPages: TRadioButton;
    procedure cbPrintersChange(Sender: TObject);
    procedure bOKClick(Sender: TObject);
    procedure mmSpeedButton1Click(Sender: TObject);
    procedure rgSelectPrintDocuClick(Sender: TObject);
    procedure rbPrintAllClick(Sender: TObject);
  private
    { Private declarations }
    fOsType: string;
    fPCName: string;
    fCompany: string;

    mPrintPossibility :TStrings;
    mViewText  : String;
    mActiveSecurityClass : TBaseMMSecurity;
    mSelectedAppl, mSelectedForm, mSelectedGroup : String;
    mShowProgs : Boolean;

    procedure GetPrinterInfo(aPrinterName:String);

  public
    { Public declarations }
    constructor Create(aOwner: TComponent;
                       aActiveSecurityClass : TBaseMMSecurity;
                       aView : TTabSheet;
                       aSelectedAppl, aSelectedForm, aSelectedGroup: String); reintroduce; virtual;


    property MachineName :string read fPCName write fPCName;
    property WinType :string read fOsType write fOsType;
    property Company :string read fCompany write fCompany;

  end;

var
  PrintDialog: TPrintDialog;

resourcestring
  rsPrint            = '(*)Druckt'; //ivlm
  rsPrepareData      = '(*)Druckt Vorbereitung'; //ivlm

  rsProg             = '(*)Programm'; //ivlm
  rsProgAllWindows   = '(*)und alle Fenster'; //ivlm
  rsProgAndSelWindow = '(*)und Fenster'; //ivlm
  rsProgSelWindows   = '(*)und selektierte Fenster'; //ivlm
 // rsAllProgs         = '(*)Alle Programme'; //ivlm
  rsAll              = '(*)Alles'; //ivlm


  rsGroup            = '(*)Gruppe'; //ivlm
  rsSelGroups        = '(*)Selektierte Gruppen'; //ivlm
  rsAllGroups        = '(*)Alle Gruppen'; //ivlm

//  rsPrinterReady = '(*)Im Leerlauf'; //ivlm
  rsPrinterReady = '(*)Bereit'; //ivlm
//  rsPrinterPaused  = '(*)PRINTER_STATUS_PAUSED'; //ivlm
//  rsPrinterPending = '(*)PRINTER_STATUS_PENDING_DELETION'; //ivlm

  rsPrinterPaused       = '(*)Druck beended'; //ivlm
  rsPrinterPending      = '(*)Druck warted'; //ivlm
  rsPrinterNotAvailable = '(*)Drucker nicht verfuegbar'; //ivlm
  rsPrinterOffline      = '(*)Drucker ist ausser betrieb'; //ivlm
  rsPrinterWaiting      = '(*)Druck warted'; //ivlm
  rsPrinterWarmingUp    = '(*)Drucker startet auf'; //ivlm
  rsPrinterBusy         = '(*)Drucker ist besetzt';  //ivlm

implementation

{$R *.DFM}
uses  printers, WinSpool,u_SecurityReport, RzCSIntf;

{ TPrintDialog }

//------------------------------------------------------------------------------
constructor TPrintDialog.Create(aOwner: TComponent;
  aActiveSecurityClass: TBaseMMSecurity; aView: TTabSheet; aSelectedAppl,
  aSelectedForm, aSelectedGroup: String);
var xText: String;
    x: Integer;
begin
  CodeSite.SendMsg('TPrintDialog.Create() begin');
  try
      inherited Create(aOwner);
      laStatus.Caption   := '';
      laType.Caption   := '';
      laOrt.Caption    := '';

      cbPrinters.clear;
      cbPrinters.Items :=  printer.Printers;
      printer.PrinterIndex :=-1;

      x:= cbPrinters.Items.IndexOf( printer.Printers.Strings[printer.PrinterIndex] );
      cbPrinters.ItemIndex := x;

      GetPrinterInfo(cbPrinters.Text);

      mActiveSecurityClass := aActiveSecurityClass;

      mSelectedAppl  := aSelectedAppl;
      mSelectedForm  := aSelectedForm;
      mSelectedGroup := aSelectedGroup;

      Caption:= Format('%s : %s',[rsPrint,aView.Caption]);

      mPrintPossibility:= TStringList.Create;


      if aView.Name = 'tsSecurityAppl' then begin
         mShowProgs := TRUE;

         //Appl. for select
         xText:= Format('%s %s', [aSelectedAppl, rsProgSelWindows]);
         mPrintPossibility.Add(xText);
         clItemSelect.items.CommaText := aActiveSecurityClass.GetApplicationForms(aSelectedAppl);

         //Selected Appl.
         xText:= Format('%s %s %s', [aSelectedAppl, rsProgAndSelWindow, aSelectedForm]);
         mPrintPossibility.Add(xText);

         //Program & all forms
         xText:= Format('%s %s', [ aSelectedAppl, rsProgAllWindows]);
         mPrintPossibility.Add(xText);

         //All Prog. & all forms
         xText:= Format('%s', [rsAll]);
         mPrintPossibility.Add(xText);

         Application.ProcessMessages;


      end else begin
         mShowProgs := FALSE;
         pSelectItems.Top := pSelectItems.Top + 5;
         clItemSelect.DropHeight :=  clItemSelect.DropHeight - 5;

         //Groups for select
         xText:= Format('%s', [rsSelGroups]);
         mPrintPossibility.Add(xText);
         clItemSelect.items.CommaText := aActiveSecurityClass.GetUserGroups;

         //Selected groups
         xText:= Format('%s %s', [rsGroup, aSelectedGroup]);
         mPrintPossibility.Add(xText);

         //All groups
         xText:= Format('%s', [rsAllGroups]);
         mPrintPossibility.Add(xText);

         Application.ProcessMessages;      
       end;

      clItemSelect.items.Sort;
      clItemSelect.Checked[0]:=TRUE;
      clItemSelect.Enabled := FALSE;

      rgSelectPrintDocu.Items.Clear;
      rgSelectPrintDocu.Items.AddStrings(mPrintPossibility);

      rgSelectPrintDocu.ItemIndex := 1;

      mViewText := aView.Caption;

  except
   on E: Exception do
         CodeSite.SendError( 'Error in ' + ClassName + '.Create. Error msg : ' + e.message) ;
  end;
  CodeSite.SendMsg('TPrintDialog.Create() done');
end;
//------------------------------------------------------------------------------
procedure TPrintDialog.GetPrinterInfo(aPrinterName: String);
var xNeeded   : DWORD;
    xReturned : DWORD;
    xBuffer   : Array[0..255] OF TPrinterInfo2;
    x         : Integer;
begin
  try
      CodeSite.SendMsg('TPrintDialog.GetPrinterInfo() begin');

      EnumPrinters(PRINTER_ENUM_LOCAL or PRINTER_ENUM_NETWORK,
                   NIL,
                   2,   // PrinterInfo2
                   @xBuffer,
                   SizeOf(xBuffer),
                   xNeeded,
                   xReturned );

      for x := 0 to xReturned do
          if xBuffer[x].pPrinterName = aPrinterName then break;

  except
    on E: Exception do
          CodeSite.SendError( 'Error in ' + ClassName + '.GetPrinterInfo. Error msg : ' + e.message) ;
  end;

  CodeSite.SendMsg('TPrintDialog.GetPrinterInfo() EnumPrinters  x -> ' + Inttostr(x)    +  ' xReturned -> '  + Inttostr(xReturned) );

  try
      laStatus.Caption := '';

      laType.Caption   := xBuffer[x].pDriverName;
      laOrt.Caption    := xBuffer[x].pPortName;

      case xBuffer[x].Status of
        0                               : laStatus.Caption := rsPrinterReady;
        PRINTER_STATUS_PAUSED           : laStatus.Caption := rsPrinterPaused;
        PRINTER_STATUS_PENDING_DELETION : laStatus.Caption := rsPrinterPending;
        PRINTER_STATUS_NOT_AVAILABLE    : laStatus.Caption := rsPrinterNotAvailable;
        PRINTER_STATUS_OFFLINE          : laStatus.Caption := rsPrinterOffline;        PRINTER_STATUS_WAITING          : laStatus.Caption := rsPrinterWaiting;
        PRINTER_STATUS_WARMING_UP       : laStatus.Caption := rsPrinterWarmingUp;
        PRINTER_STATUS_BUSY             : laStatus.Caption := rsPrinterBusy;


        else
          laStatus.Caption := '';
      end;
  except
    on E: Exception do
          CodeSite.SendError( 'Error in ' + ClassName + '.GetPrinterInfo.  xBuffer[x].Status Error msg : ' + e.message) ;
  end;


  Printer.PrinterIndex := Printer.Printers.IndexOf(cbPrinters.text);
  CodeSite.SendMsg('TPrintDialog.GetPrinterInfo() done');
end;
//------------------------------------------------------------------------------
procedure TPrintDialog.cbPrintersChange(Sender: TObject);
begin
  inherited;
  //if self.Visible then
  GetPrinterInfo(cbPrinters.Text);
end;
//------------------------------------------------------------------------------
procedure TPrintDialog.bOKClick(Sender: TObject);
var
    xPrintData : TSecurityList;
    xRec       : pSecurityRec;
    x, n       : integer;
    xText      : String;
begin

  inherited;
  CodeSite.SendMsg('TPrintDialog.bOKClick() begin print');
  Screen.Cursor := crHourGlass;

  laStatus.Caption := Translate( rsPrepareData );

  Application.ProcessMessages;

  CodeSite.SendMsg('TPrintDialog.bOKClick() prepare print');

  xPrintData := TSecurityList.Create;
  if mShowProgs then begin
     case rgSelectPrintDocu.ItemIndex of
          //Selected Appl and Selected Form
          1 : for x:= 0 to mActiveSecurityClass.Count-1 do
                if (mActiveSecurityClass.SecurityItem.Items[x].TranslAppl = mSelectedAppl) and
                   ( mActiveSecurityClass.SecurityItem.Items[x].TranslFormCaption = mSelectedForm) then begin
                    new(xRec);

                    xRec.Appl          := mActiveSecurityClass.SecurityItem.Items[x].TranslAppl;
                    xRec.FormCaption   := Translate( mActiveSecurityClass.SecurityItem.Items[x].FormCaption );
                    xRec.Info          := Translate( mActiveSecurityClass.SecurityItem.Items[x].Info );
                    xRec.SecurityGroup := mActiveSecurityClass.SecurityItem.Items[x].SecurityGroup;
                    xRec.SecurityLevel := mActiveSecurityClass.SecurityItem.Items[x].SecurityLevel;
                    xRec.DefaultLevel  := mActiveSecurityClass.SecurityItem.Items[x].DefaultLevel;
                    xPrintData.Add(xRec);

                    Application.ProcessMessages;
              end;

          //Selected Appl and all Forms
          2 : for x:= 0 to mActiveSecurityClass.Count-1 do
                if (mActiveSecurityClass.SecurityItem.Items[x].TranslAppl = mSelectedAppl) then begin
                    new(xRec);
                    xRec.Appl          := mActiveSecurityClass.SecurityItem.Items[x].TranslAppl;
                    xRec.FormCaption   := Translate( mActiveSecurityClass.SecurityItem.Items[x].FormCaption );
                    xRec.Info          := Translate( mActiveSecurityClass.SecurityItem.Items[x].Info );
                    xRec.SecurityGroup := mActiveSecurityClass.SecurityItem.Items[x].SecurityGroup;
                    xRec.SecurityLevel := mActiveSecurityClass.SecurityItem.Items[x].SecurityLevel;
                    xRec.DefaultLevel  := mActiveSecurityClass.SecurityItem.Items[x].DefaultLevel;
                    //xRec.Print         := xPrint;
                    xPrintData.Add(xRec);

                    Application.ProcessMessages;
              end;

          //All Appls and all Forms
          3 : begin
                  for x:= 0 to mActiveSecurityClass.Count-1 do begin
                      new(xRec);
                      xRec.Appl          := mActiveSecurityClass.SecurityItem.Items[x].TranslAppl;
                      xRec.FormCaption   := Translate( mActiveSecurityClass.SecurityItem.Items[x].FormCaption );
                      xRec.Info          := Translate( mActiveSecurityClass.SecurityItem.Items[x].Info );
                      xRec.SecurityGroup := mActiveSecurityClass.SecurityItem.Items[x].SecurityGroup;
                      xRec.SecurityLevel := mActiveSecurityClass.SecurityItem.Items[x].SecurityLevel;
                      xRec.DefaultLevel  := mActiveSecurityClass.SecurityItem.Items[x].DefaultLevel;
                      xPrintData.Add(xRec);

                      Application.ProcessMessages;
                  end;
              end;
          //Selected Appl and Selected Forms
          0 : for x:= 0 to mActiveSecurityClass.Count-1 do
                if (mActiveSecurityClass.SecurityItem.Items[x].TranslAppl = mSelectedAppl) then begin
                    for n:= 0 to clItemSelect.Items.Count-1 do
                        if clItemSelect.Checked[n] = TRUE then begin
                           xText := clItemSelect.Items[n];
                           if (mActiveSecurityClass.SecurityItem.Items[x].TranslFormCaption = xText) then begin
                                new(xRec);
                                xRec.Appl          := mActiveSecurityClass.SecurityItem.Items[x].TranslAppl;
                                xRec.FormCaption   := Translate( mActiveSecurityClass.SecurityItem.Items[x].FormCaption );
                                xRec.Info          := Translate( mActiveSecurityClass.SecurityItem.Items[x].Info );
                                xRec.SecurityGroup := mActiveSecurityClass.SecurityItem.Items[x].SecurityGroup;
                                xRec.SecurityLevel := mActiveSecurityClass.SecurityItem.Items[x].SecurityLevel;
                                xRec.DefaultLevel  := mActiveSecurityClass.SecurityItem.Items[x].DefaultLevel;
                                xPrintData.Add(xRec);
                                Application.ProcessMessages;
                          end;
                        end;
                end;
     end; //END Case
     xPrintData.sort(Sort_Appls);
  end else begin
     case rgSelectPrintDocu.ItemIndex of
          // Selected Group
          1: begin
                for x:= 0 to mActiveSecurityClass.Count-1 do
                    if (mActiveSecurityClass.SecurityItem.Items[x].SecurityGroup = mSelectedGroup) then begin
                        new(xRec);

                        xRec.Appl          := mActiveSecurityClass.SecurityItem.Items[x].TranslAppl;
                        xRec.FormCaption   := Translate( mActiveSecurityClass.SecurityItem.Items[x].FormCaption );
                        xRec.Info          := Translate( mActiveSecurityClass.SecurityItem.Items[x].Info );
                        xRec.SecurityGroup := mActiveSecurityClass.SecurityItem.Items[x].SecurityGroup;
                        xRec.SecurityLevel := mActiveSecurityClass.SecurityItem.Items[x].SecurityLevel;
                        xRec.DefaultLevel  := mActiveSecurityClass.SecurityItem.Items[x].DefaultLevel;
                        xPrintData.Add(xRec);
                        Application.ProcessMessages;
                    end;
              end;
          // All Groups
          2:  begin
                for x:= 0 to mActiveSecurityClass.Count-1 do begin
                    new(xRec);
                    xRec.Appl          := mActiveSecurityClass.SecurityItem.Items[x].TranslAppl;
                    xRec.FormCaption   := Translate( mActiveSecurityClass.SecurityItem.Items[x].FormCaption );
                    xRec.Info          := Translate( mActiveSecurityClass.SecurityItem.Items[x].Info );
                    xRec.SecurityGroup := mActiveSecurityClass.SecurityItem.Items[x].SecurityGroup;
                    xRec.SecurityLevel := mActiveSecurityClass.SecurityItem.Items[x].SecurityLevel;
                    xRec.DefaultLevel  := mActiveSecurityClass.SecurityItem.Items[x].DefaultLevel;
                    xPrintData.Add(xRec);
                    Application.ProcessMessages;
                 end;
              end;

          //  Selected Groups
          0:  begin
               for x:= 0 to mActiveSecurityClass.Count-1 do
                  for n:= 0 to clItemSelect.Items.Count-1 do
                      if clItemSelect.Checked[n] = TRUE then begin
                        xText := clItemSelect.Items[n];
                        if (mActiveSecurityClass.SecurityItem.Items[x].SecurityGroup = xText) then begin
                            new(xRec);
                            xRec.Appl          := mActiveSecurityClass.SecurityItem.Items[x].TranslAppl;
                            xRec.FormCaption   := Translate( mActiveSecurityClass.SecurityItem.Items[x].FormCaption) ;
                            xRec.Info          := Translate( mActiveSecurityClass.SecurityItem.Items[x].Info );
                            xRec.SecurityGroup := mActiveSecurityClass.SecurityItem.Items[x].SecurityGroup;
                            xRec.SecurityLevel := mActiveSecurityClass.SecurityItem.Items[x].SecurityLevel;
                            xRec.DefaultLevel  := mActiveSecurityClass.SecurityItem.Items[x].DefaultLevel;

                            xPrintData.Add(xRec);
                            Application.ProcessMessages;
                        end;
                      end;
              end;
     end; //End case

     xPrintData.sort(Sort_Groups);
     Application.ProcessMessages;
  end;

  if xPrintData.Count <=0 then begin
     xPrintData.Free;
     close;
     Screen.Cursor := crDefault;
     CodeSite.SendMsg('TPrintDialog.bOKClick() no print data, exit');
     exit;
  end;


  Application.ProcessMessages;

  printer.PrinterIndex := printer.Printers.IndexOf(cbPrinters.text);

  //  printer.Orientation  := poPortrait;

  with TSecurityReport.Create(self, xPrintData) do begin

       // Header settings
//       qlCompany.Caption      := Company;        // Firmenname
//       qlType.Caption         := WinType;        // NT-Produkt
//       qlComputername.Caption := MachineName;    // Computername
       //qlDate.Caption         := DateToStr(Now);
       {
       qlViewText.Caption     := mViewText + '   /   ' +
                                 rgSelectPrintDocu.Items.Strings[ rgSelectPrintDocu.ItemIndex ];
       }
       qlViewText.Caption     := mViewText;


       ShowProgramms := mShowProgs;
       printer.Copies := nePageCopies.AsInteger;

       qrSecurityReport.UsePrinterIndex( printer.PrinterIndex );
       qrSecurityReport.PrinterSettings.Copies := printer.Copies;
       
       qrSecurityreport.Prepare;
       qrSecurityreport.ReportTitle := 'MillMaster security';

       laStatus.Caption := Translate( rsPrint );

       CodeSite.SendMsg('TPrintDialog.bOKClick() begin print');

       qrSecurityreport.Print;

       Free;
  end;

  GetPrinterInfo( cbPrinters.text);

  xPrintData.Free;
  close;
  Screen.Cursor := crDefault;
  CodeSite.SendMsg('TPrintDialog.bOKClick() print done');
end;
//------------------------------------------------------------------------------
procedure TPrintDialog.rgSelectPrintDocuClick(Sender: TObject);
begin
  inherited;
//  if (Sender as TmmRadioGroup).itemindex = (Sender as TmmRadioGroup).Items.Count-1 then
  if (Sender as TmmRadioGroup).itemindex = 0 then
      clItemSelect.Enabled := TRUE
  else
     clItemSelect.Enabled := FALSE;
end;
//------------------------------------------------------------------------------
procedure TPrintDialog.mmSpeedButton1Click(Sender: TObject);
begin
  inherited;
  PrinterSetupDialog1.Execute;
end;
//------------------------------------------------------------------------------
procedure TPrintDialog.rbPrintAllClick(Sender: TObject);
var xEnable : Boolean;
begin
  inherited;
  if rbPrintAll.Checked then
     xEnable := FALSE
  else
     xEnable := TRUE;

  lbFrom.Enabled  := xEnable;
  lbTo.Enabled    := xEnable;
  neFrom.Enabled  := xEnable;
  neTo.Enabled    := xEnable;

  if xEnable then
     neFrom.Color := clWindow
  else
     neFrom.Color := clBtnFace;

  neTo.Color := neFrom.Color;
end;
//------------------------------------------------------------------------------
end.
