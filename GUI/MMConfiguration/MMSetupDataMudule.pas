unit MMSetupDataMudule;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ADODB, mmADOQuery, mmADOConnection, Db,
  mmExtStringGrid;

  //{Db, DBTables, mmQuery, mmDatabase,} ADODB, mmADOQuery, mmADOConnection;

type
  TMMConfigDataModule = class(TDataModule)
    dbWork: TmmADOConnection;
    qShifts: TmmADOQuery;
    qMMSequity: TmmADOQuery;
    qMMSecurityDefault: TmmADOQuery;
    qMMSecurityCustomer: TmmADOQuery;
    qMMAppls: TmmADOQuery;
    qMMApplForms: TmmADOQuery;
    MMADOConnection: TmmADOConnection;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
  public
    mError: Boolean;
    procedure FetchAllShifts;
  end;

var
  MMConfigDataModule: TMMConfigDataModule;

const
  cGetAllShifts = 'SELECT * FROM t_shiftcal ORDER BY c_Shiftcal_Name';
  //cSpace = #32#32#32;
  //cSpace = '     '; // 5 spaces

implementation // 15.07.2002 added mmMBCS to imported units

{$R *.DFM}
uses
  mmMBCS, mmCS,
  LoepfeGlobal, u_MMConfigSecurity {Grids, AdvGrid, StdCtrls};

//------------------------------------------------------------------------------
procedure TMMConfigDataModule.DataModuleDestroy(Sender: TObject);
begin
  qShifts.Close;
  dbWork.Connected := False;
//  gCellRadioButton.free;
end;
//------------------------------------------------------------------------------
procedure TMMConfigDataModule.FetchAllShifts;
begin
  qShifts.Close;
  qShifts.SQL.Text := cGetAllShifts;
  qShifts.Open;
end;
//------------------------------------------------------------------------------
procedure TMMConfigDataModule.DataModuleCreate(Sender: TObject);
var
  xErrorMsg, xFile: string;
  xSecurityHeader, xSpace: string;
  x, xPos0, xPos1: integer;
  xOffset: Integer;
begin
EnterMethod('DataModuleCreate');
  {
  //Text fuer Header in MMSecurityAppl & MMSecurityMMGroups
  //gSecurityMMGroups := rsSecurityMMGroups0 + ' \' + #13 + ' ' + rsSecurityMMGroups1;

  //gSecurityMMGroups := Format('%s \ %xxx %s', [rsSecurityMMGroups0, #13, rsSecurityMMGroups1]);
  gSecurityMMGroups := Format('%s \',[rsSecurityMMGroups0]);
  gSecurityMMGroups := gSecurityMMGroups + #13;
  gSecurityMMGroups := Format('%s %s',[gSecurityMMGroups, rsSecurityMMGroups1]);
  }
  {
  xOffset := 12;
  gCellRadioButton := TStringlist.Create;

  //Die Spaces d�rfen nicht gleich lang sein, weil bei einem
  //Grid-Radiobutton-Click alle drei Rb's markiert werden ???
  xSpace:= '';
  for x:= 0 to xOffset   do
      xSpace := xSpace + ' ';
  gCellRadioButton.Add(xSpace);

  xSpace:= '';
  for x:= 0 to xOffset-1   do
      xSpace := xSpace + ' ';
  gCellRadioButton.Add(xSpace);

  xSpace:= '';
  for x:= 0 to xOffset-2   do
      xSpace := xSpace + ' ';
  gCellRadioButton.Add(xSpace);
  }

  //DB Connection
  mError := False;
  if not(csDesigning in ComponentState) then begin
    with dbWork do
    try
      CodeSite.SendMsg('TMMConfigDataModule.DataModuleCreate');
      CodeSite.SendDateTime('Time : ' , Now);
      Open;
      CodeSite.SendMsg('TMMConfigDataModule.DataModuleCreate ADO is connected.');
    except
      on E: Exception do begin
        Sleep(2000);
        try
          open;
        except
          xFile := ExtractFileName(Application.ExeName);

          xErrorMsg := 'Check SQLServerName in registry. ' +
            'No ADO connection to ' + SQLServerName + '.' +
            #10#13 + xFile + ' will terminate.' + #10#13 +
            'Error msg: ' + e.Message;
          MessageDlg(xErrorMsg, mtError, [mbOk], 0);
          xErrorMsg := 'Check SQLServerName in registry. ' +
            'No ADO connection to ' + SQLServerName + '. Error msg: ' + e.Message;
          CodeSite.SendError(xErrorMsg);
          mError := True;
        end;
      end;
    end;
    CodeSite.SendDateTime('Time : ' , Now);
  end;
end;
//------------------------------------------------------------------------------
end.

