{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_MMDiagnostic_SupportFunc.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: - Erstellt die Rubriken & den Report
|                 - Rubriken:
|                   1. Machine
|                   2. Operating System
|                   3. ADO
|                   4. XML
|                   5. MS Firewall
|                   6. Printers
|                   7. MillMaster
|                   8. XML Map Files
|                   9. MM Personal & Polices
|                   10. SQL Server
|                   11. Texnet
|                   12. WSC
|                   13. Machine park
|                   14. Service
|                   15. Installed Programs
|                   16. Running Processes
|                   17. Registry
|                   18. MM Files
| Info..........: - open with ModelMaker
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 24.07.2003  1.00  SDo | File created
| 15.12.2004  1.01  SDo | XML & Firewall Rubrik eingebaut
| 25.05.2005  1.00  SDo | Neu: DecimalSymbol & DigitGroupingSymbol in Kl. TOs (Locale Info)
|                       | -> LogOnAs LocalUser & MMSystemUser
| 30.05.2005  1.00  SDo | - DHCPServerEnabled ins Printout eingebaut
| 23.09.2005  1.05  SDo | Neu: Ausgabe Default Printer fur MMSystemUser (nur MMHost)
|                       | Wird der Kl. TMachinePrintes von aussen zugewiesen
| 19.11.2005  1.06  SDo | MMVisual eingebaut
| 28.11.2005  1.06  SDo | LZE/LX Handler eingebaut
| 23.07.2007  1.07  SDo | Vista Absturz: RTF-Report Komp. erh�lt kein Default Printer?!!
|                       | Func. PrepareReport() : Property  xReport.EditWidth:= rewWindowSize setzen
===============================================================================}



(*:********************************************************************
 *  Modulname:        u_MMDiagnostic_SupportFunc
 *  Kurzbeschreibung:
 *
 *  Beschreibung:
    FGHFGHFGH
 ************************************************************************)
{{
FGHFGHFGH
}
unit u_MMDiagnostic_SupportFunc;

interface

uses Windows, Messages, SysUtils, Classes,
     MMList, wwriched;

     procedure CreateRubrics(var aRubricList : TMMList);
     procedure DestroyRubrics(var aRubricList : TMMList);
     procedure PrepareReport(var aRubricList : TMMList; aShowReport : Boolean; aRichEdit : TwwDBRichEdit; var aErrorTxt: String);

     procedure SetCategoryHeader(var aReport: TwwDBRichEdit; aID: Word; aHeader: String);
     procedure SetValue(var aReport: TwwDBRichEdit; aLabel, aValue: string);   overload;
     procedure SetValue(var aReport: TwwDBRichEdit; aLabel, aValue: string; aValuePos_cm: Real ); overload;
     procedure SetValueNonBullet(var aReport: TwwDBRichEdit; aValue: string); overload;
     procedure SetValueNonBullet(var aReport: TwwDBRichEdit; aValue: string; aValuePos_cm: Real ); overload;

     procedure SendProcessStatus(aValue:Integer);
     procedure SendWaitStatusON_OFF(aMSGON_OFF:Integer);

var   gDiagnosticHost : Boolean;
      gRubricList : TMMList;
      gHandle     : THandle;
      gMessageApplID : THandle;
      gMMSystemUserToken : THandle;

const cTab          = #9;
      cLabelFactor  = 550;
      cBulletFactor = cLabelFactor;
      cValue1Factor = 568;
      cPath         = 'c:\temp';
      cFile         = 'DiagnosticReport.rtf';

      cMsgAppl               = 'MMDiagnostics';
      WM_MMDiagnisticStatus  = WM_USER + 1;
      WM_AnalyzeWaitStatusOn  = WM_USER + 2;
      WM_AnalyzeWaitStatusOFF = WM_USER + 3;

implementation


uses Netware, u_MMCategory, mmRichEdit, ShellAPI, FileCtrl, Controls, Forms,
     Dialogs, graphics, mmCS, RzCSIntf, LoepfeGlobal, Printers;


//------------------------------------------------------------------------------
procedure DestroyRubrics(var aRubricList : TMMList);
var xOB : TBaseCategory;
begin
  while aRubricList.Count > 0 do begin
        xOB :=  TBaseCategory(aRubricList.Items[0]);
        xOB.Free;
        aRubricList.Delete(0);
        Application.ProcessMessages;
  end;
end;
//------------------------------------------------------------------------------
procedure CreateRubrics(var aRubricList : TMMList);
var x, xDelta    : integer;
    xOB  : TObject;
    xMSG : String;
    xList : TStringList;
begin
  //Srv + WKS + EASY
  EnterMethod('CreateRubrics');

  try
    DestroyRubrics(aRubricList);
  finally
  end;

  SendProcessStatus(2);
  //Reihenfolge fuer Ausdruck bestimmen
  try
    if gDiagnosticHost then begin
      //MM-Host
      aRubricList.Add( TMachineHost.Create );
     Application.ProcessMessages;
      aRubricList.Add( TOS.Create(gMMSystemUserToken) );
     Application.ProcessMessages;
      aRubricList.Add( TAdo.Create );
     Application.ProcessMessages;
      aRubricList.Add( TXML.Create );
     Application.ProcessMessages;
      aRubricList.Add( TFirewall.Create );
     Application.ProcessMessages;
      aRubricList.Add( TMachinePrinters.Create );
     Application.ProcessMessages;
      aRubricList.Add( TMillMasterHost.Create );
     Application.ProcessMessages;
      aRubricList.Add( TXMLMapFiles.Create );
     Application.ProcessMessages;
      aRubricList.Add( TMMPersonalHost.Create );
     Application.ProcessMessages;
      aRubricList.Add( TSQLServer.Create );
     Application.ProcessMessages;
      aRubricList.Add( TTexnet.Create );
     Application.ProcessMessages;
      aRubricList.Add( TWSC.Create );
     Application.ProcessMessages;
      aRubricList.Add( TLZE.Create );
     Application.ProcessMessages;
      aRubricList.Add( TMachinePark.Create );
    Application.ProcessMessages;
      aRubricList.Add( THostService.Create );
     Application.ProcessMessages;
      aRubricList.Add( TProgramms.Create );
     Application.ProcessMessages;
      aRubricList.Add( TProcesses.Create );
     Application.ProcessMessages;
      aRubricList.Add( TMachineRegistry.Create );
     Application.ProcessMessages;
      aRubricList.Add( TMMFiles.Create );
     Application.ProcessMessages;
    end else begin
      //MM-Client
      aRubricList.Add( TMachineClient.Create );
     Application.ProcessMessages;
      aRubricList.Add( TOS.Create(gMMSystemUserToken) );
     Application.ProcessMessages;
      aRubricList.Add( TAdo.Create );
     Application.ProcessMessages;
      aRubricList.Add( TSQLServer.Create );
     Application.ProcessMessages;
      aRubricList.Add( TXML.Create );
     Application.ProcessMessages;
      aRubricList.Add( TFirewall.Create );
     Application.ProcessMessages;
      aRubricList.Add( TMachinePrinters.Create );
     Application.ProcessMessages;
      aRubricList.Add( TMillMasterClient.Create );
     Application.ProcessMessages;
      aRubricList.Add( TMMPersonalClient.Create );
     Application.ProcessMessages;
      aRubricList.Add( TService.Create );
     Application.ProcessMessages;
      aRubricList.Add( TProgramms.Create );
     Application.ProcessMessages;
      aRubricList.Add( TProcesses.Create );
     Application.ProcessMessages;
      aRubricList.Add( TMachineRegistry.Create );
     Application.ProcessMessages;
      aRubricList.Add( TMMFiles.Create );
     Application.ProcessMessages;
    end;

    aRubricList.Add( TMMVisual.Create );
    Application.ProcessMessages;
  except
     on e: Exception do begin
        xList := TStringList.Create;
        try
          for x:= 0 to aRubricList.Count -1 do
             xList.Add(TObject(aRubricList.Items[x]).ClassName );

        finally
          xMSG := Format( 'Error in rubric class. Created classes %s. Error msg: %s ',[xList.CommaText, e.Message]);
          xList.Free;
          CodeSite.SendError(xMSG);
          MessageDLG(xMSG, mtError, [mbOK],0 );
        end;
        exit;
     end;
  end;

//  xDelta := (100-mProcessStatus) div (gRubricList.Count ) ;
  try
    if Assigned(gRubricList) then
      if gRubricList.Count > 0 then
        xDelta := 100 div (gRubricList.Count ) + 1
      else Exit
    else Exit;


    CodeSite.SendMsg(IntToSTr(gRubricList.Count) + ' rubricts created' );

    for x:= 0 to aRubricList.Count -1 do begin
        Application.ProcessMessages;
        try
          SendProcessStatus(xDelta);
          xOB := TObject(aRubricList.Items[x]);
          CodeSite.SendMsg('Rubricts : ' +  xOB.Classname + '.FetchData');
          {
          //Dauert etwas laenger; Wait-Meldung ausgeben
          SendWaitStatusON_OFF(WM_AnalyzeWaitStatusOn);
          Application.ProcessMessages;
          (xOB as TBaseCategory).FetchData;
          SendWaitStatusON_OFF(WM_AnalyzeWaitStatusOFF);
          Application.ProcessMessages;
          }

          xMSG := xOB.Classname;

          //Dauert etwas laenger; Wait-Meldung ausgeben
          if (xMSG = 'TMillMasterHost') or (xMSG = 'TMillMasterClient') or
             (xMSG = 'TMachineClient') or (xMSG = 'TMachineHost') then begin

             SendWaitStatusON_OFF(WM_AnalyzeWaitStatusOn);
             (xOB as TBaseCategory).FetchData;
             SendWaitStatusON_OFF(WM_AnalyzeWaitStatusOFF);
          end else
            (xOB as TBaseCategory).FetchData;

        except
           on e: Exception do begin
              xMSG := Format( 'Error in rubric class %s.FetchData. Error msg: %s ',[xOB.Classname, e.Message]);
              CodeSite.SendError(xMSG);
              MessageDLG(xMSG, mtError, [mbOK],0 );
           end;
        end;
    end;
  finally
  end;
end;
//------------------------------------------------------------------------------
procedure PrepareReport(var aRubricList : TMMList; aShowReport : Boolean; aRichEdit : TwwDBRichEdit; var aErrorTxt: String);
var xCount, x, n, xID : integer;
    xOB     : TObject;
    xText, xTxt1, xMachine, xValue, xRubricTxt : String;
    xReport : TwwDBRichEdit;
    xForm : TForm;
    xPathFile : String;
    xRet : Boolean;
    xFirewallPort : integer;
begin

  try
    EnterMethod('PrepareReport');

    if aRubricList.Count <= 0 then Exit;

    xReport:=   aRichEdit;
    xReport.Clear;

    xReport.EditorOptions:= [reoShowSaveAs, reoShowSaveExit, reoShowPrint, reoShowPageSetup, reoShowStatusBar, reoShowRuler];

    xReport.PopupOptions :=[];
    xReport.SetBold(TRUE);

    xReport.EditorPosition.Top  := 1;
    xReport.EditorPosition.Left := 1;
    xReport.EditorPosition.Width:= Screen.Width;
    xReport.EditorPosition.Height:= Screen.Height-50;

    xReport.Paragraph.FirstIndent := 20;
    xReport.PrintJobName :=  xReport.EditorCaption;
    xReport.Font.Size := 10;
    xReport.MeasurementUnits := muCentimeters;
    //xReport.MeasurementUnits := muInches;

    xReport.PrintPageSize := DMPAPER_A4; //9; -> Anpassen Letter oder A4 !!!
    xReport.BorderWidth := 10;

    //xReport.EditWidth:= rewPrinterSize;  // -> ab Vista wird ein Error ausgel�st
                                           //    Error MSG: "Kein Default Printer ausgewaehlt", obwohl ein Default Printer selektiert ist!!???

    xReport.EditWidth:= rewWindowSize;

    //Header
    xReport.SetBullet(FALSE);
    xReport.SetRichEditFontAttributes('Arial', 17 , [fsBold, fsUnderline], clBlack);
    xReport.Paragraph.Alignment := taCenter;
    xText := 'MillMaster Diagnostic';
    xReport.Lines.Add(xText);
    xReport.Lines.Add('');
    xReport.Lines.Add('');
    xReport.Paragraph.Alignment := taLeftJustify;

    xReport.SetRichEditFontAttributes('Arial', 10 , [], clBlack);

    if gDiagnosticHost then
       xText := 'Diagnose the ''MillMaster Host'' machine'
    else
       xText := 'Diagnose the ''MillMaster Client'' machine';

    xReport.Lines.Add(xText);

    xText := Format('Date : %s  Time : %s',[DateToStr(Date), TimeToStr(Time)]);
    xReport.Lines.Add(xText);

    xText := Format('MMDiagnostic version : %s',[GetFileVersion(Application.ExeName)] );
    xReport.Lines.Add(xText);

    xReport.Lines.Add('');
    xReport.Lines.Add('');

    //Daten ausgeben
    for xCount:= 0 to gRubricList.Count-1 do begin
        xID := xCount +1;
        xOB :=  TObject(gRubricList.Items[xCount]);

      try
        //ADO ------------------------------------------------------------------
        if (xOB is TAdo) then begin

           CodeSite.SendMsg('Report ADO');
           SetCategoryHeader(xReport, xID, 'ADO');

           xReport.SetBullet(TRUE);

           case TAdo(xOB).Exists of
              TRUE  :  xText :=  'yes';
              FALSE :  xText :=  'no';
           end;
           SetValue(xReport,  'ADO installed', xText);

           SetValue(xReport,  'ADO version', TAdo(xOB).Version);
        end;
        //ADO ------------------------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;

      try
      //XML ------------------------------------------------------------------
        if (xOB is TXML) then begin
           CodeSite.SendMsg('Report XML');
           SetCategoryHeader(xReport, xID, 'XML');
           xReport.SetBullet(TRUE);
           xRet := TXML(xOB).Exists;
           case xRet of
              TRUE  :  xText :=  'yes';
              FALSE :  xText :=  'no';
           end;
           SetValue(xReport,  'XML installed', xText);
           if xRet then
              SetValue(xReport,  'XML version', TXML(xOB).Version);
        end;
        //XML ------------------------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;

      try
      //XML Map files ----------------------------------------------------------
        if (xOB is TXMLMapFiles) then begin
           CodeSite.SendMsg('Report XML Map-Files');
           SetCategoryHeader(xReport, xID, 'XML Map Files');
           xReport.SetBullet(TRUE);
           n:= TXMLMapFiles(xOB).MapFileCount;
           if n > 0 then begin
              for x:= 0 to n-1 do begin
                  xText := TXMLMapFiles(xOB).Mapfile[x].Version;
                  if xText = '' then xText:= cUnknown;
                  xText := Format('Version :  %s', [ xText ] );
                  SetValue(xReport, TXMLMapFiles(xOB).Mapfile[x].FileName, xText, 4 );
              end;
            end else begin
             xText := '';
             SetValue(xReport,  'no XML map files exists', xText);
           end;
        end;
      //XML Map files ----------------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;


      try
        //OS -------------------------------------------------------------------
        if (xOB is TOS) then begin
           CodeSite.SendMsg('Report OS');
           SetCategoryHeader(xReport, xID, 'Operating System');

           SetValue(xReport, 'Machine type:',  TOS(xOB).Product);

           SetValue(xReport, 'Operating System:',  TOS(xOB).OSType);

           xText := Format('%d ',[TOS(xOB).ServicePack]) ;
           SetValue(xReport, 'Service Pack:',  xText);

           SetValue(xReport, 'Operating System Language:',  TOS(xOB).OSLanguage);

           case TOS(xOB).ExistsMultiLanguage of
              TRUE  :  xText :=  'yes';
              FALSE :  xText :=  'no';
           end;

           SetValue(xReport, 'Multi language installed:',  xText);

           xText := Format('Country settings: (%s)',[TOS(xOB).LogonUserName]) ;
           SetValue(xReport, xText,  TOS(xOB).CountrySettings);

           xText := Format('Keybord settings: (%s)',[TOS(xOB).LogonUserName]) ;
           SetValue(xReport, xText,  TOS(xOB).KeyboardSet);

           xText := Format('Date time format: (%s)',[TOS(xOB).LogonUserName]) ;
           SetValue(xReport, xText, TOS(xOB).DateTimeFormat);


           xText := Format('Decimal sysmbol: (%s)',[TOS(xOB).LogonUserName]) ;
           SetValue(xReport, xText,  TOS(xOB).LogonUserDecimalSymbol);

           xText := Format('Digit grouping symbol: (%s)',[TOS(xOB).LogonUserName]) ;
           SetValue(xReport, xText,  TOS(xOB).LogonUserDigitGroupingSymbol);


           xText := Format('Decimal sysmbol: (%s)',['MillMaster system user']) ;
           SetValue(xReport, xText,  TOS(xOB).MMSystemUserDecimalSymbol);

           xText := Format('Digit grouping symbol: (%s)',['MillMaster system user']) ;
           SetValue(xReport, xText,  TOS(xOB).MMSystemUserDigitGroupingSymbol);

        end;
        //OS -------------------------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;

      try
      //Firewall ---------------------------------------------------------------
        if (xOB is TFirewall) then begin
           CodeSite.SendMsg('Report Firewall');
           SetCategoryHeader(xReport, xID, 'MS Firewall');
           xReport.SetBullet(TRUE);
           xRet := TFirewall(xOB).ExistsMSFirewall;
           case xRet of
              TRUE  :  xText :=  'yes';
              FALSE :  xText :=  'no';
           end;
           SetValue(xReport,  'MS Firewall installed', xText);
           if xRet then begin
              xRet := TFirewall(xOB).RunningMSFirewall;
              case xRet of
                 TRUE  :  xText :=  'yes';
                 FALSE :  xText :=  'no';
              end;
              SetValue(xReport,  'Firewall running', xText);

              xFirewallPort := TFirewall(xOB).IsPort135Open;
              if xFirewallPort >=0 then begin
                xRet := Boolean(xFirewallPort);
                case xRet of
                   TRUE  :  xText :=  'open';
                   FALSE :  xText :=  'close';
                end;
              end else
                xText := cUnknown;
              SetValue(xReport,  'Port 135', xText);

              xFirewallPort := TFirewall(xOB).IsPort138Open;
              if xFirewallPort >=0 then begin
                xRet := Boolean(xFirewallPort);
                case xRet of
                   TRUE  :  xText :=  'open';
                   FALSE :  xText :=  'close';
                end;
              end else
                xText := cUnknown;
              SetValue(xReport,  'Port 138', xText);

           end;
        end;
        //Firewall -------------------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;


      try
        //Printers -------------------------------------------------------------
        if (xOB is TMachinePrinters) then begin
            CodeSite.SendMsg('Report Printers');
            SetCategoryHeader(xReport, xID, 'Printers');
            for xID:= 0 to TMachinePrinters(xOB).Count-1 do begin
              xText := Format('Port : %s',[ TMachinePrinters(xOB).Printers[xID].Port]);
              SetValue(xReport, TMachinePrinters(xOB).Printers[xID].PrinterName,  xText );

              //SetValue(xReport, TMachinePrinters(xOB).Printer[xID].PrinterName, xText, 10 );
            end;

            if TMachinePrinters(xOB).Count <= 0 then
               SetValue(xReport, 'Printer:',  'not instelled')
            else
              if gDiagnosticHost then begin
                xText := 'Default printer:';
                if gMMSystemUserDafaultPrinter = '' then
                   xTxt1 := Format('"%s" for logon user',[ GetDefaultPrinter ])
                else begin
                  TMachinePrinters(xOB).MMSystemUserDefaultPrinter := gMMSystemUserDafaultPrinter;
                  xTxt1 := Format('"%s" for MMSysetmUser on %s',[ TMachinePrinters(xOB).MMSystemUserDefaultPrinter, TMachinePrinters(xOB).MMSystemUserLocation]);
                end;
                SetValue(xReport, xText, xTxt1 );
              end;

        end;
        //Printers -------------------------------------------------------------

        //Programms-------------------------------------------------------------
        if (xOB is TProgramms) then begin
            CodeSite.SendMsg('Report Programs');
            SetCategoryHeader(xReport, xID, 'Installed Programs');

            for xID:= 0 to TProgramms(xOB).Count-1 do
              SetValue(xReport, TProgramms(xOB).Programs[xID], '' );

        end;
        //Programms-------------------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;

      try
        //Machine Base ---------------------------------------------------------
        if (xOB is TMachineClient) or (xOB is TMachineHost) then begin
            CodeSite.SendMsg('Report Machine base');
            SetCategoryHeader(xReport, xID, 'Machine');

            SetValue(xReport, 'Computer name:' , TMachineBase(xOB).ComputerName );

            if TMachineBase(xOB).IsWorkgroup then
               xText := 'Workgroup:'
            else
               xText := 'Domain:';
            SetValue(xReport, xText,  TMachineBase(xOB).Domain);

            SetValue(xReport, 'Company name:' , TMachineBase(xOB).CompanyName );

            SetValue(xReport, 'Logon user:' , TMachineBase(xOB).LogonUser );
            SetValue(xReport, 'Logon server:' , TMachineBase(xOB).LogonServer );

            case TMachineBase(xOB).HasLogonUserAdminright of
              TRUE  : xText := 'yes';
              FALSE : xText := 'no';
            end;
            SetValue(xReport, 'Logon user has administrator right:' , xText );

            SetValue(xReport, 'Processor:' , TMachineBase(xOB).Processor );
            SetValue(xReport, 'Number of processor(s) :' , IntToStr(TMachineBase(xOB).ProcessorNumber ));
            SetValue(xReport, 'Disk total space:' , TMachineBase(xOB).PartitionsSize_GB );
            SetValue(xReport, 'Disk free space:' , TMachineBase(xOB).PartitionFreeSpace_GB );

            xText := Format('%d MB',[TMachineBase(xOB).RAMTotal_MB]);
            SetValue(xReport, 'Total memory (RAM) :' , xText );
            xText := Format('%d MB',[TMachineBase(xOB).RAMFree_MB]);
            SetValue(xReport, 'Available memory (RAM):' ,  xText);


            if TMachineBase(xOB).ExistsTCPIP then begin
               SetValue(xReport, 'Network adapter:' , TMachineBase(xOB).NetworkAdapter);
               SetValue(xReport, 'MAC address:' , TMachineBase(xOB).MACAdress);

               if TMachineBase(xOB).DHCPServerEnabled then
                  SetValue(xReport, 'Obtain an IP address automatically :' , 'Yes')
               else
                  SetValue(xReport, 'Obtain an IP address automatically :' , 'No');

               SetValue(xReport, 'TCP / IP address:' , TMachineBase(xOB).TCPIP);
               SetValue(xReport, 'Subnet mask:' , TMachineBase(xOB).SubNetMask);
            end else
               SetValue(xReport, 'Network adapter:' ,  'does not exists');


            if TMachineBase(xOB).ExistsRegDHCPMediaSense then
               if TMachineBase(xOB).IsDHCPMediaSenseEnabled then
                  SetValue(xReport, 'Media Sense on the DHCP client:' , 'switched on')
               else
                  SetValue(xReport, 'Media Sense on the DHCP client:' , 'switched off')
            else
               SetValue(xReport, 'Media Sense on the DHCP client:' , 'not installed');

        end;
        //Machine Base ---------------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;
        {
        //Machine Clinet--------------------------------------------------------
        if (xOB is TMachineClient) then begin
            //SetCategoryHeader(xReport, xID, 'Machine');

            case TMachineClient(xOB).ConncetionToMMHost of
               TRUE  : xText := 'yes';
               FALSE : xText := 'no' + cTab + TMachineClient(xOB).HostConnectionMSG;
            end;

            SetValue(xReport, 'Connection to MMHost "' + TMachineClient(xOB).MMHost + '"' , xText );

            case TMachineClient(xOB).ConnectionToMM_Winding of
               TRUE  : xText := 'yes';
               FALSE : xText := 'no';
            end;
            SetValue(xReport, 'Connection to Database MM_Winding', xText );
        end;
        //Machine Clinet--------------------------------------------------------
        }

      try
        //Machine Host--------------------------------------------------------
        if (xOB is TMachineHost) then begin
           CodeSite.SendMsg('Report Machine host');
           if TMachineHost(xOB).Modems.Count <= 0 then
               SetValue(xReport, 'Modem:' , 'not installed' )
           else
             for x:= 0 to TMachineHost(xOB).Modems.Count-1 do begin
                 SetValue(xReport,'Modem:',TMachineHost(xOB).Modems.Strings[x]);

                 //xText:= Format('%d. Modem' + cTab + '%s:', [x+1, TMachineHost(xOB).Modem[x].Name]);
                 //SetValue(xReport, xText , 'Port : ' + TMachineHost(xOB).Modem[x].Port );
             end;

            if TMachineHost(xOB).UPS = '' then
               SetValue(xReport, 'UPS:' , 'not installed' )
            else
               SetValue(xReport, 'UPS:' , TMachineHost(xOB).UPS);
         end;
        //Machine Host----------------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;

      try
        //MillMaster Base-------------------------------------------------------
         if (xOB is TMillMasterHost) or (xOB is TMillMasterClient) then begin
            CodeSite.SendMsg('Report Machine host or clinet');
            SetCategoryHeader(xReport, xID, 'MillMaster');
            if not TMillMasterBase(xOB).ExistsMM then
               SetValue(xReport, 'MillMaster is not installed!' , '' )
            else begin
                xText :=  TMillMasterBase(xOB).MillMasterLogOn;
                if xText = '' then xText:= cUnknown;
                SetValue(xReport, 'Logon in MillMaster as:' , xText );

                xText :=  TMillMasterBase(xOB).MMHost;
                if xText = '' then xText:= cUnknown;
                xText := Format('Connection to MMHost (%s):', [xText] );
                if TMillMasterBase(xOB).ConnectionToMMHost then
                   xTxt1 := 'yes'
                else
                   xTxt1 := 'no';
                SetValue(xReport, xText , xTxt1 );

                xText := Format('Connection to MillMaster DB:', [TMillMasterBase(xOB).MMHost ] );
                if TMillMasterBase(xOB).ConnectionToMM_Winding then
                   xTxt1 := 'yes'
                else
                   xTxt1 := 'no';
                SetValue(xReport, xText , xTxt1 );

                SetValue(xReport, 'Database version:' , TMillMasterBase(xOB).Version );
                SetValue(xReport, 'MillMaster Package:' , TMillMasterBase(xOB).Package );
                SetValue(xReport, 'MillMaster Host:' , TMillMasterBase(xOB).MMHost );
                SetValue(xReport, 'MillMaster User Host:' , TMillMasterBase(xOB).MMUserHost );
                SetValue(xReport, 'Domain Controller:' , TMillMasterBase(xOB).MMDCName );
                SetValue(xReport, 'Domain Names:' , TMillMasterBase(xOB).MMDomainNames );
                for x:= 0 to TMillMasterBase(xOB).MachinesToPing.count-1 do begin
                    xText :=  TMillMasterBase(xOB).MachinesToPing.Strings[x];
                    if xText <> '' then begin
                        SetValue(xReport, Format('Ping to "%s" : ', [xText]) ,
                                          TMillMasterBase(xOB).PingTo(xText) );
                    end;
                end;


                SetValue(xReport, 'MillMaster status:' , TMillMasterBase(xOB).MMStatus );
                SetValue(xReport, 'MillMaster running since:' , TMillMasterBase(xOB).LastMMStart );
                SetValue(xReport, 'MillMaster running duration:' , TMillMasterBase(xOB).RunningDuration );


                //xText :=  TMillMasterBase(xOB).RunningSince;
                //SetValue(xReport, 'MillMaster running since :' , xText );

                xText :=  TMillMasterBase(xOB).MillMasterPath;
                SetValue(xReport, 'MillMaster path:', xText );


                if (xOB is TMillMasterHost) then begin
                    for xID:= 0 to TMillMasterHost(xOB).ShiftCalCount-1 do begin
                        xText := TMillMasterHost(xOB).ShiftCalendar[xID].Name;
                        SetValue(xReport, 'Shift calendar: ' +  xText,
                                          Format('Last shift from %s: %s',[xText, TMillMasterHost(xOB).ShiftCalendar[xID].Lastdate]) );
    //                                      'Last shift from : ' + TMillMasterHost(xOB).ShiftCalendar[xID].Lastdate);

                    end;

                    //SQL-LoginUsers
                    for x:= 0 to TMillMasterHost(xOB).SQLLoginUsers.Count-1 do begin
                        xText :=  TMillMasterHost(xOB).SQLLoginUsers.strings[x];
                        SetValue(xReport,'SQL login user:', xText);
                    end;

                    //SQL-WindingMaster Users
                    for x:= 0 to TMillMasterHost(xOB).SQLMMWindingUsers.Count-1 do begin
                        xText :=  TMillMasterHost(xOB).SQLMMWindingUsers.strings[x];
                        SetValue(xReport,'SQL MMWinding user:', xText);
                    end;

                    xText :=  Format('%d', [TMillMasterHost(xOB).SQLTriggerCount]);
                    SetValue(xReport,'SQL Trigger count:', xText);

                    //SQL-Storedprocedures
                    for x:= 0 to TMillMasterHost(xOB).SQLStoredProcNames.Count-1 do begin
                        xText :=  TMillMasterHost(xOB).SQLStoredProcNames.strings[x];
                        SetValue(xReport,'SQLStored procedures:', xText);
                    end;

                    //SQL-Views
                    SetValue(xReport,'SQL views count:', IntToSTr(TMillMasterHost(xOB).SQLViewList.Count-1));

                    for x:= 0 to TMillMasterHost(xOB).SQLViewList.Count-1 do begin
                        xText :=  TMillMasterHost(xOB).SQLViewList.strings[x];
                        SetValue(xReport,'SQL views:', xText);
                    end;


                end;
            end;
            //MillMaster Base---------------------------------------------------
         end;
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;

      try
            //SQL-Server--------------------------------------------------------

            if (xOB is TSQLServer) then begin
               CodeSite.SendMsg('Report SQL Server');
               SetCategoryHeader(xReport, xID, 'SQL Server');

               if TSQLServer(xOB).ExistsMM then begin
                  SetValue(xReport,'SQL Server on Machine:', TSQLServer(xOB).Host);
                  if TSQLServer(xOB).SQLServerRunning = TRUE then begin
                     SetValue(xReport,'SQL Server status :', 'running');
                     SetValue(xReport,'Winding Master Database size [MB]:', TSQLServer(xOB).SizeOfMMWindingDB_MB);
                     SetValue(xReport,'Transaction log file size [MB]:', TSQLServer(xOB).SizeOfTransactionLog_MB);
                     SetValue(xReport,'Free disk space of MM-WindingMaster [GB]:', TSQLServer(xOB).MMWindingFreeDiskSapce_GB);

                     SetValue(xReport,'SQL Server Type:', TSQLServer(xOB).SQLType);
                     SetValue(xReport,'SQL Server Version:', TSQLServer(xOB).Version);
                     SetValue(xReport,'SQL Server Service Pack:', TSQLServer(xOB).ServicePack);
                     SetValue(xReport,'SQL Server language:', TSQLServer(xOB).LocalSQLLanguage);
                  end else
                     SetValue(xReport,'SQL Server status :', 'not running');
               end else
                  if not TSQLServer(xOB).ExistsSQL then
                     SetValue(xReport,'SQL Server not installed','')
                  else if TSQLServer(xOB).LocalSQLEdition <> '' then begin
                     SetValue(xReport,'SQL Server on Machine:', 'local');
                     if TSQLServer(xOB).SQLServerRunning = TRUE then begin
                        SetValue(xReport,'SQL Server Type:', TSQLServer(xOB).LocalSQLEdition);
                        SetValue(xReport,'SQL Server Version:', TSQLServer(xOB).LocalSQLVersion);
                        SetValue(xReport,'SQL Server Service Pack:', TSQLServer(xOB).LocalSQLSPVersion);
                        SetValue(xReport,'SQL Server language:', TSQLServer(xOB).LocalSQLLanguage);
                     end;
                  end else
                     SetValue(xReport,'WindingMaster Database does not exits!','');

            end;
            //SQL-Server--------------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;

      try
            //Service-----------------------------------------------------------
            if (xOB is TService) or (xOB is THostService) then begin
               CodeSite.SendMsg('Report Service');
               SetCategoryHeader(xReport, xID, 'Service');
               for x:= 0 to TService(xOB).Count - 1 do begin
                   TService(xOB).GetService(x);
                   if not TService(xOB).ExistsService then
                     SetValue(xReport, TService(xOB).DisplayName + ':', 'Not installed')
                   else begin
                     SetValue(xReport, TService(xOB).DisplayName + ':', 'installed');
                     SetValue(xReport, TService(xOB).DisplayName + ' status:', TService(xOB).StatusText);
                     SetValue(xReport, TService(xOB).DisplayName + ' start type:', TService(xOB).StartTypeText);
                     SetValue(xReport, TService(xOB).DisplayName + ' logon as:', TService(xOB).LogonAs);
                   end;
               end;
            end;
            //Service-----------------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;

      try
            //Textnet-----------------------------------------------------------
            if (xOB is TTexnet) then begin
               CodeSite.SendMsg('Report Texnet');
               SetCategoryHeader(xReport, xID, 'Texnet');

               if not TTexnet(xOB).TexnetInstalled  then begin
                  SetValue(xReport, 'Texnet:', 'not installed');

                  SetValue(xReport, 'Texnet Adapter count:', IntToStr(TTexnet(xOB).AdapterCount) );

                  if not TTexnet(xOB).TexNetDriverRunning  then
                     SetValue(xReport, 'Texnet driver running:', 'no')
                  else
                     SetValue(xReport, 'Texnet driver running:', 'yes');

                  if TTexnet(xOB).ProcessTexnetHandler <> '' then begin
                     xText :=  TTexnet(xOB).ProcessTexnetHandler;
                     SetValue(xReport, 'Texnet process path:', xText);
                  end else
                     SetValue(xReport, 'Texnet process path:', 'none');
               end else begin
                  SetValue(xReport, 'Texnet:', 'installed');

                  SetValue(xReport, 'Texnet Adapter count:', IntToStr(TTexnet(xOB).AdapterCount) );

                  if not TTexnet(xOB).TexNetDriverRunning  then
                     SetValue(xReport, 'Texnet driver running:', 'no')
                  else
                     SetValue(xReport, 'Texnet driver running:', 'yes');

                  SetValue(xReport, 'Texnet adapter type:', TTexnet(xOB).AdapterType);
                  if TTexnet(xOB).TexNetDriverRunning then
                      SetValue(xReport, 'Texnet driver status:', 'running')
                  else
                      SetValue(xReport, 'Texnet driver status:', 'not running');

                  xText :=  TTexnet(xOB).ProcessTexnetHandler;
                  //xText :=  StringReplace(xText, '\', '\\', [rfReplaceAll]);
                  SetValue(xReport, 'Texnet process path:', xText);
               end;


               for x:= 0 to TTexnet(xOB).DriverList.Count -1 do
                   SetValue(xReport, TTexnet(xOB).DriverList.Strings[x] , '');

               xText:= Format('%d', [TTexnet(xOB).MachinesCount]);
               SetValue(xReport, 'Texnet machines count:', xText);

               for x:= 0 to TTexnet(xOB).MachinesCount -1 do begin

                   xMachine := Format('Machine: %s        Node ID: %s     Status: %s     Machine ID: %s     Last upload : %s ',
                                      [ TTexnet(xOB).Machines[x].MachineName,
                                        TTexnet(xOB).Machines[x].NodeID,
                                        TTexnet(xOB).Machines[x].MachineStatus,
                                        TTexnet(xOB).Machines[x].MachineID,
                                        TTexnet(xOB).Machines[x].LastUpload
                                      ] );

                   SetValue(xReport, xMachine, '');

               end;
            end;
            //Textnet-----------------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;

      try
            //WSC---------------------------------------------------------------
            if (xOB is TWSC) then begin
               CodeSite.SendMsg('Report WSC');
               SetCategoryHeader(xReport, xID, 'WSC');

               if not TWSC(xOB).DistinctInstalled  then
                  SetValue(xReport, 'Distinct:', 'not installed')
               else begin
                  SetValue(xReport, 'Distinct:', 'installed');
                  SetValue(xReport, 'Distinct version:', TWSC(xOB).DistinctVersion);
               end;


               if TWSC(xOB).ProcessWSCHandler <> '' then begin
                  xText :=  TWSC(xOB).ProcessWSCHandler;
                  //xText :=  StringReplace(xText, '\', '\\', [rfReplaceAll]);
                  SetValue(xReport, 'WSC process path:', xText);
               end else
                  SetValue(xReport, 'WSC process path:', 'none');


               xText:= Format('%d', [TWSC(xOB).MachinesCount]);
               SetValue(xReport, 'WSC machines count:', xText);

               for x:= 0 to TWSC(xOB).MachinesCount -1 do begin
{
                   xMachine := Format('Machine: %s        Node ID: %s     Status: %s     Machine ID: %s     Last upload : %s ',
                                      [ TWSC(xOB).Machines[x].MachineName,
                                        TWSC(xOB).Machines[x].NodeID,
                                        TWSC(xOB).Machines[x].MachineStatus,
                                        TWSC(xOB).Machines[x].MachineID,
                                        TWSC(xOB).Machines[x].LastUpload
                                      ] );
}
                   xMachine := Format('Machine: %s        Node ID: %s     Status: %s     Machine ID: %s',
                                      [ TWSC(xOB).Machines[x].MachineName,
                                        TWSC(xOB).Machines[x].NodeID,
                                        TWSC(xOB).Machines[x].MachineStatus,
                                        TWSC(xOB).Machines[x].MachineID
                                      ] );

                   SetValue(xReport, xMachine, '');
                   //SetValueNonBullet(xReport, xMachine, 2.3);

                   xMachine := Format('Last upload : %s ', [ TWSC(xOB).Machines[x].LastUpload ]);
                   SetValueNonBullet(xReport, xMachine, 3.3);
                   //SetValue(xReport, xMachine, '');

               end;
            end;
            //WSC---------------------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;

      try
            //LZE---------------------------------------------------------------
            if (xOB is TLZE) then begin
               CodeSite.SendMsg('Report LX');
               SetCategoryHeader(xReport, xID, 'LX');

               if not TLZE(xOB).LXInstalled  then
                  SetValue(xReport, 'LX Handler:', 'not installed')
               else begin
                  SetValue(xReport, 'LX Handler:', 'installed');
                  if TLZE(xOB).ProcessLZXHandler <> '' then begin
                    xText :=  TLZE(xOB).ProcessLZXHandler;
                    SetValue(xReport, 'LX process path:', xText);
                  end else
                    SetValue(xReport, 'LX process path:', 'none');


                  xText:= Format('%d', [TLZE(xOB).MachinesCount]);
                  SetValue(xReport, 'LX machines count:', xText);

                  for x:= 0 to TLZE(xOB).MachinesCount -1 do begin

                      xMachine := Format('Machine: %s        Node ID: %s     Status: %s     Machine ID: %s',
                                      [ TLZE(xOB).Machines[x].MachineName,
                                        TLZE(xOB).Machines[x].NodeID,
                                        TLZE(xOB).Machines[x].MachineStatus,
                                        TLZE(xOB).Machines[x].MachineID
                                      ] );

                      SetValue(xReport, xMachine, '');

                      xMachine := Format('Last upload : %s ', [ TLZE(xOB).Machines[x].LastUpload ]);
                      SetValueNonBullet(xReport, xMachine, 3.3);
                  end;
               end;
            end;
            //LZE---------------------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;



      try
            //Registry----------------------------------------------------------
            if (xOB is TMachineRegistry) then begin
               CodeSite.SendMsg('Report Registry');
               SetCategoryHeader(xReport, xID, 'Registry');
               for x:= 0 to TMachineRegistry(xOB).Count -1 do begin

                   //Key (gleicher Key nur einmal ausgeben)
                   if x= 0 then begin
                      xText := Format( '%s',[ TMachineRegistry(xOB).RegistryRec[x].Key] );
                      //xText := StringReplace(xText,'\', '\\', [rfReplaceAll]);
                      SetValue(xReport, xText,'' );
                   end else
                      if TMachineRegistry(xOB).RegistryRec[x].Key <> TMachineRegistry(xOB).RegistryRec[x-1].Key then  begin
                         xText := Format('%s',[ TMachineRegistry(xOB).RegistryRec[x].Key]);
                         //xText := StringReplace(xText,'\', '\\', [rfReplaceAll]);
                         SetValue(xReport, xText,'' );
                       end;

                   //Werte
                   xText := Format('  %s = %s',[ TMachineRegistry(xOB).RegistryRec[x].ValueName, TMachineRegistry(xOB).RegistryRec[x].Value]);
                   //xText := StringReplace(xText,'\', '\\', [rfReplaceAll]);
                   SetValueNonBullet(xReport, xText, 2.3);
               end;

            end;
            //Registry----------------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;

      try
            //MMPersonal Host---------------------------------------------------
            if (xOB is TMMPersonalHost) then begin
               CodeSite.SendMsg('Report MMPersonal host');
               SetCategoryHeader(xReport, xID, 'MM Personal & Polices');

               //GroupMembers
               for x:= 0 to TMMPersonalHost(xOB).MMPersonalCount -1 do begin
                   if x= 0 then
                      SetValueNonBullet(xReport, 'Groups : ');

                   xText :=  TMMPersonalHost(xOB).MMPersonal[x].Group + ' : ';
                   //xTxt1 := StringReplace(TMMPersonalHost(xOB).MMPersonal[x].GroupMembers,'\', '\\', [rfReplaceAll]);
                   xTxt1 := TMMPersonalHost(xOB).MMPersonal[x].GroupMembers;
                   xTxt1 := StringReplace(xTxt1,',', ', ', [rfReplaceAll]);

                   SetValue(xReport, xText , '' );
                   SetValueNonBullet(xReport, xTxt1, 2.3);

               end;
               xReport.Lines.Add('');
               //Polices
               if TMMPersonalHost(xOB).PolicesCount > 0 then
                   for x:= 0 to TMMPersonalHost(xOB).PolicesCount -1 do begin
                      if x= 0 then
                         SetValueNonBullet(xReport, 'Polices : ');

                      xText :=  TMMPersonalHost(xOB).Polices[x].Privileg;

                      if Pos('Police error', xText) = 0 then begin
                         //xTxt1 := StringReplace(TMMPersonalHost(xOB).Polices[x].Members,'\', '\\', [rfReplaceAll]);
                         xTxt1 := TMMPersonalHost(xOB).Polices[x].Members;
                         xTxt1 := StringReplace(xTxt1,',', ', ', [rfReplaceAll]);
                         SetValue(xReport, xText , '' );
                         SetValueNonBullet(xReport, xTxt1, 2.3);
                      end else
                         SetValue(xReport, xText , '' );
                   end
               else

                  SetValue(xReport, 'No MillMaster polices defined' , '' );

               xReport.Lines.Add('');

               if TMMPersonalHost(xOB).NetAccessSafetyModel <> '' then begin
                  SetValue(xReport, 'Network access: Sharing and security model for local accounts', '' );
                  SetValueNonBullet(xReport, TMMPersonalHost(xOB).NetAccessSafetyModel, 2.3);
               end;

            end;
            //MMPersonal Host---------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;

      try
            //MMPersonal Client-------------------------------------------------
            if (xOB is TMMPersonalClient) then begin
               CodeSite.SendMsg('Report MMPersonal client');
               SetCategoryHeader(xReport, xID, 'MM Personal & Polices');

               //GroupMembers
               for x:= 0 to TMMPersonalClient(xOB).MMPersonalCount -1 do begin
                   if x= 0 then
                      SetValueNonBullet(xReport, 'Groups : ');

                   xText :=  TMMPersonalClient(xOB).MMPersonal[x].Group + ' : ';
//                   xTxt1 := StringReplace(TMMPersonalClient(xOB).MMPersonal[x].GroupMembers,'\', '\\', [rfReplaceAll]);
                   xTxt1 := TMMPersonalClient(xOB).MMPersonal[x].GroupMembers;
                   xTxt1 := StringReplace(xTxt1,',', ', ', [rfReplaceAll]);

                   SetValue(xReport, xText , '' );
                   SetValueNonBullet(xReport, xTxt1, 2.3);

               end;
               xReport.Lines.Add('');
               //Polices
               if TMMPersonalClient(xOB).PolicesCount > 0 then
                   for x:= 0 to TMMPersonalClient(xOB).PolicesCount -1 do begin
                      if x= 0 then
                         SetValueNonBullet(xReport, 'Polices : ');

                      xText :=  TMMPersonalClient(xOB).Polices[x].Privileg;

                      if Pos('Police error', xText) = 0 then begin
//                         xTxt1 := StringReplace(TMMPersonalClient(xOB).Polices[x].Members,'\', '\\', [rfReplaceAll]);
                         xTxt1 := TMMPersonalClient(xOB).Polices[x].Members;
                         xTxt1 := StringReplace(xTxt1,',', ', ', [rfReplaceAll]);
                         SetValue(xReport, xText , '' );
                         SetValueNonBullet(xReport, xTxt1, 2.3);
                      end else
                         SetValue(xReport, xText , '' );
                   end
               else

                  SetValue(xReport, 'No MillMaster polices defined' , '' );

               xReport.Lines.Add('');

               if TMMPersonalClient(xOB).NetAccessSafetyModel <> '' then begin
                  SetValue(xReport, 'Network access: Sharing and security model for local accounts', '' );
                  SetValueNonBullet(xReport, TMMPersonalClient(xOB).NetAccessSafetyModel, 2.3);
               end;
            end;
            //MMPersonal Client-------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;

      try
            //MM Files ---------------------------------------------------------
            if (xOB is TMMFiles) then begin
               CodeSite.SendMsg('Report Files');
               SetCategoryHeader(xReport, xID, 'MM Files');

               //Path (gleicher Path nur einmal ausgeben)
               for x:= 0 to TMMFiles(xOB).MMFileCount -1 do begin
                   if x= 0 then begin
                          xText := Format( '%s',[ TMMFiles(xOB).MMFileRec[x].Path] );
//                          xText := StringReplace(xText,'\', '\\', [rfReplaceAll]);
                          SetValue(xReport, xText,'' );
                   end else
                          if TMMFiles(xOB).MMFileRec[x].Path <> TMMFiles(xOB).MMFileRec[x-1].Path then  begin
                             xText := Format('%s',[ TMMFiles(xOB).MMFileRec[x].Path]);
//                             xText := StringReplace(xText,'\', '\\', [rfReplaceAll]);
                             SetValue(xReport, xText,'' );
                           end;

                   //Werte
                   if ( TMMFiles(xOB).MMFileRec[x].Version ) <> '' then
                      xText := Format('  %s      version %s',[ TMMFiles(xOB).MMFileRec[x].MMFile, TMMFiles(xOB).MMFileRec[x].Version])
                   else
                      xText := Format('  %s',[ TMMFiles(xOB).MMFileRec[x].MMFile]);

//                   xText := StringReplace(xText,'\', '\\', [rfReplaceAll]);
                   SetValueNonBullet(xReport, xText, 2.3);
               end;

               if TMMFiles(xOB).MMFileCount <=0 then
                  SetValue(xReport,'MillMaster files not installed','');
            end;
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;

      try
         //Running Preocesses---------------------------------------------------
         if (xOB is TProcesses) then begin
             CodeSite.SendMsg('Report Processes');
             SetCategoryHeader(xReport, xID, 'Running Processes');

             for x:= 0 to TProcesses(xOB).ProcessCount -1 do begin
                 xText := TProcesses(xOB).Process[x].Name;
                 xTxt1 := Format('PDI = %d,   CPU Time = %s,  MemUsage = %d KB,   Handles = %d,   Threads = %d',
                                  [TProcesses(xOB).Process[x].PID,
                                   TProcesses(xOB).Process[x].CPUTime,
                                   TProcesses(xOB).Process[x].MemUsage DIV 1024,
                                   TProcesses(xOB).Process[x].Handles,
                                   TProcesses(xOB).Process[x].Threads ]);

                 SetValue(xReport, xText, xTxt1, 5 );
             end;
             xText := Format('Process count : %d', [TProcesses(xOB).ProcessCount] );
             SetValue(xReport, xText, '' );

             xReport.Lines.Add('');
             xText := Format('Machine running since : %s h', [TProcesses(xOB).MachineRunningTime] );
             SetValue(xReport, xText, '' );

         end;
         //Running Preocesses---------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;


      try
         //Machine Park /Informator --------------------------------------------
         if (xOB is TMachinePark) then begin
             CodeSite.SendMsg('Report Machine Park');
             SetCategoryHeader(xReport, xID, 'Machine park');

             if TMachinePark(xOB).MachineCount > 0 then
                for x:= 0 to TMachinePark(xOB).MachineCount -1 do begin
                    //Zeile 1
                    xText := Format('Net Node: %s      Machine Name: %s      Machine ID: %s      Sensing head: %s ',
                                   [ TMachinePark(xOB).Machines[x].NetNode,
                                     TMachinePark(xOB).Machines[x].MachineName,
                                     TMachinePark(xOB).Machines[x].MachineID,
                                     TMachinePark(xOB).Machines[x].TastKopf ]);
                    SetValue(xReport, xText, '' );

                    //Zeile 2
                    xText := Format('Machine status: %s     Machine type: %s     Last upload: %s',
                                    [ TMachinePark(xOB).Machines[x].MachineStatus,
                                      TMachinePark(xOB).Machines[x].MachineType,
                                      TMachinePark(xOB).Machines[x].UploadDate ]);
                    SetValueNonBullet(xReport, xText, 2.3);

                    //Zeile 3
                    if TMachinePark(xOB).Machines[x].OverloadMapFile <> '' then begin
                       xText := Format('Version: %s      YM Options: %s      Overloaded XML-Map file: %s',
                                       [ TMachinePark(xOB).Machines[x].Version,
                                         TMachinePark(xOB).Machines[x].YMOptions,
                                         TMachinePark(xOB).Machines[x].OverloadMapFile]);
                       SetValueNonBullet(xReport, xText, 2.3);
                    end else begin
                        xText := Format('Version: %s      YM Options: %s',
                                       [ TMachinePark(xOB).Machines[x].Version,
                                         TMachinePark(xOB).Machines[x].YMOptions  ]);
                        SetValueNonBullet(xReport, xText, 2.3);
                    end;
                end

             else  begin
                if TMachinePark(xOB).WindingMasterConnect and (TMachinePark(xOB).ErrorMSG = '') then
                   SetValue(xReport, 'No Machines installed', '' )
                else
                   SetValue(xReport, 'No connection to WindingMaster DB. ' + TMachinePark(xOB).ErrorMSG , '' );
             end;
          end;
         //Machine Park /Informator --------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;


      try
         //MMVisual ------------------------------------------------------------
         if (xOB is TMMVisual) then begin
             CodeSite.SendMsg('MMVisual');
             SetCategoryHeader(xReport, xID, 'MMVisual');

             xRet := TMMVisual(xOB).Exists;
             case xRet of
                TRUE  :  xText :=  'yes';
                FALSE :  xText :=  'no';
             end;
             SetValue(xReport,  'MMVisual installed:', xText);

             if xRet then begin
                 SetValue(xReport,  'SQL Server name:', TMMVisual(xOB).SQLServerName);
                 SetValue(xReport,  'MMVisual DB version:', TMMVisual(xOB).MMVisualDBVersion);
                 SetValue(xReport,  'MMVisual File version:', TMMVisual(xOB).MMVisualVersion);
                 SetValue(xReport,  'Oasys version:', TMMVisual(xOB).OasysVersion);
                 SetValue(xReport,  'Virtual COM version:', TMMVisual(xOB).VirtualCOMversion);
             end;
         end;
         //MMVisual ------------------------------------------------------------
      except
         on E: Exception do begin
            xRubricTxt:= Format('%s; %s   ', [xOB.ClassName, E.Message]) ;
         end;
      end;

      xReport.Lines.Add('');
      xReport.SetBullet(FALSE);
    end;

    Application.ProcessMessages;

    sleep(1000);

    SendProcessStatus(0);

    if aShowReport then begin
       xReport.Execute;
    end else begin
       xPathFile:= cPath + cFile;
       ForceDirectories(cPath);
       xReport.Lines.SaveToFile(xPathFile);
    end;

    if not aShowReport then
       ShowMessage(xPathFile);

  finally
    //xReport.Free;
   // xForm.Free;
  end;
end;
//------------------------------------------------------------------------------


procedure SetCategoryHeader(var aReport: TwwDBRichEdit; aID: Word; aHeader: String);
var xText: String;
begin
  aReport.SetBullet(FALSE);
  aReport.SetRichEditFontAttributes('Arial', 15 , [fsBold], clBlue);
  xText := Format('%d. %s',[aID, aHeader]);
  aReport.Lines.Add(xText);
end;
//------------------------------------------------------------------------------
procedure SetValue(var aReport: TwwDBRichEdit; aLabel, aValue: string);
var xBulletAbsPos, xLabelAbsPos, xValueAbsPos:  Integer;
    xText :String;
begin
   aValue :=  StringReplace(aValue, '\', '\\', [rfReplaceAll]);
   aLabel :=  StringReplace(aLabel, '\', '\\', [rfReplaceAll]);

   xLabelAbsPos  := Round(cLabelFactor * 2); //2cm
   xBulletAbsPos := xLabelAbsPos - Round(cBulletFactor * 1.5);  //1.5cm
   xValueAbsPos  := Round(cValue1Factor * 9);  // 8cm

   aReport.SetBullet(TRUE);
   aReport.SetRichEditFontAttributes('MS Sans Serif', 8 , [], clBlack);
   xText :=Format('{\rtf\fi-%d\li%d\tx%d %s \f2\tab\f1 %s\par}',[xBulletAbsPos, xLabelAbsPos, xValueAbsPos, aLabel, aValue ]);
   aReport.Lines.Add(xText);
   aReport.SetBullet(FALSE);


//   xText :='{\rtf\pard{\pntext\f3\''B7\tab}{\*\pn\pnlvlblt\pnf3\pnindent0{\pntxtb\''B7}}\fi-720\li1136\tx4260 Multi language installed :\f2\tab\f1 no\par';


end;
//------------------------------------------------------------------------------
procedure SetValue(var aReport: TwwDBRichEdit; aLabel, aValue: string; aValuePos_cm: Real);
var xBulletAbsPos, xLabelAbsPos, xValueAbsPos:  Integer;
    xText :String;
    x : variant;
begin
   aValue :=  StringReplace(aValue, '\', '\\', [rfReplaceAll]);
   aLabel :=  StringReplace(aLabel, '\', '\\', [rfReplaceAll]);
   if aValuePos_cm <  2 then  aValuePos_cm := 8;

   xLabelAbsPos  := Round(cLabelFactor * 2); //2cm
   xBulletAbsPos := xLabelAbsPos - Round(cBulletFactor * 1.5);  //1.5cm
   xValueAbsPos  := Round(cValue1Factor * aValuePos_cm);  // 8cm

   x:= aReport.PrintPageSize;

   aReport.SetBullet(TRUE);
   aReport.SetRichEditFontAttributes('MS Sans Serif', 8 , [], clBlack);
   xText :=Format('{\rtf\fi-%d\li%d\tx%d %s \f2\tab\f1 %s\par}',[xBulletAbsPos, xLabelAbsPos, xValueAbsPos, aLabel, aValue ]);
//   xText :=Format('{\rtf\fi-%d\li%d\tx%d %s \f2\tab\f1 %s\par}',[xBulletAbsPos, xLabelAbsPos, xValueAbsPos, aLabel, aValue ]);

   aReport.Lines.Add(xText);
   aReport.SetBullet(FALSE);
end;
//------------------------------------------------------------------------------
procedure SetValueNonBullet(var aReport: TwwDBRichEdit; aValue: string);
var xBulletAbsPos, xValueAbsPos: Integer;
    xText :String;
begin
   aValue :=  StringReplace(aValue, '\', '\\', [rfReplaceAll]);
   xValueAbsPos  := Round(cLabelFactor * 1.8); //1.8cm
   xBulletAbsPos := xValueAbsPos - Round(cBulletFactor * 1.5);  //1.5cm

   aReport.SetBullet(FALSE);
   aReport.SetRichEditFontAttributes('MS Sans Serif', 8 , [], clBlack);
   xText :=Format('{\rtf\fi-%d\li%d\f2\tab %s \par}',[xBulletAbsPos, xValueAbsPos, aValue ]);

   aReport.Lines.Add(xText);
end;
//------------------------------------------------------------------------------
procedure SetValueNonBullet(var aReport: TwwDBRichEdit; aValue: string; aValuePos_cm: Real);
var xBulletAbsPos, xValueAbsPos: Integer;
    xText :String;
begin
   aValue :=  StringReplace(aValue, '\', '\\', [rfReplaceAll]);
   xValueAbsPos  := Round(cLabelFactor * aValuePos_cm); //x cm
   xBulletAbsPos := xValueAbsPos - Round(cBulletFactor * 1.5);  //1.5cm

   aReport.SetBullet(FALSE);
   aReport.SetRichEditFontAttributes('MS Sans Serif', 8 , [], clBlack);
   xText :=Format('{\rtf\fi-%d\li%d\f2\tab %s \par}',[xBulletAbsPos, xValueAbsPos, aValue ]);

   aReport.Lines.Add(xText);
end;
//------------------------------------------------------------------------------
procedure SendProcessStatus(aValue:Integer);
var xRecp: DWord;
begin
  xRecp := BSM_APPLICATIONS;
  BroadcastSystemMessage(BSF_POSTMESSAGE, @xRecp, gMessageApplID, WM_MMDiagnisticStatus, aValue);
end;
//------------------------------------------------------------------------------
procedure SendWaitStatusON_OFF(aMSGON_OFF:Integer);
var xRecp: DWord;
begin
  xRecp := BSM_APPLICATIONS;
  BroadcastSystemMessage(BSF_POSTMESSAGE, @xRecp, gMessageApplID, aMSGON_OFF, 0);
end;
//------------------------------------------------------------------------------

initialization
  gRubricList := TMMList.Create;
  gHandle     := 0;
  gMessageApplID := RegisterWindowMessage(cMsgAppl);


finalization
  DestroyRubrics(gRubricList);
  gRubricList.Free;

end.
