{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MMDiagnostic.dpr
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: -
| Info..........: - open with ModelMaker
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 24.07.2003  1.00  SDo | File created
| 15.12.2004  1.01  SDo | Prog. als Admin laufen lassen
| 25.05.2005  1.03  SDo | Neu: DecimalSymbol & DigitGroupingSymbol in Kl. TOs (Locale Info)
|                       | -> LogOnAs LocalUser & MMSystemUser;  gMMSystemUserToken
| 23.05.2005  1.04  SDo | Neu: Default printer fuer MMSystemUser auslesen
|                       | -> muss als erstes ausgelesen werden,
|                       | weil sonst der Def.Printer nur vom LogonUser ausgelesen wird
===============================================================================}
program MMDiagnostic;

uses
  Forms,
  Dialogs,
  sysutils,
  NetWare,
  LoepfeGlobal,
  mmCS,
  Windows,
  JclSecurity,
  Printers,
  u_MMDiagnosticMain in 'u_MMDiagnosticMain.pas' {DiagnosticForm},
  u_MMCategory in 'u_MMCategory.PAS',
  ProcessesServices in 'ProcessesServices.pas',
  u_MMDiagnostic_SupportFunc in 'u_MMDiagnostic_SupportFunc.pas',
  u_SQLLogin in 'u_SQLLogin.pas' {SQLLogin};

//  u_ISFirewall in '\\Wetsrvbde2\Develop\MillMaster\Installation\SetupDLLs_ISX\u_ISFirewall.pas';



{$R *.RES}
{$R 'Version.res'}

{
function GetDefaultPrinter: string;

  var
    xResStr: array[0..255] of Char;
    xDefaultPrinter : string;
    x : Integer;

begin
  GetProfileString('Windows', 'device', '', xResStr, 255);

  xDefaultPrinter := StrPas(xResStr);

  ShowMessage(xDefaultPrinter);

  x:= Printer.Printers.Count;
  for x := 0 to Printer.Printers.Count - 1 do begin
      if AnsiPos(Printer.Printers.Strings[x], xDefaultPrinter) > 0 then  begin
         Result := Printer.Printers.Strings[x];
         Break;
      end;
  end;
end;
}
//------------------------------------------------------------------------------

function SetLogonUser: Boolean;
var xUsername, xPassword, xDomain :PChar;
    xResult, xRet : Boolean;
    xSID : PSID;
begin

   xUsername := cMMSystemuser;
   xDomain := '';

   //Check: Existiert User lokal ?
   xSID:= GetUserSid(xDomain, xUsername);

   if xSID = NIL then begin
      //Check: Existiert User auf Domain ?
      xDomain := PChar(GetLogOnServer);
      xSID:= GetUserSid(xDomain, xUsername);
   end;

   xPassword := cMMSystemuserPWD_alt;
   //Der LogonUser muss bereits das Privileg SE_TCB_NAME besitzen !!!
   xResult := FALSE;
   xResult := LogonUser(xUsername, xDomain, xPassword, LOGON32_LOGON_INTERACTIVE,
                        LOGON32_PROVIDER_DEFAULT, gMMSystemUserToken);

   if not xResult then begin
      //Neues PWD ab MM-Vers. 4.2
      xPassword := cMMSystemuserPWD_neu;
      xResult := LogonUser(xUsername, xDomain, xPassword, LOGON32_LOGON_INTERACTIVE,
                           LOGON32_PROVIDER_DEFAULT, gMMSystemUserToken);
   end;

   if xResult then begin
      xResult := ImpersonateLoggedOnUser(gMMSystemUserToken);
   end;

   Result := xResult;
end;
//------------------------------------------------------------------------------


var xMutex : THandle;
    xName :String;
    xUsername, xPassword, xDomain: PChar;
    xUserToken, xUserToken1, Token: THandle;
    xResult: Boolean;
    xSID : PSID;
begin


 try
  gMMSystemUserToken :=0;

  //Programm nur einmal pro PC laufen lassen
  xName:= ExtractFileName(Application.ExeName);
  xMutex := CreateMutex( NIL, True, PChar(  xName ));
  if  (xMutex <> 0) and (GetLastError <> 0) then begin
      xName := Format('%s is already running.',[xName]);
      MessageDlg(xName, mtInformation, [mbOk], 0);
      CloseHandle(xMutex);
      exit;
  end;

  //Laeuft erst ab W2K
  if GetOSMajorVersion < 5 then begin
     MessageDlg('Diagnostics needs Windows 2000 or higher.', mtWarning, [mbOk], 0);
     Application.Terminate;
     exit;
  end;

  EnterMethod('MMDiagnostic init');

  gMMLogUser   :=  GetCurrentMMUser;   //Muss global gesetzt werden, weil Prog.LogOnAs Admin oder MMsystemUser -> lesen in TMillMasterBase & TMachineBase
  gOSLogonUser :=  GetWindowsUserName; //Muss global gesetzt werden, weil Prog.LogOnAs Admin oder MMsystemUser -> lesen in TMillMasterBase & TMachineBase

  Codesite.SendMsg('MillMaster Logon user : ' + gMMLogUser);
  Codesite.SendMsg('OS Logon user : ' + gOSLogonUser);


  if not IsAdministrator then begin

  {
      xUsername := 'MMSystemUser';
      xDomain := '';

      //Check: Existiert User lokal ?
      xSID:= GetUserSid(xDomain, xUsername);

      if xSID = NIL then begin
         //Check: Existiert User auf Domain ?
         xDomain := PChar(GetLogOnServer);
         xSID:= GetUserSid(xDomain, xUsername);
      end;

      xPassword := 'tub5zoe';
      //Der LogonUser muss bereits das Privileg SE_TCB_NAME besitzen !!!
      xResult := FALSE;
      xResult := LogonUser(xUsername, xDomain, xPassword, LOGON32_LOGON_INTERACTIVE,
                           LOGON32_PROVIDER_DEFAULT, gMMSystemUserToken);

      if not xResult then begin
         //Neues PWD ab MM-Vers. 4.2
         xPassword := 'Sup5erMM';
         xResult := LogonUser(xUsername, xDomain, xPassword, LOGON32_LOGON_INTERACTIVE,
                              LOGON32_PROVIDER_DEFAULT, gMMSystemUserToken);
      end;

      if xResult then begin
        xResult := ImpersonateLoggedOnUser(gMMSystemUserToken);
      end;
}
      xResult := SetLogonUser;
      if not xResult then
         MessageDlg('Diagnostics needs an administrator right.' + #10#13 +
                    'Some system inspections will not work correctly!', mtWarning, [mbOk], 0)
      else
         Codesite.SendMsg('Logon user ' + gOSLogonUser  + 'is now logged in as ' +  xUsername);
  end;

  if AnsiPos( cMMSystemuser, gMMLogUser ) <= 0 then begin
     if gMMSystemUserToken = 0 then begin
        xResult := SetLogonUser;
        if xResult then
           CodeSite.SendMsg('Set "Run as" MMSystemUser')
        else
           CodeSite.SendMsg('MMSystemUser does not exists');

     end;
  end;

  gMMSystemUserDafaultPrinter := '';
  if xResult then
     gMMSystemUserDafaultPrinter := GetDefaultPrinter;

  CodeSite.SendMsg('Default printer for MMSystemUser : ' + gMMSystemUserDafaultPrinter);

  gApplicationName := Trim( StringReplace( ExtractFileName( Application.ExeName ),
                                           ExtractFileExt( Application.ExeName ),
                                           '', [rfReplaceAll] ) );

  CodeSite.Enabled := CodeSiteEnabled;

  Application.Initialize;
  CodeSite.SendMsg('Application.Initialize -> done');

  Application.CreateForm(TDiagnosticForm, DiagnosticForm);
  CodeSite.SendMsg('Application.CreateForm() -> done');

  CodeSite.SendMsg('Start ' + gApplicationName);
  Application.Run;
 except
    on E: Exception do begin   
          MessageDlg('Diagnostics unknown error.' + #10#13 +
                      #10#13 + 'Error msg : '  + #10#13 +
                      e.Message, mtError, [mbOk], 0);

       Application.Terminate;
    end;
 end;

 CloseHandle(gMMSystemUserToken);

 if (xMutex <> 0) then CloseHandle(xMutex);

 CodeSite.SendMsg('Terminated ' + gApplicationName);

 RevertToSelf();

 Application.Terminate;

end.
