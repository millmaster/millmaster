unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    StringGrid1: TStringGrid;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;


const

  cUser = 'MMSystemSQL';
  cPWD  = 'netpmek32';


  cGetAllMachines = 'select m.c_machine_id MachineID, m.c_node_id NodeID, Machine_State = ' +
                    'CASE c_machine_state '+
                    '   WHEN 1 THEN ''Running'' '+
                    '   WHEN 2 THEN ''No data collection'' '+
                    '   WHEN 3 THEN ''Offline'' '+
                    '   WHEN 4 THEN ''In synchronisation'' '+
                    '  ELSE ''Undefined'' '+
                    ' END, '+
                    ' t.c_machine_model Machine, m.c_last_confirmed_upload LastUpload, m.c_net_id, '+
                    ' m.c_nr_of_spindles SpindleCount, m.c_OptionCode YMOptions, m.c_YM_Version Version '+
                    ' from t_machine m, t_net n, t_machine_type t '+
                    ' where (m.c_machine_type=t.c_machine_type) and (m.c_net_id=n.c_net_id) '+
                    ' Order by c_machine_model, m.c_net_id asc ';

   cGetAllHeadTypes = 'select distinct(c_head_type) ' +
                      'from t_spindle ' +
                      'where c_machine_id = :MachId';


   cGetAllSpindlePos = 'select sp.c_spindle_id SpindlePos,  ht.c_head_model Model ' +
                       'from t_spindle sp, t_Head_type ht ' +
                       'where sp.c_machine_id = :MachID ' +
                       'and sp.c_head_type = :HeadType '+
                       'and sp.c_head_type = ht.c_head_type '+
                       'order by sp.c_spindle_id asc' ;



implementation

{$R *.DFM}
uses LoepfeGlobal, mmADOConnection, AdoDbAccess;


procedure TForm1.Button1Click(Sender: TObject);
var xRow, x, n: integer;
    xText, xFrom, xTo, xHeadType : string;
    xValue0, xValue1 , xTempValue : integer;

    xSpindlePos , xSpindleFromTo : TStringList;
begin
 try
    xRow:= 0;

    xSpindlePos    := TStringList.Create;
    xSpindleFromTo := TStringList.Create;


    StringGrid1.ColCount := 10;
    StringGrid1.RowCount := 3;



    StringGrid1.Cells[ 0, 0 ] :=  'MachineID';
    StringGrid1.Cells[ 1, 0 ] :=  'NodeID';
    StringGrid1.Cells[ 2, 0 ] :=  'Machine';
    StringGrid1.Cells[ 3, 0 ] :=  'TastKopf';
    StringGrid1.Cells[ 4, 0 ] :=  'Machine_State';
    StringGrid1.Cells[ 5, 0 ] :=  'LastUpload';
    StringGrid1.Cells[ 6, 0 ] :=  'SpindleCount';
    StringGrid1.Cells[ 7, 0 ] :=  'YMOptions';
    StringGrid1.Cells[ 8, 0 ] :=  'Version';      


    with TAdoDBAccess.Create(4, True) do begin
       try
         HostName := GetRegString(cRegLM, cRegMMCommonPath, 'MMHost', '');
         Init;
         Query[0].Close;
         Query[0].SQL.text := cGetAllMachines;
         Query[0].Open;
         Query[0].First;


         //HeadType ermitteln
         while not Query[0].EOF do begin

           Query[1].Close;
           Query[1].SQL.text := cGetAllHeadTypes;
           Query[1].ParamByName('MachId').AsInteger := Query[0].FieldByName('MachineID').AsInteger;
           Query[1].Open;
           Query[1].First;


           //Alle SpindelPos ermitteln mit MachID = x und HeadType = y
           while not Query[1].EOF do begin
             Query[2].Close;
             Query[2].SQL.text := cGetAllSpindlePos;
             Query[2].ParamByName('MachId').AsInteger := Query[0].FieldByName('MachineID').AsInteger;
             Query[2].ParamByName('HeadType').AsInteger := Query[1].FieldByName('c_head_type').AsInteger;
             Query[2].Open;
             Query[2].First;

             xTempValue :=  Query[1].FieldByName('c_head_type').AsInteger;
             xHeadType :=   Query[2].FieldByName('Model').AsString;

             xFrom := '';
             xTo   := '';
             n:=0;

             xSpindlePos.Clear;
             xSpindleFromTo.Clear;

             // Spindle From / To ermitteln
             while not Query[2].EOF do begin
                  xSpindlePos.Add ( Query[2].FieldByName('SpindlePos').AsString);
                  Query[2].Next;
             end;



             for n:= 0 to xSpindlePos.Count-1 do begin

                 if n = 0 then begin
                    xFrom := xSpindlePos.Strings[n];
                    xTo   := xFrom;
                 end;

                 if n < xSpindlePos.Count-1 then
                    xValue0 := StrToInt( xSpindlePos.Strings[n+1] )
                 else
                    xValue0 := StrToInt( xSpindlePos.Strings[n] );

                xValue1 := StrToInt( xSpindlePos.Strings[n]) + 1;

                if xValue0 > xValue1 then begin
                   xTo :=  xSpindlePos.Strings[n];
                   xSpindleFromTo.Add ( xFrom + '-' + xTo);
                   xFrom := xSpindlePos.Strings[n+1];
                end else if n =  xSpindlePos.Count-1 then begin
                   xTo :=  xSpindlePos.Strings[n];
                   xSpindleFromTo.Add ( xFrom + '-' + xTo);
                end;



                if xTempValue = 9 then
                   beep;

             end;

             for n:= 0 to xSpindleFromTo.Count -1 do begin


                StringGrid1.Cells[ 0, xRow +1 ] :=  Query[0].FieldByName('MachineID').AsString;
                StringGrid1.Cells[ 1, xRow +1 ] :=  Query[0].FieldByName('NodeID').AsString;
                StringGrid1.Cells[ 2, xRow +1 ] :=  Query[0].FieldByName('Machine').AsString;
                StringGrid1.Cells[ 3, xRow +1 ] := Format('%s   (%s)',[xHeadType, xSpindleFromTo.Strings[n]]);
              

                StringGrid1.Cells[ 4, xRow +1 ] :=  Query[0].FieldByName('Machine_State').AsString;


               StringGrid1.Cells[ 5, xRow +1 ] :=  Query[0].FieldByName('LastUpload').AsString;
               StringGrid1.Cells[ 6, xRow +1 ] :=  Query[0].FieldByName('SpindleCount').AsString;
               StringGrid1.Cells[ 7, xRow +1 ] :=  Query[0].FieldByName('YMOptions').AsString;
               StringGrid1.Cells[ 8, xRow +1 ] :=  Query[0].FieldByName('Version').AsString;


                inc(xRow);

                StringGrid1.RowCount := xRow+1;
             end;


            // showMessage(xSpindleFromTo.CommaText);






             Query[1].Next;
          end;


           Query[0].Next;
         end;

       except
          on e: exception do begin
                ShowMessage(e.Message);
                Free;
          end;
       end;
    end; //END with

  except
  end;
    
  xSpindlePos.Free;
  xSpindleFromTo.Free;

  label1.caption := intToStr( StringGrid1.RowCount - 1);

end;

end.
