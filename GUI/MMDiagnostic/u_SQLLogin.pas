{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_SQLLogin.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: -
| Info..........: - open with ModelMaker
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 18.11.2005  1.00  SDo | File created
===============================================================================}
unit u_SQLLogin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, mmPanel;

type
  TSQLLogin = class(TForm)
    btn1: TBitBtn;
    btn2: TBitBtn;
    edtSQLPWD: TEdit;
    edtSQLUser: TEdit;
    Image2: TImage;
    lbl1: TLabel;
    lbl2: TLabel;
    mmPanel3: TmmPanel;
    Panel1: TPanel;
    procedure FormCreate(Sender: TObject);
  private
    function GetSQLPWD: string;
    function GetSQLUser: string;
  public
    function Execute: Boolean;
    property SQLLoginUser: string read GetSQLUser;
    property SQLPWD: string read GetSQLPWD;
  end;
  
var
  SQLLogin: TSQLLogin;

implementation

{$R *.DFM}

{ TSQLLogin }
//------------------------------------------------------------------------------
{
********************************** TSQLLogin ***********************************
}
function TSQLLogin.Execute: Boolean;
begin
  edtSQLUser.Text := '';
  edtSQLPWD.Text := '';
  Result := (ShowModal = mrOK);
end;

procedure TSQLLogin.FormCreate(Sender: TObject);
begin
  edtSQLUser.Text := '';
  edtSQLPWD.Text := '';
end;

function TSQLLogin.GetSQLPWD: string;
begin
  Result := edtSQLPWD.Text;
end;

function TSQLLogin.GetSQLUser: string;
begin
  Result := edtSQLUser.Text;
end;


end.

