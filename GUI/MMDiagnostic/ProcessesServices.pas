{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: ProcessesServices.pas
| Projectpart...:
| Subpart.......: -
| Process(es)...: -
| Description...: - locate all/one active process
|                 - teminate a process
|                 - Start/Stop a service / process
| Info..........:
|
| Target.system.: Windows NT, W2k, XP
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 20.08.2001  1.00  SDo | File created
===============================================================================}
unit ProcessesServices;

interface

uses
  Windows, SysUtils, Classes, StdCtrls, Dialogs;
  //WinSVC, SvcMgr, Psapi, {TLHelp32,} registry, ShellAPI;


type

  TServiceRec = record
    DislpayName: string;
    ServiceName: string;
    StatusText: string;
    Status: Integer;
    StartTypeText: string;
    StartType: Word;
    LogonAs: string;
    Running: Boolean;
    ExistsService: Boolean;
  end;
  
  pService = ^TServiceRec;



  PPROCESS_MEMORY_COUNTERS = ^PROCESS_MEMORY_COUNTERS;
  PROCESS_MEMORY_COUNTERS = record
    cb: DWORD;
    PageFaultCount: DWORD;
    PeakWorkingSetSize: DWORD;
    WorkingSetSize: DWORD;
    QuotaPeakPagedPoolUsage: DWORD;
    QuotaPagedPoolUsage: DWORD;
    QuotaPeakNonPagedPoolUsage: DWORD;
    QuotaNonPagedPoolUsage: DWORD;
    PagefileUsage: DWORD;
    PeakPagefileUsage: DWORD;
  end;
  
  TProcessMemoryCounters = PROCESS_MEMORY_COUNTERS;

function GetProcessMemoryInfo( Process : THandle;
                               var MemoryCounters : TProcessMemoryCounters;
                               cb : DWORD) : BOOL; stdcall;




function TrimFileNameExt(aFile:String):String;


//Process
//+++++++

//Ermittelt alle laufende Processe
procedure GetProcessList(var aProcessList : TStringList);

//Ermittelt, ob ein Process/service aktiv ist
function IsProcessRunning(aProcess: String):Boolean;

//Beendet einen Process
function ProcessTerminate(aProcess:String):Boolean;

//Ermittelt den ProcessHandle eines Programms
function GetProcessHandleFromExeNT(aProcess: String): DWORD;

// Ruft eine Appliaction auf
function CallApp(aSsource, aSourceParam : String; aShowApp : Boolean; aApplWait : Boolean ):Boolean;




function GetTheProcessTimes(aPID: integer): String;
function ProcessMemoryUsage(aProcessID : DWORD): DWORD;



//Services
//++++++++
function ExistsService( aService: string ): Boolean;
function GetServiceInfo( aService: string; var aServiceInfo: TServiceRec ):Boolean;


function GetErrorText(aError:Word):String;


implementation

uses Forms, WinSVC, SvcMgr, Psapi, {TLHelp32,} registry, ShellAPI, LoepfeGlobal, mmCS;

function GetProcessMemoryInfo; external 'psapi.dll';


//******************************************************************************
//Get a CPU Time form a PID
//******************************************************************************
function GetTheProcessTimes(aPID: integer): String;
var
  xLocalFileTime : TFileTime;
  xSystemTime    : TSystemTime;

  xPH : THandle;
  xProcess : THandle;

  xCreationTime,
  xExitTime,
  xlpKernelTime,
  xlpUserTime : TFileTime;

  xKernelDay,  xUserDay : integer;
  xKernelTime, xUserTime : TDateTime;

  xRet, xHours : string;
begin
  xRet := 'n/a';

  xProcess := aPID;

  xPH := OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, xProcess);
  if xPH <> 0 then
    try
      GetProcessTimes(xPH, xCreationTime, xExitTime, xlpKernelTime, xlpUserTime);

      FileTimeToLocalFileTime(xCreationTime, xLocalFileTime);

      //Get the kernel time and format
      FileTimeToSystemTime(xlpKernelTime, xSystemTime);
      xKernelDay := xSystemTime.wDay;
      xKernelTime := SystemTimeToDateTime(xSystemTime);


      //Get the user time and format
      FileTimeToSystemTime(xlpUserTime, xSystemTime);

      xUserDay := xSystemTime.wDay;
      xUserTime := SystemTimeToDateTime(xSystemTime);

      //Calculate the cpu time and format
      xRet := TimeToStr(xUserTime + xKernelTime);
      xHours := Copy(xRet, 1, Pos(':', xRet) - 1);
      Delete(xRet, 1, Pos(':', xRet) - 1);

      Result := IntToStr(( (xUserDay - xKernelDay) * 24) +
                            StrToInt(xHours)) + xRet;

    finally
      CloseHandle(xPH);
    end
end;
//------------------------------------------------------------------------------
function ProcessMemoryUsage(aProcessID : DWORD): DWORD;
var
  xProcessHandle : THandle;
  xMemCounters   : TProcessMemoryCounters;
begin
  Result := 0;
  xProcessHandle := OpenProcess(PROCESS_QUERY_INFORMATION or
                                PROCESS_VM_READ, false, aProcessID );
  try
    if GetProcessMemoryInfo( xProcessHandle, xMemCounters, sizeof(xMemCounters))  then
      Result := xMemCounters.WorkingSetSize;
  finally
    CloseHandle( xProcessHandle );
  end;
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
function GetErrorText(aError:Word):String;
var xMsg: array[1..100] of Char;
    xErr: DWORD;
begin
  FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, nil, aError, 0, @xMsg, sizeof(xMsg), nil);
  Result := IntToStr(aError) + ': ' + StrPas(@xMsg);
end;
//------------------------------------------------------------------------------
procedure GetProcessList(var aProcessList : TStringList);
var
  i                     : Integer;
  xProcessIDNeeded      : DWord;
  xProcessIDList        : array[0..1000] of Integer; // Obergrenze !!!
  xProcessIDName        : array [0..MAX_PATH - 1] of Char;
  xProcessHandle        : THandle;
  xMod                  : HModule;
  xSize2                : DWord;

  xText                 : String;


  xProcInfo: TProcessInformation;
begin

  if not EnumProcesses(@xProcessIDList, 1000, xProcessIDNeeded) then
    raise Exception.Create('PSAPI.DLL doesn''t exists!');
  try

    aProcessList.Clear;

//    for i := 0 to (xProcessIDNeeded div sizeof (Integer)- 1) do
    for i := 0 to (xProcessIDNeeded) do
    begin
     // xProcessHandle := OpenProcess(PROCESS_QUERY_INFORMATION or PROCESS_VM_READ, False, xProcessIDList[i]);
     xProcessHandle := OpenProcess( PROCESS_QUERY_INFORMATION or
                                    PROCESS_VM_READ
                                   , False,
                                   xProcessIDList[i]);

      if xProcessHandle <> 0 then
      begin



        if GetModuleFileNameExA(xProcessHandle, 0, xProcessIDName, sizeof (xProcessIDName)) > 0 then begin

           {
           xText := Format('%s, PID = %d, CPU Time = %s, Mem = %dK',[ExtractFileName(xProcessIDName),
                                                          xProcessIDList[i],
                                                          GetTheProcessTimes( xProcessIDList[i] ),
                                                          ProcessMemoryUsage(xProcessIDList[i]) DIV 1024 ] );
           }
           xText := ExtractFileName(xProcessIDName);
           aProcessList.Add( xText );
        end;

         begin
           if EnumProcessModules(xProcessHandle, @xMod, sizeof(xMod), xSize2) then
              begin
                GetModuleFileNameExA(xProcessHandle,
                                     xMod,
                                     xProcessIDName,
                                     sizeof(xProcessIDName));
                //aProcessList.Add(xProcessIDName);
              end;
            CloseHandle(xProcessHandle);
         end;
      end;
    end;
  except
     raise Exception.Create('Error in GetProcessList() !');
  end;
  //Result := aProcessList;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermitelt ob ein Prozess aktiv ist. (Running Service oder Programm)
//Eingabe : aProcess -> Datei oder Service Name
//******************************************************************************
function IsProcessRunning(aProcess: String): Boolean;
var xStrList : TStringList;
    x: Integer;
    xProcess, xSearchProcess : String;
    xManager, xService  : SC_Handle;
    xServiceStatus      : TServiceStatus;
    xMachine            : PChar;

begin
 Result:= FALSE;

 if aProcess = '' then begin
    Codesite.SendError('Error in ProcessesService.IsProcessRunning(). No process defined');
    Result:= FALSE;
    exit;
 end;

 if ExtractFileExt(aProcess) = '' then
    if not ExistsService(aProcess) then begin
       Result:= FALSE;
       exit;
    end;

 xSearchProcess := ExtractFileName(aProcess);
 xSearchProcess := UpperCase(TrimFileNameExt(xSearchProcess));


 //Check running Programm
 try
   xStrList := TStringList.Create;
   GetProcessList(xStrList);
   xStrList.AddStrings( xStrList );
   x:=0;
   while x < xStrList.Count do begin
     xProcess  := ExtractFileName(xStrList.Strings[x]);
     xProcess  := UpperCase( TrimFileNameExt(xProcess) );
     if StrComp(PChar(xSearchProcess), PChar(xProcess) ) = 0 then begin
        Result := TRUE;
        break;
     end;
     inc(x);
   end;
   xStrList.Free;
 except
   Result:= FALSE;
 end;

 if Result then exit;

 //Check running Service

 try
  xMachine := NIL;
  xManager := OpenSCManager(xMachine, Nil, SC_MANAGER_CONNECT);
  if xManager > 0 then begin
     xService := OpenService(xManager, PChar(aProcess),
                 SERVICE_START or SERVICE_QUERY_STATUS);

     QueryServiceStatus(xService,xServiceStatus);

     if (xService > 0) and (xServiceStatus.dwCurrentState = SERVICE_RUNNING )then
        Result := TRUE;

     CloseServiceHandle(xService);
  end;
  CloseServiceHandle(xManager);

 except
   Result:= FALSE;
 end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Beendet ein Prozess
//Eingabe : aProcess -> FullPath File
//******************************************************************************
function ProcessTerminate(aProcess: String): Boolean;
var xStrList                        : TStringList;
    x                               : Integer;
    xProcess, xSearchProcess, xMsg  : String;
    xID                             : Cardinal;
begin

 Result:= TRUE;
 if not IsProcessRunning(aProcess) then exit;

 Result:= FALSE;

 xStrList := TStringList.Create;
 GetProcessList(xStrList) ;
 //xStrList.AddStrings(xStrList);

 xSearchProcess := ExtractFileName(aProcess);
 xSearchProcess := UpperCase(TrimFileNameExt(xSearchProcess));

 x:=0;
 try
     while x < xStrList.Count do begin
       xProcess := ExtractFileName(xStrList.Strings[x]);
       xProcess := UpperCase(TrimFileNameExt(xProcess));

       if StrIComp(PChar(xSearchProcess), PChar(xProcess) ) = 0 then begin

          //Process Terminate (Process with full path)
          xID := GetProcessHandleFromExeNT(xStrList.Strings[x]);

          if TerminateProcess(xID, 0) then begin
             xMsg := 'Terminated ' + xStrList.Strings[x];
             //CodeSiteSendMsg(PChar(xMsg));
             Result:= TRUE;
             break;
          end else
             Result:= FALSE;
          //break;
       end;
       inc(x);
     end;
 except
     on E: Exception do begin
        xMsg := Format('Error in ProcessTerminate( %s ) : %s ',
                      [aProcess, E.Message]);
     end;
 end;
 xStrList.Free;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Ermittelt den Handle eines Processes  (nur Win-NT)
// Eingabe : aProcess -> FullPath File
//******************************************************************************
function GetProcessHandleFromExeNT(aProcess: String): DWORD;
var i : LongInt;
    xExename : String;
    PIDs : Array[0..1000] of DWord; //process-IDs
    xListSize, xNoOfProcesses : DWord;
    xHandle : THandle;
    xName, xFullname : array [0..MAX_PATH - 1] of char;
Begin
  aProcess := AnsiUpperCase(Trim(aProcess));
  aProcess := ExtractFileName(aProcess);

  Result := 0;
  xListSize := SizeOf(PIDs);
  xNoOfProcesses := 0;
  FillChar(PIDs, xListSize, 0);
  if not EnumProcesses(@PIDs, xListSize, xNoOfProcesses)then RaiseLastWin32Error;
  xNoOfProcesses := xNoOfProcesses div SizeOf(DWord);

  for i := 0 to xNoOfProcesses-1 do begin
    xHandle := OpenProcess(PROCESS_QUERY_INFORMATION + PROCESS_VM_READ +
                           PROCESS_TERMINATE, False, PIDs[i]);
    if xHandle = 0 then begin
      CloseHandle(xHandle);
      Continue;
    end;
    FillChar(xName, SizeOf(xName), 0);
    if GetModuleBaseName(xHandle, 0, @xName, sizeof (xName)) > 0 then begin
      GetModuleFileNameEx(xHandle, 0, @xFullname, sizeof(xFullname));
      xExename := AnsiUpperCase(Trim(StrPas(xFullName)));
      xExename := ExtractFileName(xExename);
      if xExename = aProcess then begin
        Result := xHandle;
        Break;
      end else CloseHandle(xHandle);
    end else CloseHandle(xHandle);
  end;
end;
//------------------------------------------------------------------------------
function ExistsService(aService: string): Boolean;
var xErrorID            : Integer;
    xManager, xService  : SC_Handle;
    xServiceStatus      : TServiceStatus;
    xMachine            : PChar;
    xErrorTxt           : String;
begin
 Result:= FALSE;

 //Check Service
 xErrorID:= 0;
 try
  xMachine := NIL;
  xManager := OpenSCManager(xMachine, Nil, SC_MANAGER_CONNECT);
  if xManager > 0 then begin
     SetLastError(xErrorID);
     xService := OpenService(xManager, PChar(aService),
                 SERVICE_ALL_ACCESS  or SERVICE_QUERY_STATUS);

     if (xService > 0) then
        Result := TRUE
     else
        xErrorID:= GetLastError;

     CloseServiceHandle(xService);
  end;
  CloseServiceHandle(xManager);
 except
   Result:= FALSE;
 end;

 if (xErrorID > 0) and (xErrorID <> 1060) then  begin //1060 = no Service
    xErrorTxt := Format('Service %s error. Error msg: %s',[aService, GetErrorText(xErrorID)]);
    CodeSite.SendError(xErrorTxt);
    raise Exception.Create( GetErrorText(xErrorID) );
 end;

end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt Informationen ueber einen Service
//******************************************************************************
function GetServiceInfo( aService: string; var aServiceInfo: TServiceRec ):Boolean;
var xKey: String;
    xManager, xService  : SC_Handle;
    xServiceStatus      : TServiceStatus;
    xMachine            : PChar;
begin
 Result:= FALSE;
 try
    aServiceInfo.ExistsService := FALSE;
    aServiceInfo.ServiceName   := aService;
    aServiceInfo.Status := 0;
    aServiceInfo.StartType :=  0;
    aServiceInfo.Running := FALSE;
    try
      aServiceInfo.ExistsService := ExistsService(aService);
    except
    end;

    if aServiceInfo.ExistsService  then begin

       xMachine := NIL;
       xManager := OpenSCManager(xMachine, Nil, SC_MANAGER_CONNECT);
       if xManager > 0 then begin
          xService := OpenService(xManager, PChar(aService),
                      SERVICE_START or SERVICE_QUERY_STATUS);

          QueryServiceStatus(xService,xServiceStatus);

          if (xService > 0) then begin
            aServiceInfo.Status := xServiceStatus.dwCurrentState;
            case xServiceStatus.dwCurrentState of
              SERVICE_STOPPED	        : aServiceInfo.StatusText := 'The service is not running';
              SERVICE_START_PENDING	: aServiceInfo.StatusText := 'The service is starting';
              SERVICE_STOP_PENDING	: aServiceInfo.StatusText := 'The service is stopping';
              SERVICE_RUNNING	        : aServiceInfo.StatusText := 'The service is running';
              SERVICE_CONTINUE_PENDING	: aServiceInfo.StatusText := 'The service continue is pending';
              SERVICE_PAUSE_PENDING	: aServiceInfo.StatusText := 'The service pause is pending';
              SERVICE_PAUSED	        : aServiceInfo.StatusText := 'The service is paused';
            end;

            if xServiceStatus.dwCurrentState= SERVICE_RUNNING then aServiceInfo.Running := TRUE;

          end;
          CloseServiceHandle(xService);
       end;
       CloseServiceHandle(xManager);

       aServiceInfo.Running       := IsProcessRunning(aService);
    end;

    xKey := 'SYSTEM\CurrentControlSet\Services\' + aService;
    aServiceInfo.DislpayName := GetRegString(cRegLM, xKey, 'DisplayName', '');

    aServiceInfo.StartType :=  GetRegInteger(cRegLM, xKey, 'Start', 0);
    case aServiceInfo.StartType of
      2 : aServiceInfo.StartTypeText := 'Automatic';
      3 : aServiceInfo.StartTypeText := 'Manual';
      4 : aServiceInfo.StartTypeText := 'Disabled';
      else
        aServiceInfo.StartTypeText := 'unknown';
    end;

    aServiceInfo.LogonAs := GetRegString(cRegLM, xKey, 'ObjectName', '');
    Result:= TRUE;

 except
   Result:= FALSE;
 end;
end;
//------------------------------------------------------------------------------


//******************************************************************************
//Entfernt die Extension einer Datei
//******************************************************************************
function TrimFileNameExt(aFile: String): String;
var xExt, xFile : String;
begin
  xExt:= ExtractFileExt(aFile);
  xFile := aFile;
  if length(xExt) > 0 then
     Delete(xFile, Pos('.', xFile ),4);
  Result := xFile;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Ruft eine Appliaction auf
//******************************************************************************
function CallApp(aSsource, aSourceParam : String; aShowApp : Boolean; aApplWait : Boolean ):Boolean;
var xret, xPos                   : integer;
    xResult                      : Boolean;
    xError, xCommand, xDir, xExt,
    xProg, xTxt                  : String;
    xShellInfo                   : TShellExecuteInfo;
begin

  xResult:= FALSE;

  // WinExec nur fuer Exe Files
  xProg:= aSsource;

  xExt:= UpperCase( ExtractFileExt(xProg) );
  if  StrComp( PChar(xExt), '')  = 0 then begin
        xProg := xProg + '.EXE';
        xExt :=  '.EXE';
  end;

  xCommand := xProg;
  xDir:=       ExtractFileDir(xProg);

  Randomize;
  FillCHar (xShellInfo, SizeOf(xShellInfo),0);
  try
      with xShellInfo do begin
         cbSize       := SizeOf(xShellInfo);
         //Wnd          :=
         lpVerb       := 'Open';
         lpFile       := PChar(xCommand);
         lpParameters := PChar(aSourceParam);
         lpDirectory  := PChar(xDir);

         if aShowApp then
            nShow     := SW_SHOW
         else
         nShow        := SW_HIDE;
         fMask        := SEE_MASK_FLAG_DDEWAIT  or
                         SEE_MASK_FLAG_NO_UI or
                         SEE_MASK_NOCLOSEPROCESS ;
         lpClass      := 'Appl';
         hProcess     := Random(10000);
      end;

      //GetMessage
      Forms.Application.ProcessMessages;
      xResult:= ShellExecuteEx(@xShellInfo);
      Forms.Application.ProcessMessages;

      // Warted bis end of process
      if aApplWait then begin
         WaitForSingleObject(xShellInfo.hProcess, INFINITE);
      end;
      Forms.Application.ProcessMessages;
      xret:= xShellInfo.hInstApp;

      if  (xret <=32) or ((xret >=1156) and (xret <=1157)) or (xret =1223) then begin
        // Errorcode aus ShellExecute und ShellExecuteEx
        case xret of
            0,
       {8}  ERROR_NOT_ENOUGH_MEMORY : xError:= 'The operating system is out of memory or resources.';
       {2}  ERROR_FILE_NOT_FOUND    : xError:= 'The specified file was not found.';
       {3}  ERROR_PATH_NOT_FOUND    : xError:= 'The specified path was not found.';
    {1156}  ERROR_DDE_FAIL	    : xError:= 'The DDE transaction failed.'; //??
    {1155}  ERROR_NO_ASSOCIATION    : xError:= 'There is no application associated with the given filename extension.';
       {5}  ERROR_ACCESS_DENIED     : xError:= 'The operating system denied access to the specified file.';
    {1157}  ERROR_DLL_NOT_FOUND     : xError:= 'The specified dynamic-link library was not found.';
    {1223}  ERROR_CANCELLED	    : xError:= 'The function prompted the user for the location of the application, but the user cancelled the request.';
      {32}  ERROR_SHARING_VIOLATION : xError:= 'A sharing violation occurred.';

      {27}  SE_ERR_ASSOCINCOMPLETE  : xError:= 'The filename association is incomplete or invalid.';
      {30}  SE_ERR_DDEBUSY	    : xError:= 'The DDE transaction could not be completed because other DDE transactions were being processed.';
      {29}  SE_ERR_DDEFAIL	    : xError:= 'The DDE transaction failed.';
      {28}  SE_ERR_DDETIMEOUT	    : xError:= 'The DDE transaction could not be completed because the request timed out.';  //
      {31}  SE_ERR_NOASSOC	    : xError:= 'There is no application associated with the given filename extension.';
      {26}  SE_ERR_SHARE	    : xError:= 'A sharing violation occurred.';
            else
                xError:= 'Unknown error';
        end;
           xResult:= FALSE;
      end else
        xResult:= TRUE;
  except
     on E: Exception do begin

        xResult:= FALSE;
     end;
  end;

  result := xResult;
end;
//------------------------------------------------------------------------------



end.
