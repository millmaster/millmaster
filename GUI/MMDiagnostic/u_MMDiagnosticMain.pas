{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_MMDiagnosticMain.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: -
| Info..........: - open with ModelMaker
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 24.07.2003  1.00  SDo | File created
===============================================================================}
unit u_MMDiagnosticMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmLabel, ComCtrls, mmProgressBar, Buttons, ExtCtrls, mmPanel,
  mmRadioGroup,
  mmCS, RzCSIntf, wwriched, u_MMDiagnostic_SupportFunc, mmTimer, printers;

type

  TDiagnosticForm = class(TForm)
    bbCancel: TBitBtn;
    bbStart: TBitBtn;
    Image2: TImage;
    lbStatusTxt: TmmLabel;
    mmPanel1: TmmPanel;
    mmPanel2: TmmPanel;
    mmPanel3: TmmPanel;
    mmProgressBar1: TmmProgressBar;
    mmTimer1: TmmTimer;
    Panel1: TPanel;
    reReport: TwwDBRichEdit;
    rgDiagnosetype: TmmRadioGroup;
    procedure bbCancelClick(Sender: TObject);
    procedure bbStartClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure mmTimer1Timer(Sender: TObject);
    procedure reReportCloseDialog(Form: TForm);
    procedure reReportInitDialog(Form: TForm);
    procedure reReportMenuSaveAndExitClick(Form: TForm; RichEdit:
            TwwCustomRichEdit; var DoDefault: Boolean);
    procedure reReportMenuSaveAsClick(Form: TForm; RichEdit: TwwCustomRichEdit;
            var DoDefault: Boolean);
    procedure rgDiagnosetypeClick(Sender: TObject);
  private
    mCheckMMClientFirstTime: Boolean;
    mFileSaved: Boolean;
    mMMClientPath: string;
    mMMClientRunning: Boolean;
    mPathFile: string;
    mProcessStatus: Integer;
    procedure CreateRubrics;
    procedure SetProcessStatus(aValue:Integer);
    procedure StartMMClient;
    procedure StopMMClient;
  protected
    procedure WndProc(var Message: TMessage); override;
  end;


const
      cFormCaption = 'MMDiagnostic';
      cReportprepare = 'Report preparing...';
      cAnalyzeMsg = 'Analyze MillMaster System...';
      cNeedTimeMsg = 'Takes several time. Please wait a moment.';

var
  DiagnosticForm: TDiagnosticForm;

implementation

{$R *.DFM}

//Uses Netware, u_MMCategory, mmRichEdit, ShellAPI, FileCtrl;
uses Netware, u_MMCategory, FileCtrl, LoepfeGlobal, ProcessesServices, ShellApi;



{ TDiagnosticBaseForm }

//------------------------------------------------------------------------------


{
******************************* TDiagnosticForm ********************************
}
procedure TDiagnosticForm.bbCancelClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TDiagnosticForm.bbStartClick(Sender: TObject);
var
  xMsg: string;
  xErrorTxt: string;
begin
  bbStart.Enabled := FALSE;
  Screen.Cursor   := crHourGlass;
  xMsg := '';
  try
    StopMMClient;

    reReport.PrintJobName := 'MMDiagnostic';
    reReport.EditorCaption:= Format('MMDiagnostic report   [%s]',[mPathFile]);

    sleep(100);
    mProcessStatus := 0;
    mmProgressBar1.Position:= 0;

    u_MMDiagnostic_SupportFunc.DestroyRubrics(gRubricList);

    Application.ProcessMessages;

    lbStatusTxt.Caption := 'Analyze MillMaster System...';

    gHandle := Handle;
    Application.ProcessMessages;
    CreateRubrics;

    lbStatusTxt.Caption := cReportprepare;
    Application.ProcessMessages;

      //try
     xErrorTxt := '';
     PrepareReport(gRubricList, TRUE, reReport, xErrorTxt); //Report wird angezeigt
  {
      except
       //xMsg := 'Error in PrepareReport. Error msg : ';
       PrepareReport(gRubricList, FALSE, reReport, xErrorTxt); //Report wird in eine Datei geschreiben

       if (xErrorTxt <> '') then
         xMsg := Format('Error in PrepareReport. Error msg : %s ;',[xErrorTxt] )
       else
         xMsg := 'Error in PrepareReport. Error msg : ';

      end;
  }
    Application.ProcessMessages;

  except
    on E: Exception do  begin
          Screen.Cursor   := crDefault;
  //          MessageDlg(xMsg + E.Message, mtError, [mbOk], 0);

         if (xErrorTxt <> '') then
           xMsg := Format('Error in PrepareReport. Error msg : %s ; %s',[xErrorTxt, E.Message] )
         else
           xMsg := 'Error in PrepareReport. Error msg : ' + E.Message;

          MessageDlg(xMsg , mtError, [mbOk], 0);

       end;
  end;

  lbStatusTxt.Caption := '';
  Application.ProcessMessages;

  mmProgressBar1.Position:= 0;

  mCheckMMClientFirstTime := TRUE;

  Screen.Cursor   := crDefault;
  bbStart.Enabled := TRUE;

end;

procedure TDiagnosticForm.CreateRubrics;
var
  x, xDelta: Integer;
  xOB: TObject;
begin
  //Srv + WKS + EASY
  mProcessStatus := 0;
  mmProgressBar1.Position := 0;

  u_MMDiagnostic_SupportFunc.DestroyRubrics(gRubricList);

  u_MMDiagnostic_SupportFunc.CreateRubrics(gRubricList);

end;

procedure TDiagnosticForm.FormCreate(Sender: TObject);
var
  xType: string;
  xProductType: Word;
  xMillMasterPath: string;
begin
  //mRubricList := TMMList.Create;

  EnterMethod('TDiagnosticForm.FormCreate()');

  if GetOSInfo.wProductType = VER_NT_WORKSTATION then
     xType :=  'Workstation'
  else
     xType :=  'Server';

  self.Caption := Format( '%s on a %s', [cFormCaption, xType] );

  CodeSite.SendMsg(self.Caption);

  gDiagnosticHost := TRUE;
  rgDiagnosetype.ItemIndex :=0;
  mProcessStatus := 0;
  mmProgressBar1.Position := 0;

  lbStatusTxt.Caption := '';

  mPathFile := Format('%s\%s',[cPath, cFile]);

  xMillMasterPath := GetRegString(cRegLM, cRegMillMasterPath, 'MILLMASTERROOTPATH', '');
  if xMillMasterPath <> '' then
    //MMClient check
    //mMMClientPath := Format('%s\WindingMaster\MMClient.exe',[xMillMasterPath]);
    mMMClientPath := Format('%s\Bin\MMClient.exe',[xMillMasterPath]);
  try
    CodeSite.SendMsg('IsProcessRunning()');
    mMMClientRunning := IsProcessRunning(mMMClientPath);

    if not mMMClientRunning and (mMMClientPath = '') then
       CodeSite.SendError('MMClient.exe or MillMaster does not exists.');

  except
    mMMClientRunning := FALSE;
  end;
end;

procedure TDiagnosticForm.FormDestroy(Sender: TObject);
begin
   EnterMethod('TDiagnosticForm.FormDestroy()');
  u_MMDiagnostic_SupportFunc.DestroyRubrics(gRubricList);
  //  StartMMClient;
end;

procedure TDiagnosticForm.mmTimer1Timer(Sender: TObject);
begin
  //lbStatusTxt.Caption := Format('%s        %s',[cAnalyzeMsg, cNeedTimeMsg]);
  //Application.ProcessMessages;
end;

procedure TDiagnosticForm.reReportCloseDialog(Form: TForm);
var
  x: Integer;
  xSaveDLG: TSaveDialog;
  xFileName, xPath: string;
begin

  for x:= 0 to Form.ComponentCount-1 do
    if Form.Components[x] is  TSaveDialog then begin
       xSaveDLG :=  Form.Components[x] as  TSaveDialog;
       mPathFile := xSaveDLG.FileName;

       xFileName := ExtractFileName( mPathFile );
       xPath:= ExtractFileDir( mPathFile );

       if xFileName = '' then
          xFileName := cFile;

       if xPath = '' then
          xPath := cPath;

       mPathFile := Format('%s\%s',[xPath, xFileName]);

       if  not mFileSaved then begin
           mPathFile := Format('%s\%s',[cPath, cFile]);

           if (MessageDLG( 'Diagnostic Report is not saved. ' + #10#13 +
                           'Would you save it now?' + #10#13 + #10#13 + mPathFile,
                            mtConfirmation, [mbYes, mbNo], 0) = mrYes) then begin

               xPath:= ExtractFileDir(  mPathFile );

               ForceDirectories(xPath);
               try
                 reReport.Lines.SaveToFile(mPathFile);
                 mFileSaved := TRUE;
               except
               end;
           end;
       end;

       break;
    end;
end;

procedure TDiagnosticForm.reReportInitDialog(Form: TForm);
var
  x: Integer;
  xSaveDLG: TSaveDialog;
  xFileName, xPath: string;
begin

  xFileName := ExtractFileName( mPathFile );
  xPath:= ExtractFileDir( mPathFile );
  if xFileName = '' then
     xFileName := cFile;

  if xPath = '' then
     xPath := cPath;

  Application.ProcessMessages;

  for x:= 0 to Form.ComponentCount-1 do
     if Form.Components[x] is  TSaveDialog then begin
        xSaveDLG :=  Form.Components[x] as  TSaveDialog;
        xSaveDLG.FileName   := xFileName;
        xSaveDLG.InitialDir := xPath;
        xSaveDLG.Title := 'Save diagnostic report';
        break;
     end;
   mFileSaved := FALSE;
end;

procedure TDiagnosticForm.reReportMenuSaveAndExitClick(Form: TForm; RichEdit:
        TwwCustomRichEdit; var DoDefault: Boolean);
var
  xPath: string;
begin
  xPath:= ExtractFileDir(  mPathFile );

  ForceDirectories(xPath);
  try
   RichEdit.Lines.SaveToFile(mPathFile);
   mFileSaved := TRUE;
  except
  end;
end;

procedure TDiagnosticForm.reReportMenuSaveAsClick(Form: TForm; RichEdit:
        TwwCustomRichEdit; var DoDefault: Boolean);
begin
  mFileSaved := TRUE;
end;

procedure TDiagnosticForm.rgDiagnosetypeClick(Sender: TObject);
begin
  if rgDiagnosetype.ItemIndex = 0 then
     gDiagnosticHost := TRUE
  else
     gDiagnosticHost := FALSE;
end;

procedure TDiagnosticForm.SetProcessStatus(aValue:Integer);
begin
  mProcessStatus := mProcessStatus  + aValue;
  mmProgressBar1.Position := mProcessStatus;
  Application.ProcessMessages;
end;

procedure TDiagnosticForm.StartMMClient;
var
  x: Integer;
  xRet: Boolean;
begin
  x:= 0;

  if mMMClientPath = '' then exit;
  try
    xRet:= IsProcessRunning(mMMClientPath);
    if not xRet then
        if mMMClientRunning then
           repeat
             CallApp(mMMClientPath, '', FALSE, FALSE);
             xRet:= IsProcessRunning(mMMClientPath);
             inc(x);
           until xRet or (x > 50);
   except
   end;
end;

procedure TDiagnosticForm.StopMMClient;
var
  xMMClientRunning: Boolean;
  x: Integer;
  xHWND: HWND;
begin

  if mMMClientPath = '' then exit;

  //MMClient schliessen  -> TrayIcon verschwindet
  xHWND :=  FindWindow(NIL,'MMClient');
  SendMessage(xHWND, WM_CLOSE, 0, 0);

  try
     xMMClientRunning := IsProcessRunning(mMMClientPath);
     x:= 0;
     //MMClient beenden
     if xMMClientRunning then
        Repeat
         xMMClientRunning:= ProcessTerminate(mMMClientPath);
         inc(x);
        until (xMMClientRunning) or (x > 50);

     Application.ProcessMessages;
     Sleep(500);
     Application.ProcessMessages;
  except
  end;
end;

procedure TDiagnosticForm.WndProc(var Message: TMessage);
begin

  if Message.Msg = gMessageApplID then
     case (Message.WParam) of
       WM_MMDiagnisticStatus : begin
                                 SetProcessStatus( Message.LParam );

                                 if mmProgressBar1.Position > 100 then
                                    lbStatusTxt.Caption := cReportprepare;

                                 if mmProgressBar1.Position = 0 then
                                    lbStatusTxt.Caption := '';
                               end;
      WM_AnalyzeWaitStatusOn  : begin
                                  //mmTimer1.Enabled := True;
                                  //mmTimer1.Restart;
                                  //ShowMessage('go');
                                  lbStatusTxt.Caption := Format('%s        %s',[cAnalyzeMsg, cNeedTimeMsg]);
                                end;
      WM_AnalyzeWaitStatusOFF : begin
                                  //mmTimer1.Enabled := FALSE;
                                  lbStatusTxt.Caption := cAnalyzeMsg;
                                end;
     end;


  inherited WndProc(Message);
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

initialization
  gApplicationName := 'MMDiagnostic';
  CodeSite.Enabled := CodeSiteEnabled;
end.











