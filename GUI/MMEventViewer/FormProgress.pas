unit FormProgress;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, mmProgressBar;

type
  TfrmProgress = class(TForm)
    pbProgress: TmmProgressBar;
  private
    function GetValue: Integer;
    procedure SetValue(const Value: Integer);
  public
    procedure Init(aHighValue: Integer);
    property Value: Integer read GetValue write SetValue;
  end;

var
  frmProgress: TfrmProgress;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, RzCSIntf;

{$R *.DFM}

//------------------------------------------------------------------------------
// TfrmProgress
//------------------------------------------------------------------------------
function TfrmProgress.GetValue: Integer;
begin
  pbProgress.Position;
end;
//------------------------------------------------------------------------------
procedure TfrmProgress.Init(aHighValue: Integer);
begin
  //CodeSite.SendDateTime('TfrmProgress.Init( ' + IntToStr(aHighValue) +  ' )', NOW);
  pbProgress.Max := aHighValue;
  try
    Show;
  except
     on e: Exception do begin
           //CodeSite.SendError('TfrmProgress.Init(). Error msg: ' + e.Message);
     end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmProgress.SetValue(const Value: Integer);
begin
  pbProgress.Position := Value;
  BringToFront;
end;
//------------------------------------------------------------------------------
end.
