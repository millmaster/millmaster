object frmProgress: TfrmProgress
  Left = 429
  Top = 109
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Please wait...'
  ClientHeight = 21
  ClientWidth = 198
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pbProgress: TmmProgressBar
    Left = 0
    Top = 0
    Width = 198
    Height = 21
    Align = alClient
    Min = 1
    Max = 100
    Position = 1
    TabOrder = 0
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
end
