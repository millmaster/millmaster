(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: EventInfo.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Detailanzeige von einem einzelnen Event mit Hexdarstellung
|                 des Datenbereich
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 01.09.1998  0.00  Sdo | Projekt begonnen
| 25.09.1998  1.00  Wss | Ueberarbeitet
|=============================================================================*)
unit EventDetailForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids, ComCtrls, ExtCtrls;

type
  TfrmEventDetail = class(TForm)
    meDescription: TMemo;
    GroupBox1: TGroupBox;
    laDatum: TLabel;
    laZeit: TLabel;
    laCategory: TLabel;
    laSource: TLabel;
    laComputername: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    laStatus: TLabel;
    Bevel1: TBevel;
    meData: TMemo;
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
  public
    procedure InfoShowModal(aDatum, aZeit, aSource, aKategorie, aComputername,
                            aMessageTxt, aStatus: String; aData: PByte; aDataCount: Integer);
  end;

var
  frmEventDetail: TfrmEventDetail;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}
//------------------------------------------------------------------------------
procedure TfrmEventDetail.InfoShowModal(aDatum, aZeit, aSource, aKategorie,
                                     aComputername, aMessageTxt, aStatus: String;
                                     aData: PByte; aDataCount: Integer);
var
  i: Integer;
  xDataText: String;
  xDataByte: String;
  xChar: PChar;
  xStr: String;
begin
  laDatum.Caption        := aDatum;
  laZeit.Caption         := aZeit;
  laCategory.Caption     := aKategorie;
  laSource.Caption       := aSource;
  laComputername.Caption := aComputername;
  laStatus.Caption       := aStatus;

  meDescription.Text     := aMessageTxt;

  xChar := PChar(aData);

  meData.Clear;
  with meData do begin
    xDataByte := '';
    xDataText := '';
    i := 0;
    while i < aDataCount  do begin
      inc(i);
      xDataByte := Format('%s %.02x', [xDataByte, Byte(xChar^)]);
      if (ord(xChar^) >= 32) and (ord(xChar^) <= 255) then
         xDataText := xDataText + xChar^
      else
         xDataText := xDataText + '.';

      if ((i mod 8) = 0) or (i = aDataCount) then begin
        xStr := Format('%-28s %-8s', [xDataByte, xDataText]);
        Lines.Add(xStr);

        xDataByte := '';
        xDataText := '';
      end;
      inc(xChar);
    end;

{
    for i:=1 to length(aData) do begin
      xDataByte := xDataByte + ' ' + IntToHex(ord(aData[i]), 2);
      if (ord(aData[i]) > 32) and (ord(aData[i]) < 255) then
         xDataText := xDataText + aData[i]
      else
         xDataText := xDataText + '.';

      // Neue Zeile
      if ((i MOD 8) = 0) or (i = length(aData)) then begin
        // Auffuellen mit Leerzeichen
        while length(xDataByte) < 26 do
          xDataByte := xDataByte + ' ';
        // zusammensetzen und dazufuegen
        xDataByte := xDataByte + xDataText;
        Lines.Add(xDataByte);
        xDataByte := '';
        xDataText := '';
      end;
    end;
{}
  end;

  ShowModal;
end;
//------------------------------------------------------------------------------
procedure TfrmEventDetail.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  // Info-Fenster zeigen
  if (Key = VK_RETURN) or (Key = VK_ESCAPE)  then
    Close;
end;
//------------------------------------------------------------------------------
end.

