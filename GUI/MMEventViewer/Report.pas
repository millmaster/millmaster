(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: Report.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Implementierung fuer den Ausdruck der Eventtabelle
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 01.09.1998  0.00  Sdo | Projekt begonnen
| 25.09.1998  1.00  Wss | Ueberarbeitet
| 20.02.2001  1.00  Wss | EventTyp war immer Successful
|=============================================================================*)
unit Report;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QuickRpt, Qrctrls, ExtCtrls, Grids, StdCtrls, mmQRLabel, mmQRShape, EventLogProc;

type
  TReportForm = class(TForm)
    QuickRep1: TQuickRep;
    ColumnHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    TitleBand1: TQRBand;
    QRLabel2: TQRLabel;
    laSourceType: TQRLabel;
    QRLabel3: TQRLabel;
    laLogType: TQRLabel;
    QRLa_Date: TQRLabel;
    QRLa_Time: TQRLabel;
    QRLa_Source: TQRLabel;
    QRLa_Category: TQRLabel;
    QRLa_Computername: TQRLabel;
    QRLa_Description: TQRLabel;
    QRLa_Data: TQRLabel;
    laDate: TQRLabel;
    laTime: TQRLabel;
    laSource: TQRLabel;
    laCategory: TQRLabel;
    laComputer: TQRLabel;
    PageFooterBand1: TQRBand;
    QRMemo1: TQRMemo;
    Memo1: TMemo;
    QRMemo2: TQRMemo;
    laPrintDate: TQRLabel;
    QRSysData2: TQRSysData;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRLa_Page: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel1: TQRLabel;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    mmQRShape1: TmmQRShape;
    QRShape15: TmmQRShape;
    QRLa_Typ: TmmQRLabel;
    laTyp: TmmQRLabel;
    procedure QuickRep1NeedData(Sender: TObject; var MoreData: Boolean);
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QuickRep1AfterPrint(Sender: TObject);
  private
    { Private declarations }
    mHandle: THandle;
    mLineCount: Integer;
    mGrid: TStringGrid;
    mEventList: TEventList;
  public
    constructor Create(aOwner: TComponent; aHandle: THandle; aGrid: TStringGrid; aEventList: TEventList); reintroduce;
    procedure PreviewReport(aSource, aLog: String);
    procedure PrintReport(aSource, aLog: String);
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  Printers, mmEventLog;
  
{$R *.DFM}
//-----------------------------------------------------------------------------
constructor TReportForm.Create(aOwner: TComponent; aHandle: THandle; aGrid: TStringGrid; aEventList: TEventList);
begin
  inherited Create(aOwner);

  mEventList := aEventList;
  mGrid   := aGrid;
  mHandle := aHandle;
end;
//-----------------------------------------------------------------------------
procedure TReportForm.PreviewReport(aSource, aLog: String);
begin
  laSourceType.Caption := aSource;
  laLogType.Caption    := aLog;

  QuickRep1.Preview;
  SendMessage(mHandle, WM_USER + 2, 0, 0);
end;
//-----------------------------------------------------------------------------
procedure TReportForm.PrintReport(aSource, aLog: String);
begin
  laSourceType.Caption := aSource;
  laLogType.Caption    := aLog;

//  QuickRep1.PrinterSetup;
  QuickRep1.ReportTitle := 'MillMaster Eventviewer';
  QuickRep1.Print;
//  QuickRep1.PrintBackground;
end;
//-----------------------------------------------------------------------------
procedure TReportForm.QuickRep1AfterPrint(Sender: TObject);
begin
  SendMessage(mHandle, WM_USER + 2, 0, 0);
end;
//-----------------------------------------------------------------------------
procedure TReportForm.QuickRep1BeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  mLineCount := mGrid.FixedRows;
  PrintReport := ((mGrid.RowCount - mGrid.FixedRows) > 0);
  QuickRep1.PrinterSettings.PrinterIndex := Printer.PrinterIndex;
end;
//-----------------------------------------------------------------------------
procedure TReportForm.QuickRep1NeedData(Sender: TObject; var MoreData: Boolean);
var
  xHeight, xMemoHeight: Integer;
  xLines: Integer;
  xString: String;
  xText: String;
  i: Integer;
  xColor: TColor;
  xItem: PEventItemRec;
  xChar: PChar;
  xEventType: TEventType;
begin
  MoreData := (mLineCount < mGrid.RowCount);
  if MoreData then begin
    xEventType  := TEventType(StrToIntDef(mGrid.Cells[0, mLineCount], -1));
    case xEventType of
      etSuccess:     laTyp.Caption := 'S';
      etInformation: laTyp.Caption := 'I';
      etWarning:     laTyp.Caption := 'W';
      etError:       laTyp.Caption := 'E';
    else
      xText := '';
    end;
{
   try
      xColor := TColor(StrToInt(mGrid.Cells[0, mLineCount]));
    except
      xColor := clGreen;
    end;

    case xColor of
      clRed:    laTyp.Caption := 'E';
      clYellow: laTyp.Caption := 'W';
      clNavy:   laTyp.Caption := 'I';
    else
      laTyp.Caption := 'S';
    end;
{}

    laDate.Caption     := mGrid.Cells[1, mLineCount];
    laTime.Caption     := mGrid.Cells[2, mLineCount];
    laSource.Caption   := mGrid.Cells[3, mLineCount];
    laCategory.Caption := mGrid.Cells[4, mLineCount];
    laComputer.Caption := mGrid.Cells[5, mLineCount];

    // Description
    with Memo1 do begin
      Width := QRMemo1.Width;
      Lines.Text := mGrid.Cells[6, mLineCount];
      QRMemo1.Lines.Assign(Lines);
    end;

    // Data
    QRMemo2.Lines.Clear;
    i := StrToInt(mGrid.Cells[7, mLineCount]);
    xItem := mEventList.Event[i];
    if xItem^.DataCount > 0 then begin
      xChar := PChar(xItem^.Data);
      xString := '';
      for i:=1 to xItem^.DataCount do begin
        xString := Format('%s %.02x', [xString, Byte(xChar^)]);
        if ((i mod 8) = 0) or (i = xItem^.DataCount) then begin
          QRMemo2.Lines.Add(xString);
          xString := '';
        end;
        inc(xChar);
      end;
    end;

    // Welches Memo hat mehr Zeilen ?
    if QRMemo1.Lines.Count > QRMemo2.Lines.Count then begin
      xLines  :=  QRMemo1.Lines.Count;
      xMemoHeight := -QRMemo1.Font.Height;
    end else begin
      xLines  :=  QRMemo2.Lines.Count;
      xMemoHeight := -QRMemo2.Font.Height;
    end;

    // Anhand der Zeilen die Hoehe des Memo berechnen
    if xLines > 1 then
      xMemoHeight := (xLines * xMemoHeight) + 14
    else
      xMemoHeight := 17;

    // Bandhoehe ist etwas mehr
    xHeight := xMemoHeight + 3;

    DetailBand1.Height := xHeight;

    QRShape7.Height  := xHeight;
    QRShape8.Height  := xHeight;
    QRShape9.Height  := xHeight;
    QRShape10.Height := xHeight;
    QRShape11.Height := xHeight;
    QRShape12.Height := xHeight;
    QRShape15.Height := xHeight;

    QRMemo1.Height := xMemoHeight;
    QRMemo2.Height := xMemoHeight;

    inc(mLineCount);
  end;
end;

end.

