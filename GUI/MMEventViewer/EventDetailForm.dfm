object frmEventDetail: TfrmEventDetail
  Left = 195
  Top = 188
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'EventInfo'
  ClientHeight = 416
  ClientWidth = 290
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyUp = FormKeyUp
  PixelsPerInch = 96
  TextHeight = 13
  object Label6: TLabel
    Left = 5
    Top = 131
    Width = 53
    Height = 13
    Caption = 'Description'
  end
  object Label7: TLabel
    Left = 5
    Top = 283
    Width = 23
    Height = 13
    Caption = 'Data'
  end
  object meDescription: TMemo
    Left = 5
    Top = 146
    Width = 280
    Height = 133
    Color = clBtnFace
    Lines.Strings = (
      'Me_Beschreibung')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object GroupBox1: TGroupBox
    Left = 5
    Top = 10
    Width = 280
    Height = 111
    Caption = 'Event'
    TabOrder = 1
    object laDatum: TLabel
      Left = 46
      Top = 15
      Width = 39
      Height = 13
      Caption = 'laDatum'
    end
    object laZeit: TLabel
      Left = 194
      Top = 15
      Width = 26
      Height = 13
      Caption = 'laZeit'
    end
    object laCategory: TLabel
      Left = 94
      Top = 90
      Width = 50
      Height = 13
      Caption = 'laCategory'
    end
    object laSource: TLabel
      Left = 94
      Top = 74
      Width = 42
      Height = 13
      Caption = 'laSource'
    end
    object laComputername: TLabel
      Left = 94
      Top = 58
      Width = 79
      Height = 13
      Caption = 'laComputername'
    end
    object Label1: TLabel
      Left = 10
      Top = 15
      Width = 26
      Height = 13
      Caption = 'Date:'
    end
    object Label2: TLabel
      Left = 160
      Top = 15
      Width = 26
      Height = 13
      Caption = 'Time:'
    end
    object Label3: TLabel
      Left = 10
      Top = 58
      Width = 74
      Height = 13
      Caption = 'Computername:'
    end
    object Label4: TLabel
      Left = 10
      Top = 90
      Width = 45
      Height = 13
      Caption = 'Category:'
    end
    object Label5: TLabel
      Left = 10
      Top = 74
      Width = 37
      Height = 13
      Caption = 'Source:'
    end
    object Label8: TLabel
      Left = 10
      Top = 42
      Width = 33
      Height = 13
      Caption = 'Status:'
    end
    object laStatus: TLabel
      Left = 94
      Top = 42
      Width = 38
      Height = 13
      Caption = 'laStatus'
    end
    object Bevel1: TBevel
      Left = 0
      Top = 32
      Width = 280
      Height = 2
    end
  end
  object meData: TMemo
    Left = 5
    Top = 298
    Width = 280
    Height = 111
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      'meData')
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
  end
end
