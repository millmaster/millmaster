program MMEventViewer;
uses
{$IFDEF MemCheck}
  MemCheck,
{$ENDIF}
  mmMBCS,
  Forms,
  Windows,
  sysutils,
  LoepfeGlobal,
  BaseGlobal,
  EventLog,
  mmCS,
  EventMain in 'EventMain.pas' {frmMain},
  EventLogProc in 'EventLogProc.pas',
  EventDetailForm in 'EventDetailForm.pas' {frmEventDetail},
  ReadThread in 'ReadThread.pas',
  Report in 'Report.pas' {ReportForm},
  FormProgress in 'FormProgress.pas' {frmProgress},
  AdjustDateForm in 'AdjustDateForm.pas' {frmAdjustDateTime},
  BASEDialog in '..\..\..\LoepfeShares\Templates\BASEDIALOG.pas' {mmDialog},
  BASEDialogBottom in '..\..\..\LoepfeShares\Templates\BASEDIALOGBOTTOM.pas' {DialogBottom},
  BASEAPPLMAIN in '..\..\..\LoepfeShares\Templates\BASEAPPLMAIN.pas' {BaseApplMainForm};

{$R *.RES}
{$R 'Version.res'}

begin
  // Check if running platform is Windows NT
  if (GetSystemPlatform <> VER_PLATFORM_WIN32_NT) then begin
    MessageBeep($FFFFFFFF);
    Application.MessageBox('MillMaster EventViewer runs only with Windows NT.',
                           'Application Error', MB_OK);
    Exit;
  end;


//  if not ApplicationExist(Application) then
  gApplicationName := 'MMEventViewer';
{$IFDEF MemCheck}
  MemChk(gApplicationName);
{$ENDIF}
  try
    CodeSite.Enabled := GetRegBoolean(cRegLM, cRegMMDebug, gApplicationName, False);
    Application.Initialize;
    Application.CreateForm(TfrmMain, frmMain);
    Application.Run;
  except
  end;
end.
