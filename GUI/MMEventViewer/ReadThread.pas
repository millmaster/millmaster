(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: ReadEventLog.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Thread fuer das Auslesen des Eventlogs von Windows NT
| Info..........:       GetNumberOfEventLogRecords	advapi32.dll
                        GetOldestEventLogRecord		advapi32.dll
                        OpenEventLog			advapi32.dll
                        CloseEventLog			advapi32.dll
                        LoadLibrary			Kernel32.dll
                        FreeLibrary			Kernel32.dll
                        GetTimeZoneInformation		Kernel32.dll
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 01.09.1998  0.00  Sdo | Projekt begonnen
| 25.09.1998  1.00  Wss | Ueberarbeitet
| 15.10.1998  1.00  Sdo | Ueberarbeitet siehe Text Unit EventLogProc
| 29.07.2002  1.01  Sdo | TfrmProgress wird nicht mehr angezeigt ( in GetAllEvents() )
|                       | -> Error : Canvas does not allow drawing
| 01.09.2004  1.00  Wss | CriticalSection Class von Delphi verwenden
| 16.02.2005  1.02  Sdo | Neue Proc. ReadAllEvents() -> Autosave auf Kommandozeile
|                       | Neue Proc. SortEventsDateTime(); sortiert nach Datum Zeit
|=============================================================================*)
unit ReadThread;

interface

uses
  Windows, Classes, Controls, Messages, Grids, EventLogProc, SysUtils, Graphics,
  SyncObjs, Report, Forms, mmStringGrid, mmThread, Dialogs, IPCClass,
  FormProgress, mmList;

const
  cEventViewerEvent = '';
  cTimeIntervall   = 1000;   // wait period for readout event

  WM_ReadEvent = WM_User + 1;
  // Parameter fuer Message
  cGridInit   = 0;   // 1. mal Grid anzeigen
  cNewEvents  = 1;   // neue Events
  cSameEvents = 2;   // keine Aenderung der Events
  cDefaultSourceSection = 'Application';
type
  //............................................
  TReadThread = class(TmmThread)
  private
    FEventCount         : integer;
    mCriticalSection    : TCriticalSection;
    mEventLogHnd        : THandle;
    mLastTime           : DWord;
    mHostName           : String;
    mHandle             : THandle;
    mLastEventCount     : DWord;
    mLastOldestEvent    : DWord;
    mSourceSection      : String;
    mEventList: TEventList;

    mGetAllEventsOnce : Boolean;
    procedure ClearEventTextList;
  protected
    procedure Execute; override;
    function GetAllEvents(var aEventCount: DWord): Boolean;
  public
    constructor Create(aHandle : THandle; aList: TEventList; aCritSection: TCriticalSection); virtual;
    destructor Destroy; override;
    procedure Init(aHostName, aSourceSection: String);
    procedure  ReadAllEvents;
    property EventCount : integer read FEventCount;
  end;


  function SortEventsDateTime(aItem1, aItem2: Pointer): Integer;


implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmCS, NTApis,
  LoepfeGlobal, BaseGlobal, MMEventLog;

//------------------------------------------------------------------------------
function SortEventsDateTime(aItem1, aItem2: Pointer): Integer;
begin

  if TEventItemRec(aItem1^).DateTime = TEventItemRec(aItem2^).DateTime  then begin
     result := 0;
  end else begin
        if TEventItemRec(aItem1^).DateTime < TEventItemRec(aItem2^).DateTime then
           result := -1;

        if TEventItemRec(aItem1^).DateTime > TEventItemRec(aItem2^).DateTime then
           result := 1;
  end;
end;
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
// TReadThread
//------------------------------------------------------------------------------
constructor TReadThread.Create(aHandle : THandle; aList: TEventList; aCritSection: TCriticalSection);
begin
  inherited Create(True);

  FreeOnTerminate := True;
  FEventCount:=-1;
  mEventLogHnd := 0;
  mHandle    := aHandle;
  mCriticalSection := aCritSection;
  mEventList := aList;

  mHostName := '.';
  mSourceSection := cDefaultSourceSection;

  mGetAllEventsOnce := FALSE;
end;
//------------------------------------------------------------------------------
destructor TReadThread.Destroy;
begin
  // Record loeschen
  CloseEventLog(mEventLogHnd);

  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TReadThread.ClearEventTextList;
begin
  mEventList.Clear;
end;
//------------------------------------------------------------------------------
procedure TReadThread.Execute;
var
  xCount: DWord;
begin
  xCount:=0;
  if not SetKernelObjectSecurity(Handle, 'LOEPFE\MMSupp', True) then
    CodeSite.SendMsg('Security set failed');

  while not Terminated do begin
    if (GetTickCount - mLastTime) > cTimeIntervall then begin
      Application.ProcessMessages;
      if not mGetAllEventsOnce then // Events nur einmal lesen -> speichern mittels Kommandozeile
        // Daten aus Logfile, DLL's lesen und ins Hidden-Grid einsetzen
        if GetAllEvents(xCount) then begin
          if (xCount = 0) then
            // Keine Events, Anzeige-Grid leeren
            SendMessage(mHandle, WM_ReadEvent, cGridInit, 0)
          else
            // Neue Events anzeigen
            SendMessage(mHandle, WM_ReadEvent, cNewEvents, 0);
        end;
      mLastTime := GetTickCount;
      Application.ProcessMessages;
    end else  begin // end if
      //CodeSite.SendDateTime('Execute Sleep', NOW);
      Application.ProcessMessages;
      Sleep(1000);
    end;
  end; // End while
end;
//------------------------------------------------------------------------------
function TReadThread.GetAllEvents(var aEventCount: DWord): Boolean;
var
  xIndex : integer;
  xRec : DWord;
  xEventRecord       : PEventRecord;
  xEventRecordSize   : DWord;
  xOldestEvent       : DWord;
  xCount             : DWord;
  xHighNr, xLowNr    : DWord;
  xEventItem: PEventItemRec;
  xStr: String;
  xTime0  : TTime;
begin
  Result := False;

  try
  //  CodeSite.SendDateTime('Begin GetAllEvents', NOW);
    Application.ProcessMessages;
    GetNumberOfEventLogRecords(mEventLogHnd, aEventCount);

    Application.ProcessMessages;
    GetOldestEventLogRecord(mEventLogHnd, xOldestEvent);

    { get number of events: N = new, O = old, L = lost events because event log was full
      Events if event log were empty:
        NNNN:      xOldestEvent = 1, aEventCount = 4, xNew = 4, xCount = 4
      Events if they are appended:
        NNOOOO:    xOldestEvent = 1, aEventCount = 6, xNew = 6, xCount = 2
      Events if event log is full:
        NNNOOOOLL: xOldestEvent = 3, aEventCount = 7, xNew = 9, xCount = 3

      But to get only new events in the last example: xHighNr = 9, xLowNr = 7
    }
    FEventCount := aEventCount;

    // if count and oldest event are still the same no new events appended
    // should be only in mTimerMode = True for remote access
    Result := (aEventCount <> mLastEventCount) or (xOldestEvent <> mLastOldestEvent);
    if Result then begin
      xTime0 := NOW;
      CodeSite.SendInteger('GetNumberOfEventLogRecords', aEventCount);
      CodeSite.SendInteger('GetOldestEventLogRecord', xOldestEvent);
      if aEventCount = 0 then begin
        // After clearing EventLog a new handle has to be created
        if mEventLogHnd <> 0 then
          CloseEventLog(mEventLogHnd);

        mEventLogHnd := OpenEventLog(PChar(mHostName), PChar(mSourceSection));

        ClearEventTextList;  // Stringlist loeschen

        mLastEventCount  := 0;
        mLastOldestEvent := 0;
      end else begin
        Screen.Cursor := crHourGlass;

        // get count of new entries !!!!!
        CodeSite.SendFmtMsg('mLastEventCount: %d, mLastOldestEvent: %d', [mLastEventCount, mLastOldestEvent]);

        if (xOldestEvent <> mLastOldestEvent) and (xOldestEvent > 1) then
          // if eventlog is top of size check for oldest event number
          if mLastOldestEvent = 0 then
            xCount := aEventCount
          else
            xCount := xOldestEvent - mLastOldestEvent
        else
          // if event log incrased then check for event count
          xCount := aEventCount - mLastEventCount;
        xHighNr := xOldestEvent + aEventCount - 1;
        xLowNr := xHighNr - xCount + 1;

        CodeSite.SendFmtMsg('Read event number from %d to %d', [xHighNr, xLowNr]);

        CodeSite.SendDateTime('Read event number from', NOW);

        // Speicher fuer EventRecord allozieren (wird in ReadOneEventRec bei Bedarf vergroessert)
        xEventRecordSize := SizeOf(TEventRecord);
        xEventRecord     := AllocMem(xEventRecordSize);
        {
        if xCount > 10 then begin
          CodeSite.SendInteger('Create progress form with xCount = ', xCount);
          CodeSite.SendDateTime('Create progress form with xCount', NOW);

          try
            Application.ProcessMessages;
            xProgressForm := Nil;
            Sleep(1000);
            Application.ProcessMessages;
            xProgressForm := TfrmProgress.Create(NIL);
            Application.ProcessMessages;
            CodeSite.SendDateTime('before xProgressForm.Init(xCount)', NOW);

            xProgressForm.Init(xCount);
          except
             on e: Exception do CodeSite.SendError('xProgressForm := TfrmProgress.Create(NIL) : Error msg : ' +  e.Message);
          end;
          CodeSite.SendDateTime('xProgressForm.Init(xCount)', NOW);
        end else
          xProgressForm := Nil;
         }

        mCriticalSection.Enter;
        try
          xIndex := 0;
          // oldest event is the first entry in list, newest the last one
          for xRec:=xLowNr to xHighNr do begin

            Application.ProcessMessages;

            inc(xIndex);
           {
            if Assigned(xProgressForm) then
            try
//              CodeSite.SendInteger('Set progress form value', xIndex);
              xProgressForm.Value := xIndex;
            except
              on e:Exception do begin
                CodeSite.SendFmtMsg('Set progress form value failed: %s', [e.Message]);
                raise;
              end;
            end;

            }

            // Event-Struktur lesen
            if ReadOneEventRec(mEventLogHnd, xRec, xEventRecord, xEventRecordSize) then begin
              new(xEventItem);
              FillChar(xEventItem^, Sizeof(TEventItemRec), 0);

              xEventItem^.Data := NIL;

              xEventItem^.DateTime   := EncodeEventDateTime(xEventRecord^.TimeWritten);
              xEventItem^.EventID    := xEventRecord^.EventID;
              xEventItem^.CategoryNr := xEventRecord^.EventCategory;
              case xEventRecord^.EventType of
                EVENTLOG_INFORMATION_TYPE: xEventItem^.EventType  := etInformation;
                EVENTLOG_WARNING_TYPE:     xEventItem^.EventType  := etWarning;
                EVENTLOG_ERROR_TYPE:       xEventItem^.EventType  := etError;
              else  // success
                xEventItem^.EventType  := etSuccess;
              end;

              Application.ProcessMessages;
              try                         
                xStr := 'GetSourceAndPCName';
                GetSourceAndPCName(xEventRecord, xEventItem);

                xStr := 'DecodeEventlogRecord';
                DecodeEventlogRecord(xEventRecord, xEventItem, mSourceSection);   //need a lot of time (4 times more than without DecodeEventlogRecord()

                xStr := 'GetBinaryData';
                GetBinaryData(xEventRecord, xEventItem);
              except
                on e:Exception do
                  CodeSite.SendError(Format('GetAllEvents: %s = %s', [xStr, e.Message]));
              end;

              Application.ProcessMessages;

              case xEventRecord.EventType of
                EVENTLOG_SUCCESS,
                EVENTLOG_AUDIT_SUCCESS: begin
                    xEventItem^.EventType := etSuccess;
                  end;
                EVENTLOG_AUDIT_FAILURE,
                EVENTLOG_ERROR_TYPE: begin
                    xEventItem^.EventType := etError;
                  end;
                EVENTLOG_WARNING_TYPE: begin
                    xEventItem^.EventType := etWarning;
                  end;
                EVENTLOG_INFORMATION_TYPE: begin
                    xEventItem^.EventType := etInformation;
                  end;
              end;

              mEventList.Add(xEventItem);
              
            end;  // if
            Application.ProcessMessages;
          end; // end for


        finally
        {
          if Assigned(xProgressForm) then
             FreeAndNil(xProgressForm);
         }
          // Speicher wieder freigeben
          ReallocMem(xEventRecord, 0);
          mCriticalSection.Leave;

          mLastOldestEvent := xOldestEvent;
          mLastEventCount  := aEventCount;
        end;

      end; // end if

      CodeSite.SendMsg( 'EV fillup time : ' + TimeToStr(NOW - xTime0) );

      CodeSite.SendInteger('mEventList.Count', mEventList.Count);
      CodeSite.AddSeparator;
    end;  // if new events
  except
    on e: exception do
      CodeSite.SendError(Format('Error in TReadThread.GetAllEvents: %s', [e.Message]));
  end;
  Screen.Cursor := crDefault;

//  CodeSite.SendDateTime('END GetAllEvents', NOW);
end; // END GetAllEvents
//------------------------------------------------------------------------------
procedure TReadThread.Init(aHostName, aSourceSection: String);
var
  i: Integer;
begin

  // protect because fast F5 brings an error of Index out of bound if thread
  // is still filling in the grid and list
  mCriticalSection.Enter;
  try
    // stop thread working
    Suspend;

    for i:=0 to gDllHandleList.Count-1 do
      FreeLibrary(THandle(gDllHandleList.Objects[i]));
    gDllHandleList.Clear;

  { will be cleared from caller
    if (aHostName <> mHostName) or (aSourceSection <> mSourceSection) then
      ClearEventTextList;
  {}

    // initialize locale variables
    mHostName      := aHostName;
    mSourceSection := aSourceSection;
    mLastTime      := 0;
    mLastEventCount  := 0;
    mLastOldestEvent := 0;

    if mEventLogHnd <> 0 then CloseEventLog(mEventLogHnd);

    mEventLogHnd := OpenEventLog(PChar(mHostName), PChar(mSourceSection));
    while Suspended do
      Resume;
  finally
    mCriticalSection.Leave;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Liest alle Events nur einmal; Der Thread muss laufen.
//Damit die Daten nur einmal gelesen werden, wird die Var. auf
//mGetAllEventsOnce = TRUE gesetzt (ansonsten mehrfaches Einlesen und Error beim
//Beenden).
//Diese Proc. wird fuer den Autosave auf der Kommandozeile gebraucht
//******************************************************************************
procedure TReadThread.ReadAllEvents;
var xEventCount : DWord;
begin
  xEventCount:= FEventCount;
  xEventCount:= 0;
  Init('.', cDefaultSourceSection);
  mCriticalSection.Enter;

  while Suspended do
    Resume;

  mGetAllEventsOnce := TRUE;
  GetAllEvents(xEventCount);
  mEventList.Sort( SortEventsDateTime );
  FEventCount := mEventList.Count;
  mCriticalSection.Leave;
  mGetAllEventsOnce := FALSE;
end;
//------------------------------------------------------------------------------
end.

