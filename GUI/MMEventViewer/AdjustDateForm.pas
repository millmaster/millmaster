unit AdjustDateForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, StdCtrls, mmLabel, mmEdit, Buttons, mmBitBtn;

type
  TfrmAdjustDateTime = class(TDialogBottom)
    edFromDate: TmmEdit;
    edToDate: TmmEdit;
    laFromDate: TmmLabel;
    laToDate: TmmLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAdjustDateTime: TfrmAdjustDateTime;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

end.
