(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: EventLogProc.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Diverse Prozeduren und Funktionen fuer das Auslesen und
|                 dekodieren des Eventlogs
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 01.09.1998  0.00  Sdo | Projekt begonnen
| 25.09.1998  1.00  Wss | Ueberarbeitet, GetSystemPlatform in GlobalUnit
|                       | verschoben
| 15.10.1998  1.01  Sdo | In Proc. GetMsgFromDll wird zusaetzlich auch der
|                       | LogText verglichen, weil gleiche Categories
|                       | unterschiedliche Texte haben koennen (MillMaster)
|                       | Das Gleiche gilt auch fuer UNIT ReadEventLog in
|                       | Proc. GetAllEvents
| 01.12.1998  1.02  Wss | ZeitDecoder hatte Fehler wenn Std=23 + Offset = 24 !! ungueltig
| 09.02.2000  1.03  SDO | Neue Func. EncodeEventDateTime implementiert, ersetzt
|                       | GetEventTime und GetEventDate
| 09.02.2000  1.04  Wss | MessageFile konnte nicht geoeffnet werden mit OpenKey -> OpenKeyReadOnly
| 28.11.2001  1.05  Wss | MemoryLeak removed in cleanup TEventList
| 28.01.2003  1.06  Wss | GetMsgFromDll: wenn tiefe Benutzerrechte Zugriff auf eine Datei verweigert
                          wird, dann lieferte diese Funktion einen Leerstring zur�ck.
                          -> in Finally wird gepr�ft, ob formatierte Message leer ist. Wenn ja
                             wird der ausgelesene Originaltext �bergeben
| 13.05.2005        Lok | In LoadEventDLL() eine eigene TRegistry Instanz verwenden --> Aufruf GetRegString()
|                       |   Die Instanz wird in initialization erzeugt                            
|=============================================================================*)
unit EventLogProc;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, mmEventLog, mmList;

const
  cEventRecordSize = 56;
  cMaxChar         = 255;
  cCRLF            = #13#10;

type
  PPCharArr = ^TPCharArr;
  TPCharArr = array[0..100] of PChar;

  PEventRecord = ^TEventRecord;
  TEventRecord = record
    Length                : DWORD ;   // Datensatzgroesse
    Reserved              : DWORD ;
    RecordNumber          : DWORD ;
    TimeGenerated         : DWORD ;
    TimeWritten           : DWORD ;
    EventID               : DWORD ;
    EventType             : WORD ;
    NumStrings            : WORD ;
    EventCategory         : WORD ;
    ReservedFlags         : WORD ;
    ClosingRecordNumber   : DWORD  ;
    StringOffset          : DWORD  ;
    UserSidLength         : DWORD  ;
    UserSidOffset         : DWORD  ;
    DataLength            : DWORD;
    DataOffset            : DWORD;
    Data                  : array [0..cMaxChar] of Char;
    //
    // Folgende Werte stehen im Data-Array
    // Diese werden mitels Offset und Length ermittelt
    //
    // TCHAR SourceName[]
    // TCHAR Computername[]
    // SID   UserSid
    // TCHAR Strings[]
    // BYTE  Data[]
    // CHAR  Pad[]
    // DWORD Length;
  end;

  //............................................
  PEventItemRec = ^TEventItemRec;
  TEventItemRec = record
    EventID: DWord;
    EventType: TEventType;
    DateTime: TDateTime;
    SourceHndIndex: Integer;
    CategoryNr: DWord;
    DataCount: DWord;
    SourceName: String[40];
    Computername: String[20];
    Category: String[20];
    Description: String;
    Data: PByte;
  end;
  //............................................
  TEventList = class(TmmList)
  private
    fMaxCount: Integer;
    function GetEventRec(aIndex: Integer): PEventItemRec;
    procedure SetMaxCount(const Value: Integer);
  public
    property Event[aIndex: Integer]: PEventItemRec read GetEventRec;
    property MaxCount: Integer read fMaxCount write SetMaxCount;
    procedure Clear; override;
    procedure Delete(Index: Integer); override;
    function LoadFromFile(aFileName: String): Boolean;
    procedure SaveToFile(aFileName: String);
  end;

const
  cEventFileId: Word = $FF01;

  cEventLogLocation  = 'SYSTEM\CurrentControlSet\Services\EventLog\';
  cSystemRoot        = '%SystemRoot%';
  cReadBufSize       = 16383;
  cDateBase1970      = 25569;  // equal to TDateTime = '1.1.1970 00:00:00'

var
  gDllHandleList: TStringList;
  gEventList : TStringList;

//******************************************************************************
//function DatumDecoder(aTimeStamp: DWORD): String;
function EncodeEventDateTime(aTimeStamp: LongInt): TDateTime;

procedure DecodeEventlogRecord(aEvLogRec: PEventRecord; aEventItem: PEventItemRec; aSourceSection: String);
function GetEventDate(aTimeStamp: DWord): String;
procedure GetBinaryData(aEvLogRec: PEventRecord; aEventItem: PEventItemRec);
procedure GetSourceAndPCName(aEvLogRec: PEventRecord; aEventRec: PEventItemRec);
function ReadOneEventRec(aHandle_EventLog : THandle; aRecNr:Integer; var aEvLogRec: PEventRecord; var aSize: DWord): Boolean;


//******************************************************************************
implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  RzCSIntf,
  LoepfeGlobal, mmRegistry, BaseGlobal;

//------------------------------------------------------------------------------
// TEventList
//------------------------------------------------------------------------------
procedure TEventList.Clear;
begin
//  if Count = 0 then exit;
  while Count > 0 do
     try
       Delete(0);
     finally
     end;
  inherited Clear;
end;
//------------------------------------------------------------------------------
procedure TEventList.Delete(Index: Integer);
var
  xRec: PEventItemRec;
begin
  xRec := Event[Index];

  if Assigned(xRec) then begin
    if Assigned(xRec^.Data) then begin
       FreeMem(xRec^.Data, xRec^.DataCount);
       xRec^.Data := Nil;
    end;
    Dispose(xRec);
  end;
  inherited Delete(Index);
end;
//------------------------------------------------------------------------------
function TEventList.GetEventRec(aIndex: Integer): PEventItemRec;
begin
  Result := PEventItemRec(Items[aIndex]);
end;
//------------------------------------------------------------------------------
function TEventList.LoadFromFile(aFileName: String): Boolean;
var
  i: Integer;
  xStream: TMemoryStream;
  xInt: Integer;
  xCount: Integer;
  xId: Word;
  xItem: PEventItemRec;
  xPChar: PChar;
begin
  Result := True;
  xStream := TMemoryStream.Create;
  xPChar := Nil;
  with xStream do
  try
    LoadFromFile(aFileName);
    // read file id and count of events
    ReadBuffer(xId, sizeof(xId));
    if xId <> cEventFileId then begin
      ShowMessage('This EventFile is created with an previous version.');
      Exit;
    end;
    ReadBuffer(xCount, sizeof(Integer));
    i := 0;
    while (i < xCount) and (Position < Size) do begin
      new(xItem);
      with xItem^ do
      try
        ReadBuffer(EventID, sizeof(DWord));

        ReadBuffer(EventType, sizeof(TEventType));
        ReadBuffer(DateTime, sizeof(TDateTime));
        ReadBuffer(CategoryNr, sizeof(DWord));
        ReadBuffer(DataCount, sizeof(DWord));

        ReadBuffer(SourceName, sizeof(SourceName));
        ReadBuffer(Computername, sizeof(Computername));
        ReadBuffer(Category, sizeof(Category));

        ReadBuffer(xInt, sizeof(xInt));

        ReallocMem(xPChar, xInt);
        ReadBuffer(xPChar^, xInt);
        Description := xPChar;

        if DataCount > 0 then begin
          Data := AllocMem(DataCount);
          ReadBuffer(Data^, DataCount);
        end else
          Data := Nil;
      except
        on e:Exception do begin
          CodeSite.SendError(Format('Stream read error: %s/%s', [e.Message, GetLastErrorText]));
          CodeSite.SendFmtMsg('  xCount = %d, i = %d', [xCount, i]);
          Exit;
        end;
      end;
      Add(xItem);
      inc(i);
    end; // while
{
    for i:=1 to xCount do begin
      new(xItem);
      with xItem^ do
      try
        ReadBuffer(EventID, sizeof(DWord));

        ReadBuffer(EventType, sizeof(TEventType));
        ReadBuffer(DateTime, sizeof(TDateTime));
        ReadBuffer(CategoryNr, sizeof(DWord));
        ReadBuffer(DataCount, sizeof(DWord));

        ReadBuffer(SourceName, sizeof(SourceName));
        ReadBuffer(Computername, sizeof(Computername));
        ReadBuffer(Category, sizeof(Category));

        ReadBuffer(xInt, sizeof(xInt));

        ReallocMem(xPChar, xInt);
        ReadBuffer(xPChar^, xInt);
        Description := xPChar;

        if DataCount > 0 then begin
          Data := AllocMem(DataCount);
          ReadBuffer(Data^, DataCount);
        end else
          Data := Nil;
      except
        on e:Exception do begin
          CodeSite.SendError(Format('Stream read error: %s/%s', [e.Message, GetLastErrorText]));
          CodeSite.SendFmtMsg('  xCount = %d, i = %d', [xCount, i]);
          Exit;
        end;
      end;
      Add(xItem);
    end; // for i
{}
  finally
    FreeAndNil(xStream);
  end;
end;
//------------------------------------------------------------------------------
procedure TEventList.SaveToFile(aFileName: String);
var
  i: Integer;
  xStream: TMemoryStream;
  xInt: Integer;
  xPChar: PChar;
begin
  xPChar := Nil;
  xStream := TMemoryStream.Create;
  with xStream do begin
    // write file id and count of events
    WriteBuffer(cEventFileId, sizeof(cEventFileId));
    xInt := Count;
    WriteBuffer(xInt, sizeof(xInt));

    for i:=0 to Count-1 do begin
      with Event[i]^ do begin
        WriteBuffer(EventID, sizeof(DWord));

        WriteBuffer(EventType, sizeof(TEventType));
        WriteBuffer(DateTime, sizeof(TDateTime));
        WriteBuffer(CategoryNr, sizeof(DWord));
        WriteBuffer(DataCount, sizeof(DWord));

        WriteBuffer(SourceName, sizeof(SourceName));
        WriteBuffer(Computername, sizeof(Computername));
        WriteBuffer(Category, sizeof(Category));

        xInt := Length(Description)+1;
        WriteBuffer(xInt, sizeof(xInt));

        ReallocMem(xPChar, xInt);
        StrPCopy(xPChar , Description);
        WriteBuffer(xPChar^, xInt);

        if DataCount > 0 then
          WriteBuffer(Data^, DataCount);
      end;
    end;
  end;
  xStream.SaveToFile(aFileName);
  FreeAndNil(xStream);
end;
//------------------------------------------------------------------------------
procedure TEventList.SetMaxCount(const Value: Integer);
begin
  fMaxCount := Value;
  if Count > fMaxCount then begin
    while Count > fMaxCount do
      Delete(0);
  end;
end;

var
  gRegInstance: TmmRegistry;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//function LoadEventDLL(aSourceName: string): THandle;
procedure LoadEventDLL(aEventItem: PEventItemRec; aSourceSection: String);
var
  xStr: String;
  xIndex: Integer;
  xHnd: THandle;
  xBuf: Array[0..cReadBufSize] of Char;
begin
  xIndex := -1;
  try
    // read Registry to find aSource of message.
    xStr := IncludeTrailingBackslash(cEventLogLocation + aSourceSection) + aEventItem^.SourceName;
    xStr := GetRegString(cRegLM, xStr, 'EventMessageFile', '', gRegInstance);
    gRegInstance.CloseKey;
    
    if xStr <> '' then begin
      if Pos(cSystemRoot, xStr) > 0 then begin
        GetWindowsDirectory(xBuf, cReadBufSize);
        xStr := StringReplace(xStr, cSystemRoot, StrPas(xBuf), [rfReplaceAll]);
      end;


      xIndex := gDllHandleList.IndexOf(xStr);
      if xIndex = -1 then begin
        // Load DLL
        xHnd := LoadLibrary(PChar(xStr));
        xIndex := gDllHandleList.AddObject(xStr, TObject(xHnd));
      end;
    end;
  except
    CodeSite.SendError('LoadLibrary failed: ' + xStr);
  end;
  aEventItem^.SourceHndIndex := xIndex;
end;
//------------------------------------------------------------------------------


//******************************************************************************
// Decodiert das Datum mit der Basis 1.1.1970,
// welches als Sek. ab Basis vorliegt;
//******************************************************************************
function GetEventDate(aTimeStamp: DWORD): String;
begin
  Result := '0';
  if aTimeStamp > 0 then
    Result := DateToStr(cDateBase1970 + (aTimeStamp div (3600 * 24)));
end; // END GetEventDate
//******************************************************************************
// Alle moeglichen Eventtexte in aEventTextListe lesen
//******************************************************************************
//******************************************************************************
// Messagetext aus der DLL lesen
//******************************************************************************
procedure GetMsgFromDll(aEventItem: PEventItemRec; var aMessageText, aCategory, aSourceSection: String);
var
  i: integer;
  xBuf: array[0..cReadBufSize-1] of Char;
//  xParamList: TStringList;
  xArgumentArr: TPCharArr;
  xDllHnd: THandle;
begin
  aCategory    := 'None';
  aMessageText := '';

  xBuf := '';
  try
    FillChar(xArgumentArr, cReadBufSize, 0);
    // fill structure of parameters;
    with TStringList.Create do
    try
      CommaText := aEventItem^.Description;
      for i:=0 to Count-1 do
        xArgumentArr[i] := PChar(Strings[i]);
    finally
      Free;
    end;

    LoadEventDLL(aEventItem, aSourceSection);

    CodeSite.SendString('aEventItem^.Description', aEventItem^.Description);

    if  aEventItem^.SourceHndIndex >= 0 then begin  // wird mit LoadEventDLL() geladen
      xDllHnd := THandle(gDllHandleList.Objects[aEventItem^.SourceHndIndex]);
      // Text auslesen von EventID
      FormatMessage(FORMAT_MESSAGE_FROM_HMODULE or
                    FORMAT_MESSAGE_ARGUMENT_ARRAY or FORMAT_MESSAGE_FROM_SYSTEM,
                    Pointer(xDllHnd),  aEventItem^.EventID, LANG_NEUTRAL,
                    xBuf, cReadBufSize, @xArgumentArr);
      aMessageText := (xBuf);

      //CodeSite.SendDWord('aEventItem^.EventID', aEventItem^.EventID);

      // Text auslesen von CategoryID
      if aEventItem^.CategoryNr <> 0 then begin
        FormatMessage(FORMAT_MESSAGE_FROM_HMODULE or
                      FORMAT_MESSAGE_ARGUMENT_ARRAY,
                      Pointer(xDllHnd), aEventItem^.CategoryNr, LANG_NEUTRAL,
                      xBuf, cReadBufSize, @xArgumentArr);
        aCategory := xBuf;
      end;
    end else begin
      // DLL nicht gefunden oder konnte nicht geoeffent werden
      aMessageText := Format('The description for Event ID (%d) '+
                             'in Source (%s) could not be found. '+
                             'It contains the following insertion string(s): %s',
                             [(aEventItem^.EventID and $3FFFFFFF), aEventItem^.SourceName, aEventItem^.Description]);

      aCategory := IntToStr(aEventItem^.CategoryNr);
    end;
  finally
    if Trim(aMessageText) = '' then
      aMessageText := aEventItem^.Description;
  end;
end; // GetMsgFromDll
//******************************************************************************
// Liest die Beschreibung aus dem Event-Log-File und schreibt sie
// in eine Stringliste. Der Text aus der Stringliste wird mit dem
// Text aus dem Messagefile (DLL) zusammengefuegt
//******************************************************************************
//------------------------------------------------------------------------------
procedure DecodeEventlogRecord(aEvLogRec: PEventRecord; aEventItem: PEventItemRec; aSourceSection: String);
var
  xMessageText, xCategory{, xLogText{}: String;
  n : integer;
  //..................................................
  function GetDescriptionText: String;
  var
    i: DWord;
    xCharStart, xCharIndex: PChar;
    xStrList: TStringList;
  begin
    xStrList := TStringList.Create;
    // set start position of text
    xCharStart := PChar(aEvLogRec) + aEvLogRec.StringOffset;
    xCharIndex := xCharStart;

    i := aEvLogRec.StringOffset;
    while (i < aEvLogRec^.DataOffset) do begin
      if xCharIndex^ = #0 then begin
        xStrList.Add(StrPas(xCharStart));
        xCharStart := xCharIndex + 1;  // jump over the #0 character
      end;
      inc(i);
      inc(xCharIndex);
    end;
    Result := xStrList.CommaText;
    xStrList.Free;
  end; // END GetDescriptionText
  //..................................................
begin
  aEventItem^.Description := GetDescriptionText;

  // lese dazuheoerige MessageFile.dll aus
  // nur Text aus DLL in die Liste schreiben, ist sehr langsam
  GetMsgFromDll(aEventItem, xMessageText, xCategory, aSourceSection);

  // Datenrecord abfuellen
  aEventItem^.Category    := xCategory;
  aEventItem^.Description := xMessageText; // Text von Log und DLL
end; // END FillupEventTextList

//******************************************************************************
// Liest Zuasatzbeschreibung aus dem Event-Log-File
//******************************************************************************
//procedure GetBinaryData(var aText: String; var aDataSize: Integer; aEvLogRec: PEventRecord);
procedure GetBinaryData(aEvLogRec: PEventRecord; aEventItem: PEventItemRec);
var
  xStartpos: Integer;
  xData: PChar;
begin
   if (aEvLogRec^.DataLength > 0) then begin
     xStartPos := aEvLogRec^.DataOffset - cEventRecordSize;  // DataOffset-56
     if xStartPos >= 0 then begin
       aEventItem^.DataCount := aEvLogRec^.DataLength;
       aEventItem^.Data := AllocMem(aEventItem^.DataCount);

       xData := PChar(aEvLogRec) + aEvLogRec^.DataOffset;
       Move(xData^, aEventItem^.Data^, aEventItem^.DataCount);
     end;
   end;
end; // END GetBinaryData
//******************************************************************************
// Liest die Source und den Computernamen aus dem Event-Log-File
//******************************************************************************
procedure GetSourceAndPCName(aEvLogRec: PEventRecord; aEventRec: PEventItemRec);
var
  xChar: PChar;
begin
  // set offset to get source name (terminated with #0)
  xChar := PChar(aEvLogRec) + cEventRecordSize;
  aEventRec^.SourceName := StrPas(xChar);
  // set offset to get computer name (terminated with #0)
  xChar := xChar + StrLen(xChar) + 1;
  aEventRec^.Computername := StrPas(xChar);
end; // END GetSourceAndPCName

//******************************************************************************
// Description: Liest einen Eventlog-Record aus der NT-Eventlog-Datei
// Input......: OpenEventLog-Handle, Recordnummer
// Output.....: Record mit den Daten
// Info.......: Benoetigt ConstAPI.pas
//******************************************************************************
function ReadOneEventRec(aHandle_EventLog : THandle; aRecNr:Integer;
                        var aEvLogRec: pEventRecord; var aSize: DWord): Boolean;

var
  xReadFlags,
  xBytesRead ,
  xMinNumberOfBytesNeeded: DWord;
begin
  xReadFlags := EVENTLOG_SEEK_READ or EVENTLOG_FORWARDS_READ;

  Result := ReadEventLog(aHandle_EventLog,
                         xReadFlags,               // specifies how to read log
                         aRecNr,	           // number of first record
                         aEvLogRec,	           // address of buffer for read data
                         aSize,                    // number of bytes to read
                         xBytesRead,	           // number of bytes read
                         xMinNumberOfBytesNeeded); // number of bytes required for next record


  // Buffergroesse war zu klein: Speicherbereich vergroessern
  if not Result and (aSize < xMinNumberOfBytesNeeded) then begin
    aSize := xMinNumberOfBytesNeeded;
    ReallocMem(aEvLogRec, aSize);

    Result := ReadEventLog(aHandle_EventLog,
                         xReadFlags,               // specifies how to read log
                         aRecNr,	           // number of first record
                         aEvLogRec,	           // address of buffer for read data
                         aSize,                    // number of bytes to read
                         xBytesRead,	           // number of bytes read
                         xMinNumberOfBytesNeeded); // number of bytes required for next record
  end;
end;// END ReadOneEventRec

//******************************************************************************
// Decodiert Datum und Zeit mit der Basis 1.1.1970 00:00:00;
// aTimeStamp ist die Zeit in UTC (GMT). Diese Zeit wird mit dem Bias auf LT
// umgerechnet.
//******************************************************************************
function EncodeEventDateTime(aTimeStamp: LongInt): TDateTime;
var
  xTimeZoneInformation: TTimeZoneInformation;
  xBias : LongInt;
  xDWord: DWord;
begin
  if aTimeStamp > 0 then begin
    xDWord := GetTimeZoneInformation(xTimeZoneInformation);
    xBias  := xTimeZoneInformation.Bias * 60;
    case xDWord of
      TIME_ZONE_ID_DAYLIGHT:  xBias := (xTimeZoneInformation.Bias  +
                                        xTimeZoneInformation.StandardBias +
                                        xTimeZoneInformation.DaylightBias) * 60;
    else
    end;
    Result:= (aTimeStamp - xBias) / 3600 / 24 + cDateBase1970 ;
  end else
    Result := 0;
end;
//------------------------------------------------------------------------------

var
  i: Integer;
initialization
  gDllHandleList := TStringList.Create;

  gEventList := TStringList.Create;
  gEventList.Duplicates := dupIgnore;
  gEventList.Sorted := TRUE;
  gRegInstance := TmmRegistry.Create;

finalization
  FreeAndNil(gRegInstance);
  for i:=0 to gDllHandleList.Count-1 do begin
    FreeLibrary(THandle(gDllHandleList.Objects[i]));
  end;
  gDllHandleList.Free;
  gEventList.Free;
end.

