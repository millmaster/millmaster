(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: EventMain.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Hauptprogramm und Form des MillMaster EventViewers
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 01.09.1998  0.00  Sdo | Projekt begonnen
| 25.09.1998  1.00  Wss | Ueberarbeitet
|                       | ReportForm wird nun statisch gehalten: Druck mit mReport.PrintReport()
| 19.03.1999  1.01  Wss | Fehler wenn Eventlog leer behoben, HeaderControl entfernt und FixedRows
|                       | vom Grid selber benutzt, StringGrid durch TmmStringGrid mit sortierung,
|                       | Zwischen lokal und remote (gMMHost variable) waehlbar
| 17.05.1999  1.02  Sdo | TmmStringGrid durch TAdvStringGrid ersetzt.
|                       | EventLogfile loeschen -> ClearEventLog()
| 19.08.1999  1.03  Wss | Pause Button eingefuegt
| 20.01.2000  1.04  Sdo | TAdvStringGrid wieder durch TmmStringGrid ersetzt,
|                       | da im TmmStringGrid das TAdvStringGrid eingebaut wurde
| 27.01.2000  1.05  Sdo | Fehler, '1. Zeile wird immer ueberschrieben, wenn neue
|                       | Events eintreffen', wurde behoben. -> Row einfuegen immer in 1.Zeile
| 10.02.2000  1.06  Sdo | - Grid Header auf Bold gestellt.
|                       | - Status mit Buchstabe hinterlegt. (sgEventsGetCellColor, sgEventsDrawCell)
|                       | - Absturz durch Refresh DBlClick behoben. (acGoExecute)
| 12.10.2000  1.07  Wss | nochmals start ueberarbeitet. PrintForm (mReport) wird nun dynamisch erzeugt
                          - Filterfunktion implementiert.
                          - Fehler bei vollem Eventlog behoben.
                          - Fehler bei Acess denied auf drucker abgefangen
| 25.01.2001  1.08  Wss | Adjust Date/Time dialog implementiert um Zeitbasis von Kunden anzupassen
| 16.11.2001  1.09  Wss | When access denied for deleting EventLog -> Grid was still cleared
| 28.11.2001  1.10  Wss | Default save dir set to ..\Log (=D:\MillMaster\Log)
| 04.11.2002        LOK | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 11.12.2002        Wss | Beim Speichern wird default gleich ein Dateiname ala yyyymmdd vorgegeben
| 12.02.2003        Wss | 2 Icons ersetzt: Print, Exit
| 13.04.2004        Wss | Bei Filter "Computername" -> "Computer"
| 01.09.2004        Wss | Beim �ffnen einer Datei per Parameter wurde auf direkt per = '.ev2' verglichen statt per AnsiSameText
| 16.02.2005        SDo | Erweiterung:  mittels Komandozeile Eventlog speichern oder oeffnen
| 16.11.2005        Wss | Filter nun per Kommatrennung mehrere Eintr�ge m�glich
|=============================================================================*)
unit EventMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EventLogProc, ExtCtrls, ToolWin, ComCtrls, Menus, ImgList, ActnList,
  StdCtrls, Grids, mmActionList, IvMlDlgs, mmPrinterSetupDialog,
  mmSaveDialog, mmOpenDialog, mmImageList, mmMainMenu, mmStringGrid,
  mmAnimate, mmLabel, mmToolBar, mmPanel, IPCClass, BaseForm,
  MMEventLog, Report, ReadThread, AdvGrid, mmTimer, mmEdit, mmRadioGroup,
  mmStatusBar, mmExtStringGrid, syncobjs, mmBitBtn;


const
  WM_FinishPrint = WM_User + 2;

  cMMName      = 'MillMaster';
  cMMDebName   = 'MillMasterDebug';
  cMMGuardName = 'MMGuard';
  cAllName     = 'All';

  cFormCaptionLog  = 'MillMaster EventViewer  [ Location = %s,  Log = %s ]';
  //cFormCaptionFile = 'Millmaster Eventviewer  [ Location = %s,  File = %s ]';
  cFormCaptionFile = 'Millmaster Eventviewer  [File = %s ]';
  cMsgPrintError = 'Could not print events. Reason: %s';

  cFixedRowCount = 1;

type
  TGridHeaderRec = record
    Title: String;
    Width: Integer;
    Sorting: Boolean;
  end;

const
  cMsgClearEvent = 'Do you want to save this event log before clearing it?'; //iv-----

  cMaxHeaderCount     = 10;
  cVisibleHeaderCount = 7;
  cGridHeaderArr: Array[0..cMaxHeaderCount-1] of TGridHeaderRec = (
    (Title: '';             Width:  25; Sorting: True),
    (Title: 'Date ';        Width:  65; Sorting: True),
    (Title: 'Time ';        Width:  65; Sorting: True),
    (Title: 'Source ';      Width: 100; Sorting: True),
    (Title: 'Category';     Width: 100; Sorting: False),
    (Title: 'Computername'; Width: 100; Sorting: False),
    (Title: 'Description';  Width: 100; Sorting: False),
    (Title: '';             Width:  -1; Sorting: False),
    (Title: '';             Width:  -1; Sorting: False),
    (Title: '';             Width:  -1; Sorting: False)
  );

type
  TMMFilterEnum = (mmfMillMaster, mmfMillMasterDebug, mmfMMGuard, mmfAll);
  TMMFilterSet = set of TMMFilterEnum;

  TfrmMain = class(TmmForm)
    MainMenu1: TmmMainMenu;
    File1: TMenuItem;
    Log1: TMenuItem;
    miApplication: TMenuItem;
    miSystem: TMenuItem;
    Print1: TMenuItem;
    PrinterSetup1: TMenuItem;
    N1: TMenuItem;
    Saveas1: TMenuItem;
    Load1: TMenuItem;
    N2: TMenuItem;
    Exit1: TMenuItem;
    ImageList1: TmmImageList;
    Panel1: TmmPanel;
    ToolBar1: TmmToolBar;
    ToolButton1: TToolButton;
    tb_Space1: TToolButton;
    Label1: TmmLabel;
    ToolButton3: TToolButton;
    tb_MillMaster: TToolButton;
    tb_MMDebug: TToolButton;
    tb_MMGuard: TToolButton;
    tb_All: TToolButton;
    ToolButton8: TToolButton;
    Label2: TmmLabel;
    ToolButton9: TToolButton;
    tb_Status1: TToolButton;
    tb_Status2: TToolButton;
    tb_Status3: TToolButton;
    tb_Status4: TToolButton;
    Tb_Space5: TToolButton;
    Animate1: TmmAnimate;
    ToolButton5: TToolButton;
    ActionList1: TmmActionList;
    acPrint: TAction;
    acPreview: TAction;
    Preview1: TMenuItem;
    miLocation: TMenuItem;
    miSource: TMenuItem;
    miRemote: TMenuItem;
    miLocal: TMenuItem;
    sgEvents: TmmExtStringGrid;
    acClearAllEvents: TAction;
    CearAllEvents1: TMenuItem;
    CearAllEvents2: TMenuItem;
    acSaveAs: TAction;
    bPause: TToolButton;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    PrinterSetupDialog1: TmmPrinterSetupDialog;
    acOpen: TAction;
    acExit: TAction;
    pnOptions: TmmPanel;
    rgFiltered: TmmRadioGroup;
    edFilter: TmmEdit;
    mmLabel1: TmmLabel;
    ToolButton6: TToolButton;
    acFiltered: TAction;
    UpdateTimer: TmmTimer;
    sbMain: TmmStatusBar;
    acAdjustDateTime: TAction;
    N3: TMenuItem;
    AdjustDateTime1: TMenuItem;
    ToolButton2: TToolButton;
    ToolButton4: TToolButton;

    procedure acClearAllEventsExecute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acOpenClick(Sender: TObject);
    procedure acPreviewExecute(Sender: TObject);
    procedure acPrintClick(Sender: TObject);
    procedure acSaveAsExecute(Sender: TObject);
    procedure EventLogClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure miApplicationClick(Sender: TObject);
    procedure PrinterSetup1Click(Sender: TObject);
    procedure sgEventsDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEventsDblClick(Sender: TObject);
    procedure sgEventsKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgEventsMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure StateFilterClick(Sender: TObject);
    procedure tb_AllClick(Sender: TObject);
    procedure tb_MillMasterClick(Sender: TObject);
    procedure ToolBar1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure WMFillupNewData(var Message: TMessage); message WM_ReadEvent;
    procedure WMFinishPrint(var Message: TMessage); message WM_FinishPrint;
    procedure sgEventsEndColumnSize(Sender: TObject; aCol: Integer);
    procedure sgEventsCanSort(Sender: TObject; aCol: Integer; var dosort: Boolean);
    procedure bPauseClick(Sender: TObject);
    procedure sgEventsGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure acFilteredExecute(Sender: TObject);
    procedure edFilterChange(Sender: TObject);
    procedure rgFilteredClick(Sender: TObject);
    procedure UpdateTimerTimer(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure sgEventsClickCell(Sender: TObject; Arow, Acol: Integer);
    procedure acAdjustDateTimeExecute(Sender: TObject);
  private
    mCol: Integer;
    mCriticalSection : TCriticalSection;
    mEventList: TEventList;
//    mFilterStr: String;
    mHostName: String;
    mMillMasterSet: TMMFilterSet;
    mOldRow: Integer;
    mReadThread: TReadThread;
    mRow: Integer;
    mSelectedEvents: TEventTypeSet;
    mSortingCol: Integer;
    mSourceSection: String;
    mClickRow: Integer;
    mFilterList: TStringList;

    function Anzeigefilter(aSource: String; aEventItem: PEventItemRec): Boolean;
    procedure ClearEventList;
    procedure DruckReport(aPreview: Boolean);
    function GetSourceText:String;
    procedure ReadNewLogEvents;
    procedure setHintDefaults;
    procedure ShowDetail;
    procedure ShowFiltredEvents;
    procedure OpenEventFile(aFile: String);
  protected
  public
  end;

var
  frmMain: TfrmMain;

implementation // 15.07.2002 added mmMBCS to imported units

{$R *.DFM}
//{$R MMLogo.RES}

{+$I}

uses
  mmMBCS, mmCS,
  LoepfeGlobal, BaseGlobal, EventDetailForm, printers, Settingsreader,
  AdjustDateForm, FileCtrl;



//------------------------------------------------------------------------------
procedure TfrmMain.acClearAllEventsExecute(Sender: TObject);
var
  xHnd: THandle;
  xReturn: Word;
begin
  xReturn := MessageDlg(cMsgClearEvent, mtConfirmation, [mbYes, mbNo, mbCancel], 0);
  if xReturn in [mrYes, mrNo] then begin
    if xReturn = mrYes then
      acSaveAs.Execute;

    xHnd := OpenEventLog(PChar(mHostName), PChar(mSourceSection));
    if xHnd <> Null then begin
      if ClearEventLog(xHnd, NIL) then
        ClearEventList
      else
        ShowMessage(Format('Could not clear eventlog: %s', [GetLastErrorText]));
      CloseEventLog(xHnd);
    end else
      ShowMessage(Format('Could not connect to eventlog: %s', [GetLastErrorText]));
  end; // if xReturn
{
  xReturn := MessageDlg(cMsgClearEvent, mtConfirmation, [mbYes, mbNo, mbCancel], 0);
  if xReturn in [mrYes, mrNo] then begin
    if xReturn = mrYes then
      acSaveAs.Execute;

    xHnd := OpenEventLog(PChar(mHostName), PChar(mSourceSection));
    if xHnd <> Null then begin
      if not ClearEventLog(xHnd, NIL) then
        ShowMessage(Format('Could not clear eventlog: %s', [GetLastErrorText]));
      CloseEventLog(xHnd);
      ClearEventList;
    end else
      ShowMessage(Format('Could not connect to eventlog: %s', [GetLastErrorText]));
  end; // if xReturn
{}
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acExitExecute(Sender: TObject);
begin
  Close;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acPreviewExecute(Sender: TObject);
begin
  DruckReport(True);
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acPrintClick(Sender: TObject);
begin
  DruckReport(False);
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acSaveAsExecute(Sender: TObject);
begin
//  SaveDialog.InitialDir:= ExtractFilepath(Application.ExeName);
  with SaveDialog do begin
    InitialDir := '..\Log';
    FileName   := FormatDateTime('yyyymmdd', Now);
    if Execute then begin
      mReadThread.Suspend;
      mEventList.SaveToFile(Filename);
      mReadThread.Resume;
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// AnzeigeAnzeigefilter
// Anhand der gewaehlten Sourcen, weden die entsprechenden Daten angezeigt
//******************************************************************************
function TfrmMain.Anzeigefilter(aSource: String; aEventItem: PEventItemRec): Boolean;
var
  xSource: TMMFilterEnum;
  xStr: String;
  i: Integer;
begin
  if mmfAll in mMillMasterSet then    xSource := mmfAll
  else if aSource = cMMName then      xSource := mmfMillMaster
  else if aSource = cMMDebName then   xSource := mmfMillMasterDebug
  else if aSource = cMMGuardName then xSource := mmfMMGuard
  else                                xSource := TMMFilterEnum(255);

  Result := (aEventItem^.EventType in mSelectedEvents) and (xSource in mMillMasterSet);
  if Result and acFiltered.Checked and (mFilterList.Count > 0) then begin
    case rgFiltered.ItemIndex of
      // Source
      0: xStr := LowerCase(aEventItem^.SourceName);
      // Category
      1: xStr := LowerCase(aEventItem^.Category);
      // Computername
      2: xStr := LowerCase(aEventItem^.Computername);
      // Description
      3: xStr := LowerCase(aEventItem^.Description);
    else
      Exit;
    end;

    for i:=0 to mFilterList.Count-1 do begin
      Result := (Pos(mFilterList.Strings[i], xStr) > 0);
      if Result then
        Break;
    end;
  end;
//  if mmfAll in mMillMasterSet then    xSource := mmfAll
//  else if aSource = cMMName then      xSource := mmfMillMaster
//  else if aSource = cMMDebName then   xSource := mmfMillMasterDebug
//  else if aSource = cMMGuardName then xSource := mmfMMGuard
//  else                                xSource := TMMFilterEnum(255);
//
//  Result := (aEventItem^.EventType in mSelectedEvents) and (xSource in mMillMasterSet);
//  if Result and acFiltered.Checked and (Trim(edFilter.Text) <> '') then begin
//    case rgFiltered.ItemIndex of
//      // Source
//      0: xStr := LowerCase(aEventItem^.SourceName);
//      // Category
//      1: xStr := LowerCase(aEventItem^.Category);
//      // Computername
//      2: xStr := LowerCase(aEventItem^.Computername);
//      // Description
//      3: xStr := LowerCase(aEventItem^.Description);
//    else
//      Exit;
//    end;
//    Result := (Pos(mFilterStr, xStr) > 0);
//  end;
end; // END Anzeigefilter
//------------------------------------------------------------------------------
// Events drucken
procedure TfrmMain.DruckReport(aPreview: Boolean);
begin
  mReadThread.Suspend;

  acPrint.Enabled   := False;
  acPreview.Enabled := False;
  Animate1.Active := TRUE;
  // Report im Hintergrund drucken
  try
    with TReportForm.Create(Self, Handle, sgEvents, mEventList) do
    try
      if aPreview then
        PreviewReport(GetSourceText, mSourceSection)
      else
        PrintReport(GetSourceText, mSourceSection);
    finally
      Free;
    end;
  except
    ShowMessage(Format(cMsgPrintError, [GetLastErrorText]));
  end;
  acPrint.Enabled   := True;
  acPreview.Enabled := True;
  Animate1.Active := FALSE;

  mReadThread.Resume;
end; // END DruckReport
//------------------------------------------------------------------------------
procedure TfrmMain.EventLogClick(Sender: TObject);
begin
  (Sender as TMenuItem).Checked := True;
  ReadNewLogEvents;
  ShowFiltredEvents;  // Rows loeschen
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormCreate(Sender: TObject);
var
  i: integer;
  xParam1 , xParam2, xPath, xFile, xExt, xHelpText : String;
  xDrive: Char;
  xOpen, xSave, xHelp : boolean;
begin

  CodeSite.SendDateTime('Begin FormCreate', NOW);

  RestoreFormLayout;

  mEventList      := TEventList.Create;
//  mFilterStr      := '';
  mFilterList     := TStringList.Create;
  mSelectedEvents := [etSuccess..etError];


  sgEvents.Align := alClient;
  sgEvents.HideColumns(7,sgEvents.ColCount);

  for i:=0 to cVisibleHeaderCount-1 do begin
    sgEvents.Cells[i, 0]  := cGridHeaderArr[i].Title;
    sgEvents.ColWidths[i] := cGridHeaderArr[i].Width - sgEvents.GridLineWidth;
  end;

//  mSourceSection := 'Application';
  mSourceSection := 'System';
  if mHostName = '.' then
     Self.Caption := Format(cFormCaptionLog, ['Local', mSourceSection])
  else
     Self.Caption := Format(cFormCaptionLog, [mHostName, mSourceSection]);

  tb_MillMaster.Hint := cMMName;
  tb_MMDebug.Hint    := cMMDebName;
  tb_MMGuard.Hint    := cMMGuardName;
  tb_All.Hint        := cAllName;

  mMillMasterSet := [mmfAll]; //[mmfMillMaster..mmfMMGuard];
//  mStateSet      := [0..3];

  mOldRow     := -1;
  mSortingCol := -1;

  // AVI-File aus Res. laden
  Animate1.ResHandle := FindResource(HInstance, 'MMLogo.avi', 'AVI');
  Animate1.ResName   := 'LOGO';

  // Thread und ReportForm erstellen
  mCriticalSection := TCriticalSection.Create;
  mReadThread      := TReadThread.Create(Handle, mEventList, mCriticalSection);

  {
  if ParamCount = 1 then
    OpenEventFile(ParamStr(1))
  else
    ReadNewLogEvents;
  }

  xOpen := FALSE;
  xSave := FALSE;
  xHelp := FALSE;
  xPath := '';
  xFile := '';

  case ParamCount of
    1, 2: begin
         xParam1 := ParamStr ( 1 );
         xParam2 := ParamStr ( 2 );

         //Eventviewer oeffnen mit dbclick auf Logfile
         if ParamCount = 1 then
            if FileExists( xParam1 ) then begin
               xParam2 := 'f=' + xParam1;
               xParam1 := 'm=o';
            end;

         //Mode = save
         if (Pos('m=', LowerCase(xParam1)) >0 )then begin
            //if (Pos('=as', LowerCase(xParam1)) >0 )then xAutoSave := TRUE;
            if (Pos('=o', LowerCase(xParam1)) >0 )then xOpen := TRUE;
            if (Pos('=s', LowerCase(xParam1)) >0 )then xSave := TRUE;
         end;

         if xOpen and (xParam2 = '') then begin
            Application.Terminate;
            exit;
         end;

         if xSave and (xParam2 = '') then xParam2:= 'f='; //-> Auto-File erstellen

         //Folder / File
         if (Pos('f=', LowerCase(xParam2)) >0 ) then begin
            xParam2 := StringReplace(xParam2, 'f=', '', [rfReplaceAll]);
            xParam2 := StringReplace(xParam2, 'F=', '', [rfReplaceAll]);

            if xSave then begin
                try
                   if xParam2 <> '' then
                      ProcessPath (xParam2, xDrive, xPath, xFile);
                finally
                end;

                if (xPath <> '') and (xDrive <> '') then
                   xPath := Format('%s:%s',[xDrive, xPath])
                else
                   xPath := ExtractFilePath(Application.ExeName);  //Auto-Path

                if xFile = '' then begin
                   xFile := FormatDateTime('"AutoName_"yyyy-mm-dd_hh-mm."ev2"', Now); //Auto-File
                end;

                //Extension check
                xExt:= ExtractFileExt(xFile);
                xExt := StringReplace( xExt, '.', '',[rfReplaceAll]);
                if xExt = '' then
                   xPath := xPath + '.ev2';
            end;
         end;

         if xOpen then
           if FileExists( xParam2 ) then  begin
              mReadThread.Suspend;
              ClearEventList;
              acClearAllEvents.Enabled := FALSE;
              OpenEventFile( xParam2 );
              setHintDefaults;
           end else begin
             Application.Terminate;
             exit;
           end;


         //Help Window
         if ( ParamStr ( 1 ) = 'H' ) or ( ParamStr ( 1 ) = 'h' ) or
            ( ParamStr ( 1 ) = '?' ) then begin

             xHelpText:= 'Help:' + cCRLF +  cCRLF +
                         'Parameter:' + cCRLF + 'm = mode [s save; o open; h or ? help],' +
                         cCRLF + 'f = folder or file name' + cCRLF + cCRLF +
                         'Outut Eventlog autoname : AutoName_yyyy-mm-dd_hh-mm.ev2' + cCRLF + cCRLF +
                         'Examples:' + cCRLF +
                         'Save an auto Eventlog file name in actual directory : MMEventViewer.exe m=s' +  cCRLF +
                         'Save an auto Eventlog file name in a specific directory : MMEventViewer.exe m=s f=c:\temp' +  cCRLF +
                         'Save an own Eventlog file name in actual directory : MMEventViewer.exe m=s f=EventLog.ev2' +  cCRLF +
                         'Save an own Eventlog file name in a specific directory : MMEventViewer.exe m=s f=c:\temp\EventLog.ev2' +  cCRLF +
                         cCRLF +
                         'Open an Eventlog file form a specific directory : MMEventViewer.exe m=o f=c:\temp\EventLog.ev2' +  cCRLF +
                         'Open an Eventlog file form actual directory : MMEventViewer.exe m=o f=EventLog.ev2';

             MessageDlg( xHelpText, mtInformation,   [mbOk], 0);

             Application.Terminate;
             exit;
         end;

         if xSave then begin
            mReadThread.ReadAllEvents;

            xPath := xPath + '\' + xFile;
            xPath := StringReplace( xPath, '\\', '\',[rfReplaceAll]);
            if mEventList.Count > 0 then
               mEventList.SaveToFile(xPath);

            Application.Terminate;
         end;
       end;
     else begin
        ReadNewLogEvents;
     end;
  end; //END case

  setHintDefaults;
  CodeSite.SendDateTime('END FormCreate', NOW);

//  ReadNewLogEvents;
//  setHintDefaults;
//  CodeSite.SendDateTime('END FormCreate', NOW);
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  mReadThread.Terminate;
  try
    while mReadThread.Suspended do
      mReadThread.Resume;

    mReadThread.WaitFor;  // wait for terminated thread

    mEventList.Free;
  except
    on E: Exception do
        CodeSite.SendError('FormDestroy mReadThread.WaitFor. Error msg : ' + e.Message);
  end;
  mCriticalSection.Free;

  mFilterList.Free;
  SaveFormLayout;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acOpenClick(Sender: TObject);
{
var
  i: Integer;
  xItem: PEventItemRec;
  xSG: TmmExtStringGrid;
  xStr: String;
  xDateFormat: String;
  xDateSeparator: Char;
  xTimeFormat: String;
  xAM, xPM: String;
  xDate: TDate;
  xTime: TTime;
}
begin
  // Thread zum lesen des Eventlogs wird im Moment nicht mehr benoetigt
  mReadThread.Suspend;
  OpenDialog.InitialDir:= ExtractFilepath(Application.ExeName);
  if OpenDialog.Execute then begin
    ClearEventList;
    acClearAllEvents.Enabled := FALSE;
    OpenEventFile(OpenDialog.FileName);
  end else
    mReadThread.Resume;


end;
//------------------------------------------------------------------------------
procedure TfrmMain.OpenEventFile(aFile: String);
var
  i: Integer;
  xItem: PEventItemRec;
  xSG: TmmExtStringGrid;
  xStr: String;
  xDateFormat: String;
  xDateSeparator: Char;
  xTimeFormat: String;
  xAM, xPM: String;
  xDate: TDate;
  xTime: TTime;
begin
  // load file in *.ev2 format
//  if OpenDialog.FilterIndex = 1 then
  if FileExists(aFile) then begin
    Self.Caption:= Format(cFormCaptionFile, [aFile]);


    if AnsiSameText(ExtractFileExt(aFile), '.ev2') then
      mEventList.LoadFromFile(aFile)
    else begin
      // load file in *.mev format (previous version)
      // save original date format
      xDateFormat := ShortDateFormat;
      xDateSeparator := DateSeparator;
      xTimeFormat := ShortTimeFormat;
      xAM         := TimeAMString;
      xPM         := TimePMString;
      with TmmExtStringGrid.Create(Self) do
      try
        FixedCols := 0;
        FixedRows := 1;
        LoadFromFile(aFile);
        for i:=RowCount-1 downto 1 do begin
          new(xItem);
          with xItem^ do
          try
            case StrToInt(Cells[0, i]) of
              clGreen:  EventType := etSuccess;
              clNavy:   EventType := etInformation;
              clYellow: EventType := etWarning;
              clRed:    EventType := etError;
            else
            end;
            SourceName   := Cells[3, i];          // Source
            Category     := Cells[4, i];            // Category
            Computername := Cells[5, i];        // Computername
            // Beschreibung ohne Steuerzeichen
            Description  := Cells[6, i];
            // nonvisible
            DataCount := 0;
            Data      := Nil;
            DataCount := Length(Cells[7, i]);
            try
              try
                xStr := Cells[2, i];
                xTime := StrToTime(Cells[2, i]);
              except
                // maybe it is in the US format: 12 hours
                ShortTimeFormat := 'HH:mm:ss tt';
                TimeAMString := 'AM';
                TimePMString := 'PM';
                try
                  xTime := StrToTime(Cells[2, i]);
                except
                  xTime := Now;
                end;
              end;

              xStr := Cells[1, i];
              // if / is in date string the it is the US format
              if (Pos('/', Cells[1, i]) > 0) or (Pos('-', Cells[1, i]) > 0) and (Pos('/', ShortDateFormat) = 0) then begin
                if (Pos('/', Cells[1, i]) > 0) then begin
                  ShortDateFormat := 'mm/dd/yy';
                  DateSeparator := '/';
                end else begin
                  DateSeparator := '-';
                end;
              end;
              xDate := StrToDate(Cells[1, i]);

              DateTime := xDate + xTime;
            except
              DateTime := Now;
            end;
          except
            on e:Exception do begin
              CodeSite.SendError(Format('MEV Import error: %s/%s', [GetLastErrorText, e.Message]));
            end;
          end;  // with xItem^
          mEventList.Add(xItem);
        end;  // for i
      finally
        Free;
        ShortDateFormat := xDateFormat;
        DateSeparator   := xDateSeparator;
        ShortTimeFormat := xTimeFormat;
        TimeAMString    := xAM;
        TimePMString    := xPM;
      end;  // with xSG
    end;
    ShowFiltredEvents;
    if sgEvents.RowCount > 1 then
      mClickRow := 1;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.miApplicationClick(Sender: TObject);
begin
  acClearAllEvents.Enabled := True;
  (Sender as TMenuItem).Checked := True;
  ReadNewLogEvents;
  ShowFiltredEvents;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.PrinterSetup1Click(Sender: TObject);
begin
  PrinterSetupDialog1.Execute;
end;
//------------------------------------------------------------------------------
// Liest Events neu ein;  ReadEvents-Thread loeschen und neu erstellen
procedure TfrmMain.ReadNewLogEvents;
var //xSettings: TMMSettings;
    xSettings: TMMSettingsReader;
begin
//  if mReadThread.Suspended then mReadThread.Resume;

  if miApplication.checked then begin
    // Source Logfile = Application
    mSourceSection := miApplication.hint;
    // Status-Buttons aktiv
    if (tb_All.Down = FALSE) then
      exclude(mMillMasterSet, mmfAll)
    else
      include(mMillMasterSet, mmfAll);

    tb_MillMaster.enabled := TRUE;
    tb_MMGuard.enabled := TRUE;
    tb_MMDebug.enabled := TRUE;
    tb_All.enabled := TRUE;

    tb_MillMaster.Marked := FALSE;
    tb_MMGuard.Marked := FALSE;
    tb_MMDebug.Marked := FALSE;
    tb_All.Marked := FALSE;
  end else begin
    // Source Logfile = System
    mSourceSection := miSystem.hint;
    // Status-Buttons inaktiv
    include(mMillMasterSet, mmfAll);
    tb_MillMaster.enabled := FALSE;
    tb_MMGuard.enabled := FALSE;
    tb_MMDebug.enabled := FALSE;
    tb_All.enabled := FALSE;

    tb_MillMaster.Marked := TRUE;
    tb_MMGuard.Marked := TRUE;
    tb_MMDebug.Marked := TRUE;
    tb_All.Marked := TRUE;
  end;

  mHostName := '.';
  if miRemote.Checked then
  try
    with TMMSettingsReader.Instance do begin
      Init;
      mHostName:= Value['RegMMHost']; // Servername aus Reg, ermitteln
    end;
  except
  end;

  mSourceSection := Trim(mSourceSection);

  if mHostName = '.' then
     Self.Caption := Format(cFormCaptionLog, ['Local', mSourceSection])
  else
     Self.Caption := Format(cFormCaptionLog, [mHostName, mSourceSection]);

  ClearEventList;
  // Thread neu initialisieren
  mReadThread.Init(mHostName, mSourceSection);
end; // END ReadNewLogEvents
//------------------------------------------------------------------------------
procedure TfrmMain.setHintDefaults;
begin
  // Setzt Hint auf Defaultwerte
  Application.HintColor:= clInfoBk;
  sgEvents.HintColor:= Application.HintColor;
  Application.HintPause:=250;
  Application.HintHidePause:=2500;

  Application.HintHidePause := 5000;
  sgEvents.HintColor        := $00FFFF80; // HEX -> 00,B,G,R;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.sgEventsCanSort(Sender: TObject;
  aCol: Integer; var dosort: Boolean);
begin
  if  aCol = 6 then dosort := FALSE;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.sgEventsDblClick(Sender: TObject);
begin
  if mRow >= sgEvents.FixedRows then
    ShowDetail;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.sgEventsDrawCell(Sender: TObject; ACol,
          ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  xText: String;
  xEventType: TEventType;
begin
  // Event Attribut zeichen
  if (ACol = 0) and (ARow >= sgEvents.FixedRows) then
    with sgEvents.Canvas do begin
      xEventType  := TEventType(StrToIntDef(sgEvents.Cells[0, ARow], -1));
      Brush.Color := cEventColors[xEventType];
      FillRect(Rect);

      Font.Color := clWindowText;
      case xEventType of
        etSuccess:     xText := 'S';
        etInformation: begin Font.Color:= clWhite; xText := 'I'; end;
        etWarning:     xText := 'W';
        etError:       xText := 'E';
      else
        xText := '';
      end;
      TextOut( sgEvents.ColWidths[0] DIV 2 - TextWidth(xText) Div 2 ,Rect.Top, xText);
    end;  // with
end; // END SoG_EventsDrawCell
//------------------------------------------------------------------------------
procedure TfrmMain.sgEventsEndColumnSize(Sender: TObject;
  aCol: Integer);
begin
 if aCol = 0 then begin
   sgEvents.ColWidths[0] := cGridHeaderArr[0].Width;
   sgEvents.StretchRightColumn;
 end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.sgEventsKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  // Info-Fenster zeigen
 if (key = VK_RETURN) then ShowDetail;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.sgEventsMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  xCellWidth: Integer;
  xRect: TRect;
begin
  with sgEvents do begin
    MouseToCell(X, Y, mCol, mRow);
    xRect := CellRect(mCol, mRow);
    xCellWidth := xRect.Right - xRect.Left - 2;  // to have 2 pixels spare
    HintShowCells := (mCol = 6) and (xCellWidth < Canvas.TextWidth(Cells[6, mRow]));
  end;
end;
//------------------------------------------------------------------------------
// Filtriert Events aus dem Hiddengrid (sgHideEvents) und kopiert diese ins
// Sortgrig (sgEvents)
procedure TfrmMain.ShowFiltredEvents;
var i, xRow : Integer;
begin

  sgEvents.ClearNormalCells;
  xRow := sgEvents.FixedRows;

  if mEventList.Count > 0 then begin
    mCriticalSection.Enter;
    with mEventList do
    try
//      sgEvents.BeginUpdate;
      try
        for i:=Count-1 downto 0 do begin
          if Anzeigefilter(Event[i]^.SourceName, Event[i]) then begin
            Application.ProcessMessages;
            sgEvents.Cells[0,xRow]:= IntToStr(Ord(Event[i]^.EventType));

            //sgEvents.Cells[12,xRow]:=  sgHideEvents.Cells[12,y];
            sgEvents.Cells[1,xRow]:= DateToStr(Event[i]^.DateTime); // Datum
            sgEvents.Cells[2,xRow]:= TimeToStr(Event[i]^.DateTime); // Zeit
            sgEvents.Cells[3,xRow]:= Event[i]^.SourceName;          // Source
            sgEvents.Cells[4,xRow]:= Event[i]^.Category;            // Category
            sgEvents.Cells[5,xRow]:= Event[i]^.Computername;        // Computername
            // Beschreibung ohne Steuerzeichen
            sgEvents.Cells[6,xRow]:= StringReplace(Event[i]^.Description, cCRLF, ' ', [rfReplaceAll]);
            // nonvisible
            sgEvents.Cells[7,xRow]:= IntToStr(i);  // save index in mEventList
  {
            sgEvents.Cells[7,xRow]:= sgHideEvents.Cells[7,y]; // Data
            sgEvents.Cells[8,xRow]:= sgHideEvents.Cells[8,y]; // Beschreibung mit Steuerzeichen
            sgEvents.Cells[9,xRow]:= sgHideEvents.Cells[9,y]; // Statustext
            sgEvents.Cells[10,xRow]:= sgHideEvents.Cells[10,y]; // Kodierte Zeit (zum Sortzeren)
  {}

            inc(xRow);
          end;  // if
        end;  // for i

        sbMain.Panels[0].Text := Format('Event count/visible: %d/%d', [mEventList.Count, xRow - sgEvents.FixedRows]);

        if xRow = sgEvents.FixedRows then
          sgEvents.RowCount := xRow + 1 // nur eine Zeile -> es muessen 2 Zeilen angezeigt werden (FixedRows)
        else
          sgEvents.RowCount := xRow; // letzte xRow
      finally
//        sgEvents.EndUpdate;
      end;
    finally
      mCriticalSection.Leave;
    end;
  end;  // if
end; // END ShowFiltredEvents
//******************************************************************************
// Zeigt Zusatzinformationen im Informationsfenster an.
// Daten werden aus der angeklickten Grid-Zeile gelesen und mit den
// Zusatzinformationen (Bin. Data) ans Info-Fenster uebergeben
//******************************************************************************
procedure TfrmMain.ShowDetail;
begin
  with sgEvents do
    if (Cells[1,Row] <> '') then
      with TfrmEventDetail.Create(Self) do
      try
                      //   Datum         Zeit          Source
        InfoShowModal(Cells[1,Row], Cells[2,Row], Cells[3,Row],
                      //   Category      Computername
                      Cells[4,Row], Cells[5,Row],
                      //   Messagetext   Data          Status
                      Cells[6,Row], Cells[9,Row], mEventList.Event[StrToInt(Cells[7,Row])]^.Data, mEventList.Event[StrToInt(Cells[7,Row])]^.DataCount);
      finally
        Free;
      end;
end; // END ShowDetail
//------------------------------------------------------------------------------
procedure TfrmMain.StateFilterClick(Sender: TObject);
begin
  with (Sender as TToolButton) do begin
    if Down then
      include(mSelectedEvents, TEventType(Tag))
    else
      exclude(mSelectedEvents, TEventType(Tag));
  end;

 (Sender as TToolButton).Repaint;
  ShowFiltredEvents;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.tb_AllClick(Sender: TObject);
begin
  mMillMasterSet := [mmfAll];
  if tb_All.Down then
  begin
    tb_MillMaster.Down := FALSE;
    tb_MMGuard.Down    := FALSE;
    tb_MMDebug.Down    := FALSE;
  end;

  ShowFiltredEvents;
end; // END tb_AllClick
//------------------------------------------------------------------------------
procedure TfrmMain.tb_MillMasterClick(Sender: TObject);
begin
  tb_All.Down := FALSE;
  Exclude(mMillMasterSet, mmfAll);
  with (Sender as TToolButton) do begin
    if Down then
      include(mMillMasterSet, TMMFilterEnum(Tag))
    else
      exclude(mMillMasterSet, TMMFilterEnum(Tag));
  end;
  (Sender as TToolButton).Repaint;
  ShowFiltredEvents;
end; // END tb_MillMasterClick
//------------------------------------------------------------------------------
procedure TfrmMain.ToolBar1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  //setHintDefaults;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.WMFillupNewData(var Message: TMessage);
begin
  // Meldung auswerten
  // Neu Anzeigen, wenn Daten sich geaendert haben. Sonst Exit
  case (Message.WParam) of
    cGRIDINIT  : begin  // Prog. Aufstart
                   ShowFiltredEvents;  // Rows loeschen
                 end;
    cSAMEEVENTS:; // Daten haben sich nicht geandert
  else
     // Daten haben sich geaendert
     ShowFiltredEvents;
  end;
end; // END WMFillupNewData
//------------------------------------------------------------------------------
procedure TfrmMain.WMFinishPrint(var Message: TMessage);
begin
  acPrint.Enabled   := True;
  acPreview.Enabled := True;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bPauseClick(Sender: TObject);
begin
  if bPause.Down then
    mReadThread.Suspend
  else
    mReadThread.Resume;
end;
//------------------------------------------------------------------------------
function TfrmMain.GetSourceText: String;
var
  s0, s1, s2, xSource: String;
begin
  s0:=''; s1:=''; s2:=''; xSource:='';

  if tb_All.Down then
     xSource:=tb_All.hint
  else begin
    if tb_Millmaster.Down then  s0 := tb_Millmaster.hint +  '; ';
    if tb_MMGuard.Down    then  s1 := tb_MMGuard.hint + '; ';
    if tb_MMDebug.Down    then  s2 := tb_MMDebug.hint + '; ';
    xSource:= s0 +  s1 + s2;
    xSource:= Copy(xSource, 1, length(xSource)-2); // letztes '; ' abschneiden
  end;
  Result:= xSource;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.sgEventsGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  // Header fett schreiben
  if (aRow = 0) {or (ACol=0)} then aFont.Style := [fsBold];
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acFilteredExecute(Sender: TObject);
begin
  acFiltered.Checked := not acFiltered.Checked;
  pnOptions.Visible := acFiltered.Checked;
  ShowFiltredEvents;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.edFilterChange(Sender: TObject);
begin
  UpdateTimer.Enabled := False;
  UpdateTimer.Enabled := True;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.rgFilteredClick(Sender: TObject);
begin
  ShowFiltredEvents;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.ClearEventList;
begin
  mEventList.Clear;
  sgEvents.ClearNormalCells;
  sgEvents.RowCount := sgEvents.FixedRows + 1;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.UpdateTimerTimer(Sender: TObject);
begin
  UpdateTimer.Enabled := False;
//  mFilterStr := LowerCase(edFilter.Text);
  mFilterList.CommaText := LowerCase(Trim(edFilter.Text));
  ShowFiltredEvents;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
const
  cNoYes: Array[Boolean] of String = ('No', 'Yes');
begin
  acAdjustDateTime.Enabled := not acClearAllEvents.Enabled;
{
  if mEventList.Count > 0 then
    with sgEvents do
    try
      sbMain.Panels[1].Text := Format('Current row: %d', [Row]);
      if Trim(Cells[7,Row]) <> '' then
        sbMain.Panels[2].Text := Format('Data: %s', [cNoYes[Assigned(mEventList.Event[StrToInt(Cells[7,Row])]^.Data)]]);
    except
    end;
{}
end;
//------------------------------------------------------------------------------
procedure TfrmMain.sgEventsClickCell(Sender: TObject; Arow, Acol: Integer);
begin
  mClickRow := aRow;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acAdjustDateTimeExecute(Sender: TObject);
var
  xData: PEventItemRec;
  xOffset: TDateTime;
  i: Integer;
begin
//          sgEvents.Cells[7,xRow]:= IntToStr(i);  // save index in mEventList
  i := StrToIntDef(sgEvents.Cells[7, mClickRow], -1);
  if (i = -1) or (i > mEventList.Count-1) then
    Exit;

  xData := mEventList.Items[i];
  with TfrmAdjustDateTime.Create(Self) do
  try
    edFromDate.Text := DateTimeToStr(xData^.DateTime);
    edToDate.Text   := DateTimeToStr(Now);
    if ShowModal = mrOK then begin
      try
        xOffset := StrToDateTime(edToDate.Text) - StrToDateTime(edFromDate.Text);
        with mEventList do begin
          for i:=0 to Count-1 do begin
            with PEventItemRec(Items[i])^ do
              DateTime := DateTime + xOffset;
          end;
        end;
        ShowFiltredEvents;
      except
        on e:Exception do begin
          ShowMessage('Adjust Date/Time not possible. Reason: ' + e.Message);
        end;
      end;
    end;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------

initialization
  CodeSite.Enabled := GetRegBoolean(cRegLM, cRegMMDebug, 'MMEventViewer', False);
end.

