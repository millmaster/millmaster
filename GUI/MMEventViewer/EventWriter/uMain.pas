unit uMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton, Grids, AdvGrid, mmStringGrid, mmLabel, mmEdit,
  mmCheckBox;

type
  TForm1 = class(TForm)
    bWrite60: TmmButton;
    bWrite1: TmmButton;
    edPCName: TmmEdit;
    cbRemote: TmmCheckBox;
    mmButton1: TmmButton;
    mmButton2: TmmButton;
    procedure bWrite60Click(Sender: TObject);
    procedure bWrite1Click(Sender: TObject);
    procedure cbRemoteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    mHost: String;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,

  RzCSIntf,
  LoepfeGlobal, EventLog, EventLogProc;

const
  cMsg = 'Testmeldung XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';

procedure TForm1.bWrite60Click(Sender: TObject);
var
  i: Integer;
begin
  for i:=1 to 20 do begin
    WriteToEventLog(Format(cMsg, [GetTickCount]), 'Event Writer: ', etError, 'MillMaster', mHost);
    Sleep(50);
    WriteToEventLog(Format(cMsg, [GetTickCount]), 'Event Writer: ', etInformation, 'MillMaster', mHost);
    Sleep(50);
    WriteToEventLog(Format(cMsg, [GetTickCount]), 'Event Writer: ', etWarning, 'MillMaster', mHost);
    Sleep(50);
  end;
end;
//------------------------------------------------------------------------------
procedure TForm1.bWrite1Click(Sender: TObject);
var
  xEvent: TEventType;
begin
  xEvent := TEventType(TButton(Sender).Tag);
  WriteToEventLog(Format(cMsg, [GetTickCount]), 'Event Writer: ', xEvent, 'MillMaster', mHost);
end;
//------------------------------------------------------------------------------

procedure TForm1.cbRemoteClick(Sender: TObject);
begin
  edPCName.Enabled := cbRemote.Checked;
  if cbRemote.Checked then mHost := edPCName.Text
                      else mHost := '.';
end;
//------------------------------------------------------------------------------
procedure TForm1.FormCreate(Sender: TObject);
begin
  edPCName.Text := GetRegString(cRegLM, cRegMMCommonPath, cRegMMHost, '.');
  mHost := '.';
end;

begin
  CodeSite.Enabled := False;
end.
