object Form1: TForm1
  Left = 772
  Top = 67
  Width = 246
  Height = 134
  Caption = 'Event Writer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object bWrite60: TmmButton
    Left = 20
    Top = 70
    Width = 75
    Height = 25
    Caption = 'Write 60'
    TabOrder = 0
    Visible = True
    OnClick = bWrite60Click
    AutoLabel.LabelPosition = lpLeft
  end
  object bWrite1: TmmButton
    Tag = 1
    Left = 143
    Top = 6
    Width = 75
    Height = 25
    Caption = 'Write I'
    TabOrder = 1
    Visible = True
    OnClick = bWrite1Click
    AutoLabel.LabelPosition = lpLeft
  end
  object edPCName: TmmEdit
    Left = 30
    Top = 28
    Width = 96
    Height = 21
    Enabled = False
    TabOrder = 2
    Visible = True
    AutoLabel.LabelPosition = lpTop
  end
  object cbRemote: TmmCheckBox
    Left = 10
    Top = 10
    Width = 126
    Height = 17
    Caption = 'On remote computer'
    TabOrder = 3
    Visible = True
    OnClick = cbRemoteClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mmButton1: TmmButton
    Tag = 2
    Left = 143
    Top = 38
    Width = 75
    Height = 25
    Caption = 'Write W'
    TabOrder = 4
    Visible = True
    OnClick = bWrite1Click
    AutoLabel.LabelPosition = lpLeft
  end
  object mmButton2: TmmButton
    Tag = 3
    Left = 143
    Top = 70
    Width = 75
    Height = 25
    Caption = 'Write E'
    TabOrder = 5
    Visible = True
    OnClick = bWrite1Click
    AutoLabel.LabelPosition = lpLeft
  end
end
