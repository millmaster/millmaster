inherited frmAdjustDateTime: TfrmAdjustDateTime
  Left = 381
  Top = 238
  Caption = 'Adjust Date/Time'
  ClientHeight = 151
  ClientWidth = 239
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object laFromDate: TmmLabel [0]
    Left = 24
    Top = 17
    Width = 50
    Height = 13
    Caption = 'From date:'
    FocusControl = edFromDate
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laToDate: TmmLabel [1]
    Left = 24
    Top = 73
    Width = 40
    Height = 13
    Caption = 'To date:'
    FocusControl = edToDate
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  inherited bOK: TmmBitBtn
    Left = 47
    Top = 121
  end
  inherited bCancel: TmmBitBtn
    Left = 147
    Top = 121
  end
  object edFromDate: TmmEdit
    Left = 24
    Top = 32
    Width = 169
    Height = 21
    TabOrder = 2
    Text = 'edFromDate'
    Visible = True
    AutoLabel.Control = laFromDate
    AutoLabel.LabelPosition = lpTop
  end
  object edToDate: TmmEdit
    Left = 24
    Top = 88
    Width = 169
    Height = 21
    TabOrder = 3
    Text = 'edToDate'
    Visible = True
    AutoLabel.Control = laToDate
    AutoLabel.LabelPosition = lpTop
  end
end
