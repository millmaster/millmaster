object Form1: TForm1
  Left = 416
  Top = 256
  BorderStyle = bsDialog
  Caption = 'Caller'
  ClientHeight = 86
  ClientWidth = 162
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 15
    Top = 8
    Width = 130
    Height = 25
    Caption = 'Call ClassDesigner'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 15
    Top = 40
    Width = 130
    Height = 25
    Caption = 'Destroy ClassDesigner'
    TabOrder = 1
  end
end
