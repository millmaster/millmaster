(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_Composite.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Panel fuer Composites
|                 Wird in Matrix Editor(u_Editor) auf Panel paItemDataField gelegt
| Info..........: -
| Develop.system: W2k
| Target.system.: W2k, XP
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 03.07.2003  1.00  Sdo | Projekt erstellt
|=============================================================================*)
unit u_Composite;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmListBox, Buttons, mmEdit,
  u_EditorClasses, ExtCtrls, IvDictio, IvMulti, IvEMulti, mmTranslator,
  mmBitBtn, mmGroupBox, mmSpeedButton;

type
  TComposite = class(TFrame)
    lbCompName: TLabel;
    edFormula: TmmEdit;
    mmTranslator1: TmmTranslator;
    Panel2: TPanel;
    lbHinweis: TLabel;
    Image1: TImage;
    pElements: TmmGroupBox;
    bbAddTerm: TmmSpeedButton;
    bbSubTerm: TmmSpeedButton;
    lbElement: TmmListBox;
    mmGroupBox1: TmmGroupBox;
    bbAdd: TmmSpeedButton;
    bbSub: TmmSpeedButton;
    procedure bbAddClick(Sender: TObject);
    procedure bbUndoClick(Sender: TObject);
    procedure bbSubClick(Sender: TObject);
    procedure lbElementClick(Sender: TObject);
    procedure edFormulaChange(Sender: TObject);
    procedure edFormulaKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure bbAddTermClick(Sender: TObject);
    procedure bbSubTermClick(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure lbElementDblClick(Sender: TObject);
  private
    { Private declarations }
    mData : TMatrixDef;

    procedure FillBasisElemntList;
    function GetData: TMatrixDef;
  protected
    procedure SetData(const Value: TMatrixDef); //virtual;
    procedure Undo;
    procedure AddItem;
    procedure AddOp(aOP:Char);
    procedure CheckLastOp;
  public
    { Public declarations }
    property Data: TMatrixDef read GetData write SetData;
    procedure Save(var aMatrixDef :TMatrixDef); //override;
    procedure Reset;
    constructor Create(AOwner: TComponent); override;
  end;

resourcestring


    rsRemark0  = '(*)Datenelement aus Liste "%s" auswaehlen.'; //ivlm
    rsRemark1  = '(*)Element zur Formel mit [Hinzufuegen] hinzufuegen.'; //ivlm
    rsRemark2  = '(*)Wahl des gewuenschten Operators [+] oder [-] fuer das naechste Datenelement.'; //ivlm
    rsRemark3  = '(*)Mit der Schaltflaeche [Entfernen] kann das zuletzt gewaehlte Datenelement entfernt werden.'; //ivlm
    rsLabelElement = '(23)Verfuegbare Datenelemente'; //ivlm
implementation

{$R *.DFM}
uses QualityMatrixDef, LabMasterDef;

{ TComposite }

//------------------------------------------------------------------------------
procedure TComposite.Save(var aMatrixDef: TMatrixDef);
var xData: TMatrixDef;
    xText :string;
    x : integer;
    xOk : Boolean;
begin

 if assigned(aMatrixDef) then begin

    if aMatrixDef.MatrixDef <> edFormula.Text then begin
       aMatrixDef.Modified  := TRUE;
    end;

    xText := edFormula.Text;

    xOk := FALSE;

    x:= length(xText);
    if x>0 then
        while (not xOk) or (x <= 0) do begin
           if (xText[x] = '+') or (xText[x] = '-')  then begin
              delete(xText, x-1 , length(xText) );
              xOk := TRUE;
              x:= length(xText);
           end else begin
              if xText[x] <> ' ' then
                 xOk := TRUE;
              dec(x);
           end
        end;

    xText := Trim(xText);
    aMatrixDef.Formula := xText;

    //In DB speichern
    aMatrixDef.Save(NIL);
    aMatrixDef.Modified := FALSE;

 end;// if assigned (xData) then begin
end;
//------------------------------------------------------------------------------
procedure TComposite.SetData(const Value: TMatrixDef);
var
  xData: TMatrixDef;
  xCompName, xCategory :string;
  xStatus : Boolean;
begin

  mData := Value;

  xCompName := '';
  xCategory := '';
  xStatus := FALSE;
{
  if assigned(DataItemTree1.GetNodeDataItem(Node)) then
     if DataItemTree1.GetNodeDataItem(Node) is TMatrixDef then begin
        xData := TMatrixDef(DataItemTree1.GetNodeDataItem(Node));
}
     if assigned(mData) then begin
        if mData.MatrixType = mtNone then begin
           edFormula.Text := mData.Formula;
           xCompName := Format('%s = ',[mData.ItemName]);
           //xCategory := Format('Category : %s',[mData.Category]);
           xStatus := TRUE;
        end;
     end;

  lbCompName.Caption := xCompName;

  CheckLastOp; 
end;
//------------------------------------------------------------------------------
procedure TComposite.bbAddClick(Sender: TObject);
begin
  AddOp('+');
end;
//------------------------------------------------------------------------------


procedure TComposite.FillBasisElemntList;
var x: integer;
begin

 for x:= low(cDataItemArray) to high(cDataItemArray) do begin
     if cDataItemArray[x].CD = TRUE then begin 
        if cDataItemArray[x].DisplayName <> '' then
           lbElement.Items.Add( Translate( cDataItemArray[x].DisplayName) )
        else
           lbElement.Items.Add(Translate( cDataItemArray[x].Field) );
     end;
 end;

end;
//------------------------------------------------------------------------------
function TComposite.GetData: TMatrixDef;
begin
  result := mData;
end;
//------------------------------------------------------------------------------
procedure TComposite.Reset;
begin
  edFormula.Text:= '';
end;
//------------------------------------------------------------------------------
constructor TComposite.Create(AOwner: TComponent);
var xText : String;
begin
  inherited;
  FillBasisElemntList;
  lbElement.ItemIndex := 0;


  xText := Format( rsRemark0,[rsLabelElement] );

  lbHinweis.Caption :=  xText + #10#13 + #10#13 + #10#13 +
                        rsRemark1 + #10#13 +#10#13 + #10#13 +
                        rsRemark2 + #10#13 + #10#13 + #10#13 +
                        rsRemark3;

end;
//------------------------------------------------------------------------------
procedure TComposite.bbUndoClick(Sender: TObject);
begin
  Undo;
end;
//------------------------------------------------------------------------------
procedure TComposite.Undo;
var xText :string;
    x : integer;
    xLastOp : Boolean;
begin
  xText := edFormula.Text;

  if xText = '' then exit;

  for x:= Length(xText) downto 1 do begin
      case xText[x] of
         '+', '-' : break;
      end;
  end;

  if x = 0 then
     //1. Element loeschen
     xText := ''
  else
    delete(xText, x-1, length(xText) );

  Trim(xText);
  edFormula.Text := xText;
  edFormula.SelStart := length(edFormula.Text);

  CheckLastOp;

  mData.Modified := TRUE;
end;
//------------------------------------------------------------------------------
procedure TComposite.bbSubClick(Sender: TObject);
begin
  AddOp('-');
end;
//------------------------------------------------------------------------------
procedure TComposite.lbElementClick(Sender: TObject);
begin
  bbAddTerm.Enabled := TRUE;
  bbAdd.Enabled := not bbAddTerm.Enabled;
  bbSub.Enabled := not bbAddTerm.Enabled;
end;
//------------------------------------------------------------------------------
procedure TComposite.edFormulaChange(Sender: TObject);
begin
  edFormula.Text:= StringReplace(edFormula.Text, '  ', ' ' ,[rfReplaceAll] );
  edFormula.SelStart := length(edFormula.Text);
end;
//------------------------------------------------------------------------------
procedure TComposite.edFormulaKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_Back then
     Undo;
end;
//------------------------------------------------------------------------------
procedure TComposite.AddItem;
var xFormula : String;
    xNewTerm, xTxt :String;
    x:integer;
begin
  xFormula :=  edFormula.text;
  xFormula := StringReplace(xFormula, 'Formel', '' ,[rfReplaceAll] );

  xNewTerm :=  lbElement.Items.Strings[ lbElement.itemindex ];

  if xFormula <> '' then
     edFormula.text := format('%s %s',[xFormula, xNewTerm])
   else
     edFormula.text := format('%s',[xNewTerm]);

  mData.Modified := TRUE;
//  mData.Formula :=


  lbElement.Enabled := FALSE;
  bbAddTerm.Enabled := FALSE;
  bbAdd.Enabled := TRUE;
  bbSub.Enabled := TRUE;
end;
//------------------------------------------------------------------------------
procedure TComposite.AddOp(aOP:Char);
begin
 edFormula.text := format('%s %s',[edFormula.text, aOP]);

 lbElement.Enabled := TRUE;
 bbAddTerm.Enabled := FALSE;
 bbAdd.Enabled := FALSE;
 bbSub.Enabled := FALSE;

 mData.Modified := TRUE;
end;
//------------------------------------------------------------------------------
procedure TComposite.bbAddTermClick(Sender: TObject);
begin
 AddItem;
end;
//------------------------------------------------------------------------------
procedure TComposite.bbSubTermClick(Sender: TObject);
begin
  Undo;
end;
//------------------------------------------------------------------------------
procedure TComposite.CheckLastOp;
var xText : String;
    xLastOp : Boolean;
    x : integer;
begin
  xText :=  edFormula.text ;

  if xText = '' then begin
     lbElement.Enabled := TRUE;
     bbAddTerm.Enabled := FALSE;
     bbAdd.Enabled := FALSE;
     bbSub.Enabled := FALSE;
     edFormula.SelStart := 0;
  end else begin
     x:= Length(xText);
     xLastOp := FALSE;
     for x:= Length(xText)-2 to Length(xText) do
       case xText[x] of
         '+', '-' : xLastOp:= TRUE;
       end;

     if not xLastOp then begin
        lbElement.Enabled := FALSE;
        bbAddTerm.Enabled := FALSE;
        bbAdd.Enabled := TRUE;
        bbSub.Enabled := TRUE;
     end else begin
        lbElement.Enabled := TRUE;
        bbAddTerm.Enabled := FALSE;
        bbAdd.Enabled := FALSE;
        bbSub.Enabled := FALSE;
     end;
  end;
end;
//------------------------------------------------------------------------------
procedure TComposite.FrameResize(Sender: TObject);
begin


  pElements.Height := Height - pElements.Top - 20;
  lbElement.Height := pElements.Height - lbElement.Top - 20;

  Panel2.Height := Height - Panel2.Top - 20;
  lbHinweis.Height :=Panel2.Height - lbHinweis.Top - 20;

end;

procedure TComposite.lbElementDblClick(Sender: TObject);
begin
  AddItem;
end;

end.


