(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_PrintOut.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Printout
| Info..........: -
| Develop.system: W2k
| Target.system.: W2k, XP
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 17.07.2003  1.00  Sdo | Projekt erstellt
| 09.06.2004  1.01  Sdo | Neue Func.  UsePrinterIndex() & GetPaperSize aus mmQuickRep implementiert
| 06.09.2004  1.01  Sdo | qlTitle Font Size = 14 & Bold gesetzt
| 21.01.2005  1.05  Sdo | Anpassung an XML-Matrix  -> QuickRepNeedData()
| 16.06.2005  1.05  SDo | Anpassung  Printout Matrix, Siro wird als ZenitF (Bright/Dark) angezeigt
|=============================================================================*)
unit u_PrintOut;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, YMQRUnit, mmQRLabel,

  u_EditorClasses, mmQRExprMemo, mmQRStringsBand, mmQRSysData, mmQRBand,
  mmQRGroup, mmQRSubDetail, mmQRShape, mmQRImage, IvDictio, IvMulti,
  IvEMulti, mmTranslator,
  QRPrntr;


type

  TPrintOutRep = class(TQuickRep)
    TitleBand1: TQRBand;
    qlTitle: TmmQRLabel;
    PageHeaderBand1: TQRBand;
    DetailBand: TQRBand;
    qrlRoot: TQRLabel;
    qrlClassname: TmmQRLabel;
    SubDetailMatrix: TmmQRSubDetail;
    qrRemarkLabel: TmmQRLabel;
    meRemarks: TmmQRExprMemo;
    SubDetailComposite: TmmQRSubDetail;
    qrlRemarkComp: TmmQRLabel;
    meRemarks1: TmmQRExprMemo;
    qrlFormula: TmmQRLabel;
    ChildBand3: TQRChildBand;
    ChildBand2: TQRChildBand;
    ChildBand1: TQRChildBand;
    qrlFormulaValue: TmmQRLabel;
    mmTranslator1: TmmTranslator;
    qlCompanyName: TmmQRLabel;
    mmQRImage1: TmmQRImage;
    qlPageValue: TmmQRSysData;
    mmQRSysData1: TmmQRSysData;
    DetailShape: TmmQRShape;
    qM: TQRQMatrix;


    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure DetailBandBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure PageHeaderBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepEndPage(Sender: TCustomQuickRep);
    procedure SubDetailMatrixBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure SubDetailCompositeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
     mPrintList: TPrintList;
     mCounter, xMaxItems : Integer;
     mActualPage : integer;
     mMaxPages : Integer;
     mMaxPageCounted: Boolean;
     //mItemPerPage : integer;

     mClassFieldList : TStringList;


  public
     constructor create(aOwner: TComponent; aPrintList :TPrintList); overload;
     procedure UsePrinterIndex(aIndex: Integer);
     destructor Destroy; override;
  end;


  function GetPaperSize: TQRPaperSize;

var
  PrintOutRep: TPrintOutRep;


resourcestring

    rsPage      = '(*)Seite'; //ivlm
    rsNoRemark  = '(*)keine'; //ivlm
    rsNoFormula = '(*)keine Definition'; //ivlm
    rsDate      = '(*)Datum'; //ivlm


implementation

{$R *.DFM}
uses QualityMatrixDef, YMParaDef, SettingsReader, MMQualityMatrixClass, Printers;



//------------------------------------------------------------------------------
function GetPaperSize: TQRPaperSize;
var
  xDevice: Array[0..255] of Char;
  xDriver: Array[0..255] of Char;
  xPort: Array[0..255] of Char;
  xDeviceMode: THandle;
  xDevMode: PDevMode;
  //.................................................................
  procedure GPaperSize;
  var
    xPS: TQRPaperSize;
  begin
    Result := Default;
    if (xDevMode^.dmFields and dm_papersize) = dm_papersize then begin
      for xPS:=Default to Custom do begin
        if xDevMode^.dmPaperSize = cQRPaperTranslate[xPS] then begin
          Result := xPS;
          Exit;
        end
      end
    end
  end;
  //.................................................................
begin
  Result := Default;
  Printer.GetPrinter(xDevice, xDriver, xPort, xDeviceMode);
  if xDeviceMode = 0 then
    Printer.GetPrinter(xDevice, xDriver, xPort, xDeviceMode);

  if xDeviceMode <> 0 then
  try
    xDevMode := GlobalLock(xDeviceMode);
    GPaperSize;
  finally
    GlobalUnlock(xDeviceMode);
  end;
end;
//------------------------------------------------------------------------------












{ TPrintOutRep }

constructor TPrintOutRep.create(aOwner: TComponent; aPrintList: TPrintList);

begin
  inherited create(aOwner);
  mPrintList := aPrintList;

  mCounter  := -1;
  mMaxPages := 0;

  mMaxPageCounted := FALSE;

  xMaxItems := mPrintList.Count;

  mClassFieldList := TStringList.Create;
  mClassFieldList.Sorted := TRUE;
  mClassFieldList.Duplicates := dupIgnore;

end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin

 mCounter  := -1;
 mActualPage := 1;

 if mMaxPages >= 1 then
    mMaxPageCounted := TRUE
 else
    mMaxPages := 0;

// qrlDate.Caption := Format('%s:', [rsDate] );
// qrlPage.Caption := Format('%s:', [rsPage] );

 DetailShape.Left   := 0;
 DetailShape.Width  := DetailBand.Width;
 DetailShape.Top    := 15;
 DetailShape.Height := 2;
 DetailShape.Top    := 18;

 qlPageValue.Text := Translate( qlPageValue.Text ) + ' ';

 qrlRoot.Left := (DetailBand.Width DIV 2) + 30;

 qrRemarkLabel.Left := qrlRoot.Left;
 qrlRemarkComp.Left := qrlRoot.Left;

 meRemarks.Left  := qrRemarkLabel.Left + qrRemarkLabel.Width + 15;
 meRemarks1.Left := qrlRemarkComp.Left + qrlRemarkComp.Width + 15;

 qrlFormula.Left := qM.Left + 3;

 try
   qlCompanyName.Caption := TMMSettingsReader.Instance.Value[cCompanyName];
 except
   qlCompanyName.Caption :='';
 end;

end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
var xText, xRoot, xSubTree, xDisplName, xFormula, xDescription : String;
    x, xFieldNr: Integer;

  xSiroClearSettingsArr  : TSiroClearSettingsArr;
  xClassClearSettingsArr : TClassClearSettingsArr;

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  function GetSiroArray(aSettings: TClassClearSettingsArr): TSiroClearSettingsArr;
  var
    i: integer;
  begin
    assert(High(TSiroClearSettingsArr) <= High(TClassClearSettingsArr));
    for i := 0 to High(TSiroClearSettingsArr) do
        Result[i] := aSettings[i];
  end;
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


begin

  inc(mCounter);

  MoreData := (mCounter < xMaxItems);

  if MoreData then begin

     xRoot        := mPrintList.PrintItems[mCounter].PrintOutInfo.Root;
     xSubTree     := mPrintList.PrintItems[mCounter].PrintOutInfo.SubTree;
     xDisplName   := mPrintList.PrintItems[mCounter].DisplayName;
     xDescription := mPrintList.PrintItems[mCounter].Description;
     xFormula     := mPrintList.PrintItems[mCounter].Formula ;


     if xSubTree = '' then xSubTree := ' - ';
    // if xDescription = '' then xDescription := rsNoRemark;
     if xFormula = '' then xFormula := rsNoFormula;

     qrlClassname.caption := xDisplName;

     qrlRoot.caption := Format ('%s / %s',[xRoot, xSubTree]);

     if mPrintList.PrintItems[mCounter].MatrixType = mtNone then begin
        //Composite Daten
        DetailBand.LinkBand := SubDetailComposite;
        qrlFormula.caption := Format('%s = ',[xDisplName] );
        qrlFormulaValue.caption := xFormula;

        qrlFormulaValue.Left := qrlFormula.Left + qrlFormula.Width;
        qrlFormulaValue.Width := qrlRemarkComp.Left - qrlFormulaValue.Left - 5;

        meRemarks1.Lines.Clear;
        meRemarks.Lines.Add(xDescription);
     end else begin
        //Matrix Daten


        DetailBand.LinkBand := SubDetailMatrix;

        meRemarks.Lines.Clear;
        meRemarks.Lines.Add(xDescription);

        qM.MatrixType := mPrintList.PrintItems[mCounter].MatrixType; //17.01.2005 SDO@

        // Beim Umschalten auf Splice wird diese Eigenschaft automatisch True
        qM.Splicevisible := false;
        qM.ActiveVisible := true;
        qM.ChannelVisible := false;

        if (qM.MatrixType = mtShortLongThin) or (qM.MatrixType = mtSiro) then begin
           qM.QMatrix.MatrixSubType := mstSiroF;
           if qM.MatrixType = mtSiro then begin
              qm.QMatrix.MatrixSubType := mstZenitF;
              qm.QMatrix.FModeBD := TRUE;
           end;

           qM.QMatrix.ShowYarnFaultClass := TRUE;
           qM.QMatrix.ShowGroupSubFieldNr := TRUE;
        end;

        qm.QMatrix.CalculateMatrixSize;

        mClassFieldList.Clear;
        mClassFieldList.CommaText := mPrintList.PrintItems[mCounter].MatrixDef;
        qM.QMatrix.ClearValues;

        //Feldmarkierungen loeschen
        for x:=0 to qm.QMatrix.TotValues-1 do
            qm.QMatrix.FieldState[x]:= FALSE;   

        //Cut-Felder auslesen un in QMatrix schreiben
        for x:= 0 to  mClassFieldList.Count-1 do begin
            try
              xFieldNr := StrToInt(mClassFieldList.Strings[x]) -1 ;
              qM.QMatrix.FieldState[xFieldNr]:= TRUE;
            finally
            end;
        end;
        qM.Invalidate;

        SubDetailMatrix.Height :=  qM.Height + 20;
     end;
  end;
end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.DetailBandBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  PrintBand := (mCounter < xMaxItems);
end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.ChildBandBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
   PrintBand := (mCounter < xMaxItems);
   if not PrintBand then exit;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Footer preparieren
//******************************************************************************
procedure TPrintOutRep.PageHeaderBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var x, n : Integer;
begin
{
  x:= PageHeaderBand1.Width Div 2;
  n:= (qrlDate.Width + qrlPage.Width + 5) DIV 2;

  //Date: (Mitte)
  qrlDate.Left := x - n;
  qrDate.Left  := qrlDate.Left +  qrlDate.Width + 5;

  //Pages: (Rechts)
  qrlPage.caption := Format('%s: %d / %d', [rsPage, mActualPage, mMaxPages] );
  qrlPage.Left  :=  PageHeaderBand1.Width - qrlPage.Width - 5;
}
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Max Pages ermitteln
//******************************************************************************
procedure TPrintOutRep.QuickRepEndPage(Sender: TCustomQuickRep);
begin
  inc(mActualPage);
  if not mMaxPageCounted then
     inc(mMaxPages);
end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.SubDetailMatrixBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if (mCounter < xMaxItems) then
     if mPrintList.PrintItems[mCounter].MatrixType = mtNone then
       PrintBand := FALSE
     else begin
       qM.Invalidate;
       PrintBand := TRUE;
     end;
end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.SubDetailCompositeBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if (mCounter < xMaxItems) then
     if mPrintList.PrintItems[mCounter].MatrixType <> mtNone then
        PrintBand := FALSE
     else
        PrintBand := TRUE;
end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
 {
  PrintBand := (mCounter < xMaxItems);
  if not PrintBand then exit;

//  if (mCounter < xMaxItems) then
     PrintBand := (mPrintList.PrintItems[mCounter].MatrixType <> mtNone);

 }

  PrintBand := (mPrintList.PrintItems[mCounter].MatrixType <> mtNone);
end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.ChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin

  PrintBand := (mPrintList.PrintItems[mCounter].MatrixType = mtNone);
{
  PrintBand := (mCounter < xMaxItems);
  if not PrintBand then exit;

//  if (mCounter < xMaxItems) then
      PrintBand := (mPrintList.PrintItems[mCounter].MatrixType = mtNone);
 }
end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
   PrintBand := (mCounter < xMaxItems);
   if not PrintBand then exit;
end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.UsePrinterIndex(aIndex: Integer);
begin
  PrinterSettings.PrinterIndex := aIndex;
  Page.Orientation := Printers.Printer.Orientation;
  Page.PaperSize   := GetPaperSize;
end;
//------------------------------------------------------------------------------
destructor TPrintOutRep.Destroy;
begin
  inherited;
  mClassFieldList.Free;
end;
//------------------------------------------------------------------------------
end.
