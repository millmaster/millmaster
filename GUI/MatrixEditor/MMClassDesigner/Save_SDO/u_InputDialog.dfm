object InputDialog: TInputDialog
  Left = 570
  Top = 345
  BorderStyle = bsDialog
  Caption = '(*)Daten definieren'
  ClientHeight = 264
  ClientWidth = 367
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Shell Dlg 2'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbRemarks: TmmLabel
    Left = 20
    Top = 155
    Width = 67
    Height = 13
    Caption = '(*)Bemerkung'
    FocusControl = meDescription
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object lbClass: TmmLabel
    Left = 20
    Top = 110
    Width = 76
    Height = 13
    Caption = '(*)Klassenname'
    FocusControl = edName
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object lbGroup: TmmLabel
    Left = 20
    Top = 65
    Width = 81
    Height = 13
    Caption = '(*)Gruppenname'
    FocusControl = cbGroups
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object edName: TmmEdit
    Left = 20
    Top = 125
    Width = 330
    Height = 21
    Color = clWindow
    MaxLength = 50
    TabOrder = 0
    Visible = True
    AutoLabel.Control = lbClass
    AutoLabel.LabelPosition = lpTop
    NumMode = False
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object meDescription: TmmMemo
    Left = 20
    Top = 170
    Width = 330
    Height = 57
    MaxLength = 250
    ScrollBars = ssVertical
    TabOrder = 1
    Visible = True
    AutoLabel.Control = lbRemarks
    AutoLabel.LabelPosition = lpTop
  end
  object rgItemType: TmmRadioGroup
    Left = 20
    Top = 3
    Width = 330
    Height = 57
    Caption = '(*)Typ'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      '(*)Garn Klassierung'
      '(*)Fremdfaser Klassierung'
      '(*)Spleiss Klassierung'
      '(*)Kalkulation')
    TabOrder = 2
    CaptionSpace = True
  end
  object cbGroups: TmmComboBox
    Left = 20
    Top = 80
    Width = 330
    Height = 21
    Color = clWindow
    ItemHeight = 13
    MaxLength = 50
    TabOrder = 3
    Visible = True
    OnDrawItem = cbGroupsDrawItem
    AutoLabel.Control = lbGroup
    AutoLabel.LabelPosition = lpTop
    Edit = True
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object Panel2: TPanel
    Left = 0
    Top = 234
    Width = 367
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 4
    object bbCancel: TBitBtn
      Tag = 74
      Left = 165
      Top = 2
      Width = 90
      Height = 25
      Cancel = True
      Caption = '(10)&Abbrechen'
      ModalResult = 2
      TabOrder = 1
      OnClick = bbCancelClick
      NumGlyphs = 2
    end
    object bbOk: TBitBtn
      Tag = 100
      Left = 70
      Top = 2
      Width = 90
      Height = 25
      Caption = '(12)&OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = bbCancelClick
      NumGlyphs = 2
    end
    object bbHelp: TBitBtn
      Left = 260
      Top = 2
      Width = 90
      Height = 25
      Caption = '(9)&Hilfe'
      TabOrder = 2
      OnClick = bbHelpClick
      NumGlyphs = 2
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 8
    Top = 232
    TargetsData = (
      1
      5
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        'TmmMemo'
        'Text'
        0)
      (
        '*'
        'Lines'
        0)
      (
        '*'
        'Items'
        0))
  end
end
