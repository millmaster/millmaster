unit ClassDesigner_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88.1.0.1.0  $
// File generated on 22.07.2003 10:29:08 from Type Library described below.

// ************************************************************************ //
// Type Lib: W:\MillMaster\GUI\MatrixEditor\MMClassDesigner\ClassDesigner.tlb (1)
// IID\LCID: {A165E453-E455-4074-98A4-909BEC07EC0B}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\System32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ClassDesignerMajorVersion = 1;
  ClassDesignerMinorVersion = 0;

  LIBID_ClassDesigner: TGUID = '{A165E453-E455-4074-98A4-909BEC07EC0B}';

  IID_IClassDesignerServer: TGUID = '{813C632B-A23E-42C8-A3C1-0E6987ED19DA}';
  CLASS_ClassDesignerServer: TGUID = '{91896E21-5615-445F-BBA5-C531E54D8D46}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IClassDesignerServer = interface;
  IClassDesignerServerDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  ClassDesignerServer = IClassDesignerServer;


// *********************************************************************//
// Interface: IClassDesignerServer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {813C632B-A23E-42C8-A3C1-0E6987ED19DA}
// *********************************************************************//
  IClassDesignerServer = interface(IDispatch)
    ['{813C632B-A23E-42C8-A3C1-0E6987ED19DA}']
    procedure Execute; safecall;
  end;

// *********************************************************************//
// DispIntf:  IClassDesignerServerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {813C632B-A23E-42C8-A3C1-0E6987ED19DA}
// *********************************************************************//
  IClassDesignerServerDisp = dispinterface
    ['{813C632B-A23E-42C8-A3C1-0E6987ED19DA}']
    procedure Execute; dispid 1;
  end;

// *********************************************************************//
// The Class CoClassDesignerServer provides a Create and CreateRemote method to          
// create instances of the default interface IClassDesignerServer exposed by              
// the CoClass ClassDesignerServer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoClassDesignerServer = class
    class function Create: IClassDesignerServer;
    class function CreateRemote(const MachineName: string): IClassDesignerServer;
  end;

implementation

uses ComObj;

class function CoClassDesignerServer.Create: IClassDesignerServer;
begin
  Result := CreateComObject(CLASS_ClassDesignerServer) as IClassDesignerServer;
end;

class function CoClassDesignerServer.CreateRemote(const MachineName: string): IClassDesignerServer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_ClassDesignerServer) as IClassDesignerServer;
end;

end.
