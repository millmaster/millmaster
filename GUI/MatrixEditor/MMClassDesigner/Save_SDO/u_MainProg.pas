(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_MainProg.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Container fuer ClassDesigner.exe
| Info..........: -
| Develop.system: W2k
| Target.system.: W2k, XP
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 03.07.2003  1.00  Sdo | Projekt erstellt
|=============================================================================*)
unit u_MainProg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ToolWin, u_Editor, IvDictio, IvMulti, IvEMulti, mmTranslator,
  IvAMulti, IvBinDic, mmDictionary, MMHtmlHelp;


type
  TfrmMain = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    mmTranslator1: TmmTranslator;
    mmDictionary1: TmmDictionary;
    MMHtmlHelp1: TMMHtmlHelp;
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  public
  end;

var
  frmMain: TfrmMain;

const
  cEditorWidth = 700;
  cEditorHight = 600;


implementation

uses ComObj;

{$R *.DFM}
//------------------------------------------------------------------------------
procedure TfrmMain.ToolButton1Click(Sender: TObject);
begin
  try
    screen.Cursor :=  crHourGlass;
    with TMatrixEditor.Create(NIL) do
    try
      ShowModal;
    finally
      Free;
    end;
  except
    on e:Exception do begin
      screen.Cursor :=  crDefault;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.ToolButton2Click(Sender: TObject);
begin
  //MatrixEditor.Free;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormCreate(Sender: TObject);
begin
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormShow(Sender: TObject);
var
  xMsg: String;
begin
  if (DebugHook <> 0) then begin
    //Nur fuer DebugMode
    try
      screen.Cursor :=  crHourGlass;

      with TMatrixEditor.Create(NIL) do
      try
        ShowModal;
      finally
        Free;
      end;
    except
      on e:Exception do begin
        xMSG := Format('Error in "%s".' + #10#13 + 'Error msg: %s',[Application.ExeName, e.Message] );
        MessageDlg(xMSG, mtError, [mbOk], 0 );
      end;
    end;
    Screen.Cursor :=  crDefault;
  end;
end;
//------------------------------------------------------------------------------
end.
