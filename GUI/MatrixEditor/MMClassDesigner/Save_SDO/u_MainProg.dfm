object frmMain: TfrmMain
  Left = 497
  Top = 306
  BorderStyle = bsToolWindow
  Caption = '(*)MillMaster'
  ClientHeight = 318
  ClientWidth = 486
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 486
    Height = 29
    ButtonHeight = 21
    ButtonWidth = 63
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 2
      Caption = 'Open Editor'
      ImageIndex = 0
      OnClick = ToolButton1Click
    end
    object ToolButton2: TToolButton
      Left = 63
      Top = 2
      Caption = 'Close Editor'
      ImageIndex = 1
      Visible = False
      OnClick = ToolButton2Click
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 40
    Top = 32
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
  object mmDictionary1: TmmDictionary
    DictionaryName = 'Dictionary1'
    AutoConfig = True
    Left = 6
    Top = 29
  end
  object MMHtmlHelp1: TMMHtmlHelp
    AutoConfig = True
    MMHelp = mmpEasy
    Left = 136
    Top = 40
  end
end
