program ClassDesigner;

uses
  mmCS,
  Forms,
  LoepfeGlobal,
  SettingsReader,
  u_MainProg in 'u_MainProg.pas' {frmMain},
  u_Editor in 'u_Editor.pas' {MatrixEditor},
  u_EditorClasses in 'u_EditorClasses.pas',
  u_InputDialog in 'u_InputDialog.pas' {InputDialog},
  u_Matrix in 'u_Matrix.pas' {Matrix: TFrame},
  u_Composite in 'u_Composite.pas' {Composite: TFrame},
  u_PrintOut in 'u_PrintOut.pas' {PrintOutRep: TQuickRep},
  ClassDesigner_TLB in 'ClassDesigner_TLB.pas',
  ClassDesignerIMPL in 'ClassDesignerIMPL.pas' {ClassDesignerServer: CoClass};

{$R *.TLB}

{$R *.RES}
{$R Version.res}

begin
  gApplicationName := 'ClassDesigner';
  CodeSite.Enabled := CodeSiteEnabled;
  gMinDBVersion    := '4.01.00';
//  with TMMSettingsReader.Instance do begin
//    if not DBVersionOK(gMinDBVersion, True) then
//      Exit;
//  end;

  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
