object Matrix: TMatrix
  Left = 0
  Top = 0
  Width = 443
  Height = 277
  Align = alClient
  TabOrder = 0
  object SizingPanel1: TSizingPanel
    Left = 0
    Top = 0
    Width = 443
    Height = 277
    Align = alClient
    BevelOuter = bvNone
    Control = qm
    object qm: TMMQualityMatrix
      Left = 0
      Top = 0
      Width = 360
      Height = 277
      Color = clWhite
      LastCutMode = lcNone
      LastCutColor = clLime
      LastCutField = 0
      MatrixType = mtShortLongThin
      MatrixSubType = mstNone
      ActiveColor = clGreen
      ActiveVisible = True
      ChannelColor = clRed
      ChannelStyle = psSolid
      ChannelVisible = True
      ClusterColor = clPurple
      ClusterStyle = psSolid
      ClusterVisible = False
      CutsColor = clRed
      DefectsColor = clBlack
      DisableMessage = 'Quality Matrix disabled'
      DisplayMode = dmValues
      DotsColor = clBlack
      Enabled = True
      InactiveColor = 9502719
      MatrixMode = mmDisplayData
      OnFieldClick = qmFieldClick
      SpliceColor = clBlue
      SpliceStyle = psSolid
      SpliceVisible = False
      SubFieldX = 0
      SubFieldY = 0
      ZeroLimit = 0.01
      FontColorGroupSubField = clRed
      QMatrixType = mtShortLongThin
      ShowGroupSubFieldNr = True
      ShowYarnFaultClass = True
      Version = 'MillMaster MMQMatrix Vers. 1.0'
      YarnFaultClassFont.Charset = DEFAULT_CHARSET
      YarnFaultClassFont.Color = clBlack
      YarnFaultClassFont.Height = -13
      YarnFaultClassFont.Name = 'Arial'
      YarnFaultClassFont.Style = []
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 392
    Top = 109
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
end
