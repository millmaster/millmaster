(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: ClassDesignerIMPL.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: ClassDesigner Interface
| Info..........: -
| Develop.system: W2k
| Target.system.: W2k, XP
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 22.07.2003  1.00  Sdo | Projekt erstellt
| 27.11.2003  1.00  Wss | Erweiterung f�r die Integration in der Floor
|=============================================================================*)
unit ClassDesignerIMPL;

interface

uses
  ComObj, ActiveX, LoepfePluginIMPL, LOEPFELIBRARY_TLB, ClassDesigner_TLB, StdVcl;

type
  TClassDesignerServer = class(TLoepfePlugin, IClassDesignerServer)
  public
    function ExecuteCommand: SYSINT; override;
    procedure Initialize; override;
  protected
    procedure Execute; safecall;
  end;

implementation
{$R Toolbar.res}
uses
  ComServ, Forms, BaseForm, u_Editor;

const
  cCmdShow = 0;

  cFloorMenuDef: Array[0..0] of TFloorMenuRec = (
    // --- Main Menu File ---
    (CommandID: cCmdShow; SubMenu: 0; MenuBmpName: 'CLASSDESIGNER';
     MenuText: '(*)Klassendesigner'; MenuHint:'(*)Klassendesigner'; MenuStBar: '(*)Klassendesigner'; //ivlm
     ToolbarText: 'Klassendesigner';
     MenuGroup: mgMain; MenuTyp: mtEdit; MenuState: msEnabled; MenuAvailable: maAll)
  );

//------------------------------------------------------------------------------
procedure TClassDesignerServer.Execute;
begin
  with TMatrixEditor.Create(NIL) do
  try
    ShowModal;
  finally
    Free;
  end;

end;
//------------------------------------------------------------------------------
function TClassDesignerServer.ExecuteCommand: SYSINT;
begin
  Result := 0;
  case CommandIndex of
    cCmdShow: Execute;
  else
  end;
end;
//------------------------------------------------------------------------------
procedure TClassDesignerServer.Initialize;
begin
  inherited Initialize;
  InitMenu(cFloorMenuDef);
end;
//------------------------------------------------------------------------------

initialization
  InitializeObjectFactory(TClassDesignerServer, Class_ClassDesignerServer, False);
end.
