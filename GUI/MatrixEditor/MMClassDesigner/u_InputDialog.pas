(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_InputDialog.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Dialog fuer Dataitem Gruppe, Klasse, Bemerkung
| Info..........: -
| Develop.system: W2k
| Target.system.: W2k, XP
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 03.07.2003  1.00  Sdo | Projekt erstellt
|=============================================================================*)
unit u_InputDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmStaticText, Buttons, ExtCtrls, mmEdit, mmComboBox, mmLabel,
  mmMemo, mmGroupBox, mmRadioGroup, IvDictio, IvMulti, IvEMulti,
  mmTranslator;

type

  TItemType = (itYarnClass, itFFClass, itSplice, itComposite);
  TEditMode = (emNew, emCopy, emEdit);


  TInputDialog = class(TForm)
    edName: TmmEdit;
    meDescription: TmmMemo;
    lbRemarks: TmmLabel;
    lbClass: TmmLabel;
    lbGroup: TmmLabel;
    rgItemType: TmmRadioGroup;
    cbGroups: TmmComboBox;
    Panel2: TPanel;
    bbCancel: TBitBtn;
    bbOk: TBitBtn;
    mmTranslator1: TmmTranslator;
    bbHelp: TBitBtn;
    procedure bbCancelClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure cbGroupsDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure FormCreate(Sender: TObject);
    procedure bbHelpClick(Sender: TObject);
  private
    FExistsItemNames: String;
    FEditMode : TEditMode;
    mClassNameExists : Boolean;

    function GetClassName: string;
    function GetGroupName: string;
    function GetDescription:String;
    procedure SetClassName(const Value: string);
    procedure SetDescription(const Value: string);
    procedure SetGroupName(const Value: string);
    function GetItemType: TItemType;
    procedure SetItemType(const Value: TItemType);
    procedure SetEditMode(const Value: TEditMode);
    procedure SetExistsItemNames(const Value: String);
    { Private declarations }
  public
    procedure AddGroups(aGroupName: string);
    procedure SelectGroup(aGroupName: string);
    property NewClassName:string read GetClassName write SetClassName;
    property NewGroupName: string read GetGroupName write SetGroupName;
    property NewDescription: string read GetDescription write SetDescription;
    property ItemType: TItemType read GetItemType write SetItemType;
    property EditMode : TEditMode write SetEditMode;
    property ExistsItemNames : String  write SetExistsItemNames;

    { Public declarations }
  end;

var
  InputDialog: TInputDialog;

  const cNewLine = #$D#$A;
        cClassnameLen = 50;
        cCategory     = 50;
        cDescription  = 250;



resourcestring

    rsNewDataItem   = '(*)Neues Datenelement erstellen';  //ivlm
    rsCopyDataItem  = '(*)Datenelement kopieren';    //ivlm
    rsEditDataItem  = '(*)Datenelement aendern';       //ivlm
    rsInputName     = '(*)Geben Sie bitte einen Namen ein!' ;  //ivlm
    rsNoGroup       = '(*)Kein Gruppennamen'; //ivlm
    rsExistsClassName = '(*)Dieser Klassenname existiert bereits!'; //ivlm

    //For_gItemType ='(*)Kalkulation';


implementation

uses MMHTMLHelp;

{$R *.DFM}
//------------------------------------------------------------------------------
procedure TInputDialog.AddGroups(aGroupName: string);
var xGroup : String;
begin
  cbGroups.Items.CommaText :=  aGroupName;
  cbGroups.Sorted:= TRUE;

  xGroup := Format('[ %s ]',[rsNoGroup]);
  cbGroups.Items.Add(xGroup);
  cbGroups.ItemIndex := cbGroups.Items.IndexOf(xGroup);
end;
//------------------------------------------------------------------------------
procedure TInputDialog.bbCancelClick(Sender: TObject);
var xMsg : String;
    xRet : TModalResult;
    xList : TStringList;
begin

  xRet := (Sender as TBitBtn).ModalResult;

  mClassNameExists := FALSE;

  if (xRet = mrOk) or (xRet = mrYes) then begin
     if edName.Text = '' then begin
        xMsg :=rsInputName;
        MessageDlg(xMsg, mtWarning, [mbOK], 0);
        edName.SetFocus;
        exit;
     end;

     if FEditMode <> emEdit then
        if Pos(rsNoGroup, edName.Text) = 0 then begin
           xList := TStringList.Create;
           xList.Sorted := TRUE;
           xList.CommaText :=  FExistsItemNames;
           if xList.IndexOf(edName.Text) >= 0 then begin
//           if Pos(edName.Text, FExistsItemNames) <> 0 then begin
              xMsg :=rsExistsClassName;
              MessageDlg(xMsg, mtWarning, [mbOK], 0);
              edName.SetFocus;
              mClassNameExists := TRUE;
              xList.Free;
              exit;
           end;
           xList.Free;
        end;
  end;


  ModalResult := xRet;
end;
//------------------------------------------------------------------------------
function TInputDialog.GetClassName: string;
begin
  result := edName.Text;
end;
//------------------------------------------------------------------------------
function TInputDialog.GetDescription: String;
var x     : integer;
    xText : String;
begin
   xText := '';
   for x:= 0 to meDescription.Lines.Count-1 do
       if x = 0 then
          xText:= meDescription.Lines.Strings[x]
       else
          xText := xText + cNewLine + meDescription.Lines.Strings[x];  
   result :=    xText;
end;
//------------------------------------------------------------------------------
function TInputDialog.GetGroupName: string;
var xText : String;
begin
  xText := cbGroups.Text;
  if Pos( rsNoGroup, xText) > 0 then xText:=  '';
  result := xText;
end;
//------------------------------------------------------------------------------
function TInputDialog.GetItemType: TItemType;
begin
  result := TItemType( rgItemType.ItemIndex);
end;
//------------------------------------------------------------------------------
procedure TInputDialog.SelectGroup(aGroupName: string);
begin
  cbGroups.Text := aGroupName;
end;
//------------------------------------------------------------------------------
procedure TInputDialog.SetClassName(const Value: string);
begin
 if Value <> '' then edName.Text :=  Value;
end;
//------------------------------------------------------------------------------
procedure TInputDialog.SetDescription(const Value: string);
begin
 if Value <> '' then meDescription.Lines.Add( Value );
end;
//------------------------------------------------------------------------------
procedure TInputDialog.SetEditMode(const Value: TEditMode);
var xText :String;
    xEnabledItemType : Boolean;
begin

  FEditMode := Value;
  case FEditMode of
     emNew    : begin
                  xText := rsNewDataItem;
                  xEnabledItemType := TRUE;
                end;
     emCopy   : begin
                  xText := rsCopyDataItem;
                  xEnabledItemType := FALSE;
                end;
     emEdit   : begin
                  xText := rsEditDataItem;
                  xEnabledItemType := FALSE;
                end;
  end;

  Caption            := xText;
  rgItemType.Enabled := xEnabledItemType
end;
//------------------------------------------------------------------------------
procedure TInputDialog.SetGroupName(const Value: string);
begin
  if Value <> '' then begin
     cbGroups.ItemIndex := cbGroups.Items.IndexOf(Value);
  end;
end;
//------------------------------------------------------------------------------
procedure TInputDialog.SetItemType(const Value: TItemType);
begin
  try
    rgItemType.ItemIndex := Integer( Value );
  except
    rgItemType.ItemIndex := 0;
  end;
end;
//------------------------------------------------------------------------------
procedure TInputDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
   if (ModalResult = mrCancel )then exit;

   if (edName.Text = '') or (mClassNameExists) then begin
      CanClose:= FALSE;
   end;
end;
//------------------------------------------------------------------------------
procedure TInputDialog.cbGroupsDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var xIsSelected: boolean;
    xPos: integer;
begin

  with (Control as TComboBox).Canvas do begin

    xIsSelected := odSelected in state;
    FillRect(Rect);

    if xIsSelected then
      Font.Color := clHighlightText
    else
      Font.Color := clWindowText;

    if Pos(rsNoGroup, cbGroups.Items.Strings[Index] ) > 0 then begin
       //Font.Style:= [fsItalic];
       if xIsSelected then
          Font.Color := $00C8D0D4
       else
          Font.Color := clGray;
    end;

    xPos := clInactiveBorder;
    TextOut( Rect.Left + 3, Rect.Top, cbGroups.Items.Strings[Index] );
  end; //END with

end;
//------------------------------------------------------------------------------
procedure TInputDialog.SetExistsItemNames(const Value: String);
begin
  FExistsItemNames := Value;
end;
//------------------------------------------------------------------------------
procedure TInputDialog.FormCreate(Sender: TObject);
begin

   HelpContext := GetHelpContext('LabMaster\ClassDesigner\CLASS_Neue_Kalkulation.htm');

   //Maximale Eingabelaenge setzen
   cbGroups.MaxLength := cCategory;
   edName.MaxLength := cClassnameLen;
   meDescription.MaxLength := cDescription;
end;
//------------------------------------------------------------------------------
procedure TInputDialog.bbHelpClick(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;

end.
