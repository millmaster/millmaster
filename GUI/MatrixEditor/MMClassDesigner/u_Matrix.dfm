object Matrix: TMatrix
  Left = 0
  Top = 0
  Width = 443
  Height = 277
  Align = alClient
  AutoSize = True
  TabOrder = 0
  object SizingPanel1: TSizingPanel
    Left = 0
    Top = 0
    Width = 443
    Height = 277
    Align = alClient
    BevelOuter = bvNone
    object qm: TMMQualityMatrix
      Left = 0
      Top = 0
      Width = 443
      Height = 246
      Color = clWhite
      LastCutColor = clLime
      LastCutField = 0
      LastCutMode = lcNone
      MatrixSubType = mstSiroF
      MatrixType = mtSiro
      ActiveColor = clGreen
      ActiveVisible = True
      ChannelColor = clSilver
      ChannelStyle = psSolid
      ChannelVisible = False
      ClusterColor = clSilver
      ClusterStyle = psSolid
      ClusterVisible = False
      CutsColor = clRed
      DefectsColor = clBlack
      DisableMessage = 'Quality Matrix disabled'
      DisplayMode = dmValues
      DotsColor = clBlack
      Enabled = True
      InactiveColor = 9502719
      MatrixMode = mmDisplayData
      SpliceColor = clSilver
      SpliceStyle = psSolid
      SpliceVisible = False
      SubFieldX = 0
      SubFieldY = 0
      ZeroLimit = 0.01
      FontColorGroupSubField = clRed
      ShowGroupSubFieldNr = True
      ShowYarnFaultClass = True
      Version = 'MillMaster MMQMatrix Vers. 5.0'
      YarnFaultClassFont.Charset = DEFAULT_CHARSET
      YarnFaultClassFont.Color = clBlack
      YarnFaultClassFont.Height = -13
      YarnFaultClassFont.Name = 'Arial'
      YarnFaultClassFont.Style = []
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 392
    Top = 109
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
end
