(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_Editor.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Class Editor
|                 - Superklassen erstellen
|                 - Composites erstellen (Formeln)
|                 Ben�tigt Untis:
|                 - u_Matrix.pas -> Matrix Panel
                  - u_Composite  -> Composite Panel
                  - u_EditorClasses -> Definition Dekodierer und Dataitem-Liste
| Info..........: -
| Develop.system: W2k
| Target.system.: W2k, XP
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 03.07.2003  1.00  Sdo | Projekt erstellt
| 22.10.2003        lok | Vordefinierte DataItems k�nnen nicht ver�ndert werden, deshalb
|                       |   werden die Buttons f�r Speichern und Editieren disabled (FocusChanged)
| 28.11.2003        SDo | TQualityMatrix durch TMMQualityMatrix (Ableitung) ersetzt -> u_Matrix.pas
| 05.12.2003        SDo | Speicher-DLG aufrufen, wenn Daten ge�ndert wurden und der
|                       | Benutzer Toolbar-Buttons anklickt  -> SaveChangedValues
| 09.06.2006        SDo | Print & Print-Layout angepasst
| 06.09.2004  1.01  Sdo | Print Button aktiv, nur wenn ein Item im DataItemTree selektiert ist.  -> Func. DataItemTreeFocusChanged() acPrint.Enabled := TRUE;
| 21.01.2005  1.02  SDo | Umbau & Anpassungen an XML (Aenderung in Func. NewItem)
| 21.02.2005  1.02  SDo | Datenelement L�sch-Dialog in DeleteItem() eingebaut
| 14.06.2005  1.03  SDo | Neue Func. Is_ClName_In_QOff:
|                       | Es wird �berpr�ft, ob ein Klassen-Name im Q-Offlimit zugewiesen wurde
|                       | => Zugewiesene Klassen k�nnen nicht mutiert werden.
|                       | => Im LabReport werden keine Klassen-Namen mehr angezeigt
| 15.06.2005  1.03  SDo | Aenderung in SaveItemAS(): mtColor in TMatrixType existiert nicht mehr
|                       | -> TItemType Zuweisung mittels case of
|=============================================================================*)
unit u_Editor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DataItemTree, Buttons, ExtCtrls, StdCtrls,

  u_Matrix, u_Composite, u_EditorClasses, ClassDef, VirtualTrees,
  u_InputDialog, ImgList, ComCtrls, ToolWin, fcStatusBar, ActnList,
  mmActionList, IvDictio, IvAMulti, IvBinDic, mmDictionary, IvMulti,
  IvEMulti, mmTranslator, mmImageList, MMSecurity;

type
 
  TMatrixEditor = class(TForm)
    paItemTree: TPanel;
    DataItemTree: TDataItemTree;
    Splitter1: TSplitter;
    ImageList1: TmmImageList;
    ToolBar2: TToolBar;
    tbExit: TToolButton;
    ToolButton2: TToolButton;
    tbNew: TToolButton;
    tbSave: TToolButton;
    tbSaveAS: TToolButton;
    paItemDataField: TPanel;
    Panel1: TPanel;
    mmActionList1: TmmActionList;
    acExit: TAction;
    acNew: TAction;
    acSave: TAction;
    acCopy: TAction;
    mmTranslator1: TmmTranslator;
    acDelete: TAction;
    acPrint: TAction;
    tbEdit: TToolButton;
    tbPrint: TToolButton;
    ToolButton1: TToolButton;
    tbDelete: TToolButton;
    acEdit: TAction;
    tbSecurity: TToolButton;
    acSetSecurity: TAction;
    MMSecurityDB: TMMSecurityDB;
    MMSecurityControl: TMMSecurityControl;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    tbHelp: TToolButton;
    acHelp: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Splitter1CanResize(Sender: TObject; var NewSize: Integer;
      var Accept: Boolean);
    procedure DataItemTreeFocusChanged(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex);
    procedure DataItemTreeFocusChanging(Sender: TBaseVirtualTree; OldNode,
      NewNode: PVirtualNode; OldColumn, NewColumn: TColumnIndex;
      var Allowed: Boolean);
    procedure sbSaveClick(Sender: TObject);
    procedure imTrashDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure imTrashDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure tbSave_Click(Sender: TObject);
    procedure tbNew_Click(Sender: TObject);
    procedure tbSaveAS_Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acNewExecute(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure DataItemTreeKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acDeleteExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acSetSecurityExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
  private
    { Private declarations }
    mMatrix : TMatrix;
    mComposite : TComposite;
    mMatrixContainer : TMatrixContainer;
    mNewItem : Boolean;
    mNode: PVirtualNode;

    mPrintList : TPrintList;

    function InputName(var aClassName:string; var aGroupName: string;
                       var aDescription: string; var aItemType: TItemType;
                       aEditMode : TEditMode): Boolean;

    procedure DeleteItem;
    procedure NewItem;
    procedure SaveItem;
    procedure SaveItemAS;
    procedure SaveChangedValues;
    procedure SetButtonStatus(aEnabled : Boolean);
    procedure PreparePrintList(aNode: PVirtualNode);

    function PrepareSubTree(aNode: PVirtualNode; aPrintRec: TPrintRec):Boolean;
    function ADDItem(aNode : PVirtualNode; aPrintRec: TPrintRec) : Boolean;

    function Is_ClName_In_QOff(aDataItem: TMatrixDef):Boolean;


  public
  end;

var
  MatrixEditor: TMatrixEditor;

//const
 // cEditorWidth = 620;
 // cEditorHight = 500;


const
   cSQL_GetClassName_from_QOfflimit = 'select u.c_ClassName ClassName, a.c_Name AlarmName, a.c_Active Active '+
                                      'from t_UserClass_Def u, t_QO_Item_Setting q, t_QO_Alarm_Setting a ' +
                                      'where u.c_UserClass_ID = %d ' +
                                      'and u.c_ClassName = q.c_Name ' +
                                      'and q.c_QOAlarm_ID = a.c_QOAlarm_ID';

resourcestring

    rsDataItemTree_EigeneKlassen   = '(*)Eigene Klassen';      //ivlm
    rsCategory                     = '(*)Kategorie';           //ivlm
    rsQuestion1                    = '(*)Wollen Sie die neuen "%s" Daten speichern?'; //ivlm
    rsQuestion2                    = '(*)"%s" geaendert! Wollen Sie die Daten speichern?'; //ivlm
    rsDleteItem                    = '(*)Soll %s %s geloescht werden?'; //ivlm
    reItem                         = '(*)Datenelement'; //ivlm
    reActive                       = '(*)Aktiv'; //ivlm
    reInaktiv                      = '(*)Inaktiv'; //ivlm
    reMSGMutation1                 = '(*)"%s" ist dem Alarm "%s" im Q-Offlimit zugeordnet.'; //ivlm
    reMSGMutation2                 = '(*)"%s" kann nicht geloescht oder umbenannt werden.'; //ivlm


implementation

{$R *.DFM}
uses YMParaDef, QualityMatrixDef, mmVirtualStringTree, u_PrintOut, MMHTMLHelp,
     PrintSettingsTemplateForm, mmCS, AdoDbAccess;

//------------------------------------------------------------------------------
procedure TMatrixEditor.FormCreate(Sender: TObject);
begin

  HelpContext := GetHelpContext('LabMaster\ClassDesigner\CLASS_Fenster_Klassendesigner.htm');

  screen.Cursor :=  crHourGlass;

  mMatrix := TMatrix.Create(Self);
  mMatrix.AutoSize := TRUE;
  mMatrix.Hide;

  mComposite := TComposite.Create(self) ;
  mComposite.AutoSize:= TRUE;
  mComposite.Hide;

  mMatrixContainer := TMatrixContainer.Create;
  mPrintList       := TPrintList.Create(FALSE);

  //Klassen von DB holen; Daten werden in die Klasse TMatrixDef gepackt
  //Pro Item eine TMatrixDef Kl.
  mMatrixContainer.LoadFromDatabase(cItemClass);

  // Container zuweisen (wird kopiert)
  DataItemTree.ClassDefList   := mMatrixContainer;
  DataItemTree.ExpandedGroups := [ctCustomDataItem];

  SetButtonStatus(False);

  screen.Cursor :=  crDefault;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.FormDestroy(Sender: TObject);
begin
  mMatrix.Free;
  mComposite.Free;
  mPrintList.Free;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.Splitter1CanResize(Sender: TObject;
  var NewSize: Integer; var Accept: Boolean);
begin
  if NewSize <= 225 then
     Accept:= FALSE
  else
     Accept:= TRUE;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.DataItemTreeFocusChanged(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex);
var
  xData: TMatrixDef;
  xClassClearSettingsArr: TClassClearSettingsArr;
  xSiroClearSettingsArr: TSiroClearSettingsArr;
  xBaseDataItem : TBaseDataItem;
  xText: String;

  xCategory, xItemName :String;

  function GetSiroArray(aSettings: TClassClearSettingsArr): TSiroClearSettingsArr;
  var
    i: integer;
  begin
    assert(High(TSiroClearSettingsArr) <= High(TClassClearSettingsArr));
    for i := 0 to High(TSiroClearSettingsArr) do
      Result[i] := aSettings[i];
  end;// function GetSiroArray(aSettings: TClassClearSettingsArr): TSiroClearSettingsArr;

begin

  //EnterMethod('TMatrixEditor.DataItemTreeFocusChanged()');

  acPrint.Enabled := TRUE;
  mNode := Node;

  FillChar(xClassClearSettingsArr,16,0);

  xData := nil;

  if assigned(DataItemTree.GetNodeDataItem(Node)) then
     if DataItemTree.GetNodeDataItem(Node) is TMatrixDef then begin

        xData := TMatrixDef(DataItemTree.GetNodeDataItem(Node));

        SetButtonStatus(TRUE);

        if xData.MatrixType = mtNone then begin
           //Composite
           mMatrix.Hide;
           mComposite.Parent := paItemDataField;
           mComposite.Data   := xData;
           mComposite.Show;
        end else begin
           //Matrix
           mComposite.Hide;
           mMatrix.Parent := paItemDataField;
           mMatrix.Top    := paItemDataField.Top;
           mMatrix.Data   := xData;
           mMatrix.Show;
        end;

        // ACHTUNG Securrity
        acSave.Enabled := not(xData.Predefined);
        acEdit.Enabled := not(xData.Predefined);
        acDelete.Enabled := not(xData.Predefined);
        tbSave.Enabled := not(xData.Predefined);
        tbEdit.Enabled := not(xData.Predefined);
        tbDelete.Enabled := not(xData.Predefined);

        xCategory:= Format('%s: %s',[rsCategory, xData.Category]);
        xItemName:= Format('%s',[xData.ItemName]);

     end else begin
        mComposite.Hide;
        mMatrix.Hide;
        xBaseDataItem := DataItemTree.GetNodeDataItem(Node);

        xCategory:= Format('%s: %s',[rsCategory, xBaseDataItem.DisplayName]);
        xItemName := '';
        SetButtonStatus(False);
     end;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.DataItemTreeFocusChanging(Sender: TBaseVirtualTree;
  OldNode, NewNode: PVirtualNode; OldColumn, NewColumn: TColumnIndex;
  var Allowed: Boolean);
var
  xData, xData1: TMatrixDef;
  xMsg, xNextItem : String;
  xDataItem: TBaseDataItem;
begin
  xData  := NIL;
  xData1 := NIL;

  if assigned(DataItemTree.GetNodeDataItem(OldNode)) then
     if DataItemTree.GetNodeDataItem(OldNode) is TMatrixDef then
        xData := TMatrixDef(DataItemTree.GetNodeDataItem(OldNode));

  if assigned(DataItemTree.GetNodeDataItem(NewNode)) then
     if DataItemTree.GetNodeDataItem(NewNode) is TMatrixDef then begin
        xData1 := TMatrixDef(DataItemTree.GetNodeDataItem(NewNode));
        xNextItem := xData1.FieldName;
     end;

  if assigned(xData) then begin
    if OldNode <> NewNode then begin
      if xData.Modified then begin
          if mNewItem then
             xMsg := Format(rsQuestion1, [xData.FieldName])
          else
            xMsg := Format(rsQuestion2, [xData.FieldName]);

          if MessageDlg(xMsg, mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin

             Application.ProcessMessages;

             SaveItem;
             Allowed:= FALSE;

             xDataItem := mMatrixContainer.DataItemByName[xNextItem];
             DataItemTree.SelectDataItem(xDataItem, FALSE);

          end else begin
             xData.Modified := FALSE;
             mNewItem := FALSE;
             exit;
          end;
      end;
    end;// if OldNode <> NewNode then begin
  end;// if assigned (xData) then begin
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.sbSaveClick(Sender: TObject);
  var xData: TMatrixDef;
begin
  xData := NIL;
  xData := DataItemTree.SelectedDataItem as TMatrixDef;

  if assigned(xData) then begin
     if xData.MatrixType <> mtNone then
        mMatrix.Save(xData);

     if xData.MatrixType = mtNone then
        mComposite.Save(xData);

     DataItemTree.ClassDefList := mMatrixContainer;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.imTrashDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
var xTxt: String;
begin
  xTxt:= Source.ClassParent.ClassName;
  xTxt:= Sender.ClassName;

  Accept := (Source.ClassName = 'TmmVirtualStringTree');

end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.imTrashDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
  if  (Source.ClassName = 'TmmVirtualStringTree') then begin
      DeleteItem;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.DeleteItem;
var xData     : TMatrixDef;
    xText : String;
begin

  xData := NIL;
  xData := DataItemTree.SelectedDataItem as TMatrixDef;

  if Is_ClName_In_QOff(xData) then exit;
  
  if Assigned (xData) then begin
     xText := Format(rsDleteItem, [ reItem, xData.FieldName  ] );
     if MessageDlg(xText, mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
        mMatrixContainer.DeleteData(xData);

        // Container zuweisen (wird kopiert)
        DataItemTree.Refresh;

        DataItemTree.SelectNode(DataItemTree.GroupPointerArrray[ctCustomDataItem]);
     end;
  end;

end;
//------------------------------------------------------------------------------
function TMatrixEditor.InputName(var aClassName:string; var aGroupName: string;
                                 var aDescription: string;
                                 var aItemType: TItemType;
                                 aEditMode : TEditMode ): Boolean;
var
  xData: TBaseDataItem;
  xNode: PVirtualNode;
  x: Integer;
  xList : TStringList;
  xGroups :  TStringList;
  xExistsDataItemNames :  TStringList;

begin
  result := false;

  with TInputDialog.Create(nil) do try

    xList:= TStringList.Create;
    xList.Sorted := TRUE;
    xList.Duplicates := dupIgnore;

    xGroups:= TStringList.Create;
    xGroups.Sorted := TRUE;
    xGroups.Duplicates := dupIgnore;


    xExistsDataItemNames := TStringList.Create;
    xExistsDataItemNames.Sorted := TRUE;
    xExistsDataItemNames.Duplicates := dupIgnore;


    for x:= 0 to mMatrixContainer.DataItemCount-1 do begin
       // if  mMatrixContainer.DataItems[x].ClassTypeGroup = ctCustomDataItem then begin
            xList.Add( mMatrixContainer.DataItems[x].Category);
            xExistsDataItemNames.Add( mMatrixContainer.DataItems[x].ItemName );
       //  end;
    end;

    for x:=0 to xList.Count-1 do
       if xList.Strings[x] <> '' then
          xGroups.Add( xList.Strings[x] );

    AddGroups(xGroups.CommaText);

    if aClassName <> '' then NewClassName:= aClassName;

    //Gruppenamen in CB setzen
    if aEditMode <> emNew then
       if aClassName <> '' then  NewGroupName:= aGroupName;

    if aDescription <> '' then NewDescription:= aDescription;

    ItemType := aItemType;
    EditMode := aEditMode;
    ExistsItemNames := xExistsDataItemNames.CommaText;

    if ShowModal = mrOK then begin
       aClassName := NewClassName;
       aGroupName := NewGroupName;
       aDescription := NewDescription;
       aItemType := ItemType;
       result := true;
    end;
  finally
    Free;

    xExistsDataItemNames.Free;
    xList.Free;
    xGroups.Free;
  end;// with TClassNameInputDialog.Create(nil) do try
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.SaveItem;
var xData: TMatrixDef;
    xDataItem: TBaseDataItem;
    xText : string;
    x : integer;
    xTxtOk : Boolean;
begin
  xData := NIL;
  xData := DataItemTree.SelectedDataItem as TMatrixDef;

  if assigned(xData) then begin

     //Matrix
     if xData.MatrixType <> mtNone then begin
        xData.MatrixType := mMatrix.qm.MatrixType;
        mMatrix.Save(xData);
     end;

     //Composite
     if xData.MatrixType = mtNone then
        mComposite.Save(xData);


     xData.Modified:= FALSE;
     DataItemTree.Refresh;
     //DataItemTree.ClassDefList := mMatrixContainer;

     xDataItem := mMatrixContainer.DataItemByName[xData.FieldName];
     DataItemTree.SelectDataItem(xDataItem, FALSE);

     mNewItem := FALSE;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.tbSave_Click(Sender: TObject);
begin
  SaveItem;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.tbNew_Click(Sender: TObject);
begin
  NewItem;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.NewItem;
var xArray : TClassClearSettingsArr;
    xClassName, xGroupName, xDescription: string;
    xIndex: integer;
    xMatrixDef : TMatrixDef;
    xClassGroup : TClassTypeGroup;
    xItemType   :TItemType;
    xDataItem: TBaseDataItem;
    xLen: Integer;
    xFClass : String;
begin
  xClassName   := '';
  xGroupName   := '';
  xDescription := '';

  xItemType:= itYarnClass;
  if InputName(xClassName, xGroupName, xDescription, xItemType, emNew) then begin
    if xItemType <> itComposite then begin
       //Matrix
       with mMatrix.qm do begin
            {
            FillChar(xArray, 16, 0);
            ShowMessage('TMatrixEditor.NewItem');
            //SetFieldStateTable(xArray) ; @SDO1
            }

            xClassGroup := ctCustomDataItem;
       end;
       xIndex := mMatrixContainer.CreateAndAddNewDataItem(TClassDef, TMatrixDef);

       case xItemType of
         itYarnClass : (mMatrixContainer.DataItems[xIndex] as TMatrixDef).MatrixType:= mtShortLongThin;
         itFFClass   : (mMatrixContainer.DataItems[xIndex] as TMatrixDef).MatrixType:= mtSiro;
         itSplice    : (mMatrixContainer.DataItems[xIndex] as TMatrixDef).MatrixType:= mtSplice;
       end;

    end else begin
       //Composite
       with mComposite do begin
            Reset;
            xClassGroup := ctCustomDataItem;
       end;
       xIndex := mMatrixContainer.CreateAndAddNewDataItem(TCompositClassDef, TMatrixDef);
       (mMatrixContainer.DataItems[xIndex] as TMatrixDef).MatrixType:=  mtNone;
    end;

    // Eigenschaften des neuen Items setzen
    mMatrixContainer[xIndex].ClassTypeGroup   := ctCustomDataItem;
    mMatrixContainer[xIndex].Description      := xDescription;
    mMatrixContainer[xIndex].ItemName         := xClassName;
    mMatrixContainer[xIndex].Predefined       := false;
    mMatrixContainer[xIndex].Category         := xGroupName;

    //Neu zuweisen -> nur in Komponente
    DataItemTree.Refresh;

    xDataItem := mMatrixContainer.DataItemByName[xClassName];
    DataItemTree.SelectDataItem(xDataItem, FALSE);

    mNewItem := TRUE;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.tbSaveAS_Click(Sender: TObject);
begin
  SaveItemAS;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.SaveItemAS;
var xData: TMatrixDef;
    xArray : TClassClearSettingsArr;
    xClassName, xGroupName, xDescription: string;
    xIndex, xNewIndex: integer;
    xMatrixDef : TMatrixDef;
    xClassGroup : TClassTypeGroup;
    xItemType   :TItemType;
    xDataItem: TBaseDataItem;
begin

  xClassName := '';
  xGroupName := '';
  xDescription := '';

  xData := NIL;
  xData := DataItemTree.SelectedDataItem as TMatrixDef;

  if assigned(xData) then begin
     case xData.MatrixType of
          mtNone          : xItemType := itComposite;
          mtShortLongThin : xItemType := itYarnClass;
          mtSiro          : xItemType := itFFClass;
          mtSplice        : xItemType := itSplice;
     end;

  (*
     if xData.MatrixType <> mtNone then
        xItemType := TItemType (xData.MatrixType)
     else
     //if xData.MatrixType = mtNone then
        xItemType := itComposite;

     // TItemType =   (itYarnClass, itFFClass, itSplice, itComposite);
     // TMatrixType = (mtNone, mtShortLongThin, mtSiro, mtSplice{, mtColor}
   *)

     xDescription := xData.Description;
     xClassName   := xData.ItemName;
     xGroupName   := xData.Category;

     if InputName(xClassName, xGroupName, xDescription, xItemType, emCopy) then begin
         xIndex:= mMatrixContainer.IndexOf(xData);

         if (xClassName = xData.ItemName) or (xGroupName = xData.Category) or
            (mMatrixContainer[xIndex].Predefined = TRUE) then begin

            if xItemType <> itComposite then begin
               //Matrix
               xNewIndex := mMatrixContainer.CreateAndAddNewDataItem(TClassDef, TMatrixDef);
            end else begin
               //Composite
               xNewIndex := mMatrixContainer.CreateAndAddNewDataItem(TCompositClassDef, TMatrixDef);
            end;
         end else xNewIndex := xIndex;

         mMatrixContainer[xNewIndex].Assign( mMatrixContainer[xIndex] );
         mMatrixContainer[xNewIndex].Predefined:=FALSE;
         mMatrixContainer[xNewIndex].ClassTypeGroup   := ctCustomDataItem;
         mMatrixContainer.MatrixItems[xNewIndex].ID := -1; //New Data in DB

         mMatrixContainer[xNewIndex].Description := xDescription;
         mMatrixContainer[xNewIndex].ItemName    := xClassName;
         mMatrixContainer[xNewIndex].Category    := xGroupName;

         DataItemTree.Refresh;

         xDataItem := mMatrixContainer.DataItemByName[xClassName];
         DataItemTree.SelectDataItem(xDataItem, FALSE);

         SaveItem;
     end;
  end; // END if assigned(xData) then begin
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.SetButtonStatus(aEnabled: Boolean);
begin
  tbSave.enabled   := aEnabled;
  tbSaveAS.enabled := aEnabled;
  tbEdit.enabled := aEnabled;
  tbDelete.enabled := aEnabled;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.acExitExecute(Sender: TObject);
var xData: TMatrixDef;
    xMsg : String;
begin
  try
    xData := NIL;
    xData := DataItemTree.SelectedDataItem as TMatrixDef;

    if assigned(xData) then begin
       mNewItem := mNewItem;
       if xData.Modified then begin
          if mNewItem then
             xMsg := Format(rsQuestion1, [xData.FieldName])
          else
             xMsg := Format(rsQuestion2, [xData.FieldName]);

          if MessageDlg(xMsg, mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
             SaveItem;
          end;
       end;
    end; // if assigned(xData)
  except
  end;
  close;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.acNewExecute(Sender: TObject);
begin
  SaveChangedValues;
  NewItem;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.acSaveExecute(Sender: TObject);
begin
  SaveItem;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.acCopyExecute(Sender: TObject);
begin
  SaveChangedValues;
  SaveItemAS;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.DataItemTreeKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DELETE then acDelete.Execute;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.acDeleteExecute(Sender: TObject);
begin
  DeleteItem;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.acEditExecute(Sender: TObject);
var xData: TMatrixDef;
    xClassName, xGroupName, xDescription: string;
    xItemType :TItemType;
    xDataItem : TBaseDataItem;
begin

  //SaveChangedValues;


  xClassName := '';
  xGroupName := '';
  xDescription := '';

  xData := NIL;
  xData := DataItemTree.SelectedDataItem as TMatrixDef;

  //if Is_ClName_In_QOff(xData) then exit;

  if assigned(xData) then begin

     case xData.MatrixType of
       mtNone           :  xItemType    :=  itComposite;
       mtShortLongThin  :  xItemType    :=  itYarnClass;
       mtSiro           :  xItemType    :=  itFFClass;
       mtSplice         :  xItemType    :=  itSplice;
       //mtColor        :    xItemType    :=
     end;
  
     xDescription := xData.Description;
     xClassName   := xData.ItemName;
     xGroupName   := xData.Category;

     if InputName(xClassName, xGroupName, xDescription, xItemType, emEdit) then begin

       if (xData.ItemName <> xClassName) and (xData.ItemName <> '') then
         if Is_ClName_In_QOff(xData) then exit;

       xData.Description := xDescription;
       xData.ItemName    := xClassName;
       xData.Category    := xGroupName;

       if xItemType = itComposite then
          mComposite.Save(xData)
       else
          mMatrix.Save(xData);

       DataItemTree.Refresh;

       xDataItem := mMatrixContainer.DataItemByName[xClassName];
       DataItemTree.SelectDataItem(xDataItem, FALSE);
     end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.PreparePrintList(aNode: PVirtualNode);
var xNode, xPrevNode : PVirtualNode;
    xData            : TMatrixDef;
    xDataItem, xDataItemPrev, xRootDataItem: TBaseDataItem;
    xClass, xGroup, xName, xText, xRoot :String;
    x, xCount : integer;
    xLevel : Integer;
    xPrintRec : TPrintRec;
begin
  if Not assigned( aNode ) then exit;

  x:= mPrintList.Count;
  mPrintList.Clear;

  x:= mPrintList.Count;
  xDataItem := DataItemTree.GetNodeDataItem(aNode);

  //Root ermitteln
  xPrevNode:= aNode;

  xRootDataItem:= DataItemTree.GetNodeDataItem(xPrevNode);
  xText := xRootDataItem.Category;
  xText := xRootDataItem.FieldName;

  xLevel:=-1;

  while xRootDataItem <> NIL do begin
        xNode :=  xPrevNode;
        xPrevNode:= xPrevNode.Parent;
        xRootDataItem:= DataItemTree.GetNodeDataItem(xPrevNode);

        if xRootDataItem <> NIL then
           xText := xText + ', ' + xRootDataItem.FieldName;

        inc(xLevel);
  end;

  //Root Node Name auslesen
  xRoot := '';
  if xRootDataItem = NIL  then begin
      xRootDataItem:= DataItemTree.GetNodeDataItem(xNode);
      if xRootDataItem <> NIL then
         xRoot  := xRootDataItem.DisplayName;
  end;

  xPrintRec.Root    := xRoot;
  xPrintRec.GroupID := 0 ;

  //Printable DataItems into PrintList
  if xDataItem.DataItemType.InheritsFrom(TClassDef) then begin
     //Click on a DataItem (only one Item)
     ADDItem(aNode, xPrintRec);
  end else begin
     //Click on a Node Point
     xNode := aNode;

     xCount:= xNode.ChildCount;

     if xlevel = 0 then begin
        //Click on Root
        xNode := DataItemTree.mTree.GetFirstChild(xNode);
        for x:= 0 to xCount-1 do begin

           xPrintRec.GroupID := x  ;

           if xNode.ChildCount = 0 then begin
              //Einzelen DataItems
              ADDItem(xNode, xPrintRec)
           end else
              //DataItems im SubTree
              PrepareSubTree(xNode, xPrintRec);

           xNode:=  DataItemTree.mTree.GetNextSibling(xNode);
        end;
     end else
        //Click on a SubTree
        PrepareSubTree(xNode, xPrintRec);
  end;
end;
//------------------------------------------------------------------------------
function TMatrixEditor.PrepareSubTree(aNode: PVirtualNode; aPrintRec: TPrintRec): Boolean;
var xId   : Integer;
    xNode : PVirtualNode;
    xRet  : Boolean;
begin

   if aNode = NIL then exit;

   xNode := DataItemTree.mTree.GetNext(aNode);
   for xId:= 0 to aNode.ChildCount-1 do begin

      //SubTree
      xRet:= TRUE;
      while xRet do begin
         xRet := ADDItem(xNode, aPrintRec );
         xNode := DataItemTree.mTree.GetNextSibling(xNode);
      end;

      if not xRet then break;
   end;
   if aNode.ChildCount = 0 then  xRet := ADDItem(xNode, aPrintRec);

end;
//------------------------------------------------------------------------------
function TMatrixEditor.ADDItem(aNode: PVirtualNode; aPrintRec: TPrintRec): Boolean;
var xDataItem : TBaseDataItem;
    xData     : TMatrixDef;
    xIndex    : Integer;
    xPrintRec : pPrintRec;
begin
    result := FALSE;
    if aNode = NIL then exit;

    xDataItem := DataItemTree.GetNodeDataItem(aNode);
    aPrintRec.SubTree := xDataItem.Category;

    if xDataItem.DataItemType.InheritsFrom(TClassDef) then begin

       xData := DataItemTree.GetNodeDataItem(aNode) as TMatrixDef;

       xIndex := mPrintList.AddCopy(xData, TMatrixDef);

       New(xPrintRec);
       xPrintRec^.Root     := aPrintRec.Root;
       xPrintRec^.SubTree  := aPrintRec.SubTree;
       xPrintRec^.GroupID  := aPrintRec.GroupID;

       mPrintList.PrintItems[xIndex].PrintOutInfo := xPrintRec;
       mPrintList.PrintItems[xIndex].Assign(xData);

       result := TRUE;
    end;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.FormShow(Sender: TObject);
begin
  DataItemTree.SetFocus;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.acPrintExecute(Sender: TObject);
begin

  with TfrmPrintSettings.Create(Self) do
  try

       ShowClearerSettings := FALSE;

       PrinterSet.Portrait := TRUE;
       PrinterSet.OrientationEnable := FALSE;

       if ShowModal = mrOK then begin
          PreparePrintList(mNode);

          with TPrintOutRep.create(NIL, mPrintList) do
          try
             UsePrinterIndex( PrinterSet.PrinterIndex ) ;
             qM.BlackWhite :=  PrinterSet.PrintBlackWhite;

              Prepare;
              ShowProgress:= TRUE;
              Print;
          finally
              Free;
          end;
       end;

  finally
       Free;
  end;
end;
//------------------------------------------------------------------------------

procedure TMatrixEditor.acSetSecurityExecute(Sender: TObject);
begin
  MMSecurityControl.Configure;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Uebrprueft, ob Daten geaendert wurden und speichert diese auf Verlangen
//Aufruf: acNewExecute, acCopyExecute
//******************************************************************************
procedure TMatrixEditor.SaveChangedValues;
var xData: TMatrixDef;
    xMsg : String;
    xDataItem: TBaseDataItem;
begin
  xData := NIL;
  xData := DataItemTree.SelectedDataItem as TMatrixDef;

  if not Assigned(xData) then exit;

  if xData.Modified then begin
     if mNewItem then
        xMsg := Format(rsQuestion1, [xData.FieldName])
     else
       xMsg := Format(rsQuestion2, [xData.FieldName]);

     xDataItem := mMatrixContainer.DataItemByName[xData.FieldName];
     if MessageDlg(xMsg, mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
        Application.ProcessMessages;
        SaveItem;
     end else begin
        xData.Modified := FALSE;
        mNewItem := FALSE;
     end;

     //Neu zuweisen -> nur in Komponente
     DataItemTree.Refresh;

     xDataItem := mMatrixContainer.DataItemByName[xData.FieldName];
     DataItemTree.SelectDataItem(xDataItem, FALSE);
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;
//------------------------------------------------------------------------------
function TMatrixEditor.Is_ClName_In_QOff(aDataItem: TMatrixDef): Boolean;
var xClassName, xAlarmName, xActive, xTxt, xSQL :String;
    xRet : Boolean;
begin

 try
   xRet := FALSE;
   with TAdoDBAccess.Create(1, True) do begin
        Init;
        with Query[0] do begin
             Close;
             SQL.text := Format( cSQL_GetClassName_from_QOfflimit, [aDataItem.ID] );
             Open;
             First;

             if not Eof then begin
                xClassName := FieldByName('ClassName').asString;
                xAlarmName := FieldByName('AlarmName').asString;
                {
                case FieldByName('Active').AsBoolean of
                    TRUE  : xActive := reActive;
                    FALSE : xActive := reInaktiv;
                end;
                }
                xRet :=  xClassName <> '';

                if xRet then begin
                   xTxt := Format( reMSGMutation1  + #10#13 +
                                   reMSGMutation2, [xClassName, xAlarmName, xClassName] );

                   MessageDLG(xTxt, mtWarning , [mbYes], 0);
                end;
             end;
        end;//END with Query[0]
        Free;
   end;//End with TAdoDBAccess.Create
 finally
 end;
 Result := xRet;
end;
//------------------------------------------------------------------------------
end.

