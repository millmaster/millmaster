(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_Matrix.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Panel fuer Matrix
|                 Wird in Matrix Editor(u_Editor) auf Panel paItemDataField gelegt
| Info..........: -
| Develop.system: W2k
| Target.system.: W2k, XP
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 03.07.2003  1.00  Sdo | Projekt erstellt
| 22.10.2003        lok | Vordefinierte DataItems k�nnen nicht ver�ndert werden, deshalb
|                       |   wird die Matrix in diesem Fall ReadOnly (TMatrix.SetData und TMatrix.qmFieldClick)
| 28.11.2003        SDo | TQualityMatrix durch TMMQualityMatrix (Ableitung) ersetzt
|                       | Feldbezeichung wird permanent angezeigt
| 21.01.2005  1.01  SDo | Umbau & Anpassungen an XML (Aenderung in Func. SetData() & Save() )
| 02.06.2005  1.01  SDo | - Feldmarkierungen zuerst loeschen, wenn Matrix mit neuen Daten gef�llt wird. -> TMatrix.SetData()
|                       | - Matrixgroesse dem Parent-Panel anpassen. -> TMatrix.SetData()  qm.CalculateMatrixSize;
|                       | - Aenderung in TMatrix.Save: Wird jetzt immer gespeichert (ohne Aenderungs Check)
| 16.06.2005  1.02  SDo | - Siro wird als ZenitF (Bright/Dark) angezeigt ->  SetData()
|=============================================================================*)
unit u_Matrix;

interface

uses

  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  u_EditorClasses, QualityMatrixBase, QualityMatrix,
  YMParaDef, ComCtrls, ToolWin, ImgList, ExtCtrls, SizingPanel, IvDictio,
  IvMulti, IvEMulti, mmTranslator, MMQualityMatrixClass;

type
  TMatrix = class(TFrame)
    SizingPanel1: TSizingPanel;
    mmTranslator1: TmmTranslator;
    qm: TMMQualityMatrix;
    procedure qmFieldClick(aFieldId: Integer; aButton: TMouseButton;
      aShift: TShiftState);
  private
    { Private declarations }
    mData : TMatrixDef;
    procedure SetClassClearFromSiro(var aSettings: TClassClearSettingsArr; aSiro: TSiroClearSettingsArr);
  protected
    function GetData: TMatrixDef;
    procedure SetData(const Value: TMatrixDef); //virtual;
    { Private declarations }
  public
    { Public declarations }
    property Data: TMatrixDef read GetData write SetData;
    procedure Save(var aMatrixDef :TMatrixDef); //override;
  end;


implementation

{$R *.DFM}
uses QualityMatrixDef, mmCS;

{ TMatrix }

//------------------------------------------------------------------------------
function TMatrix.GetData: TMatrixDef;
begin
  Result := mData;
end;
//------------------------------------------------------------------------------
procedure TMatrix.Save(var aMatrixDef: TMatrixDef);
var xData: TMatrixDef;
    xModified : Boolean;
    x : Integer;
    xClassFields, xSelectedClassFields :String;
    xCFList : TStringList;
begin

 xModified := FALSE;

 if assigned(aMatrixDef) then begin

    xCFList:= TStringList.Create;
    xCFList.Sorted := TRUE;
    xCFList.Duplicates := dupIgnore;

    xModified := TRUE;


    xClassFields := aMatrixDef.MatrixDef;        // Selected Class-Fields aus Container lesen
    qm.GetFieldStateTable(xSelectedClassFields); // Selected Class-Fields aus Matrix lesen

    //xClassFields sortieren
    xCFList.CommaText := xClassFields;
    xClassFields := xCFList.CommaText;

    xCFList.Clear;
    for x:= 1 to length(xSelectedClassFields) do begin
       if xSelectedClassFields[x] = 'C' then
          try
            xCFList.Add( Format('%d',[x]));
          finally
          end;
    end;

    //Aenderungs Check
//    if StrComp(PChar(xClassFields), PChar(xCFList.CommaText)) = 0 then
//       xModified := FALSE
//    else begin

      //Wird jetzt immer gespeichert
      aMatrixDef.MatrixDef := xCFList.CommaText;
      aMatrixDef.Modified := xModified;
      aMatrixDef.MatrixType := qm.MatrixType;

      //In DB speichern
      aMatrixDef.Save(NIL);
//    end;
    aMatrixDef.Modified := FALSE;

    xCFList.Free;
 end;
end;
//------------------------------------------------------------------------------
procedure TMatrix.SetClassClearFromSiro(
  var aSettings: TClassClearSettingsArr; aSiro: TSiroClearSettingsArr);
var
  i: integer;
begin
    assert(High(TSiroClearSettingsArr) <= High(TClassClearSettingsArr));
    for i := 0 to High(TSiroClearSettingsArr) do
      aSettings[i] := aSiro[i];
end;
//------------------------------------------------------------------------------
procedure TMatrix.SetData(const Value: TMatrixDef);
var xSiroClearSettingsArr: TSiroClearSettingsArr;
    xClassClearSettingsArr: TClassClearSettingsArr;
    xCutFields, xClassSettings : String;
    xClassFieldList : TStringList;
    x, xFieldNr : integer;
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  function GetSiroArray(aSettings: TClassClearSettingsArr): TSiroClearSettingsArr;
  var
    i: integer;
  begin
    assert(High(TSiroClearSettingsArr) <= High(TClassClearSettingsArr));
    for i := 0 to High(TSiroClearSettingsArr) do
      Result[i] := aSettings[i];
  end;
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
begin

  mData := Value;

// Matrixfeld disablen, wenn keine Klassendefinition selektiert ist
  qm.enabled := assigned(mData);

  if assigned(mData) then begin

      case mData.MatrixType of
        mtShortLongThin: begin
                            qm.MatrixType := mtShortLongThin;
                            //tbYarnClass.Down:= TRUE;
                         end;
               mtSplice: begin
                           qm.MatrixType := mtSplice;
                           //tbSiro.Down:= TRUE;
                         end;
                 mtSiro: begin
                           qm.MatrixType := mtSiro;
                           qm.MatrixSubType := mstZenitF;
                           qm.FModeBD := TRUE;
                           //tbSiro.Down:= TRUE;
                         end;
      end;// END case xData.ClassTypeGroup of

      qm.MatrixMode := mmSelectSettings;
      qm.CalculateMatrixSize;

      // Beim Umschalten auf Splice wird diese Eigenschaft automatisch True
      qm.Splicevisible := false;
      qm.ActiveVisible := true;
      qm.ChannelVisible := false;
      qm.CalculateMatrixSize;

      // Vordefinierte DataItems k�nnen nicht ver�ndert werden
      if Value.Predefined then
        qm.MatrixMode := mmDisplayData
      else
        qm.MatrixMode := mmSelectCutFields;

      xClassFieldList := TStringList.Create;
      xClassFieldList.Sorted := TRUE;
      xClassFieldList.Duplicates := dupIgnore;

      //Feldmarkierungen loeschen
      for x:=0 to qm.TotValues-1 do
          qm.FieldState[x]:= FALSE;

      //Matrix-Werte leeren
      qm.ClearValues;

      //Cut-Fields (selectierte Felder) aus Container lesen
      xClassFieldList.CommaText := mData.MatrixDef;
      for x:= 0 to xClassFieldList.Count-1 do begin
         try
           xFieldNr := StrToInt( xClassFieldList.Strings[x]) -1 ;
           //Cut-Fields der Matrix zuweisen
           qm.FieldState[xFieldNr]:= TRUE;
         finally
         end;
      end;

      xClassFieldList.Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrix.qmFieldClick(aFieldId: Integer; aButton: TMouseButton;
  aShift: TShiftState);
begin
  mData.Modified:= TRUE and (not(mData.Predefined));
end;
//------------------------------------------------------------------------------

end.
