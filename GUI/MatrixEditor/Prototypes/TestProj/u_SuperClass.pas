unit u_SuperClass;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DataItemTree, QuickRpt, YMQRUnit, VirtualTrees, mmVirtualStringTree,
  ClassDataSuck, SettingsFinderUnit,

  ClassDef, MMUGlobal, YMParaDef, QualityMatrixBase, QualityMatrix, QualityMatrixDef,
  AdoDBAccess, Buttons, StdCtrls;

type


  (*---------------------------------------------------------
    Dekorierer
  ----------------------------------------------------------*)
  TMatrixDef = class (TDataItemDecorator)
  private
    mCompositClassDef : TCompositClassDef;

    function GetClassSettingsArr: TClassClearSettingsArr;
    function GetID: Integer;
    function GetMatrixDef: String;
    function GetMatrixType: TMatrixType;
    procedure SetClassSettingsArr(const Value: TClassClearSettingsArr);
    procedure SetID(const Value: Integer);
    procedure SetMatrixDef(const Value: String);
    procedure SetMatrixType(const Value: TMatrixType);
    function GetCompositClassDef: TCompositClassDef;
    procedure SetCompositClassDef(const Value: TCompositClassDef);

    function GetFormula: String;
    procedure SetFormula(const Value: String);

  public
    constructor Create; override;
    destructor Destroy; override;
    //1 Gibt einen Kommaseparierten String zur�ck der der �bergebenen Matrix entspricht
    function GetMatrixFromArray(aArray: TClassClearSettingsArr): String;
    //1 Speichert die Klassendefinition in der Datenbank
    procedure Save(aNativeQuery: TNativeAdoQuery = nil);
    //1 Array mit den gesetzten Klassen. Jedes gesetzte Bit entspricht einer gesetzten Loepfe Klasse (Achtung! Nummerierung)
    property ClassSettingsArr: TClassClearSettingsArr read GetClassSettingsArr write SetClassSettingsArr;
    //1 ID des Datensatzes auf der Datenbank
    property ID: Integer read GetID write SetID;
    //1 Definition der selektierten Felder (zB: '23, 31, 39, 47, 55, 56')
    property MatrixDef: String read GetMatrixDef write SetMatrixDef;
    //1 Typ der Klassendefinition (Class, Splice, FF)
    property MatrixType: TMatrixType read GetMatrixType write SetMatrixType;

    property Formula :String read GetFormula write SetFormula;

  end;// TTestDataItem = class (TDataItemDecorator)


  TMatrixContainer = class (TClassDefContainer)
  private
    function GetMatrixCount: Word;
    function GetMatrixItems(aIndex: word): TMatrixDef;
    procedure SetMatrixItems(aIndex: word; const Value: TMatrixDef);
  public
    constructor Create(aPersistent: boolean = false); override;
    destructor Destroy; override;
    procedure DeleteData(aDataItem : TMatrixDef);
    property MatrixCount : Word read GetMatrixCount;
    property MatrixItems[aIndex:word]: TMatrixDef read GetMatrixItems write SetMatrixItems;
  end;


  TForm1 = class(TForm)
    DataItemTree1: TDataItemTree;
    qm: TQualityMatrix;
    sbSave: TSpeedButton;
    sbDelete: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure DataItemTree1FocusChanged(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex);
    procedure DataItemTree1FocusChanging(Sender: TBaseVirtualTree; OldNode,
      NewNode: PVirtualNode; OldColumn, NewColumn: TColumnIndex;
      var Allowed: Boolean);
    procedure sbSaveClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure sbDeleteClick(Sender: TObject);
    procedure FormDockOver(Sender: TObject; Source: TDragDockObject; X,
      Y: Integer; State: TDragState; var Accept: Boolean);
    procedure FormDockDrop(Sender: TObject; Source: TDragDockObject; X,
      Y: Integer);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }

    mMatrixContainer: TMatrixContainer;

    procedure SetClassClearFromSiro(var aSettings: TClassClearSettingsArr; aSiro: TSiroClearSettingsArr);
    procedure SaveMatrixData(var aMatrixDef :TMatrixDef; aMatrix: TQualityMatrix);
    function InputName(var aClassName:string; var aGroupName: string; var aDescription: string): Boolean;

  public
    { Public declarations }
  end;





var
  Form1: TForm1;


implementation

{$R *.DFM}
uses u_NewClassDialog;

const  cItemClass: TDataItemDecoratorClass = TMatrixDef;


procedure TForm1.FormCreate(Sender: TObject);
begin
  mMatrixContainer := TMatrixContainer.Create;

  //Klassen von DB holen; Daten werden in die Klasse TMatrixDef gepackt
  //Pro Item eine TMatrixDef Kl.
  mMatrixContainer.LoadFromDatabase(cItemClass);
  
  // Container zuweisen (wird kopiert)
  DataItemTree1.ClassDefList := mMatrixContainer;
  DataItemTree1.ExpandedGroups := [ctCustomDataItem];

  label1.caption := IntToStr( mMatrixContainer.MatrixCount );
end;
//------------------------------------------------------------------------------
procedure TForm1.DataItemTree1FocusChanged(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex);
var
  xData: TMatrixDef;
  xClassClearSettingsArr: TClassClearSettingsArr;
  xSiroClearSettingsArr: TSiroClearSettingsArr;
  xBaseDataItem : TBaseDataItem;

  function GetSiroArray(aSettings: TClassClearSettingsArr): TSiroClearSettingsArr;
  var
    i: integer;
  begin
    assert(High(TSiroClearSettingsArr) <= High(TClassClearSettingsArr));
    for i := 0 to High(TSiroClearSettingsArr) do
      Result[i] := aSettings[i];
  end;// function GetSiroArray(aSettings: TClassClearSettingsArr): TSiroClearSettingsArr;

begin

  FillChar(xClassClearSettingsArr,16,0);

  xData := nil;
  //sbDelete.Enabled := FALSE;
  if assigned(DataItemTree1.GetNodeDataItem(Node)) then
     if DataItemTree1.GetNodeDataItem(Node) is TMatrixDef then begin
        xData := TMatrixDef(DataItemTree1.GetNodeDataItem(Node));
        if xData.MatrixType = mtNone then begin
           qm.enabled := FALSE;
           FillChar(xClassClearSettingsArr,16,0);
           qm.SetFieldStateTable(xClassClearSettingsArr);
           exit;
        end;


     end;

  // Matrixfeld disablen, wenn keine Klassendefinition selektiert ist
  qm.enabled := assigned(xData);

  if assigned(xData) then begin

    case xData.MatrixType of
      mtShortLongThin: begin
        qm.MatrixType := mtShortLongThin;
      end;// mtShortLongThin: begin
      mtSplice: begin
        qm.MatrixType := mtSplice;
      end;// mtSplice: begin
      mtSiro: begin
        qm.MatrixType := mtSiro;
      end;// mtSiro: begin
    end;// case xData.ClassTypeGroup of
    // Beim Umschalten auf Splice wird diese Eigenschaft automatisch True
    qm.Splicevisible := false;
    qm.ActiveVisible := true;
    qm.ChannelVisible := false;

    if xData.MatrixType = mtSiro then begin
      xSiroClearSettingsArr := GetSiroArray(xData.ClassSettingsArr);

      qm.SetFieldStateTable(xSiroClearSettingsArr, xSiroClearSettingsArr);
    end else begin
      qm.SetFieldStateTable(xData.ClassSettingsArr);
    end;// if xData.ClassTypeGroup = ctFF then begin

    //if xData.Modified then ShowMessage(xData.GetItemName + ' ge�ndert! Bitte speichern.');

  end else begin
    FillChar(xClassClearSettingsArr,16,0);
    qm.SetFieldStateTable(xClassClearSettingsArr);
  end;// if assigned(xData) then begin
end;
//------------------------------------------------------------------------------
procedure TForm1.DataItemTree1FocusChanging(Sender: TBaseVirtualTree;
  OldNode, NewNode: PVirtualNode; OldColumn, NewColumn: TColumnIndex;
  var Allowed: Boolean);
var
  xData: TMatrixDef;

begin
  xData := nil;
  if assigned(DataItemTree1.GetNodeDataItem(OldNode)) then
   if DataItemTree1.GetNodeDataItem(OldNode) is TMatrixDef then
      xData := TMatrixDef(DataItemTree1.GetNodeDataItem(OldNode));

  if assigned(xData) then begin
    if OldNode <> NewNode then begin
      SaveMatrixData(xData, qm);
      if xData.Modified then ShowMessage('"' + xData.GetItemName + '" ge�ndert! Bitte speichern.');
    end;// if OldNode <> NewNode then begin

  end;// if assigned (xData) then begin

end;
//------------------------------------------------------------------------------
{ TMatrixDef }

constructor TMatrixDef.Create;
begin
  inherited;
  mCompositClassDef := TCompositClassDef.Create;
end;
//------------------------------------------------------------------------------
destructor TMatrixDef.Destroy;
begin
  mCompositClassDef.Free;

  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetClassSettingsArr: TClassClearSettingsArr;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      result := TClassDef(FDataItem).ClassSettingsArr;
  end else
    FillChar(result,16,0);
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetCompositClassDef: TCompositClassDef;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TCompositClassDef then
//      result := TCompositClassDef(FDataItem);
       mCompositClassDef := TCompositClassDef(FDataItem);
  end else
    result := NIL;
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetFormula: String;
begin
  Result := StringReplace(MatrixDef, ',', ' ' ,[rfReplaceAll] );
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetID: Integer;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      result := TClassDef(FDataItem).ID;
  end else
    result := -1;
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetMatrixDef: String;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      result := TClassDef(FDataItem).MatrixDef;
  end else
    result := '';
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetMatrixFromArray(
  aArray: TClassClearSettingsArr): String;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      result := TClassDef(FDataItem).GetMatrixFromArray(aArray);
  end else
    result := '';
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetMatrixType: TMatrixType;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      result := TClassDef(FDataItem).MatrixType;
  end else
    result := mtNone;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.Save(aNativeQuery: TNativeAdoQuery);
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      TClassDef(FDataItem).Save(aNativeQuery);
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetClassSettingsArr(
  const Value: TClassClearSettingsArr);
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      TClassDef(FDataItem).ClassSettingsArr := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetCompositClassDef(const Value: TCompositClassDef);
begin
  if assigned(FDataItem) then begin
    if FDataItem is TCompositClassDef then
       mCompositClassDef := Value;
      //TCompositClassDef(FDataItem) := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetFormula(const Value: String);
begin
  MatrixDef := Value;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetID(const Value: Integer);
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      TClassDef(FDataItem).ID := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetMatrixDef(const Value: String);
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      TClassDef(FDataItem).MatrixDef := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetMatrixType(const Value: TMatrixType);
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      TClassDef(FDataItem).MatrixType := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TForm1.sbSaveClick(Sender: TObject);
var xData: TMatrixDef;
begin
  xData := NIL;
  xData := DataItemTree1.SelectedDataItem as TMatrixDef;

  if assigned(xData) then begin
     SaveMatrixData(xData,qm);
     //In DB speichern
     xData.Save(NIL);
     xData.Modified := FALSE;
     //DataItemTree1.ClassDefList := mMatrixContainer;
  end;// if assigned (xData) then begin
end;
//------------------------------------------------------------------------------
procedure TForm1.SetClassClearFromSiro( var aSettings: TClassClearSettingsArr; aSiro: TSiroClearSettingsArr);
var
  i: integer;
begin
    assert(High(TSiroClearSettingsArr) <= High(TClassClearSettingsArr));
    for i := 0 to High(TSiroClearSettingsArr) do
      aSettings[i] := aSiro[i];
end;
//------------------------------------------------------------------------------
procedure TForm1.SaveMatrixData(var aMatrixDef: TMatrixDef; aMatrix: TQualityMatrix);
var xData: TMatrixDef;
    xArray, xArray0: TClassClearSettingsArr;
    xSiroClearSettingsArr: TSiroClearSettingsArr;
    xModified : Boolean;
    x : Integer;
begin

 xModified := FALSE;

 if assigned(aMatrixDef) then begin
    FillChar(xArray, 16, 0);
    if aMatrixDef.MatrixType = mtSiro then begin
       aMatrix.GetFieldStateTable(xSiroClearSettingsArr, xSiroClearSettingsArr);
       SetClassClearFromSiro(xArray, xSiroClearSettingsArr);
    end else begin
       aMatrix.GetFieldStateTable(xArray);
    end;// if xData.MatrixType = mtSiro then begin

    xModified := aMatrixDef.Modified;
    xArray0 := aMatrixDef.ClassSettingsArr;
    aMatrixDef.ClassSettingsArr := xArray;

    //Aenderungs check
    for x:= low(xArray) to high(xArray) do
        if xArray[x] <> xArray0[x] then begin
           xModified := TRUE;
           break;
        end;

    aMatrixDef.Modified := xModified;
  end;

end;
//------------------------------------------------------------------------------
function TForm1.InputName(var aClassName: string;  var aGroupName: string; var aDescription: string): Boolean;
var
  xData: TBaseDataItem;
  xNode: PVirtualNode;
  x: Integer;

  xList : TStringList;
  //xData : TMatrixDef;
begin
  result := false;

  //aGroupName := '';
  //aClassName := '';


  with TClassNameInputDialog.Create(nil) do try

    xList:= TStringList.Create;
    xList.Sorted := TRUE;
    xList.Duplicates := dupIgnore;

    for x:= 0 to mMatrixContainer.DataItemCount-1 do begin
        if  mMatrixContainer.DataItems[x].ClassTypeGroup = ctCustomDataItem then
            xList.Add( mMatrixContainer.DataItems[x].Category);
    end;


    for x:=0 to xList.Count-1 do
       if xList.Strings[x] <> '' then
          AddGroup(xList.Strings[x]);


    xList.Free;

    edGroup.ItemIndex:=0;

    if aClassName <> '' then NewClassName:= aClassName;

    if aClassName <> '' then  NewGroupName:= aGroupName;

    if aDescription <> '' then NewDescription:= aDescription;


    if ShowModal = mrOK then begin
       aClassName := NewClassName;
       aGroupName := NewGroupName;
       aDescription := NewDescription;
       result := true;
    end;
  finally
    Free;
  end;// with TClassNameInputDialog.Create(nil) do try

end;
//------------------------------------------------------------------------------
procedure TForm1.SpeedButton3Click(Sender: TObject);
var xArray : TClassClearSettingsArr;
    xClassName, xGroupName, xDescription: string;
    xIndex: integer;
    xMatrixDef : TMatrixDef;
begin
  xClassName := '';
  xGroupName := '';
  xDescription := '';


  with qm do begin
       FillChar(xArray, 16, 0);
       qm.SetFieldStateTable(xArray)
  end;


  if InputName(xClassName, xGroupName, xDescription) then begin

    xIndex := mMatrixContainer.CreateAndAddNewDataItem(TClassDef, TMatrixDef);

    // Eigenschaften des neuen Items setzen
    mMatrixContainer[xIndex].ClassTypeGroup   := ctLongThickThin;
    mMatrixContainer[xIndex].Description      := xDescription;
    mMatrixContainer[xIndex].ItemName         := xClassName;
    mMatrixContainer[xIndex].Predefined       := false;
    mMatrixContainer[xIndex].Category         := xGroupName;

    //Matrixtyp setzen
    (mMatrixContainer.DataItems[xIndex] as TMatrixDef).SetMatrixType(mtShortLongThin);

    //Neu zuweisen -> nur in Komponente
    DataItemTree1.ClassDefList := mMatrixContainer;

  end;
end;
//------------------------------------------------------------------------------
procedure TForm1.sbDeleteClick(Sender: TObject);
var xData     : TMatrixDef;
    x         : integer;
    xTxt      : String;
    xClassDef : TClassDef;
    xIndex    : integer;
begin

  xData := NIL;
  xData := DataItemTree1.SelectedDataItem as TMatrixDef;

  if Assigned (xData) then begin

     mMatrixContainer.DeleteData(xData);

     // Container zuweisen (wird kopiert)
     DataItemTree1.ClassDefList := mMatrixContainer;

  end;
end;
//------------------------------------------------------------------------------
procedure TForm1.FormDockOver(Sender: TObject; Source: TDragDockObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := TRUE;
end;
//------------------------------------------------------------------------------
procedure TForm1.FormDockDrop(Sender: TObject; Source: TDragDockObject; X,
  Y: Integer);
var
  xDataItemArray: TDataItemArray;
  i:integer;
begin
  xDataItemArray := nil;
end;
//------------------------------------------------------------------------------
procedure TForm1.SpeedButton2Click(Sender: TObject);
var xData     : TMatrixDef;
    x         : integer;
    xTxt      : String;
    xClassDef : TClassDef;

    xClassName, xGroupName, xDescription : String;
begin

  xData := NIL;
  xData := DataItemTree1.SelectedDataItem as TMatrixDef;

  if Assigned (xData) then begin

     xClassName    :=  xData.ItemName;
     xGroupName    :=  xData.Category;
     xDescription  :=  xData.Description;

     if InputName(xClassName, xGroupName, xDescription) then begin

        x:= mMatrixContainer.IndexOf(xData);

        // Eigenschaften des neuen Items setzen
        mMatrixContainer[x].Description      := xDescription;
        mMatrixContainer[x].ItemName         := xClassName;
        mMatrixContainer[x].Category         := xGroupName;

        sbSave.Click;

        // Container zuweisen (wird kopiert)
        DataItemTree1.ClassDefList := mMatrixContainer;
     end;
  end;
end;
//------------------------------------------------------------------------------


{ TMatrixContainer }

constructor TMatrixContainer.Create;
begin
  inherited Create;
end;
//------------------------------------------------------------------------------
procedure TMatrixContainer.DeleteData(aDataItem: TMatrixDef);
var xIndex : integer;
begin
  if Assigned (aDataItem) then begin
     //Daten aus Kontainer & DB loeschen
     xIndex := IndexOf(aDataItem);
     if xIndex >= 0 then begin
        if aDataItem.DataItemType.InheritsFrom(TClassDef) then begin
           TClassDef(aDataItem.DataItem).DeleteFromDatabase(NIL);
           Delete(xIndex);
        end;
     end;
   end;
end;
//------------------------------------------------------------------------------
destructor TMatrixContainer.Destroy;
begin
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TMatrixContainer.GetMatrixCount: Word;
var x: integer;
begin
  x:= GetDataItemCount;
  if x < 0 then x:=0;
  Result:=x;
end;
//------------------------------------------------------------------------------
function TMatrixContainer.GetMatrixItems(aIndex: word): TMatrixDef;
begin
  try
    Result:= DataItems[aIndex] as TMatrixDef;
  except
    Result:= NIL;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixContainer.SetMatrixItems(aIndex: word; const Value: TMatrixDef);
var xMatrixDef : TMatrixDef;
begin
  if assigned(Value) then begin
     xMatrixDef := DataItems[aIndex]as TMatrixDef;
     xMatrixDef := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TForm1.FormDestroy(Sender: TObject);
begin
  mMatrixContainer.Free;
end;
//------------------------------------------------------------------------------


end.
