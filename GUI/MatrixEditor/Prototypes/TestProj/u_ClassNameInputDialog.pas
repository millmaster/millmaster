unit u_ClassNameInputDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmStaticText, Buttons, ExtCtrls, mmEdit, mmComboBox, mmLabel,
  mmMemo;

type
  TClassNameInputDialog = class(TForm)
    edName: TmmEdit;
    Panel1: TPanel;
    Panel2: TPanel;
    butCancel: TBitBtn;
    butOk: TBitBtn;
    mmStaticText1: TmmStaticText;
    mmStaticText2: TmmStaticText;
    edGroup: TmmComboBox;
    meDescription: TmmMemo;
    mmStaticText3: TmmLabel;
    procedure butCancelClick(Sender: TObject);
  private
    function GetClassName: string;
    function GetGroupName: string;
    function GetDescription:String;
    { Private declarations }
  public
    procedure AddGroup(aGroupName: string);
    procedure SelectGroup(aGroupName: string);

    property NewClassName:string read GetClassName;
    property NewGroupName: string read GetGroupName;
    property NewDescription string read GetDescription;
    { Public declarations }
  end;

var
  ClassNameInputDialog: TClassNameInputDialog;

implementation

{$R *.DFM}
//------------------------------------------------------------------------------
procedure TClassNameInputDialog.AddGroup(aGroupName: string);
begin
  edGroup.Items.Add(aGroupName);
end;
//------------------------------------------------------------------------------
procedure TClassNameInputDialog.butCancelClick(Sender: TObject);
begin
  ModalResult := (Sender as TBitBtn).ModalResult;
end;
//------------------------------------------------------------------------------
function TClassNameInputDialog.GetClassName: string;
begin
  result := edName.Text;
end;
//------------------------------------------------------------------------------
function TClassNameInputDialog.GetDescription: String;
begin
   result := meDescription.Lines.CommaText;
end;
//------------------------------------------------------------------------------
function TClassNameInputDialog.GetGroupName: string;
begin
  result := edGroup.Text;
end;
//------------------------------------------------------------------------------
procedure TClassNameInputDialog.SelectGroup(aGroupName: string);
begin
  edGroup.Text := aGroupName;
end;
//------------------------------------------------------------------------------

end.
