object Form1: TForm1
  Left = 138
  Top = 152
  Width = 816
  Height = 474
  Hint = 'New Class'
  Caption = 'SuperClass'
  Color = clBtnFace
  DragMode = dmAutomatic
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnDockDrop = FormDockDrop
  OnDockOver = FormDockOver
  PixelsPerInch = 96
  TextHeight = 13
  object sbSave: TSpeedButton
    Left = 48
    Top = 8
    Width = 23
    Height = 22
    Hint = 'Save Class'
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888880000000000000880330000008803088033000000880308803300000088
      0308803300000000030880333333333333088033000000003308803088888888
      0308803088888888030880308888888803088030888888880308803088888888
      0008803088888888080880000000000000088888888888888888}
    ParentShowHint = False
    ShowHint = True
    OnClick = sbSaveClick
  end
  object sbDelete: TSpeedButton
    Left = 200
    Top = 8
    Width = 23
    Height = 22
    Hint = 'Delete Class'
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
      33333330F777777033333330F080807033333330F080707033333330F0808070
      33333330F080707033333330F080807033333030F080707030333300F0808070
      03333330F0807070333333307070707033333300000000000333330F88877777
      0333330000000000033333333077703333333333300000333333}
    ParentShowHint = False
    ShowHint = True
    OnClick = sbDeleteClick
  end
  object SpeedButton3: TSpeedButton
    Left = 16
    Top = 8
    Width = 23
    Height = 22
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      888888888888888888888800000000000888880FFFFFFFFF0888880FFFFFFFFF
      0888880FFFFFFFFF0888880FFFFFFFFF0888880FFFFFFFFF0888880FFFFFFFFF
      0888880FFFFFFFFF0888880FFFFFFFFF0888880FFFFFF0000888880FFFFFF0F0
      8888880FFFFFF008888888000000008888888888888888888888}
    ParentShowHint = False
    ShowHint = True
    OnClick = SpeedButton3Click
  end
  object SpeedButton2: TSpeedButton
    Left = 80
    Top = 8
    Width = 23
    Height = 22
    Hint = 'Edit Class (Name, Category, Description)'
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888000000
      00008888880FFFFFFFF000800000FF0F00F0E00BFBFB0FFFFFF0E0BFBF000FFF
      F0F0E0FBFBFBF0F00FF0E0BFBF00000B0FF0E0FBFBFBFBF0FFF0E0BF0000000F
      FFF0000BFB00B0FF00F08880000B0FFFFFF0888880B0FFFF000088880B0FF00F
      0FF08880B00FFFFF0F088809080FFFFF00888880880000000888}
    ParentShowHint = False
    ShowHint = True
    OnClick = SpeedButton2Click
  end
  object Label1: TLabel
    Left = 24
    Top = 416
    Width = 32
    Height = 13
    Caption = 'Label1'
  end
  object DataItemTree1: TDataItemTree
    Left = 16
    Top = 48
    Width = 241
    Height = 345
    TabOrder = 0
    DataItemTreeOptions = [dtoRowColor, dtoIcon, dtoGroupItalic, dtoNodeBold, dtoShowDescription]
    LineColor = 15596027
    OnFocusChanged = DataItemTree1FocusChanged
    OnFocusChanging = DataItemTree1FocusChanging
  end
  object qm: TQualityMatrix
    Left = 272
    Top = 8
    Width = 513
    Height = 409
    Color = clWhite
    LastCutMode = lcNone
    LastCutColor = clLime
    LastCutField = 0
    MatrixType = mtShortLongThin
    MatrixSubType = mstNone
    ActiveColor = clGreen
    ActiveVisible = True
    ChannelColor = clRed
    ChannelStyle = psSolid
    ChannelVisible = False
    ClusterColor = clPurple
    ClusterStyle = psSolid
    ClusterVisible = False
    CutsColor = clRed
    DefectsColor = clBlack
    DisableMessage = 'Quality Matrix disabled'
    DisplayMode = dmValues
    DotsColor = clBlack
    Enabled = True
    InactiveColor = 9502719
    MatrixMode = mmSelectCutFields
    SpliceColor = clBlue
    SpliceStyle = psSolid
    SpliceVisible = False
    SubFieldX = 0
    SubFieldY = 0
    ZeroLimit = 0.01
  end
end
