object CompositForm: TCompositForm
  Left = 121
  Top = 123
  Width = 743
  Height = 482
  Caption = 'CompositForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object sbSaveComp: TSpeedButton
    Left = 32
    Top = 24
    Width = 23
    Height = 22
    Hint = 'Save Class'
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888880000000000000880330000008803088033000000880308803300000088
      0308803300000000030880333333333333088033000000003308803088888888
      0308803088888888030880308888888803088030888888880308803088888888
      0008803088888888080880000000000000088888888888888888}
    ParentShowHint = False
    ShowHint = True
    OnClick = sbSaveCompClick
  end
  object spNewComp: TSpeedButton
    Tag = 1
    Left = 56
    Top = 24
    Width = 23
    Height = 22
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      888888888888888888888800000000000888880FFFFFFFFF0888880FFFFFFFFF
      0888880FFFFFFFFF0888880FFFFFFFFF0888880FFFFFFFFF0888880FFFFFFFFF
      0888880FFFFFFFFF0888880FFFFFFFFF0888880FFFFFF0000888880FFFFFF0F0
      8888880FFFFFF008888888000000008888888888888888888888}
    ParentShowHint = False
    ShowHint = True
    OnClick = spNewCompClick
  end
  object sbEditComp: TSpeedButton
    Left = 96
    Top = 24
    Width = 23
    Height = 22
    Hint = 'Edit Class (Name, Category, Description)'
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888000000
      00008888880FFFFFFFF000800000FF0F00F0E00BFBFB0FFFFFF0E0BFBF000FFF
      F0F0E0FBFBFBF0F00FF0E0BFBF00000B0FF0E0FBFBFBFBF0FFF0E0BF0000000F
      FFF0000BFB00B0FF00F08880000B0FFFFFF0888880B0FFFF000088880B0FF00F
      0FF08880B00FFFFF0F088809080FFFFF00888880880000000888}
    ParentShowHint = False
    ShowHint = True
    OnClick = sbEditCompClick
  end
  object sbDeleteComp: TSpeedButton
    Left = 176
    Top = 24
    Width = 23
    Height = 22
    Hint = 'Delete Class'
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
      33333330F777777033333330F080807033333330F080707033333330F0808070
      33333330F080707033333330F080807033333030F080707030333300F0808070
      03333330F0807070333333307070707033333300000000000333330F88877777
      0333330000000000033333333077703333333333300000333333}
    ParentShowHint = False
    ShowHint = True
    OnClick = sbDeleteCompClick
  end
  object Label1: TLabel
    Left = 264
    Top = 160
    Width = 44
    Height = 13
    Caption = 'Elemente'
  end
  object lbCategory: TLabel
    Left = 264
    Top = 63
    Width = 50
    Height = 13
    Caption = 'lbCategory'
  end
  object lbCompName: TLabel
    Left = 264
    Top = 80
    Width = 74
    Height = 13
    Caption = 'lbCompName'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object DataItemTree1: TDataItemTree
    Left = 32
    Top = 56
    Width = 209
    Height = 387
    TabOrder = 0
    LineColor = 15596027
    OnFocusChanged = DataItemTree1FocusChanged
    OnFocusChanging = DataItemTree1FocusChanging
  end
  object lbElement: TmmListBox
    Left = 264
    Top = 176
    Width = 145
    Height = 233
    Enabled = True
    ItemHeight = 13
    TabOrder = 1
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object bbAdd: TBitBtn
    Left = 264
    Top = 128
    Width = 50
    Height = 25
    Caption = '+ ADD'
    TabOrder = 2
    OnClick = bbAddClick
  end
  object BitBtn2: TBitBtn
    Left = 344
    Top = 128
    Width = 50
    Height = 25
    Caption = '- Minus'
    TabOrder = 3
    Visible = False
  end
  object edFormula: TmmEdit
    Left = 288
    Top = 96
    Width = 321
    Height = 21
    AutoSelect = False
    Color = clAqua
    ReadOnly = True
    TabOrder = 4
    Text = 'Formel'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object bbUndo: TBitBtn
    Left = 632
    Top = 96
    Width = 75
    Height = 25
    Caption = 'Undo'
    TabOrder = 5
    OnClick = bbUndoClick
  end
end
