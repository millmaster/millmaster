unit u_NewClassDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmStaticText, Buttons, ExtCtrls, mmEdit, mmComboBox, mmLabel,
  mmMemo;

type
  TClassNameInputDialog = class(TForm)
    edName: TmmEdit;
    Panel1: TPanel;
    Panel2: TPanel;
    butCancel: TBitBtn;
    butOk: TBitBtn;
    mmStaticText1: TmmStaticText;
    mmStaticText2: TmmStaticText;
    edGroup: TmmComboBox;
    meDescription: TmmMemo;
    mmStaticText3: TmmLabel;
    procedure butCancelClick(Sender: TObject);
  private
    function GetClassName: string;
    function GetGroupName: string;
    function GetDescription:String;
    procedure SetClassName(const Value: string);
    procedure SetDescription(const Value: string);
    procedure SetGroupName(const Value: string);
    { Private declarations }
  public
    procedure AddGroup(aGroupName: string);
    procedure SelectGroup(aGroupName: string);

    property NewClassName:string read GetClassName write SetClassName;
    property NewGroupName: string read GetGroupName write SetGroupName;
    property NewDescription: string read GetDescription write SetDescription;

    { Public declarations }
  end;

var
  ClassNameInputDialog: TClassNameInputDialog;

implementation

{$R *.DFM}
//------------------------------------------------------------------------------
procedure TClassNameInputDialog.AddGroup(aGroupName: string);
begin
  edGroup.Items.Add(aGroupName);
end;
//------------------------------------------------------------------------------
procedure TClassNameInputDialog.butCancelClick(Sender: TObject);
begin
  ModalResult := (Sender as TBitBtn).ModalResult;
end;
//------------------------------------------------------------------------------
function TClassNameInputDialog.GetClassName: string;
begin
  result := edName.Text;
end;
//------------------------------------------------------------------------------
function TClassNameInputDialog.GetDescription: String;
var x : integer;
    xRet :string;
begin
   xRet := '';

   for x:= 0 to meDescription.Lines.Count-1 do
       if x = meDescription.Lines.Count-1 then
          xRet :=  xRet  + meDescription.Lines.Strings[x]
       else
          xRet :=  xRet  + meDescription.Lines.Strings[x] + #10#13;

   result := xRet;
end;
//------------------------------------------------------------------------------
function TClassNameInputDialog.GetGroupName: string;
begin
  result := edGroup.Text;
end;
//------------------------------------------------------------------------------
procedure TClassNameInputDialog.SelectGroup(aGroupName: string);
begin
  edGroup.Text := aGroupName;
end;
//------------------------------------------------------------------------------
procedure TClassNameInputDialog.SetClassName(const Value: string);
begin
  if Value <> '' then edName.Text :=  Value;
end;
//------------------------------------------------------------------------------
procedure TClassNameInputDialog.SetDescription(const Value: string);
begin
  if Value <> '' then meDescription.Lines.CommaText  :=  Value;
end;
//------------------------------------------------------------------------------
procedure TClassNameInputDialog.SetGroupName(const Value: string);
begin
  if Value <> '' then begin
     edGroup.ItemIndex := edGroup.Items.IndexOf(Value);
  end;    
end;
//------------------------------------------------------------------------------
end.
