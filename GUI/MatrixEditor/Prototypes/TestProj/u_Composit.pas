unit u_Composit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ClassDef, AdoDbAccess, MMUGlobal, StdCtrls, Buttons,
  mmEdit, mmListBox, DataItemTree, u_SuperClass, VirtualTrees, QualityMatrixDef;

type

  TMatrixCompositContainer = class (TClassDefContainer)
  private
    function GetCompositeItems(aIndex: word): TMatrixDef;
    function GetMatrixCount: Word;
    procedure SetCompositeItems(aIndex: word; const Value: TMatrixDef);
  public
    constructor Create(aPersistent: boolean = false); override;
    destructor Destroy; override;
    procedure DeleteData(aDataItem : TMatrixDef);
    property CompositesCount : Word read GetMatrixCount;
    property CompositeItem[aIndex:word]: TMatrixDef read GetCompositeItems write SetCompositeItems;
  end;


  TCompositForm = class(TForm)
    DataItemTree1: TDataItemTree;
    lbElement: TmmListBox;
    bbAdd: TBitBtn;
    BitBtn2: TBitBtn;
    edFormula: TmmEdit;
    bbUndo: TBitBtn;
    sbSaveComp: TSpeedButton;
    spNewComp: TSpeedButton;
    sbEditComp: TSpeedButton;
    sbDeleteComp: TSpeedButton;
    Label1: TLabel;
    lbCategory: TLabel;
    lbCompName: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure spNewCompClick(Sender: TObject);
    procedure sbSaveCompClick(Sender: TObject);
    procedure DataItemTree1FocusChanged(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex);
    procedure DataItemTree1FocusChanging(Sender: TBaseVirtualTree; OldNode,
      NewNode: PVirtualNode; OldColumn, NewColumn: TColumnIndex;
      var Allowed: Boolean);
    procedure bbAddClick(Sender: TObject);
    procedure sbDeleteCompClick(Sender: TObject);
    procedure bbUndoClick(Sender: TObject);
    procedure sbEditCompClick(Sender: TObject);
    //procedure DataItemTree1FocusChanged(Sender: TBaseVirtualTree;
    //  Node: PVirtualNode; Column: TColumnIndex);
  private
    { Private declarations }
     mMatrixCompositContainer: TMatrixCompositContainer;
     function InputName(var aClassName:string; var aGroupName: string; var aDescription: string): Boolean;
     procedure FillElemntList;
     procedure SetButtonStatus(aStatus : Boolean);
  public
    { Public declarations }
  end;

var
  CompositForm: TCompositForm;

implementation

{$R *.DFM}
uses u_NewClassDialog;

const  cItemClass: TDataItemDecoratorClass = TMatrixDef;

{ TMatrixCompositContainer }

//------------------------------------------------------------------------------
constructor TMatrixCompositContainer.Create(aPersistent: boolean);
begin
  inherited;

end;
//------------------------------------------------------------------------------
procedure TMatrixCompositContainer.DeleteData(aDataItem: TMatrixDef);
begin

end;
//------------------------------------------------------------------------------
destructor TMatrixCompositContainer.Destroy;
begin
  inherited;

end;
//------------------------------------------------------------------------------
function TMatrixCompositContainer.GetCompositeItems(
  aIndex: word): TMatrixDef;
begin

end;
//------------------------------------------------------------------------------
function TMatrixCompositContainer.GetMatrixCount: Word;
begin

end;
//------------------------------------------------------------------------------
procedure TMatrixCompositContainer.SetCompositeItems(aIndex: word;
  const Value: TMatrixDef);
begin

end;
//------------------------------------------------------------------------------
procedure TCompositForm.FormCreate(Sender: TObject);
begin

     mMatrixCompositContainer := TMatrixCompositContainer.Create;

     //Klassen von DB holen; Daten werden in die Klasse TMatrixDef gepackt
     //Pro Item eine TMatrixDef Kl.
     mMatrixCompositContainer.LoadFromDatabase(cItemClass);


     // Container zuweisen (wird kopiert)
     DataItemTree1.ClassDefList := mMatrixCompositContainer;
     //DataItemTree1.ExpandedGroups := [ctCustomDataItem];
     DataItemTree1.ExpandedGroups := [ctDataItem];

     FillElemntList;

     //label1.caption := IntToStr( mMatrixCompositContainer.CompositesCount );
     lbCompName.Caption := '';
     lbCategory.Caption := '';
     
     SetButtonStatus(FALSE);
end;
//------------------------------------------------------------------------------
procedure TCompositForm.spNewCompClick(Sender: TObject);
var xClassName, xGroupName, xDescription: string;
    xIndex: integer;
    xMatrixDef : TMatrixDef;
begin
  xClassName := '';
  xGroupName := '';
  xDescription := '';


  if InputName(xClassName, xGroupName, xDescription) then begin

    xIndex := mMatrixCompositContainer.CreateAndAddNewDataItem(TCompositClassDef, TMatrixDef);

    with mMatrixCompositContainer[xIndex] do begin
         ClassTypeGroup   := ctCustomDataItem;
         Description      := xDescription;
         ItemName         := xClassName;
         Category         := xGroupName;
         Predefined       := false;
    end;
    (mMatrixCompositContainer.DataItems[xIndex] as TMatrixDef).MatrixType :=  mtNone;
    //Neu zuweisen -> nur in Komponente
    DataItemTree1.ClassDefList := mMatrixCompositContainer;
  end;
end;
//------------------------------------------------------------------------------
procedure TCompositForm.sbSaveCompClick(Sender: TObject);
var xData: TMatrixDef;
    x :string;
begin
  xData := NIL;
  xData := DataItemTree1.SelectedDataItem as TMatrixDef;

  if assigned(xData) then begin

     if xData.MatrixDef <> edFormula.Text then begin
        xData.Modified := TRUE;
     end;

     xData.Formula := edFormula.Text;

     //In DB speichern
     xData.Save(NIL);
     xData.Modified := FALSE;

     DataItemTree1.ClassDefList := mMatrixCompositContainer;
  end;// if assigned (xData) then begin

end;
//------------------------------------------------------------------------------
function TCompositForm.InputName(var aClassName, aGroupName,
  aDescription: string): Boolean;
var
  x: Integer;
  xList : TStringList;
begin
  result := false;

  with TClassNameInputDialog.Create(nil) do try

    xList:= TStringList.Create;
    xList.Sorted := TRUE;
    xList.Duplicates := dupIgnore;

    for x:= 0 to mMatrixCompositContainer.DataItemCount-1 do begin
        if  mMatrixCompositContainer.DataItems[x].ClassTypeGroup = ctCustomDataItem then
            xList.Add( mMatrixCompositContainer.DataItems[x].Category);
    end;


    for x:=0 to xList.Count-1 do
       if xList.Strings[x] <> '' then
          AddGroup(xList.Strings[x]);


    xList.Free;

    edGroup.ItemIndex:=0;

    if aClassName <> '' then NewClassName:= aClassName;

    if aClassName <> '' then  NewGroupName:= aGroupName;

    if aDescription <> '' then NewDescription:= aDescription;


    if ShowModal = mrOK then begin
       aClassName := NewClassName;
       aGroupName := NewGroupName;
       aDescription := NewDescription;
       result := true;
    end;
  finally
    Free;
  end;// with TClassNameInputDialog.Create(nil) do try
end;
//------------------------------------------------------------------------------
procedure TCompositForm.DataItemTree1FocusChanged(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex);
var
  //Column: TColumnIndex);
  xData: TMatrixDef;
  xCompName, xCategory :string;
  xStatus : Boolean;
begin

  xData := nil;
  xCompName := '';
  xCategory := '';
  xStatus := FALSE;

  if assigned(DataItemTree1.GetNodeDataItem(Node)) then
     if DataItemTree1.GetNodeDataItem(Node) is TMatrixDef then begin
        xData := TMatrixDef(DataItemTree1.GetNodeDataItem(Node));

        if xData.MatrixType = mtNone then begin
           edFormula.Text := xData.Formula;
           xCompName := Format('%s = ',[xData.ItemName]);
           xCategory := Format('Category : %s',[xData.Category]);
           xStatus := TRUE;
        end;
     end;

  SetButtonStatus(xStatus);

  lbCompName.Caption := xCompName;
  lbCategory.Caption := xCategory;
end;
//------------------------------------------------------------------------------
procedure TCompositForm.DataItemTree1FocusChanging(
  Sender: TBaseVirtualTree; OldNode, NewNode: PVirtualNode; OldColumn,
  NewColumn: TColumnIndex; var Allowed: Boolean);
var
  xData: TMatrixDef;

begin
  xData := nil;
  if assigned(DataItemTree1.GetNodeDataItem(OldNode)) then
   if DataItemTree1.GetNodeDataItem(OldNode) is TMatrixDef then
      xData := TMatrixDef(DataItemTree1.GetNodeDataItem(OldNode));

  if assigned(xData) then begin
    if OldNode <> NewNode then begin

      //SaveMatrixData(xData, qm);

     // if xData.Modified then ShowMessage('"' + xData.GetItemName + '" ge�ndert! Bitte speichern.');

    end;// if OldNode <> NewNode then begin

  end;// if assigned (xData) then begin

end;
//------------------------------------------------------------------------------
procedure TCompositForm.FillElemntList;
begin

    lbElement.Items.Add('%EffP');
    lbElement.Items.Add('%CCI');
    lbElement.Items.Add('%CCIFF');
    lbElement.Items.Add('%CL');
    lbElement.Items.Add('%CN');
    lbElement.Items.Add('COffCnt');
    lbElement.Items.Add('CSFI');
    lbElement.Items.Add('CSp');
    lbElement.Items.Add('CS');
    lbElement.Items.Add('CSFI');
    lbElement.Items.Add('CT');
    lbElement.Items.Add('CUpY');
    lbElement.Items.Add('CYTot');
    lbElement.Items.Add('Len');
    lbElement.Items.Add('SFI');
    lbElement.Items.Add('SFI/D');
    lbElement.Items.Add('Sp');
    lbElement.Items.Add('Weight');
    lbElement.Items.Add('YB');
end;
//------------------------------------------------------------------------------
procedure TCompositForm.bbAddClick(Sender: TObject);
var xFormula : String;
    xNewTerm :String;
    x:integer;
begin
  xFormula :=  edFormula.text;
  xFormula := StringReplace(xFormula, 'Formel', '' ,[rfReplaceAll] );


  xNewTerm :=  lbElement.Items.Strings[ lbElement.itemindex ];


  edFormula.text := format('%s + %s',[xFormula, xNewTerm]);
  xFormula :=  edFormula.text;
  x:= Pos('+', xFormula);
  if x < 3 then begin
     delete(xFormula, 1, x+1);
     Trim (xFormula);
     edFormula.text := xFormula;
  end;
end;
//------------------------------------------------------------------------------
procedure TCompositForm.sbDeleteCompClick(Sender: TObject);
var xData     : TMatrixDef;
    x         : integer;
    xTxt      : String;
    xClassDef : TClassDef;
    xIndex    : integer;
begin

  xData := NIL;
  xData := DataItemTree1.SelectedDataItem as TMatrixDef;

  if Assigned (xData) then begin

     mMatrixCompositContainer.DeleteData(xData);

     // Container zuweisen (wird kopiert)
     DataItemTree1.ClassDefList := mMatrixCompositContainer;

  end;
end;
//------------------------------------------------------------------------------
procedure TCompositForm.SetButtonStatus(aStatus: Boolean);
var x: integer;

begin
  for x:= 0 to ComponentCount-1 do begin
      if Components[x] is  TSpeedButton then
         if (Components[x] as TSpeedButton).Tag = 0 then
            (Components[x] as TSpeedButton).enabled:= aStatus;

      if Components[x] is  TBitBtn then
        (Components[x] as TBitBtn).enabled:= aStatus;
  end;
end;
//------------------------------------------------------------------------------
procedure TCompositForm.bbUndoClick(Sender: TObject);
var xText :string;
    x : integer;
begin
  xText := edFormula.Text;

  for x:= Length(xText) downto 1 do begin
      if xText[x] <> '+' then
         xText[x] := #0
      else break;
  end;
  xText[x] := #0;
  xText[x-1] := #0;

  Trim(xText);
  edFormula.Text := xText;

end;
//------------------------------------------------------------------------------
procedure TCompositForm.sbEditCompClick(Sender: TObject);
var xData     : TMatrixDef;
    x         : integer;
   // xTxt      : String;
   // xClassDef : TClassDef;

    xClassName, xGroupName, xDescription : String;
begin

  xData := NIL;
  xData := DataItemTree1.SelectedDataItem as TMatrixDef;

  if Assigned (xData) then begin

     xClassName    :=  xData.ItemName;
     xGroupName    :=  xData.Category;
     xDescription  :=  xData.Description;

     if InputName(xClassName, xGroupName, xDescription) then begin

        x:= mMatrixCompositContainer.IndexOf(xData);

        // Eigenschaften des neuen Items setzen
        mMatrixCompositContainer[x].Description      := xDescription;
        mMatrixCompositContainer[x].ItemName         := xClassName;
        mMatrixCompositContainer[x].Category         := xGroupName;

        sbSaveComp.Click;

        // Container zuweisen (wird kopiert)
        DataItemTree1.ClassDefList := mMatrixCompositContainer;
     end;
  end;

end;

end.
