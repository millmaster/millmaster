unit u_MainProg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ToolWin, u_Editor;

type
  TfrmMain = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    mMatrixEditor : TMatrixEditor;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

const
  cEditorWidth = 700;
  cEditorHight = 530;


implementation

{$R *.DFM}
//------------------------------------------------------------------------------
procedure TfrmMain.ToolButton1Click(Sender: TObject);
begin
  //screen.Cursor :=  crHourGlass;
  MatrixEditor  :=  TMatrixEditor.Create(Self);
  //screen.Cursor :=  crDefault;

  MatrixEditor.Show;
  MatrixEditor.WindowState := wsMaximized;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.ToolButton2Click(Sender: TObject);
begin
  MatrixEditor.Free;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormCreate(Sender: TObject);
begin
  Width  := cEditorWidth;
  Height := cEditorHight;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  MatrixEditor.Free;
end;

end.
