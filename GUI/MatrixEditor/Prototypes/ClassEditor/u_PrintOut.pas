unit u_PrintOut;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, YMQRUnit, mmQRLabel,

  u_EditorClasses, mmQRExprMemo, mmQRStringsBand, mmQRSysData, mmQRBand,
  mmQRGroup, mmQRSubDetail, mmQRShape, mmQRImage;

type
  TPrintOutRep = class(TQuickRep)
    TitleBand1: TQRBand;
    mmQRLabel1: TmmQRLabel;
    PageHeaderBand1: TQRBand;
    DetailBand: TQRBand;
    qrlRoot: TQRLabel;
    qrDate: TmmQRSysData;
    qrlDate: TmmQRLabel;
    qrlPage: TmmQRLabel;
    qrlClassname: TmmQRLabel;
    SubDetailMatrix: TmmQRSubDetail;
    mmQRLabel2: TmmQRLabel;
    meRemarks: TmmQRExprMemo;
    qM: TQRQMatrix;
    SubDetailComposite: TmmQRSubDetail;
    qrlRemarkComp: TmmQRLabel;
    meRemarks1: TmmQRExprMemo;
    qrlFormula: TmmQRLabel;
    ChildBand3: TQRChildBand;
    ChildBand2: TQRChildBand;
    DetailShape: TQRShape;
    ChildBand1: TQRChildBand;
    qrlFormulaValue: TmmQRLabel;
    mmQRLabel3: TmmQRLabel;
    mmQRImage1: TmmQRImage;


    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure DetailBandBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure PageHeaderBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepEndPage(Sender: TCustomQuickRep);
    procedure SubDetailMatrixBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure SubDetailCompositeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
     mPrintList: TPrintList;
     mCounter, xMaxItems : Integer;
     mActualPage : integer;
     mMaxPages : Integer;
     mMaxPageCounted: Boolean;
     //mItemPerPage : integer;


  public
     constructor create(aOwner: TComponent; aPrintList :TPrintList); overload;
  end;

var
  PrintOutRep: TPrintOutRep;


resourcestring

    c_Page      = '(*)Seite'; //ivlm
    c_NoRemark  = '(*)keine'; //ivlm
    c_NoFormula = '(*)keine Definition'; //ivlm
    c_Date      = '(*)Datum'; //ivlm


implementation

{$R *.DFM}
uses QualityMatrixDef, YMParaDef;


{ TPrintOutRep }

constructor TPrintOutRep.create(aOwner: TComponent; aPrintList: TPrintList);

begin
  inherited create(aOwner);
  mPrintList := aPrintList;

  mCounter  := -1;
  mMaxPages := 0;

  mMaxPageCounted := FALSE;

  xMaxItems := mPrintList.Count;
end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin

 mCounter  := -1;
 mActualPage := 1;

 if mMaxPages >= 1 then
    mMaxPageCounted := TRUE
 else
    mMaxPages := 0;


 qrlDate.Caption := Format('%s:', [c_Date] );
 qrlPage.Caption := Format('%s:', [c_Page] );

 DetailShape.Top    := 0;
 DetailShape.Left   := 0;
 DetailShape.Width  := DetailBand.Width;
 DetailShape.Height := DetailBand.Height;

end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
var xText, xRoot, xSubTree, xDisplName, xFormula, xDescription : String;
    x: Integer;

  xSiroClearSettingsArr  : TSiroClearSettingsArr;
  xClassClearSettingsArr : TClassClearSettingsArr;

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  function GetSiroArray(aSettings: TClassClearSettingsArr): TSiroClearSettingsArr;
  var
    i: integer;
  begin
    assert(High(TSiroClearSettingsArr) <= High(TClassClearSettingsArr));
    for i := 0 to High(TSiroClearSettingsArr) do
        Result[i] := aSettings[i];
  end;
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


begin

  inc(mCounter);

  MoreData := (mCounter < xMaxItems);

  if MoreData then begin

     xRoot        := mPrintList.PrintItems[mCounter].PrintOutInfo.Root;
     xSubTree     := mPrintList.PrintItems[mCounter].PrintOutInfo.SubTree;
     xDisplName   := mPrintList.PrintItems[mCounter].DisplayName;
     xDescription := mPrintList.PrintItems[mCounter].Description;
     xFormula     := mPrintList.PrintItems[mCounter].Formula ;


     if xSubTree = '' then xSubTree := ' - ';
     if xDescription = '' then xDescription := c_NoRemark;
     if xFormula = '' then xFormula := c_NoFormula;

     qrlClassname.caption := xDisplName;

     qrlRoot.caption := Format ('%s / %s',[xRoot, xSubTree]);

     if mPrintList.PrintItems[mCounter].MatrixType = mtNone then begin
        //Composite Daten
        DetailBand.LinkBand := SubDetailComposite;
        qrlFormula.caption := Format('%s = ',[xDisplName] );
        qrlFormulaValue.caption := xFormula;

        qrlFormulaValue.Left := qrlFormula.Left + qrlFormula.Width;
        qrlFormulaValue.Width := qrlRemarkComp.Left - qrlFormulaValue.Left - 5;

        meRemarks1.Lines.Clear;
        meRemarks1.Lines.CommaText := xDescription;


     end else begin
        //Matrix Daten
        DetailBand.LinkBand := SubDetailMatrix;

        meRemarks.Lines.Clear;
        meRemarks.Lines.CommaText := xDescription;

        qM.MatrixTyp := mPrintList.PrintItems[mCounter].MatrixType;

        // Beim Umschalten auf Splice wird diese Eigenschaft automatisch True
        qM.Splicevisible := false;
        qM.ActiveVisible := true;
        qM.ChannelVisible := false;

        if qM.MatrixTyp = mtSiro then begin
           xSiroClearSettingsArr := GetSiroArray( mPrintList.PrintItems[mCounter].ClassSettingsArr);

           qM.QMatrix.QMatrix.SetFieldStateTable(xSiroClearSettingsArr, xSiroClearSettingsArr);

        end else begin
            qM.QMatrix.QMatrix.SetFieldStateTable(mPrintList.PrintItems[mCounter].ClassSettingsArr);
        end;

        qM.Invalidate;
     end;
  end;
end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.DetailBandBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  PrintBand := (mCounter < xMaxItems);
end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.ChildBandBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
   PrintBand := (mCounter < xMaxItems);
   if not PrintBand then exit;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Footer preparieren
//******************************************************************************
procedure TPrintOutRep.PageHeaderBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var x, n : Integer;
begin

  x:= PageHeaderBand1.Width Div 2;
  n:= (qrlDate.Width + qrlPage.Width + 5) DIV 2;

  //Date: (Mitte)
  qrlDate.Left := x - n;
  qrDate.Left  := qrlDate.Left +  qrlDate.Width + 5;

  //Pages: (Rechts)
  qrlPage.caption := Format('%s: %d / %d', [c_Page, mActualPage, mMaxPages] );
  qrlPage.Left  :=  PageHeaderBand1.Width - qrlPage.Width - 5;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Max Pages ermitteln
//******************************************************************************
procedure TPrintOutRep.QuickRepEndPage(Sender: TCustomQuickRep);
begin
  inc(mActualPage);
  if not mMaxPageCounted then
     inc(mMaxPages);
end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.SubDetailMatrixBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if (mCounter < xMaxItems) then
     if mPrintList.PrintItems[mCounter].MatrixType = mtNone then
       PrintBand := FALSE
     else begin
       qM.Invalidate;
       PrintBand := TRUE;
     end;
end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.SubDetailCompositeBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if (mCounter < xMaxItems) then
     if mPrintList.PrintItems[mCounter].MatrixType <> mtNone then
        PrintBand := FALSE
     else
        PrintBand := TRUE;
end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
 {
  PrintBand := (mCounter < xMaxItems);
  if not PrintBand then exit;

//  if (mCounter < xMaxItems) then
     PrintBand := (mPrintList.PrintItems[mCounter].MatrixType <> mtNone);

 }

  PrintBand := (mPrintList.PrintItems[mCounter].MatrixType <> mtNone);
end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.ChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin

  PrintBand := (mPrintList.PrintItems[mCounter].MatrixType = mtNone);
{
  PrintBand := (mCounter < xMaxItems);
  if not PrintBand then exit;

//  if (mCounter < xMaxItems) then
      PrintBand := (mPrintList.PrintItems[mCounter].MatrixType = mtNone);
 }
end;
//------------------------------------------------------------------------------
procedure TPrintOutRep.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
   PrintBand := (mCounter < xMaxItems);
   if not PrintBand then exit;
end;
//------------------------------------------------------------------------------
end.
