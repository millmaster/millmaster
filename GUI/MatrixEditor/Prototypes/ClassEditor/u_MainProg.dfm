object frmMain: TfrmMain
  Left = 163
  Top = 276
  Width = 711
  Height = 539
  Caption = 'Main Appl (LabReport oder QMatrix) Class designer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIForm
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 703
    Height = 29
    ButtonHeight = 21
    ButtonWidth = 63
    Caption = 'ToolBar1'
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 2
      Caption = 'Open Editor'
      ImageIndex = 0
      OnClick = ToolButton1Click
    end
    object ToolButton2: TToolButton
      Left = 63
      Top = 2
      Caption = 'Close Editor'
      ImageIndex = 1
      OnClick = ToolButton2Click
    end
  end
end
