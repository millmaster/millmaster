(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_Matrix.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Panel fuer Matrix
|                 Wird in Matrix Editor(u_Editor) auf Panel paItemDataField gelegt
| Info..........: -
| Develop.system: W2k
| Target.system.: W2k, XP
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 03.07.2003  1.00  Sdo | Projekt erstellt
|=============================================================================*)
unit u_Matrix;

interface

uses

  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  u_EditorClasses, QualityMatrixBase, QualityMatrix,
  YMParaDef, ComCtrls, ToolWin, ImgList, ExtCtrls, SizingPanel, IvDictio,
  IvMulti, IvEMulti, mmTranslator;

type
  TMatrix = class(TFrame)
    SizingPanel1: TSizingPanel;
    qm: TQualityMatrix;
    mmTranslator1: TmmTranslator;
    procedure qmFieldClick(aFieldId: Integer; aButton: TMouseButton;
      aShift: TShiftState);
    procedure tbYarnClassClick(Sender: TObject);
    procedure tbSiroClick(Sender: TObject);
  private
    { Private declarations }
    mData : TMatrixDef;
    procedure SetClassClearFromSiro(var aSettings: TClassClearSettingsArr; aSiro: TSiroClearSettingsArr);
  protected
    function GetData: TMatrixDef;
    procedure SetData(const Value: TMatrixDef); //virtual;
    { Private declarations }
  public
    { Public declarations }
    property Data: TMatrixDef read GetData write SetData;
    procedure Save(var aMatrixDef :TMatrixDef); //override;
  end;


implementation

{$R *.DFM}
uses QualityMatrixDef;

{ TMatrix }

//------------------------------------------------------------------------------
function TMatrix.GetData: TMatrixDef;
begin
  Result := mData;
end;
//------------------------------------------------------------------------------
procedure TMatrix.Save(var aMatrixDef: TMatrixDef);
var xData: TMatrixDef;
    xArray, xArray0: TClassClearSettingsArr;
    xSiroClearSettingsArr: TSiroClearSettingsArr;
    xModified : Boolean;
    x : Integer;
begin

 xModified := FALSE;

 if assigned(aMatrixDef) then begin
    FillChar(xArray, 16, 0);
    if aMatrixDef.MatrixType = mtSiro then begin
       qm.GetFieldStateTable(xSiroClearSettingsArr, xSiroClearSettingsArr);
       SetClassClearFromSiro(xArray, xSiroClearSettingsArr);
    end else begin
       qm.GetFieldStateTable(xArray);
    end;

    xModified := aMatrixDef.Modified;
    xArray0 := aMatrixDef.ClassSettingsArr;
    aMatrixDef.ClassSettingsArr := xArray;

    //Aenderungs check
    for x:= low(xArray) to high(xArray) do
        if xArray[x] <> xArray0[x] then begin
           xModified := TRUE;
           break;
        end;

    aMatrixDef.Modified := xModified;
    aMatrixDef.MatrixType := qm.MatrixType;
    //In DB speichern
    aMatrixDef.Save(NIL);
    aMatrixDef.Modified := FALSE;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrix.SetClassClearFromSiro(
  var aSettings: TClassClearSettingsArr; aSiro: TSiroClearSettingsArr);
var
  i: integer;
begin
    assert(High(TSiroClearSettingsArr) <= High(TClassClearSettingsArr));
    for i := 0 to High(TSiroClearSettingsArr) do
      aSettings[i] := aSiro[i];
end;
//------------------------------------------------------------------------------
procedure TMatrix.SetData(const Value: TMatrixDef);
var xSiroClearSettingsArr: TSiroClearSettingsArr;
    xClassClearSettingsArr: TClassClearSettingsArr;

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  function GetSiroArray(aSettings: TClassClearSettingsArr): TSiroClearSettingsArr;
  var
    i: integer;
  begin
    assert(High(TSiroClearSettingsArr) <= High(TClassClearSettingsArr));
    for i := 0 to High(TSiroClearSettingsArr) do
      Result[i] := aSettings[i];
  end;
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
begin

  mData := Value;

// Matrixfeld disablen, wenn keine Klassendefinition selektiert ist
  qm.enabled := assigned(mData);

  if assigned(mData) then begin

      case mData.MatrixType of
        mtShortLongThin: begin
                            qm.MatrixType := mtShortLongThin;
                            //tbYarnClass.Down:= TRUE;
                         end;
               mtSplice: begin
                           qm.MatrixType := mtSplice;
                           //tbSiro.Down:= TRUE;
                         end;
                 mtSiro: begin
                           qm.MatrixType := mtSiro;
                           //tbSiro.Down:= TRUE;
                         end;
      end;// END case xData.ClassTypeGroup of

      // Beim Umschalten auf Splice wird diese Eigenschaft automatisch True
      qm.Splicevisible := false;
      qm.ActiveVisible := true;
      qm.ChannelVisible := false;

      if mData.MatrixType = mtSiro then begin
         xSiroClearSettingsArr := GetSiroArray(mData.ClassSettingsArr);

         qm.SetFieldStateTable(xSiroClearSettingsArr, xSiroClearSettingsArr);
      end else begin
         qm.SetFieldStateTable(mData.ClassSettingsArr);
      end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrix.qmFieldClick(aFieldId: Integer; aButton: TMouseButton;
  aShift: TShiftState);
begin
  mData.Modified:= TRUE;
end;
//------------------------------------------------------------------------------
procedure TMatrix.tbYarnClassClick(Sender: TObject);
begin
  qm.MatrixType    := mtShortLongThin;
  //mData.MatrixType := qm.MatrixType;
  mData.Modified:= TRUE;
end;
//------------------------------------------------------------------------------
procedure TMatrix.tbSiroClick(Sender: TObject);
begin
  qm.MatrixType := mtSiro;
  mData.Modified:= TRUE;
  //mData.MatrixType := qm.MatrixType;
end;
//------------------------------------------------------------------------------

end.
