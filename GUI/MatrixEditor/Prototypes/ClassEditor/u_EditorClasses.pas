(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_EditorClasses.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Definition TMatrixDef (Decodierer)
|                 Liste mit den Dataitems -> TMatrixContainer
| Info..........: -
| Develop.system: W2k
| Target.system.: W2k, XP
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 03.07.2003  1.00  Sdo | Projekt erstellt
|=============================================================================*)

unit u_EditorClasses;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  DataItemTree, QuickRpt, YMQRUnit, VirtualTrees, mmVirtualStringTree,
  ClassDataSuck, SettingsFinderUnit,

  ClassDef, MMUGlobal, YMParaDef, QualityMatrixBase, QualityMatrix, QualityMatrixDef,
  AdoDBAccess, mmList;

type


   TPrintRec = Record
      Root     : String;
      SubTree  : String;
      GroupID  : Integer;
   end;
   pPrintRec = ^TPrintRec;


  (*---------------------------------------------------------
    Dekorierer
  ----------------------------------------------------------*)
  TMatrixDef = class (TDataItemDecorator)
  private
    mCompositClassDef : TCompositClassDef;
    mPrintRec : pPrintRec;

    function GetClassSettingsArr: TClassClearSettingsArr;
    function GetID: Integer;
    function GetMatrixDef: String;
    function GetMatrixType: TMatrixType;
    procedure SetClassSettingsArr(const Value: TClassClearSettingsArr);
    procedure SetID(const Value: Integer);
    procedure SetMatrixDef(const Value: String);
    procedure SetMatrixType(const Value: TMatrixType);
    function GetCompositClassDef: TCompositClassDef;
    procedure SetCompositClassDef(const Value: TCompositClassDef);

    function GetFormula: String;
    procedure SetFormula(const Value: String);

    //Kovertiert den Displayname in ein DB-FieldName
    function ConvertDisplayToFieldName(aDisplayName : String) : String;

    //Kovertiert den DB-FieldName
    function ConvertFieldNameToDisplay(aFieldName : String) : String;

    function GetPrintRec: pPrintRec;
    procedure SetPrintRec(const Value: pPrintRec);

  public
    constructor Create; override;
    destructor Destroy; override;
    //1 Gibt einen Kommaseparierten String zur�ck der der �bergebenen Matrix entspricht
    function GetMatrixFromArray(aArray: TClassClearSettingsArr): String;
    //1 Speichert die Klassendefinition in der Datenbank
    procedure Save(aNativeQuery: TNativeAdoQuery = nil);
    //1 Array mit den gesetzten Klassen. Jedes gesetzte Bit entspricht einer gesetzten Loepfe Klasse (Achtung! Nummerierung)
    property ClassSettingsArr: TClassClearSettingsArr read GetClassSettingsArr write SetClassSettingsArr;
    //1 ID des Datensatzes auf der Datenbank
    property ID: Integer read GetID write SetID;
    //1 Definition der selektierten Felder (zB: '23, 31, 39, 47, 55, 56')
    property MatrixDef: String read GetMatrixDef write SetMatrixDef;
    //1 Typ der Klassendefinition (Class, Splice, FF)
    property MatrixType: TMatrixType read GetMatrixType write SetMatrixType;

    property Formula :String read GetFormula write SetFormula;

    property PrintOutInfo : pPrintRec read GetPrintRec write SetPrintRec;

  end;// TTestDataItem = class (TDataItemDecorator)



  TPrintList = class (TClassDefContainer)
  private
    function GetCount: Word;
    function GetPrintItems(aIndex: word): TMatrixDef;
    procedure SetPrintItems(aIndex: word; const Value: TMatrixDef);
  public
    constructor Create(aPersistent: boolean = false); override;
    destructor Destroy; override;
    property Count : Word read GetCount;
    property PrintItems[aIndex:word]: TMatrixDef read GetPrintItems write SetPrintItems;
  end;






  TMatrixContainer = class (TClassDefContainer)
  private
    mPrintList : TPrintList;

    function GetMatrixCount: Word;
    function GetMatrixItems(aIndex: word): TMatrixDef;
    procedure SetMatrixItems(aIndex: word; const Value: TMatrixDef);

    function GetPrintList: TPrintList;
    procedure SetPrintList(const Value: TPrintList);
  public
    constructor Create(aPersistent: boolean = false); override;
    destructor Destroy; override;

    procedure DeleteData(aDataItem : TMatrixDef);
    property MatrixCount : Word read GetMatrixCount;
    property MatrixItems[aIndex:word]: TMatrixDef read GetMatrixItems write SetMatrixItems;
    //property PrintList: TPrintList read GetPrintList Write SetPrintList;

  end;



  TBaseItemFrame = class(TFrame)
  private
  protected
    mData : TMatrixDef;
    function GetData: TMatrixDef; virtual;
    procedure SetData(const Value: TMatrixDef); virtual; abstract;
    { Private declarations }
  public
    { Public declarations }
    //constructor Create(AOwner: TComponent); override;
    property Data: TMatrixDef read GetData write SetData;
    procedure Save(var aMatrixDef :TMatrixDef); virtual;
  end;


const  cItemClass: TDataItemDecoratorClass = TMatrixDef;


implementation

{ TMatrixDef }
uses LabMasterDef;

//------------------------------------------------------------------------------
function TMatrixDef.ConvertDisplayToFieldName(aDisplayName: String): String;
var x, xIndex : integer;
begin
  for x:= low(cDataItemArray) to high(cDataItemArray) do
      if cDataItemArray[x].CompositeElement = TRUE then begin
         xIndex := Pos( cDataItemArray[x].DisplayName, aDisplayName);
         if xIndex > 0 then
            aDisplayName := StringReplace(aDisplayName, cDataItemArray[x].DisplayName, cDataItemArray[x].Field ,[rfReplaceAll] );
      end;
  result := aDisplayName;
end;
//------------------------------------------------------------------------------
function TMatrixDef.ConvertFieldNameToDisplay(aFieldName: String): String;
var x, xIndex : integer;
begin
  for x:= low(cDataItemArray) to high(cDataItemArray) do
      if cDataItemArray[x].CompositeElement = TRUE then begin
         xIndex := Pos( cDataItemArray[x].Field, aFieldName);
         if xIndex > 0 then
            aFieldName := StringReplace(aFieldName, cDataItemArray[x].Field, cDataItemArray[x].DisplayName, [rfReplaceAll] );
      end;
  result := aFieldName;
end;
//------------------------------------------------------------------------------
constructor TMatrixDef.Create;
begin
  inherited;
  mCompositClassDef := TCompositClassDef.Create;
  New(mPrintRec);
  mPrintRec^ .GroupID:= -1;
end;
//------------------------------------------------------------------------------
destructor TMatrixDef.Destroy;
begin
  mCompositClassDef.Free;
  Dispose(mPrintRec);

  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetClassSettingsArr: TClassClearSettingsArr;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      result := TClassDef(FDataItem).ClassSettingsArr;
  end else
    FillChar(result,16,0);
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetCompositClassDef: TCompositClassDef;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TCompositClassDef then
       mCompositClassDef := TCompositClassDef(FDataItem);
  end else
    result := NIL;
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetFormula: String;
begin
  Result := StringReplace(MatrixDef, ',', ' ' ,[rfReplaceAll] );
  Result := ConvertFieldNameToDisplay(Result);
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetID: Integer;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      result := TClassDef(FDataItem).ID;
  end else
    result := -1;
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetMatrixDef: String;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      result := TClassDef(FDataItem).MatrixDef;
  end else
    result := '';
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetMatrixFromArray(
  aArray: TClassClearSettingsArr): String;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      result := TClassDef(FDataItem).GetMatrixFromArray(aArray);
  end else
    result := '';
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetMatrixType: TMatrixType;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      result := TClassDef(FDataItem).MatrixType;
  end else
    result := mtNone;
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetPrintRec: pPrintRec;
begin
  if assigned(FDataItem) then begin
     result := mPrintRec;
  end else
    result := NIL;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.Save(aNativeQuery: TNativeAdoQuery);
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then begin
      TClassDef(FDataItem).Save(aNativeQuery);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetClassSettingsArr(
  const Value: TClassClearSettingsArr);
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      TClassDef(FDataItem).ClassSettingsArr := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetCompositClassDef(const Value: TCompositClassDef);
begin
  if assigned(FDataItem) then begin
    if FDataItem is TCompositClassDef then
       mCompositClassDef := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetFormula(const Value: String);
begin
  MatrixDef := ConvertDisplayToFieldName(Value);
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetID(const Value: Integer);
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      TClassDef(FDataItem).ID := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetMatrixDef(const Value: String);
begin
 if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      TClassDef(FDataItem).MatrixDef := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetMatrixType(const Value: TMatrixType);
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      TClassDef(FDataItem).MatrixType := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetPrintRec(const Value: pPrintRec);
begin

  if not assigned( Value ) then exit;

  if not assigned( mPrintRec ) then
     New( mPrintRec );

  mPrintRec := Value;


end;
//------------------------------------------------------------------------------


{ TMatrixContainer }

constructor TMatrixContainer.Create;
begin
  inherited Create;
  mPrintList := TPrintList.Create;
end;
//------------------------------------------------------------------------------
procedure TMatrixContainer.DeleteData(aDataItem: TMatrixDef);
var xIndex : integer;
begin
  if Assigned (aDataItem) then begin
     //Daten aus Kontainer & DB loeschen
     xIndex := IndexOf(aDataItem);
     if xIndex >= 0 then begin
        if aDataItem.DataItemType.InheritsFrom(TClassDef) then begin
           TClassDef(aDataItem.DataItem).DeleteFromDatabase(NIL);
           Delete(xIndex);
        end;
     end;
   end;
end;
//------------------------------------------------------------------------------
destructor TMatrixContainer.Destroy;
begin
  inherited Destroy;
  mPrintList.Clear;
  mPrintList.Free;
end;
//------------------------------------------------------------------------------
function TMatrixContainer.GetMatrixCount: Word;
var x: integer;
begin
  x:= GetDataItemCount;
  if x < 0 then x:=0;
  Result:=x;
end;
//------------------------------------------------------------------------------
function TMatrixContainer.GetMatrixItems(aIndex: word): TMatrixDef;
begin
  try
    Result:= DataItems[aIndex] as TMatrixDef;
  except
    Result:= NIL;
  end;
end;
//------------------------------------------------------------------------------
function TMatrixContainer.GetPrintList: TPrintList;
begin
  result := mPrintList;
end;
//------------------------------------------------------------------------------
procedure TMatrixContainer.SetMatrixItems(aIndex: word; const Value: TMatrixDef);
var xMatrixDef : TMatrixDef;
begin
  if assigned(Value) then begin
     xMatrixDef := DataItems[aIndex]as TMatrixDef;
     xMatrixDef := Value;
  end;
end;
//------------------------------------------------------------------------------


{ TBaseItemFrame }

//------------------------------------------------------------------------------
function TBaseItemFrame.GetData: TMatrixDef;
begin
  Result := mData;
end;
//------------------------------------------------------------------------------
procedure TBaseItemFrame.Save(var aMatrixDef: TMatrixDef);
begin
 if not assigned(aMatrixDef) then exit;
end;
//------------------------------------------------------------------------------


{ TPrintList }

{ TPrintList }
//------------------------------------------------------------------------------
constructor TPrintList.Create(aPersistent: boolean);
begin
  inherited Create(aPersistent);
end;
//------------------------------------------------------------------------------
destructor TPrintList.Destroy;
begin
  inherited;    
end;
//------------------------------------------------------------------------------
procedure TMatrixContainer.SetPrintList(const Value: TPrintList);
begin
  if Assigned(Value) then
     mPrintList := Value;
end;
//------------------------------------------------------------------------------
function TPrintList.GetCount: Word;
var x: integer;
begin
  x:= GetDataItemCount;
  if x < 0 then x:=0;
  Result:=x;
end;
//------------------------------------------------------------------------------
function TPrintList.GetPrintItems(aIndex: word): TMatrixDef;
begin
  try
    Result:= DataItems[aIndex] as TMatrixDef;
  except
    Result:= NIL;
  end;
end;
//------------------------------------------------------------------------------
procedure TPrintList.SetPrintItems(aIndex: word; const Value: TMatrixDef);
var xMatrixDef : TMatrixDef;
begin
  if assigned(Value) then begin
     xMatrixDef := DataItems[aIndex]as TMatrixDef;
     xMatrixDef := Value;
  end;
end;
//------------------------------------------------------------------------------
end.
