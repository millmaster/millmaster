object ClassNameInputDialog: TClassNameInputDialog
  Left = 291
  Top = 243
  Width = 336
  Height = 153
  Caption = 'ClassNameInputDialog'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Shell Dlg 2'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object mmStaticText2: TmmStaticText
    Left = 24
    Top = 8
    Width = 121
    Height = 17
    Caption = 'Name der neuen Gruppe'
    TabOrder = 4
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmStaticText1: TmmStaticText
    Left = 24
    Top = 48
    Width = 116
    Height = 17
    Caption = 'Name der neuen Klasse'
    TabOrder = 2
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object edName: TmmEdit
    Left = 24
    Top = 64
    Width = 281
    Height = 21
    Color = clWindow
    TabOrder = 0
    Visible = True
    AutoLabel.LabelPosition = lpLeft
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object Panel1: TPanel
    Left = 0
    Top = 97
    Width = 328
    Height = 29
    Align = alBottom
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 1
    object Panel2: TPanel
      Left = 105
      Top = 0
      Width = 223
      Height = 29
      Align = alRight
      BevelOuter = bvNone
      Caption = ' '
      TabOrder = 0
      object butCancel: TBitBtn
        Tag = 74
        Left = 128
        Top = 2
        Width = 91
        Height = 25
        Cancel = True
        Caption = '&Abbruch'
        ModalResult = 2
        TabOrder = 0
        OnClick = butCancelClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333F3933333333333993338FF33333333388339933333333
          393333388F33333333833399933333339933333888F333333883333999333339
          93333333888F33338833333399933399933333333888F3388833333399933999
          333333333888FF88833333333999999333333333338888883333333333999933
          33333333333888833333333333999933333333333338888FF333333339999993
          3333333333888888FF33333399993999933333333888838888F3333999933399
          9933333388883338888F33999333333399933338883333333888399933333333
          3393338883333333333899333333333333333883333333333333}
        NumGlyphs = 2
      end
      object butOk: TBitBtn
        Tag = 100
        Left = 32
        Top = 2
        Width = 91
        Height = 25
        Caption = '&OK'
        Default = True
        ModalResult = 1
        TabOrder = 1
        OnClick = butCancelClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          555555555555555555555555555555555555555555FF55555555555552055555
          55555555588FF5555555555522205555555555558888F5555555555522205555
          555555558888FF5555555552222205555555555888888F555555552222220555
          5555558888888FF5555558220522205555555888858888F55555820555522055
          55558885555888FF5555555555522205555555555558888F5555555555552205
          555555555555888FF5555555555552205555555555555888FF55555555555582
          05555555555555888FF5555555555558205555555555555888FF555555555555
          5220555555555555588855555555555555555555555555555555}
        NumGlyphs = 2
      end
    end
  end
  object edGroup: TmmComboBox
    Left = 24
    Top = 24
    Width = 281
    Height = 21
    Color = clWindow
    ItemHeight = 13
    TabOrder = 3
    Visible = True
    AutoLabel.LabelPosition = lpLeft
    Edit = True
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
end
