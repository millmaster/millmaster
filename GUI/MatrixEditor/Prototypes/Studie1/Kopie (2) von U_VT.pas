unit U_VT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QualityMatrixBase, QualityMatrix, ExtCtrls, mmSplitter, mmPanel,
  VirtualTrees, StdCtrls, mmCheckBox, mmGroupBox, Buttons, mmSpeedButton,
  mmStaticText, fcCombo, fcColorCombo, fctreecombo, mmBevel, fcTreeView,
  ImgList, mmImageList;

type
  (*  Klasse:        TfrmVT
   *  Vorg�nger:     TForm
   *  Kategorie:     Undefined
   *  Kurzbeschrieb: Formular f�r den Editor 
   *  Beschreibung:  - *)
  // 1 Formular f�r den Editor 
  TfrmVT = class (TForm)
    bFullCollapse: TmmSpeedButton;
    bFullExp: TmmSpeedButton;
    bInitTree: TmmSpeedButton;
    bVTFullCollapse: TmmSpeedButton;
    bVTFullExpand: TmmSpeedButton;
    bVTInitTree: TmmSpeedButton;
    cbBold: TmmCheckBox;
    cbClassCollor: TmmCheckBox;
    cbClasses: TfcTreeCombo;
    cbGridLines: TmmCheckBox;
    cbImages: TmmCheckBox;
    cbNodeItalic: TmmCheckBox;
    cbSwimmlanes: TmmCheckBox;
    fcColorCombo1: TfcColorCombo;
    imTreeImages: TmmImageList;
    mmBevel1: TmmBevel;
    mmGroupBox1: TmmGroupBox;
    mmGroupBox2: TmmGroupBox;
    mmPanel1: TmmPanel;
    mmPanel2: TmmPanel;
    mmSplitter1: TmmSplitter;
    mmStaticText1: TmmStaticText;
    mmStaticText2: TmmStaticText;
    qm: TQualityMatrix;
    vst: TVirtualStringTree;
    // 1 Ruft den Farbendialog auf 
    procedure bColorClick(Sender: TObject);
    // 1 Klappt das TreeView der Combobox zusammen 
    procedure bFullCollapseClick(Sender: TObject);
    // 1 Klappt das TreeView der ComboBox auf 
    procedure bFullExpClick(Sender: TObject);
    // 1 Klappt die erste Ebene des TreeViews der ComboBox auf 
    procedure bInitTreeClick(Sender: TObject);
    // 1 Klappt das VirtualTreeView zu 
    procedure bVTFullCollapseClick(Sender: TObject);
    // 1 Klappt das VirtualTreeView auf 
    procedure bVTFullExpandClick(Sender: TObject);
    // 1 Klappt die erste Ebene des VirtualTreeViews auf 
    procedure bVTInitTreeClick(Sender: TObject);
    // 1 Jede zweite Zeile des TreeVies einf�rben 
    procedure cbClassCollorClick(Sender: TObject);
    // 1 Setzt Schrift und Hintergrundfarbe des TreeViews der ComboBox 
    procedure cbClassesCalcNodeAttributes(TreeView: TfcCustomTreeView; Node: TfcTreeNode; State: TfcItemStates);
    // 1 Wird gefeuert, wenn ein Tree aufgeklappt wird (ComboBox) 
    procedure cbClassesExpand(TreeView: TfcCustomTreeView; Node: TfcTreeNode);
    // 1 Blendet die Gitternetzlinien des VirtualTreeView ein 
    procedure cbGridLinesClick(Sender: TObject);
    // 1 Setzt f�r jede Klassendefinition ein Icon 
    procedure cbImagesClick(Sender: TObject);
    // 1 Schlaltet Kursiv f�r Folders ein oder aus 
    procedure cbNodeItalicClick(Sender: TObject);
    // 1 Schaltet die Vertikale Einf�rbung f�r das VirtualTreeView ein oder aus 
    procedure cbSwimmlanesClick(Sender: TObject);
    // 1 Legt die Farbe der Zeilenf�rbung fest 
    procedure fcColorCombo1Change(Sender: TObject);
    // 1 Initialisierung - Erzeugt die Nodes 
    procedure FormCreate(Sender: TObject);
    // 1 Legt die Hintergrundfarbe des Nodes fest 
    procedure vstBeforeItemErase(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; ItemRect: TRect; 
      var ItemColor: TColor; var EraseAction: TItemEraseAction);
    // 1 Wird aufgerufen, wenn die Selktion �ndert 
    procedure vstChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
    // 1 Wird gefeuert, wenn das VirtualTreeView einen Node freigibt 
    procedure vstFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    // 1 Wird aufgerufen, wenn f�r einen Node des VirtualTreeViews das Icon abgefragt wird 
    procedure vstGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex; 
      var Ghosted: Boolean; var ImageIndex: Integer);
    // 1 Wird f�r jeden Knoten und jede Spalte aufgerufen 
    procedure vstGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; 
      var CellText: WideString);
    // 1 Hier kann der Font f�r den Text des Nodes gesetzt werden (Pro Spalte) 
    procedure vstPaintText(Sender: TBaseVirtualTree; const TargetCanvas: TCanvas; Node: PVirtualNode; Column: 
      TColumnIndex; TextType: TVSTTextType);
  private
    mLineColor: TColor;
  end;
  
  PTreeData = ^TTreeData;
  TTreeData = record
    Name: String;
    Count: integer;
    Classes: string;
  end;


var
  frmVT: TfrmVT;

implementation

uses
  YMParaDef;

{$R *.DFM}

function AddVSTStructure(AVST: TCustomVirtualStringTree; ANode: PVirtualNode;
  ARecord: PTreeData): PVirtualNode;
var
  Data: PTreeData;
begin
  Result:=AVST.AddChild(ANode);
  Data:=AVST.GetNodeData(Result);
  Avst.ValidateNode(Result, False);
  Data^.Name:=ARecord.Name;
  Data^.Count:=ARecord.Count;
  Data^.Classes:=ARecord.Classes;
end;

(*
  Klasse:        TfrmVT
 *  Vorg�nger:     TForm
 *  Kategorie:     Undefined
 *  Kurzbeschrieb: Field mLineColor 
 *  Beschreibung:  - 
 *)
 
//:-------------------------------------------------------------------
(*  Member:           bColorClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Ruft den Farbendialog auf
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.bColorClick(Sender: TObject);
begin
  with TColorDialog.Create(self) do try
    Color := mLineColor;
  
    if Execute then begin
      mLineColor := Color;
      vst.Invalidate;
      fcColorCombo1.SelectedColor := mLineColor;
    end;//
  finally
    free;
  end;
end;// TfrmVT.bColorClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           bFullCollapseClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Klappt das TreeView der Combobox zusammen
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.bFullCollapseClick(Sender: TObject);
begin
  cbClasses.Items[0].Collapse(true);
  cbClasses.Items[0].GetNextSibling.Collapse(true);
end;// TfrmVT.bFullCollapseClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           bFullExpClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Klappt das TreeView der ComboBox auf
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.bFullExpClick(Sender: TObject);
begin
  cbClasses.Items[0].Expand(true);
  cbClasses.Items[0].GetNextSibling.Expand(true);
end;// TfrmVT.bFullExpClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           bInitTreeClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Klappt die erste Ebene des TreeViews der ComboBox auf
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.bInitTreeClick(Sender: TObject);
begin
  bFullCollapseClick(sender);
  cbClasses.Items[0].Expand(false);
  cbClasses.Items[0].GetNextSibling.Expand(false);
end;// TfrmVT.bInitTreeClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           bVTFullCollapseClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Klappt das VirtualTreeView zu
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.bVTFullCollapseClick(Sender: TObject);
begin
  vst.FullCollapse;
end;// TfrmVT.bVTFullCollapseClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           bVTFullExpandClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Klappt das VirtualTreeView auf
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.bVTFullExpandClick(Sender: TObject);
begin
  vst.FullExpand;
end;// TfrmVT.bVTFullExpandClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           bVTInitTreeClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Klappt die erste Ebene des VirtualTreeViews auf
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.bVTInitTreeClick(Sender: TObject);
var
  xNode: PVirtualNode;
begin
  vst.FullCollapse;
  xNode := vst.GetFirst;
  vst.Expanded[xNode] := true;
  xNode := vst.GetNextSibling(xNode);
  vst.Expanded[xNode] := true;
end;// TfrmVT.bVTInitTreeClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           cbClassCollorClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Jede zweite Zeile des TreeVies einf�rben
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.cbClassCollorClick(Sender: TObject);
begin
  vst.invalidate;
end;// TfrmVT.cbClassCollorClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           cbClassesCalcNodeAttributes
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (TreeView, Node, State)
 *
 *  Kurzbeschreibung: Setzt Schrift und Hintergrundfarbe des TreeViews der ComboBox
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.cbClassesCalcNodeAttributes(TreeView: TfcCustomTreeView; Node: TfcTreeNode; State: TfcItemStates);
var
  xItemHeight: Integer;
  xVisibleItemIndex: Integer;
  xRect: TRect;
begin
  if cbClassCollor.Checked then begin
    xRect := node.DisplayRect(false);
    xItemHeight := xRect.Bottom - xRect.Top;
    xVisibleItemIndex := xRect.Top div xItemHeight;
    if not(odd(xVisibleItemIndex)) then
      TreeView.Canvas.Brush.Color := mLineColor
    else
      TreeView.Canvas.Brush.Color := clWhite;
  end;// if cbClassCollor.Checked then begin
  if cbNodeItalic.Checked then begin
    if not(Node.HasChildren) then
      TreeView.Canvas.Font.Style := TreeView.Canvas.Font.Style + [fsItalic];
  end;
  if cbBold.Checked then begin
    if Node.HasChildren then
      TreeView.Canvas.Font.Style := TreeView.Canvas.Font.Style + [fsBold];
  end;
end;// TfrmVT.cbClassesCalcNodeAttributes cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           cbClassesExpand
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (TreeView, Node)
 *
 *  Kurzbeschreibung: Wird gefeuert, wenn ein Tree aufgeklappt wird (ComboBox)
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.cbClassesExpand(TreeView: TfcCustomTreeView; Node: TfcTreeNode);
begin
  cbClasses.TreeView.Invalidate;
end;// TfrmVT.cbClassesExpand cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           cbGridLinesClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Blendet die Gitternetzlinien des VirtualTreeView ein
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.cbGridLinesClick(Sender: TObject);
begin
  if toShowHorzGridLines in vst.TreeOptions.PaintOptions then begin
    vst.TreeOptions.PaintOptions := vst.TreeOptions.PaintOptions - [toShowHorzGridLines];
    vst.TreeOptions.PaintOptions := vst.TreeOptions.PaintOptions - [toShowVertGridLines];
  end else begin
    vst.TreeOptions.PaintOptions := vst.TreeOptions.PaintOptions + [toShowHorzGridLines];
    vst.TreeOptions.PaintOptions := vst.TreeOptions.PaintOptions + [toShowVertGridLines];
  end;//
end;// TfrmVT.cbGridLinesClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           cbImagesClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Setzt f�r jede Klassendefinition ein Icon
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.cbImagesClick(Sender: TObject);
begin
  if cbImages.Checked then begin
    vst.Images := imTreeImages;
    cbClasses.Images := imTreeImages;
  end else begin
    vst.Images := nil;
    cbClasses.Images := nil;
  end;//
end;// TfrmVT.cbImagesClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           cbNodeItalicClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Schlaltet Kursiv f�r Folders ein oder aus
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.cbNodeItalicClick(Sender: TObject);
begin
  vst.Invalidate;
end;// TfrmVT.cbNodeItalicClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           cbSwimmlanesClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Schaltet die Vertikale Einf�rbung f�r das VirtualTreeView ein oder aus
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.cbSwimmlanesClick(Sender: TObject);
begin
  if cbSwimmlanes.Checked then begin
    vst.Header.Columns[0].Color := $FBFBFB;
    vst.Header.Columns[3].Color := $E2E7E9;
  end else begin
    vst.Header.Columns[0].Color := clWindow;
    vst.Header.Columns[0].Options := vst.Header.Columns[0].Options + [coParentColor];
    vst.Header.Columns[3].Color := clWindow;
    vst.Header.Columns[3].Options := vst.Header.Columns[3].Options + [coParentColor];
  end;
end;// TfrmVT.cbSwimmlanesClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           fcColorCombo1Change
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Legt die Farbe der Zeilenf�rbung fest
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.fcColorCombo1Change(Sender: TObject);
begin
  mLineColor := fcColorCombo1.SelectedColor;
  vst.Invalidate;
end;// TfrmVT.fcColorCombo1Change cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           FormCreate
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Initialisierung - Erzeugt die Nodes
 *  Beschreibung:     Die Nodes des VirtualTreeViews k�nnen erst zur Laufzeit erzeugt werden
 ************************************************************************)
procedure TfrmVT.FormCreate(Sender: TObject);
var
  xNode: PVirtualNode;
  xNode1: PVirtualNode;
  
  function SetRecord(aName:string; aCount: integer; aClasses: string):PTreeData;
  begin
    result := new(PTreeData);
    with result^ do begin
      Name := aName;
      Count := aCount;
      Classes := aClasses;
    end;// with result^ do begin
  end;// function SetRecord(aName:string; aCount: integer; aClasses: string):PTreeData;
  
begin
  mLineColor := $D3F0FB;
  VST.NodeDataSize:=SizeOf(TTreeData);
  VST.BeginUpdate;

//: ----------------------------------------------
  xNode1 := AddVSTStructure(VST,nil,SetRecord('Uster Klassen',0,''));
    xNode := AddVSTStructure(VST,xNode1,SetRecord('A',0,''));
      AddVSTStructure(VST,xNode,SetRecord('1',1,'A1'));
      AddVSTStructure(VST,xNode,SetRecord('2',1,'A2'));
      AddVSTStructure(VST,xNode,SetRecord('3',1,'A3'));
      AddVSTStructure(VST,xNode,SetRecord('4',1,'A4'));
  
    xNode := AddVSTStructure(VST,xNode1,SetRecord('B',0,''));
      AddVSTStructure(VST,xNode,SetRecord('1',1,'B1'));
      AddVSTStructure(VST,xNode,SetRecord('2',1,'B2'));
      AddVSTStructure(VST,xNode,SetRecord('3',1,'B3'));
      AddVSTStructure(VST,xNode,SetRecord('4',1,'B4'));
  
    xNode := AddVSTStructure(VST,xNode1,SetRecord('C',0,''));
      AddVSTStructure(VST,xNode,SetRecord('1',1,'C1'));
      AddVSTStructure(VST,xNode,SetRecord('2',1,'C2'));
      AddVSTStructure(VST,xNode,SetRecord('3',1,'C3'));
      AddVSTStructure(VST,xNode,SetRecord('4',1,'C4'));
  
    xNode := AddVSTStructure(VST,xNode1,SetRecord('D',0,''));
      AddVSTStructure(VST,xNode,SetRecord('1',1,'D1'));
      AddVSTStructure(VST,xNode,SetRecord('2',1,'D2'));
      AddVSTStructure(VST,xNode,SetRecord('3',1,'D3'));
      AddVSTStructure(VST,xNode,SetRecord('4',1,'D4'));
  
    AddVSTStructure(VST,xNode1,SetRecord('E',1,''));
    AddVSTStructure(VST,xNode1,SetRecord('F',1,''));
    AddVSTStructure(VST,xNode1,SetRecord('G',1,''));
  
    xNode := AddVSTStructure(VST,xNode1,SetRecord('H',0,''));
      AddVSTStructure(VST,xNode,SetRecord('1',1,'H1'));
      AddVSTStructure(VST,xNode,SetRecord('2',1,'H2'));
  
    xNode := AddVSTStructure(VST,xNode1,SetRecord('I',0,''));
      AddVSTStructure(VST,xNode,SetRecord('1',1,'I1'));
      AddVSTStructure(VST,xNode,SetRecord('2',1,'I2'));
  
  xNode1 := AddVSTStructure(VST,nil,SetRecord('Imperfektionen',0,''));
    AddVSTStructure(VST,xNode1,SetRecord('I2-4',24,'C1 - C4, ...'));
    AddVSTStructure(VST,xNode1,SetRecord('I4-8',24,'D1 - D4, ...'));
    AddVSTStructure(VST,xNode1,SetRecord('I8-20',24,'...'));
    AddVSTStructure(VST,xNode1,SetRecord('I20-70',16,'...'));
  
  AddVSTStructure(VST,nil,SetRecord('Eigene Klassen',0,''));

//: ----------------------------------------------
  VST.EndUpdate;
  bVTInitTreeClick(nil);
  bInitTreeClick(nil);
  cbClasses.TreeView.OnCollapsed  := cbClassesExpand;
  cbClasses.TreeView.OnExpanded   := cbClassesExpand;
  fcColorCombo1.SelectedColor := mLineColor;
end;// TfrmVT.FormCreate cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           vstBeforeItemErase
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender, TargetCanvas, Node, ItemRect, ItemColor, EraseAction)
 *
 *  Kurzbeschreibung: Legt die Hintergrundfarbe des Nodes fest
 *  Beschreibung:     Hier kann die Hintergrundfarbe jedes Nodes festgelegt werden. Die �bergebene 
                      Farbe wird verwendet um den Hintergerund zu l�schen.
 ************************************************************************)
procedure TfrmVT.vstBeforeItemErase(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; ItemRect: 
  TRect; var ItemColor: TColor; var EraseAction: TItemEraseAction);
var
  xItemHeight: Integer;
  xVisibleItemIndex: Integer;
begin
  if cbClassCollor.Checked then begin
    xItemHeight := ItemRect.Bottom - ItemRect.Top;
    xVisibleItemIndex := ItemRect.Top div xItemHeight;
  
    if not(Odd(xVisibleItemIndex)) then begin
      ItemColor := mLineColor;
      EraseAction := eaColor;
    end;// if Odd(xVisibleItemIndex) then begin
  end;// if cbClassCollor.Checked then begin
end;// TfrmVT.vstBeforeItemErase cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           vstChange
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender, Node)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn die Selktion �ndert
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.vstChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  i: Integer;
  xData: PTreeData;
  xClassClearSettingsArr: TClassClearSettingsArr;
  
  procedure SetABCDE(aIndex: integer; aValue: integer);
  begin
    xClassClearSettingsArr[(aIndex - 1) * 2] := aValue;
    xClassClearSettingsArr[(aIndex - 1) * 2 + 1] := aValue;
  end;// SetABCDE(aIndex: integer; aValue: integer);
  
  procedure SetFG(aValue: integer);
  begin
    xClassClearSettingsArr[13] := aValue;
    xClassClearSettingsArr[14] := aValue;
    xClassClearSettingsArr[15] := aValue;
  end;// procedure SetFG(aValue: integer);
  
  procedure SetHI(aIndex: integer; aValue: integer);
  begin
    case aIndex of
      1: begin
           xClassClearSettingsArr[9]  := aValue;
           xClassClearSettingsArr[10] := aValue;
      end;
      2: xClassClearSettingsArr[8] := aValue;
    end;//
  end;// procedure SetHI(aIndex: integer; aValue: integer);
  
  const
    cA = $02;
    cB = $0c;
    cC = $30;
    cD = $40;
    cE = $80;
    cF = $38;
    cG = $c0;
    cH = $30;
    cI = $c0;
  
begin
  FillChar(xClassClearSettingsArr,16,0);
  
  xData:=VST.GetNodeData(vst.FocusedNode);
  if assigned(xData) then begin
    case vst.AbsoluteIndex(vst.FocusedNode) of
      // A
      2,3,4,5     : SetABCDE(vst.AbsoluteIndex(vst.FocusedNode) - 1, cA);
      // B
      7,8,9,10    : SetABCDE(vst.AbsoluteIndex(vst.FocusedNode) - 6, cB);
      // C
      12,13,14,15 : SetABCDE(vst.AbsoluteIndex(vst.FocusedNode) - 11, cC);
      // D
      17,18,19,20 : SetABCDE(vst.AbsoluteIndex(vst.FocusedNode) - 16, cD);
      // E
      21          : SetABCDE(1, cE);
      // F
      22          : SetFG(cF);
      // G
      23          : SetFG(cG);
      // H
      25,26       : SetHI(vst.AbsoluteIndex(vst.FocusedNode) - 24, cH);
      // I
      28,29       : SetHI(vst.AbsoluteIndex(vst.FocusedNode) - 27, cI);
      // I2-4
      31 : begin
        for i := 1 to 4 do
          SetABCDE(i,cC);
        for i := 8 to 15 do
          xClassClearSettingsArr[i]  := 1;
      end;
      // I4-8
      32 : begin
        for i := 1 to 4 do
          SetABCDE(i,cD);
        for i := 8 to 15 do
          xClassClearSettingsArr[i]  := 6;
      end;
      // I8-20
      33 : begin
        for i := 8 to 15 do
          xClassClearSettingsArr[i]  := $18;
      end;
      // I20 -70
      34 : begin
        for i := 8 to 15 do
          xClassClearSettingsArr[i]  := $60;
      end;
    else ;
    end;//
  end;//
  qm.SetFieldStateTable(xClassClearSettingsArr);
end;// TfrmVT.vstChange cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           vstFreeNode
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender, Node)
 *
 *  Kurzbeschreibung: Wird gefeuert, wenn das VirtualTreeView einen Node freigibt
 *  Beschreibung:     Hier m�ssen alle Elemente des Records freigegeben werden die mit einem
                      Pointer realisiert sind (Strings, Dynamische Array's, Klassen)
 ************************************************************************)
procedure TfrmVT.vstFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  xData: PTreeData;
begin
  xData:=VST.GetNodeData(Node);
  if Assigned(xData) then begin
    Finalize(xData^);
  end;
end;// TfrmVT.vstFreeNode cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           vstGetImageIndex
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender, Node, Kind, Column, Ghosted, ImageIndex)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn f�r einen Node des VirtualTreeViews das Icon abgefragt wird
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.vstGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: 
  TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
begin
  if (Node.ChildCount = 0) and (Column = 0) then
    ImageIndex := 0;
  
  if (Sender.FocusedNode = Node) and (Column = 3) then
    ImageIndex := 2;
end;// TfrmVT.vstGetImageIndex cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           vstGetText
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender, Node, Column, TextType, CellText)
 *
 *  Kurzbeschreibung: Wird f�r jeden Knoten und jede Spalte aufgerufen
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.vstGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; 
  var CellText: WideString);
var
  xData: PTreeData;
begin
  xData:=VST.GetNodeData(Node);
  
  case Column of
    0: CellText := xData.Name;
    1: begin
      if xData.Count = 0 then
        CellText := ''
      else
        CellText := IntToStr(xData.Count);
    end;
    2: CellText := xData.Classes;
    3: CellText := '';
  end;
end;// TfrmVT.vstGetText cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           vstPaintText
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender, TargetCanvas, Node, Column, TextType)
 *
 *  Kurzbeschreibung: Hier kann der Font f�r den Text des Nodes gesetzt werden (Pro Spalte)
 *  Beschreibung:     -
 ************************************************************************)
procedure TfrmVT.vstPaintText(Sender: TBaseVirtualTree; const TargetCanvas: TCanvas; Node: PVirtualNode; Column: 
  TColumnIndex; TextType: TVSTTextType);
begin
  if cbNodeItalic.Checked then begin
    with TargetCanvas do begin
      if node.ChildCount > 0 then
        Font.Style := Font.Style + [fsItalic]
    end;// with TargetCanvas do begin
  end;// if cbNodeItalic.Checked then begin
  
  if cbBold.Checked then begin
    with TargetCanvas do begin
      if node.ChildCount = 0 then
        Font.Style := Font.Style + [fsBold]
    end;// with TargetCanvas do begin
  end;// if cbBold.Checked then begin
end;// TfrmVT.vstPaintText cat:Undefined

end.

