unit U_VT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QualityMatrixBase, QualityMatrix, ExtCtrls, mmSplitter, mmPanel,
  VirtualTrees, StdCtrls, mmCheckBox, mmGroupBox, Buttons, mmSpeedButton,
  mmStaticText, fcCombo, fcColorCombo, fctreecombo, mmBevel, fcTreeView,
  ImgList, mmImageList;

type
  TfrmVT = class(TForm)
    mmPanel1: TmmPanel;
    mmPanel2: TmmPanel;
    mmSplitter1: TmmSplitter;
    qm: TQualityMatrix;
    vst: TVirtualStringTree;
    cbGridLines: TmmCheckBox;
    mmSpeedButton1: TmmSpeedButton;
    mmSpeedButton2: TmmSpeedButton;
    bInitExpand: TmmSpeedButton;
    mmStaticText1: TmmStaticText;
    mmGroupBox1: TmmGroupBox;
    mmSpeedButton3: TmmSpeedButton;
    cbClassCollor: TmmCheckBox;
    fcColorCombo1: TfcColorCombo;
    mmGroupBox2: TmmGroupBox;
    cbClasses: TfcTreeCombo;
    mmStaticText2: TmmStaticText;
    mmBevel1: TmmBevel;
    bFullExp: TmmSpeedButton;
    bFullCollapse: TmmSpeedButton;
    bInitTree: TmmSpeedButton;
    imTreeImages: TmmImageList;
    cbImages: TmmCheckBox;
    cbNodeItalic: TmmCheckBox;
    cbBold: TmmCheckBox;
    cbSwimmlanes: TmmCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure vstFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure vstGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure vstChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure cbGridLinesClick(Sender: TObject);
    procedure vstBeforeItemErase(Sender: TBaseVirtualTree;
      TargetCanvas: TCanvas; Node: PVirtualNode; ItemRect: TRect;
      var ItemColor: TColor; var EraseAction: TItemEraseAction);
    procedure cbClassCollorClick(Sender: TObject);
    procedure mmSpeedButton1Click(Sender: TObject);
    procedure mmSpeedButton2Click(Sender: TObject);
    procedure bInitExpandClick(Sender: TObject);
    procedure mmSpeedButton3Click(Sender: TObject);
    procedure fcColorCombo1Change(Sender: TObject);
    procedure bFullExpClick(Sender: TObject);
    procedure bFullCollapseClick(Sender: TObject);
    procedure bInitTreeClick(Sender: TObject);
    procedure cbClassesCalcNodeAttributes(TreeView: TfcCustomTreeView;
      Node: TfcTreeNode; State: TfcItemStates);
    procedure cbClassesExpand(TreeView: TfcCustomTreeView;
      Node: TfcTreeNode);
    procedure vstGetImageIndex(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex;
      var Ghosted: Boolean; var ImageIndex: Integer);
    procedure cbImagesClick(Sender: TObject);
    procedure vstPaintText(Sender: TBaseVirtualTree;
      const TargetCanvas: TCanvas; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType);
    procedure cbNodeItalicClick(Sender: TObject);
    procedure cbSwimmlanesClick(Sender: TObject);
(*    procedure VSTGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: Integer; TextType: TVSTTextType; var Text: WideString);*)
  private
    mLineColor : TColor;
    { Private declarations }
  public
    { Public declarations }
  end;

  PTreeData = ^TTreeData;
  TTreeData = record
    Name: String;
    Count: integer;
    Classes: string;
  end;


var
  frmVT: TfrmVT;

implementation

uses
  YMParaDef;

{$R *.DFM}

function AddVSTStructure(AVST: TCustomVirtualStringTree; ANode: PVirtualNode;
  ARecord: PTreeData): PVirtualNode;
var
  Data: PTreeData;
begin
  Result:=AVST.AddChild(ANode);
  Data:=AVST.GetNodeData(Result);
  Avst.ValidateNode(Result, False);
  Data^.Name:=ARecord.Name;
  Data^.Count:=ARecord.Count;
  Data^.Classes:=ARecord.Classes;
end;

procedure TfrmVT.FormCreate(Sender: TObject);
var
  xNode : PVirtualNode;
  xNode1 : PVirtualNode;
  function SetRecord(aName:string; aCount: integer; aClasses: string):PTreeData;
  begin
    result := new(PTreeData);
    with result^ do begin
      Name := aName;
      Count := aCount;
      Classes := aClasses;
    end;// with result^ do begin
  end;// function SetRecord(aName:string; aCount: integer; aClasses: string):PTreeData;
begin
  mLineColor := $D3F0FB;
  VST.NodeDataSize:=SizeOf(TTreeData);
  VST.BeginUpdate;

  xNode1 := AddVSTStructure(VST,nil,SetRecord('Uster Klassen',0,''));
    xNode := AddVSTStructure(VST,xNode1,SetRecord('A',0,''));
      AddVSTStructure(VST,xNode,SetRecord('1',1,'A1'));
      AddVSTStructure(VST,xNode,SetRecord('2',1,'A2'));
      AddVSTStructure(VST,xNode,SetRecord('3',1,'A3'));
      AddVSTStructure(VST,xNode,SetRecord('4',1,'A4'));

    xNode := AddVSTStructure(VST,xNode1,SetRecord('B',0,''));
      AddVSTStructure(VST,xNode,SetRecord('1',1,'B1'));
      AddVSTStructure(VST,xNode,SetRecord('2',1,'B2'));
      AddVSTStructure(VST,xNode,SetRecord('3',1,'B3'));
      AddVSTStructure(VST,xNode,SetRecord('4',1,'B4'));

    xNode := AddVSTStructure(VST,xNode1,SetRecord('C',0,''));
      AddVSTStructure(VST,xNode,SetRecord('1',1,'C1'));
      AddVSTStructure(VST,xNode,SetRecord('2',1,'C2'));
      AddVSTStructure(VST,xNode,SetRecord('3',1,'C3'));
      AddVSTStructure(VST,xNode,SetRecord('4',1,'C4'));

    xNode := AddVSTStructure(VST,xNode1,SetRecord('D',0,''));
      AddVSTStructure(VST,xNode,SetRecord('1',1,'D1'));
      AddVSTStructure(VST,xNode,SetRecord('2',1,'D2'));
      AddVSTStructure(VST,xNode,SetRecord('3',1,'D3'));
      AddVSTStructure(VST,xNode,SetRecord('4',1,'D4'));

    AddVSTStructure(VST,xNode1,SetRecord('E',1,''));
    AddVSTStructure(VST,xNode1,SetRecord('F',1,''));
    AddVSTStructure(VST,xNode1,SetRecord('G',1,''));

    xNode := AddVSTStructure(VST,xNode1,SetRecord('H',0,''));
      AddVSTStructure(VST,xNode,SetRecord('1',1,'H1'));
      AddVSTStructure(VST,xNode,SetRecord('2',1,'H2'));

    xNode := AddVSTStructure(VST,xNode1,SetRecord('I',0,''));
      AddVSTStructure(VST,xNode,SetRecord('1',1,'I1'));
      AddVSTStructure(VST,xNode,SetRecord('2',1,'I2'));

  xNode1 := AddVSTStructure(VST,nil,SetRecord('Imperfektionen',0,''));
    AddVSTStructure(VST,xNode1,SetRecord('I2-4',24,'C1 - C4, ...'));
    AddVSTStructure(VST,xNode1,SetRecord('I4-8',24,'D1 - D4, ...'));
    AddVSTStructure(VST,xNode1,SetRecord('I8-20',24,'...'));
    AddVSTStructure(VST,xNode1,SetRecord('I20-70',16,'...'));


  AddVSTStructure(VST,nil,SetRecord('Eigene Klassen',0,''));

  VST.EndUpdate;
  bInitExpandClick(nil);
  bInitTreeClick(nil);
  cbClasses.TreeView.OnCollapsed  := cbClassesExpand;
  cbClasses.TreeView.OnExpanded   := cbClassesExpand;
  fcColorCombo1.SelectedColor := mLineColor;
end;

procedure TfrmVT.vstFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  xData: PTreeData;
begin
  xData:=VST.GetNodeData(Node);
  if Assigned(xData) then begin
    Finalize(xData^);
  end;
end;

procedure TfrmVT.vstGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; TextType: TVSTTextType; var CellText: WideString);
var
  xData: PTreeData;
begin
  xData:=VST.GetNodeData(Node);

  case Column of
    0: CellText := xData.Name;
    1: begin
      if xData.Count = 0 then
        CellText := ''
      else
        CellText := IntToStr(xData.Count);
    end;
    2: CellText := xData.Classes;
    3: CellText := '';
  end;
end;

procedure TfrmVT.vstChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  i: integer;
  xData: PTreeData;
  xClassClearSettingsArr : TClassClearSettingsArr;
  procedure SetABCDE(aIndex: integer; aValue: integer);
  begin
    xClassClearSettingsArr[(aIndex - 1) * 2] := aValue;
    xClassClearSettingsArr[(aIndex - 1) * 2 + 1] := aValue;
  end;// SetABCDE(aIndex: integer; aValue: integer);

  procedure SetFG(aValue: integer);
  begin
    xClassClearSettingsArr[13] := aValue;
    xClassClearSettingsArr[14] := aValue;
    xClassClearSettingsArr[15] := aValue;
  end;// procedure SetFG(aValue: integer);

  procedure SetHI(aIndex: integer; aValue: integer);
  begin
    case aIndex of
      1: begin
           xClassClearSettingsArr[9]  := aValue;
           xClassClearSettingsArr[10] := aValue;
      end;
      2: xClassClearSettingsArr[8] := aValue;
    end;//
  end;// procedure SetHI(aIndex: integer; aValue: integer);

  const
    cA = $02;
    cB = $0c;
    cC = $30;
    cD = $40;
    cE = $80;
    cF = $38;
    cG = $c0;
    cH = $30;
    cI = $c0;
begin
  FillChar(xClassClearSettingsArr,16,0);

  xData:=VST.GetNodeData(vst.FocusedNode);
  if assigned(xData) then begin
    case vst.AbsoluteIndex(vst.FocusedNode) of
      // A
      2,3,4,5     : SetABCDE(vst.AbsoluteIndex(vst.FocusedNode) - 1, cA);
      // B
      7,8,9,10    : SetABCDE(vst.AbsoluteIndex(vst.FocusedNode) - 6, cB);
      // C
      12,13,14,15 : SetABCDE(vst.AbsoluteIndex(vst.FocusedNode) - 11, cC);
      // D
      17,18,19,20 : SetABCDE(vst.AbsoluteIndex(vst.FocusedNode) - 16, cD);
      // E
      21          : SetABCDE(1, cE);
      // F
      22          : SetFG(cF);
      // G
      23          : SetFG(cG);
      // H
      25,26       : SetHI(vst.AbsoluteIndex(vst.FocusedNode) - 24, cH);
      // I
      28,29       : SetHI(vst.AbsoluteIndex(vst.FocusedNode) - 27, cI);
      // I2-4
      31 : begin
        for i := 1 to 4 do
          SetABCDE(i,cC);
        for i := 8 to 15 do
          xClassClearSettingsArr[i]  := 1;
      end;
      // I4-8
      32 : begin
        for i := 1 to 4 do
          SetABCDE(i,cD);
        for i := 8 to 15 do
          xClassClearSettingsArr[i]  := 6;
      end;
      // I8-20
      33 : begin
        for i := 8 to 15 do
          xClassClearSettingsArr[i]  := $18;
      end;
      // I20 -70
      34 : begin
        for i := 8 to 15 do
          xClassClearSettingsArr[i]  := $60;
      end;
    else ;
    end;//
  end;//
  qm.SetFieldStateTable(xClassClearSettingsArr);
end;

procedure TfrmVT.cbGridLinesClick(Sender: TObject);
begin
  if toShowHorzGridLines in vst.TreeOptions.PaintOptions then begin
    vst.TreeOptions.PaintOptions := vst.TreeOptions.PaintOptions - [toShowHorzGridLines];
    vst.TreeOptions.PaintOptions := vst.TreeOptions.PaintOptions - [toShowVertGridLines];
  end else begin
    vst.TreeOptions.PaintOptions := vst.TreeOptions.PaintOptions + [toShowHorzGridLines];
    vst.TreeOptions.PaintOptions := vst.TreeOptions.PaintOptions + [toShowVertGridLines];
  end;//
end;

procedure TfrmVT.vstBeforeItemErase(Sender: TBaseVirtualTree;
  TargetCanvas: TCanvas; Node: PVirtualNode; ItemRect: TRect;
  var ItemColor: TColor; var EraseAction: TItemEraseAction);
var
  xItemHeight: integer;
  xVisibleItemIndex: integer;
begin
  if cbClassCollor.Checked then begin
    xItemHeight := ItemRect.Bottom - ItemRect.Top;
    xVisibleItemIndex := ItemRect.Top div xItemHeight;

    if not(Odd(xVisibleItemIndex)) then begin
      ItemColor := mLineColor;
      EraseAction := eaColor;
    end;// if Odd(xVisibleItemIndex) then begin
  end;// if cbClassCollor.Checked then begin
end;

procedure TfrmVT.cbClassesCalcNodeAttributes(TreeView: TfcCustomTreeView;
  Node: TfcTreeNode; State: TfcItemStates);
var
  xItemHeight: integer;
  xVisibleItemIndex: integer;
  xRect: TRect;
begin
  if cbClassCollor.Checked then begin
    xRect := node.DisplayRect(false);
    xItemHeight := xRect.Bottom - xRect.Top;
    xVisibleItemIndex := xRect.Top div xItemHeight;
    if not(odd(xVisibleItemIndex)) then
      TreeView.Canvas.Brush.Color := mLineColor
    else
      TreeView.Canvas.Brush.Color := clWhite;
  end;// if cbClassCollor.Checked then begin
  if cbNodeItalic.Checked then begin
    if not(Node.HasChildren) then
      TreeView.Canvas.Font.Style := TreeView.Canvas.Font.Style + [fsItalic];
  end;
  if cbBold.Checked then begin
    if Node.HasChildren then
      TreeView.Canvas.Font.Style := TreeView.Canvas.Font.Style + [fsBold];
  end;
end;

procedure TfrmVT.cbClassCollorClick(Sender: TObject);
begin
  vst.invalidate;
end;

procedure TfrmVT.mmSpeedButton1Click(Sender: TObject);
begin
  vst.FullExpand;
end;

procedure TfrmVT.mmSpeedButton2Click(Sender: TObject);
begin
  vst.FullCollapse;
end;

procedure TfrmVT.bInitExpandClick(Sender: TObject);
var
  xNode : PVirtualNode;
begin
  vst.FullCollapse;
  xNode := vst.GetFirst;
  vst.Expanded[xNode] := true;
  xNode := vst.GetNextSibling(xNode);
  vst.Expanded[xNode] := true;
end;

procedure TfrmVT.mmSpeedButton3Click(Sender: TObject);
begin
  with TColorDialog.Create(self) do try
    Color := mLineColor;

    if Execute then begin
      mLineColor := Color;
      vst.Invalidate;
      fcColorCombo1.SelectedColor := mLineColor;
    end;//
  finally
    free;
  end;
end;

procedure TfrmVT.fcColorCombo1Change(Sender: TObject);
begin
  mLineColor := fcColorCombo1.SelectedColor;
  vst.Invalidate;
end;

procedure TfrmVT.bFullExpClick(Sender: TObject);
begin
  cbClasses.Items[0].Expand(true);
  cbClasses.Items[0].GetNextSibling.Expand(true);
end;

procedure TfrmVT.bFullCollapseClick(Sender: TObject);
begin
  cbClasses.Items[0].Collapse(true);
  cbClasses.Items[0].GetNextSibling.Collapse(true);
end;

procedure TfrmVT.bInitTreeClick(Sender: TObject);
begin
  bFullCollapseClick(sender);
  cbClasses.Items[0].Expand(false);
  cbClasses.Items[0].GetNextSibling.Expand(false);
end;

procedure TfrmVT.cbClassesExpand(TreeView: TfcCustomTreeView;
  Node: TfcTreeNode);
begin
  cbClasses.TreeView.Invalidate;
end;

procedure TfrmVT.vstGetImageIndex(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex;
  var Ghosted: Boolean; var ImageIndex: Integer);
begin
  if (Node.ChildCount = 0) and (Column = 0) then
    ImageIndex := 0;

  if (Sender.FocusedNode = Node) and (Column = 3) then
    ImageIndex := 2;
end;

procedure TfrmVT.cbImagesClick(Sender: TObject);
begin
  if cbImages.Checked then begin
    vst.Images := imTreeImages;
    cbClasses.Images := imTreeImages;
  end else begin
    vst.Images := nil;
    cbClasses.Images := nil;
  end;//
end;

procedure TfrmVT.vstPaintText(Sender: TBaseVirtualTree;
  const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType);
begin
  if cbNodeItalic.Checked then begin
    with TargetCanvas do begin
      if node.ChildCount > 0 then
        Font.Style := Font.Style + [fsItalic]
    end;// with TargetCanvas do begin
  end;// if cbNodeItalic.Checked then begin

  if cbBold.Checked then begin
    with TargetCanvas do begin
      if node.ChildCount = 0 then
        Font.Style := Font.Style + [fsBold]
    end;// with TargetCanvas do begin
  end;// if cbBold.Checked then begin
end;

procedure TfrmVT.cbNodeItalicClick(Sender: TObject);
begin
  vst.Invalidate;
end;

procedure TfrmVT.cbSwimmlanesClick(Sender: TObject);
begin
  if cbSwimmlanes.Checked then begin
    vst.Header.Columns[0].Color := $FBFBFB;
    vst.Header.Columns[3].Color := $E2E7E9;
  end else begin
    vst.Header.Columns[0].Color := clWindow;
    vst.Header.Columns[0].Options := vst.Header.Columns[0].Options + [coParentColor];
    vst.Header.Columns[3].Color := clWindow;
    vst.Header.Columns[3].Options := vst.Header.Columns[3].Options + [coParentColor];
  end;
end;

end.
