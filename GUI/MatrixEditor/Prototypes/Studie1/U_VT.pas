unit U_VT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QualityMatrixBase, QualityMatrix, ExtCtrls, mmSplitter, mmPanel,
  VirtualTrees, StdCtrls, mmCheckBox, mmGroupBox, Buttons, mmSpeedButton,
  mmStaticText, fcCombo, fcColorCombo, fctreecombo, mmBevel, fcTreeView,
  ImgList, mmImageList, ClassDef, YMParaDef, Menus, VTHeaderPopup,
  mmBitBtn, ADODBAccess;

type
  TBuildItemsType = (biList, biTree);

  PTreeData = ^TTreeData;
  TTreeData = record
    DataItem : TBaseDataItem;
  end;// TTreeData = record

  // 1 Formular f�r den Editor 
  TfrmVT = class (TForm)
    bCreateNewClass: TmmSpeedButton;
    bDeleteClass: TmmSpeedButton;
    bFullCollapse: TmmSpeedButton;
    bFullExp: TmmSpeedButton;
    bInitTree: TmmSpeedButton;
    bQuery: TmmBitBtn;
    bVTFullCollapse: TmmSpeedButton;
    bVTFullExpand: TmmSpeedButton;
    bVTInitTree: TmmSpeedButton;
    cbBold: TmmCheckBox;
    cbClassCollor: TmmCheckBox;
    cbClasses: TfcTreeCombo;
    cbClassImages: TmmCheckBox;
    cbGridLines: TmmCheckBox;
    cbImages: TmmCheckBox;
    cbNodeItalic: TmmCheckBox;
    cbSwimmlanes: TmmCheckBox;
    fcColorCombo1: TfcColorCombo;
    imTreeImages: TmmImageList;
    mmBevel1: TmmBevel;
    mmGroupBox1: TmmGroupBox;
    mmGroupBox2: TmmGroupBox;
    mmPanel1: TmmPanel;
    mmPanel2: TmmPanel;
    mmPanel3: TmmPanel;
    mmPanel4: TmmPanel;
    mmSplitter1: TmmSplitter;
    mmStaticText1: TmmStaticText;
    mmStaticText2: TmmStaticText;
    qm: TQualityMatrix;
    vst: TVirtualStringTree;
    VTHeaderPopupMenu1: TVTHeaderPopupMenu;
    // 1 Ruft den Farbendialog auf 
    procedure bColorClick(Sender: TObject);
    // 1 Erzeugt eine neue Klassendefinition 
    procedure bCreateNewClassClick(Sender: TObject);
    procedure bDeleteClassClick(Sender: TObject);
    // 1 Klappt das TreeView der Combobox zusammen 
    procedure bFullCollapseClick(Sender: TObject);
    // 1 Klappt das TreeView der ComboBox auf 
    procedure bFullExpClick(Sender: TObject);
    // 1 Klappt die erste Ebene des TreeViews der ComboBox auf 
    procedure bInitTreeClick(Sender: TObject);
    procedure bQueryClick(Sender: TObject);
    // 1 Klappt das VirtualTreeView zu 
    procedure bVTFullCollapseClick(Sender: TObject);
    // 1 Klappt das VirtualTreeView auf 
    procedure bVTFullExpandClick(Sender: TObject);
    // 1 Klappt die erste Ebene des VirtualTreeViews auf 
    procedure bVTInitTreeClick(Sender: TObject);
    // 1 Jede zweite Zeile des TreeVies einf�rben 
    procedure cbClassCollorClick(Sender: TObject);
    // 1 Setzt Schrift und Hintergrundfarbe des TreeViews der ComboBox 
    procedure cbClassesCalcNodeAttributes(TreeView: TfcCustomTreeView; Node: TfcTreeNode; State: TfcItemStates);
    // 1 Wird gefeuert, wenn ein Tree aufgeklappt wird (ComboBox) 
    procedure cbClassesExpand(TreeView: TfcCustomTreeView; Node: TfcTreeNode);
    // 1 Blendet die Gitternetzlinien des VirtualTreeView ein 
    procedure cbGridLinesClick(Sender: TObject);
    // 1 Setzt f�r jede Klassendefinition ein Icon 
    procedure cbImagesClick(Sender: TObject);
    // 1 Schlaltet Kursiv f�r Folders ein oder aus 
    procedure cbNodeItalicClick(Sender: TObject);
    // 1 Schaltet die Vertikale Einf�rbung f�r das VirtualTreeView ein oder aus 
    procedure cbSwimmlanesClick(Sender: TObject);
    // 1 Legt die Farbe der Zeilenf�rbung fest 
    procedure fcColorCombo1Change(Sender: TObject);
    // 1 Programmende 
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    // 1 Initialisierung 
    procedure FormCreate(Sender: TObject);
    // 1 Wird aufgerufen bevor die entsprechende Zelle gemalt wird 
    procedure vstBeforeCellPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; Column: 
      TColumnIndex; CellRect: TRect);
    // 1 Legt die Hintergrundfarbe des Nodes fest 
    procedure vstBeforeItemErase(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; ItemRect: TRect; 
      var ItemColor: TColor; var EraseAction: TItemEraseAction);
    // 1 Wird aufgerufen nachdem die Selektion ge�ndert hat 
    procedure vstFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);
    // 1 Wird aufgerufen nachdem die Selektion ge�ndert hat 
    procedure vstFocusChanging(Sender: TBaseVirtualTree; OldNode, NewNode: PVirtualNode; OldColumn, NewColumn: 
      TColumnIndex; var Allowed: Boolean);
    // 1 Wird gefeuert, wenn das VirtualTreeView einen Node freigibt 
    procedure vstFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    // 1 Wird aufgerufen, wenn f�r einen Node des VirtualTreeViews das Icon abgefragt wird 
    procedure vstGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex; 
      var Ghosted: Boolean; var ImageIndex: Integer);
    // 1 Initialisierung - Erzeugt die Nodes 
    procedure vstGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; 
      var CellText: WideString);
    // 1 Hier kann der Font f�r den Text des Nodes gesetzt werden (Pro Spalte) 
    procedure vstPaintText(Sender: TBaseVirtualTree; const TargetCanvas: TCanvas; Node: PVirtualNode; Column: 
      TColumnIndex; TextType: TVSTTextType);
  private
    FCustomRootNode: PVirtualNode;
    mClassDefList: TClassDefList;
    mDB: TAdoDBAccess;
    mLineColor: TColor;
    // 1 Erzeugt einen neuen Record mit einem Item mit dem angegebenen Namen 
    function SetRecord(aName:string): PTreeData; overload;
    // 1 Erzeugt einen neuen Record mit dem �bergebenen Item 
    function SetRecord(aClassDef: TClassDef): PTreeData; overload;
  protected
    // 1 Erzeugt die Eintr�ge anhand der Liste mit den Klassendefinitionen 
    procedure BuildItemsFormList;
  public
    // 1 Initialisierung - Erzeugt die Nodes 
    function GetClassCount(aSettingsArray: TClassClearSettingsArr): Integer;
    // 1 Gibt eine Klassendefinition zur�ck die nicht in der Lsite eingetragen ist 
    function GetDummyClassDef(aName: string): TGroupDataItem;
    // 1 Gibt einen Gruppen Knoten anhand seines Namens zur�ck 
    function GroupNodeByName(aName: string): PVirtualNode;
    // 1 �ffnet den Dialog f�r den Namen der Klassendefintion 
    function InputName(out aClassName:string; out aGroupName: string): Boolean;
    // 1 Erzeugt den Elternordner f�r die Eigenen Klassen 
    procedure MakeCustomRootNode;
    // 1 Wurzel Knoten f�r die Eigenen Klassendefinitionen 
    property CustomRootNode: PVirtualNode read FCustomRootNode write FCustomRootNode;
  end;
  


var
  frmVT: TfrmVT;

implementation
uses
  u_ClassNameInputDialog, QualityMatrixDef;

{$R *.DFM}

function AddVSTStructure(AVST: TCustomVirtualStringTree; ANode: PVirtualNode;
  aDataItem: TBaseDataItem): PVirtualNode;
var
  Data: PTreeData;
begin
  Result:=AVST.AddChild(ANode);
  Avst.ValidateNode(Result, False);
  Data:=AVST.GetNodeData(Result);
  if assigned(Data) then
    Data^.DataItem:=aDataItem;
end;

//:-------------------------------------------------------------------
(*  Member:           bColorClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Ruft den Farbendialog auf
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.bColorClick(Sender: TObject);
begin
  with TColorDialog.Create(self) do try
    Color := mLineColor;
  
    if Execute then begin
      mLineColor := Color;
      vst.Invalidate;
      fcColorCombo1.SelectedColor := mLineColor;
    end;// if Execute then begin
  finally
    free;
  end;// with TColorDialog.Create(self) do try
end;// TfrmVT.bColorClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           bCreateNewClassClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Erzeugt eine neue Klassendefinition
 *  Beschreibung:     Die selektierte Gruppe wird als "Parent" angenommen.
 --------------------------------------------------------------------*)
procedure TfrmVT.bCreateNewClassClick(Sender: TObject);
var
  xNode: PVirtualNode;
  xArray: TClassClearSettingsArr;
  xClassDef: TClassDef;
  xClassName: String;
  xGroupName: String;
begin
  (* Wurden noch keine eigenen Definitionen erfasst, dann
     muss zuerst ein Eintrag f�r die neuen Klassen erstellt werden *)
  if FCustomRootNode = nil then
    MakeCustomRootNode;
  
  FillChar(xArray, 16, 0);
  qm.GetFieldStateTable(xArray);
  
  xClassDef := TClassDef.Create;
  if InputName(xClassName, xGroupName) then begin
    xClassDef.DisplayName := xClassName;
    xClassDef.Category := xGroupName;
    xClassDef.ClassSettingsArr := xArray;
    xClassDef.MatrixType := mtShortLongThin;
  
    if xGroupName = '' then
      xNode := FCustomRootNode
    else
      xNode := GroupNodeByName(xGroupName);
  
    if not(assigned(xNode)) then begin
      (* Gruppe besteht noch nicht. Deshalb muss zuerst eine
         neue Gruppe erzeugt werden und dann die neue Klassendefinition
         in dieser Gruppe eingetragen werden *)
      xNode := AddVSTStructure(VST,FCustomRootNode,GetDummyClassDef(xGroupName));
    end;// if not(assigned(xNode)) then begin
    // Neue Klassendefinition eintragen
    AddVSTStructure(VST,xNode,xClassDef);
    mClassDefList.Add(xClassDef);
  end;// if InputName(xClassName, xGroupName) then begin
end;// TfrmVT.bCreateNewClassClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           bDeleteClassClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: 
 *  Beschreibung:     Die Definition wird lediglich auf der Datenbank gel�scht. F�r
                      das Programm ist sie weiterhin sichtbar (disabled).
 --------------------------------------------------------------------*)
procedure TfrmVT.bDeleteClassClick(Sender: TObject);
var
  xNode: PVirtualNode;
  xData: TBaseDataItem;
begin
  xData := nil;

//: ----------------------------------------------
  xNode := vst.FocusedNode;
  // Wenn ein Eintrag selektiert ist
  if assigned(xNode) then begin
    // Sicherstellen, dass Daten vorhanden sind
    if assigned(VST.GetNodeData(xNode)) then
      xData := PTreeData((VST.GetNodeData(xNode))).DataItem;
  
    // Sicherstellen, dass im Record eine Klassendefinition vorhanden ist
    if assigned(xData) then begin
      if (xData is TClassDef) then
        TClassDef(xData).DeleteFromDatabase(nil);
    end;// if assigned(xData) then begin
  end;// if assigned(xNode) then begin
end;// TfrmVT.bDeleteClassClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           bFullCollapseClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Klappt das TreeView der Combobox zusammen
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.bFullCollapseClick(Sender: TObject);
begin
  cbClasses.Items[0].Collapse(true);
  cbClasses.Items[0].GetNextSibling.Collapse(true);
end;// TfrmVT.bFullCollapseClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           bFullExpClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Klappt das TreeView der ComboBox auf
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.bFullExpClick(Sender: TObject);
begin
  cbClasses.Items[0].Expand(true);
  cbClasses.Items[0].GetNextSibling.Expand(true);
end;// TfrmVT.bFullExpClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           bInitTreeClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Klappt die erste Ebene des TreeViews der ComboBox auf
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.bInitTreeClick(Sender: TObject);
begin
  bFullCollapseClick(sender);
  cbClasses.Items[0].Expand(false);
  cbClasses.Items[0].GetNextSibling.Expand(false);
end;// TfrmVT.bInitTreeClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           bQueryClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: 
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.bQueryClick(Sender: TObject);
begin
{  with TClassDefContainer.Create do try
    Add('A1', mClassDefList);
    Add('A2', mClassDefList);
    Add('I1', mClassDefList);
    Add('I2', mClassDefList);
    if Add('aaaa', mClassDefList) < 0 then
      ShowMessage('Klasse aaaa nicht vorhanden');
    CreateTempTable;
  finally
    free;
  end;// with TClassDefContainer.Create do try}
end;// TfrmVT.bQueryClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           BuildItemsFormList
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Erzeugt die Eintr�ge anhand der Liste mit den Klassendefinitionen
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.BuildItemsFormList;
var
  xFirstLevelNode: PVirtualNode;
  xClassLevelNode: PVirtualNode;
  i: Integer;
  xOldCat: String;
  xOldPreDefClassGroup: TClassTypeGroup;
begin
  xFirstLevelNode := nil;
  xClassLevelNode := nil;
  xOldPreDefClassGroup := ctCustomDataItem;
  xOldCat := 'NotInitialized';

//: ----------------------------------------------
  with mClassDefList do begin
    for i := 0 to DataItemCount - 1 do begin
  
      // Neue Kategorie
      if ((xOldCat <> DataItems[i].Category) and (not(DataItems[i].Predefined)))
          or ((xOldPreDefClassGroup <> DataItems[i].ClassTypeGroup) and (DataItems[i].Predefined))then begin
  
        case DataItems[i].Predefined of
  
          true: begin  // Vordefinierte Klassen
            // Neue Kategorie Merken
            xOldPreDefClassGroup := DataItems[i].ClassTypeGroup;
            case xOldPreDefClassGroup of
              ctLongThickThin:
                xClassLevelNode := AddVSTStructure(vst,nil, GetDummyClassDef('Uster Klassen'));
              ctSplice:
                xClassLevelNode := AddVSTStructure(vst,nil, GetDummyClassDef('Spleiss Klassen'));
              ctFF:
                xClassLevelNode := AddVSTStructure(vst,nil, GetDummyClassDef('FF Klassen'));
              ctLabData:
                xClassLevelNode := AddVSTStructure(vst,nil, GetDummyClassDef('Imperfektionen'));
            end;
          end;// true: begin
  
          false: begin // Eigene Klassen
            // Neue Kategorie merken
            xOldCat := DataItems[i].Category;
  
            // F�r die erste eigene Klasse zuerst ein Root Node erzeugen
            if xFirstLevelNode = nil then begin
              MakeCustomRootNode;
              xFirstLevelNode := FCustomRootNode;
            end;// if xFirstLevelNode = nil then begin
  
            (* Wenn die Kategorie leer ist, dann wird ein Eintrag als Child unter
               Eigene Klassen hinzugef�gt *)
            if DataItems[i].Category <> '' then
              xClassLevelNode := AddVSTStructure(vst,xFirstLevelNode, GetDummyClassDef(xOldCat))
            else
              xClassLevelNode := xFirstLevelNode;
          end;// false: begin
        end;//case ClassDefs[i].Predefined of
  
      end;// if xOldClass <> ClassDefs[i].Category then begin
  
      AddVSTStructure(vst,xClassLevelNode, DataItems[i]);
    end;// for i := 0 to mClassDefList.ClassDefCount - 1 do begin
  end;// with mClassDefList do begin
end;// TfrmVT.BuildItemsFormList cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           bVTFullCollapseClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Klappt das VirtualTreeView zu
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.bVTFullCollapseClick(Sender: TObject);
begin
  vst.FullCollapse;
end;// TfrmVT.bVTFullCollapseClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           bVTFullExpandClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Klappt das VirtualTreeView auf
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.bVTFullExpandClick(Sender: TObject);
begin
  vst.FullExpand;
end;// TfrmVT.bVTFullExpandClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           bVTInitTreeClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Klappt die erste Ebene des VirtualTreeViews auf
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.bVTInitTreeClick(Sender: TObject);
var
  xNode: PVirtualNode;
begin
  vst.FullCollapse;
  xNode := vst.GetFirst;
  vst.Expanded[xNode] := true;
  xNode := vst.GetNextSibling(xNode);
  vst.Expanded[xNode] := true;
end;// TfrmVT.bVTInitTreeClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           cbClassCollorClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Jede zweite Zeile des TreeVies einf�rben
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.cbClassCollorClick(Sender: TObject);
begin
  vst.invalidate;
end;// TfrmVT.cbClassCollorClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           cbClassesCalcNodeAttributes
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (TreeView, Node, State)
 *
 *  Kurzbeschreibung: Setzt Schrift und Hintergrundfarbe des TreeViews der ComboBox
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.cbClassesCalcNodeAttributes(TreeView: TfcCustomTreeView; Node: TfcTreeNode; State: TfcItemStates);
var
  xItemHeight: Integer;
  xVisibleItemIndex: Integer;
  xRect: TRect;
begin
  if cbClassCollor.Checked then begin
    xRect := node.DisplayRect(false);
    xItemHeight := xRect.Bottom - xRect.Top;
    xVisibleItemIndex := xRect.Top div xItemHeight;
    if not(odd(xVisibleItemIndex)) then
      TreeView.Canvas.Brush.Color := mLineColor
    else
      TreeView.Canvas.Brush.Color := clWhite;
  end;// if cbClassCollor.Checked then begin
  if cbNodeItalic.Checked then begin
    if not(Node.HasChildren) then
      TreeView.Canvas.Font.Style := TreeView.Canvas.Font.Style + [fsItalic];
  end;
  if cbBold.Checked then begin
    if Node.HasChildren then
      TreeView.Canvas.Font.Style := TreeView.Canvas.Font.Style + [fsBold];
  end;
end;// TfrmVT.cbClassesCalcNodeAttributes cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           cbClassesExpand
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (TreeView, Node)
 *
 *  Kurzbeschreibung: Wird gefeuert, wenn ein Tree aufgeklappt wird (ComboBox)
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.cbClassesExpand(TreeView: TfcCustomTreeView; Node: TfcTreeNode);
begin
  cbClasses.TreeView.Invalidate;
end;// TfrmVT.cbClassesExpand cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           cbGridLinesClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Blendet die Gitternetzlinien des VirtualTreeView ein
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.cbGridLinesClick(Sender: TObject);
begin
  if toShowHorzGridLines in vst.TreeOptions.PaintOptions then begin
    vst.TreeOptions.PaintOptions := vst.TreeOptions.PaintOptions - [toShowHorzGridLines];
    vst.TreeOptions.PaintOptions := vst.TreeOptions.PaintOptions - [toShowVertGridLines];
  end else begin
    vst.TreeOptions.PaintOptions := vst.TreeOptions.PaintOptions + [toShowHorzGridLines];
    vst.TreeOptions.PaintOptions := vst.TreeOptions.PaintOptions + [toShowVertGridLines];
  end;//
end;// TfrmVT.cbGridLinesClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           cbImagesClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Setzt f�r jede Klassendefinition ein Icon
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.cbImagesClick(Sender: TObject);
begin
  if cbImages.Checked then begin
    vst.Images := imTreeImages;
    cbClasses.Images := imTreeImages;
  end else begin
    vst.Images := nil;
    cbClasses.Images := nil;
  end;// if cbImages.Checked then begin

//: ----------------------------------------------
  cbClassImages.Enabled := cbImages.Checked;
end;// TfrmVT.cbImagesClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           cbNodeItalicClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Schlaltet Kursiv f�r Folders ein oder aus
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.cbNodeItalicClick(Sender: TObject);
begin
  vst.Invalidate;
end;// TfrmVT.cbNodeItalicClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           cbSwimmlanesClick
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Schaltet die Vertikale Einf�rbung f�r das VirtualTreeView ein oder aus
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.cbSwimmlanesClick(Sender: TObject);
begin
  if cbSwimmlanes.Checked then begin
    vst.Header.Columns[0].Color := $FBFBFB;
    vst.Header.Columns[3].Color := $E2E7E9;
  end else begin
    vst.Header.Columns[0].Color := clWindow;
    vst.Header.Columns[0].Options := vst.Header.Columns[0].Options + [coParentColor];
    vst.Header.Columns[3].Color := clWindow;
    vst.Header.Columns[3].Options := vst.Header.Columns[3].Options + [coParentColor];
  end;
end;// TfrmVT.cbSwimmlanesClick cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           fcColorCombo1Change
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Legt die Farbe der Zeilenf�rbung fest
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.fcColorCombo1Change(Sender: TObject);
begin
  mLineColor := fcColorCombo1.SelectedColor;
  vst.Invalidate;
end;// TfrmVT.fcColorCombo1Change cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           FormClose
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender, Action)
 *
 *  Kurzbeschreibung: Programmende
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(mClassDefList);
  FreeAndNil(mDB);
end;// TfrmVT.FormClose cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           FormCreate
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Initialisierung
 *  Beschreibung:     Die Nodes des VirtualTreeViews k�nnen erst zur Laufzeit erzeugt werden
 --------------------------------------------------------------------*)
procedure TfrmVT.FormCreate(Sender: TObject);
begin
  mLineColor := $EDF9FB;
  VST.NodeDataSize:=SizeOf(TTreeData);
  
  cbImages.Checked := true;
  cbNodeItalic.Checked := true;
  cbClassCollor.Checked := true;
  cbBold.Checked := true;
  
  mDB := TAdoDBAccess.Create(1);
  mDB.Init;

//: ----------------------------------------------
  mClassDefList := TClassDefList.Create;
  BuildItemsFormList;

//: ----------------------------------------------
  // BuildItems(biTree);
  bVTInitTreeClick(nil);
  bInitTreeClick(nil);
  cbClasses.TreeView.OnCollapsed  := cbClassesExpand;
  cbClasses.TreeView.OnExpanded   := cbClassesExpand;
  fcColorCombo1.SelectedColor := mLineColor;
  vst.Header.Columns[0].Options := vst.Header.Columns[0].Options + [coParentColor];
end;// TfrmVT.FormCreate cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           GetClassCount
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (aSettingsArray)
 *
 *  Kurzbeschreibung: Initialisierung - Erzeugt die Nodes
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
function TfrmVT.GetClassCount(aSettingsArray: TClassClearSettingsArr): Integer;
var
  i: Integer;
  
  {
    Es werden Bitfelder gebildet und jeweils in diesen Bitfeldern die Anzahl Bits gez�hlt.
    Bsp:
    geg: aByte = 01001011
  
    1) Das Byte wird mit einer Maske and verkn�pft
        01001011      01001011 shr 1 = 00100101
    and 01010101                   and 01010101
        --------                       --------
        01000001                       00000101
  
    Diese zwei Zahlen werden zusammengez�hlt:
        01000001
      + 00000101
      ----------
        01000110
  
    Werden jetzt immer zwei Bits als Bitfeld gelesen (als 2 Bit Zahl), dann
    steht in diesen zwei Bits, wieviele bits in der original Zahl gesetzt waren.
  
    2) Diese Zahl wird jetzt in Bitfelder mit 4 Bits unterteilt und mit der Maske verkn�pft
        01000110      01000110 shr 2 = 00010001
    and 00110011                   and 00110011
        --------                       --------
        00000010                       00010001
  
     Diese Zahlen werden wieder zusammengez�hlt
        00000010
      + 00010001
        --------
        00010011
  
     3) Diese Zahl zeigt jetzt wieviele Bits in je einem vierer Feld gesetzt sind.
        Die beiden Bitfelder m�ssen jetzt noch addiert werden.
  
        00010011 and 00001111 = 00000011
        00010011 shr 4        = 00000001
                                --------
                                00000100 = 4
      Das Ergebnis ist die Anzahl gesetzter Bits in der Ausgabszahl. F�r ein Word muss der
      selbe Vorgang nochmals gemacht werden (Maske $0F0F).
      F�r ein Long ein weiteres Mal mit der Maske ($0F0F0F0F)
  }
  function GetBitCount(aByte: Byte):integer;
  begin
    aByte := (aByte and $55) + ((aByte shr 1) and $55);
    aByte := (aByte and $33) + ((aByte shr 2) and $33);
    result := (aByte and $0F) + (aByte shr 4);
  end;// function GetBitCount(aByte: Byte):integer;
  
begin
  result := 0;
  for i := low(aSettingsArray) to high(aSettingsArray) do
    inc(result, GetBitCount(aSettingsArray[i]));
end;// TfrmVT.GetClassCount cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           GetDummyClassDef
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (aName)
 *
 *  Kurzbeschreibung: Gibt eine Klassendefinition zur�ck die nicht in der Lsite eingetragen ist
 *  Beschreibung:     Dummy Klassen werden f�r die "Verzeichnisse" von 
                      Klassendefinitionen gebraucht. Sie werden nicht in der Datenbank 
                      gesichert.
 --------------------------------------------------------------------*)
function TfrmVT.GetDummyClassDef(aName: string): TGroupDataItem;
begin
  result := TGroupDataItem.Create;
  result.DisplayName := aName;
end;// TfrmVT.GetDummyClassDef cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           GroupNodeByName
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (aName)
 *
 *  Kurzbeschreibung: Gibt einen Gruppen Knoten anhand seines Namens zur�ck
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
function TfrmVT.GroupNodeByName(aName: string): PVirtualNode;
var
  xNode: PVirtualNode;
  xData: TBaseDataItem;
  xAbort: Boolean;
begin
  // Initialisieren
  result := nil;
  xAbort := false;
  xNode := vst.GetFirstChild(FCustomRootNode);
  
  // Alle Childs von FCustomRootNode untersuchen ob sie Gruppen darstellen
  while (not(xAbort)) and (assigned(xNode)) do begin
    // Ein Eintrag ist eine Gruppe, wenn Untereintr�ge vorhanden sind
    if vst.HasChildren[xNode] then begin
      xData := nil;
      if assigned(VST.GetNodeData(xNode)) then
        xData := PTreeData((VST.GetNodeData(xNode))).DataItem;
  
      if assigned(xData) then begin
        // Namen vergleichen
        if CompareText(xData.DisplayName, aName) = 0 then begin
          // Im Erfolgsfall abbrechen und den gefundenen Knoten zur�ckgeben
          result := xNode;
          xAbort := true;
        end;// if CompareText(xData, aName) = 0 then begin
      end;// if assigned(xData) then begin
    end;// if vst.HasChildren[xNode] then begin
    xNode := vst.GetNextSibling(xNode);
  end;// while (not(xAbort)) and (assigned(xNode)) do begin
end;// TfrmVT.GroupNodeByName cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           InputName
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (aClassName, aGroupName)
 *
 *  Kurzbeschreibung: �ffnet den Dialog f�r den Namen der Klassendefintion
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
function TfrmVT.InputName(out aClassName:string; out aGroupName: string): Boolean;
var
  xData: TBaseDataItem;
  xNode: PVirtualNode;
begin
  result := false;
  aGroupName := '';
  aClassName := '';
  
  with TClassNameInputDialog.Create(nil) do try
  
    xNode := vst.GetFirstChild(FCustomRootNode);
    while assigned(xNode) do begin
      xData := nil;
      if assigned(VST.GetNodeData(xNode)) then
        xData := PTreeData((VST.GetNodeData(xNode))).DataItem;
      if assigned(xData) then
        AddGroup(xData.DisplayName);
      xNode := vst.GetNextSibling(xNode);
    end;// while assigned(xNode) do begin
  
    xNode := vst.FocusedNode;
    if vst.AbsoluteIndex(xNode) > vst.AbsoluteIndex(FCustomRootNode) then begin
      xData := nil;
      if assigned(VST.GetNodeData(xNode)) then
        xData := PTreeData((VST.GetNodeData(xNode))).DataItem;
      if assigned(xData) then begin
        if vst.HasChildren[xNode] then
          SelectGroup(xData.DisplayName)
        else
          SelectGroup(xData.Category);
      end;//
    end;// if vst.AbsoluteIndex(xNode) > vst.AbsoluteIndex(FCustomRootNode) then begin
  
    if ShowModal = mrOK then begin
      aClassName := NewClassName;
      aGroupName := NewGroupName;
      result := true;
    end;
  finally
    Free;
  end;// with TClassNameInputDialog.Create(nil) do try
end;// TfrmVT.InputName cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           MakeCustomRootNode
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Erzeugt den Elternordner f�r die Eigenen Klassen
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.MakeCustomRootNode;
begin
  FCustomRootNode := AddVSTStructure(vst,nil, GetDummyClassDef('Eigene Klassen'));
end;// TfrmVT.MakeCustomRootNode cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           SetRecord
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (aName)
 *
 *  Kurzbeschreibung: Erzeugt einen neuen Record mit einem Item mit dem angegebenen Namen
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
function TfrmVT.SetRecord(aName:string): PTreeData;
begin
  result := new(PTreeData);
  result^.DataItem := TClassDef.Create;
  try
    with result^.DataItem do begin
      Name := aName;
    end;// with result^ do begin
  except
    FreeAndNil(result^.DataItem);
  end;// try finally
end;// TfrmVT.SetRecord cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           SetRecord
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (aClassDef)
 *
 *  Kurzbeschreibung: Erzeugt einen neuen Record mit dem �bergebenen Item
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
function TfrmVT.SetRecord(aClassDef: TClassDef): PTreeData;
begin
  result := new(PTreeData);
  result.DataItem := aClassDef;
end;// TfrmVT.SetRecord cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           vstBeforeCellPaint
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender, TargetCanvas, Node, Column, CellRect)
 *
 *  Kurzbeschreibung: Wird aufgerufen bevor die entsprechende Zelle gemalt wird
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.vstBeforeCellPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; Column: 
  TColumnIndex; CellRect: TRect);
var
  xItemHeight: Integer;
  xVisibleItemIndex: Integer;
begin
  if cbClassCollor.Checked then begin
    xItemHeight := CellRect.Bottom - CellRect.Top;
    xVisibleItemIndex := CellRect.Top div xItemHeight;
  
    if not(Odd(xVisibleItemIndex)) then begin
      TargetCanvas.Brush.Color := mLineColor;
    end;// if Odd(xVisibleItemIndex) then begin
  end;// if cbClassCollor.Checked then begin
end;// TfrmVT.vstBeforeCellPaint cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           vstBeforeItemErase
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender, TargetCanvas, Node, ItemRect, ItemColor, EraseAction)
 *
 *  Kurzbeschreibung: Legt die Hintergrundfarbe des Nodes fest
 *  Beschreibung:     Hier kann die Hintergrundfarbe jedes Nodes festgelegt werden. Die �bergebene 
                      Farbe wird verwendet um den Hintergerund zu l�schen.
 --------------------------------------------------------------------*)
procedure TfrmVT.vstBeforeItemErase(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; ItemRect: 
  TRect; var ItemColor: TColor; var EraseAction: TItemEraseAction);
var
  xItemHeight: Integer;
  xVisibleItemIndex: Integer;
begin
  if cbClassCollor.Checked then begin
    xItemHeight := ItemRect.Bottom - ItemRect.Top;
    xVisibleItemIndex := ItemRect.Top div xItemHeight;
  
    if not(Odd(xVisibleItemIndex)) then begin
      ItemColor := mLineColor;
      EraseAction := eaColor;
    end;// if Odd(xVisibleItemIndex) then begin
  end;// if cbClassCollor.Checked then begin
end;// TfrmVT.vstBeforeItemErase cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           vstFocusChanged
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender, Node, Column)
 *
 *  Kurzbeschreibung: Wird aufgerufen nachdem die Selektion ge�ndert hat
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.vstFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);
var
  xData: TClassDef;
  xClassClearSettingsArr: TClassClearSettingsArr;
  xSiroClearSettingsArr: TSiroClearSettingsArr;
  
  function GetSiroArray(aSettings: TClassClearSettingsArr): TSiroClearSettingsArr;
  var
    i: integer;
  begin
    assert(High(TSiroClearSettingsArr) <= High(TClassClearSettingsArr));
    for i := 0 to High(TSiroClearSettingsArr) do
      Result[i] := aSettings[i];
  end;// function GetSiroArray(aSettings: TClassClearSettingsArr): TSiroClearSettingsArr;
  
begin
  FillChar(xClassClearSettingsArr,16,0);
  
  xData := nil;
  if assigned(VST.GetNodeData(vst.FocusedNode))then
    if (PTreeData(VST.GetNodeData(vst.FocusedNode)).DataItem is TClassDef) then
      xData := TClassDef(PTreeData(VST.GetNodeData(vst.FocusedNode)).DataItem);
  
  // Matrixfeld disablen, wenn keine Klassendefinition selektiert ist
  qm.enabled := assigned(xData);
  
  if assigned(xData) then begin
    case xData.MatrixType of
      mtShortLongThin: begin
        qm.MatrixType := mtShortLongThin;
      end;// mtShortLongThin: begin
      mtSplice: begin
        qm.MatrixType := mtSplice;
      end;// mtSplice: begin
      mtSiro: begin
        qm.MatrixType := mtSiro;
      end;// mtSiro: begin
    end;// case xData.ClassTypeGroup of
    // Beim Umschalten auf Splice wird diese Eigenschaft automatisch True
    qm.Splicevisible := false;
    qm.ActiveVisible := true;
    qm.ChannelVisible := false;
  
    if xData.MatrixType = mtSiro then begin
      xSiroClearSettingsArr := GetSiroArray(xData.ClassSettingsArr);
      qm.SetFieldStateTable(xSiroClearSettingsArr, xSiroClearSettingsArr);
    end else begin
      qm.SetFieldStateTable(xData.ClassSettingsArr);
    end;// if xData.ClassTypeGroup = ctFF then begin
  
  end else begin
    FillChar(xClassClearSettingsArr,16,0);
    qm.SetFieldStateTable(xClassClearSettingsArr);
  end;// if assigned(xData) then begin
end;// TfrmVT.vstFocusChanged cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           vstFocusChanging
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender, OldNode, NewNode, OldColumn, NewColumn, Allowed)
 *
 *  Kurzbeschreibung: Wird aufgerufen nachdem die Selektion ge�ndert hat
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.vstFocusChanging(Sender: TBaseVirtualTree; OldNode, NewNode: PVirtualNode; OldColumn, NewColumn: 
  TColumnIndex; var Allowed: Boolean);
var
  xData: TClassDef;
  xArray: TClassClearSettingsArr;
  xSiroClearSettingsArr: TSiroClearSettingsArr;
  
  procedure SetClassClearFromSiro(var aSettings: TClassClearSettingsArr; aSiro: TSiroClearSettingsArr);
  var
    i: integer;
  begin
    assert(High(TSiroClearSettingsArr) <= High(TClassClearSettingsArr));
    for i := 0 to High(TSiroClearSettingsArr) do
      aSettings[i] := aSiro[i];
  end;// procedure SetClassClearFromSiro(var aSettings: TClassClearSettingsArr; aSiro: TSiroClearSettingsArr);
  
begin
  xData := nil;
  if assigned(VST.GetNodeData(OldNode)) then
    if (PTreeData((VST.GetNodeData(OldNode))).DataItem is TClassDef) then
      xData := TClassDef(PTreeData((VST.GetNodeData(OldNode))).DataItem);
  
  if assigned(xData) then begin
    if OldNode <> NewNode then begin
      FillChar(xArray, 16, 0);
      if xData.MatrixType = mtSiro then begin
        qm.GetFieldStateTable(xSiroClearSettingsArr, xSiroClearSettingsArr);
        SetClassClearFromSiro(xArray, xSiroClearSettingsArr);
      end else begin
        qm.GetFieldStateTable(xArray);
      end;// if xData.MatrixType = mtSiro then begin
      xData.ClassSettingsArr := xArray;
    end;// if OldNode <> NewNode then begin
  end;// if assigned (xData) then begin
end;// TfrmVT.vstFocusChanging cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           vstFreeNode
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender, Node)
 *
 *  Kurzbeschreibung: Wird gefeuert, wenn das VirtualTreeView einen Node freigibt
 *  Beschreibung:     Hier m�ssen alle Elemente des Records freigegeben werden die mit einem
                      Pointer realisiert sind (Strings, Dynamische Array's, Klassen)
 --------------------------------------------------------------------*)
procedure TfrmVT.vstFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  xData: PTreeData;
begin
  xData:=VST.GetNodeData(Node);
  
  if Assigned(xData) then begin
    // Nur Gruppen haben Child Nodes. Die Gruppen werden nicht von der Liste freigegeben
    if vst.HasChildren[node] then
      if (xData^.DataItem is TGroupDataItem) then
        FreeAndNil(xData^.DataItem);
  end;// if Assigned(xData) then begin
end;// TfrmVT.vstFreeNode cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           vstGetImageIndex
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender, Node, Kind, Column, Ghosted, ImageIndex)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn f�r einen Node des VirtualTreeViews das Icon abgefragt wird
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.vstGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: 
  TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
begin
  if Column = 0 then begin
    case Kind of
      ikNormal,
      ikSelected: begin
        if (Node.ChildCount = 0) then begin
          ImageIndex := 6
        end else begin
          if not(cbClassImages.Checked) then begin
            if Sender.Expanded[Node] then
              ImageIndex := 3
            else
              ImageIndex := 4;
          end;// if not(cbClassImages.Checked) then begin
        end;// if (Node.ChildCount = 0) then begin
      end;// ikNormal,ikSelected: begin
    end;// case Kind of
  end;// if Column = 0 then begin
  
  if (Sender.FocusedNode = Node) and (Column = 1) then
    ImageIndex := 2;
end;// TfrmVT.vstGetImageIndex cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           vstGetText
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender, Node, Column, TextType, CellText)
 *
 *  Kurzbeschreibung: Initialisierung - Erzeugt die Nodes
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.vstGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; 
  var CellText: WideString);
var
  xData: TBaseDataItem;
begin
  xData := nil;
  if assigned(VST.GetNodeData(Node)) then
    xData := PTreeData((VST.GetNodeData(Node))).DataItem;
  
  if assigned(xData) then begin
    // Wenn die Klasse bereits von der Datenbank gel�scht ist, dann Eintrag disablen
    if xData.Deleted then
      Node.States := Node.States + [vsDisabled]
    else
      Node.States := Node.States - [vsDisabled];
  
    case Column of
      0: CellText := xData.DisplayName;
      1: CellText := '';
    end;// case Column of
  end;// if assigned(xData) then begin
end;// TfrmVT.vstGetText cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           vstPaintText
 *  Klasse:           TfrmVT
 *  Kategorie:        Undefined 
 *  Argumente:        (Sender, TargetCanvas, Node, Column, TextType)
 *
 *  Kurzbeschreibung: Hier kann der Font f�r den Text des Nodes gesetzt werden (Pro Spalte)
 *  Beschreibung:     -
 --------------------------------------------------------------------*)
procedure TfrmVT.vstPaintText(Sender: TBaseVirtualTree; const TargetCanvas: TCanvas; Node: PVirtualNode; Column: 
  TColumnIndex; TextType: TVSTTextType);
begin
  if cbNodeItalic.Checked then begin
    with TargetCanvas do begin
      if node.ChildCount > 0 then
        Font.Style := Font.Style + [fsItalic]
    end;// with TargetCanvas do begin
  end;// if cbNodeItalic.Checked then begin
  
  if cbBold.Checked then begin
    with TargetCanvas do begin
      if node.ChildCount = 0 then
        Font.Style := Font.Style + [fsBold]
    end;// with TargetCanvas do begin
  end;// if cbBold.Checked then begin
end;// TfrmVT.vstPaintText cat:Undefined

end.
















