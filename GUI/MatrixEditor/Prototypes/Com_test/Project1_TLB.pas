unit Project1_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88.1.0.1.0  $
// File generated on 19.06.2003 16:20:34 from Type Library described below.

// ************************************************************************ //
// Type Lib: W:\MillMaster\GUI\MatrixEditor\Com_test\Project1.tlb (1)
// IID\LCID: {086D77D0-7C73-472D-B8AA-C415647074C1}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\System32\StdOle2.tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  Project1MajorVersion = 1;
  Project1MinorVersion = 0;

  LIBID_Project1: TGUID = '{086D77D0-7C73-472D-B8AA-C415647074C1}';

  IID_IComTest: TGUID = '{70CCEF11-D551-4A28-8BC7-34B000609BF6}';
  CLASS_ComTest: TGUID = '{733759A5-7FA5-407B-8A60-519B1574DD3F}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IComTest = interface;
  IComTestDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  ComTest = IComTest;


// *********************************************************************//
// Interface: IComTest
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {70CCEF11-D551-4A28-8BC7-34B000609BF6}
// *********************************************************************//
  IComTest = interface(IDispatch)
    ['{70CCEF11-D551-4A28-8BC7-34B000609BF6}']
    procedure OnStartPage(const AScriptingContext: IUnknown); safecall;
    procedure OnEndPage; safecall;
    procedure ShowForm; safecall;
    procedure ShowInfo; safecall;
  end;

// *********************************************************************//
// DispIntf:  IComTestDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {70CCEF11-D551-4A28-8BC7-34B000609BF6}
// *********************************************************************//
  IComTestDisp = dispinterface
    ['{70CCEF11-D551-4A28-8BC7-34B000609BF6}']
    procedure OnStartPage(const AScriptingContext: IUnknown); dispid 1;
    procedure OnEndPage; dispid 2;
    procedure ShowForm; dispid 3;
    procedure ShowInfo; dispid 4;
  end;

// *********************************************************************//
// The Class CoComTest provides a Create and CreateRemote method to          
// create instances of the default interface IComTest exposed by              
// the CoClass ComTest. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoComTest = class
    class function Create: IComTest;
    class function CreateRemote(const MachineName: string): IComTest;
  end;

implementation

uses ComObj;

class function CoComTest.Create: IComTest;
begin
  Result := CreateComObject(CLASS_ComTest) as IComTest;
end;

class function CoComTest.CreateRemote(const MachineName: string): IComTest;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_ComTest) as IComTest;
end;

end.
