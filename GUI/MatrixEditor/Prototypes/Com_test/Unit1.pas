unit Unit1;

interface

uses
  ComObj, ActiveX, AspTlb, Project1_TLB, StdVcl;

type
  TComTest = class(TASPObject, IComTest)
  protected
    procedure OnEndPage; safecall;
    procedure OnStartPage(const AScriptingContext: IUnknown); safecall;
    procedure ShowForm; safecall;
    procedure ShowInfo; safecall;
  end;

implementation

uses ComServ, Windows, Unit2;

procedure TComTest.OnEndPage;
begin
  inherited OnEndPage;
end;

procedure TComTest.OnStartPage(const AScriptingContext: IUnknown);
begin
  inherited OnStartPage(AScriptingContext);
end;

procedure TComTest.ShowForm;
begin
  with TForm2.Create(nil) do begin
       ShowModal;
       Free;
  end; 
end;

procedure TComTest.ShowInfo;
begin
  Windows.MessageBox(0, 'Version 1.0', 'TComTest', MB_ICONINFORMATION);
end;

initialization
  TAutoObjectFactory.Create(ComServer, TComTest, Class_ComTest, ciMultiInstance, tmBoth);
  
end.
