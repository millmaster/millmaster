unit u_Editor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DataItemTree, Buttons, ExtCtrls, StdCtrls,

  u_Matrix, u_Composite, u_EditorClasses, ClassDef, VirtualTrees,
  u_InputDialog, ImgList, ComCtrls, ToolWin, Menus;

  {
  YMQRUnit, VirtualTrees, mmVirtualStringTree,
  ClassDataSuck, SettingsFinderUnit,
  ClassDef, MMUGlobal, YMParaDef, QualityMatrixBase, QualityMatrix,
  QualityMatrixDef, AdoDBAccess;
  }
type

  //TItemType = (itMatrix, itComposite);


  TMatrixEditor = class(TForm)
    paItemTree: TPanel;
    DataItemTree: TDataItemTree;
    Panel1: TPanel;
    Panel2: TPanel;
    Splitter1: TSplitter;
    paHeader: TPanel;
    paItemDataField: TPanel;
    Label1: TLabel;
    lbCategorie: TLabel;
    lbCategoryName: TLabel;
    PopupMenu: TPopupMenu;
    MenueMatrixKlasse: TMenuItem;
    MenueComposite: TMenuItem;
    ToolBar1: TToolBar;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    SpeedButton3: TSpeedButton;
    sbSave: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Image1: TImage;
    ToolButton4: TToolButton;
    ImageList1: TImageList;
    tbNew: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure paItemDataFieldResize(Sender: TObject);
    procedure Splitter1CanResize(Sender: TObject; var NewSize: Integer;
      var Accept: Boolean);
    procedure DataItemTreeFocusChanged(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex);
    procedure DataItemTreeFocusChanging(Sender: TBaseVirtualTree; OldNode,
      NewNode: PVirtualNode; OldColumn, NewColumn: TColumnIndex;
      var Allowed: Boolean);
    procedure sbSaveClick(Sender: TObject);
    procedure Image1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure Image1DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure DataItemTreeDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DataItemTreeStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure DataItemTreeDblClick(Sender: TObject);
    procedure MenueMatrixKlasseClick(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure tbNewClick(Sender: TObject);
    procedure MenueCompositeClick(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
  private
    { Private declarations }
    mMatrix : TMatrix;
    mComposite : TComposite;
    mMatrixContainer : TMatrixContainer;
    function InputName(var aClassName:string; var aGroupName: string;
                       var aDescription: string; var aItemType: TItemType;
                       aEditMode : Boolean): Boolean;

    procedure DeleteItem;
    procedure NewItem( aItemType : TItemType );  overload;
    procedure NewItem; overload;
    procedure SaveItem;
  public
    { Public declarations }
  end;

var
  MatrixEditor: TMatrixEditor;

implementation

{$R *.DFM}
uses YMParaDef, QualityMatrixDef ;


procedure TMatrixEditor.FormCreate(Sender: TObject);
begin

  screen.Cursor :=  crHourGlass;
  //Application.ProcessMessages;


  mMatrix := TMatrix.Create(Self);
  mMatrix.AutoSize:= TRUE;
  //mMatrix.Parent := paItemDataField;
  mMatrix.Hide;



  mComposite := TComposite.Create(self) ;
  mComposite.AutoSize:= TRUE;
  //mComposite.Parent := paItemDataField;
  mComposite.Hide;


  mMatrixContainer := TMatrixContainer.Create;

  //Klassen von DB holen; Daten werden in die Klasse TMatrixDef gepackt
  //Pro Item eine TMatrixDef Kl.
  mMatrixContainer.LoadFromDatabase(cItemClass);

  // Container zuweisen (wird kopiert)
  DataItemTree.ClassDefList := mMatrixContainer;
  DataItemTree.ExpandedGroups := [ctCustomDataItem];

  screen.Cursor :=  crDefault;

end;

procedure TMatrixEditor.FormDestroy(Sender: TObject);
begin
  mMatrix.Free;
  mComposite.Free;
end;

procedure TMatrixEditor.paItemDataFieldResize(Sender: TObject);
begin
  //mMatrix.Align := alClient;
end;

procedure TMatrixEditor.Splitter1CanResize(Sender: TObject;
  var NewSize: Integer; var Accept: Boolean);
begin
  if NewSize <= 225 then
     Accept:= FALSE
  else
     Accept:= TRUE;
end;

procedure TMatrixEditor.DataItemTreeFocusChanged(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex);
var
  xData: TMatrixDef;
  xClassClearSettingsArr: TClassClearSettingsArr;
  xSiroClearSettingsArr: TSiroClearSettingsArr;
  xBaseDataItem : TBaseDataItem;
  xText: String;

  function GetSiroArray(aSettings: TClassClearSettingsArr): TSiroClearSettingsArr;
  var
    i: integer;
  begin
    assert(High(TSiroClearSettingsArr) <= High(TClassClearSettingsArr));
    for i := 0 to High(TSiroClearSettingsArr) do
      Result[i] := aSettings[i];
  end;// function GetSiroArray(aSettings: TClassClearSettingsArr): TSiroClearSettingsArr;

begin

  FillChar(xClassClearSettingsArr,16,0);

  xData := nil;
  //sbDelete.Enabled := FALSE;
  if assigned(DataItemTree.GetNodeDataItem(Node)) then
     if DataItemTree.GetNodeDataItem(Node) is TMatrixDef then begin
        xData := TMatrixDef(DataItemTree.GetNodeDataItem(Node));

        if xData.MatrixType = mtNone then begin
           //Composite
           mMatrix.Hide;

           mComposite.Parent := paItemDataField;
           mComposite.Show;
           mComposite.Data := xData;

        end else begin
           //Matrix
           mComposite.Hide;

           mMatrix.Parent := paItemDataField;
           mMatrix.Show;

           mMatrix.Data:= xData;
        end;

        lbCategoryName.Caption := xData.Category;
     end else begin
        mComposite.Hide;
        mMatrix.Hide;
        xBaseDataItem := DataItemTree.GetNodeDataItem(Node);

        xText := xBaseDataItem.ItemName;
        xText := xBaseDataItem.Category;
        xText := xBaseDataItem.ConfigString;
        xText := xBaseDataItem.Description;
        xText := xBaseDataItem.DisplayName;
        xText := xBaseDataItem.FieldName;
        xText := xBaseDataItem.ItemName;

        lbCategoryName.Caption := xBaseDataItem.DisplayName;



//        DataItemTree.GetNodeDataItem(Node)
     end;


  {
  // Matrixfeld disablen, wenn keine Klassendefinition selektiert ist
  qm.enabled := assigned(xData);

  if assigned(xData) then begin

    case xData.MatrixType of
      mtShortLongThin: begin
                          qm.MatrixType := mtShortLongThin;
                       end;// mtShortLongThin: begin
             mtSplice: begin
                         qm.MatrixType := mtSplice;
                       end;// mtSplice: begin
               mtSiro: begin
                         qm.MatrixType := mtSiro;
                       end;// mtSiro: begin
    end;// case xData.ClassTypeGroup of

    // Beim Umschalten auf Splice wird diese Eigenschaft automatisch True
    qm.Splicevisible := false;
    qm.ActiveVisible := true;
    qm.ChannelVisible := false;

    if xData.MatrixType = mtSiro then begin
       xSiroClearSettingsArr := GetSiroArray(xData.ClassSettingsArr);

       qm.SetFieldStateTable(xSiroClearSettingsArr, xSiroClearSettingsArr);
    end else begin
       qm.SetFieldStateTable(xData.ClassSettingsArr);
    end;// if xData.ClassTypeGroup = ctFF then begin

    //if xData.Modified then ShowMessage(xData.GetItemName + ' ge�ndert! Bitte speichern.');

  end else begin
     FillChar(xClassClearSettingsArr,16,0);
     qm.SetFieldStateTable(xClassClearSettingsArr);
  end;// if assigned(xData) then begin
  }
end;

procedure TMatrixEditor.DataItemTreeFocusChanging(Sender: TBaseVirtualTree;
  OldNode, NewNode: PVirtualNode; OldColumn, NewColumn: TColumnIndex;
  var Allowed: Boolean);
var
  xData: TMatrixDef;
begin
  xData := nil;
  if assigned(DataItemTree.GetNodeDataItem(OldNode)) then
   if DataItemTree.GetNodeDataItem(OldNode) is TMatrixDef then
      xData := TMatrixDef(DataItemTree.GetNodeDataItem(OldNode));

 {
  if assigned(xData) then begin
    if OldNode <> NewNode then begin
      //SaveMatrixData(xData, qm);
      if xData.Modified then ShowMessage('"' + xData.GetItemName + '" ge�ndert! Bitte speichern.');
    end;// if OldNode <> NewNode then begin
  end;// if assigned (xData) then begin
}
end;

procedure TMatrixEditor.sbSaveClick(Sender: TObject);
  var xData: TMatrixDef;
begin
  xData := NIL;
  xData := DataItemTree.SelectedDataItem as TMatrixDef;

  if assigned(xData) then begin
     if xData.MatrixType <> mtNone then
        mMatrix.Save(xData);

     if xData.MatrixType = mtNone then
        mComposite.Save(xData);

     DataItemTree.ClassDefList := mMatrixContainer;
  end;
end;

procedure TMatrixEditor.Image1DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
var xTxt: String;
begin
  xTxt:= Source.ClassParent.ClassName;
  xTxt:= Sender.ClassName;

  Accept := (Source.ClassName = 'TmmVirtualStringTree');

end;

procedure TMatrixEditor.Image1DragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
  if  (Source.ClassName = 'TmmVirtualStringTree') then begin
      DeleteItem;
  end;
end;

procedure TMatrixEditor.DataItemTreeDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := FALSE;
  State := dsDragMove;
end;

procedure TMatrixEditor.DataItemTreeStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
 beep;
end;

procedure TMatrixEditor.DataItemTreeDblClick(Sender: TObject);
  var xData: TMatrixDef;
      xText :String;
begin
  xData := NIL;
  xData := DataItemTree.SelectedDataItem as TMatrixDef;

  if assigned(xData) then begin
     xText := Format('(*)Wollen Sie das DataItem "%s" l�schen?',[xData.DisplayName]);

     if MessageDlg(xText, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        DeleteItem;
  end;
end;

procedure TMatrixEditor.DeleteItem;
var xData     : TMatrixDef;
begin

  xData := NIL;
  xData := DataItemTree.SelectedDataItem as TMatrixDef;

  if Assigned (xData) then begin
     mMatrixContainer.DeleteData(xData);
     // Container zuweisen (wird kopiert)
     DataItemTree.ClassDefList := mMatrixContainer;
  end;

end;

procedure TMatrixEditor.NewItem( aItemType : TItemType );
var xArray : TClassClearSettingsArr;
    xClassName, xGroupName, xDescription: string;
    xIndex: integer;
    xMatrixDef : TMatrixDef;
    xClassGroup : TClassTypeGroup;
begin
{
  xClassName := '';
  xGroupName := '';
  xDescription := '';

  if aItemType = itMatrix then
     with mMatrix.qm do begin
       FillChar(xArray, 16, 0);
       SetFieldStateTable(xArray) ;
       xClassGroup := ctCustomDataItem;
     end
  else
     with mComposite do begin
         Reset;
         xClassGroup := ctCustomDataItem;
     end;

  InputName(var aClassName:string; var aGroupName: string; var aDescription: string; var aItemType: TItemType): Boolean;
  if InputName(xClassName, xGroupName, xDescription) then begin

    if aItemType = itMatrix then begin
       xIndex := mMatrixContainer.CreateAndAddNewDataItem(TClassDef, TMatrixDef);
       (mMatrixContainer.DataItems[xIndex] as TMatrixDef).MatrixType:= mtShortLongThin
    end else begin
       xIndex := mMatrixContainer.CreateAndAddNewDataItem(TCompositClassDef, TMatrixDef);
       mMatrixContainer.MatrixItems[xIndex].MatrixType:=  mtNone;
    end;

    // Eigenschaften des neuen Items setzen
   // mMatrixContainer[xIndex].ClassTypeGroup   := ctLongThickThin;
    mMatrixContainer[xIndex].ClassTypeGroup   := ctDataItem;
    mMatrixContainer[xIndex].Description      := xDescription;
    mMatrixContainer[xIndex].ItemName         := xClassName;
    mMatrixContainer[xIndex].Predefined       := false;
    mMatrixContainer[xIndex].Category         := xGroupName;
{
   // mMatrixContainer[xIndex].DataItemType.
    if aItemType = itMatrix then begin
       //Matrixtyp setzen
       (mMatrixContainer.DataItems[xIndex] as TMatrixDef).MatrixType:= mtShortLongThin
    end else begin
        //Composite
        //mMatrixContainer[xIndex].ClassTypeGroup  := ctCustomDataItem;
       (mMatrixContainer.DataItems[xIndex] as TMatrixDef).MatrixType:= mtNone;
        mMatrixContainer.MatrixItems[xIndex].MatrixType:=  mtNone;
    end;
}

    //Neu zuweisen -> nur in Komponente
//    DataItemTree.ClassDefList := mMatrixContainer;
//  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.MenueMatrixKlasseClick(Sender: TObject);
begin
  NewItem(itMatrix);
end;
//------------------------------------------------------------------------------
function TMatrixEditor.InputName(var aClassName:string; var aGroupName: string;
                                 var aDescription: string;
                                 var aItemType: TItemType;
                                 aEditMode : Boolean ): Boolean;
var
  xData: TBaseDataItem;
  xNode: PVirtualNode;
  x: Integer;
  xList : TStringList;
begin
  result := false;

  with TInputDialog.Create(nil) do try

    xList:= TStringList.Create;
    xList.Sorted := TRUE;
    xList.Duplicates := dupIgnore;

    for x:= 0 to mMatrixContainer.DataItemCount-1 do begin
        if  mMatrixContainer.DataItems[x].ClassTypeGroup = ctCustomDataItem then
            xList.Add( mMatrixContainer.DataItems[x].Category);
    end;

    for x:=0 to xList.Count-1 do
       if xList.Strings[x] <> '' then
          AddGroup(xList.Strings[x]);

    xList.Free;

    edGroup.ItemIndex:=0;

    if aClassName <> '' then NewClassName:= aClassName;

    if aClassName <> '' then  NewGroupName:= aGroupName;

    if aDescription <> '' then NewDescription:= aDescription;


    ItemType := aItemType;
    EditMode := aEditMode;

    if ShowModal = mrOK then begin
       aClassName := NewClassName;
       aGroupName := NewGroupName;
       aDescription := NewDescription;
       aItemType := ItemType;
       result := true;
    end;
  finally
    Free;
  end;// with TClassNameInputDialog.Create(nil) do try
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.SaveItem;
  var xData: TMatrixDef;
begin
  xData := NIL;
  xData := DataItemTree.SelectedDataItem as TMatrixDef;

  if assigned(xData) then begin
     if xData.MatrixType <> mtNone then
        mMatrix.Save(xData);

     if xData.MatrixType = mtNone then
        mComposite.Save(xData);

     DataItemTree.ClassDefList := mMatrixContainer;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.ToolButton2Click(Sender: TObject);
begin
  SaveItem;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.tbNewClick(Sender: TObject);
begin
 // tbNew.CheckMenuDropdown;
 NewItem;
end;

procedure TMatrixEditor.MenueCompositeClick(Sender: TObject);
begin
 // NewItem(itComposite);
end;

procedure TMatrixEditor.NewItem;
var xArray : TClassClearSettingsArr;
    xClassName, xGroupName, xDescription: string;
    xIndex: integer;
    xMatrixDef : TMatrixDef;
    xClassGroup : TClassTypeGroup;
    xItemType   :TItemType;
begin
  xClassName := '';
  xGroupName := '';
  xDescription := '';


  if InputName(xClassName, xGroupName, xDescription, xItemType, FALSE) then begin
    if xItemType = itMatrix then begin
       with mMatrix.qm do begin
            FillChar(xArray, 16, 0);
            SetFieldStateTable(xArray) ;
            xClassGroup := ctCustomDataItem;
       end;
       xIndex := mMatrixContainer.CreateAndAddNewDataItem(TClassDef, TMatrixDef);
      (mMatrixContainer.DataItems[xIndex] as TMatrixDef).MatrixType:= mtShortLongThin
    end else begin
       with mComposite do begin
            Reset;
            xClassGroup := ctCustomDataItem;
       end;
       xIndex := mMatrixContainer.CreateAndAddNewDataItem(TCompositClassDef, TMatrixDef);
       mMatrixContainer.MatrixItems[xIndex].MatrixType:=  mtNone;
    end;

    // Eigenschaften des neuen Items setzen
    mMatrixContainer[xIndex].ClassTypeGroup   := ctDataItem;
    mMatrixContainer[xIndex].Description      := xDescription;
    mMatrixContainer[xIndex].ItemName         := xClassName;
    mMatrixContainer[xIndex].Predefined       := false;
    mMatrixContainer[xIndex].Category         := xGroupName;


    //Neu zuweisen -> nur in Komponente
    DataItemTree.ClassDefList := mMatrixContainer;
  end;


end;

procedure TMatrixEditor.ToolButton3Click(Sender: TObject);
  var xData: TMatrixDef;
var xArray : TClassClearSettingsArr;
    xClassName, xGroupName, xDescription: string;
    xIndex: integer;
    xMatrixDef : TMatrixDef;
    xClassGroup : TClassTypeGroup;
    xItemType   :TItemType;
begin

  xClassName := '';
  xGroupName := '';
  xDescription := '';

  xData := NIL;
  xData := DataItemTree.SelectedDataItem as TMatrixDef;

  if assigned(xData) then begin
     if xData.MatrixType <> mtNone then
        xItemType := itMatrix;

     if xData.MatrixType = mtNone then
        xItemType := itComposite;

     xDescription := xData.Description;
     xClassName   := xData.ItemName;
     xGroupName   := xData.Category;

     if InputName(xClassName, xGroupName, xDescription, xItemType, TRUE) then begin

         xIndex:= mMatrixContainer.IndexOf(xData);
         mMatrixContainer[xIndex].Description  := xDescription;
         mMatrixContainer[xIndex].ItemName     := xClassName;
         mMatrixContainer[xIndex].Category     := xGroupName;

         DataItemTree.ClassDefList := mMatrixContainer;
     end;
  end;

end;

end.
