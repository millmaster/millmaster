unit u_MainProg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ToolWin, u_Editor;

type
  TForm1 = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
  private
    { Private declarations }
    mMatrixEditor : TMatrixEditor;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.ToolButton1Click(Sender: TObject);
begin
  //screen.Cursor :=  crHourGlass;
  MatrixEditor  :=  TMatrixEditor.Create(Self);
  //screen.Cursor :=  crDefault;
  //MatrixEditor.WindowState:= wsMaximized;
  MatrixEditor.Show;
end;

procedure TForm1.ToolButton2Click(Sender: TObject);
begin
  MatrixEditor.Free;
end;

end.
