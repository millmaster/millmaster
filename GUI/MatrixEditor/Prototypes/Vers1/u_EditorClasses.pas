unit u_EditorClasses;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  DataItemTree, QuickRpt, YMQRUnit, VirtualTrees, mmVirtualStringTree,
  ClassDataSuck, SettingsFinderUnit,

  ClassDef, MMUGlobal, YMParaDef, QualityMatrixBase, QualityMatrix, QualityMatrixDef,
  AdoDBAccess;




type


  (*---------------------------------------------------------
    Dekorierer
  ----------------------------------------------------------*)
  TMatrixDef = class (TDataItemDecorator)
  private
    mCompositClassDef : TCompositClassDef;

    function GetClassSettingsArr: TClassClearSettingsArr;
    function GetID: Integer;
    function GetMatrixDef: String;
    function GetMatrixType: TMatrixType;
    procedure SetClassSettingsArr(const Value: TClassClearSettingsArr);
    procedure SetID(const Value: Integer);
    procedure SetMatrixDef(const Value: String);
    procedure SetMatrixType(const Value: TMatrixType);
    function GetCompositClassDef: TCompositClassDef;
    procedure SetCompositClassDef(const Value: TCompositClassDef);

    function GetFormula: String;
    procedure SetFormula(const Value: String);

  public
    constructor Create; override;
    destructor Destroy; override;
    //1 Gibt einen Kommaseparierten String zur�ck der der �bergebenen Matrix entspricht
    function GetMatrixFromArray(aArray: TClassClearSettingsArr): String;
    //1 Speichert die Klassendefinition in der Datenbank
    procedure Save(aNativeQuery: TNativeAdoQuery = nil);
    //1 Array mit den gesetzten Klassen. Jedes gesetzte Bit entspricht einer gesetzten Loepfe Klasse (Achtung! Nummerierung)
    property ClassSettingsArr: TClassClearSettingsArr read GetClassSettingsArr write SetClassSettingsArr;
    //1 ID des Datensatzes auf der Datenbank
    property ID: Integer read GetID write SetID;
    //1 Definition der selektierten Felder (zB: '23, 31, 39, 47, 55, 56')
    property MatrixDef: String read GetMatrixDef write SetMatrixDef;
    //1 Typ der Klassendefinition (Class, Splice, FF)
    property MatrixType: TMatrixType read GetMatrixType write SetMatrixType;

    property Formula :String read GetFormula write SetFormula;

  end;// TTestDataItem = class (TDataItemDecorator)


  TMatrixContainer = class (TClassDefContainer)
  private
    function GetMatrixCount: Word;
    function GetMatrixItems(aIndex: word): TMatrixDef;
    procedure SetMatrixItems(aIndex: word; const Value: TMatrixDef);
  public
    constructor Create(aPersistent: boolean = false); override;
    destructor Destroy; override;
    procedure DeleteData(aDataItem : TMatrixDef);
    property MatrixCount : Word read GetMatrixCount;
    property MatrixItems[aIndex:word]: TMatrixDef read GetMatrixItems write SetMatrixItems;

  end;



  TBaseItemFrame = class(TFrame)
  private
  protected
    mData : TMatrixDef;
    function GetData: TMatrixDef; virtual;
    procedure SetData(const Value: TMatrixDef); virtual; abstract;
    { Private declarations }
  public
    { Public declarations }
    //constructor Create(AOwner: TComponent); override;
    property Data: TMatrixDef read GetData write SetData;
    procedure Save(var aMatrixDef :TMatrixDef); virtual;
  end;


const  cItemClass: TDataItemDecoratorClass = TMatrixDef;


implementation

{ TMatrixDef }

constructor TMatrixDef.Create;
begin
  inherited;
  mCompositClassDef := TCompositClassDef.Create;
end;
//------------------------------------------------------------------------------
destructor TMatrixDef.Destroy;
begin
  mCompositClassDef.Free;

  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetClassSettingsArr: TClassClearSettingsArr;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      result := TClassDef(FDataItem).ClassSettingsArr;
  end else
    FillChar(result,16,0);
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetCompositClassDef: TCompositClassDef;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TCompositClassDef then
//      result := TCompositClassDef(FDataItem);
       mCompositClassDef := TCompositClassDef(FDataItem);
  end else
    result := NIL;
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetFormula: String;
begin
  Result := StringReplace(MatrixDef, ',', ' ' ,[rfReplaceAll] );
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetID: Integer;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      result := TClassDef(FDataItem).ID;
  end else
    result := -1;
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetMatrixDef: String;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      result := TClassDef(FDataItem).MatrixDef;
  end else
    result := '';
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetMatrixFromArray(
  aArray: TClassClearSettingsArr): String;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      result := TClassDef(FDataItem).GetMatrixFromArray(aArray);
  end else
    result := '';
end;
//------------------------------------------------------------------------------
function TMatrixDef.GetMatrixType: TMatrixType;
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      result := TClassDef(FDataItem).MatrixType;
  end else
    result := mtNone;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.Save(aNativeQuery: TNativeAdoQuery);
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      TClassDef(FDataItem).Save(aNativeQuery);
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetClassSettingsArr(
  const Value: TClassClearSettingsArr);
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      TClassDef(FDataItem).ClassSettingsArr := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetCompositClassDef(const Value: TCompositClassDef);
begin
  if assigned(FDataItem) then begin
    if FDataItem is TCompositClassDef then
       mCompositClassDef := Value;
      //TCompositClassDef(FDataItem) := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetFormula(const Value: String);
begin
  MatrixDef := Value;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetID(const Value: Integer);
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      TClassDef(FDataItem).ID := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetMatrixDef(const Value: String);
begin
{
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      TClassDef(FDataItem).MatrixDef := Value;
  end;
}
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      TClassDef(FDataItem).MatrixDef := Value;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixDef.SetMatrixType(const Value: TMatrixType);
begin
  if assigned(FDataItem) then begin
    if FDataItem is TClassDef then
      TClassDef(FDataItem).MatrixType := Value;
  end;
end;
//------------------------------------------------------------------------------

{ TMatrixContainer }

constructor TMatrixContainer.Create;
begin
  inherited Create;
end;
//------------------------------------------------------------------------------
procedure TMatrixContainer.DeleteData(aDataItem: TMatrixDef);
var xIndex : integer;
begin
  if Assigned (aDataItem) then begin
     //Daten aus Kontainer & DB loeschen
     xIndex := IndexOf(aDataItem);
     if xIndex >= 0 then begin
        if aDataItem.DataItemType.InheritsFrom(TClassDef) then begin
           TClassDef(aDataItem.DataItem).DeleteFromDatabase(NIL);
           Delete(xIndex);
        end;
     end;
   end;
end;
//------------------------------------------------------------------------------
destructor TMatrixContainer.Destroy;
begin
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TMatrixContainer.GetMatrixCount: Word;
var x: integer;
begin
  x:= GetDataItemCount;
  if x < 0 then x:=0;
  Result:=x;
end;
//------------------------------------------------------------------------------
function TMatrixContainer.GetMatrixItems(aIndex: word): TMatrixDef;
begin
  try
    Result:= DataItems[aIndex] as TMatrixDef;
  except
    Result:= NIL;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixContainer.SetMatrixItems(aIndex: word; const Value: TMatrixDef);
var xMatrixDef : TMatrixDef;
begin
  if assigned(Value) then begin
     xMatrixDef := DataItems[aIndex]as TMatrixDef;
     xMatrixDef := Value;
  end;
end;
//------------------------------------------------------------------------------



{ TBaseItemFrame }
{
//------------------------------------------------------------------------------
constructor TBaseItemFrame.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;
}
//------------------------------------------------------------------------------
function TBaseItemFrame.GetData: TMatrixDef;
begin
  Result := mData;
end;
//------------------------------------------------------------------------------
procedure TBaseItemFrame.Save(var aMatrixDef: TMatrixDef);
begin
 if not assigned(aMatrixDef) then exit;
end;
//------------------------------------------------------------------------------

end.
