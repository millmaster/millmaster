program Vers1;

uses
  Forms,
  u_MainProg in 'u_MainProg.pas' {Form1},
  u_Editor in 'u_Editor.pas' {MatrixEditor},
  u_EditorClasses in 'u_EditorClasses.pas',
  u_InputDialog in 'u_InputDialog.pas' {InputDialog},
  u_Matrix in 'u_Matrix.pas' {Matrix: TFrame},
  u_Composite in 'u_Composite.pas' {Composite: TFrame};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
