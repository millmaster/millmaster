unit u_Composite;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmListBox, Buttons, mmEdit,
  u_EditorClasses;

type
  TComposite = class(TFrame)
    lbCompName: TLabel;
    Label1: TLabel;
    edFormula: TmmEdit;
    bbAdd: TBitBtn;
    lbElement: TmmListBox;
    bbUndo: TBitBtn;
    procedure bbAddClick(Sender: TObject);
  private
    { Private declarations }
    mData : TMatrixDef;
    procedure FillElemntList;
    function GetData: TMatrixDef;
  protected
    procedure SetData(const Value: TMatrixDef); //virtual;
  public
    { Public declarations }
    property Data: TMatrixDef read GetData write SetData;
    procedure Save(var aMatrixDef :TMatrixDef); //override;
    procedure Reset;
    //procedure AfterConstruction; virtual;
    //constructor Create(AOwner: TComponent); override;
  end;

implementation

{$R *.DFM}
uses QualityMatrixDef;

{ TComposite }

//------------------------------------------------------------------------------
procedure TComposite.Save(var aMatrixDef: TMatrixDef);
var xData: TMatrixDef;
    x :string;
begin

// inherited Save(aMatrixDef);

 if assigned(aMatrixDef) then begin

    if aMatrixDef.MatrixDef <> edFormula.Text then begin
       aMatrixDef.Modified := TRUE;
    end;

    aMatrixDef.Formula := edFormula.Text;

    //In DB speichern
    aMatrixDef.Save(NIL);
    aMatrixDef.Modified := FALSE;

 end;// if assigned (xData) then begin
end;
//------------------------------------------------------------------------------
procedure TComposite.SetData(const Value: TMatrixDef);
var
  xData: TMatrixDef;
  xCompName, xCategory :string;
  xStatus : Boolean;
begin

  mData := Value;

  xCompName := '';
  xCategory := '';
  xStatus := FALSE;
{
  if assigned(DataItemTree1.GetNodeDataItem(Node)) then
     if DataItemTree1.GetNodeDataItem(Node) is TMatrixDef then begin
        xData := TMatrixDef(DataItemTree1.GetNodeDataItem(Node));
}
     if assigned(mData) then begin

        if mData.MatrixType = mtNone then begin
           edFormula.Text := mData.Formula;
           xCompName := Format('%s = ',[mData.ItemName]);
           //xCategory := Format('Category : %s',[mData.Category]);
           xStatus := TRUE;
        end;
     end;

 // SetButtonStatus(xStatus);

  lbCompName.Caption := xCompName;
 // lbCategory.Caption := xCategory;
end;
//------------------------------------------------------------------------------
procedure TComposite.bbAddClick(Sender: TObject);
var xFormula : String;
    xNewTerm :String;
    x:integer;
begin
  xFormula :=  edFormula.text;
  xFormula := StringReplace(xFormula, 'Formel', '' ,[rfReplaceAll] );

  xNewTerm :=  lbElement.Items.Strings[ lbElement.itemindex ];

  edFormula.text := format('%s + %s',[xFormula, xNewTerm]);
  xFormula :=  edFormula.text;
  x:= Pos('+', xFormula);
  if x < 3 then begin
     delete(xFormula, 1, x+1);
     Trim (xFormula);
     edFormula.text := xFormula;
  end;
end;
//------------------------------------------------------------------------------
procedure TComposite.FillElemntList;
begin
    lbElement.Items.Add('%EffP');
    lbElement.Items.Add('%CCI');
    lbElement.Items.Add('%CCIFF');
    lbElement.Items.Add('%CL');
    lbElement.Items.Add('%CN');
    lbElement.Items.Add('COffCnt');
    lbElement.Items.Add('CSFI');
    lbElement.Items.Add('CSp');
    lbElement.Items.Add('CS');
    lbElement.Items.Add('CSFI');
    lbElement.Items.Add('CT');
    lbElement.Items.Add('CUpY');
    lbElement.Items.Add('CYTot');
    lbElement.Items.Add('Len');
    lbElement.Items.Add('SFI');
    lbElement.Items.Add('SFI/D');
    lbElement.Items.Add('Sp');
    lbElement.Items.Add('Weight');
    lbElement.Items.Add('YB');
end;
//------------------------------------------------------------------------------
function TComposite.GetData: TMatrixDef;
begin
  result := mData;
end;
//------------------------------------------------------------------------------
procedure TComposite.Reset;
begin
  edFormula.Text:= '';
end;
//------------------------------------------------------------------------------

end.
