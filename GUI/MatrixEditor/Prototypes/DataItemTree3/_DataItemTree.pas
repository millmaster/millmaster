(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: DataItemTree
| Projectpart...: Matrix Trend
| Subpart.......: -
| Process(es)...: LabReport, MatrixEditor, QOfflimit
| Description...: Visuelle Darstellung der zu Verf�gung stehenden DataItems
| Info..........: -
| Develop.system: Windows 2000 SP3
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.01 / ModelMaker 6.20 1402
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
| 11.03.2003 1.00 Lok | Datei erstellt
|=============================================================================*)
(**********************************************************************
 *  Modulname:        DataItemTree
 *  Kurzbeschreibung: Komponente f�r die Darrstellung der DataItems
 *
 *  Beschreibung:
    -
 ************************************************************************)
//1 Komponente f�r die Darrstellung der DataItems
unit DataItemTree;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, VTHeaderPopup, ImgList, mmImageList, ToolWin, ComCtrls, mmToolBar,
  VirtualTrees, ClassDef, ActiveX, mmPageScroller, ExtCtrls, mmPanel,
  mmPopupMenu, StdCtrls, mmMemo, mmTabControl, mmVirtualStringTree, LoepfeGlobal,
  NCButton;

type
  // Optionen
  TDataItemTreeOption = (
    dtoRowColor,         // Jede zweite Zeile mit der Farbe 'LineColor' einf�rben
    dtoIcon,             // Icons f�r dei einzelnen Items verwenden
    dtoGroupItalic,      // Grupenknoten in kursiv
    dtoMultiselection,   // Zulassen, dass mehrere Items markiert werden k�nnen
    dtoNodeBold,         // DataItems Fett
    dtoGridLines,        // Gitternetzlinien
    dtoToolButtons,      // Buttonleiste
    dtoToolMenuButton,   // Einzelner DropDown Button mit einem Men�punkt f�r jede Gruppe DataItems
    dtoTabControl,       // F�r jede Gruppe wird ein Tab angezeigt
    dtoHideOtherGroups,  // Immer nur die aktuelle Gruppe sichtbar
    dtoShowDescription   // Beschreibung des DataItems anzeigen (sofern vorhanden)
  );
  TDataItemTreeOptions = set of TDataItemTreeOption;

  // Array aus DataItems (wird f�r die Selektion gebraucht)
  TDataItemArray = Array of TBaseDataItem;

  TTabControlPointerArray = Array of PVirtualNode;

  TGroupPointerArrray = Array [TClassTypeGroup] of PVirtualNode;

  TClassTypeGroupSet = set of TClassTypeGroup;

const
  DefaultItemTreeOptions = [dtoIcon,dtoGroupItalic,dtoNodeBold, dtoMultiselection, dtoShowDescription];

  // Nummern der verwendeten Icons f�r das TreeView
  cDataPointerImage =  2;
  cOpenDirImage     =  3;
  cClosedDirImage   =  4;
  cDataItemImage    =  6;
  cMatrixTrend      =  9;
  cMatrixTrendEdit  = 10;
  cFunctionImage    = 11;

type
  (*: Klasse:        TDataItemTree
      Vorg�nger:     TFrame
      Kategorie:     No category
      Kurzbeschrieb: Tree View um die definierten DataItems darzustellen 
      Beschreibung:  
                     - *)
  //1 Tree View um die definierten DataItems darzustellen 
  TDataItemTree = class (TFrame)
    mDescriptionMemo: TmmMemo;
    mGroupMenu: TmmPopupMenu;
    mNCImages: TmmImageList;
    mTree: TmmVirtualStringTree;
    mTreeImages: TmmImageList;
    //1 Wird aufgerufen, wenn ein Knoten zusammengefaltet wird 
    procedure mTreeCollapsed(Sender: TBaseVirtualTree; Node: PVirtualNode);
    //1 Wird aufgerufen, wenn ein Knoten aufgeht 
    procedure mTreeExpanded(Sender: TBaseVirtualTree; Node: PVirtualNode);
    //1 Breite der Titel Leiste berechnen und setzen 
    procedure mTreeResize(Sender: TObject);
  private
    FClassDefList: TClassDefList;
    FDataItemTreeOptions: TDataItemTreeOptions;
    FExpandedGroups: TClassTypeGroupSet;
    FImages: TImageList;
    FLineColor: TColor;
    FOnAfterDataItemLoad: TAfterDataItemLoad;
    FOnBeforeDataItemsLoad: TBeforeDataItemsLoad;
    FOnClick: TNotifyEvent;
    FOnDblClick: TNotifyEvent;
    FOnFocusChanged: TVTFocusChangeEvent;
    FOnFocusChanging: TVTFocusChangingEvent;
    FOnGetHint: TVSTGetTextEvent;
    FOnGetImage: TVTGetImageEvent;
    FOnGetText: TVSTGetTextEvent;
    FOnHelpContext: TVTHelpContextEvent;
    FOnNewText: TVSTNewTextEvent;
    FOnSetAdditionalDataItems: TSetAdditionalDataItems;
    FOnVTDragAllowed: TVTDragAllowedEvent;
    FOnVTDragDrop: TVTDragDropEvent;
    FOnVTDragOver: TVTDragOverEvent;
    mButtonsLeft: Integer;
    mCollapseButton: TNCButton;
    mCustomRootNode: PVirtualNode;
    mExpandButton: TNCButton;
    mGroupPointerArrray: TGroupPointerArrray;
    mStartInternalImages: Integer;
    //1 Gibt eine Klassendefinition zur�ck die nicht in die Liste eingetragen wird 
    procedure BuildItemsFromList;
    //1 Gibt eine Klassendefinition zur�ck die nicht in die Liste eingetragen wird 
    function GetDummyClassDef(aName: string): TGroupDataItem;
    //1 Gibt das aktuelle DataItem zur�ck 
    function GetSelectedDataItem: TBaseDataItem;
    //1 Gibt ein Array mit allen selektierten DataItems zur�ck 
    function GetSelection: TDataItemArray;
    //1 Gibt die Anzahl der selektierten DataItems zur�ck 
    function GetSelectionCount: Integer;
    //1 Erzeugt den Elternordner f�r die Eigenen Klassen 
    procedure MakeCustomRootNode;
    //1 Weist eine neue Liste zu 
    procedure SetClassDefList(Value: TClassDefList); virtual;
    //1 Setzt Optionen f�r die Darstellung des TreeViews 
    procedure SetDataItemTreeOptions(Value: TDataItemTreeOptions);
    //1 Gibt eine Klassendefinition zur�ck die nicht in die Liste eingetragen wird 
    procedure SetExpandedGroups(Value: TClassTypeGroupSet);
    //1 Setzt die neue Imagelist f�r zus�tzliche Bilder f�r die Items 
    procedure SetImages(Value: TImageList);
    //1 Legt die Hintergrundfarbe des Nodes fest 
    procedure TreeBeforeItemErase(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; ItemRect: TRect; 
      var ItemColor: TColor; var EraseAction: TItemEraseAction);
    //1 Freigebe des Records eines Knotens 
    procedure TreeFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    //1 Wird aufgerufen, bevor der Text eines Knoten geschrieben wird 
    procedure TreePaintText(Sender: TBaseVirtualTree; const TargetCanvas: TCanvas; Node: PVirtualNode; Column: 
      TColumnIndex; TextType: TVSTTextType);
  protected
    //1 Leitet den Event vond er Liste nach "aussen" 
    procedure DoAfterDataItemLoad(aList: TClassDefList); virtual;
    //1 Leitet den Event vond er Liste nach "aussen" 
    procedure DoBeforeDataItemsLoad(aList: TClassDefList; var aCancel: boolean); virtual;
    //1 Eventhandler f�r OnClick 
    procedure DoClick(Sender: TObject); virtual;
    //1 Eventhandler f�r OnDblClick 
    procedure DoDblClick(Sender: TObject); virtual;
    //1 nachdem ein Knoten neu selektiert wurde 
    procedure DoFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex); virtual;
    //1 Wird aufgerufen bevor der Fokus �ndert 
    procedure DoFocusChanging(Sender: TBaseVirtualTree; OldNode, NewNode: PVirtualNode; OldColumn, NewColumn: 
      TColumnIndex; var Allowed: Boolean); virtual;
    //1 Leitet den Event des TreeViews nach "aussen" 
    procedure DoGetHint(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var 
      CellText: WideString); virtual;
    //1 Leitet den Event des TreeViews nach "aussen" 
    procedure DoGetImage(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex; var 
      Ghosted: Boolean; var ImageIndex: Integer); virtual;
    //1 Leitet den Event des TreeViews nach "aussen" 
    procedure DoGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var 
      CellText: WideString); virtual;
    //1 Leitet den Event des TreeViews nach "aussen" 
    procedure DoHelpContext(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; var HelpContext: 
      Integer); virtual;
    //1 Leitet den Event des TreeViews nach "aussen" 
    procedure DoNewText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; NewText: WideString); 
      virtual;
    //1 Wird aufgerufen, bevor der Datenbaum gebildet wird 
    procedure DoSetAdditionalDataItems(Sender: TObject; aClassDefList: TClassDefList); virtual;
    //1 Leitet den Event des TreeViews nach "aussen" 
    procedure DoVTDragAllowed(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; var Allowed: Boolean);
      virtual;
    //1 Leitet den Event des TreeViews nach "aussen" 
    procedure DoVTDragDrop(Sender: TBaseVirtualTree; Source: TObject; DataObject: IDataObject; Formats: TFormatArray; 
      Shift: TShiftState; Pt: TPoint; var Effect: Integer; Mode: TDropMode); virtual;
    //1 Leitet den Event des TreeViews nach "aussen" 
    procedure DoVTDragOver(Sender: TBaseVirtualTree; Source: TObject; Shift: TShiftState; State: TDragState; Pt: TPoint;
      Mode: TDropMode; var Effect: Integer; var Accept: Boolean); virtual;
    //1 Gibt den DragModus an das TreeView weiter 
    procedure SetDragMode(Value: TDragMode); override;
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 F�gt einen Knoten zum TreeView hinzu 
    function AddVSTStructure(ANode: PVirtualNode; aDataItem: TBaseDataItem): PVirtualNode;
    //1 Zieht den Tree zusammen 
    procedure FullCollapse(Sender: TObject);
    //1 Klappt den ganzen Tree aus 
    procedure FullExpand(Sender: TObject);
    //1 gibt die globale L�ngeneinheit zur�ck 
    function GetLenBase: TLenBase;
    //1 Gibt das DataItem das mit dem selektierten Knoten verbunden ist, zur�ck 
    function GetNodeDataItem(aNode: PVirtualNode): TBaseDataItem;
    //1 Liste mit den anzuzeigenden DatenItems 
    property ClassDefList: TClassDefList read FClassDefList write SetClassDefList;
    //1 Alle offenen Gruppen 
    property ExpandedGroups: TClassTypeGroupSet read FExpandedGroups write SetExpandedGroups;
    //1 Zeigt an welches DataItem selektiert ist und gibt das DataItem zur�ck 
    property SelectedDataItem: TBaseDataItem read GetSelectedDataItem;
    //1 Gibt eine Liste mit den selektierten Items zur�ck 
    property Selection: TDataItemArray read GetSelection;
    //1 Gibt die Anzahl der selektierten DataItems zur�ck 
    property SelectionCount: Integer read GetSelectionCount;
  published
    //1 Optionen f�r die Darstellung des TreeViews 
    property DataItemTreeOptions: TDataItemTreeOptions read FDataItemTreeOptions write SetDataItemTreeOptions default 
      DefaultItemTreeOptions;
    //1 Zus�tzliche Images von einer externen Imageliste 
    property Images: TImageList read FImages write SetImages;
    //1 Farbe mit der jede zweite Zeiel eingef�rbt wird (dtoRowColor in Optionen gesetzt) 
    property LineColor: TColor read FLineColor write FLineColor;
    //1 Wird gefeuert, nachdem die DatenItems von der DB gelesen wurden 
    property OnAfterDataItemLoad: TAfterDataItemLoad read FOnAfterDataItemLoad write FOnAfterDataItemLoad;
    //1 Wird gefeuert bevor die DataItems von der DB gelesen werden 
    property OnBeforeDataItemsLoad: TBeforeDataItemsLoad read FOnBeforeDataItemsLoad write FOnBeforeDataItemsLoad;
    //1 OnClick Event 
    property OnClick: TNotifyEvent read FOnClick write FOnClick;
    //1 Wird beim Doppelklick ausgel�st 
    property OnDblClick: TNotifyEvent read FOnDblClick write FOnDblClick;
    //1 Wird aufgerufen nachdem der Fokus ge�ndert hat 
    property OnFocusChanged: TVTFocusChangeEvent read FOnFocusChanged write FOnFocusChanged;
    //1 Wird aufgerufen bevor der Fokus �ndert 
    property OnFocusChanging: TVTFocusChangingEvent read FOnFocusChanging write FOnFocusChanging;
    //1 Fragt den Hint f�r einen bestimmten Knoten ab 
    property OnGetHint: TVSTGetTextEvent read FOnGetHint write FOnGetHint;
    //1 Fragt den ImageIndex der Imagelist ab mit dem der aktuelle Knoten in der aktuellen Splate versehen werden soll 
    property OnGetImage: TVTGetImageEvent read FOnGetImage write FOnGetImage;
    //1 Fragt den Text f�r den Koten ab 
    property OnGetText: TVSTGetTextEvent read FOnGetText write FOnGetText;
    //1 Fragt den Help Kontext f�r den entsprechenden Knoten ab 
    property OnHelpContext: TVTHelpContextEvent read FOnHelpContext write FOnHelpContext;
    //1 Wird gefeuert, wenn der Text des Knotens fertig editiert wurde 
    property OnNewText: TVSTNewTextEvent read FOnNewText write FOnNewText;
    //1 Wird gefeuert bevor der Tree neu aufgebaut wird 
    property OnSetAdditionalDataItems: TSetAdditionalDataItems read FOnSetAdditionalDataItems write 
      FOnSetAdditionalDataItems;
    //1 Fragt ab ob DragDrop erlaubt ist 
    property OnVTDragAllowed: TVTDragAllowedEvent read FOnVTDragAllowed write FOnVTDragAllowed;
    //1 Wird aufgerufen, wenn ein Element "fallen gelassen" wird 
    property OnVTDragDrop: TVTDragDropEvent read FOnVTDragDrop write FOnVTDragDrop;
    //1 Wird aufgerufen, wenn mit einem Element �ber das TreeView gefahren wird 
    property OnVTDragOver: TVTDragOverEvent read FOnVTDragOver write FOnVTDragOver;
  end;
  

type
  PTreeData = ^TTreeData;
  TTreeData = record
    DataItem : TBaseDataItem;
  end;// TTreeData = record

const
  // Nummern der Spalten
  cPointerCol = 0;
  cNameCol    = 1;

implementation
{$R *.DFM}


uses
  mmMBCS, mmSpeedButton, Buttons, mmcs, Commctrl, ivDictio, SettingsReader;

// START resource string wizard section
resourcestring
  su_DataItemTree_SpleissKlassen  = '(*)Spleiss Klassen';     //ivlm
  su_DataItemTree_NotInitialized  = '(*)Nicht Initialisiert'; //ivlm
  su_DataItemTree_LabData         = '(*)Lab Daten';           //ivlm
  su_DataItemTree_Grobklassierung = '(*)Garn Klassen';        //ivlm
  su_DataItemTree_FFKlassen       = '(*)FF Klassen';          //ivlm
  su_DataItemTree_EigeneKlassen   = '(*)Eigene Klassen';      //ivlm
  su_DataItemTree_DataItems       = '(*)Basis Daten';         //ivlm

// END resource string wizard section
  sColumnHeader = '(*)Daten Elemente'; //ivlm
  sAbsolute     = '(30)Absolut';       //ivlm

const
  // Breite eines Toolbuttons
  cButtonWidth = 90;

  // NCButtons (Expand / Collapse)
  cButtonWidthHeight = 14;
  cButtonFrame       = 3;
  cButtonTop         = 2;
  cCollapseImage     = 8;
  cExpandImage       = 9;

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TDataItemTree.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

//: ----------------------------------------------
  (* Die Buttons f�r Expand und Collapse werden im Ereignis
     OnResize erzeugt (Erkl�rung siehe dort). *)

//: ----------------------------------------------
  // TreeView initialisieren
  mTree.NodeDataSize := SizeOf(TTreeData);
  
  // Farbe mit der jede zweite Zeile eingef�rbt wird ([dtoRowColor] in FDataItemTreeOptions)
  FLineColor := $EDF9FB;
  // Optionen f�r die Anzeige
  DataItemTreeOptions := DefaultItemTreeOptions;
  
  // Ereignis Handler (intern)
  mTree.OnBeforeItemErase  := TreeBeforeItemErase;
  mTree.OnFreeNode         := TreeFreeNode;
  mTree.OnPaintText        := TreePaintText;
  
  // Ereignis Handler (durchgeschlauft)
  mTree.OnGetImageIndex    := DoGetImage;
  mTree.OnGetText          := DoGetText;
  mTree.OnGetHint          := DoGetHint;
  mTree.OnFocusChanged     := DoFocusChanged;
  mTree.OnFocusChanging    := DoFocusChanging;
  mTree.OnGetHelpContext   := DoHelpContext;
  mTree.OnNewText          := DoNewText;
  mTree.OnClick            := DoClick;
  mTree.OnDblClick         := DoDblClick;
  mTree.OnDragAllowed      := DoVTDragAllowed;
  mTree.OnDragDrop         := DoVTDragDrop;
  mTree.OnDragOver         := DoVTDragOver;

//: ----------------------------------------------
  FClassDefList        := nil;
  mStartInternalImages := 0;
end;// TDataItemTree.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TDataItemTree.Destroy;
begin
  (* Keine Kopie sondern Referenz
  FreeAndNil(FClassDefList);*)

//: ----------------------------------------------
  inherited Destroy;
end;// TDataItemTree.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddVSTStructure
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (ANode, aDataItem)
 *
 *  Kurzbeschreibung: F�gt einen Knoten zum TreeView hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemTree.AddVSTStructure(ANode: PVirtualNode; aDataItem: TBaseDataItem): PVirtualNode;
var
  Data: PTreeData;
begin
  // Knoten hinzuf�gen
  Result:=mTree.AddChild(ANode);
  
  // Jetz den Record abf�llen
  mTree.ValidateNode(Result, False);
  Data:=mTree.GetNodeData(Result);
  if assigned(Data) then
    Data^.DataItem:=aDataItem;
end;// TDataItemTree.AddVSTStructure cat:No category

//:-------------------------------------------------------------------
(*: Member:           BuildItemsFromList
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt eine Klassendefinition zur�ck die nicht in die Liste eingetragen wird
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.BuildItemsFromList;
var
  xFirstLevelNode: PVirtualNode;
  xClassLevelNode: PVirtualNode;
  i: Integer;
  xOldCat: String;
  xOldPreDefClassGroup: TClassTypeGroup;
  xClassTypeGroup: TClassTypeGroup;
  xColumnHeader: String;
begin
  // Initialisieren
  xFirstLevelNode := nil;
  xClassLevelNode := nil;
  xOldPreDefClassGroup := ctCustomDataItem;
  xOldCat := 'NotInitialized';

//: ----------------------------------------------
  // Header der Spalte bestimmen
  xColumnHeader := sColumnHeader + ' [%s]';
  
  // Globale L�ngenbasis holen
  case GetLenBase of
    lbAbsolute : xColumnHeader := Format(xColumnHeader, [sAbsolute]);
    lb100KM    : xColumnHeader := Format(xColumnHeader, ['../100km']);
    lb1000KM   : xColumnHeader := Format(xColumnHeader, ['../1000km']);
    lb100000Y  : xColumnHeader := Format(xColumnHeader, ['../100000 yards']);
    lb1000000Y : xColumnHeader := Format(xColumnHeader, ['../1000000 yards']);
  end;// case GetLenBase of
  
  mTree.Header.Columns[cNameCol].Text := xColumnHeader;

//: ----------------------------------------------
  // Alle Verweise auf Gruppen Items l�schen
  for xClassTypeGroup := Low(mGroupPointerArrray) to High(mGroupPointerArrray) do
    mGroupPointerArrray[xClassTypeGroup] := nil;

//: ----------------------------------------------
  if assigned(FClassDefList) then begin
    // Alle alten DataItems l�schen
    mTree.Clear;
  
    with FClassDefList do begin
  
      // Jetzt werden noch die Anwendugsspezifischen DataItems geladen ...
      DoSetAdditionalDataItems(self, FClassDefList);
      // Und dann noch sortiert
      Sort;
  
      for i := 0 to DataItemCount - 1 do begin
        // DataItems ohne 'DisplayName' werden nicht angezeigt
        if DataItems[i].DisplayName <> '' then begin
  
          // Neue Kategorie
          if ((xOldCat <> DataItems[i].Category) and (not(DataItems[i].Predefined)))
              or ((xOldPreDefClassGroup <> DataItems[i].ClassTypeGroup) and (DataItems[i].Predefined))then begin
  
            case DataItems[i].Predefined of
  
              true: begin  // Vordefinierte Klassen
                // Neue Kategorie Merken
                xOldPreDefClassGroup := DataItems[i].ClassTypeGroup;
                xClassLevelNode := nil;
                case xOldPreDefClassGroup of
                  ctLongThickThin:
                    xClassLevelNode := AddVSTStructure(nil, GetDummyClassDef(su_DataItemTree_Grobklassierung));
                  ctSplice:
                    xClassLevelNode := AddVSTStructure(nil, GetDummyClassDef(su_DataItemTree_SpleissKlassen));
                  ctFF:
                    xClassLevelNode := AddVSTStructure(nil, GetDummyClassDef(su_DataItemTree_FFKlassen));
                  ctLabData:
                    xClassLevelNode := AddVSTStructure(nil, GetDummyClassDef(su_DataItemTree_LabData));
                  ctDataItem:
                    xClassLevelNode := AddVSTStructure(nil, GetDummyClassDef(su_DataItemTree_DataItems));
                end;
                mGroupPointerArrray[xOldPreDefClassGroup] := xClassLevelNode;
              end;// true: begin
  
              false: begin // Eigene Klassen
                // Neue Kategorie merken
                xOldCat := DataItems[i].Category;
  
                // F�r die erste eigene Klasse zuerst ein Root Node erzeugen
                if xFirstLevelNode = nil then begin
                  MakeCustomRootNode;
                  xFirstLevelNode := mCustomRootNode;
                  mGroupPointerArrray[ctCustomDataItem] := xFirstLevelNode;
                end;// if xFirstLevelNode = nil then begin
  
                (* Wenn die Kategorie leer ist, dann wird ein Eintrag als Child unter
                   Eigene Klassen hinzugef�gt *)
                if DataItems[i].Category <> '' then
                  xClassLevelNode := AddVSTStructure(xFirstLevelNode, GetDummyClassDef(xOldCat))
                else
                  xClassLevelNode := xFirstLevelNode;
              end;// false: begin
            end;//case ClassDefs[i].Predefined of
  
          end;// if xOldClass <> ClassDefs[i].Category then begin
  
          AddVSTStructure(xClassLevelNode, DataItems[i]);
        end;// if DataItems[i].DisplayName <> '' then begin
      end;// for i := 0 to mClassDefList.ClassDefCount - 1 do begin
    end;// with FClassDefList do begin
  end;// if assigned(FClassDefList) then begin

//: ----------------------------------------------
  SetExpandedGroups(FExpandedGroups);
end;// TDataItemTree.BuildItemsFromList cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoAfterDataItemLoad
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (aList)
 *
 *  Kurzbeschreibung: Leitet den Event vond er Liste nach "aussen"
 *  Beschreibung:     
                      Dieser Event kommt von der Liste und wird gefeuert 
                      bevor die DataItems von der Datenbank gelesen werden.
 --------------------------------------------------------------------*)
procedure TDataItemTree.DoAfterDataItemLoad(aList: TClassDefList);
begin
  if Assigned(FOnAfterDataItemLoad) then FOnAfterDataItemLoad(aList);
end;// TDataItemTree.DoAfterDataItemLoad cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoBeforeDataItemsLoad
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (aList, aCancel)
 *
 *  Kurzbeschreibung: Leitet den Event vond er Liste nach "aussen"
 *  Beschreibung:     
                      Dieser Event kommt von der Liste und wird gefeuert 
                      nachdem die DataItems von der Datenbank gelesen wurden.
 --------------------------------------------------------------------*)
procedure TDataItemTree.DoBeforeDataItemsLoad(aList: TClassDefList; var aCancel: boolean);
begin
  if Assigned(FOnBeforeDataItemsLoad) then FOnBeforeDataItemsLoad(aList, aCancel);
end;// TDataItemTree.DoBeforeDataItemsLoad cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoClick
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Eventhandler f�r OnClick
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.DoClick(Sender: TObject);
begin
  if assigned(FOnClick) then
    FOnClick(self);
end;// TDataItemTree.DoClick cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoDblClick
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Eventhandler f�r OnDblClick
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.DoDblClick(Sender: TObject);
begin
  if assigned(FOnDblClick) then
    FOnDblClick(self);
end;// TDataItemTree.DoDblClick cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoFocusChanged
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Column)
 *
 *  Kurzbeschreibung: nachdem ein Knoten neu selektiert wurde
 *  Beschreibung:     
                      Der Fokus ist nicht unbedingt gleichbedeutend mit der 
                      Selektion (Mehrfachselektion)
 --------------------------------------------------------------------*)
procedure TDataItemTree.DoFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);
var
  xData: TBaseDataItem;
  xHeight: Integer;
begin
  // Wenn die Beschreibung im Memo angezeigt werden soll
  if dtoShowDescription in DataItemTreeOptions then begin
    xData := GetNodeDataItem(Node);
  
    if assigned(xData) then begin
      // Memo nur anzeigen, wenn eine Beschreibung vorhanden ist
      if xData.Description > '' then begin
        mDescriptionMemo.visible := true;
  
        // Mit einem tempor�ren Bitmap die H�he einer Zeile des Textes bestimmen
        with TBitmap.Create do try
          // Den selben Font verwenden
          Canvas.Font := mDescriptionMemo.Font;
          xHeight := Canvas.TextHeight(xData.Description);
        finally
          free;
        end;// with TBitmap.Create do try
  
        // Text zuweisen
        mDescriptionMemo.Text := Translate(xData.Description);
        // Notwendige H�he des MEmo bestimmen und zuweisen
        xHeight := xHeight * mDescriptionMemo.Lines.count + 2;
        mDescriptionMemo.ClientHeight := xHeight;
  
        Application.ProcessMessages;
        // Eventuell muss der Knoten wieder in sichtweite gebracht werden
        if not(mTree.IsVisible[mTree.FocusedNode]) then
          mTree.ScrollIntoView(mTree.FocusedNode, false);
      end else begin
        // Memo ausblenden, wenn kein Beschreibung vorhanden ist
        mDescriptionMemo.visible := false;
      end;// if xData.Description > '' then begin
    end;// if assigned(xData) then begin
  end else begin
    mDescriptionMemo.visible := false;
  end;// if dtoShowDescription in DataItemTreeOptions then begin

//: ----------------------------------------------
  // Ereignis aufrufen
  if Assigned(FOnFocusChanged) then
    FOnFocusChanged(Sender, Node, Column);
end;// TDataItemTree.DoFocusChanged cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoFocusChanging
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender, OldNode, NewNode, OldColumn, NewColumn, Allowed)
 *
 *  Kurzbeschreibung: Wird aufgerufen bevor der Fokus �ndert
 *  Beschreibung:     
                      Der Fokus ist nicht unbedingt gleichbedeutend mit der 
                      Selektion (Mehrfachselektion)
 --------------------------------------------------------------------*)
procedure TDataItemTree.DoFocusChanging(Sender: TBaseVirtualTree; OldNode, NewNode: PVirtualNode; OldColumn, NewColumn: 
  TColumnIndex; var Allowed: Boolean);
begin
  // Code nach dem �ndern des Focus
  
  if Assigned(FOnFocusChanging) then FOnFocusChanging(Sender, OldNode, NewNode, OldColumn, NewColumn, Allowed);
end;// TDataItemTree.DoFocusChanging cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoGetHint
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Column, TextType, CellText)
 *
 *  Kurzbeschreibung: Leitet den Event des TreeViews nach "aussen"
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.DoGetHint(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: 
  TVSTTextType; var CellText: WideString);
begin
  if assigned(FOnGetHint) then
    FOnGetHint(Sender, Node, Column, TextType, CellText);
end;// TDataItemTree.DoGetHint cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoGetImage
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Kind, Column, Ghosted, ImageIndex)
 *
 *  Kurzbeschreibung: Leitet den Event des TreeViews nach "aussen"
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.DoGetImage(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: 
  TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
var
  xData: TBaseDataItem;
  
  function GetImageData(aDataItem: TBaseDataItem): Integer;
  begin
    Result := cDataItemImage;
  
    (* Es kann nicht mit dem Operator 'is' gearbeitet werden, da
       'DataItemType' eine Klassenreferenz ist *)
    if aDataItem.DataItemType.InheritsFrom(TClassDef) then begin
      if aDataItem.Predefined then begin
        Result := cMatrixTrend
      end else begin
        Result := cMatrixTrendEdit;
      end;// if aDataItem.Predefined then begin
      if aDataItem.DataItemType.InheritsFrom(TCompositClassDef) then
        result := cFunctionImage;
    end;// if xDataItem.DataItemType.InheritsFrom(TClassDef) then begin
  end;// function GetImageData(aDataItem: TBaseDataItem): Integer;
  
begin
  // Objekt holen
  xData := GetNodeDataItem(Node);
  
  if assigned(xData) then begin
    // F�r das Bild in der Namensspalte
    if Column = cNameCol then begin
      case Kind of
        ikNormal,
        ikSelected: begin
          if (dtoIcon in DataItemTreeOptions) then begin
            if not(xData is TGroupDataItem) then begin
              ImageIndex := GetImageData(xData) +  mStartInternalImages
            end else begin
              if Sender.Expanded[Node] then
                ImageIndex := cOpenDirImage + mStartInternalImages
              else
                ImageIndex := cClosedDirImage + mStartInternalImages;
            end;// if (Node.ChildCount = 0) then begin
          end;// if (dtoIcon in DataItemTreeOptions) then begin
        end;// ikNormal,ikSelected: begin
      end;// case Kind of
    end;// if Column = 0 then begin
  
    // Satzzeiger
    if (Sender.FocusedNode = Node) and (Column = cPointerCol) then
      ImageIndex := cDataPointerImage + mStartInternalImages;
  end;// if assigned(xData) then begin
end;// TDataItemTree.DoGetImage cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoGetText
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Column, TextType, CellText)
 *
 *  Kurzbeschreibung: Leitet den Event des TreeViews nach "aussen"
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.DoGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: 
  TVSTTextType; var CellText: WideString);
var
  xData: TBaseDataItem;
begin
  // Initialisieren
  xData := GetNodeDataItem(Node);

//: ----------------------------------------------
  if assigned(xData) then begin
    // Wenn die Klasse bereits von der Datenbank gel�scht ist, dann Eintrag disablen
    if xData.Deleted then
      Node.States := Node.States + [vsDisabled]
    else
      Node.States := Node.States - [vsDisabled];
  
    // Anzuzeigender Text kommt aus dem Objekt
    case Column of
      cPointerCol : CellText := '';
      cNameCol    : CellText := Translate(xData.DisplayName);
    end;// case Column of
  end;// if assigned(xData) then begin
end;// TDataItemTree.DoGetText cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoHelpContext
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Column, HelpContext)
 *
 *  Kurzbeschreibung: Leitet den Event des TreeViews nach "aussen"
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.DoHelpContext(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; var 
  HelpContext: Integer);
begin
  if assigned(FOnHelpContext) then
    FOnHelpContext(Sender, Node, Column, HelpContext);
end;// TDataItemTree.DoHelpContext cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoNewText
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Column, NewText)
 *
 *  Kurzbeschreibung: Leitet den Event des TreeViews nach "aussen"
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.DoNewText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; NewText: 
  WideString);
begin
  if assigned(FOnNewText) then
    FOnNewText(Sender, Node, Column, NewText);
end;// TDataItemTree.DoNewText cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoSetAdditionalDataItems
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender, aClassDefList)
 *
 *  Kurzbeschreibung: Wird aufgerufen, bevor der Datenbaum gebildet wird
 *  Beschreibung:     
                      Das Laden der DataItems von der Datenbank, bewirkt dass zuerst
                      die vorhandenen DataItems aus der Liste gel�scht werden. Nachdem
                      die DataItems von der Datenbank gelesen wurden, k�nnen mit diesem 
                      Event zus�tzliche DataItems hinzugef�gt werden.
 --------------------------------------------------------------------*)
procedure TDataItemTree.DoSetAdditionalDataItems(Sender: TObject; aClassDefList: TClassDefList);
begin
  if Assigned(FOnSetAdditionalDataItems) then FOnSetAdditionalDataItems(Sender, aClassDefList);
end;// TDataItemTree.DoSetAdditionalDataItems cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoVTDragAllowed
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node, Column, Allowed)
 *
 *  Kurzbeschreibung: Leitet den Event des TreeViews nach "aussen"
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.DoVTDragAllowed(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; var 
  Allowed: Boolean);
begin
  if assigned(FOnVTDragAllowed) then
    FOnVTDragAllowed(Sender, Node, Column, Allowed);
end;// TDataItemTree.DoVTDragAllowed cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoVTDragDrop
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Source, DataObject, Formats, Shift, Pt, Effect, Mode)
 *
 *  Kurzbeschreibung: Leitet den Event des TreeViews nach "aussen"
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.DoVTDragDrop(Sender: TBaseVirtualTree; Source: TObject; DataObject: IDataObject; Formats: 
  TFormatArray; Shift: TShiftState; Pt: TPoint; var Effect: Integer; Mode: TDropMode);
begin
  if assigned(FOnVTDragDrop) then
    FOnVTDragDrop(Sender, Source, DataObject, Formats, Shift, Pt, Effect, Mode);
end;// TDataItemTree.DoVTDragDrop cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoVTDragOver
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Source, Shift, State, Pt, Mode, Effect, Accept)
 *
 *  Kurzbeschreibung: Leitet den Event des TreeViews nach "aussen"
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.DoVTDragOver(Sender: TBaseVirtualTree; Source: TObject; Shift: TShiftState; State: TDragState; 
  Pt: TPoint; Mode: TDropMode; var Effect: Integer; var Accept: Boolean);
begin
  if assigned(FOnVTDragOver) then
    FOnVTDragOver(Sender, Source, Shift, State, Pt, Mode, Effect, Accept);
end;// TDataItemTree.DoVTDragOver cat:No category

//:-------------------------------------------------------------------
(*: Member:           FullCollapse
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Zieht den Tree zusammen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.FullCollapse(Sender: TObject);
begin
  mTree.FullCollapse;
end;// TDataItemTree.FullCollapse cat:No category

//:-------------------------------------------------------------------
(*: Member:           FullExpand
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Klappt den ganzen Tree aus
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.FullExpand(Sender: TObject);
begin
  mTree.FullExpand;
end;// TDataItemTree.FullExpand cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetDummyClassDef
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (aName)
 *
 *  Kurzbeschreibung: Gibt eine Klassendefinition zur�ck die nicht in die Liste eingetragen wird
 *  Beschreibung:     
                      Dummy Klassen werden f�r die "Verzeichnisse" von 
                      Klassendefinitionen gebraucht. Sie werden nicht in der Datenbank 
                      gesichert.
 --------------------------------------------------------------------*)
function TDataItemTree.GetDummyClassDef(aName: string): TGroupDataItem;
begin
  result := TGroupDataItem.Create;
  result.ItemName := aName;
end;// TDataItemTree.GetDummyClassDef cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetLenBase
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: gibt die globale L�ngeneinheit zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemTree.GetLenBase: TLenBase;
begin
  result := lb100KM;
  
  with TMMSettingsReader.Instance do begin
    // Initialisieren soweit notwendig
    if init then
      result := TLenBase(Value[cMMUnit]);
  end;// with TMMSettingsReader.Instance do begin
end;// TDataItemTree.GetLenBase cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetNodeDataItem
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (aNode)
 *
 *  Kurzbeschreibung: Gibt das DataItem das mit dem selektierten Knoten verbunden ist, zur�ck
 *  Beschreibung:     
                      Ist kein DataItem vorhanden wird nil zur�ckgegeben.
 --------------------------------------------------------------------*)
function TDataItemTree.GetNodeDataItem(aNode: PVirtualNode): TBaseDataItem;
begin
  // Initialisieren
  result := nil;
  if assigned(mTree.GetNodeData(aNode)) then
    result := PTreeData((mTree.GetNodeData(aNode))).DataItem;
end;// TDataItemTree.GetNodeDataItem cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetSelectedDataItem
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt das aktuelle DataItem zur�ck
 *  Beschreibung:     
                      Wenn mehrere Items selektiert sind, wird das jenige mit
                      dem Fokus zur�ckgegeben.
 --------------------------------------------------------------------*)
function TDataItemTree.GetSelectedDataItem: TBaseDataItem;
begin
  // Initialisieren
  result := GetNodeDataItem(mTree.FocusedNode);
  
  if assigned(result) then begin
    if result is TGroupDataItem then
      result := nil;
  end;// if assigned(xData) then begin
end;// TDataItemTree.GetSelectedDataItem cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetSelection
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt ein Array mit allen selektierten DataItems zur�ck
 *  Beschreibung:     
                      Es werden keine Gruppen im Array gespeichert.
 --------------------------------------------------------------------*)
function TDataItemTree.GetSelection: TDataItemArray;
var
  xNodeArray: TNodeArray;
  i: Integer;
  xData: TBaseDataItem;
  xSelectionIndex: Integer;
  
  procedure AddItemToSelection(xItem: TBaseDataItem);
  begin
    result[xSelectionIndex] := xItem;
    inc(xSelectionIndex);
  end;// procedure AddItemToSelection(xItem: TBaseDataItem);
  
begin
  // Es sind maximal alle selektierten Knoten im Array enthalten
  xSelectionIndex := 0;
  SetLength(result, mTree.SelectedCount);

//: ----------------------------------------------
  // Alle Selektierten Items abholen
  xNodeArray := mTree.GetSortedSelection(false);

//: ----------------------------------------------
  // Die Daten umkopieren
  for i := 0 to mTree.SelectedCount - 1 do begin
    // Initialisieren
    xData := GetNodeDataItem(xNodeArray[i]);
  
    if assigned(xData) then begin
      if not(xData is TGroupDataItem) then
        AddItemToSelection(xData);
    end;// if assigned(xData) then begin
  
  end;// for i := 0 to mTree.SelectedCount - 1 do begin

//: ----------------------------------------------
  // Das Resultat auf die richtige L�nge bringen
  SetLength(result, xSelectionIndex);
end;// TDataItemTree.GetSelection cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetSelectionCount
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die Anzahl der selektierten DataItems zur�ck
 *  Beschreibung:     
                      Gruppen Items werden nicht mitgez�hlt.
 --------------------------------------------------------------------*)
function TDataItemTree.GetSelectionCount: Integer;
begin
  result := High(GetSelection);
end;// TDataItemTree.GetSelectionCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           MakeCustomRootNode
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Erzeugt den Elternordner f�r die Eigenen Klassen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.MakeCustomRootNode;
begin
  mCustomRootNode := AddVSTStructure(nil, GetDummyClassDef(su_DataItemTree_EigeneKlassen));
end;// TDataItemTree.MakeCustomRootNode cat:No category

//:-------------------------------------------------------------------
(*: Member:           mTreeCollapsed
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn ein Knoten zusammengefaltet wird
 *  Beschreibung:     
                      Hier wird festgestellt ob das selektierte Item unsichtbar
                      wurde. Wenn ja, dann muss die Beschreibung ausgeblendet
                      werden.
 --------------------------------------------------------------------*)
procedure TDataItemTree.mTreeCollapsed(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  if assigned(mTree.FocusedNode) then
    if not(mTree.FullyVisible[mTree.FocusedNode]) then
      mDescriptionMemo.visible := false;
end;// TDataItemTree.mTreeCollapsed cat:No category

//:-------------------------------------------------------------------
(*: Member:           mTreeExpanded
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node)
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn ein Knoten aufgeht
 *  Beschreibung:     
                      Hier wird festgestellt ob das selektierte Item wieder sichtbar
                      wurde, und wenn ja ob die Beschreibung wieder eingeblendet
                      werden muss.
 --------------------------------------------------------------------*)
procedure TDataItemTree.mTreeExpanded(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  xData: TBaseDataItem;
begin
  xData := GetNodeDataItem(mTree.FocusedNode);
  
  if assigned(xData) then begin
    // Memo nur anzeigen, wenn eine Beschreibung vorhanden ist
    if (xData.Description > '') and (mTree.FullyVisible[mTree.FocusedNode]) then
      mDescriptionMemo.visible := true;
  end;// if assigned(xData) then begin
end;// TDataItemTree.mTreeExpanded cat:No category

//:-------------------------------------------------------------------
(*: Member:           mTreeResize
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Breite der Titel Leiste berechnen und setzen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.mTreeResize(Sender: TObject);
begin
  // Buttons erzeugen
  if not(assigned(mCollapseButton)) then begin
    mCollapseButton := TNCButton.Create(mTree);
    with mCollapseButton do begin
      mNCImages.GetBitmap(cCollapseImage, Glyph);
      Top := cButtonTop;
      Width := cButtonWidthHeight;
      Height := cButtonWidthHeight;
      Left := mTree.Width - Width - cButtonFrame;
      OnClick := FullCollapse;
    end;// with mCollapseButton do begin
  end;// if not(assigned(mCollapseButton)) then begin
  
  if not(assigned(mExpandButton)) then begin
    mExpandButton   := TNCButton.Create(mTree);
    with mExpandButton do begin
      mNCImages.GetBitmap(cExpandImage, Glyph);
      Top := cButtonTop;
      Width := cButtonWidthHeight;
      Height := cButtonWidthHeight;
      Left := mCollapseButton.Left - Width - cButtonFrame;
      OnClick := FullExpand;
    end;// with mExpandButton do begin
  end;// if not(assigned(mExpandButton)) then begin

//: ----------------------------------------------
  // Position der Buttons bestimmen
  if assigned(mCollapseButton) and assigned(mExpandButton) then begin
    with mCollapseButton do
      Left := mTree.Width - Width - cButtonFrame;
  
    with mExpandButton do
      Left := mCollapseButton.Left - Width - cButtonFrame;
  
    mButtonsLeft := mExpandButton.Left;
  end;// if assigned(mCollapseButton) and assigned(mExpandButton) then begin

//: ----------------------------------------------
  // Breite der Spalte festlegen
  mTree.Header.Columns[cNameCol].width := mButtonsLeft - cButtonFrame - 1;
end;// TDataItemTree.mTreeResize cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetClassDefList
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Weist eine neue Liste zu
 *  Beschreibung:     
                      Es wird eine Referenz auf eine Liste abgelegt.
 --------------------------------------------------------------------*)
procedure TDataItemTree.SetClassDefList(Value: TClassDefList);
begin
  FClassDefList := Value;

//: ----------------------------------------------
  (* Daten immer neu aufbauen, da es sein kann, dass die Liste
     intern ge�ndert wurde. *)
  if Value <> nil then
    BuildItemsFromList;
end;// TDataItemTree.SetClassDefList cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetDataItemTreeOptions
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt Optionen f�r die Darstellung des TreeViews
 *  Beschreibung:     
                      Die meisten Optionen m�ssen nicht direkt gesetzt werden, 
                      sondern werden bei Bedarf abgefragt.
 --------------------------------------------------------------------*)
procedure TDataItemTree.SetDataItemTreeOptions(Value: TDataItemTreeOptions);
var
  xValue: TDataItemTreeOptions;
  
  {(*-----------------------------------------------------------
    Erzeugt die Toolbuttons f�r die einzelnen DataItem Gruppen
  -------------------------------------------------------------*)
  procedure BuildButtons(aContainer: TComponent);
  begin
    mButtonCount := 0;
    (* Durchsucht alle Nodes (erstes Argument = nil) und ruft f�r jeden Knoten
       die Callback Funktion auf. Zus�tzlich wird nur nach Knoten mit Untereintr�gen
       gesucht.
       In der Callback Methode werden die Buttons erzeugt. *)
    mTree.IterateSubTree(nil, CallbackIterateSubTree, aContainer, [vsHasChildren]);
  end;// procedure BuildButtons(aContainer: TComponent);
  
  (*-----------------------------------------------------------
    L�scht die Buttons wieder, wenn sie nicht mehr ben�tigt werden
  -------------------------------------------------------------*)
  procedure DeleteButtons(aContainer: TComponent);
  var
    i: integer;
  begin
    i := 0;
    if aContainer = mToolButtons then begin
      while i < mToolButtons.ComponentCount do begin
        if mToolButtons.Components[i] is TmmSpeedButton then
          mToolButtons.Components[i].Free
        else
          inc(i);
      end;// while i < mToolButtons.ComponentCount do begin
    end;// if aContainer = mToolButtons then begin
  
    if aContainer = mGroupMenu then begin
      while mGroupMenu.Items.Count > 0 do
        mGroupMenu.Items[0].Free;
    end;// if aContainer = mGroupMenu then begin
  end;// procedure DeleteButtons(aContainer: TComponent);}
  
begin
  // Wert sichern
  xValue := FDataItemTreeOptions;

//: ----------------------------------------------
  if FDataItemTreeOptions <> Value then
  begin

//: ----------------------------------------------
  FDataItemTreeOptions := Value;

//: ----------------------------------------------
  // Grid Lines
  if dtoGridLines in DataItemTreeOptions then begin
    mTree.TreeOptions.PaintOptions := mTree.TreeOptions.PaintOptions + [toShowHorzGridLines];
    mTree.TreeOptions.PaintOptions := mTree.TreeOptions.PaintOptions + [toShowVertGridLines];
  end else begin
    mTree.TreeOptions.PaintOptions := mTree.TreeOptions.PaintOptions - [toShowHorzGridLines];
    mTree.TreeOptions.PaintOptions := mTree.TreeOptions.PaintOptions - [toShowVertGridLines];
  end;// if dtoGridLines in DataItemTreeOptions then begin

//: ----------------------------------------------
  if dtoMultiselection in DataItemTreeOptions then
    mTree.TreeOptions.SelectionOptions := mTree.TreeOptions.SelectionOptions + [toMultiselect]
  else
    mTree.TreeOptions.SelectionOptions := mTree.TreeOptions.SelectionOptions - [toMultiselect];

//: ----------------------------------------------
  // TreeView neu Zeichnen
  mTree.Invalidate;

//: ----------------------------------------------
  end;
end;// TDataItemTree.SetDataItemTreeOptions cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetDragMode
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Gibt den DragModus an das TreeView weiter
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.SetDragMode(Value: TDragMode);
begin
  inherited SetDragMode(Value);
  mTree.DragMode := Value;
end;// TDataItemTree.SetDragMode cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetExpandedGroups
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Gibt eine Klassendefinition zur�ck die nicht in die Liste eingetragen wird
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemTree.SetExpandedGroups(Value: TClassTypeGroupSet);
var
  xClassTypeGroup: TClassTypeGroup;
  
  procedure SetGroupOption(aClassTypeGroup: TClassTypeGroup);
  begin
    if (aClassTypeGroup in FExpandedGroups) then begin
      if assigned(mGroupPointerArrray[aClassTypeGroup])then
        mTree.FullExpand(mGroupPointerArrray[aClassTypeGroup])
    end else begin
      if assigned(mGroupPointerArrray[aClassTypeGroup])then
        mTree.FullCollapse(mGroupPointerArrray[aClassTypeGroup]);
    end;// if (ctDataItem in FExpandedGroups) then begin
  end;// procedure SetGroupOption(aClassTypeGroup: TClassTypeGroup);
  
begin
  if FExpandedGroups <> Value then
  begin

//: ----------------------------------------------

//: ----------------------------------------------
  FExpandedGroups := Value;

//: ----------------------------------------------

//: ----------------------------------------------
  end;

//: ----------------------------------------------
  for xClassTypeGroup := Low(TClassTypeGroup) to High(TClassTypeGroup) do
    SetGroupOption(xClassTypeGroup);
end;// TDataItemTree.SetExpandedGroups cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetImages
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die neue Imagelist f�r zus�tzliche Bilder f�r die Items
 *  Beschreibung:     
                      Die einzelnen Bilder werden in die interne Imagelist(mTreeImages) 
                      ab dem Index 0 hineinkopiert. Von aussen ist somit der Index der 
                      Bilder der selbe geblieben. 
                      Die internen Bilder werden �ber den Offset 'mStartInternalImages'
                      angesprochen.
 --------------------------------------------------------------------*)
procedure TDataItemTree.SetImages(Value: TImageList);
var
  i: Integer;
  xImage, xMask: TBitmap;
  xRect: TRect;
begin
  FImages := Value;

//: ----------------------------------------------
  // mStartInternalImages
  // Zuerst die alten Bilder l�schen
  for i := 0 to mStartInternalImages - 1 do
    mTreeImages.Delete(0);
  
  mStartInternalImages := 0;

//: ----------------------------------------------
  xRect := Rect(0, 0, mTreeImages.Width, mTreeImages.Height);
  
  xMask := nil;
  xImage := TBitmap.Create;
  try
    xImage.Height := mTreeImages.Height;
    xImage.Width  := mTreeImages.Width;
  
    xMask := TBitmap.Create;
    xMask.Monochrome := True;
    xMask.Height := mTreeImages.Height;
    xMask.Width  := mTreeImages.Width;
    for i := 0 to FImages.Count - 1 do begin
      xImage.Canvas.FillRect(xRect);
      ImageList_Draw(FImages.Handle, i, xImage.Canvas.Handle, 0, 0, ILD_NORMAL);
      xMask.Canvas.FillRect(xRect);
      ImageList_Draw(FImages.Handle, i, xMask.Canvas.Handle, 0, 0, ILD_MASK);
      mTreeImages.Insert(i, xImage, xMask);
    end;// for i := 0 to FImages.Count - 1 do begin
  finally
    FreeAndNil(xMask);
    FreeAndNil(xImage);
  end;// try finally
  
  mStartInternalImages := FImages.Count;
end;// TDataItemTree.SetImages cat:No category

//:-------------------------------------------------------------------
(*: Member:           TreeBeforeItemErase
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender, TargetCanvas, Node, ItemRect, ItemColor, EraseAction)
 *
 *  Kurzbeschreibung: Legt die Hintergrundfarbe des Nodes fest
 *  Beschreibung:     
                      Hier kann die Hintergrundfarbe jedes Nodes festgelegt werden. Die �bergebene 
                      Farbe wird verwendet um den Hintergerund zu l�schen.
 --------------------------------------------------------------------*)
procedure TDataItemTree.TreeBeforeItemErase(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; 
  ItemRect: TRect; var ItemColor: TColor; var EraseAction: TItemEraseAction);
var
  xItemHeight: Integer;
  xVisibleItemIndex: Integer;
begin
  if dtoRowColor in FDataItemTreeOptions then begin
    xItemHeight := ItemRect.Bottom - ItemRect.Top;
    xVisibleItemIndex := ItemRect.Top div xItemHeight;
  
    if not(Odd(xVisibleItemIndex)) then begin
      ItemColor := FLineColor;
      EraseAction := eaColor;
    end;// if Odd(xVisibleItemIndex) then begin
  end;// if dtoRowColor in FDataItemTreeOptions then begin
end;// TDataItemTree.TreeBeforeItemErase cat:No category

//:-------------------------------------------------------------------
(*: Member:           TreeFreeNode
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Node)
 *
 *  Kurzbeschreibung: Freigebe des Records eines Knotens
 *  Beschreibung:     
                      Hier m�ssen alle Elemente des Records freigegeben werden die mit einem
                      Pointer realisiert sind (Strings, Dynamische Array's, Klassen)
 --------------------------------------------------------------------*)
procedure TDataItemTree.TreeFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  xData: PTreeData;
begin
  // Initialisieren
  xData := mTree.GetNodeData(Node);

//: ----------------------------------------------
  if Assigned(xData) then begin
    (* Die Gruppen werden nicht ausserhalb verwaltet
       Deshalb m�ssen die Gruppen Items hier freigegeben werden.
       Es muss allerdings beachtet werden, dass die Liste mit den Items
       bereits zerst�rt ist, und deshalb d�rfen die anderen Items nicht
       nach ihrem Typ gefragt werden *)
    if mTree.HasChildren[node] then
      if (xData^.DataItem is TGroupDataItem) then
        FreeAndNil(xData^.DataItem);
  end;// if Assigned(xData) then begin
end;// TDataItemTree.TreeFreeNode cat:No category

//:-------------------------------------------------------------------
(*: Member:           TreePaintText
 *  Klasse:           TDataItemTree
 *  Kategorie:        No category 
 *  Argumente:        (Sender, TargetCanvas, Node, Column, TextType)
 *
 *  Kurzbeschreibung: Wird aufgerufen, bevor der Text eines Knoten geschrieben wird
 *  Beschreibung:     
                      Hier kann der Font jedes einzelnen Knotens ge�ndert werden.
 --------------------------------------------------------------------*)
procedure TDataItemTree.TreePaintText(Sender: TBaseVirtualTree; const TargetCanvas: TCanvas; Node: PVirtualNode; 
  Column: TColumnIndex; TextType: TVSTTextType);
begin
  // Gruppen kursiv darstellen oder nicht
  if (dtoGroupItalic in DataItemTreeOptions) then begin
    with TargetCanvas do begin
      if node.ChildCount > 0 then
        Font.Style := Font.Style + [fsItalic]
    end;// with TargetCanvas do begin
  end;// if (dtoGroupItalic in DataItemTreeOptions) then begin
  
  // Items Fett darstellen oder nicht
  if (dtoNodeBold in DataItemTreeOptions) then begin
    with TargetCanvas do begin
      if node.ChildCount = 0 then
        Font.Style := Font.Style + [fsBold]
    end;// with TargetCanvas do begin
  end;// if (dtoNodeBold in DataItemTreeOptions) then begin
end;// TDataItemTree.TreePaintText cat:No category

end.













