program DataItemTreeTest3;

uses
  MemCheck,
  Forms,
  u_MainWindow in 'u_MainWindow.pas' {MainWindow},
  mmVirtualStringTree in '..\..\..\Components\VCL\mmVirtualStringTree.pas';

{$R *.RES}

begin
{$IFDEF MemCheck}
  MemChk('DataItemTreeTest3');
{$ENDIF}
  Application.Initialize;
  Application.CreateForm(TMainWindow, MainWindow);
  Application.Run;
end.
