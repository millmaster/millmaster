(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: LabMasterDef.pas
| Projectpart...: Definition file for LabMaster
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
| 10.11.2000 1.00 Wss | File created
| 27.08.2001 1.00 Wss | Coarse/Fine QMatrixMode implemented
| 30.08.2001 1.00 Wss | Enable/disable visibility for filter values at printout
| 26.02.2002 1.00 Wss | Feld AdjustBaseCnt hinzugefuegt fuer korrekte SFI berechnung wie in Hostlink
| 13.06.2002 1.01 Nue | Zusaetze wegen Einbau Longterm.
| 10.07.2002 1.02 Nue | Conditional compiling added for OrgLabReport an MMUSB
| 18.07.2002 1.03 Nue | Name TParameters changed to TQueryParameters
| 04.10.2002 1.03 Wss | Option for Hierarchy table and DescendingSort member for Key sorting added
| 14.10.2002 1.03 Wss | COffCnt, CUpY Element hinzugef�gt
| 13.02.2003 1.03 Wss | Property UsedInFloor hinzugef�gt
|=============================================================================*)
unit LabMasterDef;

interface

uses
  Classes, DB, Graphics, Messages, IniFiles, Windows,
  Chart, QualityMatrixDef, MMUGlobal, ClassDef;
//  mmStringList,
//  LoepfeGlobal, BaseGlobal, ClassDef, mmSeries

resourcestring
  rsAll             = '(*)Alle'; // ivlm
  rsHidenFilterText = '(*)Anzeige ausgeschalten'; // ivlm
  rsPrintSettings   = '(20)Eigenschaften'; // ivlm
  rsNavigate        = '(14)Navigieren'; //ivlm
  rsAnalyse         = '(14)Auswerten'; //ivlm

type
//  TAxisItem         = (aiX, aiY, aiZ);
  TBarStyle         = (bsNormal, bsSide, bsStacked, bsStacked100);
  TChartType        = (ctNone, ctLine, ctBar, ctArea, ctMixed);
//  TYScalingType     = (ysNone,ys2,ys5,ys10,ys50,ys100,ys500,ys1000,ys1_2,ys1_10,ys1_20,ys1_100,ys1_200,ys1_1000,ys1_2000);
  TDataMode         = (dmChart, dmGrid, dmMatrix{, dmTemp});
  TStatistic        = (stMin,stMax,stMean,stStdDev,stQ90,stQ95,stQ99,{stLowerLimit,stUpperLimit,}stCount,stCV,stVariance,stData);

  TAxisType         = (atLeft, atLeftTop, atRight, atRightTop);
  TAxisScaling      = (asAutomatic, asManual, asDetermine);

  TDataModeSet      = set of TDataMode;
  TStatisticSet     = set of stMin..stMean;
  TSet10            = set of 1..10;
  TSet255           = set of 1..255;

const
  cPrefixCol  = 'c_';
  cPrefixCalc = 'calc_';
  cXLabelAngle: Array[Boolean] of Integer = (0, 300);

  cStatisticLegendStr: Array[stMin..stMean] of String = ('min', 'max', 'avg');

  cAvgTextWidth   = 5;  // needed for calculating pixels from DisplayWidth and back
  cDummySeriesCnt = 3;
  cDummyValuesCnt = 10;
  cDummyValues: Array[0..cDummySeriesCnt-1, 1..cDummyvaluesCnt] of Integer = (
    (1, 2, 4, 3, 6, 8, 3, 5, 9, 7),
    (3, 7, 1, 5, 9, 3, 2, 4, 6, 8),
    (4, 3, 2, 7, 5, 9, 5, 8, 2, 1)
  );

  cMaxCommaCount = 3;
  cNumberFormat: Array[0..cMaxCommaCount] of String = (
    '#0',
    '#0.0',
    '#0.0#',
    '#0.00#'
  );

  cChartAlign: Array[0..3] of TLegendAlignment = (laLeft, laRight, laTop, laBottom);
  cDisplayModeIndex: Array[0..1] of TDisplayMode = (dmCalculateSCValues, dmValues);

  cYScalingFactors: Array[0..3] of Double = (1.0, 2.0, 5.0, 10.0);

  // Diese Konstante definiert die Zeit f�r die TmmLineSeries, ab wieviel Zeitdifferenz
  // zwischen den St�tzdaten KEINE Verbindunslinie gezeichnet werden soll, damit
  // zeitliche Pausen von Artikeln auch optisch (=grafisch) angezeigt werden kann.
  // TmmLineSeries.DateTimeDelta := cSeriesDateTimeDeltas[]
  // In  der Draw-Methode wird momentan noch mit dem Faktor 2 gerechnet, um eine
  // gewisse Toleranz zu erhalten
  cSeriesDateTimeDeltas: Array[TTimeMode] of Single = (
    1/24,  // f�r Interval 1 Stunde wird als default genommen
    8/24,  // f�r Schicht 8 Stunden werden als default genommen
    7);    // f�r Langzeit 7 Tage werden als Default genommen

  cTabJumpWidth = 40; // wenn #9 f�r Textformatierungen verwendet wird dann entspricht dies einem TextWidth = 40

// wss, verwendet?
//const
//  // messages for data exchange
//  cWM_VIEWDATA_CHANGED = WM_USER + 1;
//  cWM_VIEWDATA_CLOSING = WM_USER + 2;

type
// wss, verwendet?
//  TListBoxQuery = (lbqStyle, lbqLot, lbqMach, lbqShifts);
//  TListBoxQuerySet = set of TListboxQuery;

  // used for drag mode in StringGrid for col moving
  TScrollDirection = (sdNone, sdLeft, sdRight);

// wss, verwendet?
//  TWhereStrArr = Array[TListBoxQuery] of String;

// wss, verwendet?
//  TParameterRec = record
//    Where: TWhereStrArr;
//  end;

type
  PAxisTypeRec = ^TAxisInfoRec;
  TAxisInfoRec = record
    Scaling: TAxisScaling;
    Min: Integer;
    Max: Integer;
  end;

  PReportOptions = ^TReportOptions;
  TReportOptions = record
    CompanyName: String;
    ReportTitle: String;
    ShowExtend: Boolean;
    UseYMSettings: Boolean;
    // Options for filter label
    ShowStyle: Boolean;
    ShowLot:   Boolean;
    ShowMachine: Boolean;
    ShowYMSettings: Boolean;
    // individual options
    PrintChart: Boolean;
    PrintQMatrix: Boolean;
    PrintCluster: Boolean;
    PrintSpliceMatrix: Boolean;
    PrintFFMatrix: Boolean;
    PrintYMSettings: Boolean;
    PrintTable: Boolean;
    StretchTable: Boolean;

    NewPageMatrix: Boolean;
    NewPageYMSettings: Boolean;
    NewPageTable: Boolean;

    RectChart: TRect;
    RectQMatrix: TRect;
    RectSpliceMatrix: TRect;
    RectFFMatrix: TRect;
  end;

  PFontRec = ^TFontRec;
  TFontRec = record
    Name  : TFontName;
    Color : TColor;
    BkColor: TColor;
    Size  : Integer;
    Style : TFontStyles;
  end;

  PScaleRec = ^TScaleRec;
  TScaleRec = record
    Autoscale:   Boolean;
    Min:         Longint;
    Max:         Longint;
    Logarithmic: Boolean;
  end;

  PGridOptions = ^TGridOptions;
  TGridOptions = record
    Options: TSet10;
    FixedCols: Integer;
    TitleFont: TFontRec;
    DataFont: TFontRec;
    Hierarchy: Boolean;
  end;

  PChartOptions = ^TChartOptions;
  TChartOptions = record
    BarStyle: TBarStyle;
    LegendAlign: TLegendAlignment;
    LeftAxis: TScaleRec;
    RightAxis: TScaleRec;
    View3D: Boolean;
    AllowRotate: Boolean;
    Width3D: Integer;
    RotateX: Integer;
    RotateY: Integer;
    EndColor: TColor;
    StartColor: TColor;
    GradientDir: Integer;
    ImageInside: Boolean;
    ImageFile: String;
    Zoom: Integer;
    HPos: Integer;
    VPos: Integer;
    XLabelRotated: Boolean;
    AxisInfo: Array[TAxisType] of TAxisInfoRec;
  end;

  PMatrixOptions = ^TMatrixOptions;
  TMatrixOptions = record
    ChannelVisible: Boolean;
    ClusterVisible: Boolean;
    SpliceVisible: Boolean;
    DefectValues: Boolean;
    CutValues: Boolean;
    ActiveVisible: Boolean;
    ActiveColor: TColor;
    MatrixMode: Integer; // data visible as coarse (0) or fine (1) matrix
  end;

  TUseMode   = (umNormal, umNever, umAlways);
  TMassField = (mfNone, mfLen, mfWT);

//  PDataItemRec = ^TDataItemRec;
  TDataItemRec = record
    // static settings
    DisplayName  : String[30];  // string visible in dbgrid and chart series
    Field        : String[30];  // associated field as calc_XXX format
    Col          : String[30];  // linked original database field as c_XXX
    KeyField     : Boolean;     // true: this field appears in the group statement if selected
    KeyFieldName : String;      // associated field name eg. c_style_id for c_style_name
    MassField    : TMassField;  // wird f�r die Gewichtung von Durchschnittswerten verwendet (Default: mfLen)
    DataType     : TFieldType;
    CalcMode     : Integer;        // indicates the mode or factor how to calculate
    UseMode      : TUseMode;
    Commas       : Integer;
    ShowSeconds  : Boolean;     // show seconds in Date/Time format
    Thousands    : Boolean;
    // Configurable settings
    // table info
    Alignment    : TAlignment;
    DisplayWidth : Integer;     // with in pixel of column size of dbgrid
    ShowInTable  : Boolean;     // true if this item is selected to be shown in grid
    TableIndex   : Integer;     // order position in table
    DescendingSort: Boolean;    // sort order in query statement
    // chart settings
    ShowInChart  : Boolean;     // true if this item is selected to be shown in chart
    ChartIndex   : Integer;     // order position in chart
    ChartType    : TChartType;  // none, line, bar, area
    Color        : TColor;      // color for chart or for key item if hierarchy table
    LowerLimit   : Single;      // limitiations for chart
    UpperLimit   : Single;
    RightAxis    : Boolean;     // True: RightAxis; False: LeftAxis
    Statistic    : TStatisticSet; // set of statistics
    AverageStep  : Integer;
    ClassTypeGroup: TClassTypeGroup;
//    YScalingType : TYScalingType;
  end;

// wss, verwendet?
//  TDefaultColorRec = record
//    Fieldname: String;
//    Color: TColor;
//  end;



//------------------------------------------------------------------------------
const
  cTimeModeTables: Array[TTimeMode] of String = (
    'v_production_interval v', 'v_production_shift v', 'v_production_week v'
  );

  cMassFields: Array[TMassField] of String = ('', 'c_len', 'c_wtSpd');

  cBasicQuery: Array[TTimeMode] of String = (
                // Interval
                'select %s %s ' +  // first key fields then data fiels (cQryDataCols)
                'from %s ' +
                'where v.c_shiftcal_id = %d ' +
                '%s ',             // zus�tzliche Where Anweisungen
                // shift
                'select %s %s ' +  // first key fields then data fiels (cQryDataCols)
                'from %s ' +
                'where v.c_shiftcal_id = %d ' +
                '%s ',             // zus�tzliche Where Anweisungen
                // Longterm
                'select %s %s ' +  // first key fields then data fiels (cQryDataCols)
                'from %s ' +
                'where 1 = %d ' +    //NUE nicht fuer ewig
                'and v.c_time_filter between :TimeFrom and :TimeTo ' +
                '%s '              // zus�tzliche Where Anweisungen
  );

//   cQryStyle   = 'select c_style_id, c_style_name from t_style ';
//   cQryLot     = 'select distinct c_style_id, c_prod_name from t_prodgroup';
//   cQryMach    = 'select c_machine_id, c_machine_name from t_machine ';

  cWhereStyleId_lr  = 'and v.c_style_id in (%s)';
  cWhereLotId_lr    = 'and v.c_prod_id in (%s)';
  cWhereMachId_lr   = 'and v.c_machine_id in (%s)';
  cWhereShiftId_lr  = 'and v.c_time_id in (%s)';
//  cWhereShiftId_lr  = 'and c_shift_id in (%s)';
  cWhereYMSetId_lr  = 'and v.c_ym_set_id in (%s)';

  cSelectCountField = '('+ '''#''' + '+cast(count(distinct %s) as char)) AS %s';

const
  // CalcMode:
  // 0  = Laengenbasiert (LenBase)
  // >0 = Laengenbasiert (Zahl entspricht Meter)
  // -1 = Keine Berechnung: Mode fuer 1:1 Durchreichung ab DB c_ -> calc_
  // -2 = SFI/D
  // -3 = SFI
  // -4 = ProdNutzeffekt
  // -5 = Temporaerer Mode fuer 1:1 Durchreichung ab DB c_ -> calc_ (in spaeterer Phase KeyField mit Visibility mit enum's erweitern! NUE

  cDataItemCount = 15;
  cDataItemArray: Array[0..cDataItemCount-1] of TDataItemRec = (
(*    // this are key fields
    (DisplayName:'(15)Artikel'; // ivlm
     Field:'style_name';   KeyField:True; KeyFieldName: 'c_style_name';   MassField:mfNone; DataType: ftString;   CalcMode: -1; Color: clInfoBk),
    (DisplayName:'(15)Partie';   // ivlm
     Field:'prod_name';    KeyField:True; KeyFieldName: 'c_prod_name';    MassField:mfNone; DataType: ftString;   CalcMode: -1; Color: clInfoBk),
    (DisplayName:'(15)Maschine'; // ivlm
     Field:'machine_name'; KeyField:True; KeyFieldName: 'c_machine_name'; MassField:mfNone; DataType: ftString;   CalcMode: -1; ShowInTable: True; TableIndex: 1; Color: clInfoBk),
    (DisplayName:'(25)Zeit';  // ivlm
     Field:'time';  {Col: 'c_week_start'; }KeyField:True; KeyFieldName: 'c_time';   DataType: ftDateTime; CalcMode: -1; ShowInTable: True; TableIndex: 0; Color: clInfoBk),
//     Field:'shift_start';  {Col: 'c_week_start'; }KeyField:True; KeyFieldName: 'c_shift_start';   DataType: ftDateTime; CalcMode: -1; ShowInTable: True; TableIndex: 0; Color: clInfoBk),
    // this are data fields

    (DisplayName:'%EffP';        Field:'PEff';    KeyField:False; MassField:mfWT; DataType: ftFloat; CalcMode: -4; UseMode: umNever; Color: 65280), //
    (DisplayName:'Weight';       Field:'Wei';     KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: -1;Color: 128), //
//    (DisplayName:'Len';          Field:'Len';     KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; UseMode: umAlways; Color: 255),
    (DisplayName:'Weight kg';    Field:'kg';      KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 128),
    (DisplayName:'Weight lb';    Field:'lb';      KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 128),
    (DisplayName:'Len km';       Field:'km';      KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 255),
    (DisplayName:'Len yards';    Field:'yards';   KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 255),
    (DisplayName:'SFI';          Field:'SFI';     KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: -3; UseMode: umAlways; Color: 16777041; ClassTypeGroup: ctLabData), //
    (DisplayName:'SFI/D';        Field:'SFI_D';   KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: -2; UseMode: umNever; Color: 16570115; ClassTypeGroup: ctLabData), //
    (DisplayName:'CSFI';         Field:'CSFI';    KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;  Color: 1451435), //
*)
    (DisplayName:'CYTot';        Field:'CYTot';   KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    ShowInTable: True; TableIndex: 2; ShowInChart: True; ChartIndex: 2; Color: 16744576), //
    (DisplayName:'CN';           Field:'CN';      KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 14802077), //
    (DisplayName:'CS';           Field:'CS';      KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 32896), //
    (DisplayName:'CL';           Field:'CL';      KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 4210688), //
    (DisplayName:'CT';           Field:'CT';      KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 12615808), //
    (DisplayName:'CFF';          Field:'CSiro';   KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 1341291), //
    (DisplayName:'CSp';          Field:'CSp';     KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 1341291), //

(*    (DisplayName:'CCl';          Field:'CCl';     KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 1424106), //
    (DisplayName:'CClFF';        Field:'CSIROCl'; KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 16711680), //
    (DisplayName:'YB';           Field:'YB';      KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 16744703), //
    (DisplayName:'Sp';           Field:'Sp';      KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 16711808), //
    (DisplayName:'COffCnt';      Field:'COffCnt'; KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 13251949), //
    (DisplayName:'CUpY';         Field:'CUpY';    KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 65535), //

*)
    (DisplayName:'INeps/km';     Field:'INeps';   KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 1000; Color: 14079702; ClassTypeGroup: ctLabData), //
    (DisplayName:'ISmall/m';     Field:'ISmall';  KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 1;    Color: 204; ClassTypeGroup: ctLabData), //
    (DisplayName:'IThick/km';    Field:'IThick';  KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 1000; Color: 183; ClassTypeGroup: ctLabData), //
    (DisplayName:'IThin/km';     Field:'IThin';   KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 1000; Color: 7783934; ClassTypeGroup: ctLabData), //
    (DisplayName:'I2-4/km';      Field:'I2_4';    KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 1000; Color: 12711922; ClassTypeGroup: ctLabData), //
    (DisplayName:'I4-8/km';      Field:'I4_8';    KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 1000; Color: 293070; ClassTypeGroup: ctLabData), //
    (DisplayName:'I8-20/km';     Field:'I8_20';   KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 1000; Color: 15573757; ClassTypeGroup: ctLabData), //
    (DisplayName:'I20-70/km';    Field:'I20_70';  KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 1000; Color: 14411488; ClassTypeGroup: ctLabData) //
(*
    // this field are always required
    (DisplayName:'';   Field:'Len';           KeyField:False; DataType: ftFloat; CalcMode: -1; UseMode: umAlways),
    (DisplayName:'';   Field:'SFICnt';        KeyField:False; DataType: ftFloat; CalcMode: -1; UseMode: umAlways),
    (DisplayName:'';   Field:'AdjustBase';    KeyField:False; DataType: ftFloat; CalcMode: -1; UseMode: umAlways),
    (DisplayName:'';   Field:'rtSpd';         KeyField:False; DataType: ftFloat; CalcMode: -1; UseMode: umAlways),
    (DisplayName:'';   Field:'wtSpd';         KeyField:False; DataType: ftFloat; CalcMode: -1; UseMode: umAlways),
    (DisplayName:'';   Field:'AdjustBaseCnt'; KeyField:False; DataType: ftFloat; CalcMode: -1; UseMode: umAlways)*)
  );

//  cYScalingStr : array[TYScalingType] of string[10] = ('1x','2x','5x','10x','50x','100x','500x','1000x','1/2x','1/10x','1/20x','1/100x','1/200x','1/1000x','1/2000x');
//------------------------------------------------------------------------------
(**
  // Default Feldfarben
  cDefaultColor : Array[0..9] of TDefaultColorRec = (
    // imperfection

    (Fieldname : 'calc_LckCl';              Color: 16570115  ),
     // Offlimit
    (Fieldname : 'calc_OfflineRatio';       Color: 16744576),
    (Fieldname : 'c_offline_offlimit_time'; Color:  8421376),
    (Fieldname : 'c_YB_offline_time';       Color:  12615680),
    (Fieldname : 'c_Eff_offline_time';      Color:  8421631),
    (Fieldname : 'c_CSp_offline_time';      Color:  16711680  ),
    (Fieldname : 'c_RSp_offline_time';      Color:  32896  ),
    (Fieldname : 'c_CBu_offline_time';      Color:  12615935  ),
    (Fieldname : 'c_CUpY_offline_time';     Color:  65535  ),
    (Fieldname : 'c_tLSt_offline_time';     Color:  16744576  ),
    (Fieldname : 'c_LSt_offline_time';      Color:  8453888  ),
    (Fieldname : 'c_CSIRO_offline_time';    Color:  128  ),
    (Fieldname : 'c_CYTot_offline_time';    Color:  16776960  ),

   // Machine
    (Fieldname : 'calc_npProdGrp';          Color: 16711680  ),
    (Fieldname : 'calc_MEff';               Color:  65535  ),
    (Fieldname : 'c_Bob';                   Color:  33023  ),
    (Fieldname : 'c_Cones';                 Color:  8421376  ),

//    (Fieldname : 'calc_COFFCNT';            Color:  13251949  ),
    (Fieldname : 'c_LSt';                   Color:  5530027  ),

    (Fieldname : 'c_LStProd';               Color:  10132324  ),
    (Fieldname : 'calc_CUPY';               Color: 16777143  ),
    (Fieldname : 'calc_CBU';                Color:  13372404  ),
    (Fieldname : 'calc_CSP';                Color: 5532130  ),
    (Fieldname : 'calc_CSYS';               Color: 15456472  ),
    (Fieldname : 'calc_RSP';                Color:  16766333  ),
    (Fieldname : 'calc_PCSp';               Color: 6619057  ),
    (Fieldname : 'calc_PRSp';               Color: 16512  ),
    (Fieldname : 'c_LckSys';                Color: 10976513  ),
    (Fieldname : 'c_LckSIRO';               Color: 16711680  ),

    (Fieldname : 'calc_ManSt';              Color: 8684799  ),
    (Fieldname : 'calc_UCLS';               Color: 9623533  ),
    (Fieldname : 'calc_UCLL';               Color: 2023734  ),
    (Fieldname : 'calc_UCLT';               Color: 2872763  ),
    (Fieldname : 'calc_CDBU';               Color: 15513591  ),
    (Fieldname : 'c_tRed';                  Color: 16745665  ),
    (Fieldname : 'c_tYell';                 Color: 13676240  ),
    (Fieldname : 'c_tManSt';                Color: 261291  ),
    (Fieldname : 'calc_LckCl';              Color: 16570115  ),
{}
 );
(**)
//------------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units

end.

