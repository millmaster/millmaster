unit u_MainWindow;

interface


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton, mmEdit, mmCheckBox,
  ExtCtrls, mmPanel, mmStaticText, mmGroupBox, VirtualTrees, ComCtrls,
  ToolWin, mmToolBar, mmSplitter, mmListBox, mmLabel, Buttons,
  mmSpeedButton, ImgList, mmImageList, Menus, mmMainMenu, mmBitBtn,
  Db, ADODB, mmADODataSet, mmADOConnection, mmDataSource, Grids, DBGrids,
  mmDBGrid, mmComboBox, CheckLst, mmCheckListBox, DataItemTree, mmuglobal,
  ClassDef, LabMasterDef, LabReportClass;
// Wenn "Classdefects" definiert ist muss die unit ClassDef lokal wierd umbenannt werden
{$define Classdefects}
{$define _LABREPORT}

type
  TMainWindow = class(TForm)
    mmPanel1: TmmPanel;
    mmGroupBox1: TmmGroupBox;
    mmCheckBox1: TmmCheckBox;
    mmCheckBox2: TmmCheckBox;
    mmCheckBox3: TmmCheckBox;
    mmCheckBox4: TmmCheckBox;
    mmCheckBox5: TmmCheckBox;
    mmSplitter1: TmmSplitter;
    mmPanel2: TmmPanel;
    mmGroupBox2: TmmGroupBox;
    bToolButtons: TmmCheckBox;
    mmCheckBox6: TmmCheckBox;
    mmCheckBox7: TmmCheckBox;
    mmCheckBox8: TmmCheckBox;
    mmDBGrid1: TmmDBGrid;
    mmDataSource1: TmmDataSource;
    conDefault: TmmADOConnection;
    dseQuery: TmmADODataSet;
    mmPanel3: TmmPanel;
    mmPanel4: TmmPanel;
    mmPanel5: TmmPanel;
    mmLabel2: TmmLabel;
    mmLabel3: TmmLabel;
    mmLabel4: TmmLabel;
    mmLabel5: TmmLabel;
    mmLabel6: TmmLabel;
    mmLabel7: TmmLabel;
    bCreateTempTable: TmmBitBtn;
    edMachId: TmmEdit;
    edProdID: TmmEdit;
    edStyleID: TmmEdit;
    EdYMSetID: TmmEdit;
    edTo: TmmEdit;
    edFrom: TmmEdit;
    mmLabel1: TmmLabel;
    bGetFields: TmmButton;
    cbTimeMode: TmmComboBox;
    mmLabel8: TmmLabel;
    lbSelected: TmmCheckListBox;
    bGetConfigString: TmmButton;
    bSetConfigString: TmmButton;
    mAdditionalTreeImages: TmmImageList;
    mmCheckBox9: TmmCheckBox;
    mTree: TDataItemTree;
    IndexOf: TmmBitBtn;
    mmStaticText1: TmmLabel;
    mmStaticText2: TmmLabel;
    bExpandDefault: TmmButton;
    laExpanded: TmmLabel;
    mmButton1: TmmButton;
    procedure FormCreate(Sender: TObject);
    procedure mmCheckBox1Click(Sender: TObject);
    procedure lbSelectedDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure lbSelectedDragDrop(Sender, Source: TObject; X, Y: Integer);
    // 1 Wird aufgerufen nachdem der Fokus ge�ndert hat
    procedure DoFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);
    // 1 Wird aufgerufen bevor der Fokus �ndert
    procedure DoFocusChanging(Sender: TBaseVirtualTree; OldNode, NewNode: PVirtualNode; OldColumn, NewColumn:
      TColumnIndex; var Allowed: Boolean);
    procedure AddStandardItems(Sender: TObject; aClassDefList: TClassDefList);
    procedure bCreateTempTableClick(Sender: TObject);
    procedure bListBoxClearClick(Sender: TObject);
    procedure bGetFieldsClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure bGetConfigStringClick(Sender: TObject);
    procedure bSetConfigStringClick(Sender: TObject);
    procedure mTreeGetImage(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Kind: TVTImageKind; Column: TColumnIndex; var Ghosted: Boolean;
      var ImageIndex: Integer);
    procedure IndexOfClick(Sender: TObject);
    procedure bExpandDefaultClick(Sender: TObject);
    procedure mmButton1Click(Sender: TObject);
  private
    {$ifdef LABREPORT}
      mClassDefContainer: TLabDataItemList;
    {$else}
      mClassDefContainer: TClassDefContainer;
    {$endif}

    mQueryParameters: TQueryParameters;
    FTest: TRect;
    procedure FillContainer;
    procedure GetDataItemGenerateSQL(Sender: TObject;
      aDataItem: TBaseDataItem; var aEnabled: boolean);
    procedure QueryContainer;
  public
    { Public declarations }
    property test: TRect read FTest write FTest;
  end;

  (*---------------------------------------------------------
    Dekorierer f�r die Testanwendung
  ----------------------------------------------------------*)
  TTestDataItem = class (TDataItemDecorator)
  private
    Ftest: Integer;
  public
    // 1 Beinhaltet die Tabelle mit dem Tabellenalias von der die Daten gesammelt werden
    function GetSQLFromTable: String; override;
    // 1 Gibt die Spalten zur�ck die im SELECT Statement stehen m�ssen
    function GetSQLSelectFields(aWithSum: boolean = true; aWithAlias: boolean = true): String; override;
    // 1 Beinhaltet die Where-Klausel (Join) die die Felder betreffen
    function GetSQLWhere: String; override;
    //1 Kopiert die Eigenschaften von Source   
    procedure Assign(Source: TPersistent); override;
    property test: Integer read Ftest write Ftest;
  end;// TTestDataItem = class (TDataItemDecorator)

  TNewPropertyRec = record
    test:integer;
  end;

var
  MainWindow: TMainWindow;

implementation

{$R *.DFM}

uses
  TypInfo, mmCS, LoepfeGlobal;

const
    cRowColorTag     =  1;
    cIconTag         =  2;
    cGroupItalicTag  =  3;
    cNodeBoldTag     =  4;
    cGridLinesTag    =  5;
    cToolButtons     =  6;
    cDropDownButton  =  7;
    cHideOtherGroups =  8;
    cShowDescription =  9;
    cTabControl      = 10;

    {$ifdef LABREPORT}
      cItemClass: TDataItemDecoratorClass =  TLabDataItem;
    {$else}
      cItemClass: TDataItemDecoratorClass =  TTestDataItem;
    {$endif}

var
  // Globale Variable f�r die slektierten Items
  {$ifdef LABREPORT}
    lClassDefContainer: TLabDataItemList = nil;
  {$else}
    lClassDefContainer: TClassDefContainer = nil;
  {$endif}

  // Wird mit GetConfigString abgef�llt (Button)
  lLastSelectionConfigString : string = '';

{ TTestDataItem }

(*---------------------------------------------------------
  F�r jedes DataItem sicherstellen, dass nur f�r Klassendefinitionen
  das Query zusammengestellt wird (nur als Beispiel ohne spezielle Funktion)
----------------------------------------------------------*)
procedure TTestDataItem.Assign(Source: TPersistent);
begin
  if Source is TTestDataItem then begin
    Test := TTestDataItem(Source).Test;
  end;//
  inherited;
end;

function TTestDataItem.GetSQLFromTable: String;
begin
  result := inherited GetSQLFromTable;
end;// function TTestDataItem.GetSQLFromTable: String;

(*---------------------------------------------------------
  F�r jedes DataItem sicherstellen, dass nur f�r Klassendefinitionen
  das Query zusammengestellt wird (nur als Beispiel ohne spezielle Funktion)
----------------------------------------------------------*)
function TTestDataItem.GetSQLSelectFields(aWithSum: boolean = true; aWithAlias: boolean = true): String;
begin
    result := inherited GetSQLSelectFields(aWithSum, aWithAlias);
end;// function TTestDataItem.GetSQLSelectFields(aWithSum: boolean = true; aWithAlias: boolean = true): String;

(*---------------------------------------------------------
  F�r jedes DataItem sicherstellen, dass nur f�r Klassendefinitionen
  das Query zusammengestellt wird (nur als Beispiel ohne spezielle Funktion)
----------------------------------------------------------*)
function TTestDataItem.GetSQLWhere: String;
begin
  result := inherited GetSQLWhere;
end;// function TTestDataItem.GetSQLWhere: String;

{ TMainWindow }

(*---------------------------------------------------------
  Initialisierung
----------------------------------------------------------*)
procedure TMainWindow.FormCreate(Sender: TObject);
begin
  CodeSite.Clear;

  (* Programm interner Container. Die zuweisung kopiert diesen Container
     somit muss die Anwendung diese Instanz selber wieder freigeben *)
  {$ifdef LABREPORT}
    mClassDefContainer := TLabDataItemList.Create;
    TLabDataItemList(mClassDefContainer).InitDataItems(0);
  {$else}
    mClassDefContainer := TClassDefContainer.Create;
  (* L�dt die definierten Superklassen von der Datenbank. bevor die
     Daten geladen werden, wird das Ereignis 'OnSetAdditionalDataItems' gefeuert.
     Dieses Ereignis wird immer dann gefeuert, wenn der Baum des TreeViews neu aufgebaut
     werden muss. *)
    mClassDefContainer.LoadFromDatabase(cItemClass);
  {$endif}

  mQueryParameters := TQueryParameters.Create;
  mClassDefContainer.QueryParameters := mQueryParameters;

  // Container zuweisen (wird kopiert)
  mTree.ClassDefList := mClassDefContainer;

  cbTimeMode.ItemIndex := 1;
end;// procedure TMainWindow.FormCreate(Sender: TObject);

(*---------------------------------------------------------
  Behandlungsroutine f�r die Checkboxen die das Aussehen des Frames
  steuern.
  Das Aussehen des Frames wird mittels eines Optionen Set
  (mTree.DataItemTreeOptions: TDataItemTreeOptions) gesteuert.
----------------------------------------------------------*)
procedure TMainWindow.mmCheckBox1Click(Sender: TObject);
var
  xChecked : boolean;
  xOptions : TDataItemTreeOptions;
  xMsg: string;

  // --- F�gt Elemente im Set hinzu oder l�scht sie ---
  procedure InEx(aItem: TDataItemTreeOption);
  begin
    if xChecked then
      include(xOptions, aItem)
    else
      exclude(xOptions, aItem);
  end;// procedure InEx(aItem: TDataItemTreeOption);
begin
  if Sender is TCheckBox then begin
    // Status der entsprechenden Checkbox feststellen
    xChecked := TCheckBox(Sender).Checked;

    // Optionen in eine lokale Variable laden
    xOptions := mTree.DataItemTreeOptions;

    // Je nach Checkbox die entsprechende Option setzen oder l�schen
    case TCheckBox(Sender).Tag of
      cRowColorTag     : InEx(dtoRowColor);
      cIconTag         : InEx(dtoIcon);
      cGroupItalicTag  : InEx(dtoGroupItalic);
      cNodeBoldTag     : InEx(dtoNodeBold);
      cGridLinesTag    : InEx(dtoGridLines);
      cToolButtons     : InEx(dtoToolButtons);
      cDropDownButton  : InEx(dtoToolMenuButton);
      cHideOtherGroups : InEx(dtoHideOtherGroups);
      cShowDescription : InEx(dtoShowDescription);
      cTabControl      : InEx(dtoTabControl);
    else
      xMsg := 'TMainWindow.mmCheckBox1Click: Typ nicht Implementiert (';
      xMsg := xMsg + GetEnumName(TypeInfo(TDataItemTreeOption), TCheckBox(Sender).Tag - 1) + ')';
      raise
        Exception.Create(xMsg);
    end;// case TCheckBox(Sender).Tag of

    // Optionen setzen
    mTree.DataItemTreeOptions := xOptions;
  end;// if Sender is TCheckBox then begin
end;// procedure TMainWindow.mmCheckBox1Click(Sender: TObject);

(*---------------------------------------------------------
  wird aufgerufen nachdem ein neues Element selektiert wurde
----------------------------------------------------------*)
procedure TMainWindow.DoFocusChanged(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex);
begin
  if assigned(mTree.SelectedDataItem) then
    CodeSite.SendNote('Select ' + mTree.SelectedDataItem.ItemName);
end;// procedure TMainWindow.DoFocusChanged(...);

(*---------------------------------------------------------
  Wird aufgerufen, wenn bevor ein neues Element selektiert wird
----------------------------------------------------------*)
procedure TMainWindow.DoFocusChanging(Sender: TBaseVirtualTree; OldNode,
  NewNode: PVirtualNode; OldColumn, NewColumn: TColumnIndex;
  var Allowed: Boolean);
begin
  if assigned(mTree.SelectedDataItem) then
    CodeSite.SendNote('Deselect ' + mTree.SelectedDataItem.ItemName);
end;// procedure TMainWindow.DoFocusChanging(...);

(*---------------------------------------------------------
  F�gt die Standard Items wie '%EffP' hinzu. Diese Funktion
  wird immer dann vom TDataItemTree aufgerufen, wenn der BAum
  des TreeViews neu aufgebaut wird.
----------------------------------------------------------*)
procedure TMainWindow.AddStandardItems(Sender: TObject; aClassDefList: TClassDefList);
var
  xClassDefContainer: TClassDefContainer;

  (*---------------------------------------------------------
    Erzeugt das neue DataItem
  ----------------------------------------------------------*)
  procedure SetNewClass(aName,aDescription: string ;aOrderPos: integer);
  var
    xIndex: integer;
  begin
    (* Neues DataItem mit der gew�nschten Klasse erzeugen und anhand
       des zur�ckgegebenen Index auf das neue Item zuggreiffen*)
    xIndex := xClassDefContainer.CreateAndAddNewDataItem(TDataItem, cItemClass);
    // Eigenschaften des neuen Items setzen
    xClassDefContainer[xIndex].ClassTypeGroup   := ctDataItem;
    xClassDefContainer[xIndex].Description      := aDescription;
    xClassDefContainer[xIndex].ItemName         := aName;
//    xClassDefContainer[xIndex].DisplayName      := '';
    xClassDefContainer[xIndex].OrderPos         := aOrderPos;
    xClassDefContainer[xIndex].Predefined       := true;
    TTestDataItem(xClassDefContainer[xIndex]).Test := xIndex;
  end;// procedure FillClass(aDataItem: TBaseDataItem; aName,aDescription: string ;aOrderPos: integer);

begin
  {$ifndef LABREPORT}
    if aClassDefList is TClassDefContainer then begin
      xClassDefContainer := TClassDefContainer(aClassDefList);

      // Einzelne Items hinzuf�gen
      SetNewClass('%EffP','',0);
      SetNewClass('CCI','',1);
      SetNewClass('CCIFF','',02);
      SetNewClass('CL','',3);
      SetNewClass('CN','',4);
      SetNewClass('COffCnt','',5);
      SetNewClass('CS','',6);
      SetNewClass('CSFI','',7);
      SetNewClass('CSp','',8);
      SetNewClass('CT','',9);
      SetNewClass('CUpY','',10);
      SetNewClass('CYTot','',11);
      SetNewClass('Len','',20);
      SetNewClass('SFI','',21);
      SetNewClass('SFI/D','',22);
      SetNewClass('Sp','',23);
      SetNewClass('Weight','',24);
      SetNewClass('YB','',25);
    end;// if aClassDefList is TClassDefContainer then begin
  {$endif}
end;// procedure TMainWindow.AddStandardItems(aClassDefList: TClassDefContainer);

(*---------------------------------------------------------
  Listbox kann mit Drag & Drop "bef�llt" werden
----------------------------------------------------------*)
procedure TMainWindow.lbSelectedDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := true;
end;// procedure TMainWindow.lbSelectedDragOver(...

(*---------------------------------------------------------
  Selektierte DataItems in die Listbox kopieren
----------------------------------------------------------*)
procedure TMainWindow.lbSelectedDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  xDataItemArray: TDataItemArray;
  i:integer;
begin
  xDataItemArray := nil;
  (* In dieser Anwendung eigentlich immer der Fall, da das TreeView
     die einzige Quelle f�r Drag&Drop Funktionen ist*)
  if Source is TVirtualStringTree then begin
    // Array mit allen Selektierten DataItems holen
    xDataItemArray := mTree.Selection;
    // Alle sleektierten Items per Namen aus dem Array in die Listbox abf�llen
    for i:=0 to High(xDataItemArray) do begin
      lbSelected.Items.Add(xDataItemArray[i].ItemName);
    end;// for i:=0 to High(xDataItemArray) do begin
  end;// if Source is TVirtualStringTree then begin
end;// procedure TMainWindow.lbSelectedDragDrop(Sender, Source: TObject; X, Y: Integer);

(*---------------------------------------------------------
  Wird vom Container aufgerufen, wenn ein DataItem in das Query aufgenommen
  werden soll. mit dem Parameter 'aEnabled' kann bestimmt werden,
  ob dieses DataItem verwendet werden soll oder nicht.
----------------------------------------------------------*)
procedure TMainWindow.GetDataItemGenerateSQL(Sender: TObject; aDataItem: TBaseDataItem; var aEnabled: boolean);
begin
  // 'A1' soll nicht verwendet werden. Alle anderen Items werden verwendet
  aEnabled := (aDataItem.ItemName <> 'A1');
  if not(aEnabled) then
    showMessage(Format('Item %s ist nicht aktiviert',[aDataItem.ItemName]));
end;// procedure TMainWindow.GetDataItemGenerateSQL(Sender: TObject; aDataItem: TBaseDataItem; var aEnabled: boolean);

(*---------------------------------------------------------
  Erzeugt einen neuen Container und f�llt ihn mit allen ausgew�hlten DataItems.
  Ausserdem werden dem neuen Container die 'SQLParameters' �bergeben.
  Die Parameters werden beim Zuweisen kopiert.
----------------------------------------------------------*)
procedure TMainWindow.FillContainer;
var
  i: integer;
  xDataItemIndex: integer;
  xDataItem: TBaseDataItem;
begin
  // Zuerst einen allf�lligen alten Container freigeben
  if assigned(lClassDefContainer) then
    FreeAndNil(lClassDefContainer);

  {$ifdef LABREPORT}
    // Neuen Container erzeugen
    lClassDefContainer := TLabDataItemList.Create;
  {$else}
    // Neuen Container erzeugen
    lClassDefContainer := TClassDefContainer.Create;
  {$endif}

  (* Wird vom Container aufgerufen, wenn ein DataItem in das Query aufgenommen
  werden soll. mit dem Parameter 'aEnabled' kann bestimmt werden,
  ob dieses DataItem verwendet werden soll oder nicht. *)
  lClassDefContainer.OnGetDataItemGenerateSQL := GetDataItemGenerateSQL;

  // Alle DataItems anhand ihres Namens in den Container aufnehmen
  for i:=0 to lbSelected.Items.Count - 1 do begin
    (* Das Item wird aus dem aktuellen Container in den neuen Container kopiert.
       Es wird immer eine Kopie des Items erstellt.
       mit dem dritten Parameter kann bestimmt werden mit welchem Dekorator
       das DataItem erstellt wird.
       xDataItemIndex wird hier ben�tigt, da es sein kann, dass die Methode 'Add()' kein
       passendes Objekt (TClassDef) mit dem entsprechenden Namen findet. Dies ist
       dann der Fall, wenn zB. versucht wird das Item '%EffP' hinzuzuf�gen. In
       diesem Fall liefert die Methode einen Index von -1 zur�ck *)
(*
    xDataItemIndex := lClassDefContainer.AddCopy(lbSelected.Items[i], mClassDefContainer);
*)
    xDataItem := mClassDefContainer.DataItems[mClassDefContainer.IndexOfItemName(lbSelected.Items[i])];
    TTestDataItem(xDataItem).Test := i;
    xDataItemIndex := lClassDefContainer.AddCopy(xDataItem);

    // So denn das Item gefunden und hinzugef�gt wurde
    if xDataItemIndex >= 0 then
      // Cut / Uncut zuweisen
      {$ifdef Classdefects}
      // Wenn "Classdefects" definiert ist muss die unit aClassDef eingebunden werden
        case lbSelected.State[i] of
          cbUnchecked : lClassDefContainer.DataItems[xDataItemIndex].ClassDefects := cdUncut;
          cbChecked   : lClassDefContainer.DataItems[xDataItemIndex].ClassDefects := cdCut;
          cbGrayed    : lClassDefContainer.DataItems[xDataItemIndex].ClassDefects := cdBoth;
        end;// case lbSelected.State[i] of
      {$else}
        lClassDefContainer.DataItems[xDataItemIndex].Cut := lbSelected.Checked[i];
      {$endif}
  end;// for i:=0 to lbSelected.Items.Count - 1 do begin

  // QueryParameters abf�llen
  with mQueryParameters do begin
    MachIDs    := edMachID.Text;
    ProdIDs    := edProdID.Text;
    ShiftCalID := 1;
    StyleIDs   := edStyleID.Text;
    try
      if edFrom.Text > '' then
        TimeFrom := StrToDateTime(edFrom.Text);
    except
    end;
    try
      if edTo.Text > '' then
        TimeTo := StrToDateTime(edTo.Text);
    except
    end;
    TimeMode   := TTimeMode(cbTimeMode.ItemIndex);
    YMSetIDs   := edYMSetID.Text;
  end;// with mQueryParameters do begin

  // Query Parameters zuweisen (Es wird eine Kopie erstellt)
  lClassDefContainer.QueryParameters := mQueryParameters;
end;// procedure TMainWindow.FillContainer;

(*---------------------------------------------------------
  Erzeugt die tempor�re Tabelle und zeigt diese an
----------------------------------------------------------*)
procedure TMainWindow.bCreateTempTableClick(Sender: TObject);
begin
  try
    Screen.Cursor := crSQLWait;
    // Erzeugt einen neuen Container mit den ausgew�hlten Items
    FillContainer;
    lClassDefContainer.CreateTempTable;
    dseQuery.Close;
    dseQuery.CommandText := 'SELECT * FROM ' + lClassDefContainer.TempTableName;
    dseQuery.Open;
  finally
    Screen.Cursor := crDefault;
  end;// try finally
end;// procedure TMainWindow.bCreateTempTableClick(Sender: TObject);

(*---------------------------------------------------------
  L�scht alle Eintr�ge in der Listbox
----------------------------------------------------------*)
procedure TMainWindow.bListBoxClearClick(Sender: TObject);
begin
  lbSelected.Clear;
end;// procedure TMainWindow.bListBoxClearClick(Sender: TObject);

(*---------------------------------------------------------
  F�hrt eine Abfrage f�r die selektierten DataItems aus
----------------------------------------------------------*)
procedure TMainWindow.bGetFieldsClick(Sender: TObject);
var
  i: integer;
begin
  try
    Screen.Cursor := crSQLWait;
    // Container mit den selektierten Items "bef�llen"
    FillContainer;
    // Abfrage ausf�hren
    QueryContainer;
    for i:= 0 to lClassDefContainer.DataItemCount - 1 do
      CodeSite.SendInteger('Item' + IntToStr(i), TTestDataItem(lClassDefContainer.DataItems[i]).test);
  finally
    Screen.Cursor := crDefault;
  end;// try finally
end;// procedure TMainWindow.bGetFieldsClick(Sender: TObject);

(*---------------------------------------------------------
  Ende des Programms
----------------------------------------------------------*)
procedure TMainWindow.FormDestroy(Sender: TObject);
begin
  // Aufr�umen
  FreeAndNil(lClassDefContainer);
  FreeAndNil(mClassDefContainer);
  FreeAndNil(mQueryParameters);
end;// procedure TMainWindow.FormDestroy(Sender: TObject);

(*---------------------------------------------------------
  gibt den Konfigurationsstring f�r die Items aus der Listbox zur�ck
----------------------------------------------------------*)
procedure TMainWindow.bGetConfigStringClick(Sender: TObject);
var
  i: integer;
begin
  lLastSelectionConfigString := '';

  FillContainer;
  for i:=0 to lClassDefContainer.DataItemCount - 1 do
    lLastSelectionConfigString := lLastSelectionConfigString + lClassDefContainer.DataItems[i].ConfigString + cCrlf;
  CodeSite.SendString('Konfigurations String', cCrlf + lLastSelectionConfigString);
end;// procedure TMainWindow.bGetConfigStringClick(Sender: TObject);

(*---------------------------------------------------------
  Setzt den Konfigurations String und erzeugt die ben�tigten Items
  im Container.
----------------------------------------------------------*)
procedure TMainWindow.bSetConfigStringClick(Sender: TObject);
var
  i: integer;
begin
  try
    Screen.Cursor := crSQLWait;
    // Zuerst den Container f�llen ...
    FillContainer;
    (* ... und dann wieder leeren. Damit ist sichergestellet,
       dass lClassDefContainer existiert und dass die SQLParameters
       korrekt abgef�llt ist. *)
    lClassDefContainer.Clear;

    with TStringList.Create do try
      // Die einzelnen Strings auftzeilen --> Ergiebt f�r jedes DataItem ein Eintrag
      Text := lLastSelectionConfigString;
      for i:=0 to Count - 1 do
        // DataItem erzeugen und im Container ablegen
        lClassDefContainer.AddClassFromConfigString(Strings[i], mClassDefContainer);
    finally
      free;
    end;// with TStringList do try

    // Query ausf�hren
    QueryContainer;
  finally
    Screen.Cursor := crDefault;
  end;// try finally
end;// procedure TMainWindow.mmButton1Click(Sender: TObject);

(*---------------------------------------------------------
  F�hrt das Query f�r die Elemente im Container aus
----------------------------------------------------------*)
procedure TMainWindow.QueryContainer;
var
  xViewName: string;
begin
  dseQuery.Close;
  with lClassDefContainer do begin
    // legt den Alias fest mit dem die Schl�sselfelder verkn�pft werden
    LinkAlias := 'v';
    // Je nach Zugriffsmodus die View festlegen die verwendet wird
    case cbTimeMode.ItemIndex of
      0: xViewName := 'v_production_interval';
      1: xViewName := 'v_production_shift';
      2: xViewName := 'v_production_week';
    else
      raise
        Exception.CreateFmt('TMainWindow.bGetFieldsClick: TimeMode nicht implementiert %d',[cbTimeMode.ItemIndex]);
    end;// case cbTimeMode.ItemIndex of

    // Bereitet den Zugriff auf die Teile des Query's vor
    PrepareTable;

    // Query zusammenstellen
    dseQuery.CommandText := 'SELECT ' + GetSQLSelectFields + ', SUM(v.c_wei) as Gewicht' +
                            ' FROM ' + GetSQLFromTables {$ifndef LABREPORT} + ', ' + xViewName + ' v'{$endif} +
                            ' WHERE 1=1 ' + GetSQLWhere + ' AND v.c_machine_id in(' + edMachId.Text + ')';
  end;// with lClassDefContainer do begin

  CodeSite.SendString('Query', dseQuery.CommandText);
  // Query ausf�hren
  dseQuery.Open;
end;// procedure TMainWindow.QueryContainer;

(*---------------------------------------------------------
  Teilt einzelnen DataItems ein neues Icon zu
----------------------------------------------------------*)
procedure TMainWindow.mTreeGetImage(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex;
  var Ghosted: Boolean; var ImageIndex: Integer);
var
  xDataItem: TBaseDataItem;
begin
  (* Anhand des Nodes der anfragt, kann das betroffene DataItem
     abgefragt werden *)
  xDataItem := mTree.GetNodeDataItem(Node);
  if assigned(xDataItem) then begin

    (* Es kann nicht mit dem Operator 'is' gearbeitet werden, da
       'DataItemType' eine Klassenreferenz ist *)
    if xDataItem.DataItemType.InheritsFrom(TDataItem) then
      // Es soll nur in der Namensspalte ein Bild angezeigt werden
      if Column = cNameCol then
        ImageIndex := 9;

(*    // siehe oben
    if xDataItem.DataItemType.InheritsFrom(TClassDef) then
      if (Column = cNameCol) and (not(xDataItem.Predefined)) then
        ImageIndex := 2;
*)
  end;// if assigned(xDataItem) then begin
end;// procedure TMainWindow.mTreeGetImage(...);

procedure TMainWindow.IndexOfClick(Sender: TObject);
begin
  showMessage(IntToStr(mTree.ClassDefList.IndexOfItemName(lbSelected.Items[lbSelected.ItemIndex])));
end;

procedure TMainWindow.bExpandDefaultClick(Sender: TObject);
begin
//  TClassTypeGroup = (ctNone, ctDataItem, ctLongThickThin, ctSplice, ctFF, ctImperfektionen);
  mTree.ExpandedGroups := [ctCustomDataItem, ctDataItem];
  laExpanded.Caption := '[ctNone, ctDataItem]';
end;

procedure TMainWindow.mmButton1Click(Sender: TObject);
begin
  mTree.ExpandedGroups := [ctDataItem, ctLabData];
  laExpanded.Caption := '[ctDataItem, ctImperfektionen]';
end;

end.


