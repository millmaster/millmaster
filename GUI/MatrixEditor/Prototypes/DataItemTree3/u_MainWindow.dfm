object MainWindow: TMainWindow
  Left = 135
  Top = 62
  Width = 999
  Height = 590
  Caption = 'MainWindow'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Shell Dlg 2'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object mmSplitter1: TmmSplitter
    Left = 253
    Top = 105
    Width = 3
    Height = 458
    Cursor = crHSplit
    Align = alRight
  end
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 991
    Height = 105
    Align = alTop
    TabOrder = 0
    object laExpanded: TmmLabel
      Left = 496
      Top = 48
      Width = 3
      Height = 13
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmGroupBox1: TmmGroupBox
      Left = 1
      Top = 1
      Width = 321
      Height = 103
      Align = alLeft
      Caption = 'Anzeigeoptionen'
      TabOrder = 0
      object mmStaticText1: TmmLabel
        Left = 258
        Top = 17
        Width = 23
        Height = 13
        Caption = 'Fett'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmStaticText2: TmmLabel
        Left = 107
        Top = 65
        Width = 29
        Height = 13
        Caption = 'Kursiv'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsItalic]
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmCheckBox1: TmmCheckBox
        Tag = 1
        Left = 8
        Top = 16
        Width = 161
        Height = 17
        Caption = 'jede zweite Zeile einfärben'
        TabOrder = 0
        Visible = True
        OnClick = mmCheckBox1Click
        AutoLabel.LabelPosition = lpLeft
      end
      object mmCheckBox2: TmmCheckBox
        Tag = 2
        Left = 8
        Top = 40
        Width = 97
        Height = 17
        Caption = 'Icons darstellen'
        Checked = True
        State = cbChecked
        TabOrder = 1
        Visible = True
        OnClick = mmCheckBox1Click
        AutoLabel.LabelPosition = lpLeft
      end
      object mmCheckBox3: TmmCheckBox
        Tag = 3
        Left = 8
        Top = 64
        Width = 97
        Height = 17
        Caption = 'Gruppen Namen'
        Checked = True
        State = cbChecked
        TabOrder = 2
        Visible = True
        OnClick = mmCheckBox1Click
        AutoLabel.LabelPosition = lpLeft
      end
      object mmCheckBox4: TmmCheckBox
        Tag = 4
        Left = 184
        Top = 16
        Width = 73
        Height = 17
        Caption = 'Data Items'
        Checked = True
        State = cbChecked
        TabOrder = 3
        Visible = True
        OnClick = mmCheckBox1Click
        AutoLabel.LabelPosition = lpLeft
      end
      object mmCheckBox5: TmmCheckBox
        Tag = 5
        Left = 184
        Top = 40
        Width = 129
        Height = 17
        Caption = 'Gitter Linien anzeigen'
        TabOrder = 4
        Visible = True
        OnClick = mmCheckBox1Click
        AutoLabel.LabelPosition = lpLeft
      end
      object mmCheckBox8: TmmCheckBox
        Tag = 9
        Left = 184
        Top = 64
        Width = 129
        Height = 17
        Caption = 'Beschreibung anzeigen'
        Checked = True
        State = cbChecked
        TabOrder = 5
        Visible = True
        OnClick = mmCheckBox1Click
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object mmGroupBox2: TmmGroupBox
      Left = 322
      Top = 1
      Width = 161
      Height = 103
      Align = alLeft
      Caption = 'Tool Butons'
      TabOrder = 1
      object bToolButtons: TmmCheckBox
        Tag = 6
        Left = 16
        Top = 14
        Width = 97
        Height = 18
        Caption = 'Tool Buttons'
        TabOrder = 0
        Visible = True
        OnClick = mmCheckBox1Click
        AutoLabel.LabelPosition = lpLeft
      end
      object mmCheckBox6: TmmCheckBox
        Tag = 7
        Left = 16
        Top = 33
        Width = 137
        Height = 17
        Caption = 'DropDown Button'
        TabOrder = 1
        Visible = True
        OnClick = mmCheckBox1Click
        AutoLabel.LabelPosition = lpLeft
      end
      object mmCheckBox7: TmmCheckBox
        Tag = 8
        Left = 16
        Top = 70
        Width = 137
        Height = 17
        Caption = 'nur ausgewählter Zweig'
        TabOrder = 2
        Visible = True
        OnClick = mmCheckBox1Click
        AutoLabel.LabelPosition = lpLeft
      end
      object mmCheckBox9: TmmCheckBox
        Tag = 10
        Left = 16
        Top = 51
        Width = 97
        Height = 17
        Caption = 'Tab Control'
        TabOrder = 3
        Visible = True
        OnClick = mmCheckBox1Click
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object bExpandDefault: TmmButton
      Left = 496
      Top = 16
      Width = 129
      Height = 25
      Caption = 'Expand 1'
      TabOrder = 2
      Visible = True
      OnClick = bExpandDefaultClick
      AutoLabel.LabelPosition = lpLeft
    end
    object mmButton1: TmmButton
      Left = 640
      Top = 16
      Width = 129
      Height = 25
      Caption = 'Expand 2'
      TabOrder = 3
      Visible = True
      OnClick = mmButton1Click
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmPanel2: TmmPanel
    Left = 256
    Top = 105
    Width = 735
    Height = 458
    Align = alRight
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'mmPanel2'
    TabOrder = 1
    object mmDBGrid1: TmmDBGrid
      Left = 1
      Top = 160
      Width = 733
      Height = 297
      Align = alBottom
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = mmDataSource1
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Shell Dlg 2'
      TitleFont.Style = []
    end
    object mmPanel3: TmmPanel
      Left = 1
      Top = 1
      Width = 226
      Height = 159
      Align = alLeft
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 1
      object mmLabel1: TmmLabel
        Left = 2
        Top = 2
        Width = 222
        Height = 13
        Align = alTop
        Caption = 'Drag && Drop (Checked = cut, gray = both)'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmPanel4: TmmPanel
        Left = 2
        Top = 134
        Width = 222
        Height = 23
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvNone
        Caption = 'Clear'
        TabOrder = 1
        OnClick = bListBoxClearClick
      end
      object lbSelected: TmmCheckListBox
        Left = 2
        Top = 15
        Width = 222
        Height = 119
        Align = alTop
        AllowGrayed = True
        ItemHeight = 13
        TabOrder = 0
        Visible = True
        OnDragDrop = lbSelectedDragDrop
        OnDragOver = lbSelectedDragOver
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object mmPanel5: TmmPanel
      Left = 429
      Top = 1
      Width = 305
      Height = 159
      Align = alRight
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 2
      object mmLabel2: TmmLabel
        Left = 176
        Top = 11
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'ProdID'#39's'
        FocusControl = edProdID
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel3: TmmLabel
        Left = 11
        Top = 35
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Style ID'#39's'
        FocusControl = edStyleID
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel4: TmmLabel
        Left = 162
        Top = 35
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'YM Set ID'#39's'
        FocusControl = EdYMSetID
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel5: TmmLabel
        Left = 181
        Top = 59
        Width = 18
        Height = 13
        Alignment = taRightJustify
        Caption = 'Von'
        FocusControl = edFrom
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel6: TmmLabel
        Left = 10
        Top = 59
        Width = 13
        Height = 13
        Alignment = taRightJustify
        Caption = 'bis'
        FocusControl = edTo
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel7: TmmLabel
        Left = 10
        Top = 11
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Mach ID'#39's'
        FocusControl = edMachId
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel8: TmmLabel
        Left = 11
        Top = 92
        Width = 51
        Height = 13
        Caption = 'Time Mode'
        FocusControl = cbTimeMode
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object bCreateTempTable: TmmBitBtn
        Left = 8
        Top = 127
        Width = 289
        Height = 25
        Caption = 'Temp. Tabelle erzeugen'
        TabOrder = 0
        Visible = True
        OnClick = bCreateTempTableClick
        AutoLabel.LabelPosition = lpLeft
      end
      object edMachId: TmmEdit
        Left = 58
        Top = 7
        Width = 80
        Height = 21
        Color = clWindow
        TabOrder = 1
        Text = '5, 21'
        Visible = True
        AutoLabel.Control = mmLabel7
        AutoLabel.LabelPosition = lpLeft
        Decimals = 0
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object edProdID: TmmEdit
        Left = 218
        Top = 7
        Width = 80
        Height = 21
        Color = clWindow
        TabOrder = 2
        Text = '-2147482021'
        Visible = True
        AutoLabel.Control = mmLabel2
        AutoLabel.LabelPosition = lpLeft
        Decimals = 0
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object edStyleID: TmmEdit
        Left = 58
        Top = 31
        Width = 80
        Height = 21
        Color = clWindow
        TabOrder = 3
        Text = '1'
        Visible = True
        AutoLabel.Control = mmLabel3
        AutoLabel.LabelPosition = lpLeft
        Decimals = 0
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object EdYMSetID: TmmEdit
        Left = 218
        Top = 31
        Width = 80
        Height = 21
        Color = clWindow
        TabOrder = 4
        Text = '403,415,419'
        Visible = True
        AutoLabel.Control = mmLabel4
        AutoLabel.LabelPosition = lpLeft
        Decimals = 0
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object edTo: TmmEdit
        Left = 25
        Top = 55
        Width = 113
        Height = 21
        Color = clWindow
        TabOrder = 5
        Text = '19.12.2002 9:00:00'
        Visible = True
        AutoLabel.Control = mmLabel6
        AutoLabel.LabelPosition = lpLeft
        Decimals = 0
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object edFrom: TmmEdit
        Left = 201
        Top = 55
        Width = 97
        Height = 21
        Color = clWindow
        TabOrder = 6
        Text = '18.12.2002 8:00:00'
        Visible = True
        AutoLabel.Control = mmLabel5
        AutoLabel.LabelPosition = lpLeft
        Decimals = 0
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object cbTimeMode: TmmComboBox
        Left = 64
        Top = 88
        Width = 105
        Height = 21
        Color = clWindow
        ItemHeight = 13
        TabOrder = 7
        Visible = True
        Items.Strings = (
          'tmInterval'
          'tmShift'
          'tmLongTerm')
        AutoLabel.Control = mmLabel8
        AutoLabel.LabelPosition = lpLeft
        Edit = True
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
    end
    object bGetFields: TmmButton
      Left = 232
      Top = 8
      Width = 161
      Height = 25
      Caption = 'GetFields (CodeSite)'
      TabOrder = 3
      Visible = True
      OnClick = bGetFieldsClick
      AutoLabel.LabelPosition = lpLeft
    end
    object bGetConfigString: TmmButton
      Left = 232
      Top = 40
      Width = 161
      Height = 25
      Caption = 'GetConfigString (CodeSite)'
      TabOrder = 4
      Visible = True
      OnClick = bGetConfigStringClick
      AutoLabel.LabelPosition = lpLeft
    end
    object bSetConfigString: TmmButton
      Left = 232
      Top = 72
      Width = 161
      Height = 25
      Caption = 'SetConfigString'
      TabOrder = 5
      Visible = True
      OnClick = bSetConfigStringClick
      AutoLabel.LabelPosition = lpLeft
    end
    object IndexOf: TmmBitBtn
      Left = 232
      Top = 120
      Width = 161
      Height = 25
      Caption = 'IndexOf'
      TabOrder = 6
      Visible = True
      OnClick = IndexOfClick
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mTree: TDataItemTree
    Left = 0
    Top = 105
    Width = 253
    Height = 458
    Align = alClient
    DragMode = dmAutomatic
    TabOrder = 2
    Images = mAdditionalTreeImages
    LineColor = 15596027
    OnFocusChanged = DoFocusChanged
    OnFocusChanging = DoFocusChanging
    OnGetImage = mTreeGetImage
    OnSetAdditionalDataItems = AddStandardItems
  end
  object mmDataSource1: TmmDataSource
    DataSet = dseQuery
    Left = 336
    Top = 385
  end
  object conDefault: TmmADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Data Source=WETPC1148;Initial Catalog=MM_Win' +
      'ding;Password=netpmek32;User ID=MMSystemSQL;Auto Translate=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 272
    Top = 384
  end
  object dseQuery: TmmADODataSet
    Connection = conDefault
    Parameters = <>
    Left = 304
    Top = 384
  end
  object mAdditionalTreeImages: TmmImageList
    Left = 344
    Top = 329
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000084000000840000008400000084000000840000008400000084000000
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000084000000840000008400000084000000840000008400000084000000
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000848400008484
      000084840000848400008484000084840000FF000000FF000000FF000000FF00
      0000FF000000FF00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF00000000000000
      0000000000000000000000000000000000000000000084840000C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600FFFF0000FFFF0000FFFF
      0000FFFF000084840000FF000000FF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      00000000000000000000000000000000000084840000C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600FFFF0000FFFF0000FFFF
      0000FFFF00008484000084840000FF000000000000000000000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF0000FFFF0000000000000000000000
      00000000000000000000000000000000000084840000C6C6C600848400008484
      000084840000848400008484000084840000FF000000FF000000FF000000FF00
      0000FF000000FF00000084840000FF000000000000000000000000000000FFFF
      000000000000000000000000000000000000FFFF0000FFFF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000008484000084840000C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600FFFF0000FFFF0000FFFF
      00008484000084840000FF000000FF000000000000000000000000000000FFFF
      0000000000000000000000000000FFFF0000FFFF000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF00FFFFFF0000FFFF000000
      0000000000000000000000000000000000000000000084840000C6C6C6008484
      000084840000848400008484000084840000FF000000FF000000FF000000FF00
      0000FF00000084840000FF00000000000000000000000000000000000000FFFF
      00000000000000000000FFFF0000FFFF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000FFFF0000FFFF0000FFFF00FFFFFF0000FFFF0000FFFF0000FF
      FF0000000000000000000000000000000000000000008484000084840000C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600FFFF0000FFFF0000FFFF00008484
      000084840000FF000000FF00000000000000000000000000000000000000FFFF
      000000000000FFFF0000FFFF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400848484008484
      840000000000FFFFFF0000FFFF00FFFFFF0000FFFF0000000000000000000000
      000000000000000000000000000000000000000000000000000084840000C6C6
      C60084840000848400008484000084840000FF000000FF000000FF000000FF00
      000084840000FF0000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF0000000000000000000000000000000000000000000000FF
      FF00848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0084840000848400008484
      00000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000848400008484
      0000C6C6C600C6C6C600C6C6C600C6C6C600FFFF0000FFFF0000848400008484
      0000FF000000FF0000000000000000000000000000000000000000000000FFFF
      0000FFFF000000000000000000000000000000000000000000000000000000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000008400000084
      0000008400000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000008484
      0000C6C6C600C6C6C600C6C6C600FFFF0000FFFF000084840000848400008484
      0000FF000000000000000000000000000000000000000000000000000000FFFF
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF000000000000FF00000084
      00000084000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000008484
      0000848400008484000084840000FF000000FF000000FF000000FF000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000000000000FFFFFF0000FFFF00FFFFFF00FFFFFF0000FF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000008400000084000000840000008400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000840000008400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000008400000084000000840000008400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400848484008484
      8400848484008484840084848400848484008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FCFFF00F80000000FE7FF00FBFFE0000
      FE3FC003AAAA0000F81F800080000000F80F0000C00F0000FC070000C01F0000
      FC1F0000C41F0000E00F8001C04F000000078001C0C700000003C003C1C30000
      001FC003C3CB0000000FE007C7E100000007E007CFE100000003FC3FDFF00000
      0001FC3FFFF10000007FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
end
