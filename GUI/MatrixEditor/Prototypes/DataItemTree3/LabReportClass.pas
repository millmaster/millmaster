(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: Data.pas
| Projectpart...: Datamodul fuer LabReport
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows 2000 SP2
| Target.system.: Windows NT/2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
| 05.06.2002 1.00 Lok | File created
| 18.07.2002 1.01 Nue | Name TParameters changed to TQueryParameters
| 26.11.2002 1.01 Wss | Size of TStringField set to 70 to have enough space
| April 2003 1.01 Wss | Klassen in eine Unit zusammengefasst
|=============================================================================*)
unit LabReportClass;



interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, mmADODataSet, mmADOConnection, IniFiles,
  Series, Chart, TeEngine,
  mmADOQuery, mmDataSource, mmStringList, mmDBGrid, mmSeries, mmChart,
  MMUGlobal, LoepfeGlobal,
  mmADOCommand, ClassDef, LabMasterDef;

type
  TdmLabReport = class;
  TLabDataItem = class (TDataItemDecorator)
  private
    fAlignment: TAlignment;
    fAverageStep: Integer;
    fAxisType: TAxisType;
    fCalcMode: Integer;
    fChartIndex: Integer;
    fChartType: TChartType;
    fCol: string;
    fColor: TColor;
    fCommas: Integer;
    fDataLevel: Integer;
    fDataMode: TDataMode;
    fDataType: TFieldType;
    fDescendingSort: Boolean;
    fDisplayWidth: Integer;
    fKeyField: Boolean;
    fKeyFieldName: string;
    fLowerLimit: Single;
    fLowerLimitMode: TBorderMode;
    fMassField: TMassField;
    fRightAxis: Boolean;
    fShowAverage: Boolean;
    fShowInChart: Boolean;
    fShowInTable: Boolean;
    fShowSeconds: Boolean;
    fStatistic: TStatisticSet;
    fTableIndex: Integer;
    fThousands: Boolean;
    fUpperLimit: Single;
    fUpperLimitMode: TBorderMode;
    fUseMode: TUseMode;
    fYScalingFactor: Integer;
    function GetCalcName: string;
    function GetColName: string;
    function GetMassFieldName: string;
  protected
    function GetConfigString: string; override;
    procedure SetConfigString(Value: String); override;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function GetSQLFromTable: string; overload; override;
    function GetSQLSelectFields(aWithSum: boolean = true; aWithAlias: boolean = true): string; override;
    function GetSQLWhere: string; override;
    property Alignment: TAlignment read fAlignment write fAlignment;
    property AverageStep: Integer read fAverageStep write fAverageStep;
    property AxisType: TAxisType read fAxisType write fAxisType;
    property CalcMode: Integer read fCalcMode write fCalcMode;
    property CalcName: string read GetCalcName;
    property ChartIndex: Integer read fChartIndex write fChartIndex;
    property ChartType: TChartType read fChartType write fChartType;
    property Col: string read fCol write fCol;
    property ColName: string read GetColName;
    property Color: TColor read fColor write fColor;
    property Commas: Integer read fCommas write fCommas;
    property DataLevel: Integer read fDataLevel write fDataLevel;
    property DataMode: TDataMode read fDataMode write fDataMode;
    property DataType: TFieldType read fDataType write fDataType;
    property DescendingSort: Boolean read fDescendingSort write fDescendingSort;
    property DisplayWidth: Integer read fDisplayWidth write fDisplayWidth;
    property KeyField: Boolean read fKeyField write fKeyField;
    property KeyFieldName: string read fKeyFieldName write fKeyFieldName;
    property LowerLimit: Single read fLowerLimit write fLowerLimit;
    property LowerLimitMode: TBorderMode read fLowerLimitMode write fLowerLimitMode;
    property MassField: TMassField read fMassField write fMassField;
    property MassFieldName: string read GetMassFieldName;
    property RightAxis: Boolean read fRightAxis write fRightAxis;
    property ShowAverage: Boolean read fShowAverage write fShowAverage;
    property ShowInChart: Boolean read fShowInChart write fShowInChart;
    property ShowInTable: Boolean read fShowInTable write fShowInTable;
    property ShowSeconds: Boolean read fShowSeconds write fShowSeconds;
    property Statistic: TStatisticSet read fStatistic write fStatistic;
    property TableIndex: Integer read fTableIndex write fTableIndex;
    property Thousands: Boolean read fThousands write fThousands;
    property UpperLimit: Single read fUpperLimit write fUpperLimit;
    property UpperLimitMode: TBorderMode read fUpperLimitMode write fUpperLimitMode;
    property UseMode: TUseMode read fUseMode write fUseMode;
    property YScalingFactor: Integer read fYScalingFactor write fYScalingFactor;
  end;
  
  TLabDataItemList = class (TClassDefContainer)
  private
    fChartOptions: TChartOptions;
    fDataLevel: Integer;
    fDataMode: TDataMode;
    fGridOptions: TGridOptions;
    fMatrixOptions: TMatrixOptions;
    fReportOptions: TReportOptions;
    fXAxis: string;
    fZAxis: string;
    mDataMode: TDataMode;
    function GetChartOptions: PChartOptions;
    function GetGridOptions: PGridOptions;
    function GetItems(aIndex: Integer): TLabDataItem;
    function GetMatrixOptions: PMatrixOptions;
    function GetReportOptions: PReportOptions;
    function GetXAxisItem: TLabDataItem;
    function GetZAxisItem: TLabDataItem;
    procedure SetDataLevel(Value: Integer);
    procedure SetDataMode(Value: TDataMode);
    procedure SetXAxis(const Value: string);
    procedure SetZAxis(const Value: string);
  public
    LevelInfo: TmmStringList;
    constructor Create(aPersistent: boolean = false); override;
    destructor Destroy; override;
    procedure Assign(aSource: TClassDefList); override;
    procedure Clear; override;
    function GetKeyFieldForLevel(aLevel: Integer): string;
    function GetKeyItemForLevel(aLevel: Integer): TLabDataItem;
    function GetSQLFromTables: string; override;
    function GetSQLSelectFields(aWithSum: boolean = true; aWithAlias: boolean = true): string; reintroduce; override;
    function GetSQLWhere: string; override;
    function HasLeftTop: Boolean;
    function HasRightTop: Boolean;
    procedure InitDataItems(aMode: Integer);
    function LevelCount: Integer;
    procedure ReadFromConfig(aIniFile: TCustomIniFile);
    procedure WriteToConfig(aIniFile: TCustomIniFile);
    property ChartOptions: PChartOptions read GetChartOptions;
    property DataLevel: Integer read fDataLevel write SetDataLevel;
    property DataMode: TDataMode read fDataMode write SetDataMode;
    property GridOptions: PGridOptions read GetGridOptions;
    property Items[aIndex: Integer]: TLabDataItem read GetItems;
    property MatrixOptions: PMatrixOptions read GetMatrixOptions;
    property ReportOptions: PReportOptions read GetReportOptions;
    property XAxis: string read fXAxis write SetXAxis;
    property XAxisItem: TLabDataItem read GetXAxisItem;
    property ZAxis: string read fZAxis write SetZAxis;
    property ZAxisItem: TLabDataItem read GetZAxisItem;
  end;
  
  TDetailInfoHelper = class (TObject)
  private
    mDataItemList: TLabDataItemList;
    mDM: TdmLabReport;
    mInfoList: TIniStringList;
    mMaxLabelWidth: Integer;
    mSeries: TChartSeries;
  public
    constructor Create(aContainer: TLabDataItemList; aDataModul: TdmLabReport);
    destructor Destroy; override;
    function GetDetailInfoString(aClickedSeries: TChartSeries; X, Y, aValueIndex: Integer): string;
  end;
  
  TdmLabReport = class (TDataModule)
    conDefault: TmmADOConnection;
    dseChart: TmmADODataSet;
    dseGrid: TmmADODataSet;
    dseGridDetail: TmmADODataSet;
    dseWork: TmmADODataSet;
    procedure DataSetCalcFields(DataSet: TDataSet);
    procedure dseBeforeOpen(DataSet: TDataSet);
  private
    fDBParams: TQueryParameters;
    fDefaultShiftCal: Integer;
    fLenBase: TLenBase;
    fResultQuery: TmmADODataSet;
    mDataItemList: TLabDataItemList;
    mFrom: TStringList;
    mSelect: TStringList;
    mWhere: TStringList;
    function GetWhereStr(aAddWhere: String; aWithContainerWhere: Boolean): string;
  protected
    procedure DoGetDataItemGenerateSQL(Sender: TObject; aDataItem: TBaseDataItem; var aEnabled: boolean);
  public
    constructor Create(aOwner: TComponent; aDataItemList: TLabDataItemList); reintroduce;
    destructor Destroy; override;
    function CreateAllQueryFields(aDataMode: TDataMode; aLevel: Integer = 0; aAddWhere: String = ''): Boolean;
    function GetAxisNamesForChart(aFieldName: String; var aStrList: TStringList): Boolean;
    property DBParams: TQueryParameters read fDBParams;
    property DefaultShiftCal: Integer read fDefaultShiftCal write fDefaultShiftCal;
    property LenBase: TLenBase read fLenBase write fLenBase;
    property ResultQuery: TmmADODataSet read fResultQuery;
  end;
  

const
  cLabItemClass: TDataItemDecoratorClass =  TLabDataItem;
//------------------------------------------------------------------------------
var
  gDataItemPool: TLabDataItemList;

implementation
{$R *.DFM}
uses
  mmMBCS, mmCS, IvDictio;

//------------------------------------------------------------------------------
procedure ConvertIntToSet(aSrc: Integer; aDest: PByte; aSize: Integer);
begin
  System.Move(aSrc, aDest^, aSize);
end;
//------------------------------------------------------------------------------
function ConvertSetToInt(aPByte: PByte; aSize: Integer): DWord;
begin
  Result := 0;
  System.Move(aPByte^, Result, aSize);
end;
//------------------------------------------------------------------------------
function RectToStr(aRect: TRect): String;
begin
  with aRect do
    Result := Format('%d,%d,%d,%d', [Left, Top, Right, Bottom]);
end;
//------------------------------------------------------------------------------
function StrToRect(aRect: String): TRect;
begin
  with TStringList.Create do
  try
    CommaText := aRect;
    with Result do begin
      Left   := StrToIntDef(Strings[0], -1);
      Top    := StrToIntDef(Strings[1], -1);
      Right  := StrToIntDef(Strings[2], -1);
      Bottom := StrToIntDef(Strings[3], -1);
    end;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
function KeyStrSortProc(List: TStringList; Index1, Index2: Integer):Integer;
var
  xVal1, xVal2: Integer;
begin
  xVal1 := Integer(List.Objects[Index1]);
  xVal2 := Integer(List.Objects[Index2]);
  if xVal1 > xVal2 then
    Result := 1
  else if xVal1 < xVal2 then
    Result := -1
  else
    Result := 0;
end;

//:---------------------------------------------------------------------------
//:--- Class: TLabDataItem
//:---------------------------------------------------------------------------
constructor TLabDataItem.Create;
begin
  inherited Create;
  //:...................................................................
  // So, noch ein paar Defaultwerte
  fChartType    := ctBar;
  fDataLevel    := -1;
  fDisplayWidth := 64;
  fMassField    := mfLen; // mfLen, weil bei den Superklassen ich sonst alle durchgehen und setzen m�sste
  UseMode := umAlways;
end;

//:---------------------------------------------------------------------------
destructor TLabDataItem.Destroy;
begin
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TLabDataItem.Assign(Source: TPersistent);
var
  xSource: TLabDataItem;
begin
  if Source is TLabDataItem then begin
    xSource := TLabDataItem(Source);
    // Alle Eigenschaften kopieren
    Alignment      := xSource.Alignment;
    AverageStep    := xSource.AverageStep;
    AxisType       := xSource.AxisType;
    CalcMode       := xSource.CalcMode;
    ChartIndex     := xSource.ChartIndex;
    ChartType      := xSource.ChartType;
    Col            := xSource.Col;
    Color          := xSource.Color;
    Commas         := xSource.Commas;
    DataType       := xSource.DataType;
    DescendingSort := xSource.DescendingSort;
    DisplayWidth   := xSource.DisplayWidth;
  //  Field          := xSource.Field;
    KeyField       := xSource.KeyField;
    KeyFieldName   := xSource.KeyFieldName;
    LowerLimit     := xSource.LowerLimit;
    LowerLimitMode := xSource.LowerLimitMode;
    MassField      := xSource.MassField;
    RightAxis      := xSource.RightAxis;
    ShowAverage    := xSource.ShowAverage;
    ShowInChart    := xSource.ShowInChart;
    ShowInTable    := xSource.ShowInTable;
    ShowSeconds    := xSource.ShowSeconds;
    Statistic      := xSource.Statistic;
    TableIndex     := xSource.TableIndex;
    Thousands      := xSource.Thousands;
    UpperLimit     := xSource.UpperLimit;
    UpperLimitMode := xSource.UpperLimitMode;
    UseMode        := xSource.UseMode;
    YScalingFactor := xSource.YScalingFactor;
  //  YScalingType   := xSource.YScalingType;
  end;// if Source is TLabDataItem then begin
  //:...................................................................
  inherited Assign(Source);
end;

//:---------------------------------------------------------------------------
function TLabDataItem.GetCalcName: string;
begin
  Result := cPrefixCalc + FieldName;
end;

//:---------------------------------------------------------------------------
function TLabDataItem.GetColName: string;
begin
  //if ClassTypeGroup = ctDataItem then
  if ClassTypeGroup in [ctDataItem, ctLabData] then
    Result := cPrefixCol + FieldName
  else
    Result := FieldName;
end;

//:---------------------------------------------------------------------------
function TLabDataItem.GetConfigString: string;
begin
  Result := '';
  with TIniStringList.Create do
  try
    Values['Alignment'] := Alignment;
    Values['AverageStep'] := AverageStep;
  //  Values['CalcMode'] := CalcMode;
    Values['ChartIndex'] := ChartIndex;
    Values['ChartType'] := ChartType;
    Values['Col'] := Col;
    Values['Color'] := Color;
    Values['Commas'] := Commas;
  //  Values['DataType'] := DataType;
    Values['DescendingSort'] := DescendingSort;
    Values['DisplayWidth'] := DisplayWidth;
  //  Values['Field'] := Field;
    Values['KeyField'] := KeyField;
    Values['KeyFieldName'] := KeyFieldName;
    Values['LowerLimit'] := LowerLimit;
    Values['LowerLimitMode'] := LowerLimitMode;
    Values['RightAxis'] := RightAxis;
    Values['ShowAverage'] := ShowAverage;
    Values['ShowInChart'] := ShowInChart;
    Values['ShowInTable'] := ShowInTable;
    Values['ShowSeconds'] := ShowSeconds;
  //  Values['Statistic'] := Statistic;
    Values['Statistic'] := ConvertSetToInt(@Statistic, SizeOf(Statistic));
    Values['TableIndex'] := TableIndex;
    Values['Thousands'] := Thousands;
    Values['UpperLimit'] := UpperLimit;
    Values['UpperLimitMode'] := UpperLimitMode;
    Values['YScalingFactor'] := YScalingFactor;
  //  Values['UseMode'] := UseMode;
  //  Values['YScalingType'] := YScalingType;
  
    Result := inherited GetConfigString + ',' + CommaText;
  finally
    Free;
  end;// with TIniStringList do try
end;

//:---------------------------------------------------------------------------
function TLabDataItem.GetMassFieldName: string;
begin
  Result := cMassFields[fMassField];
end;

//:---------------------------------------------------------------------------
function TLabDataItem.GetSQLFromTable: string;
begin
  if ClassTypeGroup = ctDataItem then begin
    Result := '';
  end else
    Result := inherited GetSQLFromTable;
end;

//:---------------------------------------------------------------------------
function TLabDataItem.GetSQLSelectFields(aWithSum: boolean = true; aWithAlias: boolean = true): string;
begin
  if (ClassTypeGroup in [ctDataItem, ctLabData]) then begin
    // Je nachdem was gew�nscht ist, das komplete Statement zusammensetzen
    if UseMode <> umNever then begin
      if aWithSum and not KeyField then begin
        if aWithAlias then
          // zb: SUM(v.c_Len) as "c_Len"
          Result := Format('SUM(%s.%s) as "%s"',[LinkAlias, cPrefixCol+FieldName, cPrefixCol+FieldName])
        else
          // zb: SUM(v.c_Len)
          Result := Format('SUM(%s.%s)',[LinkAlias, cPrefixCol+FieldName]);
      end else if (fDataLevel <> -1) and (TableIndex >= fDataLevel) and (DataMode = dmGrid) then
        // Wenn im Hierarchy Modus dann das Count-Feld f�r das Query angeben
        Result := Format(cSelectCountField, [cPrefixCol+FieldName, cPrefixCol+FieldName])
      else begin
        if aWithSum then
          // zB: (v.c_style_name) as "c_style_name"
          Result := Format('(%s.%s) as "%s"',[LinkAlias, cPrefixCol+FieldName, cPrefixCol+FieldName])
        else
          // zB: (v.c_style_name) as "c_style_name"
          Result := Format('(%s.%s)',[LinkAlias, cPrefixCol+FieldName]);
      end;
    end else
      Result := '';
  end else
    Result := inherited GetSQLSelectFields(aWithSum, aWithAlias);
end;

//:---------------------------------------------------------------------------
function TLabDataItem.GetSQLWhere: string;
begin
  if ClassTypeGroup = ctDataItem then begin
    Result := '';
  end else
    Result := inherited GetSQLWhere;
end;

//:---------------------------------------------------------------------------
procedure TLabDataItem.SetConfigString(Value: String);
begin
  with TIniStringList.Create do
  try
    CommaText      := Value;
    Alignment      := ValueDef('Alignment', 0);
    AverageStep    := ValueDef('AverageStep', 2);
  //  CalcMode       := ValueDef('CalcMode', 0);
    ChartIndex     := ValueDef('ChartIndex', 0);
    ChartType      := ValueDef('ChartType', ctBar);
    Col            := ValueDef('Col', 0);
    Color          := ValueDef('Color', 0);
    Commas         := ValueDef('Commas', 0);
  //  DataType       := ValueDef('DataType', 0);
    DescendingSort := ValueDef('DescendingSort', 0);
    DisplayWidth   := ValueDef('DisplayWidth', 0);
  //  Field := ValueDef('Field', 0);
    KeyField       := ValueDef('KeyField', '');
    KeyFieldName   := ValueDef('KeyFieldName', '');
    LowerLimit     := ValueDef('LowerLimit', 0);
    LowerLimitMode := ValueDef('LowerLimitMode', 0);
    RightAxis      := ValueDef('RightAxis', 0);
    ShowAverage    := ValueDef('ShowAverage', False);
    ShowInChart    := ValueDef('ShowInChart', 0);
    ShowInTable    := ValueDef('ShowInTable', 0);
    ShowSeconds    := ValueDef('ShowSeconds', 0);
  //  Statistic      := ValueDef('Statistic', 0);
    ConvertIntToSet(ValueDef('Statistic', 0), @Statistic, SizeOf(Statistic));
    TableIndex     := ValueDef('TableIndex', 0);
    Thousands      := ValueDef('Thousands', 0);
    UpperLimit     := ValueDef('UpperLimit', 0);
    UpperLimitMode := ValueDef('UpperLimitMode', 0);
    YScalingFactor := ValueDef('YScalingFactor', 0);
  //  UseMode        := ValueDef('UseMode', 0);
  //  YScalingType   := Values['YScalingType'];
  finally
    Free;
  end;// with TIniStringList do try
  
  inherited SetConfigString(Value);
end;

//:---------------------------------------------------------------------------
//:--- Class: TLabDataItemList
//:---------------------------------------------------------------------------
constructor TLabDataItemList.Create(aPersistent: boolean = false);
begin
  inherited Create(aPersistent);
  //:...................................................................
  LevelInfo  := TmmStringList.Create;
  fDataLevel := -1;
  fDataMode  := dmChart; // wird einfach mal auf dmChart initialisiert
  fXAxis     := '';
  fZAxis     := '';
  
  // set default report options
  with fReportOptions do begin
    CompanyName    := Translate('(*)Firmenname');
    ReportTitle    := Translate('(*)Berichttitel');
    ShowExtend     := False;
    UseYMSettings  := False;
    ShowStyle      := True;
    ShowLot        := True;
    ShowMachine    := True;
    ShowYMSettings := True;
    PrintChart     := True;
    PrintQMatrix   := True;
    PrintFFMatrix  := True;
    PrintTable     := False;
    StretchTable   := True;
    NewPageMatrix  := False;
    NewPageYMSettings := False;
    NewPageTable   := False;
    RectChart      := Rect(-1, -1, -1, -1);
    RectQMatrix    := Rect(-1, -1, -1, -1);
    RectFFMatrix   := Rect(-1, -1, -1, -1);
  end;
  // set default settings for table
  with fGridOptions do begin
    Options := [0,1,3];
  
    DataFont.Name     := 'MS Sans Serif'; //mDataGrid.Font.Name;
    DataFont.Color    := clWindowText; //mDataGrid.Font.Color;
    DataFont.Size     := 8; //mDataGrid.Font.Size;
    DataFont.Style    := []; //mDataGrid.Font.Style;
    DataFont.BkColor  := clWindow; //mDataGrid.Color;
  
    TitleFont.Name    := 'MS Sans Serif'; //mDataGrid.TitleFont.Name;
    TitleFont.Color   := clWindowText; //mDataGrid.TitleFont.Color;
    TitleFont.Size    := 8; //mDataGrid.TitleFont.Size;
    TitleFont.Style   := []; //mDataGrid.TitleFont.Style;
    TitleFont.BkColor := clBtnFace; //mDataGrid.FixedColor;
  end;
  
  // set default settings for chart
  with fChartOptions do begin
    AllowRotate         := False;
    BarStyle            := bsSide;
    StartColor          := clWhite;
    EndColor            := clWhite;
    GradientDir         := 1;
    LeftAxis.Autoscale  := True;
    LegendAlign         := laLeft;
    RightAxis.Autoscale := True;
    RotateX             := 350;
    RotateY             := 350;
    View3D              := False;
    Width3D             := 25;
    Zoom                := 90;
  end;
  
  with fMatrixOptions do begin
    ChannelVisible := True;
    ClusterVisible := True;
    SpliceVisible  := True;
    DefectValues   := True;
    CutValues      := True;
    ActiveVisible  := True;
    ActiveColor    := clAqua;
    MatrixMode     := 0; // default Coarse mode
  end;
end;

//:---------------------------------------------------------------------------
destructor TLabDataItemList.Destroy;
begin
  LevelInfo.Free;
  //:...................................................................
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TLabDataItemList.Assign(aSource: TClassDefList);
var
  i: Integer;
  xSource: TLabDataItemList;
begin
  if Assigned(aSource) and (aSource is TLabDataItemList) then begin
    Clear;
    xSource := TLabDataItemList(aSource);
  
    fXAxis         := xSource.XAxis;
    fZAxis         := xSource.ZAxis;
    fChartOptions  := xSource.ChartOptions^;
    fGridOptions   := xSource.GridOptions^;
    fMatrixOptions := xSource.MatrixOptions^;
    fReportOptions := xSource.ReportOptions^;
    LevelInfo.Text := xSource.LevelInfo.Text;
    // Nun noch �berpr�fen, ob auch alle Items mit umAlways vorhanden sind
    InitDataItems(1);
  end;
  inherited Assign(aSource);
end;

//:---------------------------------------------------------------------------
procedure TLabDataItemList.Clear;
begin
  inherited Clear;
  //:...................................................................
  fDataLevel := -1;
end;

//:---------------------------------------------------------------------------
function TLabDataItemList.GetChartOptions: PChartOptions;
begin
  Result := @fChartOptions;
end;

//:---------------------------------------------------------------------------
function TLabDataItemList.GetGridOptions: PGridOptions;
begin
  Result := @fGridOptions;
end;

//:---------------------------------------------------------------------------
function TLabDataItemList.GetItems(aIndex: Integer): TLabDataItem;
begin
  Result := TLabDataItem(DataItems[aIndex]);
end;

//:---------------------------------------------------------------------------
function TLabDataItemList.GetKeyFieldForLevel(aLevel: Integer): string;
var
  i: Integer;
begin
  Result := '';
  for i:=0 to DataItemCount-1 do begin
    with Items[i] do begin
      if KeyField and ShowInTable and (aLevel = TableIndex) then begin
        Result := KeyFieldName;
        break;
      end;
    end;
  end;
end;

//:---------------------------------------------------------------------------
function TLabDataItemList.GetKeyItemForLevel(aLevel: Integer): TLabDataItem;
var
  i: Integer;
begin
  Result := Nil;
  for i:=0 to DataItemCount-1 do begin
    with Items[i] do begin
      if KeyField and ShowInTable and (aLevel = TableIndex) then begin
        Result := Items[i];
        break;
      end;
    end;
  end;
end;

//:---------------------------------------------------------------------------
function TLabDataItemList.GetMatrixOptions: PMatrixOptions;
begin
  Result := @fMatrixOptions;
end;

//:---------------------------------------------------------------------------
function TLabDataItemList.GetReportOptions: PReportOptions;
begin
  Result := @fReportOptions;
end;

//:---------------------------------------------------------------------------
function TLabDataItemList.GetSQLFromTables: string;
begin
  Result := inherited GetSQLFromTables;
  //:...................................................................
  if Result = '' then
    Result := cTimeModeTables[Queryparameters.TimeMode]
  else
    Result := cTimeModeTables[Queryparameters.TimeMode] + ',' + Result;
  
  CodeSite.SendString('DataItemFromTables', Result);
end;

//:---------------------------------------------------------------------------
function TLabDataItemList.GetSQLSelectFields(aWithSum: boolean = true; aWithAlias: boolean = true): string;
begin
  Result := inherited GetSQLSelectFields(aWithSum);
  //:...................................................................
  if fDataMode = dmChart then begin
    if ZAxis <> '' then
      Result := ZAxisItem.GetSQLSelectFields + ',' + Result;
    if XAxis <> '' then
      Result := XAxisItem.GetSQLSelectFields + ',' + Result;
  end;
end;

//:---------------------------------------------------------------------------
function TLabDataItemList.GetSQLWhere: string;
begin
  Result := inherited GetSQLWhere;
end;

//:---------------------------------------------------------------------------
function TLabDataItemList.GetXAxisItem: TLabDataItem;
begin
  if fXAxis <> '' then
    Result := TLabDataItem(gDataItemPool.DataItemByName[fXAxis])
  else
    Result := Nil;
end;

//:---------------------------------------------------------------------------
function TLabDataItemList.GetZAxisItem: TLabDataItem;
begin
  if fZAxis <> '' then
    Result := TLabDataItem(gDataItemPool.DataItemByName[fZAxis])
  else
    Result := Nil;
end;

//:---------------------------------------------------------------------------
function TLabDataItemList.HasLeftTop: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i:=0 to DataItemCount-1 do begin
    with Items[i] do
      Result := ShowInChart and (AxisType = atLeftTop);
    if Result then
      Break;
  end;
end;

//:---------------------------------------------------------------------------
function TLabDataItemList.HasRightTop: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i:=0 to DataItemCount-1 do begin
    with Items[i] do
      Result := ShowInChart and (AxisType = atRightTop);
    if Result then
      Break;
  end;
end;

//:---------------------------------------------------------------------------
procedure TLabDataItemList.InitDataItems(aMode: Integer);
var
  xIndex: Integer;
  i: Integer;
  xStrList: TMemIniFile;
  xList: TStringList;
begin
  // Vordefinierte DataItems von der Datenbank holen (ClassItems)
  if aMode = 0 then
    LoadFromDatabase(cLabItemClass);
  
  // Nun noch die normalen DataItems f�r den LabReport hinzuf�gen
  for i:=0 to cDataItemCount-1 do begin
    if (aMode = 0) OR ((aMode = 1) and (cDataItemArray[i].UseMode = umAlways)) then begin
      // Wenn dieses DataItem noch nicht vorhanden ist, dann noch hinzuf�gen
      xIndex := IndexOfItemName(cDataItemArray[i].Field);
      if xIndex = -1 then begin
        xIndex := CreateAndAddNewDataItem(TDataItem, cLabItemClass);
          // Eigenschaften des neuen Items setzen
        with Items[xIndex] do begin
          if cDataItemArray[i].ClassTypeGroup = ctLabData then
            ClassTypeGroup := ctLabData
          else
            ClassTypeGroup := ctDataItem;
          Description    := ''; //aDescription;
          OrderPos       := i;
          Predefined     := True;
  
          Alignment      := cDataItemArray[i].Alignment;
          AverageStep    := cDataItemArray[i].AverageStep;
          CalcMode       := cDataItemArray[i].CalcMode;
          ChartIndex     := cDataItemArray[i].ChartIndex;
          Col            := cDataItemArray[i].Col;
          Color          := cDataItemArray[i].Color;
          Commas         := cDataItemArray[i].Commas;
          DataType       := cDataItemArray[i].DataType;
          DescendingSort := cDataItemArray[i].DescendingSort;
          ItemName       := cDataItemArray[i].Field;
          KeyField       := cDataItemArray[i].KeyField;
          KeyFieldName   := cDataItemArray[i].KeyFieldName;
          LowerLimit     := cDataItemArray[i].LowerLimit;
          MassField      := cDataItemArray[i].MassField;
          RightAxis      := cDataItemArray[i].RightAxis;
          ShowInChart    := cDataItemArray[i].ShowInChart;
          ShowInTable    := cDataItemArray[i].ShowInTable;
          ShowSeconds    := cDataItemArray[i].ShowSeconds;
          Statistic      := cDataItemArray[i].Statistic;
          TableIndex     := cDataItemArray[i].TableIndex;
          Thousands      := cDataItemArray[i].Thousands;
          UpperLimit     := cDataItemArray[i].UpperLimit;
          // DisplayName am Schluss setzen, da sonst von ItemName �berschrieben
          DisplayName    := cDataItemArray[i].DisplayName;
  //        YScalingType   := cDataItemArray[i].YScalingType;
        end; // with
      end; // if xIndex
      with Items[xIndex] do begin
        UseMode := cDataItemArray[i].UseMode;
      end;
    end; // if
  end; // for
end;

//:---------------------------------------------------------------------------
function TLabDataItemList.LevelCount: Integer;
var
  i: Integer;
begin
  Result := 0;
  for i:=0 to DataItemCount-1 do begin
    with Items[i] do begin
      if KeyField and ShowInTable then
        inc(Result);
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TLabDataItemList.ReadFromConfig(aIniFile: TCustomIniFile);
var
  i: Integer;
  xDataItemCount: Integer;
  xAxisType: TAxisType;
  
  function DecodeAxis(aAxisString: String): TAxisInfoRec;
  begin
    ZeroMemory(@Result, SizeOf(TAxisInfoRec));
    with TIniStringList.Create do
    try
      CommaText := aAxisString;
      Result.Scaling := ValueDef('Scaling', asAutomatic);
      Result.Min     := ValueDef('Min', 0);
      Result.Max     := ValueDef('Max', 0);
    finally
      Free;
    end;
  end;
  
begin
  Clear;
  with aIniFile do begin
    with fReportOptions do begin
      CompanyName       := ReadString('ReportOptions', 'CompanyName',   CompanyName);
      ReportTitle       := ReadString('ReportOptions', 'ReportTitle',   ReportTitle);
      ShowExtend        := ReadBool  ('ReportOptions', 'ShowExtend',    ShowExtend);
      UseYMSettings     := ReadBool  ('ReportOptions', 'UseYMSettings', UseYMSettings);
      ShowStyle         := ReadBool  ('ReportOptions', 'ShowStyle',     ShowStyle);
      ShowLot           := ReadBool  ('ReportOptions', 'ShowLot',       ShowLot);
      ShowMachine       := ReadBool  ('ReportOptions', 'ShowMachine',   ShowMachine);
      ShowYMSettings    := ReadBool  ('ReportOptions', 'ShowYMSettings', ShowYMSettings);
      PrintChart        := ReadBool  ('ReportOptions', 'PrintChart',    PrintChart);
      PrintQMatrix      := ReadBool  ('ReportOptions', 'PrintQMatrix',  PrintQMatrix);
      PrintCluster      := ReadBool  ('ReportOptions', 'PrintCluster',  PrintCluster);
      PrintSpliceMatrix := ReadBool  ('ReportOptions', 'PrintSpliceMatrix',  True);
      PrintFFMatrix     := ReadBool  ('ReportOptions', 'PrintFFMatrix', PrintFFMatrix);
      PrintTable        := ReadBool  ('ReportOptions', 'PrintTable',    PrintTable);
      PrintYMSettings   := ReadBool  ('ReportOptions', 'PrintYMSettings', PrintYMSettings);
      StretchTable      := ReadBool  ('ReportOptions', 'StretchTable',  StretchTable);
      NewPageMatrix     := ReadBool  ('ReportOptions', 'NewPageMatrix', NewPageMatrix);
      NewPageYMSettings := ReadBool  ('ReportOptions', 'NewPageYMSettings', NewPageYMSettings);
      NewPageTable      := ReadBool  ('ReportOptions', 'NewPageTable',  NewPageTable);
      RectChart         := StrToRect(ReadString('ReportOptions', 'RectChart',     '-1,-1,-1,-1'));
      RectQMatrix       := StrToRect(ReadString('ReportOptions', 'RectQMatrix',   '-1,-1,-1,-1'));
      RectSpliceMatrix  := StrToRect(ReadString('ReportOptions', 'RectSpliceMatrix', '-1,-1,-1,-1'));
      RectFFMatrix      := StrToRect(ReadString('ReportOptions', 'RectFFMatrix',  '-1,-1,-1,-1'));
    end;
  
    with fChartOptions do begin
      BarStyle    := TBarStyle(ReadInteger('ChartOptions', 'BarStyle', Ord(BarStyle)));
      LegendAlign := TLegendAlignment(ReadInteger('ChartOptions', 'LegendAlign', Ord(LegendAlign)));
      View3D      := ReadBool('ChartOptions', 'View3D', View3D);
      AllowRotate := ReadBool('ChartOptions', 'AllowRotate', AllowRotate);
      Width3D     := ReadInteger('ChartOptions', 'Width3D', Width3D);
      RotateX     := ReadInteger('ChartOptions', 'RotateX', RotateX);
      RotateY     := ReadInteger('ChartOptions', 'RotateY', RotateY);
  
      StartColor  := ReadInteger('ChartOptions', 'StartColor', StartColor);
      EndColor    := ReadInteger('ChartOptions', 'EndColor', EndColor);
      ImageInside := ReadBool('ChartOptions',    'ImageInside',  ImageInside);
      ImageFile   := ReadString('ChartOptions',  'ImageFile',  '');
      Zoom        := ReadInteger('ChartOptions', 'Zoom', Zoom);
      HPos        := ReadInteger('ChartOptions', 'HPos', 0);
      VPos        := ReadInteger('ChartOptions', 'VPos', 0);
      XLabelRotated := ReadBool('ChartOptions', 'XLabelRotated', False);
  
      with LeftAxis do begin
        AutoScale := ReadBool('ChartOptions', 'LeftAxisAutoScale', AutoScale);
        Min       := ReadInteger('ChartOptions', 'LeftAxisMin', Min);
        Max       := ReadInteger('ChartOptions', 'LeftAxisMax', Max);
      end;
      with RightAxis do begin
        AutoScale := ReadBool('ChartOptions', 'RightAxisAutoScale', AutoScale);
        Min       := ReadInteger('ChartOptions', 'RightAxisMin', Min);
        Max       := ReadInteger('ChartOptions', 'RightAxisMax', Max);
      end;
  
      for xAxisType:=Low(TAxisType) to High(TAxisType) do begin
        AxisInfo[xAxisType] := DecodeAxis(ReadString('ChartOptions', 'AxisInfo' + IntToStr(ord(xAxisType)), ''));
      end;
    end;
  
    // read MatrixOptions
    with fMatrixOptions do begin
      ChannelVisible := ReadBool('MatrixOptions', 'ChannelVisible', ChannelVisible);
      ClusterVisible := ReadBool('MatrixOptions', 'ClusterVisible', ClusterVisible);
      SpliceVisible  := ReadBool('MatrixOptions', 'SpliceVisible',  SpliceVisible);
      DefectValues   := ReadBool('MatrixOptions', 'DefectValues',   DefectValues);
      CutValues      := ReadBool('MatrixOptions', 'CutValues',      CutValues);
      ActiveVisible  := ReadBool('MatrixOptions', 'ActiveVisible',  ActiveVisible);
      ActiveColor    := ReadInteger('MatrixOptions', 'ActiveColor', ActiveColor);
      MatrixMode     := ReadInteger('MatrixOptions', 'MatrixMode',  MatrixMode);
    end;
  
    // read GridOptions
    with fGridOptions do begin
      i := ReadInteger('GridOptions', 'Options', 0);
      if i = 0 then
        Options := [0,1,3]
      else
        ConvertIntToSet(i, @Options, SizeOf(Options));
      FixedCols := ReadInteger('GridOptions', 'FixedCols', FixedCols);
      Hierarchy := ReadBool('GridOptions', 'Hierarchy', False);
      with TitleFont do begin
        Name    := ReadString('GridOptions', 'TitleFontName', 'MS Sans Serif');
        Color   := ReadInteger('GridOptions', 'TitleFontColor', Color);
        BkColor := ReadInteger('GridOptions', 'TitleFontBkColor', BkColor);
        Size    := ReadInteger('GridOptions', 'TitleFontSize', Size);
        ConvertIntToSet(ReadInteger('GridOptions', 'TitleFontStyle', 0), @Style, SizeOf(Style));
      end;
      with DataFont do begin
        Name    := ReadString( 'GridOptions', 'DataFontName', 'MS Sans Serif');
        Color   := ReadInteger('GridOptions', 'DataFontColor', Color);
        BkColor := ReadInteger('GridOptions', 'DataFontBkColor', BkColor);
        Size    := ReadInteger('GridOptions', 'DataFontSize', Size);
        ConvertIntToSet(ReadInteger('GridOptions', 'DataFontStyle', 0), @Style, SizeOf(Style));
      end;
    end;
  
    // read data items settings
    xDataItemCount := ReadInteger('DataItems', 'DataItemsCount', 0);
    for i:=0 to xDataItemCount-1 do begin
      AddClassFromConfigString(ReadString('DataItems', 'Item' + IntToStr(i), ''), gDataItemPool{, cLabItemClass});
    end; // for i
  
    // read key axis values
    XAxis := ReadString('ChartOptions', 'XAxis', '');
    ZAxis := ReadString('ChartOptions', 'ZAxis', '');
  
    // Nun noch �berpr�fen, ob auch alle Items mit umAlways vorhanden sind
    InitDataItems(1);
  end; // with aIniFile
end;

//:---------------------------------------------------------------------------
procedure TLabDataItemList.SetDataLevel(Value: Integer);
var
  xDataLevel: Integer;
  i: Integer;
begin
  fDataLevel := Value;
  if fGridOptions.Hierarchy then xDataLevel := fDataLevel
                            else xDataLevel := -1;
  for i:=0 to DataItemCount-1 do
    Items[i].DataLevel := xDataLevel;
end;

//:---------------------------------------------------------------------------
procedure TLabDataItemList.SetDataMode(Value: TDataMode);
var
  i: Integer;
begin
  fDataMode := Value;
  for i:=0 to DataItemCount-1 do
    Items[i].DataMode := fDataMode;
end;

//:---------------------------------------------------------------------------
procedure TLabDataItemList.SetXAxis(const Value: string);
begin
  if fXAxis <> Value then begin
    // if previous selection was valid remove flag
  //TODO: Anpassen
    if gDataItemPool.IndexOfItemName(Value) <> -1 then
      fXAxis := Value
    else
      fXAxis := '';
  end;
end;

//:---------------------------------------------------------------------------
procedure TLabDataItemList.SetZAxis(const Value: string);
begin
  if fZAxis <> Value then begin
    // if previous selection was valid remove flag
  //TODO: Anpassen
    if gDataItemPool.IndexOfItemName(Value) <> -1 then
      fZAxis := Value
    else
      fZAxis := '';
  end;
end;

//:---------------------------------------------------------------------------
procedure TLabDataItemList.WriteToConfig(aIniFile: TCustomIniFile);
var
  i: Integer;
  xAxis: TAxisType;
  
  function EncodeAxis(aAxisInfo: TAxisInfoRec): string;
  begin
    Result := '';
    with TIniStringList.Create do
    try
      with aAxisInfo do begin
        Values['Scaling'] := Scaling;
        Values['Min']     := Min;
        Values['Max']     := Max;
      end;
      Result := CommaText;
    finally
      Free;
    end;
  end;
  
begin
  with aIniFile do begin
    with fReportOptions do begin
      WriteString('ReportOptions', 'CompanyName',   CompanyName);
      WriteString('ReportOptions', 'ReportTitle',   ReportTitle);
      WriteBool  ('ReportOptions', 'ShowExtend',    ShowExtend);
      WriteBool  ('ReportOptions', 'UseYMSettings', UseYMSettings);
      WriteBool  ('ReportOptions', 'ShowStyle',     ShowStyle);
      WriteBool  ('ReportOptions', 'ShowLot',       ShowLot);
      WriteBool  ('ReportOptions', 'ShowMachine',   ShowMachine);
      WriteBool  ('ReportOptions', 'ShowYMSettings', ShowYMSettings);
      WriteBool  ('ReportOptions', 'PrintChart',    PrintChart);
      WriteBool  ('ReportOptions', 'PrintQMatrix',  PrintQMatrix);
      WriteBool  ('ReportOptions', 'PrintCluster',  PrintCluster);
      WriteBool  ('ReportOptions', 'PrintSpliceMatrix', PrintSpliceMatrix);
      WriteBool  ('ReportOptions', 'PrintFFMatrix', PrintFFMatrix);
      WriteBool  ('ReportOptions', 'PrintYMSettings', PrintYMSettings);
      WriteBool  ('ReportOptions', 'PrintTable',    PrintTable);
      WriteBool  ('ReportOptions', 'StretchTable',    StretchTable);
      WriteBool  ('ReportOptions', 'NewPageMatrix', NewPageMatrix);
      WriteBool  ('ReportOptions', 'NewPageYMSettings', NewPageYMSettings);
      WriteBool  ('ReportOptions', 'NewPageTable',  NewPageTable);
      WriteString('ReportOptions', 'RectChart',     RectToStr(RectChart));
      WriteString('ReportOptions', 'RectQMatrix',   RectToStr(RectQMatrix));
      WriteString('ReportOptions', 'RectSpliceMatrix', RectToStr(RectSpliceMatrix));
      WriteString('ReportOptions', 'RectFFMatrix',  RectToStr(RectFFMatrix));
    end;
  
    // write ChartOptions
    with fChartOptions do begin
      WriteInteger('ChartOptions', 'BarStyle', Ord(BarStyle));
      WriteInteger('ChartOptions', 'LegendAlign', Ord(LegendAlign));
  
      WriteBool('ChartOptions', 'View3D', View3D);
      WriteBool('ChartOptions', 'AllowRotate', AllowRotate);
      WriteInteger('ChartOptions', 'Width3D', Width3D);
      WriteInteger('ChartOptions', 'RotateX', RotateX);
      WriteInteger('ChartOptions', 'RotateY', RotateY);
  
      WriteInteger('ChartOptions', 'StartColor', StartColor);
      WriteInteger('ChartOptions', 'EndColor', EndColor);
      WriteBool('ChartOptions',    'ImageInside',  ImageInside);
      WriteString('ChartOptions',  'ImageFile',  ImageFile);
      WriteInteger('ChartOptions', 'Zoom', Zoom);
      WriteInteger('ChartOptions', 'HPos', HPos);
      WriteInteger('ChartOptions', 'VPos', VPos);
      WriteBool('ChartOptions', 'XLabelRotated', XLabelRotated);
  
      with LeftAxis do begin
        WriteBool('ChartOptions', 'LeftAxisAutoScale', AutoScale);
        WriteInteger('ChartOptions', 'LeftAxisMin', Min);
        WriteInteger('ChartOptions', 'LeftAxisMax', Max);
      end;
  
      with RightAxis do begin
        WriteBool('ChartOptions', 'RightAxisAutoScale', AutoScale);
        WriteInteger('ChartOptions', 'RightAxisMin', Min);
        WriteInteger('ChartOptions', 'RightAxisMax', Max);
      end;
  
      for xAxis:=Low(TAxisType) to High(TAxisType) do begin
        WriteString('ChartOptions', 'AxisInfo' + IntToStr(ord(xAxis)), EncodeAxis(AxisInfo[xAxis]));
      end;
    end;
  
      // write MatrixOptions
    with fMatrixOptions do begin
      WriteBool('MatrixOptions', 'ChannelVisible', ChannelVisible);
      WriteBool('MatrixOptions', 'ClusterVisible', ClusterVisible);
      WriteBool('MatrixOptions', 'SpliceVisible',  SpliceVisible);
      WriteBool('MatrixOptions', 'DefectValues', DefectValues);
      WriteBool('MatrixOptions', 'CutValues',    CutValues);
      WriteBool('MatrixOptions', 'ActiveVisible',  ActiveVisible);
      WriteInteger('MatrixOptions', 'ActiveColor',  ActiveColor);
      WriteInteger('MatrixOptions', 'MatrixMode',  MatrixMode);
    end;
  
      // write GridOptions
    with fGridOptions do begin
      WriteInteger('GridOptions', 'Options', ConvertSetToInt(@Options, SizeOf(Options)));
      WriteInteger('GridOptions', 'FixedCols', fGridOptions.FixedCols);
      WriteBool('GridOptions', 'Hierarchy', Hierarchy);
      with TitleFont do begin
        WriteString('GridOptions', 'TitleFontName', Name);
        WriteInteger('GridOptions', 'TitleFontColor', Color);
        WriteInteger('GridOptions', 'TitleFontBkColor', BkColor);
        WriteInteger('GridOptions', 'TitleFontSize', Size);
        WriteInteger('GridOptions', 'TitleFontStyle', ConvertSetToInt(@Style, SizeOf(Style)));
      end;
      with DataFont do begin
        WriteString( 'GridOptions', 'DataFontName', Name);
        WriteInteger('GridOptions', 'DataFontColor', Color);
        WriteInteger('GridOptions', 'DataFontBkColor', BkColor);
        WriteInteger('GridOptions', 'DataFontSize', Size);
        WriteInteger('GridOptions', 'DataFontStyle', ConvertSetToInt(@Style, SizeOf(Style)));
      end;
    end;
  
    // write data items settings
    WriteInteger('DataItems', 'DataItemsCount', DataItemCount);
    for i:=0 to DataItemCount-1 do begin
      WriteString('DataItems', 'Item' + IntToStr(i), Items[i].ConfigString);
    end;
  
    // write key axis values
    WriteString('ChartOptions', 'XAxis', fXAxis);
    WriteString('ChartOptions', 'ZAxis', fZAxis);
  end;
end;

//:---------------------------------------------------------------------------
//:--- Class: TDetailInfoHelper
//:---------------------------------------------------------------------------
constructor TDetailInfoHelper.Create(aContainer: TLabDataItemList; aDataModul: TdmLabReport);
begin
  inherited Create;
  //:...................................................................
  mDataItemlist := aContainer;
  mDM           := aDataModul;
  mSeries       := Nil;
  mInfoList     := TIniStringList.Create;
end;

//:---------------------------------------------------------------------------
destructor TDetailInfoHelper.Destroy;
begin
  mInfoList.Free;
  //:...................................................................
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
function TDetailInfoHelper.GetDetailInfoString(aClickedSeries: TChartSeries; X, Y, aValueIndex: Integer): string;
var
  xItem: TLabDataItem;
  xValueIndex: Integer;
  
  //..................................................................
  procedure AddValue(aLabel: String; aValue: Variant);
  var
    xTextWidth: Integer;
  begin
    xTextWidth := Application.MainForm.Canvas.TextWidth(aLabel);
    if xTextWidth > mMaxLabelWidth then
      mMaxLabelWidth := xTextWidth;
  
    mInfoList.Values[aLabel] := aValue;
  end;
  //..................................................................
  function ExtractInfoString: String;
  begin
    Result := mInfoList.Text;
  end;
  //..................................................................
  
begin
  Result := '';
  mInfoList.Clear;
  mMaxLabelWidth := 0;
  
  mSeries := aClickedSeries;
  xItem    := TLabDataItem(mSeries.Tag);
  xValueIndex := mSeries.Clicked(X, Y);
  
  with mDataItemList do
  try
  //    AddValue(Translate(XAxisItem.DisplayName), mSeries.XLabel[aValueIndex]);
  //    if mDataItemList.ZAxis <> '' then
  //      AddValue(Translate(ZAxisItem.DisplayName), '');
  
    AddValue('DatenItem', mSeries.Title);
      // X-Achse
    if mSeries.XValues.DateTime then
      AddValue(Translate(XAxisItem.DisplayName), DateTimeToStr(mSeries.XValue[aValueIndex]))
    else
      AddValue(Translate(XAxisItem.DisplayName), mSeries.XValue[aValueIndex]);
      // Z-Achse
    if mDataItemList.ZAxis <> '' then
      AddValue(Translate(ZAxisItem.DisplayName), '');
  
    if xValueIndex <> -1 then
      AddValue(Translate(xItem.DisplayName), mSeries.YValues[xValueIndex]);
    Result := ExtractInfoString;
  
  //    xStrList.Add(Translate(XAxisItem.DisplayName));
  //    if mDataItemList.ZAxis <> '' then
  //      xStrList.Add(Translate(ZAxisItem.DisplayName));
  //    if xValueIndex <> -1 then
  //      xStrList.Add(Format('%s'#9'%f', [Translate(xItem.DisplayName), mSeries.YValues[xValueIndex]]));
  //    Result := xStrList.Text;
  finally
  end;
end;

//:---------------------------------------------------------------------------
//:--- Class: TdmLabReport
//:---------------------------------------------------------------------------
constructor TdmLabReport.Create(aOwner: TComponent; aDataItemList: TLabDataItemList);
begin
  inherited Create(aOwner);
  fResultQuery  := Nil;
  fDBParams     := TQueryParameters.Create;
  mDataItemList := aDataItemList;
  // �ber diesen Event wird definiert, welche Felder im Chart oder Grid Query erscheinen werden
  // Dazu wird ein Flag verwendet wo vorher gesetzt wird, ob das Chart oder das Grid behandelt wird.
  mDataItemList.OnGetDataItemGenerateSQL := DoGetDataItemGenerateSQL;
  mFrom         := TStringList.Create;
  mSelect       := TStringList.Create;
  mWhere        := TStringList.Create;
end;

//:---------------------------------------------------------------------------
destructor TdmLabReport.Destroy;
begin
  FreeAndNil(mFrom);
  FreeAndNil(mSelect);
  FreeAndNil(mWhere);
  //:...................................................................
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
function TdmLabReport.CreateAllQueryFields(aDataMode: TDataMode; aLevel: Integer = 0; aAddWhere: String = ''): Boolean;
var
  xGroupList: TmmStringList;
  xOrderList: TmmStringList;
  xCountKeyField: Boolean;
  i: Integer;
  xQuery: TmmADODataSet;
  xParam: TParameter;
  xTag: Integer;
  
  function CreateNewField(aItem: TLabDataItem; aKind: TFieldKind): TField;
  var
    xStr: String;
    xTag: Integer;
  begin
    // create field ...
    if xCountKeyField then
      Result := TStringField.Create(Self)
    else begin
      case aItem.DataType of
        ftDateTime: Result := TDateTimeField.Create(Self);
        ftString:   Result := TStringField.Create(Self);
        ftInteger:  Result := TIntegerField.Create(Self);
      else  // ftFloat
        Result := TFloatField.Create(Self);
      end;
    end;
  
    if Result is TStringField then
      with TStringField(Result) do
        Size := 70;
  
    // and set some properties
    with Result do begin
      FieldKind := aKind;
      Tag       := -1;
      Visible   := (aKind = fkCalculated); // or aItem.KeyField;
  
      // Feldname mit c_ oder calc_ setzen
      if (aKind = fkData) then begin
        FieldName := aItem.ColName;
        // Create dynamically the key fields for the query
        if aItem.KeyField then begin
          // bei hierarchischer Tabelle sind z.T. Schl�sselfelder nur die Anzahl sichtbar
          if not xCountKeyField then begin
            xStr := aItem.ColName;
            // bei KeyField Details muss noch das ID-Field in der Group-Anweisung auch noch hinzugef�gt werden
            xGroupList.AddObject(xStr, Pointer(xTag));
            if aItem.DescendingSort then
              xOrderList.AddObject(xStr + ' DESC', Pointer(xTag))
            else
              xOrderList.AddObject(xStr + ' ASC', Pointer(xTag));
          end;
        end;
      end else
        FieldName := aItem.CalcName;
  
      Name := xQuery.Name + Format('_Field_%d', [xQuery.Fields.Count]);
  
      if Visible then begin
        // In Tag wird ein Link zum DataItem Objekt abgelegt
        Tag := Integer(aItem);
        if mDataItemList.DataMode = dmGrid then xTag := aItem.TableIndex
                                           else xTag := aItem.ChartIndex;
        Alignment    := aItem.Alignment;
        DisplayLabel := aItem.DisplayName;
        DisplayWidth := 20;
  
        // if is visible and a nummeric field then set formating string
        if Result is TNumericField then begin
          with TNumericField(Result) do begin
            DisplayFormat := cNumberFormat[aItem.Commas];
            if aItem.Thousands then
              DisplayFormat := ',' + DisplayFormat;
          end;
        end;
      end else begin
        DisplayLabel := FieldName;
        xTag          := -1;
      end; // if Visible
      // now assign the field to its dataset
      DataSet := xQuery;
      if xTag <> -1 then
        Index := 0;
    end; // with Result
  end;
        //.......................................................................
  
begin
  Result := False;
  mDataItemList.DataMode  := aDataMode;
  mDataItemList.DataLevel := aLevel;
  case mDataItemList.DataMode of
    dmChart: xQuery := dseChart;
    dmGrid:  if aLevel = 0 then xQuery := dseGrid
                           else xQuery := dseGridDetail;
  end;// case aQueryType of
  FResultQuery := xQuery;
  
  EnterMethod('CreateAllQueryFields: ' + xQuery.Name);
    //  xKeyList := TmmStringList.Create;
  xGroupList := TmmStringList.Create;
  xOrderList := TmmStringList.Create;
  
        // clean up unneccessary fields and hide fields with Tag = -1
  with xQuery do begin
    Close;
    while FieldCount > 0 do
      Fields[0].Free;
  end;
  
      // create field from selected item list
  with mDataItemList do begin
        // F�r die Grafik m�ssen die DataItems f�r die X- und Z-Achse sonderbehandelt werden
        // da diese nicht �ber das Property ShowInChart aktiviert werden
    if mDataItemList.DataMode = dmChart then begin
      if XAxis <> '' then begin
        CreateNewField(XAxisItem, fkData);
        CreateNewField(XAxisItem, fkCalculated);
      end;
      if ZAxis <> '' then begin
        CreateNewField(ZAxisItem, fkData);
        CreateNewField(ZAxisItem, fkCalculated);
      end;
    end;
        // create dynamic needed key fields and set associated properties in calc_XXX fields
    for i:=0 to DataItemCount-1 do begin
      with Items[i] do begin
            // FIRST: create a field with direct link to a column (c_xxx) in database
        if (UseMode = umAlways) or
           (ShowInTable and (mDataItemList.DataMode = dmGrid)) or
           (ShowInChart and (mDataItemList.DataMode = dmChart)) then begin
  
          xCountKeyField := GridOptions^.Hierarchy and KeyField and
                            (mDataItemList.DataMode = dmGrid) and (TableIndex >= aLevel);
          if UseMode <> umNever then
            CreateNewField(Items[i], fkData);
  
          if ((ShowInTable and (mDataItemList.DataMode = dmGrid)) or
              (ShowInChart and (mDataItemList.DataMode = dmChart))) then begin
            CreateNewField(Items[i], fkCalculated);
          end;
        end; // if
  
          // SECOND: create a field for use as calculated field (calc_xxx). ONLY for data fields
  //        if not KeyField and
  //           ((ShowInTable and (mDataItemList.DataMode = dmGrid)) or
  //            (ShowInChart and (mDataItemList.DataMode = dmChart))) then begin
  //          CreateNewField(Items[i], fkCalculated);
  //        end;
      end; // with Items
    end; // for i
  end; // with mDataItemList
  
      // -------------------
      // now reordering the fields according to the Tag value
      // -------------------
  with xQuery do begin
      {}
      CodeSite.EnterMethod('Debug xQuery vor sortieren');
      for i:=0 to FieldCount-1 do begin
        with Fields[i] do begin
      //        if Visible then
            CodeSite.SendFmtMsg('%s: Index=%d, Tag=%d', [FieldName, Index, Tag]);
        end;
      end;
      CodeSite.ExitMethod('');
      {}
    i := 0;
    while i < FieldCount do begin
      with Fields[i] do begin
        if (Tag <> -1) and (Tag <> 0) then begin
          if mDataItemList.DataMode = dmGrid then xTag := TLabDataItem(Tag).TableIndex
                                             else xTag := TLabDataItem(Tag).ChartIndex;
        end else
          xTag := -1;
  
        if (xTag = -1) or (xTag = i) then
          inc(i)
        else if xTag < i then begin
          Index := xTag;
          inc(i);
        end else if xTag > i then
          Index := xTag;
      end;
    end;
      {
        CodeSite.EnterMethod('Debug xQuery nach sortieren');
        for i:=0 to FieldCount-1 do begin
          with Fields[i] do begin
      //        if Visible then
              CodeSite.SendFmtMsg('%s: Index=%d, Tag=%d', [FieldName, Index, Tag]);
          end;
        end;
      {}
  end;
  
    //  xKeyList.CustomSort(KeyStrSortProc);
  xGroupList.CustomSort(KeyStrSortProc);
  xOrderList.CustomSort(KeyStrSortProc);
    //  CodeSite.SendString('Key sorting', xKeyList.CommaText);
  
      // assign query string and open query object
  with xQuery do
  try
    CommandText := Format(cBasicQuery[fDBParams.TimeMode], ['', mDataItemList.GetSQLSelectFields, mDataItemList.GetSQLFromTables, fDefaultShiftCal, GetWhereStr(aAddWhere, True)]);
    if xGroupList.Count > 0 then
      CommandText := CommandText + Format(' Group by %s', [StringReplace(xGroupList.CommaText, '"', '', [rfReplaceAll])]);
    if xOrderList.Count > 0 then
      CommandText := CommandText + Format(' Order by %s', [StringReplace(xOrderList.CommaText, '"', '', [rfReplaceAll])]);
  
    CodeSite.SendString('CommandText', CommandText);
  
    Open;
    Result := Active;
  except
    on e:Exception do begin
      CodeSite.SendError('aQuery open failed: ' + e.Message);
    end;
  end;
  
    //  xKeyList.Free;
  xGroupList.Free;
  xOrderList.Free;
end;

//:---------------------------------------------------------------------------
procedure TdmLabReport.DataSetCalcFields(DataSet: TDataSet);
var
  xLen: Single;
  xDoCalc: Boolean;
  i: Integer;
  xInt: Integer;
  xItem: TLabDataItem;
begin
  with DataSet do begin
    // first get the c_len value and check if it is valid
    xLen    := FieldByName('c_Len').AsFloat;
    xDoCalc := (xLen >= 1.0);
    for i:=0 to FieldList.Count-1 do begin
      if Fields[i].Visible then begin
        xItem := TLabDataItem(Fields[i].Tag);
        case xItem.CalcMode of
          -1: with FieldByName(xItem.CalcName) do begin
                Value := FieldByName(xItem.ColName).Value;
              end;
          // calculate SFI/D value
          -2: with FieldByName('calc_SFI_D') do begin
                Value := CalcSFI_D(FieldByName('c_SFI').AsFloat, FieldByName('c_SFICnt').AsFloat);
              end;
          // calculate SFI value
          -3: begin
              xInt := FieldByName('c_AdjustBaseCnt').AsInteger;
              with FieldByName('calc_SFI') do begin
                if xInt > 0 then
                  Value := CalcSFI(FieldByName('c_SFI').AsFloat, FieldByName('c_SFICnt').AsFloat, FieldByName('c_AdjustBase').AsFloat / xInt)
                else
                  Value := 0.0;
              end;
            end;
          // ProdNutzeffekt
          -4: with FieldByName('calc_PEff') do begin
                if FieldByName('c_wtSpd').AsFloat > 0.0 then
                  Value := (FieldByName('c_rtSpd').AsFloat / FieldByName('c_wtSpd').AsFloat) * 100
                else
                  Value := 0.0;
              end;
        else
          // distinguish mode for calculation in CalcValue from CalcMode value
          // CalcMode = 0: general data items has to be calcualted to LenBase
          // CalcMode > 0: calculations for Imperfection at fixed meter
          try
            with FieldByName(xItem.CalcName) do begin
              if xDoCalc then
                Value := CalcValue(fLenBase, FieldByName(xItem.ColName).AsFloat, xLen, xItem.CalcMode)
              else
                Value := 0.0;
            end;
          except
            on e:Exception do begin
              CodeSite.SendError('DataSetCalcFields() ' + e.Message);
              break;
            end;//
          end;// try except
        end; // case
      end;
    end;
  end; // with
end;

//:---------------------------------------------------------------------------
procedure TdmLabReport.DoGetDataItemGenerateSQL(Sender: TObject; aDataItem: TBaseDataItem; var aEnabled: boolean);
var
  xItem: TLabDataItem;
begin
  xItem := TLabDataItem(aDataItem);
  // Enabled = True wenn
  // alle ODER KlassierItems ODER (nicht Never UND verwendet in (Chart ODER Grid))
  //aEnabled := (xItem.UseMode = umAlways) OR (xItem.ClassTypeGroup <> ctDataItem) OR
  aEnabled := (xItem.UseMode = umAlways) OR (xItem.ClassTypeGroup in [ctLongThickThin, ctSplice, ctFF]) OR
              (
                ( (xItem.UseMode <> umNever) )
                AND
                ( ((mDataItemList.DataMode = dmChart) and xItem.ShowInChart) OR
                  ((mDataItemList.DataMode = dmGrid)  and xItem.ShowInTable) )
              );
  CodeSite.SendBoolean(xItem.ItemName, aEnabled);
end;

//:---------------------------------------------------------------------------
procedure TdmLabReport.dseBeforeOpen(DataSet: TDataSet);
var
  xParam: TParameter;
begin
  CodeSite.SendMsg('dseBeforOpen');
  with TmmADODataSet(DataSet) do begin
    xParam := Parameters.FindParam('TimeFrom');
    if xParam <> nil then begin
      xParam.DataType := ftDateTime;
      xParam.Value    := fDBParams.TimeFrom;
    end;
    xParam := Parameters.FindParam('TimeTo');
    if xParam <> nil then begin
      xParam.DataType := ftDateTime;
      xParam.Value    := fDBParams.TimeTo;
    end;
  end;
end;

//:---------------------------------------------------------------------------
function TdmLabReport.GetAxisNamesForChart(aFieldName: String; var aStrList: TStringList): Boolean;
begin
  Result := False;
  aStrList.Clear;
  with dseWork do
  try
    CommandText := Format(cBasicQuery[fDBParams.TimeMode], ['distinct', aFieldName, cTimeModeTables[fDBParams.TimeMode], fDefaultShiftCal, GetWhereStr('', False)]);
    CodeSite.SendString('GetAxisNamesForChart ' + aFieldName, CommandText);
    Open;
    while not EOF do begin
      aStrList.Add(FieldByName(aFieldName).AsString);
      Next;
    end;
    Close;
    Result := True;
  except
    on e:Exception do
      CodeSite.SendError('GetAxisNamesForChart: ' + e.Message);
  end;
end;

//:---------------------------------------------------------------------------
function TdmLabReport.GetWhereStr(aAddWhere: String; aWithContainerWhere: Boolean): string;
  
  //NUE
  
begin
  with TStringList.Create do
  try
    if Trim(fDBParams.StyleIDs) <> '' then
      Add(Format(cWhereStyleId_lr, [fDBParams.StyleIDs]));
  
    if Trim(fDBParams.ProdIDs) <> '' then
      Add(Format(cWhereLotId_lr, [fDBParams.ProdIDs]));
  
    if Trim(fDBParams.MachIDs) <> '' then
      Add(Format(cWhereMachId_lr, [fDBParams.MachIDs]));
  
    if fDBParams.TimeMode <> tmLongtermWeek then begin
      if Trim(fDBParams.TimeIDs) <> '' then
        Add(Format(cWhereShiftId_lr, [fDBParams.TimeIDs]));
    end;
  //    if Trim(fDBParams.TimeIDs) <> '' then
  //      if fDBParams.MultiTimeRangeAllowed then
  //        Add(Format(cWhereShiftId_lr, [fDBParams.TimeIDs]))
  //      else  //Nue Longterm
  //        Add(cWhereShiftRange_lr[fDBParams.Timemode]);
  
  //@@@@@@@@@@@@ACHTUNG muss wieder aktiviert werden!!!!!!!!!!!!!!!!!  NUE
  //      if fDBParams.UseClearerSettings then
  //        if Trim(fDBParams.YMSetIDs) <> '' then
  //          Add(Format(cWhereYMSetId_lr, [fDBParams.YMSetIDs]));
  
    if aWithContainerWhere then begin
      CodeSite.SendString('DataItemWhere', mDataItemList.GetSQLWhere);
      Add(mDataItemList.GetSQLWhere);
    end;
  
    if aAddWhere <> '' then
      Add(aAddWhere);
  
    Result := Text;
  finally
    Free;
  end;
end;


initialization
  gDataItemPool := Nil;
finalization
  FreeAndNil(gDataItemPool);
end.












