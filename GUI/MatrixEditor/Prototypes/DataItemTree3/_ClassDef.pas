(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: ClassDef
| Projectpart...: Matrix Trend
| Subpart.......: -
| Process(es)...: LabReport, MatrixEditor, QOfflimit
| Description...: Klassen f�r die Definition und die Verwendung von Superklassen
| Info..........: -
| Develop.system: Windows 2000 SP3
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.01 / ModelMaker 6.20 1402
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
| 11.03.2003 1.00 Lok | Datei erstellt
|=============================================================================*)
unit ClassDef;

(*-----------------------------------------------------------
  Anwendung Dekorierer:
  Die �bliche Anwendung des Dekorierers besteht darin, dass in der Liste (TClassDefList, TClassDefContainer, ...) 
  nur DataItems einer bestimmten Klasse abgelegt werden. Diese Klasse sollte von 'TDataItemDecorator' erben.
  Der Dekorator "umh�llt" dann das eigentliche DataItem, das von 'TBaseDataItem' erbt.
  
  Das bedeutet, dass von Anfang an die Klassendefinitionen, die von der Datenbank geladen werden, dekoriert 
  werden m�ssen, da sonst ja verschiedene Klassen in der Liste abgelegt w�ren (Klassendefinitionen von der Datenbank
  werden immer als 'TClassDef' geladen). Aus diesem Grunde kann der Methode 'LoadFromDatabase' ein Dekorierer 
  mitgegeben werden. Damit werden die geladenen Klassendefinitionen in ein Objekt der Klasse 'TClassDef' 
  abgelegt. Dieses Objekt wird dann mit der Dekoriererklasse dekoriert, so dass ein Objekt der Klasse 'TClassDef' 
  in einem Objekt der Klasse 'TLabReportDataItem' eingebettet wird. 'TLabReportDataItem' ist in diesem Fall
  der Dekorierer.
  
  Von diesem Zeitpunkt an hat die Applikation kein Interesse mehr an der Tatsache dass die DataItems dekoriert sind.
  Es wird immer davon ausgegangen, dass ein DataItem der Klasse 'TLabReportDataItem' ein ganz gew�nliches DataItem
  ist. Das bedeutet, dass bei den diversen Hinzuf�ge Operationen der Nachfolger von 'TClassDefContainer' KEIN 
  Decorator mehr angegeben wird. Es soll ja lediglich ein DAtaItem kopiert werden, und nicht eine weitere
  "Dekorator-H�lle" darum gelegt werden.
  
  �blicherweise gibt es in einer Applikation nur zwei Orte an denen der Dekorierer ein Thema ist. Dies ist 
  einerseits die bereits oben erw�hnte Funktion 'LoadFromDataBase'. 
  Andererseits muss der Dekorator bei den Applikations Definierten DataItems (zB: '%Eff', 'Len', ...) 
  beachtet werden.
-------------------------------------------------------------*)

interface
uses
  SysUtils, YMParaDef, MMUGlobal, Classes, AdoDbAccess, QualityMatrixDef,contnrs;

type
  (* Tabellen Typ
     Jedes Item wird aus einer bestimmten Tabelle geholt. Hier sind alle 
     M�glichkeiten aufgelistet. *)
  TUsedTable = (utNone,
                utIntervalClassCut, utIntervalClassUncut, utIntervalSpliceCut,
                  utIntervalSpliceUncut, utIntervalFFCut, utIntervalFFUncut, 
                utShiftClassCut, utShiftClassUnCut, utShiftSpliceCut, 
                  utShiftSpliceUncut, utShiftFFCut, utShiftFFUncut, 
                utLongTermClassCut, utLongTermClassUncut, utLongTermSpliceCut, 
                  utLongTermSpliceUncut, utLongTermFFCut, utLongTermFFUncut);

  // Definition f�r das Konstantenarray
  TUsedTablesArray = array[TUsedTable] of string;
  //Set mit allen beteiligten Tabellen
  TUsedTablesSet = set of TUsedTable;

  // Typ des Namens der gesucht wird (zB: IndexOfName)
  TDataItemNameType = (ntDisplay, ntItem, ntField);
  
  // Typ f�r die Mathematischen Operationen (TCompositClassDef)
  TMathOperation = (moNone, moAdd, moSub, moMul, moDiv);
  TMathOperationArray = Array [TMathOperation] of string;
  
const
  (* Namen der Tabellen f�r den entsprechenden Tabellen Typ *)
  cUsedTablesArray: TUsedTablesArray = ('',
                                        '', '', '',
                                        '', '', '',
                                        't_dw_classcut', 't_dw_classuncut', 't_dw_splicecut',
                                        't_dw_spliceuncut', 't_dw_sirocutuncut', 't_dw_sirocutuncut',
                                        't_longterm_classcut', 't_longterm_classuncut', 't_longterm_splicecut',
                                        't_longterm_spliceuncut', 't_longterm_sirocutuncut', 't_longterm_sirocutuncut');  

  // Tabellen mit den geschnittenen Daten
  cCutTables = [utIntervalClassCut, utIntervalSpliceCut, utIntervalFFCut, utShiftClassCut, utShiftSpliceCut, 
                utShiftFFCut, utLongTermClassCut, utLongTermSpliceCut, utLongTermFFCut];
  // Tabelle mit den Ungeschnittenen Daten
  cUnCutTables = [utIntervalClassUncut, utIntervalSpliceUncut, utIntervalFFUncut, 
                  utShiftClassUnCut, utShiftSpliceUncut, utShiftFFUncut,
                  utLongTermClassUncut,utLongTermSpliceUncut,  utLongTermFFUncut];





  (* Alias Array f�r die Aliasse der Tabellen 
     Der Alias setzt sich aus verschiedenen Informationen zusammen.
     1) 'cd_'          = ClassDef
     2) 's' oder 'lt'  = Shift oder LongTerm
     3) 'c', 's', 'ff' = Class, Splice, Siro
     4) 'c' oder 'uc'  = ClassCut oder ClassUncut *)                                         
  cTableAlias: TUsedTablesArray =       ('',
                                         'cd_tmp', 'cd_tmp', 'cd_tmp',
                                         'cd_tmp', 'cd_tmp', 'cd_tmp',
                                         'cd_scc', 'cd_scuc', 'cd_ssc',
                                         'cd_ssuc', 'cd_sffc', 'cd_sffc',
                                         'cd_ltcc', 'cd_ltcuc', 'cd_ltsc',
                                         'cd_ltsuc', 'cd_ltffc', 'cd_ltffc');  
  
  // Zeichen f�r die mathematischen Operationen                                       
  cMathOperation: TMathOperationArray = ('', '+', '-', '*', '/');
type                                        
  TClassTypeGroup = (ctCustomDataItem, ctDataItem, ctLongThickThin, ctSplice, ctFF, ctLabData);
  TClassTypeGroupArray = array[TClassTypeGroup] of TClassTypeGroup;
  
const
  // Sortierreihenfolge f�r die Gruppen, wie sie im TreeView dargestellt werden
  cClassTypeGroupSorting : TClassTypeGroupArray = (ctDataItem, ctLabData, ctLongThickThin, ctSplice, ctFF, ctCustomDataItem);
  
type
  TClassDefects = (cdCut, cdUncut, cdBoth);

type // !!! WICHTIG !!! Dies muss der letzte Type vor der Klassendefinition sein.
  // Klassenreferenz (f�r Assign() in TClassDefList)
  TBaseDataItemClass = class of TBaseDataItem;
  TDataItemDecoratorClass = class of TDataItemDecorator;
  TBaseClassDefListClass  = class of TClassDefList;
  
// ------------------------------------------------------------------------
  TBaseDataItem = class;
  TDataItem = class;
  TClassDefList = class;
  TClassDef = class;
  TDataItemDecorator = class;
  TClassDefContainer = class;
  TGroupDataItem = class;
  //1 Wird gefeuert nachdem alle Items von der DB geladen wurden 
  TAfterDataItemLoad = procedure (aList: TClassDefList) of object;
  //1 Wird gefeuert, bevor die Items von der DB geladen werden 
  TBeforeDataItemsLoad = procedure (aList: TClassDefList; var aCancel: boolean) of object;
  //1 Wird gefeuert, wenn der Baum neu dargestellt wird 
  TSetAdditionalDataItems = procedure (Sender: TObject; aClassDefList: TClassDefList) of object;
  //1 Wird immer dann gefeuert, wenn ein DataItem ein Statement zusammensetzen soll 
  TGetDataItemGenerateSQL = procedure (Sender: TObject; aDataItem: TBaseDataItem; var aEnabled: boolean) of object;
// ------------------------------------------------------------------------

  (*: Klasse:        TBaseDataItem
      Vorg�nger:     TPersistent
      Kategorie:     No category
      Kurzbeschrieb: Basisklasse f�r die Datendefintionen 
      Beschreibung:  
                     Diese Klasse bildet die Basis f�r alle DataItems. 
                     
                     Die Liste kennt nur diese Basisklasse. Hier sind vor allem die 
                     Felder der Datenbank als Eigenschaften vorhanden. 
                     
                     Ausserdem gibt es einige Eigenschaften die von der Liste
                     f�r die Verwaltung ben�tigt werden. *)
  //1 Basisklasse f�r die Datendefintionen 
  TBaseDataItem = class (TPersistent)
  private
    FCategory: String;
    FClassDefects: TClassDefects;
    FClassTypeGroup: TClassTypeGroup;
    FDataItemType: TBaseDataItemClass;
    FDeleted: Boolean;
    FDescription: String;
    FDisplayName: String;
    FFieldName: String;
    FItemName: String;
    FLinkAlias: String;
    FModified: Boolean;
    FOrderPos: Integer;
    FPredefined: Boolean;
    FTimeMode: TTimeMode;
  protected
    //1 Gibt die Kategorie unter der die Definition erscheint zur�ck 
    function GetCategory: String; virtual;
    //1 Gibt zur�ck ob die Cut Daten abgefragt werden sollen  (false = Uncut) 
    function GetClassDefects: TClassDefects; virtual;
    //1 Gibt den Typ der vordefinierten Klasse (LongShortThin, Splice, FF, Imperfektionen...) zur�ck 
    function GetClassTypeGroup: TClassTypeGroup; virtual;
    //1 Gibt den Konfigurationsstring f�r das DataItem zur�ck 
    function GetConfigString: String; virtual;
    //1 Gibt den Datentyp des DataItems zur�ck (Beschreibung siehe Zugriffsmethoden) 
    function GetDataItemType: TBaseDataItemClass; virtual;
    //1 gib zur�ck ob das DataItem nicht mehr g�ltig ist 
    function GetDeleted: Boolean; virtual;
    //1 Gibt die Beschreibung des DataItems zur�ck 
    function GetDescription: String; virtual;
    //1 Zugriffsmethode f�r 'DisplayName' 
    function GetDisplayName: String; virtual;
    //1 Name des Feldes f�r die SQL Abfrage wenn das Query zusammengesetzt wird 
    function GetFieldName: String; virtual;
    //1 Zugriffsmethode f�r den Namen 
    function GetItemName: String; virtual;
    //1 Gibt den Alias f�r die Tabelle oder View mit der verkn�pft wird (zB: 'v') zur�ck 
    function GetLinkAlias: String; virtual;
    //1 Zugriffsmethode f�r Modified 
    function GetModified: Boolean; virtual;
    //1 Zugriffsmethode f�r die Order Position (Reihenfolge) 
    function GetOrderPos: Integer; virtual;
    //1 Zugriffsmethode f�r Predefined 
    function GetPredefined: Boolean; virtual;
    //1 Beinhaltet die Tabelle mit dem Tabellenalias von der die Daten gesammelt werden 
    function GetTableType: TUsedTablesSet; virtual;
    //1 Zugriffsmethode f�r TimeMode 
    function GetTimeMode: TTimeMode; virtual;
    //1 Setzt die Kategorie unter der die Definition erscheint 
    procedure SetCategory(Value: String); virtual;
    //1 Setzt ob die Cut Daten abgefragt werden sollen  (false = Uncut) 
    procedure SetClassDefects(Value: TClassDefects); virtual;
    //1 Setzt den Typ der vordefinierten Klasse (LongShortThin, Splice, FF, Imperfektionen...) 
    procedure SetClassTypeGroup(Value: TClassTypeGroup); virtual;
    //1 Setzt den Konfigurationsstring f�r das DataItem 
    procedure SetConfigString(Value: String); virtual;
    //1 Setz die Beschreibung des DataItems 
    procedure SetDescription(Value: String); virtual;
    //1 Zugriffsmethode f�r Display Name 
    procedure SetDisplayName(Value: String); virtual;
    //1 Name des Feldes f�r die SQL Abfrage wenn das Query zusammengesetzt wird 
    procedure SetFieldName(const Value: String); virtual;
    //1 Zugriffsmethode f�r den Namen 
    procedure SetItemName(const Value: String); virtual;
    //1 Setzt den Alias f�r die Tabelle oder View mit der verkn�pft wird (zB: 'v') 
    procedure SetLinkAlias(Value: String); virtual;
    //1 Zugriffsmethode f�r Modified 
    procedure SetModified(Value: Boolean); virtual;
    //1 Zugriffsmethode f�r die Order Position (Reihenfolge) 
    procedure SetOrderPos(Value: Integer); virtual;
    //1 Zugriffsmethode f�r Predefined 
    procedure SetPredefined(Value: Boolean); virtual;
    //1 Zugriffsmethode f�r TimeMode 
    procedure SetTimeMode(Value: TTimeMode); virtual;
  public
    //1 Konstruktor 
    constructor Create; virtual;
    //1 Kopiert dei Eigenscahften von Source 
    procedure Assign(Source: TPersistent); override;
    //1 Beinhaltet die Tabelle mit dem Tabellenalias von der die Daten gesammelt werden 
    function GetSQLFromTable: String; virtual;
    //1 Gibt die Spalten zur�ck die im SELECT Statement stehen m�ssen 
    function GetSQLSelectFields(aWithSum: boolean = true; aWithAlias:boolean = true): String; virtual;
    //1 Beinhaltet die Where-Klausel (Join) die die Felder betreffen 
    function GetSQLWhere: String; virtual;
    //1 Kategorie unter der die Definition erscheint 
    property Category: String read GetCategory write SetCategory;
    //1 True, wenn die Cut Daten abgefragt werden (false = Uncut) 
    property ClassDefects: TClassDefects read GetClassDefects write SetClassDefects;
    //1 Typ der vordefinierten Klasse (LongShortThin, Splice, FF, Imperfektionen...) 
    property ClassTypeGroup: TClassTypeGroup read GetClassTypeGroup write SetClassTypeGroup;
    //1 Configurationsstring im Stile einer Inidatei. Beim schreibenden Zugriff werden die entsprechenden Properties gesetzt 
    property ConfigString: String read GetConfigString write SetConfigString;
    //1 Gibt den Datentyp des DataItems zur�ck (Beschreibung siehe Zugriffsmethoden) 
    property DataItemType: TBaseDataItemClass read GetDataItemType write FDataItemType;
    //1 True, wenn das DataItem nicht mehr g�ltig ist 
    property Deleted: Boolean read GetDeleted;
    //1 Beschreibung des DataItems 
    property Description: String read GetDescription write SetDescription;
    //1 Name des DataItems der vom Benutzer festgelegt wird. Dieser Name wird intern nirgends verwendet 
    property DisplayName: String read GetDisplayName write SetDisplayName;
    //1 Name des Feldes f�r die SQL Abfrage wenn das Query zusammengesetzt wird 
    property FieldName: String read GetFieldName write SetFieldName;
    //1 Name der definierten Klasse der Angezeigt wird 
    property ItemName: String read GetItemName write SetItemName;
    //1 Alias f�r die Tabelle oder View mit der verkn�pft wird (zB: 'v') 
    property LinkAlias: String read GetLinkAlias write SetLinkAlias;
    //1 True, wenn das DAtaItem ver�ndert wurde 
    property Modified: Boolean read GetModified write SetModified;
    //1 Position in der vordefinierten Kategorie (Eigene Klassen nach Name sortiert) 
    property OrderPos: Integer read GetOrderPos write SetOrderPos;
    //1 True, wenn es sich um eine vordefinierte Klasse handelt 
    property Predefined: Boolean read GetPredefined write SetPredefined;
    //1 Beinhaltet die Tabelle mit dem Tabellenalias von der die Daten gesammelt werden 
    property TableType: TUsedTablesSet read GetTableType;
    //1 Zeitmodus (Schicht, Intervall, ...) 
    property TimeMode: TTimeMode read GetTimeMode write SetTimeMode;
  end;
  
  (*: Klasse:        TDataItem
      Vorg�nger:     TBaseDataItem
      Kategorie:     No category
      Kurzbeschrieb: Repr�sentiert ein Data Item 
      Beschreibung:  
                     Ein Data Item ist ein "normales" DataItem das sich nicht auf
                     einzelne Klassierfelder bezieht (Bsp: 'CYTot' oder '%EffP') *)
  //1 Repr�sentiert ein Data Item 
  TDataItem = class (TBaseDataItem)
  end;
  
  (*: Klasse:        TClassDefList
      Vorg�nger:     TObject
      Kategorie:     No category
      Kurzbeschrieb: Eine Liste von Klassendefinitionen 
      Beschreibung:  
                     Diese Liste verwaltet alle Klassendefinitionen die auf der 
                     Klassiermatrix basieren. Es existieren Methoden um die Daten
                     von der Datenbank zu lesen und auf diese zu schreiben. *)
  //1 Eine Liste von Klassendefinitionen 
  TClassDefList = class (TObject)
  private
    FOnAfterDataItemLoad: TAfterDataItemLoad;
    FOnBeforeDataItemsLoad: TBeforeDataItemsLoad;
    FReadOnly: Boolean;
  protected
    mDataItems: TList;
    mDB: TAdoDBAccess;
    mPersistent: Boolean;
    //1 Feuert den Event OnAfterDataItemLoad 
    procedure DoAfterDataItemLoad(aList: TClassDefList); virtual;
    //1 Feuert den Event OnBeforeDataItemsLoad 
    procedure DoBeforeDataItemsLoad(aList: TClassDefList; var aCancel: boolean); virtual;
    //1 Gibt das DataItem mit dem gew�nschten Namen zur�ck (sofern vorhanden) 
    function GetDataItemByName(aName: string): TBaseDataItem; virtual;
    //1 Holt die Anzahl der eingebetteten DataItems 
    function GetDataItemCount: Integer;
    //1 Gibt das DataItem mit dem gew�nschten Index zur�ck 
    function GetDataItems(aIndex: Integer): TBaseDataItem; virtual;
    //1 L�dt alle Klassendefinitionen von der Datenbank 
    procedure LoadFromDatabase(aDataItemDecoratorClass: TDataItemDecoratorClass = nil); virtual;
    //1 Speichert die Einstellungen in der Datenbank 
    procedure SaveToDatabase; virtual;
  public
    //1 Konstruktor 
    constructor Create(aPersistent: boolean = true); virtual;
    //1 Destruktor 
    destructor Destroy; override;
    //1 F�gt ein Element zur Liste hinzu (Nur eine Referenz auf ein bestehendes Objekt 
    function Add(aDataItem: TBaseDataItem): Integer; virtual;
    //1 Kopiert die Liste mit allen DataItems 
    procedure Assign(aSource: TClassDefList); virtual;
    //1 L�scht alle Klassendefinitionen 
    procedure Clear; virtual;
    //1 L�scht eine Klassendefinition 
    procedure Delete(aIndex: integer); virtual;
    //1 Gibt den Index des �bergebenen DataItems zur�ck (falls vorhanden) 
    function IndexOf(aBaseDataItem: TBaseDataItem): Integer;
    //1 Gibt den Index des DataItems mit dem entsprechenden 'DisplayName' zur�ck (falls vorhanden) 
    function IndexOfDisplayName(aDisplayName: string): Integer;
    //1 Gibt den Index des DataItems mit dem entsprechenden 'FieldName' zur�ck (falls vorhanden) 
    function IndexOfFieldName(aFieldName: string): Integer;
    //1 Gibt den Index des DataItems mit dem entsprechenden 'ItemName' zur�ck (falls vorhanden) 
    function IndexOfItemName(aItemName: string): Integer;
    //1 Gibt den Index des DataItems mit dem entsprechenden Namen zur�ck (falls vorhanden) 
    function IndexOfName(aName: string; aNameType: TDataItemNameType): Integer;
    //1 Sortiert die DataItems nach Kategorie 
    procedure Sort; virtual;
    //1 Gibt das DataItem mit dem gew�nschten Namen zur�ck (sofern vorhanden) 
    property DataItemByName[aName: string]: TBaseDataItem read GetDataItemByName;
    //1 Anzahl der DataItems 
    property DataItemCount: Integer read GetDataItemCount;
    //1 Einzelenes DataItem �ber den Index 
    property DataItems[aIndex: Integer]: TBaseDataItem read GetDataItems; default;
    //1 Wird gefeuert, wenn die Daten von der DB geladen wurden 
    property OnAfterDataItemLoad: TAfterDataItemLoad read FOnAfterDataItemLoad write FOnAfterDataItemLoad;
    //1 Wird gefeuert bevor die Data Items ab der DB geladen werden 
    property OnBeforeDataItemsLoad: TBeforeDataItemsLoad read FOnBeforeDataItemsLoad write FOnBeforeDataItemsLoad;
    //1 Wenn True, dann werden die Daten lediglich von der Datenbank gelesen 
    property ReadOnly: Boolean read FReadOnly write FReadOnly;
  end;
  
  (*: Klasse:        TClassDef
      Vorg�nger:     TBaseDataItem
      Kategorie:     No category
      Kurzbeschrieb: Definition einer Superklasse 
      Beschreibung:  
                     Jede Instanz dieser Klasse repr�sentiert eine zusammengefasste
                     Klassendefinition. Jede "Superklasse" besteht aus einem oder 
                     mehreren einzelnen, nicht notwendigerweise zusammenh�ngenden, Feldern
                     aus der Klassiermatrix. 
                     Die einzelnen Felder werden �ber ihre Position in der Klassiermatrix
                     identifiziert. Die Nummerierung der Felder folgt dabei dem Datenbankschema.
                     
                     Die Nummern Schema sieht wie folgt aus:
                     7   15   23   31   39   47   55   63
                     6   14   22   30   38   46   54   62
                     5   13   21   29   37   45   53   61 
                     4   12   20   28   36   44   52   60
                     3   11   19   27   35   43   51   59
                     2   10   18   26   34   42   50   58
                     1    9   17   25   33   41   49   57
                     0    8   16   24   32   40   48   56
                                        71   79   87   95  103  111  119  127
                                        70   78   86   94  102  110  118  126
                                        69   77   85   93  101  109  117  125
                                        68   76   84   92  100  108  116  124
                                        
                                        67   75   83   91   99  107  115  123
                                        66   74   82   90   98  106  114  122
                                        65   73   81   89   97  105  113  121
                                        64   72   80   88   96  104  112  120
                                        
                     Es kann aber auch die interne Darstellung der Klassiermatrix 
                     abgefragt werden. Dabei werden die einzelnen Felder zu einzelnen
                     Bytes zusammengefasst, wobei ein Bit in diesem Byte einem Feld entspricht.
                     Jedes Byte repr�sentiert eine horizontale Linie von Feldern. Das LSB ist das 
                     Feld das am weitesten Links steht. Als Beispiel die erste Reihe mit den
                     Feldern 0 - 56.
                     
                     Bitnummer:    0   1   2   3   4   5   6   7
                     Feldnummer:   0   8  16  24  32  40  48  56
                     
                     Die Bytes sind wie folgt nummeriert (Es wird nur jeweils
                     das Feld das dem LSB entspricht dargestellt):
                     Bytenummer:            0  1  2  3  4  5  6  7   8   9  10  11  12  13  14  15
                     Feldnummer das LSB:    0  1  2  3  4  5  6  7  64  65  66  67  68  69  70  71
                     
                     Beide Arten der Definition kann abgefragt werden:
                     Datenbank Schema mit der Eigenschaft 'MatrixDef: string'
                     Klassiermatrix mit der Eigenschaft 'ClassSettingsArr: TClassSettingsArr'
                     
                     --------------------------------------------
                     Es werden drei verschiedene Namen verwendet.
                     'ItemName'    ist der Name der in der DAtenbank gespeichert ist.
                     'FieldName'   ist der Name des Feldes in der Abfrage (evt. nummeriert bei gleichen  DataItems)
                     'DisplayName' ist der Name der in der Applikation definiert wird (vom Benutzer oder der 
                     Applikation) *)
  //1 Definition einer Superklasse 
  TClassDef = class (TBaseDataItem)
  private
    FClassSettingsArr: TClassClearSettingsArr;
    FID: Integer;
    FMatrixType: TMatrixType;
    FPersistent: Boolean;
  protected
    //1 Zugriffsmethode f�r MatrixDef 
    function GetMatrixDef: String; virtual;
    //1 Gibt eine Matrix zur�ck. Der String ist eine Komma separierte Liste mit den Klassen die gesetzt sind 
    function GetMatrixFormString(aMatrix: string): TClassClearSettingsArr;
    //1 Gibt den Typ der Tabelle zur�ck 
    function GetTableType: TUsedTablesSet; override;
    //1 Zugriffsmethode f�r das Class Settings Array 
    procedure SetClassSettingsArr(Value: TClassClearSettingsArr); virtual;
    //1 Zugriffsmethode f�r MatrixDef 
    procedure SetMatrixDef(Value: String); virtual;
    //1 Zugriffsmethode f�r den Typ der Matrix 
    procedure SetMatrixType(Value: TMatrixType); virtual;
  public
    //1 Konstruktor 
    constructor Create; override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Kopiert die Eigenschaften von Source 
    procedure Assign(Source: TPersistent); override;
    //1 L�scht eine Klassendefinition von der Datenbank 
    function DeleteFromDatabase(aNativeQuery: TNativeAdoQuery): Boolean;
    //1 Gibt einen Kommaseparierten String zur�ck der der �bergebenen Matrix entspricht 
    function GetMatrixFromArray(aArray: TClassClearSettingsArr): String;
    //1 Gibt den Namen der Tabelle zur�ck aus der die Daten geholt werden 
    function GetSQLFromTable: String; override;
    //1 Gibt die beteiligten Felder zur�ck 
    function GetSQLSelectFields(aWithSum: boolean = true; aWithAlias:boolean = true): String; override;
    //1 Speichert die Klassendefinition in der Datenbank 
    procedure Save(aNativeQuery: TNativeAdoQuery = nil);
    //1 Array mit den gesetzten Klassen. Jedes gesetzte Bit entspricht einer gesetzten Loepfe Klasse (Achtung! Nummerierung) 
    property ClassSettingsArr: TClassClearSettingsArr read FClassSettingsArr write SetClassSettingsArr;
    //1 ID des Datensatzes auf der Datenbank 
    property ID: Integer read FID write FID;
    //1 Definition der selektierten Felder (zB: '23, 31, 39, 47, 55, 56') 
    property MatrixDef: String read GetMatrixDef write SetMatrixDef;
    //1 Typ der Klassendefinition (Class, Splice, FF) 
    property MatrixType: TMatrixType read FMatrixType write SetMatrixType;
    //1 True, wenn die Klasse gespeichert werden soll 
    property Persistent: Boolean read FPersistent write FPersistent;
  end;
  
  (*: Klasse:        TDataItemDecorator
      Vorg�nger:     TBaseDataItem
      Kategorie:     No category
      Kurzbeschrieb: Basisklasse f�r alle Dekorierer 
      Beschreibung:  
                     Um die Bed�rfnisse der verschiedenen Anwendungen die 
                     dieses Framework verwenden abzudecken, steht ein 
                     Dekorierer(Decorator Pattern [1] Seite 199)zu Verf�gung. 
                     
                     Die verschiedenen Anwendungen definieren eigene Dekoriererklassen
                     die um die Eigenschaften die in diesen Anwendungen gew�nscht sind
                     erweitert werden. Da die BAsisklasse des Dekorierers von der selben
                     BAsisklasse erbt wie alle anderen DataItems, kann die Liste diese
                     Dekorierten Items aufnehmen und v�llig transparent verwenden.
                     
                     
                     [1] Gamma, Helm, Johanson, Vlissides
                         Addison-Wesley 1996
                         ISBN: 0-201-63361-2 *)
  //1 Basisklasse f�r alle Dekorierer 
  TDataItemDecorator = class (TBaseDataItem)
  protected
    FDataItem: TBaseDataItem;
    //1 Reicht die Abfrage an das eingebettete Objekt weiter 
    function GetCategory: String; override;
    //1 Reicht die Abfrage an das eingebettete Objekt weiter 
    function GetClassDefects: TClassDefects; override;
    //1 Reicht die Abfrage an das eingebettete Objekt weiter 
    function GetClassTypeGroup: TClassTypeGroup; override;
    //1 Reicht die Abfrage an das eingebettete Objekt weiter 
    function GetConfigString: String; override;
    //1 Holt den Typ der Dekorierten Klasse 
    function GetDataItemType: TBaseDataItemClass; override;
    //1 Reicht die Abfrage an das eingebettete Objekt weiter 
    function GetDeleted: Boolean; override;
    //1 Reicht die Abfrage an das eingebettete Objekt weiter 
    function GetDescription: String; override;
    //1 Reicht die Abfrage an das dekorierte DataItem weiter 
    function GetDisplayName: String; override;
    //1 Reicht die Abfrage an das eingebettete Objekt weiter 
    function GetFieldName: String; override;
    //1 Reicht die Abfrage an das eingebettete Objekt weiter 
    function GetItemName: String; override;
    //1 Reicht die Abfrage an das eingebettete Objekt weiter 
    function GetLinkAlias: String; override;
    //1 Reicht die Abfrage an das eingebettete Objekt weiter 
    function GetModified: Boolean; override;
    //1 Reicht die Abfrage an das eingebettete Objekt weiter 
    function GetOrderPos: Integer; override;
    //1 Reicht die Abfrage an das eingebettete Objekt weiter 
    function GetPredefined: Boolean; override;
    //1 Delegiert die Abfrage an das Eingebettete DataItem 
    function GetTableType: TUsedTablesSet; override;
    //1 Delegiert die Abfrage an das Eingebettete DataItem 
    function GetTimeMode: TTimeMode; override;
    //1 Setzt die Eigenschaft im eingebetteten Objekt 
    procedure SetCategory(Value: String); override;
    //1 Setzt die Eigenschaft im eingebetteten Objekt 
    procedure SetClassDefects(Value: TClassDefects); override;
    //1 Setzt die Eigenschaft im eingebetteten Objekt 
    procedure SetClassTypeGroup(Value: TClassTypeGroup); override;
    //1 Setzt die Eigenschaft im eingebetteten Objekt 
    procedure SetConfigString(Value: String); override;
    //1 Setzt die Eigenschaft im eingebetteten Objekt 
    procedure SetDescription(Value: String); override;
    //1 Setzt die Eigenschaft im eingebetten Objekt 
    procedure SetDisplayName(Value: String); override;
    //1 Setzt die Eigenschaft im eingebetteten Objekt 
    procedure SetFieldName(const Value: String); override;
    //1 Setzt die Eigenschaft im eingebetteten Objekt 
    procedure SetItemName(const Value: String); override;
    //1 Setzt die Eigenschaft im eingebetteten Objekt 
    procedure SetLinkAlias(Value: String); override;
    //1 Setzt die Eigenschaft im eingebetteten Objekt 
    procedure SetModified(Value: Boolean); override;
    //1 Setzt die Eigenschaft im eingebetteten Objekt 
    procedure SetOrderPos(Value: Integer); override;
    //1 Setzt die Eigenschaft im eingebetteten Objekt 
    procedure SetPredefined(Value: Boolean); override;
    //1 Delegiert die Abfrage an das Eingebettete DataItem 
    procedure SetTimeMode(Value: TTimeMode); override;
  public
    //1 Destruktor 
    destructor Destroy; override;
    //1 Kopiert die Eigenschaften von Source   
    procedure Assign(Source: TPersistent); override;
    //1 Kopiert sich selber auf Dest 
    procedure AssignTo(Dest: TPersistent); override;
    //1 Beinhaltet die Tabelle mit dem Tabellenalias von der die Daten gesammelt werden 
    function GetSQLFromTable: String; overload; override;
    //1 Beinhaltet alle Felder die f�r die Abfrage ben�tigt werden 
    function GetSQLSelectFields(aWithSum: boolean = true; aWithAlias:boolean = true): String; override;
    //1 Beinhaltet die Where-Klausel (Join) die die Felder betreffen 
    function GetSQLWhere: String; override;
    //1 Dekoriertes Objekt 
    property DataItem: TBaseDataItem read FDataItem write FDataItem;
  end;
  
  (*: Klasse:        TClassDefContainer
      Vorg�nger:     TClassDefList
      Kategorie:     No category
      Kurzbeschrieb: Liste f�r DataItems 
      Beschreibung:  
                     Deise Liste kann alle m�glichen Datenitems beinhalten.
                     Im Unterschied zur Vorg�ngerklasse werden beliebige
                     DataItems verwaltet. *)
  //1 Liste f�r DataItems 
  TClassDefContainer = class (TClassDefList)
  private
    FLinkAlias: String;
    FOnGetDataItemGenerateSQL: TGetDataItemGenerateSQL;
    FQueryParameters: TQueryParameters;
    FTempTableName: String;
    //1 Gibt die beteiligten Felder als Komma separierten string zur�ck 
    function GetFields(aCompleteFieldDef: boolean; aOnlyTClassDef: boolean = false): String;
    //1 Gibt das Query der TADODBAccess Instanz zur�ck 
    function GetQuery(aIndex: integer = 0): TNativeAdoQuery;
    //1 Fragt die Tabellen ab die f�r die  DataItems ben�tigt werden 
    function GetUsedTables: TUsedTablesSet;
    //1 Setzt den Alias der verlinkten Tabelle 
    procedure SetLinkAlias(Value: String);
    //1 Zugriffsmethode f�r QueryParameters 
    procedure SetQueryParameters(Value: TQueryParameters);
  protected
    //1 Dispatcher f�r OnGetDataItemGenarateSQL 
    procedure DoGetDataItemGenerateSQL(Sender: TObject; aDataItem: TBaseDataItem; var aEnabled: boolean); virtual;
    //1 Setzt f�r alle Elemente die Parameter die f�r das erzeugen des Query's ben�tigt werden 
    procedure SetDataItemParameters; virtual;
  public
    //1 Konstruktor 
    constructor Create(aPersistent: boolean = false); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 F�gt ein DataItem zum Container hinzu 
    procedure AddClassFromConfigString(aConfigString:string; aClassDefList: TClassDefList; aDecorator: 
      TDataItemDecoratorClass = nil); virtual;
    //1 F�gt ein neues DataItem hinzu 
    function AddCopy(aDataItem: string; aList: TClassDefList; aDecoratorClass: TDataItemDecoratorClass = nil): Integer; 
      reintroduce; overload; virtual;
    //1 F�gt ein neues DataItem hinzu 
    function AddCopy(aDataItem: TBaseDataItem; aDecoratorClass: TDataItemDecoratorClass = nil): Integer; reintroduce; 
      overload; virtual;
    //1 Kopiert die Eigenschaften von Source 
    procedure Assign(aSource: TClassDefList); override;
    //1 Erzeugt ein neues DataItem und f�gt es der Liste hinzu 
    function CreateAndAddNewDataItem(aDataItemClass: TBaseDataItemClass; aDecoratorClass: TDataItemDecoratorClass = 
      nil): Integer; virtual;
    //1 Erzeugt die tempor�re Tabelle 
    function CreateTempTable: String;
    //1 F�llt die tempor�re Tabelle mit den Intervall Daten 
    procedure FillTempTable;
    //1 Gibt die beteiligten Tabellen zur�ck, so dass der String direkt in einem Statement verwendet werden kann 
    function GetSQLFromTables: String; virtual;
    //1 Liefert die beteiligten Felder der Klassendefinition 
    function GetSQLSelectFields(aWithSum: boolean = true; aWithAlias: boolean = true): String; virtual;
    //1 Beinhaltet die Where-Klausel (Join) die die Felder betreffen 
    function GetSQLWhere: String; virtual;
    //1 Gibt einen eindeutigen Bezeichner f�r den �bergebnen Namen zur�ck 
    function GetUniqueFieldName(const aDisplayName: string): String;
    //1 L�dt die Classen Definitionen von der Datenbank 
    procedure LoadFromDatabase(aDataItemDecoratorClass: TDataItemDecoratorClass = nil); override;
    //1 Bereitet die Tabelle vor, wenn eine tempor�re Tabelle erzeugt werden muss 
    procedure PrepareTable;
    //1 Alias f�r die Tabelle oder View mit der verlinkt wird (Join) 
    property LinkAlias: String read FLinkAlias write SetLinkAlias;
    //1 Wird gefeuert, bevor das SQL Statement erzeugt wird  
    property OnGetDataItemGenerateSQL: TGetDataItemGenerateSQL read FOnGetDataItemGenerateSQL write 
      FOnGetDataItemGenerateSQL;
    //1 Parametersatz f�r die Abfrage 
    property QueryParameters: TQueryParameters read FQueryParameters write SetQueryParameters;
    //1 Name der tempor�ren TAbelle falls eine erzeugt wurde 
    property TempTableName: String read FTempTableName;
    //1 Ben�tigte Tabellen um die DataItems abzufragen 
    property UsedTables: TUsedTablesSet read GetUsedTables;
  end;
  
  (*: Klasse:        TIntervalQueryBuilder
      Vorg�nger:     TObject
      Kategorie:     No category
      Kurzbeschrieb: Klasse um das Query f�r die Abfrage der Intervalldaten zu bilden 
      Beschreibung:  
                     Die Intervalldaten m�ssen ausgepackt und in eine 
                     tempor�re Tabelle geschrieben werden. Damit nicht immer alle 
                     Daten ausgepackt werden m�ssen, sollen nur die Datens�tze kopiert
                     werden, die f�r eine Abfrage relevant sind. *)
  //1 Klasse um das Query f�r die Abfrage der Intervalldaten zu bilden 
  TIntervalQueryBuilder = class (TObject)
  private
    FQueryParameters: TQueryParameters;
    //1 Zugriffsmethode f�r QueryParameters 
    procedure SetQueryParameters(Value: TQueryParameters);
  protected
    //1 Gibt den From Teil des Query's zur�ck 
    function GetFrom: String;
    //1 Gibt den GroupBy Teil des Query's zur�ck 
    function GetGroupBy: String;
    //1 Gibt den Having Teil des Query's zur�ck 
    function GetHaving: String;
    //1 Gibt den Order By Teil des Query's zur�ck 
    function GetOrderBy: String;
    //1 Gibt den Select Teil des Query's zur�ck 
    function GetSelect: String;
    //1 Gibt den WHERE Teil des Statements zur�ck 
    function GetWhere: String;
  public
    //1 Konstruktor 
    constructor Create;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Gibt das komplete Query f�r die Intervalldaten zur�ck 
    procedure GetQueryText(aStringList: TStringList);
    //1 Parametersatz f�r die Abfrage 
    property QueryParameters: TQueryParameters read FQueryParameters write SetQueryParameters;
  end;
  
  (*: Klasse:        TGroupDataItem
      Vorg�nger:     TBaseDataItem
      Kategorie:     No category
      Kurzbeschrieb: Gruppierungsklasse 
      Beschreibung:  
                     Diese Klasse wird ben�tzt um bei der 
                     Visualisierung eine Gruppierung zu erm�glichen *)
  //1 Gruppierungsklasse 
  TGroupDataItem = class (TBaseDataItem)
  end;
  
  (*: Klasse:        TCompositClassDef
      Vorg�nger:     TClassDef
      Kategorie:     No category
      Kurzbeschrieb: Komposit Klasse f�r eine Sammlung von Datenelementen 
      Beschreibung:  
                     Verschiedene Datenelemente k�nnen miteinander 
                     verrechnet werden (Bsp. CyTot - CyFF).
                     Die Items werden nur mit einem String (ItemName) 
                     in eier StringList abgelegt. Die Operation mit dem 
                     n�chsten Item in der Liste, ist in der Eigenschaft 
                     "Objects[]" der StringList abgelegt. *)
  //1 Komposit Klasse f�r eine Sammlung von Datenelementen 
  TCompositClassDef = class (TClassDef)
  private
    FClassDefList: TClassDefList;
    FDataItemList: TStringList;
    //1 Zugriffsmethode f�r DataItemCount 
    function GetDataItemCount: Integer;
    //1 Zugriffsmethode f�r DataItemName[Index] 
    function GetDataItemName(Index: Integer): String;
    //1 Zugriffsmethode f�r MathOperation[Index] 
    function GetMathOperation(Index: Integer): TMathOperation;
    //1 Zugriffsmethode f�r DataItemName[Index] 
    procedure SetDataItemName(Index: Integer; const Value: String);
    //1 Zugriffsmethode f�r MathOperation[Index] 
    procedure SetMathOperation(Index: Integer; Value: TMathOperation);
  protected
    //1 Gibt die Operation(Typ) aus einem String zur�ck 
    function GetMathOpFromString(aOp: string): TMathOperation; virtual;
    //1 Zugriffsmethode f�r die Eigenschaft "MatrixDef" von TClassDef 
    function GetMatrixDef: String; override;
    //1 Gibt die Tabelle(n) zur�ck die beteiligt ist(sind) 
    function GetTableType: TUsedTablesSet; override;
    //1 Zugriffsmethode f�r die Eigenschaft "MatrixDef" von TClassDef 
    procedure SetMatrixDef(Value: String); reintroduce; override;
    //1 Setzt die notwendigen Properties, damit das Query korrekt gebildet werden kann 
    procedure SetRequiredProps(var aDataItem: TBaseDataItem); virtual;
  public
    //1 Konstruktor 
    constructor Create; override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Kopiert die Eigenschaften von Source 
    procedure Assign(Source: TPersistent); override;
    //1 Beinhaltet die Tabelle mit dem Tabellenalias von der die Daten gesammelt werden 
    function GetSQLFromTable: String; override;
    //1 Gibt die Spalten zur�ck die im SELECT Statement stehen m�ssen 
    function GetSQLSelectFields(aWithSum: boolean = true; aWithAlias:boolean = true): String; override;
    //1 Beinhaltet die Where-Klausel (Join) die die Felder betreffen 
    function GetSQLWhere: String; override;
    //1 Container aus dem die DataItems geholt werden (f�r das bilden des SQL Statement) 
    property ClassDefList: TClassDefList read FClassDefList write FClassDefList;
    //1 Anzahl der beinhalteten DataItems 
    property DataItemCount: Integer read GetDataItemCount;
    //1 Liste mit den DataItems (Im Array "Objects[]" ist die math. Operation abgelegt 
    property DataItemList: TStringList read FDataItemList;
    //1 Zugriff �ber einen Index auf die Namen der Elemente 
    property DataItemName[Index: Integer]: String read GetDataItemName write SetDataItemName;
    //1 Zugriff �ber einen Index auf die mathematischen Operationen der Elemente 
    property MathOperation[Index: Integer]: TMathOperation read GetMathOperation write SetMathOperation;
  end;
  

resourcestring
  rsDoubleItemCaption  = '(*)Doppeltes Daten Element';                    // ivlm
  rsDoubleItemPrompt   = '(*)Bitte geben Sie einen neuen Namen ein';      // ivlm
  rsDataItemNotDefined = '(*)Das Daten Element "%s" ist nicht verf�gbar'; //ivlm
  
implementation
uses
  LoepfeGlobal, mmcs, windows, DataLayerColDef, typInfo, mmStringList, Controls, Forms;

type
  TQueryFragmentOrder = (qfoSelect, qfoFrom, qfoWhere, qfoGroupBy, qfoHaving, qfoOrderBy);

  TKeyField = record
    name      : string;
    FieldType : string;
    Appendage : string
  end;// TKeyField = record
  
  TKeyFields = array[0..7] of TKeyField;
  
  TFieldType = (ftClassCut, ftClassUnCut, ftSpiceCut, ftSpliceUncut, ftSiroCut, ftSiroUnCut);
  
const
  cINVALID_USER_CLASS_ID = -1;

  cDropTable  = 'DROP TABLE ';


  cKeyFields: TKeyFields = ((name:'c_spindle_id';     FieldType:'TINYINT';  Appendage:'NULL'),
                            (name:'c_machine_id';     FieldType:'SMALLINT'; Appendage:'NULL'),
                            (name:'c_interval_id';    FieldType:'TINYINT';  Appendage:'NULL'),
                            (name:'c_prod_id';        FieldType:'INT';      Appendage:'NULL'),
                            (name:'c_style_id';       FieldType:'SMALLINT'; Appendage:'NULL'),
                            (name:'c_shiftcal_id';    FieldType:'TINYINT';  Appendage:'NULL'),
                            (name:'c_interval_start'; FieldType:'DATETIME'; Appendage:'NULL'),
                            (name:'c_YM_set_id';      FieldType:'SMALLINT'; Appendage:'NULL')
                           );
                           
  cFieldTypeStrings: Array [Low(TFieldType)..High(TFieldType)] of string = 
                                        ('c_cC', 'c_cU', 'c_spC', 'c_spU', 'c_siC', 'c_siU');
                           
  cClassContainerQueryCount = 2;   

  cFieldAlias   = ' as "%s"';
  
  cClassPrefix  = 'c_C';
  cSiroPrefix   = 'c_si';
  cSplicePrefix = 'c_sp';

  cCutPrefix    = 'C';
  cUnCutPrefix  = 'U';
(*----------------------------------------------------------------------------------
  DataItemSort gibt einen Wert kleiner Null zur�ck, wenn Item1 kleiner ist als Item2.
  Der R�ckgabewert ist 0, wenn beide Elemente gleich sind. Wenn Item1 gr��er
  als Item2 ist, ist der R�ckgabewert gr��er 0.
  Item1 = Item2  Result :=  0;
  Item1 > Item2  Result :=  1;
  Item1 < Item2  Result := -1;
------------------------------------------------------------------------------------*)
function DataItemSort(Item1, Item2: Pointer): Integer;
var
  xItem1: TBaseDataItem;
  xItem2: TBaseDataItem;
  // ------------------------ Vergleichsfunktionen ----------------------------------
  function ComparePredefined(aItem1, aItem2: TBaseDataItem): integer;
  begin
    result := 0;
    if (aItem1.Predefined = aItem2.Predefined) then
      Result :=  0;
    if (aItem1.Predefined > aItem2.Predefined) then
      Result :=  -1;
    if (aItem1.Predefined < aItem2.Predefined) then
      Result := 1;
  end;// function ComparePredefined(AItem1, aItem2: TBaseDataItem): integer;
  // --------------------------------------------------------------------------------
  
  function CompareCategory(aItem1, aItem2: TBaseDataItem): integer;
  begin
    result := AnsiCompareText(aItem1.Category, aItem2.Category);
    // Compare Text gibt nicht immer -1, 0 und 1 zur�ck
    result := result mod 2;
  end;// function CompareCategory(aItem1, aItem2: TBaseDataItem): integer;
  // --------------------------------------------------------------------------------
  
  function CompareClassTypeGroup(aItem1, aItem2: TBaseDataItem): integer;
  var
    xOrder1, xOrder2: integer;
    // --------------- Liefert die Sortierreihenfolge -------------------------------
    function GetSortIndexOfGroup(aGroup: TClassTypeGroup): integer;
    var
      i: TClassTypeGroup;
      xFound: boolean;
    begin
      // Die Sortierreihenfolge ist im Konstanntenarray 'cClassTypeGropup' festgelegt.
      result := ord(High(TClassTypeGroup)) + 1;
      
      i := Low(TClassTypeGroup);
      xFound := false;
      while (i <> High(TClassTypeGroup)) and (not(xFound)) do begin
        if (aGroup = cClassTypeGroupSorting[i]) then begin
          xFound := true;
          result := ord(i);
        end;// if (aGroup = cClassTypeGroupSorting[i]) then begin
        inc(i);
      end;// while (i <> High(TClassTypeGroup)) and (not(xFound)) do begin            
    end;// function GetSortIndexOfGroup(aGroup: TClassTypeGroup): integer;
  begin
    xOrder1 := GetSortIndexOfGroup(aItem1.ClassTypeGroup);
    xOrder2 := GetSortIndexOfGroup(aItem2.ClassTypeGroup);
    result := (xOrder1 - xOrder2);
  end;// function CompareClassTypeGroup(AItem1, aItem2: TBaseDataItem): integer;
  // --------------------------------------------------------------------------------
  
  function CompareOrderPos(aItem1, aItem2: TBaseDataItem): integer;
  begin
    result := (aItem1.OrderPos - aItem2.OrderPos);
  end;// function Compare(AItem1, aItem2: TBaseDataItem): integer;
  // --------------------------------------------------------------------------------
  
  function CompareClassName(aItem1, aItem2: TBaseDataItem): integer;
  begin
    result := AnsiCompareText(aItem1.ItemName, aItem2.ItemName);
    // Compare Text gibt nicht immer -1, 0 und 1 zur�ck
    result := result mod 2;
  end;// function CompareClassName(aItem1, aItem2: TBaseDataItem): integer;
  // --------------------------------------------------------------------------------
  
begin
//c_Predefined desc, c_Category, c_ClassTypeGroup, c_OrderPos, c_ClassName
  xItem1 := TBaseDataItem(Item1);
  xItem2 := TBaseDataItem(Item2);
  
  result := ComparePredefined(xItem1, xItem2);
  // Nur wenn ungleich muss weiter verglichen werden
  if result = 0 then begin
    result := CompareCategory(xItem1, xItem2);
    if result = 0 then begin
      result := CompareClassTypeGroup(xItem1, xItem2);
      if result = 0 then begin
        result := CompareOrderPos(xItem1, xItem2);
        if result = 0 then begin
          result := CompareClassName(xItem1, xItem2);
        end;// if result = 0 then begin (OrderPos)
      end;// if result = 0 then begin (ClassTypeGroup)
    end;// if result = 0 then begin (Category)
  end;// if result = 0 then begin (Predefined)
end;// function DataItemSort(Item1, Item2: Pointer): Integer;
  
//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TBaseDataItem.Create;
begin
  inherited Create;
end;// TBaseDataItem.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Kopiert dei Eigenscahften von Source
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TBaseDataItem.Assign(Source: TPersistent);
var
  xSource: TBaseDataItem;
begin
  if (Source is TBaseDataItem) then begin
    xSource := TBaseDataItem(Source);
    // Alle Eigenschaften kopieren
    Category       := xSource.Category;
    ClassTypeGroup := xSource.ClassTypeGroup;
    ClassDefects   := xSource.ClassDefects;
    FDeleted       := xSource.FDeleted;
    Description    := xSource.Description;
    LinkAlias      := xSource.LinkAlias;
    ItemName       := xSource.ItemName;
    OrderPos       := xSource.OrderPos;
    Predefined     := xSource.Predefined;
    FieldName      := xSource.FieldName;
    DisplayName    := xSource.DisplayName;
  
    (* Modified darf erst am Ende gesetzt werden, da das Flag bei jeder
       Zuweisung gesetzt wird.
       Dies muss auch in einem Nachfolger geschehen*)
    Modified         := xSource.Modified;
  end else begin
    // Weiterreichen
    inherited Assign(Source);
  end;// if (Source is TBaseDataItem) then begin
end;// TBaseDataItem.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetCategory
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die Kategorie unter der die Definition erscheint zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TBaseDataItem.GetCategory: String;
begin
  Result := FCategory;
end;// TBaseDataItem.GetCategory cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetClassDefects
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt zur�ck ob die Cut Daten abgefragt werden sollen  (false = Uncut)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TBaseDataItem.GetClassDefects: TClassDefects;
begin
  Result := FClassDefects;
end;// TBaseDataItem.GetClassDefects cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetClassTypeGroup
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt den Typ der vordefinierten Klasse (LongShortThin, Splice, FF, Imperfektionen...) zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TBaseDataItem.GetClassTypeGroup: TClassTypeGroup;
begin
  Result := FClassTypeGroup;
end;// TBaseDataItem.GetClassTypeGroup cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetConfigString
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt den Konfigurationsstring f�r das DataItem zur�ck
 *  Beschreibung:     
                      Der Konfigurationsstring baut sich wie eine INI-Datei auf.
                      Es wird eine Stringliste aufgebaut in der jedes Property
                      das persistent sein soll im Format 'PROPERTY=WERT' abgespeichert 
                      ist. Das Resultat ist dann die Eigenschaft 'CommaText' der 
                      Stringliste.
 --------------------------------------------------------------------*)
function TBaseDataItem.GetConfigString: String;
begin
  result := '';

//: ----------------------------------------------
  with TIniStringList.Create do try
    Values['ItemName'] := ItemName;
    Values['DisplayName'] := DisplayName;
    Values['ClassDefects'] := ClassDefects;
  
    result := CommaText;
  finally
    free;
  end;// with TIniStringList do try
end;// TBaseDataItem.GetConfigString cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetDataItemType
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt den Datentyp des DataItems zur�ck (Beschreibung siehe Zugriffsmethoden)
 *  Beschreibung:     
                      Diese Eigenschaft gilt vor allem f�r Dekorierte Items. Der Dekorierer
                      tritt gegen aussen als 'TDataItemDecorator' oder als einer seiner 
                      Nachfahren auf. Es kann aber notwendig sein, zu wissen welcer Datentyp
                      dekoriert wurde.
                      
                      Im Normalfall wird eine Anwendung mit dekorierten Items arbeiten. Es k�nnte
                      aber F�lle geben wo mit verschiedenen DataItems gearbeitet werden muss. In diesem 
                      Fall soll eine einfache Abfrage des dekorierten Datentyps m�glich sein, ohne dass 
                      die Verschachtelungstiefe des Dekorierers bekannt sein muss.
 --------------------------------------------------------------------*)
function TBaseDataItem.GetDataItemType: TBaseDataItemClass;
begin
  result := TBaseDataItemClass(ClassType);
end;// TBaseDataItem.GetDataItemType cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetDeleted
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: gib zur�ck ob das DataItem nicht mehr g�ltig ist
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TBaseDataItem.GetDeleted: Boolean;
begin
  Result := FDeleted;
end;// TBaseDataItem.GetDeleted cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetDescription
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die Beschreibung des DataItems zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TBaseDataItem.GetDescription: String;
begin
  Result := FDescription;
end;// TBaseDataItem.GetDescription cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetDisplayName
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r 'DisplayName'
 *  Beschreibung:     
                      Name des DataItems der vom Benutzer festgelegt wird. Dieser 
                      Name wird intern nirgends verwendet.
 --------------------------------------------------------------------*)
function TBaseDataItem.GetDisplayName: String;
begin
  Result := FDisplayName;
end;// TBaseDataItem.GetDisplayName cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetFieldName
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Name des Feldes f�r die SQL Abfrage wenn das Query zusammengesetzt wird
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TBaseDataItem.GetFieldName: String;
begin
  Result := FFieldName;
end;// TBaseDataItem.GetFieldName cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetItemName
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r den Namen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TBaseDataItem.GetItemName: String;
begin
  Result := FItemName;
end;// TBaseDataItem.GetItemName cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetLinkAlias
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt den Alias f�r die Tabelle oder View mit der verkn�pft wird (zB: 'v') zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TBaseDataItem.GetLinkAlias: String;
begin
  Result := FLinkAlias;
end;// TBaseDataItem.GetLinkAlias cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetModified
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r Modified
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TBaseDataItem.GetModified: Boolean;
begin
  Result := FModified;
end;// TBaseDataItem.GetModified cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetOrderPos
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r die Order Position (Reihenfolge)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TBaseDataItem.GetOrderPos: Integer;
begin
  Result := FOrderPos;
end;// TBaseDataItem.GetOrderPos cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetPredefined
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r Predefined
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TBaseDataItem.GetPredefined: Boolean;
begin
  Result := FPredefined;
end;// TBaseDataItem.GetPredefined cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetSQLFromTable
 *  Klasse:           TBaseDataItem
 *  Kategorie:        Datenbank 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Beinhaltet die Tabelle mit dem Tabellenalias von der die Daten gesammelt werden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TBaseDataItem.GetSQLFromTable: String;
begin
  result := '';
end;// TBaseDataItem.GetSQLFromTable cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           GetSQLSelectFields
 *  Klasse:           TBaseDataItem
 *  Kategorie:        Datenbank 
 *  Argumente:        (aWithSum, aWithAlias)
 *
 *  Kurzbeschreibung: Gibt die Spalten zur�ck die im SELECT Statement stehen m�ssen
 *  Beschreibung:     
                      Mit dem Argument kann bestimmt werden ob die Felder f�r 
                      eine Gruppierung summiert werden ('sum(c_cC1 * 1.0) as c_cC1, sum(c_cC2 * 1.0) as c_cC2')
                      oder nicht ('c_cC1, c_cC2').
 --------------------------------------------------------------------*)
function TBaseDataItem.GetSQLSelectFields(aWithSum: boolean = true; aWithAlias:boolean = true): String;
begin
  result := '';
end;// TBaseDataItem.GetSQLSelectFields cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           GetSQLWhere
 *  Klasse:           TBaseDataItem
 *  Kategorie:        Datenbank 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Beinhaltet die Where-Klausel (Join) die die Felder betreffen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TBaseDataItem.GetSQLWhere: String;
begin
  result := '';
end;// TBaseDataItem.GetSQLWhere cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           GetTableType
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Beinhaltet die Tabelle mit dem Tabellenalias von der die Daten gesammelt werden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TBaseDataItem.GetTableType: TUsedTablesSet;
begin
  result := [];
end;// TBaseDataItem.GetTableType cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetTimeMode
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r TimeMode
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TBaseDataItem.GetTimeMode: TTimeMode;
begin
  Result := FTimeMode;
end;// TBaseDataItem.GetTimeMode cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetCategory
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Kategorie unter der die Definition erscheint
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TBaseDataItem.SetCategory(Value: String);
begin
  FCategory := Value;
end;// TBaseDataItem.SetCategory cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetClassDefects
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt ob die Cut Daten abgefragt werden sollen  (false = Uncut)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TBaseDataItem.SetClassDefects(Value: TClassDefects);
begin
  FClassDefects := Value;
end;// TBaseDataItem.SetClassDefects cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetClassTypeGroup
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt den Typ der vordefinierten Klasse (LongShortThin, Splice, FF, Imperfektionen...)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TBaseDataItem.SetClassTypeGroup(Value: TClassTypeGroup);
begin
  FClassTypeGroup := Value;
end;// TBaseDataItem.SetClassTypeGroup cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetConfigString
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt den Konfigurationsstring f�r das DataItem
 *  Beschreibung:     
                      Der Konfigurationsstring baut sich wie eine INI-Datei auf.
                      Es wird eine Stringliste aufgebaut in der jedes Property
                      das persistent sein soll im Format 'PROPERTY=WERT' abgespeichert 
                      ist. Das Resultat ist dann die Eigenschaft 'CommaText' der 
                      Stringliste.
                      
                      Hier wird einfach abgefragt ob ein bestimmtes Value existiert,
                      und welchen Wert es hat. Dieser WErt wird f�r das entsprechende
                      Property gesetzt.
 --------------------------------------------------------------------*)
procedure TBaseDataItem.SetConfigString(Value: String);
begin
  with TIniStringList.Create do try
    commaText := Value;
  
    DisplayName := Values['DisplayName'];
    ClassDefects := ValueDef('ClassDefects',0);
  finally
    free;
  end;// with TIniStringList do try
end;// TBaseDataItem.SetConfigString cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetDescription
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setz die Beschreibung des DataItems
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TBaseDataItem.SetDescription(Value: String);
begin
  FDescription := Value;
end;// TBaseDataItem.SetDescription cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetDisplayName
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r Display Name
 *  Beschreibung:     
                      Name des DataItems der vom Benutzer festgelegt wird. Dieser 
                      Name wird intern nirgends verwendet.
 --------------------------------------------------------------------*)
procedure TBaseDataItem.SetDisplayName(Value: String);
begin
  FDisplayName := Value;
end;// TBaseDataItem.SetDisplayName cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetFieldName
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Name des Feldes f�r die SQL Abfrage wenn das Query zusammengesetzt wird
 *  Beschreibung:     
                      Beim Setzen des Namens, wird das Modified Flag gesetzt.
 --------------------------------------------------------------------*)
procedure TBaseDataItem.SetFieldName(const Value: String);
begin
  FFieldName := Value;
end;// TBaseDataItem.SetFieldName cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetItemName
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r den Namen
 *  Beschreibung:     
                      Beim Setzen des Namens, wird das Modified Flag gesetzt.
 --------------------------------------------------------------------*)
procedure TBaseDataItem.SetItemName(const Value: String);
begin
  FItemName := Value;

//: ----------------------------------------------
  SetModified(true);

//: ----------------------------------------------
  (* FieldName ist im Normalfall immer identisch mit DisplayName.
     Eine Ausnahme ist es, wenn in einem Container 2 oder mehr
     Items mit dem selben DisplayName "residieren". Dann muss f�r
     die Unterscheidung der FieldName nummeriert werden *)
  FieldName := ItemName;
  (* Der DisplayName ist der Name der in der Anwendung verwendet werden kann.
     Der DisplayName wird intern nicht verwendet. *)
  DisplayName := ItemName;
end;// TBaseDataItem.SetItemName cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetLinkAlias
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt den Alias f�r die Tabelle oder View mit der verkn�pft wird (zB: 'v')
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TBaseDataItem.SetLinkAlias(Value: String);
begin
  FLinkAlias := Value;
end;// TBaseDataItem.SetLinkAlias cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetModified
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r Modified
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TBaseDataItem.SetModified(Value: Boolean);
begin
  FModified := Value;
end;// TBaseDataItem.SetModified cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetOrderPos
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r die Order Position (Reihenfolge)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TBaseDataItem.SetOrderPos(Value: Integer);
begin
  FOrderPos := Value;
end;// TBaseDataItem.SetOrderPos cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetPredefined
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r Predefined
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TBaseDataItem.SetPredefined(Value: Boolean);
begin
  FPredefined := Value;
end;// TBaseDataItem.SetPredefined cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetTimeMode
 *  Klasse:           TBaseDataItem
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r TimeMode
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TBaseDataItem.SetTimeMode(Value: TTimeMode);
begin
  if FTimeMode <> Value then
  begin

//: ----------------------------------------------
    FTimeMode := Value;

//: ----------------------------------------------
  end;
end;// TBaseDataItem.SetTimeMode cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        (aPersistent)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      Erzeugt die aggregierte Liste.
                      Die Items werden von der Datenbank geladen, wenn 'aPersistent'
                      true ist. 
                      Abgeleitete Klassen k�nnen dieses Verhalten �ndern, indem
                      der Konstruktor mit false aufgerufen wird.
 --------------------------------------------------------------------*)
constructor TClassDefList.Create(aPersistent: boolean = true);
begin
  CodeSite.SendNote('ClassDefList.Create');

//: ----------------------------------------------
  inherited Create;

//: ----------------------------------------------
  mDataItems := TList.Create;
  mDB := TAdoDBAccess.Create(cClassContainerQueryCount);

//: ----------------------------------------------
  // Wenn True, dann werden die Daten im Destruktor gespeichert (nur wenn ReadOnly = true)
  mPersistent := aPersistent;
  
  // Eine Anwendung muss ReadOnly implizit ausschalten um Daten zu speichern
  FReadOnly := true;
  
  // Einstellungen laden
  if mPersistent then
    LoadFromDatabase;
end;// TClassDefList.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      Gibt die aggregierte Liste wieder frei.
 --------------------------------------------------------------------*)
destructor TClassDefList.Destroy;
begin
  CodeSite.SendNote('ClassDefList.Destroy');

//: ----------------------------------------------
  // Speichern nur, wenn definiert
  if (mPersistent) and (not(FReadOnly)) then
    SaveToDatabase;

//: ----------------------------------------------
  // Objekte freigeben
  Clear;

//: ----------------------------------------------
  FreeAndNil(mDataItems);
  FreeAndNil(mDB);

//: ----------------------------------------------
  inherited Destroy;
end;// TClassDefList.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           Add
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        (aDataItem)
 *
 *  Kurzbeschreibung: F�gt ein Element zur Liste hinzu (Nur eine Referenz auf ein bestehendes Objekt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TClassDefList.Add(aDataItem: TBaseDataItem): Integer;
begin
  result := -1;

//: ----------------------------------------------
  (* Hier wird nur der Pointer auf das Item gespeichert.
     Die Items sind aber dennoch Eigentum der Liste und
     werden beim L�schen freigegeben *)
  if assigned(mDataItems) then
    result := mDataItems.Add(aDataItem);
end;// TClassDefList.Add cat:No category

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        (aSource)
 *
 *  Kurzbeschreibung: Kopiert die Liste mit allen DataItems
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TClassDefList.Assign(aSource: TClassDefList);
var
  xDataItem: TBaseDataItem;
  xClass: TBaseDataItemClass;
  i: Integer;
begin
  if (aSource is TClassDefList) then begin
    // Alte Liste l�schen
    Clear;
    // Eigenschaften �bertragen
    FReadOnly := aSource.ReadOnly;
  
    for i := 0 to aSource.DataItemCount - 1 do begin
     (* Es k�nnen verschiedene Nachfahren von TBaseDataItem in der Liste sein
        deshalb muss eine Klassenreferenz geholt werden *)
      // Kein Decorator
      xClass := TBaseDataItemClass(aSource.DataItems[i].ClassType);
      // Mit der Klassnreferenz ein neues DataItem erzeugen
      xDataItem := xClass.Create;
      // DataItem kopieren
      xDataItem.Assign(aSource.DataItems[i]);
      // DataItem ganz normal zur Liste hinzuf�gen
      Add(xDataItem);
    end;// for i := 0 to DataItemCount - 1 do begin
  end;// if (aSource is TClassDefContainer) then begin
end;// TClassDefList.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           Clear
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�scht alle Klassendefinitionen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TClassDefList.Clear;
begin
  // Solange l�schen bis kein Element mehr �brig ist
  while DataItemCount > 0 do
    delete(0);
end;// TClassDefList.Clear cat:No category

//:-------------------------------------------------------------------
(*: Member:           Delete
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: L�scht eine Klassendefinition
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TClassDefList.Delete(aIndex: integer);
begin
  // Das gew�nschte Item wird vor dem L�schen freigegeben
  if assigned(mDataItems) then begin
    if aIndex < mDataItems.Count then begin
      DataItems[aIndex].Free;
      mDataItems.delete(aIndex);
    end;// if aIndex < mDataItems.Count then begin
  end;// if assigned(mDataItems) then begin
end;// TClassDefList.Delete cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoAfterDataItemLoad
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        (aList)
 *
 *  Kurzbeschreibung: Feuert den Event OnAfterDataItemLoad
 *  Beschreibung:     
                      Dieser Event wird ausgel�st, nachdem die Daten von der 
                      Datenbank gelesen wurden. 
 --------------------------------------------------------------------*)
procedure TClassDefList.DoAfterDataItemLoad(aList: TClassDefList);
begin
  if Assigned(FOnAfterDataItemLoad) then FOnAfterDataItemLoad(aList);
end;// TClassDefList.DoAfterDataItemLoad cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoBeforeDataItemsLoad
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        (aList, aCancel)
 *
 *  Kurzbeschreibung: Feuert den Event OnBeforeDataItemsLoad
 *  Beschreibung:     
                      Wird ausgel�st, bevor die Daten von der 
                      Datenbank gelesen werden.
 --------------------------------------------------------------------*)
procedure TClassDefList.DoBeforeDataItemsLoad(aList: TClassDefList; var aCancel: boolean);
begin
  if Assigned(FOnBeforeDataItemsLoad) then FOnBeforeDataItemsLoad(aList, aCancel);
end;// TClassDefList.DoBeforeDataItemsLoad cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetDataItemByName
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        (aName)
 *
 *  Kurzbeschreibung: Gibt das DataItem mit dem gew�nschten Namen zur�ck (sofern vorhanden)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TClassDefList.GetDataItemByName(aName: string): TBaseDataItem;
var
  i: Integer;
begin
  result := nil;
  i := 0;

//: ----------------------------------------------
  // Klassendefinition mit dem gew�nschten Namen in der Liste suchen
  while (i < mDataItems.Count) and (result = nil) do begin
    if AnsiSameText(TBaseDataItem(mDataItems.Items[i]).ItemName, aName) then
      result := TBaseDataItem(mDataItems.Items[i]);
    inc(i);
  end;// while (i < mDataItems.Count) and (result = nil) do begin
end;// TClassDefList.GetDataItemByName cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetDataItemCount
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Holt die Anzahl der eingebetteten DataItems
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TClassDefList.GetDataItemCount: Integer;
begin
  result := 0;

//: ----------------------------------------------
  // Fragt die Anzahl bei der aggregierten Liste nach
  if assigned(mDataItems) then
    result := mDataItems.Count;
end;// TClassDefList.GetDataItemCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetDataItems
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: Gibt das DataItem mit dem gew�nschten Index zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TClassDefList.GetDataItems(aIndex: Integer): TBaseDataItem;
begin
  result := nil;

//: ----------------------------------------------
  // Holt das gew�nschte Data Item bei der aggregierten Liste ab
  if assigned(mDataItems) then
    if mDataItems.Count > aIndex then
      result := TBaseDataItem(mDataItems.Items[aIndex]);
end;// TClassDefList.GetDataItems cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOf
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        (aBaseDataItem)
 *
 *  Kurzbeschreibung: Gibt den Index des �bergebenen DataItems zur�ck (falls vorhanden)
 *  Beschreibung:     
                      Gibt f�r nicht vorhandene DataItems -1 zur�ck.
 --------------------------------------------------------------------*)
function TClassDefList.IndexOf(aBaseDataItem: TBaseDataItem): Integer;
begin
  result := -1;

//: ----------------------------------------------
  if assigned(mDataItems) then
    result := mDataItems.IndexOf(aBaseDataItem);
end;// TClassDefList.IndexOf cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOfDisplayName
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        (aDisplayName)
 *
 *  Kurzbeschreibung: Gibt den Index des DataItems mit dem entsprechenden 'DisplayName' zur�ck (falls vorhanden)
 *  Beschreibung:     
                      Gibt f�r nicht vorhandene DataItems -1 zur�ck.
 --------------------------------------------------------------------*)
function TClassDefList.IndexOfDisplayName(aDisplayName: string): Integer;
begin
  result := -1;

//: ----------------------------------------------
  if assigned(mDataItems) then
    result := IndexOfName(aDisplayName, ntDisplay);
end;// TClassDefList.IndexOfDisplayName cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOfFieldName
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        (aFieldName)
 *
 *  Kurzbeschreibung: Gibt den Index des DataItems mit dem entsprechenden 'FieldName' zur�ck (falls vorhanden)
 *  Beschreibung:     
                      Gibt f�r nicht vorhandene DataItems -1 zur�ck.
 --------------------------------------------------------------------*)
function TClassDefList.IndexOfFieldName(aFieldName: string): Integer;
begin
  result := -1;

//: ----------------------------------------------
  if assigned(mDataItems) then
    result := IndexOfName(aFieldName, ntField);
end;// TClassDefList.IndexOfFieldName cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOfItemName
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        (aItemName)
 *
 *  Kurzbeschreibung: Gibt den Index des DataItems mit dem entsprechenden 'ItemName' zur�ck (falls vorhanden)
 *  Beschreibung:     
                      Gibt f�r nicht vorhandene DataItems -1 zur�ck.
 --------------------------------------------------------------------*)
function TClassDefList.IndexOfItemName(aItemName: string): Integer;
begin
  result := -1;

//: ----------------------------------------------
  if assigned(mDataItems) then
    result := IndexOfName(aItemName, ntItem);
end;// TClassDefList.IndexOfItemName cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOfName
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        (aName, aNameType)
 *
 *  Kurzbeschreibung: Gibt den Index des DataItems mit dem entsprechenden Namen zur�ck (falls vorhanden)
 *  Beschreibung:     
                      Gibt f�r nicht vorhandene DataItems -1 zur�ck.
 --------------------------------------------------------------------*)
function TClassDefList.IndexOfName(aName: string; aNameType: TDataItemNameType): Integer;
var
  xIndex: Integer;
  xFound: Boolean;
begin
  result := -1;

//: ----------------------------------------------
  if assigned(mDataItems) then begin
    xIndex := 0;
    xFound := false;
  
    while (xIndex < mDataItems.Count) and (not(xFound)) do begin
      case aNameType of
        ntDisplay : xFound := AnsiSameText(aName, DataItems[xIndex].DisplayName);
        ntItem    : xFound := AnsiSameText(aName, DataItems[xIndex].ItemName);
        ntField   : xFound := AnsiSameText(aName, DataItems[xIndex].FieldName);
        else
          raise Exception.CreateFMT('TClassDefList.IndexOfName: NameType (%s) not implemented',[GetEnumName(TypeInfo(TDataItemNameType),ord(aNameType))]);
      end;// case aNameType of
  
      // Weitersuchen, oder Resultat sichern
      if not(xFound) then
        inc(xIndex)
      else
        result := xIndex;
    end;// while (xIndex < mDataItems.Count) and (not(xFound)) do begin
  end;// if assigned(mDataItems) then begin
end;// TClassDefList.IndexOfName cat:No category

//:-------------------------------------------------------------------
(*: Member:           LoadFromDatabase
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        (aDataItemDecoratorClass)
 *
 *  Kurzbeschreibung: L�dt alle Klassendefinitionen von der Datenbank
 *  Beschreibung:     
                      Diese Funktion wird vom Konstruktor aufgerufen (mPersistent = true).
 --------------------------------------------------------------------*)
procedure TClassDefList.LoadFromDatabase(aDataItemDecoratorClass: TDataItemDecoratorClass = nil);
var
  xClassDef: TClassDef;
  xDataItemDecorator: TDataItemDecorator;
  xCancel: boolean;
  
  const
    cLoadClassDef ='SELECT c_UserClass_id, c_ClassName, c_Predefined, c_Matrix_Type, ' +
                   '       c_Matrix_Def, c_Category, c_ClassTypeGroup, c_OrderPos, c_Description ' +
                   'FROM t_UserClass_Def ' +
                   'ORDER BY c_Predefined desc, c_Category, c_ClassTypeGroup, c_OrderPos, c_ClassName';
  
begin
  // Die Datenbankschnittstelle wird erst initialisiert wenn der Zugriff ben�tigt wird
  if not(mDB.Initialized) then
    mDB.Init;

//: ----------------------------------------------
  xCancel := false;
  // Event feuern
  DoBeforeDataItemsLoad(self, xCancel);
  if not(xCancel) then begin

//: ----------------------------------------------
    with mDB.Query[0] do begin
      // Liest alle Klassendefinitionen von der Datenbank
      SQL.Text := cLoadClassDef;
      Open;
      try
        // Jeder Datensatz entspricht einer Klassendefinition
        while not(eof) do begin
          // Wenn der Typ undefiniert ist, dann ist es eine zusammengesetzte Klasse (z.B.: 'CYTot - CFF')
          if TMatrixType(FieldByName('c_Matrix_Type').AsInteger) = mtNone then begin
            xClassDef := TCompositClassDef.Create;
            TCompositClassDef(xClassDef).ClassDefList := self;
          end else begin
            xClassDef := TClassDef.Create;
          end;// if TMatrixType(FieldByName('c_Matrix_Type').AsInteger) = mtNone then begin
  
          // Felder abf�llen
          xClassDef.ID             := FieldByName('c_UserClass_id').AsInteger;
          xClassDef.ItemName       := FieldByName('c_ClassName').AsString;
          xClassDef.DisplayName    := xClassDef.ItemName;
          xClassDef.Predefined     := boolean(FieldByName('c_Predefined').AsInteger);
          xClassDef.MatrixType     := TMatrixType(FieldByName('c_Matrix_Type').AsInteger);
          xClassDef.MatrixDef      := FieldByName('c_Matrix_Def').AsString;
          xClassDef.Category       := FieldByName('c_Category').AsString;
          xClassDef.ClassTypeGroup := TClassTypeGroup(FieldByName('c_ClassTypeGroup').AsInteger);
          xClassDef.OrderPos       := FieldByName('c_OrderPos').AsInteger;
          xClassDef.Description    := FieldByName('c_Description').AsString;
  
          // Da die Definition von der Datenbank geladen wurde, ist sie aktuell
          xClassDef.Modified := false;
  
          (* Wird eine Dekoratorklasse �bergeben, dann wird zuerst
             eine Dekoratorklasse der gew�nschten Art erzeugt. Danach
             wird die Klassendefinition dem Dekorator hinzugef�gt. Der
             Dekorator landet schlussendlich in der Liste.
             Wird keine Dekoratorklasse �bergeben, dann landet die
             Klassendefinition direkt in der Liste *)
          if aDataItemDecoratorClass = nil then begin
            Add(xClassDef);
          end else begin
            xDataItemDecorator := aDataItemDecoratorClass.Create;
            xDataItemDecorator.DataItem := xClassDef;
            Add(xDataItemDecorator);
          end;// if aDataItemDecoratorClass = nil then begin
  
          // N�chsten Datensatz lesen
          next;
        end;// while not(eof) do begin
      finally
        Close;
      end;// try finally
    end;// with mDB.Query[0] do begin

//: ----------------------------------------------
    // Event feuern
    DoAfterDataItemLoad(self);

//: ----------------------------------------------
  end;// if not(xCancel) then begin
end;// TClassDefList.LoadFromDatabase cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveToDatabase
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Speichert die Einstellungen in der Datenbank
 *  Beschreibung:     
                      Diese Funktion wird vom Destruktor aufgerufen ((mPersistent = true) and (ReadOnly = false)).
 --------------------------------------------------------------------*)
procedure TClassDefList.SaveToDatabase;
var
  i: Integer;
begin
  if not(FReadOnly) then begin
    (* Jedes Item einzeln in der Datenbank speichern
       Das Item k�mmert sich selber darum, ob ein INSERT oder ein
       UPDATE notwendig ist. *)
    for i := 0 to DataItemCount - 1 do begin
      if (DataItems[i] is TClassDef) then
        TClassDef(DataItems[i]).Save(mDB.Query[0]);
    end;// for i := 0 to ClassDefCount - 1 do begin
  end;// if not(FReadOnly) then begin
end;// TClassDefList.SaveToDatabase cat:No category

//:-------------------------------------------------------------------
(*: Member:           Sort
 *  Klasse:           TClassDefList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Sortiert die DataItems nach Kategorie
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TClassDefList.Sort;
begin
  // sortierreihenfolge: c_Predefined desc, c_Category, c_ClassTypeGroup, c_OrderPos, c_ClassName
  if assigned(mDataItems) then
    mDataItems.Sort(DataItemSort);
end;// TClassDefList.Sort cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TClassDef
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TClassDef.Create;
begin
  inherited Create;

//: ----------------------------------------------
  FillChar(FClassSettingsArr, 16, 0);
  ID          := cINVALID_USER_CLASS_ID;
  FPersistent := true;
  
  SetModified(false);
end;// TClassDef.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TClassDef
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TClassDef.Destroy;
begin
  inherited Destroy;
end;// TClassDef.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TClassDef
 *  Kategorie:        No category 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Kopiert die Eigenschaften von Source
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TClassDef.Assign(Source: TPersistent);
var
  xClassDef: TClassDef;
begin
  if (Source is TClassDef) then begin
    xClassDef := TClassDef(Source);
    // Alle Eigenschaften kopieren
    ClassSettingsArr := xClassDef.ClassSettingsArr;
    ID               := xClassDef.ID;
    MatrixDef        := xClassDef.MatrixDef;
    MatrixType       := xClassDef.MatrixType;
    Persistent       := xClassDef.Persistent;
    TimeMode         := xClassDef.TimeMode;
  end;// if (Source is TClassDef) then begin

//: ----------------------------------------------
  (* !!! ACHTUNG !!!
     Modified darf erst am Schluss �bernommmen werden, da einige Eigenschaften
     bei der Zuweisung das Flag auf True setzen.
     Wenn inherited am Ende aufgeruffen wird, muss Modified nicht erneut gesetzt werden *)
  inherited Assign(Source)
end;// TClassDef.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           DeleteFromDatabase
 *  Klasse:           TClassDef
 *  Kategorie:        Datenbank 
 *  Argumente:        (aNativeQuery)
 *
 *  Kurzbeschreibung: L�scht eine Klassendefinition von der Datenbank
 *  Beschreibung:     
                      Das Objekt selbst muss von "aussen" freigegeben werden. Es wird hier
                      lediglich als 'Deleted' markiert.
 --------------------------------------------------------------------*)
function TClassDef.DeleteFromDatabase(aNativeQuery: TNativeAdoQuery): Boolean;
var
  xQuery: TNativeAdoQuery;
  xDB: TAdoDBAccess;
  
  const
    cSQLDelete = 'DELETE t_UserClass_Def ' +
                 'WHERE  c_UserClass_id = :c_UserClass_id';
  
begin
  result := false;
  xQuery := nil;
  xDB    := nil;

//: ----------------------------------------------
  // Wenn kein Query �bergeben wird, dann muss eines zur Verf�gung gestellt werden
  if aNativeQuery = nil then begin
    xDB := TAdoDBAccess.Create(1);
    try
      xDB.Init;
      xQuery := xDB.Query[0];
    except
      FreeAndNil(xDB);
    end;// try except
  end else begin
    xQuery := aNativeQuery;
  end;// if aNativeQuery <> nil then begin

//: ----------------------------------------------
  try
    // ID = 0 bedeutet dass die User Klasse noch nicht in der Datenbank ist
    if Id > 0 then begin
      xQuery.SQL.Text := cSQLDelete;
      xQuery.ParamByName('c_UserClass_id').AsInteger := Id;
  
      // Datensatz ist gel�scht, wenn eine Zeile betroffen ist
      if xQuery.ExecSQL > 0 then begin
        // Markieren als neuer Datensatz
        Id := 0;
        result := true;
        FDeleted := true;
      end;// if xQuery.ExecSQL > 0 then begin
    end;// if ID > 0 then begin
  finally
    FreeAndNil(xDB);
  end;// try finally
end;// TClassDef.DeleteFromDatabase cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           GetMatrixDef
 *  Klasse:           TClassDef
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r MatrixDef
 *  Beschreibung:     
                      Die Matrix wird intern als Array gespeichert. Um auf der 
                      Datenbank eine kommaseparierte Liste sichern zu k�nnen, muss
                      die Matrix zuerst "�bersetzt" werden.
 --------------------------------------------------------------------*)
function TClassDef.GetMatrixDef: String;
begin
  result := GetMatrixFromArray(FClassSettingsArr);
end;// TClassDef.GetMatrixDef cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMatrixFormString
 *  Klasse:           TClassDef
 *  Kategorie:        No category 
 *  Argumente:        (aMatrix)
 *
 *  Kurzbeschreibung: Gibt eine Matrix zur�ck. Der String ist eine Komma separierte Liste mit den Klassen die gesetzt 
 sind
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TClassDef.GetMatrixFormString(aMatrix: string): TClassClearSettingsArr;
var
  i: Integer;
  xClassValue: Integer;
  xRow: Integer;
  xCol: Integer;
  
  // Setzt das ensprechende Bit im Byte
  // Setzt ein Bit in einem Byte
  procedure SetBit(aBit: integer; var aValue: Byte);
  begin
    aValue := aValue or (1 shl aBit);
  end;// procedure SetBit(aBit: integer; var aValue: Byte);
  
begin
  // Initialisieren
  FillChar(result, 16, 0);

//: ----------------------------------------------
  if aMatrix > '' then begin
    with TStringList.Create do try
      // um die einzelnen Elemente zu bekommen kann die Stringliste verwendet werden
      CommaText := aMatrix;
      for i := 0 to Count - 1 do begin
        xClassValue := StrToIntDef(Strings[i], -1);
        // Datenbank 1 basiert (1-128)
        xClassValue := xClassValue - 1;
  
        if xClassValue >= 0 then begin
          // Berechnung des Matrix Index aus der Nummer des Feldes
          xRow := ((xClassValue div 64) * 8) + (xClassValue mod 8);
          xCol := (xClassValue div 8) - ((xClassValue div 64) * 8);
          SetBit(xCol, result[xRow]);
        end;// if xClassValue >= 0 then begin
      end;// for i := 0 to Count - 1 do begin
    finally
      free;
    end;// with TStringList.Create do try
  end;// if aMatrix > '' then begin
end;// TClassDef.GetMatrixFormString cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMatrixFromArray
 *  Klasse:           TClassDef
 *  Kategorie:        No category 
 *  Argumente:        (aArray)
 *
 *  Kurzbeschreibung: Gibt einen Kommaseparierten String zur�ck der der �bergebenen Matrix entspricht
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TClassDef.GetMatrixFromArray(aArray: TClassClearSettingsArr): String;
var
  i,j: Integer;
  xClassValue: Integer;
  
  // Testet ob ein Bit gesetzt ist (true)
  function BitTest(aBit: integer; aValue: Byte): Boolean;
  begin
    result := false;
    case aBit of
      0: Result := ((aValue and 1) > 0);
      1: Result := ((aValue and 2) > 0);
      2: Result := ((aValue and 4) > 0);
      3: Result := ((aValue and 8) > 0);
      4: Result := ((aValue and 16) > 0);
      5: Result := ((aValue and 32) > 0);
      6: Result := ((aValue and 64) > 0);
      7: Result := ((aValue and 128) > 0);
    end;// case aBit of
  end;// function BitTest(aBit: integer; aValue: Byte): Boolean;
  
begin
  result := '';

//: ----------------------------------------------
  for i := Low(TClassClearSettingsArr) to High(TClassClearSettingsArr) do begin
    for j := 0 to 7 do begin
      if BitTest(j, FClassSettingsArr[i]) then begin
        (*   j   ist die Spalte von links
           * 8   ergibt den Wert des Feldes in der ersten Reihe der oberen H�lfte der Matrix
           + i   ergibt den Wert des Feldes in der i-ten Reihe der oberen H�lfte
           ((i div 8) * 64) ergibt f�r die ober H�lfte 0 und f�r die untere H�lfte 64. Dies
                            entspricht dem Offset f�r die Felder der unteren H�lfte
           + 1    weil Datenbank 1 Basiert (1-128) *)
        xClassValue := (j * 8) + i + ((i div 8) * 56) + 1;
        if result > '' then
          result := result + ',' + IntToStr(xClassValue)
        else
          result := result + IntToStr(xClassValue);
      end;// if BitTest(j, FClassSettingsArr[i]) then begin
    end;// for j := 0 to 7 do begin
  end;// for i := Low(TClassClearSettingsArr) to High(TClassClearSettingsArr) do begin
end;// TClassDef.GetMatrixFromArray cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetSQLFromTable
 *  Klasse:           TClassDef
 *  Kategorie:        Datenbank 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt den Namen der Tabelle zur�ck aus der die Daten geholt werden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TClassDef.GetSQLFromTable: String;
var
  i: TUsedTable;
begin
  result := '';
  for i := Low(TUsedTable) to High(TUsedTable) do begin
    if (i in TableType) then
      result := cUsedTablesArray[i];
  end;// for i := Low(TUsedTable) to High(TUsedTable) do begin
end;// TClassDef.GetSQLFromTable cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           GetSQLSelectFields
 *  Klasse:           TClassDef
 *  Kategorie:        Datenbank 
 *  Argumente:        (aWithSum, aWithAlias)
 *
 *  Kurzbeschreibung: Gibt die beteiligten Felder zur�ck
 *  Beschreibung:     
                      Mit dem Argument kann bestimmt werden ob die Felder f�r 
                      eine Gruppierung summiert werden ('sum(c_cC1 * 1.0) as c_cC1, sum(c_cC2 * 1.0) as c_cC2')
                      oder nicht ('c_cC1, c_cC2').
 --------------------------------------------------------------------*)
function TClassDef.GetSQLSelectFields(aWithSum: boolean = true; aWithAlias:boolean = true): String;
var
  xCutFieldPrefix: String;
  xUncutFieldPrefix: string;
  i: Integer;
  j: TUsedTable;
  xStartCount: Integer;
  
    const
      cClassPrefix  = 'c_C';
      cSiroPrefix   = 'c_si';
      cSplicePrefix = 'c_sp';
  
      cCutPrefix    = 'C';
      cUnCutPrefix  = 'U';
  
begin
  result := '';

//: ----------------------------------------------
  (* Prefix f�r die Tabellenspalten ist vom Typ (Class, Siro, Splice) der Daten
     und von der Art der Daten (Cut, Uncut) abh�ngig *)
  xCutFieldPrefix   := '';
  xUncutFieldPrefix := '';
  
  case MatrixType of
    mtShortLongThin: begin
      case ClassDefects of
        cdCut   : xCutFieldPrefix   := cClassPrefix + cCutPrefix;
        cdUncut : xUncutFieldPrefix := cClassPrefix + cUnCutPrefix;
        cdBoth  : begin
          xCutFieldPrefix   := cClassPrefix + cCutPrefix;;
          xUncutFieldPrefix := cClassPrefix + cUnCutPrefix;
        end;// cdBoth  : begin
      else
        raise Exception.Create('TClassDef.GetSQLSelectFields: undefined Type (' + GetEnumName(TypeInfo(TClassDefects),ord(ClassDefects)));
      end;// case ClassDefects of
    end;// mtShortLongThin: begin
  
    mtSiro: begin
      case ClassDefects of
        cdCut   : xCutFieldPrefix   := cSiroPrefix + cCutPrefix;
        cdUncut : xUncutFieldPrefix := cSiroPrefix + cUnCutPrefix;
        cdBoth  : begin
          xCutFieldPrefix   := cSiroPrefix + cCutPrefix;
          xUncutFieldPrefix := cSiroPrefix + cUnCutPrefix;
        end;// cdBoth  : begin
      else
        raise Exception.Create('TClassDef.GetSQLSelectFields: undefined Type (' + GetEnumName(TypeInfo(TClassDefects),ord(ClassDefects)));
      end;// case ClassDefects of
    end;// mtSiro: begin
  
    mtSplice: begin
      case ClassDefects of
        cdCut   : xCutFieldPrefix   := cSplicePrefix + cCutPrefix;
        cdUncut : xUncutFieldPrefix := cSplicePrefix + cUnCutPrefix;
        cdBoth  : begin
          xCutFieldPrefix   := cSplicePrefix + cCutPrefix;
          xUncutFieldPrefix := cSplicePrefix + cUnCutPrefix;
        end;// cdBoth  : begin
      else
        raise Exception.Create('TClassDef.GetSQLSelectFields: undefined Type (' + GetEnumName(TypeInfo(TClassDefects),ord(ClassDefects)));
      end;// case ClassDefects of
    end;// mtSplice: begin
  else
    raise Exception.CreateFMT('TClassDef.GetSQLSelectFields: %s not defined',[GetEnumName(TypeInfo(TMatrixType),ord(MatrixType))]);
  end;// case MatrixType of

//: ----------------------------------------------
  // Feldliste generieren
  with TStringList.Create do try
    for j := Low(TUsedTable) to High(TUsedTable) do begin
      // Vorgang f�r jede beteiligte Tabelle ausf�hren
      if (j in TableType) then begin
        xStartCount := Count;
        // Pro verwendete Tabelle die Matrixdefinition hnzuf�gen
        if CommaText <> '' then
          CommaText := CommaText + ',' + MatrixDef
        else
          CommaText := MatrixDef;
  
        for i := xStartCount to Count - 1 do begin
          // Je nach Cut oder Uncut muss ein anderes Prefix verwendet werden
          if j in cCutTables then
            Strings[i] := xCutFieldPrefix + Strings[i]
          else
            Strings[i] := xUncutFieldPrefix + Strings[i];
  
          // Wird die Komplete Definition verlangt, dann f�r die Summe vorbereiten
          if aWithSum then
            result := result + '+' + cTableAlias[j] + '.' + Strings[i] + ' * 1.0 ';
        end;// for i := xStartCount to Count - 1 do begin
  
      end;// if (j in TableType) then begin
    end;// for j := Low(TUsedTable) to High(TUsedTable) do begin
  
    // Wenn mindestens ein Feld hinzugef�gt wurde, dann das f�hrende Komma l�schen
    if result > '' then
      system.delete(result, 1, 1);
  
    // Je nachdem was gew�nscht ist, das komplete Statement zussammensetzen
    if aWithSum then begin
      if aWithAlias then
        // zb: SUM(cd_tmp.c_CU13 * 1.0 +cd_tmp.c_CU14 * 1.0 ) as "A3"
        result := Format (' SUM(%s)' + cFieldAlias, [result, FieldName])
      else
        // zb: SUM(cd_tmp.c_CU13 * 1.0 +cd_tmp.c_CU14 * 1.0 )
        result := Format (' SUM(%s)', [result]);
    end else begin
      // zB: c_CU13, c_CU14, c_CU15, c_CU16)
      result := StringReplace(CommaText, '"', '', [rfReplaceAll]);
    end;// if aWithSum then begin
  finally
    Free;
  end;// with TStringList.Create do try
end;// TClassDef.GetSQLSelectFields cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           GetTableType
 *  Klasse:           TClassDef
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt den Typ der Tabelle zur�ck
 *  Beschreibung:     
                      Der Typ h�ngt vom 'TimeMode' und vom 'MatrixType' ab. Je 
                      nach Typ m�ssen schlussendlich die Daten aus einer anderen 
                      Tabelle geholt werden.
 --------------------------------------------------------------------*)
function TClassDef.GetTableType: TUsedTablesSet;
begin
  case TimeMode of

//: ----------------------------------------------
    // Intervalldaten
    tmInterval:begin
      case MatrixType of
        mtShortLongThin:
          case ClassDefects of
            cdCut   : result := [utIntervalClassCut];
            cdUncut : result := [utIntervalClassUncut];
            cdBoth  : result := [utIntervalClassCut, utIntervalClassUncut];
          else
            raise Exception.Create('TClassDef.GetTableType: undefined Type (' + GetEnumName(TypeInfo(TClassDefects),ord(ClassDefects)));
          end;// case ClassDefects of
        mtSiro:
          case ClassDefects of
            cdCut   : result := [utIntervalFFCut];
            cdUncut : result := [utIntervalFFUncut];
            cdBoth  : result := [utIntervalFFCut, utIntervalFFUncut];
          else
            raise Exception.Create('TClassDef.GetTableType: undefined Type (' + GetEnumName(TypeInfo(TClassDefects),ord(ClassDefects)));
          end;// case ClassDefects of
  //        if Cut then result := utIntervalFFCut else result := utIntervalFFUncut;
        mtSplice:
          case ClassDefects of
            cdCut   : result := [utIntervalSpliceCut];
            cdUncut : result := [utIntervalSpliceUncut];
            cdBoth  : result := [utIntervalSpliceCut, utIntervalSpliceUncut];
          else
            raise Exception.Create('TClassDef.GetTableType: undefined Type (' + GetEnumName(TypeInfo(TClassDefects),ord(ClassDefects)));
          end;// case ClassDefects of
  //        if Cut then result := utIntervalSpliceCut else result := utIntervalSpliceUncut;
      else
        raise Exception.CreateFmt('TClassDef.GetSQLFromTable: %d.%d not implemented',[ord(TimeMode),ord(MatrixType)]);
      end;// case MatrixType of
    end;// tmInterval:begin

//: ----------------------------------------------
    tmShift:begin
      case MatrixType of
        mtShortLongThin:
          case ClassDefects of
            cdCut   : result := [utShiftClassCut];
            cdUncut : result := [utShiftClassUnCut];
            cdBoth  : result := [utShiftClassCut, utShiftClassUnCut];
          else
            raise Exception.Create('TClassDef.GetTableType: undefined Type (' + GetEnumName(TypeInfo(TClassDefects),ord(ClassDefects)));
          end;// case ClassDefects of
  //        if Cut then result := utShiftClassCut else result := utShiftClassUnCut;
        mtSiro:
          case ClassDefects of
            cdCut   : result := [utShiftFFCut];
            cdUncut : result := [utShiftFFUncut];
            cdBoth  : result := [utShiftFFCut, utShiftFFUncut];
          else
            raise Exception.Create('TClassDef.GetTableType: undefined Type (' + GetEnumName(TypeInfo(TClassDefects),ord(ClassDefects)));
          end;// case ClassDefects of
  //        if Cut then result := utShiftFFCut else result := utShiftFFUncut;
        mtSplice:
          case ClassDefects of
            cdCut   : result := [utShiftSpliceCut];
            cdUncut : result := [utShiftSpliceUnCut];
            cdBoth  : result := [utShiftSpliceCut, utShiftSpliceUnCut];
          else
            raise Exception.Create('TClassDef.GetTableType: undefined Type (' + GetEnumName(TypeInfo(TClassDefects),ord(ClassDefects)));
          end;// case ClassDefects of
  //        if Cut then result := utShiftSpliceCut else result := utShiftSpliceUnCut;
      else
        raise Exception.CreateFmt('TClassDef.GetSQLFromTable: %d.%d not implemented',[ord(TimeMode),ord(MatrixType)]);
      end;// case MatrixType of
    end;// tmShift:begin

//: ----------------------------------------------
    tmLongtermWeek:begin
      case MatrixType of
        mtShortLongThin:
          case ClassDefects of
            cdCut   : result := [utLongTermClassCut];
            cdUncut : result := [utLongTermClassUncut];
            cdBoth  : result := [utLongTermClassCut, utLongTermClassUncut];
          else
            raise Exception.Create('TClassDef.GetTableType: undefined Type (' + GetEnumName(TypeInfo(TClassDefects),ord(ClassDefects)));
          end;// case ClassDefects of
  //        if Cut then result := utLongTermClassCut else result := utLongTermClassUncut;
        mtSiro:
          case ClassDefects of
            cdCut   : result := [utLongTermFFCut];
            cdUncut : result := [utLongTermFFUncut];
            cdBoth  : result := [utLongTermFFCut, utLongTermFFUncut];
          else
            raise Exception.Create('TClassDef.GetTableType: undefined Type (' + GetEnumName(TypeInfo(TClassDefects),ord(ClassDefects)));
          end;// case ClassDefects of
  //        if Cut then result := utLongTermFFCut else result := utLongTermFFUncut;
        mtSplice:
          case ClassDefects of
            cdCut   : result := [utLongTermSpliceCut];
            cdUncut : result := [utLongTermSpliceUncut];
            cdBoth  : result := [utLongTermSpliceCut, utLongTermSpliceUncut];
          else
            raise Exception.Create('TClassDef.GetTableType: undefined Type (' + GetEnumName(TypeInfo(TClassDefects),ord(ClassDefects)));
          end;// case ClassDefects of
  //        if Cut then result := utLongTermSpliceCut else result := utLongTermSpliceUncut;
      else
        raise Exception.CreateFmt('TClassDef.GetSQLFromTable: %d.%d not implemented',[ord(TimeMode),ord(MatrixType)]);
      end;// case MatrixType of
    end;// tmLongtermWeek:begin

//: ----------------------------------------------
  else
    raise Exception.CreateFmt('TClassDef.GetSQLFromTable: %d not implemented',[ord(TimeMode)]);
  end;// case TimeMode of
end;// TClassDef.GetTableType cat:No category

//:-------------------------------------------------------------------
(*: Member:           Save
 *  Klasse:           TClassDef
 *  Kategorie:        Datenbank 
 *  Argumente:        (aNativeQuery)
 *
 *  Kurzbeschreibung: Speichert die Klassendefinition in der Datenbank
 *  Beschreibung:     
                      Je nach Modus wird ein Update, oder ein Insert 
                      Statement erzeugt.
                      Wird ein Query �bergeben, dann wird dieses verwendet, sonst
                      wird eines erzeugt.
 --------------------------------------------------------------------*)
procedure TClassDef.Save(aNativeQuery: TNativeAdoQuery = nil);
var
  xQuery: TNativeAdoQuery;
  xDB: TAdoDBAccess;
  
  procedure SetQueryParams;
  begin
    with xQuery do begin
      ParamByName('c_ClassName').AsString    := ItemName;
      ParamByName('c_Predefined').AsInteger  := integer(Predefined);
      ParamByName('c_Matrix_Type').AsInteger := ord(MatrixType);
      ParamByName('c_Matrix_Def').AsString   := MatrixDef;
      ParamByName('c_Category').AsString     := Category;
      ParamByName('c_Description').AsString  := Description;
    end;// with xQuery do begin
  end;// procedure SetQueryParams;
  
  procedure DoInsert;
  const
    cSQLInsert = 'INSERT INTO t_UserClass_Def ' +
                 '(c_UserClass_id, c_ClassName, c_Predefined, c_Matrix_Type, c_Matrix_Def, c_Category, c_Description) ' +
                 'VALUES (:c_UserClass_id, :c_ClassName, :c_Predefined, :c_Matrix_Type, :c_Matrix_Def, :c_Category, :c_Description)';
  begin
    xQuery.SQL.Text := cSQLInsert;
    SetQueryParams;
    id := xQuery.InsertSQL('t_UserClass_Def', 'c_UserClass_id', High(Smallint), 1);
  end;// procedure DoInsert;
  
  procedure DoUpdate;
  const
    cSQLUpdate = 'UPDATE t_UserClass_Def ' +
                 'SET    c_ClassName = :c_ClassName, c_Predefined = :c_Predefined,c_Matrix_Type = :c_Matrix_Type,  ' +
                 '       c_Matrix_Def = :c_Matrix_Def, c_Category = :c_Category, c_Description = :c_Description ' +
                 'WHERE  c_UserClass_id = :c_UserClass_id';
  begin
    if Modified then begin
      xQuery.SQL.Text := cSQLUpdate;
      SetQueryParams;
      xQuery.ParamByName('c_UserClass_id').AsInteger := Id;
      if xQuery.ExecSQL = 0 then
        DoInsert;
    end;// if Modified then begin
  end;// procedure DoUpdate;
  
begin
  // Wenn die Klasse gel�scht ist, dann nicht mehr speichern
  if Deleted then
    exit;

//: ----------------------------------------------
  xQuery := nil;
  xDB := nil;

//: ----------------------------------------------
  // Nur wenn die Daten persistent erzeugt wurden, wieder speichern
  if (modified) and (persistent) then begin
    // Wenn kein Query �bergeben wird, dann muss eines zur Verf�gung gestellt werden
    if aNativeQuery = nil then begin
      xDB := TAdoDBAccess.Create(1);
      try
        xDB.Init;
        xQuery := xDB.Query[0];
      except
        FreeAndNil(xDB);
      end;// try except
    end else begin
      xQuery := aNativeQuery;
    end;// if aNativeQuery <> nil then begin

//: ----------------------------------------------
    try
      // Wenn noch keine ID vergeben wurde, dann ist es eine neue Klasse
      if ID = cINVALID_USER_CLASS_ID then
        DoInsert
      else
        DoUpdate;
      // Datensatz ist gespeichert
      SetModified(false);
    finally
      FreeAndNil(xDB);
    end;// try finally
  end;// if (modified) and (persistent) then begin
end;// TClassDef.Save cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           SetClassSettingsArr
 *  Klasse:           TClassDef
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r das Class Settings Array
 *  Beschreibung:     
                      Wird die Definition ge�ndert, dann wird das Modified Flag
                      gesetzt.
 --------------------------------------------------------------------*)
procedure TClassDef.SetClassSettingsArr(Value: TClassClearSettingsArr);
begin
  FClassSettingsArr := Value;

//: ----------------------------------------------
  SetModified(true);
end;// TClassDef.SetClassSettingsArr cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetMatrixDef
 *  Klasse:           TClassDef
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r MatrixDef
 *  Beschreibung:     
                      Die Matrix wird intern als Array gespeichert. Um auf der 
                      Datenbank eine kommaseparierte Liste sichern zu k�nnen, muss
                      die Matrix zuerst "�bersetzt" werden.
 --------------------------------------------------------------------*)
procedure TClassDef.SetMatrixDef(Value: String);
begin
  FClassSettingsArr := GetMatrixFormString(value);

//: ----------------------------------------------
  SetModified(true);
end;// TClassDef.SetMatrixDef cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetMatrixType
 *  Klasse:           TClassDef
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r den Typ der Matrix
 *  Beschreibung:     
                      Beim Setzen des Matrix Typs wird das Modified Flag gesetzt.
 --------------------------------------------------------------------*)
procedure TClassDef.SetMatrixType(Value: TMatrixType);
begin
  FMatrixType := Value;

//: ----------------------------------------------
  SetModified(true);
end;// TClassDef.SetMatrixType cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TDataItemDecorator.Destroy;
begin
  inherited Destroy;

//: ----------------------------------------------
  // Dekoriertes DataItem freigeben
  if assigned(FDataItem) then
    FreeAndNil(FDataItem);
end;// TDataItemDecorator.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        No category 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Kopiert die Eigenschaften von Source  
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemDecorator.Assign(Source: TPersistent);
var
  xClass: TBaseDataItemClass;
begin
  if Source is TDataItemDecorator then begin
    xClass := TBaseDataItemClass(TDataItemDecorator(Source).DataItem.ClassType);
  
    if not(assigned(FDataItem)) then
      FDataItem := xClass.Create;
  
    FDataItem.Assign(TDataItemDecorator(Source).DataItem);
  end;// if Source is TDataItemDecorator then begin
end;// TDataItemDecorator.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           AssignTo
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        No category 
 *  Argumente:        (Dest)
 *
 *  Kurzbeschreibung: Kopiert sich selber auf Dest
 *  Beschreibung:     
                      AssignTo wird aufgerufen, wenn Assign nicht erfolgreich war. 
                      Der Dekorierer muss demnach Assign auch �berschreiben um 
                      die eigenen Eigenschaften zu kopieren.
                      Im Assign muss allerdings immer 'inherited Assign()' 
                      aufgerufen werden.
 --------------------------------------------------------------------*)
procedure TDataItemDecorator.AssignTo(Dest: TPersistent);
begin
  if (Dest is TBaseDataItem) then begin
    if assigned(FDataItem) then
      Dest.Assign(FDataItem);
  end else begin
    inherited AssignTo(Dest);
  end;// if (Dest is TBaseDataItem) then begin
end;// TDataItemDecorator.AssignTo cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetCategory
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Reicht die Abfrage an das eingebettete Objekt weiter
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetCategory: String;
begin
  if assigned(FDataItem) then
    result := FDataItem.Category
  else
    result := inherited GetCategory;
end;// TDataItemDecorator.GetCategory cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetClassDefects
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Reicht die Abfrage an das eingebettete Objekt weiter
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetClassDefects: TClassDefects;
begin
  if assigned(FDataItem) then
    result := FDataItem.ClassDefects
  else
    result := inherited GetClassDefects;
end;// TDataItemDecorator.GetClassDefects cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetClassTypeGroup
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Reicht die Abfrage an das eingebettete Objekt weiter
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetClassTypeGroup: TClassTypeGroup;
begin
  if assigned(FDataItem) then
    result := FDataItem.ClassTypeGroup
  else
    result := inherited GetClassTypeGroup;
end;// TDataItemDecorator.GetClassTypeGroup cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetConfigString
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Reicht die Abfrage an das eingebettete Objekt weiter
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetConfigString: String;
begin
  if assigned(FDataItem) then
    result := FDataItem.ConfigString
  else
    result := inherited GetConfigString;
end;// TDataItemDecorator.GetConfigString cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetDataItemType
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Holt den Typ der Dekorierten Klasse
 *  Beschreibung:     
                      Damit der Programmierer von aussen eine Ahnung davon hat
                      womit er es zu tun hat, kann der Typ der dekorierten Klasse abgefragt werden.
                      
                      Im Normalfall wird eine Anwendung mit dekorierten Items arbeiten. Es k�nnte
                      aber F�lle geben wo mit verschiedenen DataItems gearbeitet werden muss. In diesem 
                      Fall soll eine einfache Abfrage des dekorierten Datentyps m�glich sein, ohne dass 
                      die Verschachtelungstiefe des Dekorierers bekannt sein muss.
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetDataItemType: TBaseDataItemClass;
begin
  result := nil;

//: ----------------------------------------------
  if assigned(FDataItem) then
    result := FDataItem.DataItemType;
end;// TDataItemDecorator.GetDataItemType cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetDeleted
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Reicht die Abfrage an das eingebettete Objekt weiter
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetDeleted: Boolean;
begin
  if assigned(FDataItem) then
    result := FDataItem.Deleted
  else
    result := inherited GetDeleted;
end;// TDataItemDecorator.GetDeleted cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetDescription
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Reicht die Abfrage an das eingebettete Objekt weiter
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetDescription: String;
begin
  if assigned(FDataItem) then
    result := FDataItem.Description
  else
    result := inherited GetDescription;
end;// TDataItemDecorator.GetDescription cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetDisplayName
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Reicht die Abfrage an das dekorierte DataItem weiter
 *  Beschreibung:     
                      Name des DataItems der vom Benutzer festgelegt wird. Dieser 
                      Name wird intern nirgends verwendet
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetDisplayName: String;
begin
  if assigned(FDataItem) then
    result := FDataItem.DisplayName
  else
    result := inherited GetDisplayName;
end;// TDataItemDecorator.GetDisplayName cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetFieldName
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Reicht die Abfrage an das eingebettete Objekt weiter
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetFieldName: String;
begin
  if assigned(FDataItem) then
    result := FDataItem.FieldName
  else
    result := inherited GetFieldName;
end;// TDataItemDecorator.GetFieldName cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetItemName
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Reicht die Abfrage an das eingebettete Objekt weiter
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetItemName: String;
begin
  if assigned(FDataItem) then
    result := FDataItem.ItemName
  else
    result := inherited GetItemName;
end;// TDataItemDecorator.GetItemName cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetLinkAlias
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Reicht die Abfrage an das eingebettete Objekt weiter
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetLinkAlias: String;
begin
  if assigned(FDataItem) then
    result := FDataItem.LinkAlias
  else
    result := inherited GetLinkAlias;
end;// TDataItemDecorator.GetLinkAlias cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetModified
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Reicht die Abfrage an das eingebettete Objekt weiter
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetModified: Boolean;
begin
  if assigned(FDataItem) then
    result := FDataItem.Modified
  else
    result := inherited GetModified;
end;// TDataItemDecorator.GetModified cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetOrderPos
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Reicht die Abfrage an das eingebettete Objekt weiter
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetOrderPos: Integer;
begin
  if assigned(FDataItem) then
    result := FDataItem.OrderPos
  else
    result := inherited GetOrderPos;
end;// TDataItemDecorator.GetOrderPos cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetPredefined
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Reicht die Abfrage an das eingebettete Objekt weiter
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetPredefined: Boolean;
begin
  if assigned(FDataItem) then
    result := FDataItem.Predefined
  else
    result := inherited GetPredefined;
end;// TDataItemDecorator.GetPredefined cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetSQLFromTable
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Beinhaltet die Tabelle mit dem Tabellenalias von der die Daten gesammelt werden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetSQLFromTable: String;
begin
  if assigned(FDataItem) then
    result := FDataItem.GetSQLFromTable
  else
    result := inherited GetSQLFromTable;
end;// TDataItemDecorator.GetSQLFromTable cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetSQLSelectFields
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        (aWithSum, aWithAlias)
 *
 *  Kurzbeschreibung: Beinhaltet alle Felder die f�r die Abfrage ben�tigt werden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetSQLSelectFields(aWithSum: boolean = true; aWithAlias:boolean = true): String;
begin
  if assigned(FDataItem) then
    result := FDataItem.GetSQLSelectFields(aWithSum, aWithAlias)
  else
    result := inherited GetSQLSelectFields(aWithSum, aWithAlias);
end;// TDataItemDecorator.GetSQLSelectFields cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetSQLWhere
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Beinhaltet die Where-Klausel (Join) die die Felder betreffen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetSQLWhere: String;
begin
  if assigned(FDataItem) then
    result := FDataItem.GetSQLWhere
  else
    result := inherited GetSQLWhere;
end;// TDataItemDecorator.GetSQLWhere cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetTableType
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Delegiert die Abfrage an das Eingebettete DataItem
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetTableType: TUsedTablesSet;
begin
  if assigned(FDataItem) then
    result := FDataItem.TableType
  else
    result := inherited GetTableType;
end;// TDataItemDecorator.GetTableType cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           GetTimeMode
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Delegiert die Abfrage an das Eingebettete DataItem
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TDataItemDecorator.GetTimeMode: TTimeMode;
begin
  if assigned(FDataItem) then
    result := FDataItem.TimeMode
  else
    result := inherited GetTimeMode;
end;// TDataItemDecorator.GetTimeMode cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           SetCategory
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Eigenschaft im eingebetteten Objekt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemDecorator.SetCategory(Value: String);
begin
  if assigned(FDataItem) then
    FDataItem.Category := Value
  else
    inherited SetCategory(Value);
end;// TDataItemDecorator.SetCategory cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           SetClassDefects
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Eigenschaft im eingebetteten Objekt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemDecorator.SetClassDefects(Value: TClassDefects);
begin
  if assigned(FDataItem) then
    FDataItem.ClassDefects := Value
  else
    inherited SetClassDefects(Value);
end;// TDataItemDecorator.SetClassDefects cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           SetClassTypeGroup
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Eigenschaft im eingebetteten Objekt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemDecorator.SetClassTypeGroup(Value: TClassTypeGroup);
begin
  if assigned(FDataItem) then
    FDataItem.ClassTypeGroup := Value
  else
    inherited SetClassTypeGroup(Value);
end;// TDataItemDecorator.SetClassTypeGroup cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           SetConfigString
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Eigenschaft im eingebetteten Objekt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemDecorator.SetConfigString(Value: String);
begin
  if assigned(FDataItem) then
    FDataItem.ConfigString := Value
  else
    inherited SetConfigString(Value);
end;// TDataItemDecorator.SetConfigString cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           SetDescription
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Eigenschaft im eingebetteten Objekt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemDecorator.SetDescription(Value: String);
begin
  if assigned(FDataItem) then
    FDataItem.Description := Value
  else
    inherited SetDescription(Value);
end;// TDataItemDecorator.SetDescription cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           SetDisplayName
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Eigenschaft im eingebetten Objekt
 *  Beschreibung:     
                      Name des DataItems der vom Benutzer festgelegt wird. Dieser
                      Name wird intern nirgends verwendet.
 --------------------------------------------------------------------*)
procedure TDataItemDecorator.SetDisplayName(Value: String);
begin
  if assigned(FDataItem) then
    FDataItem.DisplayName := Value
  else
    inherited SetDisplayName(Value);
end;// TDataItemDecorator.SetDisplayName cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetFieldName
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Eigenschaft im eingebetteten Objekt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemDecorator.SetFieldName(const Value: String);
begin
  if assigned(FDataItem) then
    FDataItem.FieldName := Value
  else
    inherited SetFieldName(Value);
end;// TDataItemDecorator.SetFieldName cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           SetItemName
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Eigenschaft im eingebetteten Objekt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemDecorator.SetItemName(const Value: String);
begin
  if assigned(FDataItem) then
    FDataItem.ItemName := Value
  else
    inherited SetItemName(Value);
end;// TDataItemDecorator.SetItemName cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           SetLinkAlias
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Eigenschaft im eingebetteten Objekt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemDecorator.SetLinkAlias(Value: String);
begin
  if assigned(FDataItem) then
    FDataItem.LinkAlias := Value
  else
    inherited SetLinkAlias(Value);
end;// TDataItemDecorator.SetLinkAlias cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           SetModified
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Eigenschaft im eingebetteten Objekt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemDecorator.SetModified(Value: Boolean);
begin
  if assigned(FDataItem) then
    FDataItem.Modified := Value
  else
    inherited SetModified(Value);
end;// TDataItemDecorator.SetModified cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           SetOrderPos
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Eigenschaft im eingebetteten Objekt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemDecorator.SetOrderPos(Value: Integer);
begin
  if assigned(FDataItem) then
    FDataItem.OrderPos := Value
  else
    inherited SetOrderPos(Value);
end;// TDataItemDecorator.SetOrderPos cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           SetPredefined
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Eigenschaft im eingebetteten Objekt
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemDecorator.SetPredefined(Value: Boolean);
begin
  if assigned(FDataItem) then
    FDataItem.Predefined := Value
  else
    inherited SetPredefined(Value);
end;// TDataItemDecorator.SetPredefined cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           SetTimeMode
 *  Klasse:           TDataItemDecorator
 *  Kategorie:        Dekoriert 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Delegiert die Abfrage an das Eingebettete DataItem
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDataItemDecorator.SetTimeMode(Value: TTimeMode);
begin
  if assigned(FDataItem) then
    FDataItem.TimeMode := Value
  else
    inherited SetTimeMode(Value);
end;// TDataItemDecorator.SetTimeMode cat:Dekoriert

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TClassDefContainer
 *  Kategorie:        No category 
 *  Argumente:        (aPersistent)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      Erzeugt die aggregierte Liste.
                      Die Items werden im Vorg�nger von der Datenbank geladen, 
                      wenn 'aPersistent' true ist. 
 --------------------------------------------------------------------*)
constructor TClassDefContainer.Create(aPersistent: boolean = false);
begin
  inherited Create(aPersistent);
end;// TClassDefContainer.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TClassDefContainer
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TClassDefContainer.Destroy;
begin
  // ------ Wenn bereits eine Tabelle erzeugt wurde, dann diese l�schen -----
  if fTempTableName <> '' then begin
    GetQuery.SQL.Text := cDropTable + fTempTableName;
    GetQuery.ExecSQL;
  end;// if fTempTableName <> '' then

//: ----------------------------------------------
  FreeAndNil(FQueryParameters);

//: ----------------------------------------------
  inherited Destroy;
end;// TClassDefContainer.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddClassFromConfigString
 *  Klasse:           TClassDefContainer
 *  Kategorie:        No category 
 *  Argumente:        (aConfigString, aClassDefList, aDecorator)
 *
 *  Kurzbeschreibung: F�gt ein DataItem zum Container hinzu
 *  Beschreibung:     
                      Es wird versucht das DataItem aus der Liste der verf�gbaren 
                      DataItems zu erzeugen. Ist dies gelungen, wird der Dekorierer
                      erzeugt und das DataItem diesem zugewiesen. 
                      Die Eigenschaften aus dem ConfigString werden dem DataItem
                      zugewiesen.
 --------------------------------------------------------------------*)
procedure TClassDefContainer.AddClassFromConfigString(aConfigString:string; aClassDefList: TClassDefList; aDecorator: 
  TDataItemDecoratorClass = nil);
var
  xItemName: string;
  xNewDataItemIndex: Integer;
begin
  xItemName := '';

//: ----------------------------------------------
  with TIniStringList.Create do try
    // ConfigString in einzele Eintr�ge aufschneiden
    commaText := aConfigString;
  
    xItemName := Values['ItemName'];
  
    // Neues DataItem erzeugen und im Container ablegen
    xNewDataItemIndex := self.AddCopy(xItemName, aClassDefList, aDecorator);
  
    // Dem neuen DataItem den Configstring weitergeben
    if xNewDataItemIndex >= 0 then
      DataItems[xNewDataItemIndex].ConfigString := aConfigString;
  finally
    free;
  end;// with TIniStringList do try
end;// TClassDefContainer.AddClassFromConfigString cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddCopy
 *  Klasse:           TClassDefContainer
 *  Kategorie:        No category 
 *  Argumente:        (aDataItem, aList, aDecoratorClass)
 *
 *  Kurzbeschreibung: F�gt ein neues DataItem hinzu
 *  Beschreibung:     
                      Das Element wird anhand des Namens in der Liste(TClassDefList) 
                      gesucht. Wenn es gefunden wird, dann wird dieses Item hinzugef�gt
                      und der Index in der Liste zur�ckgegeben. Ist das Item nicht
                      vorhanden wird -1 zur�ckgegeben.
 --------------------------------------------------------------------*)
function TClassDefContainer.AddCopy(aDataItem: string; aList: TClassDefList; aDecoratorClass: TDataItemDecoratorClass = 
  nil): Integer;
var
  xDataItem: TBaseDataItem;
begin
  result := -1;
  
  // Klassendefinition anhand des Namens suchen
  xDataItem := aList.DataItemByName[aDataItem];
  
  // Wenn die Klassendefinition vorhanden ist, dann hinzuf�gen
  if assigned(xDataItem) then
    result := AddCopy(xDataItem, aDecoratorClass);
end;// TClassDefContainer.AddCopy cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddCopy
 *  Klasse:           TClassDefContainer
 *  Kategorie:        No category 
 *  Argumente:        (aDataItem, aDecoratorClass)
 *
 *  Kurzbeschreibung: F�gt ein neues DataItem hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TClassDefContainer.AddCopy(aDataItem: TBaseDataItem; aDecoratorClass: TDataItemDecoratorClass = nil): Integer;
var
  xDataItem: TBaseDataItem;
  xBaseDataItemClass: TBaseDataItemClass;
  xDecorator: TDataItemDecorator;
  xNewFieldName: string;
begin
  result := -1;

//: ----------------------------------------------
  // Sicherstellen, dass der Name eindeutig ist
  xNewFieldName := GetUniqueFieldName(aDataItem.FieldName);

//: ----------------------------------------------
  if assigned(mDataItems) then begin
    if (aDecoratorClass = nil) then begin
      (* Hier wird auch im Fall, dass 'aDataItem' ein Dekorator ist, der Klassentyp des
         �bergebenen DataItems ben�tigt. Wird ein Dekorator angegeben, dann wird das
         DataItem "ausgepackt" und in den neuen Dekorator wieder "eingepackt". Deshalb
         wird unten der Typ des (allf�llig) dekorierten DataItems gebraucht. *)
      xBaseDataItemClass := TBaseDataItemClass(aDataItem.ClassType);
      // Zuerst ein eigenes Objekt erzeugen
      xDataItem := xBaseDataItemClass.Create;
      // Dann die Eigenschaften kopieren
      xDataItem.Assign(aDataItem);
      // FieldName muss eventuell ge�ndert werden
      xDataItem.FieldName := xNewFieldName;
      // und das neue Objekt der Liste hinzuf�gen
      result := inherited Add(xDataItem);
    end else begin
      // Klasse des (evt.) dekorierten DataItem's feststellen
      xBaseDataItemClass := TBaseDataItemClass(aDataItem.DataItemType);
      // Zuerst ein Dekorator Objekt erzeugen
      xDecorator := aDecoratorClass.Create;
      // Dann im Dekorator das gew�nschte Objekt erzeugen
      xDecorator.DataItem := xBaseDataItemClass.Create;
      // Dann die Eigenschaften kopieren
      xDecorator.DataItem.Assign(aDataItem);
      // FieldName muss eventuell ge�ndert werden
      xDecorator.DataItem.FieldName := xNewFieldName;
      // und das neue Objekt der Liste hinzuf�gen
      result := inherited Add(xDecorator);
    end;// if (aDecoratorClass = nil) then begin
  end;// if assigned(mDataItems) then begin
end;// TClassDefContainer.AddCopy cat:No category

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TClassDefContainer
 *  Kategorie:        No category 
 *  Argumente:        (aSource)
 *
 *  Kurzbeschreibung: Kopiert die Eigenschaften von Source
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TClassDefContainer.Assign(aSource: TClassDefList);
var
  xClassDefContainer: TClassDefContainer;
begin
  inherited Assign(aSource);

//: ----------------------------------------------
  // Im Vorg�nger werden alle Items kopiert. Jetzt noch die Eigenschaften kopieren
  if (aSource is TClassDefContainer) then begin
    xClassDefContainer := TClassDefContainer(aSource);
  
    FLinkAlias := xClassDefContainer.LinkAlias;
    if assigned(xClassDefContainer.QueryParameters) then begin
      if not(assigned(FQueryParameters)) then
        FQueryParameters := TQueryParameters.Create;
  
      // Es wird eine Kopie der Parameter erzeugt
      FQueryParameters.Assign(xClassDefContainer.QueryParameters);
    end else begin
      FreeAndNil(FQueryParameters);
    end;// if assigned(xClassDefContainer.QueryParameters) then begin
  end;// if (aSource is TClassDefList) then begin
end;// TClassDefContainer.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           CreateAndAddNewDataItem
 *  Klasse:           TClassDefContainer
 *  Kategorie:        No category 
 *  Argumente:        (aDataItemClass, aDecoratorClass)
 *
 *  Kurzbeschreibung: Erzeugt ein neues DataItem und f�gt es der Liste hinzu
 *  Beschreibung:     
                      Als Argumente werden die Klasse des DataItems und die 
                      Klasse des Dekorierers �bergeben.
                      
                      Ist der Dekorierer nil, dann wird das DataItem mit der 
                      �bergebenen Klasse erzeugt.
 --------------------------------------------------------------------*)
function TClassDefContainer.CreateAndAddNewDataItem(aDataItemClass: TBaseDataItemClass; aDecoratorClass: 
  TDataItemDecoratorClass = nil): Integer;
var
  xDataItem: TBaseDataItem;
  xDecorator: TDataItemDecorator;
begin
  result := -1;

//: ----------------------------------------------
  if assigned(mDataItems) then begin
    if (aDecoratorClass = nil) then begin
      // Zuerst ein eigenes Objekt erzeugen
      xDataItem := aDataItemClass.Create;
      // und das neue Objekt der Liste hinzuf�gen
      result := inherited Add(xDataItem);
    end else begin
      // Zuerst ein Dekorator Objekt erzeugen
      xDecorator := aDecoratorClass.Create;
      // Dann im Dekorator das gew�nschte Objekt erzeugen
      xDecorator.DataItem := aDataItemClass.Create;
      // und das neue Objekt der Liste hinzuf�gen
      result := inherited add(xDecorator);
    end;// if (aDecoratorClass = nil) then begin
  end;// if assigned(mDataItems) then begin
end;// TClassDefContainer.CreateAndAddNewDataItem cat:No category

//:-------------------------------------------------------------------
(*: Member:           CreateTempTable
 *  Klasse:           TClassDefContainer
 *  Kategorie:        Datenbank 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Erzeugt die tempor�re Tabelle
 *  Beschreibung:     
                      Die Tabelle wird erzeugt und abgef�llt. Dazu m�ssen die 
                      Intervaldaten ausgepackt und einzeln in diese Tabelle
                      abgef�llt werden.
                      Die Tabelle enth�lt nur die ben�tigten Felder (Nur die Felder 
                      die von mindestens einer Superklasse in der Liste abgefeckt werden)
 --------------------------------------------------------------------*)
function TClassDefContainer.CreateTempTable: String;
var
  xTableNumber: Integer;
  xCode: Integer;
  xNumberString: String;
  xQuery: TNativeAdoQuery;
  xCreateTable: String;
  
  const
    (* Wenn der Tabellen Name mit zwei Doppelkreuzen beginnt, dann erzeugt SQL Server
       die Tabelle in der Datenbank 'tempdb'. Diese Tabelle ist von der Datenbank
       MM_Windig sichtbar.
       Die Tabelle wird automatisch gel�scht, sobald die Anwendung, die die Tabelle
       erzeugt hat, beendet wird. *)
    cTableName = '##ClassDefContainerTable';
  
    cCreateTable = 'Create Table %s ( ' +
                   ' c_spindle_id   tinyint  NOT NULL , ' +
                   ' c_machine_id   smallint NOT NULL , ' +
                   ' c_interval_id  tinyint  NOT NULL , ' +
                   ' c_prod_id      int      NOT NULL , ' +
                   ' c_fragshift_id int      NOT NULL , ' +
                   ' %s)';
  
    cCheckTable = 'SELECT name ' +
                  ' FROM tempdb..sysobjects' +
                  ' WHERE name like ''%s''' +
                  ' ORDER BY name DESC';
  
  (*
  INSERT INTO %s VALUES (:ID, :Feld)
  *)
  
  (*-------------------------------------------------------
    Erzeugt die Tabelle.
    Der Tabellen Name wird dynamisch erzeugt, damit mehrere Instanzen dieser
    Klasse auf die eigene Tabelle zugreifen kann.
    Das Namensschema sieht vor, dass sich der Name der Tabelle aus dem String
    '##ClassDefContainerTable' und einer laufenden 5-Stelligen Nummer besteht
  ---------------------------------------------------------*)
  function CreateTable: string;
  var
    i: integer;
  begin
    // ------ Wenn bereits eine Tabelle erzeugt wurde, dann diese l�schen -----
    if fTempTableName <> '' then begin
      xQuery.SQL.Text := cDropTable + fTempTableName;
      xQuery.ExecSQL;
    end;// if mTempTableName <> '' then begin
  
    // ---------------------- Tabellen Name festlegen -------------------------
    result := cTableName + '00000';
    // Abfragen ob bereits Tabelle(n) von anderen Instanzen existieren
    xQuery.SQL.Text := Format(cCheckTable,[cTableName + '%']);
    xQuery.Open;
  
    (* Wenn keine Tabelle vorhanden ist, dann kann der Standard Name verwendet werden,
       sonst ist der erste Datensatz der mit der h�chsten Nummer *)
    if xQuery.FindFirst then begin
      // extrahierte Nummer der Tabelle mit der gr�ssten Nummer
      xNumberString := copy(xQuery.FieldByName('name').AsString, Length(cTableName) + 1, MAXINT);
      // String in eine Nummer verwandeln
      val(xNumberString, xTableNumber, xCode);
  
      // Umwandlung erfolgreich, wenn xCode = 0
      if xCode = 0 then begin
        // Zuerst die Nummer inkrementieren und in einen String mit 5 Stellen verwandeln
        result := Format('%5d',[xTableNumber + 1]);
        // Dann die f�hrenden Leerzeichen mit '0' f�llen
        result := StringReplace(result, ' ', '0', [rfReplaceAll]);
        // Und schliesslich noch das Prefix
        result := cTableName + result;
      end else begin
        raise exception.Create('Error creating temp table (' + result + ')');
      end;// if xCode = 0 then begin
    end;// if xQuery.FindFirst then begin
  
    // -------------------- Tabelle erzeugen -----------------------
  
    // Zuerst das CreateTable aus dem Konstanten Array 'cKeyFields' zusammensetzen
    xCreateTable := '';
    for i := Low(cKeyFields) to High(cKeyFields) do begin
      with cKeyFields[i] do
        xCreateTable := xCreateTable + Name + ' ' + FieldType + ' ' + Appendage + ', ';
    end;// for i := Low(cKeyFields) to High(cKeyFields) do begin
    xCreateTable := 'Create Table %s ( ' + xCreateTable + ' %s)';
  
    xQuery.SQL.Text := Format(xCreateTable, [result, GetFields(true, true)]);
    xQuery.ExecSQL;
  end;// function CreateTable: string;
  
begin
  xQuery := GetQuery;

//: ----------------------------------------------
  // Erzeugt die tempor�re Tabelle
  fTempTableName := CreateTable;
  // Tabelle mit den gew�nschten Daten f�llen
  FillTempTable;

//: ----------------------------------------------
  result := fTempTableName;
end;// TClassDefContainer.CreateTempTable cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           DoGetDataItemGenerateSQL
 *  Klasse:           TClassDefContainer
 *  Kategorie:        Datenbank 
 *  Argumente:        (Sender, aDataItem, aEnabled)
 *
 *  Kurzbeschreibung: Dispatcher f�r OnGetDataItemGenarateSQL
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TClassDefContainer.DoGetDataItemGenerateSQL(Sender: TObject; aDataItem: TBaseDataItem; var aEnabled: boolean);
begin
  if Assigned(FOnGetDataItemGenerateSQL) then FOnGetDataItemGenerateSQL(Sender, aDataItem, aEnabled);
end;// TClassDefContainer.DoGetDataItemGenerateSQL cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           FillTempTable
 *  Klasse:           TClassDefContainer
 *  Kategorie:        Datenbank 
 *  Argumente:        
 *
 *  Kurzbeschreibung: F�llt die tempor�re Tabelle mit den Intervall Daten
 *  Beschreibung:     
                      Die tempor�re Tabelle wird nur im Fall 'Intervalldaten'
                      benutzt. In diesem Fall m�ssen die Daten aus dem Image 
                      aufbereitet werden.
 --------------------------------------------------------------------*)
procedure TClassDefContainer.FillTempTable;
var
  xIQB: TIntervalQueryBuilder;
  xQueryText: TStringList;
  xReadQuery: TNativeAdoQuery;
  i: Integer;
  xWriteQuery: TNativeAdoQuery;
  xClassImageRec: TClassFieldRec;
  xFieldList: string;
  
  const
    cReadQueryIndex  = 0;
    cWriteQueryIndex = 1;
  
    cInsertTempTable = 'INSERT INTO %s (%s) VALUES %s';
  
  (*-----------------------------------------------------------
    Baut das Query f�r das Schreiben in die tempor�re Tabelle zusammen.
    Dazu wird f�r dan aktuellen Datensatz ein INSERT Statement erzeugt
  -------------------------------------------------------------*)
  function CreateWriteQueryText: String;
    (*-----------------------------------------------------------
      Fix Felder sind die Schl�ssel Felder der Tabelle
      Die Felder werden einmal f�r den INSERT Teil erzeugt, und ein
      zweites Mal als Parameter f�r den VALUES Teil
    -------------------------------------------------------------*)
    procedure SetFixFields(aPrefix: string; var aQueryText: string);
    var
      i: integer;
    begin
      // Die Fixfelder sind im Konstantenarrray 'cKeyFields' abgelegt
      for i := Low(cKeyFields) to High(cKeyFields) do begin
        if i > Low(cKeyFields) then
          aQueryText := aQueryText + ', ';
        aQueryText := aQueryText + aPrefix + cKeyFields[i].Name;
      end;// for i := Low(cKeyFields) to High(cKeyFields) do begin
    end;// procedure SetFixFields(aPrefix: string; var aQueryText: string);
  
    (*-----------------------------------------------------------
      Variantte Felder sind die Mitglieder der einzelnen Superklassen
      Die Felder werden einmal f�r den INSERT Teil erzeugt, und ein
      zweites Mal als Parameter f�r den VALUES Teil
    -------------------------------------------------------------*)
    procedure SetVarFields(aPrefix: string; var aQueryText: string);
    var
      i: integer;
    begin
      with TStringList.Create do try
        CommaText := xFieldList;
        for i := 0 to Count - 1 do
          aQueryText := aQueryText + ', ' + aPrefix + Strings[i];
      finally
        Free;
      end;// with TStringList.Create do try
    end;// procedure SetVarFields(aPrefix: string; var aQueryText: string);
  begin
    // Query bauen
    result := 'INSERT INTO ' + fTempTableName + ' (';
  
    // FElder hinzuf�gen
    SetFixFields('', result);
    SetVarFields('', result);
    result := result + ') VALUES (';
  
    // Parameter hinzuf�gen
    SetFixFields(':', result);
    SetVarFields(':', result);
  
    result := result + ')';
  end;// function CreateWriteQueryText: String;
  
  (*-----------------------------------------------------------
    Liest die Daten aus dem ClassImage in den entsprechenden Record
  -------------------------------------------------------------*)
  procedure ReadClassDataInterval;
  var
    xStream: TMemoryStream;
    xData: Variant;
    xBuffer: PByte;
  begin
    xStream := TMemoryStream.Create;
    try
      // Um ein Variant Array zu bekommen, muss �ber das native ADO Objekt abgefragt werden
      xData := xReadQuery.Recordset.Fields[cFLD_ClassImage].Value;
      if xData <> NULL then begin
        // Liefert einen Pointer auf die Daten zur�ck
        xBuffer := VarArrayLock(xData);
        try
          // Daten in den Stream ablegen
          xStream.Write(xBuffer^, xReadQuery.Recordset.Fields[cFLD_ClassImage].ActualSize);
        finally
          // Nach Lock muss immer Unlock aufgerufen werden
          VarArrayUnLock(xData);
        end;
      end;// if xData <> NULL then begin
      xStream.Position := 0;
      // Daten im Record ablegen
      xStream.Read(xClassImageRec, sizeof(TClassFieldRec));
    finally
      xStream.Free;
    end;// try finally
  end;// procedure ReadClassDataInterval;
  
  (*-----------------------------------------------------------
    Gibt den Wert eines einzelnen Feldes zur�ck
  -------------------------------------------------------------*)
  function GetFieldValue(aField: string): Integer;
  var
    xFieldType: TFieldType;
    xFieldNumber: integer;
    xFound: boolean;
    i: TFieldType;
  begin
    Result := 0;
  
    // initialisieren
    i            := Low(TFieldType);
    xFound       := false;
    xFieldType   := ftClassCut;
    xFieldNumber := 0;
  
    // sucht die Nummer und den Typ des Feldes anhand des Namens
    while (i <= High(TFieldType)) and (not(xFound)) do begin
      if AnsiSameText(cFieldTypeStrings[i], copy(aField,1,Length(cFieldTypeStrings[i])))then begin
        xFieldType := i;
        xFieldNumber := StrToIntDef(copy(aField, Length(cFieldTypeStrings[i]) + 1, MAXINT),0);
        xFound := true;
      end;// if AnsiSameText() then begin
      inc(i);
    end;// while (i <= ord(High(TFieldType))) and( not(xFound)) do begin
  
    if xFound then begin
      // Je nach Typ des Feldes wird der Wert aus dem entsprechenden Teil des Records geholt
      case xFieldType of
        ftClassCut    : result := xClassImageRec.ClassCutField[xFieldNumber];
        ftClassUnCut  : result := xClassImageRec.ClassUncutField[xFieldNumber];
        ftSpiceCut    : result := xClassImageRec.SpCutField[xFieldNumber];
        ftSpliceUncut : result := xClassImageRec.SpUncutField[xFieldNumber];
        ftSiroCut     : result := xClassImageRec.SIROCutField[xFieldNumber];
        ftSiroUnCut   : result := xClassImageRec.SIROUncutField[xFieldNumber];
      else
        raise
          Exception.Create('TClassDefContainer.FillTempTable: FieldType ' + GetEnumName(TypeInfo(TFieldType),ord(xFieldType)) + ' not implemented');
      end;// case xFieldType of
    end;// if xFound then begin
  end;// function GetFieldValue(aField: string): Integer;
  
begin
  xQueryText := nil;
  xReadQuery := GetQuery(cReadQueryIndex);
  xWriteQuery := GetQuery(cWriteQueryIndex);
  (* Zus�tzliche Felder holen (Nur die Schl�sselspalten sind fest, die restlichen
     Felder ergeben sich aus den enthaltenen Klassendefinitionen *)
  xFieldList := GetFields(false, true);

//: ----------------------------------------------
  // Zuerst einmal das Query generieren. Dazu wird eine Helperklasse eingesetzt
  xIQB := TIntervalQueryBuilder.Create;
  try
    xQueryText := TStringList.Create;
  
    xIQB.QueryParameters := FQueryParameters;
    xIQB.GetQueryText(xQueryText);
  
    xReadQuery.SQL.Text := xQueryText.Text;
  
    codeSite.Sendstringlist('ReadQuery',xQueryText);
  finally
    FreeAndNil(xIQB);
    FreeAndNil(xQueryText);
  end;// try finally

//: ----------------------------------------------
  // Daten abf�llen
  xReadQuery.Open;
  
  while not(xReadQuery.EOF) do begin
    // Das Schreib Query generieren (Alle Felder werden als Parameter generiert)
    xWriteQuery.SQL.Text := CreateWriteQueryText;
  
    (* F�r den fixen Teil des Query's wird das Konstanten Array cKeyFields verwendet.
       Dasselbe Konstanten Array wird f�r das generieren der tempor�ren Tabelle verwendet *)
    for i := Low(cKeyFields) to High(cKeyFields) do
      xWriteQuery.ParamByName(cKeyFields[i].Name).asString := xReadQuery.FieldByName(cKeyFields[i].Name).asString;
  
    // Extrahiert einen einzelnen Datensatz aus dem Class Image und legt die Daten in die 'xClassImageRec' ab
    ReadClassDataInterval;
    // Jetzt die Daten in die Parameter zum schreiben abf�llen
    for i := High(cKeyFields) - Low(cKeyFields) + 1 to xWriteQuery.Params.Count - 1 do
      xWriteQuery.Params[i].asInteger := GetFieldValue(xWriteQuery.Params[i].Name);
  
    // Einzelnen Datensatz schreiben
    xWriteQuery.ExecSQL;
  
    // solange bis alle Datens�tzte in der tempor�ren Tabelle gelandet sind
    xReadQuery.next;
  end;// while not(xQuery.EOF) do begin
end;// TClassDefContainer.FillTempTable cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           GetFields
 *  Klasse:           TClassDefContainer
 *  Kategorie:        Datenbank 
 *  Argumente:        (aCompleteFieldDef, aOnlyTClassDef)
 *
 *  Kurzbeschreibung: Gibt die beteiligten Felder als Komma separierten string zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TClassDefContainer.GetFields(aCompleteFieldDef: boolean; aOnlyTClassDef: boolean = false): String;
var
  i: Integer;
  xEnable: Boolean;
  xSQLSelectFields: string;
begin
  result := '';
  // Zuerst alle Klassendefinitionen zusammenlegen
  for i := 0 to DataItemCount - 1 do begin
    xEnable := true;
    if   (not(aOnlyTClassDef))
         or ((aOnlyTClassDef) and (DataItems[i].DataItemType.InheritsFrom(TClassDef))) then begin
      DoGetDataItemGenerateSQL(self, DataItems[i], xEnable);
      if xEnable then begin
        xSQLSelectFields := DataItems[i].GetSQLSelectFields(false, false);
        if xSQLSelectFields > '' then
          result := result + ',' + xSQLSelectFields;
      end;// if xEnable then begin
    end;// if (not(aOnlyTClassDef)) or (...))
  end;// for i := 0 to DataItemCount - 1 do begin
  
  // Wenn Klassendefinitionen vorhanden sind, dann ist das erste Zeichen ein Komma
  if result <> '' then
    system.Delete(result,1,1);
  
  // Jetzt die Doppelg�nger eliminieren
  with TStringList.Create do try
    // Alle Felder einf�gen
    CommaText := result;
  
    // Um die Duplikate zu elimminieren, muss die Liste sortiert sein
    Sorted := true;
    // Duplikate eliminieren
    i := 0;
    while i < Count - 1 do begin
      if Trim(Strings[i]) = '' then begin
        delete(i);
        continue;
       end;// if Strings[i] = '' then begin
      if Strings[i] = Strings [i + 1] then
        Delete(i)
      else
        inc(i);
    end;// while i < Count - 1 do begin
  
    if aCompleteFieldDef then begin
      // Sortierung wieder ausschalten um die Strings editieren zu k�nnen
      Sorted := false;
      // Felddefinitionen hinzuf�gen
      for i := 0 to Count - 1 do
        Strings[i] := strings[i] + ' int NULL';
    end;// if aCompleteFieldDef then begin
  
    (* Und wieder dem Resultat zuweisen
       Da CommaText Strings mit Leerzeichen in Anf�hrungszeichen setzt,
       m�ssen diese Entfernt werden*)
    result := StringReplace(CommaText, '"', '', [rfReplaceAll]);
  
  finally
    Free;
  end;// with TStringList.Create do try
end;// TClassDefContainer.GetFields cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           GetQuery
 *  Klasse:           TClassDefContainer
 *  Kategorie:        No category 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: Gibt das Query der TADODBAccess Instanz zur�ck
 *  Beschreibung:     
                      Ist die Instanz noch nicht initialisiert, dann wird dies 
                      hier erledigt.
 --------------------------------------------------------------------*)
function TClassDefContainer.GetQuery(aIndex: integer = 0): TNativeAdoQuery;
begin
  assert(aIndex < cClassContainerQueryCount);

//: ----------------------------------------------
  // Wenn die DB noch nicht initialisiert ist, dann noch initialisieren
  if not(mDB.Initialized) then
    mDB.init;
  
  result := mDB.Query[aIndex];
end;// TClassDefContainer.GetQuery cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetSQLFromTables
 *  Klasse:           TClassDefContainer
 *  Kategorie:        Datenbank 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die beteiligten Tabellen zur�ck, so dass der String direkt in einem Statement verwendet 
 werden kann
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TClassDefContainer.GetSQLFromTables: String;
var
  xUsedTables: TUsedTablesSet;
begin
  result := '';
  
  // Die ben�tigten Tabellen abfragen
  xUsedTables := UsedTables;

//: ----------------------------------------------
  (* F�r jede beteiligte Tabelle muss ein Eintrag erzeugt werden. Die Beteiligten Tabellen
     werden zuerst �ber das Property 'UsedTables' ermittelt und liegen danach
     in 'FUsedTables' *)
  
  // F�r die Intervall Daten kommen alle Felder aus der tempor�ren Tabelle
  if ([utIntervalClassCut, utIntervalClassUncut, utIntervalSpliceCut,
       utIntervalSpliceUncut, utIntervalFFCut, utIntervalFFUncut] * xUsedTables) <> [] then begin
    result := result + ',' + TempTableName + ' ' + cTableAlias[utIntervalClassCut];
  end;// if ([...] * FUsedTables) > [] then begin

//: ----------------------------------------------
  if utShiftClassCut in xUsedTables then
    result := result + ',' + cUsedTablesArray[utShiftClassCut] + ' ' + cTableAlias[utShiftClassCut];

//: ----------------------------------------------
  if utShiftClassUnCut in xUsedTables then
    result := result + ',' + cUsedTablesArray[utShiftClassUnCut] + ' ' + cTableAlias[utShiftClassUnCut];

//: ----------------------------------------------
  if utShiftSpliceCut in xUsedTables then
    result := result + ',' + cUsedTablesArray[utShiftSpliceCut] + ' ' + cTableAlias[utShiftSpliceCut];

//: ----------------------------------------------
  if utShiftSpliceUncut in xUsedTables then
    result := result + ',' + cUsedTablesArray[utShiftSpliceUncut] + ' ' + cTableAlias[utShiftSpliceUncut];

//: ----------------------------------------------
  if ([utShiftFFCut,utShiftFFUncut] * xUsedTables) <> [] then
    result := result + ',' + cUsedTablesArray[utShiftFFCut] + ' ' + cTableAlias[utShiftFFCut];

//: ----------------------------------------------
  if utLongTermClassCut in xUsedTables then
    result := result + ',' + cUsedTablesArray[utLongTermClassCut] + ' ' + cTableAlias[utLongTermClassCut];

//: ----------------------------------------------
  if utLongTermClassUncut in xUsedTables then
    result := result + ',' + cUsedTablesArray[utLongTermClassUncut] + ' ' + cTableAlias[utLongTermClassUncut];

//: ----------------------------------------------
  if utLongTermSpliceCut in xUsedTables then
    result := result + ',' + cUsedTablesArray[utLongTermSpliceCut] + ' ' + cTableAlias[utLongTermSpliceCut];

//: ----------------------------------------------
  if utLongTermSpliceUncut in xUsedTables then
    result := result + ',' + cUsedTablesArray[utLongTermSpliceUncut] + ' ' + cTableAlias[utLongTermSpliceUncut];

//: ----------------------------------------------
  if ([utLongTermFFCut, utLongTermFFUncut] * xUsedTables) <> [] then
    result := result + ',' + cUsedTablesArray[utLongTermFFCut] + ' ' + cTableAlias[utLongTermFFCut];

//: ----------------------------------------------
  (* Wurde mindestens eine Tabelle hinzugef�gt, dann ist das erste Zeichen ein ','.
     Dieses muss jetzt entfernt werden *)
  if result > '' then
    system.delete(result, 1, 1);
end;// TClassDefContainer.GetSQLFromTables cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           GetSQLSelectFields
 *  Klasse:           TClassDefContainer
 *  Kategorie:        Datenbank 
 *  Argumente:        (aWithSum, aWithAlias)
 *
 *  Kurzbeschreibung: Liefert die beteiligten Felder der Klassendefinition
 *  Beschreibung:     
                      Mit dem Argument kann bestimmt werden ob die Felder f�r 
                      eine Gruppierung summiert werden ('sum(c_cC1 * 1.0) as c_cC1, sum(c_cC2 * 1.0) as c_cC2')
                      oder nicht ('c_cC1, c_cC2').
 --------------------------------------------------------------------*)
function TClassDefContainer.GetSQLSelectFields(aWithSum: boolean = true; aWithAlias: boolean = true): String;
var
  i: Integer;
  xEnable: Boolean;
  xSelectDataItemFields: string;
begin
  result := '';
  
  // TimeMode und LinkAlias an alle DataItems senden
  for i := 0 to DataItemCount - 1 do begin
    xEnable := true;
    // Abfragen ob das DataItem verwendet werden darf
    DoGetDataItemGenerateSQL(self, DataItems[i], xEnable);
    if xEnable then begin
      // Feld Definitionen des DataItems abfragen
      xSelectDataItemFields := DataItems[i].GetSQLSelectFields(aWithSum, aWithAlias);
      // Nicht jedes DataItem gibt Feld Definitionen zur�ck
      if xSelectDataItemFields <> '' then
        result := result + ',' + xSelectDataItemFields;
    end;// if xEnable then begin
  end;// for i := 0 to DataItemCount - 1 do begin
  
  // Wenn mindestens ein Feld hinzugef�gt wurde muss das f�hrende Komma entfernt werden
  if Length(result) > 0 then
    system.Delete(result, 1, 1);
end;// TClassDefContainer.GetSQLSelectFields cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           GetSQLWhere
 *  Klasse:           TClassDefContainer
 *  Kategorie:        Datenbank 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Beinhaltet die Where-Klausel (Join) die die Felder betreffen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TClassDefContainer.GetSQLWhere: String;
var
  xUsedTables: TUsedTablesSet;
  
  function GetWherePart(aField: string; aUsedTable: TUsedTable): String;
  begin
    result := Format (' AND %s.%s = %s.%s',[cTableAlias[aUsedTable], aField, LinkAlias, aField]);
  end;// function GetWherePart(aField: string; aUsedTable: TUsedTable): String;
  
begin
  result := '';
  
  // Die ben�tigten Tabellen abfragen
  xUsedTables := UsedTables;

//: ----------------------------------------------
  (* F�r jede beteiligte Tabelle muss ein Eintrag erzeugt werden. Die Beteiligten Tabellen
     werden zuerst �ber das Property 'UsedTables' ermittelt und liegen danach
     in 'FUsedTables' *)
  
  // F�r die Intervall Daten kommen alle Felder aus der tempor�ren Tabelle
  if ([utIntervalClassCut, utIntervalClassUncut, utIntervalSpliceCut,
       utIntervalSpliceUncut, utIntervalFFCut, utIntervalFFUncut] * xUsedTables) <> [] then begin
    result := result + GetWherePart('c_interval_id', utIntervalClassCut);
    result := result + GetWherePart('c_spindle_id', utIntervalClassCut);
    result := result + GetWherePart('c_prod_id', utIntervalClassCut);
    result := result + GetWherePart('c_machine_id', utIntervalClassCut);
  end;// if ([...] * FUsedTables) > [] then begin

//: ----------------------------------------------
  if utShiftClassCut in xUsedTables then
    result := result + GetWherePart('c_classcut_id', utShiftClassCut);

//: ----------------------------------------------
  if utShiftClassUnCut in xUsedTables then
    result := result + GetWherePart('c_classuncut_id', utShiftClassUnCut);

//: ----------------------------------------------
  if utShiftSpliceCut in xUsedTables then
    result := result + GetWherePart('c_splicecut_id', utShiftSpliceCut);

//: ----------------------------------------------
  if utShiftSpliceUncut in xUsedTables then
    result := result + GetWherePart('c_spliceuncut_id', utShiftSpliceUncut);

//: ----------------------------------------------
  if ([utShiftFFCut,utShiftFFUncut] * xUsedTables) <> [] then
    result := result + GetWherePart('c_sirocutuncut_id', utShiftFFCut);

//: ----------------------------------------------
  if utLongTermClassCut in xUsedTables then
    result := result + GetWherePart('c_classcut_id', utLongTermClassCut);

//: ----------------------------------------------
  if utLongTermClassUncut in xUsedTables then
    result := result + GetWherePart('c_classuncut_id', utLongTermClassUncut);

//: ----------------------------------------------
  if utLongTermSpliceCut in xUsedTables then
    result := result + GetWherePart('c_splicecut_id', utLongTermSpliceCut);

//: ----------------------------------------------
  if utLongTermSpliceUncut in xUsedTables then
    result := result + GetWherePart('c_spliceuncut_id', utLongTermSpliceUncut);

//: ----------------------------------------------
  if ([utLongTermFFCut, utLongTermFFUncut] * xUsedTables) <> [] then
    result := result + GetWherePart('c_sirocutuncut_id', utLongTermFFCut);
end;// TClassDefContainer.GetSQLWhere cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           GetUniqueFieldName
 *  Klasse:           TClassDefContainer
 *  Kategorie:        No category 
 *  Argumente:        (aDisplayName)
 *
 *  Kurzbeschreibung: Gibt einen eindeutigen Bezeichner f�r den �bergebnen Namen zur�ck
 *  Beschreibung:     
                      Es kann bereits ein Element gleichen Namens in der Liste stehen.
                      Ist dies der Fall, muss ein eindeutiger Name f�r das Feld f�r
                      die SQL Abfrage generiert werden. Dieser eindeutige Namen kann 
                      dann der Eigenschaft 'FieldName' zugewiesen werden. 
                      Anhand dieses Namens kann dann im Recordset das Feld mit dem
                      Ergebnis identifiziert werden.
                      
                      Die Funktion probiert solange die Nummer zu erh�hen bis der Name 
                      eindutig ist. Dies geschieht mit Rekursion.
                      
                      Es ist nicht zu erwarten, dass das hochz�hlen ein Problem
                      wird, da kaum so viele gleichnahmige Items in einem Container
                      Sinn machen. In der Prasis werden es warscheinlich eher 
                      max 2 gleiche Items sein.
 --------------------------------------------------------------------*)
function TClassDefContainer.GetUniqueFieldName(const aDisplayName: string): String;
var
  i: Integer;
  xFound: Boolean;
  xNumber: Integer;
  xPosIdentifier: Integer;
  xNumberString: string;
  
  const
    (* Muss der Name aufnummeriert werden, dann wird dieser String und eine laufende Nummer
       an den Namen angeh�ngt.
       Aus 'test' wird dann 'test!cdc!_1' oder 'test!cdc!_2' usw.
       'cdc' steht f�r 'ClassDefContainer'*)
    cUniqueIdentifier = '!!cdc!_';
  
begin
  // Initialisieren
  i := 0;
  xFound := false;
  result := aDisplayName;

//: ----------------------------------------------
  (* solange suchen bis alle Items durchlaufen sind, oder ein Item
     mit dem gleichen 'FiledName' gefunden wurde *)
  while (i < DataItemCount) and (not(xFound)) do begin
    if AnsiSameText(aDisplayName, DataItems[i].FieldName) then begin
      // Leider gibt es diesen Bezeichner bereits
      xFound := true;
  
      result := aDisplayName;
      (* Position des Identifiers im Namen feststellen
         (erst ab dem dritten identischen FieldName vorhanden) *)
      xPosIdentifier := AnsiPos(cUniqueIdentifier, aDisplayName);
      if xPosIdentifier > 0 then begin
        // Hier ist die Nummer bereits mindestens 2
        xNumberString := copy(result, xPosIdentifier + Length(cUniqueIdentifier), MAXINT);
        // Urspr�nglicher Name
        result := copy(result, 1, xPosIdentifier - 1);
        xNumber := StrToIntDef(xNumberString, -1);
        inc(xNumber);
        // Neuen FieldName zusammensetzen
        result := result + cUniqueIdentifier + IntToStr(xNumber);
      end else begin
        // Das zweite Item mit dem selben FieldName
        result := result + cUniqueIdentifier + '2';
      end;// if xPosIdentifier > 0 then begin
      // rekursiver Aufruf dieser Funktion bis ein eindeutiger Name feststeht
      result := GetUniqueFieldName(result);
    end;// if AnsiSameText(aDisplayName, DataItems[i].FieldName) then begin
    // N�chstes Item
    inc (i);
  end;// while (i < DataItemCount) and (not(xFound)) do begin
end;// TClassDefContainer.GetUniqueFieldName cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetUsedTables
 *  Klasse:           TClassDefContainer
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Fragt die Tabellen ab die f�r die  DataItems ben�tigt werden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TClassDefContainer.GetUsedTables: TUsedTablesSet;
var
  i: Integer;
  xEnable: Boolean;
begin
  result := [];
  
  for i := 0 to DataItemCount - 1 do begin
    xEnable := true;
    // Abfragen ob das DataItem verwendet werden darf
    DoGetDataItemGenerateSQL(self, DataItems[i], xEnable);
    if xEnable then
      result := result + DataItems[i].TableType;
  end;// for i := 0 to DataItemCount - 1 do begin
end;// TClassDefContainer.GetUsedTables cat:No category

//:-------------------------------------------------------------------
(*: Member:           LoadFromDatabase
 *  Klasse:           TClassDefContainer
 *  Kategorie:        Datenbank 
 *  Argumente:        (aDataItemDecoratorClass)
 *
 *  Kurzbeschreibung: L�dt die Classen Definitionen von der Datenbank
 *  Beschreibung:     
                      Die bisherigen Eintr�ge werden zuerst gel�scht. Wenn also
                      noch weitere Datenitems in der Liste sind, dann m�ssen diese zuerst
                      nachher wieder hinzugef�gt werden werden.
 --------------------------------------------------------------------*)
procedure TClassDefContainer.LoadFromDatabase(aDataItemDecoratorClass: TDataItemDecoratorClass = nil);
begin
  // Zuerst die gesammte Liste L�schen
  Clear;

//: ----------------------------------------------
  inherited LoadFromDatabase(aDataItemDecoratorClass);
end;// TClassDefContainer.LoadFromDatabase cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           PrepareTable
 *  Klasse:           TClassDefContainer
 *  Kategorie:        Datenbank 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Bereitet die Tabelle vor, wenn eine tempor�re Tabelle erzeugt werden muss
 *  Beschreibung:     
                      Die tempor�re Tabelle wird nur f�r Intervalldaten ben�tigt.
                      Sie enth�lt die Intervalldaten f�r die interessierenden Schichten.
                      Es wird f�r alle Schnittdaten eine gemeinsame Tabelle erzeugt - Es sind
                      allerdings nur jene Felder vorhanden, die in den Klassendefinitionen
                      gesetzt sind. 
                      In der erzeugten Tabelle sind jetzt also f�r jedes Intervall
                      alle Felder verf�gbar die in den enthaltenen Klassendefinitionen gew�hlt sind.
                      Das Query das generiert wird, kann so auf die selben Felder zugreifen, wie auch 
                      in den Tabellen 't_dw_classCut' und Konsorten.
 --------------------------------------------------------------------*)
procedure TClassDefContainer.PrepareTable;
var
  xCursor: TCursor;
begin
  // Cursor f�r den Fall sichern
  xCursor := Screen.Cursor;

//: ----------------------------------------------
  // Ben�tigte Parameter auf die DataItems �bertragen
  SetDataItemParameters;
  
  if assigned(QueryParameters) then begin
    // Im Fall von Intervall Daten muss die tempor�re Tabelle erzeugt werden
    if QueryParameters.TimeMode = tmInterval then begin
      try
        Screen.Cursor := crSQLWait;
        (* Die tempor�re Tabelle wird mit allen ben�tigten Feldern
           erzeugt.
           ACHTUNG Die tempor�re Tabelle hat nur solange der SQL Server l�uft,
           und die Connection, welche die Tabelle erzeugt hat existiert
           (in diesem Fall mDB), Bestand *)
        CreateTempTable;
      finally
        Screen.Cursor := xCursor;
      end;// try finally
    end;// if QueryParameters.TimeMode = tmInterval then begin
  end;// if assigned(QueryParameters) then begin
end;// TClassDefContainer.PrepareTable cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           SetDataItemParameters
 *  Klasse:           TClassDefContainer
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Setzt f�r alle Elemente die Parameter die f�r das erzeugen des Query's ben�tigt werden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TClassDefContainer.SetDataItemParameters;
var
  i: Integer;
begin
  for i := 0 to DataItemCount - 1 do begin
    if assigned(QueryParameters) then
      DataItems[i].TimeMode := QueryParameters.TimeMode;
    DataItems[i].LinkAlias := LinkAlias;
  end;// for i := 0 to DataItemCount - 1 do begin
end;// TClassDefContainer.SetDataItemParameters cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetLinkAlias
 *  Klasse:           TClassDefContainer
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt den Alias der verlinkten Tabelle
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TClassDefContainer.SetLinkAlias(Value: String);
var
  i: Integer;
begin
  FLinkAlias := Value;

//: ----------------------------------------------
  for i := 0 to DataItemCount - 1 do
    DataItems[i].LinkAlias := FLinkAlias;
end;// TClassDefContainer.SetLinkAlias cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetQueryParameters
 *  Klasse:           TClassDefContainer
 *  Kategorie:        Datenbank 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r QueryParameters
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TClassDefContainer.SetQueryParameters(Value: TQueryParameters);
begin
  // QueryParameters kopieren
  if assigned(Value) then begin
    if not(assigned(FQueryParameters)) then
      FQueryParameters := TQueryParameters.Create;
  
    // Die Parameter werden kopiert
    FQueryParameters.Assign(Value);
  end else begin
    FreeAndNil(FQueryParameters);
  end;// if assigned(Value) then begin
end;// TClassDefContainer.SetQueryParameters cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TIntervalQueryBuilder
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TIntervalQueryBuilder.Create;
begin
  inherited Create;

//: ----------------------------------------------
  QueryParameters := nil;
end;// TIntervalQueryBuilder.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TIntervalQueryBuilder
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TIntervalQueryBuilder.Destroy;
begin
  FreeAndNil(FQueryParameters);

//: ----------------------------------------------
  inherited Destroy;
end;// TIntervalQueryBuilder.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetFrom
 *  Klasse:           TIntervalQueryBuilder
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt den From Teil des Query's zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TIntervalQueryBuilder.GetFrom: String;
begin
  result := 'FROM v_production_interval v';
end;// TIntervalQueryBuilder.GetFrom cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetGroupBy
 *  Klasse:           TIntervalQueryBuilder
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt den GroupBy Teil des Query's zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TIntervalQueryBuilder.GetGroupBy: String;
begin
  result := '';
end;// TIntervalQueryBuilder.GetGroupBy cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetHaving
 *  Klasse:           TIntervalQueryBuilder
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt den Having Teil des Query's zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TIntervalQueryBuilder.GetHaving: String;
begin
  result := '';
end;// TIntervalQueryBuilder.GetHaving cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetOrderBy
 *  Klasse:           TIntervalQueryBuilder
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt den Order By Teil des Query's zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TIntervalQueryBuilder.GetOrderBy: String;
begin
  result := '';
end;// TIntervalQueryBuilder.GetOrderBy cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetQueryText
 *  Klasse:           TIntervalQueryBuilder
 *  Kategorie:        No category 
 *  Argumente:        (aStringList)
 *
 *  Kurzbeschreibung: Gibt das komplete Query f�r die Intervalldaten zur�ck
 *  Beschreibung:     
                      Die Stringlist wird erzeugt, wenn die �bergebene StringList
                      nil ist. F�r die Freigabe muss der Aufrufer sorgen.
                      Der Zugriff auf einzelne Teile des Query's sollte �ber 
                      die Elemente der Aufz�hlung TQueryFragmentOrder erfolgen
                      [zB ord(qfoWhere)].
 --------------------------------------------------------------------*)
procedure TIntervalQueryBuilder.GetQueryText(aStringList: TStringList);
begin
  // Initialisieren
  aStringList.Clear;
  
  // Alle Teile des Querys abf�llen
  aStringList.Add(GetSelect);
  (* Ein Leerzeichen einf�gen, damit der Text auch
     ohne Nachbearbeitung verwendet werden kann *)
  aStringList.Add(' ' + GetFrom);
  aStringList.Add(' ' + GetWhere);
  aStringList.Add(' ' + GetGroupBy);
  aStringList.Add(' ' + GetHaving);
  aStringList.Add(' ' + GetOrderBy);
end;// TIntervalQueryBuilder.GetQueryText cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetSelect
 *  Klasse:           TIntervalQueryBuilder
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt den Select Teil des Query's zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TIntervalQueryBuilder.GetSelect: String;
var
  i: Integer;
begin
  // die gew�nschten Felder aus dem Konstanten Array 'cKeyFields' zusammensetzen
  result := 'SELECT ';
  for i := Low(cKeyFields) to High(cKeyFields) do
      result := result + 'v.' + cKeyFields[i].Name + ', ';
  result := result + ' v.c_class_image ';
end;// TIntervalQueryBuilder.GetSelect cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetWhere
 *  Klasse:           TIntervalQueryBuilder
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt den WHERE Teil des Statements zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TIntervalQueryBuilder.GetWhere: String;
  
  (*-----------------------------------------------------------
    Konvertiert einen TDateTime Wert in einen String
    Dies ist notwendig, da Delphi und der SQL Server unterschiedliche
    Startdaten haben (Delphi 30.12.1899, SQL Server 1.1.1900).
  -------------------------------------------------------------*)
  function ConvertDateTimeToSQL_Ex(aDate: TDateTime) : string;
    function Pad2(s: string) : string;
    begin
      result := s;
      if (Length(result) < 2) then
        result := '0' + result;
    end;// function Pad2(s: string) : string;
  var
    Hour, Min, Sec, MSec : Word;
    Year, Month, Day     : Word;
  begin
    DecodeDate(aDate, Year, Month, Day);
    DecodeTime(aDate, Hour, Min, Sec, MSec);
    Result := '';
    Result := Result + IntToStr(Year) + '-' + Pad2(IntToStr(Month)) + '-' + Pad2(IntToStr(Day));
    Result := Result + ' ';
    Result := Result + Pad2(IntToStr(Hour)) + ':' + Pad2(IntToStr(Min)) + ':' + Pad2(IntToStr(Sec));
    Result := Result + '.' + IntToStr(MSec);
    Result := #39 + Result + #39;
  end;// function ConvertDateTimeToSQL_Ex(aDate: TDateTime) : string;
  
begin
  result := '';
  if assigned(FQueryParameters) then begin

//: ----------------------------------------------
    if QueryParameters.ProdIDs <> '' then begin
      if result > '' then
        result := result + ' AND ';
      result := result + 'v.c_prod_id in (' + QueryParameters.ProdIDs + ') ';
    end;// if QueryParameters.ProdIDs <> '' then begin

//: ----------------------------------------------
    if QueryParameters.MachIDs <> '' then begin
      if result > '' then
        result := result + ' AND ';
      result := result + 'v.c_machine_id in (' + QueryParameters.MachIDs + ') ';
    end;// if QueryParameters.MachIDs <> '' then begin

//: ----------------------------------------------
    if QueryParameters.SpindleIDs <> '' then begin
      if result > '' then
        result := result + ' AND ';
      result := result + 'v.c_spindle_id in (' + QueryParameters.SpindleIDs + ') ';
    end;// if QueryParameters.SpindleIDs <> '' then begin

//: ----------------------------------------------
    if QueryParameters.StyleIDs <> '' then begin
      if result > '' then
        result := result + ' AND ';
      result := result + 'v.c_style_id in (' + QueryParameters.StyleIDs + ') ';
    end;// if QueryParameters.StyleIDs <> '' then begin

//: ----------------------------------------------
    if QueryParameters.YMSetIDs <> '' then begin
      if result > '' then
        result := result + ' AND ';
      result := result + 'v.c_YM_set_id in (' + QueryParameters.YMSetIDs + ') ';
    end;// if QueryParameters.YMSetIDs <> '' then begin

//: ----------------------------------------------
    // Time ID's (Interval ID's) haben die h�here Priorit�t
    if QueryParameters.TimeIDs <> '' then begin
      if result > '' then
        result := result + ' AND ';
      result := result + 'v.c_interval_id in (' + QueryParameters.TimeIDs + ') ';
    end;// if QueryParameters.TimeIDs <> '' then begin

//: ----------------------------------------------
    if result > '' then
      result := result + ' AND ';
    result := result + 'v.c_shiftcal_id =' + IntToStr(QueryParameters.ShiftCalID) + ' ';

//: ----------------------------------------------
  end;// if assigned(FQueryParameters) then begin

//: ----------------------------------------------
  if result > '' then
    result := 'WHERE ' + result;
end;// TIntervalQueryBuilder.GetWhere cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetQueryParameters
 *  Klasse:           TIntervalQueryBuilder
 *  Kategorie:        Datenbank 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r QueryParameters
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TIntervalQueryBuilder.SetQueryParameters(Value: TQueryParameters);
begin
  // Die QueryParameters werden kopiert
  if assigned(Value) then begin
    if not(assigned(FQueryParameters)) then
      FQueryParameters := TQueryParameters.Create;
    FQueryParameters.Assign(Value);
  end else begin
    FreeAndNil(FQueryParameters);
  end;// if assigned(Value) then begin
end;// TIntervalQueryBuilder.SetQueryParameters cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TCompositClassDef
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      Der MatrixType ist f�r KompositClassDefs immer mtNone.
 --------------------------------------------------------------------*)
constructor TCompositClassDef.Create;
begin
  inherited Create;

//: ----------------------------------------------
  FDataItemList := TStringList.Create;

//: ----------------------------------------------
  // Bei Composit Items ist der MatrixTyp immer mtNone
  MatrixType := mtNone;
end;// TCompositClassDef.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TCompositClassDef
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TCompositClassDef.Destroy;
begin
  FreeAndNil(FDataItemList);

//: ----------------------------------------------
  inherited Destroy;
end;// TCompositClassDef.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TCompositClassDef
 *  Kategorie:        No category 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Kopiert die Eigenschaften von Source
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TCompositClassDef.Assign(Source: TPersistent);
var
  xCompositClassDef: TCompositClassDef;
begin
  if (Source is TCompositClassDef) then begin
    xCompositClassDef := TCompositClassDef(Source);
  
    (* Eigenschaften kopieren:
       Die StringList(FDataItemList) muss nicht kopiert werden,
       da sie neu erzeugt wird wenn TClassDef.MatrixDef gesetzt wird (inherited Aufruf)  *)
    ClassDefList := xCompositClassDef.ClassDefList;
  
  end;// if (Source is TClassDef) then begin

//: ----------------------------------------------
  (* !!! ACHTUNG !!!
     Modified darf erst am Schluss �bernommmen werden, da einige Eigenschaften
     bei der Zuweisung das Flag auf True setzen.
     Wenn inherited am Ende aufgeruffen wird, muss Modified nicht erneut gesetzt werden *)
  inherited Assign(Source)
end;// TCompositClassDef.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetDataItemCount
 *  Klasse:           TCompositClassDef
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r DataItemCount
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TCompositClassDef.GetDataItemCount: Integer;
begin
  result := FDataItemList.Count;
end;// TCompositClassDef.GetDataItemCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetDataItemName
 *  Klasse:           TCompositClassDef
 *  Kategorie:        No category 
 *  Argumente:        (Index)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r DataItemName[Index]
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TCompositClassDef.GetDataItemName(Index: Integer): String;
begin
  result := '';
  
  if Index < FDataItemList.Count  then
    result := FDataItemList[Index];
end;// TCompositClassDef.GetDataItemName cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMathOperation
 *  Klasse:           TCompositClassDef
 *  Kategorie:        No category 
 *  Argumente:        (Index)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r MathOperation[Index]
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TCompositClassDef.GetMathOperation(Index: Integer): TMathOperation;
begin
  result := moNone;
  
  if Index < FDataItemList.Count  then
    if integer(FDataItemList.Objects[Index]) <= ord(High(TMathOperation)) then
      result := TMathOperation(FDataItemList.Objects[Index]);
end;// TCompositClassDef.GetMathOperation cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMathOpFromString
 *  Klasse:           TCompositClassDef
 *  Kategorie:        No category 
 *  Argumente:        (aOp)
 *
 *  Kurzbeschreibung: Gibt die Operation(Typ) aus einem String zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TCompositClassDef.GetMathOpFromString(aOp: string): TMathOperation;
var
  i: TMathOperation;
begin
  // Initialisieren
  result := moNone;
  i := Low(TMathOperation);

//: ----------------------------------------------
  // Sucht die richtige Operation
  while (i < High(TMathOperation)) and (result = moNone) do begin
    inc(i);
    if AnsiSameText(aOP, cMathOperation[i]) then
      // richtige Operation gefunden
      result := i;
  end;// while (i < High(TMathOperation)) and (result = moNone) do begin
end;// TCompositClassDef.GetMathOpFromString cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMatrixDef
 *  Klasse:           TCompositClassDef
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r die Eigenschaft "MatrixDef" von TClassDef
 *  Beschreibung:     
                      Die Methode wird �berschrieben, da keine Matrixdefinition 
                      vorhanden ist, sondern eine Definition der verwendeten 
                      DataItems (Name und Operation)
 --------------------------------------------------------------------*)
function TCompositClassDef.GetMatrixDef: String;
var
  i: Integer;
begin
  result := '';

//: ----------------------------------------------
  with TStringList.Create do try
    for i := 0 to FDataItemList.Count - 1 do begin
      // Zuerst das DataItem, ...
      Add(DataItemList[i]);
      // ... dann die Operation als String
      Add(cMathOperation[MathOperation[i]]);
    end;// for i := 0 to FDataItemList.Count - 1 do begin
    // Liste als Komma separierter String speichern
    result := CommaText;
  finally
    free;
  end;// with TStringList.Create do try
end;// TCompositClassDef.GetMatrixDef cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetSQLFromTable
 *  Klasse:           TCompositClassDef
 *  Kategorie:        Datenbank 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Beinhaltet die Tabelle mit dem Tabellenalias von der die Daten gesammelt werden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TCompositClassDef.GetSQLFromTable: String;
var
  xDataItem: TBaseDataItem;
  i: Integer;
begin
  result := '';

//: ----------------------------------------------
  if assigned(ClassDefList) then begin
    for i := 0 to FDataItemList.Count - 1 do begin
      xDataItem := ClassDefList.DataItemByName[FDataItemList[i]];
      if assigned(xDataItem) then begin
        // Properties in den globalen DataItems setzten, damit alle Queries richtig erzeugt werden k�nnen
        SetRequiredProps(xDataItem);
        // Holt die Definition beim entsprechenden DataItem ab
        result := result + xDataItem.GetSQLFromTable;
      end;// if assigned(xDataItem) then begin
    end;// for i := 0 to FDataItemList.Count - 1 do begin
  end else begin
    raise Exception.Create('TCompositClassDef.GetSQLFromTable: ClassDefList unassigned.');
  end;// if assigned(ClassDefList) then begin
end;// TCompositClassDef.GetSQLFromTable cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           GetSQLSelectFields
 *  Klasse:           TCompositClassDef
 *  Kategorie:        Datenbank 
 *  Argumente:        (aWithSum, aWithAlias)
 *
 *  Kurzbeschreibung: Gibt die Spalten zur�ck die im SELECT Statement stehen m�ssen
 *  Beschreibung:     
                      Mit dem Argument kann bestimmt werden ob die Felder f�r 
                      eine Gruppierung summiert werden ('sum(c_cC1 * 1.0) as c_cC1, sum(c_cC2 * 1.0) as c_cC2')
                      oder nicht ('c_cC1, c_cC2').
 --------------------------------------------------------------------*)
function TCompositClassDef.GetSQLSelectFields(aWithSum: boolean = true; aWithAlias:boolean = true): String;
var
  i: Integer;
  xDataItem: TBaseDataItem;
begin
  result := '';

//: ----------------------------------------------
  if assigned(ClassDefList) then begin
    for i := 0 to FDataItemList.Count - 1 do begin
      // DataItem aus der globalen Liste holen
      xDataItem := ClassDefList.DataItemByName[FDataItemList[i]];
      if assigned(xDataItem) then begin
        // Properties in den globalen DataItems setzten, damit alle Queries richtig erzeugt werden k�nnen
        SetRequiredProps(xDataItem);
        (* aWithSum ist TRUE f�r die Abfrage der Felder der tempor�ren Tabelle, deshalb darf
           in diesem Fall auch die mathematische Operation nicht angegeben werden. *)
        if aWithSum then begin
          // z.B.: 'SUM(v.c_CSiro) + '
          result := Format('%s %s %s', [result, xDataItem.GetSQLSelectFields(aWithSum, false), cMathOperation[MathOperation[i]]])
        end else begin
          (* Keine math. Operation und nur f�r TClassDefItems, da nur f�r diese Felder
             die tempor�re Tabelle erzeugt werden muss *)
          if xDataItem.DataItemType.inheritsFrom(TClassDef) then begin
            if result > '' then
              result := result + ',';
            // ohne Summe f�r die tempor�re Tabelle (Nur ClassDef)
            result := Format('%s %s', [result, xDataItem.GetSQLSelectFields(aWithSum, false)])
          end;// if xDataItem.DataItemType.inheritsFrom(TClassDef) then begin
        end;// if aWithSum then begin
  
      end else begin
        // Das DataItem ist nicht in der globalen Liste vorhanden
        raise Exception.CreateFMT(rsDataItemNotDefined, [FDataItemList[i]]);
      end;// if assigned(xDataItem) then begin
    end;// for i := 0 to FDataItemList.Count - 1 do begin
  
    if aWithAlias then
      // H�ngt hinten noch den FEldalias an (z.B.: 'SUM(v.c_CSiro) + SUM(v.c_CN) as "CFF + CN"')
      result := Format (result + cFieldAlias, [FieldName]);
  end else begin
    raise Exception.Create('TCompositClassDef.GetSQLSelectFields: ClassDefList unassigned.');
  end;// if assigned(ClassDefList) then begin
end;// TCompositClassDef.GetSQLSelectFields cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           GetSQLWhere
 *  Klasse:           TCompositClassDef
 *  Kategorie:        Datenbank 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Beinhaltet die Where-Klausel (Join) die die Felder betreffen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TCompositClassDef.GetSQLWhere: String;
var
  i: Integer;
  xDataItem: TBaseDataItem;
begin
  result := '';

//: ----------------------------------------------
  if assigned(ClassDefList) then begin
    for i := 0 to FDataItemList.Count - 1 do begin
      xDataItem := ClassDefList.DataItemByName[FDataItemList[i]];
      if assigned(xDataItem) then begin
        // Properties in den globalen DataItems setzten, damit alle Queries richtig erzeugt werden k�nnen
        SetRequiredProps(xDataItem);
        // Holt die Definition beim entsprechenden DataItem ab
        result := result + xDataItem.GetSQLWhere;
      end;// if assigned(xDataItem) then begin
    end;// for i := 0 to FDataItemList.Count - 1 do begin
  end else begin
    raise Exception.Create('TCompositClassDef.GetSQLWhere: ClassDefList unassigned.');
  end;// if assigned(ClassDefList) then begin
end;// TCompositClassDef.GetSQLWhere cat:Datenbank

//:-------------------------------------------------------------------
(*: Member:           GetTableType
 *  Klasse:           TCompositClassDef
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die Tabelle(n) zur�ck die beteiligt ist(sind)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TCompositClassDef.GetTableType: TUsedTablesSet;
var
  i: Integer;
  xDataItem: TBaseDataItem;
begin
  result := [];

//: ----------------------------------------------
  if assigned(ClassDefList) then begin
    for i := 0 to FDataItemList.Count - 1 do begin
      xDataItem := ClassDefList.DataItemByName[FDataItemList[i]];
      if assigned(xDataItem) then begin
        // Properties in den globalen DataItems setzten, damit alle Queries richtig erzeugt werden k�nnen
        SetRequiredProps(xDataItem);
        // Holt die Definition beim entsprechenden DataItem ab
        result := result + xDataItem.TableType;
      end;// if assigned(xDataItem) then begin
    end;// for i := 0 to FDataItemList.Count - 1 do begin
  end else begin
    raise Exception.Create('TCompositClassDef.GetTableType: ClassDefList unassigned.');
  end;// if assigned(ClassDefList) then begin
end;// TCompositClassDef.GetTableType cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetDataItemName
 *  Klasse:           TCompositClassDef
 *  Kategorie:        No category 
 *  Argumente:        (Index, Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r DataItemName[Index]
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TCompositClassDef.SetDataItemName(Index: Integer; const Value: String);
begin
  if Index < FDataItemList.Count then
     FDataItemList[Index] := Value;
end;// TCompositClassDef.SetDataItemName cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetMathOperation
 *  Klasse:           TCompositClassDef
 *  Kategorie:        No category 
 *  Argumente:        (Index, Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r MathOperation[Index]
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TCompositClassDef.SetMathOperation(Index: Integer; Value: TMathOperation);
begin
  if Index < FDataItemList.Count then
     FDataItemList.Objects[Index] := pointer(ord(Value));
end;// TCompositClassDef.SetMathOperation cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetMatrixDef
 *  Klasse:           TCompositClassDef
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r die Eigenschaft "MatrixDef" von TClassDef
 *  Beschreibung:     
                      Die Methode wird �berschrieben, da keine Matrixdefinition 
                      vorhanden ist, sondern eine Definition der verwendeten 
                      DataItems (Name und Operation)
 --------------------------------------------------------------------*)
procedure TCompositClassDef.SetMatrixDef(Value: String);
var
  i: Integer;
  xOp: Boolean;
  
  (*
  function GetMathOp(aOp: string): TMathOperation;
  var
    i: TMathOperation;
  begin
    result := moNone;
    i := Low(TMathOperation);
  
    while (i < High(TMathOperation)) and (result = moNone) do begin
      inc(i);
      if AnsiSameText(aOP, cMathOperation) then
        result := i;
    end;// while (i < High(TMathOperation)) and (result = moNone) do begin
  
    if AnsiSameText(aOP, cAddOp) then
      result := moAdd;
  
    if AnsiSameText(aOP, cSubOp) then
      result := moSub;
  
    if AnsiSameText(aOP, cMulOp) then
      result := moMul;
  
    if AnsiSameText(aOP, cDivOp) then
      result := moDiv;
  end;
  *)
  
begin
  // Initialisieren
  FDataItemList.CommaText := Value;
  i := 0;

//: ----------------------------------------------
  // True, wenn der aktuelle Eintrag eine Operation ist
  xOp := false;
  
  while i < DataItemCount do begin
    if (xOp) then begin
      // mathematische Operation
      MathOperation[i - 1] := GetMathOpFromString(FDataItemList[i]);
      // Eintrag l�schen (die Operation wurde dem vorherigen Eintrag als Pointer hinzugef�gt)
      FDataItemList.Delete(i);
    end else begin
      inc(i);
    end;// if (xOp) then begin
  
    // Togeln (Jeder zweite Eintrag ist eine Operation)
    xOp := not(xOp);
  end;// while i < DataItemCount do begin
end;// TCompositClassDef.SetMatrixDef cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetRequiredProps
 *  Klasse:           TCompositClassDef
 *  Kategorie:        Datenbank 
 *  Argumente:        (aDataItem)
 *
 *  Kurzbeschreibung: Setzt die notwendigen Properties, damit das Query korrekt gebildet werden kann
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TCompositClassDef.SetRequiredProps(var aDataItem: TBaseDataItem);
var
  xDataItem: TBaseDataItem;
begin
  aDataItem.TimeMode := TimeMode;
  aDataItem.ClassDefects := ClassDefects;
  aDataItem.LinkAlias := LinkAlias;
  { TODO : Ereignis in dem eventuell von aussen noch Properties kopiert werden k�nnen }
  GetSQLFromTable
end;// TCompositClassDef.SetRequiredProps cat:Datenbank



end.
