object dmLabReport: TdmLabReport
  OldCreateOrder = False
  Left = 340
  Top = 129
  Height = 479
  Width = 741
  object conDefault: TmmADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=netpmek32;User ID=MMSystemSQL;Initi' +
      'al Catalog=MM_Winding;Data Source=WETLOK2000\LOCAL_MSDB;Use Proc' +
      'edure for Prepare=1;Auto Translate=False;Packet Size=4096;Workst' +
      'ation ID=WETLOK2000;Use Encryption for Data=False;Tag with colum' +
      'n collation when possible=False'
    LoginPrompt = False
    Mode = cmRead
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 24
    Top = 8
  end
  object dseChart: TmmADODataSet
    Connection = conDefault
    CursorType = ctStatic
    BeforeOpen = dseBeforeOpen
    OnCalcFields = DataSetCalcFields
    CommandText = 
      'select  (v.c_time) as "c_time",(v.c_style_name) as "c_style_name' +
      '",SUM(v.c_Len) as "c_Len",SUM(v.c_SFI) as "c_SFI",SUM(v.c_SFICnt' +
      ') as "c_SFICnt",SUM(v.c_AdjustBase) as "c_AdjustBase",SUM(v.c_rt' +
      'Spd) as "c_rtSpd",SUM(v.c_wtSpd) as "c_wtSpd",SUM(v.c_AdjustBase' +
      'Cnt) as "c_AdjustBaseCnt",SUM(v.c_CYTot) as "c_CYTot",SUM(v.c_CN' +
      ') as "c_CN", SUM(cd_scuc.c_CU9 * 1.0 +cd_scuc.c_CU10 * 1.0 ) as ' +
      '"A1" from v_production_shift v,t_dw_classuncut cd_scuc where v.c' +
      '_shiftcal_id = 1 and v.c_style_id in ('#39'332'#39','#39'3'#39','#39'12'#39','#39'294'#39','#39'21'#39',' +
      #39'30'#39','#39'489'#39','#39'42'#39','#39'351'#39')'#13#10' AND cd_scuc.c_classuncut_id = v.c_class' +
      'uncut_id'#13#10'  Group by c_style_name,c_time Order by c_style_name A' +
      'SC,c_time ASC'
    Parameters = <>
    Left = 24
    Top = 56
  end
  object dseWork: TmmADODataSet
    Tag = 1
    Connection = conDefault
    CursorType = ctStatic
    BeforeOpen = dseBeforeOpen
    Parameters = <>
    Left = 240
    Top = 56
  end
  object dseGrid: TmmADODataSet
    Tag = 1
    Connection = conDefault
    CursorType = ctStatic
    BeforeOpen = dseBeforeOpen
    OnCalcFields = DataSetCalcFields
    Parameters = <>
    Left = 96
    Top = 56
  end
  object dseGridDetail: TmmADODataSet
    Tag = 1
    Connection = conDefault
    BeforeOpen = dseBeforeOpen
    OnCalcFields = DataSetCalcFields
    Parameters = <>
    Left = 168
    Top = 56
  end
end
