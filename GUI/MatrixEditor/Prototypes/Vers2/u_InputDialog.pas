unit u_InputDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmStaticText, Buttons, ExtCtrls, mmEdit, mmComboBox, mmLabel,
  mmMemo;

type

  TItemType = (itMatrix, itComposite);

  TInputDialog = class(TForm)
    edName: TmmEdit;
    Panel1: TPanel;
    Panel2: TPanel;
    butCancel: TBitBtn;
    butOk: TBitBtn;
    edGroup: TmmComboBox;
    meDescription: TmmMemo;
    lbRemarks: TmmLabel;
    lbClass: TmmLabel;
    lbGroup: TmmLabel;
    rgItemType: TRadioGroup;
    procedure butCancelClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    function GetClassName: string;
    function GetGroupName: string;
    function GetDescription:String;
    procedure SetClassName(const Value: string);
    procedure SetDescription(const Value: string);
    procedure SetGroupName(const Value: string);
    function GetItemType: TItemType;
    procedure SetItemType(const Value: TItemType);
    procedure SetEditMode(const Value: Boolean);
    { Private declarations }
  public
    procedure AddGroup(aGroupName: string);
    procedure SelectGroup(aGroupName: string);
    property NewClassName:string read GetClassName write SetClassName;
    property NewGroupName: string read GetGroupName write SetGroupName;
    property NewDescription: string read GetDescription write SetDescription;
    property ItemType: TItemType read GetItemType write SetItemType;
    property EditMode : Boolean write SetEditMode;

    { Public declarations }
  end;

var
  InputDialog: TInputDialog;

  const cNewLine = #$D#$A;

implementation

{$R *.DFM}
//------------------------------------------------------------------------------
procedure TInputDialog.AddGroup(aGroupName: string);
begin
  edGroup.Items.Add(aGroupName);
end;
//------------------------------------------------------------------------------
procedure TInputDialog.butCancelClick(Sender: TObject);
var xMsg : String;
    xRet : TModalResult;
begin
  xRet := (Sender as TBitBtn).ModalResult;
  if (xRet = mrOk) or (xRet = mrYes) then
     if edName.Text = '' then begin
        xMsg := '(*)Geben Sie bitte einen Namen ein!' ;
        MessageDlg(xMsg, mtWarning, [mbOK], 0);
        edName.SetFocus;
        exit;
     end;

  ModalResult := xRet;
end;
//------------------------------------------------------------------------------
function TInputDialog.GetClassName: string;
begin
  result := edName.Text;
end;
//------------------------------------------------------------------------------
function TInputDialog.GetDescription: String;
var x     : integer;
    xText : String;
begin
   xText := '';
   for x:= 0 to meDescription.Lines.Count-1 do
       if x = 0 then
          xText:= meDescription.Lines.Strings[x]
       else
          xText := xText + cNewLine + meDescription.Lines.Strings[x];  
   result :=    xText;
end;
//------------------------------------------------------------------------------
function TInputDialog.GetGroupName: string;
begin
  result := edGroup.Text;
end;
//------------------------------------------------------------------------------
function TInputDialog.GetItemType: TItemType;
begin
  result := TItemType( rgItemType.ItemIndex);
end;
//------------------------------------------------------------------------------
procedure TInputDialog.SelectGroup(aGroupName: string);
begin
  edGroup.Text := aGroupName;
end;
//------------------------------------------------------------------------------
procedure TInputDialog.SetClassName(const Value: string);
begin
 if Value <> '' then edName.Text :=  Value;
end;
//------------------------------------------------------------------------------
procedure TInputDialog.SetDescription(const Value: string);
begin
 if Value <> '' then meDescription.Lines.CommaText := Value;
end;
//------------------------------------------------------------------------------
procedure TInputDialog.SetEditMode(const Value: Boolean);
begin
  rgItemType.Enabled := Not Value;
end;
//------------------------------------------------------------------------------
procedure TInputDialog.SetGroupName(const Value: string);
begin
  if Value <> '' then begin
     edGroup.ItemIndex := edGroup.Items.IndexOf(Value);
  end;
end;
//------------------------------------------------------------------------------
procedure TInputDialog.SetItemType(const Value: TItemType);
begin
  try
    rgItemType.ItemIndex := Integer( Value );
  except
    rgItemType.ItemIndex := 0;
  end;
end;
//------------------------------------------------------------------------------
procedure TInputDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin

//   if (Sender is TForm) or (ModalResult = mrCancel )then exit;

   if (ModalResult = mrCancel )then exit;

   if edName.Text = '' then begin
      CanClose:= FALSE;
   end;
end;
//------------------------------------------------------------------------------
end.
