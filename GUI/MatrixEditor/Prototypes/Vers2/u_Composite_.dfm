object Composite: TComposite
  Left = 282
  Top = 68
  Width = 448
  Height = 427
  Align = alClient
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object lbCompName: TLabel
    Left = 32
    Top = 15
    Width = 74
    Height = 13
    Caption = 'lbCompName'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 112
    Top = 75
    Width = 44
    Height = 13
    Caption = 'Elemente'
  end
  object edFormula: TmmEdit
    Left = 30
    Top = 35
    Width = 321
    Height = 21
    AutoSelect = False
    Color = clAqua
    ReadOnly = True
    TabOrder = 0
    Text = 'Formel'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object bbAdd: TBitBtn
    Left = 32
    Top = 70
    Width = 50
    Height = 25
    Caption = '+ ADD'
    TabOrder = 1
    OnClick = bbAddClick
  end
  object lbElement: TmmListBox
    Left = 32
    Top = 104
    Width = 145
    Height = 280
    Enabled = True
    ItemHeight = 13
    TabOrder = 2
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object bbUndo: TBitBtn
    Left = 356
    Top = 35
    Width = 75
    Height = 25
    Caption = 'Undo'
    TabOrder = 3
  end
end
