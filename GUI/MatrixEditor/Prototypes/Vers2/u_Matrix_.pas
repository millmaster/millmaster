unit u_Matrix_;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QualityMatrixBase, QualityMatrix,
  u_EditorClasses, YMParaDef;

type

  TMatrix = class(TBaseItemFrame)
//  TMatrix  = class(TFrame)
    qm: TQualityMatrix;
  private
    //procedure SetClassClearFromSiro(var aSettings: TClassClearSettingsArr; aSiro: TSiroClearSettingsArr);
  protected
    function GetData: TMatrixDef; virtual;
    procedure SetData(const Value: TMatrixDef); virtual;
    { Private declarations }
  public
    { Public declarations }
    property Data: TMatrixDef read GetData write SetData;
    procedure Save(var aMatrixDef :TMatrixDef); override;
 (*
  protected
    function GetData: TMatrixDef; virtual;
    procedure SetData(const Value: TMatrixDef); virtual;
    { Private declarations }
  public
    { Public declarations }
    property Data: TMatrixDef read GetData write SetData;
    procedure Save(var aMatrixDef :TMatrixDef); Override;
  *)
  end;

implementation

{$R *.DFM}

//uses QualityMatrixDef;

{ TfrMatrix }




{
//------------------------------------------------------------------------------
function TMatrix.GetData: TMatrixDef;
begin
  Result := mData;
end;
//------------------------------------------------------------------------------
procedure TMatrix.Save(var aMatrixDef: TMatrixDef);
var xData: TMatrixDef;
    xArray, xArray0: TClassClearSettingsArr;
    xSiroClearSettingsArr: TSiroClearSettingsArr;
    xModified : Boolean;
    x : Integer;
begin

 inherited Save(aMatrixDef);

 xModified := FALSE;

 if assigned(aMatrixDef) then begin
    FillChar(xArray, 16, 0);
    if aMatrixDef.MatrixType = mtSiro then begin
       qm.GetFieldStateTable(xSiroClearSettingsArr, xSiroClearSettingsArr);
       SetClassClearFromSiro(xArray, xSiroClearSettingsArr);
    end else begin
       qm.GetFieldStateTable(xArray);
    end;// if xData.MatrixType = mtSiro then begin

    xModified := aMatrixDef.Modified;
    xArray0 := aMatrixDef.ClassSettingsArr;
    aMatrixDef.ClassSettingsArr := xArray;

    //Aenderungs check
    for x:= low(xArray) to high(xArray) do
        if xArray[x] <> xArray0[x] then begin
           xModified := TRUE;
           break;
        end;

    aMatrixDef.Modified := xModified;

    //In DB speichern
    aMatrixDef.Save(NIL);
    aMatrixDef.Modified := FALSE;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrix.SetClassClearFromSiro(
  var aSettings: TClassClearSettingsArr; aSiro: TSiroClearSettingsArr);
var
  i: integer;
begin
    assert(High(TSiroClearSettingsArr) <= High(TClassClearSettingsArr));
    for i := 0 to High(TSiroClearSettingsArr) do
      aSettings[i] := aSiro[i];
end;
//------------------------------------------------------------------------------
procedure TMatrix.SetData(const Value: TMatrixDef);
var xSiroClearSettingsArr: TSiroClearSettingsArr;
    xClassClearSettingsArr: TClassClearSettingsArr;

  function GetSiroArray(aSettings: TClassClearSettingsArr): TSiroClearSettingsArr;
  var
    i: integer;
  begin
    assert(High(TSiroClearSettingsArr) <= High(TClassClearSettingsArr));
    for i := 0 to High(TSiroClearSettingsArr) do
      Result[i] := aSettings[i];
  end;// function GetSiroArray(aSettings: TClassClearSettingsArr): TSiroClearSettingsArr;

begin

  mData := Value;

// Matrixfeld disablen, wenn keine Klassendefinition selektiert ist
  qm.enabled := assigned(mData);

  if assigned(mData) then begin

      case mData.MatrixType of
        mtShortLongThin: begin
                            qm.MatrixType := mtShortLongThin;
                         end;// mtShortLongThin: begin
               mtSplice: begin
                           qm.MatrixType := mtSplice;
                         end;// mtSplice: begin
                 mtSiro: begin
                           qm.MatrixType := mtSiro;
                         end;// mtSiro: begin
      end;// case xData.ClassTypeGroup of

      // Beim Umschalten auf Splice wird diese Eigenschaft automatisch True
      qm.Splicevisible := false;
      qm.ActiveVisible := true;
      qm.ChannelVisible := false;

      if mData.MatrixType = mtSiro then begin
         xSiroClearSettingsArr := GetSiroArray(mData.ClassSettingsArr);

         qm.SetFieldStateTable(xSiroClearSettingsArr, xSiroClearSettingsArr);
      end else begin
         qm.SetFieldStateTable(mData.ClassSettingsArr);
      end;// if xData.ClassTypeGroup = ctFF then begin
  end;
end;
//------------------------------------------------------------------------------
}

{ TMatrix }

function TMatrix.GetData: TMatrixDef;
begin
  beep;
end;
//------------------------------------------------------------------------------
procedure TMatrix.Save(var aMatrixDef: TMatrixDef);
begin
  beep;
end;
//------------------------------------------------------------------------------
{
procedure TMatrix.SetClassClearFromSiro(
  var aSettings: TClassClearSettingsArr; aSiro: TSiroClearSettingsArr);
var
  i: integer;
begin
    assert(High(TSiroClearSettingsArr) <= High(TClassClearSettingsArr));
    for i := 0 to High(TSiroClearSettingsArr) do
      aSettings[i] := aSiro[i];
end;
//------------------------------------------------------------------------------
}
procedure TMatrix.SetData(const Value: TMatrixDef);
begin

end;
//------------------------------------------------------------------------------
end.
