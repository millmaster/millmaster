object Composite: TComposite
  Left = 0
  Top = 0
  Width = 476
  Height = 436
  Align = alClient
  TabOrder = 0
  object lbCompName: TLabel
    Left = 30
    Top = 20
    Width = 74
    Height = 13
    Caption = 'lbCompName'
    FocusControl = edFormula
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 32
    Top = 105
    Width = 137
    Height = 13
    Caption = '(*)Verf�gbare Datenelemente'
    FocusControl = lbElement
  end
  object edFormula: TmmEdit
    Left = 30
    Top = 35
    Width = 400
    Height = 21
    AutoSelect = False
    Color = clWhite
    ReadOnly = True
    TabOrder = 0
    Text = 'Formel'
    Visible = True
    AutoLabel.Control = lbCompName
    AutoLabel.LabelPosition = lpTop
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object bbAdd: TBitBtn
    Left = 185
    Top = 150
    Width = 73
    Height = 25
    Hint = '(*)Plus Datenelement'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = bbAddClick
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      888888888888888888888888888888888888888888800088888888888880C088
      888888888880C088888888880000C000088888880CCCCCCC088888880000C000
      088888888880C088888888888880C08888888888888000888888888888888888
      8888888888888888888888888888888888888888888888888888}
  end
  object lbElement: TmmListBox
    Left = 32
    Top = 120
    Width = 145
    Height = 280
    Color = 14811135
    Enabled = True
    ItemHeight = 13
    Sorted = True
    TabOrder = 2
    Visible = True
    AutoLabel.Control = Label1
    AutoLabel.LabelPosition = lpTop
  end
  object bbUndo: TBitBtn
    Left = 185
    Top = 120
    Width = 75
    Height = 25
    Hint = '(*)Letztes Datenelement r�ckg�ngig'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OnClick = bbUndoClick
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888888888888888888888888888888888888888888888888888888888888887
      4888884444488888478888444488888884888844488888888488884484888888
      8488884888448888478888888888444478888888888888888888888888888888
      8888888888888888888888888888888888888888888888888888}
  end
  object bbMinus: TBitBtn
    Left = 185
    Top = 180
    Width = 75
    Height = 25
    Hint = '(*)Minus Datenelement'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    OnClick = bbMinusClick
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888888888888888888888888888888888888888888888888888888888888888
      88888888888888888888888800000000088888880CCCCCCC0888888800000000
      0888888888888888888888888888888888888888888888888888888888888888
      8888888888888888888888888888888888888888888888888888}
  end
  object Panel1: TPanel
    Left = 30
    Top = 80
    Width = 400
    Height = 2
    TabOrder = 5
  end
end
