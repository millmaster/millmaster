unit u_Composite;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmListBox, Buttons, mmEdit,
  u_EditorClasses, ExtCtrls;

type
  TComposite = class(TFrame)
    lbCompName: TLabel;
    Label1: TLabel;
    edFormula: TmmEdit;
    bbAdd: TBitBtn;
    lbElement: TmmListBox;
    bbUndo: TBitBtn;
    bbMinus: TBitBtn;
    Panel1: TPanel;
    procedure bbAddClick(Sender: TObject);
    procedure bbUndoClick(Sender: TObject);
    procedure bbMinusClick(Sender: TObject);
  private
    { Private declarations }
    mData : TMatrixDef;
    procedure FillElemntList;
    function GetData: TMatrixDef;
  protected
    procedure SetData(const Value: TMatrixDef); //virtual;
    procedure Undo;
    procedure TermToFormula(aOP:Char);
  public
    { Public declarations }
    property Data: TMatrixDef read GetData write SetData;
    procedure Save(var aMatrixDef :TMatrixDef); //override;
    procedure Reset;
    //procedure AfterConstruction; virtual;
    constructor Create(AOwner: TComponent); override;
  end;

implementation

{$R *.DFM}
uses QualityMatrixDef, LabMasterDef;

{ TComposite }

//------------------------------------------------------------------------------
procedure TComposite.Save(var aMatrixDef: TMatrixDef);
var xData: TMatrixDef;
    x :string;
begin

 if assigned(aMatrixDef) then begin

    if aMatrixDef.MatrixDef <> edFormula.Text then begin
       aMatrixDef.Modified  := TRUE;
    end;

    aMatrixDef.Formula := edFormula.Text;

    //In DB speichern
    aMatrixDef.Save(NIL);
    aMatrixDef.Modified := FALSE;

 end;// if assigned (xData) then begin
end;
//------------------------------------------------------------------------------
procedure TComposite.SetData(const Value: TMatrixDef);
var
  xData: TMatrixDef;
  xCompName, xCategory :string;
  xStatus : Boolean;
begin

  mData := Value;

  xCompName := '';
  xCategory := '';
  xStatus := FALSE;
{
  if assigned(DataItemTree1.GetNodeDataItem(Node)) then
     if DataItemTree1.GetNodeDataItem(Node) is TMatrixDef then begin
        xData := TMatrixDef(DataItemTree1.GetNodeDataItem(Node));
}
     if assigned(mData) then begin

        if mData.MatrixType = mtNone then begin
           edFormula.Text := mData.Formula;
           xCompName := Format('%s = ',[mData.ItemName]);
           //xCategory := Format('Category : %s',[mData.Category]);
           xStatus := TRUE;
        end;
     end;

  lbCompName.Caption := xCompName;

end;
//------------------------------------------------------------------------------
procedure TComposite.bbAddClick(Sender: TObject);
begin
  //ADDElement;
  TermToFormula('+');
end;
//------------------------------------------------------------------------------
procedure TComposite.FillElemntList;
var x: integer;
begin

 for x:= low(cDataItemArray) to high(cDataItemArray) do begin
     if cDataItemArray[x].CompositeElement = TRUE then
        lbElement.Items.Add(cDataItemArray[x].DisplayName);
 end;
{

    lbElement.Items.Add('CL');
    lbElement.Items.Add('CCL');
    lbElement.Items.Add('YB');
    lbElement.Items.Add('CY');
    lbElement.Items.Add('CT');
    lbElement.Items.Add('CSP');
    lbElement.Items.Add('CS');
    lbElement.Items.Add('CSFI');
    lbElement.Items.Add('CN');
    lbElement.Items.Add('CYTot');
    lbElement.Items.Add('Sp');
    lbElement.Items.Add('CUpY');
    lbElement.Items.Add('%Eff');
}



{
    lbElement.Items.Add('%EffP');
    lbElement.Items.Add('%CCI');
    lbElement.Items.Add('%CCIFF');
    lbElement.Items.Add('%CL');
    lbElement.Items.Add('%CN');
    lbElement.Items.Add('COffCnt');
    lbElement.Items.Add('CSFI');
    lbElement.Items.Add('CSp');
    lbElement.Items.Add('CS');
    lbElement.Items.Add('CSFI');
    lbElement.Items.Add('CT');
    lbElement.Items.Add('CUpY');
    lbElement.Items.Add('CYTot');
    lbElement.Items.Add('Len');
    lbElement.Items.Add('SFI');
    lbElement.Items.Add('SFI/D');
    lbElement.Items.Add('Sp');
    lbElement.Items.Add('Weight');
    lbElement.Items.Add('YB');
}
end;
//------------------------------------------------------------------------------
function TComposite.GetData: TMatrixDef;
begin
  result := mData;
end;
//------------------------------------------------------------------------------
procedure TComposite.Reset;
begin
  edFormula.Text:= '';
end;
//------------------------------------------------------------------------------
constructor TComposite.Create(AOwner: TComponent);
begin
  inherited;
  FillElemntList;
  lbElement.ItemIndex := 0;
end;
//------------------------------------------------------------------------------
procedure TComposite.bbUndoClick(Sender: TObject);
begin
  Undo;
end;
//------------------------------------------------------------------------------
procedure TComposite.Undo;
var xText :string;
    x : integer;
begin
  xText := edFormula.Text;

  if xText = '' then exit;

  for x:= Length(xText) downto 1 do begin
      case xText[x] of
         '+', '-' : break;
      end;
  end;

  if x = 0 then
     //1. Element loeschen
     xText := ''
  else
    delete(xText, x-1, length(xText) );

  Trim(xText);
  edFormula.Text := xText;
  mData.Modified := TRUE;
end;
//------------------------------------------------------------------------------
procedure TComposite.bbMinusClick(Sender: TObject);
begin
  TermToFormula('-');
end;
//------------------------------------------------------------------------------
procedure TComposite.TermToFormula(aOP: Char);
var xFormula : String;
    xNewTerm, xTxt :String;
    x:integer;
begin
  xFormula :=  edFormula.text;
  xFormula := StringReplace(xFormula, 'Formel', '' ,[rfReplaceAll] );

  xNewTerm :=  lbElement.Items.Strings[ lbElement.itemindex ];

  if Pos( xNewTerm, xFormula) > 0 then begin
     xTxt := Format('(*)Das Element %s ist bereits in der Formel enthalten!',[xNewTerm] );
     MessageDlg(xTxt, mtWarning	, [mbOk], 0);
     exit;
  end;

  edFormula.text := format('%s %s %s',[xFormula, aOP, xNewTerm]);
  xFormula :=  edFormula.text;
  x:= Pos(aOP, xFormula);
  if x < 3 then begin
     delete(xFormula, 1, x+1);
     Trim (xFormula);
     edFormula.text := xFormula;
  end;
  mData.Modified := TRUE;
end;
//------------------------------------------------------------------------------
end.

.
