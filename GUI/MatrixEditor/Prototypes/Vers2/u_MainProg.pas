unit u_MainProg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ToolWin, u_Editor;

type
  TfrmMain = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
  private
    { Private declarations }
    mMatrixEditor : TMatrixEditor;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.DFM}

procedure TfrmMain.ToolButton1Click(Sender: TObject);
begin
  //screen.Cursor :=  crHourGlass;
  MatrixEditor  :=  TMatrixEditor.Create(Self);
  //screen.Cursor :=  crDefault;
  //MatrixEditor.WindowState:= wsMaximized;
  MatrixEditor.Show;
end;

procedure TfrmMain.ToolButton2Click(Sender: TObject);
begin
  MatrixEditor.Free;
end;

end.
