unit u_Editor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DataItemTree, Buttons, ExtCtrls, StdCtrls,

  u_Matrix, u_Composite, u_EditorClasses, ClassDef, VirtualTrees,
  u_InputDialog, ImgList, ComCtrls, ToolWin, Menus, fcStatusBar;

  {
  YMQRUnit, VirtualTrees, mmVirtualStringTree,
  ClassDataSuck, SettingsFinderUnit,
  ClassDef, MMUGlobal, YMParaDef, QualityMatrixBase, QualityMatrix,
  QualityMatrixDef, AdoDBAccess;
  }
type

  //TItemType = (itMatrix, itComposite);


  TMatrixEditor = class(TForm)
    paItemTree: TPanel;
    DataItemTree: TDataItemTree;
    Panel1: TPanel;
    Splitter1: TSplitter;
    ToolBar1: TToolBar;
    tbSave: TToolButton;
    tbSaveAS: TToolButton;
    Image1: TImage;
    ToolButton4: TToolButton;
    ImageList1: TImageList;
    tbNew: TToolButton;
    ToolBar2: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    StatusBar: TfcStatusBar;
    paItemDataField: TPanel;
    lbNoElements: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure paItemDataFieldResize(Sender: TObject);
    procedure Splitter1CanResize(Sender: TObject; var NewSize: Integer;
      var Accept: Boolean);
    procedure DataItemTreeFocusChanged(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex);
    procedure DataItemTreeFocusChanging(Sender: TBaseVirtualTree; OldNode,
      NewNode: PVirtualNode; OldColumn, NewColumn: TColumnIndex;
      var Allowed: Boolean);
    procedure sbSaveClick(Sender: TObject);
    procedure Image1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure Image1DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure DataItemTreeDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DataItemTreeStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure DataItemTreeDblClick(Sender: TObject);
    procedure MenueMatrixKlasseClick(Sender: TObject);
    procedure tbSaveClick(Sender: TObject);
    procedure tbNewClick(Sender: TObject);
    procedure MenueCompositeClick(Sender: TObject);
    procedure tbSaveASClick(Sender: TObject);
  private
    { Private declarations }
    mMatrix : TMatrix;
    mComposite : TComposite;
    mMatrixContainer : TMatrixContainer;
    mNewItem : Boolean;
    function InputName(var aClassName:string; var aGroupName: string;
                       var aDescription: string; var aItemType: TItemType;
                       aEditMode : Boolean): Boolean;

    procedure DeleteItem;
    procedure NewItem;
    procedure SaveItem;
    procedure SaveItemAS;
    procedure SetButtonStatus(aEnabled : Boolean);

  public
    { Public declarations }
  end;

var
  MatrixEditor: TMatrixEditor;

resourcestring
    c_DataItemTree_EigeneKlassen   = '(*)Eigene Klassen';      //ivlm

implementation

{$R *.DFM}
uses YMParaDef, QualityMatrixDef ;


procedure TMatrixEditor.FormCreate(Sender: TObject);
begin

  screen.Cursor :=  crHourGlass;
  //Application.ProcessMessages;


  mMatrix := TMatrix.Create(Self);
  mMatrix.AutoSize:= TRUE;
  //mMatrix.Parent := paItemDataField;
  mMatrix.Hide;



  mComposite := TComposite.Create(self) ;
  mComposite.AutoSize:= TRUE;
  //mComposite.Parent := paItemDataField;
  mComposite.Hide;


  mMatrixContainer := TMatrixContainer.Create;

  //Klassen von DB holen; Daten werden in die Klasse TMatrixDef gepackt
  //Pro Item eine TMatrixDef Kl.
  mMatrixContainer.LoadFromDatabase(cItemClass);

  // Container zuweisen (wird kopiert)
  DataItemTree.ClassDefList := mMatrixContainer;
  DataItemTree.ExpandedGroups := [ctCustomDataItem];

  screen.Cursor :=  crDefault;

end;

procedure TMatrixEditor.FormDestroy(Sender: TObject);
begin
  mMatrix.Free;
  mComposite.Free;
end;

procedure TMatrixEditor.paItemDataFieldResize(Sender: TObject);
begin
  lbNoElements.Top  := (paItemDataField.Height - lbNoElements.Height) DIV 2;
  if lbNoElements.Top  <= lbNoElements.Height + 2 then
     lbNoElements.Top:= lbNoElements.Height + 2;

  lbNoElements.Left := (paItemDataField.Width - lbNoElements.Width) DIV  2 ;
  if lbNoElements.Left <= 5 then lbNoElements.Left:= 5;
end;

procedure TMatrixEditor.Splitter1CanResize(Sender: TObject;
  var NewSize: Integer; var Accept: Boolean);
begin
  if NewSize <= 225 then
     Accept:= FALSE
  else
     Accept:= TRUE;
end;

procedure TMatrixEditor.DataItemTreeFocusChanged(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex);
var
  xData: TMatrixDef;
  xClassClearSettingsArr: TClassClearSettingsArr;
  xSiroClearSettingsArr: TSiroClearSettingsArr;
  xBaseDataItem : TBaseDataItem;
  xText: String;

  xCategory, xItemName :String;

  function GetSiroArray(aSettings: TClassClearSettingsArr): TSiroClearSettingsArr;
  var
    i: integer;
  begin
    assert(High(TSiroClearSettingsArr) <= High(TClassClearSettingsArr));
    for i := 0 to High(TSiroClearSettingsArr) do
      Result[i] := aSettings[i];
  end;// function GetSiroArray(aSettings: TClassClearSettingsArr): TSiroClearSettingsArr;

begin

  FillChar(xClassClearSettingsArr,16,0);

  xData := nil;
  //sbDelete.Enabled := FALSE;
  if assigned(DataItemTree.GetNodeDataItem(Node)) then
     if DataItemTree.GetNodeDataItem(Node) is TMatrixDef then begin
        xData := TMatrixDef(DataItemTree.GetNodeDataItem(Node));

        SetButtonStatus(TRUE);


        if xData.MatrixType = mtNone then begin
           //Composite
           mMatrix.Hide;

           mComposite.Parent := paItemDataField;
           mComposite.Show;
           mComposite.Data := xData;

        end else begin
           //Matrix
           mComposite.Hide;

           mMatrix.Parent := paItemDataField;
           mMatrix.Show;

           mMatrix.Data:= xData;
        end;

        xCategory:= Format('Category: %s',[xData.Category]);
        xItemName:= Format('%s',[xData.ItemName]);

     end else begin
        mComposite.Hide;
        mMatrix.Hide;
        xBaseDataItem := DataItemTree.GetNodeDataItem(Node);

        xCategory:= Format('Category: %s',[xBaseDataItem.DisplayName]);
        xItemName := '';
        SetButtonStatus(False);
     end;



     StatusBar.Panels[0].Text:= xCategory;
     StatusBar.Panels[1].Text:= xItemName;
     StatusBar.Panels[2].Text := '';
end;

procedure TMatrixEditor.DataItemTreeFocusChanging(Sender: TBaseVirtualTree;
  OldNode, NewNode: PVirtualNode; OldColumn, NewColumn: TColumnIndex;
  var Allowed: Boolean);
var
  xData, xData1: TMatrixDef;
  xMsg, xNextItem : String;
  xDataItem: TBaseDataItem;
begin
  xData  := NIL;
  xData1 := NIL;

  if assigned(DataItemTree.GetNodeDataItem(OldNode)) then
     if DataItemTree.GetNodeDataItem(OldNode) is TMatrixDef then
        xData := TMatrixDef(DataItemTree.GetNodeDataItem(OldNode));

  if assigned(DataItemTree.GetNodeDataItem(NewNode)) then
     if DataItemTree.GetNodeDataItem(NewNode) is TMatrixDef then begin
        xData1 := TMatrixDef(DataItemTree.GetNodeDataItem(NewNode));
        xNextItem := xData1.FieldName;
     end;

  if assigned(xData) then begin
    if OldNode <> NewNode then begin
      if xData.Modified then begin
          if mNewItem then
             xMsg := Format('(*)Wollen Sie die neunen "%s" Daten speichern?', [xData.FieldName])
          else
            xMsg := Format('(*)"%s" ge�ndert! Wollen Sie die Daten speichern?', [xData.FieldName]);

          if MessageDlg(xMsg, mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin

             SaveItem;
             Allowed:= FALSE;

             xDataItem := mMatrixContainer.DataItemByName[xNextItem];
             DataItemTree.SelectDataItem(xDataItem, FALSE);

          end else begin
             xData.Modified := FALSE;
             mNewItem := FALSE;
             exit;
          end;
      end;
    end;// if OldNode <> NewNode then begin
  end;// if assigned (xData) then begin
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.sbSaveClick(Sender: TObject);
  var xData: TMatrixDef;
begin
  xData := NIL;
  xData := DataItemTree.SelectedDataItem as TMatrixDef;

  if assigned(xData) then begin
     if xData.MatrixType <> mtNone then
        mMatrix.Save(xData);

     if xData.MatrixType = mtNone then
        mComposite.Save(xData);

     DataItemTree.ClassDefList := mMatrixContainer;
  end;
end;

procedure TMatrixEditor.Image1DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
var xTxt: String;
begin
  xTxt:= Source.ClassParent.ClassName;
  xTxt:= Sender.ClassName;

  Accept := (Source.ClassName = 'TmmVirtualStringTree');

end;

procedure TMatrixEditor.Image1DragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
  if  (Source.ClassName = 'TmmVirtualStringTree') then begin
      DeleteItem;
  end;
end;

procedure TMatrixEditor.DataItemTreeDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := FALSE;
  State := dsDragMove;
end;

procedure TMatrixEditor.DataItemTreeStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
// beep;
end;

procedure TMatrixEditor.DataItemTreeDblClick(Sender: TObject);
  var xData: TMatrixDef;
      xText :String;
begin
  xData := NIL;
  xData := DataItemTree.SelectedDataItem as TMatrixDef;

  if assigned(xData) then begin
     xText := Format('(*)Wollen Sie das DataItem "%s" l�schen?',[xData.DisplayName]);

     if MessageDlg(xText, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        DeleteItem;
  end;
end;

procedure TMatrixEditor.DeleteItem;
var xData     : TMatrixDef;
    xDataItem: TBaseDataItem;
    xNode : PVirtualNode;
begin

  xData := NIL;
  xData := DataItemTree.SelectedDataItem as TMatrixDef;

  if Assigned (xData) then begin
     mMatrixContainer.DeleteData(xData);
     // Container zuweisen (wird kopiert)
     DataItemTree.ClassDefList := mMatrixContainer;
     DataItemTree.SetFocus;


     //xDataItem := mMatrixContainer.DataItemByName[c_DataItemTree_EigeneKlassen];
     //DataItemTree.SelectDataItem(xDataItem, TRUE);
  end;

end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.MenueMatrixKlasseClick(Sender: TObject);
begin

end;
//------------------------------------------------------------------------------
function TMatrixEditor.InputName(var aClassName:string; var aGroupName: string;
                                 var aDescription: string;
                                 var aItemType: TItemType;
                                 aEditMode : Boolean ): Boolean;
var
  xData: TBaseDataItem;
  xNode: PVirtualNode;
  x: Integer;
  xList : TStringList;
begin
  result := false;

  with TInputDialog.Create(nil) do try

    xList:= TStringList.Create;
    xList.Sorted := TRUE;
    xList.Duplicates := dupIgnore;

    for x:= 0 to mMatrixContainer.DataItemCount-1 do begin
        if  mMatrixContainer.DataItems[x].ClassTypeGroup = ctCustomDataItem then
            xList.Add( mMatrixContainer.DataItems[x].Category);
    end;


    for x:=0 to xList.Count-1 do
       if xList.Strings[x] <> '' then
          AddGroup(xList.Strings[x]);

    xList.Free;

    edGroup.ItemIndex:=0;

    if aClassName <> '' then NewClassName:= aClassName;

    if aClassName <> '' then  NewGroupName:= aGroupName;

    if aDescription <> '' then NewDescription:= aDescription;


    ItemType := aItemType;
    EditMode := aEditMode;

    if ShowModal = mrOK then begin
       aClassName := NewClassName;
       aGroupName := NewGroupName;
       aDescription := NewDescription;
       aItemType := ItemType;
       result := true;
    end;
  finally
    Free;
  end;// with TClassNameInputDialog.Create(nil) do try
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.SaveItem;
var xData: TMatrixDef;
    xDataItem: TBaseDataItem;
begin
  xData := NIL;
  xData := DataItemTree.SelectedDataItem as TMatrixDef;

  if assigned(xData) then begin

     if xData.MatrixType <> mtNone then begin
        //TMatrixType
        mMatrix.Save(xData);
     end;

     if xData.MatrixType = mtNone then
        mComposite.Save(xData);

     xData.Modified:= FALSE;
     DataItemTree.ClassDefList := mMatrixContainer;

     xDataItem := mMatrixContainer.DataItemByName[xData.FieldName];
     DataItemTree.SelectDataItem(xDataItem, FALSE);

     StatusBar.Panels[2].Text := '(*)Eintrag gespeichert';
     mNewItem := FALSE;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.tbSaveClick(Sender: TObject);
begin
  SaveItem;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.tbNewClick(Sender: TObject);
begin
  NewItem;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.MenueCompositeClick(Sender: TObject);
begin
 // NewItem(itComposite);
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.NewItem;
var xArray : TClassClearSettingsArr;
    xClassName, xGroupName, xDescription: string;
    xIndex: integer;
    xMatrixDef : TMatrixDef;
    xClassGroup : TClassTypeGroup;
    xItemType   :TItemType;

    xDataItem: TBaseDataItem;
begin
  xClassName := '';
  xGroupName := '';
  xDescription := '';

  xItemType:= itMatrix;
  if InputName(xClassName, xGroupName, xDescription, xItemType, FALSE) then begin
    if xItemType = itMatrix then begin
       //Matrix
       with mMatrix.qm do begin
            FillChar(xArray, 16, 0);
            SetFieldStateTable(xArray) ;
            xClassGroup := ctCustomDataItem;
       end;
       xIndex := mMatrixContainer.CreateAndAddNewDataItem(TClassDef, TMatrixDef);
      (mMatrixContainer.DataItems[xIndex] as TMatrixDef).MatrixType:= mtShortLongThin
    end else begin
       //Composite
       with mComposite do begin
            Reset;
            xClassGroup := ctCustomDataItem;
       end;
       xIndex := mMatrixContainer.CreateAndAddNewDataItem(TCompositClassDef, TMatrixDef);
//       mMatrixContainer.MatrixItems[xIndex].MatrixType:=  mtNone;
       (mMatrixContainer.DataItems[xIndex] as TMatrixDef).MatrixType:=  mtNone;
    end;

    // Eigenschaften des neuen Items setzen
    mMatrixContainer[xIndex].ClassTypeGroup   := ctDataItem;
    mMatrixContainer[xIndex].Description      := xDescription;
    mMatrixContainer[xIndex].ItemName         := xClassName;
    mMatrixContainer[xIndex].Predefined       := false;
    mMatrixContainer[xIndex].Category         := xGroupName;

    //Neu zuweisen -> nur in Komponente
    DataItemTree.ClassDefList := mMatrixContainer;

    xDataItem := mMatrixContainer.DataItemByName[xClassName];
    DataItemTree.SelectDataItem(xDataItem, FALSE);

    StatusBar.Panels[2].Text := '(*)Neuer Eintrag';
    mNewItem := TRUE;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.tbSaveASClick(Sender: TObject);
begin
  SaveItemAS;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.SaveItemAS;
var xData: TMatrixDef;
    xArray : TClassClearSettingsArr;
    xClassName, xGroupName, xDescription: string;
    xIndex, xNewIndex: integer;
    xMatrixDef : TMatrixDef;
    xClassGroup : TClassTypeGroup;
    xItemType   :TItemType;
    xDataItem: TBaseDataItem;
begin

  xClassName := '';
  xGroupName := '';
  xDescription := '';

  xData := NIL;
  xData := DataItemTree.SelectedDataItem as TMatrixDef;

  if assigned(xData) then begin
     if xData.MatrixType <> mtNone then
        xItemType := itMatrix;

     if xData.MatrixType = mtNone then
        xItemType := itComposite;

     xDescription := xData.Description;
     xClassName   := xData.ItemName;
     xGroupName   := xData.Category;

     if InputName(xClassName, xGroupName, xDescription, xItemType, TRUE) then begin


         xIndex:= mMatrixContainer.IndexOf(xData);

         if (xClassName = xData.ItemName) or (xGroupName = xData.Category) then begin

            if xItemType = itMatrix then begin
                xNewIndex := mMatrixContainer.CreateAndAddNewDataItem(TClassDef, TMatrixDef);
            end else begin
                xNewIndex := mMatrixContainer.CreateAndAddNewDataItem(TCompositClassDef, TMatrixDef);
            end;
            mMatrixContainer[xNewIndex].Assign( mMatrixContainer[xIndex] );

         end else xNewIndex := xIndex;

         mMatrixContainer[xNewIndex].Description  := xDescription;
         mMatrixContainer[xNewIndex].ItemName     := xClassName;
         mMatrixContainer[xNewIndex].Category     := xGroupName;

         DataItemTree.ClassDefList := mMatrixContainer;

         xDataItem := mMatrixContainer.DataItemByName[xClassName];
         DataItemTree.SelectDataItem(xDataItem, FALSE);

         SaveItem;

     end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMatrixEditor.SetButtonStatus(aEnabled: Boolean);
begin
  tbSave.enabled := aEnabled;
  tbSaveAS.enabled := aEnabled;
end;

end.

.
