program MMMaConfig;
 // 15.07.2002 added mmMBCS to imported units
//Compilerswitches
//Nue:Um zu debuggen, wenn Basissystem mit dem Autolauncher gestartet ist, kann Compilerswitch SetFixMMAvailableForAutoLauncher gesetzt werden!
uses
  MemCheck,
  mmMBCS,
  Forms,
  LoepfeGlobal,
  SettingsReader,
  BASEAPPLMAIN in '..\..\..\LoepfeShares\Templates\BASEAPPLMAIN.pas' {BaseApplMainForm},
  u_MaConfigMain in 'u_MaConfigMain.pas' {MaConfigMain},
  BaseForm in '..\..\..\LoepfeShares\Templates\BASEFORM.pas' {mmForm},
  BASEDialog in '..\..\..\LoepfeShares\Templates\BASEDIALOG.pas' {mmDialog},
  BASEDialogBottom in '..\..\..\LoepfeShares\Templates\BASEDIALOGBOTTOM.pas' {DialogBottom},
  u_HeadType in 'u_HeadType.pas' {dwHeadType},
  u_InsMachNode in 'u_InsMachNode.pas' {dwInsMachNode},
  u_MaConfigComp in 'u_MaConfigComp.pas',
  u_NetType in 'u_NetType.pas' {dwNetType},
  u_FrontType in 'u_FrontType.pas' {dwFrontType},
  AssignComp in '..\..\Components\VCL\AssignComp.pas',
  PrintMaConfigForm in 'PrintMaConfigForm.pas' {frmPrintMachConfig},
  u_dmMaConfig in 'u_dmMaConfig.pas' {dmMaConfig: TDataModule},
  YMMachConfigBox in '..\..\..\LoepfeShares\XMLSettings\YMMachConfigBox.pas' {GBMachConfigBox: TFrame};

{$R *.RES}
{$R Version.RES}
begin
  gApplicationName := 'MMMaConfig';
  gMinDBVersion    := '5.01.00';
  with TMMSettingsReader.Instance do begin
    if not DBVersionOK(gMinDBVersion, True) then
      Exit;
  end;

{$IFDEF MemCheck}
  MemChk(gApplicationName);
{$ENDIF}
  Application.Initialize;
  Application.CreateForm(TdmMaConfig, dmMaConfig);
  Application.CreateForm(TMaConfigMain, MaConfigMain);
  Application.Run;

end.

