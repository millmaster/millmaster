(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: PrintMaConfigForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 22.03.2002  1.00  Wss  | Datei erstellt, nachdem vorhergehendes PrintForm (MaConfigQR)
                           beim Drucken immer erschienen ist (Visible)
| 20.06.2004  1.00  SDo  | neues Print-Layout 
|=============================================================================*)
unit PrintMaConfigForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, IvDictio, IvMulti, IvEMulti, mmTranslator, QRPrintGrid,
  QuickRpt, Qrctrls, mmQRLabel, ExtCtrls, mmQuickRep, mmQRSysData,
  mmQRImage;

type
  TfrmPrintMachConfig = class(TmmForm)
    mmQuickRep: TmmQuickRep;
    ColumnHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    PageHeaderBand1: TQRBand;
    mmQRLabel1: TmmQRLabel;
    TitleBand1: TQRBand;
    mPrintGrid: TQRPrintStringGrid;
    mmTranslator: TmmTranslator;
    QRBand1: TQRBand;
    mmQRImage1: TmmQRImage;
    qlPageValue: TmmQRSysData;
    mmQRSysData2: TmmQRSysData;
    qlCompanyName: TmmQRLabel;
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
  frmPrintMachConfig: TfrmPrintMachConfig;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, SettingsReader;

{$R *.DFM}

procedure TfrmPrintMachConfig.FormCreate(Sender: TObject);
begin
  inherited;
  try
    qlCompanyName.Caption := TMMSettingsReader.Instance.Value[cCompanyName];
  except
    qlCompanyName.Caption :='';
  end;

  qlPageValue.Text := Translate( qlPageValue.Text ) + ' ';
end;

end.
