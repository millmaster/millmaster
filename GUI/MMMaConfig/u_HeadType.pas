(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_HeadType.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows 2000 SP3
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| unbekannt   1.00  Nue  | Datei erstellt
| 08.11.2002        LOK  | Umbau ADO
| 25.02.2005        Wss  | Fenster wird nun bei Bedarf erstellt und nicht mehr pauschal beim Start der Applikation
|=============================================================================*)
unit u_HeadType;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, StdCtrls, mmLabel, DBCtrls, mmDBComboBox, Buttons,
  mmBitBtn, Db, DBTables, mmQuery, mmDataSource, mmComboBox, DBVisualBox, mmDialogs,
  mmDBLookupComboBox, Grids, DBGrids, mmDBGrid, mmTable, u_MaConfigComp,
  IvDictio, IvMulti, IvEMulti, mmTranslator, mmButton, ADODB, mmADODataSet;

  type
  TdwHeadType = class(TDialogBottom)
    mmDBGrid1: TmmDBGrid;
    mmDataSource2: TmmDataSource;
    mmTable1: TmmADODataSet;
    bDelete: TmmBitBtn;
    bInsert: TmmBitBtn;
    mmTranslator1: TmmTranslator;
    procedure bDeleteClick(Sender: TObject);
    procedure bInsertClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mmTable1AfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dwHeadType: TdwHeadType;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

procedure TdwHeadType.bDeleteClick(Sender: TObject);
begin
  try
    if MMMessageDlg(rsAskForDelete, mtConfirmation, [mbYes, mbNo], 0)= mrYes then begin
      mmTable1.Delete;
      MMMessageDlg(rsSuccDeleted, mtWarning, [mbOk], 0);
    end;
  except
    on e:Exception do begin
      raise Exception.Create ( 'TdwHeadType.bDeleteClick: DB delete on t_clear_type failed. ' + e.Message );
    end;
  end;

end;

procedure TdwHeadType.bInsertClick(Sender: TObject);
begin
  try
    mmTable1.Insert;
  except
    on e:Exception do begin
      raise Exception.Create ( 'TdwHeadType.bInsertClick: DB insert on t_head_type failed. ' + e.Message );
    end;
  end;
end;

procedure TdwHeadType.FormCreate(Sender: TObject);
begin
  mmTable1.Active := True;
end;

procedure TdwHeadType.mmTable1AfterPost(DataSet: TDataSet);
begin
  mmTable1.Active := False;
  mmTable1.Active := True;
end;

end.
