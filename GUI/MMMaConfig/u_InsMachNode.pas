(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: InsMachNode.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -     
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.12.1999  1.00  Nue  | Datei erstellt
| 29.01.2001  1.01  Nue  | Integration Componente from Kr in u_InsMachNode
| 10.07.2001  1.02  Nue  | In QueryUpdMachine InsSpindles;   //Update der Spindeln added.
| 05.11.2001  1.03  Nue  | c_maoffset_id=:c_maoffset_id from cQryUpdMachine deleted.
| 22.01.2002  1.04  Nue  | Handling with FixedSpdRange modified. fFixSpdRangeChanged added.
| 07.03.2002  1.05  Nue  | At connect from machine with node always set FixSpdRange
|                        |   for ftZE80i,ftZE800i,ftSWSInformatorSBC5.
| 11.04.2002  1.06  Nue  | ucDrumCorr added and dependent handling (only 0.5-1.5 allowed) default=1.0
| 07.11.2002  1.07  Nue  | Problems with spindlerange changing on new machines handled.
| 08.11.2002        LOK  | Umbau ADO
| 05.03.2003  1.08  Nue  | Grossumbau auf EasyVersion.
| 18.03.2003  1.09  Nue  | Suchen von L�cken zwischen den Spindelbereichen und Reaktion. Check NetTyp mit HasNetTyp
| 24.03.2003  1.10  Nue  | Security eingebaut und Verhalten von Buttons usw. in mmActionListMainUpdate verlegt.
| 09.04.2003  1.11  Nue  | Indexfehler beim speichern von Spindeln behoben.(Nue:9.4.03)
| 03.09.2003  1.12  Nue  | ShowHint aller ToolButton auf TRUE gesetzt.
| 04.05.2004  1.13  Nue  | Bugfix in Proc. GetSettings(local) f�r V4.2 und h�her(NrOfSpindles und L�cher-Dialog).
| 09.08.2004  1.14  Nue  | Bugfix in Proc. bFrontTypeMachNodeChanged f�r V4.2 und h�her((Self.Visible) Abfrage added.).
| 09.12.2004  2.00  Nue  | Umbau auf XML V.5.0.
| 23.05.2005  2.00  Wss  | Bei UpdateMachine wurde SpindleCount auf der DB nicht nachgef�hrt
| 16.06.2005        Lok  | IP Adressfeld vergr�ssert wegen Problemen mit UnicodeFont (Chinesische Version --> Zeichen "verschluckt")
| 21.06.2005        Wss  | Upload: Hinweis wenn keine oder nicht alle Spulstellen in Produktion
| 12.07.2005        Nue  | Parameters.ParamByName('c_AWE_type').value  := ORD(aweNone); //Added
| 27.10.2005        Wss  | Wenn NetTyp = LZE, dann FixSpdRange fix setzen und disablen
| 13.06.2006  2.01  Nue  | edLastSpindle.Validate bei TabChange und SaveButton eingef�gt.
|                        | L�cherCheck nur noch bei WSC-Maschinen (ntWSC)
| 27.11.2006  2.02  Nue  | c_use_newer_mapfile und cbNewerMap Handling added.
| 21.02.2008  2.10  Nue  | Anpassungen wegen neuer Maschinentypen. (mtAC5, mtSavioEsperoNuovo, mtSavioExcello, mtSavioPolar)
| 16.06.2008        Nue  | Neu kann der Netztyp auch bei bereits erfassten Maschinen ver�ndert werden.(z.B. ZE zu LZE)
| 18.09.2008  2.11  Nue  | Bei Ver�ndern des Netztyp bei bereits erfassten Maschinen DoRestartMMInfo aufrufen.
|==============================================================================*)
unit u_InsMachNode;


interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, StdCtrls, Buttons, mmBitBtn, ExtCtrls,
  ImgList, mmImageList, ActnList, mmActionList, Menus, mmMainMenu, ToolWin,
  ComCtrls, mmToolBar, mmPopupMenu,
  u_MaConfigComp, mmEdit, mmLabel, mmComboBox, mmStringList,
  mmLineLabel, mmCheckBox, LoepfeGlobal, MMUGlobal,
  BaseGlobal, YMParaDef, mmDialogs, Db, IvDictio, AssignComp, MachNodeComp,
  YMMachConfigBox, KeySpinEdit, mmButton, IvMulti, IvEMulti, mmTranslator,
  MMSecurity, u_dmMAConfig, mmRadioButton,
  mmPageControl, mmPanel, mmTabControl, mmRadioGroup,
  YMParaUtils, UEditIPAddr, SettingsReader,
  MMHTMLHelp, XMLDef;

resourcestring
  cNrNotSet     = '(*)Bitte eine Zahl eingeben.'; //ivlm
  cSpindleRange = '(*)Spindelbereich'; //ivlm
  rsIPUsed      = '(*)Gewuenschte IP-Adresse ist bereits belegt!'; //ivlm

const
  cDefaultMaOffsetID = 1; //MaOffset-Set with this ID has to exist on the DB
  cDummyNodeID       = '0';
  cLargeHeight       = 690;
  cSmallHeight       = 303;
type
  TSpindPos = (spLeftDown, spRightDown, spLeftTop, spRightTop);
  // Bit0:Headright(1)/Headleft(0); Bit1:Headtop(1)/Headdown(0)
const
  cSpindPosNames: array[spLeftDown..spRightTop] of string =
    ('Headleft/Headdown(0)', 'Headright/Headdown(1)', 'Headleft/Headtop(2)', 'Headright/Headtop(3)');

type
//..............................................................................
  TdwInsMachNode = class(TDialogBottom)
    pmToolbar: TmmPopupMenu;
    mActionList: TmmActionList;
    ImageList16x16: TmmImageList;
    Benutzerdefiniert1: TMenuItem;
    Standard1: TMenuItem;
    tbMain: TmmToolBar;
    acSave: TAction;
    bSave: TToolButton;
    mmPopupMenu1: TmmPopupMenu;
    acNew: TAction;
    bNew: TToolButton;
    bDefaultSettings: TmmButton;
    mmLineLabelBottom: TmmLineLabel;
    mmTranslator1: TmmTranslator;
    MMSecurityControl: TMMSecurityControl;
    acSecurityConfig: TAction;
    bSecurity: TToolButton;
    acFixSpdRange: TAction;
    pcMachineInfo: TmmPageControl;
    tsMachine: TTabSheet;
    tsMachineInfo: TTabSheet;
    mmLineLabel1: TmmLineLabel;
    mmLineLabel3: TmmLineLabel;
    mmLineLabel4: TmmLineLabel;
    labMachName: TmmLabel;
    edMachName: TBaseEdit;
    laMachType: TmmLabel;
    cobMachType: TBaseComboBox;
    cobNodeID: TBaseComboBox;
    laAdapt: TmmLabel;
    cobAdapter: TBaseComboBox;
    edMachID: TBaseEdit;
    labMachID: TmmLabel;
    labStartTime: TmmLabel;
    edStartTime: TBaseEdit;
    labSpindPos: TmmLabel;
    cobSpindPos: TBaseComboBox;
    labSlip: TmmLabel;
    cbFixSpdRange: TBaseCheckBox;
    cbOnlineSpeed: TBaseCheckBox;
    cbDataCollection: TBaseCheckBox;
    pnSectionConfig: TmmPanel;
    laNodeID: TmmLabel;
    laIPAddress: TmmLabel;
    laIPMachName: TmmLabel;
    cobIPMachName: TBaseComboBox;
    rgNetType: TmmRadioGroup;
    edIPAddress: TBaseEditIPAddr;
    labGoOn: TmmLabel;
    acUpload: TAction;
    acHelp: TAction;
    bHelp: TToolButton;
    bUpload: TToolButton;
    mGBMachConfigBox: TGBMachConfigBox;
    edSlip: TmmEdit;
    mmPanel1: TmmPanel;
    labFirstSpindle: TmmLabel;
    edFirstSpindle: TBaseEdit;
    labLastSpindle: TmmLabel;
    edLastSpindle: TBaseEdit;
    tcSpindleRanges: TmmTabControl;
    cobMapfile: TmmComboBox;
    laMapfile: TmmLabel;
    acOverruleMapfile: TAction;
    acExit: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    cbNewestMap: TmmCheckBox;
    bEnableNet: TToolButton;
    acEnableNet: TAction;
    procedure acEnableNetExecute(Sender: TObject);
//    procedure edFirstSpindleMachNodeChanged(Sender: TObject);
    procedure edLastSpindleMachNodeChanged(Sender: TObject);
    procedure mActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure cobAdapterChange(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure edFirstSpindleKeyPress(Sender: TObject; var Key: Char);
    procedure edLastSpindleKeyPress(Sender: TObject; var Key: Char);
    procedure OnValueChange(Sender: TObject);
    procedure acNewExecute(Sender: TObject);
    procedure mGBMachConfigBoxMachNodeChanged(Sender: TObject); //????
    procedure edFirstSpindleExit(Sender: TObject);
    procedure edLastSpindleExit(Sender: TObject);
    procedure cbDataCollectionClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure acSecurityConfigExecute(Sender: TObject);
    procedure tcSpindleRangesChange(Sender: TObject);
    procedure rgNetTypeClick(Sender: TObject);
    procedure acUploadExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure MMSecurityControlAfterSecurityCheck(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
  private
    fFixSpdRangeChanged: Boolean;
    fMachineChanged: Boolean;
    fRestartMM: Boolean;
    mAssWindowMsg: DWord;
    mMachineIsOnline: Boolean;
    mSaveFlag: Boolean;
    mNewFlag: Boolean;
    fSettingsHandler: TGetSettingsHandler;
    mComplete: Boolean;
    mFullMachine: TMaConfigFullReader;
    mNewNodeID: String;
    mNewNetTyp: Integer;  //Nue:18.9.08
    fSavedAfterChange: Boolean;
    mDifferentHashcode: Boolean;
    mEnabledChanges: Boolean;
//    mMachineName: string;
    mNewHashcode1: Integer;
    mNewHashcode2: Integer;
    mUploadManuallyDisabled: Boolean;
    procedure CheckComponents;
    procedure SelDropDownListItem(aComboBox: TmmComboBox; aID: Integer);
    procedure FillValues(aTexnet: Boolean);
    procedure InvisibleNetFields();
    procedure FillComboBoxTXNNodes;
//    procedure FillAvailableNodes;
    procedure FillComboBoxesFromNettyp(aClearMachTypes: Boolean=True);
    procedure QryInsSpindle(aSpdID: Integer);
    procedure QryInsNode;
    procedure QryInsMachine;
    procedure QryUpdNode;
    procedure QryUpdMachine;
    function CheckInputValues: Integer;
    procedure DisableControls(aEnabled: Boolean);
    procedure DoRestartMMInfo;
//    procedure edIPAdressKeyPress(Sender: TObject; var Key: Char);
    procedure SaveValue(aTag: Integer);
//    property SavedAfterChange: Boolean read fSavedAfterChange;
  protected
    procedure WndProc(var msg: TMessage); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Init(aFullMachine: TMaConfigFullReader);
    property MachineChanged: Boolean read fMachineChanged;
    property RestartMM: Boolean read fRestartMM;
  published
    property SettingsHandler: TGetSettingsHandler read fSettingsHandler write fSettingsHandler;
  end;

//..............................................................................
//..............................................................................

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS, MMMessages, xmlMaConfigClasses, XMLGlobal;

{$R *.DFM}
//------------------------------------------------------------------------------

{TdwInsMachNode}
//------------------------------------------------------------------------------
//--------------------------------------------------------------------------
procedure TdwInsMachNode.WndProc(var msg: TMessage);
var
  xFrontType: TFrontType;
  xLastAvailable: Boolean;
  //............................................................
  function CheckSpindleCount: Boolean;
  var
    xCount: Integer;
    i: Integer;
  begin
    Result := False;
    if mFullMachine.Available then begin
      xCount := 0;
      for i:=0 to mFullMachine.GroupCount-1 do
        xCount := xCount + (mFullMachine.GroupValueDef[i, cXPSpindleToItem, 1] - mFullMachine.GroupValueDef[i, cXPSpindleFromItem, 1] + 1);
      // Die Anzahl muss mit der LastSpindle stimmen, ansonsten hat es "L�cher"  
      Result := (xCount = mFullMachine.LastSpindle);
    end;
  end;
  //............................................................
begin
  if msg.Msg = mAssWindowMsg then begin
    try
      try
        case msg.WParam of
          WM_GETSETTINGS_DONE: begin
              // Wenn der SaveMaConfig Job fertig ist, wird Maconfig �ber MMClient benachrichtig. Dann muss
              // nur die Maschine neu geladen werden, damit die Konfiguration angezeigt wird.
              xLastAvailable := mFullMachine.Available;
              codesite.SendMsg(FormatXML(fSettingsHandler.XMLMaConfig));
              mFullMachine.LoadDOM(fSettingsHandler.XMLMaConfig);
              // wenn es eine Savio, Murata Maschine UND eine Inside ZE ist, dann beim 1. Upload FixSpdRange setzen
              if (not xLastAvailable) and mFullMachine.Available and (mFullMachine.Machine.NetTyp <> ntLX) then begin
                xFrontType := GetFrontTypeValue(mFullMachine.MachValueDef[cXPYMTypeItem, cFrontTypeNames[ftNone]]);
                mFullMachine.Machine.FixSpdRanges := (xFrontType in [ftZE80i, ftZE800i, ftZE900i]) and
                                                     (mFullMachine.Machine.MachineType in [mtSavioEspero, mtSavioOrion, mtMurata7_2, mtMurata7_5,
                                                      mtSavioEsperoNuovo, mtSavioExcello, mtSavioPolar]); //Nue:21.02.08
              end;
              Init(mFullMachine);
              // Wenn z.B. bei AC338 keine Partie in Produktion ist, dann kommt ein leeres MaConfig zur�ck
              // -> Hinweis ausgeben, dass ALLE Spulstellen aktiv sein m�ssen f�r einen sauberen Upload
//              if (not mFullMachine.Available) and (mFullMachine.Machine.MachineType = mtAC338) then
//Nue:13.6.06              if (not mFullMachine.Available) or (not CheckSpindleCount) then
              if (not mFullMachine.Available) or ((not CheckSpindleCount) and (mFullMachine.Machine.NetTyp=ntWSC))then
                ShowMessage(cNoGroupInProd);
              // Davon ausgehen, dass nach einem Upload etwas ge�ndert hat
              // -> beim Schliessen des Dialogs Daten im MainForm neu auslesen lassen
              fMachineChanged   := True;
              fSavedAfterChange := False;
            end;
          WM_GETSETTINGS_TIMEOUT: begin
              UserErrorMsg(cTimeoutMsg + '(TIMEOUT)');
              // Bei Fehler  im Upload kein Speicherhinweis anzeigen
              fMachineChanged   := False;
              fSavedAfterChange := True;
            end;
          WM_GETSETTINGS_FAILED: begin
              UserErrorMsg(cTimeoutMsg + '(FAILED)');
              // Bei Fehler  im Upload kein Speicherhinweis anzeigen
              fMachineChanged   := False;
              fSavedAfterChange := True;
            end;
        else
        end; // case
      finally
        DisableControls(True);
//        Self.Enabled := True; //Neu
//        Screen.Cursor := crDefault;
      end;
    except
      on e: Exception do
        SystemErrorMsg_('TdwInsMachNode.WndProc failed: ' + e.Message);
    end;
  end
  else if (msg.Msg = gMMClientMsgID) and Assigned(mFullMachine) and (mFullMachine.Validity = mvFull) then begin
    // Bei Status�nderungen der Maschine wird vom MMClient der MMRefresh als Window Msg weitergeleitet
    if msg.WParam = cMMRefresh then
      mMachineIsOnline := mFullMachine.Machine.IsOnline;
  end else
    inherited WndProc(msg);
end;
//------------------------------------------------------------------------------
procedure TdwInsMachNode.SelDropDownListItem(aComboBox: TmmComboBox; aID: Integer);
var
  xIndex: Integer;
begin
  xIndex := aComboBox.Items.IndexOfObject(TObject(aID));
  aComboBox.ItemIndex := xIndex;

//  for i:=0 to aComboBox.Items.Count-1 do begin
//    if Integer(aComboBox.Items.Objects[i]) = aID then begin
//      aComboBox.ItemIndex := i;
//      break;
//    end;
//  end;

//  for i := 0 to (acbObject.Items.Count - 1) do
//    if (acbObject.Items.Objects[i] as TMyIDClass).mID = aOrdID then begin
//      acbObject.ItemIndex := i;
//      break;
//    end;
end;
//------------------------------------------------------------------------------
procedure TdwInsMachNode.QryInsSpindle(aSpdID: Integer);
begin
{ TODO 1 -oNue -cXMLSettings : Brauchts diese Tabelle �berhaupt noch? Oder gen�gt es einen Eintrag in t_magroup_config zu machen?}
  with dmMaConfig.WorkCommand do
  try
    CommandText := cQryInsSpindle;
    Parameters.ParamByName('c_spindle_id').value := aSpdID;
    Parameters.ParamByName('c_machine_id').value := edMachID.AsInteger; //mFullMachine.MachineID;
    Parameters.ParamByName('c_head_type').value  := ORD(shNone);
    Parameters.ParamByName('c_AWE_type').value  := ORD(aweNone); //Added Nue:12.7.05
    Execute;
  except
    on e: Exception do begin
      e.Message := ('TdwInsMachNode.QryInsSpindle failed: ' + e.Message);
      Raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TdwInsMachNode.QryInsMachine;
var
  xCount: Integer;
begin
EnterMethod('QryInsMachine');
  with dmMaConfig.WorkCommand do
  try
    CommandText := cQryInsMachine;
    CodeSite.SendString('cQryInsMachine', cQryInsMachine);

    Parameters.ParamByName('c_machine_id').value          := edMachID.AsInteger; //mFullMachine.MachineID;
    Parameters.ParamByName('c_machine_name').value        := edMachName.Text;
    Parameters.ParamByName('c_net_id').value              := mFullMachine.NetNode.NetTyp;
    Parameters.ParamByName('c_node_id').value             := mNewNodeID;
    Parameters.ParamByName('c_machine_type').value        := Ord(mFullMachine.Machine.MachineType);
    Parameters.ParamByName('c_maoffset_id').value         := cDefaultMaOffsetID;
    Parameters.ParamByName('c_nr_of_spindles').value      := cMaxSpindeln;
    Parameters.ParamByName('c_spindle_pos').value         := 0; //cobSpindPos.ItemIndex; // mFullMachine.Machine.SpindlePos;
    Parameters.ParamByName('c_starting').DataType         := ftDateTime;
    Parameters.ParamByName('c_starting').value            := Now; //edStartTime.Value; // mFullMachine.Machine.Starting;
    Parameters.ParamByName('c_slip').value                := mFullMachine.Machine.Slip;
    Parameters.ParamByName('c_fixSpindlerange').value     := cbFixSpdRange.Checked; // mFullMachine.Machine.FixSpdRanges;
    Parameters.ParamByName('c_use_newer_mapfile').value   := cbNewestMap.Checked; // mFullMachine.Machine.CheckNewestMapfile;
    Parameters.ParamByName('c_data_collection').value     := 1; //cbDataCollection.Checked; // mFullMachine.Machine.DataCollection;
    if cobMapfile.ItemIndex > 0 then
      Parameters.ParamByName('c_overrule_mapfile_id').value := Integer(cobMapfile.Items.Objects[cobMapfile.ItemIndex])
    else
      Parameters.ParamByName('c_overrule_mapfile_id').value := NULL; //DefaultID, referenziert auf 'NO_MAP'  (nue:15.9.04)

    Execute;

    //Einfuegen Spindeln
    for xCount:=1 to cMaxSpindeln do
      QryInsSpindle(xCount);

//    if xSpindles = cMaxSpindeln then begin
//      for xCount:=1 to xSpindles do //F�r erste Maschine
//        QryInsSpindle(xCount);
//    end else begin
//      for xCount := mFullMachine.GroupValueDef[mFullMachine.ActiveSpdGrp-1,cXPSpindleFromItem,1] to
//                    mFullMachine.GroupValueDef[mFullMachine.ActiveSpdGrp-1,cXPSpindleToItem,1] do begin
//        QryInsSpindle(xCount);
//      end;
//    end;
  except
    on e: Exception do begin
      e.Message := ('TdwInsMachNode.QryInsMachine failed: ' + e.Message);
      Raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TdwInsMachNode.QryInsNode;
begin
  with dmMaConfig.WorkCommand do
  try
    CommandText := cQryInsNode;
    mFullMachine.NetNode.NodeId := mNewNodeID;
    Parameters.ParamByName('c_node_id').value    := mFullMachine.NetNode.NodeId;
    Parameters.ParamByName('c_net_id').value     := mFullMachine.NetNode.NetTyp;
    // wss: ob 80, 80i 800, 800i, Informator 68k/PPC wird nicht per GUI entschieden sondern per Bauzustand aus XML
    Parameters.ParamByName('c_front_type').value := 0; //Integer(cobFrontType.Items.Objects[cobFrontType.ItemIndex]);
    Parameters.ParamByName('c_node_stat').value  := ORD(nsOffline);
    Parameters.ParamByName('c_front_stat').value := ORD(nsOffline);
    if mFullMachine.NetNode.NetTyp = ntTXN then
      Parameters.ParamByName('c_adapt_or_name').value := cobAdapter.Text
    else
      Parameters.ParamByName('c_adapt_or_name').value := cobIPMachName.Text;
    Execute;
  except
    on e: Exception do begin
//      SystemErrorMsg_('TdwInsMachNode.QryInsNode failed: ' + e.Message);
      e.Message := ('TdwInsMachNode.QryInsNode failed: ' + e.Message);
      Raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TdwInsMachNode.QryUpdMachine;
begin
  with dmMaConfig.WorkDataSet do
  try
    Close;
    // erst pr�fen, ob es diese Maschine �berhaupt gibt
    CommandText := cQrySelMachine;
    Parameters.ParamByName('c_machine_id').value := edMachID.AsInteger; //mFullMachine.MachineID;
    Open;
    if FindFirst then begin
      with dmMaConfig.WorkCommand do begin
        Close;
        CommandText := cQryUpdMachine;
        Parameters.ParamByName('c_machine_id').value      := edMachID.AsInteger; //mFullMachine.MachineID;
        Parameters.ParamByName('c_machine_name').value    := edMachName.Text;
        Parameters.ParamByName('c_net_id').value          := mFullMachine.NetNode.NetTyp;
        Parameters.ParamByName('c_node_id').value         := mNewNodeID;
        Parameters.ParamByName('c_machine_type').value    := ord(mFullMachine.Machine.MachineType);
        Parameters.ParamByName('c_spindle_pos').value     := mFullMachine.Machine.SpindlePos;
        Parameters.ParamByName('c_nr_of_spindles').value  := mFullMachine.LastSpindle; // mFullMachine.GroupValue[tcSpindleRanges.TabIndex, cXPSpindleToItem];
        Parameters.ParamByName('c_slip').value            := mFullMachine.Machine.Slip;
        Parameters.ParamByName('c_fixSpindlerange').value := cbFixSpdRange.Checked; //mFullMachine.Machine.FixSpdRanges;
        Parameters.ParamByName('c_use_newer_mapfile').value   := cbNewestMap.Checked; // mFullMachine.Machine.CheckNewestMapfile;
        if cbDataCollection.Checked then
          Parameters.ParamByName('c_data_collection').value := 1
        else
          Parameters.ParamByName('c_data_collection').value := mFullMachine.Machine.DataCollection;

        if cobMapfile.ItemIndex > 0 then
          Parameters.ParamByName('c_overrule_mapfile_id').value := Integer(cobMapfile.Items.Objects[cobMapfile.ItemIndex])
        else
          Parameters.ParamByName('c_overrule_mapfile_id').value := NULL; //DefaultID, referenziert auf 'NO_MAP'  (nue:15.9.04)

        Execute;
      end; // with WorkCommand
    end; // if FindFirst cQrySelMachine
  except
    on e:Exception do begin
      e.Message := ('TdwInsMachNode.QryUpdMachine failed: ' + e.Message);
      Raise;
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TdwInsMachNode.QryUpdNode;
begin
  with dmMaConfig.WorkCommand do
  try
    CommandText := cQryUpdNode;
    Parameters.ParamByName('c_node_id').value       := mFullMachine.NetNode.NodeId;
    Parameters.ParamByName('c_net_id').value        := mFullMachine.NetNode.NetTyp;
    // wss: ob 80, 80i 800, 800i, Informator 68k/PPC wird nicht per GUI entschieden sondern per Bauzustand aus XML
    Parameters.ParamByName('c_front_type').value    := 0; //ORD(GetFrontTypeValue(mFullMachine.MachValueDef[cXPFrontTypeItem,cFrontTypeNames[TFrontType(0)]]));
//wss    Parameters.ParamByName('c_front_stat').value    := mFullMachine.NetNode.FrontState;
//wss    Parameters.ParamByName('c_node_stat').value     := mFullMachine.NetNode.NodeState;
    Parameters.ParamByName('c_newnode_id').value    := mNewNodeID;
    Parameters.ParamByName('c_adapt_or_name').value := mFullMachine.NetNode.AdaptOrName;
    Execute;
    // Neue NodeID erst jetzt �bernehmen
    mFullMachine.NetNode.NodeId := mNewNodeID;
  except
    on e:Exception do begin
      e.Message := ('TdwInsMachNode.QryUpdNode failed: ' + e.Message);
      Raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TdwInsMachNode.Init(aFullMachine: TMaConfigFullReader);
var
  i: Integer;
begin
  if Assigned(aFullMachine) then begin
    mFullMachine.Assign(aFullMachine);
    mNewNodeID := mFullMachine.NetNode.NodeId;
    mNewNetTyp := rgNetType.ItemIndex;  //Nue:18.9.08
  end
  else // if Assigned
    mFullMachine.Validity := mvNewMachine;

  CheckComponents;
  case mFullMachine.Validity of
    mvFull: begin
        mSaveFlag := True;
//        CheckComponents;
        FillComboBoxesFromNettyp;
        FillValues(mFullMachine.NetNode.NetTyp = ntTXN);
        // Der untere Bereich ist nur Sichtbar wenn wirklich auch MaConfig Informationen vorhanden sind
        pnSectionConfig.Visible := (mFullMachine.GroupCount > 0);
        if pnSectionConfig.Visible then begin
          Self.Height              := cLargeHeight;
          //Adding new tabsheets for spindlerages dynamicaly and add correct tabsheet-caption
          tcSpindleRanges.Tabs.Clear;
          for i:=0 to mFullMachine.GroupCount-1 do
            tcSpindleRanges.Tabs.Add(Format('%s %d', [cSpindleRange, i+1]));

          mGBMachConfigBox.Init; //Nue:7.2.06

          tcSpindleRanges.TabIndex := 0;
          tcSpindleRangesChange(Nil);
        end
        else begin
          Self.Height              := cSmallHeight;
        end;

        mNewFlag         := False;
        mMachineIsOnline := mFullMachine.Machine.IsOnline;
//        mUploadFlag   := (TTxnNodeState(mFullMachine.NetNode.NodeState) = nsOnline);
        Self.Position    := poScreenCenter;
      end;
    mvNewMachine: begin
        Self.Height := cSmallHeight;
        InvisibleNetFields;
        // Damit acSave erst nach eingabe der Maschinendaten freigegeben werden kann
        mComplete                     := False;
        mSaveFlag                     := False;
        mNewFlag                      := False;
        mFullMachine.Machine.Starting := Now();
        edMachName.Text               := '';

        rgNetType.Enabled             := True;
        rgNetType.ItemIndex           := -1; //No Selection
        tsMachineInfo.TabVisible      := False;
      end;
  end; //case

  if not TMMSettingsReader.Instance.HasNetTyp(ntTXN) then
    TControl(rgNetType.Components[ORD(ntTXN)-1]).Enabled := False;

  if not TMMSettingsReader.Instance.HasNetTyp(ntWSC) then
    TControl(rgNetType.Components[ORD(ntWSC)-1]).Enabled := False;

  if not TMMSettingsReader.Instance.HasNetTyp(ntLX) then
    TControl(rgNetType.Components[ORD(ntLX)-1]).Enabled := False;

  fSavedAfterChange := True;
  // Sodele, ab jetzt sind �nderungen zugelassen -> SaveValue()
  mEnabledChanges   := True;
end;
//------------------------------------------------------------------------------
procedure TdwInsMachNode.edLastSpindleKeyPress(Sender: TObject;
  var Key: Char);
begin
  CheckIntChar(Key);
end;
//------------------------------------------------------------------------------
procedure TdwInsMachNode.edLastSpindleExit(Sender: TObject);
begin
  // TODO wss: �hm hier eventuell noch was pr�fen wenn der Spindelbereich ver�ndert wurde
end;

//------------------------------------------------------------------------------
procedure TdwInsMachNode.edLastSpindleMachNodeChanged(Sender: TObject);
begin
{
  if (ucSpindleLast in AdminMachNode1.ChangedItems) or (edLastSpindle.Handle = GetFocus) then begin
    if edLastSpindle.Text = '' then begin
      edLastSpindle.SetFocus;
//      InfoMsg (cNrNotSet );
    end
    else if edLastSpindle.Text = '0' then begin
      Beep;
      edLastSpindle.SetFocus;
    end
    else if (StrToInt(edLastSpindle.Text) < StrToInt(edFirstSpindle.Text)) then begin
//      edLastSpindle.Text := edFirstSpindle.Text;
      edFirstSpindle.Text := edLastSpindle.Text;
    end
    else begin
//      fTmpMachineYMConfigRec := mFullMachine.MachineYMConfigRec;
      fTmpMachineYMConfigRec := mFullMachine;
//      fTmpMachineYMConfigRec.spec[mFullMachine.ActiveSpdGrp].spindle.stop := StrToInt(edLastSpindle.Text);
      fTmpMachineYMConfigRec.GroupValue[mFullMachine.ActiveSpdGrp,cXPSpindleToItem] := StrToInt(edFirstSpindle.Text);
//      mFullMachine.MachineYMConfigRec := fTmpMachineYMConfigRec;
      mFullMachine := fTmpMachineYMConfigRec;
//      mGBMachConfigBox.MachineConfig := mFullMachine.MachineYMConfigRec; //Nue:21.8.01
      mGBMachConfigBox.MaConfig := (mFullMachine as TXMLMaConfigHelper);
    end;
  end
  else if (ucReadAllData in AdminMachNode1.ChangedItems) then begin
//    edLastSpindle.Text := IntToStr(mFullMachine.MachineYMConfigRec.spec[mFullMachine.ActiveSpdGrp].spindle.stop);
    edLastSpindle.Text := IntToStr(mFullMachine.GroupValueDef[mFullMachine.ActiveSpdGrp,cXPSpindleToItem,1]);
  end
  else begin
  end;
{}
end;
//------------------------------------------------------------------------------
procedure TdwInsMachNode.edFirstSpindleKeyPress(Sender: TObject; var Key: Char);
begin
  CheckIntChar(Key);
//  AdminMachNode1.ItemChanged(ucSpindleFirst);
end;

//------------------------------------------------------------------------------
procedure TdwInsMachNode.edFirstSpindleExit(Sender: TObject);
begin
//  AdminMachNode1.ItemChanged(ucSpindleFirst);
end;
//------------------------------------------------------------------------------

//procedure TdwInsMachNode.edFirstSpindleMachNodeChanged(Sender: TObject);
//var
//  xCount, xCount2: Integer;
//begin
//{
//  if (ucSpindleFirst in AdminMachNode1.ChangedItems) or (edFirstSpindle.Handle = GetFocus) then begin
//    if edFirstSpindle.Text = '' then begin
//      edFirstSpindle.SetFocus;
////      InfoMsg (cNrNotSet );
//    end
//    else if edFirstSpindle.Text = '0' then begin
//      Beep;
//      edFirstSpindle.SetFocus;
//    end
//    else if (StrToInt(edFirstSpindle.Text) > StrToInt(edLastSpindle.Text)) then begin
////      edFirstSpindle.Text := edLastSpindle.Text;
//      edLastSpindle.Text := edFirstSpindle.Text;
//    end
//    else begin
////      fTmpMachineYMConfigRec := mFullMachine.MachineYMConfigRec;
////      fTmpMachineYMConfigRec.spec[mFullMachine.ActiveSpdGrp].spindle.start := StrToInt(edFirstSpindle.Text);
////      mFullMachine.MachineYMConfigRec := fTmpMachineYMConfigRec;
////      mGBMachConfigBox.MachineConfig := mFullMachine.MachineYMConfigRec; //Nue: 21.8.01
//      fTmpMachineYMConfigRec := mFullMachine;
//      fTmpMachineYMConfigRec.GroupValue[mFullMachine.ActiveSpdGrp,cXPSpindleFromItem] := StrToInt(edFirstSpindle.Text);
//      mFullMachine := fTmpMachineYMConfigRec;
//      mGBMachConfigBox.MaConfig := (mFullMachine as TXMLMaConfigHelper);
//    end;
//  end
//  else if (ucReadAllData in AdminMachNode1.ChangedItems) then begin
////    edFirstSpindle.Text := IntToStr(mFullMachine.MachineYMConfigRec.spec[mFullMachine.ActiveSpdGrp].spindle.start);
//    edFirstSpindle.Text := IntToStr(mFullMachine.GroupValueDef[mFullMachine.ActiveSpdGrp,cXPSpindleFromItem,1]);
//    xCount2 := 0;
//    for xCount := 1 to mFullMachine.ActiveSpdGrp do begin
//      if (mFullMachine.GroupValueDef[xCount,cXPSpindleFromItem,0] <> 0) and
//        (mFullMachine.GroupValueDef[xCount,cXPSpindleFromItem,0] <> $FF) then
//        inc(xCount2);
//    end; //for
//    tcSpindleRanges.TabIndex := xCount2 - 1;
//    tcSpindleRangesChange(Self);
//  end
//  else begin
//  end;
//{}
//end;
//------------------------------------------------------------------------------

procedure TdwInsMachNode.FillValues(aTexnet: Boolean);
begin
EnterMethod('FillValues');
  edMachName.Text          := mFullMachine.MachName;
  rgNetType.ItemIndex      := ORD(mFullMachine.NetNode.NetTyp) - 1;

  SelDropDownListItem(cobMachType, ord(mFullMachine.Machine.MachineType));
  SelDropDownListItem(cobSpindPos, mFullMachine.Machine.SpindlePos);
  SelDropDownListItem(cobMapfile,  mFullMachine.Machine.OverruleMapID);

  //Enablen cbNewestMap wenn Overrule not NONE  Nue:27.11.06
  if cobMapfile.ItemIndex > 0 then
    cbNewestMap.Enabled := True;

  if aTexnet then begin
    cobAdapter.ItemIndex := StrToIntDef(mFullMachine.NetNode.AdaptOrName, 1) - 1;
  end
  else begin
    edIPAddress.Text   := mFullMachine.NetNode.NodeId;
    cobIPMachName.Text := mFullMachine.NetNode.AdaptOrName;
  end;

  edMachID.AsInteger       := mFullMachine.MachID;
  edStartTime.Text         := DateTimeToStr(mFullMachine.Machine.Starting);

  if mFullMachine.Machine.Slip <> 0 then
    edSlip.Text := Format('%1.3f', [mFullMachine.Machine.Slip / 1000.0]);

  mFullMachine.Machine.OnlineSpeed := not mGBMachConfigBox.SpeedSimulation;
  cbFixSpdRange.Checked            := mFullMachine.Machine.FixSpdRanges;
  cbNewestMap.Checked              := mFullMachine.Machine.CheckNewestMapfile;
  cbDataCollection.Checked         := (mFullMachine.Machine.DataCollection >= 1);
  mGBMachConfigBox.MachineType     := mFullMachine.Machine.MachineType;
  mGBMachConfigBox.NetType         := mFullMachine.NetNode.NetTyp; //Nue: 6.2.06
end;

//procedure TdwInsMachNode.FillValues(aTexnet: Boolean);
//begin
////  edMachName.Enabled       := aMach;
////  edMachID.Visible         := True;
//
////  edMachIDl.Enabled         := False;
////  edStartTime.Enabled      := False;
////  cobSpindPos.Enabled      := True;
////  edSlip.Enabled           := True;
////  cbOnlineSpeed.Enabled    := False;
////  cbDataCollection.Enabled := True;
////  cbFixSpdRange.Enabled    := True;
////wss  edFirstSpindle.Enabled   := False; //aMach;
////  edLastSpindle.Enabled    := False; //aMach;
//
//
////  rgNetType.Enabled        := False;
////  rgNetType.Visible        := True;
////  cobNodeID.Enabled        := True;
////  cobNodeID.Visible        := aTexnet;
////  edIPAddress.Enabled      := True;
////  edIPAddress.Visible      := not aTexnet;
////  laIPAddress.Visible      := not aTexnet;
////  cobAdapter.Enabled       := True;
////  cobAdapter.Visible       := aTexnet;
////  cobIPMachName.Visible    := not aTexnet;
////  cobMachType.Enabled      := True;
////  cobMachType.Visible      := True;
////----------------------------------------------------------------------//
//  //Clear Text
////  edMachID.Text             := '1';
////  edStartTime.Text          := '';
////  cobSpindPos.Text          := '';
////  chkOnlineSpeed.Text    := '';
////  chkDataCollection.Text := '';
////  edMachName.Text        := '';
////  cobMachType.Text          := '';
////  edFirstSpindle.Text       := '1';
////  edLastSpindle.Text        := '1';
////  edSlip.Text               := '';
////
////  cobNodeID.Text            := '';
////  cobAdapter.Text           := '';
//
//  //Set field values
////  if aMach then begin
////    edMachName.Enabled       := True;
//    edMachName.Text          := mFullMachine.MachName;
//    rgNetType.ItemIndex      := ORD(mFullMachine.NetNode.NetTyp) - 1;
//
//    SelDropDownListItem(cobMachType, ord(mFullMachine.Machine.MachineType));
//    SelDropDownListItem(cobSpindPos, mFullMachine.Machine.SpindlePos);
//    SelDropDownListItem(cobMapfile,  mFullMachine.Machine.OverruleMapID);
//
//    if aTexnet then begin
//      cobAdapter.ItemIndex := StrToIntDef(mFullMachine.NetNode.AdaptOrName, 1) - 1;
//    end
//    else begin
//      edIPAddress.Text   := mFullMachine.NetNode.NodeId;
//      cobIPMachName.Text := mFullMachine.NetNode.AdaptOrName;
//    end;
//
//    edMachID.AsInteger       := mFullMachine.MachID;
////    cobSpindPos.Text         := cSpindPosNames[TSpindPos(mFullMachine.Machine.SpindlePos)];
//    edStartTime.Text         := DateTimeToStr(mFullMachine.Machine.Starting);
//
//    if mFullMachine.Machine.Slip <> 0 then
//      edSlip.Text := Format('%1.3f', [mFullMachine.Machine.Slip / 1000.0]);
////wss    mGBMachConfigBox.MaConfig        := (mFullMachine as TXMLMaConfigHelper);
////    mGBMachConfigBox.ActiveGroup     := 1;
////Alt    mGBMachConfigBox.ActiveGroup                 := mFullMachine.ActiveSpdGrp;
//
//    mFullMachine.Machine.OnlineSpeed := not mGBMachConfigBox.SpeedSimulation;
//    cbFixSpdRange.Checked            := mFullMachine.Machine.FixSpdRanges;
//    cbDataCollection.Checked         := (mFullMachine.Machine.DataCollection >= 1);
//    mGBMachConfigBox.MachineType     := mFullMachine.Machine.MachineType;
//// TODO wss: noch n�tig?
//{
//    if not (MMSecurityControl.CanEnabled(acFixSpdRange)) then
//      if mFullMachine.Machine.FixSpdRanges <> mGBMachConfigBox.DependendSpdleRange then begin
//        cbFixSpdRange.Checked := mGBMachConfigBox.DependendSpdleRange;
//        chkFixSpdRangeMachNodeChanged(Self);
//        fFixSpdRangeChanged := True;
//      end;
//
//    chkOnlineSpeed.Checked := mFullMachine.Machine.OnlineSpeed;
//    cbFixSpdRange.Enabled := mGBMachConfigBox.DependendSpdleRange;
//{}
////  end;
//
////  if aNode then begin
////    cobNodeID.Text  := mFullMachine.NetNode.NodeId;
////    cobAdapter.Text := mFullMachine.NetNode.AdaptOrName;
//    // wss: ob 80, 80i 800, 800i, Informator 68k/PPC wird nicht per GUI entschieden sondern per Bauzustand aus XML
////    SelDropDownListItem(cobFrontType, ORD(GetFrontTypeValue(mFullMachine.MachValueDef[cXPYMTypeItem, cFrontTypeNames[ftNone]])));
////  end;
//
//end;
//------------------------------------------------------------------------------
procedure TdwInsMachNode.InvisibleNetFields();
begin
  cobMachType.Visible      := False;

  cobNodeID.Visible        := False;
  cobAdapter.Visible       := False;

  laIPAddress.Visible      := False;
  edIPAddress.Visible      := False;
  cobIPMachName.Visible    := False;

  tsMachineInfo.TabVisible := False;
end;   
//------------------------------------------------------------------------------

procedure TdwInsMachNode.mActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  Handled := True;

  rgNetType.Visible     := (edMachName.Text <> '');
//  cbFixSpdRange.Enabled := MMSecurityControl.CanEnabled(acFixSpdRange);
  acUpload.Enabled      := {(DebugHook <> 0) or }(gMMAvailable and mMachineIsOnline and (not mUploadManuallyDisabled) and MMSecurityControl.CanEnabled(acUpload));
  acSave.Enabled        := mSaveFlag and ((not fSavedAfterChange) or mDifferentHashcode) and MMSecurityControl.CanEnabled(acSave);
  acNew.Enabled         := mNewFlag and MMSecurityControl.CanEnabled(acSave);
//  bCancel.Enabled       := acSave.Enabled {or (mFullMachine.Validity = mvNewMachine)};
  bOK.Enabled           := mComplete and (Self.Cursor = crDefault);
end;
//------------------------------------------------------------------------------

procedure TdwInsMachNode.FillComboBoxTXNNodes;
var
//  xID: Integer; //TMyIDClass;
  i: Integer;
  xStr: String;
begin
  cobNodeID.Clear;
  cobAdapter.Clear;
  with dmMaConfig.WorkDataSet do
  try
    Close;
    CommandText := cQrySelNetNodes + cQryWhereNetID;
    Parameters.ParamByName('netTyp').value := mFullMachine.NetNode.NetTyp;
    Open;

    //TEXNET
    if mFullMachine.NetNode.NetTyp = ntTXN then begin
      for i := 1 to cMaxTxnAdapter do
        cobAdapter.Items.Add(IntToStr(i));

      if mFullMachine.Validity in [mvNewMachine, mvFull] then begin
        // mal die Combobox mit allen NodeIDs abf�llen
        for i:=cMinTxnNodeID to cMaxTxnNodeID do
          cobNodeID.Items.Add(IntToStr(i));

        cobNodeID.ItemIndex := 0;
        // nun noch die schon verwendeten NodeIDs herausl�schen
        while not EOF do begin
          xStr := FieldByName('c_node_id').AsString;
          i := cobNodeID.Items.IndexOf(xStr);
          if i >= 0 then begin
            if (mFullMachine.Validity in [mvFull]) and (mFullMachine.NetNode.NodeId = xStr) then
              cobNodeID.ItemIndex := i
            else
              cobNodeID.Items.Delete(i);
          end;
          Next;
        end; // while not EOF
        if mFullMachine.Validity = mvNewMachine then 
          mNewNodeID := cobNodeID.Text;
      end;
    end
    //ETHERNET
    else if mFullMachine.NetNode.NetTyp in [ntWSC,ntLX] then begin
      cobIPMachName.Style := csSimple;
    end; //if
  except
    on e: Exception do begin
      SystemErrorMsg_('TdwInsMachNode.FillComboBoxTXNNodes failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TdwInsMachNode.cobAdapterChange(Sender: TObject);
begin
  if (mFullMachine.NetNode.AdaptOrName <> cobAdapter.Text) then begin
    mFullMachine.NetNode.AdaptOrName := cobAdapter.Text;
    if (mFullMachine.NetNode.NetTyp = ntTXN) and (mFullMachine.Validity = mvFull) then
      MMMessageDlg(Format('%s %s', [rsStopStartMM, rsDoUpload]), mtInformation, [mbOk], 0);
  end;
end;
//------------------------------------------------------------------------------

constructor TdwInsMachNode.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  HelpContext                  := GetHelpContext('Machinenkonfiguration\MACONF_Neue_Maschine_einfuegen.htm');
  edFirstSpindle.HelpContext   := GetHelpContext('Machinenkonfiguration\MACONF_Neue_Maschine_einfuegen.htm');
  edLastSpindle.HelpContext    := edFirstSpindle.HelpContext;
  edMachName.HelpContext       := edFirstSpindle.HelpContext;
  cobMachType.HelpContext      := edFirstSpindle.HelpContext;
  edMachID.HelpContext         := edFirstSpindle.HelpContext;
  edStartTime.HelpContext      := edFirstSpindle.HelpContext;
  cobSpindPos.HelpContext      := edFirstSpindle.HelpContext;
  edSlip.HelpContext           := edFirstSpindle.HelpContext;
  cbFixSpdRange.HelpContext    := edFirstSpindle.HelpContext;
  cbOnlineSpeed.HelpContext    := edFirstSpindle.HelpContext;
  cbDataCollection.HelpContext := edFirstSpindle.HelpContext;
  cobNodeID.HelpContext        := edFirstSpindle.HelpContext;
  cobAdapter.HelpContext       := edFirstSpindle.HelpContext;
  cobIPMachName.HelpContext    := edFirstSpindle.HelpContext;

  edLastSpindle.MaxValue       := cMaxSpindeln;

  fMachineChanged     := False;
  fRestartMM          := False;
  fSavedAfterChange   := True;
  fFixSpdRangeChanged := False;

  // Registrierung der Windows Meldung
  mAssWindowMsg      := RegisterWindowMessage(ASSIGN_REG_STR);
  mComplete          := True;
  mEnabledChanges    := False;
  mFullMachine       := TMaConfigFullReader.Create;
  mMachineIsOnline   := False;
  mDifferentHashcode := False;
  mNewHashcode1      := 0;
  mNewHashcode2      := 0;
  mUploadManuallyDisabled := False;

  // Mit mFullMachine wird gearbeitet. Damit die MachConfig Komponente das gleiche verwendet muss dieses
  // noch an die Komponente weitergegeben werden.
  mGBMachConfigBox.MaConfig := mFullMachine;
end;
//------------------------------------------------------------------------------

destructor TdwInsMachNode.Destroy;
begin
  FreeAndNil(mFullMachine);

  inherited Destroy;
end;

//------------------------------------------------------------------------------
procedure TdwInsMachNode.FillComboBoxesFromNettyp(aClearMachTypes:
    Boolean=True);
var
  xName: TSpindPos;
  xMachType: TMachineType;
begin
  if aClearMachTypes then begin   //Nue:16.06.08  if und aClearMachTypes eingef�gt
    cobMachType.Clear;
    for xMachType:=mtGeneric to High(TMachineType) do begin
      if mFullMachine.NetNode.NetTyp in cMachineTypeArr[xMachType].NetSet then
        cobMachType.Items.AddObject(cMachineTypeArr[xMachType].Name, TObject(xMachType));
    end;

    cobMachType.Sorted := True;   //Nue:21.02.08

    cobSpindPos.Clear;
    for xName:=spLeftDown to spRightTop do
      cobSpindPos.Items.Add(cSpindPosNames[xName]);
  end;

  dmMaConfig.FillMapfiles(cobMapfile.Items, mFullMachine.NetNode.NetTyp);
  FillComboBoxTXNNodes;

//  cobMachType.Clear;
//  cobSpindPos.Clear;
//  if dmMaConfig.FillMachineTypes(cobMachType.Items, mFullMachine.NetNode.NetTyp) then begin
//    for xName:=spLeftDown to spRightTop do
//      cobSpindPos.Items.Add(cSpindPosNames[xName]);
//
//    dmMaConfig.FillMapfiles(cobMapfile.Items, mFullMachine.NetNode.NetTyp);
//    FillComboBoxTXNNodes;
//  end else
//    MMMessageDlg(rsNoMachTypFound, mtWarning, [mbOk], 0);
end;
//------------------------------------------------------------------------------

function TdwInsMachNode.CheckInputValues: Integer;
begin
  Result := -1;
  pcMachineInfo.ActivePage := tsMachine;
  //Check entryfields Nue:12.2.03
  if edMachName.CanFocus and (edMachName.Text = '') then
    Result := 1
  else if cobMachType.CanFocus and (cobMachType.Text = '') then
    Result := 2
  //TEXNET
  else if mFullMachine.NetNode.NetTyp = ntTXN then begin
    if cobNodeID.CanFocus and (cobNodeID.Text = '') then begin
      Result := 3;
    end
    else if cobAdapter.CanFocus and (cobAdapter.Text = '') then begin
      Result := 4;
    end;
  end
  // WSC
  else if mFullMachine.NetNode.NetTyp in cNetTypeEthernet then begin
    if cobIPMachName.CanFocus and (cobIPMachName.Text = '') then begin
      Result := 6;
    end
    else if edIPAddress.CanFocus then begin
      if edIPAddress.Text = '' then begin
        Result := 5;
      end
      else begin
        if (mFullMachine.NetNode.NodeId <> mNewNodeID) and dmMaConfig.IPAdressInUse(edIPAddress.Text) then begin
          MMMessageDlg(rsIPUsed, mtError, [mbOk], 0);
          Result := 5;
        end;
      end;
    end;
  end;

  if edSlip.Text = '' then   //Check Syntax of field edSlip
    mFullMachine.Machine.Slip := 1000 //Nue:11.4.02 Before 0 now 1000
  else if (edSlip.AsFloat >= cMinSlip) and (edSlip.AsFloat <= cMaxSlip) then
    mFullMachine.Machine.Slip := Trunc(edSlip.AsFloat * 1000.0)
  else begin
    pcMachineInfo.ActivePage := tsMachineInfo;
    if edSlip.CanFocus then
      Result := 8;
  end;

  if Result = -1 then
    Result := 0
end;
//------------------------------------------------------------------------------
procedure TdwInsMachNode.acSaveExecute(Sender: TObject);
begin
  mComplete     := False;
  Screen.Cursor := crHourGlass;
  try
    edLastSpindle.Validate; //Nue:13.6.06
    //Check entryfields Nue:12.2.03
    case CheckInputValues of
      1: edMachName.SetFocus;
      2: cobMachType.SetFocus;
      3: cobNodeID.SetFocus;
      4: cobAdapter.SetFocus;
      5: edIPAddress.SetFocus;
      6: cobIPMachName.SetFocus;
      8: edSlip.SetFocus;
    else
      mComplete := True;
    end;
    if not mComplete then
      Exit;

    with dmMaConfig.WorkDataSet do begin
      case mFullMachine.Validity of
        mvFull: begin
            try
              edMachName.Text := Trim(edMachName.Text);
              if dmMaConfig.IsUniqueMachName(edMachID.AsInteger, edMachName.Text) then begin
                // Vormerken, ob z.B. die NodeID, Adapter ge�ndert hat -> RestartMM Notwendig
                Connection.BeginTrans;
                QryUpdMachine;
                QryUpdNode;
                Connection.CommitTrans;
                if mFullMachine.GroupCount > 0 then begin
                  SaveMaConfigToDB(mFullMachine.MachID, mFullMachine.LastSpindle, mFullMachine.DOM.xml, Nil);
                  mFullMachine.LoadMachConfigFromDB(mFullMachine.MachID);
                  mDifferentHashcode := False;
                end;
                fSavedAfterChange := True;
                fMachineChanged   := True;
                MMMessageDlg(rsSuccSaved, mtInformation, [mbOk], 0);
              end else
                MMMessageDlg(rsMachNameNotUnique, mtError, [mbOk], 0);
            except
              on e: Exception do begin
                SystemErrorMsg_(' TdwInsMachNode.acUpdateExecute failed. ' + e.Message);
                Connection.RollbackTrans;
              end;
            end;
          end;
        mvNewMachine: begin
            try //Add machine and node
              mFullMachine.Machine.Slip := 1000;
              edMachName.Text           := Trim(edMachName.Text);
              edMachID.AsInteger        := dmMaConfig.GetNewMachineID;
              if dmMaConfig.IsUniqueMachName(edMachID.AsInteger, edMachName.Text) then begin
                Connection.BeginTrans;
                QryInsMachine;
                QryInsNode;
                Connection.CommitTrans;
                rgNetType.Enabled     := False;
                mNewFlag              := True;
                mFullMachine.Validity := mvFull;
                fSavedAfterChange     := True;
                fMachineChanged       := True;
                fRestartMM            := True;
                MMMessageDlg(rsSuccInserted, mtInformation, [mbOk], 0);
              end else
                MMMessageDlg(rsMachNameNotUnique, mtError, [mbOk], 0);
            except
              on e: Exception do begin
                Connection.RollbackTrans;
                MMMessageDlg(cNotUnique + ' TdwInsMachNode.cQueryInsMachine/cQryInsSpindle failed. ' + e.Message, mtError, [mbOk], 0);
              end;
            end;
          end;
      else
      end; // case
    end; // with
  finally
    if not mComplete then
      Beep;
    Screen.Cursor := crDefault;
  end; // try
end;
//------------------------------------------------------------------------------

procedure TdwInsMachNode.OnValueChange(Sender: TObject);
begin
  SaveValue(TControl(Sender).Tag);
end;
//------------------------------------------------------------------------------

procedure TdwInsMachNode.acNewExecute(Sender: TObject);
begin
  if not fSavedAfterChange then
    acSaveExecute(Sender);

  if mComplete then begin
    edMachName.Text    := '';
    edIPAddress.Text   := '';
    cobIPMachName.Text := '';

    mFullMachine.Validity := mvNewMachine;
    Init(Nil);
  end;
end;
//------------------------------------------------------------------------------

procedure TdwInsMachNode.mGBMachConfigBoxMachNodeChanged(Sender: TObject);
begin
  if mFullMachine.GroupCount > 0 then begin
    HashByMM(mFullMachine.DOM.xml, mNewHashcode1, mNewHashcode2);
    mDifferentHashcode := (mFullMachine.DBHashcode1 <> mNewHashcode1) or (mFullMachine.DBHashcode2 <> mNewHashcode2);
  end;
end;

//------------------------------------------------------------------------------

procedure TdwInsMachNode.cbDataCollectionClick(Sender: TObject);
begin
  if not cbDataCollection.Checked then begin
    with dmMaConfig.WorkDataSet do
    try
      Close;
      CommandText := cQryGetRunningProdGrps;
      Parameters.ParamByName('c_machine_id').value := mFullMachine.MachID;
      Open;
      if FindFirst then begin
        MMMessageDlg(cProdGrpRunning, mtInformation, [mbOk], 0);
        cbDataCollection.Checked := True;
      end;
    except
      on e: Exception do begin
        SystemErrorMsg_(' TdwInsMachNode.chkDataCollectionClick: cQryGetRunningProdGrps failed. ' + e.Message);
      end;
    end; // with
  end; // if
  SaveValue(TControl(Sender).Tag);
end;
//------------------------------------------------------------------------------

procedure TdwInsMachNode.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if acSave.Enabled or fFixSpdRangeChanged then begin
    case MMMessageDlg(cNotSaved, mtWarning, [mbYes, mbNo, mbCancel], 0) of
      mrYes: begin
          fFixSpdRangeChanged := False;
          acSaveExecute(Sender);
          if not mComplete then
            CanClose := False;
        end;
      mrNo: begin
        end;
      mrCancel:
        CanClose := False;
    else
    end;
  end; // if acSave.Enabled or fFixSpdRangeChanged

// TODO wss: CanClose pr�fen
  if CanClose and fRestartMM then begin
    if gMMAvailable then begin
//      CanClose := False;
      case MMMessageDlg(rsRestartMM, mtConfirmation, [mbOK, mbCancel, mbIgnore], 0) of
        mrOK: begin
            CanClose := True;
//            SendGuardCommand(gcRestartMM);
          end;
        mrCancel: begin
            CanClose := False;
          end;
        mrIgnore: begin
            fRestartMM  := False;  // Dann halt nicht neu starten
            ModalResult := mrCancel;
            CanClose := True;
          end;
      end; // case MMMessageDlg
    end; // if gMMAvailable
  end; // if CanClose
end;

//------------------------------------------------------------------------------
procedure TdwInsMachNode.acSecurityConfigExecute(Sender: TObject);
begin
  MMSecurityControl.Configure;
end;

//------------------------------------------------------------------------------
procedure TdwInsMachNode.tcSpindleRangesChange(Sender: TObject);
begin
  mEnabledChanges := False;
  with tcSpindleRanges do begin
    mGBMachConfigBox.ActiveGroup := TabIndex; // + 1;
    edFirstSpindle.AsInteger     := mFullMachine.GroupValueDef[TabIndex, cXPSpindleFromItem, 1];
    edLastSpindle.AsInteger      := mFullMachine.GroupValueDef[TabIndex, cXPSpindleToItem, 1];
    edLastSpindle.MinValue       := edFirstSpindle.AsInteger;
    edLastSpindle.ReadOnly       := (TabIndex < (Tabs.Count-1));
    edLastSpindle.Validate; //Nue:13.6.06
  end;
  mEnabledChanges := True;
end;
//------------------------------------------------------------------------------
procedure TdwInsMachNode.rgNetTypeClick(Sender: TObject);
begin
//alt  if mFullMachine.Validity = mvNewMachine then
  if (mFullMachine.Validity = mvNewMachine) or (mFullMachine.Validity = mvFull) then
    SaveValue(TControl(Sender).Tag);
end;
//:-----------------------------------------------------------------------------
procedure TdwInsMachNode.acUploadExecute(Sender: TObject);
begin
//  if dmMaConfig.MachineIsOnline(mFullMachine.MachID) then begin
  if mFullMachine.Machine.IsOnline then begin
    if not Assigned(fSettingsHandler) then
      fSettingsHandler := TGetSettingsHandler.Create(Self);
//    Self.Cursor := crSQLWait; // crHourGlass;
//    Screen.Cursor := crHourGlass;
//    Self.Enabled  := False; // Wird in WndProc wieder freigegeben
    SettingsHandler.AssMachine := mFullMachine.Machine;
    SettingsHandler.StartSaveSettings(Self.Handle);
    DisableControls(False);
//    Self.Cursor  := crHourGlass;
//    Self.Enabled := False; // Wird in WndProc wieder freigegeben
  end else
    MMMessageDlg(Format(rsMachineReadNOk, [mFullMachine.MachName]), mtWarning, [mbOk], 0);
end;

//------------------------------------------------------------------------------
procedure TdwInsMachNode.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;

//:-----------------------------------------------------------------------------
procedure TdwInsMachNode.acEnableNetExecute(Sender: TObject);
begin
  if mFullMachine.Validity = mvFull then
    rgNetType.Enabled := not(rgNetType.Enabled);
end;

//:-----------------------------------------------------------------------------
procedure TdwInsMachNode.CheckComponents;
var
  xBool: Boolean;
  xFixSpd: Boolean;
  xFrontType: TFrontType;
begin
  xBool := (mFullMachine.NetNode.NetTyp = ntTXN);
  cobNodeID.Visible     := xBool;
  cobAdapter.Visible    := xBool;
  laIPAddress.Visible   := not xBool;
  edIPAddress.Visible   := not xBool;
  cobIPMachName.Visible := not xBool;
  cbNewestMap.Visible := not xBool;   //Nue:27.11.06

  cobMapfile.Enabled    := MMSecurityControl.CanEnabled(acOverruleMapfile);

{
    cDependendSpdleRange:
      Result := ((MachineConfig.frontType = ftZE80i) or
                 (MachineConfig.frontType = ftZE800i)) AND

                ((MachineConfig.aWEMachType = amtESPERO) or
                 (MachineConfig.aWEMachType = amtMUR_IND_INV) or
                 (MachineConfig.aWEMachType = amtOrion)) OR

                (MachineConfig.aWEMachType = amtAC338); //Added by Nue 5.11.01
{}
  if mFullMachine.NetNode.NetTyp = ntLX then begin
    cbFixSpdRange.Enabled := False;
  end
  else begin
    xFixSpd := (mFullMachine.Machine.MachineType in [mtGeneric, mtAC338, {Nue:21.02.08}mtAC5]);
    if mFullMachine.Available then begin
      // TODO wss: tempor�re M�glichkeit einen FrontType zu simulieren. MUSS WIEDER ENTFERNT WERDEN!!!
//      xFrontType := GetFrontTypeValue(GetRegString(cRegLM, cRegMMDebug, 'FrontType', mFullMachine.MachValueDef[cXPYMTypeItem, cFrontTypeNames[ftNone]]));
      xFrontType := GetFrontTypeValue(mFullMachine.MachValueDef[cXPYMTypeItem, cFrontTypeNames[ftNone]]);
      xFixSpd := xFixSpd OR ((xFrontType in [ftZE80i, ftZE800i, ftZE900i]) and
                             (mFullMachine.Machine.MachineType in [mtSavioEspero, mtSavioOrion, mtMurata7_2, mtMurata7_5,
                                                      mtSavioEsperoNuovo, mtSavioExcello, mtSavioPolar])); //Nue:21.02.08

    end;
    cbFixSpdRange.Enabled := xFixSpd and MMSecurityControl.CanEnabled(acFixSpdRange);
  end;
end;
//:-----------------------------------------------------------------------------
procedure TdwInsMachNode.DisableControls(aEnabled: Boolean);
begin
  if aEnabled then Self.Cursor := crDefault
              else Self.Cursor := crHourGlass;
  tbMain.Enabled          := aEnabled;
  pcMachineInfo.Enabled   := aEnabled;
  pnSectionConfig.Enabled := aEnabled;
end;

//:-----------------------------------------------------------------------------
procedure TdwInsMachNode.SaveValue(aTag: Integer);
begin
  if mEnabledChanges then begin
    if mFullMachine.Validity = mvFull then begin
      case aTag of
        // Maschinenname
        0: ;  // wird direkt von edMachName.Text genommen
        // Netztyp
//alt        1: ;
        1: begin  //Nue:16.06.08
            mFullMachine.NetNode.NetTyp := TNetTyp(rgNetType.ItemIndex + 1);
            // Flag in Komponente setzen, da bei Save dies verwendet wird
            cbFixSpdRange.Checked := (mFullMachine.NetNode.NetTyp in [ntWSC, ntLX]);
            CheckComponents;
//            InvisibleNetFields;
            FillComboBoxesFromNettyp(False);  //FALSE=Kein L�schen der Maschinentypen
            cobMachType.Visible := True;
            if mNewNetTyp <> rgNetType.ItemIndex then begin  //Nue:18.9.08
              DoRestartMMInfo;
            end;
           end;
        // Maschinentyp
        // Maschinentyp
        2: begin
            mFullMachine.Machine.MachineType := TMachineType(cobMachType.Items.Objects[cobMachType.ItemIndex]);
            mGBMachConfigBox.MachineType     := mFullMachine.Machine.MachineType;
          end;
        // ZE NodeID
        3: begin
            if cobNodeID.Text <> mNewNodeID then begin
              mNewNodeID := cobNodeID.Text;
              DoRestartMMInfo;
            end;
          end;
        // IP Adresse
        4: begin
            if edIPAddress.Text <> mNewNodeID then begin
              mNewNodeID := edIPAddress.Text;
              DoRestartMMInfo;
            end;
          end;
        // Hostname
        5: mFullMachine.NetNode.AdaptOrName := cobIPMachName.Text;
        // TXN Adapter
        6: begin
            if mFullMachine.NetNode.AdaptOrName <> cobAdapter.Text then begin
              mFullMachine.NetNode.AdaptOrName := cobAdapter.Text;
              DoRestartMMInfo;
            end;
          end;
        // MachID
        7: ; //mFullMachine.MachineID := StrToInt(edMachID.Text);
        // Spindelpos
        8: mFullMachine.Machine.SpindlePos := cobSpindPos.ItemIndex;
        // Startzeit
        9:;
        // Schlupf
        10: mFullMachine.Machine.Slip := Trunc(edSlip.AsFloat * 1000.0);
        // Fix Spindelbereich
        11: mFullMachine.Machine.FixSpdRanges := cbFixSpdRange.Checked;
        // Onlinespeed
        12: mFullMachine.Machine.OnlineSpeed := cbOnlineSpeed.Checked;
        // Datenerfassung
        13:  if cbDataCollection.Checked then mFullMachine.Machine.DataCollection := 1
                                         else mFullMachine.Machine.DataCollection := 0;
        14: // Mapfile wird direkt in QryUpdMachine verwendet
            begin
              cbNewestMap.Checked := True;
              if cobMapfile.ItemIndex > 0 then
                cbNewestMap.Enabled := True
              else
                cbNewestMap.Enabled := False;
            end;
        15: mFullMachine.GroupValue[tcSpindleRanges.TabIndex, cXPSpindleToItem] := edLastSpindle.AsInteger;
        //Check Newest Mapfile
        16: mFullMachine.Machine.CheckNewestMapfile := cbNewestMap.Checked;
      else
      end;
    end
    else begin
      case aTag of
        // Netztyp
        1: begin
            mFullMachine.NetNode.NetTyp := TNetTyp(rgNetType.ItemIndex + 1);
            // Flag in Komponente setzen, da bei Save dies verwendet wird
            cbFixSpdRange.Checked := (mFullMachine.NetNode.NetTyp in [ntWSC, ntLX]);
            CheckComponents;
//            InvisibleNetFields;
            FillComboBoxesFromNettyp;
            cobMachType.Visible := True;
          end;
        // Maschinentyp
        2: begin
            mFullMachine.Machine.MachineType := TMachineType(cobMachType.Items.Objects[cobMachType.ItemIndex]);
          end;
        // ZE NodeID
        3: mNewNodeID := cobNodeID.Text;
        // IP Adresse
        4: mNewNodeID := edIPAddress.Text;
        // Frontmodel
        6: begin
            // wss: ob 80, 80i 800, 800i, Informator 68k/PPC wird nicht per GUI entschieden sondern per Bauzustand aus XML
  //          if TFrontType(cobFrontType.Items.Objects[cobFrontType.ItemIndex]) in [ftZE80i, ftZE800i, ftInf68K, ftInfPPC] then begin
  //            cbFixSpdRange.Enabled := False;
  //            cbFixSpdRange.Checked := True;
  //          end;
          end;
      else
      end;
      mSaveFlag := (edMachName.Text <> '') and (rgNetType.ItemIndex <> -1) and (cobMachType.ItemIndex <> -1) AND
                   ( ((cobNodeID.ItemIndex <> -1) and (cobAdapter.ItemIndex <> -1)) OR  // TXN Werte
                     ((edIPAddress.Text <> '') and (cobIPMachName.Text <> '')) );      // IP Werte
      mComplete := mSaveFlag;
    end;  // mFullMachine.Validity = mvFull
    fSavedAfterChange := False;
    CheckComponents;
  end; // if mEnabledChanges
end;
//:-----------------------------------------------------------------------------
//procedure TdwInsMachNode.edIPAdressKeyPress(Sender: TObject; var Key: Char);
//begin
//  SaveValue(edIPAddress.Tag);
//end;
//:-----------------------------------------------------------------------------
procedure TdwInsMachNode.MMSecurityControlAfterSecurityCheck(
  Sender: TObject);
begin
  acUpload.Visible := acUpload.Visible and (mFullMachine.Validity <> mvNewMachine);
end;
//:-----------------------------------------------------------------------------
procedure TdwInsMachNode.acExitExecute(Sender: TObject);
begin
  Close;
end;

//:-----------------------------------------------------------------------------
procedure TdwInsMachNode.DoRestartMMInfo;
begin
  if mFullMachine.Validity = mvFull then begin
    fRestartMM := True;
    if not mUploadManuallyDisabled then begin
      mUploadManuallyDisabled := True;
      // Bewirkt dass MMRestart Info nur 1x kommt und acUpload permanent disabled wird wenn Node/IP ver�ndert wurde
      MMMessageDlg(Format('%s %s', [rsStopStartMM, rsDoUpload]), mtInformation, [mbOk], 0);
    end;
  end;
end;
//:-----------------------------------------------------------------------------

end.

