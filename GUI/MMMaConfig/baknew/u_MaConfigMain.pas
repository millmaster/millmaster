(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MaConfigMain.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.12.1999  1.00  Nue  | Datei erstellt
| 29.01.2001  1.01  Nue  | Integration Componente from Kr in u_InsMachNode
| 25.03.2001  1.02  Nue  | Suggested efficiency removed
| 28.08.2001  1.03  Nue  | Access over ODBC and no further over Native-DB-Driver
|                        | GUI-Component mDatabase: New =>Connected:=True (helped to
|                        | solve the problems at Streiff with BDE-Initalisation-crash
| 22.10.2001  1.04  Nue  | (Row > FixedRows) changed to (Row >= FixedRows)
| 01.11.2001  1.05  Nue  | Order of the columns in the grid changed.
| 24.01.2002  1.06  Nue  | Several minor changes. (Especially in section "Upload from machines")
| 07.03.2002  1.07  Nue  | Additional ResourceString cLoadNewData and corresponding handling added.
| 22.03.2002  1.08  Wss  | Umgestellt auf neues PrintForm, da beim alten beim Drucken das Form
                           immer sichtbar war
| 25.03.2002  1.09  Nue  | ResourceString cLoadFromMach before cLoadNewData added.
| 31.10.2002  1.10  Nue  | mrIgnore in acMachNodeDisconnectExecute added.
| 07.11.2002        Nue  | Problems with spindlerange changing on new machines handled.
| 08.11.2002        LOK  | Umbau ADO
| 10.12.2002        Nue  | Setzen des Maschinenstatus auf Online nach erfolgreichem Upload.
| 18.02.2003        Wss  | Funktion FromMachine abh�ngig, ob MMSystem vorhanden ist oder nicht.
                           MMClient MUSS gestarted sein, da auf Broadcast Meldungen reagiert wird!!
| 27.02.2003  1.11  Wss  | Beim warten Systemmeldung bei neuen Maschinen Toolbar und
                           Grid disabled, damit keine Mausaktivit�t ausgel�st werden kann.
                           Self.Enabled geht nicht, da die Sanduhr damit verschwindet!
| 05.03.2003        Nue  | Grossumbau auf EasyVersion.
| 18.03.2003        Nue  | Defaultausdruck auf poLandscape.
| 24.03.2003  1.12  Nue  | Security eingebaut und Verhalten von Buttons usw. in mmActionListMainUpdate verlegt.
| 30.06.2003  1.13  Nue  | Begrezen Texte in Toolbar-Buttons; Grid alBotton und Ausrichtung.
| 01.10.2003  1.13  Wss  | Druckbefehl erfolgt nun �ber PrintGrid Komponente
| 20.06.2004  1.13  SDo  | Print-DLG eingebaut, Preview vom Menue funktioniert jetzt
| 10.08.2004  1.14  Nue  | Printers.Printer.Orientation := poLandscape; Aus Init nach acPrintExecute gez�gelt)
|=============================================================================*)
unit u_MaConfigMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEAPPLMAIN, Menus, mmMainMenu, IvDictio, IvAMulti, IvBinDic,
  mmDictionary, IvMulti, IvEMulti, mmTranslator, mmLogin, mmPopupMenu,
  mmOpenDialog, IvMlDlgs, mmSaveDialog, mmPrintDialog,
  mmPrinterSetupDialog, Db, StdActns,
  ActnList, mmActionList, ImgList, mmImageList, ComCtrls, mmStatusBar,
  ToolWin, mmToolBar, mmAnimate, ExtCtrls, mmPanel, Grids, AdvGrid,
  mmExtStringGrid, StdCtrls, mmLabel, mmEdit,
  AssignComp, BaseGlobal, MMUGlobal, mmList, mmDialogs,
  u_MaConfigComp, u_InsMachNode, u_ClearerType, u_HeadType,
  u_NetType, u_MachineType, u_FrontType,
  mmComboBox, mmGroupBox, YMParaUtils, YMParaDef, MMSecurity, LoepfeGlobal,
  mmStringGrid, ShellAPI, MMHtmlHelp, u_dmMaConfig, ADODB, mmADODataSet, mmADOCommand,
  xmlMaConfigClasses, mmButton,
  Printers,  //Nue:17.3.03
  AdoDBAccess, MMQualityMatrixClass;

resourcestring
  cMachineReadOk = '(*)Daten von Maschine %s erfolgreich gelesen!'; //ivlm
  cMachineReadNOk = '(*)Daten von Maschine %s konnten nicht gelesen werden!'; //ivlm
  cLoadNewData = '(*)(Einige Datenfelder sind nicht mehr aktuell! Neu laden!)'; //ivlm
  cLoadFromMach = '(*)Daten geladen von Maschinen'; //ivlm
  cLoadFromDB = '(*)Daten geladen von Datenbank'; //ivlm
  cObjectNotFound = '(*)Objekt konnte nicht gefunden werden:'; //ivlm
  cTexnetTester = '(*)Der TexnetTester laeuft nur auf dem Server und nur bei ausgeschaltetem Basissystem!'; //ivlm
  cUploadProblem = '(*)Beim Upload von Maschine "%s" ist ein Problem aufgetaucht! (MaConfig neu starten!)'; //ivlm
  cNoGroupInProd = '(*)(ACHTUNG! Bei Maschinen vom Typ AC338 muessen ALLE Spulstellen in Produktion sein um die komplette Maschinenkonfiguration von allen Spulstellen korrekt hoch zuladen!)'; //ivlm
//  cNoGroupInProd    = '(*)ACHTUNG! Bei Maschine "%s" vom Typ AC338 ist keine Partie in Produktion. Um die komplette Maschinenkonfiguration von allen Spulstellen korrekt hoch zuladen muessen bei ALLEN AC338-Maschinen ALLE Spulstellen in Produktion sein!';

const
  cDefColCount = 20; //Nr of col count in Grid
  cDefRowCount = 2; //Nr of row count in Grid
  cOrgGridIndexCol = 37; //Nr of col count in Grid during design mode
  cMachTypeCol = 6;
  cNotFound = 'Not Found';
//  cTrue = 'YES';
//  cFalse = 'NO';
  c_mrMachInserted = mrAll + 1; //ModalResult to signalize, that at least one new machine was inserted

  cNoYesStrArr: Array[Boolean] of String = ('NO', 'YES');

  cUnusedComponents: array[1..12] of string = (
    'acNew', 'acOpen', 'acSave', 'acSaveas', {'acPreview', 'acPrint',{}
    'acWindowCascade', 'acWindowTileVertical', 'acTileHorizontally',
    'miNew', 'miOpen', 'miSave', 'miSaveAs', {'miPreview', 'miPrint',{} 'miFenster'
    );

type
  TFileOpenMode = (omOpen, omPrint, omExplore);

const
  cFileOpenMode: array[TFileOpenMode] of string = ('open', 'print', 'explore');

type
  TMaConfigMain = class(TBaseApplMainForm)
    MaschinenDaten1: TMenuItem;
    acDBData: TAction;
    DBDaten1: TMenuItem;
    mmSGMachNode: TmmExtStringGrid;
    acInsClearer: TAction;
    acInsert1: TMenuItem;
    acInsClearer1: TMenuItem;
    acInsSensHead: TAction;
    acInsSensHead1: TMenuItem;
    acNewMach: TAction;
    acMachNode1: TMenuItem;
    acChange: TAction;
    acMachNodeDisconnect1: TMenuItem;
    gbMachState: TmmGroupBox;
    labOnline: TmmLabel;
    labOffline: TmmLabel;
    labInProd: TmmLabel;
    labLoadInfo: TmmLabel;
    acInsNetTypen: TAction;
    acInsMachType: TAction;
    acInsFrontType: TAction;
    MMSecurityDB: TMMSecurityDB;
    MMSecurityControl: TMMSecurityControl;
    acSecurityConfig: TAction;
    Zugriffkontrolle1: TMenuItem;
    NeuerKnoten1: TMenuItem;
    acMachineData: TAction;
    acInsFrontType1: TMenuItem;
    acInsMachType1: TMenuItem;
    acInsNetTypen1: TMenuItem;
    acDelete: TAction;
    fQuery: TmmADODataSet;
    fQuery1: TmmADODataSet;
    fQuery2: TmmADODataSet;
    Tools1: TMenuItem;
    miTexnetTester: TMenuItem;
    miInformatorTester: TMenuItem;
    N2: TMenuItem;
    Maschine1: TMenuItem;
    Netzadresse1: TMenuItem;
    Maschineaktivieren1: TMenuItem;
    MMHtmlHelp: TMMHtmlHelp;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    cmdADO: TmmADOCommand;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure acMachineDataExecute(Sender: TObject);
    procedure acDBDataExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure mmSGMachNodeClickCell(Sender: TObject; Arow, Acol: Integer);
    procedure mmSGMachNodeDblClickCell(Sender: TObject; Arow, Acol: Integer);
    procedure acInsertExecute(Sender: TObject);
    procedure acInsClearerExecute(Sender: TObject);
    procedure acNewMachExecute(Sender: TObject);
//    procedure acDownloadExecute(Sender: TObject);
    procedure mmSGMachNodeGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure acChangeExecute(Sender: TObject);
    procedure acInsSensHeadExecute(Sender: TObject);
    procedure acInsNetTypenExecute(Sender: TObject);
    procedure acInsMachTypeExecute(Sender: TObject);
    procedure acInsFrontTypeExecute(Sender: TObject);
    procedure acSecurityConfigExecute(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure miTexnetTesterClick(Sender: TObject);
    procedure miInformatorTesterClick(Sender: TObject);
    procedure acPreViewExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure mmSGMachNodeClickSort(Sender: TObject; aCol: Integer);
    procedure mmSGMachNodeCanSort(Sender: TObject; aCol: Integer; var dosort: Boolean);
    procedure mDictionaryAfterLangChange(Sender: TObject);
    procedure mmActionListMainUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure acLogin1Execute(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    mMMClientMsgID: DWord;
    mMachineDataFlag: Boolean;
    mDeleteFlag: Boolean;
    mChangeFlag: Boolean;
    mUploadReminderMsg: Boolean;
    mViewFromDB: Boolean;
//    mOnlyNode       : Integer;    //Index of mMachineDBArr if OnlyNode else -1
//    mOnlyNode       : Integer;    //Index of mMachineDBArr if OnlyNode else -1
    mNodeStates: TNodeStatesSet;
    mAssWindowMsg: DWord;
//    mWindowHandle  : HWND;
    fSettingsHandler: TGetSettingsHandler;
    fSetMaConfigHandler: TSetMaConfigHandler;
    fYMSettingsUtils: TYMMachineSettings;
    fInsMachFormChanged: Boolean;
    fGridNewSorted: Boolean;
    mFullMachine: TFullMachine;
    mMachineMaArr: TFullMachineList;
    mMachineDBArr: TFullMachineList;
    mDBAccess: TAdoDBAccess;
    mMachInserted: Boolean;
    mMaConfigReaderList: TMaConfigReader;
    function StartApplication(aFileName: string; aMode: TFileOpenMode; aParam: string): THandle;
    function FillSelLineToFullMachine(aValidity: TMachValidity): TFullMachine;
    procedure Init;
    procedure CreateInsMachNode(aFullMachine: TFullMachine);
(*    procedure GetFromMachines;*)
    procedure WriteDBInfoToCells(aRow: Integer; aStringGrid: TmmExtStringGrid; aArr: TFullMachine; aGroupSpecRec: TGroupSpecificMachConfigRec);
//   procedure WriteMaInfoToCells(aCount, aRow: Integer; aStringGrid: TmmExtStringGrid);
    procedure AddSingleSGLine(Index: Integer);
    function GetActMachArr: TFullMachineList;
   //   procedure SetSGLength(aQuery: TmmQuery; var aMachineArr: array of TFullMachine);
    procedure QryDelNode;
    procedure QryDelMachine;
    procedure SetEnabledState(aEnabled: Boolean);
  protected
    procedure GetUserToolButtons; override;
    procedure SaveUserToolButtons; override;
    procedure WndProc(var msg: TMessage); override;
  public
    property ActMachArr: TFullMachineList read GetActMachArr;
    property ViewFromDB: Boolean read mViewFromDB;
    property InsMachFormChanged: Boolean read fInsMachFormChanged write fInsMachFormChanged;
    procedure GetFromDB;
  published
    property Query: TmmADODataSet read fQuery write fQuery;
    property Query1: TmmADODataSet read fQuery1 write fQuery1;
    property SettingsHandler: TGetSettingsHandler read fSettingsHandler write fSettingsHandler;
    property SetMaConfigHandler: TSetMaConfigHandler read fSetMaConfigHandler write fSetMaConfigHandler;
    property YMSettingsUtils: TYMMachineSettings read fYMSettingsUtils write fYMSettingsUtils;
  end;

  TMaConfigReaderList = class(TPersistent)
  private
    mMachineDOMList: TmmList;
    function GetCount: Integer;
    function GetItems(Index: Integer): TMaConfigReader;
  public
    constructor Create;
    destructor Destroy; override;
    procedure LoadMaConfigFromDB; overload;
    property Count: Integer read GetCount;
    property Items[Index: Integer]: TMaConfigReader read GetItems;
  end;

var
  MaConfigMain: TMaConfigMain;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS, MMMessages, IPCClass,
  PrintMaConfigForm, PrintSettingsTemplateForm;

{$R *.DFM}
//------------------------------------------------------------------------------

{ TMaConfigMain }
//------------------------------------------------------------------------------
function TMaConfigMain.StartApplication(aFileName: string; aMode: TFileOpenMode; aParam: string): THandle;
var
  xStr: string;
begin
  Result := ShellExecute(0, PChar(cFileOpenMode[aMode]), PChar(aFileName), PChar(aParam), '', SW_SHOWNORMAL);
  if Result <= 32 then begin
    case Result of
      0: xStr := 'The operating system is out of memory or resources.';
      ERROR_FILE_NOT_FOUND: xStr := 'The specified file was not found.';
      ERROR_PATH_NOT_FOUND: xStr := 'The specified path was not found.';
      ERROR_BAD_FORMAT: xStr := 'The .EXE file is invalid (non-Win32 .EXE or error in .EXE image).';
      SE_ERR_ACCESSDENIED: xStr := 'The operating system denied access to the specified file.';
      SE_ERR_ASSOCINCOMPLETE: xStr := 'The filename association is incomplete or invalid.';
      SE_ERR_DDEBUSY: xStr := 'The DDE transaction could not be completed because other DDE transactions were being processed.';
      SE_ERR_DDEFAIL: xStr := 'The DDE transaction failed.';
      SE_ERR_DDETIMEOUT: xStr := 'The DDE transaction could not be completed because the request timed out.';
      SE_ERR_DLLNOTFOUND: xStr := 'The specified dynamic-link library was not found.';
      SE_ERR_NOASSOC: xStr := 'There is no application associated with the given filename extension.';
      SE_ERR_OOM: xStr := 'There was not enough memory to complete the operation.';
      SE_ERR_SHARE: xStr := 'A sharing violation occurred.';
    else
      xStr := 'Undefined error';
    end;
    ShowMessage(Format('ShellExecute error for "%s": %d = %s', [aFileName, Result, xStr]));
  end;
end;
//------------------------------------------------------------------------------

function TMaConfigMain.FillSelLineToFullMachine(aValidity: TMachValidity): TFullMachine;
begin
//wss: Result Wert kann undefiniert sein!!
  with mmSGMachNode do begin
    if (Row >= FixedRows) and (Row < RowCount)  then begin
//        and (GetActMachArr.Count>0) then begin    //Nue:3.3.03
      if ((GetActMachArr.Objects[Row - 1] as TFullMachine).Validity = mvFull) and (aValidity = mvNone) then begin
        (GetActMachArr.Objects[Row - 1] as TFullMachine).MachineInWork := True;
        Result := (GetActMachArr.Objects[Row - 1] as TFullMachine);
      end
      else begin
        mFullMachine.Assign(GetActMachArr.Objects[Row - 1] as TFullMachine);
        if aValidity <> mvNone then
          mFullMachine.Validity := aValidity;
        Result := mFullMachine;
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------

function TMaConfigMain.GetActMachArr: TFullMachineList;
begin
  if mViewFromDB then
    Result := mMachineDBArr
  else
    Result := mMachineMaArr;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.AddSingleSGLine(Index: Integer);
begin
  if (mmSGMachNode.RowCount <= (Index + mmSGMachNode.FixedRows)) then
    mmSGMachNode.InsertRows(mmSGMachNode.RowCount, 1);
end;

//------------------------------------------------------------------------------

procedure TMaConfigMain.WriteDBInfoToCells(aRow: Integer; aStringGrid: TmmExtStringGrid; aArr: TFullMachine;
  aGroupSpecRec: TGroupSpecificMachConfigRec);
var
  xRow: Integer;
  xSlip: Real;
begin
  xRow := aRow + mmSGMachNode.FixedRows;
//  with aQuery do begin
//  aStringGrid.Options
  with aArr do begin
    aStringGrid.Cells[1, xRow] := NetNode.NetName;
    aStringGrid.Cells[2, xRow] := NetNode.NodeId;
    if LastConfirmedUpload < cDateTime1970 then //Check auf 0.0 geht nicht, da (wahrscheinlich) ADO aus 0.0->2 macht!!!! Bug!! Nue:29.1.03
      aStringGrid.Cells[3, xRow] := '?????'
    else
      aStringGrid.Cells[3, xRow] := DateTimeToStr(LastConfirmedUpload);
    aStringGrid.Cells[19, xRow] := NetNode.AdaptOrName;
    aStringGrid.Cells[26, xRow] := NetNode.FrontManuf;
//    if (Validity in [mvNodeOnly]) then
//      aStringGrid.Cells[27,xRow] := NetNode.FrontModel
//    else
{ TODO -oNue -cXMLSettings : Ersetzten? call von XMLSettings }
//    aStringGrid.Cells[27, xRow] := cFrontTypeNames[MachineYMConfigRec.FrontType];

//Nue 8.3.01    if ((mMachineDBArr.Objects[aRow] as TFullMachine).Validity in [mvFull, mvMachineOnly]) then begin
    aStringGrid.Cells[4, xRow] := IntToStr(MachineID);
    aStringGrid.Cells[5, xRow] := MachineName;

    aStringGrid.Cells[6, xRow] := MachineModel;
    aStringGrid.Cells[20, xRow] := cAWEMachTypNames[MachineYMConfigRec.aWEMachType];
    aStringGrid.Cells[21, xRow] := DateToStr(Starting);
    aStringGrid.Cells[7, xRow] := IntToStr(aGroupSpecRec.spindle.start);
    aStringGrid.Cells[8, xRow] := IntToStr(aGroupSpecRec.spindle.stop);
    aStringGrid.Cells[16, xRow] := IntToStr(SpindlePos);

    aStringGrid.Cells[17, xRow] := cNoYesStrArr[FixSpdRanges];
//    if FixSpdRanges then
//      aStringGrid.Cells[17, xRow] := cTrue
//    else
//      aStringGrid.Cells[17, xRow] := cFalse;

    xSlip := Slip / 1000.0;
    if (xSlip < cMinSlip) or (xSlip > cMaxSlip) then
      aStringGrid.Cells[18, xRow] := ''
    else
      aStringGrid.Cells[18, xRow] := Format('%1.3f', [xSlip]);

    aStringGrid.Cells[22, xRow] := cNoYesStrArr[(DataCollection >= 1)];
//    if DataCollection >= 1 then
//      aStringGrid.Cells[22, xRow] := cTrue
//    else
//      aStringGrid.Cells[22, xRow] := cFalse;

    aStringGrid.Cells[23, xRow] := cNoYesStrArr[OnlineSpeed];
//    if OnlineSpeed then
//      aStringGrid.Cells[23, xRow] := cTrue
//    else
//      aStringGrid.Cells[23, xRow] := cFalse;

    aStringGrid.Cells[24, xRow] := IntToStr(MachineYMConfigRec.defaultSpeed);
    aStringGrid.Cells[25, xRow] := IntToStr(MachineYMConfigRec.defaultSpeedRamp);

    aStringGrid.Cells[11, xRow] := YMVersion;
    aStringGrid.Cells[12, xRow] := cFrontSwOption[YMOption];
    aStringGrid.Cells[10, xRow] := cAWETypeNames[aGroupSpecRec.aWEType];
{ TODO -oNue -cXMLSettings : Ersetzten? call von XMLSettings }
//    aStringGrid.Cells[9, xRow] := cSensingHeadTypeNames[aGroupSpecRec.sensingHead];
    aStringGrid.Cells[13, xRow] := IntToStr(MachineConfigA);
    aStringGrid.Cells[14, xRow] := IntToStr(MachineConfigB);
    aStringGrid.Cells[15, xRow] := IntToStr(MachineConfigC);
    aStringGrid.Cells[28, xRow] := IntToStr(aGroupSpecRec.configA);
    aStringGrid.Cells[29, xRow] := IntToStr(aGroupSpecRec.configB);
    aStringGrid.Cells[30, xRow] := IntToStr(aGroupSpecRec.configC);
    aStringGrid.Cells[31, xRow] := IntToStr(MachineYMConfigRec.inoperativePara[0]);
    aStringGrid.Cells[32, xRow] := IntToStr(MachineYMConfigRec.inoperativePara[1]);
    aStringGrid.Cells[33, xRow] := IntToStr(aGroupSpecRec.inoperativePara[0]);
    aStringGrid.Cells[34, xRow] := IntToStr(aGroupSpecRec.inoperativePara[1]);
    aStringGrid.Cells[35, xRow] := IntToStr(MachineYMConfigRec.checkLen);
//Nue:21.8.01      aStringGrid.Cells[35,xRow] := IntToStr(MachineYMConfigRec.cutRetries);
    aStringGrid.Cells[36, xRow] := IntToStr(MachineYMConfigRec.longStopDef);
    aStringGrid.Cells[cOrgGridIndexCol, xRow] := IntToStr(xRow);
  end;
end;

//------------------------------------------------------------------------------
//
//procedure TMaConfigMain.GetFromMachines;
//var
//  xCount: Word;
//begin
//  mViewFromDB := False;
//  labLoadInfo.Font.Color := clGreen;
//  labLoadInfo.Caption := cLoadFromMach;
//  //Clear stringGrid
////  mmSGMachNode.ClearNormalCells;
//  fGridNewSorted := False;
//  mmSGMachNode.Clear;
//  mmSGMachNode.ShowColumnHeaders;
//  mmSGMachNode.RowCount := cDefRowCount;
//
//  mMachineMaArr.Clear;
//
//  xCount := 0;
//  with Query do
//  try
//    Close;
//    CommandText := cQryGetMachines;
//    Open;
//    while (not EOF) do begin
////        acMachineData.Enabled := False;
//      mMachineDataFlag := False;
//      Query1.Close;
//      Query1.CommandText := cQrySelNodeState;
//      Query1.Parameters.ParamByName('c_machine_id').value := FieldByName('c_machine_id').AsInteger;
//      Query1.Open;
//      if TTxnNodeState(Query1.FieldByName('c_node_stat').AsInteger) in mNodeStates then
//        with TGetSettingsHandler.Create(self) do
//        try
//          mFullMachine.Query     := fQuery2;
//          mFullMachine.Validity  := mvFull;
//          mFullMachine.MachineID := FieldByName('c_machine_id').AsInteger;
//          mFullMachine.NetNode.SetNode(FieldByName('c_node_id').ASString);
////              AssMachine := mMachine;
//          mMachineMaArr.InsertObject(xCount, mFullMachine.NetNode.NodeId + mFullMachine.MachineName, TFullMachine.Create(nil (*nil, weil von TComponent abgeleitet*)));
//          (mMachineMaArr.Objects[xCount] as TFullMachine).Assign(mFullMachine);
//          INC(xCount);
//        finally
//          free;
//        end; //try..finally
////Nue        mMachineMaArr.Sort;
//      Next;
//    end;
//    if (mMachineMaArr.Count) <> 0 then begin
////        SettingsHandler := TGetSettingsHandler.Create(Self);
//      mFullMachine.Assign(mMachineMaArr.Objects[0] as TFullMachine);
//      SettingsHandler.AssMachine := mFullMachine;
//      SettingsHandler.StartGetSettings(Self.Handle {mWindowHandle});
//    end;
//    mUploadReminderMsg := False;
//  except
//    on e: Exception do begin
//      SystemErrorMsg_('TMaConfigMain.GetFromMachines failed. ' + e.Message);
////        acMachineData.Enabled := True;
//      mMachineDataFlag := True;
//    end;
//  end;
//end;
//
//------------------------------------------------------------------------------
procedure TMaConfigMain.GetFromDB;
var
  xCount, xCount1, xBaseMach: Integer;
  xOldCursor: TCursor;
  xTmpMachConfig: TMachineYMConfigRec;
begin
  xOldCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  mViewFromDB := True;
  labLoadInfo.Font.Color := clGreen;
  labLoadInfo.Caption := cLoadFromDB;
  //Clear stringGrid
//  mmSGMachNode.ClearNormalCells;
  fGridNewSorted := False;
  mmSGMachNode.Clear;
  mmSGMachNode.ShowColumnHeaders;
  mmSGMachNode.ScrollBars := ssNone;
  mmSGMachNode.RowCount := cDefRowCount;

  mMachineDBArr.Clear;

  xCount := 0;
//wss  xBaseMach := xCount;
  acDBData.Enabled := False;
  with Query do
  try
    Close;
    CommandText := cQryGetMachineNode;
    Open;

    if EOF then begin //Einf�llen einer Dummymaschine (sonst Indexverletzungen)
      mFullMachine.Query := fQuery2;
      mFullMachine.Validity := mvFull;
      xTmpMachConfig := mFullMachine.MachineYMConfigRec;
      mFullMachine.ActiveSpdGrp := 1;
      xTmpMachConfig.spec[mFullMachine.ActiveSpdGrp].spindle.start := 1;
      xTmpMachConfig.spec[mFullMachine.ActiveSpdGrp].spindle.stop := cMaxSpindeln;
      mFullMachine.MachineYMConfigRec := xTmpMachConfig;
      mMachineDBArr.InsertObject(xCount, 'NoMachine', TFullMachine.Create(nil (*nil, weil von TComponent abgeleitet*)));
      (mMachineDBArr.Objects[xCount] as TFullMachine).Assign(mFullMachine);
      mDeleteFlag := False;
//      acDelete.Enabled := False;
      mChangeFlag := False;
//      acChange.Enabled := False;
    end
    else begin
      mDeleteFlag := True;
//      acDelete.Enabled := True;
      mChangeFlag := True;
//      acChange.Enabled := True;

      if mMaConfigReaderList=NIL then begin
        //Initialisieren mMaConfigReaderList
        mMaConfigReaderList := TMaConfigReaderList.Create;
      end; //if

      //Neu laden der Konfiguration ab DB
      mMaConfigReaderList.LoadMachConfigFromDB

      for i := 0 to (mMaConfigReaderList.Count-1) do begin

      end; //for




      while (not EOF) do begin
        with TGetSettingsHandler.Create(self) do
        try
          mFullMachine.Query        := fQuery2;
          mFullMachine.Validity     := mvFull;
          mFullMachine.MachineID    := FieldByName('c_machine_id').AsInteger;
          mFullMachine.ActiveSpdGrp := 1;
          mFullMachine.NetNode.SetNode(FieldByName('c_node_id').ASString);
  //          AssMachine := mMachine;

          mMachineDBArr.InsertObject(xCount, mFullMachine.MachineName, TFullMachine.Create(nil (*nil, weil von TComponent abgeleitet*)));
          (mMachineDBArr.Objects[xCount] as TFullMachine).Assign(mFullMachine);

          AddSingleSGLine(xCount);
          WriteDBInfoToCells(xCount, mmSGMachNode, (mMachineDBArr.Objects[xCount] as TFullMachine),
            (mMachineDBArr.Objects[xCount] as TFullMachine).MachineYMConfigRec.spec[1]);
          xBaseMach := xCount;
          INC(xCount);

          //Check SubMachines
          xCount1 := 1;
          while ((mMachineDBArr.Objects[xBaseMach] as TFullMachine).MachineYMConfigRec.spec[xCount1 + 1].spindle.start <> 0) do begin
            mMachineDBArr.InsertObject(xCount, mFullMachine.MachineName, TFullMachine.Create(nil (*nil, weil von TComponent abgeleitet*)));
            mFullMachine.ActiveSpdGrp := xCount1 + 1;
            (mMachineDBArr.Objects[xCount] as TFullMachine).Assign(mFullMachine);

            INC(xCount1);
            AddSingleSGLine(xCount);
            WriteDBInfoToCells(xCount, mmSGMachNode, (mMachineDBArr.Objects[xCount] as TFullMachine),
              (mMachineDBArr.Objects[xBaseMach] as TFullMachine).MachineYMConfigRec.spec[xCount1]);
            INC(xCount);
          end;

        finally
          free;
        end; //try..finally
        Next;
      end; // while
    end; //else
//    GetMachinesNotAssigned(xCount);
//    GetNodesNotAssigned(xCount);
  except
    on e: Exception do begin
      SystemErrorMsg_('TMaConfigMain.GetFromDB failed. ' + e.Message);
      Screen.Cursor := xOldCursor;
    end;
  end; // with Query
  acDBData.Enabled := True;
  mmSGMachNode.ScrollBars := ssBoth;
//  SetScrollPos(mmSGMachNode.Handle,SB_HORZ,0,True);
  mmSGMachNode.LeftCol := 1;
  Screen.Cursor := xOldCursor;
end;

//--------------------------------------------------------------------------
procedure TMaConfigMain.FormCreate(Sender: TObject);
var
  i: Integer;
  xComp: TComponent;
begin
  HelpContext := GetHelpContext('Machinenkonfiguration\MACONF_Maschinen_Konfiguration.htm');
  acNewMach.HelpContext := GetHelpContext('Machinenkonfiguration\MACONF_Neue_Maschine_einfuegen.htm');
  acDelete.HelpContext := GetHelpContext('Machinenkonfiguration\MACONF_Maschinendaten_loeschen.htm');
  //Wird f�r setzen des Maschinenstatus auf Online gebraucht  Nue:10.12.02
  try
    mDBAccess := TADODBAccess.Create(1);
    mDBAccess.Init;
    mMachInserted := False;
  except
    on e: Exception do begin
      SystemErrorMsg_('TMaConfigMain.FormCreate: TADODBAccess.Create failed. ' + e.Message);
    end;
  end;

  // cleanup unused components: TActions and TMenuItems
  for i := 1 to High(cUnusedComponents) do begin
    xComp := FindComponent(cUnusedComponents[i]);
    if Assigned(xComp) then
      FreeAndNil(xComp);
  end;

  Init;
end;

//------------------------------------------------------------------------------
procedure TMaConfigMain.Init;
begin
  // Registrierung der Windows Meldung
  mAssWindowMsg      := RegisterWindowMessage(ASSIGN_REG_STR);
  mMMClientMsgID     := RegisterWindowMessage(cMsgAppl);
  mMachineDataFlag   := True;
  mUploadReminderMsg := False;

//  mWindowHandle    := AllocateHWnd( WndProc );

  mNodeStates        := [nsOnline];
  InsMachFormChanged := False;

  mFullMachine        := TFullMachine.Create(nil);
  mMachineMaArr       := TFullMachineList.Create;
  mMachineDBArr       := TFullMachineList.Create;
  fSetMaConfigHandler := TSetMaConfigHandler.Create(nil);
  fYMSettingsUtils    := TYMMachineSettings.Create;
  fSettingsHandler    := TGetSettingsHandler.Create(Self);
  fSettingsHandler.ShowWait := False; // @@Nue  Mit True gibts Absturz!!!!!!!!!
  GetFromDB;

  mmSGMachNode.HideColumn(cOrgGridIndexCol);

//  if mmSGMachNode.RowCount >= (mmSGMachNode.FixedRows) then
//    mmSGMachNodeClickCell(Self, mmSGMachNode.FixedRows, 1);

  // nun wird noch MMGuard �ber MM Status angefragt, die Funktion
  // FromMachine ist vom laufenden MMSystem abh�ngig!
  // Status, ob MillMaster verf�gbar ist, wird in der globalen Variable
  // gMMAvailable in der WndProc Funktion gesetzt
  SendGuardCommand(gcGetState);
//  Printers.Printer.Orientation := poLandscape;   //Nue:17.3.03
end;

//--------------------------------------------------------------------------
procedure TMaConfigMain.FormDestroy(Sender: TObject);
begin
  FreeAndNil(fSetMaConfigHandler);
  FreeAndNil(fYMSettingsUtils);

  FreeAndNil(mMachineMaArr);
  FreeAndNil(mMachineDBArr);
  FreeAndNil(mFullMachine);

  mDBAccess.Free; //Nue:10.12.02

  inherited;
end;

//--------------------------------------------------------------------------
procedure TMaConfigMain.WndProc(var msg: TMessage);
  //local
  //........................................................
  procedure GetSettings(aStr: string);
  var
    xCount, xCount2, xCount3, xCount4, xNrOfMach, xGrpIndex, xMachId: Integer;
    xSettingsArr: TSettingsArr;
//    xSettingsArr: PXMLSettingsCollection;
    xStr: string;
  begin
    for xCount := 0 to (mMachineMaArr.Count - 1) do begin
      if (msg.LParam = (mMachineMaArr.Objects[xCount] as TFullMachine).MachineID) then begin
        mmStatusBar.SimpleText := Format(aStr, [(mMachineMaArr.Objects[xCount] as TFullMachine).MachineName]);
        //Mit Settings von der Maschine ueberschreiben
        if Assigned(SettingsHandler.XMLSettingsCollection) then begin
//wss was wird hier in dieser lokalen Procedure genau mit dem SettingsArr gemacht?
          xSettingsArr := SettingsHandler.XMLSettingsCollection^.SettingsArr;
          FillChar(SettingsHandler.XMLSettingsCollection^.SettingsArr, sizeof(TSettingsArr), 0);
//          xSettingsArr := SettingsHandler.SettingsArr^;
//          FillChar(SettingsHandler.SettingsArr^, sizeof(TSettingsArr), 0);

          //Nue: Block noetig, weil GetMaConfig noch nicht sauber in aelteren Versionen von AC338 implementiert  21.3.01
//          (mMachineMaArr.Objects[xCount] as TFullMachine).MaConfigAvailable := YMSettingsUtils.YMMachineConfigAvailable(SettingsHandler.SettingsArr^);
          //wss Verwendung xGrpIndex einmal 1-basiert, anderesmal 0-basiert -> angleichen?
          for xGrpIndex:=1 to cZESpdGroupLimit do begin
            (mMachineMaArr.Objects[xCount] as TFullMachine).SetGroupState(xGrpIndex, xSettingsArr[xGrpIndex-1].GroupState);
            if ((mMachineMaArr.Objects[xCount] as TFullMachine).GetGroupState(xGrpIndex) in [gsFree, gsInProd]) then begin
              SettingsHandler.XMLSettingsCollection^.SettingsArr[xGrpIndex-1] := xSettingsArr[xGrpIndex-1];
            end;
          end;
//          (mMachineMaArr.Objects[xCount] as TFullMachine).MachineYMConfigRec := YMSettingsUtils.ExtractYMMachineConfig(SettingsHandler.SettingsArr^);

          //Setzen des Maschinenstatus auf Online   Nue:10.12.02
          with mDBAccess.Query[0] do
          try
            SQL.Text := cQrySetMachineState;
            ParamByName('c_machine_id').AsInteger := msg.LParam; //MachineID
            ExecSQL;
          except
            on e: Exception do begin
              SystemErrorMsg_(Format('TMaConfigMain.WndProc.GetSettings: Setting c_machine_state for MachID: %d to ONLINE failed. ', [msg.LParam]) + e.Message);
            end;
          end; //with

        end else begin
          (mMachineMaArr.Objects[xCount] as TFullMachine).UploadTimeout := True;
        end;

        AddSingleSGLine(xCount);
        (mMachineMaArr.Objects[xCount] as TFullMachine).ActiveSpdGrp := 1; //Nue:21.8.01
        WriteDBInfoToCells(xCount, mmSGMachNode, (mMachineMaArr.Objects[xCount] as TFullMachine),
          (mMachineMaArr.Objects[xCount] as TFullMachine).MachineYMConfigRec.spec[1]);

        if (xCount < (mMachineMaArr.Count - 1)) then begin
          mFullMachine.Assign(mMachineMaArr.Objects[xCount + 1] as TFullMachine);
          SettingsHandler.AssMachine := mFullMachine;
          SettingsHandler.StartGetSettings(Self.Handle {mWindowHandle});
        end else begin
          xNrOfMach := (mMachineMaArr.Count - 1);

//Alt          for xCount2:= 0 to xNrOfMach do begin
          for xCount2 := xNrOfMach downto 0 do begin
            //Check SubMachines
            xGrpIndex := 2;

            while (xGrpIndex <= cZESpdGroupLimit) do begin

              if ((mMachineMaArr.Objects[xCount2] as TFullMachine).MachineYMConfigRec.spec[xGrpIndex].spindle.contents <> cSpdlRngNotDefined) and
                ((mMachineMaArr.Objects[xCount2] as TFullMachine).MachineYMConfigRec.spec[xGrpIndex].spindle.contents <> 0) then begin //Nue: tmp bis GetMaConfig sauber in AC338 implementiert
                mMachineMaArr.InsertObject(mMachineMaArr.Count,
                  (mMachineMaArr.Objects[xCount2] as TFullMachine).NetNode.NodeId + (mMachineMaArr.Objects[xCount2] as TFullMachine).MachineName,
                  TFullMachine.Create(Nil));
                (mMachineMaArr.Objects[mMachineMaArr.Count - 1] as TFullMachine).Assign(mMachineMaArr.Objects[xCount2] as TFullMachine);
                (mMachineMaArr.Objects[mMachineMaArr.Count - 1] as TFullMachine).ActiveSpdGrp := xGrpIndex;
                AddSingleSGLine(mMachineMaArr.Count);
                WriteDBInfoToCells(mMachineMaArr.Count - 1, mmSGMachNode, (mMachineMaArr.Objects[mMachineMaArr.Count - 1] as TFullMachine),
                  (mMachineMaArr.Objects[xCount2] as TFullMachine).MachineYMConfigRec.spec[xGrpIndex]);
              end;
              INC(xGrpIndex);
            end;

            //Nue: Block noetig, weil GetMaConfig noch nicht sauber in aelteren Versionen von AC338 implementiert  21.3.01
            if not ((mMachineMaArr.Objects[xCount2] as TFullMachine).MaConfigAvailable) and
               not ((mMachineMaArr.Objects[xCount2] as TFullMachine).AtLeastOneGroupInProd) then begin
{ TODO -oNue -cXMLSettings : Ersetzten? call von XMLSettings }
//              if ((mMachineMaArr.Objects[xCount2] as TFullMachine).FrontType = ftSWSInformatorSBC5) then
//                xStr := cUploadProblem + cNoGroupInProd
//              else
//                xStr := cUploadProblem;

              MMMessageDlg(Format(xStr, [(mMachineMaArr.Objects[xCount2] as TFullMachine).MachineName]), mtWarning, [mbOk], 0);

              xMachId := (mMachineMaArr.Objects[xCount2] as TFullMachine).MachineID;
              xCount4 := mMachineMaArr.Count - 1;
              for xCount3 := xCount4 downto 0 do begin
                if (mMachineMaArr.Objects[xCount3] as TFullMachine).MachineID = xMachId then begin
                  if (xCount2 = 0) and (xCount3 = 0) then
                    mmSGMachNode.ClearRows(mmSGMachNode.FixedRows + xCount3, 1)
                  else
                    mmSGMachNode.RemoveRows(mmSGMachNode.FixedRows + xCount3, 1);
                  mMachineMaArr.Delete(xCount3);
                end;
              end;
            end;

          end;

          //Neu sortieren und neu auf Grid
          mMachineMaArr.Sort;

          mmSGMachNode.Visible := False;
          mmSGMachNode.Clear;
          mmSGMachNode.ShowColumnHeaders;
          for xCount3 := 0 to (mMachineMaArr.Count - 1) do begin
            WriteDBInfoToCells(xCount3, mmSGMachNode, (mMachineMaArr.Objects[xCount3] as TFullMachine),
              (mMachineMaArr.Objects[xCount3] as TFullMachine).MachineYMConfigRec.spec[(mMachineMaArr.Objects[xCount3] as TFullMachine).ActiveSpdGrp]);
          end;
          mmSGMachNode.Visible := True;

          Screen.Cursor := crDefault;
//          acMachineData.Enabled := True;
          mMachineDataFlag := True;

        end;
        Break;
      end;
    end;
  end;

//  procedure GetSettings(aStr: string);
//  var xCount, xCount2, xCount3, xCount4, xNrOfMach, xGrpIndex, xMachId: Integer;
//    xSettingsArr: TSettingsArr;
//    xStr: string;
//  begin
//    for xCount := 0 to (mMachineMaArr.Count - 1) do begin
//      if (msg.LParam = (mMachineMaArr.Objects[xCount] as TFullMachine).MachineID) then begin
//        mmStatusBar.SimpleText := Format(aStr, [(mMachineMaArr.Objects[xCount] as TFullMachine).MachineName]);
//        //Mit Settings von der Maschine ueberschreiben
//        if Assigned(SettingsHandler.SettingsArr) then begin
//          xSettingsArr := SettingsHandler.SettingsArr^;
//          FillChar(SettingsHandler.SettingsArr^, sizeof(TSettingsArr), 0);
//
//          //Nue: Block noetig, weil GetMaConfig noch nicht sauber in aelteren Versionen von AC338 implementiert  21.3.01
//          (mMachineMaArr.Objects[xCount] as TFullMachine).MaConfigAvailable := YMSettingsUtils.YMMachineConfigAvailable(SettingsHandler.SettingsArr^);
//          for xGrpIndex := 1 to cZESpdGroupLimit do begin
//            (mMachineMaArr.Objects[xCount] as TFullMachine).SetGroupState(xGrpIndex, xSettingsArr[xGrpIndex - 1].GroupState);
//            if ((mMachineMaArr.Objects[xCount] as TFullMachine).GetGroupState(xGrpIndex) in [gsfree, gsInProd]) then begin
//              SettingsHandler.SettingsArr^[xGrpIndex - 1] := xSettingsArr[xGrpIndex - 1];
//            end;
//          end;
//          (mMachineMaArr.Objects[xCount] as TFullMachine).MachineYMConfigRec := YMSettingsUtils.ExtractYMMachineConfig(SettingsHandler.SettingsArr^);
//
//          //Setzen des Maschinenstatus auf Online   Nue:10.12.02
//          with mDBAccess.Query[0] do
//          try
//            SQL.Text := cQrySetMachineState;
//            ParamByName('c_machine_id').AsInteger := msg.LParam; //MachineID
//            ExecSQL;
//          except
//            on e: Exception do begin
//              SystemErrorMsg_(Format('TMaConfigMain.WndProc.GetSettings: Setting c_machine_state for MachID: %d to ONLINE failed. ', [msg.LParam]) + e.Message);
//            end;
//          end; //with
//
//        end else begin
//          (mMachineMaArr.Objects[xCount] as TFullMachine).UploadTimeout := True;
//        end;
//
//        AddSingleSGLine(xCount);
//        (mMachineMaArr.Objects[xCount] as TFullMachine).ActiveSpdGrp := 1; //Nue:21.8.01
//        WriteDBInfoToCells(xCount, mmSGMachNode, (mMachineMaArr.Objects[xCount] as TFullMachine),
//          (mMachineMaArr.Objects[xCount] as TFullMachine).MachineYMConfigRec.spec[1]);
//
//        if (xCount < (mMachineMaArr.Count - 1)) then begin
//          mFullMachine.Assign(mMachineMaArr.Objects[xCount + 1] as TFullMachine);
//          SettingsHandler.AssMachine := mFullMachine;
//          SettingsHandler.StartGetSettings(Self.Handle {mWindowHandle});
//        end else begin
//          xNrOfMach := (mMachineMaArr.Count - 1);
//
////Alt          for xCount2:= 0 to xNrOfMach do begin
//          for xCount2 := xNrOfMach downto 0 do begin
//            //Check SubMachines
//            xGrpIndex := 2;
//
//            while (xGrpIndex <= cZESpdGroupLimit) do begin
//
//              if ((mMachineMaArr.Objects[xCount2] as TFullMachine).MachineYMConfigRec.spec[xGrpIndex].spindle.contents <> cSpdlRngNotDefined) and
//                ((mMachineMaArr.Objects[xCount2] as TFullMachine).MachineYMConfigRec.spec[xGrpIndex].spindle.contents <> 0) then begin //Nue: tmp bis GetMaConfig sauber in AC338 implementiert
//                mMachineMaArr.InsertObject(mMachineMaArr.Count,
//                  (mMachineMaArr.Objects[xCount2] as TFullMachine).NetNode.NodeId + (mMachineMaArr.Objects[xCount2] as TFullMachine).MachineName,
//                  TFullMachine.Create(Nil));
//                (mMachineMaArr.Objects[mMachineMaArr.Count - 1] as TFullMachine).Assign(mMachineMaArr.Objects[xCount2] as TFullMachine);
//                (mMachineMaArr.Objects[mMachineMaArr.Count - 1] as TFullMachine).ActiveSpdGrp := xGrpIndex;
//                AddSingleSGLine(mMachineMaArr.Count);
//                WriteDBInfoToCells(mMachineMaArr.Count - 1, mmSGMachNode, (mMachineMaArr.Objects[mMachineMaArr.Count - 1] as TFullMachine),
//                  (mMachineMaArr.Objects[xCount2] as TFullMachine).MachineYMConfigRec.spec[xGrpIndex]);
//              end;
//              INC(xGrpIndex);
//            end;
//
//            //Nue: Block noetig, weil GetMaConfig noch nicht sauber in aelteren Versionen von AC338 implementiert  21.3.01
//            if not ((mMachineMaArr.Objects[xCount2] as TFullMachine).MaConfigAvailable) and
//               not ((mMachineMaArr.Objects[xCount2] as TFullMachine).AtLeastOneGroupInProd) then begin
//              if ((mMachineMaArr.Objects[xCount2] as TFullMachine).FrontType = ftSWSInformatorSBC5) then
//                xStr := cUploadProblem + cNoGroupInProd
//              else
//                xStr := cUploadProblem;
//
//              MMMessageDlg(Format(xStr, [(mMachineMaArr.Objects[xCount2] as TFullMachine).MachineName]), mtWarning, [mbOk], 0);
//
//              xMachId := (mMachineMaArr.Objects[xCount2] as TFullMachine).MachineID;
//              xCount4 := mMachineMaArr.Count - 1;
//              for xCount3 := xCount4 downto 0 do begin
//                if (mMachineMaArr.Objects[xCount3] as TFullMachine).MachineID = xMachId then begin
//                  if (xCount2 = 0) and (xCount3 = 0) then
//                    mmSGMachNode.ClearRows(mmSGMachNode.FixedRows + xCount3, 1)
//                  else
//                    mmSGMachNode.RemoveRows(mmSGMachNode.FixedRows + xCount3, 1);
//                  mMachineMaArr.Delete(xCount3);
//                end;
//              end;
//            end;
//
//          end;
//
//          //Neu sortieren und neu auf Grid
//          mMachineMaArr.Sort;
//
//          mmSGMachNode.Visible := False;
//          mmSGMachNode.Clear;
//          mmSGMachNode.ShowColumnHeaders;
//          for xCount3 := 0 to (mMachineMaArr.Count - 1) do begin
//            WriteDBInfoToCells(xCount3, mmSGMachNode, (mMachineMaArr.Objects[xCount3] as TFullMachine),
//              (mMachineMaArr.Objects[xCount3] as TFullMachine).MachineYMConfigRec.spec[(mMachineMaArr.Objects[xCount3] as TFullMachine).ActiveSpdGrp]);
//          end;
//          mmSGMachNode.Visible := True;
//
//          Screen.Cursor := crDefault;
////          acMachineData.Enabled := True;
//          mMachineDataFlag := True;
//
//        end;
//        Break;
//      end;
//    end;
//  end;
  //.....................................................
begin
  try
    if msg.Msg = mAssWindowMsg then begin
      case msg.WParam of
        WM_GETSETTINGS_DONE: begin
            GetSettings(cMachineReadOk);
          end;
        WM_GETSETTINGS_TIMEOUT: begin
            GetSettings(cMachineReadNOk + '(TIMEOUT)');
          end;
        WM_GETSETTINGS_FAILED: begin
            GetSettings(cMachineReadNOk + '(FAILED)');
            UserErrorMsg(cSettingsNotOK);
          end;
      else
      end; // case
    end else if msg.Msg = mMMClientMsgID then begin
      if Msg.WParam in [cMMStarted, cMMStarting, cMMStopped, cMMStopping, cMMAlert, cMMError, cMMAquisation] then begin
        gMMAvailable := (Msg.WParam = cMMStarted) OR
                        ((Msg.WParam = cMMAquisation) and (Msg.LParam = 0));

//Nue:Um zu debuggen, wenn Basissystem mit dem Autolauncher gestartet ist, muss hier fix gMMAvailable := True; gesetzt werden!
///gMMAvailable := True; //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if gMMAvailable and mMachInserted and mUploadReminderMsg then begin
          SetEnabledState(True);
          Screen.Cursor      := crDefault;
          mUploadReminderMsg := False;
          ShowMessage(rsDoUpload);
          GetFromDB;
        end;
      end;
    end else
      inherited WndProc(msg);
  except
    on e: Exception do begin
      SystemErrorMsg_('TMaConfigMain.WndProc failed. ' + e.Message);
      Exit;
    end;
  end;
end;
//--------------------------------------------------------------------------

procedure TMaConfigMain.acMachineDataExecute(Sender: TObject);
begin
//  acDBData.Enabled := True;
//  acDelete.Enabled := False;
//  acChange.Enabled := False;
//  GetFromMachines;
end;

//--------------------------------------------------------------------------
procedure TMaConfigMain.acDBDataExecute(Sender: TObject);
begin
  mMachineDataFlag := True;
  SendGuardCommand(gcGetState);
  GetFromDB;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.mmSGMachNodeClickCell(Sender: TObject; Arow, Acol: Integer);
begin
  if {@Nue:9.4.01 (Acol<=mmSGMachNode.FixedCols) and }(Arow >= mmSGMachNode.FixedRows)
     and (GetActMachArr.Count > 0) then begin //Nue:24.1.02
    if ((GetActMachArr.Objects[Arow - mmSGMachNode.FixedRows] as TFullMachine).Validity = mvFull) then begin
      mChangeFlag := True;
//      acChange.Enabled := True;
      mDeleteFlag := True;
//      acDelete.Enabled := True;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.mmSGMachNodeDblClickCell(Sender: TObject; Arow, Acol: Integer);
var
  xRealCol: Integer;
begin
//  if {@Nue:9.4.01 (Acol<=mmSGMachNode.FixedCols) and }(Arow >= mmSGMachNode.FixedRows)
//     and (GetActMachArr.Count > 0) then begin //Nue:24.1.02
//    xRealCol := StrToInt(mmSGMachNode.Cells[cOrgGridIndexCol, Arow]) - mmSGMachNode.FixedRows;
//    if ((GetActMachArr.Objects[xRealCol] as TFullMachine).Validity in [mvFull]) then begin
//      (GetActMachArr.Objects[xRealCol] as TFullMachine).MachineInWork := True;
//      CreateInsMachNode(GetActMachArr.Objects[xRealCol] as TFullMachine);
//    end;
//  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acInsertExecute(Sender: TObject);
begin
  inherited;

end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acInsClearerExecute(Sender: TObject);
begin
  dwClearerType.Show;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acNewMachExecute(Sender: TObject);
begin
  //Nue Einfuellen neue Maschine
//  CreateInsMachNode(FillSelLineToFullMachine(mvNewMachine));
end;
//------------------------------------------------------------------------------
//procedure TMaConfigMain.acDownloadExecute(Sender: TObject);
//var
//  xMachineArr: TFullMachineList;
//begin
//  if mViewFromDB then
//    xMachineArr := mMachineDBArr
//  else
//    xMachineArr := mMachineMaArr;
////Kr   FillSack(xMachineArr[mmSGMachNode.GetRealRow-mmSGMachNode.FixedRows]
//  SetMaConfigHandler.Assign(xMachineArr.Objects[mmSGMachNode.GetRealRow - mmSGMachNode.FixedRows] as TFullMachine);
//end;
//------------------------------------------------------------------------------

procedure TMaConfigMain.mmSGMachNodeGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (ACol = 0) and fGridNewSorted then
    ABrush.Color := clBlack
  else if (ACol = 0) and (ARow > 0)  then begin
    ABrush.Color := labOffline.Color;
    if (GetActMachArr.Count >= ARow) and (TFullMachine(GetActMachArr.Objects[ARow - mmSGMachNode.FixedRows]).Validity = mvFull) then begin
      if not TFullMachine(GetActMachArr.Objects[ARow - mmSGMachNode.FixedRows]).UploadTimeout and
         TFullMachine(GetActMachArr.Objects[ARow - mmSGMachNode.FixedRows]).IsOnline then begin
        if TFullMachine(GetActMachArr.Objects[ARow - mmSGMachNode.FixedRows]).IsInProduction then
          ABrush.Color := labInProd.Color
        else
          ABrush.Color := labOnline.Color;
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TMaConfigMain.acChangeExecute(Sender: TObject);
begin
  if (GetActMachArr.Count > 0) then //Nue:24.1.02
//    CreateInsMachNode(FillSelLineToFullMachine(mvFull));
end;
//------------------------------------------------------------------------------

procedure TMaConfigMain.CreateInsMachNode(aFullMachine: TFullMachine);
begin
  with TdwInsMachNode.Create(Self) do
  try
    MyShow(aFullMachine);
    if ShowModal = c_mrMachInserted then begin
      // beim Schliessend vom Dialog wird ein Restart zum System geschickt.
      mMachInserted      := True;
      mUploadReminderMsg := gMMAvailable;
      // in der Zwischenzeit den Wartemauszeiger anzeigen lassen, bis Anfrage
      // �ber MMClient per Windowsmessage best�tigt wird -> WndProc
      if gMMAvailable then begin
        //TODO: Eventuell noch ein Timer starten, wenn was schief geht mit der MMStatus Meldung
        Screen.Cursor   := crHourGlass;
        SetEnabledState(False);
      end;
    end;
  finally
    Free;
  end;

  mmSGMachNode.Col := 1;
  if InsMachFormChanged then
    if mViewFromDB then begin
      InsMachFormChanged := False;
      GetFromDB;
    end else begin
      labLoadInfo.Font.Color := clRed;
      labLoadInfo.Caption    := cLoadFromMach + ' ' + cLoadNewData;
    end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acInsSensHeadExecute(Sender: TObject);
begin
  dwHeadType.Show;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acInsNetTypenExecute(Sender: TObject);
begin
  dwNetType.Show;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acInsMachTypeExecute(Sender: TObject);
begin
  dwMachineType.Show;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acInsFrontTypeExecute(Sender: TObject);
begin
  dwFrontType.Show;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acSecurityConfigExecute(Sender: TObject);
begin
  MMSecurityControl.Configure;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.QryDelMachine;
begin
  with cmdADO do begin
    CommandText := cQryDelMachine; //Spindle members of the machine will be deleted by trigger
    Parameters.ParamByName('c_machine_id').value := (GetActMachArr.Objects[mmSGMachNode.Row - 1] as TFullMachine).MachineID;
    Execute;
  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.QryDelNode;
begin
  with cmdADO do begin
    CommandText := cQryDelNode;
    Parameters.ParamByName('c_node_id').value := (GetActMachArr.Objects[mmSGMachNode.Row - 1] as TFullMachine).NetNode.NodeId;
    Execute;
  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acDeleteExecute(Sender: TObject);
begin
  // Wenn Maschine in Produktion ist, dann erst nachfragen, ob diese wirklich gel�scht werden soll
  with mmSGMachNode do begin
    if (GetActMachArr.Objects[Row-1] as TFullMachine).IsInProduction then begin
      if MMMessageDlg(cMaInProduction, mtWarning, [mbOk,mbIgnore], 0) = mrIgnore then begin
        // wenn IGNORIEREN, dann auf der Datenbank den Status knallhart auf STOPPED setzen, ohne
        // die Daten zu holen!!!
        with TmmAdoCommand.Create(nil) do
        try
          ConnectionString := GetDefaultConnectionString;
          try
            CommandText := cQryStopProdGrpForced;
            Parameters.ParamByName ( 'c_prod_end' ).DataType := ftDateTime;
            Parameters.ParamByName ( 'c_prod_end' ).value := Now;
            Parameters.ParamByName ( 'c_machine_id' ).value := (GetActMachArr.Objects[Row-1] as TFullMachine).MachineID;
            Execute;
          except
            on e:Exception do begin
              SystemErrorMsg_ ( ' TMaConfigMain.acDeleteExecute1 cQryStopProdGrpForced failed. ' + e.Message );
              Exit;
            end;// on e:Exception do begin
          end;// try except
        finally
          free;
        end;// with TmmAdoCommand.Create(nil) do try
      end // if (xState = mrIgnore) then begin
      else //mrOk
        Exit;
    end;// if (GetActMachArr.Objects[Row-1] as TFullMachine).IsInProduction then begin
  end; //with

//  if MMMessageDlg(Format(cAskForDelete, [(GetActMachArr.Objects[mmSGMachNode.Row - 1] as TFullMachine).MachineName]), mtConfirmation, [mbYes, mbNo], acDelete.HelpContext) = mrYes then begin
  if MMMessageDlg(Format(cAskForDelete, [(GetActMachArr.Objects[mmSGMachNode.Row - 1] as TFullMachine).MachineName]), mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
    with (GetActMachArr.Objects[mmSGMachNode.Row - 1] as TFullMachine).Query do
    try
      Connection.BeginTrans;
      QryDelMachine; //Spindle members of the machine will be deleted by trigger
      QryDelNode;
      Connection.CommitTrans;
      MMMessageDlg(cSuccDeleted, mtInformation, [mbOk], 0);
    except
      on e: Exception do begin
        Connection.RollbackTrans;
        SystemErrorMsg_(' TdwInsMachNode.acDeleteFromDBExecute2 cQryMachine/cQryDelNode failed. ' + e.Message);
      end;
    end; //with

    GetFromDB;
  end; // if
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.miTexnetTesterClick(Sender: TObject);
var
  xStr: string;
begin
  xStr := GetRegString(cRegLM, cRegMMToolsPath, 'TexnetTesterPath', cNotFound);
  if xStr = cNotFound then begin
    MMMessageDlg(cObjectNotFound + cRegMMToolsPath + ' ' + cTexnetTester, mtWarning, [mbOk], 0);
  end else begin
    StartApplication(xStr, omOpen, '');
  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.miInformatorTesterClick(Sender: TObject);
var
  xStr: string;
begin
  xStr := GetRegString(cRegLM, cRegMMServicePath, 'WSCTesterPath', cNotFound);
  if xStr = cNotFound then begin
    MMMessageDlg(cObjectNotFound + cRegMMServicePath, mtWarning, [mbOk], 0);
  end else begin
    StartApplication(xStr, omOpen, '');
  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acPreViewExecute(Sender: TObject);
begin
  with TfrmPrintMachConfig.Create(Self) do
  try
    mPrintGrid.Preview;
//    mmQuickRep.Preview;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acPrintExecute(Sender: TObject);
begin   
  Printers.Printer.Orientation := poLandscape;   //Nue:10.8.04 (Aus Init hierher gez�gelt)
  with TfrmPrintSettings.Create(Self) do
  try
       ShowClearerSettings := FALSE;
       PrinterSet.Portrait := FALSE;

       PrinterSet.OrientationEnable := FALSE;
       PrinterSet.BlackWhiteEnable := FALSE;
       if ShowModal = mrOK then
          with TfrmPrintMachConfig.Create(Self) do
          try
            mPrintGrid.Print;
          finally
            Free;
          end;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.mmSGMachNodeClickSort(Sender: TObject;
  aCol: Integer);
var
  i: Integer;
begin
  fGridNewSorted := True;
  for i := 1 to (mmSGMachNode.RowCount - mmSGMachNode.FixedRows) do begin
    mmSGMachNode.Cells[0, i] := '';
  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.mmSGMachNodeCanSort(Sender: TObject; aCol: Integer; var dosort: Boolean);
begin
//because '/' are in this column. AdvGrid detects <Number>/<Number> as a date and tries to sort as -> crash
// although the date colum in mmSGMachNode will be sorted wrong because it contains '.' instead of '/'
// ==> Bad programming by ADV!!!!!!! Nue
  if aCol = cMachTypeCol then
    dosort := False;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.mDictionaryAfterLangChange(Sender: TObject);
begin
  MMHtmlHelp.LoepfeIndex := mDictionary.LoepfeIndex;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.mmActionListMainUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  Handled := True;
  acMachineData.Enabled := mMachineDataFlag and gMMAvailable and MMSecurityControl.CanEnabled(acMachineData);
  acNewMach.Enabled := MMSecurityControl.CanEnabled(acNewMach);
  acDelete.Enabled := mDeleteFlag and MMSecurityControl.CanEnabled(acDelete);
  acChange.Enabled := mChangeFlag and MMSecurityControl.CanEnabled(acChange);
  acInsClearer.Enabled := MMSecurityControl.CanEnabled(acInsClearer);
  acInsFrontType.Enabled := MMSecurityControl.CanEnabled(acInsFrontType);
  acInsMachType.Enabled := MMSecurityControl.CanEnabled(acInsMachType);
  acInsNetTypen.Enabled := MMSecurityControl.CanEnabled(acInsNetTypen);
  acInsSensHead.Enabled := MMSecurityControl.CanEnabled(acInsSensHead);
end;

//------------------------------------------------------------------------------
procedure TMaConfigMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if gMMAvailable and  mMachInserted and mUploadReminderMsg then begin
    mUploadReminderMsg := False;
    CanClose := (MMMessageDlg(rsDoUpload, mtConfirmation, [mbAbort, mbIgnore], 0) = mrIgnore);
  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.GetUserToolButtons;
begin
  FreeAndNil(pmToolbar);
  FreeAndNil(acStandard);
  FreeAndNil(acCustomize);
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.SaveUserToolButtons;
begin
// dieses Override nicht l�schen, da die Funktionalit�t der Toolbar nicht ben�tigt wird
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.SetEnabledState(aEnabled: Boolean);
begin
  mToolBar.Enabled     := aEnabled;
  mmSGMachNode.Enabled := aEnabled;
end;
//------------------------------------------------------------------------------

procedure TMaConfigMain.acLogin1Execute(Sender: TObject);
begin
  inherited;
  MMSecurityControl.Refresh
end;

//------------------------------------------------------------------------------
procedure TMaConfigMain.FormShow(Sender: TObject);
begin
  inherited;
  if not ToolButton6.Visible then begin
    ToolButton1.AutoSize := False;
    ToolButton2.AutoSize := False;
    ToolButton3.AutoSize := False;
    ToolButton4.AutoSize := False;
    ToolButton5.AutoSize := False;
  end;//if
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
constructor TMaConfigReaderList.Create;
begin
  // TODO -cMM: TMaConfigReaderList.Create default body inserted
  inherited;
end;

//------------------------------------------------------------------------------
destructor TMaConfigReaderList.Destroy;
var
  i : Integer;
begin
  inherited;
  for i := 0 to mMachineDOMList.Count-1 do
    FreeAndNil(mMachineDOMList[i]^);
  FreeAndNil(mMachineDOMList)
end;

function TMaConfigReaderList.GetCount: Integer;
begin
  Result := mMachineDOMList.Count;
end;

//------------------------------------------------------------------------------
function TMaConfigReaderList.GetItems(Index: Integer): TMaConfigReader;
begin
  Result := NIL;
  if Index<mMachineDOMList.Count then
    Result := mMachineDOMList[Index];
end;

//------------------------------------------------------------------------------
procedure TMaConfigReaderList.LoadMaConfigFromDB;
const
  cLoadMaConfigAll = 'select c_machine_id from c_magroup_config ';
var
  xMaConfigReader: TMaConfigReader;
begin
  mMachineDOMList.Create;

  with TAdoDBAccess.Create(1) do try
    Init;
    with Query[0] do begin
      SQL.Text := cLoadMaConfigAll;
      open;
      while not(EOF) do begin
        xMaConfigReader := TMaConfigReader.Create;
        if xMaConfigReader<>NIL then begin
          with xMaConfigReader do begin
            MachID := FieldByName('c_machine_id').AsInteger;
            mMachineDOMList.Add(xMaConfigReader);
          end; //with xMaConfigReader begin
        end; //if xMaConfigReader<>NIL
        Next;
      end; //while
    end;// with Query[0] do begin
  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try
end;

//------------------------------------------------------------------------------
end.

