(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MaConfigMain.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00-
|------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.12.1999  1.00  Nue  | Datei erstellt
| 29.01.2001  1.01  Nue  | Integration Componente from Kr in u_InsMachNode
| 25.03.2001  1.02  Nue  | Suggested efficiency removed
| 28.08.2001  1.03  Nue  | Access over ODBC and no further over Native-DB-Driver
|                        | GUI-Component mDatabase: New =>Connected:=True (helped to
|                        | solve the problems at Streiff with BDE-Initalisation-crash
| 22.10.2001  1.04  Nue  | (Row > FixedRows) changed to (Row >= FixedRows)
| 01.11.2001  1.05  Nue  | Order of the columns in the grid changed.
| 24.01.2002  1.06  Nue  | Several minor changes. (Especially in section "Upload from machines")
| 07.03.2002  1.07  Nue  | Additional ResourceString cLoadNewData and corresponding handling added.
| 22.03.2002  1.08  Wss  | Umgestellt auf neues PrintForm, da beim alten beim Drucken das Form
                           immer sichtbar war
| 25.03.2002  1.09  Nue  | ResourceString cLoadFromMach before cLoadNewData added.
| 31.10.2002  1.10  Nue  | mrIgnore in acMachNodeDisconnectExecute added.
| 07.11.2002        Nue  | Problems with spindlerange changing on new machines handled.
| 08.11.2002        LOK  | Umbau ADO
| 10.12.2002        Nue  | Setzen des Maschinenstatus auf Online nach erfolgreichem Upload.
| 18.02.2003        Wss  | Funktion FromMachine abh�ngig, ob MMSystem vorhanden ist oder nicht.
                           MMClient MUSS gestarted sein, da auf Broadcast Meldungen reagiert wird!!
| 27.02.2003  1.11  Wss  | Beim warten Systemmeldung bei neuen Maschinen Toolbar und
                           Grid disabled, damit keine Mausaktivit�t ausgel�st werden kann.
                           Self.Enabled geht nicht, da die Sanduhr damit verschwindet!
| 05.03.2003        Nue  | Grossumbau auf EasyVersion.
| 18.03.2003        Nue  | Defaultausdruck auf poLandscape.
| 24.03.2003  1.12  Nue  | Security eingebaut und Verhalten von Buttons usw. in mmActionListMainUpdate verlegt.
| 30.06.2003  1.13  Nue  | Begrezen Texte in Toolbar-Buttons; Grid alBotton und Ausrichtung.
| 01.10.2003  1.13  Wss  | Druckbefehl erfolgt nun �ber PrintGrid Komponente
| 20.06.2004  1.13  SDo  | Print-DLG eingebaut, Preview vom Menue funktioniert jetzt
| 10.08.2004  1.14  Nue  | Printers.Printer.Orientation := poLandscape; Aus Init nach acPrintExecute gez�gelt)
| 25.02.2005        Wss  | div Dialoge werden nun bei Bedarf erstellt und nicht mehr pauschal beim Start der Applikation
| 26.10.2005        Lok  | Software Optioenn neu aus dem FPattern generieren (vorher Element im XML)
|=============================================================================*)
unit u_MaConfigMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEAPPLMAIN, Menus, mmMainMenu, IvDictio, IvAMulti, IvBinDic,
  mmDictionary, IvMulti, IvEMulti, mmTranslator, mmLogin, mmPopupMenu,
  mmOpenDialog, IvMlDlgs, mmSaveDialog, mmPrintDialog,
  mmPrinterSetupDialog, Db, StdActns,
  ActnList, mmActionList, ImgList, mmImageList, ComCtrls, mmStatusBar,
  ToolWin, mmToolBar, mmAnimate, ExtCtrls, mmPanel, Grids, AdvGrid,
  mmExtStringGrid, StdCtrls, mmLabel, mmEdit,
  AssignComp, BaseGlobal, MMUGlobal, mmList, mmDialogs,
  u_MaConfigComp, u_InsMachNode, u_HeadType,
  u_NetType, u_FrontType,
  mmComboBox, mmGroupBox, YMParaUtils, YMParaDef, MMSecurity, LoepfeGlobal,
  mmStringGrid, ShellAPI, MMHtmlHelp, u_dmMaConfig, ADODB, mmADODataSet, mmADOCommand,
  xmlMaConfigClasses, mmButton,
  Printers,  //Nue:17.3.03
  AdoDBAccess, MMQualityMatrixClass, MSXML2_TLB, XMLDef;

resourcestring
  cMachineReadOk = '(*)Daten von Maschine %s erfolgreich gelesen!'; //ivlm
  cMachineReadNOk = '(*)Daten von Maschine %s konnten nicht gelesen werden!'; //ivlm
  cLoadNewData = '(*)(Einige Datenfelder sind nicht mehr aktuell! Neu laden!)'; //ivlm
  cLoadFromMach = '(*)Daten geladen von Maschinen'; //ivlm
  cLoadFromDB = '(*)Daten geladen von Datenbank'; //ivlm
  cObjectNotFound = '(*)Objekt konnte nicht gefunden werden:'; //ivlm
  cTexnetTester = '(*)Der TexnetTester laeuft nur auf dem Server und nur bei ausgeschaltetem Basissystem!'; //ivlm
  cUploadProblem = '(*)Beim Upload von Maschine "%s" ist ein Problem aufgetaucht! (MaConfig neu starten!)'; //ivlm

const
  cMachTypeCol = 5;

  cNoYesStrArr: Array[Boolean] of String = ('NO', 'YES');

  cUnusedComponents: array[1..12] of string = (
    'acNew', 'acOpen', 'acSave', 'acSaveas', {'acPreview', 'acPrint',{}
    'acWindowCascade', 'acWindowTileVertical', 'acTileHorizontally',
    'miNew', 'miOpen', 'miSave', 'miSaveAs', {'miPreview', 'miPrint',{} 'miFenster'
    );

type
  TFileOpenMode = (omOpen, omPrint, omExplore);

const
  cFileOpenMode: array[TFileOpenMode] of string = ('open', 'print', 'explore');

type
  TMaConfigReaderList = class(TPersistent)
  private
  protected
    FOnDeleteItem: TNotifyEvent;
    mItems: TList;
    function GetCount: Integer;
    function GetItems(aIndex: Integer): TMaConfigFullReader;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    function Add(aItem: TMaConfigFullReader): Integer; virtual;
    procedure Assign(Source: TPersistent); override;
    procedure Clear;
    procedure Delete(aIndex: integer); virtual;
    function IndexOf(aItem: TMaConfigFullReader): Integer; virtual;
    procedure LoadAllMachConfigFromDB(aQuery: TmmADODataSet);
    property Count: Integer read GetCount;
    property Items[aIndex: Integer]: TMaConfigFullReader read GetItems; default;
    property OnDeleteItem: TNotifyEvent read FOnDeleteItem write FOnDeleteItem;
  end;

  TMaConfigMain = class(TBaseApplMainForm)
    acDBData: TAction;
    sgMachines: TmmExtStringGrid;
    acInsert1: TMenuItem;
    acInsSensHead: TAction;
    acInsSensHead1: TMenuItem;
    acNewMach: TAction;
    acMachNode1: TMenuItem;
    acChange: TAction;
    acMachNodeDisconnect1: TMenuItem;
    gbMachState: TmmGroupBox;
    labOnline: TmmLabel;
    labOffline: TmmLabel;
    labInProd: TmmLabel;
    labLoadInfo: TmmLabel;
    acInsNetTypen: TAction;
    acInsFrontType: TAction;
    MMSecurityDB: TMMSecurityDB;
    MMSecurityControl: TMMSecurityControl;
    acSecurityConfig: TAction;
    Zugriffkontrolle1: TMenuItem;
    NeuerKnoten1: TMenuItem;
    acInsFrontType1: TMenuItem;
    acInsNetTypen1: TMenuItem;
    acDelete: TAction;
    mQuery2: TmmADODataSet;
    Tools1: TMenuItem;
    miTexnetTester: TMenuItem;
    miInformatorTester: TMenuItem;
    Maschine1: TMenuItem;
    Netzadresse1: TMenuItem;
    Maschineaktivieren1: TMenuItem;
    MMHtmlHelp: TMMHtmlHelp;
    bEditMachine: TToolButton;
    bNewMachine: TToolButton;
    bDeleteMachine: TToolButton;
    bLoadMachinesFromDB: TToolButton;
    bPrint: TToolButton;
    bAccessControl: TToolButton;
    miLXTester: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure acDBDataExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure sgMachinesClickCell(Sender: TObject; Arow, Acol: Integer);
    procedure sgMachinesDblClickCell(Sender: TObject; Arow, Acol: Integer);
    procedure acNewMachExecute(Sender: TObject);
//    procedure acDownloadExecute(Sender: TObject);
    procedure sgMachinesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure acChangeExecute(Sender: TObject);
    procedure acInsSensHeadExecute(Sender: TObject);
    procedure acInsNetTypenExecute(Sender: TObject);
    procedure acInsFrontTypeExecute(Sender: TObject);
    procedure acSecurityConfigExecute(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure miTexnetTesterClick(Sender: TObject);
    procedure miInformatorTesterClick(Sender: TObject);
    procedure acPreViewExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure sgMachinesCanSort(Sender: TObject; aCol: Integer; var dosort: Boolean);
    procedure mDictionaryAfterLangChange(Sender: TObject);
    procedure mmActionListMainUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure acLogin1Execute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure miLXTesterClick(Sender: TObject);
    procedure sgMachinesGridHint(Sender: TObject; Arow, Acol: Integer;
      var hintstr: String);
  private
    mMachineDataFlag: Boolean;
    mDeleteFlag: Boolean;
    mChangeFlag: Boolean;
    mUploadReminderMsg: Boolean;
    mNodeStates: TNodeStatesSet;
    mAssWindowMsg: DWord;
    mMachInserted: Boolean;
    mMaConfigReaderList: TMaConfigReaderList;
    function StartApplication(aFileName: string; aMode: TFileOpenMode; aParam: string): THandle;
    procedure CreateInsMachNode(aFullMachine: TMaConfigFullReader);
    procedure WriteDBInfoToCells(aRow: Integer; aMaReader: TMaConfigFullReader; aGroupIndex: Integer);
    procedure SetEnabledState(aEnabled: Boolean);
//NUE1    property YMSettingsUtilsXX: TYMMachineSettings read fYMSettingsUtilsXX write fYMSettingsUtilsXX;
  protected
    procedure GetFromDB;
    procedure GetUserToolButtons; override;
    procedure SaveUserToolButtons; override;
    procedure WndProc(var msg: TMessage); override;
  public
  published
  end;

var
  MaConfigMain: TMaConfigMain;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS, MMMessages, IPCClass,
  PrintMaConfigForm, PrintSettingsTemplateForm, FPatternClasses;

{$R *.DFM}
//------------------------------------------------------------------------------

{ TMaConfigMain }
//------------------------------------------------------------------------------
function TMaConfigMain.StartApplication(aFileName: string; aMode: TFileOpenMode; aParam: string): THandle;
var
  xStr: string;
begin
  Result := ShellExecute(0, PChar(cFileOpenMode[aMode]), PChar(aFileName), PChar(aParam), '', SW_SHOWNORMAL);
  if Result <= 32 then begin
    case Result of
      0: xStr := 'The operating system is out of memory or resources.';
      ERROR_FILE_NOT_FOUND: xStr := 'The specified file was not found.';
      ERROR_PATH_NOT_FOUND: xStr := 'The specified path was not found.';
      ERROR_BAD_FORMAT: xStr := 'The .EXE file is invalid (non-Win32 .EXE or error in .EXE image).';
      SE_ERR_ACCESSDENIED: xStr := 'The operating system denied access to the specified file.';
      SE_ERR_ASSOCINCOMPLETE: xStr := 'The filename association is incomplete or invalid.';
      SE_ERR_DDEBUSY: xStr := 'The DDE transaction could not be completed because other DDE transactions were being processed.';
      SE_ERR_DDEFAIL: xStr := 'The DDE transaction failed.';
      SE_ERR_DDETIMEOUT: xStr := 'The DDE transaction could not be completed because the request timed out.';
      SE_ERR_DLLNOTFOUND: xStr := 'The specified dynamic-link library was not found.';
      SE_ERR_NOASSOC: xStr := 'There is no application associated with the given filename extension.';
      SE_ERR_OOM: xStr := 'There was not enough memory to complete the operation.';
      SE_ERR_SHARE: xStr := 'A sharing violation occurred.';
    else
      xStr := 'Undefined error';
    end;
    ShowMessage(Format('ShellExecute error for "%s": %d = %s', [aFileName, Result, xStr]));
  end;
end;
//------------------------------------------------------------------------------

procedure TMaConfigMain.WriteDBInfoToCells(aRow: Integer; aMaReader: TMaConfigFullReader; aGroupIndex: Integer);
var
  xSlip: Real;
  xCol: Integer;
  //............................................................
  procedure SetCellText(aString: String);
  begin
    sgMachines.Cells[xCol, aRow] := aString;
    inc(xCol);
  end;
  //............................................................
begin
  sgMachines.Objects[0, aRow] := aMaReader;
  xCol := 1;
  with aMaReader do begin
    SetCellText(NetNode.NetName);                            // Nodename
    SetCellText(NetNode.NodeId);                             // Node
    SetCellText(IntToStr(MachID));                           // Maschinen ID
    SetCellText(MachName);                                   // Maschinenname
    SetCellText(cMachineTypeArr[Machine.MachineType].Name);  // Maschinentyp
    SetCellText(cMachineTypeArr[Machine.MachineType].Manufacturer); // FrontType: Hersteller der Maschine
    SetCellText(cGUIFrontTypeNames[GetFrontTypeValue(MachValueDef[cXPYMTypeItem, ''])]);
    if Machine.LastConfirmedUpload < cDateTime1970 then      // LastSaved MaConfig
      SetCellText('')
    else
      SetCellText(DateToStr(Machine.LastConfirmedUpload));
    SetCellText(GroupValueDef[aGroupIndex, cXPSpindleFromItem,'']); // FirstSpd
    SetCellText(GroupValueDef[aGroupIndex, cXPSpindleToItem,'']);   // LastSpd
    SetCellText(cGUISensingHeadNames[GetSensingHeadValue(GroupValueDef[aGroupIndex, cXPSensingHeadItem, ''])]);     // Tastkopf
    SetCellText(GetAWEType(aMaReader, aGroupIndex)); // Clearertype
    SetCellText(MachValueDef[cXPYMSwVersionItem,'']);              // SW Version
    // DEie Software Optionen werden neu (26.10.2005) aus dem FPattern gebildet
    if Available then
      SetCellText(GetSWOptionSetNames(GroupNode[aGroupIndex, cXPFPatternNode])); // SW Option
    SetCellText(IntToStr(Machine.SpindlePos));                     // Spindel Pos
    SetCellText(cNoYesStrArr[Machine.FixSpdRanges]);               // Fix Spindlerange
    xSlip := Machine.Slip / 1000.0;
    if (xSlip < cMinSlip) or (xSlip > cMaxSlip) then               // Schlupf
      SetCellText('')
    else
      SetCellText(Format('%1.3f', [xSlip]));
    SetCellText(NetNode.AdaptOrName);                               // IP Name oder TXN Adapter
    SetCellText(DateToStr(Machine.Starting));                       // Erfassungsdatum der Maschine
    SetCellText(cNoYesStrArr[(Machine.DataCollection >= 1)]);       // Datenerfassung
    SetCellText(cNoYesStrArr[not Boolean(MachValueDef[cXPSpeedSimulationItem, False])]); // Speedsimulation
    SetCellText(GroupValueDef[aGroupIndex, cXPSpeedItem,'']);       // Geschwindigkeit
    SetCellText(GroupValueDef[aGroupIndex, cXPSpeedRampItem,'']);   // Anlauframpe
    SetCellText(MachValueDef[cXPCheckLengthItem,'']);               // Spleisscheckl�nge
    SetCellText(MachValueDef[cXPLongStopDefItem,'']);               // Langstopzeit
{
    if Machine.LastConfirmedUpload < cDateTime1970 then //Check auf 0.0 geht nicht, da (wahrscheinlich) ADO aus 0.0->2 macht!!!! Bug!! Nue:29.1.03
      sgMachines.Cells[3, aRow] := '?????'
    else
      sgMachines.Cells[3, aRow] := DateTimeToStr(Machine.LastConfirmedUpload);
    sgMachines.Cells[26, aRow] := NetNode.FrontManuf;

    sgMachines.Cells[20, aRow] := MachValueDef[cXPAWEMachTypeItem,''];  //??

    if FixSpdRanges then
      sgMachines.Cells[17, aRow] := cTrue
    else
      sgMachines.Cells[17, aRow] := cFalse;
    sgMachines.Cells[23, aRow] := cNoYesStrArr[Machine.OnlineSpeed];
    if OnlineSpeed then
      sgMachines.Cells[23, aRow] := cTrue
    else
      sgMachines.Cells[23, aRow] := cFalse;
    sgMachines.Cells[13, aRow] := IntToStr(Machine.MachineConfigA);
    sgMachines.Cells[14, aRow] := IntToStr(Machine.MachineConfigB);
    sgMachines.Cells[15, aRow] := IntToStr(Machine.MachineConfigC);
    sgMachines.Cells[28, aRow] := GroupValueDef[aGroupIndex,cXPconfigANode,''];
    sgMachines.Cells[29, aRow] := GroupValueDef[aGroupIndex,cXPconfigBNode,''];
    sgMachines.Cells[30, aRow] := GroupValueDef[aGroupIndex,cXPconfigCNode,''];
    sgMachines.Cells[31, aRow] := IntToStr(Machine.MachineYMConfigRec.spec[aGroupIndex+1].inoperativePara[0]);
    sgMachines.Cells[32, aRow] := IntToStr(Machine.MachineYMConfigRec.spec[aGroupIndex+1].inoperativePara[1]);
    sgMachines.Cells[33, aRow] := '?'; //GroupValueDef[aGroupIndex.inoperativePara[0]);
    sgMachines.Cells[34, aRow] := '?'; //GroupValueDef[aGroupIndex.inoperativePara[1]);

    sgMachines.Cells[cOrgGridIndexCol, aRow] := IntToStr(aRow);
{}
  end;
end;

//------------------------------------------------------------------------------
procedure TMaConfigMain.GetFromDB;
var
  i: Integer;
  xGrp: Integer;
  xOldCursor: TCursor;
  //.......................................................
  procedure AddSingleSGLine;
  begin
    // wenn erste Datenzeile noch leer ist dann diese erst verwenden bevor eine Neue hinzugef�gt wird
    if Assigned(sgMachines.Objects[0, sgMachines.Row]) then
      sgMachines.RowCount := sgMachines.RowCount + 1;
  end;
  //.......................................................
begin
  xOldCursor             := Screen.Cursor;
  Screen.Cursor          := crHourGlass;
  labLoadInfo.Font.Color := clGreen;
  labLoadInfo.Caption    := cLoadFromDB;

  // alle Referenzen zu einem Maschinenobjekt entfernen
  for i:=0 to sgMachines.RowCount-1 do
    sgMachines.Objects[0, i] := Nil;
  sgMachines.Clear;

  sgMachines.ShowColumnHeaders;
  sgMachines.ScrollBars := ssNone;
  sgMachines.RowCount   := sgMachines.FixedRows + 1;

  acDBData.Enabled := False;
  try
    // Neu laden der Konfiguration ab DB
    mMaConfigReaderList.LoadAllMachConfigFromDB(mQuery2);
    if mMaConfigReaderList.Count > 0 then begin
      mDeleteFlag := True;
      mChangeFlag := True;
      for i:=0 to mMaConfigReaderList.Count-1 do begin
        if mMaConfigReaderList.Items[i].GroupCount > 0 then begin
          for xGrp:=0 to mMaConfigReaderList.Items[i].GroupCount-1 do begin
            // f�gt wenn n�tig eine neue Zeile im Grid ein
            AddSingleSGLine;
            mMaConfigReaderList.Items[i].ActiveSpdGrp := xGrp + 1;
            WriteDBInfoToCells(sgMachines.RowCount-1, mMaConfigReaderList.Items[i], xGrp);
          end; // for xGrp
        end
        else begin
          // f�gt wenn n�tig eine neue Zeile im Grid ein
          AddSingleSGLine;
          WriteDBInfoToCells(sgMachines.RowCount-1, mMaConfigReaderList.Items[i], 0);
        end;
      end; //for i
    end
    else begin
      //Einf�llen einer Dummymaschine (sonst Indexverletzungen)
    end; //else
  except
    on e: Exception do 
      SystemErrorMsg_('TMaConfigMain.GetFromDB failed. ' + e.Message);
  end; 
  acDBData.Enabled := True;
  sgMachines.ScrollBars := ssBoth;
//  SetScrollPos(sgMachines.Handle,SB_HORZ,0,True);
  sgMachines.LeftCol := 1;
  Screen.Cursor := xOldCursor;
end;
//--------------------------------------------------------------------------
procedure TMaConfigMain.FormCreate(Sender: TObject);
var
  i: Integer;
  xComp: TComponent;
begin
  // cleanup unused components: TActions and TMenuItems
  for i := 1 to High(cUnusedComponents) do begin
    xComp := FindComponent(cUnusedComponents[i]);
    if Assigned(xComp) then
      FreeAndNil(xComp);
  end;

  HelpContext           := GetHelpContext('Machinenkonfiguration\MACONF_Maschinen_Konfiguration.htm');
  acNewMach.HelpContext := GetHelpContext('Machinenkonfiguration\MACONF_Neue_Maschine_einfuegen.htm');
  acDelete.HelpContext  := GetHelpContext('Machinenkonfiguration\MACONF_Maschinendaten_loeschen.htm');

  // Registrierung der Windows Meldung
  mAssWindowMsg      := RegisterWindowMessage(ASSIGN_REG_STR);

  mMachInserted      := False;
  mMachineDataFlag   := True;
  mUploadReminderMsg := False;

  mNodeStates         := [nsOnline];
  mMaConfigReaderList := TMaConfigReaderList.Create;

  acDBData.Execute;
//  GetFromDB;
  // nun wird noch MMGuard �ber MM Status angefragt, die Funktion
  // FromMachine ist vom laufenden MMSystem abh�ngig!
  // Status, ob MillMaster verf�gbar ist, wird in der globalen Variable
  // gMMAvailable in der WndProc Funktion gesetzt
//  SendGuardCommand(gcGetState);
//  Printers.Printer.Orientation := poLandscape;   //Nue:17.3.03
end;

//--------------------------------------------------------------------------
procedure TMaConfigMain.FormDestroy(Sender: TObject);
begin
  FreeAndNil(mMaConfigReaderList);
  inherited;
end;

//--------------------------------------------------------------------------
procedure TMaConfigMain.WndProc(var msg: TMessage);
begin
  try
{$IFDEF SetFixMMAvailableForAutoLauncher}
  gMMAvailable := True;
{$ENDIF}
    if msg.Msg = gMMClientMsgID then begin
      if Msg.WParam in [cMMStarted, cMMStarting, cMMStopped, cMMStopping, cMMAlert, cMMError, cMMAquisation] then begin
        gMMAvailable := (Msg.WParam = cMMStarted) OR
                        ((Msg.WParam = cMMAquisation) and (Msg.LParam = 0));

//Nue:Um zu debuggen, wenn Basissystem mit dem Autolauncher gestartet ist, muss hier fix gMMAvailable := True; gesetzt werden!
{$IFDEF SetFixMMAvailableForAutoLauncher}
  gMMAvailable := True;
{$ENDIF}
        if gMMAvailable and mMachInserted and mUploadReminderMsg then begin
          SetEnabledState(True);
          Screen.Cursor      := crDefault;
          mUploadReminderMsg := False;
          ShowMessage(rsDoUpload);
          GetFromDB;
        end;
      end
      else if msg.WParam = cMMRefresh then
        sgMachines.Invalidate;
    end else
      inherited WndProc(msg);
  except
    on e: Exception do begin
      SystemErrorMsg_('TMaConfigMain.WndProc failed. ' + e.Message);
      Exit;
    end;
  end;
end;
//--------------------------------------------------------------------------
procedure TMaConfigMain.acDBDataExecute(Sender: TObject);
begin
  mMachineDataFlag := True;
  SendGuardCommand(gcGetState, 'MaConfig');
  GetFromDB;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.sgMachinesClickCell(Sender: TObject; Arow, Acol: Integer);
var
  xMachine: TMaConfigFullReader;
begin
  if Arow >= sgMachines.FixedRows then begin
    xMachine := TMaConfigFullReader(sgMachines.Objects[0, Arow]);
    if Assigned(xMachine) and (xMachine.Validity = mvFull) then begin
      mChangeFlag := True;
      mDeleteFlag := True;
    end;
  end;

//  if {@Nue:9.4.01 (Acol<=sgMachines.FixedCols) and }(Arow >= sgMachines.FixedRows)
//     and (GetActMachArr.Count > 0) then begin //Nue:24.1.02
//    if ((GetActMachArr.Objects[Arow - sgMachines.FixedRows] as TMaConfigFullReader).Validity = mvFull) then begin
//      mChangeFlag := True;
////      acChange.Enabled := True;
//      mDeleteFlag := True;
////      acDelete.Enabled := True;
//    end;
//  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.sgMachinesDblClickCell(Sender: TObject; Arow, Acol: Integer);
begin
  acChange.Execute;
//  if Arow >= sgMachines.FixedRows then begin
//    xMachine := TMaConfigFullReader(sgMachines.Objects[0, sgMachines.Row]);
//    if Assigned(xMachine) then begin
////      xMachine.MachineInWork := True;
//      CreateInsMachNode(xMachine);
//    end;
//  end;

//  if (Arow >= sgMachines.FixedRows) and (GetActMachArr.Count > 0) then begin //Nue:24.1.02
//    xRealCol := StrToInt(sgMachines.Cells[cOrgGridIndexCol, Arow]) - sgMachines.FixedRows;
//    if ((GetActMachArr.Objects[xRealCol] as TMaConfigFullReader).Validity in [mvFull]) then begin
//      (GetActMachArr.Objects[xRealCol] as TMaConfigFullReader).MachineInWork := True;
//      CreateInsMachNode(GetActMachArr.Objects[xRealCol] as TMaConfigFullReader);
//    end;
//  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acNewMachExecute(Sender: TObject);
begin
  CreateInsMachNode(Nil);
end;
//------------------------------------------------------------------------------

procedure TMaConfigMain.sgMachinesGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
var
  xMachine: TMaConfigFullReader;
begin
  if (ACol = 0) and (ARow >= sgMachines.FixedRows)  then begin
    ABrush.Color := labOffline.Color;
    xMachine := TMaConfigFullReader(sgMachines.Objects[0, ARow]);
    if Assigned(xMachine) and (xMachine.Validity = mvFull) then begin
      if (not xMachine.Machine.UploadTimeout) and xMachine.Machine.IsOnline then begin
        xMachine.NetNode.NodeState := ord(nsOnline);
        if xMachine.Machine.IsInProduction then
          ABrush.Color := labInProd.Color
        else
          ABrush.Color := labOnline.Color;
      end else
        xMachine.NetNode.NodeState := ord(nsOffline);
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TMaConfigMain.acChangeExecute(Sender: TObject);
var
  xMachine: TMaConfigFullReader;
begin
  xMachine := TMaConfigFullReader(sgMachines.Objects[0, sgMachines.Row]);
  if Assigned(xMachine) then
    CreateInsMachNode(xMachine);
end;
//------------------------------------------------------------------------------

procedure TMaConfigMain.CreateInsMachNode(aFullMachine: TMaConfigFullReader);
begin
  with TdwInsMachNode.Create(Self) do
  try
    Init(aFullMachine);
    if (ShowModal = mrOK) and RestartMM then begin
      // beim Schliessend vom Dialog wird ein Restart zum System geschickt.
      mMachInserted      := True;
      mUploadReminderMsg := gMMAvailable;
      // in der Zwischenzeit den Wartemauszeiger anzeigen lassen, bis Anfrage
      // �ber MMClient per Windowsmessage best�tigt wird -> WndProc
      if gMMAvailable then begin
        SendGuardCommand(gcRestartMM, 'MaConfig');
        //TODO: Eventuell noch ein Timer starten, wenn was schief geht mit der MMStatus Meldung
        Screen.Cursor   := crHourGlass;
        SetEnabledState(False);
      end;
    end;
    // Wenn sonst irgendetwas ge�ndert hat -> neu von DB lesen
    if MachineChanged then begin
      sgMachines.Col := 1;
      GetFromDB;
    end;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acInsSensHeadExecute(Sender: TObject);
begin
  with TdwHeadType.Create(Self) do
  try
    ShowModal;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acInsNetTypenExecute(Sender: TObject);
begin
  with TdwNetType.Create(Self) do
  try
    ShowModal;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acInsFrontTypeExecute(Sender: TObject);
begin
  with TdwFrontType.Create(Self) do
  try
    ShowModal;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acSecurityConfigExecute(Sender: TObject);
begin
  MMSecurityControl.Configure;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acDeleteExecute(Sender: TObject);
var
  xMachine: TMaConfigFullReader;
  //................................................
//  procedure QryDelMachine;
//  begin
//    with cmdADO do begin
//      CommandText := cQryDelMachine; //Spindle members of the machine will be deleted by trigger
//      Parameters.ParamByName('c_machine_id').value := xMachine.MachID;
//      Execute;
//    end;
//  end;
//  //................................................
//  procedure QryDelNode;
//  begin
//    with cmdADO do begin
//      CommandText := cQryDelNode;
//      Parameters.ParamByName('c_node_id').value := xMachine.NetNode.NodeId;
//      Execute;
//    end;
//  end;
  //................................................
begin
  xMachine := TMaConfigFullReader(sgMachines.Objects[0, sgMachines.Row]);
  if Assigned(xMachine) then begin
    // Wenn Maschine in Produktion ist, dann erst nachfragen, ob diese wirklich gel�scht werden soll
    if xMachine.Machine.IsInProduction then begin
      if MMMessageDlg(rsMaInProduction, mtWarning, [mbOk, mbIgnore], 0) = mrIgnore then begin
        // wenn IGNORIEREN, dann auf der Datenbank den Status knallhart auf STOPPED setzen, ohne
        // die Daten zu holen!!!
        with TmmAdoCommand.Create(nil) do
        try
          ConnectionString := GetDefaultConnectionString;
          try
            CommandText := cQryStopProdGrpForced;
            Parameters.ParamByName('c_prod_end').DataType := ftDateTime;
            Parameters.ParamByName('c_prod_end').value    := Now;
            Parameters.ParamByName('c_machine_id').value  := xMachine.MachID;
            Execute;
          except
            on e:Exception do begin
              SystemErrorMsg_ ( ' TMaConfigMain.acDeleteExecute1 cQryStopProdGrpForced failed. ' + e.Message );
              Exit;
            end;// on e:Exception do begin
          end;// try except
        finally
          free;
        end;// with TmmAdoCommand.Create(nil) do try
      end // if (xState = mrIgnore) then begin
      else //mrOk
        Exit;
    end;// if (GetActMachArr.Objects[Row-1] as TFullMachine).IsInProduction then begin

    if MMMessageDlg(Format(rsAskForDelete, [xMachine.MachName]), mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
      with TAdoDBAccess.Create(1) do
      try
        Init;
        with Query[0] do try
          Connection.BeginTrans;
//          QryDelMachine; //Spindle members of the machine will be deleted by trigger
          SQL.Text := cQryDelMachine; //Spindle members of the machine will be deleted by trigger
          ParamByName('c_machine_id').AsInteger := xMachine.MachID;
          ExecSQL;

//          QryDelNode;
          SQL.Text := cQryDelNode;
          ParamByName('c_node_id').AsString := xMachine.NetNode.NodeId;
          ExecSQL;

          Connection.CommitTrans;
          if MMMessageDlg(rsSuccDeleted + cCRLF + cCRLF + rsRestartMM, mtConfirmation, [mbOK, mbCancel], 0) = mrOK then
            SendGuardCommand(gcRestartMM, 'MaConfig');

//          MMShowMessage(rsSuccDeleted);
//          MMMessageDlg(rsSuccDeleted, mtInformation, [mbOk], 0);
        except
          on e: Exception do begin
            Connection.RollbackTrans;
            SystemErrorMsg_(' TdwInsMachNode.acDeleteFromDBExecute2 cQryMachine/cQryDelNode failed. ' + e.Message);
          end;
        end;// with Query[0] do begin
      finally
        Free;
      end;// with TAdoDBAccess.Create(1) do try

      GetFromDB;
    end; // if
  end;

//  // Wenn Maschine in Produktion ist, dann erst nachfragen, ob diese wirklich gel�scht werden soll
//  with sgMachines do begin
//    if (GetActMachArr.Objects[Row-1] as TMaConfigFullReader).Machine.IsInProduction then begin
//      if MMMessageDlg(cMaInProduction, mtWarning, [mbOk,mbIgnore], 0) = mrIgnore then begin
//        // wenn IGNORIEREN, dann auf der Datenbank den Status knallhart auf STOPPED setzen, ohne
//        // die Daten zu holen!!!
//        with TmmAdoCommand.Create(nil) do
//        try
//          ConnectionString := GetDefaultConnectionString;
//          try
//            CommandText := cQryStopProdGrpForced;
//            Parameters.ParamByName ( 'c_prod_end' ).DataType := ftDateTime;
//            Parameters.ParamByName ( 'c_prod_end' ).value := Now;
//            Parameters.ParamByName ( 'c_machine_id' ).value := (GetActMachArr.Objects[Row-1] as TMaConfigFullReader).MachID;
//            Execute;
//          except
//            on e:Exception do begin
//              SystemErrorMsg_ ( ' TMaConfigMain.acDeleteExecute1 cQryStopProdGrpForced failed. ' + e.Message );
//              Exit;
//            end;// on e:Exception do begin
//          end;// try except
//        finally
//          free;
//        end;// with TmmAdoCommand.Create(nil) do try
//      end // if (xState = mrIgnore) then begin
//      else //mrOk
//        Exit;
//    end;// if (GetActMachArr.Objects[Row-1] as TFullMachine).IsInProduction then begin
//  end; //with
//
////  if MMMessageDlg(Format(cAskForDelete, [(GetActMachArr.Objects[sgMachines.Row - 1] as TFullMachine).MachineName]), mtConfirmation, [mbYes, mbNo], acDelete.HelpContext) = mrYes then begin
//  if MMMessageDlg(Format(cAskForDelete, [(GetActMachArr.Objects[sgMachines.Row - 1] as TMaConfigFullReader).MachValue[cXPMachNameItem]]), mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
//    with TAdoDBAccess.Create(1) do try
//      Init;
//      with Query[0] do try
//        Connection.BeginTrans;
//        QryDelMachine; //Spindle members of the machine will be deleted by trigger
//        QryDelNode;
//        Connection.CommitTrans;
//        MMMessageDlg(cSuccDeleted, mtInformation, [mbOk], 0);
//      except
//        on e: Exception do begin
//          Connection.RollbackTrans;
//          SystemErrorMsg_(' TdwInsMachNode.acDeleteFromDBExecute2 cQryMachine/cQryDelNode failed. ' + e.Message);
//        end;
//      end;// with Query[0] do begin
//    finally
//      Free;
//    end;// with TAdoDBAccess.Create(1) do try
//
//    GetFromDB;
//  end; // if
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.miTexnetTesterClick(Sender: TObject);
var
  xStr: string;
begin
  xStr := GetRegString(cRegLM, cRegMMToolsPath, 'TexnetTesterPath', '');
  if xStr = '' then MMMessageDlg(cObjectNotFound + cRegMMToolsPath + ' ' + cTexnetTester, mtWarning, [mbOk], 0)
               else StartApplication(xStr, omOpen, '');
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.miInformatorTesterClick(Sender: TObject);
var
  xStr: string;
begin
  xStr := GetRegString(cRegLM, cRegMMServicePath, 'WSCTesterPath', '');
  if xStr = '' then MMMessageDlg(cObjectNotFound + cRegMMServicePath, mtWarning, [mbOk], 0)
               else StartApplication(xStr, omOpen, '');
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.miLXTesterClick(Sender: TObject);
var
  xStr: string;
begin
  xStr := GetRegString(cRegLM, cRegMMToolsPath, 'LZETesterPath', '');
  if xStr = '' then MMMessageDlg(cObjectNotFound + cRegMMToolsPath, mtWarning, [mbOk], 0)
               else StartApplication(xStr, omOpen, '');
end;

//------------------------------------------------------------------------------
procedure TMaConfigMain.acPreViewExecute(Sender: TObject);
begin
  with TfrmPrintMachConfig.Create(Self) do
  try
    mPrintGrid.DataGrid := sgMachines;
    mPrintGrid.Preview;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.acPrintExecute(Sender: TObject);
begin
  Printers.Printer.Orientation := poLandscape;   //Nue:10.8.04 (Aus Init hierher gez�gelt)
  with TfrmPrintSettings.Create(Self) do
  try
    ShowClearerSettings := FALSE;
    PrinterSet.Portrait := FALSE;

    PrinterSet.OrientationEnable := FALSE;
    PrinterSet.BlackWhiteEnable := FALSE;
    if ShowModal = mrOK then
      with TfrmPrintMachConfig.Create(Self) do
      try
        mPrintGrid.DataGrid := sgMachines;
        mPrintGrid.Print;
      finally
        Free;
      end;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.sgMachinesCanSort(Sender: TObject; aCol: Integer; var dosort: Boolean);
begin
//because '/' are in this column. AdvGrid detects <Number>/<Number> as a date and tries to sort as -> crash
// although the date colum in sgMachines will be sorted wrong because it contains '.' instead of '/'
// ==> Bad programming by ADV!!!!!!! Nue
  dosort := (aCol <> cMachTypeCol)
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.mDictionaryAfterLangChange(Sender: TObject);
begin
  MMHtmlHelp.LoepfeIndex := mDictionary.LoepfeIndex;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.mmActionListMainUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  Handled := True;
//  acMachineData.Enabled  := mMachineDataFlag and gMMAvailable and MMSecurityControl.CanEnabled(acMachineData);
  acDelete.Enabled       := mDeleteFlag and MMSecurityControl.CanEnabled(acDelete);
  acChange.Enabled       := mChangeFlag and MMSecurityControl.CanEnabled(acChange);
//wss: das Verwenden der Funktion CanEnabled ist nur dann n�tig, wenn zus�tzlich noch andere Bedingungen verwendet werden
//  acNewMach.Enabled      := MMSecurityControl.CanEnabled(acNewMach);
//  acInsClearer.Enabled   := MMSecurityControl.CanEnabled(acInsClearer);
//  acInsFrontType.Enabled := MMSecurityControl.CanEnabled(acInsFrontType);
//  acInsMachType.Enabled  := MMSecurityControl.CanEnabled(acInsMachType);
//  acInsNetTypen.Enabled  := MMSecurityControl.CanEnabled(acInsNetTypen);
//  acInsSensHead.Enabled  := MMSecurityControl.CanEnabled(acInsSensHead);
end;

//------------------------------------------------------------------------------
procedure TMaConfigMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  // Info nur ausgeben wenn neue Maschinen erfasst aber noch nicht Uploaded wurde!!
  if gMMAvailable and mMachInserted and mUploadReminderMsg then begin
    mUploadReminderMsg := False;
    CanClose := (MMMessageDlg(rsDoUpload, mtConfirmation, [mbAbort, mbIgnore], 0) = mrIgnore);
  end;
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.GetUserToolButtons;
begin
  FreeAndNil(pmToolbar);
  FreeAndNil(acStandard);
  FreeAndNil(acCustomize);
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.SaveUserToolButtons;
begin
// dieses Override nicht l�schen, da die Funktionalit�t der Toolbar nicht ben�tigt wird
end;
//------------------------------------------------------------------------------
procedure TMaConfigMain.SetEnabledState(aEnabled: Boolean);
begin
  mToolBar.Enabled     := aEnabled;
  sgMachines.Enabled := aEnabled;
end;
//------------------------------------------------------------------------------

procedure TMaConfigMain.acLogin1Execute(Sender: TObject);
begin
  inherited;
  MMSecurityControl.Refresh
end;

//------------------------------------------------------------------------------
procedure TMaConfigMain.FormShow(Sender: TObject);
begin
  inherited;
  if not bAccessControl.Visible then begin
    bNewMachine.AutoSize         := False;
    bDeleteMachine.AutoSize      := False;
    bEditMachine.AutoSize        := False;
    bLoadMachinesFromDB.AutoSize := False;
    bPrint.AutoSize              := False;
  end;//if
{$IFDEF SetFixMMAvailableForAutoLauncher}
     MessageDlg('Autolauncher Version for MaConfig!!  NOT FOR RETAIL!!!!',mtError,[mbOK],-1);
{$ENDIF}

end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
constructor TMaConfigReaderList.Create;
begin
  inherited Create;
  mItems := TList.Create;
end;

//------------------------------------------------------------------------------
destructor TMaConfigReaderList.Destroy;
begin
  Clear;
  FreeAndNil(mItems);
  inherited Destroy;
end;

//------------------------------------------------------------------------------
function TMaConfigReaderList.Add(aItem: TMaConfigFullReader): Integer;
begin
  result := -1;
  if assigned(mItems) then
    result := mItems.Add(aItem);
end;

//------------------------------------------------------------------------------
procedure TMaConfigReaderList.Assign(Source: TPersistent);
var
  xSource: TMaConfigReaderList;
  xItem: TMaConfigFullReader;
  i: Integer;
begin
  if Source is TMaConfigReaderList then begin
    xSource := TMaConfigReaderList(Source);
    Clear;
    // Alle Eigenschaften kopieren
    for i := 0 to xSource.Count - 1 do begin
      xItem := TMaConfigFullReader.Create;
      xItem.Assign(xSource.Items[i]);
      Add(xItem);
    end;// for i := 0 to xSource.Count - 1 do begin

  end else begin
    // Weiterreichen
    inherited Assign(Source);
  end;// if Source is TMaConfigReaderList then begin
end;

//------------------------------------------------------------------------------
procedure TMaConfigReaderList.Clear;
begin
  while mItems.Count > 0 do
    delete(0);
end;

//------------------------------------------------------------------------------
procedure TMaConfigReaderList.Delete(aIndex: integer);
begin
  if assigned(mItems) then begin
    if aIndex < mItems.Count then begin
      if assigned(FOnDeleteItem) then
        FOnDeleteItem(Items[aIndex]);
      Items[aIndex].Free;
      mItems.delete(aIndex);
    end;// if aIndex < mItems.Count then begin
  end;// if assigned(mItems) then begin
end;

//------------------------------------------------------------------------------
function TMaConfigReaderList.GetCount: Integer;
begin
  result := 0;

  if assigned(mItems) then
    result := mItems.Count;
end;

//------------------------------------------------------------------------------
function TMaConfigReaderList.GetItems(aIndex: Integer): TMaConfigFullReader;
begin
  result := nil;
  if assigned(mItems) then
    if mItems.Count > aIndex then
      result := TMaConfigFullReader(mItems.Items[aIndex]);
end;

//------------------------------------------------------------------------------
function TMaConfigReaderList.IndexOf(aItem: TMaConfigFullReader): Integer;
var
  i: integer;
begin
  result := -1;

  i := 0;
  while (i < mItems.Count) and (result = -1) do begin
    if mItems[i] = aItem then
      result := i;
    inc(i);
  end;// while (i < mItems.Count) and (result = -1) do begin
end;

//------------------------------------------------------------------------------
procedure TMaConfigReaderList.LoadAllMachConfigFromDB(aQuery: TmmADODataSet);
var
  xMaConfigFullReader: TMaConfigFullReader;
begin
  Clear;
  with TAdoDBAccess.Create(2) do
  try
    Init;
    with Query[0] do begin
      SQL.Text := cQryGetMachineNode;
      Open;
      while not EOF do begin
        xMaConfigFullReader := TMaConfigFullReader.Create;
        with xMaConfigFullReader do begin
          MachID            := FieldByName('c_machine_id').AsInteger;  //Zuweisen der Maschinenkonfiguration  XML
          Query             := aQuery;  //Zuweisen Query f�r NetNode und TMachine
          Machine.MachineID := FieldByName('c_machine_id').AsInteger;  //Zuweisen der Maschinenkonfiguration NonXML
          NetNode.SetNode(FieldByName('c_node_id').AsString);     //Zuweisen der Nodeinfo
          Validity          := mvFull;
          ActiveSpdGrp      := 1;
          mItems.Add(xMaConfigFullReader);                  //Aufbereitetes Element der Liste zuf�gen
        end; //with xMaConfigFullReader begin
        Next;
      end; //while
    end;// with Query[0] do begin
  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try
end;

//------------------------------------------------------------------------------
procedure TMaConfigMain.sgMachinesGridHint(Sender: TObject; Arow,
  Acol: Integer; var hintstr: String);
begin
  with sgMachines do begin
    if (aRow = 0) and (Canvas.TextWidth(Cells[aCol, aRow]) > (ColWidths[aCol]-4)) then
      hintstr := Cells[aCol, aRow]
    else
      hintstr := '';
  end;
end;

end.

