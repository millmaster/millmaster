unit u_dmMaConfig;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, mmADODataSet, mmADOConnection, BaseGlobal, mmADOCommand;

type
  TdmMaConfig = class(TDataModule)
    conDefault: TmmADOConnection;
    dseQuery: TmmADODataSet;
    dseWork: TmmADODataSet;
    cmdWork: TmmADOCommand;
  private
    function MachineIsOnline(aMachID: Integer): Boolean;
  public
    function FillMapfiles(aList: TStrings; aNetTyp: TNetTyp): Boolean;
    function GetNewMachineID: Integer;
    function IPAdressInUse(aIPAdress: String): Boolean;
    function IsUniqueMachName(aMachID: Integer; aMachName: String): Boolean;
    property WorkDataSet: TmmADODataSet read dseWork;
    property WorkCommand: TmmADOCommand read cmdWork;
  end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

var
  gMMAvailable: Boolean;
  gMMClientMsgID: DWord;
  dmMaConfig: TdmMaConfig;

implementation
{$R *.DFM}
uses
  IPCClass, LoepfeGlobal, MMMessages;

//------------------------------------------------------------------------------
const
  cQryGetMaxMachineID =
    'select max(c_machine_id) max_machine_id from %s';

//  cQrySelMachType =
//    'SELECT * FROM t_machine_type ';

  cQryCheckIP =
    'select Count(*) AS IPCount from t_net_node where c_node_id=:c_node_id'; //Nue:1.3.04

  cQrySelNodeState =
    ' SELECT n.* FROM t_net_node n, t_machine m' +
    ' WHERE m.c_machine_id = :MachID AND m.c_node_id = n.c_node_id';

  cQrySelMapfile =
    ' SELECT c_mapfile_id, c_mapfile_name FROM t_xml_mapfile' +
    ' WHERE c_net_id in (0, :NetTyp)';

  cQryWhereDefNetID =
    'WHERE c_default_net_id in (0, :netTyp) ';

  cQryUniqueMachName =
    ' SELECT * FROM t_machine' +
    ' WHERE c_machine_id <> :MachID AND c_machine_name = :MachName';
//:-----------------------------------------------------------------------------
function TdmMaConfig.FillMapfiles(aList: TStrings; aNetTyp: TNetTyp): Boolean;
begin
  Result := False;
  aList.Clear;
  with dseQuery do
  try
    Close;
    // bei der RadioGroup wird ja immer gleich der Index geschrieben. Daher kann dieser hier verwendet werden.
    CommandText := cQrySelMapfile;
    Parameters.ParamByName('NetTyp').value := ORD(aNetTyp);
    Open;
    if FindFirst then begin
      while not EOF do begin
        aList.AddObject(FieldByName('c_mapfile_name').AsString, TObject(FieldByName('c_mapfile_id').AsInteger));
        Next;
      end;
      Result := True;
    end;
  except
    on e: Exception do
      SystemErrorMsg_('TdmMaConfig.FillMachineTypes failed: ' + e.Message);
  end;
end;

//------------------------------------------------------------------------------

function TdmMaConfig.GetNewMachineID: Integer;
  //............................................................
  function GetMaxMachindeID(aTable: String; aDefault: Integer): Integer;
  begin
    Result := aDefault;
    with dseQuery do
    try
      Close;
      // erst mal die MaxID von t_machine holen
      CommandText := Format(cQryGetMaxMachineID, [aTable]);
      Open;
      if FindFirst then
        Result := FieldByName('max_machine_id').AsInteger + 1;
    except
      on e: Exception do begin
        SystemErrorMsg_('TdmMaConfig.GetMaxMachindeID failed: ' + e.Message);
      end;
    end;
  end;
  //............................................................
begin
  // erst mal die MaxID von t_machine holen
  Result := GetMaxMachindeID('t_machine', 1);

  with dseQuery do
  try
    // nun noch pr�fen, ob diese MaID nicht schon in der Tabelle t_prodgroup vorhanden ist
    Close;
    CommandText := Format('select count(*) AS Count from t_prodgroup where c_machine_id = %d', [Result]);
    Open;
    if FindFirst then begin
      // f�r diese MaID hat es schon Produktionsdaten -> DARF NICHT VERWENDET WERDEN
      if FieldByName('Count').AsInteger > 0 then begin
        // nun wird halt der MaxMaID aus der Produktionstabelle ermittelt
        Result := GetMaxMachindeID('t_prodgroup', Result);
      end;
    end;
  except
    on e: Exception do begin
      SystemErrorMsg_('TdmMaConfig.GetNewMachineID failed: ' + e.Message);
    end;
  end;
end;

//:-----------------------------------------------------------------------------
function TdmMaConfig.IPAdressInUse(aIPAdress: String): Boolean;
begin
  Result := True;
  with dseQuery do
  try
    Close;
    CommandText := cQryCheckIP;
    Parameters.ParamByName('c_node_id').value := aIPAdress;
    Open;
    Result := (FieldByName('IPCount').AsInteger > 0);
  except
    on e: Exception do
      SystemErrorMsg_('TdmMaConfig.IPAdressInUse failed: ' + e.Message);
  end;
end;
//:-----------------------------------------------------------------------------
function TdmMaConfig.MachineIsOnline(aMachID: Integer): Boolean;
begin
  with dseQuery do
  try
    Close;
    CommandText := cQrySelNodeState;
    Parameters.ParamByName('MachID').value := aMachID;
    Open;
    Result := FindFirst and (TNodeState(FieldByName('c_node_stat').AsInteger) = nsOnline);
  except
    on e: Exception do begin
      SystemErrorMsg_('TdmMaConfig.MachineIsOnline failed: ' + e.Message);
    end;
  end;
end;

//:-----------------------------------------------------------------------------
function TdmMaConfig.IsUniqueMachName(aMachID: Integer; aMachName: String): Boolean;
begin
  with dseQuery do
  try
    Close;
    CommandText := cQryUniqueMachName;
    Parameters.ParamByName('MachID').value   := aMachID;
    Parameters.ParamByName('MachName').value := aMachName;
    Open;
    Result := EOF;
//    Result := not FindFirst;
  except
    on e: Exception do begin
      SystemErrorMsg_('TdmMaConfig.IsUniqueMachName failed: ' + e.Message);
    end;
  end;
end;

//------------------------------------------------------------------------------
initialization
  gMMAvailable   := False; //Nue:Um zu debuggen, wenn Basissystem mit dem Autolauncher gestartet ist, muss hier TRUE gesetzt werden!
  gMMClientMsgID := RegisterWindowMessage(cMsgAppl);
end.

