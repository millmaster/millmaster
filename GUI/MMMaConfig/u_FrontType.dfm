inherited dwFrontType: TdwFrontType
  Left = 417
  Top = 222
  Caption = '(*)Frontendtypen'
  ClientHeight = 201
  ClientWidth = 386
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited bOK: TmmButton
    Left = 195
    Top = 164
  end
  inherited bCancel: TmmButton
    Left = 288
    Top = 164
    Visible = False
  end
  object mmDBGrid1: TmmDBGrid
    Left = 16
    Top = 8
    Width = 354
    Height = 144
    Anchors = [akRight, akBottom]
    DataSource = mmDataSource2
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'c_front_type'
        Title.Caption = '(*)Typ'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'c_front_manuf'
        Title.Caption = '(*)Hersteller'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'c_front_model'
        Title.Caption = '(*)Modell'
        Visible = True
      end>
  end
  object bDelete: TmmBitBtn
    Left = 8
    Top = 164
    Width = 87
    Height = 25
    Hint = '(*)Loescht den selektierten Eintrag auf der DB'
    Anchors = [akRight, akBottom]
    Caption = '(9)Loeschen'
    Enabled = False
    TabOrder = 3
    Visible = True
    OnClick = bDeleteClick
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333FF33333333333330003333333333333777333333333333
      300033FFFFFF3333377739999993333333333777777F3333333F399999933333
      3300377777733333337733333333333333003333333333333377333333333333
      3333333333333333333F333333333333330033333F33333333773333C3333333
      330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
      993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
      333333377F33333333FF3333C333333330003333733333333777333333333333
      3000333333333333377733333333333333333333333333333333}
    NumGlyphs = 2
    AutoLabel.LabelPosition = lpLeft
  end
  object bInsert: TmmBitBtn
    Left = 101
    Top = 164
    Width = 87
    Height = 25
    Hint = '(*)Fuegt einen neuen Eintrag auf der DB ein'
    Anchors = [akRight, akBottom]
    Caption = '(9)Einfuegen'
    Enabled = False
    TabOrder = 4
    Visible = True
    OnClick = bInsertClick
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      33333333FF33333333FF333993333333300033377F3333333777333993333333
      300033F77FFF3333377739999993333333333777777F3333333F399999933333
      33003777777333333377333993333333330033377F3333333377333993333333
      3333333773333333333F333333333333330033333333F33333773333333C3333
      330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
      993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
      333333333337733333FF3333333C333330003333333733333777333333333333
      3000333333333333377733333333333333333333333333333333}
    NumGlyphs = 2
    AutoLabel.LabelPosition = lpLeft
  end
  object mmDataSource2: TmmDataSource
    DataSet = mmTable1
    Left = 48
    Top = 48
  end
  object mmTable1: TmmADODataSet
    Connection = dmMaConfig.conDefault
    AfterPost = mmTable1AfterPost
    CommandText = 't_front_type'
    CommandType = cmdTable
    FieldDefs = <
      item
        Name = 'c_front_type'
        Attributes = [faRequired]
        DataType = ftSmallint
      end
      item
        Name = 'c_front_manuf'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'c_front_model'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'c_default_net_id'
        DataType = ftSmallint
      end>
    Parameters = <>
    StoreDefs = True
    Left = 144
    Top = 48
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 48
    Top = 104
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
end
