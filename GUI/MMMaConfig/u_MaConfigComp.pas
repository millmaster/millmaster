(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MaConfigMain.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.12.1999  1.00  Nue  | Datei erstellt
| 25.03.2002  1.10  Nue  | cQryStopProdGrpForced added.
| 08.11.2002        LOK  | Umbau ADO
| 09.12.2002  1.11  Nue  | select * from t_prodgroup_state => neu select * from v_prodgroup_state
| 10.12.2002        Nue  | c_machine_state=0 added in query cQryUpdDisconnectMachine
| 27.02.2003        Wss  | cQryGetMaxMachineID: Query f�r neue MachID hold max ID nicht mehr von
                           t_machine, sondern von t_prodgroup, da wenn die Maschine mit der h�chste
                           MaId gel�scht wird und anschliessend wieder eine neu hinzugef�gt
                           wird, diese dann die gleiche ID bekommt wie die gel�scht. Somit w�ren
                           auch Historydaten von der alten Maschine mit der neuen verlinkt, was
                           jedoch nie sein darf!!
| 14.05.2004        Wss  | TSetMaConfigHandler.Assign: jtSetMaConfig remarked, da dieser im JobHandler
                           nirgends ausgewertet wird.
| 09.12.2004  2.00  Nue  | Umbau auf XML V.5.0.
| 23.05.2005  2.00  Wss  | Bei UpdateMachine wurde SpindleCount auf der DB nicht nachgef�hrt
| 12.07.2005        Nue  | Parameters.ParamByName('c_AWE_type').value  := ORD(aweNone); //Added
| 27.11.2006  2.01  Nue  | c_use_newer_mapfile in Queries added
|=============================================================================*)
unit u_MaConfigComp;                                                  

interface                                                

uses
  Windows, Messages, SysUtils, Classes, mmLabel, mmEdit,
  mmQuery,
  AssignComp, BaseGlobal, YMParaDef, MMUGlobal, mmADODataSet,
  XMLDef, xmlMaConfigClasses;

resourcestring
  cNotUnique           = '(*)Eines oder mehr Eingabefelder sind auf der DB bereits vorhanden!'; //ivlm
  rsSuccDeleted        = '(*)Maschine erfolgreich auf DB geloescht! Produktionsdaten sind noch verfuegbar.'; //ivlm
  rsSuccInserted       = '(*)Maschine erfolgreich auf DB eingefuegt!'; //ivlm
  rsSuccSaved          = '(*)Aenderungen der Maschine erfolgreich auf DB gespeichert!'; //ivlm
  rsStopStartMM        = '(*)MillMaster STOPPEN und STARTEN, damit die neuen Maschinen verfuegbar werden.'; //ivlm
  rsRestartMM          = '(*)MillMaster muss neu gestarted werden, damit die neuen Maschinen verfuegbar werden.'; //ivlm
  rsDoUpload           = '(*)Upload ausfuehren und allenfalls Zusatzinfo fuer Maschinen eingeben.'; //ivlm
  rsAskForDelete       = '(*)Wollen Sie Maschine %s wirklich loeschen?'; //ivlm
  cAskForDisconnect    = '(*)Wollen Sie die Verbindung zwischen der Netzadresse und der Maschine wirklich trennen?'; //ivlm
  cAskForSpindleUpdate = '(*)Wollen sie wirklich den Spulstellenbereich fuer diese Maschine uebernehmen?'; //ivlm
  cSpindleDeleted      = '(*)Spulstellen %d - %d von Maschine %s geloescht!'; //ivlm
  cSpindleLack         = '(*)Spulstellen %d - %d von Maschine %s werden nicht gespeichert, weil Spindelloecher entstehen.!'; //ivlm
  cNoNodeAvailable     = '(*)Keine freie Netzadresse vorhanden! Bitte neue Netzadresse eingeben!'; //ivlm
  rsMaInProduction     = '(*)Es ist zur Zeit nicht moeglich, diese Maschine zu deaktivieren, da noch Spulstellen in Produktion sind!'; //ivlm
  cProdGrpRunning      = '(*)Es ist zur Zeit nicht moeglich, bei dieser Maschine die Datenerfassung zu stoppen. Zuerst alle Partien stoppen!'; //ivlm
  cNotSaved            = '(*)Daten wurden veraendert, aber noch nicht gespeichert! Wollen sie jetzt speichern?'; //ivlm

  cMachineReadOk       = '(*)Daten von Maschine %s erfolgreich gelesen!'; //ivlm
  rsMachineReadNOk      = '(*)Daten von Maschine %s konnten nicht gelesen werden!'; //ivlm
  cLoadNewData         = '(*)(Einige Datenfelder sind nicht mehr aktuell! Neu laden!)'; //ivlm
  cLoadFromMach        = '(*)Daten geladen von Maschinen'; //ivlm
  cLoadFromDB          = '(*)Daten geladen von Datenbank'; //ivlm
  cObjectNotFound      = '(*)Objekt konnte nicht gefunden werden:'; //ivlm
  cTexnetTester        = '(*)Der TexnetTester laeuft nur auf dem Server und nur bei ausgeschaltetem Basissystem!'; //ivlm
  cUploadProblem       = '(*)Beim Upload von Maschine "%s" ist ein Problem aufgetaucht! (MaConfig neu starten!)'; //ivlm
  cNoGroupInProd       = '(*)(ACHTUNG! Bei Maschinen vom Typ AC338 muessen ALLE Spulstellen in Produktion sein um die komplette Maschinenkonfiguration von allen Spulstellen korrekt hoch zuladen!)'; //ivlm
  rsMachNameNotUnique  = '(*)Eine Maschine mit diesem Namen existiert bereits!'; // ivlm
//  rsNoMachTypFound     = '(*)Es wurden keine passenden Maschnentypen gefunden. Bitte ergaenzen Sie diese im Dialog "Maschinentypen".';

  //  cNoGroupInProd    = '(*)ACHTUNG! Bei Maschine "%s" vom Typ AC338 ist keine Partie in Produktion. Um die komplette Maschinenkonfiguration von allen Spulstellen korrekt hoch zuladen muessen bei ALLEN AC338-Maschinen ALLE Spulstellen in Produktion sein!';
//alt  cAskForSpindleUpdate = '(*)Im Updatebereich sind bereits Spindeln vorhanden! Wollen sie diese wirklich mit den neuen Werten ueberschreiben?';
//alt  cRestartMM        = '(*)Nach dem Aktivieren oder Deaktivieren von Maschinen muss das Millmaster-System gestoppt und wieder gestartet werden, um den Millmaster-Kernel mit der DB zu synchronisieren. (DB ist i.O.!)';
//  cRestartMM       = '(*)MillMaster STOPPEN und STARTEN, damit die Maschinenkonfiguration aktualisiert wird!';
//alt  cNotSaved        = '(*)Daten wurden veraendert, aber noch nicht gespeichert! Wollen sie das Fenster wirklich verlassen?';

const
//------------------------------------------------------------------------------

  cMaConfigPipeName = cAssignmentsPipeName + 'MaConfig' + cAddOnChannelName;

//Queries
//------------------------------------------------------------------------------
  cQryGetRunningProdGrps =
    'select * from v_prodgroup_state where c_machine_id=:c_machine_id '; //Nue:09.12.02
//------------------------------------------------------------------------------
  cQrySetMachineState =
    'update t_machine set c_machine_state=1 where c_machine_id=:c_machine_id '; //Nue:10.12.02 Set Online';
//------------------------------------------------------------------------------
  cQryGetMachines =
    'select * from t_machine ';
//------------------------------------------------------------------------------
  //wss: siehe Kommentar im Header, warum MaxID von t_prodgroup und nicht mehr t_machine
//  cQryGetMaxMachineID =
//    'select max(c_machine_id) max_machine_id from %s';
//  'select max(c_machine_id) max_machine_id from t_machine ';
//------------------------------------------------------------------------------
  cQryGetMachinesCount =
    'select anz=count(*) from t_machine ';
//------------------------------------------------------------------------------
//  cQryGetMinMaxSpindle =
//    'select spindle_first=MIN(c_spindle_id),spindle_last=MAX(c_spindle_id) from t_spindle where c_machine_id=:c_machine_id ';
//------------------------------------------------------------------------------
//  cQryGetSpindleExists =
//    'select * from t_spindle where c_machine_id=:c_machine_id and c_spindle_id>=:spindle_first and ' +
//    'c_spindle_id<=:spindle_last ';
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  cQryGetNodesNotAssigned =
    'select distinct n.c_node_id,n.c_adapt_or_name,n.c_node_stat,n.c_front_stat,f.c_front_manuf,f.c_front_model,t.c_net_id,' +
    't.c_net_manuf,t.c_net_name,t.c_net_type from t_net_node n, t_net t, t_front_type f ' +
    'where t.c_net_id=n.c_net_id and n.c_front_type=f.c_front_type and n.c_node_id not in ' +
    '(select c_node_id from t_machine) ';
//------------------------------------------------------------------------------
//  cQryGetMachinesNotAssigned =
//    'select distinct * from t_machine m, t_machine_type mt ' +
//    'where mt.c_machine_type=m.c_machine_type and ' +
//    'm.c_node_id like ''0'' ';
//------------------------------------------------------------------------------
  cQueryNodeInfo =
    'select n.c_net_id, n.c_net_manuf, n.c_net_name, n.c_net_type, ' +
    'f.c_front_type, f.c_front_manuf, f.c_front_model, ' +
    'nn.c_adapt_or_name, nn.c_node_stat, nn.c_front_stat ' +
    'from t_net n, t_net_node nn, t_front_type f ' +
    'where nn.c_front_type = f.c_front_type and n.c_net_id = nn.c_net_id ' +
    'and nn.c_node_id =:c_node_id ';

//------------------------------------------------------------------------------
  cQrySelFrontType =
    'SELECT * FROM t_front_type ';
//------------------------------------------------------------------------------
  cQrySelNetType =
    'SELECT * FROM t_net ';
//------------------------------------------------------------------------------
  cQrySelNetNodes =
    'SELECT * FROM t_net_node ';
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  cQryWhereNetID =
    'WHERE c_net_id in (0, :netTyp) ';
//------------------------------------------------------------------------------
  cQryInsNode =
    'insert t_net_node (c_node_id,c_net_id,c_adapt_or_name,c_front_type,c_node_stat,c_front_stat) ' +
    'values(:c_node_id,:c_net_id,:c_adapt_or_name,:c_front_type,:c_node_stat,:c_front_stat) ';
//------------------------------------------------------------------------------
//  cQryInsMachine =
//    'insert t_machine (c_machine_id,c_net_id,c_node_id,c_machine_type,' +
//    'c_maoffset_id,c_machine_name,c_starting,c_nr_of_spindles,c_data_collection,c_online_speed,' +
//    'c_fixSpindlerange,c_slip,c_opt_speed,c_ls_in_prod,c_ls_break,c_ls_wait_mat,c_ls_wait_rev,' +
//    'c_ls_clean,c_ls_free,c_ls_define,c_spindle_pos,c_speedRamp,c_longStopDef,' +
//    'c_cutRetries,c_checkLen,c_configA,c_configB,c_configC,c_inopSettings0,c_inopSettings1,c_YM_version,' +
//    'c_AWE_mach_type,c_optionCode,c_last_confirmed_upload,c_overrule_mapfile) ' +
//    'values(:c_machine_id,:c_net_id,:c_node_id,:c_machine_type,' +
//    ':c_maoffset_id,:c_machine_name,:c_starting,:c_nr_of_spindles,:c_data_collection,:c_online_speed,' +
//    ':c_fixSpindlerange,:c_slip,:c_opt_speed,0,0,0,0,' +
//    '0,0,0,:c_spindle_pos,:c_speedRamp,:c_longStopDef,' +
//    ':c_cutRetries,:c_checkLen,0,0,0,:c_inopSettings0,:c_inopSettings1,:c_YM_version,' +
//    ':c_AWE_mach_type,:c_optionCode,:c_last_confirmed_upload,:c_overrule_mapfile) ';
  cQryInsMachine =
    'insert t_machine (c_machine_id, c_net_id, c_node_id, c_machine_type,' +
    'c_maoffset_id, c_machine_name, c_starting, c_nr_of_spindles, c_data_collection,' +
//    'c_fixSpindlerange, c_slip, c_spindle_pos) ' +
    'c_fixSpindlerange, c_slip, c_spindle_pos, c_overrule_mapfile_id, c_use_newer_mapfile) ' +
    'values(:c_machine_id, :c_net_id, :c_node_id, :c_machine_type,' +
    ':c_maoffset_id, :c_machine_name, :c_starting, :c_nr_of_spindles, :c_data_collection,' +
//    ':c_fixSpindlerange, :c_slip, :c_spindle_pos) ';
    ':c_fixSpindlerange, :c_slip, :c_spindle_pos, :c_overrule_mapfile_id, :c_use_newer_mapfile) ';
//------------------------------------------------------------------------------
  cQryUpdNode =
//    'update t_net_node set c_front_type=:c_front_type, c_node_stat=:c_node_stat, '+
//    'c_front_stat=:c_front_stat, c_net_id=:c_net_id, ' +
//    'c_adapt_or_name=:c_adapt_or_name, ' + //Nue:6.2.03
//    'c_node_id=:c_newnode_id ' + //Nue:1.3.04
//    'where c_node_id=:c_node_id ';

    'update t_net_node set c_front_type=:c_front_type, c_net_id=:c_net_id, ' +
    'c_adapt_or_name=:c_adapt_or_name, c_node_id=:c_newnode_id ' +
    'where c_node_id=:c_node_id ';
//------------------------------------------------------------------------------
//  cQryUpdMachine =
//    'update t_machine set c_net_id=:c_net_id,c_node_id=:c_node_id,' +
//    'c_machine_type=:c_machine_type,' + //Alt bis 5.11.01 Nue c_maoffset_id=:c_maoffset_id,'+
//    'c_machine_name=:c_machine_name,c_starting=:c_starting,c_nr_of_spindles=:c_nr_of_spindles,' +
//    'c_data_collection=:c_data_collection,c_online_speed=:c_online_speed,c_fixSpindlerange=:c_fixSpindlerange,c_slip=:c_slip,' +
//    'c_opt_speed=:c_opt_speed,c_spindle_pos=:c_spindle_pos,c_speedRamp=:c_speedRamp,' +
//    'c_longStopDef=:c_longStopDef,c_cutRetries=:c_cutRetries,c_checkLen=:c_checkLen,c_configA=:c_configA,' +
//    'c_configB=:c_configB,c_configC=:c_configC,c_inopSettings0=:c_inopSettings0,c_inopSettings1=:c_inopSettings1,' +
//    'c_YM_version=:c_YM_version,c_AWE_mach_type=:c_AWE_mach_type,c_optionCode=:c_optionCode,c_last_confirmed_upload=:c_last_confirmed_upload ' +
//    'where c_machine_id=:c_machine_id ';
//  cQryUpdMachine =
//    'update t_machine set c_net_id=:c_net_id,c_node_id=:c_node_id,' +
//    'c_machine_type=:c_machine_type,' +
//    'c_machine_name=:c_machine_name, c_starting=:c_starting, c_nr_of_spindles=:c_nr_of_spindles,' +
//    'c_data_collection=:c_data_collection, c_fixSpindlerange=:c_fixSpindlerange, c_slip=:c_slip,' +
//    'c_spindle_pos=:c_spindle_pos,' +
//    'c_longStopDef=:c_longStopDef, c_cutRetries=:c_cutRetries, c_checkLen=:c_checkLen ' +
//    'where c_machine_id=:c_machine_id ';
  cQryUpdMachine =
    'update t_machine set c_net_id=:c_net_id, c_node_id=:c_node_id, ' +
    'c_machine_type=:c_machine_type, c_machine_name=:c_machine_name, c_nr_of_spindles=:c_nr_of_spindles, ' +
    'c_data_collection=:c_data_collection, c_fixSpindlerange=:c_fixSpindlerange, c_slip=:c_slip, ' +
    'c_spindle_pos=:c_spindle_pos, c_overrule_mapfile_id=:c_overrule_mapfile_id, c_use_newer_mapfile=:c_use_newer_mapfile ' +
    'where c_machine_id=:c_machine_id ';
//------------------------------------------------------------------------------
  cQryUpdDisconnectMachine =
    'update t_machine set c_net_id=:c_net_id, c_node_id=:c_node_id, c_AWE_mach_type=:c_AWE_mach_type,' +
    'c_machine_state=0 ' + //Nue:10.12.02
    'where c_machine_id=:c_machine_id ';
//------------------------------------------------------------------------------
//  cQryUpdNrOfSpindle =
//    'update t_machine set c_nr_of_spindles=(select max(c_spindle_id) from t_spindle where c_machine_id=:c_machine_id) where c_machine_id=:c_machine_id ';
//------------------------------------------------------------------------------
  cQryInsSpindle =
    'insert t_spindle (c_spindle_id,c_machine_id,c_clear_type,c_head_type,c_AWE_type,' +
    'c_act_stat,c_prod_km_cum,c_splices_cum,c_runtime_cum) ' +
    'values(:c_spindle_id,:c_machine_id,0,:c_head_type,:c_AWE_type,' +
    '0,0,0,0) ';
//------------------------------------------------------------------------------
//  cQryUpdSpindle =
//    'update t_spindle set c_clear_type=:c_clear_type,c_head_type=:c_head_type ' +
//    'where c_spindle_id=:c_spindle_id and c_machine_id=:c_machine_id ';
//------------------------------------------------------------------------------
//  cQryDelSpindle =
//    'delete t_spindle where c_spindle_id>:c_spindle_start and c_spindle_id<=:c_spindle_stop and c_machine_id=:c_machine_id';
//------------------------------------------------------------------------------
  cQryDelNode =
    'delete t_net_node where c_node_id=:c_node_id ';
//------------------------------------------------------------------------------
  cQryDelMachine =
    'delete t_machine where c_machine_id=:c_machine_id ';
//------------------------------------------------------------------------------
  cQryStopProdGrpForced = //Added with V1.10
    'UPDATE t_prodgroup SET c_prod_state=4, c_prod_end=:c_prod_end ' + //4=stopped
    'WHERE c_machine_id=:c_machine_id ' +
    'DELETE t_prodgroup_state WHERE c_machine_id=:c_machine_id';
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

type
//..............................................................................
  TMachValidity = (mvNone, mvFull, mvNewMachine, mvDisconnect);
  TNodeStatesSet = set of TTxnNodeState;

//..............................................................................
//  TFullMachine = class(TMachine)
//  private
//    fValidity: TMachValidity;
//    mNetNode: TNetNode;
//    fActiveSpdGrp: Integer; //Die SpdGrp welche im fMachineYMConfigRec gueltig ist (1..12)
//    FMachineInWork: Boolean;
//    fGroupStates: array[1..cZESpdGroupLimit] of TGroupState;
//    fMaConfigAvailable: Boolean;
//    fAtLeastOneGroupInProd: Boolean; //Nue: tmp bis GetMaConfig sauber in AC338 implementiert
//    function GetQuery: TmmADODataSet;
//    procedure SetQuery(aQuery: TmmADODataSet);
//    procedure SetMachineInWork(const Value: Boolean);
//    procedure setActiveSpdGrp(const Value: Integer);
//  public
//    constructor Create(aOwner: TComponent); override;
//    destructor Destroy; override;
//    procedure Assign(aSource: TPersistent); override;
//    function GetGroupState(aGroupID: Integer): TGroupState; //Nue: tmp bis GetMaConfig sauber in AC338 implementiert
//    function SetGroupState(aGroupID: Integer; aGroupState: TGroupState): Boolean; //Nue: tmp bis GetMaConfig sauber in AC338 implementiert
//    property Validity: TMachValidity read fValidity write fValidity;
//    property NetNode: TNetNode read mNetNode write mNetNode;
//    property Query: TmmADODataSet read GetQuery write SetQuery;
//    property ActiveSpdGrp: Integer read fActiveSpdGrp write setActiveSpdGrp;
//    property MachineInWork: Boolean read FMachineInWork write SetMachineInWork default False;
//    property MaConfigAvailable: Boolean read fMaConfigAvailable write fMaConfigAvailable;
//    property AtLeastOneGroupInProd: Boolean read fAtLeastOneGroupInProd write fAtLeastOneGroupInProd; //Nue: tmp bis GetMaConfig sauber in AC338 implementiert
//    //    property MaxSpindIfNoRange : Boolean read fMaxSpindIfNoRange write SetMaxSpindIfNoRange default False;
//  end;
//
//..............................................................................
  TFullMachineList = class(TStringList)
  private
  public
    procedure Clear; override;
  end;
  //...........................................................
  TNetNode = class(TComponent)
  private
    fQuery: TmmADODataSet;
    fNetTyp: TNetTyp;
    fNetName: string;
    fNetManuf: string;
    fNodeId: string;
    fAdaptOrName: string;
    fNodeState: integer;
    fFrontTyp: TFrontType;
    fFrontManuf: string;
    fFrontModel: string;
    fFrontState: Integer;
  protected
  public
    constructor Create(aOwner: TComponent); override;
    procedure Assign(aSource: TPersistent); override;
    procedure SetNode(aNodeID: string);
    property NetTyp: TNetTyp read fNetTyp write fNetTyp;
    property NetName: string read fNetName write fNetName;
    property NetManuf: string read fNetManuf;
    property NodeId: string read fNodeId write fNodeId;
    property AdaptOrName: string read fAdaptOrName write fAdaptOrName;
    property NodeState: Integer read fNodeState write fNodeState;
    property FrontTyp: TFrontType read fFrontTyp write fFrontTyp;
    property FrontManuf: string read fFrontManuf;
    property FrontModel: string read fFrontModel write fFrontModel; //?Typ
    property FrontState: Integer read fFrontState write fFrontState; //?Typ  write Nue:13.2.03
  published
    property Query: TmmADODataSet read fQuery write fQuery;
  end;
  //...........................................................
  TMaConfigFullReader = class(TMaConfigReader)
  //Added by Nue 1.12.04
  private
    fActiveSpdGrp: Integer;
    fAtLeastOneGroupInProd: Boolean;
    FMachine: TMachine;
    fMachineInWorkXX: Boolean;
    fMaConfigAvailableXX: Boolean;
    FNetNode: TNetNode;
    fValidity: TMachValidity;
    function GetQuery: TmmADODataSet;
    procedure setActiveSpdGrp(const Value: Integer);
    procedure SetMachineInWorkXX(const Value: Boolean);
    procedure SetNetNode(const Value: TNetNode);
    procedure SetQuery(aQuery: TmmADODataSet);
    property MachineInWorkXX: Boolean read fMachineInWorkXX write SetMachineInWorkXX default False;
    property MaConfigAvailableXX: Boolean read fMaConfigAvailableXX write fMaConfigAvailableXX;
  protected
    function GetGroupValueDef(aGroupIndex: Integer; aSelPath: string; aDefault: variant): Variant; override;
    function GetMachValueDef(aSelPath: string; aDefault: variant): Variant; override;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Assign(aSource: TObject); virtual;
    property ActiveSpdGrp: Integer read fActiveSpdGrp write setActiveSpdGrp;
    property AtLeastOneGroupInProd: Boolean read fAtLeastOneGroupInProd write
        fAtLeastOneGroupInProd;
    property Machine: TMachine read FMachine write FMachine;
    property NetNode: TNetNode read FNetNode write SetNetNode;
    property Query: TmmADODataSet read GetQuery write SetQuery;
    property Validity: TMachValidity read fValidity write fValidity;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

type
  //...........................................................
  TSetMaConfigHandler = class(TBaseHandler)
  private
    fFullMachine: TMaConfigFullReader;
  protected
    procedure OnMsgReceived(aReceivedMsg: PResponseRec); override;
    procedure OnTimeout(aSender: TObject); override;
    procedure OnWndMsg(msg: TMessage); override;
  public
    constructor Create(aOwner: TComponent); override;
//    procedure Assign(aSource: TPersistent); override;
    property FullMachine: TMaConfigFullReader read fFullMachine write fFullMachine;
  published
  end;

//------------------------------------------------------------------------------
constructor TSetMaConfigHandler.Create(aOwner: TComponent);
begin
  fFullMachine := TMaConfigFullReader.Create;//(aOwner);
  inherited Create(aOwner, cMaConfigPipeName);
end;
//------------------------------------------------------------------------------
procedure TSetMaConfigHandler.OnMsgReceived(aReceivedMsg: PResponseRec);
begin
end;
//------------------------------------------------------------------------------
procedure TSetMaConfigHandler.OnTimeout(aSender: TObject);
begin
end;
//------------------------------------------------------------------------------
procedure TSetMaConfigHandler.OnWndMsg(msg: TMessage);
begin
end;
//------------------------------------------------------------------------------
procedure TFullMachineList.Clear;
var
  i: Integer;
begin
// Diese Liste f�hrt nur Referenzen von Objekten. Daher d�rfen die Inhalte nicht mehr freigegeben werden, nur l�schen
//  for i:=0 to Count-1 do
//    TMaConfigFullReader(Objects[i]).Free;

  inherited Clear;
end;

//------------------------------------------------------------------------------
{ TNetNode }
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

constructor TNetNode.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fNetTyp      := ntNone;
  fNetName     := '';
  fNetManuf    := '';
  fNodeId      := '';
  fAdaptOrName := '';
  fNodeState   := 0;
  fFrontTyp    := ftNone;
  fFrontManuf  := '';
  fFrontModel  := '';
  fFrontState  := 0;
end;

//------------------------------------------------------------------------------
procedure TNetNode.Assign(aSource: TPersistent);
var
  xSrc: TNetNode;
begin
  if aSource is TNetNode then begin
    xSrc := aSource as TNetNode;

    fQuery       := xSrc.Query;
    fNetTyp      := xSrc.NetTyp;
    fNetName     := xSrc.NetName;
    fNetManuf    := xSrc.NetManuf;
    fNodeId      := xSrc.NodeId;
    fAdaptOrName := xSrc.AdaptOrName;
    fNodeState   := xSrc.NodeState;
    fFrontTyp    := xSrc.FrontTyp;
    fFrontManuf  := xSrc.FrontManuf;
    fFrontModel  := xSrc.FrontModel;
    fFrontState  := xSrc.FrontState;
  end
  else
    inherited Assign(aSource);
end;

//------------------------------------------------------------------------------

procedure TNetNode.SetNode(aNodeID: string);
begin
  try
    if not Assigned(fQuery) then
      raise Exception.Create('TNetNode Query not assigned.');
    with fQuery do begin
      Close;
      CommandText := cQueryNodeInfo;
      Parameters.ParamByName('c_node_id').value := aNodeID;
      Open;
      if FindFirst then begin
        fNetTyp      := TNetTyp(FieldByName('c_net_id').AsInteger);
        fNetName     := FieldByName('c_net_name').AsString;
        fNetManuf    := FieldByName('c_net_manuf').AsString;
        fNodeId      := aNodeID;
        fAdaptOrName := FieldByName('c_adapt_or_name').AsString;
        fNodeState   := FieldByName('c_node_stat').AsInteger;
        fFrontTyp    := TFrontType(FieldByName('c_front_type').AsInteger);
        fFrontManuf  := FieldByName('c_front_manuf').AsString;
        fFrontModel  := FieldByName('c_front_model').AsString;
        fFrontState  := FieldByName('c_front_stat').AsInteger;
      end else
        raise Exception.Create('No Node available with NodeID=' + aNodeID)
    end; // wiht fQuery
  except
    on e: Exception do begin
      raise Exception.Create('TNetNode.SetNodeID failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
// TMaConfigFullReader
//------------------------------------------------------------------------------
constructor TMaConfigFullReader.Create;
begin
  inherited Create;
//  mNetNode := TNetNode.Create(aOwner);
  fNetNode := TNetNode.Create(NIL);
  fMachine := TMachine.Create(NIL);
  fValidity := mvFull;
  fMaConfigAvailableXX := False;
  fAtLeastOneGroupInProd := False; //Nue: tmp bis GetMaConfig sauber in AC338 implementiert
//    fActiveSpdGrp := 1; //Nue: Defaulteinstellung seit 04.03.03 (Vorher nicht initiallisiert und daher (meistens) 0 -> Index ausserhalb!!
end;

//------------------------------------------------------------------------------
destructor TMaConfigFullReader.Destroy;
begin
  FreeAndNil(fNetNode);
  inherited Destroy;
end;

//------------------------------------------------------------------------------
procedure TMaConfigFullReader.Assign(aSource: TObject);
var
  xSrc: TMaConfigFullReader;
begin
  if aSource is TMaConfigFullReader then begin
    xSrc := TMaConfigFullReader(aSource);
//    fGroupStates := (aSource as TMaConfigFullReader).fGroupStates; //Nue: tmp bis GetMaConfig sauber in AC338 implementiert
    fAtLeastOneGroupInProd := xSrc.fAtLeastOneGroupInProd; //Nue: tmp bis GetMaConfig sauber in AC338 implementiert
    fValidity              := xSrc.Validity;
    fActiveSpdGrp          := xSrc.fActiveSpdGrp;
    Machine.Assign(xSrc.Machine);
    MachID                 := xSrc.MachID;
//    DOM := xSrc.DOM;
    if fValidity in [mvFull, mvNewMachine, mvDisconnect] then
      fNetNode.Assign(xSrc.NetNode);
//    inherited Assign(aSource);
  end
  else
    fValidity := mvNone;

//  if aSource is TMaConfigFullReader then begin
////    fGroupStates := (aSource as TMaConfigFullReader).fGroupStates; //Nue: tmp bis GetMaConfig sauber in AC338 implementiert
//    fAtLeastOneGroupInProd := (aSource as TMaConfigFullReader).fAtLeastOneGroupInProd; //Nue: tmp bis GetMaConfig sauber in AC338 implementiert
//    fValidity := (aSource as TMaConfigFullReader).Validity;
//    fActiveSpdGrp := (aSource as TMaConfigFullReader).fActiveSpdGrp;
//    Machine.Assign((aSource as TMaConfigFullReader).Machine);
////    xSrc := aSource as TMaConfigFullReader;
//    MachID := (aSource as TMaConfigFullReader).MachID;
////    DOM := xSrc.DOM;
//    if fValidity in [mvFull, mvNewMachine, mvDisconnect] then
//      fNetNode.Assign((aSource as TMaConfigFullReader).NetNode);
////    inherited Assign(aSource);
//  end
//  else
//    fValidity := mvNone;
end;

//------------------------------------------------------------------------------
function TMaConfigFullReader.GetGroupValueDef(aGroupIndex: Integer; aSelPath: string;
  aDefault: variant): Variant;
begin
  Result := False;
//  if DOM<>NIL then
  if Available then
    Result := inherited GetGroupValueDef(aGroupIndex, aSelPath, aDefault);
end;

//------------------------------------------------------------------------------
function TMaConfigFullReader.GetMachValueDef(aSelPath: string; aDefault:
    variant): Variant;
begin
  Result := aDefault; //False;
//  if DOM<>NIL then
  if Available then
    Result := inherited GetMachValueDef(aSelPath, aDefault);
end;

//------------------------------------------------------------------------------
function TMaConfigFullReader.GetQuery: TmmADODataSet;
begin
  Result := fNetNode.Query;
end;

//------------------------------------------------------------------------------
procedure TMaConfigFullReader.setActiveSpdGrp(const Value: Integer);
begin
  fActiveSpdGrp := Value;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
procedure TMaConfigFullReader.SetMachineInWorkXX(const Value: Boolean);
begin
  fMachineInWorkXX := Value;
end;

//------------------------------------------------------------------------------
procedure TMaConfigFullReader.SetNetNode(const Value: TNetNode);
begin
  if FNetNode <> Value then
  begin
    FNetNode := Value;
  end;
end;

//------------------------------------------------------------------------------

procedure TMaConfigFullReader.SetQuery(aQuery: TmmADODataSet);
begin
//  inherited Query := aQuery;
  fNetNode.Query := aQuery;
  fMachine.Query := aQuery;
end;



end.

