{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: ClearerAssistantDef.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Definitionen
| Info..........:
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 05.10.2001  1.00  SDo  | File created
| 08.03.2004  1.00  Wss  | SensingHead aus TSettings entfernt
| 23.04.2004  1.00  Wss  | ResourceString (14)Laufzeit das Sonderzeichen � entfernt
| 06.01.2005  1.01  SDo  | Umbau & Anpassungen an XML
| 31.01.2008  1.02  Roda | Enhancement of sample yarn fault image functionality:
|                        |   a) Default yarn fault images (provided by Loepfe):
|                        |      - added support for fine class and grob class images
|                        |      - changed name scheme of images to alphanumeric (eg. "D_N1_1.jpg" instead of "0.jpg")
|                        |   b) Support for custom yarn fault images:
|                        |      - Show, Import, Delete custom images (options available in popupmenu by right click on corresponding matrix field)
|                        |   c) Several bugfixes
|                        |   d) Design adjustments
|                        |   e) Added functions GetYFCategoryString() and GetYFImageFilePath()
===============================================================================}
unit ClearerAssistantDef;


interface

uses

  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, comctrls, MMUSBSelectNavigatorFrame,
  mmList, DataLayerDef, YMParaDef, MMUGlobal, QualityMatrixDef, QualityMatrix,
  MMQualityMatrixClass, XMLDef ;

resourcestring
  rsNoPrinterAvailable    = '(*)Drucker nicht verfuegbar'; //ivlm
  rsApplCaption           = '(*)Reiniger Assistent';       //ivlm
  rsClearerSettingsCaption= '(*)Reinigereinstellung:';     //ivlm
  rsActualSerie           = '(*)Aktuell';  //ivlm
  rsSerie                 = '(*)Serie %d'; //ivlm
  rsCheckTemplateSettings = '(*)Die Vorlage wurde gespeichert. Ueberpruefen Sie die Einstellungen in der Vorlagenverwaltung!'; //ivlm
  rsHidenFilterText       = '(*)Anzeige ausgeschalten'; //ivlm

  rsFormatDurationDay    = '(*)%d Tg %d Std';   //ivlm
  rsFormatDurationHour   = '(*)%d Std %d Min';  //ivlm
  rsFormatDurationMin    = '(*)%d Min %d Sek';  //ivlm

  rsGrid1 = '(*)Bezeichnung'; //ivlm
  rsGrid2 = '(14)Kanal';      //ivlm
  rsGrid3 = '(14)Spleiss';    //ivlm
  rsGrid4 = '(14)FF';         //ivlm
  rsgrid5 = '(10)Total';      //ivlm
  rsGrid6 = '(14)Nutzeffekt'; //ivlm
  rsGrid7 = '(14)Laufzeit';   //ivlm

  rsNavigate = '(14)Navigieren'; //ivlm
  rsAnalyse  = '(14)Auswerten';  //ivlm

  // popup menu mmMatrix menu items
  rsStrMiShowDefaultYarnFaultImage  = '(*)Zeige Standardbild fuer %s';      //ivlm
  rsStrMiShowCustomYarnFaultImage   = '(*)Zeige eigenes Bild fuer %s';      //ivlm
  rsStrMiImportCustomYarnFaultImage = '(*)Importiere eigenes Bild fuer %s'; //ivlm
  rsStrMiDeleteCustomYarnFaultImage = '(*)Loesche eigenes Bild fuer %s';    //ivlm

  rsStrYarnFaultClass      = '(*)Garnklasse';  //ivlm
  rsStrForeignMatter       = '(*)Fremdfasern';       //ivlm

  rsStrImportImageSuccess  = '(*)Bildimport erfolgreich.';               //ivlm
  rsStrImportImageFailed   = '(*)Bild konnte nicht importiert werden.';  //ivlm
  rsStrNoImageAvailableFor = '(*)Kein Bild vorhanden fuer %s.';          //ivlm
  rsStrSelectJPGFile       = '(*)jpeg - Bilddatei auswaehlen'; //ivlm
  rsStrOnlyJPGSupport      = '(*)Falsches Bildformat (%s). Nur Bilder im jpeg-Format werden unterstuetzt.'; //ivlm

  rsStrCustomImageAlreladyExisting = '(*)Eigenes Bild bereits vorhanden fuer %s.'; //ivlm

  rsStrFileName   = '(30)Dateiname';   //ivlm
  rsStrSrcFile    = '(*)Quelldatei';   //ivlm
  rsStrDestFile   = '(*)Zieldatei';    //ivlm
  rsStrAskProceed = '(*)Fortsetzen?';  //ivlm
  rsStrAskReplaceWith        = '(*)Ersetzen durch %s?'; //ivlm
  rsStrCreateDirectoryFailed = '(*)Verzeichnis %s konnte nicht erstellt werden.'; //ivlm
  rsStrCopyFileFailed        = '(*)Datei konnte nicht kopiert werden.';           //ivlm
  rsStrDeleteFileFailed      = '(*)Datei %s konnte nicht geloescht werden.';      //ivlm

const
  cFloatFormatMask = '%.1f';
  cSelectColor     = $00FFD2D2;

  cSampleInfo: Array[0..2] of String = (rsGrid2, rsGrid3, rsGrid4);

const
  cDesignationWidth     = 100;
  cActualWidth          = 60;
  cSerieWidth           = 60;

const
  cMaxFieldCount: Array[0..2] of Integer = (cClassCutFields, cSpCutFields, cSIROCutFields);
  cMaxSubFieldX = 10;
  cMaxSubFieldY = 10;

type
   TQMColIndex = 0..15;
   TQMColIndexSet = set of TQMColIndex;

  TFieldDefRec = record
    Low: Integer;
    High: Integer;
  end;
  
type
  TTemplateInfo = record
    Name: string;
    SetID: Integer;
    Color: TColor;
  end;
  
  TFGClassTypeEnum    = (tClassFine, tClassGrob);
  TDCImageTypeEnum    = (tImageDefault, tImageCustom); // Default=Loepfe provided image; Custom=image from customer
  TYFCategoryTypeEnum = (tYFDiameter, tYFSplice, tYFForeignMatter); // Yarn Fault Category

  // This structure holds fault class and image file information corresponding to the selected Matrix field by a right mouse click
  // The records are initialized before the popup menu pmMatrix is acitvated
  // This information is used when a popup menu item is selected
  TYarnFaultImageInfoRec = record
    fileExists: Boolean;             // if the image does not exist, corresponding show image and delete image menu items in pmMatrix are disabled
    fileName: string;                // file name with path relative to application, e.g. 'Cuts\Class\D_N1_1.jpg'
    classNumId: Integer;             // 0-127 (numeric class id)
    classAlphaNumId: string;         // e.g. 'N1-1'
    fgClassType: TFGClassTypeEnum;   // fine or grob class
    imageType: TDCImageTypeEnum;     // default or custom image
    yfCategory: TYFCategoryTypeEnum; // Diameter, Splice, Foreign Matter (yarn fault category)
  end;

  TCurveTypeEnum = (ctChannel, ctSplicse);

  TDistributionTypeEnum = (dtColor, dtNum, dtDots);

  TCutRec = record
    CutType: string;
    Hint: string;
  end;
  
//------------------------------------------------------------------------------
// definitions for detail data information to calculate virtual efficiency
//------------------------------------------------------------------------------
const
  cFldDetailData = '(c_spindle_last - c_spindle_first + 1) SpdRange, ' +
                   'sum(c_CN) + sum(c_CS) + sum(c_CL) + sum(c_CT) ChannelCuts, ' +
                   'sum(c_CSp) SpliceCuts, ' +
                   'sum(c_CSiro) FFCuts, ' +
                   'sum(c_CYTot) CYTot, ' +
                   'sum(c_kg) Kg, ' +
                   'sum(c_rtSpd) tRu, ' +
                   'sum(c_otSpd) "tOp", ' +
                   'sum(c_wtSpd) tWa, ' +
                   'sum(c_LStProd) tLSt, ' +
                   'sum(c_Sp) Sp ';

type
  TCAConfigRec = record
    QMChannel: TQualityMatrix;
    QMSplice: TQualityMatrix;
    QMFF: TQualityMatrix;
    DisplayMode: TDisplayMode;
    ZeroLimit: Single;
    FineMode: Boolean;
    ShowChannel: Boolean;
    ShowSplice: Boolean;
    ShowYarnCount: Boolean;
    ShowCluster: Boolean;
    ShowSFI: Boolean;
    ShowFFCluster: Boolean;
    ShowClassData: Boolean;
    ShowVCV: Boolean;   //Nue:14.02.08
    PrintBlackWhite: Boolean;
    ActiveColor: TColor;
    PassiveColor: TColor;
  end;
  
  TDetailDataRec = record
    tRu: Single;
    tOp: Single;
    tWa: Single;
    tLSt: Single;
    t100: Single;
    t100LSt: Single;
    tOpSpd: Single;
    tWaSpd: Single;
    tRuSpd: Single;
    tLStSpd: Single;
    t100Spd: Single;
    t100LStSpd: Single;
    Sp: Single;
    Kg: Single;
    CYTot: Single;
    ChannelCuts: Single;
    SpliceCuts: Single;
    FFCuts: Single;
    TotCuts: Single;
    tSpCycle1: Single;
    tSpCycle2: Single;
    SpBase: Single;
    SpdCount: Integer;
  end;
  
//------------------------------------------------------------------------------

  {:----------------------------------------------------------------------------
   ----------------------------------------------------------------------------}
  TSettings = class(TObject)
  public
    SerieName: string;
    //XMLSettingsInfoRec : TXMLSettingsInfoRec; //SDO 11.01.2005
    XMLData  : string; //Matrix Settings (Kurve & Param etc.)
  end;
  
  TReportInfoRec = record
    Style: string;
    Lot: string;
    Machine: string;
    TimeInfo: TTimeRangeInfo;
    Template: string;
    SelectedSeries: string;
    ProdKg: string;
    YarnCount: Single;
    YarnUnit: TYarnUnit;
  end;
  

var
  Settings  : TSettings;
  gApplicationPath: String;


const
  cThickFieldDef: Array[0..15] of TFieldDefRec = (
    (Low: 1;  High: 8),  (Low: 9;  High: 16),
    (Low: 17; High: 24), (Low: 25; High: 32),
    (Low: 33; High: 40), (Low: 41; High: 48),
    (Low: 49; High: 56), (Low: 57; High: 64),

    (Low: 69; High: 72),   (Low: 77;  High: 80),
    (Low: 85; High: 88),   (Low: 93;  High: 96),
    (Low: 101; High: 104), (Low: 109; High: 112),
    (Low: 117; High: 120), (Low: 125; High: 128)
  );

  cThinFieldDef: Array[8..15] of TFieldDefRec = (
    (Low: 65; High: 68),   (Low: 73;  High: 76),
    (Low: 81; High: 84),   (Low: 89;  High: 92),
    (Low: 97; High: 100),  (Low: 105; High: 108),
    (Low: 113; High: 116), (Low: 121; High: 124)
  );


  cChannelFields: Array[1..18] of TFieldDefRec = (
    // Neps
    (Low: 1;  High: 8),
    // Short
    (Low: 9;  High: 64),
    // Long
    (Low: 69; High: 72),   (Low: 77;  High: 80),
    (Low: 85; High: 88),   (Low: 93;  High: 96),
    (Low: 101; High: 104), (Low: 109; High: 112),
    (Low: 117; High: 120), (Low: 125; High: 128),
    // Thin
    (Low: 65; High: 68),   (Low: 73;  High: 76),
    (Low: 81; High: 84),   (Low: 89;  High: 92),
    (Low: 97; High: 100),  (Low: 105; High: 108),
    (Low: 113; High: 116), (Low: 121; High: 124)
  );


  // PPW 2015/03/06: Added id's of thin fields instead of thick fields
  // PPW 2015/03/19: Added id's of thick fields again, because also necessary for Splice
  cSpliceFields: Array[1..17] of TFieldDefRec = (
     // for splice -> complete neps, short and long fields
    (Low: 1;  High: 64),
     // Thick
    (Low: 69; High: 72),   (Low: 77;  High: 80),
    (Low: 85; High: 88),   (Low: 93;  High: 96),
    (Low: 101; High: 104), (Low: 109; High: 112),
    (Low: 117; High: 120), (Low: 125; High: 128),
    // Thin
    (Low: 65; High: 68),   (Low: 73;  High: 76),
    (Low: 81; High: 84),   (Low: 89;  High: 92),
    (Low: 97; High: 100),  (Low: 105; High: 108),
    (Low: 113; High: 116), (Low: 121; High: 124)
  );

  cFFFields: Array[1..1] of TFieldDefRec = (
    (Low: 1;  High: 64)
  );

const
  cDataCount = 6;
//  cCutType : Array[1..cDataCount] of TCutRec = (
//    (CutType: '(14)Kanal';      Hint: '(*)Kanal Schnitte'),   //ivlm
//    (CutType: '(14)Spleiss';    Hint: '(*)Spleiss Schnitte'), //ivlm
//    (CutType: '(14)FF';         Hint: '(*)FF Schnitte'),      //ivlm
//    (CutType: '(10)Total';      Hint: '(*)Total'),            //ivlm
//    (CutType: '(14)Nutzeffekt'; Hint: '(*)Nutzeffekt'),       //ivlm
//    (CutType: '(14)Laufzeit �'; Hint: '(*)Durchschnittliche Spindellaufzeit')
//  );

//------------------------------------------------------------------------------
// prototype of global procedures/functions
//------------------------------------------------------------------------------
function FormatDuration(aDuration: Single): String;
function IsThickField(aIndex: Integer): Boolean;
function GetYFCategoryString(aYFCategory: TYFCategoryTypeEnum; aShortString: Boolean): String;
function GetYFImageFilePath(aYFCategory: TYFCategoryTypeEnum) : String;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmCS,
  LoepfeGlobal;
{:------------------------------------------------------------------------------
 TSettings}
//------------------------------------------------------------------------------
function FormatDuration(aDuration: Single): String;
var
  xDuration: LongInt;
  xSec, xMin, xHour, xDay: Integer;

  function CalcMod(var aDuration: LongInt; aMode: Integer): Integer;
  begin
    Result    := aDuration mod aMode;
    aDuration := aDuration div aMode;
  end;

begin
  Result := '';
  try
    xDuration := Round(aDuration);
    xSec := CalcMod(xDuration, 60);
    xMin := CalcMod(xDuration, 60);
    xHour := CalcMod(xDuration, 24);
    xDay  := xDuration;

    if      xDay > 0 then  Result := Format(rsFormatDurationDay,  [xDay, xHour])
    else if xHour > 0 then Result := Format(rsFormatDurationHour, [xHour, xMin])
    else                   Result := Format(rsFormatDurationMin,  [xMin, xSec]);
  except
    on e:Exception do
      Result := 'FormatDuration: ' + e.Message;
  end;
end;
//------------------------------------------------------------------------------
function IsThickField(aIndex: Integer): Boolean;
begin
  Result := (aIndex <= 64) or ((aIndex mod 8) > 4)
end;

//------------------------------------------------------------------------------
function GetYFCategoryString(aYFCategory: TYFCategoryTypeEnum; aShortString: Boolean): String;
begin

  // only the short terms are clear, therefore only short version supported by now
  case aYFCategory of
    tYFDiameter:      Result := 'D';
    tYFSplice:        Result := 'Sp';
    tYFForeignMatter: Result := 'F';
  end;

{
  case aYFCategory of

    tYFDiameter:
         begin
           if aShortString then Result := 'D'
           else Result := rsGrid2;
         end;

    tYFSplice:
         begin
           if aShortString then Result := 'Sp'
           else Result := rsGrid3;
         end;

    tYFForeignMatter:
         begin
           if aShortString then Result := 'F'
           else Result := rsStrForeignMatter;
         end;
  end;
}
end;
//------------------------------------------------------------------------------
function GetYFImageFilePath(aYFCategory: TYFCategoryTypeEnum) : String;
begin

  case aYFCategory of
  
    tYFDiameter:      Result := gApplicationPath + 'Cuts\Class\';
    tYFSplice:        Result := gApplicationPath + 'Cuts\Splice\';
    tYFForeignMatter: Result := gApplicationPath + 'Cuts\FF\';
  
  end;

end;

end.

