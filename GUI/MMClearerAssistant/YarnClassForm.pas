(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: DataForm.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows
| Target.system.: Windows
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 01.09.2004  1.00  Wss  | Korrektur f�r SIRO Bilder, da diese nicht einfach durchnummeriert sind
| 22.06.2005  1.00  Wss  | Anpassen an neue Bildgr�sse f�r normale Garnfehler
| 31.01.2008  1.01  Roda | Enhancement of sample yarn fault image functionality:
|                        |   a) Default yarn fault images (provided by Loepfe):
|                        |      - added support for fine class and grob class images
|                        |      - changed name scheme of images to alphanumeric (eg. "D_N1_1.jpg" instead of "0.jpg")
|                        |   b) Support for custom yarn fault images:
|                        |      - Show, Import, Delete custom images (available in popupmenu by right click on correspondingmatrix field)
|                        |   c) Several bugfixes
|                        |   d) Design adjustments
|=========================================================================================*)
unit YarnClassForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, mmImage, JPEG, StdCtrls, mmLabel, IvDictio, IvMulti, BaseForm,
  IvEMulti, mmTranslator, mmToolBar, ComCtrls, ToolWin, Math,
  ClearerAssistantDef;


type
  {:----------------------------------------------------------------------------
   Form to show a sample image of selected field.
   ----------------------------------------------------------------------------}
  TfrmShowSample = class (TmmForm)
    imgYarn: TmmImage;
    mmToolBar: TmmToolBar;
    mmTranslator: TmmTranslator;
    ToolButton1: TToolButton;
    laNoImage: TmmLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ToolButton1Click(Sender: TObject);
    procedure mmTranslatorLanguageChange(Sender: TObject);
  private
    mClassAlphaNumId: String;
    mYFCategory: TYFCategoryTypeEnum;
    mFGClassType: TFGClassTypeEnum; 
    mImageType: TDCImageTypeEnum;
    mShowImportSuccessMsg: Boolean;
    procedure UpdateCaption;
  public
    //procedure ShowImage(aImageName: String; aMode: Integer);
    constructor Create(AOwner: TComponent); override;
    procedure ShowImage(aFileName, aClassAlphaNumId: String; aYFCategory: TYFCategoryTypeEnum; aFGClassType: TFGClassTypeEnum; aImageType: TDCImageTypeEnum; aHCallingWindow: HWND; aShowImportSuccessMsg: Boolean);
  end;

var
  frmShowSample: TfrmShowSample;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,
  MainForm, mmcs, JclStrings;

{:------------------------------------------------------------------------------
 TfrmShowSample}
{:-----------------------------------------------------------------------------}
procedure TfrmShowSample.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmShowSample.UpdateCaption;
var
  xClassString: string;
  xStrYarnFaultClass: string;
begin
  // set first character to upper char with Ansi string methods
  xStrYarnFaultClass := AnsiUpperCase(StrLeft(rsStrYarnFaultClass, 1)) + StrRestOf(rsStrYarnFaultClass, 2);
  xClassString := Format('%s "%s %s"', [xStrYarnFaultClass, GetYFCategoryString(mYFCategory,false), mClassAlphaNumId]);

  if mShowImportSuccessMsg then
    Caption := Format('%s %s', [rsStrImportImageSuccess, xClassString])
  else
    Caption := xClassString;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmShowSample.ShowImage(aFileName, aClassAlphaNumId: String; aYFCategory: TYFCategoryTypeEnum; aFGClassType: TFGClassTypeEnum; aImageType: TDCImageTypeEnum; aHCallingWindow: HWND; aShowImportSuccessMsg: Boolean);
const
  cMinDim = 300;                     // minimum image display size of larger dimension of width and height

var
  xImgSize   , yImgSize   : Integer; // image display size
  xImgWinSize, yImgWinSize: Integer; // image display window size
  xCallingWin, yCallingWin: Integer; // x-size and y-size of calling window (aHCallWindow)
  xPos       , yPos       : Integer; // Calculation of Position of image display window
  xScaling   , yScaling   : Integer; // Calculation of Image scaling factor for displaying image
  xBorder    , yBorder    : Integer; // Bordersize of image display window = Windowsize - Imagesize
  xRectCallingWin, xRectImgWin: TRect; // Rect of calling window and image display Window

begin
  // check parameters
  if (Length(aFileName)=0) or (Length(aClassAlphaNumId)=0) then begin
    raise ERangeError.CreateFmt('TfrmShowSample.ShowImage: Invalid FileName or class id %s %s', [aFileName, aClassAlphaNumId]);
  end;

  mClassAlphaNumId := aClassAlphaNumId;
  mYFCategory  := aYFCategory;
  mFGClassType := aFGClassType;
  mImageType   := aImageType;
  mShowImportSuccessMsg := aShowImportSuccessMsg;

  CodeSite.SendFmtMsg('ShowImage class: %s  file:  %s', [aClassAlphaNumId, aFileName]);
  SetLastError(0);

  UpdateCaption;

  GetWindowRect(aHCallingWindow, xRectCallingWin);
  xCallingWin := xRectCallingWin.Right - xRectCallingWin.Left;
  yCallingWin := xRectCallingWin.Bottom - xRectCallingWin.Top;

  if FileExists(aFileName) then begin
    laNoImage.Visible := False;
    imgYarn.Align := alClient;
    imgYarn.Picture.LoadFromFile(aFileName);
    imgYarn.Visible := True;

    // initialize values for calculating image window size and position
    xImgSize := imgYarn.Picture.Width;
    yImgSize := imgYarn.Picture.Height;

    GetWindowRect(WindowHandle, xRectImgWin);
    xBorder := xRectImgWin.Right - xRectImgWin.Left - ClientWidth;
    yBorder := xRectImgWin.Bottom - xRectImgWin.Top - ClientHeight + mmToolBar.Height;
    if (xBorder<1) then xBorder := 1;
    if (yBorder<1) then yBorder := 1;

    if (xImgSize > 0) and (yImgSize > 0) then begin

      //
      // Calculating image display window size
      // If the image size has to be scaled for displaying, a integer scaling factor is used.
      // The same integer scaling factor is used for scaling width and height.
      //

      // image display size: larger dimension of width (x) and height (y) must be in minimum cMinDim (300 Pixel)
      if (xImgSize < cMinDim) and (yImgSize < cMinDim) then begin
        xScaling := cMinDim div xImgSize + 1;
        yScaling := cMinDim div yImgSize + 1;
        if xScaling < 1 then xScaling := 1;
        if yScaling < 1 then yScaling := 1;
        xImgSize := xImgSize * Min(xScaling,yScaling);
        yImgSize := yImgSize * Min(xScaling,yScaling);
      end;

      // at least one Dimension of the image display window has to be smaller
      // than the corresponding calling window dimension
      if ((xImgSize+xBorder) > xCallingWin) and ((yImgSize+yBorder) > yCallingWin) then begin
        xScaling := xImgSize div Max((xCallingWin-xBorder),cMinDim) + 1;
        yScaling := yImgSize div Max((yCallingWin-yBorder),cMinDim) + 1;
        if xScaling < 1 then xScaling := 1;
        if yScaling < 1 then yScaling := 1;
        xImgSize := xImgSize div Min(xScaling,yScaling);
        yImgSize := yImgSize div Min(xScaling,yScaling);
      end;

      // image display window must be smaller than the desktop
      if ((xImgSize+xBorder) > Screen.DesktopWidth) or ((yImgSize+yBorder) > Screen.DesktopHeight) then begin
       xScaling := xImgSize div Max((Screen.DesktopWidth -xBorder),cMinDim) + 1;
       yScaling := yImgSize div Max((Screen.DesktopHeight-yBorder),cMinDim) + 1;
       if xScaling < 1 then xScaling := 1;
       if yScaling < 1 then yScaling := 1;
       xImgSize := xImgSize div Max(xScaling,yScaling);
       yImgSize := yImgSize div Max(xScaling,yScaling);
      end;

      xImgSize := Max(xImgSize,1);
      yImgSize := Max(yImgSize,1);
      ClientWidth := xImgSize;
      ClientHeight:= yImgSize + mmToolBar.Height;

    end;

  end else begin
    laNoImage.Align := alClient;
    ClientHeight := laNoImage.Height + mmToolBar.Height;
    laNoImage.Visible := True;
  end;

  //
  // Positioning of image display window: centered relative to calling window (aHCallWindow)
  // Delphi window positioning (Position := PoMainFormCenter;) does
  // not ensure that the window is within visible desktop area
  //
  GetWindowRect(WindowHandle, xRectImgWin);
  xImgWinSize := xRectImgWin.Right-xRectImgWin.Left;
  yImgWinSize := xRectImgWin.Bottom-xRectImgWin.Top;
  xPos := xRectCallingWin.Left + ((xCallingWin - xImgWinSize) div 2);
  yPos := xRectCallingWin.Top  + ((yCallingWin - yImgWinSize) div 2);

  // Check that image display window is not outside desktop
  if (xPos > (Screen.DesktopLeft+Screen.DesktopWidth-xImgWinSize)) then xPos := Screen.DesktopLeft+Screen.DesktopWidth-xImgWinSize;
  if (xPos < Screen.DesktopLeft) then xPos := Screen.DesktopLeft;
  if (yPos > (Screen.DesktopTop+Screen.DesktopHeight-yImgWinSize)) then yPos := Screen.DesktopTop+Screen.DesktopHeight-yImgWinSize;
  if (yPos < Screen.DesktopTop)  then yPos := Screen.DesktopTop;

  // Assign calculated position to image display window
  Left := xPos;
  Top  := yPos;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmShowSample.ToolButton1Click(Sender: TObject);
begin
  Close;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmShowSample.mmTranslatorLanguageChange(Sender: TObject);
begin
  UpdateCaption;
end;
{:-----------------------------------------------------------------------------}
constructor TfrmShowSample.Create(AOwner: TComponent);
begin
  inherited;
end;

end.

