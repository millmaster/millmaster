object frmShowSample: TfrmShowSample
  Left = 592
  Top = 481
  Width = 415
  Height = 238
  BorderIcons = [biSystemMenu]
  Caption = '(*)Klassierschnitt'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object imgYarn: TmmImage
    Left = 0
    Top = 123
    Width = 407
    Height = 88
    Align = alBottom
    Center = True
    Stretch = True
    Visible = False
    AutoLabel.LabelPosition = lpLeft
  end
  object laNoImage: TmmLabel
    Left = 0
    Top = 29
    Width = 407
    Height = 84
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = '(*)Fuer diese Klassierung ist kein Bild vorhanden.'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Layout = tlCenter
    Visible = False
    AutoLabel.LabelPosition = lpLeft
  end
  object mmToolBar: TmmToolBar
    Left = 0
    Top = 0
    Width = 407
    Height = 29
    Flat = True
    Images = frmMain.ImageList16x16
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      ImageIndex = 24
      OnClick = ToolButton1Click
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'MainDictionary'
    OnLanguageChange = mmTranslatorLanguageChange
    Left = 192
    Top = 8
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
end
