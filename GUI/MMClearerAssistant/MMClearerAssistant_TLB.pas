unit MMClearerAssistant_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88.1.0.1.0  $
// File generated on 01.07.2003 11:12:55 from Type Library described below.

// ************************************************************************ //
// Type Lib: W:\MillMaster\GUI\MMClearerAssistant\MMClearerAssistant.tlb (1)
// IID\LCID: {63B22BAA-1B12-4ECF-9240-F34B3ADCFF01}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\System32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  MMClearerAssistantMajorVersion = 1;
  MMClearerAssistantMinorVersion = 0;

  LIBID_MMClearerAssistant: TGUID = '{63B22BAA-1B12-4ECF-9240-F34B3ADCFF01}';

  IID_IMMClearerAssistantServer: TGUID = '{B22EB26D-9CB7-4191-BA13-8DA9C8F61E06}';
  CLASS_MMClearerAssistantServer: TGUID = '{FBEA0E88-B2FD-420D-B48B-062BFC7F83B5}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IMMClearerAssistantServer = interface;
  IMMClearerAssistantServerDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  MMClearerAssistantServer = IMMClearerAssistantServer;


// *********************************************************************//
// Interface: IMMClearerAssistantServer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B22EB26D-9CB7-4191-BA13-8DA9C8F61E06}
// *********************************************************************//
  IMMClearerAssistantServer = interface(IDispatch)
    ['{B22EB26D-9CB7-4191-BA13-8DA9C8F61E06}']
  end;

// *********************************************************************//
// DispIntf:  IMMClearerAssistantServerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B22EB26D-9CB7-4191-BA13-8DA9C8F61E06}
// *********************************************************************//
  IMMClearerAssistantServerDisp = dispinterface
    ['{B22EB26D-9CB7-4191-BA13-8DA9C8F61E06}']
  end;

// *********************************************************************//
// The Class CoMMClearerAssistantServer provides a Create and CreateRemote method to          
// create instances of the default interface IMMClearerAssistantServer exposed by              
// the CoClass MMClearerAssistantServer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoMMClearerAssistantServer = class
    class function Create: IMMClearerAssistantServer;
    class function CreateRemote(const MachineName: string): IMMClearerAssistantServer;
  end;

implementation

uses ComObj;

class function CoMMClearerAssistantServer.Create: IMMClearerAssistantServer;
begin
  Result := CreateComObject(CLASS_MMClearerAssistantServer) as IMMClearerAssistantServer;
end;

class function CoMMClearerAssistantServer.CreateRemote(const MachineName: string): IMMClearerAssistantServer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_MMClearerAssistantServer) as IMMClearerAssistantServer;
end;

end.
