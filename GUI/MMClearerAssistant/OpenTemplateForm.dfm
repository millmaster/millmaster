inherited frmOpenTemplate: TfrmOpenTemplate
  Left = 516
  Caption = '(*)Vorlage laden'
  ClientHeight = 290
  ClientWidth = 210
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited bOK: TmmButton
    Left = 10
    Top = 260
    Width = 90
  end
  inherited bCancel: TmmButton
    Left = 110
    Top = 260
    Width = 90
  end
  object vlbTemplates: TDBVisualListBox
    Left = 10
    Top = 12
    Width = 190
    Height = 237
    Enabled = True
    ExtendedSelect = False
    ItemHeight = 13
    Sorted = True
    TabOrder = 2
    Visible = True
    OnDblClick = vlbTemplatesDblClick
    AutoLabel.LabelPosition = lpLeft
    DataField = 'c_YM_Set_name'
    KeyField = 'c_ym_Set_id'
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 72
    Top = 8
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
end
