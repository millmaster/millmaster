(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: DataForm.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows
| Target.system.: Windows
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis. | Reason
|-------------------------------------------------------------------------------------------
| 00.12.2001  1.00  Wss  | First Beta release
| 28.03.2002  1.00  Wss  | Close Befehl wurde vergessen in acClose
| 02.04.2002  1.00  Wss  | Diverse Bugfixes
| 20.10.2002  1.00  Wss  | Bei FF �nderungen wurde keine Berechnung ausgel�st
| 04.11.2002        LOK  | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 01.07.2003  1.00  Wss  | - Bei Dots und Color werden nun die Werte Cut/Uncut zusammengefasst
|                        | - Zeitinfos tRu/tOp/tWa werden bei Schichtdaten nicht mehr durch
|                        |   Anzahl Spindeln gerechnet, da dies schon im MMSystem beim speichern
|                        |   auf eine Spindelzeit normiert wird.
| 13.11.2003  1.01  SDo  | Tempalte SaveDialog erweitert.
|                        | - Bei neuem oder bestehenden Tempalte wird der Artikel zugewiesen
| 20.11.2003  1.01  SDo  | - Button Speichern ist desabled, wenn im MMConfig
|                        |   "Vorlagen nur ab Vorlagenverwaltung moeglich" = True  -> mOnlyTemplateAdministrator
| 27.11.2003        SDo  | - TQualityMatrix durch TMMQualityMatrix (Ableitung) ersetzt
|                        | - Neuer Button zum Ein/Ausblenden von Feldbezeichnung
| 21.01.2004        Nue  | - Komponenten YarnCountEditBox1,FaultClusterEditBox1,SFIEditBox1,FFClusterEditBox1 eingef�gt,
|                        | auf Visible=False gesetzt und der Komponente ymsControl zugewiesen. (Damit beim speichern alle
|                        | Werte �bernommen werden.)
| 03.02.2004  1.02  SDO  | SaveTemplateDLG Error behoben, wenn keine YMSetId gesetzt ist.
|                        | -> Ist keine YMSetId gesetzt, dann neues Template ohne Artikelzuweisung
| 19.02.2004  1.02  Wss  | - Bug in Berechnung behoben
|                        | - Selekton beim Ausdruck korrigiert
| 25.02.2004  1.02  Wss  | - Security wurde z.T. nicht dynamisch in mActionList.OnUpdate gepr�ft
| 25.02.2004  1.02  SDo  | - Zentrierung von Form TfrmShowSample
| 23.04.2004  1.02  Wss  | - ColSizing im Datengrid freigeschalten
|                        | - InfoText von Filtereingabe wird nun auch �bersetzt
| 26.05.2004  1.02  SDo  | Matrix Vales & Fault-Classification k�nnen zusammen angezeigt werden ( acMatrixValueMode() )
| 16.11.2004  1.02  Wss  | Wenn MMEasy dann nur Intervaldaten zulassen -> Buttons freigeben in Create()
| 06.01.2005  1.03  SDo  | Umbau & Anpassungen an XML
| 06.04.2005  1.03  SDo  | Fehler behoben:  - Serie XML-Daten wurden beim Seriewechsel (ab 2. mal) falsch angezeigt
|                        |                  - Accessviolation beim Navibation/Auswerten Button und Serie-Aktuell -> Ausloser Sereiwechsel
| 07.04.2005  1.03  SDo  | Aenderung in Func. mActionListUpdate() Steuerung bNavigate.Enabled = ...
|                        | Func. ReadClassDataAndSettings()
|                        |   - Settings werden neu mCDR.XMLSettings.Items[0]^ einglesen (nur 1. Set)
|                        |   - nicht mehr mittels mCDR.GetXMLSettingsFromID()
| 15.04.2005  1.03  SDo  | Aenderung in Func. acShowSettingsExecute() YarnCount wird nicht mehr in TfrmClearerSettings angezeigt -> Supp.
| 16.06.2005  1.03  Wss  | Unit TemplateForm umbenannt nach OpenTemplateForm wegen Unit Namenskonflikt
| 21.06.2005  1.03  Wss  | SampleForm platziert sich immer in der Linken Oberen Ecke des Datenfensters (BGos)
| 30.09.2005  1.03  Wss  | F-Matrix war nicht sichtbar -> kurzes Resize vom Form l�ste das Problem
| 05.05.2006  1.03  Wss  | F-Matrix war nicht sichtbar -> kurzes Resize vom Form l�ste das Problem
| 31.01.2008  1.04  Roda | Enhancement of sample yarn fault image functionality:
|                        |   a) Default yarn fault images (provided by Loepfe):
|                        |      - added support for fine class and grob class images
|                        |      - changed name scheme of images to alphanumeric (eg. "D_N1_1.jpg" instead of "0.jpg")
|                        |   b) Support for custom yarn fault images:
|                        |      - Show, Import, Delete custom images (options available in popupmenu by right click on corresponding matrix field)
|                        |   c) Several bugfixes
|                        |   d) Design adjustments
| 14.02.2008  1.05   Nue | Einbau VCV
| 23.09.2008  1.06   Nue | Zeile qmSplice.ActiveVisible := False; in CheckEditState geklammert u.m.=> richtige Spiceclass Anzeige.
|=========================================================================================*)
unit DataForm;

interface

uses
  Windows, Messages, SysUtils, FileCtrl, Classes, Graphics, Controls, Forms, Dialogs,
  YMParaEditBox, QualityMatrixBase, QualityMatrix, StdCtrls, mmGroupBox,
  EditBox, ComCtrls, mmPageControl, mmToolBar, Buttons, ToolWin,
  mmSpeedButton, ExtCtrls, mmPanel, mmBevel,
  mmMemo, Grids, mmLabel, mmSplitter,
  ClassDataReader, MMSecurity,
  ImgList, mmImageList, ActnList, mmActionList, BASEFORM,
  YMParaDef, ClearerAssistantDef, VirtualCuts, IvDictio, IvMulti, IvEMulti,
  mmTranslator, Menus, mmPopupMenu, mmLengthUnit, SettingsIniDB,
  mmStringGrid, QualityMatrixDef, MMUSBSelectNavigatorFrame,
  MMQualityMatrixClass, MainDataModul,
  //YMParaDBAccessForGUI,
  XMLSettingsModel, VCLXMLSettingsModel, XMLSettingsAccess, DataLayerDef,
  IvMlDlgs, mmOpenDialog, mmOpenPictureDialog;


  //YMParaDBAccessForGUI;

type
  {:----------------------------------------------------------------------------
   Contains complete implementation for the Clearer Assistant.

   - Doubleclick in matrix shows sample cuts
   - Drag curves and edit values
   - Reset settings to original
   - Open template and compare with actual data
   - Saves new settings as template
   - Print each serie on a seperate page
   ----------------------------------------------------------------------------
   ----------------------------------------------------------------------------}

  TfrmData = class(TmmForm)
    acAddSerie: TAction;
    acClose: TAction;
    acCurveEditMode: TAction;
    acDeleteSerie: TAction;
    acLenBase: TAction;
    acOpenTemplate: TAction;
    acOptions: TAction;
    acPrint: TAction;
    acReset: TAction;
    acResetSettings: TAction;
    acSaveAsTemplate: TAction;
    acSecurity: TAction;
    acInterval: TAction;
    acShowColor: TAction;
    acShowDots: TAction;
    acShowNumbers: TAction;
    acShowSettings: TAction;
    bEditValues: TmmSpeedButton;
    bReset: TmmSpeedButton;
    bResetSettings: TmmSpeedButton;
    bShowYMSettings: TmmSpeedButton;
    laKg: TmmLabel;
    latSpCycle0: TmmLabel;
    laVirtualEff: TmmLabel;
    laVirtualOp0: TmmLabel;
    laVirtualOp1: TmmLabel;
    laVirtualOpSpd0: TmmLabel;
    laVirtualOpSpd1: TmmLabel;
    laVirtualRu0: TmmLabel;
    laVirtualRu1: TmmLabel;
    laVirtualRuSpd0: TmmLabel;
    laVirtualRuSpd1: TmmLabel;
    mActionList: TmmActionList;
    mActionList32: TmmActionList;
    mCDR: TClassDataReader;
    meBaseData: TmmMemo; //17.01.2005 SDO@
    mImages16: TmmImageList;
    mImages32: TmmImageList;
    mmLabel11: TmmLabel;
    mmLabel12: TmmLabel;
    mmLabel26: TmmLabel;
    mmLabel3: TmmLabel;
    mmLabel5: TmmLabel;
    mmLabel7: TmmLabel;
    mmLabel8: TmmLabel;
    mmLabel9: TmmLabel;
    mmLengthUnits: TmmLengthUnits;
    mmPanel3: TmmPanel;
    mmPanel5: TmmPanel;
    mmPanel7: TmmPanel;
    MMSecurityControl: TMMSecurityControl;
    mmSpeedButton1: TmmSpeedButton;
    mmSpeedButton2: TmmSpeedButton;
    mmSplitter: TmmSplitter;
    mmSplitter1: TmmSplitter;
    mmTranslator: TmmTranslator;
    mPanel: TmmPanel;
    mToolBar: TmmToolBar;
    pcMatrix: TmmPageControl;
    pmMatrix: TmmPopupMenu;
    sbColor: TmmSpeedButton;
    sbFSchwarm: TmmSpeedButton;
    sbNum: TmmSpeedButton;
    miShowDefaultYarnFaultImage: TMenuItem;
    miShowCustomYarnFaultImage: TMenuItem;
    miImportCustomYarnFaultImage: TMenuItem;
    miDelCustomYarnFaultImage: TMenuItem;
    miCancel: TMenuItem;
    SettingsIniDB: TSettingsIniDB;
    ToolButton1: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton2: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    tsChannel: TTabSheet;
    tsSiro: TTabSheet;
    tsSplice: TTabSheet;
    tsVirtualData: TTabSheet;
    sgData: TmmStringGrid;
    bNavigate: TmmSpeedButton;
    mSelectNavigator: TMMUSBSelectNavigator;
    ChannelEditBox: TChannelEditBox;
    SpliceEditBox: TSpliceEditBox;
    bInterval: TToolButton;
    bShift: TToolButton;
    ToolButton13: TToolButton;
    acShift: TAction;
    acShowFaultClasses: TAction;
    bShowFaultClasses: TmmSpeedButton;
    ToolButton3: TToolButton;
    ToolButton12: TToolButton;
    acHelp: TAction;
    XMLSettingsModel: TVCLXMLSettingsModel;
    qmChannel: TMMQualityMatrix;
    qmSplice: TMMQualityMatrix;
    qmSiro: TMMQualityMatrix;
    //Abbruch1: TMenuItem; // Roda probably not used
    //N1: TMenuItem;
    dlgImportCustomYarnFaultImage: TmmOpenDialog;
    acShowYarnFaultImage: TAction;
    procedure acAddSerieExecute(Sender: TObject);
    procedure acDeleteSerieExecute(Sender: TObject);
    procedure acLenBaseExecute(Sender: TObject);
    procedure acMatrixValueMode(Sender: TObject);
    procedure acOpenTemplateExecute(Sender: TObject);
    procedure acOptionsExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acQMDataMode(Sender: TObject);
    procedure acResetExecute(Sender: TObject);
    procedure acResetSettingsExecute(Sender: TObject);
    procedure acSaveAsTemplateExecute(Sender: TObject);
    procedure acSecurityExecute(Sender: TObject);
    procedure acTimeSelectionExecute(Sender: TObject);
    procedure acShiftIntervalExecute(Sender: TObject);
    procedure acShowSettingsExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mActionList32Update(Action: TBasicAction; var Handled: Boolean);
    procedure mActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure mmTranslatorLanguageChange(Sender: TObject);
    procedure mSelectNavigatorAfterSelection(Sender: TObject);
    procedure pcMatrixChange(Sender: TObject);
    procedure qmMatrixFieldDblClick(aFieldId: Integer; aButton: TMouseButton; aShift: TShiftState);
    procedure qmMatrixTouch(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ymsControlChange(aOwner: TComponent);  //Model-Event
    procedure sgDataMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure sgDataDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgDataSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgDataMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure bNavigateClick(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acCurveEditModeExecute(Sender: TObject);
    procedure acShowFaultClassesExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acShowYarnFaultImageExecute(Sender: TObject);
    procedure mCDRException(Sender: TObject; aMsg: string);
    procedure miShowDefaultYarnFaultImageClick(Sender: TObject);
    procedure miShowCustomYarnFaultImageClick(Sender: TObject);
    procedure miImportCustomYarnFaultImageClick(Sender: TObject);
    procedure miDelCustomYarnFaultImageClick(Sender: TObject);
    procedure XMLSettingsModelChanged(Sender: TObject);
  private
    mDM: TdmMain;
    mDefaultYFImageInfo: TYarnFaultImageInfoRec;
    mCustomYFImageInfo: TYarnFaultImageInfoRec;

    procedure AddNewSerie(aTitle: string; aWidth: Integer);
    //procedure AddStringGridObj(aCol: Integer);
    procedure CalculateCuts;
    procedure CalculateVirtualCuts;
    procedure CheckEditState;
    procedure ClearSeriesGrid(aUntilCol: Integer);
    procedure DeleteCol(aIndex: Integer);
    procedure GetMinMaxValues(aMode: Integer; var aMin, aMax: Single);
    procedure InitDataGrid;
    procedure OpenSettings;
    procedure ReadClassDataAndSettings;
    procedure UpdateSettingsToMatrix;
    //procedure RestoreYMSettings(aIndex: Integer);
    function ActSerie: TStrings;
    procedure FillMatrixValues;
    procedure SetPropertiesToMatrix;
    procedure FillUpModel( aXMLText :String);

    function  GetSelectedYFCategory(): TYFCategoryTypeEnum;
    function  GetSelectedYFClassNumId(): Integer;
    function  GetYFClassAlphaNumId(aClassNumId: Integer; aYFCategory: TYFCategoryTypeEnum; aFGClassType: TFGClassTypeEnum): String;
    function  GetYFImageFileName(aClassNumId: Integer; aYFCategory: TYFCategoryTypeEnum; aFGClassType: TFGClassTypeEnum; aImageType: TDCImageTypeEnum): String;

  protected
    mAllowEdit: Boolean;
    mCAConfig: TCAConfigRec;
    mCurrentSerie: Integer;
    mDataReady: Boolean;
    mDisplayMode: TDisplayMode;
    mAnalyseMode: Boolean;
    mRepInfo: TReportInfoRec;
    mSelectedMatrixField: Integer;
    mIsValidMatrixField: Boolean;
    mSerieCount: Integer;
    mTemplateName: string;
    mVirtualCuts: TVirtualCuts;
    // mYMSettings: TYMSettingsRec;   //06.01.2005 SDO@
    // mOrgYMSettings: TYMSettingsRec;  //06.01.2005 SDO@
    // mXMLSettingsAccess : TXMLSettingsAccess;
    mSettings: TXMLSettingsInfoRec;
    //mOrgSettings: TXMLSettingsInfoRec;

    mOnlyTemplateAdministrator : Boolean;
    procedure SaveSettings;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  end;

var
  frmData: TfrmData;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
{$R Navigate.res}
uses
  mmMBCS, mmCS, MMHtmlHelp,
  Math, mmStringList, Printers,
  LoepfeGlobal, MMUGlobal,
  PrintSettingsForm,
  YarnClassForm,
  mmDialogs,
  SettingsReader, ClearerSettingsForm, SettingsForm,
  OpenTemplateForm, LabMasterDef,
  SaveTemplateDLG, XMLGlobal, XMLDef,
  BaseGlobal;

const
  crArrowWithCamera = 5;

{:------------------------------------------------------------------------------
 TfrmData}
{:-----------------------------------------------------------------------------}

procedure TfrmData.acAddSerieExecute(Sender: TObject);
begin
  EnterMethod('TfrmData.acAddSerieExecute');

  AddNewSerie(Format(rsSerie, [mSerieCount]), cSerieWidth);
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.acDeleteSerieExecute(Sender: TObject);
begin
  EnterMethod('TfrmData.acDeleteSerieExecute');

  DeleteCol(sgData.Col);
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.acLenBaseExecute(Sender: TObject);
var
  i: Integer;
begin
  mmLengthUnits.LengthUnit := mCDR.Params.LenBase;
  if mmLengthUnits.Execute then begin
    mCDR.Params.LenBase := mmLengthUnits.LengthUnit;
    // if LenBase has changed read class data and settings into mYMSettings
    ReadClassDataAndSettings;
    // now go through all series and update virtual data
    with sgData do begin
      // save selected col
//      xCol := Col;
{}
      for i := 1 to ColCount - 1 do begin
        Col := i;
        CalculateVirtualCuts;
      end;
{}
      // set selected col back
//      Col := xCol;
    end;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmData.acMatrixValueMode(Sender: TObject);
var
  xShow : Boolean;
begin
  EnterMethod('TfrmData.acMatrixValueMode');

  //SDO 26.05.2004
  if mAnalyseMode then begin
    case (Sender as TAction).Tag of
      0: if mCAConfig.FineMode then mDisplayMode := dmValues
                               else mDisplayMode := dmCalculateSCValues;
      1: mDisplayMode := dmColor;
      2: mDisplayMode := dmDots;
    end;


    if not sbFSchwarm.Down and not sbColor.Down and not sbNum.Down then
      mDisplayMode := dmNone;

    UpdateSettingsToMatrix;
    FillMatrixValues;

    xShow := bShowFaultClasses.Down;
    if qmChannel.ShowYarnFaultClass <> xShow then begin
      qmChannel.ShowYarnFaultClass := xShow;
      qmSiro.ShowYarnFaultClass    := xShow;
      //Hinweis:
      //qmSplice.ShowYarnFaultClass  := xShow;   ->  wird in TMMQualityMatrix.PrepareFaultClassification gesetzt
      //                                         ->  Die Splice FlaultClass wird nicht mehr ausgegeben ( festglegt im J. 2003 / 2004 )
    end;
  end; // if mAnalyseMode

{
  if mAnalyseMode then begin
    case (Sender as TAction).Tag of
      0: mDisplayMode := dmValues;
      1: mDisplayMode := dmColor;
      2: mDisplayMode := dmDots;
    else begin
        //Show FaultClassifiaction
        mDisplayMode := dmNone;
        xShow := TRUE;
    end;
    end;


    if not bShowFaultClasses.Down and
       not sbFSchwarm.Down and
       not sbColor.Down and
       not sbNum.Down then begin
           mDisplayMode := dmNone;
           xShow := FALSE;
    end;

    UpdateSettingsToMatrix;
    FillMatrixValues;
  end;

  if qmChannel.ShowYarnFaultClass <> xShow then begin
     qmChannel.ShowYarnFaultClass  := xShow;
     qmChannel.ShowGroupSubFieldNr := xShow;
     //qmSplice.ShowYarnFaultClass := xShow;

     qmSiro.ShowYarnFaultClass  := xShow;
     qmSiro.ShowGroupSubFieldNr := xShow;
  end;
}

end;
{:-----------------------------------------------------------------------------}

procedure TfrmData.acOpenTemplateExecute(Sender: TObject);
var
  xYMSetName: String;
begin
  EnterMethod('TfrmData.acOpenTemplateExecute');

  with TfrmOpenTemplate.Create(Self) do try
    vlbTemplates.DataSource := mDM.dsTemplates;
    if ShowModal = mrOK then begin
      // add a new serie for editing...
      acAddSerie.Execute;
      // ...load new settings...
      //ymsControl.LoadFromDB(TemplateID, xYMSetName); //06.01.2005 SDO@

      //Settings von DB einlesen
      mCDR.GetXMLSettingsFromID(TemplateID, mSettings);

      xYMSetName :=  mSettings.SetName;
      ActSerie.Strings[0] := xYMSetName;
      sgData.ColWidths[sgData.Col] := sgData.canvas.TextWidth(xYMSetName) + 5;

      FillUpModel(mSettings.XMLData);


      // ...and update mYMSettings, save settings to column in sgData and calculates virtual cuts
      CalculateVirtualCuts;
    end;
  finally
    Free;
  end;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.acOptionsExecute(Sender: TObject);
begin
  EnterMethod('TfrmData.acOptionsExecute');

  with TfrmSettings.Create(Self) do
  try
    CAConfig := mCAConfig;
    if ShowModal = mrOk then begin
      mCAConfig := CAConfig;
      SetPropertiesToMatrix;
    end;
  finally
    Free;
  end;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.acPrintExecute(Sender: TObject);
var
  xStrList: TmmStringList;
  xTime, xDate : String;
  //.................................................................
  function GetFilterValues(aSelection: TSourceSelection): String;
  var
    xCount: Integer;
  begin
    xCount := mSelectNavigator.SelCount[aSelection];
    if xCount = 0 then begin
      Result := Format('#%d', [mSelectNavigator.ItemsCount[aSelection]]);
    end else begin
      Result := mSelectNavigator.SelectedName[aSelection];
      // replace CRLF with '; '
      Result := StringReplace(Result, cCRLF, '; ', [rfReplaceAll]);
    end;
  end;
  //....................................................................
begin
  xStrList := TmmStringList.Create;
  with mRepInfo do begin
    Style    := GetFilterValues(ssStyle);
    Lot      := GetFilterValues(ssLot);
    Machine  := GetFilterValues(ssMach);
    Template := GetFilterValues(ssYMSet);
    { TODO : TimeRangeinfo �berarbeiten }
//    TimeInfo := mSelectNavigator.TimeRangeInfo;

    xTime := TimeToStr(mSelectNavigator.TimeFrom);
    xDate := DateToStr(mSelectNavigator.TimeFrom);
    TimeInfo.FromTime := Format('%s  %s',[xDate, xTime]);


    xTime := TimeToStr(mSelectNavigator.TimeTo);
    xDate := DateToStr(mSelectNavigator.TimeTo);
    TimeInfo.ToTime   := Format('%s  %s',[xDate, xTime]);

//    TimeInfo.FromTime := DateTimeToStr( mSelectNavigator.TimeFrom );
//    TimeInfo.ToTime   := DateTimeToStr( mSelectNavigator.TimeTo );
    TimeInfo.Count    := mSelectNavigator.ItemsCount[ssTime];
    TimeInfo.Selected := mSelectNavigator.SelCount[ssTime];
    ProdKg   := laKg.Caption;
    //    Eff0     := laEffM.Caption;


  end;
  xStrList.Free;



  with TfrmPrintSettings.Create(self) do
  try
    ShowFaultClassification := bShowFaultClasses.Down;
    mCAConfig.DisplayMode   := mDisplayMode;
    CAConfig := mCAConfig;
    ReportInfo := mRepInfo;
    // use this grid as source
    Grid := sgData;
    if ShowModal = mrOK then
      mCAConfig := CAConfig;
  finally
    Free;
  end;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.acQMDataMode(Sender: TObject);
var
  xMode: TDisplayMode;
begin
  case (Sender as TAction).Tag of
    1: xMode := dmColor;
    2: xMode := dmDots;
  else
    xMode := dmValues;
  end;
  qmChannel.DisplayMode := xMode;
  qmSplice.DisplayMode  := xMode;
  qmSiro.DisplayMode    := xMode;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.acResetExecute(Sender: TObject);
begin
  EnterMethod('TfrmData.acResetExecute');

  mSelectNavigator.Reset;
  mTemplateName := '';
end;

{:-----------------------------------------------------------------------------}
procedure TfrmData.acResetSettingsExecute(Sender: TObject);
var
  xOrgSettings: TSettings;
begin
  EnterMethod('TfrmData.acResetSettingsExecute');


//ShowMessage('Reset all Matrixes');

//ShowMessage('acResetSettingsExecute');
//06.01.2005 SDO@
//  xOrgSettings := TSettings(sgData.Objects[1, 0]);
//  // at reset settings copy only the specified parameters back to mYMSettings
//  case pcMatrix.ActivePageIndex of
//    0: begin
//        mYMSettings.channel := xOrgSettings.YMSettings.channel;
//        mYMSettings.ClassClear := xOrgSettings.YMSettings.classClear;
//      end;
//    1: mYMSettings.splice := xOrgSettings.YMSettings.splice;
//    2: begin
//        mYMSettings.siroClear := xOrgSettings.YMSettings.siroClear;
//        mYMSettings.fFCluster := xOrgSettings.YMSettings.FFCluster;
//      end;
//  else
//  end;
//  // now distribute current settings
//  ymsControl.PutToScreen(mYMSettings);
//  UpdateSettingsToMatrix;
//  CalculateVirtualCuts;


  xOrgSettings := TSettings(sgData.Objects[1, 0]); // Default Settings (Actual)
  XMLSettingsModel.xmlAsString  := xOrgSettings.XMLData;
 // UpdateSettingsToMatrix;
  CalculateVirtualCuts;
end;

{:-----------------------------------------------------------------------------}

function TfrmData.ActSerie: TStrings;
begin
  EnterMethod('TfrmData.ActSerie');

  Result := sgData.Cols[sgData.Col];
end;

{:-----------------------------------------------------------------------------}
procedure TfrmData.acSaveAsTemplateExecute(Sender: TObject);
var
  xYMSetID, xYMSetName, xStyleName: String;
  xText, xSQL :string;

  xList, xStrList: TmmStringList;
  x, n   : Integer;
  xSetID : Integer;

  xSaveTemplateDLGForm : TSaveTemplateDLGForm;
  // xXMLFPattern : String;
  // xPatternNode: IXMLDOMNode;
  // xTemplate: String;
begin

{
  xPatternNode := XMLSettingsModel.xmlAsDOM.selectSingleNode(cXPFPatternNode);
  if Assigned(xPatternNode) then
    xTemplate := xPatternNode.xml;



 exit;
}

 //-YM-Settings und Artikel-IDs aus Navigator ermitteln
 xSaveTemplateDLGForm := nil; // assign nil because of exception handling
 xList :=  TmmStringList.Create;
 xStrList := TmmStringList.Create;

 xYMSetName := Trim(mSelectNavigator.SettingsName);

 xYMSetID := mSelectNavigator.YMSetID;
 xYMSetID := StringReplace( xYMSetID, '"', '',  [rfReplaceAll]);
 xYMSetID := StringReplace( xYMSetID, '''', '',  [rfReplaceAll]);

 xList.Commatext := mSelectNavigator.StyleID;
 xStyleName:= xList.CommaText;

 //Artikel auslesen
 if xList.Count <=0 then begin
    mSelectNavigator.GetListValues(ssStyle, xStrList);
    for x:= 0 to xStrList.Count-1 do begin
        xText := Trim( xStrList.Strings[x] );
        xText := StringReplace( xText, '"', '',  [rfReplaceAll]);
        xText := StringReplace( xText, '''', '',  [rfReplaceAll]);
        if xText <> '*' then
           xList.Add(xText);
    end;

    if xList.Count > 0 then
       with mDM.dseWork do begin
            Close;
            for n:= 0 to xList.Count-1 do begin
                if  n = 0 then
                    xSQL := Format('select c_style_ID from t_style ' +
                                   'where c_style_name = ''%s'' ',[ xList.Strings[n] ])
                else
                    xSQL := Format('%s or c_style_name = ''%s'' ',[xSQL, xList.Strings[n]]);

            end;
            CommandText := xSQL;
            Open;

            xList.Clear;

            First;
            while not EOF do begin
              xList.Add( FieldByName('c_style_ID').AsString );
              Next;
            end;
       end; //end with

    xStyleName:= xList.CommaText;
 end;


 {
 ShowMessage( 'YM ID = ' +xYMSetID + #10#13 + 'YM name = ' + xYMSetName  +
              #10#13 + 'Artikel = ' + xStyleName);
 }

 try
     //ttNewTemplateWithAssignToStyles
//14.04.2005 SDO@
{
   if xYMSetID <> '' then
     xSaveTemplateDLGForm := TSaveTemplateDLGForm.Create(self, tcClearerAssitant)
   else
     xSaveTemplateDLGForm := TSaveTemplateDLGForm.Create(self, tcClearerAssitant, ttNewTemplateOnly);
}

//   mOnlyTemplateAdministrator

   xSetID := mCDR.XMLSettings.Items[0]^.SetID;
//   xSetID := mSettings.SetID;

   if xYMSetID <> '' then
     xSaveTemplateDLGForm := TSaveTemplateDLGForm.Create(self, tcClearerAssitant)
   else
     xSaveTemplateDLGForm := TSaveTemplateDLGForm.Create(self, tcClearerAssitant, ttNewTemplateOnly);

//06.01.2005 SDO@
//  with xSaveTemplateDLGForm do begin
//    // set properties to dialog
//    SensingHeadClass   := TSensingHeadClass(mYMSettings.available.sensingHead);
//    TemplateName       := xYMSetName;
//
//    //Color              := cDefaultStyleIDColor;
//
//    OverrideAllow      := FALSE;
//    SingleNameOnly     := TRUE;
//
//    IsTemplateSet := FALSE;  // YMSettingID stammt nicht aus einem Template
//
//    StyleIDs  := xStyleName;
//    YMSettingID := xYMSetID;
//
//    if ShowModal = mrOK then begin
//
//      with mYMSettings do begin
//        // get channel parameters and class fields
//        ChannelEditBox.GetPara(Channel);
//        qmChannel.GetFieldStateTable(ClassClear);
//
//        // get splice parameters
//        SpliceEditBox.GetPara(Splice);
//
//        // get siro class fields
//        qmSiro.GetFieldStateTable(SiroClear, FFCluster.SiroClear);
//      end;
//
//      with ymsControl do begin
//
//       if TemplateType <> ttRenameTemplateOnly then begin
//          //Neues Template & YMSetID
//          xSetID := NewTemplate(TemplateName);  //-> Tab. t_ym_settings c_tempalte_set = 1
//
//       end else  xSetID := -1;
//
//       RebuildDBConsistensy(xSetID);
//
//
//      end; //END with
//
//      // rename name of serie to TemplateName
//      ActSerie.Strings[0] := TemplateName;
//
//      //MMShowMessage(rsCheckTemplateSettings, Self);
//
//      xSaveTemplateDLGForm.Free;
//     end; //END if
//  end; //END with


  with xSaveTemplateDLGForm do begin
    // set properties to dialog
    //SensingHeadClass :=  TSensingHeadClass(mSettings.HeadClass);
    //SensingHeadClass   := TSensingHeadClass(mYMSettings.available.sensingHead);
    SensingHeadClass   := mSettings.HeadClass;
    TemplateName       := xYMSetName;

    //Color              := cDefaultStyleIDColor;

    OverrideAllow      := FALSE;
    SingleNameOnly     := TRUE;

    IsTemplateSet := FALSE;  // YMSettingID stammt nicht aus einem Template

    StyleIDs  := xStyleName;
    YMSettingID := xYMSetID;
    

    if ShowModal = mrOK then begin

       if TemplateType <> ttRenameTemplateOnly then begin
          //Neues Template & YMSetID
          //xSetID := NewTemplate(TemplateName);  //-> Tab. t_ym_settings c_tempalte_set = 1


          { TODO 1 -oSDo -cXMLSettings :
- Neues Template  speichern (als XML)
siehe auch TSaveTemplateDLGForm.RebuildDBConsistensy -> SaveTemplateDLG.pas }



          with TXMLSettingsAccess.Create do begin
                    xSetID :=NewTemplateSetting( XMLSettingsModel, TemplateName);
//                NewTemplateSetting(aXMLData, aXMLFPattern: String;  aName: String; aHeadClass: TSensingHeadClass;  aHashcode1, aHashcode2: Integer): Integer;


               //xSetID :=NewTemplateSetting( mSettings.XMLData,  xXMLFPattern, TemplateName,  SensingHeadClass, 0 , 0);
               Free;
          end;
       end else xSetID := -1;


       RebuildDBConsistensy(xSetID);

       // rename name of serie to TemplateName
       ActSerie.Strings[0] := TemplateName;



    end; //END if
  end;//END with

  xSaveTemplateDLGForm.Free;

  except
    on E: Exception do begin
       if Assigned(xSaveTemplateDLGForm) then xSaveTemplateDLGForm.Free;
       CodeSite.SendError('Error in acSaveAsTemplate.Execute: Error msg : ' + e.Message );
    end;
  end;

 xList.Free;
 xStrList.Free;


//------------------------------------------------------------------------------




{
  with TSaveAsTemplate.Create(self, mDM.qryWork) do try
    // set properties to dialog
    TemplateName := mTemplateInfo.Name;
    Color := mTemplateInfo.Color;
    OverrideAllow := True;

    if ShowModal = mrOK then begin
      // get properties from dialog
      if CompareText(mTemplateInfo.Name, TemplateName) = 0 then
        ymsControl.UpdateOnDB(mTemplateInfo.SetID)
      else begin
        mTemplateInfo.Name := TemplateName;
        mTemplateInfo.SetID := ymsControl.NewTemplate(TemplateName);
      end;
      mTemplateInfo.Color := Color;
      // rename name of serie to TemplateName
      with sgData do
        Cells[Col, 0] := TemplateName;

      // Color speichern
      with mTemplateInfo do
        mDM.SaveTemplateColor(SetID, Color);

    end;
  finally
    Free;
  end;
{}
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.acSecurityExecute(Sender: TObject);
begin
  MMSecurityControl.Configure;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.acTimeSelectionExecute(Sender: TObject);
begin
  EnterMethod('TfrmData.acTimeSelectionExecute');

  with mSelectNavigator do begin
    case (Sender as TAction).Tag of
      0: begin
           TimeMode := tmInterval;
         end;
      1: begin
           TimeMode := tmShift;
         end;
//      2: begin
//           TimeMode := tmLongtermWeek;
//         end;
    else ;
    end;
  end;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.acShiftIntervalExecute(Sender: TObject);
begin
//  mSelectNavigator.Reset;
//  if bShiftInterval.Down then
//    mSelectNavigator.TimeMode := tmShift
//  else
//    mSelectNavigator.TimeMode := tmInterval;
end;

{:-----------------------------------------------------------------------------}

function TfrmData.GetSelectedYFCategory(): TYFCategoryTypeEnum;
begin

  case pcMatrix.ActivePageIndex of
    0: Result := tYFDiameter;
    1: Result := tYFSplice;
    2: Result := tYFForeignMatter;
  else
    raise ERangeError.CreateFmt('TfrmShowSample.GetSelectedYFCategory(): Invalid Selection %d', [pcMatrix.ActivePageIndex]);
  end;

end;

{:-----------------------------------------------------------------------------}

function TfrmData.GetSelectedYFClassNumId(): Integer;
begin

  Result := mSelectedMatrixField;
  if ((Result < 0) or (Result > 127)) then
    raise ERangeError.CreateFmt('TfrmShowSample.GetSelectedYFClassNumId(): Invalid Selection %d',[Result]);

end;

{:-----------------------------------------------------------------------------}

function TfrmData.GetYFClassAlphaNumId(aClassNumId: Integer; aYFCategory: TYFCategoryTypeEnum; aFGClassType: TFGClassTypeEnum): String;
begin

  // This version returns the alphanumeric class code (e.g. 'N2-1')
  case aYFCategory of

    tYFDiameter:
        begin
          if aFGClassType = tClassFine then
            Result := Format('%s-%d', [qmChannel.GetFaultClassName(aClassNumId), qmChannel.GetFaultClassFieldNr(aClassNumId)])
          else
            Result := qmChannel.GetFaultClassName(aClassNumId);
        end;

    tYFSplice:
//       begin
//         Result := Format('%d', [aClassNumId]); // only numeric namescheme for splice classes, parameter aFGClassType ignored
//       end;
        //Nue: 23.09.08 N�chster Block analog von  tYFDiameter �bernommen.
        begin
          if aFGClassType = tClassFine then
            Result := Format('%s-%d', [qmSplice.GetFaultClassName(aClassNumId), qmSplice.GetFaultClassFieldNr(aClassNumId)])
          else
            Result := qmSplice.GetFaultClassName(aClassNumId);
        end;

    tYFForeignMatter:
       begin
         if aFGClassType = tClassFine then
           Result := Format('%s-%d', [qmSiro.GetFaultClassName(aClassNumId), qmSiro.GetFaultClassFieldNr(aClassNumId)])
         else
           Result := qmSiro.GetFaultClassName(aClassNumId);
       end;

  end;
{
  // This Version returns both the numeric class code as well as the alphanumeric class code (e.g. 'N2-1 (2)')
  case aYFCategory of

    tYFDiameter:
        begin
          if aFGClassType = tClassFine then
            Result := Format('%s-%d (%d)', [qmChannel.GetFaultClassName(aClassNumId), qmChannel.GetFaultClassFieldNr(aClassNumId), aClassNumId])
          else
            Result := Format('%s (%d)', [qmChannel.GetFaultClassName(aClassNumId), aClassNumId]);
        end;

    tYFSplice:
       begin
         Result := Format('%d', [aClassNumId]); // only numeric namescheme for splice classes, parameter aFGClassType ignored
       end;

    tYFForeignMatter:
       begin
         if aFGClassType = tClassFine then
           Result := Format('%s-%d (%d)', [qmSiro.GetFaultClassName(aClassNumId), qmSiro.GetFaultClassFieldNr(aClassNumId), aClassNumId])
         else
           Result := Format('%s (%d)', [qmSiro.GetFaultClassName(aClassNumId), aClassNumId]);
       end;

  end;
}
end;

{:-----------------------------------------------------------------------------}

//1 Returns the yarn fault image file name with path relative regarding the application
/// <param name="aClassNumId"> (Integer) fine class id (0-127) </param>
/// <param name="aYFCategory"> (TYFCategoryTypeEnum) yarn fault category: tYFDiameter, tYFSplice,tYFForeignMatter </param>
/// <param name="aFGClassType"> (Boolean) tClassFine or tClassGrob </param>
/// <param name="aImageType"> (Boolean) tImageDefault (Loepfe provided image) or tImageCustom (customer image) </param>

function TfrmData.GetYFImageFileName(aClassNumId: Integer; aYFCategory: TYFCategoryTypeEnum; aFGClassType: TFGClassTypeEnum; aImageType: TDCImageTypeEnum): String;

begin

  case aYFCategory of

    tYFDiameter:
         begin
           if aFGClassType = tClassFine then
             Result := Format('D_%s_%d.jpg',[qmChannel.GetFaultClassName(aClassNumId), qmChannel.GetFaultClassFieldNr(aClassNumId)])
           else
             Result := Format('D_%s.jpg',[qmChannel.GetFaultClassName(aClassNumId)]);
         end;

    tYFSplice:
         begin
           Result := Format('Sp_%d.jpg',[aClassNumId]); // only numeric namescheme for splice classes, Parameter aFGClassType ignored
         end;

    tYFForeignMatter:
         begin
           if aFGClassType = tClassFine then
             Result := Format('F_%s_%d.jpg',[qmSiro.GetFaultClassName(aClassNumId), qmSiro.GetFaultClassFieldNr(aClassNumId)])
           else
             Result := Format('F_%s.jpg', [qmSiro.GetFaultClassName(aClassNumId)]);
         end;
  
  end;

  // The image filenames must not contain '-' since this character is not supported by all filesystems
  Result := StringReplace(Result, '-', '_', [rfReplaceAll]);  

  if aImageType = tImageDefault then
    Result := Format('%s%s', [GetYFImageFilePath(aYFCategory), Result])
  else
    Result := Format('%sCustom_%s', [GetYFImageFilePath(aYFCategory), Result]);

end;

{:-----------------------------------------------------------------------------}

// mAction-List method called on double click on matrix field event
procedure TfrmData.acShowYarnFaultImageExecute(Sender: TObject);
var
  xClassNumId: Integer;
  xClassAlphaNumId, xImageFileName: String;
  xYFCategory: TYFCategoryTypeEnum;
  xImageType: TDCImageTypeEnum;
  xFGClassType: TFGClassTypeEnum;

begin
  EnterMethod('TfrmData.acShowYarnFaultImageExecute');


  // 1st priority custom image. Only fine class image support for custom images
  // 2nd priority default fine class image
  // 3rd priority default grob class image
  
  xClassNumId  := GetSelectedYFClassNumId();
  xYFCategory  := GetSelectedYFCategory();

  xImageFileName := GetYFImageFileName(xClassNumId, xYFCategory, tClassFine, tImageCustom);
  if FileExists(xImageFileName) then begin
    xClassAlphaNumId := GetYFClassAlphaNumId(xClassNumId, xYFCategory, tClassFine);
    xImageType   := tImageCustom;
    xFGClassType := tClassFine;
  end else begin
    xImageFileName := GetYFImageFileName(xClassNumId, xYFCategory, tClassFine, tImageDefault);
    if FileExists(xImageFileName) then begin
      xClassAlphaNumId := GetYFClassAlphaNumId(xClassNumId, xYFCategory, tClassFine);
      xImageType   := tImageDefault;
      xFGClassType := tClassFine;
    end else begin
      xImageFileName := GetYFImageFileName(xClassNumId, xYFCategory, tClassGrob, tImageDefault);
      if FileExists(xImageFileName) then begin
        xClassAlphaNumId := GetYFClassAlphaNumId(xClassNumId, xYFCategory, tClassGrob);
        xImageType   := tImageDefault;
        xFGClassType := tClassGrob;
      end else begin
        MessageDlg(Format(rsStrNoImageAvailableFor,[Format('%s "%s %s"' , [rsStrYarnFaultClass, GetYFCategoryString(xYFCategory, false), GetYFClassAlphaNumId(xClassNumId, xYFCategory, tClassFine)])]), mtInformation, [mbOK], 0);
        Exit;
      end;
    end;
  end;

  with TfrmShowSample.Create(Self) do begin
    ShowImage(xImageFileName, xClassAlphaNumId, xYFCategory, xFGClassType, xImageType, WindowHandle, false);
  end;

end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.miShowDefaultYarnFaultImageClick(Sender: TObject);
begin
  inherited;

  with TfrmShowSample.Create(Self) do begin
    ShowImage(mDefaultYFImageInfo.fileName, mDefaultYFImageInfo.classAlphaNumId, mDefaultYFImageInfo.yfCategory, mDefaultYFImageInfo.fgClassType, mDefaultYFImageInfo.imageType, WindowHandle, false);
  end;

end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.miShowCustomYarnFaultImageClick(Sender: TObject);
begin
  inherited;

  with TfrmShowSample.Create(Self) do begin
    ShowImage(mCustomYFImageInfo.fileName, mCustomYFImageInfo.classAlphaNumId, mCustomYFImageInfo.yfCategory, mCustomYFImageInfo.fgClassType, mCustomYFImageInfo.imageType, WindowHandle, false);
  end;

end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.miImportCustomYarnFaultImageClick(Sender: TObject);
var
  xStrDestDir:  String; // destination directory
  xClassString: String; // e.g. 'Garnfehlerklasse D N1-1'
  xFileExt:     String; // file extension of selected file

begin
  inherited;

  xClassString := Format('%s "%s %s"',[rsStrYarnFaultClass, GetYFCategoryString(mCustomYFImageInfo.yfCategory, true ), mCustomYFImageInfo.classAlphaNumId]); 

  dlgImportCustomYarnFaultImage.Title := Format(rsStrMiImportCustomYarnFaultImage, [xClassString]) + '. ' + rsStrSelectJPGFile;

  if dlgImportCustomYarnFaultImage.Execute then
  begin

    // Check if custom image file is jpg-format
    xFileExt := LowerCase(ExtractFileExt(dlgImportCustomYarnFaultImage.FileName));
    if (xFileExt <> '.jpg') and (xFileExt <> '.jpeg') then begin
      MessageDlg(Format('%s'#13#10'%s', [Format(rsStrOnlyJPGSupport, [xFileExt]),rsStrImportImageFailed]), mtError, [mbOK], 0);
      Exit;
    end;

    // Check if custom image file already exists and show warning message if this is the case
    if mCustomYFImageInfo.fileExists then begin
      if MessageDlg(
         Format(rsStrCustomImageAlreladyExisting, [xClassString]) + ''#13#10'' + Format(rsStrAskReplaceWith,[dlgImportCustomYarnFaultImage.FileName]),
         mtWarning, [mbOK, mbAbort], 0) <> mrOK then Exit;
    end;

    // check if destination directory exists and create if required
    xStrDestDir := ExtractFilePath(mCustomYFImageInfo.fileName);
    if not DirectoryExists(xStrDestDir) then begin
      if not ForceDirectories(xStrDestDir) then begin
        MessageDlg(Format('%s'#13#10'%s',[Format(rsStrCreateDirectoryFailed, [xStrDestDir]), rsStrImportImageFailed]), mtError, [mbOK], 0);
        Exit;
      end;
    end;

    // copy the file
    if not CopyFile(PChar(dlgImportCustomYarnFaultImage.FileName), PChar(mCustomYFImageInfo.fileName), False) then begin
      MessageDlg(Format('%s'#13#10'%s: %s'#13#10'%s: %s'#13#10'%s', [rsStrCopyFileFailed, rsStrSrcFile, dlgImportCustomYarnFaultImage.FileName,
             rsStrDestFile, mCustomYFImageInfo.fileName, rsStrImportImageFailed]), mtError, [mbOK], 0);
      Exit;
    end;

    // show the imported file
    with TfrmShowSample.Create(Self) do begin
      try
        ShowImage(mCustomYFImageInfo.fileName, mCustomYFImageInfo.classAlphaNumId, mCustomYFImageInfo.yfCategory, mCustomYFImageInfo.fgClassType, mCustomYFImageInfo.imageType, WindowHandle, true);
      except
        on E: Exception do begin
          // display image failed, e.g. because of invalid .jpg - file
          Destroy();
          MessageDlg(Format('%s'#13#10'%s',[E.Message, rsStrImportImageFailed]), mtError, [mbOK], 0);
          DeleteFile(mCustomYFImageInfo.fileName);
        end;
      end;
    end;
  end;

end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.miDelCustomYarnFaultImageClick(Sender: TObject);
var
 xClassString: String;
begin
  inherited;

  xClassString := Format('%s "%s %s"', [rsStrYarnFaultClass, GetYFCategoryString(mCustomYFImageInfo.yfCategory, false), mCustomYFImageInfo.classAlphaNumId]);

  if MessageDlg(Format('%s'#13#10'%s: %s'#13#10'%s', [Format(rsStrMiDeleteCustomYarnFaultImage,[xClassString]), rsStrFileName, mCustomYFImageInfo.fileName, rsStrAskProceed]), mtWarning, [mbOK, mbAbort],0) <> mrOK then begin
    Exit;
  end;

  if not DeleteFile(PChar(mCustomYFImageInfo.fileName)) then begin
    MessageDlg(Format(rsStrDeleteFileFailed,[mCustomYFImageInfo.fileName] ), mtError, [mbOK],0);
  end;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.acShowSettingsExecute(Sender: TObject);
begin

  with sgData do begin
    if Col > 0 then
      with TfrmClearerSettings.Create(Self) do try
//        ShowSetting(TSettings(Objects[Col, 0]), mRepInfo.YarnCount, mRepInfo.YarnUnit, mSettings);
        ShowSetting(TSettings(Objects[Col, 0]), mSettings);

        with  fraRepSettings do begin
          mQMatrix.ActiveColor   :=  mCAConfig.QMChannel.ActiveColor;
          mQMatrix.InactiveColor :=  mCAConfig.QMChannel.InactiveColor;

          //Block wegen Spleissklassierung eingebaut Nue:23.09.08
          mSpliceQMatrix.ActiveColor   :=  clSpliceQualityCutOn;  //Nue:23.09.08
          mSpliceQMatrix.InactiveColor :=  mCAConfig.QMChannel.InactiveColor;

          mFMatrix.ActiveColor   :=  mCAConfig.QMFF.ActiveColor;
          mFMatrix.InactiveColor :=  mCAConfig.QMFF.InactiveColor;

          mYarnCount.YarnCount := 0;  //Explizit auf 0 setzen, damit YarnCount nicht angezeigt wird -> Supp.
        end;
        ShowModal;
      finally
        Free;
      end;
  end;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.AddNewSerie(aTitle: string; aWidth: Integer);
var
  xSettings: TSettings;
begin
  EnterMethod('TfrmData.AddNewSerie');

  inc(mSerieCount);
  with sgData do begin
    ColCount := ColCount + 1;
    ColWidths[ColCount - 1] := aWidth;
    Cells[ColCount-1, 0]    := aTitle;
    // now assign current settings to this serie
    xSettings := TSettings.Create;
    // copy YMSettings from original
//    xSettings.YMSettings := TSettings(Objects[1, 0]).YMSettings;  //06.01.2005 SDO@

    xSettings.SerieName := aTitle;
    xSettings.XMLData := TSettings(Objects[1, 0]).XMLData;
    Objects[ColCount-1, 0] := xSettings;
    mSettings.XMLData := xSettings.XMLData;

//    Rows[0].AddObject(aTitle, xSettings);

    if FixedCols = 1 then
      FixedCols := 2;

    // now activate new column
    Col := ColCount - 1;

    UpdateSettingsToMatrix;
    CalculateCuts;
    Invalidate;
  end;

end;

{:-----------------------------------------------------------------------------}

//procedure TfrmData.AddStringGridObj(aCol: Integer);
//var
//  xSettings: TSettings;
//begin
//  xSettings := TSettings.Create;
////  sgData.Objects[aCol, 0] := xSettings;
//end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.CalculateCuts;
const
  cFloatFormat = '%f';
var
  xVOp1: Single;
  xVRu1: Single;
  xtOutProd: Single;
  //.............................................................
  function CalcPercentageStr(aSingle1, aSingle2: Single): string;
  var
    xRes: Single;
  begin
    xRes := 0;
    if aSingle2 > 0 then begin
      xRes := aSingle1 / aSingle2 * 100;
      if xRes < 0 then
        xRes := 0;
    end;
    Result := Format('%.1f %%', [xRes]);
  end;
  //.............................................................
begin
  EnterMethod('TfrmData.CalculateCuts');

  if mDataReady then begin
    with mVirtualCuts do begin
      DoVirtualCleaning(sgData.Col = 1);

      mVirtualCuts.DebugVirtualCuts;
      
      with sgData, mCDR do begin
        Cells[Col, 1] := Format(cFloatFormatMask, [CalcValue(Params.LenBase, ChannelCuts, Len)]);  // Kanal     -> mCDR
        Cells[Col, 2] := Format(cFloatFormatMask, [CalcValue(Params.LenBase, SpliceCuts, Len)]);   // Spleiss
        Cells[Col, 3] := Format(cFloatFormatMask, [CalcValue(Params.LenBase, FFCuts, Len)]);       // F
        // now set the summary field
        Cells[Col, 4] := Format(cFloatFormatMask, [CalcValue(Params.LenBase, CTot, Len)]);         // Total


        // calculate virtual run time
        with DetailData do begin
          // calculate runtime with longstop in SpCycle: base is the complete time
          xtOutProd := (SpBase * tSpCycle1) + (CTot * tSpCycle1);
          xVOp1 := t100 + (xtOutProd / 2);
          xVRu1 := t100 - (xtOutProd / 2);
          // write debug info
          laVirtualOp0.Caption := Format(cFloatFormatMask, [xVOp1 / 60]);
          laVirtualOp1.Caption := FormatDuration(xVOp1);
          laVirtualRu0.Caption := Format(cFloatFormatMask, [xVRu1 / 60]);
          laVirtualRu1.Caption := FormatDuration(xVRu1);
          laVirtualEff.Caption := CalcPercentageStr(xVRu1, xVOp1);
          // write debug info



          // now re-calculate this time to the spindle time
          if t100 > 0 then begin
            xVOp1 := t100Spd / t100 * xVOp1;
            xVRu1 := t100Spd / t100 * xVRu1;
          end;
          // write debug info
          laVirtualOpSpd0.Caption := Format(cFloatFormatMask, [xVOp1 / 60]);
          laVirtualOpSpd1.Caption := FormatDuration(xVOp1);
          laVirtualRuSpd0.Caption := Format(cFloatFormatMask, [xVRu1 / 60]);
          laVirtualRuSpd1.Caption := FormatDuration(xVRu1);
          // write debug info

          // and calculate the efficiency
          Cells[Col, 5] := CalcPercentageStr(xVRu1, xVOp1);  //Nutzeffekt
          Cells[Col, 6] := FormatDuration(xVOp1);            //Laufzeit
        end;
      end;
    end; // with mVirtualCuts
  end;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.CalculateVirtualCuts;
var
  Test: TStringList;
begin
  EnterMethod('TfrmData.CalculateVirtualCuts');

//06.01.2005 SDO@
//  // collect settings from components and save into mYMSettings
//  with mYMSettings do begin
//    // get channel parameters and class fields
//    ChannelEditBox.GetPara(Channel);
//    qmChannel.GetFieldStateTable(ClassClear);
//    // get splice parameters
//    SpliceEditBox.GetPara(Splice);
//    // get siro class fields
//    qmSiro.GetFieldStateTable(SiroClear, FFCluster.SiroClear);
//  end;
  // update current serie with new settings...
//{}
//  with sgData do begin
//    with TSettings(Objects[Col, 0]) do
//      //YMSettings := mYMSettings;
//      beep;
//  end;
//
//{}
//  // ...and calculate virtual cuts with these settings

  //XML Model-Daten in XMLSettingsInfoRec schreiben
  mSettings.XMLData := XMLSettingsModel.xmlAsString;

  with sgData do begin
      with TSettings(Objects[Col, 0]) do
           XMLData := mSettings.XMLData;   //XML-Daten der Serie zuweisen
  end;

   Test := TStringList.Create();
   try
     Test.Text := mSettings.XMLData;
     Test.SaveToFile('C:\\Log\\XMLSettingsModel.xml');
   finally
     Test.Free;
   end;

  CalculateCuts;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.CheckEditState;
begin
  mAllowEdit := (sgData.Col >= 2) and mDataReady;

  ChannelEditBox.ReadOnly := not(mAllowEdit and bEditValues.Down);
  SpliceEditBox.ReadOnly  := not(mAllowEdit and bEditValues.Down);

  if mAllowEdit and bEditValues.Down then begin
    qmChannel.MatrixMode := mmSelectSettings;
  end else begin
    qmChannel.MatrixMode := mmDisplayData;
  end;

  qmSplice.MatrixMode := qmChannel.MatrixMode;
  qmSiro.MatrixMode := qmChannel.MatrixMode;
  // prevents drawing of active fields in splice matrix when qmSplice is active after mouse move
//Die folgende Zeile nach Einf�gen Spleissklassierung geklammert. Nue:23.09.08
//  qmSplice.ActiveVisible := False;

{
  mAllowEdit := (sgData.Col >= 2) and mDataReady;

  ChannelEditBox.Enabled := mAllowEdit and bEditValues.Down;
  SpliceEditBox.Enabled := mAllowEdit and bEditValues.Down;

  if mAllowEdit and bEditValues.Down then begin
    qmChannel.MatrixMode := mmSelectSettings;
  end else begin
    qmChannel.MatrixMode := mmDisplayData;
  end;

  qmSplice.MatrixMode := qmChannel.MatrixMode;
  qmSiro.MatrixMode := qmChannel.MatrixMode;
  // prevents drawing of active fields in splice matrix when qmSplice is active after mouse move
  qmSplice.ActiveVisible := False;
{}
end;
{:-----------------------------------------------------------------------------}
procedure TfrmData.ClearSeriesGrid(aUntilCol: Integer);
begin
  EnterMethod('TfrmData.ClearSeriesGrid');

  mSerieCount := 1;
  with sgData do begin
    // delete until all editing series are deleted
    while ColCount > aUntilCol do begin
      DeleteCol(aUntilCol);
{
      Objects[aUntilCol, 0].Free;
      DeleteColumn(aUntilCol);
{}
    end;
  end;
{
  with sgData do begin
    // delete until all editing series are deleted
    while ColCount > aUntilCol do
      DeleteCol(aUntilCol);
  end;
{}
end;

{:-----------------------------------------------------------------------------}

constructor TfrmData.Create(aOwner: TComponent);
var
  xVar: Variant;
begin
  inherited Create(aOwner);

  Screen.Cursors[crArrowWithCamera] := LoadCursor(HInstance, 'CAMERA');

  HelpContext := GetHelpContext('CLA\CLA_Fenster_Reiniger_Assistent.htm');
  mmLengthUnits.HelpContext := 161;

  tsVirtualData.TabVisible := CodeSite.Enabled;
  gApplicationPath := ExtractFilePath(Application.ExeName);
  with bNavigate do begin
    Caption := rsAnalyse;
    Glyph.LoadFromResourceName(HInstance, 'ANALYSE');
  end;

  qmChannel.SubFieldX := cMaxSubFieldX;
  qmChannel.SubFieldY := cMaxSubFieldY;
  ChannelEditBox.Top  := 0;
  ChannelEditBox.Left := 0;

  qmSplice.SubFieldX := cMaxSubFieldX;
  qmSplice.SubFieldY := cMaxSubFieldY;
  SpliceEditBox.Top  := 0;
  SpliceEditBox.Left := 0;

  qmSiro.SubFieldX := cMaxSubFieldX;
  qmSiro.SubFieldY := cMaxSubFieldY;

  mDM := TdmMain.Create(Self);
  mDM.CDR := mCDR;

  //FillChar(mYMSettings, sizeof(mYMSettings), 0);  //06.01.2005 SDO@
  FillChar(mSettings, sizeof(mSettings), 0);  //06.01.2005 SDO@
  //ShowMessage( 'TfrmData.Create');


  mVirtualCuts := TVirtualCuts.Create;
  mVirtualCuts.ChannelMatrix := qmChannel;
  mVirtualCuts.SpliceMatrix  := qmSplice;
  mVirtualCuts.FFMatrix      := qmSiro;

  // Init stringgrid with strings
  InitDataGrid;

  mAllowEdit             := False;
  mDataReady             := False;
  mDisplayMode           := dmValues;
  mAnalyseMode           := False;
  mSelectedMatrixField   := -1;
  mIsValidMatrixField    := False;
  mSerieCount            := 1;
  mTemplateName          := '';

  mCAConfig.QMChannel := qmChannel;
  mCAConfig.QMSplice  := qmSplice;
  mCAConfig.QMFF      := qmSiro;

  mOnlyTemplateAdministrator := FALSE;

  with TMMSettingsReader.Instance do begin
    xVar := StringReplace(Value[cQMatrixZeroLimit], '.', DecimalSeparator, [rfReplaceAll]);
    mCAConfig.ZeroLimit := xVar;

    mOnlyTemplateAdministrator := Value[cOnlyTemplateSetsAvailable];
  if IsPackageEasy then begin
  // GL Test 
  //    ToolButton12.Free;
  //    bInterval.Free;
  //    bShift.Free;
      mSelectNavigator.TimeMode := tmInterval;
  end;
  end;

  OpenSettings;

  mSelectNavigator.Init;
  // Set mouse cursor to self defined one crMouseWithCamera
  acCurveEditModeExecute(Nil);

  mDefaultYFImageInfo.fileExists := False;
  mCustomYFImageInfo.fileExists := False;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.DeleteCol(aIndex: Integer);
begin
  EnterMethod('TfrmData.DeleteCol');

  with sgData do begin
    if (aIndex > 0) and (aIndex < ColCount) then begin
      if Assigned(Objects[aIndex, 0]) then
        Objects[aIndex, 0].Free;
      DeleteColumn(aIndex);
      if aIndex > ColCount then
        Col := ColCount - 1;
    end;
    Invalidate;
  end;
{
  with sgData do begin
    if (aIndex > 0) and (aIndex < ColCount) then begin
      if Assigned(Rows[0].Objects[aIndex]) then
        Rows[0].Objects[aIndex].Free;

      RemoveCols(aIndex, 1);
    end;
  end;
{}
end;

{:-----------------------------------------------------------------------------}

destructor TfrmData.Destroy;
begin
  SaveSettings;
  ClearSeriesGrid(1);

  mVirtualCuts.Free;

  //  CleanQRP_Ruins;
  inherited Destroy;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.GetMinMaxValues(aMode: Integer; var aMin, aMax: Single);
var
  i: Integer;
  xSingle: Single;
begin
  // get first value of class data
  case aMode of
    1: aMin := mCDR.SpliceUncut[0] + mCDR.SpliceCut[0];
    2: aMin := mCDR.SiroUncut[0]   + mCDR.SiroCut[0];
  else
    aMin := mCDR.ClassUncut[0]  + mCDR.ClassCut[0];
  end;
  aMax := aMin;

  for i:=1 to cMaxFieldCount[aMode]-1 do begin
    case aMode of
      1: xSingle := mCDR.SpliceUncut[i] + mCDR.SpliceCut[i];
      2: xSingle := mCDR.SiroUncut[i]   + mCDR.SiroCut[i];
    else
      xSingle := mCDR.ClassUncut[i]  + mCDR.ClassCut[i];
    end;
    aMax := Max(xSingle, aMax);
    if xSingle > 0.0 then
      aMin := Min(xSingle, aMin);
  end;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.InitDataGrid;
begin
  EnterMethod('TfrmData.InitDataGrid');

  // cleanup data grid until fixed col
  ClearSeriesGrid(1);
  mCurrentSerie := 1;
  // init stringgrid with strings
  with sgData do begin
    // define properties to have the second col ready for use
    ColCount := 2;
    FixedCols := 1;
    // set title strings in fixed col (first row)
    RowCount := cDataCount + FixedRows;
    {
      Cols[0].Text := rsCutTitle;
      for i:=1 to cDataCount do
        Cols[0].Add(Translate(cCutType[i].CutType));
    {}

    Cols[1].Text := rsActualSerie;
    // create object for YMSettings of actual serie (original)
    Rows[0].Objects[1] := TSettings.Create;
    TSettings(Rows[0].Objects[1]).SerieName := rsActualSerie;
    // set fixed col with different width
    ColWidths[0] := cDesignationWidth;
    ColWidths[1] := cSerieWidth;
  end;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.mActionList32Update(Action: TBasicAction; var Handled: Boolean);
begin
  Handled := True;

  CheckEditState;

  acShowNumbers.Enabled := mDataReady;
  acShowColor.Enabled := mDataReady;
  acShowDots.Enabled := mDataReady;
  acShowFaultClasses.Enabled := mDataReady;

  acCurveEditMode.Enabled := mAllowEdit;
  acResetSettings.Enabled := mAllowEdit and bEditValues.Down;
  acShowSettings.Enabled := mDataReady; //(sgData.ColCount >= 2);

  acAddSerie.Enabled := mDataReady;
  acDeleteSerie.Enabled := mAllowEdit;

  ChannelEditBox.Visible := (pcMatrix.ActivePageIndex = 0) and mDataReady;
  SpliceEditBox.Visible  := (pcMatrix.ActivePageIndex = 1) and mDataReady;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmData.mActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  Handled := True;

  acInterval.Enabled := not mAnalyseMode;
  acShift.Enabled    := not mAnalyseMode;

  acLenBase.Enabled      := mDataReady and MMSecurityControl.CanEnabled(acLenBase);
  acPrint.Enabled        := mDataReady and (Printer.Printers.Count > 0) and MMSecurityControl.CanEnabled(acPrint);
  acReset.Enabled    := not mAnalyseMode;
  acOpenTemplate.Enabled := mDataReady and MMSecurityControl.CanEnabled(acOpenTemplate);
  acSaveAsTemplate.Enabled:= bNavigate.Enabled and (sgData.Col > 1) and MMSecurityControl.CanEnabled(acSaveAsTemplate);

{
  bNavigate.Enabled := mAnalyseMode OR
                       ( (not mAnalyseMode) and
                         ((mSelectNavigator.YMSetID <> '') and
                          (mSelectNavigator.lbYMSettings.RowCount > 1)) );
}


   bNavigate.Enabled :=   mAnalyseMode OR
                      (  not mAnalyseMode and
                      ( ( mSelectNavigator.YMSetID <> '' ) or
                        ( mSelectNavigator.lbYMSettings.RowCount = 2) ) ); // 2 = '*' + 1 Eintrag

end;

{:-----------------------------------------------------------------------------}
procedure TfrmData.mmTranslatorLanguageChange(Sender: TObject);
begin
  mDataReady := False;
  with sgData do begin
    ColWidths[0] := 80;
    Cols[0].Text := rsGrid1;
    Cols[0].Add(rsGrid2);
    Cols[0].Add(rsGrid3);
    Cols[0].Add(rsGrid4);
    Cols[0].Add(rsGrid5);
    Cols[0].Add(rsGrid6);
    Cols[0].Add(rsGrid7);
    Cells[1,0] := rsActualSerie;
  end;

//  mSelectNavigator.ChangeTimeTitleText;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.mSelectNavigatorAfterSelection(Sender: TObject);
begin
  mDataReady := False; 
end;
{:-----------------------------------------------------------------------------}

procedure TfrmData.OpenSettings;
begin
  SettingsIniDB.LoadDefault;
  with mCAConfig, SettingsIniDB.Ini do begin
    ShowChannel     := ReadBool('Options', 'ShowChannel', True);
    ShowSplice      := ReadBool('Options', 'ShowSplice', True);
    ShowYarnCount   := ReadBool('Options', 'ShowYarnCount', True);
    ShowCluster     := ReadBool('Options', 'ShowCluster', True);
    ShowSFI         := ReadBool('Options', 'ShowSFI', True);
    ShowFFCluster   := ReadBool('Options', 'ShowFFCluster', True);
    ShowVCV         := ReadBool('Options', 'ShowVCV', True);    //Nue:14.02.08
    ShowClassData   := ReadBool('Options', 'ShowClassData', True);
    PrintBlackWhite := ReadBool('Options', 'PrintBlackWhite', False);
    FineMode        := ReadBool('Options', 'FineMode', True);
    ZeroLimit       := MMStrToFloat(ReadString('Options', 'ZeroLimit', '0.1'));
//    ZeroLimit     := ReadFloat('Options', 'ZeroLimit', ZeroLimit);
    ActiveColor     := ReadInteger('Options', 'ActiveColor', qmChannel.ActiveColor);
    PassiveColor    := ReadInteger('Options', 'PassiveColor', qmChannel.InactiveColor);
  end;
  SetPropertiesToMatrix;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.pcMatrixChange(Sender: TObject);
begin
//  ChannelEditBox.Visible := (pcMatrix.ActivePageIndex = 0);
//  SpliceEditBox.Visible  := (pcMatrix.ActivePageIndex = 1);

//06.01.2005 SDO@
//ShowMessage( 'pcMatrixChange' );


//  if pcMatrix.ActivePageIndex < 3 then begin
//    case pcMatrix.ActivePageIndex of
//      // tsChannel
//      0: begin
//          ymsControl.ClassMatrixEditBox := qmChannel;
//        end;
//      // tsSplice
//      1: begin
//          ymsControl.ClassMatrixEditBox := qmSplice;
//        end;
//      // tsSiro
//      2: begin
////          ymsControl.ClassMatrixEditBox := qmSiro; //nil;
//        end;
//    else
//    end;
//    UpdateSettingsToMatrix;
//  end;


end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.qmMatrixFieldDblClick(aFieldId: Integer; aButton: TMouseButton; aShift: TShiftState);
begin
  if (aButton = mbLeft) and not bEditValues.Down then begin
    mSelectedMatrixField := aFieldId;
    if mIsValidMatrixField then acShowYarnFaultImage.Execute;
  end;
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.qmMatrixTouch(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  xClassNumId: Integer;
  xYFCategory: TYFCategoryTypeEnum;
  xFineClassAlphaNumId, xGrobClassAlphaNumId: string;
  xFineClassMIString, xGrobClassMIString: string;
  xFineClassImageFileName, xGrobClassImageFileName: string;
  xExistsFineClassImageFile, xExistsGrobClassImageFile, xExistsImageFile: Boolean;
begin

  mSelectedMatrixField := (Sender as TMMQualityMatrix).GetFieldID(X, Y);
  mIsValidMatrixField :=  (Sender as TMMQualityMatrix).IsValidField(X, Y);

  // show popup only if valid field id and not in edit mode

  if (mSelectedMatrixField >= 0) and (mSelectedMatrixField < 128) and
     mIsValidMatrixField and (Button = mbRight) and not bEditValues.Down then begin

    xClassNumId := GetSelectedYFClassNumId();
    xYFCategory := GetSelectedYFCategory();

    xFineClassAlphaNumId := GetYFClassAlphaNumId(xClassNumId, xYFCategory, tClassFine);
    xGrobClassAlphaNumId := GetYFClassAlphaNumId(xClassNumId, xYFCategory, tClassGrob);

    xFineClassMIString := Format('%s "%s %s"', [rsStrYarnFaultClass, GetYFCategoryString(xYFCategory, true),xFineClassAlphaNumId]);
    xGrobClassMIString := Format('%s "%s %s"', [rsStrYarnFaultClass, GetYFCategoryString(xYFCategory, true),xGrobClassAlphaNumId]);

     // we have to set the text of the popup menu items and enable or disable it
    // 3 cases for showing default (=Loepfe provided) image: 
    //     a) fine class image exists                                  => fine class image will be used
    //     b) fine class image does not exist, grob class image exists => grob class image will be used
    //     c) fine and grob class images don't exist                   => popup menu item disabled with grayed fine class text
    //
    // For customer images, only fine class image mode is supported 


    //
    // 1st Block in Popup: default (=Loepfe provided) yarn fault image
    //

    // initialize image file info
    xFineClassImageFileName := GetYFImageFileName(xClassNumId, xYFCategory, tClassFine, tImageDefault);
    xExistsFineClassImageFile := FileExists(xFineClassImageFileName);
    if not xExistsFineClassImageFile then begin
      xGrobClassImageFileName := GetYFImageFileName(xClassNumId, xYFCategory, tClassGrob, tImageDefault);
      xExistsGrobClassImageFile := FileExists(xGrobClassImageFileName);
    end else begin
      xExistsGrobClassImageFile := False;
    end;
    xExistsImageFile := xExistsFineClassImageFile or xExistsGrobClassImageFile;

    mDefaultYFImageInfo.classNumId := xClassNumId;
    mDefaultYFImageInfo.imageType  := tImageDefault;
    mDefaultYFImageInfo.yfCategory := xYFCategory;
    mDefaultYFImageInfo.fileExists := xExistsImageFile;

    // menu item show default yarn fault image

    if xExistsFineClassImageFile or not xExistsImageFile then begin
      miShowDefaultYarnFaultImage.Caption := Format(rsStrMiShowDefaultYarnFaultImage, [xFineClassMIString]); // + '  ' + xFineClassImageFileName;
      mDefaultYFImageInfo.fileName        := xFineClassImageFileName;
      mDefaultYFImageInfo.classAlphaNumId := xFineClassAlphaNumId;
      mDefaultYFImageInfo.fgClassType     := tClassFine;
    end else begin
      miShowDefaultYarnFaultImage.Caption := Format(rsStrMiShowDefaultYarnFaultImage, [xGrobClassMIString]); // + '  ' + xGrobClassImageFileName;
      mDefaultYFImageInfo.fileName        := xGrobClassImageFileName;
      mDefaultYFImageInfo.classAlphaNumId := xGrobClassAlphaNumId;
      mDefaultYFImageInfo.fgClassType     := tClassGrob;
    end;

    miShowDefaultYarnFaultImage.Enabled := xExistsImageFile;


    //
    // 2nd block: customer yarn fault image, only fine class image files supported
    //

    // initialize image file info and record
    xFineClassImageFileName := GetYFImageFileName(xClassNumId, xYFCategory, tClassFine, tImageCustom);
    xExistsFineClassImageFile := FileExists(xFineClassImageFileName);

    mCustomYFImageInfo.fileName        := xFineClassImageFileName;
    mCustomYFImageInfo.classAlphaNumId := xFineClassAlphaNumId;
    mCustomYFImageInfo.fgClassType     := tClassFine;

    mCustomYFImageInfo.classNumId := xClassNumId;
    mCustomYFImageInfo.imageType  := tImageCustom;
    mCustomYFImageInfo.yfCategory := xYFCategory;
    mCustomYFImageInfo.fileExists := xExistsFineClassImageFile;

    // menu item: show custom yarn fault image
    miShowCustomYarnFaultImage.Caption := Format(rsStrMiShowCustomYarnFaultImage, [xFineClassMIString]); // + '  ' + xFineClassImageFileName;
    miShowCustomYarnFaultImage.Enabled := xExistsFineClassImageFile;

    // menu item: import custom yarn fault image
    miImportCustomYarnFaultImage.Caption := Format(rsStrMiImportCustomYarnFaultImage, [xFineClassMIString]);
    miImportCustomYarnFaultImage.Enabled := True; // import always allowed

    // menu item: delete custom image
    miDelCustomYarnFaultImage.Caption := Format(rsStrMiDeleteCustomYarnFaultImage, [xFineClassMIString]);
    miDelCustomYarnFaultImage.Enabled := xExistsFineClassImageFile;

    //
    // 3rd block: Cancel (no need to handle this option)
    //
    

    //
    //  Finally display the popup - menu
    //
    with (Sender as TMMQualityMatrix).ClientToScreen(Point(X, Y)) do
      pmMatrix.Popup(X, Y);

  end;
  //qmChannel.Invalidate;
 // qmSiro.Invalidate;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmData.ReadClassDataAndSettings;
//var
  //xValue : String;
  //xSetID: Integer;
  //xIDs : TStringList;

begin
  EnterMethod('TfrmData.ReadClassDataAndSettings');

  FillMatrixValues;

//06.01.2005 SDO@
//ShowMessage( 'ReadClassDataAndSettings');


//  // read set name to label and save current settings into member variable
//  with mCDR.YMSettings.YMSettings[0]^ do begin
//    mOrgYMSettings := YMSettingsExt.YMSettings;
//    mYMSettings    := mOrgYMSettings;
//    mRepInfo.YarnCount := YarnCount;
//    mRepInfo.YarnUnit := YarnUnit;
//  end;
//
//
//  // assign this settings to the settings handler
//  ymsControl.PutToScreen(mYMSettings);
//



{ 07.04.2005 SDO@
  //YM-SetID auslesen (-> nur eine ID)
  xIDs := TStringList.Create;
  xIDs.CommaText := mCDR.Params.YMSetIDs;

  xValue         := StringReplace(mCDR.Params.YMSetIDs, '"', '',[rfReplaceAll]);
  xValue         := StringReplace(xValue, '''', '',[rfReplaceAll]);
  xIDs.CommaText := xValue;

  xSetID         := StrToInt(xIDs.Strings[0]); // SetID

  xIDs.Free;
}

  //Settings von DB einlesen
//  mCDR.GetXMLSettingsFromID(xSetID, mSettings);

  mSettings := mCDR.XMLSettings.Items[0]^ ;  // immer 1. YMSet

 // read base data once
  mVirtualCuts.DetailData := mDM.ReadDetailData;


  mRepInfo.YarnCount := mSettings.YarnCount;
  mRepInfo.YarnUnit  := mSettings.YarnUnit;


  //Settings an Model ueberweisen
  FillUpModel( mSettings.XMLData );


  with mVirtualCuts.DetailData do
  try
    // production data labels
    laKg.Caption := Format('%.1f kg', [Kg]);

    // *** for debug only ***
    // virtual data labels
    latSpCycle0.Caption := Format('%.1f s', [tSpCycle1]);

{  //17.01.2005 SDO@}
    // base data memo
    with meBaseData.Lines do begin
      Clear;
      Add('Overall time');
      Add(Format('tWa' + #9 + '%.1f =' + #9 + '%s', [tWa / 60, FormatDuration(tWa)]));
      Add(Format('tOp' + #9 + '%.1f =' + #9 + '%s', [tOp / 60, FormatDuration(tOp)]));
      Add(Format('tRu' + #9 + '%.1f =' + #9 + '%s', [tRu / 60, FormatDuration(tRu)]));
      Add(Format('tLst' + #9 + '%.1f =' + #9 + '%s', [tLSt / 60, FormatDuration(tLSt)]));

      Add(Format('Spindle time (%d)', [SpdCount]));
      Add(Format('tWa' + #9 + '%.1f =' + #9 + '%s', [tWaSpd / 60, FormatDuration(tWaSpd)]));
      Add(Format('tOp' + #9 + '%.1f =' + #9 + '%s', [tOpSpd / 60, FormatDuration(tOpSpd)]));
      Add(Format('tRu' + #9 + '%.1f =' + #9 + '%s', [tRuSpd / 60, FormatDuration(tRuSpd)]));
      Add(Format('tLst' + #9 + '%.1f =' + #9 + '%s', [tLSt / 60, FormatDuration(tLStSpd)]));


      Add('Approximately 100% time');
      Add(Format('t100%%' + #9 + '%.1f min', [t100 / 60]));
      Add(Format('t100%%LSt' + #9 + '%.1f min', [t100LSt / 60]));

      Add('Some Base values');
      Add(Format('Sp' + #9 + '%f', [Sp]));
      Add(Format('SpBase' + #9 + '%f', [SpBase]));
      Add(Format('Len' + #9 + '%f km', [mCDR.Len / 1000]));

      Add('Cut information');
      Add(Format('Channel (NSLT)' + #9 + '%f', [ChannelCuts]));
      Add(Format('Splice' + #9 + '%f', [SpliceCuts]));
      Add(Format('FF' + #9 + '%f', [FFCuts]));
    end;
{}
  except
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmData.FillMatrixValues;
var
  x: Integer;
  xMin, xMax: Single;
  xSingle: Single;
begin
  EnterMethod('TfrmData.FillMatrixValues');

  // *** Yarn Matrix ***
  GetMinMaxValues(0, xMin, xMax);
//  for x:=0 to cClassCutFields-1 do begin
  for x:=0 to cMaxFieldCount[0]-1 do begin
    if mDisplayMode in [dmValues, dmCalculateSCValues] then begin
      qmChannel.SetValue(x, cCuts, mCDR.ClassCut[x]);
      qmChannel.SetValue(x, cDefects, mCDR.ClassUncut[x]);
    end else begin
      xSingle := mCDR.ClassUncut[x] + mCDR.ClassCut[x];

      qmChannel.SetValue(x, cCuts, 0);
      qmChannel.SetValue(x, cDefects, xSingle);
      if xSingle > 0.0 then begin
        qmChannel.FieldDots[x]  := GetDotColorValue(dcvDot, 600, 10, xMin, xMax, xSingle);
        qmChannel.FieldColor[x] := GetDotColorValue(dcvColor, 2, 10, xMin, xMax, xSingle);
      end else begin
        qmChannel.FieldDots[x]  := 0;
        qmChannel.FieldColor[x] := qmChannel.InactiveColor;
      end;
    end;
  end; // for x

  // *** Splice Matrix ***
  GetMinMaxValues(1, xMin, xMax);
//  for x:=0 to cClassCutFields-1 do begin
  for x:=0 to cMaxFieldCount[1]-1 do begin
    if mDisplayMode in [dmValues, dmCalculateSCValues] then begin
      qmSplice.SetValue(x, cCuts, mCDR.SpliceCut[x]);
      qmSplice.SetValue(x, cDefects, mCDR.SpliceUncut[x]);
    end else begin
      xSingle := mCDR.SpliceUncut[x] + mCDR.SpliceCut[x];

      qmSplice.SetValue(x, cCuts, 0);
      qmSplice.SetValue(x, cDefects, xSingle);
      if xSingle > 0.0 then begin
        qmSplice.FieldDots[x]  := GetDotColorValue(dcvDot, 600, 10, xMin, xMax, xSingle);
        qmSplice.FieldColor[x] := GetDotColorValue(dcvColor, 2, 10, xMin, xMax, xSingle);
      end else begin
        qmSplice.FieldDots[x]  := 0;
        qmSplice.FieldColor[x] := qmSplice.InactiveColor;
      end;
    end;
  end; // for x

  // *** FF Matrix ***
  GetMinMaxValues(2, xMin, xMax);
//  for x:=0 to cClassCutFields-1 do begin
  for x:=0 to cMaxFieldCount[2]-1 do begin
    if mDisplayMode in [dmValues, dmCalculateSCValues] then begin
      qmSiro.SetValue(x, cCuts, mCDR.SiroCut[x]);
      qmSiro.SetValue(x, cDefects, mCDR.SiroUncut[x]);
    end else begin
      xSingle := mCDR.SiroUncut[x] + mCDR.SiroCut[x];

      qmSiro.SetValue(x, cCuts, 0);
      qmSiro.SetValue(x, cDefects, xSingle);
      if xSingle > 0.0 then begin
        qmSiro.FieldDots[x]  := GetDotColorValue(dcvDot, 600, 10, xMin, xMax, xSingle);
        qmSiro.FieldColor[x] := GetDotColorValue(dcvColor, 2, 10, xMin, xMax, xSingle);
      end else begin
        qmSiro.FieldDots[x]  := 0;
        qmSiro.FieldColor[x] := qmSiro.InactiveColor;
      end;
    end;
  end; // for x
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.SaveSettings;
begin
  with mCAConfig, SettingsIniDB.Ini do begin
    WriteBool('Options', 'ShowChannel', ShowChannel);
    WriteBool('Options', 'ShowSplice', ShowSplice);
    WriteBool('Options', 'ShowYarnCount', ShowYarnCount);
    WriteBool('Options', 'ShowCluster', ShowCluster);
    WriteBool('Options', 'ShowSFI', ShowSFI);
    WriteBool('Options', 'ShowFFCluster', ShowFFCluster);
    WriteBool('Options', 'ShowVCV', ShowVCV);     //Nue:14.02.08
    WriteBool('Options', 'ShowClassData', ShowClassData);
    WriteBool('Options', 'PrintBlackWhite', PrintBlackWhite);
    WriteBool('Options', 'FineMode', FineMode);
    WriteString('Options', 'ZeroLimit', MMFloatToStr(ZeroLimit));
    WriteInteger('Options', 'ActiveColor', ActiveColor);
    WriteInteger('Options', 'PassiveColor', PassiveColor);
  end;
  SettingsIniDB.SaveDefault;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmData.UpdateSettingsToMatrix;
  //...............................................................
  procedure UpdateMatrix(aMatrix: TMMQualityMatrix);
  begin
    with aMatrix do begin

      //PutYMSettings(mYMSettings);
      //FillUpModel(mSettings.XMLData);

      if mDisplayMode = dmValues then begin
        if mCAConfig.FineMode then
          DisplayMode := dmValues
        else
          DisplayMode := dmCalculateSCValues;
      end else
        DisplayMode := mDisplayMode;
    end;
  end;
  //...............................................................
begin
  EnterMethod('TfrmData.UpdateSettingsToMatrix');


//  FillUpModel( TSettings(sgData.Objects[sgData.Col, 0]).XMLData );
  FillUpModel(mSettings.XMLData); //06.04.2005 SDO@

  //06.01.2005 SDO@
//  ShowMessage( 'UpdateSettingsToMatrix ' );

  UpdateMatrix(qmChannel);
  UpdateMatrix(qmSplice);
  UpdateMatrix(qmSiro);
end;
{:-----------------------------------------------------------------------------}

procedure TfrmData.SetPropertiesToMatrix;
  //...............................................................
  procedure UpdateMatrix(aMatrix: TMMQualityMatrix);
  begin
    with aMatrix do begin
      if aMatrix = qmSplice then
        //Fix Spiceactivecolor auf clSpliceQualityCutOn
        qmSplice.ActiveColor := clSpliceQualityCutOn  //Nue:23.09.08
      else
        ActiveColor   := mCAConfig.ActiveColor;
      InactiveColor := mCAConfig.PassiveColor;
      ZeroLimit     := mCAConfig.ZeroLimit;
      if mCAConfig.FineMode then DisplayMode := dmValues
                            else DisplayMode := dmCalculateSCValues;
      Invalidate;
    end;
  end;
  //...............................................................
begin
  EnterMethod('TfrmData.SetPropertiesToMatrix');

  UpdateMatrix(qmChannel);
  UpdateMatrix(qmSplice);
  UpdateMatrix(qmSiro);
end;

{:-----------------------------------------------------------------------------}

procedure TfrmData.ymsControlChange(aOwner: TComponent);
begin
  CalculateVirtualCuts;
end;

//------------------------------------------------------------------------------
procedure TfrmData.sgDataMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  xCol, xRow: Integer;
begin
  inherited;
  if mDataReady then begin
    with sgData do begin
      MouseToCell(X, Y, xCol, xRow);
      CodeSite.SendFmtMsg('sgDataMouseDown: %d, %d', [xCol, xRow]);
      if  xCol = 0 then exit;

      if xCol = 1 then begin
        FixedCols := 1
      end else
        FixedCols := 2;

      if xCol >= 1 then
        Col := xCol;
        // call this function only if header was clicked

//      if (xCol = 1) or (xRow = 0) then
//       RestoreYMSettings(xCol);

       mSettings.XMLData :=  TSettings(sgData.Objects[xCol, 0]).XMLData;
       UpdateSettingsToMatrix;
       Invalidate;

    end; // with sgData
  end; // if mDataReady

end;
//------------------------------------------------------------------------------
procedure TfrmData.sgDataDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  with sgData do begin
    with Canvas do begin
      if (ARow >= FixedRows) and (ACol > 0) then begin
        if ACol = Col then begin
          if ARow = 1 then
            CodeSite.SendFmtMsg('sgDataGetCellColor.CurrentSerie: %d, %d', [ACol, ARow]);
          Brush.Color := cSelectColor
        end else if (ACol = 1) and (FixedCols = 2) then begin
          if ARow = 1 then
            CodeSite.SendFmtMsg('sgDataGetCellColor.Aktuell: %d, %d', [ACol, ARow]);
          Brush.Color := clMMReadOnlyColor
        end else begin
          if ARow = 1 then
            CodeSite.SendFmtMsg('sgDataGetCellColor.Default: %d, %d', [ACol, ARow]);
          Brush.Color := clWhite; //Color;
        end;

        FillRect(Rect);
      end;
      Font.Color := clMenuText;
      TextRect(Rect, Rect.Left+1, Rect.Top+1, Cells[ACol, ARow]);
    end;
  end;
end;
//------------------------------------------------------------------------------
{
procedure TfrmData.RestoreYMSettings(aIndex: Integer);
begin
  mCurrentSerie := aIndex;
  with sgData do begin
    if (aIndex > 0) and (aIndex < ColCount) then begin
//06.01.2005 SDO@
//ShowMessage(  'RestoreYMSettings' );
      with TSettings(Objects[aIndex, 0]) do begin
           mSettings.XMLData := XMLData;
          // FillUpModel(mSettings.XMLData);

//        mYMSettings := YMSettings;
//        ymsControl.PutToScreen(mYMSettings);


      end;

    end;



    CodeSite.SendFmtMsg('mCurrentSerie: %d', [mCurrentSerie]);
    // now distribute current settings
    UpdateSettingsToMatrix;
    Invalidate;
  end;
end;
}
//------------------------------------------------------------------------------
procedure TfrmData.sgDataSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
{
  CodeSite.SendFmtMsg('sgDataGetCellColor.SelectCell: %d, %d', [ACol, ARow]);
  if (ACol <= 0) and (ACol > sgData.ColCount) then exit;
  mSettings.XMLData :=  TSettings(sgData.Objects[ACol, 0]).XMLData;
  //FillUpModel( TSettings(sgData.Objects[ACol, 0]).XMLData  );
  UpdateSettingsToMatrix;
  Invalidate;
  //sgData.Col := ACol;
  //RestoreYMSettings(ACol);
}
end;
//------------------------------------------------------------------------------
procedure TfrmData.sgDataMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  xCol, xRow: Integer;
begin
  with sgData do begin
    MouseToCell(X, Y, xCol, xRow);
    if (xCol >=0) and (xRow >= 0) then begin
      if Hint <> Cells[xCol, xRow] then
        Application.CancelHint;

      if Canvas.TextWidth(Cells[xCol, xRow]) > ColWidths[xCol] then
        Hint := Cells[xCol, xRow]
      else
        Application.CancelHint;
    end else
      Application.CancelHint;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmData.bNavigateClick(Sender: TObject);
begin

  with bNavigate do begin
    mAnalyseMode := not mAnalyseMode;
    mSelectNavigator.ShowSelection := not mAnalyseMode;
    // Zeige Infos fuer naechsten Mode an
    if mAnalyseMode then begin
      Caption := rsNavigate;
      Glyph.LoadFromResourceName(HInstance, 'NAVIGATE');
    end else begin
      Caption := rsAnalyse;
      Glyph.LoadFromResourceName(HInstance, 'ANALYSE');
    end;
    Update;
    mSelectNavigator.Update;
  end;

  if mAnalyseMode then begin
    InitDataGrid;
    mDataReady := TRUE;
    // Auswahl in ClassDataReader uebernehmen und prod. kg aus DB auslesen
    with mCDR.Params do begin
      StyleIDs := mSelectNavigator.StyleID;
      ProdIDs  := mSelectNavigator.LotID;
      MachIDs  := mSelectNavigator.MachID;
      TimeMode := mSelectNavigator.TimeMode;
      if TimeMode = tmInterval then
        TimeIDs  := mSelectNavigator.TimeID
      else begin
        TimeFrom := mSelectNavigator.TimeFrom;
        TimeTo   := mSelectNavigator.TimeTo;
      end;
      YMSetIDs := mSelectNavigator.YMSetID;
    end;


    Repaint;
    // read class data and settings into mYMSettings
    ReadClassDataAndSettings;
    // set settings to all components;
    Repaint;
    UpdateSettingsToMatrix;

    // now save informations in actual serie (original) and calculate cuts
    CalculateVirtualCuts;
    // create a new serie for editing
    acAddSerieExecute(nil);

  end else  mDataReady := FALSE;

end;
//------------------------------------------------------------------------------
procedure TfrmData.acCloseExecute(Sender: TObject);
begin
  Close;
end;
//------------------------------------------------------------------------------
procedure TfrmData.acCurveEditModeExecute(Sender: TObject);
var xBMP : TBitmap;
begin

  xBMP:=   bEditValues.Glyph ;

  if bEditValues.Down then begin
    mImages32.GetBitmap(4, xBMP);
    qmChannel.Cursor := crDefault;
    qmSplice.Cursor  := crDefault;
    qmSiro.Cursor    := crDefault;
  end else begin
    mImages32.GetBitmap(6, xBMP);
    qmChannel.Cursor := crArrowWithCamera;
    qmSplice.Cursor  := crArrowWithCamera;
    qmSiro.Cursor    := crArrowWithCamera;
  end;

  bEditValues.Glyph:= xBMP;
end;
//------------------------------------------------------------------------------
procedure TfrmData.acShowFaultClassesExecute(Sender: TObject);

  procedure UpdateMatrix(aMatrix: TMMQualityMatrix);
  begin
    with aMatrix do begin
         ShowYarnFaultClass := bShowFaultClasses.Down;
         DisplayMode := dmNone;
    end;
  end;

begin
  inherited;
{
  UpdateMatrix(qmChannel);
  UpdateMatrix(qmSplice);
  UpdateMatrix(qmSiro);
}
end;
//------------------------------------------------------------------------------
procedure TfrmData.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;

 //------------------------------------------------------------------------------
procedure TfrmData.FillUpModel(aXMLText: String);
var
  Test: TStringList;
begin
  EnterMethod('TfrmData.FillUpModel');

 try
   //aXMLText := LoadXMLFile('W:\MillMaster\TestProjects\XMLSettings\Beispiel Settings\Settings WSC 6.15.xml');
   XMLSettingsModel.xmlAsString :=aXMLText;

   Test := TStringList.Create();
   try
     Test.Text := aXMLText;
     Test.SaveToFile('C:\\Log\\XMLSettingsModel.xml');
   finally
     Test.Free;
   end;
 except
   on E: Exception do
         ShowMessage( 'Error in FillUpModel() ' + #10#13 + 'Error msg. : ' + e.Message )
 end;
end;

procedure TfrmData.mCDRException(Sender: TObject; aMsg: string);
begin
  inherited;
end;

//------------------------------------------------------------------------------
procedure TfrmData.XMLSettingsModelChanged(Sender: TObject);
begin
  inherited;
  CalculateVirtualCuts;
end;

end.


