(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: DataForm.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows
| Target.system.: Windows
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.06.2005  1.02  Wss | Namenskonfligt mit einer Klasse in einer anderen Unit gel�st
| 16.06.2005  1.02  Wss | Unit Namenskonfligt gel�st
|=========================================================================================*)
unit OpenTemplateForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, StdCtrls, mmListBox, DBVisualBox, Buttons, mmBitBtn,
  MainDataModul, IvDictio, IvMulti, IvEMulti, mmTranslator, mmButton;

type
  {:----------------------------------------------------------------------------
   Open template settings for comparing with actual clearer settings.
   ----------------------------------------------------------------------------}
  TfrmOpenTemplate = class (TDialogBottom)
    mmTranslator1: TmmTranslator;
    vlbTemplates: TDBVisualListBox;
    procedure bDeleteTemplateClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure vlbTemplatesDblClick(Sender: TObject);
  private
    function GetTemplateID: Integer;
    function GetTemplateName: string;
  public
    property TemplateID: Integer read GetTemplateID;
    property TemplateName: string read GetTemplateName;
  end;
  
var
  frmTemplate: TfrmOpenTemplate;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

{:------------------------------------------------------------------------------
 TfrmTemplate}
{:-----------------------------------------------------------------------------}
procedure TfrmOpenTemplate.bDeleteTemplateClick(Sender: TObject);
var
  xKey: string;
begin
  xKey:= vlbTemplates.KeyCommaText;
  if xKey <> '' then begin
  //    dmMain.DeleteTemplateRec(StrToInt(xKey));
  //    dmMain.GetAllTemplates;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmOpenTemplate.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  vlbTemplates.DataSource.DataSet.Close;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmOpenTemplate.FormShow(Sender: TObject);
begin
  vlbTemplates.DataSource.DataSet.Open;
end;

{:-----------------------------------------------------------------------------}
function TfrmOpenTemplate.GetTemplateID: Integer;
begin
  Result := StrToInt(vlbTemplates.KeyCommaText);
end;

{:-----------------------------------------------------------------------------}
function TfrmOpenTemplate.GetTemplateName: string;
begin
  Result := vlbTemplates.DataCommaText;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmOpenTemplate.vlbTemplatesDblClick(Sender: TObject);
begin
  bOK.Click;
end;

end.


