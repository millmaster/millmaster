(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: DataForm.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows
| Target.system.: Windows
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 08.03.2004  1.00  Wss | Sensinghead entfernt, da dieser nicht ausgewertet wird
|=========================================================================================*)
unit SettingsForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, StdCtrls, Buttons, mmBitBtn, mmNumCtrl, mmRadioButton,
  mmLabel, mmGroupBox, ClearerAssistantDef, IvDictio, IvMulti, IvEMulti,
  mmTranslator, mmButton, mmComboBox, ExtCtrls, mmRadioGroup, mmColorButton;

type
  {:----------------------------------------------------------------------------
   Option dialog for Clearer Assistant.

   - Coarse or fine view for matrix values
   - Edit ZeroLimit for matrixes
   ----------------------------------------------------------------------------}
  TfrmSettings = class (TDialogBottom)
    edZeroLimit: TmmNumEdit;
    mmGroupBox1: TmmGroupBox;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    rbCoarse: TmmRadioButton;
    rbFine: TmmRadioButton;
    mmTranslator: TmmTranslator;
    mmLabel4: TmmLabel;
    mmLabel8: TmmLabel;
    bClassActiveColor: TmmColorButton;
    bClassInactiveColor: TmmColorButton;
    procedure bOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    fCAConfig: TCAConfigRec;
    procedure SetCAConfig(const Value: TCAConfigRec);
  public
    property CAConfig: TCAConfigRec read fCAConfig write SetCAConfig;
  end;

var

  frmSettings: TfrmSettings;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHTMLHelp, YMParaDef;
{$R *.DFM}


{:------------------------------------------------------------------------------
 TfrmSettings}
{:-----------------------------------------------------------------------------}
procedure TfrmSettings.bOKClick(Sender: TObject);
begin
  with fCAConfig do begin
    ActiveColor  := bClassActiveColor.Color;
    PassiveColor := bClassInactiveColor.Color;
    ZeroLimit    := edZeroLimit.Value;
    FineMode     := rbFine.Checked;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmSettings.SetCAConfig(const Value: TCAConfigRec);
begin
  fCAConfig := Value;
  with fCAConfig do begin
    edZeroLimit.Value := ZeroLimit;
    if FineMode then rbFine.Checked   := True
                else rbCoarse.Checked := True;

    bClassActiveColor.Color   := ActiveColor;
    bClassInactiveColor.Color := PassiveColor;
  end;
end;

procedure TfrmSettings.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('CLA\Einstellungen_anpassen.htm');
end;

end.

