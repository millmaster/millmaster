unit MMClearerAssistantServerIMPL;

interface

uses
  ComObj, ActiveX, MMClearerAssistant_TLB, StdVcl, LoepfePluginIMPL, LOEPFELIBRARY_TLB;

type
  TMMClearerAssistantServer = class(TLoepfePlugin, IMMClearerAssistantServer)
  protected
  public
    function ExecuteCommand: SYSINT; override;
    procedure Initialize; override;
  end;

implementation

uses
  ComServ, Forms, BaseForm, WrapperForm, DataForm;

const
  cCLA = 1;

  cFloorMenuDef: Array[0..1] of TFloorMenuRec = (
    // --- Main Menu File ---
    (CommandID: 0; SubMenu: -1; MenuBmpName: '';
     MenuText: '(*)Reiniger Assistent'; MenuHint: '(*)Reiniger Assistent'; MenuStBar:'(*)Reiniger Assistent'; //ivlm
     ToolbarText: '';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: cCLA; SubMenu: 0; MenuBmpName: 'CLA';
     MenuText: '(*)Reiniger Assistent'; MenuHint:'(*)Reiniger Assistent'; MenuStBar: '(*)Reiniger Assistent'; //ivlm
     ToolbarText: 'CLACommand';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll)
  );

//------------------------------------------------------------------------------
// TMMClearerAssistantServer
//------------------------------------------------------------------------------
function TMMClearerAssistantServer.ExecuteCommand: SYSINT;
var
  xForm: TfrmData;
  xWrap: TmmForm;
begin
  Result := 0;
  case CommandIndex of
    cCLA: begin
        if GetWrappedDataForm(TfrmData, TForm(xForm), xWrap) then begin
//          xForm.PreloadMachID(SelectedMachines);
          Result := xWrap.Handle;
        end;
      end;
  else
  end;
end;
//------------------------------------------------------------------------------
procedure TMMClearerAssistantServer.Initialize;
begin
  inherited initialize;
  InitMenu(cFloorMenuDef);
end;
//------------------------------------------------------------------------------
initialization
  InitializeObjectFactory(TMMClearerAssistantServer, Class_MMClearerAssistantServer);
end.
