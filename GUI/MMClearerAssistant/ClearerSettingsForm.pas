
(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: ClearerSettingsForm.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows
| Target.system.: Windows
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| xx.xx.xxxx  1.00      | First  release
| 06.01.2005  1.03  SDo | Umbau & Anpassungen an XML
| 15.04.2005  1.03  SDo | Neue overload Func. ShowSetting()
|=============================================================================*)
unit ClearerSettingsForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, ComCtrls, ToolWin, mmToolBar, YMRepSettingsFrame, IvDictio,
  IvMulti, IvEMulti, mmTranslator, MMUGlobal, YMParaDef, ClearerAssistantDef, 
  XMLDef, DataLayerDef;

type
  {:----------------------------------------------------------------------------
   Shows the complete clearer settings for the selected series.
   ----------------------------------------------------------------------------}
  (*
  Shows the complete clearer settings for the selected series.
  *)
  TfrmClearerSettings = class(TmmForm)
    bClose: TToolButton;
    mmToolBar1: TmmToolBar;
    mmTranslator: TmmTranslator;
    fraRepSettings: TfraRepSettings;
    procedure bCloseClick(Sender: TObject);
  public
    constructor Create(aOwner: TComponent); override;
    procedure ShowSetting(aSettings: TSettings; aYarnCount: Single; aYarnUnit: TYarnUnit; aYMSettings: TXMLSettingsInfoRec); overload;
    procedure ShowSetting(aSettings: TSettings; aYMSettings: TXMLSettingsInfoRec); overload;
  end;

var
  frmClearerSettings: TfrmClearerSettings;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,
  MainForm; // uses for image in toolbar

{:------------------------------------------------------------------------------
 TfrmClearerSettings}
{:-----------------------------------------------------------------------------}
constructor TfrmClearerSettings.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fraRepSettings.FrameMode := fmSettings;
end;

//:-----------------------------------------------------------------------------
procedure TfrmClearerSettings.bCloseClick(Sender: TObject);
begin
  Close;
end;
//:-----------------------------------------------------------------------------
procedure TfrmClearerSettings.ShowSetting(aSettings: TSettings; aYarnCount: Single; aYarnUnit: TYarnUnit; aYMSettings: TXMLSettingsInfoRec);
begin
 try
  fraRepSettings.ShowYMSettingsInfoRec(aYMSettings);
 finally
 end;
  Self.Caption := Format('%s %s', [rsClearerSettingsCaption, aSettings.SerieName]);
  //fraRepSettings.ShowYMSettings(aSettings.YMSettings, aYarnCount, aYarnUnit); //06.01.2005 SDO
end;
//:-----------------------------------------------------------------------------
procedure TfrmClearerSettings.ShowSetting(aSettings: TSettings;
  aYMSettings: TXMLSettingsInfoRec);
begin
  try
    fraRepSettings.ShowYMSettingsInfoRec(aYMSettings);
  finally
  end;
  Self.Caption := Format('%s %s', [rsClearerSettingsCaption, aSettings.SerieName]);
end;
//:-----------------------------------------------------------------------------

end.

