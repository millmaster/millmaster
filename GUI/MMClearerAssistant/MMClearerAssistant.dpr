program MMClearerAssistant;
{%ToDo 'MMClearerAssistant.todo'}

uses
  MemCheck,
  mmMBCS,
  RzCSIntf,
  Forms,
  LoepfeGlobal,
  BASEAPPLMAIN in '..\..\..\LoepfeShares\Templates\BASEAPPLMAIN.pas' {BaseApplMainForm},
  PrintForm in 'PrintForm.pas' {frmPrint},
  PrintSettingsForm in 'PrintSettingsForm.pas' {frmPrintSettings},
  MainDataModul in 'MainDataModul.pas' {dmMain: TDataModule},
  ClearerAssistantDef in 'ClearerAssistantDef.pas',
  BASEDialog in '..\..\..\LoepfeShares\Templates\BASEDIALOG.pas' {mmDialog},
  BASEDialogBottom in '..\..\..\LoepfeShares\Templates\BASEDIALOGBOTTOM.pas' {DialogBottom},
  OpenTemplateForm in 'OpenTemplateForm.pas' {frmOpenTemplate},
  YarnClassForm in 'YarnClassForm.pas' {frmShowSample},
  VirtualCuts in 'VirtualCuts.pas',
  MainForm in 'MainForm.pas' {frmMain},
  BaseForm in '..\..\..\LoepfeShares\Templates\BASEFORM.pas' {mmForm},
  DataForm in 'DataForm.pas' {frmData},
  YMRepSettingsFrame in '..\..\Components\VCL\YMRepSettingsFrame.pas' {fraRepSettings: TFrame},
  ClearerSettingsForm in 'ClearerSettingsForm.pas' {frmClearerSettings},
  SettingsForm in 'SettingsForm.pas' {frmSettings},
  SaveTemplateDLG in '..\..\Common\SaveTemplateDLG.pas' {SaveTemplateDLGForm},
  SelectionDialog in '..\..\..\LOEPFESHARES\TEMPLATES\SELECTIONDIALOG.pas' {frmSelectionDialog},
  MMClearerAssistantServerIMPL in 'MMClearerAssistantServerIMPL.pas';

//MMClearerAssistant_TLB in 'MMClearerAssistant_TLB.pas';

//  SaveTemplateDLG in 'SaveTemplateDLG.pas' {SaveTemplateDLGForm};

{$R *.TLB}

{$R *.RES}
{$R Version.res}
{$R Toolbar.res}
begin
  gApplicationName := 'MMClearerAssistant';
  CodeSite.Enabled := CodeSiteEnabled;
{$IFDEF MemCheck}
  MemChk(gApplicationName);
{$ENDIF}

  Application.Initialize;
  Application.Title := 'MMClearerAssistant';
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
