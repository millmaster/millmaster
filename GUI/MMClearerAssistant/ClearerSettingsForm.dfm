inherited frmClearerSettings: TfrmClearerSettings
  Left = 5
  Top = 4
  Width = 667
  Height = 485
  Caption = '(*)Reinigereinstellung:'
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object mmToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 659
    Height = 29
    Flat = True
    Images = frmMain.ImageList16x16
    TabOrder = 0
    object bClose: TToolButton
      Left = 0
      Top = 0
      ImageIndex = 24
      OnClick = bCloseClick
    end
  end
  inline fraRepSettings: TfraRepSettings
    Top = 29
    Height = 422
    Align = alClient
    TabOrder = 1
    inherited pnRight: TmmPanel
      Height = 406
      inherited mFCluster: TFFClusterEditBox
        Top = 268
      end
      inherited mYarnCount: TYarnCountEditBox
        Top = 268
      end
      inherited mmPanel2: TmmPanel
        Height = 261
        inherited mFMatrix: TQualityMatrix
          MatrixMode = mmDisplayData
        end
      end
      inherited mPSettings: TPEditBox
        Top = 358
      end
    end
    inherited pnLeft: TmmPanel
      Height = 406
      inherited pnBottom: TmmPanel
        Top = 249
      end
      inherited mmPanel1: TmmPanel
        Height = 248
      end
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 8
    Top = 48
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
end
