05 Feb 2008
-----------
The old sample images have been replaced in MM Ver. 5.04 with image samples from Ko.
Old sample images are archived in the directory 'OldCuts\OldCuts_Until_MM5.03_Feb2008'.
The image samples from Ko habe been renamed, e.g. Ko-Name was: '0.jpg', new-Name: 'D_N1_1.jpg'.
The original images from Ko are in the directory 'OldCuts\CutSamplesFromKo'.