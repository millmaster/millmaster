object dmMain: TdmMain
  OldCreateOrder = False
  Left = 1321
  Top = 322
  Height = 479
  Width = 819
  object dsWork: TmmDataSource
    DataSet = dseWork
    Left = 32
    Top = 112
  end
  object conDefault: TmmADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=netpmek32;User ID=MMSystemSQL;Initi' +
      'al Catalog=MM_Winding;Data Source=VM-MMDEVELOP;Use Procedure for' +
      ' Prepare=1;Auto Translate=False;Packet Size=4096;Workstation ID=' +
      'VM-MMDEVELOP;Use Encryption for Data=False;Tag with column colla' +
      'tion when possible=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 32
    Top = 8
  end
  object dseWork: TmmADODataSet
    Connection = conDefault
    BeforeOpen = dseWorkBeforeOpen
    Parameters = <>
    Left = 32
    Top = 64
  end
  object comWork: TmmADOCommand
    Connection = conDefault
    Parameters = <>
    Left = 112
    Top = 8
  end
  object dsTemplates: TmmDataSource
    DataSet = dseTemplates
    Left = 112
    Top = 104
  end
  object dseTemplates: TmmADODataSet
    Connection = conDefault
    CommandText = 
      'Select c_YM_Set_name, c_ym_Set_id, c_Color'#13#10'From t_xml_ym_Settin' +
      'gs'#13#10'Where c_template_set = 1'#13#10'order by c_YM_Set_name'#13#10
    Parameters = <>
    Left = 112
    Top = 64
  end
end
