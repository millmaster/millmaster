{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: PrintSettingsForm.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Druckerauswahl-Dialog und PreView & Print
| Info..........:
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 15.10.2001  1.00  SDo | File created
| 26.02.2002  1.00  Wss | Filterwerte muessen eingeschalten werden, damit diese
                          Angezeigt werden
| 25.08.2003  1.00  Wss | Print Button wird �ber mPrintSetup.PrinterError kontrolliert
| 01.12.2003  1.01  SDo | Neues Property ShowFaultClassification
| 26.05.2004  1.02  SDo | Anpassung Ausdruck Daten & Fault-Classification
| 12.01.2005  1.03  SDo | Umbau & Anpassungen an XML
| 14.02.2008  1.04  Nue | Einbau VCV
===============================================================================}
unit PrintSettingsForm;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, mmLabel, mmComboBox, Buttons, mmBitBtn,
  mmGroupBox, NumCtrl, mmRadioGroup, mmRadioButton, mmEdit, IvDictio,
  IvMulti, IvEMulti, mmTranslator, Mask, mmStringGrid,
  ClearerAssistantDef, QRPrntr, mmButton, PrintSetupFrame, CheckLst,
  mmCheckListBox, mmCheckBox, BASEFORM, mmLineLabel, Qrctrls;



type

  {:----------------------------------------------------------------------------
   Print settings dialog for configure the printout.
   ----------------------------------------------------------------------------}
  TfrmPrintSettings = class (TmmForm)
    bCancel: TmmButton;
    bPreview: TmmButton;
    cbShowClassData: TmmCheckBox;
    clbSeries: TmmCheckListBox;
    bPrint: TmmButton;
    mmLabel1: TmmLabel;
    mmTranslator: TmmTranslator;
    mPrintSetup: TFramePrintSetup;
    mmLineLabel5: TmmLineLabel;
    cbFilterStyle: TmmCheckBox;
    cbFilterLot: TmmCheckBox;
    cbFilterMachine: TmmCheckBox;
    cbFilterYMSettings: TmmCheckBox;
    mmLineLabel1: TmmLineLabel;
    cbChannel: TmmCheckBox;
    cbSplice: TmmCheckBox;
    cbYarnCount: TmmCheckBox;
    cbCluster: TmmCheckBox;
    cbSFI: TmmCheckBox;
    cbFFCluster: TmmCheckBox;
    cbVCV: TmmCheckBox;
    procedure bPrintClick(Sender: TObject);
    procedure OnSettingsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mPrintSetupPrintSetupChanged(Sender: TObject);
  private
    fCAConfig: TCAConfigRec;
    fGrid: TmmStringGrid;
    fReportInfo: TReportInfoRec;
    fShowFaultClassification: Boolean;

    function GetSelectedSeries: string;
    procedure PrintOrPreview(aPreview : Boolean);
    procedure SetCAConfig(const Value: TCAConfigRec);
    procedure SetGrid(const Value: TmmStringGrid);
    procedure SetShowFaultClassification(const Value: Boolean);
    procedure SetReportInfo(const Value: TReportInfoRec);      

  public
    property CAConfig: TCAConfigRec read fCAConfig write SetCAConfig;
    property Grid: TmmStringGrid read fGrid write SetGrid;
    property ReportInfo: TReportInfoRec read fReportInfo write SetReportInfo;
    property ShowFaultClassification : Boolean read fShowFaultClassification write SetShowFaultClassification;
  end;


var
  frmPrintSettings: TfrmPrintSettings;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,

  RzCSIntf,
  Printers, WinSpool, PrintForm, QualityMatrixDef, Dialogs;


{:------------------------------------------------------------------------------
 TfrmPrintSettings}
{:-----------------------------------------------------------------------------}
procedure TfrmPrintSettings.bPrintClick(Sender: TObject);
begin
  PrintOrPreview((Sender as TControl).Tag = 0);
end;

{:-----------------------------------------------------------------------------}
function TfrmPrintSettings.GetSelectedSeries: string;
var
  i: Integer;
begin
  Result := '';
  with TStringList.Create do
  try
    with clbSeries do
      for i:=0 to Items.Count-1 do begin
        if Checked[i] then
          Add(IntToStr(i+1));
      end;

    Result := CommaText;
  finally
    Free;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmPrintSettings.OnSettingsClick(Sender: TObject);
begin
  with fCAConfig, (Sender as TCheckBox) do begin
    case Tag of
      0: ShowChannel   := Checked;
      1: ShowSplice    := Checked;
      2: ShowYarnCount := Checked;
      3: ShowCluster   := Checked;
      4: ShowSFI       := Checked;
      5: ShowFFCluster := Checked;
      6: ShowClassData := Checked;
      7: ShowVCV       := Checked;       //Nue:14.02.08
     // 7: PrintBlackWhite := Checked;
    else
    end;
    PrintBlackWhite  := mPrintSetup.PrintBlackWhite;
  end;
end;
{:-----------------------------------------------------------------------------}
procedure TfrmPrintSettings.PrintOrPreview(aPreview : Boolean);
var
  xOldCursor: TCursor;
  //...............................................................
  procedure SetFilterLabel(aCheckBox: TCheckBox; aQLabel: TQRLabel; aString: String);
  begin
    if aCheckBox.Checked then
      aQLabel.Caption := aString
    else
      aQLabel.Caption := rsHidenFilterText;
  end;
  //...............................................................
begin
  xOldCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;

  fReportInfo.SelectedSeries := GetSelectedSeries;


  with TfrmPrint.Create(Nil) do
  try

    mQuickReport.UsePrinterIndex(mPrintSetup.PrinterIndex);

    with fReportInfo do begin

      qlShiftFrom.Caption  := TimeInfo.FromTime;
      qlShiftTo.Caption    := TimeInfo.ToTime;
      qlProdQuantityValue.Caption := ProdKg;

      SetFilterLabel(cbFilterStyle,      qlStyle,      Style);
      SetFilterLabel(cbFilterLot,        qlLot,        Lot);
      SetFilterLabel(cbFilterMachine,    qlMachine,    Machine);
      SetFilterLabel(cbFilterYMSettings, qlYMSettings, Template);
    end;

    Grid       := fGrid;
    ReportInfo := fReportInfo;
    with mQuickReport do
    try
      with fCAConfig do begin
        PrintBlackWhite :=  mPrintSetup.PrintBlackWhite;

        with XMLSettingsChannel do begin
           Visible := ShowChannel;
           Enabled := ShowChannel;
        end;

        with XMLSettingsYarnCount do begin
           Visible := ShowYarnCount;
           Enabled := ShowYarnCount;

           YarnCount :=  ReportInfo.YarnCount;
           YarnUnit  :=  ReportInfo.YarnUnit;
        end;

        with XMLSettingsCluster do begin
           Visible := ShowCluster;
           Enabled := ShowCluster;
        end;

        with XMLSettingsSplice do begin
           Visible := ShowSplice;
           Enabled := ShowSplice;
        end;

        with XMLSettingsSFI do begin
           Visible := ShowSFI;
           Enabled := ShowSFI;
        end;

        with XMLSettingsFCluster do begin
           Visible := ShowFFCluster;
           Enabled := ShowFFCluster;
        end;

        //Nue:14.02.08
        with XMLSettingsVCV do begin
           Visible := ShowVCV;
           Enabled := ShowVCV;
        end;

        if cbShowClassData.Checked then begin

           qrmChannel.QMatrix.Assign(QMChannel);
           qrmSplice.QMatrix.Assign(QMSplice);
           qrmSiro.QMatrix.Assign(QMFF);


           //Klassenbezeichnung anzeigen
           if fShowFaultClassification then begin

              qrmChannel.QMatrix.ShowYarnFaultClass := TRUE;
              qrmChannel.QMatrix.ShowGroupSubFieldNr := TRUE;

              qrmSiro.QMatrix.MatrixSubType := QMFF.MatrixSubType;
              qrmSiro.QMatrix.ShowYarnFaultClass := TRUE;
              qrmSiro.QMatrix.ShowGroupSubFieldNr := TRUE;

              qrmSplice.QMatrix.ShowYarnFaultClass := TRUE;
              qrmSplice.QMatrix.ShowGroupSubFieldNr := TRUE;

           end else begin
             qrmChannel.DisplayMode := fCAConfig.DisplayMode;
             qrmSplice.DisplayMode  := fCAConfig.DisplayMode;
             qrmSiro.DisplayMode    := fCAConfig.DisplayMode;
           end;
        end;

        //Matrix
        with qrmChannel do begin
          ActiveColor   := fCAConfig.ActiveColor;
          InactiveColor := fCAConfig.PassiveColor;
          BlackWhite    := PrintBlackWhite;

          ClusterVisible :=  QMChannel.ClusterVisible;
          SpliceVisible  :=  QMChannel.SpliceVisible;
          ChannelVisible :=  QMChannel.ChannelVisible;
        end;

        with qrmSplice do begin
          ActiveColor   := fCAConfig.ActiveColor;
          InactiveColor := fCAConfig.PassiveColor;
          BlackWhite    := PrintBlackWhite;

          ClusterVisible :=  QMSplice.ClusterVisible;
          SpliceVisible  :=  QMSplice.SpliceVisible;
          ChannelVisible :=  QMSplice.ChannelVisible;
        end;

        with qrmSiro do begin
          ActiveColor   := fCAConfig.ActiveColor;
          InactiveColor := fCAConfig.PassiveColor;
          BlackWhite    := PrintBlackWhite;

          ClusterVisible :=  QMFF.ClusterVisible;
          SpliceVisible  :=  QMFF.SpliceVisible;
          ChannelVisible :=  QMFF.ChannelVisible;
        end;

      end; // End With
      Prepare;
      Screen.Cursor := xOldCursor;
      if aPreview then Preview
                  else Print;
    finally
      QRPrinter.Free;
      QRPrinter := Nil;
    end;
  finally
    Free;
    Screen.Cursor := xOldCursor;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmPrintSettings.SetCAConfig(const Value: TCAConfigRec);
begin
  fCAConfig := Value;
  with fCAConfig do begin
    cbChannel.Checked   := ShowChannel;
    cbSplice.Checked    := ShowSplice;
    cbYarnCount.Checked := ShowYarnCount;
    cbCluster.Checked   := ShowCluster;
    cbSFI.Checked       := ShowSFI;
    cbFFCluster.Checked := ShowFFCluster;
    cbVCV.Checked       := ShowVCV;   //Nue:14.02.08

    //cbBWQMatrix.Checked := PrintBlackWhite;

    mPrintSetup.PrintBlackWhite := PrintBlackWhite;
    cbShowClassData.Checked := ShowClassData;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmPrintSettings.SetGrid(const Value: TmmStringGrid);
var
  i: Integer;
  xIndex: Integer;
begin
  fGrid:= Value;

  with fGrid do
    for i:=1 to ColCount-1 do begin
      xIndex := clbSeries.Items.Add(Cells[i, 0]);
      // check actual and selected series as default
      if (i=1) or (i=Col) then
        clbSeries.Checked[xIndex] := True;
    end;
end;
{:-----------------------------------------------------------------------------}
procedure TfrmPrintSettings.FormCreate(Sender: TObject);
begin
  fShowFaultClassification := FALSE;
  //bPreview.Visible := CodeSite.Enabled;
  bPrint.Enabled   := mPrintSetup.PrinterOK;
  mPrintSetup.Portrait := TRUE;
  mPrintSetup.OrientationEnable := FALSE;
end;
{:-----------------------------------------------------------------------------}
procedure TfrmPrintSettings.mPrintSetupPrintSetupChanged(Sender: TObject);
begin
  bPrint.Enabled := mPrintSetup.PrinterOK;
end;
{:-----------------------------------------------------------------------------}
procedure TfrmPrintSettings.SetShowFaultClassification(
  const Value: Boolean);
begin
  fShowFaultClassification := Value;
end;
{:-----------------------------------------------------------------------------}
procedure TfrmPrintSettings.SetReportInfo(const Value: TReportInfoRec);
var xText : String;
begin
  fReportInfo := Value;

  xText := StringReplace(fReportInfo.Style, '#1' ,'', [rfReplaceAll] );
  if xText <> '' then  cbFilterStyle.Checked := TRUE;

  xText := StringReplace(fReportInfo.Lot, '#1' ,'', [rfReplaceAll] );
  if xText <> '' then  cbFilterLot.Checked := TRUE;

  xText := StringReplace(fReportInfo.Machine, '#1' ,'', [rfReplaceAll] );
  if xText <> '' then  cbFilterMachine.Checked := TRUE;

  xText := StringReplace(fReportInfo.Template, '#1' ,'', [rfReplaceAll] );
  if xText <> '' then  cbFilterYMSettings.Checked := TRUE;
end;
{:-----------------------------------------------------------------------------}
end.

