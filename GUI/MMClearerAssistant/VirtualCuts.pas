(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: DataForm.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows
| Target.system.: Windows
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 19.02.2004  1.02  Wss | Faktoren auf 1.0 setzen, wenn keine Schnittzahlen vorhanden sind
|=========================================================================================*)
unit VirtualCuts;

interface
uses
  Classes, ClassDataReader,
  ClearerAssistantDef, CurveLayer, QualityMatrix, MMUGlobal;

const
  cCutIndexChannel = 1;
  cCutIndexSplice  = 2;
  cCutIndexFF      = 3;
  cCutIndexCTot    = 4;

type
  {:----------------------------------------------------------------------------
   Helper class for calculation virtual cuts. 
   Important properties:
   - Needs link to channel-, splice- and FF-matrix to get the cut fields from the curve.
   - CDR: needs ClassDataReader for calculating virtual cuts with real defects/cuts
   ----------------------------------------------------------------------------}
  TVirtualCuts = class (TObject)
  private
    fCDR: TClassDataReader;
    fChannelMatrix: TQualityMatrix;
    fCutInfoArr: Array[cCutIndexChannel..cCutIndexCTot] of Single;
    fDetailData: TDetailDataRec;
    fFFMatrix: TQualityMatrix;
    fSpliceMatrix: TQualityMatrix;
    fVirtualCuts: Array[1..cClassCutFields] of Single;
    mCheckClassCleaning: Boolean;
    mCutFactorArr: Array[cCutIndexChannel..cCutIndexFF] of Single;
    mCutList: TCutCurveList;
    mInitialized: Boolean;
    mMaxFieldCount: Integer;
    mQMatrix: TQualityMatrix;
    mSubMatrix: Array of Array of Single;
    mSubX: Integer;
    mSubY: Integer;
    mThickCurveType: TCurveTypes;
    mThinCurveType: TCurveTypes;
    function GetDebugString(ArrayOfSingles: array of Single): String;
    function CreateDebugString(QualityMatrix: TQualityMatrix): TStringList;
    procedure DebugQMatrix;
    function GetCTot(Index: Integer): Single;
    function GetCutInfo(Index: Integer): Single;
    function GetCuts(aIndex: Integer): Single;
    function GetFFCuts(Index: Integer): Single;
    function GetFFMatrix(Index: Integer): TQualityMatrix;
    function GetQualityMatrix(Index: Integer): TQualityMatrix;
    function GetSpliceCuts(Index: Integer): Single;
    function GetSpliceMatrix(Index: Integer): TQualityMatrix;
//    function IsClassField(aIndex: Integer): Boolean;
    function IsCurveField(aField: Integer; var aPercentage: Single): Boolean;
    procedure SetBaseData(aBaseData: TDetailDataRec);
    procedure SetFFMatrix(Index: Integer; Value: TQualityMatrix);
    procedure SetQualityMatrix(Index: Integer; const Value: TQualityMatrix);
    procedure SetSpliceMatrix(Index: Integer; Value: TQualityMatrix);
    function SummarizeCut(aMatrix: TQualityMatrix; aCutArr: array of TFieldDefRec): Single;
  protected
    procedure CalcChannel;
    procedure CalcFF;
    procedure CalcSplice;
    procedure CalcThick(aCurveType: TCurveTypes);
    procedure CalcThin(aCurveType: TCurveTypes);
    procedure CalcVirtualCuts(aFF: Boolean);
    procedure CheckActiveFields;
    procedure Clear;
    procedure CreateSubMatrix(aSubX, aSubY: Integer);
  public
    constructor Create; virtual;
    function DoVirtualCleaning(aInit: Boolean): Boolean;
    function Init(aMatrix: TQualityMatrix): Boolean;
    function InitDebug(aMatrix: TQualityMatrix): Boolean;
    procedure DebugVirtualCuts;
    property CDR: TClassDataReader read fCDR write fCDR;
    property ChannelCuts: Single index cCutIndexChannel read GetCutInfo;
    property CTot: Single index cCutIndexCTot read GetCTot;
    property Cuts[aIndex: Integer]: Single read GetCuts;
    property FFCuts: Single index cCutIndexFF read GetFFCuts;
    property SpliceCuts: Single index cCutIndexSplice read GetSpliceCuts;
  published
    property ChannelMatrix: TQualityMatrix index 0 read GetQualityMatrix write SetQualityMatrix;
    property DetailData: TDetailDataRec read fDetailData write SetBaseData;
    property FFMatrix: TQualityMatrix index 2 read GetFFMatrix write SetFFMatrix;
    property SpliceMatrix: TQualityMatrix index 1 read GetSpliceMatrix write SetSpliceMatrix;
  end;
  
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  RzCSIntf,
  SysUtils,
  QualityMatrixDef, QualityMatrixBase;

{:------------------------------------------------------------------------------
 TVirtualCuts}
{:-----------------------------------------------------------------------------}
procedure TVirtualCuts.CalcChannel;
begin
  if Assigned(fChannelMatrix) then begin
    CodeSite.EnterMethod('CalcChannel');

    Init(fChannelMatrix);

    CheckActiveFields;

    CalcThick(cvtNSL);
    CalcThin(cvtThin);

    CodeSite.SendFmtMsg('before SummarizeCut fVirtualCuts = %s', [GetDebugString(fVirtualCuts)]);

    fCutInfoArr[cCutIndexChannel] := SummarizeCut(fChannelMatrix, cChannelFields);
  {
    fCutInfoArr[cCutIndexCS] := SummarizeCut(fChannelMatrix, cCSFields);
    fCutInfoArr[cCutIndexCL] := SummarizeCut(fChannelMatrix, cCLFields);
    fCutInfoArr[cCutIndexCT] := SummarizeCut(fChannelMatrix, cCTFields);
  {}

    CodeSite.ExitMethod('CalcChannel');
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TVirtualCuts.CalcFF;
begin
  if Assigned(fFFMatrix) then begin
  //    CodeSite.EnterMethod('CalcFF');
    Init(fFFMatrix);
  
    CheckActiveFields;
    fCutInfoArr[cCutIndexFF] := SummarizeCut(fFFMatrix, cFFFields);
  //    CodeSite.ExitMethod('CalcFF');
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TVirtualCuts.CalcSplice;
begin
  if Assigned(fSpliceMatrix) then begin
    CodeSite.EnterMethod('CalcSplice');

    Init(fSpliceMatrix);
  
    CheckActiveFields;

    // PPW 20150319: Alse calc Thin for Splice
    CalcThick(cvtNSLSplice);
    CalcThin(cvtThinSplice);

    CodeSite.SendFmtMsg('before SummarizeCut fVirtualCuts = %s', [GetDebugString(fVirtualCuts)]);

    fCutInfoArr[cCutIndexSplice] := SummarizeCut(fSpliceMatrix, cSpliceFields);

    CodeSite.ExitMethod('CalcSplice');
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TVirtualCuts.CalcThick(aCurveType: TCurveTypes);
var
  i, j: Integer;
  xSingle: Single;
  xUnhandledCols1: TQMColIndexSet;
  xUnhandledCols2: TQMColIndexSet;
  xFirstField: Boolean;
  xIsCurveField: Boolean;
begin
  if not Assigned(mQMatrix) then
    Exit;

  mCutList := mQMatrix.GetCutCurve(aCurveType);
  if mCutList.Count > 0 then begin
    with mCutList do begin
{
      if SubFieldX = 0 then xSubX := mQMatrix.SubFieldX
                       else xSubX := SubFieldX;
      if SubFieldY = 0 then xSubY := mQMatrix.SubFieldY
                       else xSubY := SubFieldY;
      CreateSubMatrix(xSubX, xSubY);
{}
       CreateSubMatrix(SubFieldX, SubFieldY);
    {
      for i:=0 to Count-1 do
        CodeSite.SendFmtMsg('ThickField: %d; SubField: %s', [Items[i].Field, Items[i].SubFields]);
    {}
    end;


    xUnhandledCols1 := [];
    xFirstField     := False;
    // first: count through the short fields
    for i:=1 to 64 do begin
      j := (i-1) div 8;
      // at first curve field save information for later filling with 100%
      xIsCurveField := IsCurveField(i, xSingle);
      if xIsCurveField and not xFirstField then begin
        xFirstField := True;
        // filling col index starts here
        xUnhandledCols1 := [j..7];
      end;

      if xIsCurveField then begin
        if fVirtualCuts[i] = 0.0 then
          fVirtualCuts[i] := xSingle;
        // this col is handled now -> remove from set
        exclude(xUnhandledCols1, j);
        // define all field above this curve field as 100% cut
        with cThickFieldDef[j] do begin
          for j:=Low to High do
            if (fVirtualCuts[j] = 0.0) and (j > i) then
              fVirtualCuts[j] := 1;
        end;
      end;
    end;

    xUnhandledCols2 := [];
    xFirstField    := False;
    // second: now count through the long fields
    for i:=65 to mMaxFieldCount do begin
      j := (i-1) div 8;
      // at first curve field save information for later filling with 100%
      xIsCurveField := IsCurveField(i, xSingle);
      if xIsCurveField and not xFirstField then begin
        xFirstField := True;
        // filling col index starts here
        xUnhandledCols2 := [j..15];
      end;
      if xIsCurveField then begin
        if fVirtualCuts[i] = 0.0 then
          fVirtualCuts[i] := xSingle;
        // this col is handled now -> remove from set
        exclude(xUnhandledCols2, j);
        // define all field above this curve field as 100% cut
        with cThickFieldDef[j] do begin
          for j:=Low to High do
            if (fVirtualCuts[j] = 0.0) and (j > i) then
              fVirtualCuts[j] := 1;
        end;
      end;
    end;

    // now unhandled cols (above, right of curve) has to be set to 100%
    xUnhandledCols1 := xUnhandledCols1 + xUnhandledCols2;
    for i:=0 to 15 do begin
      if i in xUnhandledCols1 then begin
        with cThickFieldDef[i] do begin
          for j:=Low to High do
            fVirtualCuts[j] := 1;
        end;
      end;
    end;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TVirtualCuts.CalcThin(aCurveType: TCurveTypes);
var
  i, j: Integer;
  xSingle: Single;
  xUnhandledCols: TQMColIndexSet;
  xFirstField: Boolean;
  xIsCurveField: Boolean;
begin
  if not Assigned(mQMatrix) then
    Exit;
  
  mCutList := mQMatrix.GetCutCurve(aCurveType);
  with mCutList do begin
    CreateSubMatrix(SubFieldX, SubFieldY);
  {
    for i:=0 to Count-1 do
      CodeSite.SendFmtMsg('ThinField: %d; SubField: %s', [Items[i].Field, Items[i].SubFields]);
  {}
  end;
  
  xUnhandledCols := [];
  xFirstField    := False;
  // count only thin fields
  for i:=65 to mMaxFieldCount do begin
    // get the col index of current field
    j := (i-1) div 8;
  
    // check if this is a curve field and set some variables
    xIsCurveField := IsCurveField(i, xSingle);
    if xIsCurveField and not xFirstField then begin
      // at first curve field save information for later filling with 100%
      xFirstField := True;
      // filling col index starts here
      xUnhandledCols := [j..15];
    end;
  
    if xIsCurveField then begin
      // otherwise set the calcualted percentage for this field
      if fVirtualCuts[i] = 0.0 then
        fVirtualCuts[i] := xSingle;
      // this col is handled now -> remove from set
      exclude(xUnhandledCols, j);
      // define all field below this curve field as 100% cut
      with cThinFieldDef[j] do begin
        for j:=Low to High do
          if (fVirtualCuts[j] = 0.0) and (j < i) then
            fVirtualCuts[j] := 1;
      end;
    end;
  end;
  
  // now unhandled cols (below, right of curve) has to be set to 100%
  for i:=8 to 15 do begin
    if i in xUnhandledCols then begin
      with cThinFieldDef[i] do begin
        for j:=Low to High do
          fVirtualCuts[j] := 1;
      end;
    end;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TVirtualCuts.CalcVirtualCuts(aFF: Boolean);
begin
  // first: check for set class fields
  if aFF then begin
    CheckActiveFields;
  end else begin
    // second: check for curve
    CheckActiveFields;
    CalcThick(mThickCurveType);
    CalcThin(mThinCurveType);
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TVirtualCuts.CheckActiveFields;
var
  i: Integer;
begin
  CodeSite.EnterMethod('TVirtualCuts.CheckActiveFields');

  DebugQMatrix;

  for i:=1 to mMaxFieldCount do begin
    if mQMatrix.FieldState[i-1] then
      fVirtualCuts[i] := 1;
  end;

  CodeSite.SendFmtMsg('fVirtualCuts = %s', [GetDebugString(fVirtualCuts)]);
  CodeSite.ExitMethod('TVirtualCuts.CheckActiveFields');
end;

{:-----------------------------------------------------------------------------}
procedure TVirtualCuts.Clear;
begin
  mCheckClassCleaning := False;
  mCutList            := Nil;
  mInitialized        := False;
  mMaxFieldCount      := 0;

  FillChar(fVirtualCuts, sizeof(fVirtualCuts), 0);
end;

{:-----------------------------------------------------------------------------}
constructor TVirtualCuts.Create;
begin
  inherited Create;
  
  fChannelMatrix := Nil;
  fSpliceMatrix  := Nil;
  fFFMatrix      := Nil;
  
  Clear;
end;

{:-----------------------------------------------------------------------------}
procedure TVirtualCuts.CreateSubMatrix(aSubX, aSubY: Integer);
var
  i, j: Integer;
  xSingle: Single;
begin
  if (aSubX <> mSubX) or (aSubY <> mSubY) then begin
  //    CodeSite.SendFmtMsg('CreateSubMatrix with SubX=%d, SubY=%d', [aSubX, aSubY]);
    mSubX := aSubX;
    mSubY := aSubY;
  
    xSingle := 1.0 / mSubX;
  
    // redefine the SubMatrix
    SetLength(mSubMatrix, mSubX);
    for i:=0 to High(mSubMatrix) do begin
      SetLength(mSubMatrix[i], mSubY);
      for j:=0 to High(mSubMatrix[i]) do
        mSubMatrix[i,j] := (xSingle / mSubY) * (mSubY - j);
    end;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TVirtualCuts.DebugVirtualCuts;
var
  I: Integer;
  StringList: TStringList;
begin
  CodeSite.SendFmtMsg('fCutInfoArr = %s', [GetDebugString(fCutInfoArr)]);
  CodeSite.SendFmtMsg('mCutFactorArr = %s', [GetDebugString(mCutFactorArr)]);
  CodeSite.SendFmtMsg('fVirtualCuts = %s', [GetDebugString(fVirtualCuts)]);

  CodeSite.SendFmtMsg('ChannelMatrix:', []);
  StringList := CreateDebugString(ChannelMatrix);
  try
    for I:=0 to Pred(StringList.Count) do
    begin
      CodeSite.SendFmtMsg('%s', [StringList.Strings[I]]);
    end;
  finally
    StringList.Free;
  end;

  CodeSite.SendFmtMsg('SpliceMatrix:', []);
  StringList := CreateDebugString(SpliceMatrix);
  try
    for I:=0 to Pred(StringList.Count) do
    begin
      CodeSite.SendFmtMsg('%s', [StringList.Strings[I]]);
    end;
  finally
    StringList.Free;
  end;

  CodeSite.SendFmtMsg('FFMatrix:', []);
  StringList := CreateDebugString(FFMatrix);
  try
    for I:=0 to Pred(StringList.Count) do
    begin
      CodeSite.SendFmtMsg('%s', [StringList.Strings[I]]);
    end;
  finally
    StringList.Free;
  end;
end;

function TVirtualCuts.DoVirtualCleaning(aInit: Boolean): Boolean;
begin
  Result := False;
  try
    CalcChannel;
    CalcSplice;
    CalcFF;

    if aInit then begin
      if fCutInfoArr[cCutIndexChannel] > 0 then
        mCutFactorArr[cCutIndexChannel] := fDetailData.ChannelCuts / fCutInfoArr[cCutIndexChannel]
      else
        mCutFactorArr[cCutIndexChannel] := 1.0;

      if fCutInfoArr[cCutIndexSplice] > 0 then
        mCutFactorArr[cCutIndexSplice] := fDetailData.SpliceCuts / fCutInfoArr[cCutIndexSplice]
      else
        mCutFactorArr[cCutIndexSplice] := 1.0;

      if fCutInfoArr[cCutIndexFF] > 0 then
        mCutFactorArr[cCutIndexFF] := fDetailData.FFCuts / fCutInfoArr[cCutIndexFF]
      else
        mCutFactorArr[cCutIndexFF] := 1.0;
    end;
  
    Result := True;
  except
  end;
end;

{:-----------------------------------------------------------------------------}
function TVirtualCuts.GetCTot(Index: Integer): Single;
begin
  Result := GetCutInfo(Index);
end;

{:-----------------------------------------------------------------------------}
function TVirtualCuts.GetCutInfo(Index: Integer): Single;
var
  i: Integer;
begin
  if Index = cCutIndexCTot then begin
    Result := 0;
    for i:= cCutIndexChannel to cCutIndexFF do
      Result := Result + (fCutInfoArr[i] * mCutFactorArr[i]);
  end else
    Result := fCutInfoArr[Index] * mCutFactorArr[Index];
end;

{:-----------------------------------------------------------------------------}
function TVirtualCuts.GetCuts(aIndex: Integer): Single;
begin
  Result := fVirtualCuts[aIndex];
end;

{:-----------------------------------------------------------------------------}
function TVirtualCuts.GetDebugString(
  ArrayOfSingles: array of Single): String;
var
  I: Integer;
begin
  Result := '';
  for I:=Low(ArrayOfSingles) to High(ArrayOfSingles) do
  begin
    Result := Result + Format('%f, ', [ArrayOfSingles[I]]);
  end;
end;

{
    property Cuts[aFieldID: Integer]: Double index cCuts read GetValue write SetValue;
    property Defects[aFieldID: Integer]: Double index cDefects read GetValue write SetValue;
    property FieldDots[aFieldID: Integer]: Integer read GetFieldDots write SetFieldDots;
    property FieldState[aFieldID: Integer]: Boolean read GetFieldState write SetFieldState;
}
function TVirtualCuts.CreateDebugString(
  QualityMatrix: TQualityMatrix): TStringList;
var
  I: Integer;
  ArrayString: String;
  DebugString: TStringList;
begin
  Result := TStringList.Create();

  ArrayString := 'Cuts: ';
  for I:=0 to 127 do
  begin
    ArrayString := ArrayString + Format('%f, ', [QualityMatrix.Cuts[I]]);
  end;
  Result.Add(ArrayString);

  ArrayString := 'Defects: ';
  for I:=0 to 127 do
  begin
    ArrayString := ArrayString + Format('%f, ', [QualityMatrix.Defects[I]]);
  end;
  Result.Add(ArrayString);

  ArrayString := 'FieldDots: ';
  for I:=0 to 127 do
  begin
    ArrayString := ArrayString + Format('%d, ', [QualityMatrix.FieldDots[I]]);
  end;
  Result.Add(ArrayString);

  ArrayString := 'FieldState: ';
  for I:=0 to 127 do
  begin
    ArrayString := ArrayString + Format('%d, ', [Integer(QualityMatrix.FieldState[I])]);
  end;
  Result.Add(ArrayString);
end;

function TVirtualCuts.GetFFCuts(Index: Integer): Single;
begin
  Result := GetCutInfo(Index);
end;

{:-----------------------------------------------------------------------------}
function TVirtualCuts.GetFFMatrix(Index: Integer): TQualityMatrix;
begin
  Result := GetQualityMatrix(Index);
end;

{:-----------------------------------------------------------------------------}
function TVirtualCuts.GetQualityMatrix(Index: Integer): TQualityMatrix;
begin
  case Index of
    1: Result := fSpliceMatrix;
    2: Result := fFFMatrix;
  else
    Result := fChannelMatrix;
  end;
end;

{:-----------------------------------------------------------------------------}
function TVirtualCuts.GetSpliceCuts(Index: Integer): Single;
begin
  Result := GetCutInfo(Index);
end;

{:-----------------------------------------------------------------------------}
function TVirtualCuts.GetSpliceMatrix(Index: Integer): TQualityMatrix;
begin
  Result := GetQualityMatrix(Index);
end;

{:-----------------------------------------------------------------------------}
function TVirtualCuts.Init(aMatrix: TQualityMatrix): Boolean;
begin
  Result := True;
  Clear;

  mQMatrix := aMatrix;
  with mQMatrix do begin
    mCheckClassCleaning := (MatrixType = mtShortLongThin) or (MatrixType = mtSiro);

    case MatrixType of
      mtShortLongThin: begin
          mMaxFieldCount := cMaxFieldCount[0];
          mThickCurveType := cvtNSL;
          mThinCurveType  := cvtThin;
        end;
      mtSplice: begin
          mMaxFieldCount := cMaxFieldCount[1];
          mThickCurveType := cvtNSLSplice;
          mThinCurveType  := cvtThinSplice;
        end;
      mtSiro: begin
          mMaxFieldCount := cMaxFieldCount[2];
        end;
    else
    end;
  end;
end;

{:-----------------------------------------------------------------------------}
function TVirtualCuts.InitDebug(aMatrix: TQualityMatrix): Boolean;
begin
  Result := True;
  Clear;
  
  mQMatrix := aMatrix;
  with mQMatrix do begin
    mCheckClassCleaning := (MatrixType = mtShortLongThin) or (MatrixType = mtSiro);
  
    case MatrixType of
      mtShortLongThin: begin
          mMaxFieldCount := cMaxFieldCount[0];
          mThickCurveType := cvtNSL;
          mThinCurveType  := cvtThin;
        end;
      mtSplice: begin
          mMaxFieldCount := cMaxFieldCount[1];
          mThickCurveType := cvtNSLSplice;
          mThinCurveType  := cvtThinSplice;
        end;
      mtSiro: begin
          mMaxFieldCount := cMaxFieldCount[2];
        end;
    else
    end;
  
    mSubX := SubFieldX;
    mSubY := SubFieldY;
  end;
  CreateSubMatrix(mSubX, mSubY);
  
  CalcVirtualCuts(mQMatrix.MatrixType = mtSiro);
end;

{:-----------------------------------------------------------------------------}
//function TVirtualCuts.IsClassField(aIndex: Integer): Boolean;
//begin
//  Result := mQMatrix.FieldState[aIndex-1];
//end;

{:-----------------------------------------------------------------------------}
function TVirtualCuts.IsCurveField(aField: Integer; var aPercentage: Single): Boolean;
var
  i, j: Integer;
  xIndex: Integer;
begin
  Result := False;
  
  aPercentage := 0;
  // first check if the curve goes through the field
  for i:=0 to mCutList.Count-1 do begin
    Result := (mCutList.Items[i].Field = (aField-1));
    if Result then begin
  //      CodeSite.SendFmtMsg('Field: %d; SubFields: %s', [aField, mCutList.Items[i].SubFields]);
      // Yes, it's a curve field
      with TStringList.Create do
      try
        CommaText := mCutList.Items[i].SubFields;
        // calculate the percentage of cut
        for j:=0 to mSubX-1 do begin
          xIndex := StrToIntDef(Strings[j], -1);
          // -1 = curve is higher than subfield point -> no cut
          if xIndex > -1 then begin
            // 0 = curve is lower than subfile point -> handle as = 1
            if xIndex = 0 then
              xIndex := 1;
            aPercentage := aPercentage + mSubMatrix[j, xIndex-1];
          end;
        end;
        Break;
      finally
        Free;
      end;
    end;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TVirtualCuts.SetBaseData(aBaseData: TDetailDataRec);
begin
  fDetailData := aBaseData;
end;

{:-----------------------------------------------------------------------------}
procedure TVirtualCuts.SetFFMatrix(Index: Integer; Value: TQualityMatrix);
begin
  SetQualityMatrix(Index, Value);
end;

{:-----------------------------------------------------------------------------}
procedure TVirtualCuts.SetQualityMatrix(Index: Integer; const Value: TQualityMatrix);
begin
  case Index of
    1: fSpliceMatrix := Value;
    2: fFFMatrix := Value;
  else
    fChannelMatrix := Value;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TVirtualCuts.SetSpliceMatrix(Index: Integer; Value: TQualityMatrix);
begin
  SetQualityMatrix(Index, Value);
end;

{:-----------------------------------------------------------------------------}
function TVirtualCuts.SummarizeCut(aMatrix: TQualityMatrix; aCutArr: array of TFieldDefRec): Single;
var
  i, j: Integer;
begin
  // CodeSite.EnterMethod('TVirtualCuts.SummarizeCut');
  // CodeSite.SendFmtMsg('aMatrix = %s', [aMatrix.Name]);

  Result := 0.0;
  for i:=Low(aCutArr) to High(aCutArr) do begin
    for j:=aCutArr[i].Low to aCutArr[i].High do begin
      with aMatrix do begin
        //     qmSplice.GetValue(x-1, cDefects, mCDR.SpliceUncut[x]);
        // CodeSite.SendFmtMsg('(i,j): %d,%d; defects: %f; cuts: %f; virtual: %f', [i,j,GetValue(j-1, cDefects),GetValue(j-1, cCuts),fVirtualCuts[j]]);
        Result := Result + ((GetValue(j-1, cDefects) + GetValue(j-1, cCuts)) * fVirtualCuts[j]);
      end;
    end;
  end;

  // CodeSite.ExitMethod('TVirtualCuts.SummarizeCut');
end;

procedure TVirtualCuts.DebugQMatrix;
var
  I: Integer;
  StringList: TStringList;
begin
  if not Assigned(mQMatrix) then
     CodeSite.SendMsg('mQMatrix not Assigned!')
  else if mQMatrix = ChannelMatrix then
     CodeSite.SendMsg('mQMatrix is ChannelMatrix')
  else if mQMatrix = SpliceMatrix then
     CodeSite.SendMsg('mQMatrix is SpliceMatrix')
  else if mQMatrix = FFMatrix then
     CodeSite.SendMsg('mQMatrix is FFMatrix')
  else
     CodeSite.SendMsg('mQMatrix is Unknown!');

  if Assigned(mQMatrix) then
  begin
    CodeSite.SendFmtMsg('mQMatrix:', []);
    StringList := CreateDebugString(mQMatrix);
    try
      for I:=0 to Pred(StringList.Count) do
      begin
        CodeSite.SendFmtMsg('%s', [StringList.Strings[I]]);
      end;
    finally
      StringList.Free;
    end;
  end;
end;

end.

