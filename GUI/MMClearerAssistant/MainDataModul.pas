{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MainDataModul.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: DatenModule, Queries vom Reiniger Assistenten
| Info..........:
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 12.10.2000  1.00  SDo | File created
| 30.10.2002  1.00  Wss | Umbau ADO
| 15.11.2003  1.01  SDo | neue Func. SaveNewTemplateColor, Delete_YM_SetID_StyleID
|                       | New_YM_SetID_StyleID
| 27.07.2005  1.00  Wss | dseTemplate verwies noch auf t_ym_settings statt auf die XML Tabelle
===============================================================================}
unit MainDataModul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  mmDataSource, mmStringList, MMUGlobal, ClearerAssistantDef, ADODB, mmADODataSet,
  mmADOConnection, mmADOCommand, Db, ClassDataReader;

type
  {:----------------------------------------------------------------------------
   Data modul for Clearer Assistant.
   ----------------------------------------------------------------------------}
  TdmMain = class (TDataModule)
    dsWork: TmmDataSource;
    conDefault: TmmADOConnection;
    dseWork: TmmADODataSet;
    comWork: TmmADOCommand;
    dsTemplates: TmmDataSource;
    dseTemplates: TmmADODataSet;
    procedure dseWorkBeforeOpen(DataSet: TDataSet);
  private
    fCDR: TClassDataReader;
  public
    function ReadDetailData: TDetailDataRec;
    procedure SaveTemplateColor(aID, aColor: Integer);
    property CDR: TClassDataReader read fCDR write fCDR;
  end;

const
  cSaveTemplateColor = 'update t_xml_YM_Settings set c_color = %d ' +
                       'where c_ym_set_id = %d and c_template_set = 1';

var
  dmMain: TdmMain;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS, mmCS;

{:------------------------------------------------------------------------------
 TdmMain}
{:-----------------------------------------------------------------------------}
function TdmMain.ReadDetailData: TDetailDataRec;
var
  xSpdCount: Integer;
begin
  FillChar(Result, sizeof(Result), 0);
  if Assigned(fCDR) then
    with dseWork do
    try
      Close;
      CommandText := Format(fCDR.GetBaseSQL, [cFldDetailData]) + ' GROUP BY c_spindle_first, c_spindle_last';
      CodeSite.SendString('DetailData', CommandText);
      Open;
      with Result do begin
        while not EOF do
        try
          tRu      := tRu  + FieldByName('tRu').AsFloat;
          tOp      := tOp  + FieldByName('tOp').AsFloat;
          tWa      := tWa  + FieldByName('tWa').AsFloat;
          tLSt     := tLSt + FieldByName('tLSt').AsFloat;
          SpdCount := FieldByName('SpdRange').AsInteger;

          if fCDR.Params.TimeMode = tmInterval then
            xSpdCount := SpdCount
          else
            xSpdCount := 1;

          tRuSpd  := tRuSpd  + (FieldByName('tRu').AsFloat / xSpdCount);
          tOpSpd  := tOpSpd  + (FieldByName('tOp').AsFloat / xSpdCount);
          tWaSpd  := tWaSpd  + (FieldByName('tWa').AsFloat / xSpdCount);
          tLStSpd := tLStSpd + (FieldByName('tLSt').AsFloat / xSpdCount);

          Kg        := Kg   + FieldByName('Kg').AsFloat;
          Sp        := Sp   + FieldByName('Sp').AsFloat;
          // base cut data
          ChannelCuts := ChannelCuts + FieldByName('ChannelCuts').AsFloat;
          SpliceCuts  := SpliceCuts  + FieldByName('SpliceCuts').AsFloat;
          FFCuts      := FFCuts      + FieldByName('FFCuts').AsFloat;
          CYTot       := CYTot       + FieldByName('CYTot').AsFloat;

          Next;
        except
        end;
    {
        tWaSpd    := tWaSpd / i;
        tOpSpd    := tOpSpd / i;
        tRuSpd    := tRuSpd / i;
    {}
        // calculate virtual 100% value
        t100      := (tRu + tOp) / 2;
        t100LSt   := (tRu + tOp + tLSt) / 2;
        // calculate virtual 100% value for one spindle
        t100Spd      := (tRuSpd + tOpSpd) / 2;
        t100LStSpd   := (tRuSpd + tOpSpd + tLStSpd) / 2;
        // calculate some others
        TotCuts     := ChannelCuts + SpliceCuts + FFCuts;
        SpBase      := Sp - TotCuts;
        if Sp > 0 then begin
          tSpCycle1 := (tOP - tRu) / Sp;
          tSpCycle2 := (tOP - tRu - tLSt) / Sp;
        end else begin
          tSpCycle1 := 30;
          tSpCycle2 := 10;
        end;
      end;
    finally
      try
        Close;
      except
      end;
    end;
end;

{:-----------------------------------------------------------------------------}
procedure TdmMain.SaveTemplateColor(aID, aColor: Integer);
begin
  with comWork do
  try
    CommandText := Format(cSaveTemplateColor, [aColor, aId]);
    Execute;
  finally
  end;
end;
//------------------------------------------------------------------------------
procedure TdmMain.dseWorkBeforeOpen(DataSet: TDataSet);
var
  xParam: TParameter;
begin
  if Assigned(fCDR) then
    with TmmADODataSet(DataSet) do begin
      xParam := Parameters.FindParam('TimeFrom');
      if  xParam <> Nil then begin
        xParam.DataType := ftDateTime;   //NUE: Achtung TmmADOQuery hat ein Problem mit DateTime, wenn dar DataType nicht zuvor auf ftDateTime gesetzt wird!!
        xParam.Value := fCDR.Params.TimeFrom;
        CodeSite.SendString('TimeFrom', DateTimeToStr(fCDR.Params.TimeFrom));
      end;
      xParam := Parameters.FindParam('TimeTo');
      if  xParam <> Nil then begin
        xParam.DataType := ftDateTime;   //NUE: Achtung TmmADOQuery hat ein Problem mit DateTime, wenn dar DataType nicht zuvor auf ftDateTime gesetzt wird!!
        xParam.Value := fCDR.Params.TimeTo;
        CodeSite.SendString('TimeTo', DateTimeToStr(fCDR.Params.TimeTo));
      end;
    end;
end;

end.



