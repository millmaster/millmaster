inherited frmMain: TfrmMain
  Left = 483
  Top = 152
  Width = 667
  Height = 485
  Caption = '(*)Reiniger Assistent'
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited plTopPanel: TmmPanel
    Width = 659
    inherited Panel2: TmmPanel
      Left = 595
    end
    inherited mToolBar: TmmToolBar
      Width = 595
    end
  end
  inherited mmStatusBar: TmmStatusBar
    Top = 413
    Width = 659
  end
  inherited mTranslator: TmmTranslator
    OnLanguageChange = mTranslatorLanguageChange
  end
  inherited mDictionary: TmmDictionary
    AutoConfig = True
    OnAfterLangChange = mDictionaryAfterLangChange
  end
  object MMSecurityDB: TMMSecurityDB
    Active = True
    CheckApplStart = False
    Left = 224
    Top = 72
  end
  object MMHtmlHelp: TMMHtmlHelp
    AutoConfig = True
    MMHelp = mmpEasy
    Left = 144
    Top = 72
  end
end
