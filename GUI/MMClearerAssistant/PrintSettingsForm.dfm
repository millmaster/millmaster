inherited frmPrintSettings: TfrmPrintSettings
  Left = 668
  Top = 322
  BorderStyle = bsDialog
  BorderWidth = 3
  Caption = '(*)Drucker'
  ClientHeight = 441
  ClientWidth = 337
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object mmLabel1: TmmLabel
    Left = 8
    Top = 291
    Width = 40
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = '(*)Serien'
    FocusControl = clbSeries
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLineLabel5: TmmLineLabel
    Left = 0
    Top = 136
    Width = 337
    Height = 17
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = '(*)Filterwerte anzeigen'
    Distance = 4
  end
  object mmLineLabel1: TmmLineLabel
    Left = 0
    Top = 192
    Width = 337
    Height = 17
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = '(20)Reinigereinstellungen'
    Distance = 4
  end
  object bCancel: TmmButton
    Left = 240
    Top = 412
    Width = 97
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '(9)Abbrechen'
    ModalResult = 2
    TabOrder = 0
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object bPreview: TmmButton
    Left = 240
    Top = 324
    Width = 97
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '(10)Voransicht'
    TabOrder = 1
    Visible = False
    OnClick = bPrintClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mPrintSetup: TFramePrintSetup
    Left = 0
    Top = 0
    Width = 337
    Height = 129
    Align = alTop
    TabOrder = 2
    Caption = '(*)Drucker'
    OnPrintSetupChanged = mPrintSetupPrintSetupChanged
    PrintSetup.Copies = 0
    PrintSetup.FromPage = 0
    PrintSetup.MaxPage = 0
    PrintSetup.MinPage = 0
    PrintSetup.Options = []
    PrintSetup.PrintRange = prAllPages
    PrintSetup.ToPage = 0
  end
  object clbSeries: TmmCheckListBox
    Left = 8
    Top = 306
    Width = 145
    Height = 131
    Anchors = [akLeft, akBottom]
    ItemHeight = 13
    TabOrder = 3
    Visible = True
    AutoLabel.Control = mmLabel1
    AutoLabel.LabelPosition = lpTop
  end
  object bPrint: TmmButton
    Tag = 1
    Left = 240
    Top = 381
    Width = 97
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '(10)Drucken'
    ModalResult = 1
    TabOrder = 4
    Visible = True
    OnClick = bPrintClick
    AutoLabel.LabelPosition = lpLeft
  end
  object cbShowClassData: TmmCheckBox
    Tag = 6
    Left = 8
    Top = 263
    Width = 161
    Height = 17
    Caption = '(*)Klassierdaten anzeigen'
    TabOrder = 5
    Visible = True
    OnClick = OnSettingsClick
    AutoLabel.LabelPosition = lpLeft
  end
  object cbFilterStyle: TmmCheckBox
    Left = 8
    Top = 152
    Width = 152
    Height = 17
    Caption = '(*)Artikel'
    TabOrder = 6
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object cbFilterLot: TmmCheckBox
    Tag = 1
    Left = 8
    Top = 169
    Width = 153
    Height = 17
    Caption = '(*)Partie'
    TabOrder = 7
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object cbFilterMachine: TmmCheckBox
    Tag = 2
    Left = 172
    Top = 152
    Width = 152
    Height = 17
    Caption = '(*)Maschine'
    TabOrder = 8
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object cbFilterYMSettings: TmmCheckBox
    Tag = 3
    Left = 172
    Top = 169
    Width = 153
    Height = 17
    Caption = '(*)Reinigereinstellung'
    TabOrder = 9
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object cbChannel: TmmCheckBox
    Left = 8
    Top = 212
    Width = 153
    Height = 17
    Caption = '(14)Kanal'
    TabOrder = 10
    Visible = True
    OnClick = OnSettingsClick
    AutoLabel.LabelPosition = lpLeft
  end
  object cbSplice: TmmCheckBox
    Tag = 1
    Left = 8
    Top = 229
    Width = 153
    Height = 17
    Caption = '(14)Spleiss'
    TabOrder = 11
    Visible = True
    OnClick = OnSettingsClick
    AutoLabel.LabelPosition = lpLeft
  end
  object cbYarnCount: TmmCheckBox
    Tag = 2
    Left = 8
    Top = 246
    Width = 153
    Height = 17
    Caption = '(11)Garnnummer'
    TabOrder = 12
    Visible = True
    OnClick = OnSettingsClick
    AutoLabel.LabelPosition = lpLeft
  end
  object cbCluster: TmmCheckBox
    Tag = 3
    Left = 172
    Top = 212
    Width = 153
    Height = 17
    Caption = '(15)Fehlerschwarm'
    TabOrder = 13
    Visible = True
    OnClick = OnSettingsClick
    AutoLabel.LabelPosition = lpLeft
  end
  object cbSFI: TmmCheckBox
    Tag = 4
    Left = 172
    Top = 229
    Width = 153
    Height = 17
    Caption = '(11)SFI'
    TabOrder = 14
    Visible = True
    OnClick = OnSettingsClick
    AutoLabel.LabelPosition = lpLeft
  end
  object cbFFCluster: TmmCheckBox
    Tag = 5
    Left = 172
    Top = 246
    Width = 153
    Height = 17
    Caption = '(11)FF-Cluster'
    TabOrder = 15
    Visible = True
    OnClick = OnSettingsClick
    AutoLabel.LabelPosition = lpLeft
  end
  object cbVCV: TmmCheckBox
    Tag = 7
    Left = 172
    Top = 263
    Width = 153
    Height = 17
    Caption = '(*)VCV'
    TabOrder = 16
    Visible = True
    OnClick = OnSettingsClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 96
    Top = 32
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
end
