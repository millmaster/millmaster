unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEAPPLMAIN, Menus, mmMainMenu, IvDictio, IvAMulti, IvBinDic,
  mmDictionary, IvMulti, IvEMulti, mmTranslator, mmLogin, mmPopupMenu,
  mmOpenDialog, IvMlDlgs, mmSaveDialog, mmPrintDialog,
  mmPrinterSetupDialog, Db, DBTables, mmQuery, mmDatabase, StdActns,
  ActnList, mmActionList, ImgList, mmImageList, ComCtrls, mmStatusBar,
  ToolWin, mmToolBar, mmAnimate, ExtCtrls, mmPanel, MMSecurity, MMHtmlHelp;

type
  {:----------------------------------------------------------------------------
   Main form of Clearer Assistant. Used just for a container for Floor plugin. Complete functionality is implemented in the 
   data form.
   ----------------------------------------------------------------------------}
  TfrmMain = class (TBaseApplMainForm)
    MMSecurityDB: TMMSecurityDB;
    MMHtmlHelp: TMMHtmlHelp;
    procedure acLogin1Execute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acNewExecute(Sender: TObject);
    procedure mTranslatorLanguageChange(Sender: TObject);
    procedure mDictionaryAfterLangChange(Sender: TObject);
  end;
  
var
  frmMain: TfrmMain;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
  mmCS,
  RzCSIntf,
 DataForm, ClearerAssistantDef;
{$R *.DFM}

const
  cUnusedComponents: Array[1..19] of String = (
    'acSave', 'acSaveas', 'acOpen', 'acPrint', 'acPreview', 'acPrinterSetup', 'acInfo',
    'acWindowCascade', 'acWindowTileVertical', 'acTileHorizontally',
    'miSave', 'miSaveAs', 'miOpen', 'miPrint', 'miPreview', 'miPrinterSetup', 'miInfo',
    'miFenster',
    'miBearbeiten'
  );

procedure TfrmMain.acLogin1Execute(Sender: TObject);
begin
  inherited;
end;

{:------------------------------------------------------------------------------
 TfrmMain}
{:-----------------------------------------------------------------------------}
procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ActiveMDIChild.Free;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmMain.FormCreate(Sender: TObject);
var
  i: Integer;
  xComp: TComponent;
begin
  // cleanup unused components: TActions and TMenuItems
  for i:=1 to High(cUnusedComponents) do begin
    xComp := FindComponent(cUnusedComponents[i]);
    if Assigned(xComp) then
      FreeAndNil(xComp);
  end;

end;

{:-----------------------------------------------------------------------------}
procedure TfrmMain.FormShow(Sender: TObject);
begin
  if DebugHook <> 0 then begin
    with TfrmData.Create(Self) do
      WindowState := wsMaximized;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmMain.acNewExecute(Sender: TObject);
begin
  with TfrmData.Create(Self) do
    WindowState := wsMaximized;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmMain.mTranslatorLanguageChange(Sender: TObject);
begin
  Self.Caption := rsApplCaption;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmMain.mDictionaryAfterLangChange(Sender: TObject);
begin
  MMHtmlHelp.LoepfeIndex := mDictionary.LoepfeIndex;
end;

end.

