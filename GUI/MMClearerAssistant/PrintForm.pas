{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: PrintForm.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Druckvorbereitung fuer PreView & Print
| Info..........: Printout fuer A4 & Letter
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 15.10.2001  1.00  SDo | File created
| 19.02.2004  1.00  Wss | - Ausdruck angepasst (Layout)
| 28.05.2004  1.01  SDo | - Layout angepasst (Vereinheitichung nach Support)
| 12.01.2005  1.03  SDo | Umbau & Anpassungen an XML
| 03.05.2005  1.04  SDo | Printout XMLSettings Boxen neu positioniert
| 09.06.2005  1.04  SDo | Printout Boxen neu positioniert
| 15.06.2005  1.05  SDo | Settings Box 'P-Konfiguration' (sgPExt) wird nicht angezeigt; 15.06.2005 Mj
|                       | Printout Boxen neu positioniert
| 21.02.2006  1.06  Nue | ConvertYarnCountFloatToInt eliminiert.
| 14.02.2008  1.07  Nue | Einbau VCV
===============================================================================}
unit PrintForm;        

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, YMQRUnit, IvDictio, IvMulti, IvEMulti, mmTranslator, QuickRpt,
  Qrctrls, mmQRSysData, mmQRShape, mmQRLabel, BaseForm,
  mmQuickRep, QualityMatrixDef,
  PrintSettingsForm, ClearerAssistantDef, mmStringGrid, mmQRImage, mmQRRichText,
  SettingsReader, XMLSettingsModel, VCLXMLSettingsModel, YMParaEditBox,
  StdCtrls, mmGroupBox, EditBox;


type
  {:----------------------------------------------------------------------------
   Print selected settings on seperate pages by using QuickReport components.
   ----------------------------------------------------------------------------}
  TfrmPrint = class (TmmForm)
    ColumnHeaderBand1: TQRBand;
    qrlFromLabel: TmmQRLabel;
    mmQRLabel10: TmmQRLabel;
    mmQRLabel12: TmmQRLabel;
    qrlToLabel: TmmQRLabel;
    mmQRLabel3: TmmQRLabel;
    qlEff: TmmQRLabel;
    mmQRLabel6: TmmQRLabel;
    qlDuration: TmmQRLabel;
    qsL2: TmmQRShape;
    qsL3: TmmQRShape;
    qrLine: TmmQRShape;
    qsL4: TmmQRShape;
    qsL5: TmmQRShape;
    mmQRSysData1: TmmQRSysData;
    mmTranslator1: TmmTranslator;
    mQuickReport: TmmQuickRep;
    PageHeaderBand1: TQRBand;
    qbDetail: TQRBand;
    qbFooter: TQRBand;
    qbTitle: TQRBand;
    qlActHeader: TmmQRLabel;
    qlChannel: TmmQRLabel;
    qlChannel0: TmmQRLabel;
    qlChannel1: TmmQRLabel;
    qlDuration0: TmmQRLabel;
    qlDuration1: TmmQRLabel;
    qlEff0: TmmQRLabel;
    qlEff1: TmmQRLabel;
    qlFF: TmmQRLabel;
    qlFF0: TmmQRLabel;
    qlFF1: TmmQRLabel;
    qlLot: TmmQRLabel;
    qlMachine: TmmQRLabel;
    qlPageValue: TmmQRSysData;
    qlSerieHeader: TmmQRLabel;
    qlSerieNr: TmmQRLabel;
    qlShiftFrom: TmmQRLabel;
    qlShiftTo: TmmQRLabel;
    qlSplice: TmmQRLabel;
    qlSplice0: TmmQRLabel;
    qlSplice1: TmmQRLabel;
    qlStyle: TmmQRLabel;
    qlTitle: TmmQRLabel;
    qlTotal: TmmQRLabel;
    qlTotal0: TmmQRLabel;
    qlTotal1: TmmQRLabel;
    qlUnitLabel: TmmQRLabel;
    qlVirtualCuts: TmmQRLabel;
    qlYMSettings: TmmQRLabel;
    qrmChannel: TQRQMatrix;
    qrmSiro: TQRQMatrix;
    qrmSplice: TQRQMatrix;
    qsFrame: TmmQRShape;
    qsHeader: TmmQRShape;
    qsL1: TmmQRShape;
    qsSp2: TmmQRShape;
    qsSP1: TmmQRShape;
    qlCompanyName: TmmQRLabel;
    qlYClass: TmmQRLabel;
    qlForeignMatter: TmmQRLabel;
    qlSpliceClass: TmmQRLabel;
    mmQRImage1: TmmQRImage;
    qlProdQuantityLabel: TmmQRLabel;
    qlProdQuantityValue: TmmQRLabel;
    XMLSettingsModel: TVCLXMLSettingsModel;
    XMLSettingsChannel: TQRXMLSettings;
    XMLSettingsYarnCount: TQRXMLSettings;
    XMLSettingsSFI: TQRXMLSettings;
    XMLSettingsFCluster: TQRXMLSettings;
    XMLSettingsSplice: TQRXMLSettings;
    XMLSettingsCluster: TQRXMLSettings;
    XMLSettingsPSet: TQRXMLSettings;
    XMLSettingsRepetition: TQRXMLSettings;
    XMLSettingsVCV: TQRXMLSettings;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure mQuickReportBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure mQuickReportNeedData(Sender: TObject; var MoreData: Boolean);
    procedure qlPageValuePrint(sender: TObject; var Value: String);
    procedure qbDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    fGrid: TmmStringGrid;
    fReportInfo: TReportInfoRec;
    mPageIndex: Integer;
    mSeriesList: TStringList;
    mDetailBandHeight : Integer;

    procedure SetGrid(const Value: TmmStringGrid);
    procedure PrepareDetailBand;

  public
    property Grid: TmmStringGrid read fGrid write SetGrid;
    property ReportInfo: TReportInfoRec read fReportInfo write fReportInfo;
  end;

var
  frmPrint: TfrmPrint;

const cYFaktor = 248;

implementation
{$R *.DFM}
uses
  mmMBCS,
  MMUGlobal;


{:------------------------------------------------------------------------------
 TfrmPrint}
{:-----------------------------------------------------------------------------}
procedure TfrmPrint.FormCreate(Sender: TObject);
begin

  mQuickReport.ReportTitle := Translate(qlTitle.Caption);

  mSeriesList := TStringList.Create;
  fGrid := Nil;

  qlShiftFrom.Caption  := ReportInfo.TimeInfo.FromTime;
  qlShiftTo.Caption    := ReportInfo.TimeInfo.ToTime;

  qlVirtualCuts.Caption := StringReplace(qlVirtualCuts.Caption, ':', '', [rfReplaceAll]);
end;

{:-----------------------------------------------------------------------------}
procedure TfrmPrint.FormDestroy(Sender: TObject);
begin
 try
  FreeAndNil(mSeriesList);
 finally
 end;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmPrint.mQuickReportBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
var xRightMargin, yOffSet, yOffSet1 : integer    ;
begin

  mPageIndex := 0;

  yOffSet  := 10;
  yOffSet1 := 10;

  mSeriesList.CommaText := fReportInfo.SelectedSeries;

  qlShiftFrom.Caption := fReportInfo.TimeInfo.FromTime;
  qlShiftTo.Caption := fReportInfo.TimeInfo.ToTime;



  // actual values has to be printed always
  with fGrid.Cols[1] do begin
    qlChannel0.Caption  := Strings[1];
    qlSplice0.Caption   := Strings[2];
    qlFF0.Caption       := Strings[3];
    qlTotal0.Caption    := Strings[4];
    qlEff0.Caption      := Strings[5];
    qlDuration0.Caption := Strings[6];
  end;

  // delete caption of series values
  qlSerieHeader.Caption := '';
  qlChannel1.Caption    := '';
  qlSplice1.Caption     := '';
  qlFF1.Caption         := '';
  qlTotal1.Caption      := '';
  qlEff1.Caption        := '';
  qlDuration1.Caption   := '';



  //Positionierung an linken & rechten Rand
  //+++++++++++++++++++++++++++++++++++++++

  //Positionierung
  qlShiftFrom.Left :=  qrlFromLabel.Left + qrlFromLabel.Width + 5;
  qlShiftTo.Left :=  qrlToLabel.Left + qrlToLabel.Width + 5;

  //Produzierte Menge
  qlProdQuantityLabel.Left :=  qlSerieNr.Left + qlSerieNr.Width + 20;
  qlProdQuantityValue.Left :=  qlProdQuantityLabel.Left + qlProdQuantityLabel.Width + 5;

  //Breite von Horiz. Linie im Header setzten
  qrLine.Width := qbTitle.Width;

  //Detail-Band mit Titel
  mDetailBandHeight := mQuickReport.Height - cYFaktor;



  //Matrix & Boxen an linken & oberer Rand positionieren
  qlYClass.Left := 0;
  qrmChannel.Left := qlYClass.Left;
  qlSpliceClass.Left := qlYClass.Left;

  //Channel Matrix
  qlYClass.Top    := 5;
  qrmChannel.Top  := qlYClass.Top + qlYClass.Height + 3;

  xRightMargin := qrLine.Left + qrLine.Width;

  //Siro Matrix
  qrmSiro.Top         := qrmChannel.Top;
  qlForeignMatter.Top := qlYClass.Top;

  qrmSiro.Left         := xRightMargin - qrmSiro.Width -2;
  qlForeignMatter.Left := qrmSiro.Left;

  //Channel Box
  XMLSettingsChannel.Top  := qlYClass.Top;
  XMLSettingsChannel.Left := qrmChannel.Left + qrmChannel.Width -  XMLSettingsChannel.Width;


  //Splice Matrix
  qlSpliceClass.Top := qrmChannel.Top + qrmChannel.Height + 20;
  qrmSplice.Top := qlSpliceClass.Top  + qlSpliceClass.Height + 3;

  //Splice Box
  XMLSettingsSplice.Top :=  qlSpliceClass.Top;
  XMLSettingsSplice.Left := XMLSettingsChannel.Left;


  if qrmSiro.QMatrix.MatrixSubType = mstZenitF  then begin
     //Boxen zusammenruecken  (Siro Dark & Bright)
     yOffSet  := 0;
     yOffSet1 := 5;
  end;

  //Channel & Siro Matrixen sind Ref.Positionen fuer die Settings-Boxen

  //FCluster Box (Top / Left)
  XMLSettingsFCluster.Top  := qrmSiro.Top + qrmSiro.Height  + yOffSet ;
  XMLSettingsFCluster.Left := xRightMargin  - XMLSettingsFCluster.Width - 2;

  //Garnummer Box (Top / Left)
  XMLSettingsYarnCount.Top  := XMLSettingsFCluster.Top + XMLSettingsFCluster.Height + yOffSet1;
  XMLSettingsYarnCount.Left := xRightMargin - XMLSettingsYarnCount.Width - 2;


  //RepetitionBox  (Top / Left)
  XMLSettingsRepetition.Top  := XMLSettingsYarnCount.Top + XMLSettingsYarnCount.Height + yOffSet1;
  XMLSettingsRepetition.Left := xRightMargin - XMLSettingsRepetition.Width - 2;

    //P-Set Box  (Top / Left)
  XMLSettingsPSet.Top  :=  XMLSettingsRepetition.Top;
  XMLSettingsPSet.Left :=  qrmChannel.Left + qrmChannel.Width  + 15;

  //SFI Box (Top / Left)
  XMLSettingsSFI.Top  := XMLSettingsPSet.Top + XMLSettingsPSet.Height + 10;
  XMLSettingsSFI.Left := qrmChannel.Left + qrmChannel.Width  + 15;


  //VCV Box (Top / Left)
  XMLSettingsVCV.Top  := XMLSettingsSFI.Top + XMLSettingsSFI.Height + 10;
  XMLSettingsVCV.Left := qrmChannel.Left + qrmChannel.Width  + 15;


  //Fehlerschwarm Box (left)
  XMLSettingsCluster.Left := xRightMargin - XMLSettingsCluster.Width - 2;


  try
   qlCompanyName.Caption := TMMSettingsReader.Instance.Value[cCompanyName];
  except
   qlCompanyName.Caption :='';
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmPrint.mQuickReportNeedData(Sender: TObject; var MoreData: Boolean);
var
  xSerie: Integer;
begin

  if mPageIndex < mSeriesList.Count then begin

    // get the index of this serie in data grid
    xSerie := StrToInt(mSeriesList.Strings[mPageIndex]);
    qlSerieNr.Caption     := fGrid.Cells[xSerie,0];

    qlProdQuantityValue.Caption := ReportInfo.ProdKg;

    // fill in virtual data only if not actual index
    if xSerie > 1 then begin
      qlSerieHeader.Caption := fGrid.Cells[xSerie,0];
      with fGrid.Cols[xSerie] do begin
        qlChannel1.Caption  := Strings[1];
        qlSplice1.Caption   := Strings[2];
        qlFF1.Caption       := Strings[3];
        qlTotal1.Caption    := Strings[4];
        qlEff1.Caption      := Strings[5];
        qlDuration1.Caption := Strings[6];
      end;
      qlProdQuantityValue.Caption := '--';
    end;




    // get YMSettings and distribute to QMatrix and Settings objects
    with TSettings(fGrid.Objects[xSerie,0]) do begin
      //06.01.2005 SDO
      {
      qrmChannel.PutYMSettings(YMSettings);
      qrmSplice.PutYMSettings(YMSettings);
      qrmSiro.PutYMSettings(YMSettings);

      XMLSettingsChannel.PutYMSettings(YMSettings);
      XMLSettingsFCluster.PutYMSettings(YMSettings);
      qrsSplice.PutYMSettings(YMSettings);
      }

      XMLSettingsModel.xmlAsString :=  XMLData;

      

    end;

    with fReportInfo do begin
//      XMLSettingsChannel.YarnCount := ConvertYarnCountFloatToInt(YarnCount);
      XMLSettingsChannel.YarnCount := YarnCount;
      XMLSettingsChannel.YarnUnit  := YarnUnit;
    end;

    if qrmSiro.QMatrix.Model.xmlAsString = '' then begin
    //if not qrmSiro.QMatrix.QMatrix.HasSiroData then begin   //06.01.2005 SDO
       qlForeignMatter.Caption:= '';
       qrmSiro.Frame.DrawTop := TRUE;
       qrmSiro.Frame.DrawBottom := TRUE;
       qrmSiro.Frame.DrawLeft := TRUE;
       qrmSiro.Frame.DrawRight := TRUE;
    end;

    inc(mPageIndex);  
    MoreData:= TRUE;
  end else
    MoreData:= FALSE;
end;

{:-----------------------------------------------------------------------------}
procedure TfrmPrint.qlPageValuePrint(sender: TObject; var Value: String);
begin
//  Value := Format('%s/%d', [Value, mQuickReport.PageCount]);
  Value := Format('%s', [Value]);
end;

{:-----------------------------------------------------------------------------}
procedure TfrmPrint.SetGrid(const Value: TmmStringGrid);
begin
  fGrid := Value;
end;
{:-----------------------------------------------------------------------------}
procedure TfrmPrint.qbDetailBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrepareDetailBand;
end;
{:-----------------------------------------------------------------------------}

//******************************************************************************
//Kompomponenten vertikal ausrichten
//******************************************************************************
procedure TfrmPrint.PrepareDetailBand;
var xBottom, xHeight : integer;
begin

  qbDetail.Height :=  mDetailBandHeight;;
  xBottom := mDetailBandHeight + 10;

  xHeight := 22;

  //Frame Texte
  qlDuration.Top := xBottom - qlDuration.Height ;
  qlEff.Top      := qlDuration.Top - xHeight;
  qlTotal.Top    := qlEff.Top - xHeight;
  qlFF.Top       := qlTotal.Top - xHeight;
  qlSplice.Top   := qlFF.Top - xHeight;
  qlChannel.Top  := qlSplice.Top - xHeight;
  qlUnitLabel.Top:= qlChannel.Top - xHeight;

  qlDuration0.Top := qlDuration.Top;
  qlEff0.Top      := qlEff.Top;
  qlTotal0.Top    := qlTotal.Top;
  qlFF0.Top       := qlFF.Top;
  qlSplice0.Top   := qlSplice.Top;
  qlChannel0.Top  := qlChannel.Top;
  qlActHeader.Top := qlUnitLabel.Top;

  qlDuration1.Top := qlDuration.Top;
  qlEff1.Top      := qlEff.Top;
  qlTotal1.Top    := qlTotal.Top;
  qlFF1.Top       := qlFF.Top;
  qlSplice1.Top   := qlSplice.Top;
  qlChannel1.Top  := qlChannel.Top;
  qlSerieHeader.Top:= qlUnitLabel.Top;

  qlVirtualCuts.Top := qlSerieHeader.Top - qlVirtualCuts.Height;

  //Frame Linien
  qsHeader.Top := qlUnitLabel.Top - 3;
  qsFrame.Top  := qsHeader.Top +  qsHeader.Height - 1;

  qsFrame.Height := (qlDuration.Top - qsFrame.Top) + qlDuration.Height + 3;

  xHeight := (xHeight DIV 2) + 1;

  qsL1.Top  := qlSplice.Top - xHeight;
  qsL2.Top  := qlFF.Top - xHeight;
  qsL3.Top  := qlTotal.Top - xHeight;
  qsL4.Top  := qlEff.Top - xHeight;
  qsL5.Top  := qlDuration.Top - xHeight;

  qsSP1.Top := qsHeader.Top;
  qsSP1.Height := qsFrame.Height + qsHeader.Height - 1;
  qsSp2.Top := qsHeader.Top;
  qsSP2.Height := qsSP1.Height;


  //Splice Seiten anpaseen -> im Letter-Format etwas kleiner
  qrmSplice.Width := Round(mQuickReport.Height * 0.43);
  qrmSplice.Height:= Round(mQuickReport.Height * 0.27);


  if (qrmSiro.QMatrix.MatrixSubType = mstZenitF) and  (mQuickReport.Height <1123) then
      XMLSettingsCluster.Top :=  qsFrame.Top + qsFrame.Height - XMLSettingsCluster.Height
  else
      XMLSettingsCluster.Top :=  qsHeader.Top;


  //Positioneirung Siro
//  qlForeignMatter.Top := qlVirtualCuts.Top;
//  qrmSiro.Top :=  qsHeader.Top ;

  {
  xHeight:= qlVirtualCuts.Top - qrmChannel.Top - qrmChannel.Height - qlSpliceClass.Height - 5;

  if qrmSplice.Height > xHeight then begin
     y:= qrmSplice.Height;
     x:= qrmSplice.Height - xHeight;

     //Hoehe anpassen
     qrmSplice.Height := qrmSplice.Height - x - 5;
     //mit Faktor berkleinern
     qrmSplice.Width  := qrmSplice.Width * Round(qrmSplice.Height / y);
  end;
  }


  //Hoehe Detail-Band ohne Titel
  mDetailBandHeight := mQuickReport.Height - cYFaktor + qbTitle.Height;
end;
//------------------------------------------------------------------------------



(*
constructor TfrmPrint.Create(aOwner: TComponent;  aModel: TVCLXMLSettingsModel);
var //xVCLXMLSettingsModel : TVCLXMLSettingsModel;
    x, y: Integer;
    xRet :Boolean;
   xForm : TForm;
begin
  inherited Create(aOwner);

  XMLSettingsModel:=  aModel;
  
  {
  x:=0;
  y:=0;
  xRet := FALSE;
  repeat
     if (Application.Components[x] is TForm) or (Application.Components[x] is TMMForm)then begin
        xForm :=  Application.Components[x] as TForm;
        for y:= 0 to xForm.ComponentCount-1 do begin
           if xForm.Components[y] is TVCLXMLSettingsModel then begin
              VCLXMLSettingsModel1 := xForm.Components[y] as TVCLXMLSettingsModel;
              xRet := TRUE;
              break;
           end;
        end;
     end;
     inc(x);
  until (x >= Application.ComponentCount-1 ) or xRet;
  }


  mQuickReport.ReportTitle := Translate(qlTitle.Caption);

  mSeriesList := TStringList.Create;
  fGrid := Nil;

  qlShiftFrom.Caption  := ReportInfo.TimeInfo.FromTime;
  qlShiftTo.Caption    := ReportInfo.TimeInfo.ToTime;

  qlVirtualCuts.Caption := StringReplace(qlVirtualCuts.Caption, ':', '', [rfReplaceAll]);

end;
*)


end.

