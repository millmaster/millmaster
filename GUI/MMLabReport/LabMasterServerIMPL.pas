(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: LabMasterServerIMPL.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date       Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 26.07.2001 2.01  Wss | implementation changed to use WrapperForm
| 27.02.2002 2.01  Wss | xForm.PreloadMachID ausgeklammert
|=========================================================================================*)
unit LabMasterServerIMPL;

interface

uses
  ComObj, ActiveX, LoepfePluginIMPL, LOEPFELIBRARY_TLB, LabReport_TLB, StdVcl;

type
  TLabReportServer = class(TLoepfePlugin, ILabReportServer)
  public
    function ExecuteCommand: SYSINT; override;
    procedure Initialize; override;
  protected
    procedure OpenReport(const aParameter: WideString); safecall;
  end;

implementation
{$R 'Toolbar.res'}

uses
  mmCS,
  Controls, ComServ, Forms, BaseForm, WrapperForm, DataForm;

type
  TCommandID = (ciLabMasterReport);

const
  cFloorMenuDef: Array[0..1] of TFloorMenuRec = (
    // --- Main Menu File ---
    (CommandID: 0; SubMenu: -1; MenuBmpName: '';
     MenuText: '(*)Labor Bericht'; MenuHint: '(*)Labor Bericht'; MenuStBar:'(*)Labor Bericht'; //ivlm
     ToolbarText: '';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: Ord(ciLabMasterReport); SubMenu: 0; MenuBmpName: 'LABMASTERREPORT';
     MenuText: '(*)Labor Bericht'; MenuHint:'(*)Labor Bericht'; MenuStBar: '(*)Labor Bericht'; //ivlm
     ToolbarText: 'LabReportCommand';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll)
  );

//------------------------------------------------------------------------------
// TLabMasterServer
//------------------------------------------------------------------------------
function TLabReportServer.ExecuteCommand: SYSINT;
var
  xForm: TfrmLabReport;
  xWrap: TmmForm;
begin
  Result := 0;
  case TCommandID(CommandIndex) of
    ciLabMasterReport: begin
        if GetWrappedDataForm(TfrmLabReport, TForm(xForm), xWrap) then begin
          xForm.Show;
          Result := xWrap.Handle;
        end;
      end;
  else
  end;
end;
//------------------------------------------------------------------------------
procedure TLabReportServer.Initialize;
begin
  inherited Initialize;
  InitMenu(cFloorMenuDef);
end;
//------------------------------------------------------------------------------
procedure TLabReportServer.OpenReport(const aParameter: WideString);
var
  xForm: TfrmLabReport;
  xWrap: TmmForm;
begin
  if GetWrappedDataForm(TfrmLabReport, TForm(xForm), xWrap) then begin
    xForm.OpenReport(aParameter);
  end;
end;

initialization
  InitializeObjectFactory(TLabReportServer, CLASS_LabReportServer, False);
end.

