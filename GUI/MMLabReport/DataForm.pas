(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: DataForm.pas
| Projectpart...: Parameter-Dialog fuer die Spulerei und Einzelspindelbericht
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
| 10.11.2000 1.00 Wss | File created
| 15.07.2001 1.00 Wss | supporting WarningMsg label if data bases on more than on YMSettings
| 21.08.2001 1.00 Wss | Profile commands added to popup menu
| 27.08.2001 1.00 Wss | Coarse/Fine QMatrixMode implemented
| 30.08.2001 1.00 Wss | fill data for Printout changed
| 06.09.2001 1.00 Wss | bug in SortSeries removed, default for print table set to false
| 26.09.2001 1.00 Wss | at auto color for Z-axis use of fixed palette cMMProdGroupColors
| 04.10.2001 1.00 Wss | Calculation added for SFI and SFI/D
| 26.02.2002 1.00 Wss | - Button Auswerten/Navigieren ersetzt normalen Execute Button.
                        - Feld AdjustBaseCnt hinzugefuegt fuer korrekte SFI berechnung wie in Hostlink
| 02.04.2002 1.00 Wss | Spleiss Matrix ohne aktive Klassierfelder
| 11.06.2002 1.01 Lok | Einbau ADO-Komponenten.
| 12.06.2002 1.01 Nue | Zusaetze wegen Einbau Longterm.
| 18.07.2002 1.02 Nue | Name TParameters changed to TQueryParameters
| 05.03.2003 1.02 Wss | Anzeige PopupMenu Profile des Chart korrigiert
| 06.03.2003 1.02 Wss | Automatische Farbgenerierung aus Palette korrigiert
| 06.11.2003 1.02 Wss | Profile auf Hotkey sind nun Benutzerspezifisch und werden in HKLM abgelegt
| Jan 2004   1.02 Wss | Div. Erweiterungen/Fixes f�r Version 4.02
| 17.02.2004 1.02 Wss | - in ReadData werden Daten von Chart/Grid/Matrix gel�scht
                        - div. Korrekturen
                        - BackImage war nicht gestretcht
| 28.04.2004 1.02 Wss | Q-Offlimit -> LabReport OLE Handling korrigiert
| 27.07.2004 1.02 Wss | HelpIndex nachgef�hrt
| 20.09.2005 1.02 Wss | Diverse Kleinkorrekturen zum Handling
| 15.03.2006 1.02 Wss | Im Aufruf f�r Config Dialog war ein Release() drin. WARUM!!!!! -> entfernt
| 10.01.2008 1.03 Nue | Einbau von VCV.
| 21.01.2008 1.03 Nue | Einbau von Spleissklassierung.
| 17.09.2008 1.04 Nue | mSpliceQMatrix.ActiveVisible nicht generell False in SetPropertiesToMatrix.
|=============================================================================*)
unit DataForm;           

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Menus, DB, Grids, mmPopupMenu,
  BASEFORM, TeEngine, Series, TeeProcs, TeCanvas,
  Chart, mmChart,
  ComCtrls, mmPageControl, StdCtrls, mmRadioGroup,
  ActnList, mmActionList, ImgList, mmImageList, ToolWin, mmToolBar,
  mmListBox, CheckLst, mmCheckListBox, mmLabel,
  mmGroupBox, mmColorButton, mmUpDown, mmButton,
  SettingsDialog, mmSplitter,
  SettingsIniDB, Buttons, mmSpeedButton,
  Spin, mmSpinEdit, mmPanel, mmSeries,
  IvDictio, IvMulti, IvEMulti, mmTranslator, LoepfeGlobal, mmComboBox,
  LabMasterDef, LabReportClass, IniFiles,
  ClassDataReader,
  YMRepSettingsFrame, mmDataSource,
  MMUSBSelectNavigatorFrame, DBStringGrid, mmStringGrid,
  mmHeaderControl, ClassDef, DetailInfoForm, MMSecurity,
  mmLineLabel, mmControlBar, XMLSettingsModel, VCLXMLSettingsModel,
  EditBox, YMParaEditBox;

type
  TfrmLabReport = class (TmmForm)
    acAddHotkeyProfile: TAction;
    acChartClickpoints: TAction;
    acChartColoring: TAction;
    acChartShowCalcMethodSeries: TAction;
    acChartShowDataSeries: TAction;
    acChartShowMassAvg: TAction;
    acChartZoomIn: TAction;
    acChartZoomOut: TAction;
    acDeleteHotkeyProfile: TAction;
    acExit: TAction;
    acIntervalTime: TAction;
    acLongTerm: TAction;
    acLotDetail: TAction;
    acNextDataItem: TAction;
    acPrevDataItem: TAction;
    acPrint: TAction;
    acProfileDelete: TAction;
    acProfileOpen: TAction;
    acProfileSave: TAction;
    acProfileSaveAsDefault: TAction;
    acProperties: TAction;
    acQMChannel: TAction;
    acQMFF: TAction;
    acQMSettings: TAction;
    acQMSplice: TAction;
    acResetChart: TAction;
    acResetDB: TAction;
    acSeriesMoveBack: TAction;
    acSeriesMoveBefore: TAction;
    acSeriesMoveFirst: TAction;
    acSeriesMoveFirst1: TMenuItem;
    acSeriesMoveLast: TAction;
    acShiftTime: TAction;
    acStyleDetail: TAction;
    acUseYMSettings: TAction;
    bAddProfile: TmmSpeedButton;
    bChartClickPoints: TToolButton;
    bChartColoring: TToolButton;
    bChartMove: TToolButton;
    bChartOrientation: TToolButton;
    bChartShowMassAvg: TToolButton;
    bChartToomIn: TToolButton;
    bChartZoomOut: TToolButton;
    bDeleteProfile: TmmSpeedButton;
    bExit: TToolButton;
    bInterval: TToolButton;
    bLoadSettings: TToolButton;
    bLongTerm: TToolButton;
    bLotDetail: TToolButton;
    bNavigate: TmmSpeedButton;
    bProperties: TToolButton;
    bReset: TmmSpeedButton;
    bSaveSettings: TToolButton;
    bShiftTime: TToolButton;
    bStyleDetail: TToolButton;
    bToggleCalcMethods: TToolButton;
    bToggleData: TToolButton;
    bUseYMSettings: TmmSpeedButton;
    cobChartItems: TmmComboBox;
    cobXMLSettings: TmmComboBox;
    dsGrid: TmmDataSource;
    dsGridDetail: TmmDataSource;
    Einstellungen1: TMenuItem;
    Einstellungenladen1: TMenuItem;
    Einstellungenlschen1: TMenuItem;
    Einstellungenspeichern1: TMenuItem;
    hcProfiles: TmmHeaderControl;
    mCDR: TClassDataReader;
    mChart: TmmChart;
    mDataGrid: TDBStringGrid;
    mmActionList: TmmActionList;
    mmImageList: TmmImageList;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmLabel3: TmmLabel;
    mmPanel1: TmmPanel;
    mmPanel2: TmmPanel;
    mmPanel3: TmmPanel;
    mmPanel4: TmmPanel;
    mmPanel5: TmmPanel;
    MMSelectNavigator: TMMUSBSelectNavigator;
    mmSpeedButton1: TmmSpeedButton;
    mmSpeedButton2: TmmSpeedButton;
    mmSpeedButton3: TmmSpeedButton;
    mmSpeedButton4: TmmSpeedButton;
    mmSpeedButton6: TmmSpeedButton;
    mmSplitter: TmmSplitter;
    mTranslator: TmmTranslator;
    N1: TMenuItem;
    N2: TMenuItem;
    Nachhintenverschieben1: TMenuItem;
    Nachvorneverschieben1: TMenuItem;
    pcMain: TmmPageControl;
    pmChartLegend: TmmPopupMenu;
    pmProfile: TmmPopupMenu;
    pnQOSidebar: TmmPanel;
    ProfilalsStandardspeichern1: TMenuItem;
    rgMode: TmmRadioGroup;
    SettingsIniDB: TSettingsIniDB;
    tbChart: TmmToolBar;
    tbMain: TmmToolBar;
    ToolButton1: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton15: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    tsChart: TTabSheet;
    tsQualityMatrix: TTabSheet;
    tsTable: TTabSheet;
    VerschiebenanletztePosition1: TMenuItem;
    MMSecurityControl: TMMSecurityControl;
    fraRepSettings: TfraRepSettings;
    procedure acAddHotkeyProfileExecute(Sender: TObject);
    procedure acChartZoomInOutExecute(Sender: TObject);
    procedure acDeleteHotkeyProfileExecute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acLotDetailExecute(Sender: TObject);
    procedure acNextPrevDataItemExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acProfileDeleteExecute(Sender: TObject);
    procedure acProfileOpenExecute(Sender: TObject);
    procedure acProfileSaveAsDefaultExecute(Sender: TObject);
    procedure acProfileSaveExecute(Sender: TObject);
    procedure acPropertiesExecute(Sender: TObject);
    procedure acQMatrixExecute(Sender: TObject);
    procedure acResetChartExecute(Sender: TObject);
    procedure acResetDBExecute(Sender: TObject);
    procedure acSeriesOrderExecute(Sender: TObject);
    procedure acTimeSelectionExecute(Sender: TObject);
    procedure ActionShowHideSeries(Sender: TObject);
    procedure acUseYMSettingsExecute(Sender: TObject);
    procedure bChartNavigationClick(Sender: TObject);
    procedure bNavigateClick(Sender: TObject);
    procedure cobXMLSettingsChange(Sender: TObject);
    procedure cobXMLSettingsKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure hcProfilesDrawSection(HeaderControl: THeaderControl; Section: THeaderSection; const Rect: TRect; Pressed: Boolean);
    procedure hcProfilesSectionClick(HeaderControl: THeaderControl; Section: THeaderSection);
    procedure mChartAfterDraw(Sender: TObject);
    procedure mChartClickLegend(Sender: TCustomChart; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure mChartClickSeries(Sender: TCustomChart; Series: TChartSeries; ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure mChartMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure mChartMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure mChartMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure mDataGridDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure mDataGridDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure mDataGridFillDetailGrid(Sender: TObject; aDetailGrid: TDBStringGrid);
    procedure mDataGridGetExpandInfo(Sender: TObject; aExpandInfo: TExpandInfo);
    procedure mDataGridMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure mmActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure MMSelectNavigatorAfterSelection(Sender: TObject);
    procedure pcMainChange(Sender: TObject);
    procedure rgModeClick(Sender: TObject);
    procedure seAvgPercentageChange(Sender: TObject);
    procedure seBorderConditionChange(Sender: TObject);
    procedure seFixPercentageChange(Sender: TObject);
    procedure seShiftCompressionChange(Sender: TObject);
    procedure SettingsIniDBBeforeSave(Sender: TSettingsIniDB; aOptionalChecked: Boolean);
    procedure mChartGetLegendText(Sender: TCustomAxisPanel;
      LegendStyle: TLegendStyle; Index: Integer; var LegendText: String);
    procedure MMSelectNavigatorAfterFilledAllLists(Sender: TObject);
  private
    fAxisLeftTop: TChartAxis;
    fAxisRightTop: TChartAxis;
    fSplitXAxis: TChartAxis;
    mNavigateButtonEnabled: Boolean;
    procedure AssignSettings(aProfile: TCustomIniFile);
    procedure CheckChartVisibility;
    procedure CheckMatrixVisibility;
    procedure DetailInfoHide(Sender: TObject);
    function GetAxisLeftTop: TChartAxis;
    function GetAxisRightTop: TChartAxis;
    function GetSplitXAxis: TChartAxis;
    procedure LoadSettings(aDefault: Boolean);
    procedure ReadAndFillChartData;
    procedure ReadAndFillGridData;
    procedure ReadAndFillMatrixData;
    procedure SaveColumnProperties;
    procedure SaveProfile(aDefault: Boolean);
    procedure SetPropertiesToChart;
    procedure SetPropertiesToGrid;
    procedure SetPropertiesToMatrix;
    procedure ShowCalcMethods(aLabItem: TLabDataItem; aLinkSerie: TChartSeries);
    procedure UpdateNavigateAnalyseButton;
  protected
    mAnalyseMode: Boolean;
    mAxis: TChartAxis;
    mAxisLeft: TChartAxis;
    mAxisRight: TChartAxis;
    mChartItemIndex: Integer;
    mChartOptionsOrg: TChartOptions;
    mDataItemList: TLabDataItemList;
    mDataReady: Boolean;
    mDetailInfo: TfrmDetailInfo;
    mDM: TdmLabReport;
    mHasCalcMethodSeries: Boolean;
    mLegendClickEnabled: Boolean;
    mLenBase: TLenBase;
    mMouseMoved: Boolean;
    mProfileIndex: Integer;
    mQOEventTime: TDateTime;
    mQOMode: Boolean;
    mSelectedSeriesIndex: Integer;
    mSelectionArray: Array[0..3] of String;
    mStartCol: Integer;
    function ExtractQOfflimitParameters(aParameter: String; var aNewProfile: TMemIniFile): Boolean;
    procedure ReadData;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure OpenReport(aParameter: WideString);
    procedure PreloadMachID(aMachID: String);
    procedure RestoreFormLayout(aRegistryKey: String = ''); override;
  end;
  

var
  frmLabReport: TfrmLabReport;

implementation // 23.04.2002 added mmMBCS to imported units
{$R *.DFM}
{$R Navigate.res}

uses
  mmMBCS, mmCS, mmStringList, TeeFunci, ComObj, Printers,
  PrintSettingsDialog, MMUGlobal, SettingsReader,
  QualityMatrixBase, QualityMatrixDef, DataLayerDef, CalcMethods, MMHtmlHelp;

const
  cQOSectionBasic = 'Basic';
  cRegHotkeys     = 'Hotkeys_';

//:---------------------------------------------------------------------------
//:--- Class: TfrmLabReport
//:---------------------------------------------------------------------------
constructor TfrmLabReport.Create(aOwner: TComponent);
var
  i: Integer;
  xStrList: TStringList;
begin
  inherited Create(aOwner);
  HelpContext            := GetHelpContext('LabMaster\LabBericht\LAB_Labor-Bericht.htm');

  pcMain.Align           := alClient;

  fAxisLeftTop           := Nil; // Axis Objekte werden am Schluss von mChart automatisch mitgel�scht, da Owner
  fAxisRightTop          := Nil;
  fSplitXAxis            := Nil;

  mAnalyseMode           := False;
  mAxis                  := Nil;
  mAxisLeft              := Nil;
  mAxisRight             := Nil;
  mChartItemIndex        := -1;
  mDataReady             := False;
  mDetailInfo            := Nil;
  mHasCalcMethodSeries   := False;
  mLegendClickEnabled    := False;
  mMouseMoved            := False;
  mNavigateButtonEnabled := True;
  mProfileIndex          := -1;
  mQOEventTime           := 0;
  mQOMode                := False;
  mSelectedSeriesIndex   := -1;

  with TMMSettingsReader.Instance do
  try
    mLenBase := TLenBase(Value[cMMUnit]);
    if not IsComponentLongTerm then begin
      bLongTerm.Free;
    end;
  finally
  end;

    // clean up series from design mode
  mChart.AllowZoom        := True;
  mChart.FreeAllSeries;
  mChart.Gradient.Visible := True;
  mChart.Title.Text.Clear;
  
    // Globales Objekt f�r DataItems erstellen.
  if not Assigned(gDataItemPool) then begin
      // Globaler DataItemPool 1x erstellen und entsprechend initialisieren
    gDataItemPool := TLabDataItemList.Create(False);
      // �ber LinkAlias wird die Verbindung mit den ID-Feldern gemacht
    gDataItemPool.InitDataItems(0);
    gDataItemPool.LinkAlias := 'v';
  end;
  
    // DataItemList erstellen und mit Items initialisieren welche immer ben�tigt werden
  mDataItemList := TLabDataItemList.Create(False);
    // �ber LinkAlias wird die Verbindung mit den ID-Feldern gemacht
  mDataItemList.InitDataItems(1);
  mDataItemList.LinkAlias := 'v';
  
    // Navigationsbutton initialisieren mit Caption und Bitmaps (von Ressourcefile)
  with bNavigate do begin
    Caption := rsAnalyse;
    Glyph.LoadFromResourceName(HInstance, 'ANALYSE');
  end;
  
    // Create an own Datamodul for every instance
    // Das Datenmodul erh�lt eine Referenz auf die DataItemListe
  mDM := TdmLabReport.Create(Nil, mDataItemList);
  mDM.LenBase         := mLenBase;
  dsGrid.DataSet       := mDM.dseGrid;
  dsGridDetail.DataSet := mDM.dseGridDetail;
  
    // Default Konfiguration laden, wenn sie vorhanden ist
  LoadSettings(True);
  
    // nun noch den Navigator initialisieren
  MMSelectNavigator.Init;
  //  MMSelectNavigator.DoResize := True;
  MMSelectNavigator.Height   := 210;
  mDM.DefaultShiftCal        := MMSelectNavigator.DefaultShiftCal;

    // nun noch die verf�gbaren Profile in der HeaderControl abf�llen
  xStrList := TStringList.Create;
  try
    xStrList.CommaText := GetRegString(cRegLM, cRegMMApplicationPath + '\' + gApplicationName, cRegHotkeys + MMSecurityControl.CurrentMMUser);
    if xStrList.Count = 0 then CodeSite.SendWarning('Shortcut profilelist empty');

    for i:=0 to xStrList.Count-1 do begin
      with hcProfiles.Sections.Add do begin
        Text  := xStrList.Strings[i];
        Width := Self.Canvas.TextWidth(Text) + 20;
        Style := hsOwnerDraw;
      end;
    end;
  finally
    xStrList.Free;
  end;
end;

//:---------------------------------------------------------------------------
destructor TfrmLabReport.Destroy;
var
  i: Integer;
  xStrList: TStringList;
begin
  xStrList := TStringList.Create;
  with hcProfiles.Sections do
  try
    for i:=0 to Count-1 do
      xStrList.Add(Items[i].Text);
  finally
    SetRegString(cRegLM, cRegMMApplicationPath + '\' + gApplicationName, cRegHotkeys + MMSecurityControl.CurrentMMUser, xStrList.CommaText);
    xStrList.Free;
  end;
  
  mChart.FreeAllSeries;
  
  FreeAndNil(mDataItemList);
  FreeAndNil(mDM);
  FreeAndNil(mDetailInfo);
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acAddHotkeyProfileExecute(Sender: TObject);
var
  i: Integer;
  xFound: Boolean;
begin
  if SettingsIniDB.ProfileName <> '' then begin
    xFound := False;
    with hcProfiles.Sections do begin
      for i:=0 to Count-1 do begin
        xFound := AnsiSameText(SettingsIniDB.ProfileName, Items[i].Text);
        if xFound then
          Break;
      end;
    end;
  
    if not xFound then
      with hcProfiles.Sections.Add do begin
        Text  := SettingsIniDB.ProfileName;
        Width := Self.Canvas.TextWidth(Text) + 20;
        Style := hsOwnerDraw;
      end;
  end; // if ProfileName
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acChartZoomInOutExecute(Sender: TObject);
begin
  with Sender as TAction do
    mChart.View3DOptions.Zoom := mChart.View3DOptions.Zoom + Tag;
  mDataItemList.ChartOptions^.Zoom := mChart.View3DOptions.Zoom;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acDeleteHotkeyProfileExecute(Sender: TObject);
var
  i: Integer;
begin
  if mProfileIndex <> -1 then begin
    with hcProfiles.Sections do
      for i:=0 to Count-1 do begin
        if Items[i].ID = mProfileIndex then begin
          mProfileIndex := -1;
          Delete(i);
          hcProfiles.Invalidate;
          Break;
        end;
      end; // for
  end; // if
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acExitExecute(Sender: TObject);
begin
  Close;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acLotDetailExecute(Sender: TObject);
begin
  with TAction(Sender) do
    Checked := not Checked;
  
  ReadData;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acNextPrevDataItemExecute(Sender: TObject);
begin
  if Sender is TAction then
    cobChartItems.ItemIndex := mChartItemIndex + TAction(Sender).Tag;
  mChartItemIndex := cobChartItems.ItemIndex;

  ReadData;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acPrintExecute(Sender: TObject);
var
  i: Integer;
  xCount: Integer;
  xFirst, xLast: Integer;
  xQRLeftAxis, xQRRightAxis, xQRSplitXAxis: TChartAxis;
  
  //.................................................................
  function GetFilterValues(aSelection: TSourceSelection): String;
  var
    xCount: Integer;
  begin
    xCount := MMSelectNavigator.SelCount[aSelection];
    if xCount = 0 then begin
      Result := Format('#%d', [MMSelectNavigator.ItemsCount[aSelection]]);
    end else begin
      Result := MMSelectNavigator.SelectedName[aSelection];
      // replace CRLF with '; '
      Result := StringReplace(Result, cCRLF, '; ', [rfReplaceAll]);
    end;
  end;
  //.................................................................
  
begin
  with TfrmPrintDialog.Create(Self) do
  try
    // Filter labels
    FilterStyle   := GetfilterValues(ssStyle);
    FilterLot     := GetfilterValues(ssLot);
    FilterMachine := GetfilterValues(ssMach);
    if MMSelectNavigator.UseClearerSettings then
      FilterYMSettings := GetfilterValues(ssYMSet)
    else
      FilterYMSettings := rsAll;
  
    // set link the printed DBGrid
    mPrintDBGrid.DataGrid := mDataGrid;
    // use the same instance of ClassDataReader
    CDR := mCDR;
    // use the same instance of parameter list. No copy is used
    Parameters := mDataItemList;
  

    // Shift range: First, Last, Count
    // determine time selection with selected and total number of range
    xCount := 0;
    with MMSelectNavigator do begin
      if TimeMode in [tmShift, tmLongtermWeek] then begin
        qlTimeFrom.Caption       := Format('%s %s', [rsTimeFrom, DateTimeToStr(TimeFrom)]);
        qlTimeTo.Caption         := Format('%s %s', [rsTimeTo, DateTimeToStr(TimeTo)]);;
        qlTimeCountTitle.Enabled := False;
        qlTimeCount.Enabled      := False;
      end else begin
        xFirst := 1;
        xLast  := lbTime.RowCount-1;
        if lbTime.RowCount > 1 then begin
            // init the From/To label with maximum range of time value
          qlTimeFrom.Caption  := Format('%s %s', [rsTimeFrom, lbTime.Cells[0, xLast]]);
          qlTimeTo.Caption    := Format('%s %s', [rsTimeTo, lbTime.Cells[0, 1]]);
          // Alle Eintr�ge durchz�hlen und nach Min/Max Bereich scannen
          for i:=1 to lbTime.RowCount-1 do begin
            if lbTime.IsRowSelected(i) then begin
              if xCount = 0 then begin
                xFirst := i;
                qlTimeTo.Caption  := Format('%s %s', [rsTimeTo, lbTime.Cells[0, i]]);
              end;
              // the To label has to be set allways
              qlTimeFrom.Caption  := Format('%s %s', [rsTimeFrom, lbTime.Cells[0, i]]);
              inc(xCount);
              xLast := i;
            end;
          end;
  
            // nothing is selected in time range -> show all information
          if xCount = 0 then
            xCount := xLast - xFirst + 1;
  
          qlTimeCount.Caption := Format('%d/%d', [xCount, xLast - xFirst + 1]);
        end; // if
      end; // if (TimeMode = tmInterval)
    end; // with

    // Chart: copy chart data into quickreport chart
    mQRChart.Chart.FreeAllSeries;
    mQRChart.Chart.Assign(mChart); // use same properties for chart
    xQRLeftAxis   := Nil;
    xQRRightAxis  := Nil;
    xQRSplitXAxis := Nil;
    for i:=0 to mChart.SeriesCount-1 do begin
      with CloneChartSeries(mChart.Series[i]) do begin
        ParentChart := mQRChart.Chart;
        // wenn X-Achse gesplittet -> ebenfalls in ausdruck �bernehmen
        if Assigned(mChart.Series[i].CustomHorizAxis) then begin
          // Right axis
          if not Assigned(xQRSplitXAxis) then begin
            xQRSplitXAxis := TChartAxis.Create(mQRChart.Chart);
            xQRSplitXAxis.Assign(mChart.Series[i].CustomHorizAxis);
          end;
          CustomHorizAxis := xQRSplitXAxis;
        end;

        if Assigned(mChart.Series[i].CustomVertAxis) then begin
          // Right axis
          if mChart.Series[i].CustomVertAxis.OtherSide then begin
            if not Assigned(xQRRightAxis) then begin
              xQRRightAxis := TChartAxis.Create(mQRChart.Chart);
              xQRRightAxis.Assign(mChart.Series[i].CustomVertAxis);
            end;
            CustomVertAxis := xQRRightAxis;
          end
          // Left axis
          else begin
            if not Assigned(xQRLeftAxis) then begin
              xQRLeftAxis := TChartAxis.Create(mQRChart.Chart);
              xQRLeftAxis.Assign(mChart.Series[i].CustomVertAxis);
            end;
            CustomVertAxis := xQRLeftAxis;
          end;
        end;
      end;
    end;

    ShowModal;
  finally
    Free;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acProfileDeleteExecute(Sender: TObject);
begin
  SettingsIniDB.DeleteSettings;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acProfileOpenExecute(Sender: TObject);
begin
  LoadSettings(False);
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acProfileSaveAsDefaultExecute(Sender: TObject);
begin
  SaveProfile(True);
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acProfileSaveExecute(Sender: TObject);
begin
  SaveProfile(False);
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acPropertiesExecute(Sender: TObject);
begin
  SaveColumnProperties;
  with TfrmSettingsDialog.Create(Self) do
  try
    Parameter := mDataItemList;
    SetActivePage(Self.pcMain.ActivePageIndex);
    if ShowModal = mrOK then begin
      with mDataItemList do begin
        Assign(Parameter);
        mChartItemIndex := 0;
        cobChartItems.Items.CommaText := ChartItemsCommaList;
        cobChartItems.ItemIndex       := mChartItemIndex;
        // handle new settings as original in case of pressing reset button
        mChartOptionsOrg := ChartOptions^;
      end;

      ReadData;

      SetPropertiesToChart;
      SetPropertiesToGrid;
      SetPropertiesToMatrix;
    end; // if ShowModal
  finally
    Free;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acQMatrixExecute(Sender: TObject);
begin
  with (Sender as TAction) do begin
//Nue:13.3.08    ReadAndFillMatrixData;  //Nue:13.03.08 Zuvor Aufruf nach dem Case
    case Tag of
      0: fraRepSettings.FrameMode := fmChannelData;
      1: fraRepSettings.FrameMode := fmSpliceData;
      2: fraRepSettings.FrameMode := fmFFData;
      3: fraRepSettings.FrameMode := fmSettings;
    else
      Exit;
    end;
  end;
  ReadAndFillMatrixData;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acResetChartExecute(Sender: TObject);
begin
  mChart.UndoZoom;
  if bChartMove.Enabled then begin
    with mDataItemList do
      ChartOptions^ := mChartOptionsOrg;
  end;
  SetPropertiesToChart;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acResetDBExecute(Sender: TObject);
begin
  MMSelectNavigator.Reset;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acSeriesOrderExecute(Sender: TObject);
begin
  // Index von markierte Serie wurde vorher in Tag Property von Popup Komponente gesetzt 
  mChart.MoveSeries(pmChartLegend.Tag, TMoveSeries((Sender as TAction).Tag));
//  xIndex := mChart.MoveSeries(pmChartLegend.Tag, TMoveSeries((Sender as TAction).Tag));
//  xIndex := mChart.MoveSeries(mSelectedSeriesIndex, TMoveSeries((Sender as TAction).Tag));
//  if mSelectedSeriesIndex <> -1 then
//    mSelectedSeriesIndex
  CheckChartVisibility;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acTimeSelectionExecute(Sender: TObject);
begin
  with MMSelectNavigator do begin
    case (Sender as TAction).Tag of
      0: begin
           TimeMode := tmInterval;
         end;
      1: begin
           TimeMode := tmShift;
         end;
      2: begin
           TimeMode := tmLongtermWeek;
         end;
    else
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.ActionShowHideSeries(Sender: TObject);
var
  xAction: TAction;
begin
  xAction := TAction(Sender);
  xAction.Checked := not xAction.Checked;

//  mLegendClickEnabled :=  not mChart.View3D and (acChartClickPoints.Checked or acChartColoring.Checked or acChartShowMassAvg.Checked);
//  if not mLegendClickEnabled then begin
//    mSelectedSeriesIndex := -1;
//    mChart.Invalidate;
//  end;
  CheckChartVisibility;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.acUseYMSettingsExecute(Sender: TObject);
begin
  with bUseYMSettings do begin
    MMSelectNavigator.UseClearerSettings       := Down;
    mDataItemList.ReportOptions^.UseYMSettings := Down;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.AssignSettings(aProfile: TCustomIniFile);
var
  xSelection: String;
begin
  mDataItemList.ReadFromConfig(aProfile);

  with mDataItemList do begin
    cobChartItems.Items.CommaText := ChartItemsCommaList;
    cobChartItems.ItemIndex       := mChartItemIndex;
    // save this chart options as original if user press reset button
    mChartOptionsOrg    := ChartOptions^;
    bUseYMSettings.Down := ReportOptions^.UseYMSettings;
    MMSelectNavigator.UseClearerSettings := bUseYMSettings.Down;
  end;

  xSelection := aProfile.ReadString('Selection', 'Selection', '');
  if xSelection <> '' then begin
    CodeSite.SendString('Selection', xSelection);
    MMSelectNavigator.Selection := xSelection;
  end;

  ReadData;

  SetPropertiesToChart;
  SetPropertiesToGrid;
  SetPropertiesToMatrix;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.bChartNavigationClick(Sender: TObject);
begin
  with Sender as TToolButton do begin
    mChart.AllowZoom := not Down;
    mChart.MouseMode := mmNormal;
    if Down then begin
      case Tag of
        0: mChart.MouseMode := mmRotate;
        1: mChart.MouseMode := mmMove;
      else
      end;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.bNavigateClick(Sender: TObject);
begin
  mAnalyseMode := not mAnalyseMode;
  UpdateNavigateAnalyseButton;

  if mAnalyseMode then begin
    mChartItemIndex := 0;
    cobChartItems.ItemIndex := mChartItemIndex;
    ReadData;
  end // if mAnalyseMode
  else begin
    mChartItemIndex := -1;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.CheckChartVisibility;
var
  i: Integer;
  xExtSeries: IExtendSeries;
  
  //.................................................................
  procedure AddBorderSeries(aLineSeries: TmmLineSeries);
  var
    i: Integer;
    xExtSeries: IExtendSeries;
  begin
    with mChart.SeriesList do begin
      for i:=0 to Count-1 do begin
        xExtSeries := mChart.SeriesEx[i];
        if Assigned(xExtSeries) then begin
          // Grenzserien haben in Data kein Objekt abgelegt
          if not Assigned(xExtSeries.Data) then
            aLineSeries.BorderSeries.Add(Series[i]);
        end;
      end; // for i
    end; // with
    aLineSeries.RefreshSeries;
  end;
  //.................................................................
  
begin
  with mChart.SeriesList do begin
    for i:=0 to Count-1 do begin
      xExtSeries := mChart.SeriesEx[i];
      if Assigned(xExtSeries) then begin
        // generell die Klick-Markierung ein/ausschalten
        xExtSeries.ShowClickPoint := (mSelectedSeriesIndex = i) and acChartClickpoints.Checked;
        // Serien ein/ausblenden
        // f�r Datenserien -> acChartShowDataSeries, f�r Grenzserien -> acChartShowCalcMethodSeries verwenden
        // bei Datenserien ist im Property Data das LabDataItem abgelegt
        if Assigned(xExtSeries.Data) then begin
          // Ein/Auschalten der normalen Datenserien
          Series[i].Active := acChartShowDataSeries.Checked;
          // Gewichteter Mittelwert
          xExtSeries.ShowAverage := (mSelectedSeriesIndex = i) and acChartShowMassAvg.Checked;
          if Series[i] is TmmLineSeries then begin
            with TmmLineSeries(Series[i]) do begin
//              // Gewichteter Mittelwert
//              ShowAverage := (mSelectedSeriesIndex = i) and acChartShowMassAvg.Checked;
              // Einf�rbung aber nur wenn der Modus es erlaubt (ZAxis,Trend,no StyleDetail)
              if acChartColoring.Checked and (mSelectedSeriesIndex = i) then begin
                HighBorderMode := bmBorderSeries;
                if mHasCalcMethodSeries and (not acStyleDetail.Checked) and mDataItemList.TrendMode and mDataItemList.ZAxisMode and (BorderSeries.Count = 0) then
                  AddBorderSeries(TmmLineSeries(Series[i]));
              end else
                HighBorderMode := bmNone;
            end; // with
          end; // if Series[i]
        end else
          // Ein/Auschalten der Berechnungsmethodenserien
          Series[i].Active := acChartShowCalcMethodSeries.Checked;
      end; // if Assigned(xExtSeries)
    end; // for i
  end; // with
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.CheckMatrixVisibility;
begin
{TODO: CheckMatrixVisibility}
  cobXMLSettings.Visible := (mCDR.XMLSettingNames.Count = 1) or (fraRepSettings.FrameMode = fmSettings);
  if cobXMLSettings.Visible then begin
    SetPropertiesToMatrix;
  end else begin
    with fraRepSettings do begin
      with mQMatrix do begin
        ActiveVisible  := False;
        ChannelVisible := False;
        ClusterVisible := False;
        SpliceVisible  := False;
        Invalidate;
      end; // with mQMatrix
      with mSpliceQMatrix do begin  //Nue:20.01.08
        ActiveVisible  := False;
        ChannelVisible := False;
        ClusterVisible := False;
        SpliceVisible  := False;
        Invalidate;
      end; // with mQMatrix
      with mFMatrix do begin
        ActiveVisible := False;
        Invalidate;
      end; // with mSiroMatrix
    end;
  end;
{}
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.cobXMLSettingsChange(Sender: TObject);
var
  xSettings: TXMLSettingsInfoRec;
begin
{TODO: cobYMSettingsChange}
  with cobXMLSettings do begin
    if mCDR.GetXMLSettingsFromID(Integer(Items.Objects[ItemIndex]), xSettings) then begin
      with xSettings do begin
        if not SingleYarnCount and (fraRepSettings.WarningMsg = cswNone) then
          fraRepSettings.WarningMsg := cswManyYarnCount;
      end;
      fraRepSettings.ShowYMSettingsInfoRec(xSettings);
    end;
  end;
{}
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.cobXMLSettingsKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if not((Key = VK_UP) or (Key = VK_PRIOR) or
         (Key = VK_DOWN) or (Key = VK_NEXT)) then
    Key := 0;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.DetailInfoHide(Sender: TObject);
begin
  if mSelectedSeriesIndex <> -1 then
    mChart.SeriesEx[mSelectedSeriesIndex].MarkClickPoint(False);
end;

//:---------------------------------------------------------------------------
function TfrmLabReport.ExtractQOfflimitParameters(aParameter: String; var aNewProfile: TMemIniFile): Boolean;
var
  xParamIni: TMemIniFile;
  xTmpList: TIniStringList;
  xStr: String;
  xLabItem: TLabDataItem;
  xCount: Integer;
  i, j: Integer;
  xMethodCount: Integer;
  xDataItems: TStringList;
  xActiveName: String;
  xSectionName: String;
  xHWnd: DWord;
begin
  Result := False;

  EnterMethod('TfrmLabReport.ExtractQOfflimitParameters');
  xCount          := 0;
  xParamIni       := TMemIniFile.Create('');
  xDataItems      := TStringList.Create;
  xTmpList        := TIniStringList.Create;
  try
    //.................
    // Parameters von QOfflimit �bernehmen
    //.................
    xTmpList.CommaText := aParameter;
    xParamIni.SetStrings(xTmpList);
    // verhindert, dass Labreport Fenster hinter Floor verschwindet, wenn das QO-Fenster oder
    // Floor aktiviert wird. Es wird dem eigenen WrapperFenster das WindowHandle vom QO-WrapperFenster
    // �bergeben
    if Assigned(WrapperForm) then begin
      xHWnd := xParamIni.ReadInteger(cQOSectionBasic, 'WindowHandle', 0);
      if xHWnd > 0 then
        WrapperForm.ParentWindow := xHWnd;
    end;
    //.................
    // Profil mit einer Vorlage vorbereiten
    // und vorhandene DataItems l�schen
    //.................
    aNewProfile.SetStrings(mDM.slTemplateSettings.Strings);
    aNewProfile.EraseSection('DataItems');

    //.................
    // Selektion behandeln
    //.................
//    seBorderCondition.Value  := xParamIni.ReadInteger(cQOSectionBasic, 'BorderCondition', 2);
//    seShiftCompression.Value := xParamIni.ReadInteger(cQOSectionBasic, 'ShiftCompression', 2);

    with xTmpList do begin
      Clear;
      Values['TimeMode'] := tmShift;
      Values['StyleID']  := xParamIni.ReadString(cQOSectionBasic, 'StyleID', '');
      Values['LotID']    := xParamIni.ReadString(cQOSectionBasic, 'LotID', '');
      Values['MachID']   := xParamIni.ReadString(cQOSectionBasic, 'MachID', '');
      Values['TimeFrom'] := Double(xParamIni.ReadFloat(cQOSectionBasic, 'TimeFrom', Now-20));
      Values['TimeTo']   := Double(xParamIni.ReadFloat(cQOSectionBasic, 'TimeTo', Now));
      aNewProfile.WriteString('Selection', 'Selection', CommaText);
      CodeSite.SendString('Selection', CommaText);
    end;
    mQOEventTime := xParamIni.ReadFloat(cQOSectionBasic, 'EventTime', Now-2);

    //.................
    // X-Achse behandeln
    //.................
    xStr := xParamIni.ReadString(cQOSectionBasic, 'XAxis', 'time');
    if xStr <> '' then begin
        // hole aus dem originalen DataItemPool eine Referenz...
      xLabItem := TLabDataItem(gDataItemPool.DataItemByName[xStr]);
      if Assigned(xLabItem) then begin
        aNewProfile.WriteString('ChartOptions', 'XAxis', xStr);
          // ...�bernehme den default Configstring und passe ein paar Werte an...
        with xTmpList do begin
          CommaText := xLabItem.ConfigString;
          Values['ShowInTable'] := True;
          Values['TableIndex']  := xCount;
            //...und schreibe diesen ConfigString in das tempor�re Profil
          aNewProfile.WriteString('DataItems', 'Item' + IntToStr(xCount), CommaText);
          CodeSite.SendString('XAxis', CommaText);
        end;
        inc(xCount);
      end;
    end;

    //.................
    // Z-Achse behandeln
    //.................
    xStr := xParamIni.ReadString(cQOSectionBasic, 'ZAxis', 'prod_name');
    if xStr <> '' then begin
        // hole aus dem originalen DataItemPool eine Referenz...
      xLabItem := TLabDataItem(gDataItemPool.DataItemByName[xStr]);
      if Assigned(xLabItem) then begin
        aNewProfile.WriteString('ChartOptions', 'ZAxis', xStr);
          // ...�bernehme den default Configstring und passe ein paar Werte an...
        with xTmpList do begin
          CommaText := xLabItem.ConfigString;
          Values['ShowInTable'] := True;
          Values['TableIndex']  := xCount;
            //...und schreibe diesen ConfigString in das tempor�re Profil
          aNewProfile.WriteString('DataItems', 'Item' + IntToStr(xCount), CommaText);
          CodeSite.SendString('ZAxis', CommaText);
        end;
        inc(xCount);
      end;
    end;

    //.................
    // DataItem behandeln
    //.................
    cobChartItems.Clear;
    xDataItems.CommaText := xParamIni.ReadString(cQOSectionBasic, 'DataItems', '');
    xActiveName          := xParamIni.ReadString(cQOSectionBasic, 'Active', '');
    for i:=0 to xDataItems.Count-1 do begin
      // Name des Datenelements holen
      xSectionName := xDataItems.Strings[i];
      // Prefix entfernen um richtigen Namen zu erhalten
      // Prefix n�tig wegen A1 cut, A1 uncut, A1 both -> cA1, uA1, bA1
      xStr := xSectionName;
      Delete(xStr, 1, 1);
      // hole aus dem originalen DataItemPool eine Referenz...
      xLabItem := TLabDataItem(gDataItemPool.DataItemByName[xStr]);
      if Assigned(xLabItem) then begin
//        xStr := xLabItem.DisplayName;
//        if xLabItem.IsMatrixDataItem then begin
//          xStr := Format('%s %s', [xStr, GetDefectTypeText(xLabItem.ClassDefects)])
//
//        cobChartItems.Items.Add(xStr);
        if AnsiSameText(xSectionName, xActiveName) then
          mChartItemIndex := cobChartItems.Items.Count-1;
        // ...�bernehme den default Configstring und passe ein paar Werte an...
        with xTmpList do begin
          CommaText := xLabItem.ConfigString;
          Values['ShowInChart'] := True;
          Values['ChartIndex']  := xCount - 2; // -2, da 2 KeyItems ja schon gez�hlt wurden
          Values['ChartType']   := ctLine;
          Values['ShowInTable'] := True;
          Values['TableIndex']  := xCount;
          Values['Commas']      := 1;
          //.................
          // Berechnungsmethoden behandeln
          //.................
          if xParamIni.SectionExists(xSectionName) then begin
            // bei einem ClassDataItem muss noch ermittelt werden, ob Cut/Uncut/Both
            if xLabItem.IsMatrixDataItem then begin
              Values['ClassDefects'] := xParamIni.ReadInteger(xSectionName, 'ClassDefects', 0);
//              xStr := Format('%s %s', [xStr, GetDefectTypeText(Values['ClassDefects'])]);
//              xStr := Format('%s %s', [xStr, GetDefectTypeText(xLabItem.ClassDefects)]);
            end;

            // Methodenparameter einfach in die Liste hineinf�gen
            xMethodCount := xParamIni.ReadInteger(xSectionName, 'MethodCount', 0);
            with TStringList.Create do
            try
              for j:=0 to xMethodCount-1 do
                Add(xParamIni.ReadString(xSectionName, 'Method' + IntToStr(j), ''));
            finally
              xTmpList.Values['CalcMethods'] := CommaText;
              Free;
            end;
          end;
          //...und schreibe diesen ConfigString in das tempor�re Profil
          aNewProfile.WriteString('DataItems', 'Item' + IntToStr(xCount), CommaText);
          CodeSite.SendString('DataItem', CommaText);
        end; // with xTmpList

//        cobChartItems.Items.Add(xStr);
//        if AnsiSameText(xSectionName, xActiveName) then
//          mChartItemIndex := cobChartItems.Items.Count-1;

        inc(xCount);
      end; // if Assigned(xLabItem)
    end;
    aNewProfile.WriteInteger('DataItems', 'DataItemsCount', xCount);
    // das vordefinierte Datenelement aktivieren -> wird in AssignSettings() �bernommen
    Result := True;
  finally
    if mChartItemIndex = -1 then
      mChartItemIndex := 0;
    xDataItems.Free;
    xParamIni.Free;
    xTmpList.Free;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  MMSelectNavigator.StoppFilling;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.FormShow(Sender: TObject);
begin
  // Im Delphi Modus wird automatisch das Profil "Selektion 1" geladen
{
  if (DebugHook <> 0) and not mQOMode and SettingsIniDB.LoadSettings('Selektion 1') then begin
    mChartItemIndex := 0;
    AssignSettings(SettingsIniDB.Ini);
  end;
{}
end;

//:---------------------------------------------------------------------------
function TfrmLabReport.GetAxisLeftTop: TChartAxis;
begin
  if not Assigned(fAxisLeftTop) then begin
    // wird von der TChart Komponente automatisch wieder freigegeben
    fAxisLeftTop := TChartAxis.Create(mChart);
    with fAxisLeftTop do begin
      Horizontal  := False;
      OtherSide   := False;
      EndPosition := 48;
      LabelsFont.Color := clBlue;
    end;
  end;
  Result := fAxisLeftTop;
end;

//:---------------------------------------------------------------------------
function TfrmLabReport.GetAxisRightTop: TChartAxis;
begin
  if not Assigned(fAxisRightTop) then begin
    // wird von der TChart Komponente automatisch wieder freigegeben
    fAxisRightTop := TChartAxis.Create(mChart);
    with fAxisRightTop do begin
      Horizontal  := False;
      OtherSide   := True;
      EndPosition := 48;
      LabelsFont.Color := clBlue;
      Grid.Visible     := False;
    end;
  end;
  Result := fAxisRightTop;
end;

//:---------------------------------------------------------------------------
function TfrmLabReport.GetSplitXAxis: TChartAxis;
begin
  if not Assigned(fSplitXAxis) then begin
    fSplitXAxis := TChartAxis.Create(mChart);
    fSplitXAxis.Assign(mChart.BottomAxis);
    with fSplitXAxis do begin
      Horizontal  := True;
      OtherSide   := False;
      StartPosition := 52;
      EndPosition   := 100;
    end;
  end;
  Result := fSplitXAxis;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.hcProfilesDrawSection(HeaderControl: THeaderControl; Section: THeaderSection; const Rect: TRect; Pressed: Boolean);
begin
  with hcProfiles.Canvas do begin
    // highlight pressed sections
    if mProfileIndex = Section.ID then
      Font.Color := clRed
    else
      Font.Color := clBlue;
    TextOut(Rect.Left + Font.Size, Rect.Top + 2, Section.Text);
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.hcProfilesSectionClick(HeaderControl: THeaderControl; Section: THeaderSection);
begin
  if SettingsIniDB.LoadSettings(Section.Text) then begin
    mProfileIndex := Section.ID;
    hcProfiles.Invalidate;
    mChartItemIndex := 0;
    AssignSettings(SettingsIniDB.Ini);
  end
  else begin
    if MessageDlg(rsMsgRemoveProfile, mtConfirmation, mbOKCancel, 0) = mrOK then begin
      mProfileIndex := -1;
      HeaderControl.Sections.Delete(Section.Index);
      HeaderControl.Invalidate;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.LoadSettings(aDefault: Boolean);
var
  xOk: Boolean;
begin
  if aDefault then
    xOk := SettingsIniDB.LoadDefault
  else
    xOk := SettingsIniDB.LoadSettings;

  if xOk or aDefault then begin
    mProfileIndex   := -1;
    mChartItemIndex := 0;
    AssignSettings(SettingsIniDB.Ini);
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.mChartAfterDraw(Sender: TObject);
var
  xX: Integer;
begin
  if mQOMode and (mQOEventTime > cDateTime1970) then begin
    with mChart do begin
      xX := BottomAxis.CalcXPosValue(mQOEventTime);
  
      if (xX > ChartRect.Left) and (xX < ChartRect.Right) then
        with Canvas do begin
          Pen.Color := clRed;
          Pen.Width := 2;
          Brush.Style := bsClear;
          MoveTo(xX, ChartRect.Top);
          LineTo(xX, ChartRect.Bottom);
        end;
    end; // with
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.mChartClickLegend(Sender: TCustomChart; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  xPoint: TPoint;
  xIndex: Integer;
begin
  mSelectedSeriesIndex := -1;
  xIndex := mChart.Legend.Clicked(X, Y);
  if (xIndex >= 0) and (xIndex < mChart.SeriesList.CountActive) then begin
    // Anhand vom Legendindex den richtigen Serienindex herausfinden
    xIndex  := mChart.SeriesList.IndexOf(mChart.ActiveSeriesLegend(xIndex));
    if (xIndex >= 0) and (xIndex < mChart.SeriesList.Count) then begin
//      mSelectedSeriesIndex := xIndex;
      if Button = mbRight then begin
//        mSelectedSeriesIndex := xIndex;
        xPoint := mChart.ClientToScreen(Point(X, Y));
        pmChartLegend.Tag := xIndex;
        pmChartLegend.Popup(xPoint.X, xPoint.Y);
      end
      else begin
        if mLegendClickEnabled then
          mSelectedSeriesIndex := xIndex;
        CheckChartVisibility;
      end; // if button
    end; // if xIndex <> -1
  end; // if xIndex <> -1
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.mChartClickSeries(Sender: TCustomChart; Series: TChartSeries; ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X, Y: 
        Integer);
var
  xIndex: Integer;
  xAxis: TChartAxis;
begin
  xIndex := mChart.SeriesList.IndexOf(Series);
  if (Button = mbLeft) and (mSelectedSeriesIndex = xIndex) {or not(Series is TLineSeries)} then begin
      // FIRST: determine the associated axis for this clicked series
    if Assigned(Series.CustomVertAxis) then
      xAxis := Series.CustomVertAxis
    else if Series.VertAxis = aLeftAxis then
      xAxis := Sender.LeftAxis
    else if Series.VertAxis = aRightAxis then
      xAxis := Sender.RightAxis
    else
      Exit;
      // SECOND: check boundaries if it is clicked outside of the clipped area
    if (Y < xAxis.IStartPos) or (Y > xAxis.IEndPos) then
      Exit;
  
    if not Assigned(mDetailInfo) then begin
      mDetailInfo := TfrmDetailInfo.Create(Self, mDM, mDataItemList);
      mDetailInfo.OnHide := DetailInfoHide;
    end;
  
  
    mChart.SeriesEx[xIndex].MarkClickPoint(True);
    with mChart.ClientToScreen(Point(X, Y)) do begin
      mDetailInfo.ShowInfo(Series, X, Y, ValueIndex);
    end;
  end // if (Button = mbLeft)...
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.mChartMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  mMouseMoved := False;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.mChartMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  mMouseMoved := True;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.mChartMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbRight) and (not mMouseMoved) then
    with mChart.ClientToScreen(Point(X, Y)) do
      pmProfile.Popup(X, Y);
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.mDataGridDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  ACol, ARow: Integer;
  xSrcData: TLabDataItem;
  
  //........................................................
  function GetDataRec(aColIndex: Integer): TLabDataItem;
  var
    xField: TField;
  begin
    Result := Nil;
    with mDataGrid.DataSource.DataSet do begin
      xField := FieldByName(mDataGrid.Columns[aColIndex].FieldName);
      if Assigned(xField) then
        Result := TLabDataItem(xField.Tag);
    end;
  end;
  //........................................................
  
begin
  if Sender = mDataGrid then begin
    with mDataGrid do begin
      MouseToCell(X, Y, ACol, ARow);
      if ACol <> -1 then begin
        CodeSite.SendFmtMsg('MoveColumn from %d to %d', [mStartCol, ACol]);
        xSrcData := GetDataRec(mStartCol);
  
        MoveColumn(mStartCol, ACol);
        SaveColumnProperties;
        if Assigned(xSrcData) then begin
          if xSrcData.KeyField then
            ReadAndFillGridData;
        end;
      end;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.mDataGridDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
var
  ACol, ARow: Integer;
  xSrcData: TLabDataItem;
  xDstData: TLabDataItem;
  
    //........................................................
  function GetDataRec(aColIndex: Integer): TLabDataItem;
  begin
    Result := TLabDataItem(mDataGrid.Columns[aColIndex].Field.Tag);
    if Assigned(Result) then
      CodeSite.SendString('Result', Result.FieldName);
  end;
    //........................................................
  
begin
  EnterMethod('mDataGridDragOver');
  Accept := False;
  if Sender = mDataGrid then begin
    with mDataGrid do begin
      MouseToCell(X, Y, ACol, ARow);
      CodeSite.SendInteger('ACol', aCol);
      if ACol > (FixedCols-1) then begin
        xSrcData := TLabDataItem(mDataGrid.Columns[mStartCol].Field.Tag);
        xDstData := TLabDataItem(mDataGrid.Columns[ACol].Field.Tag);
        if Assigned(xSrcData) and Assigned(xDstData) then begin
          Accept := (xSrcData.KeyField = xDstData.KeyField);
        end;
      end;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.mDataGridFillDetailGrid(Sender: TObject; aDetailGrid: TDBStringGrid);
begin
  //  EnterMethod('mDataGridFillDetailGrid');
  //  CodeSite.SendString('SQLWhere', aDetailGrid.SQLWhere);
  //  CodeSite.SendInteger('Level', aDetailGrid.Level);
  Screen.Cursor := crSQLWait;
  try
    if mDM.CreateAllQueryFields(dmGrid, aDetailGrid.Level, aDetailGrid.SQLWhere) then
      aDetailGrid.DataSource := dsGridDetail;
  finally
      // DataSource wieder abh�ngen, Daten bleiben aber in DetailGrid vorhanden
    aDetailGrid.DataSource := Nil;
    Screen.Cursor := crDefault;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.mDataGridGetExpandInfo(Sender: TObject; aExpandInfo: TExpandInfo);
var
  xItem: TLabDataItem;
begin
  EnterMethod('mDataGridGetExpandInfo');
  with aExpandInfo do begin
    CodeSite.SendInteger('Level', Level);
    if Level >= 0 then begin
      // Informationen f�r dieses Level besorgen
      xItem := mDataItemList.GetKeyItemForLevel(Level);
      if Assigned(xItem) then
        Color := xItem.Color;
  
      Expandable := (Level < mDataItemList.LevelCount);
      CodeSite.SendBoolean('Expandable', Expandable);
      if Expandable and (Level > 0) then begin
        // Informationen f�r Abfrage des n�chsten Levels abf�llen
        xItem := mDataItemList.GetKeyItemForLevel(Level-1);
        if Assigned(xItem) then begin
          ValueField := xItem.FieldName;  // Name f�r FieldByName()
          KeyField   := xItem.ColName;    // Name f�r Where Anweisung direkt auf DB-Feld c_XXX
          CodeSite.SendFmtMsg('ValueField / KeyField: %s / %s', [ValueField, KeyField]);
        end;
      end;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.mDataGridMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  ACol, ARow: Integer;
begin
  with mDataGrid do begin
    MouseToCell(X, Y, ACol, ARow);  // gets column and row
    if (ARow = 0) and (ACol > (FixedCols-1)) then begin  // drags only in row 1
      mStartCol := ACol; // stores starting column
      BeginDrag(False); // false starts drag when mouse has moved 5 pixels
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.mmActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  Handled := True;

  // Controls depending on Busy state of navigator component
  with MMSelectNavigator do
    tbMain.Enabled := not Busy;

  pcMain.Enabled := mAnalyseMode;

  bNavigate.Enabled := mNavigateButtonEnabled;

  acAddHotkeyProfile.Enabled    := (SettingsIniDB.ProfileName <> '');
  acDeleteHotkeyProfile.Enabled := (mProfileIndex <> -1);
  // Controls generaly on toolbar depending only from navigate/analyse state
  acPrint.Enabled         := mAnalyseMode and (Printer.Printers.Count > 0);
  acResetDB.Enabled       := not mAnalyseMode;
  acUseYMSettings.Enabled := not mAnalyseMode;
  acIntervalTime.Enabled  := not mAnalyseMode;
  acShiftTime.Enabled     := not mAnalyseMode;
  acLongTerm.Enabled      := not mAnalyseMode;

  // Components for chart functionality
  mLegendClickEnabled :=  not mChart.View3D and (acChartClickPoints.Checked or acChartColoring.Checked or acChartShowMassAvg.Checked);

  acChartShowDataSeries.Enabled        := mAnalyseMode;
  acChartShowCalcMethodSeries.Enabled  := mAnalyseMode and mHasCalcMethodSeries;
  acChartClickpoints.Enabled           := mAnalyseMode and not mChart.View3D;
  acChartColoring.Enabled              := mAnalyseMode and not mChart.View3D and mHasCalcMethodSeries and (not mDataItemList.ZAxisMode or not acStyleDetail.Checked);
  acChartShowMassAvg.Enabled           := mAnalyseMode and not mChart.View3D;

  acLotDetail.Enabled     := mAnalyseMode and mDataItemList.LotTrendMode;
  acStyleDetail.Enabled   := mAnalyseMode and acChartShowCalcMethodSeries.Checked and mHasCalcMethodSeries and mDataItemList.ZAxisMode;
  acPrevDataItem.Enabled  := mAnalyseMode and (mChartItemIndex > 0);
  acNextDataItem.Enabled  := mAnalyseMode and (mChartItemIndex <> -1) and (mChartItemIndex < cobChartItems.Items.Count-1);
//  acNextDataItem.Enabled  := mAnalyseMode and (mChartItemIndex <> -1) and (mChartItemIndex < mDataItemList.ChartItemCount-1);
  cobChartItems.Enabled   := mAnalyseMode;

  // Components on QualityMatrix tab
  cobXMLSettings.Enabled   := mAnalyseMode and (cobXMLSettings.Items.Count > 1);
  acQMChannel.Enabled     := mAnalyseMode;
  acQMSplice.Enabled      := mAnalyseMode;
  acQMFF.Enabled          := mAnalyseMode;
  acQMSettings.Enabled    := mAnalyseMode;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.MMSelectNavigatorAfterSelection(Sender: TObject);
begin
  mSelectedSeriesIndex := -1;
  mDataReady           := False;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.OpenReport(aParameter: WideString);
var
  xNewProfile: TMemIniFile;
begin
  if (DebugHook <> 0) and (aParameter = '') then begin
    aParameter := mDM.slQOTestParameter.CommaText;
  end;
  CodeSite.SendString('Execute Parameters', aParameter);


  xNewProfile := TMemIniFile.Create('');
  mChartItemIndex := -1;
  if ExtractQOfflimitParameters(aParameter, xNewProfile) then
  try
    mQOMode := True;
    Show;
    // dieses neue Profil nun anwenden
    AssignSettings(xNewProfile);

    mAnalyseMode := True;
    acNextPrevDataItemExecute(cobChartItems);
    UpdateNavigateAnalyseButton;
  finally
    xNewProfile.Free;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.pcMainChange(Sender: TObject);
begin
  fraRepSettings.mSpliceQMatrix.Hide;  //Nue:21.01.08
  ReadAndFillMatrixData;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.PreloadMachID(aMachID: String);
begin
  MMSelectNavigator.MachID := aMachID;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.ReadAndFillChartData;
var
  i: Integer;
  xIndex: Integer;
  xStr: String;
  xMassValue: Single;
  xSingle: Single;
  xXAxisNames: TmmStringList;
  xZAxisNames: TmmStringList;
  xTrendMode: Boolean;
  xValueItem: TLabDataItem;
  xFilterFieldName: String;
  xFilterMask: String;
  xSerie: TChartSeries;
  xSerieIntf: IExtendSeries;
  xZAxisMode: Boolean;
  x3DMode: Boolean;
  
  //........................................................
  procedure SortChartSeries;
  var
    i: Integer;
    xIndex: Integer;
  begin
    with mChart, SeriesList do begin
        i := Count-1;
        while i > 0 do begin
            // nur die Datenserien enthalten in Data ein g�ltiges Objekt
          if Assigned(SeriesEx[i].Data) then
            xIndex := TLabDataItem(SeriesEx[i].Data).ChartIndex
          else
            xIndex := -1;
  
          if (xIndex = -1) then begin
            Move(i, Count-1);
            dec(i);
          end else if (xIndex = i) then
            dec(i)
          else if xIndex < i then
            Move(i, xIndex)
          else if xIndex > i then begin
            Move(i, xIndex);
            dec(i);
          end;
        end; // while
    end; // with
  end;
    //........................................................
  function CreateNewChartSeries(aItem: TLabDataItem; aIDString: String; aColorIndex: Integer): TChartSeries;
  var
    xExtendSeries: IExtendSeries;
  begin
      // Serie erstellen
    case aItem.ChartType of
      ctBar: begin
          Result := TmmBarSeries.Create(Self);
          with TmmBarSeries(Result) do begin
            BarWidthPercent := 70;
            case mDataItemList.ChartOptions^.BarStyle of
              bsNormal:     Multibar := mbNone;
              bsSide:       MultiBar := mbSide;
              bsStacked:    MultiBar := mbStacked;
              bsStacked100: MultiBar := mbStacked100;
            else
              MultiBar := mbNone;
            end;
          end;
        end;
  
      ctArea: begin
          Result := T2DAreaSeries.Create(Self);
          with T2DAreaSeries(Result) do begin
            MultiArea   := maNone;
          end;
        end;
    else // ctLine
      Result := TmmLineSeries.Create(Self);
      with TmmLineSeries(Result) do begin
        if xTrendMode then
          DateTimeDelta := cSeriesDateTimeDeltas[mDataItemList.QueryParameters.TimeMode];
          // Grenz- und Mittelwert nur zeichnen, wenn keine Z-Achse gew�hlt wurde
        if not xZAxisMode then begin
          BorderCondition := aItem.BorderCondition;
        end; // if not
      end; // with
    end; // case ChartType
  
    if Assigned(Result) then begin
      Result.Marks.Visible      := False;
      Result.ParentChart        := mChart;
  //      Result.YValues.Multiplier := cYScalingFactors[aItem.YScalingFactor];
  
        // leer, wenn Datenelemente die Z-Achse f�llen
      if aIDString = '' then begin
        Result.Title       := Translate(aItem.DisplayName);
        Result.SeriesColor := aItem.Color
      end else begin
        Result.Title       := xZAxisNames.Values[aIDString];
        Result.SeriesColor := cMMProdGroupColors[(aColorIndex mod cColorRows)+1, (aColorIndex div cColorRows) + 1];
      end;
  
        // Interfaced Objekt abf�llen f�r die sp�tere Behandlung der Datenserien
      if Result.GetInterface(IExtendSeries, xExtendSeries) then begin
        xExtendSeries.Data  := aItem;
        xExtendSeries.Value := aIDString;  // enth�lt im ZAxisMode z.B. 'Customer D' oder im LotDetail '-12345'
      end;
  
        // wenn oben links/rechts vorhanden und Splitmodus aktiviert, dann eine 2. X-Achse
//      if mDataItemList.HasLeftTop and mDataItemList.HasRightTop and mDataItemList.ChartOptions^.SplitXAxis then begin
      if mDataItemList.DoSplitXAxis then begin
        mChart.BottomAxis.EndPosition := 48;
        if (aItem.AxisType in [atRight, atRightTop]) then
          Result.CustomHorizAxis := GetSplitXAxis;
      end else
        mChart.BottomAxis.EndPosition := 100;
  
        // Achsenausrichtung und zus�tzliche Achsenbezeichnung
      case aItem.AxisType of
        atLeft:     Result.VertAxis := aLeftAxis;
        atRight:    begin
            Result.VertAxis := aRightAxis;
            Result.Title    := Result.Title + ' (R)';
          end;
        atLeftTop:  begin
            if xZAxisMode or x3DMode then Result.VertAxis := aLeftAxis
                                     else Result.CustomVertAxis := GetAxisLeftTop;
            Result.Title := Result.Title + ' (LT)';
          end;
        atRightTop: begin
            if xZAxisMode or x3DMode then Result.VertAxis := aRightAxis
                                     else Result.CustomVertAxis := GetAxisRightTop;
            Result.Title := Result.Title + ' (RT)';
          end;
      else
      end;

      Result.XValues.DateTime := xTrendMode;
    end; // if Assigned(Result)
  end;
    //.........................................................
  procedure SetAxisProperties(aAxisType: TAxisType; aChartAxis: TChartAxis);
  begin
    with mDataItemList.AxisInfo[aAxisType]^ do begin
      case Scaling of
        asAutomatic: aChartAxis.Automatic := True;
        asManual:    aChartAxis.SetMinMax(Min, Max);
        asDetermine: aChartAxis.Automatic := True; // wird sp�ter bei der Berechnung �bersteuert RefreshSeries
      else
      end;
    end;
  end;
    //.........................................................
  function PrepareXAxis: Boolean;
  begin
    Result := True;
    mChart.BottomAxis.LabelsMultiLine := True;
    if xTrendMode then begin
      case MMSelectNavigator.TimeMode of
        tmInterval: begin
            mChart.BottomAxis.Increment := DateTimeStep[dtOneHour];
            mChart.BottomAxis.DateTimeFormat := Format('%s %s', [ShortDateFormat, ShortTimeFormat]);
          end;
        tmLongtermWeek: begin
            mChart.BottomAxis.Increment := DateTimeStep[dtOneWeek];
            mChart.BottomAxis.DateTimeFormat := ShortDateFormat;
          end;
      else // tmShift
        mChart.BottomAxis.Increment := DateTimeStep[dtSixHours];
        mChart.BottomAxis.DateTimeFormat := Format('%s %s', [ShortDateFormat, ShortTimeFormat]);
      end;
      mChart.BottomAxis.LabelsSize := 20;
    end else begin
      mChart.BottomAxis.Increment  := 0;
      mChart.BottomAxis.LabelsSize := 0;
  
        //-----------
        // wenn nicht im TrendMode dann die Beschriftung f�r die X-Achse vorholen
        //-----------
      if not mDM.GetXAxisNamesForChart(mDataItemList.XAxisItem.ColName, xXAxisNames) then begin
        Result := False;
        Exit;
      end;
    end; // if xTrendMode
  end;
    //.........................................................
  function PrepareZAxis: Boolean;
  var
    i: Integer;
  begin
    Result := True;
    xValueItem := Nil;  // im ZAxisMode wird es hier 1x bestimmt, ansonsten jeweils aus xZAxisNames ermittelt
    with mDataItemList do
    try
      for i:=0 to DataItemCount-1 do begin
        if Items[i].ShowInChart then begin
          // Im ZAxisMode nach dem Element suchen, von welchem die Daten dargestellt werden
          if xZAxisMode then begin
            // in mChartItemIndex steht der Index vom aktive Chart Element
            if Items[i].ChartIndex = mChartItemIndex then begin
              xValueItem := Items[i];
              if LotDetails then begin
                xFilterFieldName := LotDetailItem.FieldName;
                xFilterMask      := '%s = %s';
              end else begin
                xFilterFieldName := ZAxisItem.FieldName;
                xFilterMask      := '%s = ''%s''';
              end;
              // und gleich auch noch die zu erwartenden Serien aus der DB lesen
              if not mDM.GetZAxisNamesForChart(ZAxisItem, LotDetails, xZAxisNames) then begin
                Result := False;
                Exit;
              end;
              Break;
            end; // if ChartIndex
          end
          else begin
            // Im not ZAxisMode die Liste mit den Chart Elementen f�llen (ItemName = CalcName)
            // CYTot = calc_CYTot
            // PEff  = calc_PEff
            if not Items[i].KeyField then begin
//              with Items[i] do
//                CodeSite.SendFmtMsg('xZAxisNamesValues: %s, %s, %s, %s', [ItemName, DisplayName, FieldName, CalcName]);
//              xZAxisNames.Values[Items[i].ItemName] := Items[i].CalcName;
              xZAxisNames.Values[Items[i].FieldName] := Items[i].CalcName;
            end;
          end; // if xZAxisMode
        end; // if ShowInChart
      end; // for i
    except
      on e:Exception do
        CodeSite.SendError('PrepareZAxis: ' + e.Message);
    end;
  end;
    //.........................................................
  
begin
  EnterMethod('TfrmLabReport.ReadAndFillChartData');
  //-----------
  // Initialisieren
  //-----------
  acChartClickPoints.Checked := True;
  mHasCalcMethodSeries := False;
  Screen.Cursor := crSQLWait;
  mChart.FreeAllSeries;
  
  // wird nur im not TrendMode gef�llt f�r Mach1, Mach2, etc
  xXAxisNames := TmmStringList.Create;
  // not ZAxisMode: Namen der ChartElemente, ZAxisMode: Werte aus DB
  xZAxisNames := TmmStringList.Create;
  
  // Achsen formatieren
  SetAxisProperties(atLeft,  mChart.LeftAxis);
  SetAxisProperties(atRight, mChart.RightAxis);
  if mDataItemList.HasLeftTop then
    SetAxisProperties(atLeftTop, GetAxisLeftTop);
  if mDataItemList.HasRightTop then
    SetAxisProperties(atRightTop, GetAxisRightTop);
  
  with mDataItemList do
  try
    if not Assigned(XAxisItem) then
      Exit;
  
    xTrendMode := TrendMode;
    xZAxisMode := ZAxisMode;
    x3DMode    := ChartOptions^.View3D;

    // if multiple axis are used than change top start of basic left/right axis
    if HasLeftTop and not xZAxisMode then mChart.LeftAxis.StartPosition := 52
                                     else mChart.LeftAxis.StartPosition := 0;
    if HasRightTop and not xZAxisMode then mChart.RightAxis.StartPosition := 52
                                      else mChart.RightAxis.StartPosition := 0;
  
    //-----------
    // Initialisiere Daten f�r X-Achse
    //-----------
    if not PrepareXAxis then
      Exit;
    //-----------
    //  ZAxisMode: suche nach dem aktiven Chart Element (=mChartItemIndex) oder f�lle die ZAxisList mit den Chart Elementen
    //-----------
    if not PrepareZAxis then
      Exit;
  
    CodeSite.SendStringList('xXAxisNames', xXAxisNames);
    CodeSite.SendStringList('xZAxisNames', xZAxisNames);
  
    if mDM.CreateAllQueryFields(dmChart) then
    try
      for i:=0 to xZAxisNames.Count-1 do begin
        //-----------
        // Filtrieren oder ValueItem bestimmen
        //-----------
        if xZAxisMode then begin
          xStr := xZAxisNames.Names[i];
          // erstellt ein Filterstring z.B. "prod_id = -1" oder "prod_name = 'Partie 1'"
          mDM.ResultQuery.RecordSet.Filter   := Format(xFilterMask, [xFilterFieldName, xStr]);
          // xValueItem wurde oben 1x ermittelt und entspricht z.B. dem CYTot Element
          xSerie := CreateNewChartSeries(xValueItem, xStr, (i mod (cColorRows * cColorCols)));
        end else begin
          // xValueItem entspricht jeweils einem neuen Chart Element CYTot, CS, CFF, etc
          xValueItem := Items[IndexOfFieldName(xZAxisNames.Names[i])];
//          xValueItem := TLabDataItem(DataItemByName[xZAxisNames.Names[i]]);
          // und f�r dieses wird nun eine neue Chart Serie erstellt
          xSerie := CreateNewChartSeries(xValueItem, '', -1);
        end;

        //-----------
        // Daten in die Serie abf�llen
        //-----------
        with mDM.ResultQuery do
        try
          First;
          while not EOF do begin
            xSingle := FieldByName(xValueItem.CalcName).AsFloat;

            xSerie.GetInterface(IExtendSeries, xSerieIntf);
            if xTrendMode and Assigned(xSerieIntf) then begin
              if xValueItem.MassFieldName = '' then
                xMassValue := 1.0
              else
                xMassValue := FieldByName(xValueItem.MassFieldName).AsFloat;

              xSerieIntf.AddXYMass(FieldByName(mDataItemList.XAxisItem.CalcName).AsDateTime, xSingle, xMassValue, '', clTeeColor);

//              if xValueItem.MassFieldName <> '' then begin
//                xMassValue := FieldByName(xValueItem.MassFieldName).AsFloat;
//                xSerie.GetInterface(IExtendSeries, xSerieIntf);
//                xSerieIntf.AddXYMass(FieldByName(mDataItemList.XAxisItem.CalcName).AsDateTime, xSingle, xMassValue, '', clTeeColor);
//              end else
//                xSerie.AddXY(FieldByName(mDataItemList.XAxisItem.CalcName).AsDateTime, xSingle, '', clTeeColor);
            end else begin
              // hole Index von diesem X-Achsen Wert und verwende diesen f�r die X-Achse
              xIndex := xXAxisNames.IndexOf(FieldByName(mDataItemList.XAxisItem.CalcName).AsString);
              if xIndex >= 0 then
                xSerie.AddXY(xIndex, xSingle, xXAxisNames.Strings[xIndex], clTeeColor);
            end; // if ChartType = ctLine
  
            Next;
          end; // while not EOF
  
          with AxisInfo[xValueItem.AxisType]^ do begin
            if Scaling = asDetermine then
              DetermineMinMaxForAxis(xSerie);
          end;
  
        except
          on e:Exception do
            CodeSite.SendError('mDM.ResultQuery = ' + e.Message);
        end;
        // im NormalMode f�r jedes Datenelement die Berechnungsmethoden erstellen
        if not xZAxisMode then
          ShowCalcMethods(xValueItem, xSerie);
      end; // for i
  
      if xZAxisMode then
        // im ZAxisMode nur f�r das aktuelle Datenelement die Berechnungsmethoden erstellen
        ShowCalcMethods(xValueItem, Nil)
      else
        // now sort the series list by index of settings dialog (index is stored in tag field of series)
        SortChartSeries;
    except
      on e:Exception do
        CodeSite.SendError('GetXAxisNamesForChart: ' + e.Message);
    end; // if mDM.CreateAllQueryFields(dmChart)
    CheckChartVisibility;
  finally
    Screen.Cursor := crDefault;
    xXAxisNames.Free;
    xZAxisNames.Free;
  end;// with mDataItemList
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.ReadAndFillGridData;
var
  i: Integer;
  xIndex: Integer;
begin
  try
    Screen.Cursor := crSQLWait;
    mDataGrid.Level := 0;
    if mDM.CreateAllQueryFields(dmGrid, 0) then
    try
      // now set column properties to grid
      for i:=1 to mDataGrid.ColCount-1 do begin
        with mDataGrid.Columns[i] do begin
          // get index of this field from the data item list
          xIndex := mDataItemList.IndexOfFieldName(Field.FieldName);
          if xIndex <> -1 then
            mDataGrid.ColWidths[i] := mDataItemList.Items[xIndex].DisplayWidth;
        end;
      end;
    except
      on e:Exception do
        CodeSite.SendError('ReadAndFillGridData: ' + e.Message);
    end;
  finally
    Screen.Cursor := crDefault;
  end;// try finally
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.ReadAndFillMatrixData;
var
  xMin: Single;
  xMax: Single;
  
  //..........................................................................
  procedure GetMinMaxValues(aFrameMode: TFrameMode);
  var
    i: Integer;
    xSingle: Single;
  begin
    xSingle := 0;
    // get first value of class data
    case aFrameMode of
      fmChannelData: xMin := mCDR.ClassUncut[1];
      fmSpliceData:  xMin := mCDR.SpliceUncut[1];
      fmFFData:      xMin := mCDR.SiroUncut[1];
    else
    end;
    xMax := xMin;
  
    with fraRepSettings do
      // ClassDataReader hat IMMER volle FeldAnzahl, auch bei Siro wegen Bright Werten
      for i:=2 to cFrameModeFieldCount[aFrameMode] do begin
        case aFrameMode of
          fmChannelData: xSingle := mCDR.ClassUncut[i];
          fmSpliceData:  xSingle := mCDR.SpliceUncut[i];
          fmFFData:      xSingle := mCDR.SiroUncut[i];
        else
        end;
        if xSingle > xMax then xMax := xSingle;
        if (xSingle < xMin) and (xSingle > 0) then xMin := xSingle;
      end;
  end;
  //..........................................................................
  procedure FillColorAndDots(aFrameMode: TFrameMode);
  var
    i: Integer;
    xSingle: Single;
    xDataView: TClassDataTyp;
    xCount: Integer;
  begin
    case aFrameMode of
      fmChannelData: xDataView := cdClassUncut;
      fmSpliceData:  xDataView := cdSpliceUncut;
      fmFFData:      xDataView := cdSiroUncut;
    else
      Exit;
    end;

    with fraRepSettings do begin
      xCount := mQMatrix.TotFields;
      for i:=0 to xCount-1 do begin
        xSingle := mCDR.Value[xDataView, i];
        mQMatrix.FieldColor[i] := GetDotColorValue(dcvColor, 0, 20, xMin, xMax, xSingle);
        mQMatrix.FieldDots[i]  := GetDotColorValue(dcvDot, 600, 20, xMin, xMax, xSingle);
        mSpliceQMatrix.FieldColor[i] := GetDotColorValue(dcvColor, 0, 20, xMin, xMax, xSingle);    //Nue:20.01.08
        mSpliceQMatrix.FieldDots[i]  := GetDotColorValue(dcvDot, 600, 20, xMin, xMax, xSingle);    //Nue:20.01.08
      end;                                                                                         
    end; // with fraRepSettings
  end;
  //..........................................................................
begin
  cobXMLSettings.Clear;
  if mDataReady then begin
    // prepare combobox of YMSettings anyway, unless this TabSheet is active or not
    if pcMain.ActivePage = tsQualityMatrix then begin
      if cobXMLSettings.Items.Count = 0 then begin
        // fill in resulting YMSettings
        cobXMLSettings.Items.Assign(mCDR.XMLSettingNames);
        cobXMLSettings.ItemIndex := -1;

        // change visibility of the combobox if there are depending of 1 or more clearer settings
        if cobXMLSettings.Items.Count > 1 then begin
          fraRepSettings.WarningMsg := cswManySettings;
        end else begin
          fraRepSettings.WarningMsg := cswNone;
        end;
      end;

      // if there is at least one settings available -> select the first
      if cobXMLSettings.ItemIndex = -1 then begin
        cobXMLSettings.ItemIndex := 0;
        // activate settings
        cobXMLSettingsChange(Nil);
      end;
      CheckMatrixVisibility;

      // FillIn values for class data
      with fraRepSettings do begin
        if FrameMode = fmSettings then
          Clear
        else begin
          GetMinMaxValues(FrameMode);
          with mDataItemList.MatrixOptions^ do
            mCDR.FillValuesToMatrix(DefectValues, CutValues, mQMatrix);
          with mDataItemList.SpliceMatrixOptions^ do                            //Nue:20.01.08
            mCDR.FillValuesToMatrix(DefectValues, CutValues, mSpliceQMatrix);   //Nue:20.01.08
        end; // if FrameMode
      end;
    end; // if pcMain
  end; // if mDataReady
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.ReadData;
begin
  // ReadData wird auch aufgerufen, wenn nicht im AnalyseMode ist, daher kann hier
  // die Daten gel�scht werden
  mChart.FreeAllSeries;
  with mDataGrid.DataSource do begin
    if Assigned(DataSet) then
      DataSet.Close;
  end;
  fraRepSettings.Clear;

  if mAnalyseMode then begin
    mChart.UndoZoom;
    with mDataItemList do begin
      acPrevDataItem.Visible := ZAxisMode;
      acNextDataItem.Visible := acPrevDataItem.Visible;
      cobChartItems.Visible  := acPrevDataItem.Visible;
      acLotDetail.Visible    := acPrevDataItem.Visible;
      acStyleDetail.Visible  := acPrevDataItem.Visible;
      LotDetails             := acLotDetail.Checked;
    end;
  
//    mSelectedSeriesIndex := -1;
  
    with mDM.DBParams do begin
      StyleIDs := MMSelectNavigator.StyleID;
      ProdIDs  := MMSelectNavigator.LotID;
      MachIDs  := MMSelectNavigator.MachID;
  
      TimeMode := MMSelectNavigator.TimeMode;
      if TimeMode = tmInterval then
        TimeIDs  := MMSelectNavigator.TimeID
      else begin
        TimeFrom := MMSelectNavigator.TimeFrom;
        TimeTo   := MMSelectNavigator.TimeTo;
      end;
  
      if MMSelectNavigator.UseClearerSettings then
        YMSetIDs := MMSelectNavigator.YMSetID
      else
        YMSetIDs := '';
    end;
    //  mCDR.Params.Assign(mDM.DBParams);   //NUE
    mCDR.Params                   := mDM.DBParams;
    mDataItemList.QueryParameters := mDM.DBParams;

      // Im Intervalmodus werden bei Bedarf Klassierdaten vom Image in eine TempTabelle ausgepackt
    with mDataItemList do begin
      PrepareTable;
    end;

    ReadAndFillChartData;
    ReadAndFillGridData;
    mDataReady := True;

    ReadAndFillMatrixData;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.RestoreFormLayout(aRegistryKey: String = '');
begin
  inherited RestoreFormLayout(aRegistryKey);
  MMSelectNavigator.Height := ClientHeight div 3;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.rgModeClick(Sender: TObject);
begin
  case rgMode.ItemIndex of
//    0: fraRepSettings.mQMatrix.DisplayMode := dmValues;
//    1: fraRepSettings.mQMatrix.DisplayMode := dmColor;
//    2: fraRepSettings.mQMatrix.DisplayMode := dmDots;
    0: fraRepSettings.mQMatrix.DisplayMode := dmValues;
    1: fraRepSettings.mQMatrix.DisplayMode := dmColor;
    2: fraRepSettings.mQMatrix.DisplayMode := dmDots;
  else
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.SaveColumnProperties;
var
  i: Integer;
  xItem: TLabDataItem;
begin
  if mDataGrid.DataSource.DataSet.Active then begin
    for i:=1 to mDataGrid.ColCount-1 do begin
      with mDataGrid.Columns[i] do begin
        if Field.Visible then begin
          // get index of this field from the data item list
          xItem := TLabDataItem(Field.Tag);
          if Assigned(xItem) then begin
            xItem.TableIndex   := i-1;
            xItem.DisplayWidth := mDataGrid.ColWidths[i];
            CodeSite.SendFmtMsg('DataItem: %s, %d', [xItem.DisplayName, xItem.TableIndex]);
          end;
        end;
      end;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.SaveProfile(aDefault: Boolean);
var
  xBool: Boolean;
begin
  SaveColumnProperties;
  SettingsIniDB.Ini.Clear;
  mDataItemList.WriteToConfig(SettingsIniDB.Ini);
  if aDefault then xBool := SettingsIniDB.SaveDefault
              else xBool := SettingsIniDB.SaveSettings(rsSaveSelection);
  
  if xBool then begin
    mProfileIndex := -1;
    hcProfiles.Invalidate;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.seAvgPercentageChange(Sender: TObject);
begin
//
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.seBorderConditionChange(Sender: TObject);
begin
//
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.seFixPercentageChange(Sender: TObject);
begin
//
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.seShiftCompressionChange(Sender: TObject);
begin
//
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.SetPropertiesToChart;
begin
  with mDataItemList.ChartOptions^ do begin
    mChart.View3D                    := View3D;
    mChart.View3DOptions.Orthogonal  := not AllowRotate;
    mChart.View3DOptions.Rotation    := RotateX;
    mChart.View3DOptions.Elevation   := RotateY;
    mChart.View3DOptions.HorizOffset := HPos;
    mChart.View3DOptions.VertOffset  := VPos;
    mChart.View3DOptions.Zoom        := Zoom;
    if mChart.View3D then
      mChart.Chart3DPercent := Width3D;

    mChart.Legend.Alignment         := LegendAlign;

    mChart.Gradient.EndColor   := StartColor;
    mChart.Gradient.StartColor := EndColor;
    case GradientDir of
      0: mChart.Gradient.Direction := gdTopBottom;
      1: mChart.Gradient.Direction := gdBottomTop;
      2: mChart.Gradient.Direction := gdLeftRight;
      3: mChart.Gradient.Direction := gdRightLeft;
    else
    end;

    if ImageFile = '' then
      mChart.BackImage := Nil
    else if FileExists(ImageFile) then begin
      mChart.BackImage.LoadFromFile(ImageFile);
      mChart.BackImageMode := pbmStretch;
    end;

    mChart.BackImageInside := ImageInside;
    mChart.BottomAxis.LabelsAngle := cXLabelAngle[XLabelRotated];
  end;

  bChartMove.Enabled        := mChart.View3D and not mChart.View3DOptions.Orthogonal;
  bChartOrientation.Enabled := mChart.View3D and not mChart.View3DOptions.Orthogonal;
  acChartZoomIn.Enabled     := mChart.View3D and not mChart.View3DOptions.Orthogonal;
  acChartZoomOut.Enabled    := mChart.View3D and not mChart.View3DOptions.Orthogonal;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.SetPropertiesToGrid;
begin
  with mDataItemList.GridOptions^ do begin
    mDataGrid.FixedColor      := TitleFont.BkColor;
    mDataGrid.TitleFont.Name  := TitleFont.Name;
    mDataGrid.TitleFont.Color := TitleFont.Color;
    mDataGrid.TitleFont.Size  := TitleFont.Size;
    mDataGrid.TitleFont.Style := TitleFont.Style;
  
    mDataGrid.Color      := DataFont.BkColor;
    mDataGrid.Font.Name  := DataFont.Name;
    mDataGrid.Font.Color := DataFont.Color;
    mDataGrid.Font.Size  := DataFont.Size;
    mDataGrid.Font.Style := DataFont.Style;
  
    mDataGrid.EnableDetails := Hierarchy;
  
    if 0 in Options then mDataGrid.Options := mDataGrid.Options + [goHorzLine]
                    else mDataGrid.Options := mDataGrid.Options - [goHorzLine];
    if 1 in Options then mDataGrid.Options := mDataGrid.Options + [goVertLine]
                    else mDataGrid.Options := mDataGrid.Options - [goVertLine];
    if 3 in Options then mDataGrid.Options := mDataGrid.Options + [goRowSelect]
                    else mDataGrid.Options := mDataGrid.Options - [goRowSelect];
  end;
  mDataGrid.Invalidate;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.SetPropertiesToMatrix;
begin
  with fraRepSettings do begin
    with mQMatrix do begin
      ActiveColor   := mDataItemList.MatrixOptions^.ActiveColor;
      InactiveColor := mDataItemList.MatrixOptions^.InactiveColor;
      DisplayMode   := cDisplayModeIndex[mDataitemList.MatrixOptions^.MatrixMode];
      MatrixMode    := mmDisplayData;
      case FrameMode of
        fmChannelData: begin
            ActiveVisible  := mDataItemList.MatrixOptions^.ActiveVisible;
            ChannelVisible := mDataItemList.MatrixOptions^.ChannelVisible;
            ClusterVisible := mDataItemList.MatrixOptions^.ClusterVisible;
            SpliceVisible  := False;
            Invalidate;   //Nue:21.01.08
          end;
        fmSpliceData: begin
//            ActiveVisible  := False;
//            ChannelVisible := False;
//            ClusterVisible := False;
//            SpliceVisible  := mDataItemList.MatrixOptions^.SpliceVisible;
            mSpliceQMatrix.ActiveColor    := mDataItemList.SpliceMatrixOptions^.ActiveColor;    //Nue:21.01.08
            mSpliceQMatrix.InactiveColor  := mDataItemList.SpliceMatrixOptions^.InactiveColor;  //Nue:21.01.08
            mSpliceQMatrix.ActiveVisible  := mDataItemList.SpliceMatrixOptions^.ActiveVisible;  //Nue:17.09.08
            mSpliceQMatrix.ChannelVisible := False;        //Nue:21.01.08
            mSpliceQMatrix.ClusterVisible := False;        //Nue:21.01.08
            mSpliceQMatrix.SpliceVisible  := mDataItemList.SpliceMatrixOptions^.SpliceVisible;   //Nue:21.01.08
            mSpliceQMatrix.Invalidate;                     //Nue:21.01.08
          end;
        fmFFData: begin
            ActiveVisible  := mDataItemList.MatrixOptions^.ActiveVisible;
            ChannelVisible := False;
            ClusterVisible := False;
            SpliceVisible  := False;
            Invalidate;   //Nue:21.01.08
          end;
        fmSettings: begin
            ActiveVisible  := mDataItemList.MatrixOptions^.ActiveVisible;
            ChannelVisible := True;
            ClusterVisible := True;
//            SpliceVisible  := True;
            SpliceVisible  := False;  //Nue:21.01.08
            Invalidate;   //Nue:21.01.08
          end;
      else
      end;
//      Invalidate;
    end; // with mQMatrix
    with mFMatrix do begin
      ActiveColor   := mDataItemList.MatrixOptions^.ActiveColor;
      InactiveColor := mDataItemList.MatrixOptions^.InactiveColor;
      ActiveVisible := mDataItemList.MatrixOptions^.ActiveVisible;
      DisplayMode   := cDisplayModeIndex[mDataItemList.MatrixOptions^.MatrixMode];
      MatrixMode    := mmDisplayData;
      Invalidate;
    end; // with mFMatrix
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.SettingsIniDBBeforeSave(Sender: TSettingsIniDB; aOptionalChecked: Boolean);
begin
  if aOptionalChecked and not AnsiSameText(Sender.ProfileName, 'DEFAULT') then
    Sender.Ini.WriteString('Selection', 'Selection', MMSelectNavigator.Selection);
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.ShowCalcMethods(aLabItem: TLabDataItem; aLinkSerie: TChartSeries);
var
  i: TCalculationMethod;
  j, k: Integer;
  xLVList: TLimitValueList;
  xMethodSettings: TBaseCalcMethodSettings;
  xSerie: TmmLineSeries;
  xLimitValues: TLimitValues;
  xColor: TColor;

    //........................................................
  function GetStyleIDs: String;
  var
    i: Integer;
  begin
    Result := MMSelectNavigator.StyleID;
    if Result = '' then begin
      // wenn keine Selektion vorhanden ist, dann werden alle IDs verwendet
      with TmmStringList.Create do
      try
        for i:=1 to MMSelectNavigator.lbStyle.RowCount-1 do begin
          Add(IntToStr(Integer(MMSelectNavigator.lbStyle.Objects[0, i])));
//        for i:=1 to MMSelectNavigator.lbLot.RowCount-1 do begin
//          Add(IntToStr(Integer(MMSelectNavigator.lbLot.Objects[0, i])));
        end;
        Result := CommaText;
      finally
        Free;
      end;
    end;
    Result := StringReplace(Result, '''', '', [rfReplaceAll]);
    CodeSite.SendString('GetStyleIDs', Result);
  end;
    //........................................................
  function GetSerieTitle: String;
  begin
    Result := '';
    if mDataItemList.ZAxisMode then begin
      if xLimitValues.CalcMethodType = cmFix then begin
        Result := Translate(cCalculationMethods[cmFix].Name);
        Exit;
      end
      else begin
        if xLVList.BaseMethodSettings.SingleResult then
          Result := rsLabelStyle
        else
          Result := MMSelectNavigator.StyleNameFromID[xLimitValues.StyleID];
      end; // if xLimitValues.CalcMethodType
    end else
      Result := Translate(aLabItem.DisplayName);
  
    Result := Format('%s - %s', [Result, xLimitValues.ShortTitle]);
  //    Result := Format('%s - %s', [Result, cCalculationMethods[xLimitValues.CalcMethodType].ShortName]);
  end;
    //........................................................
  
begin
  // Berechnungsmethoden nur dann bearbeiten, wenn im Schichtmodus und Berechnunsmethoden vorhanden sind
  if (mDataItemList.QueryParameters.TimeMode = tmShift) and (aLabItem.CalcMethodCount > 0) then begin
    mHasCalcMethodSeries := True;
    Screen.Cursor := crSQLWait;
    xLVList       := TLimitValueList.Create;
    try
      xMethodSettings := TBaseCalcMethodSettings.Create;
      with xMethodSettings do
      try
        DataItem := aLabItem;
        LenBase  := mLenBase;
        TimeFrom := MMSelectNavigator.TimeFrom;
        TimeTo   := MMSelectNavigator.TimeTo;
        SingleResult := (not mDataItemList.ZAxisMode) or (not acStyleDetail.Checked);
        StyleIDs := GetStyleIDs;
        xLVList.BaseMethodSettings := xMethodSettings;
      finally
        Free;
      end;
  
      for i:=Low(TCalculationMethod) to High(TCalculationMethod) do begin
        if aLabItem.CalcMethods[i] <> '' then begin
          xLVList.SetMethodSettings(aLabItem.CalcMethods[i]);
          for j:=0 to xLVList.Count-1 do begin
            xLimitValues := xLVList.LimitValues[j];
            if Assigned(xLimitValues) then begin
              // eine neue Serie erstellen
              xSerie := TmmLineSeries.Create(Self);
              if aLinkSerie is TmmLineSeries then
                TmmLineSeries(aLinkSerie).BorderSeries.Add(xSerie);

              with xSerie do begin
                LinePen.Style := psSolid;
                LinePen.Width := 2;
                ParentChart   := mChart;
                Title         := GetSerieTitle;
                XValues.DateTime := mDataItemList.TrendMode;
                case aLabItem.AxisType of
                  atLeft:     VertAxis := aLeftAxis;
                  atRight:    VertAxis := aRightAxis;
                  atLeftTop:  if mDataItemList.ZAxisMode then VertAxis       := aLeftAxis
                                                         else CustomVertAxis := GetAxisLeftTop;
                  atRightTop: if mDataItemList.ZAxisMode then VertAxis       := aRightAxis
                                                         else CustomVertAxis := GetAxisRightTop;
                else
                end; // case
                // Erweiterte Eigenschaften
                Extended.Tag  := xLimitValues.StyleID;  // SingleResult = -1, ansonsten die StyleID
                Extended.Data := Nil;
              end; // with xSerie

              // Farbe bestimmen f�r die Serie
              if mDataItemList.ZAxisMode then begin
                xColor := clTeeColor;
              end
              else begin
                xColor := GetGradientColor(aLabItem.Color, ((ord(i)+1) * -15));
                xSerie.SeriesColor := xColor;
              end;

              // Daten abf�llen
              GetAsyncKeyState(VK_ESCAPE);
              for k:=0 to xLimitValues.Count-1 do begin
                if GetAsyncKeyState(VK_ESCAPE) <> 0 then
                  Break;
                with xLimitValues.GetValue(k) do begin
                  if Calculated then
                    xSerie.AddXY(TimeStamp, Value, '', xColor);
                end; // with
              end; // for k
            end; // if Assigned(xLimitValues)
          end; // for j
        end; // if <> ''
      end; // for i
  
      if aLinkSerie is TmmLineSeries then
        TmmLineSeries(aLinkSerie).RefreshSeries;
    finally
      xLVList.Free;
      Screen.Cursor := crDefault;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLabReport.UpdateNavigateAnalyseButton;
begin
  acResetChart.Execute;
  // Optik bestimmen
  with bNavigate do begin
    // Zeige Infos auf Button fuer naechsten Mode an
    if mAnalyseMode then begin
      Caption := rsNavigate;
      Glyph.LoadFromResourceName(HInstance, 'NAVIGATE');
    end else begin
      Caption := rsAnalyse;
      Glyph.LoadFromResourceName(HInstance, 'ANALYSE');
    end;
    Update;
  end;
  MMSelectNavigator.ShowSelection := not mAnalyseMode;
  MMSelectNavigator.Update;
end;

procedure TfrmLabReport.mChartGetLegendText(Sender: TCustomAxisPanel;
  LegendStyle: TLegendStyle; Index: Integer; var LegendText: String);
begin
  // Markiert die gew�hlte Serie
  if Index = mSelectedSeriesIndex then
    Sender.Canvas.Font.Style := [fsBold]
  else
    Sender.Canvas.Font.Style := [];
end;

procedure TfrmLabReport.MMSelectNavigatorAfterFilledAllLists(Sender: TObject);
begin
  with MMSelectNavigator do
    mNavigateButtonEnabled := mAnalyseMode OR (
      (lbStyle.Cols[0].Count > 1) and (lbLot.Cols[0].Count > 1) and (lbMachine.Cols[0].Count > 1) and
      (not UseClearerSettings or (lbYMSettings.Cols[0].Count > 1)) );
end;

end.













