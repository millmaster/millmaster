(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: PrintSettingsDialog.pas
| Projectpart...:
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
|-------------------------------------------------------------------------------
| 13.03.2001 1.00 Wss | File created
| 30.08.2001 1.00 Wss | - fill data for Printout changed
                        - Enable/disable visibility for filter values
| 13.09.2001 1.00 Wss | Parent of PrintDialogSetup set to this form
| 16.01.2003 1.00 Wss | Kleinere Korrekturen wegen SpliceMatrix
| 28.01.2003 1.00 Wss | Nach dem Abf�llen der Parameter werden die einzelnen OnClick Events
                        diverser Checkbox aufgerufen, um die jeweiligen Bandgr�ssen zu bestimmen
                        damit auch beim wiederholten ausdrucken der Platz optimal ausgef�llt wird.
| 01.10.2003 1.00 Wss | Wenn mit Tabelle gedruckt werden soll, dann �ber die Print Komponente
                        Umleiten
| 17.02.2004 1.00 Wss | Beim Drucken die Gr�sse der Controls wieder an original Zoom umrechnen
| 20.02.2004 1.00 Wss | Defaultposition der Matrixen angepasst an Reinigerassistent
| 21.06.2004 1.00 SDo | Print-Layout angepasst; Header, Footer, mPrintSetup, Windowsize
| 16.06.2005 1.01 Wss | "PrintRepetition" hinzugef�gt
| 15.07.2005 1.01 Wss | DialogGr�sse war zwecks GUI Entwicklung vergr�ssert worden -> im Kompilat waren die
                        Buttons unten rechts nicht mehr sichtbar
| 07.06.2007 1.02 Nue | Auch alle NonMatrix-SettingsKomponenten move- and sizeable gemacht.
                        In TfrmPrintDialog.mQuickReportSelectPrintable {Nue:7.6.07}or (Sender is TQRXMLSettings) added.
| 19.11.2007 1.03 Nue | Adding additional PrintSettings-Komponents to saved settings on DB (t_settings)
| 10.01.2008 1.04 Nue | Einbau von VCV.
| 20.01.2008      Nue | Einbau Spleissklassierung.
|=============================================================================*)
unit PrintSettingsDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, mmBitBtn, QuickRpt,
  mmQuickRep, ExtCtrls, mmPanel, mmButton, IvMlDlgs, mmPrinterSetupDialog,
  mmPrintDialog, Qrctrls, mmQRLabel, mmGroupBox, mmCheckBox, mmLabel, mmEdit,
  PrintSetupFrame, QRPrintGrid,
  TeeProcs, TeEngine, mmQRChart, mmQRExpr, mmQRSysData, mmSpeedButton,
  YMQRUnit, mmScrollBox, ActnList, mmActionList, IvDictio, IvMulti, IvEMulti,
  mmTranslator, QrTee, BASEFORM, Chart, DBChart,
  LabMasterDef, LabReportClass, ClassDataReader,
  mmComboBox, mmLineLabel, mmQRBand, mmQRShape, DataLayerDef,
  mmQRImage, QualityMatrixBase, QualityMatrix, XMLSettingsModel,
  VCLXMLSettingsModel, MMUGlobal;

type
  {:----------------------------------------------------------------------------
   }
  TfrmPrintDialog = class (TmmForm)
    acPreview: TAction;
    acZoomIn: TAction;
    acZoomOut: TAction;
    bCancel: TmmBitBtn;
    bOK: TmmBitBtn;
    bPreview: TmmButton;
    bZoomIn: TmmSpeedButton;
    bZoomOut: TmmSpeedButton;
    cbFilterLot: TmmCheckBox;
    cbFilterMachine: TmmCheckBox;
    cbFilterStyle: TmmCheckBox;
    cbFilterYMSettings: TmmCheckBox;
    cbNewPageMatrix: TmmCheckBox;
    cbNewPageTable: TmmCheckBox;
    cbNewPageAdditionalSettings: TmmCheckBox;
    
    cbPrintClassData: TmmCheckBox;
    cbCSClusterCurve: TmmCheckBox;
    cbPrintFFData: TmmCheckBox;
    cbPrintSpliceData: TmmCheckBox;
    cbPrintTable: TmmCheckBox;
    cbCSYarnCount: TmmCheckBox;
    cbStretchTable: TmmCheckBox;
    edCompany: TmmEdit;
    edReportTitle: TmmEdit;
    laCompany: TmmLabel;
    laTitle: TmmLabel;
    mActionList: TmmActionList;
    mChannelMatrix: TQRQMatrix;
    mFMatrix: TQRQMatrix;
    mmGroupBox1: TmmGroupBox;
    mmLineLabel1: TmmLineLabel;
    mmLineLabel2: TmmLineLabel;
    mmLineLabel3: TmmLineLabel;
    mmLineLabel4: TmmLineLabel;
    mmLineLabel5: TmmLineLabel;
    mmPanel1: TmmPanel;
    mmQRImage2: TmmQRImage;
    mmQRLabel10: TmmQRLabel;
    mmQRLabel12: TmmQRLabel;
    mmQRLabel3: TmmQRLabel;
    mmQRLabel6: TmmQRLabel;
    mmQRShape1: TmmQRShape;
    mmTranslator1: TmmTranslator;
    mPrintDBGrid: TQRPrintStringGrid;
    mPrintSetup: TFramePrintSetup;
    mQRChart: TmmQRChart;
    mQuickReport: TmmQuickRep;
    mSpliceMatrix: TQRQMatrix;
    PageHeaderBand1: TQRBand;
    qbDetailChart: TQRChildBand;
    qbMatrixData: TQRChildBand;
    qbPageFooter: TQRBand;
    qbTableDetail: TQRBand;
    qbTableHeader: TQRChildBand;
    qbAdditionalClearerSettings: TQRChildBand;
    qlCompany: TmmQRLabel;
    qlLot: TmmQRLabel;
    qlMachine: TmmQRLabel;
    qlStyle: TmmQRLabel;
    qlTimeCount: TmmQRLabel;
    qlTimeCountTitle: TmmQRLabel;
    qlTimeFrom: TmmQRLabel;
    qlTimeTo: TmmQRLabel;
    qlTitle: TmmQRLabel;
    qlYMSettings: TmmQRLabel;
    QRDBChart1: TQRDBChart;
    qsysDateTime: TmmQRSysData;
    qsysPageNumber: TmmQRSysData;
    sbPreview: TmmScrollBox;
    TitleBand1: TQRBand;
    bHelp: TmmButton;
    cbPrintChart: TmmCheckBox;
    mCSYarnCount: TQRXMLSettings;
    mCSCluster: TQRXMLSettings;
    mCSSFI: TQRXMLSettings;
    mCSChannel: TQRXMLSettings;
    mCSSplice: TQRXMLSettings;
    mCSFCluster: TQRXMLSettings;
    cbCSFCluster: TmmCheckBox;
    cbCSSplice: TmmCheckBox;
    cbCSChannel: TmmCheckBox;
    cbCSCluster: TmmCheckBox;
    cbCSSFI: TmmCheckBox;
    QRXMLSettingsModel: TVCLXMLSettingsModel;
    qbWarningMsg: TQRChildBand;
    qlWarningMsg: TmmQRLabel;
    qlClearerSettingsTitle: TmmQRLabel;
    qlSelectedClearerSettings: TmmQRLabel;
    mCSPSettings: TQRXMLSettings;
    mCSRepetition: TQRXMLSettings;
    cbCSRepetition: TmmCheckBox;
    cbCSVCV: TmmCheckBox;
    mCSVCV: TQRXMLSettings;
    procedure acPreviewExecute(Sender: TObject);
    procedure acZoomInOutExecute(Sender: TObject);
    procedure bOKCancelClick(Sender: TObject);
    procedure CalcSizeOfQR;
    procedure cbNewPageClick(Sender: TObject);
    procedure cbPrintChartClick(Sender: TObject);
    procedure cbPrintTableClick(Sender: TObject);
    procedure cbClearerSettingsClick(Sender: TObject);
    procedure cbStretchTableClick(Sender: TObject);
    procedure edCompanyChange(Sender: TObject);
    procedure edReportTitleChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure laTitleClick(Sender: TObject);
    procedure mActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure mPrintSetupPrintSetupChanged(Sender: TObject);
    procedure mQuickReportAfterBandResize(Sender: TObject);
    procedure mQuickReportBandResize(Sender: TObject; var CanResize: Boolean);
    procedure mQuickReportBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure mQuickReportSelectPrintable(Sender: TObject; var CanSelect, CanResize: Boolean);
    procedure OnFilterdataClick(Sender: TObject);
    procedure qsysPageNumberPrint(sender: TObject; var Value: String);
    procedure TitleBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure bHelpClick(Sender: TObject);
  private
    fCDR: TClassDataReader;
    fFilterTextArr: Array[0..3] of String;
    fParameters: TLabDataItemList;
    mQROrgWidth: Integer;
    mQROrgZoom: Integer;
    mZoomFactor: Single;
    function GetFilterText(Index: Integer): string;
    procedure OnGetBandColor(Sender: TQRCustomBand; aIndex: Integer; var BandColor: TColor);
    procedure PrintOrPreview(aPrint: Boolean);
    procedure SetCDR(const Value: TClassDataReader);
    procedure SetFilterText(Index: Integer; const Value: string);
    procedure SetParameters(const Value: TLabDataItemList);
    procedure CheckMatrixLogic;
    procedure SetWarningText(aValue: TClearerSettingsWarning);
    procedure AdaptSettingToMatrix(aMaster: TQRPrintable; aSetting: TQRXMLSettings; aRightSided: Boolean);
    procedure OnFMatrixResize(Sender: TObject);
  protected
    procedure RestoreFormLayout(aRegistryKey: String = ''); override;
    procedure SaveFormLayout(aRegistryKey: String = ''); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure PutSettingsInfo(aSettings: TXMLSettingsInfoRec);
    property CDR: TClassDataReader read fCDR write SetCDR;
    property FilterLot: string index 1   read GetFilterText write SetFilterText;
    property FilterMachine: string index 2   read GetFilterText write SetFilterText;
    property FilterStyle: string index 0   read GetFilterText write SetFilterText;
    property FilterYMSettings: string index 3   read GetFilterText write SetFilterText;
    property Parameters: TLabDataItemList read fParameters write SetParameters;
  end;
  
resourcestring
  rsStrPageCount = '(*)Seite %s'; // ivlm
  rsTimeFrom     = '(*)Zeit von:'; // ivlm
  rsTimeTo       = '(*)Zeit bis:'; // ivlm

implementation
{$R *.DFM}
uses
  mmCS, LoepfeGlobal,
  QRPrntr,
  Printers, DBStringGrid, MMHtmlHelp, SettingsReader;
  
{:------------------------------------------------------------------------------
 TfrmPrintDialog}
{:-----------------------------------------------------------------------------}
//:---------------------------------------------------------------------------
//:--- Class: TfrmPrintDialog
//:---------------------------------------------------------------------------
constructor TfrmPrintDialog.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  HelpContext := GetHelpContext('LabMaster\LabBericht\LAB_Einstellungen.htm');

//  Height := 560;
//  Width  := 685;

  Height := 630;
  Width  := 780;

  fCDR := Nil;

  mPrintSetup.mPrintDialog.Parent := Self;

  sbPreview.VertScrollBar.Position := 0;
  mQROrgZoom  := 40;
  with mQuickReport do begin
    Left := 0;
    Top  := 0;
    Zoom := mQROrgZoom;
    mQROrgWidth := Width;
  end;
  mPrintDBGrid.OnGetBandColor := OnGetBandColor;
  mFMatrix.OnResize := OnFMatrixResize;
end;

//:---------------------------------------------------------------------------
destructor TfrmPrintDialog.Destroy;
begin
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.acPreviewExecute(Sender: TObject);
begin
  bPreview.Enabled := False;
  PrintOrPreview(False);
  bPreview.Enabled := True;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.acZoomInOutExecute(Sender: TObject);
begin
  mQuickReport.Zoom   := mQuickReport.Zoom + (Sender as TAction).Tag;
//  sbPreview.VertScrollBar.Range := mQuickReport.Height;
//  sbPreview.HorzScrollBar.Range := mQuickReport.Width;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.bOKCancelClick(Sender: TObject);
var
  xFactor: Single;
  function CalcBoundRectToOrgZoom(aRect: TRect): TRect;
  begin
    with aRect do
      Result := Rect(Round(Left * xFactor), Round(Top * xFactor),
                     Round(Right * xFactor), Round(Bottom * xFactor));
  end;
begin
  if ModalResult = mrOK then begin
    xFactor := mQROrgZoom / mQuickReport.Zoom;
    with fParameters.ReportOptions^ do begin
      CompanyName := edCompany.Text;
      ReportTitle := edReportTitle.Text;

      ShowStyle      := cbFilterStyle.Checked;
      ShowLot        := cbFilterLot.Checked;
      ShowMachine    := cbFilterMachine.Checked;
      ShowYMSettings := cbFilterYMSettings.Checked;

      RectChart        := CalcBoundRectToOrgZoom(mQRChart.BoundsRect);
      RectQMatrix      := CalcBoundRectToOrgZoom(mChannelMatrix.BoundsRect);
      RectSpliceMatrix := CalcBoundRectToOrgZoom(mSpliceMatrix.BoundsRect);
      RectFFMatrix     := CalcBoundRectToOrgZoom(mFMatrix.BoundsRect);
      RectCSChannel    := CalcBoundRectToOrgZoom(mCSChannel.BoundsRect);      //Nue:19.11.07
      RectCSRepetition := CalcBoundRectToOrgZoom(mCSRepetition.BoundsRect);   //Nue:19.11.07
      RectCSSplice     := CalcBoundRectToOrgZoom(mCSSplice.BoundsRect);       //Nue:19.11.07
      RectCSFCluster   := CalcBoundRectToOrgZoom(mCSFCluster.BoundsRect);     //Nue:19.11.07
      RectCSPSettings  := CalcBoundRectToOrgZoom(mCSPSettings.BoundsRect);    //Nue:19.11.07
      RectCSCluster    := CalcBoundRectToOrgZoom(mCSCluster.BoundsRect);      //Nue:19.11.07
      RectCSSFI        := CalcBoundRectToOrgZoom(mCSSFI.BoundsRect);          //Nue:19.11.07
      RectCSVCV        := CalcBoundRectToOrgZoom(mCSVCV.BoundsRect);          //Nue:14.01.08
      RectCSYarnCount  := CalcBoundRectToOrgZoom(mCSYarnCount.BoundsRect);    //Nue:19.11.07

      PrintChart        := cbPrintChart.Checked;
      PrintQMatrix      := cbPrintClassData.Checked;
      if cbCSClusterCurve.Enabled then
        PrintCluster    := cbCSClusterCurve.Checked;
      PrintSpliceMatrix := cbPrintSpliceData.Checked;
      PrintFFMatrix     := cbPrintFFData.Checked;
      PrintBlackWhite   := mPrintSetup.PrintBlackWhite;
      PrintCSChannel    := cbCSChannel.Checked;
      PrintCSSplice     := cbCSSplice.Checked;
      PrintCSCluster    := cbCSCluster.Checked;
      PrintCSFCluster   := cbCSFCluster.Checked;
      PrintCSSFI        := cbCSSFI.Checked;
      PrintCSVCV        := cbCSVCV.Checked;
      PrintCSRepetition := cbCSRepetition.Checked;
      PrintCSYarnCount  := cbCSYarnCount.Checked;
      PrintTable        := cbPrintTable.Checked;
      StretchTable      := cbStretchTable.Checked;
  
      NewPageMatrix     := cbNewPageMatrix.Checked;
      NewPageYMSettings := cbNewPageAdditionalSettings.Checked;
      NewPageTable      := cbNewPageTable.Checked;
    end; // with
    bOK.Enabled := False;
    try
      PrintOrPreview(True);
    except
      on e:Exception do
        CodeSite.SendError('PrintOrPreview failed: ' + e.Message);
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.CalcSizeOfQR;
begin
  with mQuickReport do begin
    // Define new Height of QuickReport
    Height := qbPageFooter.Top + qbPageFooter.Height + 30;
    // Assign height to V-Scrollbar of parent scrollbox
    sbPreview.VertScrollBar.Range := Round(sbPreview.Height * (Height / sbPreview.Height));
  end;
end;

//:---------------------------------------------------------------------------


//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.cbNewPageClick(Sender: TObject);
begin
  with Sender as TmmCheckBox do begin
    case Tag of
      0: qbMatrixData.ForceNewPage                := Checked;
      1: qbAdditionalClearerSettings.ForceNewPage := Checked;
      2: qbTableHeader.ForceNewPage               := Checked;
    else
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.cbPrintChartClick(Sender: TObject);
begin
  qbDetailChart.Enabled := cbPrintChart.Checked;
  if qbDetailChart.Enabled then begin
    with mQRChart do
      qbDetailChart.Height := Top + Height + 4;
  end else
    qbDetailChart.Height := 0;

  CalcSizeOfQR;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.CheckMatrixLogic;
var
  xHeight: Integer;
  //................................................
  procedure CheckMaxHeight(aQRComp: TQRPrintable; var aHeight: Integer);
  var
    xBottom: Integer;
  begin
    with aQRComp do begin
      xBottom := Top + Height;
    end;
    if xBottom > aHeight then
      aHeight := xBottom;
  end;
  //................................................
begin
  // Klassierdaten anzeigen?
  mChannelMatrix.Visible  := cbPrintClassData.Checked or cbCSChannel.Checked;
  mSpliceMatrix.Visible   := cbPrintSpliceData.Checked or cbCSSplice.Checked;
  mFMatrix.Visible        := cbPrintFFData.Checked or cbCSFCluster.Checked;

  // Reinigereinstellungen anzeigen?
  mCSChannel.Visible      := cbCSChannel.Checked;
  mCSSplice.Visible       := cbCSSplice.Checked;
  mCSFCluster.Visible     := cbCSFCluster.Checked;
  mCSPSettings.Visible    := cbCSFCluster.Checked;
  mCSRepetition.Visible   := cbCSRepetition.Checked;

  mCSCluster.Visible      := cbCSCluster.Checked;
  mCSSFI.Visible          := cbCSSFI.Checked;
  mCSVCV.Visible          := cbCSVCV.Checked;
  mCSYarnCount.Visible    := cbCSYarnCount.Checked;

  mChannelMatrix.ClusterVisible := mCSChannel.Visible and cbCSClusterCurve.Checked;
  mChannelMatrix.Invalidate;

  // nun noch die H�he vom Band mit den Matrixen anpassen
  xHeight := 0;
  qbMatrixData.Enabled := mChannelMatrix.Visible or mSpliceMatrix.Visible or mFMatrix.Visible;
  if qbMatrixData.Enabled then begin
    if mChannelMatrix.Visible then
      CheckMaxHeight(mChannelMatrix, xHeight);

    if mSpliceMatrix.Visible then
      CheckMaxHeight(mSpliceMatrix, xHeight);

    if mFMatrix.Visible then
      CheckMaxHeight(mFMatrix, xHeight);
  end;
  qbMatrixData.Height := xHeight + 4; // 4 Pixel Rand gleich hinzuf�gen

  // Auch die H�he vom Band mit den weiteren Settingskomponenten visuell anpassen
  qbAdditionalClearerSettings.Enabled := mCSCluster.Visible or mCSSFI.Visible  or mCSVCV.Visible or mCSYarnCount.Visible;
  if qbAdditionalClearerSettings.Enabled then begin
    qbAdditionalClearerSettings.Height := mCSYarnCount.Top + mCSYarnCount.Height + 4;
  end else
    qbAdditionalClearerSettings.Height := 0;

  if qbMatrixData.Enabled or qbAdditionalClearerSettings.Enabled then qbWarningMsg.Height := 14
                                                                 else qbWarningMsg.Height := 0;

  CalcSizeOfQR;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.cbPrintTableClick(Sender: TObject);
begin
  CalcSizeOfQR;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.cbClearerSettingsClick(Sender: TObject);
begin
  CheckMatrixLogic;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.cbStretchTableClick(Sender: TObject);
begin
  mPrintDBGrid.FitToWidth := cbStretchTable.Checked;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.edCompanyChange(Sender: TObject);
begin
  qlCompany.Caption := edCompany.Text;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.edReportTitleChange(Sender: TObject);
begin
  qlTitle.Caption := edReportTitle.Text;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.FormResize(Sender: TObject);
begin
  if mQROrgWidth > 0 then begin
    mQuickReport.Zoom := Round(mQROrgZoom / mQROrgWidth * sbPreview.ClientWidth);
    CalcSizeOfQR;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.FormShow(Sender: TObject);
begin
  bPreview.Visible   := not OLEMode;
  //edCompany.Text     := fParameters.ReportOptions^.CompanyName;
  try
    begin
      edCompany.Text := TMMSettingsReader.Instance.Value[cCompanyName];
    end;
  except
    qlCompany.Caption :='';
  end;

  edReportTitle.Text := fParameters.ReportOptions^.ReportTitle;

  cbClearerSettingsClick(Nil);
  // Ein vom letzten mal selektierter Printer bereits �bernehmen
  mQuickReport.UsePrinterIndex(mPrintSetup.PrinterIndex);
end;

//:---------------------------------------------------------------------------
function TfrmPrintDialog.GetFilterText(Index: Integer): string;
begin
  Result := fFilterTextArr[Index];
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.laTitleClick(Sender: TObject);
begin
  with mQuickReport do
    Height := StrToIntDef(edReportTitle.Text, Height);
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.mActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  Handled := True;

  bOk.Enabled                 := mPrintSetup.PrinterOK;
  bPreview.Enabled            := mPrintSetup.PrinterOK;
  cbNewPageMatrix.Enabled     := qbMatrixData.Enabled;
  cbCSClusterCurve.Enabled    := cbCSChannel.Enabled and cbCSChannel.Checked;
  cbNewPageAdditionalSettings.Enabled := qbAdditionalClearerSettings.Enabled;
  cbStretchTable.Enabled      := cbPrintTable.Checked;
  cbNewPageTable.Enabled      := qbTableHeader.Enabled;

  cbCSClusterCurve.Enabled    := cbCSChannel.Checked;

  mCSChannel.Visible          := mChannelMatrix.Visible and cbCSChannel.Checked;
  mCSSplice.Visible           := mSpliceMatrix.Visible and cbCSSplice.Checked;
  mCSFCluster.Visible         := mFMatrix.Visible and cbCSFCluster.Checked;
  mCSPSettings.Visible        := mCSFCluster.Visible;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.mPrintSetupPrintSetupChanged(Sender: TObject);
begin
  mQuickReport.UsePrinterIndex(mPrintSetup.PrinterIndex);
  mChannelMatrix.BlackWhite := mPrintSetup.PrintBlackWhite;
  mSpliceMatrix.BlackWhite  := mPrintSetup.PrintBlackWhite;
  mFMatrix.BlackWhite       := mPrintSetup.PrintBlackWhite;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.mQuickReportAfterBandResize(Sender: TObject);
begin
  CalcSizeOfQR;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.mQuickReportBandResize(Sender: TObject; var CanResize: Boolean);
begin
  CanResize := (Sender = qbDetailChart) or (Sender = qbMatrixData);
//Nue:19.11.07  if Sender = qbMatrixData then begin
//Nue:19.11.07    if mQuickReport.QRControl = mChannelMatrix then AdaptSettingToMatrix(mChannelMatrix, mCSChannel, True) else
//Nue:19.11.07    if mQuickReport.QRControl = mSpliceMatrix  then AdaptSettingToMatrix(mSpliceMatrix, mCSSplice, True) else
//Nue:19.11.07  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.AdaptSettingToMatrix(aMaster: TQRPrintable; aSetting: TQRXMLSettings; aRightSided: Boolean);
begin
  if aRightSided then begin
    aSetting.Left := aMaster.Left + aMaster.Width + 4;
    aSetting.Top  := aMaster.Top;
  end
  else begin
    aSetting.Left := aMaster.Left;
    aSetting.Top  := aMaster.Top + aMaster.Height + 4;
  end;
end;
//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.mQuickReportBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  // Klassierdaten drucken?
  mChannelMatrix.Enabled  := mChannelMatrix.Visible;
  mSpliceMatrix.Enabled   := mSpliceMatrix.Visible;
  mFMatrix.Enabled        := mFMatrix.Visible;

  // Reinigereinstellungen drucken?
  mCSChannel.Enabled    := mCSChannel.Visible;
  mCSSplice.Enabled     := mCSSplice.Visible;
  mCSFCluster.Enabled   := mCSFCluster.Visible;
  mCSPSettings.Enabled  := mCSPSettings.Visible;
  mCSRepetition.Enabled := mCSRepetition.Visible;

  mCSCluster.Enabled   := mCSCluster.Visible;
  mCSSFI.Enabled       := mCSSFI.Visible;
  mCSVCV.Enabled       := mCSVCV.Visible;
  mCSYarnCount.Enabled := mCSYarnCount.Visible;

  with fParameters.MatrixOptions^ do begin
    if cbPrintClassData.Checked then
      fCDR.FillValuesToMatrix(DefectValues, CutValues, mChannelMatrix.QMatrix);

//    if cbPrintSpliceData.Checked then
//      fCDR.FillValuesToMatrix(DefectValues, CutValues, mSpliceMatrix.QMatrix);
    if cbPrintSpliceData.Checked then                                              //Nue:20.01.08
      fCDR.FillValuesToMatrix(fParameters.SpliceMatrixOptions^.DefectValues, fParameters.SpliceMatrixOptions^.CutValues, mSpliceMatrix.QMatrix);     //Nue:20.01.08

    if cbPrintFFData.Checked then
      fCDR.FillValuesToMatrix(DefectValues, CutValues, mFMatrix.QMatrix);
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.mQuickReportSelectPrintable(Sender: TObject; var CanSelect, CanResize: Boolean);
begin
  CanSelect := (Sender is TmmQRChart) or (Sender is TQRQMatrix) {Nue:7.6.07}or (Sender is TQRXMLSettings);
  CanResize := CanSelect;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.OnFilterdataClick(Sender: TObject);
var
  xQLabel: TmmQRLabel;
begin
  with Sender as TCheckBox do begin
    case Tag of
      0: xQLabel := qlStyle;
      1: xQLabel := qlLot;
      2: xQLabel := qlMachine;
      3: xQLabel := qlYMSettings;
    else
      Exit;
    end;
    if Checked then xQLabel.Caption := fFilterTextArr[Tag]
               else xQLabel.Caption := rsHidenFilterText;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.OnGetBandColor(Sender: TQRCustomBand; aIndex: Integer; var BandColor: TColor);
begin
  if mPrintDBGrid.DataGrid is TDBStringGrid then begin
    with TDBStringGrid(mPrintDBGrid.DataGrid) do begin
      if EnableDetails and (aIndex >= FixedRows) and (aIndex < RowCount) then begin
        BandColor := ExpandInfo[aIndex].Color;
      end;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.PrintOrPreview(aPrint: Boolean);
begin
  Screen.Cursor := crHourGlass;
  with mQuickReport do
  try
    ReportTitle := edReportTitle.Text;

    qbTableHeader.Enabled := cbPrintTable.Checked;
    qbTableDetail.Enabled := cbPrintTable.Checked;

    // call prepare to calculate number of pages
    UnselectQRControl;
  {
    try
      Prepare;
      Preview;
    finally
      // deallocate memory used for prepare
      QRPrinter.Free;
      QRPrinter := Nil;
    end;
  {}

{}
    try
      // wenn mit Tabelle gedruckt werden soll, so muss der Druckbefehl �ber
      // die PrintDBGrid Komponenten erfolgen
      if aPrint then begin
        if cbPrintTable.Checked then mPrintDBGrid.Print
                                else Print;
      end else begin
        Screen.Cursor := crDefault;
        if cbPrintTable.Checked then mPrintDBGrid.Preview
                                else Preview;
      end;
    except
      on e:exception do
        CodeSite.SendError('PrintOrPreview error: ' + e.Message);
    end;
{}
  finally
    Screen.Cursor := crDefault;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.PutSettingsInfo(aSettings: TXMLSettingsInfoRec);
begin
  with aSettings do begin
    QRXMLSettingsModel.xmlAsString    := XMLData;
    qlSelectedClearerSettings.Caption := SetName;
    mCSYarnCount.YarnCount            := YarnCount;
    mCSYarnCount.YarnUnit             := YarnUnit;

    if not SingleYarnCount and not qlWarningMsg.Enabled then
      SetWarningText(cswManyYarnCount);

{
    mChannelMatrix.PutYMSettings(YMSettingsExt.YMSettings);
    mSpliceMatrix.PutYMSettings(YMSettingsExt.YMSettings);
    mFMatrix.PutYMSettings(YMSettingsExt.YMSettings);
    mYMSettings.PutYMSettings(YMSettingsExt.YMSettings);
    mYMSettings.YarnCount := ConvertYarnCountFloatToInt(YarnCount);
    mYMSettings.YarnUnit  := YarnUnit;
    if not SingleYarnCount and not qlWarningMsg.Enabled then begin
      qlWarningMsg.Enabled := True;
      qlWarningMsg.Caption := GetWarningMsg(cswManyYarnCount);
    end;
{}
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.qsysPageNumberPrint(sender: TObject; var Value: String);
begin
  Value := Format(rsStrPageCount, [Value]);
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.SetCDR(const Value: TClassDataReader);
begin
  fCDR := Value;
  if Assigned(fCDR) then begin
    // if one and only one clearer settings is available -> assign settings to components
    if fCDR.XMLSettings.Count = 1 then begin
//      qbWarningMsg.Visible := False;
      SetWarningText(cswNone);
      PutSettingsInfo(fCDR.XMLSettings.Items[0]^);
    end
    else if fCDR.XMLSettings.Count > 1 then begin
      // set warning text if more than one clearer settings is available
//      qbWarningMsg.Visible := True;
//      qlWarningMsg.Enabled := True;
//      qlWarningMsg.Caption := GetWarningMsg(cswManySettings);
      SetWarningText(cswManySettings);
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.SetWarningText(aValue: TClearerSettingsWarning);
var
  xWarning: Boolean;
begin
  xWarning := (aValue <> cswNone);
  // Labels entsprechend Anzeigen oder verstecken
  qlClearerSettingsTitle.Visible    := not xWarning;
  qlClearerSettingsTitle.Enabled    := not xWarning;
  qlSelectedClearerSettings.Visible := not xWarning;
  qlSelectedClearerSettings.Enabled := not xWarning;
  qlWarningMsg.Enabled              := xWarning;
  qlWarningMsg.Visible              := xWarning;

  if xWarning then
    qlWarningMsg.Caption := GetWarningMsg(aValue);

  // ClearerSettings Checkboxen noch kontrollieren
  cbCSChannel.Enabled    := not xWarning;
  cbCSSplice.Enabled     := not xWarning;
  cbCSCluster.Enabled    := not xWarning;
  cbCSFCluster.Enabled   := not xWarning;
  cbCSSFI.Enabled        := not xWarning;
  cbCSVCV.Enabled        := not xWarning;
  cbCSRepetition.Enabled := not xWarning;
  cbCSYarnCount.Enabled  := not xWarning;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.SetFilterText(Index: Integer; const Value: string);
begin
  fFilterTextArr[Index] := Value;
end;

//:---------------------------------------------------------------------------
procedure TfrmPrintDialog.SetParameters(const Value: TLabDataItemList);
begin
  fParameters := Value;
  with fParameters do begin
    with ReportOptions^ do begin
      mPrintSetup.PrintBlackWhite    := PrintBlackWhite;

      cbFilterStyle.Checked      := ShowStyle;
      cbFilterLot.Checked        := ShowLot;
      cbFilterMachine.Checked    := ShowMachine;
      cbFilterYMSettings.Checked := ShowYMSettings;

      cbPrintChart.Checked := PrintChart;
      if RectChart.Bottom > 0 then
        mQRChart.BoundsRect := RectChart;

      cbPrintClassData.Checked := PrintQMatrix;
      mChannelMatrix.QMatrix.DisplayMode := cDisplayModeIndex[Parameters.MatrixOptions^.MatrixMode];
      if RectQMatrix.Bottom > 0 then
        mChannelMatrix.BoundsRect := RectQMatrix;

      cbPrintSpliceData.Checked := PrintSpliceMatrix;
//      mSpliceMatrix.QMatrix.DisplayMode := cDisplayModeIndex[Parameters.MatrixOptions^.MatrixMode];
      mSpliceMatrix.QMatrix.DisplayMode := cDisplayModeIndex[Parameters.SpliceMatrixOptions^.MatrixMode];     //Nue:20.01.08
      if RectSpliceMatrix.Bottom > 0 then
        mSpliceMatrix.BoundsRect := RectSpliceMatrix;

      cbPrintFFData.Checked := PrintFFMatrix;
      mFMatrix.QMatrix.DisplayMode := cDisplayModeIndex[Parameters.MatrixOptions^.MatrixMode];
      if RectFFMatrix.Bottom > 0 then
        mFMatrix.BoundsRect := RectFFMatrix;

      if cbCSChannel.Enabled then begin
        cbCSChannel.Checked      := PrintCSChannel;
        cbCSClusterCurve.Checked := PrintCluster;
        cbCSSplice.Checked       := PrintCSSplice;
        cbCSCluster.Checked      := PrintCSCluster;
        cbCSFCluster.Checked     := PrintCSFCluster;
        cbCSSFI.Checked          := PrintCSSFI;
        cbCSVCV.Checked          := PrintCSVCV;
        cbCSRepetition.Checked   := PrintCSRepetition;
        cbCSYarnCount.Checked    := PrintCSYarnCount;
        with MatrixOptions^ do begin
          mChannelMatrix.ActiveColor    := ActiveColor;
          mChannelMatrix.ActiveVisible  := ActiveVisible;
          mChannelMatrix.ChannelVisible := ChannelVisible;

//          mSpliceMatrix.SpliceVisible   := SpliceVisible;
          mSpliceMatrix.ActiveColor    := SpliceMatrixOptions^.ActiveColor;      //Nue:20.01.08
          mSpliceMatrix.ActiveVisible  := SpliceMatrixOptions^.ActiveVisible;    //Nue:20.01.08
          mSpliceMatrix.SpliceVisible  := SpliceMatrixOptions^.SpliceVisible;    //Nue:20.01.08

          mFMatrix.ActiveColor          := ActiveColor;
          mFMatrix.ActiveVisible        := ActiveVisible;
        end;

        if RectCSChannel.Bottom > 0 then              //Nue:19.11.07
          mCSChannel.BoundsRect := RectCSChannel;
        if RectCSRepetition.Bottom > 0 then              //Nue:19.11.07
          mCSRepetition.BoundsRect := RectCSRepetition;
        if RectCSSplice.Bottom > 0 then              //Nue:19.11.07
          mCSSplice.BoundsRect := RectCSSplice;
        if RectCSFCluster.Bottom > 0 then              //Nue:19.11.07
          mCSFCluster.BoundsRect := RectCSFCluster;
        if RectCSPSettings.Bottom > 0 then              //Nue:19.11.07
          mCSPSettings.BoundsRect := RectCSPSettings;
        if RectCSCluster.Bottom > 0 then              //Nue:19.11.07
          mCSCluster.BoundsRect := RectCSCluster;
        if RectCSSFI.Bottom > 0 then              //Nue:19.11.07
          mCSSFI.BoundsRect := RectCSSFI;
        if RectCSVCV.Bottom > 0 then              //Nue:14.01.08
          mCSVCV.BoundsRect := RectCSVCV;
        if RectCSYarnCount.Bottom > 0 then              //Nue:19.11.07
          mCSYarnCount.BoundsRect := RectCSYarnCount;

      end;

      cbPrintTable.Checked      := PrintTable;
      cbStretchTable.Checked    := StretchTable;
  
      cbNewPageMatrix.Checked     := NewPageMatrix;
      cbNewPageAdditionalSettings.Checked := NewPageYMSettings;
      cbNewPageTable.Checked      := NewPageTable;
    end;
  
  end;
  
  if qbDetailChart.Enabled then
    mQuickReport.CheckMinBandHeight(TQRBand(qbDetailChart));
  
  if qbMatrixData.Enabled then
    mQuickReport.CheckMinBandHeight(TQRBand(qbMatrixData));
  
  cbPrintChartClick(Nil);
  cbPrintTableClick(Nil);
  CheckMatrixLogic;
end;

procedure TfrmPrintDialog.TitleBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  mmQRShape1.Width := Sender.Width;
end;

procedure TfrmPrintDialog.bHelpClick(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;

procedure TfrmPrintDialog.OnFMatrixResize(Sender: TObject);
begin
end;

procedure TfrmPrintDialog.RestoreFormLayout(aRegistryKey: String);
begin
  // verhindert das laden der Window Pos/Size
end;

procedure TfrmPrintDialog.SaveFormLayout(aRegistryKey: String);
begin
  // verhindert das speichern der Window Pos/Size
end;

end.



