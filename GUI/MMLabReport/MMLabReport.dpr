program MMLabReport;

{%ToDo 'MMLabReport.todo'}

uses
  MemCheck,
  mmCS,
  Forms,
  LoepfeGlobal,
  SettingsReader,
  MainForm in 'MainForm.pas' {frmMainForm},
  DataForm in 'DataForm.pas' {frmLabReport},
  LabMasterDef in '..\..\Common\LabMasterDef.pas',
  SettingsDialog in 'SettingsDialog.pas' {frmSettingsDialog},
  PrintSettingsDialog in 'PrintSettingsDialog.pas' {frmPrintDialog},
  LabReport_TLB in 'LabReport_TLB.pas',
  LabMasterServerIMPL in 'LabMasterServerIMPL.pas' {LabReportServer: CoClass},
  ClassDataReader in '..\..\Components\VCL\ClassDataReader.pas',
  BASEAPPLMAIN in '..\..\..\LoepfeShares\Templates\BASEAPPLMAIN.pas' {BaseApplMainForm},
  BASEFORM in '..\..\..\LoepfeShares\Templates\BASEFORM.pas' {mmForm},
  BASEDialog in '..\..\..\LoepfeShares\Templates\BASEDIALOG.pas' {mmDialog},
  LabReportClass in 'LabReportClass.pas' {dmLabReport: TDataModule},
  YMRepSettingsFrame in '..\..\Components\VCL\YMRepSettingsFrame.pas' {fraRepSettings: TFrame},
  BASEDialogBottom in '..\..\..\LOEPFESHARES\TEMPLATES\BASEDIALOGBOTTOM.pas' {DialogBottom},
  SelectDefectDialog in '..\..\Common\SelectDefectDialog.pas' {frmSelectDefects},
  DetailInfoForm in 'DetailInfoForm.pas' {frmDetailInfo};

{$R *.TLB}

{$R *.RES}
{$R Version.res}

begin
  gApplicationName := 'MMLabReport';
  CodeSite.Enabled := CodeSiteEnabled;
{$IFDEF MemCheck}
  MemChk(gApplicationName);
{$ENDIF}

  gMinDBVersion    := '5.03.00';
  with TMMSettingsReader.Instance do begin
    if not DBVersionOK(gMinDBVersion, True) then
      Exit;
  end;

  Application.Initialize;
  Application.Title := 'MMLabReport';
  Application.CreateForm(TfrmMainForm, frmMainForm);
  Application.Run;
end.
