(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: SettingsDialog.pas
| Projectpart...: Parameter-Dialog fuer die Spulerei und Einzelspindelbericht
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
| 10.11.2000 1.00 Wss | File created
| 27.08.2001 1.00 Wss | Coarse/Fine QMatrixMode implemented
| 05.04.2002 1.00 Wss | A/V wenn doppelklick in ListView bei Tabellensicht behoben
| 10.07.2002 1.01 Nue | Problems with the "with "statement -> Replaced with fParameter.Assign(Value);
| 04.10.2002 1.03 Wss | Option for Hierarchy table and DescendingSort member for Key sorting added
| 03.10.2003 1.03 Wss | Nach ClassDesinger werden Basisinformationen der DataItems neu eingelesen
| Jan 2004   1.02 Wss | Div. Erweiterungen/Fixes f�r Version 4.02
| 21.01.2008 1.03 Nue | Einbau von Spleissklassierung.
| 21.01.2008 1.04 Nue | In OnMatrixOptionsChange: Aenderungen entsprechend auch f�r Spleissmatrix �bernommen.
|=============================================================================*)
unit SettingsDialog;                   


interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, StdCtrls, CheckLst, mmCheckListBox, ToolWin, ComCtrls,
  mmPageControl, mmButton, ExtCtrls, mmPanel, Buttons, mmSpeedButton,
  mmCheckBox, mmLabel, mmRadioGroup, mmGroupBox,
  mmComboBox, mmUpDown, mmEdit, IvMlDlgs, mmFontDialog, mmColorDialog,
  IvDictio, IvMulti, IvEMulti, mmTranslator, ActnList, mmActionList, Grids,
  mmStringList, mmStringGrid, mmList, DB, TeCanvas, Menus,
  Mask, mmMaskEdit, mmLineLabel, mmColorButton,
  ImgList, mmImageList, mmListView, mmImage, mmRadioButton,
  TeEngine, Series, MMSeries, TeeProcs, Chart, mmChart, mmTimer,
  mmOpenDialog, Spin, mmSpinEdit, mmListBox, DBGrids, mmPopupMenu,
  DataItemTree, Tabs, VirtualTrees,
  LabMasterDef, LabReportClass, mmBitBtn, mmIvMlDlgs, CalcMethods,
  CalcMethodBaseFrame;

//------------------------------------------------------------------------------
type
  {-----------------------------------------------------------------------------
   Konfigurationsdialog fuer die Grafik und Tabelle. Es koennen unterschiedliche }
  TfrmSettingsDialog = class (TmmForm)
    bCancel: TmmButton;
    bChartArea: TmmSpeedButton;
    bChartBar: TmmSpeedButton;
    bChartEndColor: TmmColorButton;
    bChartImageOpen: TmmSpeedButton;
    bChartLine: TmmSpeedButton;
    bChartStartColor: TmmColorButton;
    bClassActiveColor: TmmColorButton;
    bClassInactiveColor: TmmColorButton;
    bColorSelect: TmmColorButton;
    bDataColor: TmmBitBtn;
    bDataFont: TmmBitBtn;
    bHelp: TmmButton;
    bHierarchyColor: TmmColorButton;
    bOK: TmmButton;
    bTitleColor: TmmBitBtn;
    bTitleFont: TmmBitBtn;
    cbAlwaysShowSelection: TmmCheckBox;
    cbChannelCurve: TmmCheckBox;
    cbChart3D: TmmCheckBox;
    cbChartImageInside: TmmCheckBox;
    cbChartRotate: TmmCheckBox;
    cbClassFields: TmmCheckBox;
    cbClusterCurve: TmmCheckBox;
    cbColLines: TmmCheckBox;
    cbCutValues: TmmCheckBox;
    cbDefectValues: TmmCheckBox;
    cbHierarchyTable: TmmCheckBox;
    cbIndicator: TmmCheckBox;
    cbKeySorting: TmmCheckBox;
    cbRowLines: TmmCheckBox;
    cbRowSelect: TmmCheckBox;
    cbSpliceCurve: TmmCheckBox;
    cbThousands: TmmCheckBox;
    cbTitleAlignment: TmmCheckBox;
    cbTrailingEllipsis: TmmCheckBox;
    cbXLabelRotated: TmmCheckBox;
    cobBarStyle: TmmComboBox;
    cobChartGradient: TmmComboBox;
    cobLegend: TmmComboBox;
    cobScaling: TmmComboBox;
    ColorDialog: TmmColorDialog;
    edChartImage: TmmEdit;
    edFixedCols: TmmEdit;
    FontDialog: TmmFontDialog;
    gbExtGridSettings: TmmGroupBox;
    gbKeyOptions: TmmGroupBox;
    gbNumber: TmmGroupBox;
    gbQMatrixSettings: TmmGroupBox;
    imgBasket: TmmImage;
    la3DWidth: TmmLabel;
    laAvailable: TmmLabel;
    laAxisMax: TmmLabel;
    laAxisMin: TmmLabel;
    laAxisTitel: TmmLabel;
    laChartImg: TmmLabel;
    laColor: TmmLabel;
    laComma: TmmLabel;
    laEndColor: TmmLabel;
    laFixedCols: TmmLabel;
    laHierarchy: TmmLabel;
    laScaling: TmmLabel;
    laStartColor: TmmLabel;
    laTablePreview: TmmLabel;
    lbAxis: TmmListBox;
    lvChartSeries: TmmListView;
    mChart: TmmChart;
    mImgListView: TmmImageList;
    mItemTree: TDataItemTree;
    mmGroupBox2: TmmGroupBox;
    mmGroupBox3: TmmGroupBox;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmLabel4: TmmLabel;
    mmLabel5: TmmLabel;
    mmLabel6: TmmLabel;
    mmLabel7: TmmLabel;
    mmLabel8: TmmLabel;
    mmLabel9: TmmLabel;
    mmPageControl1: TmmPageControl;
    mmPanel1: TmmPanel;
    mOpenDialog: TmmOpenDialog;
    mTranslator: TmmTranslator;
    pcSettings: TmmPageControl;
    pmListView: TmmPopupMenu;
    pmSelectAll: TMenuItem;
    pnBottom: TmmPanel;
    pnChartXAxis: TmmPanel;
    pnChartZAxis: TmmPanel;
    pnDataFormat: TmmPanel;
    pnTitleFormat: TmmPanel;
    rbQMCoarse: TmmRadioButton;
    rbQMFine: TmmRadioButton;
    rgTextAlign: TmmRadioGroup;
    se3DWidth: TmmSpinEdit;
    seComma: TmmSpinEdit;
    Series1: TBarSeries;
    Series2: TBarSeries;
    Series3: TLineSeries;
    sgTableItems: TmmStringGrid;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    timMinMax: TmmTimer;
    timScrollGrid: TmmTimer;
    tsDataItem: TTabSheet;
    tsGridSettings: TTabSheet;
    tsQualityMatrix: TTabSheet;
    udFixedCols: TmmUpDown;
    cobItemAxis: TmmComboBox;
    laAxisOrientation: TmmLabel;
    laCalcMethodDescription: TmmLabel;
    clbCalcMethods: TmmCheckListBox;
    pnCalcMethod: TmmPanel;
    mmLabel11: TmmLabel;
    cobChartClassValue: TmmComboBox;
    mmLabel13: TmmLabel;
    edBorderCondition: TmmEdit;
    cbSplitXAxis: TmmCheckBox;
    edAxisMax: TmmEdit;
    edAxisMin: TmmEdit;
    gbGridClassValue: TmmGroupBox;
    cobGridClassValue: TmmComboBox;
    bClassDesigner: TmmSpeedButton;
    procedure bChartImageOpenClick(Sender: TObject);
    procedure bClassDesignerClick(Sender: TObject);
    procedure bGridColorClick(Sender: TObject);
    procedure bGridFontClick(Sender: TObject);
    procedure bOKClick(Sender: TObject);
    procedure ChartAxisDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ChartItemsDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure imgBasketDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure imgBasketDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure lbAxisClick(Sender: TObject);
    procedure lvChartSeriesDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure lvChartSeriesDrawItem(Sender: TCustomListView; Item: TListItem; Rect: TRect; State: TOwnerDrawState);
    procedure lvChartSeriesSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
    procedure lvItemsDblClick(Sender: TObject);
    procedure mChartAfterDraw(Sender: TObject);
    procedure mItemTreeDblClick(Sender: TObject);
    procedure mItemTreeGetImage(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex; var Ghosted: Boolean; var ImageIndex:
            Integer);
    procedure OnChartOptionsChange(Sender: TObject);
    procedure OnGridOptionsClick(Sender: TObject);
    procedure OnItemsSettingsChange(Sender: TObject);
    procedure OnMatrixOptionsChange(Sender: TObject);
    procedure OnTableItemsChange(Sender: TObject);
    procedure pmSelectAllClick(Sender: TObject);
    procedure sgTableItemsDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure sgTableItemsDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgTableItemsEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure sgTableItemsMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure sgTableItemsStartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure timMinMaxTimer(Sender: TObject);
    procedure timScrollGridTimer(Sender: TObject);
    procedure clbCalcMethodsClick(Sender: TObject);
    procedure bHelpClick(Sender: TObject);
    procedure mmActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
  private
    fAxisLeftTop: TChartAxis;
    fAxisRightTop: TChartAxis;
    fChartSelectionChangedX: Boolean;
    fConfigChanged: TDataModeSet;
    fGridSelectionChangedX: Boolean;
    fParameter: TLabDataItemList;
    fSplitXAxis: TChartAxis;
    mColDragIndex: Integer;
    mColIndex: Integer;
    mEnableChanges: Boolean;
    mInDragMode: Boolean;
    mKeyItemList: TmmStringList;
    mSelectedChartItem: TLabDataItem;
    mScrollDir: TScrollDirection;
    procedure AddToChartSeries(aItem: TLabDataItem; aPos: Integer; aInit: Boolean);
    procedure AddToTableItems(aItem: TLabDataItem; aInit: Boolean);
    procedure CheckComponentStateOfChartProperties;
    procedure DeleteItemsFromChart;
    procedure DeleteItemsFromTable;
    procedure DrawMove(aMovePos: Integer);
    function GetAxisLeftTop: TChartAxis;
    function GetAxisRightTop: TChartAxis;
    function GetParameter: TLabDataItemList;
    function GetSplitXAxis: TChartAxis;
    procedure RenumberingChartIndex;
    function RenumberingTableIndex: Boolean;
    procedure SetParameter(Value: TLabDataItemList);
    procedure SetPropertiesToChart;
    procedure SetPropertiesToGrid;
    procedure ShowChartPropFromDataItem;
    procedure ShowTablePropFromDataItem(aIndex: Integer);
    procedure UpdateChart;
    procedure UpdateStateForChartDataItem;
    procedure MethodSettingsChange(aSender: TfrmCalcMethodBaseFrame);
    procedure HandleClassDefectsItems(aItem: TLabDataItem; aCob: TmmComboBox; aMode: TDataMode);
  protected
    mMethodFrame: TfrmCalcMethodBaseFrame;
  public
    constructor Create(aOwner: TComponent); reintroduce; virtual;
    destructor Destroy; override;
    procedure SetActivePage(aIndex: Integer);
    property ChartSelectionChangedX: Boolean read fChartSelectionChangedX;
    property ConfigChanged: TDataModeSet read fConfigChanged;
    property GridSelectionChangedX: Boolean read fGridSelectionChangedX;
    property Parameter: TLabDataItemList read GetParameter write SetParameter;
  end;

//------------------------------------------------------------------------------
var
  frmSettingsDialog: TfrmSettingsDialog;

resourcestring
  rsMsgMoveKeyItem    = '(*)Ziehen Sie ein Schluesselelement hierher';
  rsMsgDragToKeyAxis  = '(*)Ziehen Sie von hier dieses Element in die X- oder Z-Achse der Grafik'; // ivlm
  rsMsgDragToDataAxis = '(*)Ziehen Sie von hier dieses Element in die Datenliste der Grafik'; // ivlm
  rsMsgDragToTable    = '(*)Ziehen Sie von hier dieses Element in die Tabellenvorschau'; // ivlm

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS, mmCS, ComObj,
  Consts, MMUGlobal, ClassDef, mmVirtualStringTree, SelectDefectDialog,
  FixBorderFrame, AVGBorderFrame, MMHtmlHelp;

{ entfernte Statistik Methoden
(20)Standard-Abweichung
(20)Q90
(20)Q95
(20)Q99
{}
const
  cKeyImageIndex = 4;
  cClassDefectsStr: Array[TClassDefects] of String = ('(30)Schnitte', '(30)Ungeschnittene', '(*)Beide');

//------------------------------------------------------------------------------
// sort fuction for listviews
//------------------------------------------------------------------------------
function LVCustomSort(aItem1, aItem2: TListItem; aParamSort: Integer): Integer stdcall;
var
  xI1, xI2: Integer;
begin
  xI1 := 0;
  xI2 := 0;
  if Assigned(aItem1) and Assigned(aItem2) then
  try
    xI1 := TLabDataItem(aItem1.Data).ChartIndex;
    xI2 := TLabDataItem(aItem2.Data).ChartIndex;
  except
    on e:Exception do begin
      CodeSite.SendError('LVCustomSort: ' + e.Message);
    end;
  end;

  if xI1 = xI2 then
    Result := 0
  else if ((xI1 < xI2) and (xI1 <> -1)) or ((xI1 >= 0) and (xI2 = -1)) then
    Result := -1
  else if ((xI1 > xI2) and (xI2 <> -1)) or ((xI2 >= 0) and (xI1 = -1)) then
    Result := 1
  else
    Result := 0;
end;
//------------------------------------------------------------------------------
// sort fuction for stringgrid
//------------------------------------------------------------------------------
procedure SortTableItems(aStringGrid: TmmStringGrid);
var
  i: Integer;
  xItem: TLabDataItem;
begin
  i := 1;
  with aStringGrid do begin
    if Rows[0].Count > 1 then
      while i < ColCount do begin
        xItem := TLabDataItem(Objects[i, 0]);
        if Assigned(xItem) then begin
          if xItem.TableIndex < i then begin
            aStringGrid.MoveColumn(i, xItem.TableIndex);
            inc(i);
          end else if xItem.TableIndex > i then
            aStringGrid.MoveColumn(i, xItem.TableIndex)
          else
            inc(i);
        end else
          inc(i);
      end;
  end;
end;

//:---------------------------------------------------------------------------
//:--- Class: TfrmSettingsDialog
//:---------------------------------------------------------------------------
constructor TfrmSettingsDialog.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  HelpContext := GetHelpContext('LabMaster\LabBericht\LAB_Einstellungen.htm');

  bColorSelect.ColorArray    := cMMProdGroupColors;
  bHierarchyColor.ColorArray := cMMProdGroupColors;
  mItemTree.ExpandedGroups   := [ctDataItem];

  fAxisLeftTop           := Nil;
  fAxisRightTop          := Nil;
  fSplitXAxis            := Nil;
  fConfigChanged         := [];
  fParameter             := TLabDataItemList.Create;
  mItemTree.ClassDefList := gDataItemPool;
  mColDragIndex          := -1;
  mColIndex              := -1;
  mEnableChanges         := False;
  mInDragMode            := False;
  mKeyItemList           := TmmStringList.Create;
  mMethodFrame           := Nil;
  mScrollDir             := LabMasterDef.sdNone;
  mSelectedChartItem     := Nil;

  // Berechnungsmethoden noch in die CheckListBox f�llen
  clbCalcMethods.Items.AddObject(Translate(cCalculationMethods[cmFix].Name), Pointer(cmFix));
  clbCalcMethods.Items.AddObject(Translate(cCalculationMethods[cmAvg].Name), Pointer(cmAvg));
end;

//:---------------------------------------------------------------------------
destructor TfrmSettingsDialog.Destroy;
begin
  fParameter.Free;
  mKeyItemList.Free;
  FreeAndNil(mMethodFrame);

  inherited Destroy;
end;

//:---------------------------------------------------------------------------
{
// Wenn ein Matrix DataItem ausgew�hlt ist, die Art der Classdefects abfragen
if xQOItem.IsMatrixDataItem then begin
  with TfrmSelectDefects.Create(self) do try
    // Name des DataItems anzeigen
    TitleInfo := xQOItem.DisplayName;
    xClassDefects := xAlarm.QOItems.GetClassDefectsFromName(xQOItem.ItemName);
    DisabledClassDefects := xClassDefects;
    if xClassDefects = [cdCut, cdUncut, cdBoth] then
      ShowModal;
    xAbort := ModalResult = mrCancel;
    xQOItem.ClassDefects := DefectType;
  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try
end;// if xQOItem.IsMatrixDef then begin

if not(xAlarm.QOItems.SameDataItemExists(xQOItem.ItemName, xQOItem.ClassDefects)) then begin
{}
procedure TfrmSettingsDialog.AddToChartSeries(aItem: TLabDataItem; aPos: Integer; aInit: Boolean);
var
  i: Integer;
  xNewItem: TListItem;
  xNewDataItem: TLabDataItem;
  xDisplayName: string;
  xClassDefects: TClassDefectsSet;
  xAbort: Boolean;
begin
  xNewDataItem := Nil;
  if not aItem.KeyField then begin
    xDisplayName := aItem.DisplayName;
    if aInit then
      xNewDataItem := aItem
    else begin
      // Wenn es ein SuperClass Item ist muss noch definiert werden, ob es
      // die Schnitte/Ungeschnittene oder Beide Werte anzeigen soll
      if aItem.IsMatrixDataItem then begin
        xAbort        := True;
        fParameter.DataMode := dmChart;
        xClassDefects := fParameter.GetClassDefectsFromName(aItem.ItemName);
        if ([cdCut, cdUncut, cdBoth] - xClassDefects) <> [] then begin
          with TfrmSelectDefects.Create(Self) do
          try
            TitleInfo := xDisplayName;
            DisabledClassDefects := xClassDefects;
            xAbort := (ShowModal <> mrOk);
            if not xAbort then begin
              aItem.ClassDefects := DefectType;
              xDisplayName := Format('%s %s', [xDisplayName, GetDefectTypeText(DefectType)]);
            end;
          finally
            Free;
          end;
        end;
        if xAbort then
          Exit;
      end else begin
        // hole ein bestehendes Item von der Parameterliste...
        i := fParameter.IndexOfDisplayName(xDisplayName);
        if i <> -1 then begin
          xNewDataItem := fParameter.Items[i];
          // Dieses Basis Item wird schon in Grafik verwendet
          if xNewDataItem.ShowInChart then
            Exit;
        end;
      end;
        // Es wurde keins gefunden -> Neues als Kopie vom Original erstellen
      if not Assigned(xNewDataItem) then begin
        i := fParameter.AddCopy(aItem);
        xNewDataItem := fParameter.Items[i];
        xNewDataItem.DisplayName := xDisplayName;
      end;

      with xNewDataItem do begin
        if mEnableChanges then begin
          Include(fConfigChanged, dmChart);
          ShowInChart := True;
        end;
      end;
    end; // if aInit

    if aPos = -1 then xNewItem := lvChartSeries.Items.Add
                 else xNewItem := lvChartSeries.Items.Insert(aPos);

    with xNewItem do begin
      Caption    := Translate(xNewDataItem.DisplayName);
      Data       := xNewDataItem;
//      ImageIndex := Ord(xNewDataItem.ChartType);
    end;
  end; // if not KeyField
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.AddToTableItems(aItem: TLabDataItem; aInit: Boolean);
var
  i: Integer;
  xItem: TLabDataItem;
  xNewDataItem: TLabDataItem;
  xDisplayName: string;
  xClassDefects: TClassDefectsSet;
  xAbort: Boolean;
begin
  xNewDataItem := Nil;
  if Assigned(aItem) then begin
      // Wenn es ein SuperClass Item ist muss noch definiert werden, ob es
      // die Schnitte/Ungeschnittene oder Beide Werte anzeigen soll
    xDisplayName := aItem.DisplayName;
    if aInit then
      xNewDataItem := aItem
    else begin
      // Wenn es ein SuperClass Item ist muss noch definiert werden, ob es
      // die Schnitte/Ungeschnittene oder Beide Werte anzeigen soll
      if aItem.IsMatrixDataItem then begin
        xAbort        := True;
        fParameter.DataMode := dmGrid;
        xClassDefects := fParameter.GetClassDefectsFromName(aItem.ItemName);
        if ([cdCut, cdUncut, cdBoth] - xClassDefects) <> [] then begin
          with TfrmSelectDefects.Create(Self) do
          try
            TitleInfo := xDisplayName;
            DisabledClassDefects := xClassDefects;
            xAbort := (ShowModal <> mrOk);
            if not xAbort then begin
              aItem.ClassDefects := DefectType;
              xDisplayName := Format('%s %s', [xDisplayName, GetDefectTypeText(DefectType)]);
            end;
          finally
            Free;
          end;
        end;
        if xAbort then
          Exit;

//      if aItem.IsMatrixDataItem then begin
//        with TfrmSelectDefects.Create(Self) do
//        try
//          TitleInfo := xDisplayName;
//          if ShowModal = mrOk then begin
//            aItem.ClassDefects := DefectType;
//            xDisplayName := Format('%s %s', [xDisplayName, GetDefectTypeText(DefectType)]);
//          end else
//            Exit;
//        finally
//          Free;
//        end;

//      if aItem.IsMatrixDataItem then begin
//        with TfrmSelectDefects.Create(Self) do
//        try
//          TitleInfo := xDisplayName;
//          if ShowModal = mrOk then begin
//            aItem.ClassDefects := DefectType;
//            xDisplayName := Format('%s %s', [xDisplayName, GetDefectTypeText(DefectType)]);
//  //            xDisplayName := Format('%s %s', [xDisplayName, rgDefectType.Items[rgDefectType.ItemIndex]]);
//          end else
//            Exit;
//        finally
//          Free;
//        end;
      end else begin
          // hole ein bestehendes Item von der Parameterliste...
        i := fParameter.IndexOfDisplayName(xDisplayName);
        if i <> -1 then begin
          xNewDataItem := fParameter.Items[i];
            // Dieses Basis Item wird schon in Tabelle verwendet
          if xNewDataItem.ShowInTable then
            Exit;
        end;
      end;

        // Es wurde keins gefunden -> Neues als Kopie vom Original erstellen
      if not Assigned(xNewDataItem) then begin
        i := fParameter.AddCopy(aItem);
        xNewDataItem := fParameter.Items[i];
        xNewDataItem.DisplayName := xDisplayName;
      end;

      with xNewDataItem do begin
              // if in creating mode do not change property value for change notification
        if mEnableChanges then begin
          Include(fConfigChanged, dmGrid);
          ShowInTable := True;
        end;
      end;
    end; // if aInit
  
    with xNewDataItem do begin
      if DisplayWidth = 0 then
        DisplayWidth := 15;
  
      with sgTableItems do begin
                // one empty col is always there, check if this is the first one
        if Trim(Rows[0].Text) <> '' then
          ColCount := ColCount + 1;
  
        Rows[0].Add(Translate(xDisplayName));
        Objects[ColCount-1, 0] := xNewDataItem;
    //        ColWidths[ColCount-1]  := Round(DisplayWidth * cAverageCharWidth);
        ColWidths[ColCount-1]  := DisplayWidth;
  
                // if this data item is a key item move it before any dataitem
        if KeyField then begin
          for i:=0 to ColCount-1 do begin
            xItem := TLabDataItem(Objects[i, 0]);
            if Assigned(xItem) then begin
              if not xItem.KeyField then begin
                sgTableItems.MoveColumn(ColCount-1, i);
                Break;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.bChartImageOpenClick(Sender: TObject);
begin
  with mOpenDialog do begin
    DefaultExt := GraphicExtension(TBitmap);
    Filter := GraphicFilter(TGraphic);
    if Execute then begin
      edChartImage.Text := FileName;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.bClassDesignerClick(Sender: TObject);
var
  xIntf: OLEVariant;
  i: Integer;
  xTempItem: TLabDataItem;
begin
  try
    xIntf := CreateOLEObject(cClassDesignerServer);
    xIntf.Execute;
    xIntf := Unassigned;
    // �nderungen neu einlesen
    gDataItemPool.InitDataItems(0);
    mItemTree.Refresh;
    // wenn Klassier- oder Berechnungselemente �nderten, dann m�ssen diese in einer vorhandenen
    // Konfiguration neu �bernommen werden
    for i:=0 to fParameter.DataItemCount-1 do
      fParameter.Items[i].DataItem.Assign(TLabDataItem(gDataItemPool.DataItemByName[fParameter.Items[i].ItemName]).DataItem);
  
  except
    on e:Exception do begin
      CodeSite.SendError('TfrmSettingsDialog.bClassDesignerClick: ' + e.Message);
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.bGridColorClick(Sender: TObject);
var
  xPanel: TPanel;
begin
  if Sender = bTitleColor then xPanel := pnTitleFormat
                          else xPanel := pnDataFormat;
  
  ColorDialog.Color := xPanel.Color;
  if ColorDialog.Execute then begin
    xPanel.Color := ColorDialog.Color;
    if Sender = bTitleColor then begin
      fParameter.GridOptions^.TitleFont.BkColor := xPanel.Color;
    end else begin
      fParameter.GridOptions^.DataFont.BkColor := xPanel.Color;
    end;
    SetPropertiesToGrid;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.bGridFontClick(Sender: TObject);
var
  xPanel: TPanel;
begin
  if Sender = bTitleFont then xPanel := pnTitleFormat
                         else xPanel := pnDataFormat;
  
  FontDialog.Font := xPanel.Font;
  if FontDialog.Execute then begin
    xPanel.Font:= FontDialog.Font;
    if Sender = bTitleFont then begin
      with fParameter.GridOptions^.TitleFont do begin
        Name  := xPanel.Font.Name;
        Color := xPanel.Font.Color;
        Size  := xPanel.Font.Size;
        Style := xPanel.Font.Style;
      end;
    end else begin
      with fParameter.GridOptions^.DataFont do begin
        Name  := xPanel.Font.Name;
        Color := xPanel.Font.Color;
        Size  := xPanel.Font.Size;
        Style := xPanel.Font.Style;
      end;
    end;
    SetPropertiesToGrid;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.bOKClick(Sender: TObject);
begin
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.ChartAxisDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  xItem: TLabDataItem;
begin
  //fChartSelectionChanged := True;
  Include(fConfigChanged, dmChart);
  with Sender as TPanel do begin
    xItem := TLabDataItem(mItemTree.SelectedDataItem); // lvAvailable.Selected.Data;
    if Sender = pnChartXAxis then begin
      // save new X axis key field
      fParameter.XAxis := xItem.ItemName;
      Caption          := Translate(xItem.DisplayName);
      // if the user drags the same keyfield to X axis -> clear Z axis
      if AnsiSameText(fParameter.XAxis, fParameter.ZAxis) then begin
        fParameter.ZAxis     := '';
        pnChartZAxis.Caption := rsMsgMoveKeyItem;
      end;
    end
    else if Sender = pnChartZAxis then begin
      fParameter.ZAxis := xItem.ItemName;
      Caption          := Translate(xItem.DisplayName);
    end;
  end; // with Sender as TPanel
  // performs a redraw of lvChartSeries to draw items as ghost items or not
  lvChartSeries.Invalidate;
  UpdateChart;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.ChartItemsDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
var
  xSrcData: TLabDataItem;
  xDstData: TLabDataItem;
  xCol, xRow: Integer;
  xOffset: Integer;
  xDataItemArray: TDataItemArray;
  xLabItem: TLabDataItem;
  xSender: TControl;
begin
  Accept := False;
    // data item from the available listbox
  xSender := TControl(Sender);
    //
    // Ein neues Element wird versucht hineinzuziehen
    //
  if Source is TmmVirtualStringTree then begin
    xDataItemArray := mItemTree.Selection;
    if Length(xDataItemArray) > 0 then begin
      if Length(xDataItemArray) = 1 then begin
        xLabItem := TLabDataItem(xDataItemArray[0]);
          // Bei X- und Z-Achsen Feld nur Key-Items zulassen...
        if xSender = pnChartXAxis then
          Accept := xLabItem.KeyField
          // ...und beim Z-Achsenfeld nur, wenn es nicht schon im X-Achsenfeld verwendet wurde
        else if xSender = pnChartZAxis then begin
          Accept := xLabItem.KeyField;
          if fParameter.XAxis <> '' then
            Accept := Accept and (xLabItem.ItemName <> fParameter.XAxis);
        end
        else if xSender = lvChartSeries then
          Accept := not xLabItem.KeyField
        else // Drag over StringGrid
          Accept := True;
      end else
        Accept := True;
    end; // if Length()
  end
      //
      // Innerhalb der Chart ListView ODER StringGrid wird das Element verschoben
      //
  else if Sender = Source then begin
        // Special handling for grid if the drag cursor is at left or right edge
    if Sender = sgTableItems then begin
          // Spalte unter dem Mauszeiger
      sgTableItems.MouseToCell(X, Y, xCol, xRow);
      if (mColIndex >= 0) and (xCol >= 0) then begin
            // Draw dragging help line
        if (xCol <> mColDragIndex) and mInDragMode then begin
          DrawMove(mColDragIndex);
          DrawMove(xCol);
        end;
            // check for valid moving: key fields allways before data fields
            // and data fields allways after key fields
        xSrcData := TLabDataItem(sgTableItems.Objects[mColIndex, 0]);
        xDstData := TLabDataItem(sgTableItems.Objects[xCol, 0]);
        if Assigned(xSrcData) and Assigned(xDstData) then
          Accept := xSrcData.KeyField = xDstData.KeyField;
      end;
  
      mColDragIndex := xCol;
  
      xOffset := 15;
          // wenn am linken Rand angekommen, dann Spalten nach rechts scrollen
      if (X >= 0) and (X <= xOffset) then begin
        if sgTableItems.LeftCol > 0 then begin
          mScrollDir := LabMasterDef.sdLeft;
          timScrollGrid.Enabled := True
        end;
          // wenn am rechten Rand angekommen, dann Spalten nach links scrollen
      end else if (X >= sgTableItems.Width-xOffset) and (X <= sgTableItems.Width) then begin
        if (sgTableItems.LeftCol + sgTableItems.VisibleColCount) < sgTableItems.ColCount then begin
          mScrollDir := LabMasterDef.sdRight;
          timScrollGrid.Enabled := True
        end;
      end else
        timScrollGrid.Enabled := False;
    end else begin
      Accept := (Sender = lvChartSeries);
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.CheckComponentStateOfChartProperties;
var
  xEnabled: Boolean;
  xColor: TColor;
begin
  cobScaling.Enabled := (lbAxis.ItemIndex <> -1);
  xEnabled := (cobScaling.ItemIndex = 1);
  if xEnabled then xColor := clWindow
              else xColor := clBtnFace;

  edAxisMin.Enabled := xEnabled;
  edAxisMin.Color   := xColor;

  edAxisMax.Enabled := xEnabled;
  edAxisMax.Color   := xColor;

  se3DWidth.Enabled     := cbChart3D.Checked;
  cbChartRotate.Enabled := cbChart3D.Checked;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.DeleteItemsFromChart;
var
  xItem: TLabDataItem;
  xIndex: Integer;
begin
  Include(fConfigChanged, dmChart);
  with lvChartSeries do begin
    while Assigned(Selected) do begin
      xItem := TLabDataItem(Selected.Data);
      xItem.ShowInChart := False;
      Items.Delete(Selected.Index);
      // aus liste l�schen, wenn nicht mehr verwendet wird
      if (xItem.UseMode <> umAlways) and (not xItem.ShowInTable) then begin
        xIndex := fParameter.IndexOf(xItem);
        if xIndex <> -1 then
          fParameter.Delete(xIndex);
      end;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.DeleteItemsFromTable;
var
  xItem: TLabDataItem;
  xIndex: Integer;
begin
  Include(fConfigChanged, dmGrid);
  with sgTableItems do begin
    // reset flag ShowInTable
    xItem := TLabDataItem(Objects[mColIndex, 0]);
    xItem.ShowInTable := False;
    xItem.DisplayWidth := 64;
    //  empty table has to be handled different than if 2 or more columns are available
    if ColCount > 1 then begin
      sgTableItems.Rows[0].Strings[mColIndex] := '';
      sgTableItems.DeleteColumn(mColIndex);
    end else begin
      Cells[mColIndex, 0]   := '';
      Objects[mColIndex, 0] := Nil;
    end;
    mColIndex := -1;
    // Nun noch aus Liste l�schen, wenn nicht mehr verwendet wird
    if (xItem.UseMode <> umAlways) and (not xItem.ShowInChart) then begin
      xIndex := fParameter.IndexOf(xItem);
      if xIndex <> -1 then
        fParameter.Delete(xIndex);
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.DrawMove(aMovePos: Integer);
var
  xOldPen: TPen;
  xPos: Integer;
  xR: TRect;
begin
  if aMovePos >= 0 then begin
    xOldPen := TPen.Create;
    try
      with sgTableItems do begin
        xR := CellRect(aMovePos, 0);
        with Canvas do begin
          xOldPen.Assign(Pen);
          try
            Pen.Style := psDot;
            Pen.Mode := pmXor;
            Pen.Width := 5;

            if aMovePos > mColIndex then
              xPos := xR.Right
            else
              xPos := xR.Left;
            MoveTo(xPos, 0);
            LineTo(xPos, ClientHeight);
          finally
            Canvas.Pen := xOldPen;
          end;
        end;
      end;
    finally
      xOldPen.Free;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  RenumberingChartIndex;
  // RenumberingTableIndex gibt True zur�ck, wenn mind. 1 KeyField verwendet wird
  CanClose := (ModalResult = mrCancel) or RenumberingTableIndex;
end;

//:---------------------------------------------------------------------------
function TfrmSettingsDialog.GetAxisLeftTop: TChartAxis;
begin
  if not Assigned(fAxisLeftTop) then begin
    fAxisLeftTop := TChartAxis.Create(mChart);
    with fAxisLeftTop do begin
      Horizontal  := False;
      OtherSide   := False;
      EndPosition := 48;
      LabelsFont.Color := clBlue;
    end;
  end;
  Result := fAxisLeftTop;
end;

//:---------------------------------------------------------------------------
function TfrmSettingsDialog.GetSplitXAxis: TChartAxis;
begin
  if not Assigned(fSplitXAxis) then begin
    fSplitXAxis := TChartAxis.Create(mChart);
    with fSplitXAxis do begin
      Horizontal  := True;
      OtherSide   := False;
      StartPosition := 52;
      LabelsFont.Color := clGreen;
    end;
  end;
  Result := fSplitXAxis;
end;

//:---------------------------------------------------------------------------
function TfrmSettingsDialog.GetAxisRightTop: TChartAxis;
begin
  if not Assigned(fAxisRightTop) then begin
    fAxisRightTop := TChartAxis.Create(mChart);
    with fAxisRightTop do begin
      Horizontal  := False;
      OtherSide   := True;
      EndPosition := 48;
      LabelsFont.Color := clBlue;
      Grid.Visible     := False;
    end;
  end;
  Result := fAxisRightTop;
end;

//:---------------------------------------------------------------------------
function TfrmSettingsDialog.GetParameter: TLabDataItemList;
begin
  Result := fParameter;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.imgBasketDragDrop(Sender, Source: TObject; X, Y: Integer);
begin
  if Source = pnChartZAxis then begin
  //  fChartSelectionChanged := True;
    Include(fConfigChanged, dmChart);
    pnChartZAxis.Caption   := rsMsgMoveKeyItem;
    fParameter.ZAxis       := '';
    UpdateStateForChartDataItem;
    UpdateChart;
    // performs a redraw of lvChartSeries to draw items as ghost items
    lvChartSeries.Invalidate;
  end else if Source = lvChartSeries then begin
    DeleteItemsFromChart;
    UpdateStateForChartDataItem;
    UpdateChart;
  end else if Source = sgTableItems then begin
    DeleteItemsFromTable;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.imgBasketDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := (Source = pnChartZAxis) or (Source = lvChartSeries) or (Source = sgTableItems);
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.lbAxisClick(Sender: TObject);
begin
  if lbAxis.ItemIndex <> -1 then begin
    with fParameter.ChartOptions^.AxisInfo[TAxisType(lbAxis.ItemIndex)] do begin
      cobScaling.ItemIndex := Ord(Scaling);
      edAxisMin.Value      := Min;
      edAxisMax.Value      := Max;
    end;
  end;
  CheckComponentStateOfChartProperties
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.lvChartSeriesDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  i: Integer;
  xDropPos: Integer;
  xItem: TListItem;
  xDataItemArray: TDataItemArray;
  xLabItem: TLabDataItem;
begin
  xDropPos := 0;
  // first get the drop position in the listview
  with Sender as TListView do
  try
    xDropPos := Items.Count;
    // change Y value for a better mouse feeling for moving items
    Y := Y  - Font.Height;
    xItem := GetItemAt(X, Y);
    if Assigned(xItem) then
      xDropPos := xItem.Index;
  except
    on e:Exception do begin
      CodeSite.SendError('lvChartSeriesDragDrop: ' + e.Message);
    end;
  end;
  
  //fChartSelectionChanged := True;
  Include(fConfigChanged, dmChart);
  // Dragging innerhalb des ListViews? -> Move = copy & delete old
  if Sender = Source then begin
    with Sender as TListView do begin
      Items.BeginUpdate;
  
      xItem := Selected;
      with Items.Insert(xDropPos) do begin
        Assign(xItem);
        Items.Delete(xItem.Index);
        Selected := True;
      end;
      Items.EndUpdate;
    end;
  end else begin
    // Add items from lvAvailable
    // Bei Drag&Drop kann es sein, dass mehrere Items selektiert wurden
    xDataItemArray := mItemTree.Selection;
    // Alle sleektierten Items per Namen aus dem Array in die Listbox abf�llen
    for i:=0 to High(xDataItemArray) do begin
      xLabItem := TLabDataItem(xDataItemArray[i]);
      AddToChartSeries(xLabItem, xDropPos, False);
    end; // for i:=0 to High(xDataItemArray) do begin
  end;
  UpdateChart;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.lvChartSeriesDrawItem(Sender: TCustomListView; Item: TListItem; Rect: TRect; State: TOwnerDrawState);
var
  xEnabled: Boolean;
  xRect: TRect;
begin
  with Sender as TListView do begin
    with Canvas do begin
      FillRect(Rect);

      xEnabled := (not fParameter.ZAxisMode) or (Item.Index = 0);
      if xEnabled then
        Font.Color := clBlack
      else
        Font.Color := clBtnFace;

      if odSelected in State then begin
        Brush.Color := clHighlight;
        Font.Color  := clHighlightText;
      end;

      xRect := Rect;
      xRect.Left := SmallImages.Width + 2;
      TextRect(xRect, xRect.Left, xRect.Top + 2, Item.Caption);

      SmallImages.Draw(Canvas, Rect.Left, Rect.Top, Ord(TLabDataItem(Item.Data).ChartType), xEnabled);
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.lvChartSeriesSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
begin
  mSelectedChartItem := Nil;
  if not(csDestroying in ComponentState){ and Selected} then begin
    // dann Selektion als neues Datenelement annehmen
    if Selected and (lvChartSeries.SelCount = 1) then begin
      mSelectedChartItem := TLabDataItem(lvChartSeries.Selected.Data);
      ShowChartPropFromDataItem;
    end;

    UpdateStateForChartDataItem;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.lvItemsDblClick(Sender: TObject);
begin
  DeleteItemsFromChart;
  UpdateChart;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.mChartAfterDraw(Sender: TObject);
begin
  with fParameter.ChartOptions^ do begin
    RotateX := mChart.View3DOptions.Rotation;
    RotateY := mChart.View3DOptions.Elevation;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.mItemTreeDblClick(Sender: TObject);
var
  xLabItem: TLabDataItem;
begin
  if Assigned(mItemTree.SelectedDataItem) then begin
    xLabItem := TLabDataItem(mItemTree.SelectedDataItem);
    if pcSettings.ActivePageIndex = 0 then begin
      AddToChartSeries(xLabItem, -1, False);
      UpdateChart;
    end
    else if pcSettings.ActivePageIndex = 1 then
      AddToTableItems(xLabItem, False);
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.mItemTreeGetImage(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex; var Ghosted: Boolean; 
        var ImageIndex: Integer);
var
  xDataItem: TBaseDataItem;
begin
  // Anhand des Nodes der anfragt, kann das betroffene DataItem abgefragt werden
  xDataItem := mItemTree.GetNodeDataItem(Node);
  if Assigned(xDataItem) then begin
    if xDataItem is TLabDataItem then begin
      // Es soll nur in der Namensspalte ein Bild angezeigt werden
      if (Column = cNameCol) and TLabDataItem(xDataItem).KeyField then
        ImageIndex := cKeyImageIndex;
    end;
  end;// if assigned(xDataItem) then begin
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.OnChartOptionsChange(Sender: TObject);
var
  xUpdate: Boolean;
begin
  if mEnableChanges then begin
    xUpdate := True;
    case (Sender as TControl).Tag of
        // View3D
      0: fParameter.ChartOptions^.View3D := cbChart3D.Checked;
        // Rotate
      1: fParameter.ChartOptions^.AllowRotate := cbChartRotate.Checked;
        // bar style
      2: begin
          Include(fConfigChanged, dmChart);
          case cobBarStyle.ItemIndex of
            0: fParameter.ChartOptions.BarStyle := bsNormal;
            1: fParameter.ChartOptions.BarStyle := bsSide;
            2: fParameter.ChartOptions.BarStyle := bsStacked;
            3: fParameter.ChartOptions.BarStyle := bsStacked100;
          else
          end;
        end;
        // legend position
      3: fParameter.ChartOptions^.LegendAlign := cChartAlign[cobLegend.ItemIndex];
      4: fParameter.ChartOptions^.SplitXAxis  := cbSplitXAxis.Checked;
        // color gradient
      5: fParameter.ChartOptions^.StartColor  := bChartStartColor.Color;
      6: fParameter.ChartOptions^.EndColor    := bChartEndColor.Color;
      7: fParameter.ChartOptions^.GradientDir := cobChartGradient.ItemIndex;
        // back image
      8: fParameter.ChartOptions^.ImageInside := cbChartImageInside.Checked;
      9: if Trim(edChartImage.Text) = '' then begin
           fParameter.ChartOptions^.ImageFile := '';
           mChart.BackImage := Nil;
         end else if FileExists(edChartImage.Text) then begin
           fParameter.ChartOptions^.ImageFile := edChartImage.Text;
           mChart.BackImage.LoadFromFile(edChartImage.Text);
         end;

      12,13: begin
          with (Sender as TmmEdit) do begin
            case Tag of
              12: fParameter.ChartOptions^.AxisInfo[TAxisType(lbAxis.ItemIndex)].Min := AsInteger;
              13: fParameter.ChartOptions^.AxisInfo[TAxisType(lbAxis.ItemIndex)].Max := AsInteger;
            else
            end;
          end;
          xUpdate := False;
          timMinMax.Restart;
        end;
      16: fParameter.ChartOptions^.Width3D := se3DWidth.Value;
      17: fParameter.ChartOptions^.XLabelRotated := cbXLabelRotated.Checked;
      18: fParameter.ChartOptions^.AxisInfo[TAxisType(lbAxis.ItemIndex)].Scaling := TAxisScaling(cobScaling.ItemIndex);
    else
    end;
  
    if xUpdate then
      SetPropertiesToChart;
  end;
  CheckComponentStateOfChartProperties;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.OnGridOptionsClick(Sender: TObject);
begin
  if mEnableChanges then begin
    with Sender as TCheckBox do begin
      case Tag of
        0..6: begin
            if Checked then fParameter.GridOptions^.Options := fParameter.GridOptions^.Options + [Tag]
                       else fParameter.GridOptions^.Options := fParameter.GridOptions^.Options - [Tag];
            SetPropertiesToGrid;
          end;
        // Hierarchy Table
        7: begin
            Include(fConfigChanged, dmGrid);
            fParameter.GridOptions^.Hierarchy := Checked;
            ShowTablePropFromDataItem(mColIndex);
          end;
      else
      end;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.OnItemsSettingsChange(Sender: TObject);
var
  i, j: Integer;
  xTag: Integer;
  xStr: string;
begin
  if mEnableChanges then begin
    xTag := TControl(Sender).Tag;
    Include(fConfigChanged, dmChart);
    for i:=0 to lvChartSeries.Items.Count-1 do begin
      if lvChartSeries.Items[i].Selected then begin
        with TLabDataItem(lvChartSeries.Items[i].Data) do begin
          case xTag of
            // ChartType
            0..2: begin
                ChartType := TChartType(xTag+1);
                lvChartSeries.Items[i].ImageIndex := xTag+1;
              end;
            3: AxisType := TAxisType(cobItemAxis.ItemIndex);
              // graphic color
            8: Color := bColorSelect.Color;
            // ClassValueMode f�r Klassierfeldertrend (Cut, Uncut oder All f�r A1, A2, etc)
            10: begin
//                ClassDefects := TClassDefects(cobChartClassValue.ItemIndex);
                ClassDefects := TClassDefects(cobChartClassValue.Items.Objects[cobChartClassValue.ItemIndex]);
                xStr := gDataItemPool.DataItemByName[ItemName].DisplayName;
                DisplayName := Format('%s %s', [xStr, GetDefectTypeText(ClassDefects)]);
                lvChartSeries.Items[i].Caption := DisplayName;
              end;
            12: BorderCondition := edBorderCondition.AsInteger;
            13: begin
                with clbCalcMethods do begin
                  if Checked[ItemIndex] and Assigned(mMethodFrame) then
                    CalcMethods[TCalculationMethod(ItemIndex)] := mMethodFrame.MethodSettings
                  else
                    CalcMethods[TCalculationMethod(ItemIndex)] := '';
                end;
              end;
          else
          end; // case
        end; // with
      end; // if Selected
    end; // for
  end;
  UpdateChart;
  UpdateStateForChartDataItem;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.OnMatrixOptionsChange(Sender: TObject);
begin
  if mEnableChanges then begin
    Include(fConfigChanged, dmMatrix);
    case (Sender as TControl).Tag of
      0: fParameter.MatrixOptions^.ChannelVisible := cbChannelCurve.Checked;
      1: fParameter.MatrixOptions^.ClusterVisible := cbClusterCurve.Checked;
//      2: fParameter.MatrixOptions^.SpliceVisible  := cbSpliceCurve.Checked;
      2: fParameter.SpliceMatrixOptions^.SpliceVisible  := cbSpliceCurve.Checked;  //Nue:21.01.08
      3: begin
           fParameter.MatrixOptions^.DefectValues         := cbDefectValues.Checked;
           fParameter.SpliceMatrixOptions^.DefectValues   := cbDefectValues.Checked;
         end;
      4: begin
           fParameter.MatrixOptions^.CutValues            := cbCutValues.Checked;
           fParameter.SpliceMatrixOptions^.CutValues      := cbCutValues.Checked;
         end;
      5: begin
           fParameter.MatrixOptions^.ActiveVisible        := cbClassFields.Checked;
           fParameter.SpliceMatrixOptions^.ActiveVisible  := cbClassFields.Checked;
         end;
      6: begin
           fParameter.MatrixOptions^.MatrixMode         := 0;
           fParameter.SpliceMatrixOptions^.MatrixMode   := 0;
         end;
      7: begin
           fParameter.MatrixOptions^.MatrixMode         := 1;
           fParameter.SpliceMatrixOptions^.MatrixMode   := 1;
         end;
//      8: fParameter.MatrixOptions^.ActiveColor    := bClassActiveColor.Color;
//      9: fParameter.MatrixOptions^.InactiveColor  := bClassInactiveColor.Color;
      8: begin
           fParameter.MatrixOptions^.ActiveColor        := bClassActiveColor.Color;
           fParameter.SpliceMatrixOptions^.ActiveColor  := clFuchsia;    //Nue:21.01.08    ==> Allenfalls zus�tzliche Checkbox f�r SpliceActiveColor im Dialog!!
         end;
      9: begin
           fParameter.MatrixOptions^.InactiveColor  := bClassInactiveColor.Color;
           fParameter.SpliceMatrixOptions^.InactiveColor  := bClassInactiveColor.Color;  //Nue:21.01.08
         end;
    else
    end; // case
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.OnTableItemsChange(Sender: TObject);
const
  cIndexToAlign: Array[0..2] of TAlignment = (taLeftJustify, taCenter, taRightJustify);
var
  xStr: string;
begin
  if mEnableChanges and (mColIndex <> -1) then begin
  //  fGridSelectionChanged := True;
    Include(fConfigChanged, dmGrid);
    with TLabDataItem(sgTableItems.Rows[0].Objects[mColIndex]) do begin
      case TControl(Sender).Tag of
        // text alignment
        1: Alignment := cIndexToAlign[rgTextAlign.ItemIndex];
        // use thousands separator
        2: Thousands := cbThousands.Checked;
        // comma count
        3: Commas := seComma.Value;
        // hierarchy color for key items
        4: Color := bHierarchyColor.Color;
        // Sort order checkbox
        5: DescendingSort := cbKeySorting.Checked;
        // ClassValueMode f�r Grid
        6: begin
//            ClassDefects := TClassDefects(rgGridClassValue.ItemIndex);
            ClassDefects := TClassDefects(cobGridClassValue.Items.Objects[cobGridClassValue.ItemIndex]);
            xStr := gDataItemPool.DataItemByName[ItemName].DisplayName;
            DisplayName := Format('%s %s', [xStr, GetDefectTypeText(ClassDefects)]);
            sgTableItems.Cells[mColIndex, 0] := DisplayName;
          end;
      else
      end; // case
    end; // with
    sgTableItems.Invalidate;
  end; // if mEnableChanges
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.pmSelectAllClick(Sender: TObject);
var
  i: Integer;
begin
  with ActiveControl as TListView do
    for i:=0 to Items.Count-1 do
      Items[i].Selected := True;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.RenumberingChartIndex;
var
  i: Integer;
begin
  with lvChartSeries do begin
    for i:=0 to Items.Count-1 do
      TLabDataItem(Items[i].Data).ChartIndex := i;
  end;
end;

//:---------------------------------------------------------------------------
function TfrmSettingsDialog.RenumberingTableIndex: Boolean;
var
  i: Integer;
begin
  Result := False;
  fParameter.LevelInfo.Clear;
  with sgTableItems do begin
    if Trim(Rows[0].Text) <> '' then begin
      for i:=0 to ColCount-1 do
      try
        with TLabDataItem(Objects[i, 0]) do begin
          TableIndex   := i;
          DisplayWidth := ColWidths[i];
          Result       := Result or KeyField;
        end; // with
      except
        Break;
      end;
    end; // if Trim
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.SetActivePage(aIndex: Integer);
begin
  if (aIndex < 0) or (aIndex > pcSettings.PageCount) then
    aIndex := 0;
  pcSettings.ActivePageIndex := aIndex;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.SetParameter(Value: TLabDataItemList);
var
  i: Integer;
begin
  // copy parameters into local object
  fParameter.Assign(Value);
  with fParameter do begin
    // set GridOptions values to components
    with GridOptions^ do begin
      // table format and fonts
      pnTitleFormat.Color := TitleFont.BkColor;
      with pnTitleFormat.Font do begin
        Name  := TitleFont.Name;
        Size  := TitleFont.Size;
        Color := TitleFont.Color;
        Style := TitleFont.Style;
      end;
  
      pnDataFormat.Color := DataFont.BkColor;
      with pnDataFormat.Font do begin
        Name  := DataFont.Name;
        Size  := DataFont.Size;
        Color := DataFont.Color;
        Style := DataFont.Style;
      end;
  
      with sgTableItems do begin
        FixedColor := TitleFont.BkColor;
        Font.Color := TitleFont.Color;
        Font.Size  := TitleFont.Size;
        Font.Style := TitleFont.Style;
      end;
  
      cbHierarchyTable.Checked      := Hierarchy;
      cbRowLines.Checked            := 0 in Options;
      cbColLines.Checked            := 1 in Options;
      cbIndicator.Checked           := 2 in Options;
      cbRowSelect.Checked           := 3 in Options;
      cbAlwaysShowSelection.Checked := 4 in Options;
      cbTrailingEllipsis.Checked    := 5 in Options;
      cbTitleAlignment.Checked      := 6 in Options;
  
      udFixedCols.Position := FixedCols;
    end;

    // fill X, Z panels with table string
    if Assigned(XAxisItem) then
      pnChartXAxis.Caption := Translate(XAxisItem.DisplayName);
    if Assigned(ZAxisItem) then
      pnChartZAxis.Caption := Translate(ZAxisItem.DisplayName);

    // set ChartOptions values to components
    with ChartOptions^ do begin
      // color gradient
      bChartStartcolor.Color     := Startcolor;
      bChartEndColor.Color       := EndColor;
      cobChartGradient.ItemIndex := GradientDir;
      // back image
      cbChartImageInside.Checked := ImageInside;
      if FileExists(ImageFile) then begin
        edChartImage.Text := ImageFile;
        mChart.BackImage.LoadFromFile(ImageFile);
      end else
        edChartImage.Text := '';
  
      // legend position
      case LegendAlign of
        laLeft:   cobLegend.ItemIndex := 0;
        laRight:  cobLegend.ItemIndex := 1;
        laBottom: cobLegend.ItemIndex := 3;
      else // laTop
        cobLegend.ItemIndex := 2;
      end;

      // bar style
      case BarStyle of
        bsNormal:     cobBarStyle.ItemIndex := 0;
        bsStacked:    cobBarStyle.ItemIndex := 2;
        bsStacked100: cobBarStyle.ItemIndex := 3;
      else // bsSide
        cobBarStyle.ItemIndex := 1;
      end;

      cbChart3D.Checked       := View3D;
      cbChartRotate.Checked   := AllowRotate;
      se3DWidth.Value         := Width3D;
      cbXLabelRotated.Checked := XLabelRotated;

      cbSplitXAxis.Checked := SplitXAxis;
    end; // with ChartOptions

    // set MatrixOptions values to components
    with MatrixOptions^ do begin
      cbChannelCurve.Checked := ChannelVisible;
      cbClusterCurve.Checked := ClusterVisible;
//      cbSpliceCurve.Checked  := SpliceVisible;
      cbSpliceCurve.Checked  := SpliceMatrixOptions.SpliceVisible;  //Nue:21.01.08
      cbDefectValues.Checked := DefectValues;
      cbCutValues.Checked    := CutValues;
      cbClassFields.Checked  := ActiveVisible;
      bClassActiveColor.Enabled := ActiveVisible;
      bClassActiveColor.Color   := ActiveColor;
      bClassInactiveColor.Color := InactiveColor;
      if MatrixMode = 0 then rbQMCoarse.Checked := True
                        else rbQMFine.Checked   := True;
    end;

    // add data items to list views and set bitmap for chart type
    lvChartSeries.Items.Clear;
    for i:=0 to DataItemCount-1 do begin
      with Items[i] do
        if DisplayName <> '' then begin
          if ShowInTable then begin
            AddToTableItems(Items[i], True);
          end;

          if ShowInChart and not KeyField then begin
            AddToChartSeries(Items[i], -1, True);
          end;
        end;
    end; // for i
    if lvChartSeries.Items.Count > 0 then
      lvChartSeries.Items[0].Selected := True;

    SortTableItems(sgTableItems);
    lvChartSeries.CustomSort(@LVCustomSort, 0);
  end; // with fParameter

  // now check some settings in conditions of selected items
  SetPropertiesToChart;
  SetPropertiesToGrid;
  lbAxis.ItemIndex := 0;
  lbAxisClick(Nil);

  UpdateStateForChartDataItem;

  sgTableItems.Col := 0;
  ShowTablePropFromDataItem(-1);

  mEnableChanges := True;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.SetPropertiesToChart;
begin
  with fParameter.ChartOptions^ do begin
    mChart.View3D                   := View3D;
    mChart.View3DOptions.Orthogonal := not AllowRotate;
    mChart.View3DOptions.Rotation   := RotateX;
    mChart.View3DOptions.Elevation  := RotateY;
    mChart.Chart3DPercent           := Width3D;
    if mChart.View3D then
      mChart.Chart3DPercent := Width3D;        

    mChart.Legend.Alignment := LegendAlign;

    mChart.Gradient.EndColor   := StartColor;
    mChart.Gradient.StartColor := EndColor;
    case GradientDir of
      0: mChart.Gradient.Direction := gdTopBottom;
      1: mChart.Gradient.Direction := gdBottomTop;
      2: mChart.Gradient.Direction := gdLeftRight;
      3: mChart.Gradient.Direction := gdRightLeft;
    else
    end;

    mChart.BackImage := Nil;
    if ImageFile <> '' then
      mChart.BackImage.LoadFromFile(ImageFile);
    mChart.BackImageInside := ImageInside;

    mChart.BottomAxis.LabelsAngle := cXLabelAngle[XLabelRotated];
  end;
  UpdateChart;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.SetPropertiesToGrid;
begin
  with sgTableItems, fParameter.GridOptions^ do begin
    FixedColor := TitleFont.BkColor;
    Font.Color := TitleFont.Color;
    Font.Name  := TitleFont.Name;
    Font.Size  := TitleFont.Size;
    Font.Style := TitleFont.Style;
  end;
  
  with fParameter.GridOptions^ do begin
    if 0 in Options then sgTableItems.Options := sgTableItems.Options + [goHorzLine]
                    else sgTableItems.Options := sgTableItems.Options - [goHorzLine];
    if 1 in Options then sgTableItems.Options := sgTableItems.Options + [goVertLine]
                    else sgTableItems.Options := sgTableItems.Options - [goVertLine];
  
  end;
  sgTableItems.Invalidate;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.sgTableItemsDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  i: Integer;
  xCol, xRow: Integer;
  xDataItemArray: TDataItemArray;
  xLabItem: TLabDataItem;
begin
  //fGridSelectionChanged := True;
  Include(fConfigChanged, dmGrid);
  // Dragging innerhalb des Grids
  if Sender = Source then begin
    sgTableItems.MouseToCell(X, Y, xCol, xRow);
    if (xCol >= 0) and (xCol <> mColIndex) then begin
      sgTableItems.MoveColumn(mColIndex, xCol);
      mColIndex := xCol;
    end;
  end else begin
    // neues Item hinzuf�gen
    // Bei Drag&Drop kann es sein, dass mehrere Items selektiert wurden -> xDataItemArray
    xDataItemArray := mItemTree.Selection;
    // Alle selektierten Items per Namen aus dem Array in die Listbox abf�llen
    for i:=0 to High(xDataItemArray) do begin
      xLabItem := TLabDataItem(xDataItemArray[i]);
      AddToTableItems(xLabItem, False);
    end;// for i:=0 to High(xDataItemArray) do begin
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.sgTableItemsDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  
  const
    cColorWidth = 8;
  var
    xItem: TLabDataItem;
    xHeaderState: Integer;
    xOffs: Integer;
    xRect: TRect;
    xStr: String;
  
begin
  with Sender as TmmStringGrid do begin
    with Canvas do begin
      if (ARow = 0) and (Trim(Rows[0].Text) <> '') then begin
        xItem := TLabDataItem(Objects[ACol, 0]);
        xRect := Rect;
        Brush.Color := FixedColor;
  
        with Font do begin
          Color := fParameter.GridOptions^.TitleFont.Color;
          Name  := fParameter.GridOptions^.TitleFont.Name;
          Style := fParameter.GridOptions^.TitleFont.Style;
          Size  := fParameter.GridOptions^.TitleFont.Size;
        end;
  
        xHeaderState := DFCS_BUTTONPUSH;
        if (ACol = mColIndex) then
          xHeaderState := xHeaderState + DFCS_PUSHED
        else begin
          xRect.Right  := xRect.Right + 1;
          xRect.Bottom := xRect.Bottom + 1;
        end;
        DrawFrameControl(Canvas.Handle, xRect, DFC_BUTTON, xHeaderState);
  
        if Assigned(xItem) then begin
          xStr := Cells[ACol, ARow];
          InflateRect(xRect, -1, -1);

          with xItem do begin
            case Alignment of
              taRightJustify:
                with xRect do
                  xOffs := Left + ((Right - Left + 1) - TextWidth(xStr) - 1);
              taCenter:
                with xRect do
                  xOffs := Left + ((Right - Left + 1) - TextWidth(xStr)) div 2;
            else
              xOffs := xRect.Left + 1;
            end;
            TextRect(xRect, xOffs, xRect.Top + 1, xStr);
          end;
        end;
      end else begin
        Brush.Color := fParameter.GridOptions^.DataFont.BkColor;
        FillRect(Rect);
      end;
    end; // with Canvas
  end; // with Sender as
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.sgTableItemsEndDrag(Sender, Target: TObject; X, Y: Integer);
begin
  // remove dragging lines and end of drag mode
  DrawMove(mColDragIndex);
  mInDragMode := False;
  mColDragIndex := -1;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.sgTableItemsMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  mColIndex := -1;
  if (Button = mbLeft) and (Trim(sgTableItems.Rows[0].Text) <> '') then begin
    mColIndex := sgTableItems.MouseCoord(X, Y).X;
    if mColIndex <> -1 then begin
      if ssDouble in Shift then
        DeleteItemsFromTable
      else
        sgTableItems.BeginDrag(False, 5);
    end;
  end;
  mEnableChanges := False;
  ShowTablePropFromDataItem(mColIndex);
  mEnableChanges := True;
  sgTableItems.Invalidate;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.sgTableItemsStartDrag(Sender: TObject; var DragObject: TDragObject);
begin
  mInDragMode := True;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.HandleClassDefectsItems(aItem: TLabDataItem; aCob: TmmComboBox; aMode: TDataMode);
var
  xCD: TClassDefects;
  xCDSet: TClassDefectsSet;
begin
  aCob.Clear;
  aCob.Visible := aItem.IsMatrixDataItem;
  if aItem.IsMatrixDataItem then begin
    fParameter.DataMode := aMode;
    xCDSet              := fParameter.GetClassDefectsFromName(aItem.ItemName);
    // die eigene Auswahl muss immer m�glich sein zum selektieren
    Exclude(xCDSet, aItem.ClassDefects);
    for xCD:=Low(TClassDefects) to High(TClassDefects) do begin
      // nur noch die M�glichkeiten zur Auswahl geben, welche noch nicht vorhanden sind
      if not(xCD in xCDSet) then begin
        aCob.Items.AddObject(Translate(cClassDefectsStr[xCD]), Pointer(xCD));
        if xCD = aItem.ClassDefects then
          aCob.ItemIndex := aCob.Items.Count-1;
      end;
    end;
  end;
end;
//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.ShowChartPropFromDataItem;
var
  i: TCalculationMethod;
  xIndex: Integer;
  xStrList: TmmStringList;
//  xCD: TClassDefects;
//  xCDSet: TClassDefectsSet;
begin
  // Parameter sind nur dann verf�gbar, wenn nur 1 Element markiert ist
  if Assigned(mSelectedChartItem) then
  try
    mEnableChanges := False;
    with mSelectedChartItem do begin
      // ChartTyp und Farbe
      case ChartType of
        ctLine: bChartLine.Down := True;
        ctBar:  bChartBar.Down  := True;
        ctArea: bChartArea.Down := True;
      else
      end;
      bColorSelect.Color := Color;

      // Achse und f�r ClassItems die RadioGroup
      cobItemAxis.ItemIndex := ord(AxisType);
      HandleClassDefectsItems(mSelectedChartItem, cobChartClassValue, dmChart);

      // Grenzwerte und einf�rben
      edBorderCondition.Value := BorderCondition;

      // Berechnungsmethoden die einzelnen Methoden pr�fen
      for i:=Low(TCalculationMethod) to High(TCalculationMethod) do
        clbCalcMethods.Checked[ord(i)] := (CalcMethods[i] <> '');
      clbCalcMethods.ItemIndex := 0;
    end; // with
  finally
    mEnableChanges := True;
  end; // if Assigned()
  clbCalcMethodsClick(Nil);
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.UpdateStateForChartDataItem;
var
  xCM: TCalculationMethod;
  xBool: Boolean;
begin
  xBool := Assigned(mSelectedChartItem);

  // Buttons f�r Grafiktyp und Farbe
  bChartLine.Enabled        := xBool;
  bChartBar.Enabled         := xBool;
  bChartArea.Enabled        := xBool;
  bColorSelect.Enabled      := xBool;

  // Achsenausrichtung und Grenzwertmodus
  cobItemAxis.Enabled       := xBool;
  edBorderCondition.Enabled := xBool;

  // Berechnungsmethoden
  clbCalcMethods.Enabled := xBool;
  if not xBool then begin
    cobChartClassValue.Visible := False;
    for xCM:=Low(TCalculationMethod) to High(TCalculationMethod) do begin
      // wenn kein Datenelement selektiert ist, dann bei den Berechnungsmethoden alle abw�hlen
      if not xBool then
        clbCalcMethods.Checked[ord(xCM)] := False;
    end;
  end;
  
  cbSplitXAxis.Enabled := (fParameter.HasLeftTop and fParameter.HasRightTop);
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.ShowTablePropFromDataItem(aIndex: Integer);
const
  cAlignToIndex: Array[taLeftJustify..taCenter] of Integer = (0, 2, 1);
var
  xItem: TLabDataItem;
  //...............................................................
  procedure SetGroupIndex(aGroupBox: TRadioGroup; aStr: String; aStrArr: Array of String);
  var
    i: Integer;
  begin
    aGroupBox.ItemIndex := -1;
    for i:=0 to High(aStrArr) do
      if AnsiSameText(aStr, aStrArr[i]) then begin
        aGroupBox.ItemIndex := i;
        break;
      end;
  end;
  //...............................................................
begin
  rgTextAlign.Enabled       := (aIndex >= 0);
  gbKeyOptions.Enabled      := (aIndex >= 0);
  gbNumber.Enabled          := (aIndex >= 0);
//  cobGridClassValue.Enabled := (aIndex >= 0);
//  gbGridClassValue.Visible := False;

  if aIndex >= 0 then begin
    xItem := TLabDataItem(sgTableItems.Rows[0].Objects[aIndex]);
    with xItem do begin
      // Group box text alignment
      rgTextAlign.ItemIndex := cAlignToIndex[Alignment];

      // Group box number formating
      gbKeyOptions.Visible     := KeyField;
      gbGridClassValue.Visible := IsMatrixDataItem;
      HandleClassDefectsItems(xItem, cobGridClassValue, dmGrid);
      gbNumber.Visible         := gbGridClassValue.Visible or
                                  (DataType in [ftFloat, ftSmallint,ftInteger,ftWord,ftAutoInc,ftLargeInt]);
  
      if KeyField then begin
        gbKeyOptions.Left       := rgTextAlign.Width + 8;
        bHierarchyColor.Enabled := cbHierarchyTable.Checked;
        bHierarchyColor.Color   := Color;
        cbKeySorting.Checked    := DescendingSort;
      end else begin
        gbNumber.Left        := rgTextAlign.Width + 8;
        cbThousands.Checked  := Thousands;
        seComma.Value        := Commas;
      end;
    end; // with
  end else begin
    // radiogroups and groupboxes
    rgTextAlign.ItemIndex    := -1;
    gbNumber.Visible         := False;
    gbKeyOptions.Visible     := False;
    gbGridClassValue.Visible := False;
  end;
  sgTableItems.Invalidate;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.timMinMaxTimer(Sender: TObject);
begin
  timMinMax.Enabled := False;
  SetPropertiesToChart;
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.timScrollGridTimer(Sender: TObject);
begin
  with timScrollGrid do begin
    Interval := 300;
    case mScrollDir of
      LabMasterDef.sdLeft:
        if sgTableItems.LeftCol > 0 then
          sgTableItems.LeftCol := sgTableItems.LeftCol - 1
        else
          Enabled := False;
  
      LabMasterDef.sdRight:
        if (sgTableItems.LeftCol + sgTableItems.VisibleColCount) < sgTableItems.ColCount then
          sgTableItems.LeftCol := sgTableItems.LeftCol + 1
        else
          Enabled := False;
    else
      Enabled := False;
    end;
  end;
  sgTableItems.Refresh;
  DrawMove(mColDragIndex);
end;

//:---------------------------------------------------------------------------
procedure TfrmSettingsDialog.UpdateChart;
var
  xXLabel: string;
  xNow: TDateTime;
  xItem: TLabDataItem;
  xIsDateTime: Boolean;
  xZAxisMode: Boolean;
  x3DMode: Boolean;
  xCount: Integer;
  //...........................................................
  procedure LocalUpdateChart;
  var
    s, v: Integer;
    xSerie: TChartSeries;
    xColor: TColor;
    xStr: String;
  begin
    xSerie := Nil;
    for s:=xCount-1 downto 0 do begin
      if xZAxisMode then begin
        // Z-Achse automatisch vom KeyItem
        xItem := TLabDataItem(lvChartSeries.Items[0].Data);
        xColor := clTeeColor;
        xStr := Translate(fParameter.ZAxisItem.DisplayName);
        xStr := Format('%s %d', [xStr, s+1]);
      end else begin
        // Z-Achse = DataItems in ListView
        xItem := TLabDataItem(lvChartSeries.Items[s].Data);
        xColor := xItem.Color;
        xStr := xItem.DisplayName;
      end;

      case xItem.ChartType of
        ctLine: begin
            xSerie := TmmLineSeries.Create(Self);
            with xSerie as TLineSeries do begin
              SeriesColor   := xColor;
            end;
          end;
        ctBar: begin
            xSerie := TBarSeries.Create(Self);
            with xSerie as TBarSeries do begin
              SeriesColor   := xColor;
              case fParameter.ChartOptions^.BarStyle of
                bsSide:       MultiBar := mbSide;
                bsStacked:    MultiBar := mbStacked;
                bsStacked100: MultiBar := mbStacked100;
              else
                MultiBar := Series.mbNone;
              end;
            end;
          end;
        ctArea: begin
            xSerie := T2DAreaSeries.Create(Self);
            with xSerie as T2DAreaSeries do begin
              MultiArea   := maNone;
              SeriesColor := xColor;
            end;
          end;
      else
        Continue;
      end; // case xItem.ChartType

//      if fParameter.HasLeftTop and fParameter.HasRightTop and fParameter.ChartOptions^.SplitXAxis then begin
      if fParameter.DoSplitXAxis then begin
        mChart.BottomAxis.EndPosition := 48;
        if (xItem.AxisType in [atRight, atRightTop]) then
          xSerie.CustomHorizAxis := GetSplitXAxis;
      end else
        mChart.BottomAxis.EndPosition := 100;

      case xItem.AxisType of
        atLeft:     xSerie.VertAxis := aLeftAxis;
        atRight:    xSerie.VertAxis := aRightAxis;
        atLeftTop:  if xZAxisMode or x3DMode then xSerie.VertAxis       := aLeftAxis
                                             else xSerie.CustomVertAxis := GetAxisLeftTop;
        atRightTop: if xZAxisMode or x3DMode then xSerie.VertAxis       := aRightAxis
                                             else xSerie.CustomVertAxis := GetAxisRightTop;
      else
      end;

      xSerie.Marks.Visible := False;
      xSerie.ParentChart   := mChart;
      xSerie.Title         := xStr;
      xSerie.XValues.DateTime := xIsDateTime;

      // add dummy values
      for v:=1 to cDummyValuesCnt do begin
        if xIsDateTime then
          xSerie.AddXY(xNow + v, cDummyValues[s mod cDummySeriesCnt, v], '', clTeeColor)
        else
          xSerie.AddXY(v, cDummyValues[s mod cDummySeriesCnt, v], Format(xXLabel, [v]), clTeeColor);
      end;
    end; // for s

//    // if multiple axis are used than change top start of basic left/right axis
//    if fParameter.HasLeftTop and not xZAxisMode then mChart.LeftAxis.StartPosition  := 52
//                                                else mChart.LeftAxis.StartPosition  := 0;
//    if fParameter.HasRightTop and not xZAxisMode then mChart.RightAxis.StartPosition := 52
//                                                 else mChart.RightAxis.StartPosition := 0;
  end;
  //...........................................................
begin
  mChart.FreeAllSeries;

  xIsDateTime := False;

  xXLabel := ' %d';
  // check if X axis is set otherwise cancel drawing
  with fParameter do begin
    if XAxis <> '' then begin
      with XAxisItem do begin
        xXLabel  := Translate(DisplayName) + xXLabel;
        xIsDateTime := (DataType = ftDateTime);
      end;
      xNow := Now - 1;
    end else
      Exit;
  end;

  // check if Z axis is set
  with fParameter do begin
    xZAxisMode := ZAxisMode;
    x3DMode    := ChartOptions^.View3D;

    // if multiple axis are used than change top start of basic left/right axis
    if HasLeftTop and not xZAxisMode then mChart.LeftAxis.StartPosition  := 52
                                     else mChart.LeftAxis.StartPosition  := 0;
    if HasRightTop and not xZAxisMode then mChart.RightAxis.StartPosition := 52
                                      else mChart.RightAxis.StartPosition := 0;

    if xZAxisMode then begin
      if lvChartSeries.Items.Count > 0 then begin
        xCount := 3;
      end else
        Exit;
    end else begin
      xCount := lvChartSeries.Items.Count;
    end;
  end;

  Randomize;
  LocalUpdateChart;
end;
//------------------------------------------------------------------------------
procedure TfrmSettingsDialog.clbCalcMethodsClick(Sender: TObject);
begin
  // Alten Frame freigeben
  FreeAndNil(mMethodFrame);
  laCalcMethodDescription.Caption := '';

  // Je nach Methode den richtigen Frame erzeugen
  if (clbCalcMethods.ItemIndex <> -1) and Assigned(mSelectedChartItem) then begin
    // Objekt per Factory sowieso erstellen um die Beschreibung zu erhalten
    mMethodFrame := CalcMethodFrameFactory(TCalculationMethod(clbCalcMethods.Items.Objects[clbCalcMethods.ItemIndex]), mSelectedChartItem);
    // Nur weiter, wenn der Frame erzeugt wurde
    if assigned(mMethodFrame) then begin
      with mMethodFrame do begin
        laCalcMethodDescription.Caption := Description;
        if clbCalcMethods.Checked[clbCalcMethods.ItemIndex] then begin
          // Frame platzieren
          MethodSettings := mSelectedChartItem.CalcMethods[TCalculationMethod(clbCalcMethods.Items.Objects[clbCalcMethods.ItemIndex])];
          Parent := pnCalcMethod;
          Tag    := clbCalcMethods.Tag;
          Align  := alClient;
          OnGUIMethodSettingsChanging := MethodSettingsChange;
          mmTranslator1.Translate;
        end; // if Checked
      end;// with mMethodFrame
    end;// if assigned(mMethodFrame)
    OnItemsSettingsChange(clbCalcMethods);
  end; // if ItemIndex <> -1
end;
//------------------------------------------------------------------------------
procedure TfrmSettingsDialog.MethodSettingsChange(
  aSender: TfrmCalcMethodBaseFrame);
begin
  OnItemsSettingsChange(aSender);
end;

procedure TfrmSettingsDialog.bHelpClick(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;

procedure TfrmSettingsDialog.mmActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
  Handled := True;
end;

end.

