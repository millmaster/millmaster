(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: DetailInfoForm.pas
| Projectpart...:
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
| 20.02.2004 1.00 Wss | Funktion mmFormatFloatToStr statt mmFloatToStr
| 21.10.2005 1.00 Wss | Begrenzung der Position, damit das Fenster nicht aus dem Monitor
                        f�llt nun per SystemParametersInfo(SPI_GETWORKAREA...) und nicht mehr
                        mit Screen.Width/Height. Die Gr�sse vom Taskbar war nie mit eingerechnet.
|=============================================================================*)
unit DetailInfoForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, ComCtrls, mmTreeView, StdCtrls, mmLabel, VirtualTrees,
  ExtCtrls, mmPanel, LabReportClass, TeEngine, IvDictio, IvMulti, IvEMulti,
  mmTranslator, mmTimer;

type
  TfrmDetailInfo = class (TmmForm)
    laXValue: TmmLabel;
    laYTitle: TmmLabel;
    laYValue: TmmLabel;
    mmLabel2: TmmLabel;
    mmTranslator1: TmmTranslator;
    pnMain: TmmPanel;
    timClose: TmmTimer;
    tvDetails: TmmTreeView;
    procedure ControlsMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ControlsMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDeactivate(Sender: TObject);
    procedure timCloseTimer(Sender: TObject);
  private
    mDataItemlist: TLabDataItemList;
    mDM: TdmLabReport;
    mMousePoint: TPoint;
    mSeries: TChartSeries;
  public
    constructor Create(aOwner: TComponent; aDataModul: TdmLabReport; aContainer: TLabDataItemList); reintroduce; virtual;
    destructor Destroy; override;
    procedure ShowInfo(aClickedSeries: TChartSeries; X, Y, aValueIndex: Integer);
  end;

var
  frmDetailInfo: TfrmDetailInfo;

implementation
{$R *.DFM}
uses
  mmCS, LoepfeGlobal,
  MMUSBSelectNavigatorFrame, LabMasterDef, mmSeries;

const
  cHeightSmall = 40;
  cHeightLarge = 270;
  
//:---------------------------------------------------------------------------
//:--- Class: TfrmDetailInfo
//:---------------------------------------------------------------------------
constructor TfrmDetailInfo.Create(aOwner: TComponent; aDataModul: TdmLabReport; aContainer: TLabDataItemList);
begin
  inherited Create(aOwner);
  mDataItemlist := aContainer;
  mDM           := aDataModul;
  mSeries       := Nil;
end;

//:---------------------------------------------------------------------------
destructor TfrmDetailInfo.Destroy;
begin
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TfrmDetailInfo.ControlsMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  mMousePoint := Point(X, Y);
end;

//:---------------------------------------------------------------------------
procedure TfrmDetailInfo.ControlsMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if ssLeft in Shift then begin
    Left := Left + (X - mMousePoint.x);
    Top  := Top + (Y - mMousePoint.y);
    timClose.Restart;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmDetailInfo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caHide;
end;

//:---------------------------------------------------------------------------
procedure TfrmDetailInfo.FormDeactivate(Sender: TObject);
begin
  Hide;
end;

//:---------------------------------------------------------------------------
procedure TfrmDetailInfo.ShowInfo(aClickedSeries: TChartSeries; X, Y, aValueIndex: Integer);
var
  i: Integer;
  xKeySet: TSourceSelectionSet;
  xSource: TSourceSelection;
  xTreeNode: TTreeNode;
  xLabItem: TLabDataItem;
  xStrList: TStringList;
  xAddWhere: string;
  xStr: string;
  xRect: TRect;
  xRight, xBottom: Integer;
  xOldCursor: TCursor;
  xExtendSeries: IExtendSeries;
  //...........................................................
  function GetEnumFromItemName(aItemName: String): TSourceSelection;
  var
    i: TSourceSelection;
  begin
    Result := TSourceSelection(-1);
    for i:=ssStyle to ssYMSet do begin
      if AnsiSameText(aItemName, cDetailKeyItemFields[i]) then begin
        Result := i;
        Break;
      end;
    end;
  end;
  //...........................................................

begin
  mSeries := aClickedSeries;
  if aValueIndex <> -1 then begin
    xOldCursor := Screen.Cursor;
    try
      Screen.Cursor := crSQLWait;

      // First: at least the value and X-Markinfo have to filled in
      mSeries.GetInterface(IExtendSeries, xExtendSeries);
      if Assigned(xExtendSeries) then begin
        // Basisinformationen abf�llen, welche auch ohne DB zu haben sind
        laYTitle.Caption := mSeries.Title;
        laYValue.Caption := mmFormatFloatToStr(mSeries.YValues[aValueIndex]);
        if mSeries.XValues.DateTime then begin
          laXValue.Caption := DateTimeToStr(mSeries.XValue[aValueIndex]);
        end else begin
          laXValue.Caption := mSeries.XLabel[aValueIndex];
        end;

        tvDetails.Items.Clear;
        if Assigned(xExtendSeries.Data) then begin
          Height := cHeightLarge;
          // Second: if detailed enabled than perform the request of detailed data

          // alle Key-Elemente freischalten
          xKeySet  := [ssStyle..ssYMSet];
          xStrList := TStringList.Create;
          with mDataItemList do
          try
            // X-Key-Element aus dem Set entfernen
            xSource := GetEnumFromItemName(XAxis);
            Exclude(xKeySet, xSource);
            // wenn Zeit auf X-Achse, dann der Filterwert f�r Zeit nach US Format machen, ansonsten einfach String �bernehmen
            if mSeries.XValues.DateTime then xStr := FormatDateTime(cUSADateMask, mSeries.XValue[aValueIndex])
                                        else xStr := laXValue.Caption;
            xAddWhere := Format('AND %s = ''%s'' ', [XAxisItem.ColName, xStr]);


            if ZAxisMode then begin
              // Z-Key-Element aus dem Set entfernen
              xSource := GetEnumFromItemName(ZAxis);
              Exclude(xKeySet, xSource);

              if LotDetails then begin
                xAddWhere := xAddWhere + Format('AND %s = %d ', [LotDetailItem.ColName, Integer(xExtendSeries.Value)]);
              end else begin
                if mSeries.YValues.DateTime then
                  // wenn Zeit auf Z-Achse, dann der Filterwert f�r Zeit nach US Format machen, ansonsten einfach String �bernehmen
                  xStr := FormatDateTime(cUSADateMask, StrToDateTime(mSeries.Title))
                else
                  xStr := xExtendSeries.Value;
                xAddWhere := xAddWhere + Format('AND %s = ''%s'' ', [ZAxisItem.ColName, xStr]);
              end;
            end;

            // Read for the left key item enums the associated values from DB
            for xSource:=ssStyle to ssYMSet do begin
              if xSource in xKeySet then begin
                xLabItem := TLabDataItem(gDataItemPool.DataItemByName[cDetailKeyItemFields[xSource]]);

                if Assigned(xLabItem) then begin
                  xTreeNode := tvDetails.Items.Add(Nil, Translate(xLabItem.DisplayName));
                  mDM.GetDetailKeyItems(xSource, xAddWhere, xStrList);
                  for i:=0 to xStrList.Count-1 do begin
                    tvDetails.Items.AddChild(xTreeNode, xStrList.Strings[i]);
                  end;
                  xTreeNode.Expand(True);
                end; // if Assigned
              end; // if xSource in xKeySet
            end; // for xSource
            tvDetails.Items.Item[0].MakeVisible;
          except
          end; // with mDataItemList do
        end else
          Height := cHeightSmall;

        // Nun noch die Koordinaten setzen und Show aufrufen
        // Screensize ohne Taskbar (= Workarea) ermitteln
        SystemParametersInfo(SPI_GETWORKAREA, 0, @xRect, 0);
        xRight  := xRect.Right - Width - 5;
        xBottom := xRect.Bottom - Height - 5;
        if X > xRight  then X := xRight;
        if Y > xBottom then Y := xBottom;

        Left := X+5;
        Top  := Y+5;
        timClose.Restart;
        Show;
      end; // if Assigned()
    finally
      Screen.Cursor := xOldCursor;
    end;
  end; // if aValueIndex <> -1
end;

//:---------------------------------------------------------------------------
procedure TfrmDetailInfo.timCloseTimer(Sender: TObject);
begin
  Close;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
end.





