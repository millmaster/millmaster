inherited frmMainForm: TfrmMainForm
  Left = 242
  Top = 119
  Width = 544
  Height = 406
  Caption = '(*)Labor Bericht - MillMaster NT'
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited plTopPanel: TmmPanel
    Width = 536
    inherited Panel2: TmmPanel
      Left = 472
    end
    inherited mToolBar: TmmToolBar
      Width = 472
    end
  end
  inherited mmStatusBar: TmmStatusBar
    Top = 342
    Width = 536
  end
  inherited ImageList16x16: TmmImageList
    Left = 120
    Top = 72
  end
  inherited mmActionListMain: TmmActionList
    Left = 120
    Top = 96
    inherited acNew: TAction
      ShortCut = 16462
    end
    object acSecurity: TAction
      Category = 'Login'
      Caption = '(*)Zugriffkontrolle'
      Hint = '(*)Zugriffkontrolle konfigurieren'
      OnExecute = acSecurityExecute
    end
  end
  inherited mMainMenu: TmmMainMenu [4]
    Left = 120
    Top = 120
    inherited miDatei: TMenuItem
      object Neu1: TMenuItem [1]
        Caption = '(*)&Neu'
        ShortCut = 16461
        OnClick = Neu1Click
      end
    end
    inherited miExtras: TMenuItem
      object Zugriffkontrolle1: TMenuItem [1]
        Action = acSecurity
      end
    end
  end
  inherited pmToolbar: TmmPopupMenu [5]
    Left = 120
    Top = 144
  end
  inherited MMLogin: TMMLogin [6]
    Left = 120
    Top = 168
  end
  inherited mDictionary: TmmDictionary [7]
    DictionaryName = 'LabMasterDic'
    Language = -1
    MasterLanguage = 2
    AutoConfig = True
    OnAfterLangChange = mDictionaryAfterLangChange
    Left = 120
    Top = 192
  end
  inherited mTranslator: TmmTranslator [8]
    DictionaryName = 'LabMasterDic'
    Left = 120
    Top = 216
    TargetsData = (
      1
      2
      (
        '*'
        'Filter'
        0)
      (
        '*'
        'Title'
        0))
  end
  object MMSecurityDB: TMMSecurityDB [9]
    Active = False
    CheckApplStart = False
    Left = 120
    Top = 240
  end
  object MMSecurityControl: TMMSecurityControl [10]
    FormCaption = '(*)Labor Bericht - MillMaster NT'
    Active = True
    MMSecurityName = 'MMSecurityDB'
    Left = 120
    Top = 272
  end
  inherited mmSaveDialog: TmmSaveDialog [11]
    Left = 336
    Top = 8
  end
  inherited mmPrinterSetupDialog: TmmPrinterSetupDialog [12]
    Left = 400
    Top = 8
  end
  inherited mmOpenDialog: TmmOpenDialog [13]
    Left = 304
    Top = 8
  end
  inherited mmPrintDialog: TmmPrintDialog [14]
    Left = 368
    Top = 8
  end
  object MMHtmlHelp: TMMHtmlHelp
    AutoConfig = True
    HelpMask = 'MMHelp-%.3d.chm'
    MMHelp = mmpEasy
    Left = 152
    Top = 72
  end
end
