(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: Data.pas
| Projectpart...: Datamodul fuer LabReport
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows 2000 SP2
| Target.system.: Windows NT/2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
| 05.06.2002 1.00 Lok | File created
| 18.07.2002 1.01 Nue | Name TParameters changed to TQueryParameters
| 26.11.2002 1.01 Wss | Size of TStringField set to 70 to have enough space
| April 2003 1.01 Wss | Klassen in eine Unit zusammengefasst
| 26.08.2003 1.01 Wss | Datenelemente f�r Klassierdaten erhalten eine Defaultfarbe
| 02.09.2003 1.01 Wss | Fix f�r DetailInfo mit Klassierelementen behoben GetWhere('', True)
| 28.04.2004 1.01 Wss | GetChartItemsCommaList: bug mit doppelter Klassenbezeichnung behoben
| 16.06.2005 1.01 Wss | "PrintRepetition" hinzugef�gt
| 21.10.2005 2.03 Wss | Handling von PEff korrigiert damit Berechnungsmethoden funktionieren
| 19.11.2007 2.04 Nue | Adding additional PrintSettings-Komponents to saved settings on DB (t_settings)
| 20.01.2008 2.05 Nue | Adding modification because adding seperate SpliceMatrixKomponent
|                     | (handling Splice-Classification)
| 12.02.2008 2.06 Nue | Anpassen f�r V5.04: VCV-Zus�tze eingef�gt.
|=============================================================================*)
unit LabReportClass;



interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, mmADODataSet, mmADOConnection, IniFiles,
  Series, Chart, TeEngine,
  mmADOQuery, mmDataSource, mmADOCommand, mmStringList, mmDBGrid, mmSeries, mmChart,
  MMUGlobal, LoepfeGlobal, mmVCLStringList, ClassDef, LabMasterDef, 
  MMUSBSelectNavigatorFrame, CalcMethods;


type
  TdmLabReport = class;
  TLabDataItem = class(TDataItemDecorator)
  private
    fAlignment: TAlignment;
    fAxisType: TAxisType;
    fBorderCondition: Integer;
    fChartIndex: Integer;
    fChartType: TChartType;
    fColor: TColor;
    fCommas: Integer;
    fDataLevel: Integer;
    fDataMode: TDataMode;
    fDataType: TFieldType;
    fDescendingSort: Boolean;
    fDisplayWidth: Integer;
    fKeyField: Boolean;
    fKeyFieldName: String;
    fMassField: TMassField;
    fRightAxis: Boolean;
    fShowInChart: Boolean;
    fShowInTable: Boolean;
    fShowSeconds: Boolean;
    fTableIndex: Integer;
    fThousands: Boolean;
    fUseMode: TUseMode;
    fYScalingFactor: Integer;
    mCalcMethods: Array[TCalculationMethod] of String;
    function GetCalcMethods(aIndex: TCalculationMethod): String;
    function GetCalcName: String;
    function GetColName: String;
    function GetKeyFieldID: String;
    function GetMassFieldName: String;
    procedure SetCalcMethods(aIndex: TCalculationMethod; const Value: String);
  protected
    function GetConfigString: String; override;
    procedure SetConfigString(Value: String); override;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function CalcMethodCount: Integer;
    function GetSQLFromTable: String; overload; override;
    function GetSQLSelectFields(aWithSum: boolean = true; aWithAlias:boolean = true): String; override;
    function GetSQLWhere: String; override;
    property Alignment: TAlignment read fAlignment write fAlignment;
    property AxisType: TAxisType read fAxisType write fAxisType;
    property BorderCondition: Integer read fBorderCondition write fBorderCondition;
    property CalcMethods[aIndex: TCalculationMethod]: String read GetCalcMethods write SetCalcMethods;
    property CalcName: String read GetCalcName;
    //1 order position in chart 
    property ChartIndex: Integer read fChartIndex write fChartIndex;
    //1 none, line, bar, area 
    property ChartType: TChartType read fChartType write fChartType;
    property ColName: String read GetColName;
    //1 color for chart or for key item if hierarchy table 
    property Color: TColor read fColor write fColor;
    property Commas: Integer read fCommas write fCommas;
    property DataLevel: Integer read fDataLevel write fDataLevel;
    property DataMode: TDataMode read fDataMode write fDataMode;
    property DataType: TFieldType read fDataType write fDataType;
    //1 sort order in query statement 
    property DescendingSort: Boolean read fDescendingSort write fDescendingSort;
    //1 with in pixel of column size of dbgrid 
    property DisplayWidth: Integer read fDisplayWidth write fDisplayWidth;
    //1 true: this field appears in the group statement if selected 
    property KeyField: Boolean read fKeyField write fKeyField;
    property KeyFieldID: String read GetKeyFieldID;
    //1 associated field name eg. c_style_id for c_style_name 
    property KeyFieldName: String read fKeyFieldName write fKeyFieldName;
    property MassField: TMassField read fMassField write fMassField;
    property MassFieldName: String read GetMassFieldName;
    //1 True: RightAxis; False: LeftAxis 
    property RightAxis: Boolean read fRightAxis write fRightAxis;
    //1 true if this item is selected to be shown in chart 
    property ShowInChart: Boolean read fShowInChart write fShowInChart;
    //1 true if this item is selected to be shown in grid 
    property ShowInTable: Boolean read fShowInTable write fShowInTable;
    //1 show seconds in Date/Time format 
    property ShowSeconds: Boolean read fShowSeconds write fShowSeconds;
    //1 order position in table 
    property TableIndex: Integer read fTableIndex write fTableIndex;
    property Thousands: Boolean read fThousands write fThousands;
    property UseMode: TUseMode read fUseMode write fUseMode;
    property YScalingFactor: Integer read fYScalingFactor write fYScalingFactor;
  end;
  
  TLabDataItemList = class(TClassDefContainer)
  private
    fChartOptions: TChartOptions;
    fDataLevel: Integer;
    fDataMode: TDataMode;
    fGridOptions: TGridOptions;
    fLotDetails: Boolean;
    fMatrixOptions: TMatrixOptions;
    fSpliceMatrixOptions: TMatrixOptions;    //Nue:20.01.08
    fReportOptions: TReportOptions;
    fXAxis: String;
    fZAxis: String;
    function CheckLeftRightTop(aAxisType: TAxisType): Boolean;
    function GetAxisInfo(Index: TAxisType): PAxisInfoRec;
    function GetChartItemsCommaList: String;
    function GetChartOptions: PChartOptions;
    function GetGridOptions: PGridOptions;
    function GetItems(aIndex: Integer): TLabDataItem;
    function GetLotDetails: Boolean;
    function GetMatrixOptions: PMatrixOptions;
    function GetReportOptions: PReportOptions;
    function GetSpliceMatrixOptions: PMatrixOptions;
    function GetXAxisItem: TLabDataItem;
    function GetZAxisItem: TLabDataItem;
    function GetZAxisMode: Boolean;
    procedure SetDataLevel(Value: Integer);
    procedure SetDataMode(Value: TDataMode);
    procedure SetXAxis(const Value: String);
    procedure SetZAxis(const Value: String);
  public
    LevelInfo: TmmStringList;
    constructor Create(aPersistent: boolean = false); override;
    destructor Destroy; override;
    procedure Assign(aSource: TClassDefList); override;
    procedure Clear; override;
    function DoSplitXAxis: Boolean;
    function GetClassDefectsFromName(aItemName: string): TClassDefectsSet; override;
    function GetKeyFieldForLevel(aLevel: Integer): String;
    function GetKeyItemForLevel(aLevel: Integer): TLabDataItem;
    function GetSQLFromTables: String; override;
    function GetSQLSelectFields(aWithSum: boolean = true; aWithAlias: boolean = true): String;
            reintroduce; override;
    function GetSQLWhere: String; override;
    function HasLeftTop: Boolean;
    function HasRightTop: Boolean;
    procedure InitDataItems(aMode: Integer);
    function LevelCount: Integer;
    function LotDetailItem: TLabDataItem;
    function LotTrendMode: Boolean;
    procedure ReadFromConfig(aIniFile: TCustomIniFile);
    function TrendMode: Boolean;
    procedure WriteToConfig(aIniFile: TCustomIniFile);
    property AxisInfo[Index: TAxisType]: PAxisInfoRec read GetAxisInfo;
    property ChartItemsCommaList: String read GetChartItemsCommaList;
    property ChartOptions: PChartOptions read GetChartOptions;
    //1 Wird f�r das Grid im Summen-Detail Modus verwendet 
    property DataLevel: Integer read fDataLevel write SetDataLevel;
    //1 Verhalten des Containers wird z.T. �ber dieses Property gesteuert 
    property DataMode: TDataMode read fDataMode write SetDataMode;
    property GridOptions: PGridOptions read GetGridOptions;
    property Items[aIndex: Integer]: TLabDataItem read GetItems;
    //1 Im LotTrendMode k�nnen die Partien aufgeschl�sselt werden 
    property LotDetails: Boolean read GetLotDetails write fLotDetails;
    property MatrixOptions: PMatrixOptions read GetMatrixOptions;
    property ReportOptions: PReportOptions read GetReportOptions;
    property SpliceMatrixOptions: PMatrixOptions read GetSpliceMatrixOptions;
    property XAxis: String read fXAxis write SetXAxis;
    //1 Liefert ein Datenelement anhand vom Property XAxis zur�ck 
    property XAxisItem: TLabDataItem read GetXAxisItem;
    property ZAxis: String read fZAxis write SetZAxis;
    //1 Liefert ein Datenelement anhand vom Property XAxis zur�ck 
    property ZAxisItem: TLabDataItem read GetZAxisItem;
    property ZAxisMode: Boolean read GetZAxisMode;
  end;
  
  TdmLabReport = class(TDataModule)
    conDefault: TmmADOConnection;
    dseChart: TmmADODataSet;
    dseGrid: TmmADODataSet;
    dseGridDetail: TmmADODataSet;
    dseWork: TmmADODataSet;
    slQOTestParameter: TmmVCLStringList;
    slTemplateSettings: TmmVCLStringList;
    procedure DataSetCalcFields(DataSet: TDataSet);
    procedure dseBeforeOpen(DataSet: TDataSet);
  private
    fDBParams: TQueryParameters;
    fDefaultShiftCal: Integer;
    fLenBase: TLenBase;
    fResultQuery: TmmADODataSet;
    mDataItemList: TLabDataItemList;
    mFrom: TStringList;
    mSelect: TStringList;
    mWhere: TStringList;
    function GetWhereStr(aAddWhere: String; aWithContainerWhere: Boolean): String;
  protected
    procedure DoGetDataItemGenerateSQL(Sender: TObject; aDataItem: TBaseDataItem; var aEnabled: boolean);
  public
    constructor Create(aOwner: TComponent; aDataItemList: TLabDataItemList); reintroduce;
    destructor Destroy; override;
    function CreateAllQueryFields(aDataMode: TDataMode; aLevel: Integer = 0; aAddWhere: String = ''):
            Boolean;
    function GetDetailKeyItems(aKeyValue: TSourceSelection; aAddWhere: String; var aValues:
            TStringList): Boolean;
    function GetXAxisNamesForChart(aFieldName: String; var aStrList: TmmStringList): Boolean;
    function GetZAxisNamesForChart(aItem: TLabDataItem; aDetailed: Boolean; var aStrList:
            TmmStringList): Boolean;
    property DBParams: TQueryParameters read fDBParams;
    property DefaultShiftCal: Integer read fDefaultShiftCal write fDefaultShiftCal;
    property LenBase: TLenBase read fLenBase write fLenBase;
    property ResultQuery: TmmADODataSet read fResultQuery;
  end;

  TLabDataItemPEff = class(TLabDataItem)
  public
    function GetSQLSelectFields(aWithSum: boolean = true; aWithAlias:boolean = true): String; override;
  end;


const
  cLabItemClass:     TDataItemDecoratorClass =  TLabDataItem;
  cLabItemPEffClass: TDataItemDecoratorClass =  TLabDataItemPEff;
//------------------------------------------------------------------------------
var
  gDataItemPool: TLabDataItemList;

implementation
{$R *.DFM}
uses
  mmMBCS, mmCS, IvDictio, mmColorButton;

const
  cDefaultItem1 = 'ItemName=CYTot,DisplayName=CYTot,Alignment=0,ChartIndex=0,ChartType=2,Col=0,Color=8388863,Commas=1,DisplayWidth=44,ShowInChart=-1,ShowInTable=-1,TableIndex=1';
  cDefaultItem2 = 'ItemName=machine_name,DisplayName=(15)Maschine,Alignment=0,Color=-2147483624,DescendingSort=0,DisplayWidth=102,ShowInTable=-1,TableIndex=0,Thousands=0';

//------------------------------------------------------------------------------
procedure ConvertIntToSet(aSrc: Integer; aDest: PByte; aSize: Integer);
begin
  System.Move(aSrc, aDest^, aSize);
end;
//------------------------------------------------------------------------------
function ConvertSetToInt(aPByte: PByte; aSize: Integer): DWord;
begin
  Result := 0;
  System.Move(aPByte^, Result, aSize);
end;
//------------------------------------------------------------------------------
function RectToStr(aRect: TRect): String;
begin
  with aRect do
    Result := Format('%d,%d,%d,%d', [Left, Top, Right, Bottom]);
end;
//------------------------------------------------------------------------------
function StrToRect(aRect: String): TRect;
begin
  with TStringList.Create do
  try
    CommaText := aRect;
    with Result do begin
      Left   := StrToIntDef(Strings[0], -1);
      Top    := StrToIntDef(Strings[1], -1);
      Right  := StrToIntDef(Strings[2], -1);
      Bottom := StrToIntDef(Strings[3], -1);
    end;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
function KeyStrSortProc(List: TStringList; Index1, Index2: Integer):Integer;
var
  xVal1, xVal2: Integer;
begin
  xVal1 := Integer(List.Objects[Index1]);
  xVal2 := Integer(List.Objects[Index2]);
  if xVal1 > xVal2 then
    Result := 1
  else if xVal1 < xVal2 then
    Result := -1
  else
    Result := 0;
end;

//:---------------------------------------------------------------------------
//:--- Class: TLabDataItem
//:---------------------------------------------------------------------------
constructor TLabDataItem.Create;
begin
  inherited Create;
  //:...................................................................
  // So, noch ein paar Defaultwerte
  fBorderCondition := 1;
  fChartType       := ctBar;
  fDataLevel       := -1;
  fDataType        := ftFloat;
  fDisplayWidth    := 64;
  fMassField       := mfLen; // mfLen, weil bei den Superklassen ich sonst alle durchgehen und setzen m�sste
end; // TLabDataItem.Create

//:---------------------------------------------------------------------------
destructor TLabDataItem.Destroy;
begin
  inherited Destroy;
end; // TLabDataItem.Destroy

//:---------------------------------------------------------------------------
procedure TLabDataItem.Assign(Source: TPersistent);
var
  xSource: TLabDataItem;
  i: TCalculationMethod;
begin
  if Source is TLabDataItem then begin
    xSource := TLabDataItem(Source);
    // Alle Eigenschaften kopieren
    Alignment      := xSource.Alignment;
    AxisType       := xSource.AxisType;
    BorderCondition := xSource.BorderCondition;
    ChartIndex     := xSource.ChartIndex;
    ChartType      := xSource.ChartType;
    Color          := xSource.Color;
    Commas         := xSource.Commas;
    DataType       := xSource.DataType;
    DescendingSort := xSource.DescendingSort;
    DisplayWidth   := xSource.DisplayWidth;
    KeyField       := xSource.KeyField;
    KeyFieldName   := xSource.KeyFieldName;
    MassField      := xSource.MassField;
    RightAxis      := xSource.RightAxis;
    ShowInChart    := xSource.ShowInChart;
    ShowInTable    := xSource.ShowInTable;
    ShowSeconds    := xSource.ShowSeconds;
    TableIndex     := xSource.TableIndex;
    Thousands      := xSource.Thousands;
    UseMode        := xSource.UseMode;
    YScalingFactor := xSource.YScalingFactor;
  
    for i:=Low(TCalculationMethod) to High(TCalculationMethod) do begin
      CalcMethods[i] := xSource.CalcMethods[i];
    end;
  end;// if Source is TLabDataItem then begin
  //:...................................................................
  inherited Assign(Source);
end; // TLabDataItem.Assign

//:---------------------------------------------------------------------------
function TLabDataItem.CalcMethodCount: Integer;
var
  i: TCalculationMethod;
begin
  Result := 0;
  for i:=Low(TCalculationMethod) to High(TCalculationMethod) do begin
    if mCalcMethods[i] <> '' then
      inc(Result);
  end;
end; // TLabDataItem.CalcMethodCount

//:---------------------------------------------------------------------------
function TLabDataItem.GetCalcMethods(aIndex: TCalculationMethod): String;
begin
  Result := mCalcMethods[aIndex];
end; // TLabDataItem.GetCalcMethods

//:---------------------------------------------------------------------------
function TLabDataItem.GetCalcName: String;
begin
  Result := cPrefixCalc + FieldName;
end; // TLabDataItem.GetCalcName

//:---------------------------------------------------------------------------
function TLabDataItem.GetColName: String;
begin
  //if ClassTypeGroup = ctDataItem then
  if ClassTypeGroup in [ctDataItem, ctLabData] then
    Result := cPrefixCol + FieldName
  else
    Result := FieldName;
end; // TLabDataItem.GetColName

//:---------------------------------------------------------------------------
function TLabDataItem.GetConfigString: String;
var
  i: TCalculationMethod;
  xList: TmmStringList;
begin
  Result := '';
  with TIniStringList.Create do
  try
    Values['Alignment']       := Alignment;
    Values['AxisType']        := AxisType;
    Values['BorderCondition'] := BorderCondition;
    Values['ChartIndex']      := ChartIndex;
    Values['ChartType']       := ChartType;
    Values['Color']           := Color;
    Values['Commas']          := Commas;
    Values['DescendingSort']  := DescendingSort;
    Values['DisplayWidth']    := DisplayWidth;
    Values['RightAxis']       := RightAxis;
    Values['ShowInChart']     := ShowInChart;
    Values['ShowInTable']     := ShowInTable;
    Values['ShowSeconds']     := ShowSeconds;
    Values['TableIndex']      := TableIndex;
    Values['Thousands']       := Thousands;

    xList := TmmStringList.Create;
    try
      for i:=Low(TCalculationMethod) to High(TCalculationMethod) do
        xList.Add(mCalcMethods[i]);
      Values['CalcMethods'] := xList.CommaText;
    finally
      xList.Free;
    end;
    Result := inherited GetConfigString + ',' + CommaText;
  finally
    Free;
  end;// with TIniStringList do try
end; // TLabDataItem.GetConfigString

//:---------------------------------------------------------------------------
function TLabDataItem.GetKeyFieldID: String;
begin
  Result := '';
  if fKeyField then begin
    Result := StringReplace(fKeyFieldName, '_name', '', [rfReplaceAll]) + '_id';
  end;
end; // TLabDataItem.GetKeyFieldID

//:---------------------------------------------------------------------------
function TLabDataItem.GetMassFieldName: String;
begin
  Result := cMassFields[fMassField];
end; // TLabDataItem.GetMassFieldName

//:---------------------------------------------------------------------------
function TLabDataItem.GetSQLFromTable: String;
begin
  if ClassTypeGroup = ctDataItem then begin
    Result := '';
  end else
    Result := inherited GetSQLFromTable;
end; // TLabDataItem.GetSQLFromTable

//:---------------------------------------------------------------------------
function TLabDataItem.GetSQLSelectFields(aWithSum: boolean = true; aWithAlias:boolean = true): String;
begin
  if DataItemType.InheritsFrom(TDataItem) then begin
    // Je nachdem was gew�nscht ist, das komplete Statement zusammensetzen
    if UseMode <> umNever then begin
      if (DataMode = dmGrid) and KeyField and (fDataLevel <> -1) and (TableIndex >= fDataLevel) then
        // Wenn im Hierarchy Modus dann das Count-Feld f�r das Query angeben
        Result := Format(cSelectCountField, [ColName, FieldName])
      else begin
        // Datenfelder unabh�ngig ob Grid oder Chart behandeln
        if aWithSum and not KeyField then
          // zb: SUM(v.c_Len) as "c_Len"
          Result := Format('SUM(%s.%s)',[LinkAlias, ColName])
        else
          // zB: (v.c_style_name) as "c_style_name"
          Result := Format('(%s.%s)',[LinkAlias, ColName]);
  
        if aWithAlias then
          Result := Format('%s as "%s"',[Result, FieldName]);
      end;
    end else
      Result := '';
  end else
    Result := inherited GetSQLSelectFields(aWithSum, aWithAlias);
end; // TLabDataItem.GetSQLSelectFields

//:---------------------------------------------------------------------------
function TLabDataItem.GetSQLWhere: String;
begin
  if ClassTypeGroup = ctDataItem then begin
    Result := '';
  end else
    Result := inherited GetSQLWhere;
end; // TLabDataItem.GetSQLWhere

//:---------------------------------------------------------------------------
procedure TLabDataItem.SetCalcMethods(aIndex: TCalculationMethod; const Value: String);
begin
  mCalcMethods[aIndex] := Trim(Value);
end; // TLabDataItem.SetCalcMethods

//:---------------------------------------------------------------------------
procedure TLabDataItem.SetConfigString(Value: String);
var
  xList: TmmStringList;
  i: Integer;
  xMethod: TCalculationMethod;
  
  function GetCalcMethodType(aConfig: String): TCalculationMethod;
  begin
//    Result := TCalculationMethod(-1);
    with TmmStringList.Create do
    try
      CommaText := aConfig;
      Result := ValueDef(cMethodType, -1);
    finally
      Free;
    end;
  end;
  
begin
  with TIniStringList.Create do
  try
    CommaText       := Value;
    Alignment       := ValueDef('Alignment', 0);
    AxisType        := ValueDef('AxisType', 0);
    BorderCondition := ValueDef('BorderCondition', 1);
    ChartIndex      := ValueDef('ChartIndex', 0);
    ChartType       := ValueDef('ChartType', ctBar);
    Color           := ValueDef('Color', 0);
    Commas          := ValueDef('Commas', 0);
    DescendingSort  := ValueDef('DescendingSort', 0);
    DisplayWidth    := ValueDef('DisplayWidth', 0);
    RightAxis       := ValueDef('RightAxis', 0);
    ShowInChart     := ValueDef('ShowInChart', 0);
    ShowInTable     := ValueDef('ShowInTable', 0);
    ShowSeconds     := ValueDef('ShowSeconds', 0);
    TableIndex      := ValueDef('TableIndex', 0);
    Thousands       := ValueDef('Thousands', 0);

    xList := TmmStringList.Create;
    try
      xList.CommaText := ValueDef('CalcMethods', '');
      for i:=0 to xList.Count-1 do begin
        xMethod := GetCalcMethodType(xList[i]);
        if xMethod <> TCalculationMethod(-1) then
          mCalcMethods[xMethod] := xList[i];
      end;
    finally
      xList.Free;
    end;
  finally
    Free;
  end;// with TIniStringList do try
  
  inherited SetConfigString(Value);
end; // TLabDataItem.SetConfigString

//:---------------------------------------------------------------------------
//:--- Class: TLabDataItemList
//:---------------------------------------------------------------------------
constructor TLabDataItemList.Create(aPersistent: boolean = false);
begin
  inherited Create(aPersistent);
  //:...................................................................
  LevelInfo   := TmmStringList.Create;
  fDataLevel  := -1;
  fDataMode   := dmChart; // wird einfach mal auf dmChart initialisiert
  fLotDetails := False;
  fXAxis      := '';
  fZAxis      := '';
  
  // set default report options
  with fReportOptions do begin
    CompanyName       := Translate('(*)Firmenname');
    ReportTitle       := Translate('(*)Berichttitel');
    ShowExtend        := False;
    UseYMSettings     := False;
    ShowStyle         := True;
    ShowLot           := True;
    ShowMachine       := True;
    ShowYMSettings    := False;
    PrintChart        := True;
    PrintQMatrix      := True;
    PrintFFMatrix     := True;
    PrintCSChannel    := True;
    PrintCSSplice     := True;
    PrintCSCluster    := True;
    PrintCSFCluster   := True;
    PrintCSSFI        := True;
    PrintCSVCV        := True;
    PrintCSRepetition := True;
    PrintCSYarnCount  := True;
    PrintTable        := False;
    StretchTable      := True;
    NewPageMatrix     := False;
    NewPageYMSettings := False;
    NewPageTable      := False;
    RectChart         := Rect(-1, -1, -1, -1);
    RectQMatrix       := Rect(-1, -1, -1, -1);
    RectFFMatrix      := Rect(-1, -1, -1, -1);
    RectCSChannel     := Rect(-1, -1, -1, -1);   //Nue:19.11.07
    RectCSRepetition  := Rect(-1, -1, -1, -1);   //Nue:19.11.07
    RectCSSplice      := Rect(-1, -1, -1, -1);   //Nue:19.11.07
    RectCSFCluster    := Rect(-1, -1, -1, -1);   //Nue:19.11.07
    RectCSPSettings   := Rect(-1, -1, -1, -1);   //Nue:19.11.07
    RectCSCluster     := Rect(-1, -1, -1, -1);   //Nue:19.11.07
    RectCSSFI         := Rect(-1, -1, -1, -1);   //Nue:19.11.07
    RectCSVCV         := Rect(-1, -1, -1, -1);   //Nue:19.11.07
    RectCSYarnCount   := Rect(-1, -1, -1, -1);   //Nue:19.11.07
  end;
  // set default settings for table
  with fGridOptions do begin
    Options := [0,1,3];
  
    DataFont.Name     := 'MS Sans Serif'; //mDataGrid.Font.Name;
    DataFont.Color    := clWindowText; //mDataGrid.Font.Color;
    DataFont.Size     := 8; //mDataGrid.Font.Size;
    DataFont.Style    := []; //mDataGrid.Font.Style;
    DataFont.BkColor  := clWindow; //mDataGrid.Color;
  
    TitleFont.Name    := 'MS Sans Serif'; //mDataGrid.TitleFont.Name;
    TitleFont.Color   := clWindowText; //mDataGrid.TitleFont.Color;
    TitleFont.Size    := 8; //mDataGrid.TitleFont.Size;
    TitleFont.Style   := []; //mDataGrid.TitleFont.Style;
    TitleFont.BkColor := clBtnFace; //mDataGrid.FixedColor;
  end;
  
  // set default settings for chart
  with fChartOptions do begin
    AllowRotate         := False;
    BarStyle            := bsSide;
    CalcMethodsConfig   := '';
    EndColor            := clWhite;
    GradientDir         := 1;
    LegendAlign         := laTop;
    RotateX             := 350;
    RotateY             := 350;
    StartColor          := clWhite;
    UseCalcMethods      := False;
    View3D              := False;
    Width3D             := 25;
    Zoom                := 90;
  end;
  
  with fMatrixOptions do begin
    ChannelVisible := True;
    ClusterVisible := True;
    SpliceVisible  := False;   //Nue:20.01.08
    DefectValues   := True;
    CutValues      := True;
    ActiveVisible  := True;
    ActiveColor    := clAqua;
    MatrixMode     := 0; // default Coarse mode
  end;

  with fSpliceMatrixOptions do begin  //Nue:20.01.08
    ChannelVisible := False;
    ClusterVisible := False;
    SpliceVisible  := True;
    DefectValues   := True;
    CutValues      := True;
    ActiveVisible  := True;
    ActiveColor    := clFuchsia;
    MatrixMode     := 0; // default Coarse mode
  end;
end; // TLabDataItemList.Create

//:---------------------------------------------------------------------------
destructor TLabDataItemList.Destroy;
begin
  LevelInfo.Free;
  //:...................................................................
  inherited Destroy;
end; // TLabDataItemList.Destroy

//:---------------------------------------------------------------------------
procedure TLabDataItemList.Assign(aSource: TClassDefList);
var
  xSource: TLabDataItemList;
begin
  if Assigned(aSource) then begin
    Clear;
    // alle Elemente der Liste �bernehmen
    inherited Assign(aSource);
    if aSource is TLabDataItemList then begin
      xSource := TLabDataItemList(aSource);
  
      fXAxis         := xSource.XAxis;
      fZAxis         := xSource.ZAxis;
      fChartOptions  := xSource.ChartOptions^;
      fGridOptions   := xSource.GridOptions^;
      fMatrixOptions := xSource.MatrixOptions^;
      fSpliceMatrixOptions := xSource.SpliceMatrixOptions^;     //Nue:20.01.08
      fReportOptions := xSource.ReportOptions^;
      LevelInfo.Text := xSource.LevelInfo.Text;
      // Nun noch �berpr�fen, ob auch alle Items mit umAlways vorhanden sind
      InitDataItems(1);
    end;
  end;
  // if Assigned(aSource) and (aSource is TLabDataItemList) then begin
  //   Clear;
  //   xSource := TLabDataItemList(aSource);
  //
  //   fXAxis         := xSource.XAxis;
  //   fZAxis         := xSource.ZAxis;
  //   fChartOptions  := xSource.ChartOptions^;
  //   fGridOptions   := xSource.GridOptions^;
  //   fMatrixOptions := xSource.MatrixOptions^;
  //   fReportOptions := xSource.ReportOptions^;
  //   LevelInfo.Text := xSource.LevelInfo.Text;
  //   // Nun noch �berpr�fen, ob auch alle Items mit umAlways vorhanden sind
  //   InitDataItems(1);
  // end;
  // inherited Assign(aSource);
end; // TLabDataItemList.Assign

//:---------------------------------------------------------------------------
{* Pr�ft ob ob ein Grafikelement der linken oder rechten oberen Skala zugewiesen wurde, wenn nicht im 3D
Modus.
*}
function TLabDataItemList.CheckLeftRightTop(aAxisType: TAxisType): Boolean;
var
  i: Integer;
begin
  Result := False;
  if not fChartOptions.View3D then
    for i:=0 to DataItemCount-1 do begin
      with Items[i] do
        Result := ShowInChart and (AxisType = aAxisType);
      if Result then
        Break;
    end;
end; // TLabDataItemList.CheckLeftRightTop

//:---------------------------------------------------------------------------
procedure TLabDataItemList.Clear;
begin
  inherited Clear;
  //:...................................................................
  fDataLevel := -1;
end; // TLabDataItemList.Clear

//:---------------------------------------------------------------------------
function TLabDataItemList.DoSplitXAxis: Boolean;
begin
  Result := HasLeftTop and HasRightTop and fChartOptions.SplitXAxis;
end; // TLabDataItemList.DoSplitXAxis

//:---------------------------------------------------------------------------
function TLabDataItemList.GetAxisInfo(Index: TAxisType): PAxisInfoRec;
begin
  Result := @fChartOptions.AxisInfo[Index];
end; // TLabDataItemList.GetAxisInfo

//:---------------------------------------------------------------------------
{* Gibt einen kommagetrennter String mit den DisplayNames von den verwendeten Grafikelementen zur�ck.
*}
function TLabDataItemList.GetChartItemsCommaList: String;
var
  i: Integer;
  xStr: String;
begin
  with TStringList.Create do
  try
    for i:=0 to DataItemCount-1 do begin
      with Items[i] do begin
        if ShowInChart then begin
          xStr := Translate(DisplayName);
// wss: warum nicht mehr n�tig?
//          if IsMatrixDataItem then
//            xStr := Format('%s %s', [xStr, GetDefectTypeText(ClassDefects)]);

          if ChartIndex > Count-1 then Add(xStr)
                                  else Insert(ChartIndex, xStr);
        end;
      end;
    end; // for i
  finally
    Result := CommaText;
    Free;
  end;
end; // TLabDataItemList.GetChartItemsCommaList

//:---------------------------------------------------------------------------
function TLabDataItemList.GetChartOptions: PChartOptions;
begin
  Result := @fChartOptions;
end; // TLabDataItemList.GetChartOptions

//:---------------------------------------------------------------------------
{* Gibt ein Set von verwendeten ClassDefects f�r ein Matrixelement zur�ck. Wird ben�tigt, um im
SettingsDialog die Auswahl einschr�nken zu k�nnen, wenn z.B. schon ein "A1 cut" vorhanden ist.
*}
function TLabDataItemList.GetClassDefectsFromName(aItemName: string): TClassDefectsSet;
var
  i: Integer;
begin
  Result := [];
  for i:=0 to DataItemCount-1 do begin
    if AnsiSameText(aItemName, DataItems[i].ItemName) then begin
      if DataItems[i].IsMatrixDataItem then begin
        if ((fDataMode = dmChart) and Items[i].ShowInChart) or
           ((fDataMode = dmGrid) and Items[i].ShowInTable) then
          Include(result, DataItems[i].ClassDefects);
      end;
    end;// if AnsiSameText(aItemName, DataItems[i].ItemName) then begin
  end;// for i := 0 to DataItemCount do begin
end; // TLabDataItemList.GetClassDefectsFromName

//:---------------------------------------------------------------------------
function TLabDataItemList.GetGridOptions: PGridOptions;
begin
  Result := @fGridOptions;
end; // TLabDataItemList.GetGridOptions

//:---------------------------------------------------------------------------
function TLabDataItemList.GetItems(aIndex: Integer): TLabDataItem;
begin
  Result := TLabDataItem(DataItems[aIndex]);
end; // TLabDataItemList.GetItems

//:---------------------------------------------------------------------------
function TLabDataItemList.GetKeyFieldForLevel(aLevel: Integer): String;
var
  i: Integer;
begin
  Result := '';
  for i:=0 to DataItemCount-1 do begin
    with Items[i] do begin
      if KeyField and ShowInTable and (aLevel = TableIndex) then begin
        Result := KeyFieldName;
        break;
      end;
    end;
  end;
end; // TLabDataItemList.GetKeyFieldForLevel

//:---------------------------------------------------------------------------
function TLabDataItemList.GetKeyItemForLevel(aLevel: Integer): TLabDataItem;
var
  i: Integer;
begin
  Result := Nil;
  for i:=0 to DataItemCount-1 do begin
    with Items[i] do begin
      if KeyField and ShowInTable and (aLevel = TableIndex) then begin
        Result := Items[i];
        break;
      end;
    end;
  end;
end; // TLabDataItemList.GetKeyItemForLevel

//:---------------------------------------------------------------------------
function TLabDataItemList.GetLotDetails: Boolean;
begin
  Result := fLotDetails and LotTrendMode;
end; // TLabDataItemList.GetLotDetails

//:---------------------------------------------------------------------------
function TLabDataItemList.GetMatrixOptions: PMatrixOptions;
begin
  Result := @fMatrixOptions;
end; // TLabDataItemList.GetMatrixOptions

//:---------------------------------------------------------------------------
function TLabDataItemList.GetReportOptions: PReportOptions;
begin
  Result := @fReportOptions;
end; // TLabDataItemList.GetReportOptions

//:---------------------------------------------------------------------------
function TLabDataItemList.GetSpliceMatrixOptions: PMatrixOptions;      //Nue:20.01.08
begin
  Result := @fSpliceMatrixOptions;
end; // TLabDataItemList.GetSpliceMatrixOptions

//:---------------------------------------------------------------------------
function TLabDataItemList.GetSQLFromTables: String;
begin
  Result := inherited GetSQLFromTables;
  //:...................................................................
  if Result = '' then
    Result := cTimeModeTables[Queryparameters.TimeMode]
  else
    Result := cTimeModeTables[Queryparameters.TimeMode] + ',' + Result;
  
  CodeSite.SendString('DataItemFromTables', Result);
end; // TLabDataItemList.GetSQLFromTables

//:---------------------------------------------------------------------------
{* Gibt die DB-Felder der verwendeten Items zur�ck. Im Trendmode und wenn Partiedetail eingeschalten,
dann wird noch die ProdID hinzugef�gt.
*}
function TLabDataItemList.GetSQLSelectFields(aWithSum: boolean = true; aWithAlias: boolean = true):
        String;
begin
  Result := inherited GetSQLSelectFields(aWithSum, aWithAlias);
  //:...................................................................
  if fDataMode = dmChart then begin
    if ZAxisMode then begin
      if LotTrendMode and LotDetails then
        Result := LotDetailItem.GetSQLSelectFields + ',' + Result;
      Result := ZAxisItem.GetSQLSelectFields + ',' + Result;
    end;
    if XAxis <> '' then
      Result := XAxisItem.GetSQLSelectFields + ',' + Result;
  end;
end; // TLabDataItemList.GetSQLSelectFields

//:---------------------------------------------------------------------------
function TLabDataItemList.GetSQLWhere: String;
begin
  Result := inherited GetSQLWhere;
end; // TLabDataItemList.GetSQLWhere

//:---------------------------------------------------------------------------
function TLabDataItemList.GetXAxisItem: TLabDataItem;
begin
  if fXAxis <> '' then begin
    Result := TLabDataItem(gDataItemPool.DataItemByName[fXAxis]);
    Result.LinkAlias := LinkAlias;
  end else
    Result := Nil;
end; // TLabDataItemList.GetXAxisItem

//:---------------------------------------------------------------------------
function TLabDataItemList.GetZAxisItem: TLabDataItem;
begin
  if fZAxis <> '' then begin
    Result := TLabDataItem(gDataItemPool.DataItemByName[fZAxis]);
    Result.LinkAlias := LinkAlias;
  end else
    Result := Nil;
end; // TLabDataItemList.GetZAxisItem

//:---------------------------------------------------------------------------
function TLabDataItemList.GetZAxisMode: Boolean;
begin
  Result := (fZAxis <> '');
end; // TLabDataItemList.GetZAxisMode

//:---------------------------------------------------------------------------
function TLabDataItemList.HasLeftTop: Boolean;
begin
  Result := CheckLeftRightTop(atLeftTop);
end; // TLabDataItemList.HasLeftTop

//:---------------------------------------------------------------------------
function TLabDataItemList.HasRightTop: Boolean;
begin
  Result := CheckLeftRightTop(atRightTop);
end; // TLabDataItemList.HasRightTop

//:---------------------------------------------------------------------------
{* F�llt vordefinierte DataItems in die Liste ein. 0 = DataPool, 1 = ItemListe
*}
procedure TLabDataItemList.InitDataItems(aMode: Integer);
var
  xIndex: Integer;
  i: Integer;
begin
  // Vordefinierte DataItems von der Datenbank holen (ClassItems)
  if aMode = 0 then begin
    LoadFromDatabase(cLabItemClass);
    // Den Klassieritems eine default Farbe geben
    for i:=0 to DataItemCount-1 do begin
      xIndex := i mod (cColorRows * cColorCols); // auf max 16 Color Indexes Einschr�nken
      Items[i].Color := cMMProdGroupColors[(xIndex mod cColorRows)+1, (xIndex div cColorRows) + 1];
    end;
  end;
  
  // Nun noch die normalen DataItems f�r den LabReport hinzuf�gen
  for i:=0 to cDataItemCount-1 do begin
    if cDataItemArray[i].LR AND ((aMode = 0) OR ((aMode = 1) and (cDataItemArray[i].UseMode = umAlways))) then begin
        // Wenn dieses DataItem noch nicht vorhanden ist, dann noch hinzuf�gen
      xIndex := IndexOfItemName(cDataItemArray[i].Field);
      if xIndex = -1 then begin
        if AnsiSameText(cDataItemArray[i].Field, 'PEff') then
          xIndex := CreateAndAddNewDataItem(TDataItem, cLabItemPEffClass)
        else
          xIndex := CreateAndAddNewDataItem(TDataItem, cLabItemClass);
            // Eigenschaften des neuen Items setzen
        with Items[xIndex] do begin
          if cDataItemArray[i].ClassTypeGroup = ctLabData then
            ClassTypeGroup := ctLabData
          else
            ClassTypeGroup := ctDataItem;
          Description    := ''; //aDescription;
          OrderPos       := i;
          Predefined     := True;

          Alignment      := cDataItemArray[i].Alignment;
          CalcMode       := cDataItemArray[i].CalcMode;
          ChartIndex     := cDataItemArray[i].ChartIndex;
          Color          := cDataItemArray[i].Color;
          Commas         := cDataItemArray[i].Commas;
          DataType       := cDataItemArray[i].DataType;
          DescendingSort := cDataItemArray[i].DescendingSort;
          ItemName       := cDataItemArray[i].Field;
          KeyField       := cDataItemArray[i].KeyField;
          KeyFieldName   := cDataItemArray[i].KeyFieldName;
          MassField      := cDataItemArray[i].MassField;
          RightAxis      := cDataItemArray[i].RightAxis;
          ShowInChart    := cDataItemArray[i].ShowInChart;
          ShowInTable    := cDataItemArray[i].ShowInTable;
          ShowSeconds    := cDataItemArray[i].ShowSeconds;
          TableIndex     := cDataItemArray[i].TableIndex;
          Thousands      := cDataItemArray[i].Thousands;
          UseMode        := cDataItemArray[i].UseMode;
          // DisplayName am Schluss setzen, da sonst von ItemName �berschrieben
          DisplayName    := cDataItemArray[i].DisplayName;
        end; // with
      end; // if xIndex
    end; // if
  end; // for
end; // TLabDataItemList.InitDataItems

//:---------------------------------------------------------------------------
{* Wird f�r das Grid verwendet, wenn die Anzeige im Summen-Detail Modus ist.
*}
function TLabDataItemList.LevelCount: Integer;
var
  i: Integer;
begin
  Result := 0;
  for i:=0 to DataItemCount-1 do begin
    with Items[i] do begin
      if KeyField and ShowInTable then
        inc(Result);
    end;
  end;
end; // TLabDataItemList.LevelCount

//:---------------------------------------------------------------------------
function TLabDataItemList.LotDetailItem: TLabDataItem;
var
  xIndex: Integer;
begin
  Result := Nil;
  xIndex := gDataItemPool.IndexOfItemName('prod_id');
  if xIndex <> -1 then
    Result := gDataItemPool.Items[xIndex];
end; // TLabDataItemList.LotDetailItem

//:---------------------------------------------------------------------------
function TLabDataItemList.LotTrendMode: Boolean;
begin
  //Result := AnsiSameText(fXAxis, 'time') and AnsiSameText(fZAxis, 'prod_name') and (QueryParameters.TimeMode = tmShift);
  Result := TrendMode and AnsiSameText(fZAxis, 'prod_name') and (QueryParameters.TimeMode = tmShift);
end; // TLabDataItemList.LotTrendMode

//:---------------------------------------------------------------------------
{* Liest die Konfiguration und die n�tigen Datenelemente aus dem vorgegebenen Profil aus.
*}
procedure TLabDataItemList.ReadFromConfig(aIniFile: TCustomIniFile);
var
  i: Integer;
  xDataItemCount: Integer;
  xAxisType: TAxisType;
  xIndex: Integer;

  procedure DecodeAxis(aAxisString: String);
  begin
    with TIniStringList.Create do
    try
      CommaText := aAxisString;
      AxisInfo[xAxisType]^.Scaling := ValueDef('Scaling', asAutomatic);
      AxisInfo[xAxisType]^.Min     := ValueDef('Min', 0);
      AxisInfo[xAxisType]^.Max     := ValueDef('Max', 0);
    finally
      Free;
    end;
  end;
  
begin
  Clear;
  with aIniFile do begin
    with fReportOptions do begin
      CompanyName       := ReadString('ReportOptions', 'CompanyName',   CompanyName);
      ReportTitle       := ReadString('ReportOptions', 'ReportTitle',   ReportTitle);
      ShowExtend        := ReadBool  ('ReportOptions', 'ShowExtend',    ShowExtend);
      UseYMSettings     := ReadBool  ('ReportOptions', 'UseYMSettings', UseYMSettings);
      ShowStyle         := ReadBool  ('ReportOptions', 'ShowStyle',     ShowStyle);
      ShowLot           := ReadBool  ('ReportOptions', 'ShowLot',       ShowLot);
      ShowMachine       := ReadBool  ('ReportOptions', 'ShowMachine',   ShowMachine);
      ShowYMSettings    := ReadBool  ('ReportOptions', 'ShowYMSettings',   ShowYMSettings);
      PrintChart        := ReadBool  ('ReportOptions', 'PrintChart',    PrintChart);
      PrintQMatrix      := ReadBool  ('ReportOptions', 'PrintQMatrix',  PrintQMatrix);
      PrintCluster      := ReadBool  ('ReportOptions', 'PrintCluster',  PrintCluster);
      PrintSpliceMatrix := ReadBool  ('ReportOptions', 'PrintSpliceMatrix',  True);
      PrintFFMatrix     := ReadBool  ('ReportOptions', 'PrintFFMatrix', PrintFFMatrix);
      PrintBlackWhite   := ReadBool  ('ReportOptions', 'PrintBlackWhite', PrintBlackWhite);
      PrintCSChannel    := ReadBool  ('ReportOptions', 'PrintCSChannel', PrintCSChannel);
      PrintCSSplice     := ReadBool  ('ReportOptions', 'PrintCSSplice', PrintCSSplice);
      PrintCSCluster    := ReadBool  ('ReportOptions', 'PrintCSCluster', PrintCSCluster);
      PrintCSFCluster   := ReadBool  ('ReportOptions', 'PrintCSFCluster', PrintCSFCluster);
      PrintCSSFI        := ReadBool  ('ReportOptions', 'PrintCSSFI', PrintCSSFI);
      PrintCSVCV        := ReadBool  ('ReportOptions', 'PrintCSVCV', PrintCSVCV);
      PrintCSRepetition := ReadBool  ('ReportOptions', 'PrintCSRepetition', PrintCSRepetition);
      PrintCSYarnCount  := ReadBool  ('ReportOptions', 'PrintCSYarnCount', PrintCSYarnCount);

      PrintTable        := ReadBool  ('ReportOptions', 'PrintTable',    PrintTable);
      StretchTable      := ReadBool  ('ReportOptions', 'StretchTable',  StretchTable);
      NewPageMatrix     := ReadBool  ('ReportOptions', 'NewPageMatrix', NewPageMatrix);
      NewPageYMSettings := ReadBool  ('ReportOptions', 'NewPageYMSettings', NewPageYMSettings);
      NewPageTable      := ReadBool  ('ReportOptions', 'NewPageTable',  NewPageTable);
      RectChart         := StrToRect(ReadString('ReportOptions', 'RectChart',     '-1,-1,-1,-1'));
      RectQMatrix       := StrToRect(ReadString('ReportOptions', 'RectQMatrix',   '-1,-1,-1,-1'));
      RectSpliceMatrix  := StrToRect(ReadString('ReportOptions', 'RectSpliceMatrix', '-1,-1,-1,-1'));
      RectFFMatrix      := StrToRect(ReadString('ReportOptions', 'RectFFMatrix',  '-1,-1,-1,-1'));
      RectCSChannel     := StrToRect(ReadString('ReportOptions', 'RectCSChannel',  '-1,-1,-1,-1'));    //Nue:19.11.07
      RectCSRepetition  := StrToRect(ReadString('ReportOptions', 'RectCSRepetition',  '-1,-1,-1,-1')); //Nue:19.11.07
      RectCSSplice      := StrToRect(ReadString('ReportOptions', 'RectCSSplice',  '-1,-1,-1,-1'));     //Nue:19.11.07
      RectCSFCluster    := StrToRect(ReadString('ReportOptions', 'RectCSFCluster',  '-1,-1,-1,-1'));   //Nue:19.11.07
      RectCSPSettings   := StrToRect(ReadString('ReportOptions', 'RectCSPSettings',  '-1,-1,-1,-1'));  //Nue:19.11.07
      RectCSCluster     := StrToRect(ReadString('ReportOptions', 'RectCSCluster',  '-1,-1,-1,-1'));    //Nue:19.11.07
      RectCSSFI         := StrToRect(ReadString('ReportOptions', 'RectCSSFI',  '-1,-1,-1,-1'));        //Nue:19.11.07
      RectCSVCV         := StrToRect(ReadString('ReportOptions', 'RectCSVCV',  '-1,-1,-1,-1'));        //Nue:14.01.08
      RectCSYarnCount   := StrToRect(ReadString('ReportOptions', 'RectCSYarnCount',  '-1,-1,-1,-1'));  //Nue:19.11.07
    end;
  
    with fChartOptions do begin
      BarStyle       := TBarStyle(ReadInteger('ChartOptions', 'BarStyle', Ord(bsSide)));
      LegendAlign    := TLegendAlignment(ReadInteger('ChartOptions', 'LegendAlign', Ord(laTop)));
      View3D         := ReadBool('ChartOptions', 'View3D', False);
      AllowRotate    := ReadBool('ChartOptions', 'AllowRotate', False);
      Width3D        := ReadInteger('ChartOptions', 'Width3D', 25);
      RotateX        := ReadInteger('ChartOptions', 'RotateX', 350);
      RotateY        := ReadInteger('ChartOptions', 'RotateY', 350);
      StartColor     := ReadInteger('ChartOptions', 'StartColor', clWhite);
      EndColor       := ReadInteger('ChartOptions', 'EndColor', clWhite);
      ImageInside    := ReadBool('ChartOptions',    'ImageInside',  False);
      ImageFile      := ReadString('ChartOptions',  'ImageFile',  '');
      Zoom           := ReadInteger('ChartOptions', 'Zoom', 90);
      HPos           := ReadInteger('ChartOptions', 'HPos', 0);
      VPos           := ReadInteger('ChartOptions', 'VPos', 0);
      XLabelRotated  := ReadBool('ChartOptions', 'XLabelRotated', False);
      UseCalcMethods := ReadBool('ChartOptions', 'UseCalcMethods', False);
      CalcMethodsConfig := ReadString('ChartOptions',  'CalcMethodsConfig', '');
      SplitXAxis := ReadBool('ChartOptions', 'SplitXAxis', False);
  
      for xAxisType:=Low(TAxisType) to High(TAxisType) do begin
        DecodeAxis(ReadString('ChartOptions', 'AxisInfo' + IntToStr(ord(xAxisType)), ''));
      end;
    end;
    // read key axis values
    XAxis := ReadString('ChartOptions', 'XAxis', 'time');
    ZAxis := ReadString('ChartOptions', 'ZAxis', '');
  
    // read MatrixOptions
    with fMatrixOptions do begin
      ChannelVisible := ReadBool('MatrixOptions', 'ChannelVisible', ChannelVisible);
      ClusterVisible := ReadBool('MatrixOptions', 'ClusterVisible', ClusterVisible);
//      SpliceVisible  := ReadBool('MatrixOptions', 'SpliceVisible',  SpliceVisible);
      SpliceVisible  := ReadBool('MatrixOptions', 'SpliceVisible',  False);            //Nue:20.01.08
      DefectValues   := ReadBool('MatrixOptions', 'DefectValues',   DefectValues);
      CutValues      := ReadBool('MatrixOptions', 'CutValues',      CutValues);
      ActiveVisible  := ReadBool('MatrixOptions', 'ActiveVisible',  ActiveVisible);
      ActiveColor    := ReadInteger('MatrixOptions', 'ActiveColor', ActiveColor);
      InactiveColor  := ReadInteger('MatrixOptions', 'InactiveColor', $0090FFFF);
      MatrixMode     := ReadInteger('MatrixOptions', 'MatrixMode',  MatrixMode);
    end;

    // read SpliceMatrixOptions    //Nue:20.01.08
    with fSpliceMatrixOptions do begin
      ChannelVisible := ReadBool('SpliceMatrixOptions', 'ChannelVisible', False);
      ClusterVisible := ReadBool('SpliceMatrixOptions', 'ClusterVisible', False);
      SpliceVisible  := ReadBool('SpliceMatrixOptions', 'SpliceVisible',  SpliceVisible);
      DefectValues   := ReadBool('SpliceMatrixOptions', 'DefectValues',   DefectValues);
      CutValues      := ReadBool('SpliceMatrixOptions', 'CutValues',      CutValues);
      ActiveVisible  := ReadBool('SpliceMatrixOptions', 'ActiveVisible',  ActiveVisible);
      ActiveColor    := ReadInteger('SpliceMatrixOptions', 'ActiveColor', ActiveColor);
      InactiveColor  := ReadInteger('SpliceMatrixOptions', 'InactiveColor', $0090FFFF);
      MatrixMode     := ReadInteger('SpliceMatrixOptions', 'MatrixMode',  MatrixMode);
    end;

    // read GridOptions
    with fGridOptions do begin
      i := ReadInteger('GridOptions', 'Options', 0);
      if i = 0 then
        Options := [0,1,3]
      else
        ConvertIntToSet(i, @Options, SizeOf(Options));
      FixedCols := ReadInteger('GridOptions', 'FixedCols', FixedCols);
      Hierarchy := ReadBool('GridOptions', 'Hierarchy', False);
      with TitleFont do begin
        Name    := ReadString('GridOptions', 'TitleFontName', 'MS Sans Serif');
        Color   := ReadInteger('GridOptions', 'TitleFontColor', Color);
        BkColor := ReadInteger('GridOptions', 'TitleFontBkColor', BkColor);
        Size    := ReadInteger('GridOptions', 'TitleFontSize', Size);
        ConvertIntToSet(ReadInteger('GridOptions', 'TitleFontStyle', 0), @Style, SizeOf(Style));
      end;
      with DataFont do begin
        Name    := ReadString( 'GridOptions', 'DataFontName', 'MS Sans Serif');
        Color   := ReadInteger('GridOptions', 'DataFontColor', Color);
        BkColor := ReadInteger('GridOptions', 'DataFontBkColor', BkColor);
        Size    := ReadInteger('GridOptions', 'DataFontSize', Size);
        ConvertIntToSet(ReadInteger('GridOptions', 'DataFontStyle', 0), @Style, SizeOf(Style));
      end;
    end;
  
    // read data items settings
    xDataItemCount := ReadInteger('DataItems', 'DataItemsCount', 0);
    if xDataItemCount = 0 then begin
      // Add default item for chart
      AddClassFromConfigString(cDefaultItem1, gDataItemPool);
      AddClassFromConfigString(cDefaultItem2, gDataItemPool);
    end
    else begin
      for i:=0 to xDataItemCount-1 do begin
        AddClassFromConfigString(ReadString('DataItems', 'Item' + IntToStr(i), ''), gDataItemPool);
      end;
    end; // if xDataItemCount
  
    // Nun noch �berpr�fen, ob auch alle Items mit umAlways vorhanden sind
    InitDataItems(1);
  end; // with aIniFile
end; // TLabDataItemList.ReadFromConfig

//:---------------------------------------------------------------------------
procedure TLabDataItemList.SetDataLevel(Value: Integer);
var
  xDataLevel: Integer;
  i: Integer;
begin
  fDataLevel := Value;
  if fGridOptions.Hierarchy then xDataLevel := fDataLevel
                            else xDataLevel := -1;
  for i:=0 to DataItemCount-1 do
    Items[i].DataLevel := xDataLevel;
end; // TLabDataItemList.SetDataLevel

//:---------------------------------------------------------------------------
procedure TLabDataItemList.SetDataMode(Value: TDataMode);
var
  i: Integer;
begin
  fDataMode := Value;
  for i:=0 to DataItemCount-1 do
    Items[i].DataMode := fDataMode;
end; // TLabDataItemList.SetDataMode

//:---------------------------------------------------------------------------
procedure TLabDataItemList.SetXAxis(const Value: String);
begin
  if fXAxis <> Value then begin
    // if previous selection was valid remove flag
    if gDataItemPool.IndexOfItemName(Value) <> -1 then
      fXAxis := Value
    else
      fXAxis := '';
  end;
end; // TLabDataItemList.SetXAxis

//:---------------------------------------------------------------------------
procedure TLabDataItemList.SetZAxis(const Value: String);
begin
  if fZAxis <> Value then begin
    // if previous selection was valid remove flag
    if gDataItemPool.IndexOfItemName(Value) <> -1 then
      fZAxis := Value
    else
      fZAxis := '';
  end;
end; // TLabDataItemList.SetZAxis

//:---------------------------------------------------------------------------
function TLabDataItemList.TrendMode: Boolean;
begin
  Result := False;
  if Assigned(XAxisItem) then
    Result := (XAxisItem.DataType = ftDateTime)
end; // TLabDataItemList.TrendMode

//:---------------------------------------------------------------------------
{* Speichert die gesamte Konfiguration in das Profil.
*}
procedure TLabDataItemList.WriteToConfig(aIniFile: TCustomIniFile);
var
  i: Integer;
  xAxis: TAxisType;
  
  function EncodeAxis(aAxisInfo: TAxisInfoRec): string;
  begin
    Result := '';
    with TIniStringList.Create do
    try
      with aAxisInfo do begin
        Values['Scaling'] := Scaling;
        Values['Min']     := Min;
        Values['Max']     := Max;
      end;
      Result := CommaText;
    finally
      Free;
    end;
  end;
  
begin
  with aIniFile do begin
    with fReportOptions do begin
      WriteString('ReportOptions', 'CompanyName',   CompanyName);
      WriteString('ReportOptions', 'ReportTitle',   ReportTitle);
      WriteBool  ('ReportOptions', 'ShowExtend',    ShowExtend);
      WriteBool  ('ReportOptions', 'UseYMSettings', UseYMSettings);
      WriteBool  ('ReportOptions', 'ShowStyle',     ShowStyle);
      WriteBool  ('ReportOptions', 'ShowLot',       ShowLot);
      WriteBool  ('ReportOptions', 'ShowMachine',   ShowMachine);
      WriteBool  ('ReportOptions', 'ShowYMSettings', ShowYMSettings);
      WriteBool  ('ReportOptions', 'PrintChart',    PrintChart);
      WriteBool  ('ReportOptions', 'PrintQMatrix',  PrintQMatrix);
      WriteBool  ('ReportOptions', 'PrintCluster',  PrintCluster);
      WriteBool  ('ReportOptions', 'PrintSpliceMatrix', PrintSpliceMatrix);
      WriteBool  ('ReportOptions', 'PrintFFMatrix', PrintFFMatrix);
      WriteBool  ('ReportOptions', 'PrintBlackWhite', PrintBlackWhite);
      WriteBool  ('ReportOptions', 'PrintCSChannel', PrintCSChannel);
      WriteBool  ('ReportOptions', 'PrintCSSplice',  PrintCSSplice);
      WriteBool  ('ReportOptions', 'PrintCSCluster', PrintCSCluster);
      WriteBool  ('ReportOptions', 'PrintCSFCluster', PrintCSFCluster);
      WriteBool  ('ReportOptions', 'PrintCSSFI',      PrintCSSFI);
      WriteBool  ('ReportOptions', 'PrintCSVCV',      PrintCSVCV);
      WriteBool  ('ReportOptions', 'PrintCSRepetition', PrintCSRepetition);
      WriteBool  ('ReportOptions', 'PrintCSYarnCount', PrintCSYarnCount);

      WriteBool  ('ReportOptions', 'PrintTable',    PrintTable);
      WriteBool  ('ReportOptions', 'StretchTable',    StretchTable);
      WriteBool  ('ReportOptions', 'NewPageMatrix', NewPageMatrix);
      WriteBool  ('ReportOptions', 'NewPageYMSettings', NewPageYMSettings);
      WriteBool  ('ReportOptions', 'NewPageTable',  NewPageTable);
      WriteString('ReportOptions', 'RectChart',     RectToStr(RectChart));
      WriteString('ReportOptions', 'RectQMatrix',   RectToStr(RectQMatrix));
      WriteString('ReportOptions', 'RectSpliceMatrix', RectToStr(RectSpliceMatrix));
      WriteString('ReportOptions', 'RectFFMatrix',  RectToStr(RectFFMatrix));
      WriteString('ReportOptions', 'RectCSChannel',  RectToStr(RectCSChannel));
      WriteString('ReportOptions', 'RectCSRepetition',  RectToStr(RectCSRepetition));
      WriteString('ReportOptions', 'RectCSSplice',  RectToStr(RectCSSplice));
      WriteString('ReportOptions', 'RectCSFCluster',  RectToStr(RectCSFCluster));
      WriteString('ReportOptions', 'RectCSPSettings',  RectToStr(RectCSPSettings));
      WriteString('ReportOptions', 'RectCSCluster',  RectToStr(RectCSCluster));
      WriteString('ReportOptions', 'RectCSSFI',  RectToStr(RectCSSFI));
      WriteString('ReportOptions', 'RectCSVCV',  RectToStr(RectCSVCV));
      WriteString('ReportOptions', 'RectCSYarnCount',  RectToStr(RectCSYarnCount));
    end;
  
    // write ChartOptions
    with fChartOptions do begin
      WriteInteger('ChartOptions', 'BarStyle', Ord(BarStyle));
      WriteInteger('ChartOptions', 'LegendAlign', Ord(LegendAlign));
      WriteBool('ChartOptions', 'View3D', View3D);
      WriteBool('ChartOptions', 'AllowRotate', AllowRotate);
      WriteInteger('ChartOptions', 'Width3D', Width3D);
      WriteInteger('ChartOptions', 'RotateX', RotateX);
      WriteInteger('ChartOptions', 'RotateY', RotateY);
      WriteInteger('ChartOptions', 'StartColor', StartColor);
      WriteInteger('ChartOptions', 'EndColor', EndColor);
      WriteBool('ChartOptions',    'ImageInside',  ImageInside);
      WriteString('ChartOptions',  'ImageFile',  ImageFile);
      WriteInteger('ChartOptions', 'Zoom', Zoom);
      WriteInteger('ChartOptions', 'HPos', HPos);
      WriteInteger('ChartOptions', 'VPos', VPos);
      WriteBool('ChartOptions', 'XLabelRotated', XLabelRotated);
      WriteBool('ChartOptions', 'UseCalcMethods', UseCalcMethods);
      WriteString('ChartOptions',  'CalcMethodsConfig',  CalcMethodsConfig);
      WriteBool('ChartOptions', 'SplitXAxis', SplitXAxis);
  
      for xAxis:=Low(TAxisType) to High(TAxisType) do begin
        WriteString('ChartOptions', 'AxisInfo' + IntToStr(ord(xAxis)), EncodeAxis(AxisInfo[xAxis]));
      end;
    end;
    // write key axis values
    WriteString('ChartOptions', 'XAxis', fXAxis);
    WriteString('ChartOptions', 'ZAxis', fZAxis);
  
      // write MatrixOptions
    with fMatrixOptions do begin
      WriteBool('MatrixOptions', 'ChannelVisible', ChannelVisible);
      WriteBool('MatrixOptions', 'ClusterVisible', ClusterVisible);
//      WriteBool('MatrixOptions', 'SpliceVisible',  SpliceVisible);
      WriteBool('MatrixOptions', 'SpliceVisible',  False);     //Nue:20.01.08
      WriteBool('MatrixOptions', 'DefectValues', DefectValues);
      WriteBool('MatrixOptions', 'CutValues',    CutValues);
      WriteBool('MatrixOptions', 'ActiveVisible',  ActiveVisible);
      WriteInteger('MatrixOptions', 'ActiveColor',  ActiveColor);
      WriteInteger('MatrixOptions', 'InactiveColor', InactiveColor);
      WriteInteger('MatrixOptions', 'MatrixMode',  MatrixMode);
    end;

      // write SpliceMatrixOptions   //Nue:20.01.08
    with fSpliceMatrixOptions do begin
      WriteBool('SpliceMatrixOptions', 'ChannelVisible', ChannelVisible);
      WriteBool('SpliceMatrixOptions', 'ClusterVisible', ClusterVisible);
      WriteBool('SpliceMatrixOptions', 'SpliceVisible',  SpliceVisible);
      WriteBool('SpliceMatrixOptions', 'DefectValues', DefectValues);
      WriteBool('SpliceMatrixOptions', 'CutValues',    CutValues);
      WriteBool('SpliceMatrixOptions', 'ActiveVisible',  ActiveVisible);
      WriteInteger('SpliceMatrixOptions', 'ActiveColor',  ActiveColor);
      WriteInteger('SpliceMatrixOptions', 'InactiveColor', InactiveColor);
      WriteInteger('SpliceMatrixOptions', 'MatrixMode',  MatrixMode);
    end;

      // write GridOptions
    with fGridOptions do begin
      WriteInteger('GridOptions', 'Options', ConvertSetToInt(@Options, SizeOf(Options)));
      WriteInteger('GridOptions', 'FixedCols', fGridOptions.FixedCols);
      WriteBool('GridOptions', 'Hierarchy', Hierarchy);
      with TitleFont do begin
        WriteString('GridOptions', 'TitleFontName', Name);
        WriteInteger('GridOptions', 'TitleFontColor', Color);
        WriteInteger('GridOptions', 'TitleFontBkColor', BkColor);
        WriteInteger('GridOptions', 'TitleFontSize', Size);
        WriteInteger('GridOptions', 'TitleFontStyle', ConvertSetToInt(@Style, SizeOf(Style)));
      end;
      with DataFont do begin
        WriteString( 'GridOptions', 'DataFontName', Name);
        WriteInteger('GridOptions', 'DataFontColor', Color);
        WriteInteger('GridOptions', 'DataFontBkColor', BkColor);
        WriteInteger('GridOptions', 'DataFontSize', Size);
        WriteInteger('GridOptions', 'DataFontStyle', ConvertSetToInt(@Style, SizeOf(Style)));
      end;
    end;
  
    // write data items settings
    WriteInteger('DataItems', 'DataItemsCount', DataItemCount);
    for i:=0 to DataItemCount-1 do begin
      WriteString('DataItems', 'Item' + IntToStr(i), Items[i].ConfigString);
    end;
  
  end;
end; // TLabDataItemList.WriteToConfig

//:---------------------------------------------------------------------------
//:--- Class: TdmLabReport
//:---------------------------------------------------------------------------
constructor TdmLabReport.Create(aOwner: TComponent; aDataItemList: TLabDataItemList);
begin
  inherited Create(aOwner);
  fResultQuery  := Nil;
  fDBParams     := TQueryParameters.Create;
  mDataItemList := aDataItemList;
  // �ber diesen Event wird definiert, welche Felder im Chart oder Grid Query erscheinen werden
  // Dazu wird ein Flag verwendet wo vorher gesetzt wird, ob das Chart oder das Grid behandelt wird.
  mDataItemList.OnGetDataItemGenerateSQL := DoGetDataItemGenerateSQL;
  mFrom         := TStringList.Create;
  mSelect       := TStringList.Create;
  mWhere        := TStringList.Create;
end; // TdmLabReport.Create

//:---------------------------------------------------------------------------
destructor TdmLabReport.Destroy;
begin
  FreeAndNil(fDBParams);
  FreeAndNil(mFrom);
  FreeAndNil(mSelect);
  FreeAndNil(mWhere);
  //:...................................................................
  inherited Destroy;
end; // TdmLabReport.Destroy

//:---------------------------------------------------------------------------
{* Kreiert die entsprechenden Feldobjekte (c_XXX und calc_XXX), sortiert die Felder gem�ss Konfiguration
und erstellt dynamisch das Datenquery. Anschliessend wird das Datenset ge�ffnet.
*}
function TdmLabReport.CreateAllQueryFields(aDataMode: TDataMode; aLevel: Integer = 0; aAddWhere: String
        = ''): Boolean;
var
  xGroupList: TmmStringList;
  xOrderList: TmmStringList;
  xCountKeyField: Boolean;
  i: Integer;
  xTag: Integer;
  xItemValid: Boolean;

  function CreateNewField(aItem: TLabDataItem; aKind: TFieldKind): TField;
  var
    xStr: String;
    xSortTag: Integer;
  begin
      // create field ...
    if xCountKeyField then
      Result := TStringField.Create(Self)
    else begin
      case aItem.DataType of
        ftDateTime: Result := TDateTimeField.Create(Self);
        ftString:   Result := TStringField.Create(Self);
        ftInteger:  Result := TIntegerField.Create(Self);
      else  // ftFloat
        Result := TFloatField.Create(Self);
      end;
    end;
  
    if Result is TStringField then
      with TStringField(Result) do
        Size := 70;
  
      // and set some properties
    with Result do begin
      FieldKind := aKind;
      Tag       := -1;
      Visible   := (aKind = fkCalculated); // or aItem.KeyField;
  
      if mDataItemList.DataMode = dmGrid then xSortTag := aItem.TableIndex
                                         else xSortTag := aItem.ChartIndex;
      // Feldname mit c_ oder calc_ setzen
      if (aKind = fkData) then begin
        FieldName := aItem.FieldName;
  //      FieldName := aItem.ColName;
        // Create dynamically the key fields for the query
        if aItem.KeyField then begin
          // bei hierarchischer Tabelle sind z.T. Schl�sselfelder nur die Anzahl sichtbar
          // und darf entsprechend nicht danach gruppiert oder sortiert werden
          if not xCountKeyField then begin
            xStr := aItem.ColName;
            // bei KeyField Details muss noch das ID-Field in der Group-Anweisung auch noch hinzugef�gt werden
            xGroupList.AddObject(xStr, Pointer(xSortTag));
            if aItem.DescendingSort then
              xOrderList.AddObject(xStr + ' DESC', Pointer(xSortTag))
            else
              xOrderList.AddObject(xStr + ' ASC', Pointer(xSortTag));
          end;
        end;
      end else
        FieldName := aItem.CalcName;
  
      Name := FResultQuery.Name + Format('_Field_%d', [FResultQuery.Fields.Count]);
  
      if Visible then begin
        // In Tag wird der Pointer zum DataItem Objekt abgelegt
        Tag := Integer(aItem);
        Alignment    := aItem.Alignment;
        DisplayLabel := Translate(aItem.DisplayName);
        DisplayWidth := Round(aItem.DisplayWidth / cAverageCharWidth);
  
          // if is visible and a nummeric field then set formating string
        if Result is TNumericField then begin
          with TNumericField(Result) do begin
            DisplayFormat := cNumberFormat[aItem.Commas];
            if aItem.Thousands then
              DisplayFormat := ',' + DisplayFormat;
          end;
        end;
      end else begin
        DisplayLabel := FieldName;
        xSortTag     := -1;
      end; // if Visible
        // now assign the field to its dataset
      DataSet := FResultQuery;
      if xSortTag <> -1 then
        Index := 0;
    end; // with Result
  end;
          //.......................................................................
  
begin
  Result := False;
  mDataItemList.DataMode  := aDataMode;
  mDataItemList.DataLevel := aLevel;
  case mDataItemList.DataMode of
    dmChart: FResultQuery := dseChart;
    dmGrid:  if aLevel = 0 then FResultQuery := dseGrid
                           else FResultQuery := dseGridDetail;
  end;// case aQueryType of
  
  EnterMethod('CreateAllQueryFields: ' + FResultQuery.Name);
  xGroupList := TmmStringList.Create;
  xOrderList := TmmStringList.Create;
  
  // alle Feldobjekte l�schen
  with FResultQuery do begin
    Close;
    while FieldCount > 0 do
      Fields[0].Free;
  end;
  
  // create field from selected item list
  with mDataItemList do begin
    // F�r die Grafik m�ssen die DataItems f�r die X- und Z-Achse sonderbehandelt werden
    // da diese nicht �ber das Property ShowInChart aktiviert werden
    if mDataItemList.DataMode = dmChart then begin
      if XAxis <> '' then begin
        CreateNewField(XAxisItem, fkData);
        CreateNewField(XAxisItem, fkCalculated);
      end;
      if ZAxis <> '' then begin
        CreateNewField(ZAxisItem, fkData);
        CreateNewField(ZAxisItem, fkCalculated);
        if LotTrendMode and LotDetails then
          CreateNewField(mDataItemList.LotDetailItem, fkData);
      end;
    end;
    // create dynamic needed key fields and set associated properties in calc_XXX fields
    for i:=0 to DataItemCount-1 do begin
      with Items[i] do begin
        xItemValid := (ShowInTable and (mDataItemList.DataMode = dmGrid)) or
                      (ShowInChart and (mDataItemList.DataMode = dmChart));
  
        if (UseMode = umAlways) or xItemValid then begin
          xCountKeyField := GridOptions^.Hierarchy and KeyField and
                            (mDataItemList.DataMode = dmGrid) and (TableIndex >= aLevel);
          // FIRST: ein Feld mit direktem Link auf die DB erstellen
          // z.B. MEff hat den Status umNever, da dies kein direktes Feld auf der DB hat und
          // demzufolge kein c_ Feld ben�tigt
          if UseMode <> umNever then
            CreateNewField(Items[i], fkData);
  
          // SECOND: nun noch f�r die gew�hlten Elemente ein calc_ Feld erstellen
          if xItemValid then
            CreateNewField(Items[i], fkCalculated)
        end; // if
      end; // with Items
    end; // for i
  end; // with mDataItemList

  // -------------------
  // now reordering the fields according to the Tag value
  // -------------------
  with FResultQuery do begin
  {
      CodeSite.EnterMethod('Debug FResultQuery vor sortieren');
      for i:=0 to FieldCount-1 do begin
        with Fields[i] do begin
        //        if Visible then
            CodeSite.SendFmtMsg('%s: Index=%d, Tag=%d', [FieldName, Index, Tag]);
        end;
      end;
      CodeSite.ExitMethod('');
  {}
    i := 0;
    while i < FieldCount do begin
      with Fields[i] do begin
        if (Tag <> -1) and (Tag <> 0) then begin
          if mDataItemList.DataMode = dmGrid then xTag := TLabDataItem(Tag).TableIndex
                                             else xTag := TLabDataItem(Tag).ChartIndex;
        end else
          xTag := -1;
  
        if (xTag = -1) or (xTag = i) then
          inc(i)
        else if xTag < i then begin
          Index := xTag;
          inc(i);
        end else if xTag > i then
          Index := xTag;
      end;
    end;
  {
      CodeSite.EnterMethod('Debug FResultQuery nach sortieren');
      for i:=0 to FieldCount-1 do begin
        with Fields[i] do begin
    //        if Visible then
            CodeSite.SendFmtMsg('%s: Index=%d, Tag=%d', [FieldName, Index, Tag]);
        end;
      end;
  {}
  end;
  
  xGroupList.CustomSort(KeyStrSortProc);
  xOrderList.CustomSort(KeyStrSortProc);
  
          // assign query string and open query object
  with FResultQuery do
  try
    CommandText := Format(cBasicQuery[fDBParams.TimeMode], ['', mDataItemList.GetSQLSelectFields, mDataItemList.GetSQLFromTables, fDefaultShiftCal, GetWhereStr(aAddWhere, True)]);
    if xGroupList.Count > 0 then
      CommandText := CommandText + Format(' Group by %s', [StringReplace(xGroupList.CommaText, '"', '', [rfReplaceAll])]);
    if xOrderList.Count > 0 then
      CommandText := CommandText + Format(' Order by %s', [StringReplace(xOrderList.CommaText, '"', '', [rfReplaceAll])]);
  
    CodeSite.SendString('CommandText', CommandText);
  
    Open;
    Result := Active;
  except
    on e:Exception do
      CodeSite.SendError('FResultQuery open failed: ' + e.Message);
  end;
  
  xGroupList.Free;
  xOrderList.Free;
end; // TdmLabReport.CreateAllQueryFields

//:---------------------------------------------------------------------------
{* calc_XXX Felder werden nach dem �ffnen des Datenquery berechnet. Die Berechung h�ngt vom CalcMode von
dem verwendeten Datenelement ab.
*}
procedure TdmLabReport.DataSetCalcFields(DataSet: TDataSet);
var
  xLen: Single;
  xDoCalc: Boolean;
  i: Integer;
  xInt: Integer;
  xItem: TLabDataItem;
begin
  with DataSet do begin
    // first get the c_len value and check if it is valid
    xLen    := FieldByName('Len').AsFloat;
    xDoCalc := (xLen >= 1.0);
    for i:=0 to FieldList.Count-1 do begin
      if Fields[i].Visible then begin
        xItem := TLabDataItem(Fields[i].Tag);
        case xItem.CalcMode of
          -1: with FieldByName(xItem.CalcName) do begin
                Value := FieldByName(xItem.FieldName).Value;
              end;
          // calculate SFI/D value
          -2: with FieldByName('calc_SFI_D') do begin
                Value := CalcSFI_D(FieldByName('SFI').AsFloat, FieldByName('SFICnt').AsFloat);
              end;
          // calculate SFI value
          -3: begin
              xInt := FieldByName('AdjustBaseCnt').AsInteger;
              with FieldByName('calc_SFI') do begin
                if xInt > 0 then
                  Value := CalcSFI(FieldByName('SFI').AsFloat, FieldByName('SFICnt').AsFloat, FieldByName('AdjustBase').AsFloat / xInt)
                else
                  Value := 0.0;
              end;
            end;
          // ProdNutzeffekt
//          -4: with FieldByName('calc_PEff') do begin
//                if FieldByName('wtSpd').AsFloat > 0.0 then
//                  Value := (FieldByName('rtSpd').AsFloat / FieldByName('wtSpd').AsFloat) * 100
//                else
//                  Value := 0.0;
//              end;
          // calculate CVd value   Nue:12.02.08
          -6: with FieldByName('calc_CVd') do begin
                Value := cVCVFactor * CalcSFI_D(FieldByName('c_SFI').AsFloat, FieldByName('c_SFICnt').AsFloat);
              end;
        else
          // distinguish mode for calculation in CalcValue from CalcMode value
          // CalcMode = 0: general data items has to be calcualted to LenBase
          // CalcMode > 0: calculations for Imperfection at fixed meter
          try
            with FieldByName(xItem.CalcName) do begin
              if xDoCalc then
                Value := CalcValue(fLenBase, FieldByName(xItem.FieldName).AsFloat, xLen, xItem.CalcMode)
              else
                Value := 0.0;
            end;
          except
            on e:Exception do begin
              CodeSite.SendError('DataSetCalcFields() ' + e.Message);
              break;
            end;//
          end;// try except
        end; // case
      end;
    end;
  end; // with
end; // TdmLabReport.DataSetCalcFields

//:---------------------------------------------------------------------------
{* Damit der Datenelementcontainer weiss, welche Elemente er f�r das erstellen des Query verwenden darf
wird diese Eventmethode zur Laufzeit dem Containerobjekt zugewiesen. Nur die eigentliche Applikation (in
diesem Fall der LabReport) weiss, welche Elemente zu welchem Zeitpunkt (Grid oder Chart Modus) verwendet
werden.
*}
procedure TdmLabReport.DoGetDataItemGenerateSQL(Sender: TObject; aDataItem: TBaseDataItem; var aEnabled:
        boolean);
var
  xItem: TLabDataItem;
begin
  xItem := TLabDataItem(aDataItem);
  // Enabled = True wenn
  // alle ODER KlassierItems ODER (nicht Never UND verwendet in (Chart ODER Grid))
  aEnabled := (xItem.UseMode = umAlways) OR (xItem.ClassTypeGroup in [ctLongThickThin, ctSplice, ctFF]) OR
              (
                (xItem.UseMode <> umNever)
                AND
                ( ((mDataItemList.DataMode = dmChart) and xItem.ShowInChart) OR
                  ((mDataItemList.DataMode = dmGrid)  and xItem.ShowInTable) )
              );
end; // TdmLabReport.DoGetDataItemGenerateSQL

//:---------------------------------------------------------------------------
{* Im Shift und Longterm Modus wird die Zeiteinschr�nkung per Zeitbereich definiert. Diese Parameter
m�ssen vor dem �ffnen noch gesetzt werden.
*}
procedure TdmLabReport.dseBeforeOpen(DataSet: TDataSet);
var
  xParam: TParameter;
begin
  //  CodeSite.SendMsg('dseBeforOpen');
  with TmmADODataSet(DataSet) do begin
    xParam := Parameters.FindParam('TimeFrom');
    if xParam <> nil then begin
      xParam.DataType := ftDateTime;
      xParam.Value    := fDBParams.TimeFrom;
    end;
    xParam := Parameters.FindParam('TimeTo');
    if xParam <> nil then begin
      xParam.DataType := ftDateTime;
      xParam.Value    := fDBParams.TimeTo;
    end;
  end;
end; // TdmLabReport.dseBeforeOpen

//:---------------------------------------------------------------------------
function TdmLabReport.GetDetailKeyItems(aKeyValue: TSourceSelection; aAddWhere: String; var aValues:
        TStringList): Boolean;
var
  xFieldName: String;
begin
  Result := False;
  aValues.Clear;
  EnterMethod('GetDetailKeyItems');
  xFieldName := 'c_' + cDetailKeyItemFields[aKeyValue];
  with dseWork do
  try
    Close;
    CommandText := Format(cBasicQuery[fDBParams.TimeMode],
      ['distinct', xFieldName, mDataItemList.GetSQLFromTables, fDefaultShiftCal,
       GetWhereStr(aAddWhere, True)]);
  
    CodeSite.SendString('CommandText', CommandText);
  
    Open;
    while not EOF do begin
      aValues.Add(FieldByName(xFieldName).AsString);
      Next;
    end;
    aValues.Sort;
    Result := True;
  except
    on e:Exception do begin
      CodeSite.SendError('GetDetailKeyItems.dseWork open failed: ' + e.Message);
    end;
  end;
end; // TdmLabReport.GetDetailKeyItems

//:---------------------------------------------------------------------------
function TdmLabReport.GetWhereStr(aAddWhere: String; aWithContainerWhere: Boolean): String;
begin
  with TStringList.Create do
  try
    if Trim(fDBParams.StyleIDs) <> '' then
      Add(Format(cWhereStyleId_lr, [fDBParams.StyleIDs]));
  
    if Trim(fDBParams.ProdIDs) <> '' then
      Add(Format(cWhereLotId_lr, [fDBParams.ProdIDs]));
  
    if Trim(fDBParams.MachIDs) <> '' then
      Add(Format(cWhereMachId_lr, [fDBParams.MachIDs]));
  
    case fDBParams.TimeMode of
      tmInterval: begin
          if Trim(fDBParams.TimeIDs) <> '' then
            Add(Format(cWhereTimeID, [fDBParams.TimeIDs]));
        end;
      tmShift: begin
          Add(cWhereShiftRange);
        end;
      tmLongtermWeek: begin
          Add(cWhereWeekRange);
        end;
    else
    end;
  
    if Trim(fDBParams.YMSetIDs) <> '' then
      Add(Format(cWhereYMSetId_lr, [fDBParams.YMSetIDs]));

    if aWithContainerWhere then begin
      CodeSite.SendString('DataItemWhere', mDataItemList.GetSQLWhere);
      Add(mDataItemList.GetSQLWhere);
    end;
  
    if aAddWhere <> '' then
      Add(aAddWhere);
  
    Result := Text;
  finally
    Free;
  end;
end; // TdmLabReport.GetWhereStr

//:---------------------------------------------------------------------------
{* F�llt die Beschriftung f�r die X-Achse in die Liste
*}
function TdmLabReport.GetXAxisNamesForChart(aFieldName: String; var aStrList: TmmStringList): Boolean;
begin
  Result := False;
  aStrList.Clear;
  with dseWork do
  try
    Close;
    CommandText := Format(cBasicQuery[fDBParams.TimeMode], ['distinct', aFieldName, cTimeModeTables[fDBParams.TimeMode], fDefaultShiftCal, GetWhereStr('', False)]);
    CodeSite.SendString('GetXAxisNamesForChart ' + aFieldName, CommandText);
    Open;
    while not EOF do begin
      aStrList.Add(FieldByName(aFieldName).AsString);
      Next;
    end;
    Close;
    Result := True;
  except
    on e:Exception do
      CodeSite.SendError('GetXAxisNamesForChart: ' + e.Message);
  end;
end; // TdmLabReport.GetXAxisNamesForChart

//:---------------------------------------------------------------------------
{* F�llt die Beschriftung f�r die Z-Achse in die Liste. Diese Funktion wird nur aufgerufen, wenn die
Z-Achse konfiguriert ist. Inhalt: Name=Wert
*}
function TdmLabReport.GetZAxisNamesForChart(aItem: TLabDataItem; aDetailed: Boolean; var aStrList:
        TmmStringList): Boolean;
var
  xIDName: String;
  xFieldName: String;
begin
  Result := False;
  aStrList.Clear;
  if aDetailed then begin
    xIDName    := aItem.KeyFieldID;
    xFieldName := Format('%s, %s', [aItem.ColName, aItem.KeyFieldID])
  end else begin
    xIDName    := aItem.ColName;
    xFieldName := aItem.ColName;
  end;
  
  with dseWork do
  try
    Close;
    CommandText := Format(cBasicQuery[fDBParams.TimeMode], ['distinct', xFieldName, cTimeModeTables[fDBParams.TimeMode], fDefaultShiftCal, GetWhereStr('', False)]) +
                   ' order by ' + xFieldName;
    CodeSite.SendString('GetZAxisNamesForChart ' + xFieldName, CommandText);
    Open;
    while not EOF do begin
      aStrList.Values[FieldByName(xIDName).AsString] := FieldByName(aItem.ColName).AsString;
      Next;
    end;
    Close;
    Result := True;
  except
    on e:Exception do
      CodeSite.SendError('GetZAxisNamesForChart: ' + e.Message);
  end;
end; // TdmLabReport.GetZAxisNamesForChart

//:---------------------------------------------------------------------------
function TLabDataItemPEff.GetSQLSelectFields(aWithSum: boolean = true; aWithAlias:boolean = true): String;
begin
  if DataItemType.InheritsFrom(TDataItem) then begin
    // Je nachdem was gew�nscht ist, das komplete Statement zusammensetzen
    if UseMode <> umNever then begin
      if (DataMode = dmGrid) and KeyField and (fDataLevel <> -1) and (TableIndex >= fDataLevel) then
        // Wenn im Hierarchy Modus dann das Count-Feld f�r das Query angeben
        Result := Format(cSelectCountField, [ColName, FieldName])
      else begin
        // Datenfelder unabh�ngig ob Grid oder Chart behandeln
        if aWithSum and not KeyField then
          // zb: SUM(v.c_Len) as "c_Len"
          Result := StringReplace('CASE sum(%s.c_wtSpd) WHEN 0 THEN 0 ELSE (sum(%s.c_rtSpd) * 100.0) / sum(%s.c_wtSpd) END AS PEff', '%s', LinkAlias, [rfReplaceAll])
        else
          // zB: (v.c_style_name) as "c_style_name"
          Result := StringReplace('CASE %s.c_wtSpd WHEN 0 THEN 0 ELSE (%s.c_rtSpd * 100.0) / %s.c_wtSpd END AS PEff', '%s', LinkAlias, [rfReplaceAll]);

//        if aWithAlias then
//          Result := Format('%s as "%s"',[Result, FieldName]);
      end;
    end else
      Result := '';
  end else
    Result := inherited GetSQLSelectFields(aWithSum, aWithAlias);
end; // TLabDataItem.GetSQLSelectFields


initialization
  gDataItemPool := Nil;
finalization
  FreeAndNil(gDataItemPool);
end.

