unit LabReport_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88.1.0.1.0  $
// File generated on 13.11.2003 10:10:06 from Type Library described below.

// ************************************************************************ //
// Type Lib: W:\MillMaster\GUI\MMLabReport\MMLabReport.tlb (1)
// IID\LCID: {EBDD1832-1D13-11D5-8481-0050DA46B864}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\System32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  LabReportMajorVersion = 1;
  LabReportMinorVersion = 0;

  LIBID_LabReport: TGUID = '{EBDD1832-1D13-11D5-8481-0050DA46B864}';

  IID_ILabReportServer: TGUID = '{EBDD1833-1D13-11D5-8481-0050DA46B864}';
  CLASS_LabReportServer: TGUID = '{EBDD1835-1D13-11D5-8481-0050DA46B864}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ILabReportServer = interface;
  ILabReportServerDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  LabReportServer = ILabReportServer;


// *********************************************************************//
// Interface: ILabReportServer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EBDD1833-1D13-11D5-8481-0050DA46B864}
// *********************************************************************//
  ILabReportServer = interface(IDispatch)
    ['{EBDD1833-1D13-11D5-8481-0050DA46B864}']
    procedure OpenReport(const aParameter: WideString); safecall;
  end;

// *********************************************************************//
// DispIntf:  ILabReportServerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EBDD1833-1D13-11D5-8481-0050DA46B864}
// *********************************************************************//
  ILabReportServerDisp = dispinterface
    ['{EBDD1833-1D13-11D5-8481-0050DA46B864}']
    procedure OpenReport(const aParameter: WideString); dispid 1;
  end;

// *********************************************************************//
// The Class CoLabReportServer provides a Create and CreateRemote method to          
// create instances of the default interface ILabReportServer exposed by              
// the CoClass LabReportServer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoLabReportServer = class
    class function Create: ILabReportServer;
    class function CreateRemote(const MachineName: string): ILabReportServer;
  end;

implementation

uses ComObj;

class function CoLabReportServer.Create: ILabReportServer;
begin
  Result := CreateComObject(CLASS_LabReportServer) as ILabReportServer;
end;

class function CoLabReportServer.CreateRemote(const MachineName: string): ILabReportServer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_LabReportServer) as ILabReportServer;
end;

end.
