object frmSettingsDialog: TfrmSettingsDialog
  Left = 241
  Top = 118
  BorderStyle = bsDialog
  Caption = '(*)Berichtseinstellungen festlegen'
  ClientHeight = 666
  ClientWidth = 878
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  ShowHint = True
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object pnBottom: TmmPanel
    Left = 0
    Top = 633
    Width = 878
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bClassDesigner: TmmSpeedButton
      Left = 24
      Top = 4
      Width = 163
      Height = 25
      Caption = '(*)Klassendesigner'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF008FF077777777
        777770FF0FBFEEE7EEEE870FF0007EE7EEEEF870FFFF07777777FF80FF00F0F7
        EEEE7B70F08800B7BEEE7F80F088707777777BF70F0788E7EEEE7FBF70008EE7
        EEEE77777777777777777BF7BFE7EEE7EEEE7FB7EEE7EEE7EEEE777777777777
        77777BF7EEE7EEE7EEEE7FE7EEE7EEE7EEEE7777777777777777}
      ParentShowHint = False
      ShowHint = True
      Visible = True
      OnClick = bClassDesignerClick
      AutoLabel.LabelPosition = lpLeft
    end
    object bOK: TmmButton
      Left = 623
      Top = 5
      Width = 80
      Height = 25
      Hint = '(*)Dialog schliessen'
      Anchors = [akRight, akBottom]
      Caption = '(9)OK'
      ModalResult = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      OnClick = bOKClick
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 707
      Top = 5
      Width = 80
      Height = 25
      Hint = '(*)Dialog abbrechen'
      Anchors = [akRight, akBottom]
      Caption = '(9)Abbrechen'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 790
      Top = 5
      Width = 80
      Height = 25
      Hint = '(*)Hilfe aufrufen...'
      Anchors = [akRight, akBottom]
      Caption = '(9)&Hilfe'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      OnClick = bHelpClick
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object pcSettings: TmmPageControl
    Left = 217
    Top = 0
    Width = 661
    Height = 633
    ActivePage = tsDataItem
    Align = alClient
    TabOrder = 0
    object tsDataItem: TTabSheet
      BorderWidth = 1
      Caption = '(*)Grafik'
      ImageIndex = 2
      object bChartLine: TmmSpeedButton
        Left = 18
        Top = 56
        Width = 33
        Height = 33
        GroupIndex = 2
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          04000000000080000000C40E0000C40E00001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FF7FFF7FFF7F
          FF7FFF7F7F7F7F7F7F7F7700000000000000FF0FFFFFFFFFFFFFF70FFFFFFFFF
          F999FF0FFFFFFFFFF9FF770FFFFFFFF99FFFFF0FFFFFFF9FFFFFF709FFFFF9FF
          FFFFFF0F9FFFF9FFFFFF770FF9FFF9FFFFFFFF0FF9FF9FFFFFFFF70FF9F9FFFF
          FFFFFF0FFF9FFFFFFFFF770FFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        Visible = True
        OnClick = OnItemsSettingsChange
        AutoLabel.LabelPosition = lpLeft
      end
      object bChartBar: TmmSpeedButton
        Tag = 1
        Left = 18
        Top = 96
        Width = 33
        Height = 33
        GroupIndex = 2
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          04000000000080000000C40E0000C40E00001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FF7FFF7FFF7F
          FF7FFF7F7F7F7F7F7F7F7700000000000000FF0FF22F44F11FFFF70FF22F44F1
          1FFFFF0FF22F44F11FFF770FF22F44F11FFFFF0FF22F44F11FFFF70FFFFF44F1
          1FFFFF0FFFFF44F11FFF770FFFFF44FFFFFFFF0FFFFF44FFFFFFF70FFFFFFFFF
          FFFFFF0FFFFFFFFFFFFF770FFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        Visible = True
        OnClick = OnItemsSettingsChange
        AutoLabel.LabelPosition = lpLeft
      end
      object bChartArea: TmmSpeedButton
        Tag = 2
        Left = 18
        Top = 136
        Width = 33
        Height = 33
        GroupIndex = 2
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          04000000000080000000C40E0000C40E00001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FF7FFF7FFF7F
          FF7FFF7F7F7F7F7F7F7F7700000000000000FF02222222222222F70222222222
          2222FF022222222222227702222222222222FF0222222222222FF70FF2222222
          22FFFF0FFFF222222FFF770FFFFF2222FFFFFF0FFFFFFFFFFFFFF70FFFFFFFFF
          FFFFFF0FFFFFFFFFFFFF770FFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        Visible = True
        OnClick = OnItemsSettingsChange
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel9: TmmLabel
        Left = 72
        Top = 24
        Width = 82
        Height = 13
        Caption = '(*)Datenelemente'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmGroupBox3: TmmGroupBox
        Left = 0
        Top = 280
        Width = 337
        Height = 321
        Anchors = [akLeft, akTop, akBottom]
        Caption = '(*)Einstellungen pro Datenelement'
        TabOrder = 6
        CaptionSpace = True
        object laAxisOrientation: TmmLabel
          Left = 8
          Top = 17
          Width = 58
          Height = 13
          Alignment = taCenter
          Caption = '(20)Y-Achse'
          FocusControl = cobItemAxis
          Layout = tlCenter
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel11: TmmLabel
          Left = 184
          Top = 80
          Width = 149
          Height = 37
          AutoSize = False
          Caption = '(55)Anzahl Verletzungen ab welcher die Einfaerbung erfolgt'
          Color = clBtnFace
          FocusControl = edBorderCondition
          ParentColor = False
          Layout = tlBottom
          Visible = True
          WordWrap = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel13: TmmLabel
          Left = 184
          Top = 17
          Width = 93
          Height = 13
          Caption = '(*)Klassierdatenwert'
          FocusControl = cobChartClassValue
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laCalcMethodDescription: TmmLabel
          Left = 8
          Top = 168
          Width = 321
          Height = 51
          AutoSize = False
          Color = clInfoBk
          ParentColor = False
          Visible = True
          WordWrap = True
          AutoLabel.LabelPosition = lpLeft
        end
        object clbCalcMethods: TmmCheckListBox
          Tag = 13
          Left = 8
          Top = 82
          Width = 161
          Height = 82
          ItemHeight = 13
          TabOrder = 1
          Visible = True
          OnClick = clbCalcMethodsClick
          AutoLabel.LabelPosition = lpLeft
        end
        object pnCalcMethod: TmmPanel
          Left = 8
          Top = 224
          Width = 321
          Height = 88
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 2
        end
        object cobChartClassValue: TmmComboBox
          Tag = 10
          Left = 184
          Top = 32
          Width = 137
          Height = 21
          Style = csDropDownList
          Color = clWindow
          ItemHeight = 13
          TabOrder = 4
          Visible = True
          OnChange = OnItemsSettingsChange
          Items.Strings = (
            '(30)Schnitte'
            '(30)Ungeschnittene'
            '(*)Beide')
          AutoLabel.Control = mmLabel13
          AutoLabel.LabelPosition = lpTop
          Edit = True
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
        end
        object edBorderCondition: TmmEdit
          Tag = 12
          Left = 184
          Top = 119
          Width = 33
          Height = 21
          Color = clWindow
          TabOrder = 3
          Text = '0'
          Visible = True
          OnExit = OnItemsSettingsChange
          Alignment = taRightJustify
          AutoLabel.Control = mmLabel11
          AutoLabel.LabelPosition = lpTop
          Decimals = -1
          NumMode = True
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
          ValidateMode = [vmInput]
        end
        object cobItemAxis: TmmComboBox
          Tag = 3
          Left = 8
          Top = 32
          Width = 161
          Height = 21
          Style = csDropDownList
          Color = clWindow
          ItemHeight = 13
          TabOrder = 0
          Visible = True
          OnChange = OnItemsSettingsChange
          Items.Strings = (
            '(15)links'
            '(15)links oben'
            '(15)rechts'
            '(15)rechts oben')
          AutoLabel.Control = laAxisOrientation
          AutoLabel.LabelPosition = lpTop
          Edit = False
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
        end
      end
      object mChart: TmmChart
        Left = 212
        Top = 40
        Width = 341
        Height = 193
        AllowPanning = pmNone
        AllowZoom = False
        BackImageInside = True
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        BackWall.Color = clBtnFace
        BottomWall.Brush.Color = clWhite
        Foot.Font.Charset = DEFAULT_CHARSET
        Foot.Font.Color = clBlack
        Foot.Font.Height = -11
        Foot.Font.Name = 'Arial'
        Foot.Font.Style = []
        Foot.Text.Strings = (
          '')
        Foot.Visible = False
        Gradient.Direction = gdBottomTop
        Gradient.Visible = True
        LeftWall.Brush.Color = clWhite
        MarginBottom = 2
        MarginLeft = 2
        MarginRight = 2
        MarginTop = 2
        Title.Text.Strings = (
          '')
        Title.Visible = False
        BackColor = clBtnFace
        BottomAxis.DateTimeFormat = 'dd.mmm.yyyy'
        Legend.Alignment = laLeft
        Legend.ColorWidth = 15
        Legend.Inverted = True
        Legend.TopPos = 1
        View3D = False
        View3DOptions.Elevation = 346
        View3DOptions.Orthogonal = False
        View3DOptions.Rotation = 341
        View3DOptions.Zoom = 93
        OnAfterDraw = mChartAfterDraw
        BevelOuter = bvNone
        Color = clSilver
        TabOrder = 2
        MouseMode = mmRotate
        object Series1: TBarSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          XValues.DateTime = True
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Bar'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
        object Series2: TBarSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clGreen
          VertAxis = aRightAxis
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Bar'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
        object Series3: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clYellow
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object bColorSelect: TmmColorButton
        Tag = 8
        Left = 18
        Top = 184
        Width = 33
        Height = 33
        AutoLabel.Distance = 5
        AutoLabel.LabelPosition = lpTop
        Caption = '&Weitere...'
        Color = clBlue
        OnChangeColor = OnItemsSettingsChange
        TabOrder = 1
        TabStop = True
        Visible = True
      end
      object lvChartSeries: TmmListView
        Tag = 2
        Left = 72
        Top = 40
        Width = 141
        Height = 193
        Hint = '(*)Ziehen Sie Datenelemente hierher'
        Color = clInfoBk
        Columns = <
          item
            Width = 138
          end>
        Ctl3D = False
        DragMode = dmAutomatic
        HideSelection = False
        MultiSelect = True
        OwnerDraw = True
        ReadOnly = True
        RowSelect = True
        PopupMenu = pmListView
        ShowColumnHeaders = False
        SmallImages = mImgListView
        TabOrder = 0
        ViewStyle = vsReport
        Visible = True
        OnDblClick = lvItemsDblClick
        OnDrawItem = lvChartSeriesDrawItem
        OnDragDrop = lvChartSeriesDragDrop
        OnDragOver = ChartItemsDragOver
        OnSelectItem = lvChartSeriesSelectItem
        AutoLabel.LabelPosition = lpTop
      end
      object pnChartXAxis: TmmPanel
        Left = 212
        Top = 232
        Width = 341
        Height = 33
        Hint = '(*)Ziehen Sie ein Schluesselelement hierher'
        BevelOuter = bvNone
        BorderWidth = 3
        BorderStyle = bsSingle
        Caption = '(*)Ziehen Sie ein Schluesselelement hierher'
        Color = clInfoBk
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 3
        OnDragDrop = ChartAxisDragDrop
        OnDragOver = ChartItemsDragOver
      end
      object pnChartZAxis: TmmPanel
        Left = 212
        Top = 8
        Width = 341
        Height = 33
        Hint = '(*)Ziehen Sie ein Schluesselelement hierher'
        BevelOuter = bvNone
        BorderWidth = 3
        BorderStyle = bsSingle
        Caption = '(*)Ziehen Sie ein Schluesselelement hierher'
        Color = clInfoBk
        Ctl3D = False
        DragMode = dmAutomatic
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 4
        OnDragDrop = ChartAxisDragDrop
        OnDragOver = ChartItemsDragOver
      end
      object mmGroupBox2: TmmGroupBox
        Left = 344
        Top = 280
        Width = 305
        Height = 321
        Anchors = [akLeft, akTop, akBottom]
        Caption = '(*)Grafikeinstellungen'
        TabOrder = 5
        CaptionSpace = True
        object mmPageControl1: TmmPageControl
          Left = 2
          Top = 24
          Width = 301
          Height = 295
          ActivePage = TabSheet1
          Anchors = [akLeft, akTop, akRight, akBottom]
          Style = tsFlatButtons
          TabOrder = 0
          object TabSheet1: TTabSheet
            Caption = '(*)Allgemein'
            object laAxisTitel: TmmLabel
              Left = 4
              Top = 9
              Width = 40
              Height = 13
              Caption = '(*)Achse'
              FocusControl = lbAxis
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object laAxisMin: TmmLabel
              Left = 108
              Top = 49
              Width = 51
              Height = 13
              Caption = '(*)Minimum'
              Enabled = False
              FocusControl = edAxisMin
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object laAxisMax: TmmLabel
              Left = 188
              Top = 49
              Width = 54
              Height = 13
              Caption = '(*)Maximum'
              Enabled = False
              FocusControl = edAxisMax
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object laScaling: TmmLabel
              Left = 108
              Top = 9
              Width = 60
              Height = 13
              Caption = '(*)Skalierung'
              Enabled = False
              FocusControl = cobScaling
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object mmLabel6: TmmLabel
              Left = 4
              Top = 97
              Width = 72
              Height = 13
              Caption = '(*)Balkenformat'
              FocusControl = cobBarStyle
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object mmLabel7: TmmLabel
              Left = 4
              Top = 137
              Width = 52
              Height = 13
              Caption = '(*)Legende'
              FocusControl = cobLegend
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object lbAxis: TmmListBox
              Left = 4
              Top = 24
              Width = 89
              Height = 65
              Enabled = True
              ItemHeight = 13
              Items.Strings = (
                '(15)links'
                '(15)links oben'
                '(15)rechts'
                '(15)rechts oben')
              TabOrder = 2
              Visible = True
              OnClick = lbAxisClick
              AutoLabel.Control = laAxisTitel
              AutoLabel.LabelPosition = lpTop
            end
            object cobScaling: TmmComboBox
              Tag = 18
              Left = 108
              Top = 24
              Width = 137
              Height = 21
              Style = csDropDownList
              Color = clWindow
              Enabled = False
              ItemHeight = 13
              TabOrder = 3
              Visible = True
              OnChange = OnChartOptionsChange
              Items.Strings = (
                '(*)Automatisch'
                '(*)Manuell'
                '(*)Ermitteln')
              AutoLabel.Control = laScaling
              AutoLabel.LabelPosition = lpTop
              Edit = False
              ReadOnlyColor = clInfoBk
              ShowMode = smNormal
            end
            object cobBarStyle: TmmComboBox
              Tag = 2
              Left = 4
              Top = 112
              Width = 121
              Height = 21
              Style = csDropDownList
              Color = clWindow
              ItemHeight = 13
              TabOrder = 4
              Visible = True
              OnChange = OnChartOptionsChange
              Items.Strings = (
                '(15)Normal'
                '(15)Seite'
                '(15)Gestapelt'
                '(15)Gestapelt 100%')
              AutoLabel.Control = mmLabel6
              AutoLabel.LabelPosition = lpTop
              Edit = False
              ReadOnlyColor = clInfoBk
              ShowMode = smNormal
            end
            object cobLegend: TmmComboBox
              Tag = 3
              Left = 4
              Top = 152
              Width = 121
              Height = 21
              Style = csDropDownList
              Color = clWindow
              ItemHeight = 13
              TabOrder = 5
              Visible = True
              OnChange = OnChartOptionsChange
              Items.Strings = (
                '(15)links'
                '(15)rechts'
                '(15)oben'
                '(15)unten')
              AutoLabel.Control = mmLabel7
              AutoLabel.LabelPosition = lpTop
              Edit = False
              ReadOnlyColor = clInfoBk
              ShowMode = smNormal
            end
            object edAxisMax: TmmEdit
              Tag = 13
              Left = 188
              Top = 64
              Width = 40
              Height = 21
              Color = clWindow
              Enabled = False
              TabOrder = 0
              Text = '0'
              Visible = True
              OnChange = OnChartOptionsChange
              Alignment = taRightJustify
              AutoLabel.Control = laAxisMax
              AutoLabel.LabelPosition = lpTop
              Decimals = -1
              NumMode = True
              ReadOnlyColor = clInfoBk
              ShowMode = smNormal
              ValidateMode = [vmInput]
            end
            object edAxisMin: TmmEdit
              Tag = 12
              Left = 108
              Top = 64
              Width = 40
              Height = 21
              Color = clWindow
              Enabled = False
              TabOrder = 1
              Text = '0'
              Visible = True
              OnChange = OnChartOptionsChange
              Alignment = taRightJustify
              AutoLabel.Control = laAxisMin
              AutoLabel.LabelPosition = lpTop
              Decimals = -1
              NumMode = True
              ReadOnlyColor = clInfoBk
              ShowMode = smNormal
              ValidateMode = [vmInput]
            end
          end
          object TabSheet2: TTabSheet
            Caption = '(*)Erweitert'
            ImageIndex = 1
            object bChartImageOpen: TmmSpeedButton
              Left = 144
              Top = 64
              Width = 24
              Height = 21
              Glyph.Data = {
                F6000000424DF600000000000000760000002800000010000000100000000100
                0400000000008000000000000000000000001000000000000000000000000000
                8000008000000080800080000000800080008080000080808000C0C0C0000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
                88888888888888888888000000000008888800333333333088880B0333333333
                08880FB03333333330880BFB0333333333080FBFB000000000000BFBFBFBFB08
                88880FBFBFBFBF0888880BFB0000000888888000888888880008888888888888
                8008888888880888080888888888800088888888888888888888}
              Visible = True
              OnClick = bChartImageOpenClick
              AutoLabel.LabelPosition = lpLeft
            end
            object laChartImg: TmmLabel
              Left = 4
              Top = 49
              Width = 81
              Height = 13
              Caption = '(*)Hintergrundbild'
              FocusControl = edChartImage
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object laStartColor: TmmLabel
              Left = 140
              Top = 7
              Width = 70
              Height = 13
              AutoSize = False
              Caption = '(14)Startfarbe'
              FocusControl = bChartStartColor
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object laEndColor: TmmLabel
              Left = 212
              Top = 7
              Width = 70
              Height = 13
              AutoSize = False
              Caption = '(14)Endfarbe'
              FocusControl = bChartEndColor
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object laColor: TmmLabel
              Left = 4
              Top = 9
              Width = 63
              Height = 13
              Caption = '(*)Farbverlauf'
              FocusControl = cobChartGradient
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object la3DWidth: TmmLabel
              Left = 22
              Top = 158
              Width = 54
              Height = 13
              AutoSize = False
              Caption = '(*)Tiefe'
              Enabled = False
              FocusControl = se3DWidth
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object bChartEndColor: TmmColorButton
              Tag = 6
              Left = 212
              Top = 24
              Width = 42
              Height = 21
              AutoLabel.Control = laEndColor
              AutoLabel.Distance = 4
              AutoLabel.LabelPosition = lpTop
              Caption = '&Weitere...'
              Color = 8454143
              OnChangeColor = OnChartOptionsChange
              TabOrder = 0
              TabStop = True
              Visible = True
            end
            object bChartStartColor: TmmColorButton
              Tag = 5
              Left = 140
              Top = 24
              Width = 42
              Height = 21
              AutoLabel.Control = laStartColor
              AutoLabel.Distance = 4
              AutoLabel.LabelPosition = lpTop
              Caption = '&Weitere...'
              Color = clWhite
              OnChangeColor = OnChartOptionsChange
              TabOrder = 1
              TabStop = True
              Visible = True
            end
            object edChartImage: TmmEdit
              Tag = 9
              Left = 4
              Top = 64
              Width = 137
              Height = 21
              Color = clWindow
              TabOrder = 2
              Visible = True
              OnChange = OnChartOptionsChange
              AutoLabel.Control = laChartImg
              AutoLabel.LabelPosition = lpTop
              Decimals = -1
              NumMode = False
              ReadOnlyColor = clInfoBk
              ShowMode = smNormal
              ValidateMode = [vmInput]
            end
            object cbChartImageInside: TmmCheckBox
              Tag = 8
              Left = 4
              Top = 88
              Width = 121
              Height = 17
              Caption = '(*)Innenseite'
              Checked = True
              State = cbChecked
              TabOrder = 3
              Visible = True
              OnClick = OnChartOptionsChange
              AutoLabel.LabelPosition = lpLeft
            end
            object cobChartGradient: TmmComboBox
              Tag = 7
              Left = 4
              Top = 24
              Width = 121
              Height = 21
              Style = csDropDownList
              Color = clWindow
              ItemHeight = 13
              TabOrder = 4
              Visible = True
              OnChange = OnChartOptionsChange
              Items.Strings = (
                '(20)Oben Unten'
                '(20)Unten Oben'
                '(20)Links Rechts'
                '(20)Rechts Links')
              AutoLabel.Control = laColor
              AutoLabel.LabelPosition = lpTop
              Edit = False
              ReadOnlyColor = clInfoBk
              ShowMode = smNormal
            end
            object cbXLabelRotated: TmmCheckBox
              Tag = 17
              Left = 4
              Top = 120
              Width = 129
              Height = 17
              Caption = '(*)X-Achse schraeg'
              TabOrder = 5
              Visible = True
              OnClick = OnChartOptionsChange
              AutoLabel.LabelPosition = lpLeft
            end
            object cbChart3D: TmmCheckBox
              Left = 4
              Top = 136
              Width = 129
              Height = 17
              Caption = '(15)3D-Ansicht'
              TabOrder = 6
              Visible = True
              OnClick = OnChartOptionsChange
              AutoLabel.LabelPosition = lpLeft
            end
            object cbChartRotate: TmmCheckBox
              Tag = 1
              Left = 4
              Top = 184
              Width = 129
              Height = 17
              Caption = '(15)Drehbar'
              TabOrder = 7
              Visible = True
              OnClick = OnChartOptionsChange
              AutoLabel.LabelPosition = lpLeft
            end
            object se3DWidth: TmmSpinEdit
              Tag = 16
              Left = 78
              Top = 154
              Width = 41
              Height = 22
              Enabled = False
              Increment = 5
              MaxValue = 100
              MinValue = 1
              TabOrder = 8
              Value = 15
              Visible = True
              OnChange = OnChartOptionsChange
              AutoLabel.Control = la3DWidth
              AutoLabel.LabelPosition = lpLeft
            end
            object cbSplitXAxis: TmmCheckBox
              Tag = 4
              Left = 4
              Top = 200
              Width = 129
              Height = 17
              Caption = '(15)X-Achse teilen'
              TabOrder = 9
              Visible = True
              OnClick = OnChartOptionsChange
              AutoLabel.LabelPosition = lpLeft
            end
          end
        end
      end
    end
    object tsGridSettings: TTabSheet
      BorderWidth = 1
      Caption = '(*)Tabelle'
      object mmLabel3: TmmLabel
        Left = 0
        Top = 167
        Width = 409
        Height = 13
        AutoSize = False
        Caption = '(40)Schriftart'
        Layout = tlCenter
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laTablePreview: TmmLabel
        Left = 0
        Top = 0
        Width = 651
        Height = 13
        Align = alTop
        Caption = '(*)Vorschau'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object bTitleFont: TmmBitBtn
        Tag = 1
        Left = 2
        Top = 224
        Width = 80
        Height = 25
        Hint = '(*)Schrifttyp einstellen'
        Caption = '(20)Schrifttyp'
        TabOrder = 5
        Visible = True
        OnClick = bGridFontClick
        AutoLabel.LabelPosition = lpLeft
      end
      object bTitleColor: TmmBitBtn
        Tag = 1
        Left = 104
        Top = 224
        Width = 80
        Height = 25
        Hint = '(*)Farbe setzen'
        Caption = '(20)Farbe'
        TabOrder = 6
        Visible = True
        OnClick = bGridColorClick
        AutoLabel.LabelPosition = lpLeft
      end
      object bDataFont: TmmBitBtn
        Tag = 2
        Left = 224
        Top = 224
        Width = 80
        Height = 25
        Hint = '(*)Schrifttyp einstellen'
        Caption = '(20)Schrifttyp'
        TabOrder = 8
        Visible = True
        OnClick = bGridFontClick
        AutoLabel.LabelPosition = lpLeft
      end
      object bDataColor: TmmBitBtn
        Tag = 2
        Left = 326
        Top = 224
        Width = 80
        Height = 25
        Hint = '(*)Farbe setzen'
        Caption = '(20)Farbe'
        TabOrder = 9
        Visible = True
        OnClick = bGridColorClick
        AutoLabel.LabelPosition = lpLeft
      end
      object pnTitleFormat: TmmPanel
        Left = 2
        Top = 188
        Width = 183
        Height = 30
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Caption = '(30)Titel'
        TabOrder = 4
      end
      object pnDataFormat: TmmPanel
        Left = 224
        Top = 188
        Width = 183
        Height = 30
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Caption = '(30)Daten'
        TabOrder = 7
      end
      object rgTextAlign: TmmRadioGroup
        Tag = 1
        Left = 0
        Top = 80
        Width = 164
        Height = 81
        Caption = '(25)Textausrichtung'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          '(15)links'
          '(15)zentriert'
          '(15)rechts')
        ParentFont = False
        TabOrder = 1
        OnClick = OnTableItemsChange
        CaptionSpace = True
      end
      object gbKeyOptions: TmmGroupBox
        Left = 172
        Top = 80
        Width = 217
        Height = 81
        Caption = '(25)Schluesselfeld Optionen'
        Enabled = False
        TabOrder = 2
        CaptionSpace = True
        object laHierarchy: TmmLabel
          Left = 53
          Top = 22
          Width = 156
          Height = 13
          AutoSize = False
          Caption = '(25)Summen-Detail Farbe'
          FocusControl = bHierarchyColor
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bHierarchyColor: TmmColorButton
          Tag = 4
          Left = 12
          Top = 18
          Width = 37
          Height = 21
          AutoLabel.Control = laHierarchy
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpRight
          Caption = '&Weitere...'
          Color = clInfoBk
          OnChangeColor = OnTableItemsChange
          Flat = True
          TabOrder = 0
          TabStop = True
          Visible = True
        end
        object cbKeySorting: TmmCheckBox
          Tag = 5
          Left = 16
          Top = 48
          Width = 193
          Height = 17
          Caption = '(25)Sortierung absteigend'
          TabOrder = 1
          Visible = True
          OnClick = OnTableItemsChange
          AutoLabel.LabelPosition = lpLeft
        end
      end
      object gbNumber: TmmGroupBox
        Left = 268
        Top = 80
        Width = 217
        Height = 81
        Caption = '(*)Zahlenformat'
        Enabled = False
        TabOrder = 3
        CaptionSpace = True
        object laComma: TmmLabel
          Left = 45
          Top = 47
          Width = 75
          Height = 13
          Caption = '(*)Kommastellen'
          FocusControl = seComma
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object cbThousands: TmmCheckBox
          Tag = 2
          Left = 8
          Top = 20
          Width = 153
          Height = 17
          Caption = '(*)Tausender Trennzeichen'
          TabOrder = 0
          Visible = True
          OnClick = OnTableItemsChange
          AutoLabel.LabelPosition = lpLeft
        end
        object seComma: TmmSpinEdit
          Tag = 3
          Left = 8
          Top = 43
          Width = 33
          Height = 22
          MaxValue = 3
          MinValue = 0
          TabOrder = 1
          Value = 1
          Visible = True
          OnChange = OnTableItemsChange
          AutoLabel.Control = laComma
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpRight
        end
      end
      object gbExtGridSettings: TmmGroupBox
        Left = 0
        Top = 289
        Width = 651
        Height = 314
        Align = alBottom
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = '(*)Erweiterte Tabelleneigenschaften'
        TabOrder = 10
        CaptionSpace = True
        object laFixedCols: TmmLabel
          Left = 16
          Top = 80
          Width = 169
          Height = 13
          AutoSize = False
          Caption = '(*)Anzeige'
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object edFixedCols: TmmEdit
          Left = 16
          Top = 96
          Width = 40
          Height = 21
          Color = clWindow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
          Text = '0'
          Visible = False
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpTop
          Decimals = -1
          NumMode = False
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
          ValidateMode = [vmInput]
        end
        object udFixedCols: TmmUpDown
          Left = 56
          Top = 96
          Width = 15
          Height = 21
          Associate = edFixedCols
          Min = 0
          Max = 10
          Position = 0
          TabOrder = 8
          Visible = False
          Wrap = False
        end
        object cbRowLines: TmmCheckBox
          Left = 16
          Top = 16
          Width = 270
          Height = 17
          Caption = '(80)Zeilenlinien anzeigen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          Visible = True
          OnClick = OnGridOptionsClick
          AutoLabel.LabelPosition = lpLeft
        end
        object cbColLines: TmmCheckBox
          Tag = 1
          Left = 16
          Top = 32
          Width = 270
          Height = 17
          Caption = '(80)Spaltenlinien anzeigen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          Visible = True
          OnClick = OnGridOptionsClick
          AutoLabel.LabelPosition = lpLeft
        end
        object cbIndicator: TmmCheckBox
          Tag = 2
          Left = 16
          Top = 200
          Width = 270
          Height = 17
          Caption = '(80)Spaltenzeiger anzeigen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          Visible = False
          OnClick = OnGridOptionsClick
          AutoLabel.LabelPosition = lpLeft
        end
        object cbRowSelect: TmmCheckBox
          Tag = 3
          Left = 16
          Top = 168
          Width = 270
          Height = 17
          Caption = '(80)Ganze Zeile markiert anzeigen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          Visible = False
          OnClick = OnGridOptionsClick
          AutoLabel.LabelPosition = lpLeft
        end
        object cbAlwaysShowSelection: TmmCheckBox
          Tag = 4
          Left = 16
          Top = 184
          Width = 273
          Height = 17
          Caption = '(80)Markierung permanent anzeigen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          Visible = False
          OnClick = OnGridOptionsClick
          AutoLabel.LabelPosition = lpLeft
        end
        object cbTitleAlignment: TmmCheckBox
          Tag = 5
          Left = 16
          Top = 128
          Width = 270
          Height = 17
          Caption = '(80)Spaltentitel zentriert anzeigen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          Visible = False
          OnClick = OnGridOptionsClick
          AutoLabel.LabelPosition = lpLeft
        end
        object cbTrailingEllipsis: TmmCheckBox
          Tag = 6
          Left = 16
          Top = 144
          Width = 270
          Height = 17
          Caption = '(80)Fortsetzungspunkte (...)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
          Visible = False
          OnClick = OnGridOptionsClick
          AutoLabel.LabelPosition = lpLeft
        end
        object cbHierarchyTable: TmmCheckBox
          Tag = 7
          Left = 16
          Top = 48
          Width = 270
          Height = 17
          Caption = '(80)Summen-Detail Tabelle'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 9
          Visible = True
          OnClick = OnGridOptionsClick
          AutoLabel.LabelPosition = lpLeft
        end
      end
      object sgTableItems: TmmStringGrid
        Tag = 4
        Left = 0
        Top = 13
        Width = 651
        Height = 60
        TabStop = False
        Align = alTop
        Color = clInfoBk
        ColCount = 1
        DefaultColWidth = 50
        DefaultRowHeight = 17
        DefaultDrawing = False
        FixedCols = 0
        RowCount = 2
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing]
        ParentFont = False
        ScrollBars = ssHorizontal
        TabOrder = 0
        Visible = True
        OnDragDrop = sgTableItemsDragDrop
        OnDragOver = ChartItemsDragOver
        OnDrawCell = sgTableItemsDrawCell
        OnEndDrag = sgTableItemsEndDrag
        OnMouseDown = sgTableItemsMouseDown
        OnStartDrag = sgTableItemsStartDrag
        AutoLabel.LabelPosition = lpLeft
      end
      object gbGridClassValue: TmmGroupBox
        Left = 396
        Top = 80
        Width = 153
        Height = 81
        Caption = '(*)Klassierdatenwert'
        TabOrder = 11
        CaptionSpace = True
        object cobGridClassValue: TmmComboBox
          Tag = 6
          Left = 8
          Top = 24
          Width = 137
          Height = 21
          Style = csDropDownList
          Color = clWindow
          ItemHeight = 13
          TabOrder = 0
          Visible = True
          OnChange = OnTableItemsChange
          Items.Strings = (
            '(30)Schnitte'
            '(30)Ungeschnittene'
            '(*)Beide')
          AutoLabel.LabelPosition = lpTop
          Edit = True
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
        end
      end
    end
    object tsQualityMatrix: TTabSheet
      BorderWidth = 1
      Caption = '(*)Klassiermatrix'
      ImageIndex = 2
      object gbQMatrixSettings: TmmGroupBox
        Left = 0
        Top = 0
        Width = 385
        Height = 209
        Caption = '(*)Einstellungen'
        TabOrder = 0
        CaptionSpace = True
        object mmLabel1: TmmLabel
          Left = 8
          Top = 132
          Width = 82
          Height = 13
          Caption = '(*)Reinigerkurven'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel4: TmmLabel
          Left = 8
          Top = 68
          Width = 72
          Height = 13
          Caption = '(*)Klassierfelder'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel2: TmmLabel
          Left = 200
          Top = 24
          Width = 59
          Height = 13
          Caption = '(*)Garnfehler'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel5: TmmLabel
          Left = 200
          Top = 132
          Width = 64
          Height = 13
          Caption = '(*)Klassierung'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel8: TmmLabel
          Left = 8
          Top = 24
          Width = 63
          Height = 13
          Caption = '(*)Grundfarbe'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object cbChannelCurve: TmmCheckBox
          Left = 16
          Top = 148
          Width = 161
          Height = 17
          Caption = '(15)Kanalkurve'
          TabOrder = 0
          Visible = True
          OnClick = OnMatrixOptionsChange
          AutoLabel.LabelPosition = lpLeft
        end
        object cbClusterCurve: TmmCheckBox
          Tag = 1
          Left = 16
          Top = 164
          Width = 161
          Height = 17
          Caption = '(15)Fehlerschwarm'
          TabOrder = 1
          Visible = True
          OnClick = OnMatrixOptionsChange
          AutoLabel.LabelPosition = lpLeft
        end
        object cbSpliceCurve: TmmCheckBox
          Tag = 2
          Left = 16
          Top = 180
          Width = 161
          Height = 17
          Caption = '(15)Spleisskurve'
          Checked = True
          State = cbChecked
          TabOrder = 2
          Visible = True
          OnClick = OnMatrixOptionsChange
          AutoLabel.LabelPosition = lpLeft
        end
        object cbClassFields: TmmCheckBox
          Tag = 5
          Left = 16
          Top = 84
          Width = 161
          Height = 17
          Caption = '(15)Sichtbar'
          TabOrder = 3
          Visible = True
          OnClick = OnMatrixOptionsChange
          AutoLabel.LabelPosition = lpLeft
        end
        object cbCutValues: TmmCheckBox
          Tag = 4
          Left = 208
          Top = 40
          Width = 161
          Height = 17
          Caption = '(*)Geschnittene'
          TabOrder = 4
          Visible = True
          OnClick = OnMatrixOptionsChange
          AutoLabel.LabelPosition = lpLeft
        end
        object cbDefectValues: TmmCheckBox
          Tag = 3
          Left = 208
          Top = 56
          Width = 161
          Height = 17
          Caption = '(*)Verbleibende'
          TabOrder = 5
          Visible = True
          OnClick = OnMatrixOptionsChange
          AutoLabel.LabelPosition = lpLeft
        end
        object rbQMCoarse: TmmRadioButton
          Tag = 6
          Left = 208
          Top = 148
          Width = 113
          Height = 17
          Caption = '(*)Grob'
          TabOrder = 6
          OnClick = OnMatrixOptionsChange
        end
        object rbQMFine: TmmRadioButton
          Tag = 7
          Left = 208
          Top = 164
          Width = 113
          Height = 17
          Caption = '(*)Fein'
          TabOrder = 7
          OnClick = OnMatrixOptionsChange
        end
        object bClassActiveColor: TmmColorButton
          Tag = 8
          Left = 34
          Top = 100
          Width = 42
          Height = 21
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpTop
          Caption = '&Weitere...'
          Color = 8454143
          OnChangeColor = OnMatrixOptionsChange
          TabOrder = 8
          TabStop = True
          Visible = True
        end
        object bClassInactiveColor: TmmColorButton
          Tag = 9
          Left = 8
          Top = 40
          Width = 42
          Height = 21
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpTop
          Caption = '&Weitere...'
          Color = 8454143
          OnChangeColor = OnMatrixOptionsChange
          TabOrder = 9
          TabStop = True
          Visible = True
        end
      end
    end
  end
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 217
    Height = 633
    Align = alLeft
    BevelOuter = bvNone
    BorderWidth = 2
    TabOrder = 2
    object laAvailable: TmmLabel
      Left = 8
      Top = 33
      Width = 108
      Height = 13
      Caption = '(25)Verfuegbare Daten'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object imgBasket: TmmImage
      Left = 177
      Top = 8
      Width = 32
      Height = 32
      AutoSize = True
      Picture.Data = {
        07544269746D617036080000424D360800000000000036040000280000002000
        0000200000000100080000000000000400000000000000000000000100000000
        000000000000000080000080000000808000800000008000800080800000C0C0
        C000C0DCC000F0CAA6000020400000206000002080000020A0000020C0000020
        E00000400000004020000040400000406000004080000040A0000040C0000040
        E00000600000006020000060400000606000006080000060A0000060C0000060
        E00000800000008020000080400000806000008080000080A0000080C0000080
        E00000A0000000A0200000A0400000A0600000A0800000A0A00000A0C00000A0
        E00000C0000000C0200000C0400000C0600000C0800000C0A00000C0C00000C0
        E00000E0000000E0200000E0400000E0600000E0800000E0A00000E0C00000E0
        E00040000000400020004000400040006000400080004000A0004000C0004000
        E00040200000402020004020400040206000402080004020A0004020C0004020
        E00040400000404020004040400040406000404080004040A0004040C0004040
        E00040600000406020004060400040606000406080004060A0004060C0004060
        E00040800000408020004080400040806000408080004080A0004080C0004080
        E00040A0000040A0200040A0400040A0600040A0800040A0A00040A0C00040A0
        E00040C0000040C0200040C0400040C0600040C0800040C0A00040C0C00040C0
        E00040E0000040E0200040E0400040E0600040E0800040E0A00040E0C00040E0
        E00080000000800020008000400080006000800080008000A0008000C0008000
        E00080200000802020008020400080206000802080008020A0008020C0008020
        E00080400000804020008040400080406000804080008040A0008040C0008040
        E00080600000806020008060400080606000806080008060A0008060C0008060
        E00080800000808020008080400080806000808080008080A0008080C0008080
        E00080A0000080A0200080A0400080A0600080A0800080A0A00080A0C00080A0
        E00080C0000080C0200080C0400080C0600080C0800080C0A00080C0C00080C0
        E00080E0000080E0200080E0400080E0600080E0800080E0A00080E0C00080E0
        E000C0000000C0002000C0004000C0006000C0008000C000A000C000C000C000
        E000C0200000C0202000C0204000C0206000C0208000C020A000C020C000C020
        E000C0400000C0402000C0404000C0406000C0408000C040A000C040C000C040
        E000C0600000C0602000C0604000C0606000C0608000C060A000C060C000C060
        E000C0800000C0802000C0804000C0806000C0808000C080A000C080C000C080
        E000C0A00000C0A02000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0
        E000C0C00000C0C02000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0
        A000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
        FF00000000000000000000000000000000009B9B9B9B00000000000000000000
        000000000000000000000000000000009B9BF7F7649B9B9B0000000000000000
        00000000000000000000000000009B9BF7F70707AD9B9B9B9B9B000000000000
        0000000000000000000000009B9BF7F707070707F79B9B9B9B9B9B0000000000
        000000000000000000009B9BF7F7070707070707F7649B9B9B9B9B9B00000000
        00000000000000009B9BF7F7070707070707070707649B9B9B9B9B9B9B000000
        0000000000009B9BF7F70707070707B56B07070707AD9B9B9B9B9B9B9B000000
        000000000000F707070707070707AB68AB07070707F79B9B9B9B9B9B9B000000
        000000000000F70707070707076A686868AA070707F79B9B9B9B9B9B9B000000
        0000000000000707070707B5070768AB6A68F7070707649B9B9B9B9B9B9B0000
        00000000000007070707B5AB0707AB07B668AB070707649B9B9B9B9B9B9B0000
        0000000000F7070707B56A070707070707ABAB070707AD9B9B9B9B9B9B9B0000
        0000000000F70707076AAB070707070707B5AD070707F79B9B9B9B9B9B9B0000
        0000000000070707B56A0707070707070707AD070707F79B9B9B9B9B9B9B9B00
        0000000000070707AB68070707070707070707070707F7649B9B9B9B9B9B9B00
        00000000000707076A680707070707B5B5AB0707070707649B9B9B9B9B9B9B00
        00000000F70707076868AA070707B36868680707070707AD9B9B9B9B9B9B9B00
        00000000F707B36A686AB507070707B3686AB507070707F79B9B9B9B9B9B9B9B
        00000000070707B36AB5070707B5B3686AABB507070707F79B9B9B9B9B9B9B9B
        0000000007070707AB070707B56A68AB07AB070707070707649B9B9B9B9B9B9B
        00000000070707070707B56A6A68AB070707070707070707AD9B9B9B9B9B9B9B
        000000F70707070707070707070707070707070707070707F79B9B9B9B9B9B9B
        9B0000F7070707070707070707070707070707080808F6F607ADA49B9B9B9B9B
        9B00000707070707070707070707070707F6F6F6F7FFFFFFFFB607ADA49B9B9B
        9B00000707070707070707070707F6F6F6F70707FFFFFFFFFFFFFFB607ADA49B
        9B0000070707070707070708F6F6F707070707FFFFFFFFFFFFFFFFFFFFB607AD
        9B0000070707070708F6F6F79B0707070707FFFFFFFFFFFFFFF6F6F608080800
        00000007F6F6F6F6F79B9B9B9B07070707FFFFFFFFFFF6F6F608080800000000
        000000F6F7F79B9B9B9B9B9BA4070707FFFFFFF6F6F608080800000000000000
        00000000070707F79B9B9B9BA40707FFF6F6F608080800000000000000000000
        00000000000000070707F79BA407F6F608080800000000000000000000000000
        000000000000000000000707080808F600000000000000000000000000000000
        0000}
      Stretch = True
      Transparent = True
      Visible = True
      OnDragDrop = imgBasketDragDrop
      OnDragOver = imgBasketDragOver
      AutoLabel.LabelPosition = lpLeft
    end
    object mItemTree: TDataItemTree
      Left = 2
      Top = 48
      Width = 213
      Height = 583
      Align = alBottom
      Anchors = [akLeft, akTop, akRight, akBottom]
      DragMode = dmAutomatic
      TabOrder = 0
      OnDblClick = mItemTreeDblClick
      Images = mImgListView
      LineColor = 15596027
      OnGetImage = mItemTreeGetImage
    end
  end
  object mImgListView: TmmImageList
    Left = 88
    Top = 424
    Bitmap = {
      494C010105000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF00000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      000000FFFF0000FFFF000000000000FFFF0000FFFF000000000000FFFF000000
      00000000000000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      0000000000000000000084848400000000000000000000000000848484000000
      0000000000000000000084848400000000000000000000000000848484000000
      0000000000000000000084848400000000000000000000000000848484000000
      0000000000000000000084848400000000000000000000000000848484000000
      0000000000000000000084848400000000000000000000000000848484000000
      0000000000000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      0000848484000000000084848400000000008484840000000000848484000000
      0000848484000000000084848400000000000000000000000000848484000000
      0000848484000000000084848400000000008484840000000000848484000000
      0000848484000000000084848400000000000000000000000000848484000000
      0000848484000000000084848400000000008484840000000000848484000000
      0000848484000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084000000840000000000008400000084000000000000000000
      8400000084000000000000000000000000000000000000000000000000000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000084000000840000008400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004200E7004200E7004200E7000000000084848400000000000000
      0000000000000084000000840000000000008400000084000000000000000000
      8400000084000000000000000000000000000000000084848400000000000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000084000000840000008400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004200E70000000000000000000000000000000000000000000000
      0000000000000084000000840000000000008400000084000000000000000000
      8400000084000000000000000000000000000000000000000000000000000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000084000000840000008400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000004200
      E7004200E7000000000000000000000000008484840084848400000000000000
      0000000000000084000000840000000000008400000084000000000000000000
      8400000084000000000000000000000000008484840084848400000000000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000084000000840000008400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004200E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084000000840000000000008400000084000000000000000000
      8400000084000000000000000000000000000000000000000000000000000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000084000000840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400000000004200
      E70000000000000000000000000000000000000000004200E700000000000000
      0000000000000000000000000000000000000000000084848400000000000000
      0000000000000000000000000000000000008400000084000000000000000000
      8400000084000000000000000000000000000000000084848400000000000000
      0000000000000084000000840000008400000084000000840000008400000084
      0000008400000084000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004200E700000000000000000000000000000000004200E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008400000084000000000000000000
      8400000084000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000008400000084000000840000008400000084
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400000000000000
      0000000000004200E7000000000000000000000000004200E700000000000000
      0000000000000000000000000000000000008484840084848400000000000000
      0000000000000000000000000000000000008400000084000000000000000000
      0000000000000000000000000000000000008484840084848400000000000000
      0000000000000000000000000000000000000084000000840000008400000084
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004200E70000000000000000004200E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008400000084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400000000000000
      0000000000004200E700000000004200E7000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004200E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFF000000000000FFFF000000000000
      FFFF000000000000CFFF00000000000087FF00000000000003FF000000000000
      0003000000000000000100000000000000000000000000000000000000000000
      025B00000000000003FF00000000000087FF000000000000CFFF000000000000
      FFFF000000000000FFFF000000000000FFFFDDDDDDDDDDDDFFFFD555D555D555
      FFFF000000000000FFFFDFFFD927C000FFFF9FF899278000FFFFDFFBD927C000
      FFFF1FE719270000FFFFDFDFD927C001FFFF8FBF9F279803FFFFD7BFDF27DE07
      FFFF1BBF1F3F1F0FFFFFDB7FDF3FDFFFFFFF9AFF9FFF9FFFFFFFDDFFDFFFDFFF
      FFFF1FFF1FFF1FFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object mTranslator: TmmTranslator
    DictionaryName = 'LabMasterDic'
    Options = [ivtoCheckFont, ivtoScaleMultiByte, ivtoMirrorBiDirectional, ivtoChangeFontCharset, ivtoAutoOpen, ivtoTranslateSystemMenu]
    Left = 56
    Top = 424
    TargetsData = (
      1
      11
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Text'
        0)
      (
        '*'
        'Lines'
        0)
      (
        '*'
        'Title'
        0)
      (
        '*'
        'Filter'
        0)
      (
        '*'
        'Cells'
        0)
      (
        '*'
        'DisplayLabel'
        0)
      (
        'TmmNumEdit'
        'Text'
        0)
      (
        '*'
        'InfoText'
        0))
  end
  object ColorDialog: TmmColorDialog
    Ctl3D = True
    Left = 88
    Top = 384
  end
  object FontDialog: TmmFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'System'
    Font.Style = []
    MinFontSize = 0
    MaxFontSize = 0
    Left = 56
    Top = 384
  end
  object timMinMax: TmmTimer
    Enabled = False
    Interval = 300
    OnTimer = timMinMaxTimer
    Left = 24
    Top = 464
  end
  object mOpenDialog: TmmOpenDialog
    Options = [ofReadOnly, ofHideReadOnly, ofFileMustExist]
    Left = 24
    Top = 384
  end
  object pmListView: TmmPopupMenu
    Left = 56
    Top = 464
    object pmSelectAll: TMenuItem
      Caption = '(*)Alles markieren'
      ShortCut = 16449
      OnClick = pmSelectAllClick
    end
  end
  object timScrollGrid: TmmTimer
    Enabled = False
    Interval = 300
    OnTimer = timScrollGridTimer
    Left = 24
    Top = 496
  end
end
