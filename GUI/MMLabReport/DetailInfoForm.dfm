inherited frmDetailInfo: TfrmDetailInfo
  Left = 867
  Top = 119
  BorderStyle = bsNone
  ClientHeight = 243
  ClientWidth = 231
  OnClose = FormClose
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  object pnMain: TmmPanel
    Left = 0
    Top = 0
    Width = 231
    Height = 243
    Align = alClient
    Color = clInfoBk
    TabOrder = 0
    OnMouseDown = ControlsMouseDown
    OnMouseMove = ControlsMouseMove
    object laYTitle: TmmLabel
      Left = 8
      Top = 4
      Width = 140
      Height = 13
      AutoSize = False
      Caption = '0'
      Visible = True
      OnMouseDown = ControlsMouseDown
      OnMouseMove = ControlsMouseMove
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel2: TmmLabel
      Left = 8
      Top = 20
      Width = 140
      Height = 13
      AutoSize = False
      Caption = '(15)X-Achse'
      Visible = True
      OnMouseDown = ControlsMouseDown
      OnMouseMove = ControlsMouseMove
      AutoLabel.LabelPosition = lpLeft
    end
    object laYValue: TmmLabel
      Left = 130
      Top = 4
      Width = 6
      Height = 13
      Caption = '0'
      Visible = True
      OnMouseDown = ControlsMouseDown
      OnMouseMove = ControlsMouseMove
      AutoLabel.LabelPosition = lpLeft
    end
    object laXValue: TmmLabel
      Left = 130
      Top = 20
      Width = 6
      Height = 13
      Caption = '0'
      Visible = True
      OnMouseDown = ControlsMouseDown
      OnMouseMove = ControlsMouseMove
      AutoLabel.LabelPosition = lpLeft
    end
    object tvDetails: TmmTreeView
      Left = 1
      Top = 39
      Width = 229
      Height = 203
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderStyle = bsNone
      Color = clInfoBk
      Indent = 19
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      Visible = True
      OnMouseDown = ControlsMouseDown
      OnMouseMove = ControlsMouseMove
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 16
    Top = 40
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
  object timClose: TmmTimer
    Enabled = False
    Interval = 120000
    OnTimer = timCloseTimer
    Left = 48
    Top = 40
  end
end
