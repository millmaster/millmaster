(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: Main.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: MDI oder SDI GUI Tamplate
|                 Style mit Formproperty 'FormStyle' setzen [fsMDIForm,fsNormal]
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.11.2003        Wss | Nach Login Wechsel wird RefreshUserInfo aufgerufen
|=============================================================================*)
unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEAPPLMAIN, Menus, mmMainMenu, IvDictio, IvAMulti, IvBinDic,
  mmDictionary, IvMulti, IvEMulti, mmTranslator, mmLogin, mmPopupMenu,
  mmOpenDialog, IvMlDlgs, mmSaveDialog, mmPrintDialog,
  mmPrinterSetupDialog, StdActns,
  ActnList, mmActionList, ImgList, mmImageList, ComCtrls, mmStatusBar,
  ToolWin, mmToolBar, mmAnimate, ExtCtrls, mmPanel, MMSecurity, StdCtrls,
  LabMasterDef, Buttons, SettingsIniDB, MMHtmlHelp;

type
  TfrmMainForm = class (TBaseApplMainForm)
    acSecurity: TAction;
    MMSecurityControl: TMMSecurityControl;
    MMSecurityDB: TMMSecurityDB;
    Zugriffkontrolle1: TMenuItem;
    MMHtmlHelp: TMMHtmlHelp;
    Neu1: TMenuItem;
    procedure acNewExecute(Sender: TObject);
    procedure acSecurityExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure mDictionaryAfterLangChange(Sender: TObject);
    procedure Neu1Click(Sender: TObject);
  private
  protected
    procedure UpdateSecurity; override;
  end;

var
  frmMainForm: TfrmMainForm;

implementation
{$R *.DFM}
uses
  DataForm;

const
  cUnusedComponents: Array[1..16] of String = (
    'acOpen', 'acSave', 'acSaveas', 'acPreview', 'acPrint', 'miPrinterSetup',
    'acWindowCascade', 'acWindowTileVertical', 'acTileHorizontally',
    'miOpen', 'miSave', 'miSaveAs', 'miPreview', 'miPrint', 'miPrinterSetup', 'miFenster'
  );

//------------------------------------------------------------------------------
{
********************************* TfrmMainForm *********************************
}
procedure TfrmMainForm.acNewExecute(Sender: TObject);
begin
  with TfrmLabReport.Create(Self) do
    Show;

//  with TfrmLabReport.Create(Self) do
//  try
//    ShowModal;
//  finally
//    Free;
//  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMainForm.acSecurityExecute(Sender: TObject);
begin
  MMSecurityControl.Configure;
end;
//------------------------------------------------------------------------------
procedure TfrmMainForm.FormCreate(Sender: TObject);
var
  i: Integer;
  xComp: TComponent;
begin
  // cleanup unused components: TActions and TMenuItems
  for i:=1 to High(cUnusedComponents) do begin
    xComp := FindComponent(cUnusedComponents[i]);
    if Assigned(xComp) then
      FreeAndNil(xComp);
  end;
  HelpContext := GetHelpContext('Allgemein\MM_LabMaster.htm');
end;
//------------------------------------------------------------------------------
procedure TfrmMainForm.FormShow(Sender: TObject);
begin
//  acNew.Execute;  
{
  with TfrmDataForm.Create(Self) do begin
    Parent := Self;
    BorderStyle := bsNone;
    Align := alClient;
    Show;
  end;
{}
end;
//------------------------------------------------------------------------------
procedure TfrmMainForm.mDictionaryAfterLangChange(Sender: TObject);
begin
  MMHtmlHelp.LoepfeIndex := mDictionary.LoepfeIndex;
end;
//------------------------------------------------------------------------------
procedure TfrmMainForm.Neu1Click(Sender: TObject);
begin
  with TfrmLabReport.Create(Self) do
    OpenReport('');
end;

procedure TfrmMainForm.UpdateSecurity;
begin
  MMSecurityDB.ReloadUserInfo;
end;

end.

