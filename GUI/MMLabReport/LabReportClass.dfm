object dmLabReport: TdmLabReport
  OldCreateOrder = False
  Left = 478
  Top = 121
  Height = 479
  Width = 741
  object conDefault: TmmADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=netpmek32;User ID=MMSystemSQL;Initi' +
      'al Catalog=MM_Winding;Data Source=WETPC1148;Use Procedure for Pr' +
      'epare=1;Auto Translate=False;Packet Size=4096;Workstation ID=WET' +
      'PC1148;Use Encryption for Data=False;Tag with column collation w' +
      'hen possible=False'
    LoginPrompt = False
    Mode = cmRead
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 24
    Top = 8
  end
  object dseChart: TmmADODataSet
    Active = True
    Connection = conDefault
    CursorType = ctStatic
    Filter = 'prod_name='#39'customer D'#39
    BeforeOpen = dseBeforeOpen
    OnCalcFields = DataSetCalcFields
    CommandText = 
      'select  (v.c_time) as "time",(v.c_prod_name) as "prod_name",SUM(' +
      'v.c_CYTot) as "CYTot",SUM(v.c_SFI) as "SFI",SUM(v.c_Len) as "Len' +
      '",SUM(v.c_SFICnt) as "SFICnt",SUM(v.c_AdjustBase) as "AdjustBase' +
      '",SUM(v.c_rtSpd) as "rtSpd",SUM(v.c_wtSpd) as "wtSpd",SUM(v.c_Ad' +
      'justBaseCnt) as "AdjustBaseCnt",SUM(v.c_CN) as "CN",SUM(v.c_CS) ' +
      'as "CS" from v_production_shift v where v.c_shiftcal_id = 1 and ' +
      'v.c_style_id in ('#39'89'#39')'#13#10'and v.c_machine_id in ('#39'4'#39','#39'6'#39')'#13#10'  Group' +
      ' by c_prod_name,c_time Order by c_prod_name ASC,c_time ASC'
    Parameters = <>
    Left = 24
    Top = 56
  end
  object dseWork: TmmADODataSet
    Tag = 1
    Connection = conDefault
    CursorType = ctStatic
    BeforeOpen = dseBeforeOpen
    Parameters = <>
    Left = 240
    Top = 56
  end
  object dseGrid: TmmADODataSet
    Tag = 1
    Connection = conDefault
    CursorType = ctStatic
    BeforeOpen = dseBeforeOpen
    OnCalcFields = DataSetCalcFields
    Parameters = <>
    Left = 96
    Top = 56
  end
  object dseGridDetail: TmmADODataSet
    Tag = 1
    Connection = conDefault
    BeforeOpen = dseBeforeOpen
    OnCalcFields = DataSetCalcFields
    Parameters = <>
    Left = 168
    Top = 56
  end
  object slTemplateSettings: TmmVCLStringList
    Strings.Strings = (
      '[ReportOptions]'
      'CompanyName=Company name'
      'ReportTitle=Report title'
      'ShowExtend=0'
      'UseYMSettings=0'
      'ShowStyle=1'
      'ShowLot=1'
      'ShowMachine=1'
      'ShowYMSettings=1'
      'PrintChart=1'
      'PrintQMatrix=1'
      'PrintCluster=0'
      'PrintSpliceMatrix=1'
      'PrintFFMatrix=1'
      'PrintYMSettings=0'
      'PrintTable=0'
      'StretchTable=1'
      'NewPageMatrix=0'
      'NewPageYMSettings=0'
      'NewPageTable=0'
      'RectChart=-1,-1,-1,-1'
      'RectQMatrix=-1,-1,-1,-1'
      'RectSpliceMatrix=-1,-1,-1,-1'
      'RectFFMatrix=-1,-1,-1,-1'
      ''
      '[ChartOptions]'
      'BarStyle=1'
      'LegendAlign=2'
      'View3D=0'
      'AllowRotate=0'
      'Width3D=25'
      'RotateX=350'
      'RotateY=350'
      'StartColor=16777215'
      'EndColor=16777215'
      'ImageInside=0'
      'ImageFile='
      'Zoom=90'
      'HPos=0'
      'VPos=0'
      'XLabelRotated=0'
      'AxisInfo0=Scaling=0,Min=0,Max=0'
      'AxisInfo1=Scaling=0,Min=0,Max=0'
      'AxisInfo2=Scaling=0,Min=0,Max=0'
      'AxisInfo3=Scaling=0,Min=0,Max=0'
      'XAxis=time'
      'ZAxis='
      ''
      '[MatrixOptions]'
      'ChannelVisible=1'
      'ClusterVisible=1'
      'SpliceVisible=1'
      'DefectValues=1'
      'CutValues=1'
      'ActiveVisible=1'
      'ActiveColor=16776960'
      'InactiveColor=9502719'
      'MatrixMode=0'
      ''
      '[GridOptions]'
      'Options=11'
      'FixedCols=0'
      'Hierarchy=0'
      'TitleFontName=MS Sans Serif'
      'TitleFontColor=-2147483640'
      'TitleFontBkColor=-2147483633'
      'TitleFontSize=8'
      'TitleFontStyle=0'
      'DataFontName=MS Sans Serif'
      'DataFontColor=-2147483640'
      'DataFontBkColor=-2147483643'
      'DataFontSize=8'
      'DataFontStyle=0'
      ''
      '[DataItems]'
      'DataItemsCount=10'
      
        'Item0=ItemName=CYTot,DisplayName=CYTot,ClassDefects=0,Alignment=' +
        '0,AverageStep=2,AxisType=0,ChartIndex=1,ChartType=1,Col=0,Color=' +
        '8388863,Commas=1,DescendingSort=0,DisplayWidth=44,LowerLimit=0,L' +
        'owerLimitMode=0,RightAxis=0,ShowAverage=0,ShowInChart=-1,ShowInT' +
        'able=-1,ShowSeconds=0,Statistic=0,TableIndex=1,Thousands=0,Upper' +
        'Limit=0,UpperLimitMode=0'
      
        'Item1=ItemName=machine_name,DisplayName=(15)Maschine,ClassDefect' +
        's=0,Alignment=0,AverageStep=2,AxisType=0,ChartIndex=0,ChartType=' +
        '2,Col=0,Color=-2147483624,Commas=0,DescendingSort=0,DisplayWidth' +
        '=104,LowerLimit=0,LowerLimitMode=0,RightAxis=0,ShowAverage=0,Sho' +
        'wInChart=0,ShowInTable=-1,ShowSeconds=0,Statistic=0,TableIndex=0' +
        ',Thousands=0,UpperLimit=0,UpperLimitMode=0'
      
        'Item2=ItemName=SFI,DisplayName=SFI,ClassDefects=0,Alignment=0,Av' +
        'erageStep=0,AxisType=0,ChartIndex=0,ChartType=2,Col=0,Color=1677' +
        '7041,Commas=0,DescendingSort=0,DisplayWidth=64,LowerLimit=0,Lowe' +
        'rLimitMode=0,RightAxis=0,ShowAverage=0,ShowInChart=0,ShowInTable' +
        '=0,ShowSeconds=0,Statistic=0,TableIndex=0,Thousands=0,UpperLimit' +
        '=0,UpperLimitMode=0'
      
        'Item3=ItemName=Len,ClassDefects=0,Alignment=0,AverageStep=0,Axis' +
        'Type=0,ChartIndex=0,ChartType=2,Col=0,Color=0,Commas=0,Descendin' +
        'gSort=0,DisplayWidth=64,LowerLimit=0,LowerLimitMode=0,RightAxis=' +
        '0,ShowAverage=0,ShowInChart=0,ShowInTable=0,ShowSeconds=0,Statis' +
        'tic=0,TableIndex=0,Thousands=0,UpperLimit=0,UpperLimitMode=0'
      
        'Item4=ItemName=SFICnt,ClassDefects=0,Alignment=0,AverageStep=0,A' +
        'xisType=0,ChartIndex=0,ChartType=2,Col=0,Color=0,Commas=0,Descen' +
        'dingSort=0,DisplayWidth=64,LowerLimit=0,LowerLimitMode=0,RightAx' +
        'is=0,ShowAverage=0,ShowInChart=0,ShowInTable=0,ShowSeconds=0,Sta' +
        'tistic=0,TableIndex=0,Thousands=0,UpperLimit=0,UpperLimitMode=0'
      
        'Item5=ItemName=AdjustBase,ClassDefects=0,Alignment=0,AverageStep' +
        '=0,AxisType=0,ChartIndex=0,ChartType=2,Col=0,Color=0,Commas=0,De' +
        'scendingSort=0,DisplayWidth=64,LowerLimit=0,LowerLimitMode=0,Rig' +
        'htAxis=0,ShowAverage=0,ShowInChart=0,ShowInTable=0,ShowSeconds=0' +
        ',Statistic=0,TableIndex=0,Thousands=0,UpperLimit=0,UpperLimitMod' +
        'e=0'
      
        'Item6=ItemName=rtSpd,ClassDefects=0,Alignment=0,AverageStep=0,Ax' +
        'isType=0,ChartIndex=0,ChartType=2,Col=0,Color=0,Commas=0,Descend' +
        'ingSort=0,DisplayWidth=64,LowerLimit=0,LowerLimitMode=0,RightAxi' +
        's=0,ShowAverage=0,ShowInChart=0,ShowInTable=0,ShowSeconds=0,Stat' +
        'istic=0,TableIndex=0,Thousands=0,UpperLimit=0,UpperLimitMode=0'
      
        'Item7=ItemName=wtSpd,ClassDefects=0,Alignment=0,AverageStep=0,Ax' +
        'isType=0,ChartIndex=0,ChartType=2,Col=0,Color=0,Commas=0,Descend' +
        'ingSort=0,DisplayWidth=64,LowerLimit=0,LowerLimitMode=0,RightAxi' +
        's=0,ShowAverage=0,ShowInChart=0,ShowInTable=0,ShowSeconds=0,Stat' +
        'istic=0,TableIndex=0,Thousands=0,UpperLimit=0,UpperLimitMode=0'
      
        'Item8=ItemName=AdjustBaseCnt,ClassDefects=0,Alignment=0,AverageS' +
        'tep=0,AxisType=0,ChartIndex=0,ChartType=2,Col=0,Color=0,Commas=0' +
        ',DescendingSort=0,DisplayWidth=64,LowerLimit=0,LowerLimitMode=0,' +
        'RightAxis=0,ShowAverage=0,ShowInChart=0,ShowInTable=0,ShowSecond' +
        's=0,Statistic=0,TableIndex=0,Thousands=0,UpperLimit=0,UpperLimit' +
        'Mode=0'
      
        'Item9=ItemName=PEff,DisplayName=%EffP,ClassDefects=0,Alignment=0' +
        ',AverageStep=0,AxisType=1,ChartIndex=0,ChartType=2,Col=0,Color=1' +
        '6771538,Commas=0,DescendingSort=0,DisplayWidth=64,LowerLimit=0,L' +
        'owerLimitMode=0,RightAxis=0,ShowAverage=0,ShowInChart=-1,ShowInT' +
        'able=0,ShowSeconds=0,Statistic=0,TableIndex=0,Thousands=0,UpperL' +
        'imit=0,UpperLimitMode=0'
      ''
      '[Selection]'
      
        'Selection=TimeMode=1,StyleID=119,"LotID=-2147481584,-2147481566,' +
        '-2147481547,-2147481530","MachID=3,4",FixedTimeRange=0,TimeFrom=' +
        '37463,TimeTo=37828.9999884259'
      ''
      '')
    Left = 40
    Top = 128
  end
  object slQOTestParameter: TmmVCLStringList
    Strings.Strings = (
      '[Basic]'
      'DataItems=cA1,uA1,bA1,xCN,xI20_70,xISmall,xIThin'
      'Active=bA1'
      'xxActive=xISmall'
      'XAxis=time'
      'ZAxis=prod_name'
      'StyleID=73'
      'xTimeFrom=37999.2291666667'
      'xTimeTo=38005.2291666667'
      'xEventTime=38002.2291666667'
      'LotID=-2147481491'
      '[cA1]'
      'ClassDefects=0'
      'MethodCount=1'
      
        'Method0=Method=1,BorderCondition=1,LengthUnit=0,LenBase=2,Deviat' +
        'ion=-10,MinProdYarnLength=105000,ShiftCompression=2'
      '[uA1]'
      'ClassDefects=1'
      'MethodCount=1'
      
        'Method0=Method=1,BorderCondition=1,LengthUnit=0,LenBase=2,Deviat' +
        'ion=-10,MinProdYarnLength=105000,ShiftCompression=2'
      '[bA1]'
      'ClassDefects=2'
      'MethodCount=1'
      
        'Method0=Method=1,BorderCondition=1,LengthUnit=0,LenBase=2,Deviat' +
        'ion=-10,MinProdYarnLength=105000,ShiftCompression=2'
      '[xCN]'
      'MethodCount=1'
      
        'Method0=Method=1,BorderCondition=1,LengthUnit=0,LenBase=2,Deviat' +
        'ion=-10,MinProdYarnLength=105000,ShiftCompression=2'
      '[xI20_70]'
      'MethodCount=1'
      
        'Method0=Method=0,BorderCondition=1,LengthUnit=0,LenBase=2,LimitV' +
        'alue=0.001'
      '[xISmall]'
      'MethodCount=1'
      
        'Method0=Method=0,BorderCondition=1,LengthUnit=0,LenBase=2,LimitV' +
        'alue=1'
      '[xIThin]'
      'MethodCount=1'
      
        'Method0=Method=0,BorderCondition=1,LengthUnit=0,LenBase=2,LimitV' +
        'alue=1'
      ''
      ' '
      ' ')
    Left = 40
    Top = 184
  end
end
