(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_ConfigMsgForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Der Benutzer kann Meldungen konfigurieren, welche wichtige
|                 Meldungen von Appl. anzeigen.
|                 Die Anzeige wird durch Auswahl der Checkboxen vorgenommen.
| Info..........:
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.06.2001  0.00  Sdo | Datei erstellt
|=============================================================================*)
unit u_ConfigMsgForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, StdCtrls, Buttons, mmBitBtn, mmCheckBox, mmGroupBox,
  IvDictio, IvMulti, IvEMulti, mmTranslator, mmButton;

type
  TConfigMsgForm = class(TDialogBottom)
    mmGroupBox1: TmmGroupBox;
    cbQOfflimitMSG: TmmCheckBox;
    mmTranslator1: TmmTranslator;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure bOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure GetMsgSettings;
    procedure SetMsgSettings;
  public
    { Public declarations }
  end;

var
  ConfigMsgForm: TConfigMsgForm;

implementation // 15.07.2002 added mmMBCS to imported units

{$R *.DFM}
uses
  mmMBCS,
 LoepfeGlobal;

//------------------------------------------------------------------------------
procedure TConfigMsgForm.FormCreate(Sender: TObject);
begin
  inherited;
  GetMsgSettings;
end;
//------------------------------------------------------------------------------
procedure TConfigMsgForm.FormDestroy(Sender: TObject);
begin
  inherited;
  //
end;
//------------------------------------------------------------------------------
procedure TConfigMsgForm.GetMsgSettings;
begin
  cbQOfflimitMSG.Checked:=
              GetRegBoolean(cRegLM, cRegMMCommonPath, cRegMsgQOfflimit, FALSE );
end;
//------------------------------------------------------------------------------
procedure TConfigMsgForm.SetMsgSettings;
begin
  SetRegBoolean(cRegLM, cRegMMCommonPath, cRegMsgQOfflimit, cbQOfflimitMSG.Checked);
end;
//------------------------------------------------------------------------------
procedure TConfigMsgForm.bOKClick(Sender: TObject);
begin
  SetMsgSettings;
  inherited;     
end;
//------------------------------------------------------------------------------
end.
