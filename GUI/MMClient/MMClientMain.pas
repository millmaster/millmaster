(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MMClientMain.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Hauptprogramm
| Info..........:
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 05.10.1998  0.00  Sdo | Projekt erstellt
| 18.03.2003  1.00  Sdo | SendMsgToMMGuard:  mIPCClient wird max. 3 mal
|                       | aufgerufen, wenn keine Verbindung zu MMGuard
| 30.08.2003  1.01  Sdo | bbStart & bbStopt( TBitBtn ) durch  TmmSpeedButton ausgetauscht
| 19.12.2003  1.01  SDo | GetAlarmSettings in Func. geaendert -> Popup erstellen, wenn QOfflimit Alarm gesetzt
| 18.02.2004  1.01  Wss | Bei einem MM Restart wird lediglich X Starting und 1 x Started gesendet. Beim
                          Starting wird ein Flag gesetzt, wo bei Started gepr�ft wird, um den Autoprint
                          1x neu zu starten, falls er bereits lief.
| 20.02.2004  1.01  Wss | Bei Stop/Start wird nun "Computername/Winlogin (MMLogin)" mitgegeben
| 15.06.2004  1.01  Wss | Eigener Bug mit LogonUser behoben
| 21.02.2005  1.01  SDo | Autoprint Service stoppen bei MM Restart ( acAutoprintStop.Execute in  cMMStopping & cMMStopped
|                       | Neues Label fuer MMStart-Zeit
| 10.03.2005  1.01  Wss | Floor Autoprint Kontrolle ignorierbar per DebugKey in Registry "IgnoreFloorRegistry"
| 21.06.2005  1.01  Wss | Timeout f�r UpdateFloor von 10s auf 5s gek�rzt
| 19.10.2005  1.01  Wss | 5 auf 5000 im timUpdateFloor Timer Property Interval ge�ndert
|=============================================================================*)
unit MMClientMain;


interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, Menus, Buttons, ExtCtrls,
  Broadcast, mmEventLog, IPCClass, LoepfeGlobal, BaseGlobal, MMMessages,
  mmLogin, mmPanel, ActnList, mmActionList, mmPopupMenu,
  IvDictio, IvMulti, IvEMulti, IvAMulti, IvBinDic, MMSecurity,
  BaseForm, mmDictionary, mmTranslator, ImgList, mmImageList, mmButton,
  mmMainMenu, mmTrayIcon, mmSpeedButton, ComCtrls, ToolWin, mmToolBar, {jpeg,}
  mmBevel, mmImage, mmLabel, mmTimer, {mmCoolBar, AbstrSec, }
  fcStatusBar, VCLThread, mmcs;

type
  TMillMasterState = (msAlarm, msStopped, msWaiting, msStarted, msNoConnect);

  TFMMClientMain = class(TForm)
    acAutoprintStart: TAction;
    acAutoprintStop: TAction;
    acConfigMsg: TAction;
    acDebug: TAction;
    acHideClose: TAction;
    acLogin: TAction;
    acLoginButton: TAction;
    acMMClient: TAction;
    acSecurity: TAction;
    acStart: TAction;
    acStop: TAction;
    acTrayIconChange: TAction;
    Anmelden1: TMenuItem;
    bbStart: TmmSpeedButton;
    bbStop: TmmSpeedButton;
    ilButtonBMPs: TmmImageList;
    ilDataAquisation: TmmImageList;
    ilHighLevel: TmmImageList;
    ilNormalLevel: TmmImageList;
    imgMain: TmmImage;
    Label3: TLabel;
    Label4: TLabel;
    laDate: TLabel;
    laNextIntervall: TmmLabel;
    laNextIntervallValue: TmmLabel;
    laNextShift: TmmLabel;
    laNextShiftValue: TmmLabel;
    laTime: TLabel;
    mActionList: TmmActionList;
    mDictionary: TmmDictionary;
    mmBevel1: TmmBevel;
    mmBevel2: TmmBevel;
    MMClient1: TMenuItem;
    MMLogin: TMMLogin;
    MMSecurityControl: TMMSecurityControl;
    MMSecurityDB: TMMSecurityDB;
    timCircular: TmmTimer;
    mmTrayIcon: TmmTrayIcon;
    mTranslator: TmmTranslator;
    N3: TMenuItem;
    N4: TMenuItem;
    pLogo: TPanel;
    pmTrayIcon: TmmPopupMenu;
    StatusBar: TfcStatusBar;
    tbAutoPrintStart: TmmSpeedButton;
    tbAutoPrintStop: TmmSpeedButton;
    tbConfigMsg: TmmSpeedButton;
    tbDebug: TmmSpeedButton;
    tbExit: TmmSpeedButton;
    tbLogin: TmmSpeedButton;
    tbSecurity: TmmSpeedButton;
    Terminate1: TMenuItem;
    timTimeOutReset: TmmTimer;
    timUpdateFloor: TmmTimer;
    Label1: TLabel;
    mmImage2: TmmImage;
    procedure acAutoprintStartExecute(Sender: TObject);
    procedure acAutoprintStopExecute(Sender: TObject);
    procedure acConfigMsgExecute(Sender: TObject);
    procedure acDebugExecute(Sender: TObject);
    procedure acHideCloseExecute(Sender: TObject);
    procedure acLoginExecute(Sender: TObject);
    procedure acMMClientExecute(Sender: TObject);
    procedure acSecurityExecute(Sender: TObject);
    procedure acStartStopExecute(Sender: TObject);
    procedure acTerminateExecute(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure timCircularTimer(Sender: TObject);
    procedure timTimeOutResetTimer(Sender: TObject);
    procedure timUpdateFloorTimer(Sender: TObject);
    procedure mActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
  private
    mAutoprintIsRunning: Boolean;
    mBarcoAutoPrintServiceName: string;
    mBroadcastThread: TBroadCastReader;
    mIcon: TIcon;
    mIconID: Integer;
    mInAquisation: Boolean;
    mIPCClient: TIPCClient;
    mLog: TEventLogWriter;
    mLogonUserActive: Boolean;
    mMessageApplID: THandle;
    mMMClientID: DWord;
    mMMIsNewStarting: Boolean;
    mMMState: TMillMasterState;
    mNTUser: string;
//    mStatusAutoprint: string;
    mUpdateFloorTimeStamp: Integer;
    procedure CheckFloorRegistry;
    procedure CheckLogonUser;
    procedure FillTrayIcon(aStart: Boolean);
    function GetAlarmSettings: boolean;
    // 1 Ermittelt den AutoPrint Status beim Aufstart und dblClick TryIcon
    procedure GetAutoPrintStatus;
    function GetBarcoService: string;
    procedure GetMMState;
    function QueryAutoPrintState(aService: string): string;
    procedure ResetTrayIcon;
    // 1 Next Interval & next Shift abfragen
    procedure SendMSGToJobHandler;
    function SendMsgToMMGuard(aMsg: TMMGuardRec): Boolean;
    procedure SetMMState(const Value: TMillMasterState);
    // 1 Ermoeglicht das Starten/Stoppen des Autoprintservices
    function SetServicePrivileg: Boolean;
    procedure ShowStartMode(aText: string);
    procedure ShowStatusText(aText: string);
    function StartAutoPrintServcie(aService: string): Boolean;
    function StopAutoPrintServcie(aService: string): Boolean;
    procedure WriteUserDataInRegistry;
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure AfterConstruction; override;
  end;

procedure FindFile(aSearchMask: string; var aFoundedFiles: TStringList);

var
  FMMClientMain: TFMMClientMain;

//const   cBarcoServiceOld    = 'Barco Automatic Output';
//        cBarcoServiceNew    = 'BarcoAutomaticOutput';

const SE_TCB_NAME = 'SeTcbPrivilege';

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
//{$R MMBMP.res}
uses
  mmMBCS,

  mmRegistry, NetAPI32MM, shellapi, RzCSIntf, PopupForm,
  u_MMClientText, u_ConfigMsgForm, Psapi,
  WinSvc, AdvApi32;

type
  TFloorConfigRec = record
    RegPath: string;
    Value: string;
  end;

const
  cTimeOutReset = 10 * 60000; //10 Min.
  // Berechnungen und Autoprint Dialog werden nur fuer diese MillMaster Gruppe freigegeben
  cFloorConfigGroup = 'MMAdminGroup';
  cFloorConfigCnt = 2;
  // Diese Werte in der Registry werden per MMClient kontrolliert
  // -> Freigabe FIX mit MMAdminGroup (siehe cFloorConfigGroup)
  cFloorConfig: array[0..cFloorConfigCnt - 1] of TFloorConfigRec = (
    // Automatic Output Menu in File von Floor
    (RegPath: 'Software\BARCO\addons\BARCO.AutoOutputPlugin.1'; Value: 'MenusEnabled'),
    // OK button in Calculation Dialog um Aenderungen zu speichern
    (RegPath: 'Software\BARCO\addons\BARCO.RPICalcPlugin.1'; Value: 'ReadWrite')
    );
  // ueber False/True wird der DWord Wert fuer die Registry bestimmt
  cRegDWordValues: array[Boolean] of Integer = (0, 1);

//------------------------------------------------------------------------------

//******************************************************************************
// Sucht Dateien und fuegt sie in eine Stringliste
//******************************************************************************
procedure FindFile(aSearchMask: string; var aFoundedFiles: TStringList);

  procedure GetAllFiles(aMask: string);
  var xSearch: TSearchRec;
    xPath, xFile: string;
  begin
    xFile := ExtractFileName(aMask);
    xPath := ExtractFilePath(aMask);
    xPath := IncludeTrailingBackslash(xPath);

    // first search all files in current path
    if FindFirst(aMask, faAnyFile and not faDirectory, xSearch) = 0 then begin
      repeat
        aFoundedFiles.Add(xPath + xSearch.Name);
      until FindNext(xSearch) <> 0;
      FindClose(xSearch);
    end;
    // next search for sub directories
    if FindFirst(xPath + '*.*', FaDirectory, xSearch) = 0 then begin
      repeat
        if ((xSearch.Attr and FaDirectory) = FaDirectory) and
          (xSearch.Name[1] <> '.') then
          GetAllFiles(xPath + xSearch.Name + '\' + xFile);
      until FindNext(xSearch) <> 0;
      FindClose(xSearch);
    end;
  end;
begin
  GetAllFiles(aSearchMask);
end;

//:-------------------------------------------------------------------
constructor TFMMClientMain.Create(aOwner: TComponent);
//var
//  xUserName: array[0..100] of Char;
//  xSize: DWord;
begin
  EnterMethod('Create');
  inherited Create(aOwner);

  //Loepfe Farbe setzen
  pLogo.Color := RGB(255, 204, 0);
  tbConfigMsg.Glyph.TransparentColor := clMaroon;

  mAutoprintIsRunning := False;
  mMMIsNewStarting    := False;

  mInAquisation := False;
  mUpdateFloorTimeStamp := 0;
  try
    mIcon := TIcon.Create;
    mIconID := 0;
    mIcon.Assign(mmTrayIcon.Icon);
    SetMMState(msStopped);

//wss: f�r was 3.5 sek warten?    Sleep(3500);

    mMMClientID := RegisterWindowMessage(cMsgClient);
    mMessageApplID := RegisterWindowMessage(cMsgAppl);

    // get current NT user
    mNTUser := GetWindowsUserName;

    // clear labels
    laTime.Caption := '';
    laDate.Caption := '';
    laNextIntervallValue.Caption := cUnknown;
    laNextShiftValue.Caption := cUnknown;

    // check if some registry are available for controlling Floor plugins
    CheckFloorRegistry;
    // check active user group depending on logged in MillMaster user
    WriteUserDataInRegistry; // NTLogon User Daten in Reg. schreiben
    CheckLogonUser;

    mLog := TEventLogWriter.Create(cEventLogClassForSubSystems, gMMHost, ssMMClient,
                                   cEventLogDefaultText[ttMMClientMain], True);

    //User and Workstation in Log File
    mLog.Write(etInformation, Format('MMClient started on %s with user %s.', [MMGetComputerName, mNTUser]));

    mBroadcastThread := TBroadCastReader.Create(Handle);
    if mBroadcastThread.Init then
      mBroadcastThread.MessageApplID := mMessageApplID
    else
      mLog.Write(etError, 'Initialize of mBroadcastThread failed: ' + mBroadcastThread.Error);

    // Verbindung zum MMGuard herstellen
    mIPCClient := TIPCClient.Create(gMMHost, cChannelNames[ttMMGuardMain]);

   // GetAlarmSettings;
    GetMMState;

    SendMSGToJobHandler;

    mBarcoAutoPrintServiceName := GetBarcoService;
    GetAutoPrintStatus;

  except
    on E: Exception do begin
      mLog.Write(etError, Format('MMClient has an error in create(). Error : %s.', [E.Message]));
    end;
  end;
end; // TFMMClientMain.Create cat:Undefined

//:-------------------------------------------------------------------
destructor TFMMClientMain.Destroy;
begin
  // Meldung an Appl. (stop von MMClient)
  MMBroadCastMessage(mMessageApplID, cMMClientStopped);
  //Login Informationen loeschen
  MMLogin.Logout;

  // cleanup objects
  mIPCClient.Free;
  mBroadcastThread.Free;
  mLog.Free;
  mIcon.Free;

  inherited Destroy;
end; // TFMMClientMain.Destroy cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.acAutoprintStartExecute(Sender: TObject);
begin
  StartAutoPrintServcie(mBarcoAutoPrintServiceName);
  QueryAutoPrintState(mBarcoAutoPrintServiceName);
end; // TFMMClientMain.acAutoprintStartExecute cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.acAutoprintStopExecute(Sender: TObject);
begin
  StopAutoPrintServcie(mBarcoAutoPrintServiceName);
  QueryAutoPrintState(mBarcoAutoPrintServiceName);
end; // TFMMClientMain.acAutoprintStopExecute cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.acConfigMsgExecute(Sender: TObject);
begin
  with TConfigMsgForm.Create(Self) do try
    ShowModal;
    //GetAlarmSettings;   
  finally
    Free;
  end;
end; // TFMMClientMain.acConfigMsgExecute cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.acDebugExecute(Sender: TObject);
var
  xPath: string;
  xFile, xFile1: string;
  xStartupInfo: TStartupInfo;
  xProcessInfo: TProcessInformation;
  i: LongInt;
  xExename: string;
  PIDs: array[0..1000] of DWord;
  xListSize, xNoOfProcesses: DWord;
  xHandle: THandle;
  xName, xFullname: array[0..MAX_PATH - 1] of char;
  xFoundedFiles: TStringList;
begin
  //  DebugConfig-Pfad ermitteln
  xFile := 'DebugConfig.exe';
  xPath := GetRegString(cRegLM, cRegMillMasterPath, cRegMillMasterRootPath, '');

  xFoundedFiles := TStringList.Create;
  FindFile(xPath + '\' + xFile, xFoundedFiles);

  if xFoundedFiles.Count = 0 then begin
    MessageDLG(rsMsgNoDebugConfig, mtError, [mbOK], 0);
    xFoundedFiles.Free;
    exit;
  end;

  xFile1 := xFoundedFiles.Strings[0];
  xPath := ExtractFilePath(xFile1);
  xFoundedFiles.Free;

    //alter DebugConfig-Process loeschen
  xFile1 := AnsiUpperCase(Trim(xFile1));
  xListSize := SizeOf(PIDs);
  xNoOfProcesses := 0;
  FillChar(PIDs, xListSize, 0);
  if not EnumProcesses(@PIDs, xListSize, xNoOfProcesses) then RaiseLastWin32Error;
  xNoOfProcesses := xNoOfProcesses div SizeOf(DWord);

  for i := 0 to xNoOfProcesses - 1 do begin
    xHandle := OpenProcess(PROCESS_QUERY_INFORMATION + PROCESS_VM_READ +
      PROCESS_TERMINATE, False, PIDs[i]);
    if xHandle = 0 then begin
      CloseHandle(xHandle);
      Continue;
    end;
    FillChar(xName, SizeOf(xName), 0);
    if GetModuleBaseName(xHandle, 0, @xName, sizeof(xName)) > 0 then begin
      GetModuleFileNameEx(xHandle, 0, @xFullname, sizeof(xFullname));
      xExename := AnsiUpperCase(Trim(StrPas(xFullName)));
      if xExename = xFile1 then begin
        TerminateProcess(xHandle, 0);
      end
      else
        CloseHandle(xHandle);
    end
    else
      CloseHandle(xHandle);
  end;

    //Neuer DebugConfig-Process erstellen
  FillChar(xStartupInfo, SizeOf(TStartupInfo), 0);
  xStartupInfo.cb := SizeOf(TStartupInfo);
  GetStartupInfo(xStartupInfo);
  xStartupInfo.wShowWindow := SW_SHOW;
  xStartupInfo.lpReserved := nil;

  if CreateProcess(
    PChar(xPath + '\' + xFile),
    nil,
    nil,
    nil,
    False,
                     //DETACHED_PROCESS or NORMAL_PRIORITY_CLASS,
    NORMAL_PRIORITY_CLASS,
    nil,
    PChar(xPath),
    xStartupInfo,
    xProcessInfo) then begin

  //      WaitForSingleObject(xProcessInfo.hProcess, INFINITE);
    CloseHandle(xProcessInfo.hProcess);
  end;
end; // TFMMClientMain.acDebugExecute cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.acHideCloseExecute(Sender: TObject);
begin
  Application.Minimize;
  Hide;
end; // TFMMClientMain.acHideCloseExecute cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.acLoginExecute(Sender: TObject);
begin
  MMLogin.Execute;
end; // TFMMClientMain.acLoginExecute cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.acMMClientExecute(Sender: TObject);
begin
  // nach GetState sendet MMGuard dass MM gestarted ist. Bei diesem Kommando wird
  // automatisch die SystemInfo abgefragt (gcMMStarted in WndProc)
  GetMMState;
  Application.Restore;
  Application.BringToFront;

  GetAutoPrintStatus;

  //Test -> Toolbar ist manchnchmal nicht mehr sichtbar ??
  //tbClient.Repaint;
  //tbExit.Repaint;

end; // TFMMClientMain.acMMClientExecute cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.acSecurityExecute(Sender: TObject);
begin
  MMSecurityControl.Configure;
end; // TFMMClientMain.acSecurityExecute cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.acStartStopExecute(Sender: TObject);
var
  xMsg: TMMGuardRec;
begin
  FillChar(xMsg, sizeof(xMsg), 0);
  xMsg.Transmitter     := ssApplication;
  xMsg.ComputerAndUser := Format('%s/%s (%s)', [MMGetComputerName, mNTUser, mmSecurityControl.CurrentMMUser]);
//  xMsg.ComputerAndUser := MMGetComputerName + '/' + mmSecurityControl.CurrentMMUser;

  if (Sender as TComponent).Tag = 0 then begin
    xMsg.MsgTyp := gcStartMM;
    SetMMState(msStarted);
  end
  else begin
    xMsg.MsgTyp := gcStopMM;
    SetMMState(msStopped);
  end;

  ShowStartMode('');
  ResetTrayIcon;

  MMBroadCastMessage(mMessageApplID, cMMSystemInfo);
  if SendMsgToMMGuard(xMsg) then begin
    //screen.cursor:= crHourGlass;
    SetMMState(msWaiting);
  end;
end; // TFMMClientMain.acStartStopExecute cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.acTerminateExecute(Sender: TObject);
begin
  Application.Terminate;
end; // TFMMClientMain.acTerminateExecute cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.AfterConstruction;
begin
  //SDO
  //Buttons werden beim 1. Start nicht angezeigt, darum diese Variante (Test)
  try
    MMSecurityDB.LoadSecurityInfo;
    MMSecurityControl.Refresh;
    Self.Refresh; //Start/Stop Buttons zeichnen
    Repaint;
  except
    on E: Exception do
      mLog.Write(etError, Format('MMClient has an error in AfterConstruction(). Error : %s.', [E.Message]));
  end;
end; // TFMMClientMain.AfterConstruction cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.CheckFloorRegistry;
var
  xReg: TmmRegistry;

  procedure WriteDefaults(aKeyName: string; aInt1, aInt2, aInt3, aInt4: Integer);
  begin
    with xReg do begin
      if OpenKey('\Software\BARCO\addons\' + aKeyName, True) then begin
        if aInt1 > 0 then begin
          WriteInteger('MenusEnabled', aInt1);
          WriteInteger('MenusSupported', aInt2);
          WriteInteger('Plugin', aInt3);
          WriteInteger('ReadWrite', aInt4);
          WriteInteger('StatusBarEnabled', 0);
          WriteInteger('StatusBarSupported', 0);
          WriteInteger('TitleEnabled', 0);
          WriteInteger('TitleSupported', 0);
        end;
      end;
    end;
  end;

begin
  xReg := TmmRegistry.Create;
  with xReg do try
    RootKey := cRegCU;
    if not KeyExists('Software\BARCO\addons') then begin
      WriteDefaults('BARCO.AutoOutputPlugin.1', 1, 1, 1, 1);
      WriteDefaults('BARCO.Rpicalc2Plugin.1', 0, 0, 0, 0);
      WriteDefaults('BARCO.RPICalcPlugin.1', 1, 1, 1, 1);
      WriteDefaults('LOEPFE.MillMasterPlugin', 15, 15, 1, 1);
    end;
  finally
    Free;
  end;
end; // TFMMClientMain.CheckFloorRegistry cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.CheckLogonUser;
var
  xStr: string;
  i: Integer;
  xEnable: Boolean;
begin
  mLogonUserActive := (GetRegString(cRegCU, cRegMMSecurityPath, cRegLogonUserName, '') <> '');
  // nun noch die Funktionen fuer die Floor freigeben oder sperren -> Floor muss neu gestartet werden
  with MMSecurityDB do begin
    ReloadUserInfo;
    xStr := UserGroups;
    xEnable := (Pos(cFloorConfigGroup, UserGroups) > 0) or GetRegBoolean(cRegLM, cRegMMDebug, 'IgnoreFloorRegistry', False);
    with TmmRegistry.Create do
    try
      RootKey := cRegCU;
      for i := 0 to cFloorConfigCnt - 1 do begin
        if OpenKey(cFloorConfig[i].RegPath, False) then begin
          WriteInteger(cFloorConfig[i].Value, cRegDWordValues[xEnable]);
          CloseKey;
        end;
      end;
    finally
      Free;
    end;
  end;
end; // TFMMClientMain.CheckLogonUser cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.FillTrayIcon(aStart: Boolean);
begin
  if aStart then begin
    if mLogonUserActive then mIconID := 4
                        else mIconID := 1;

    timCircular.Enabled := True;
    timCircular.OnTimer(Self);
    timTimeOutReset.Enabled := True;
  end;

  mmTrayIcon.Icon.Modified := True;
end; // TFMMClientMain.FillTrayIcon cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = vk_F4) and (Shift = [ssAlt]) then
    tbExit.Click;   
end; // TFMMClientMain.FormKeyDown cat:Undefined

//:-------------------------------------------------------------------
function TFMMClientMain.GetAlarmSettings: boolean;
begin
  result := GetRegBoolean(cRegLM, cRegMMCommonPath, cRegMsgQOfflimit, False);
end; // TFMMClientMain.GetAlarmSettings cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           GetAutoPrintStatus
 *  Klasse:           TFMMClientMain
 *  Kategorie:        Undefined
 *  Argumente:
 *
 *  Kurzbeschreibung: Ermittelt den AutoPrint Status beim Aufstart und dblClick TryIcon
 *  Beschreibung:     -
 ************************************************************************)
procedure TFMMClientMain.GetAutoPrintStatus;
begin
  QueryAutoPrintState(mBarcoAutoPrintServiceName);
end; // TFMMClientMain.GetAutoPrintStatus cat:Undefined

//:-------------------------------------------------------------------
function TFMMClientMain.GetBarcoService: string;
var
  xSCManager: THandle;
  xDisplName: PChar;
  xBuffer: Dword;
  xServiceName: array[0..255] of char;
begin
  Result := '';
  xSCManager := OpenSCManager(nil, nil, SC_MANAGER_CONNECT);
  xDisplName := 'Barco Automatic Output';
  xBuffer := 256;
  if GetServiceKeyName(xSCManager, xDisplName, xServiceName, xBuffer) then
    Result := xServiceName;

  CloseServiceHandle(xSCManager);
end; // TFMMClientMain.GetBarcoService cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.GetMMState;
var
  xMsg: TMMGuardRec;
begin
  FillChar(xMsg, sizeof(xMsg), 0);
  xMsg.MsgTyp      := gcGetState;
  xMsg.Transmitter := ssMMClient;
  SendMsgToMMGuard(xMsg);
end; // TFMMClientMain.GetMMState cat:Undefined

//:-------------------------------------------------------------------
function TFMMClientMain.QueryAutoPrintState(aService: string): string;
var
  xStatus: TServiceStatus;
  xService, xSCManager: THandle;
  xBool: Boolean;
  xError: DWord;
  xColor: TColor;
  xSize: DWord;
  xBuf: array[0..4095] of byte;
  xBytesNeeded: DWord;
begin
  Result     := rsAutoprintServiceNotExists;
  mAutoprintIsRunning := False;
  xBool      := False;
  xColor     := clRed;
  xService   := 0;
  xSCManager := 0;
  try
    xSCManager := OpenSCManager(nil, nil, SC_MANAGER_CONNECT);
    SetLastError(0);
    xService := OpenService(xSCManager, PChar(aService), SERVICE_QUERY_STATUS or SERVICE_QUERY_CONFIG);

    if xService = 0 then begin
      xError := GetLastError;
      case xError of
        ERROR_ACCESS_DENIED: Result := rsAutoprint_ACCESS_DENIED;
        ERROR_SERVICE_DOES_NOT_EXIST: xColor := clAqua;
        ERROR_INVALID_NAME:           xColor := clSilver;
      else
      end;
      CodeSite.SendError('QueryAutoPrintState(' + aService + '). Error msg: ' + FormatErrorText(xError));
    end else begin
      if QueryServiceStatus(xService, xStatus) then begin
        case xStatus.dwCurrentState of
          SERVICE_STOPPED:       Result := rsAutoprint_STOPPED;
          SERVICE_START_PENDING: Result := rsAutoprint_START_PENDING;
          SERVICE_STOP_PENDING:  Result := rsAutoprint_STOP_PENDING;
          SERVICE_PAUSED:        Result := rsAutoprint_PAUSED;
          SERVICE_RUNNING: begin
              mAutoprintIsRunning := True;
              Result := rsAutoprint_RUNNING;
              xColor := clLime;
            end;
        end;

        FillChar(xBuf, SizeOf(xBuf), 0);
        if AdvApi32.QueryServiceConfig(xService, PQueryServiceConfig(@xBuf)^, SizeOf(xBuf), xBytesNeeded) then begin
          if PQueryServiceConfig(@xBuf)^.dwStartType = SERVICE_DISABLED then begin
            Result := rsAutoprintServiceDisabled;
            xColor := clYellow;
          end;
        end;
      end; // if QueryServicestatus
    end; // if xService = 0

    //Status setzen
    StatusBar.Panels[2].Color := xColor;
    StatusBar.Panels[2].Hint  := Result;
    StatusBar.Update;
  except
    on e: exception do begin
      StatusBar.Panels[2].Color := clAqua;
      StatusBar.Panels[2].Hint  := rsAutoprintServiceDisabled;
      CodeSite.SendError('QueryAutoPrintState(' + aService + '). Error msg: ' + e.Message);
    end;
  end;
  CloseServiceHandle(xSCManager);
  CloseServiceHandle(xService);
end; // TFMMClientMain.QueryAutoPrintState cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.timCircularTimer(Sender: TObject);
var
  xIcon: TIcon;
begin
  if not mLogonUserActive then begin
    if mIconID >= ilDataAquisation.Count then
      mIconId := 0
    else if mIconId > 3 then
      mIconId := mIconId - 4;
  end
  else if mIconID >= ilDataAquisation.Count then
    mIconID := 4
  else if mIconID < 4 then
    mIconId := mIconId + 4;

  xIcon := TIcon.Create;
  try
    ilDataAquisation.GetIcon(mIconID, xIcon);
    mmTrayIcon.Icon := xIcon;
  finally
    xIcon.Free;
  end;

  //Weiter drehen bis MM wieder 0 Grad (MM ist oben)
  if not mInAquisation and ((mIconID = 3) or (mIconID = 7)) then begin
    ResetTrayIcon;
     //MMBroadCastMessage(mMessageApplID, cMMSystemInfo); // ??
  end;
  inc(mIconID);
end; // TFMMClientMain.mmTimer1Timer cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.ResetTrayIcon;
begin
  timCircular.Enabled     := False;
  timTimeOutReset.Enabled := False;

  if mLogonUserActive then ilHighLevel.GetIcon(3, mIcon)
                      else ilNormalLevel.GetIcon(3, mIcon);

  mmTrayIcon.Icon          := mIcon;
  mmTrayIcon.Icon.Modified := True;
end; // TFMMClientMain.ResetTrayIcon cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           SendMSGToJobHandler
 *  Klasse:           TFMMClientMain
 *  Kategorie:        Undefined
 *  Argumente:
 *
 *  Kurzbeschreibung: Next Interval & next Shift abfragen
 *  Beschreibung:     -
 ************************************************************************)
procedure TFMMClientMain.SendMSGToJobHandler;
var
  xJob: TJobRec;
begin
  //Message to JobHandler -> get SystemInfo (Next Interval, Shift)
  with TIPCClient.Create(gMMHost, cChannelNames[ttJobManager]) do
  try
    xJob.JobTyp := jtBroadcastToClients;
    xJob.JobLen := GetJobDataSize(@xJob);
//    xJob.JobLen := GetJobDataSize(xJob.JobTyp);
    xJob.BroadcastToClients.Event := bcClientStarted;
    Write(@xJob, xJob.JobLen);
  finally
    Free;
  end;
end; // TFMMClientMain.SendMSGToJobHandler cat:Undefined

//:-------------------------------------------------------------------
function TFMMClientMain.SendMsgToMMGuard(aMsg: TMMGuardRec): Boolean;
var x :integer;
begin
  if Assigned(mIPCClient) then begin
    Result := FALSE;
    x:=0;

    repeat
       Result := mIPCClient.Write(@aMsg, sizeof(aMsg));
       if not Result then begin
          Application.ProcessMessages;
          Sleep(200);
          Application.ProcessMessages;
       end;
       inc(x);
    until Result or (x > 3);

    if not Result then begin
       ShowStatusText(rsMsgMMNotConnected);
       ShowStartMode('');
       ResetTrayIcon;
       SetMMState(msNoConnect);
    end;

    {
    Result := mIPCClient.Write(@aMsg, sizeof(aMsg));
    if not Result then begin
      ShowStatusText(rsMsgMMNotConnected);
      ShowStartMode('');
      ResetTrayIcon;
      SetMMState(msNoConnect);
    end;
    }
  end else
    Result := False;
end; // TFMMClientMain.SendMsgToMMGuard cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.SetMMState(const Value: TMillMasterState);
var
  xIcon: TIcon;
begin
  mMMState := Value;

  xIcon := TIcon.Create;
  if mLogonUserActive then
    ilHighLevel.GetIcon(Integer(mMMState), xIcon)
  else
    ilNormalLevel.GetIcon(Integer(mMMState), xIcon);
  mmTrayIcon.Icon := xIcon;
  xIcon.Free;

  acStart.Enabled := (mMMState in [msAlarm, msStopped]) and MMSecurityControl.CanEnabled(acStart);
  acStop.Enabled  := (mMMState in [msAlarm, msStarted]) and MMSecurityControl.CanEnabled(acStop);
end; // TFMMClientMain.SetMMState cat:Undefined

//:-------------------------------------------------------------------
(*  Member:           SetServicePrivileg
 *  Klasse:           TFMMClientMain
 *  Kategorie:        Undefined
 *  Argumente:
 *
 *  Kurzbeschreibung: Ermoeglicht das Starten/Stoppen des Autoprintservices
 *  Beschreibung:     Ermoeglicht das Starten/Stoppen des Autoprintservices,
                      wenn der LogonUser in der MMUsergroup vorhanden ist und
                      diese Gruppe das Privileg SE_TCB_NAME besitzt !!
                      Info: Um SE_TCB_NAME auf einen User oder Gruppe setzen zu koennen,
                            benoetigt der LogonUser das Privileg SE_TCB_NAME.
                      Beachte:  nach SetServicePrivileg;
                                     ...
                                     ...
                                     RevertToSelf; aufrufen. (Temp. Privileg wird wieder abgegeben)
 ************************************************************************)
function TFMMClientMain.SetServicePrivileg: Boolean;
var
  xToken, xUserToken: THandle;
  xBufLen: DWord;
  xTp: TTokenPrivileges;
  xUsername, xPassword, xDomain: PChar;
  xRet : Boolean;
begin
  Result := True;
  SetLastError(0);

   {
    //Token von aktiven Process ermitteln (LogonUser)
    if OpenProcessToken(GetCurrentProcess,
                        TOKEN_ADJUST_PRIVILEGES or TOKEN_IMPERSONATE,
                        xToken) then begin

       // luid = locally unique identifier
       // luid von Polices 'Part of os' ermitteln
       xRet := LookupPrivilegeValue( NIL,
                                    SE_TCB_NAME,
                                    xTp.Privileges[0].luid);

       xTp.privilegecount := 1;
       xTp.privileges[0].Attributes :=  SE_PRIVILEGE_ENABLED;
       xBufLen:=sizeof(xTp);


       //Privileg SE_TCB_NAME aktivieren
       //-> der LogonUser muss SE_TCB_NAME besitzen, um AdjustTokenPrivileges
       //   auszufuehren.
       //-> Also, der LogonUser muss in der MMUsergroup deklariert sein.
       AdjustTokenPrivileges(xToken,
                             FALSE,
                             xTp,
                             0,
                             NIL,
                             xBufLen);

   end else  xRet := FALSE ;
   }

  xUsername := 'MMSystemUser';
  xPassword := 'tub5zoe';
  xDomain := '';

  xRet := FALSE;
  xRet := LogonUser(xUsername, xDomain, xPassword, LOGON32_LOGON_INTERACTIVE,
                    LOGON32_PROVIDER_DEFAULT, xUserToken);

  if not xRet then begin
     //Neues PWD ab MM-Vers. 4.2
     xPassword := 'Sup5erMM';
     xRet := LogonUser(xUsername, xDomain, xPassword, LOGON32_LOGON_INTERACTIVE,
                       LOGON32_PROVIDER_DEFAULT, xUserToken);
  end;

  //Der LogonUser muss bereits das Privileg SE_TCB_NAME besitzen !!!
//  if LogonUser(xUsername, xDomain, xPassword, LOGON32_LOGON_INTERACTIVE,
//               LOGON32_PROVIDER_DEFAULT, xUserToken) then begin

  if xRet then begin
    Result := ImpersonateLoggedOnUser(xUserToken);
    CloseHandle(xUserToken);
  end else
    Result := False;
end; // TFMMClientMain.SetServicePrivileg cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.ShowStartMode(aText: string);
begin
  StatusBar.Panels.Items[1].Text := aText;
  StatusBar.Font.Style           := StatusBar.Font.Style + [fsBold];
end; // TFMMClientMain.ShowStartMode cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.ShowStatusText(aText: string);
begin
  StatusBar.Panels.Items[0].Text := aText;
  StatusBar.Font.Style           := StatusBar.Font.Style + [fsBold];
end; // TFMMClientMain.ShowStatusText cat:Undefined

//:-------------------------------------------------------------------
function TFMMClientMain.StartAutoPrintServcie(aService: string): Boolean;
var
  xService, xSCManager: THandle;
  xStatus: TServiceStatus;
  xTime: TTime;
  xServiceArgVectors: PChar;
  xError: Integer;
  xMsg: string;
begin
  Result := False;

  SetServicePrivileg;

  SetLastError(0);

  xSCManager := OpenSCManager(nil, nil, SC_MANAGER_CONNECT);
  xService := OpenService(xSCManager, PChar(aService), SERVICE_START or SERVICE_QUERY_STATUS);

  if xService <= 0 then begin
    xError := GetLastError;
    xMsg := 'StartAutoPrintServcie( ' + aService + ' ). Error msg: ' +
      FormatErrorText(xError);
    CodeSite.SendError(xMsg);

    xMsg := 'StartAutoPrintServcie( ' + aService + ' ). ' + #10#13 +
      'Error msg: ' + FormatErrorText(xError);
    MessageDlg(xMsg, mtError, [mbOk], 0);

    Exit;
  end;

  xServiceArgVectors := '';
  Result := StartService(xService, 0, xServiceArgVectors);

  //Warten bis Service laeuft
  xTime := Now + 5 * (1 / (3600 * 24)); //5 Sec. warten

  if Result then
    repeat
      QueryServiceStatus(xService, xStatus);
      Application.ProcessMessages;
    until (xStatus.dwCurrentState = SERVICE_RUNNING) or (Now >= (xTime));

  if xStatus.dwCurrentState = SERVICE_RUNNING then
    Result := True;

  CloseServiceHandle(xSCManager);
  CloseServiceHandle(xService);

  //wss: dieser aufruf w�re nur n�tig, wenn in SetServicePrivileg die API Funktion
  // ImpersonateSelf verwendet wurde...
  RevertToSelf; //Impersonates Token zurueck geben
end; // TFMMClientMain.StartAutoPrintServcie cat:Undefined

//:-------------------------------------------------------------------
function TFMMClientMain.StopAutoPrintServcie(aService: string): Boolean;
var
  xRet: Boolean;
  xNumSec: Integer;
  xStatus: TServiceStatus;
  xService, xSCManager: THandle;
  xError: Integer;
  xMsg: string;
  xTime: TTime;
begin
  xRet := False;

  SetServicePrivileg;
  try
    xSCManager := OpenSCManager(nil, nil, SC_MANAGER_CONNECT);
    xService := OpenService(xSCManager,
      PChar(aService),
      SERVICE_STOP or SERVICE_QUERY_STATUS);

    if xService <= 0 then begin
      xError := GetLastError;
      xMsg := 'StopAutoPrintServcie( ' + aService + ' ). Error msg: ' +
        FormatErrorText(xError);
      CodeSite.SendError(xMsg);

      xMsg := 'StopAutoPrintServcie( ' + aService + ' ). ' + #10#13 +
        'Error msg: ' + FormatErrorText(xError);
      MessageDlg(xMsg, mtError, [mbOk], 0);
      exit;
    end;

    if ControlService(xService, SERVICE_CONTROL_STOP, xStatus) then begin
       //Warten bis Service stopt
      xTime := Now + 5 * (1 / (3600 * 24)); //5 Sec. warten
      repeat
        Application.ProcessMessages;
        if not QueryServiceStatus(xService, xStatus) then
          break;
      until (xStatus.dwCurrentState = SERVICE_STOPPED) or (Now >= (xTime));
    end;

    if xStatus.dwCurrentState = SERVICE_STOPPED then
      xRet := True;

    CloseServiceHandle(xSCManager);
    CloseServiceHandle(xService);
  finally
    RevertToSelf; //Impersonates Token zurueck geben
  end; // try finally

  Result := xRet;
end; // TFMMClientMain.StopAutoPrintServcie cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.timTimeOutResetTimer(Sender: TObject);
begin
  mInAquisation := False;
  ResetTrayIcon;
  mLog.Write(etWarning, 'Timeout for "end of aquisation". Trayicon reset.');
end; // TFMMClientMain.timTimeOutResetTimer cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.timUpdateFloorTimer(Sender: TObject);
begin
  timUpdateFloor.Enabled := False;
  MMBroadCastMessage(mMessageApplID, cMMRefresh);
end; // TFMMClientMain.timUpdateFloorTimer cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.WndProc(var Message: TMessage);
var
  xPChar: PChar;
  xStr: string;
  xAutoprintWasRunning: Boolean;
begin
  try
    // NT-Logoff oder Shutdown
    case (Message.Msg) of
      WM_QUERYENDSESSION,
        WM_ENDSESSION: begin
          MMLogin.Logout;
          mBroadcastThread.Terminate;
        end;
      WM_ShowClientMsg: begin
          xStr := Format(cMMClientMessage + cCRLF + cCRLF + cQOfflimitAlarm, [DateTimeToStr(Now)]);
          if GetAlarmSettings then begin
            with TfrmPopup.Create(nil) do
              Popup(cMMMessage, xStr);
          end;
        end;
      WM_UpdateFloor: begin
          CodeSite.SendMsg('WM_UpdateFloor received');
          if (GetTickCount - mUpdateFloorTimeStamp) > timUpdateFloor.Interval then
            // wenn dies die erste Meldung nach langer Zeit ist, dann Floor direkt aktuallisieren
            timUpdateFloorTimer(nil)
          else
            // bei Wiederholungen innerhalb von ca 10 Sekunden den Timer neustarten
            timUpdateFloor.Restart;
          mUpdateFloorTimeStamp := GetTickCount;
        end;
    else
    end; // End case (Message.Msg) of

    // Close MM-Client
    if Message.Msg = mMMClientID then begin
      case (Message.WParam) of
        WM_MMClientClose: begin
            MessageBeep(MB_ICONASTERISK);
            Application.Terminate;
          end;
        WM_MMClientMinimize: Application.Minimize;
      end;
    // Meldungen vom Broadcast Reader Thread, welche ueber die Mailslot einfliessen
    end
    else if Message.Msg = mMessageApplID then begin
      case (Message.WParam) of
        // User-Privileg hat gewechselt
        cMsgLoginChanged: begin
            MMSecurityControl.Refresh;
            CheckLogonUser;
            // perform only a Icon change in tray
            SetMMState(mMMState);
          end;

        cMMAlert: begin
            ShowStatusText(rsMsgMMAlarm);
            SetMMState(msAlarm);
          end;

        cMMStarted: begin
            if mMMIsNewStarting then begin
              mMMIsNewStarting := False;
              GetAutoPrintStatus;  // Setzt Flag in mAutoprintIsRunning
              if mAutoprintIsRunning then begin
                acAutoprintStop.Execute;
                Sleep(1000);
                Application.ProcessMessages;
                acAutoprintStart.Execute;
              end;
            end;
            SendMSGToJobHandler;
            ShowStatusText(rsMsgMMStarted);
            SetMMState(msStarted);
          end;

        cMMStopped: begin
            ShowStatusText(rsMsgStopped);
            acAutoprintStop.Execute;
            ResetTrayIcon;
            SetMMState(msStopped);
          end;

        cMMStarting: begin
            mMMIsNewStarting := True;
            ShowStatusText(rsMsgMMStarting);
            SetMMState(msWaiting);
          end;

        cMMStopping: begin
            ShowStatusText(rsMsgStopping);
            acAutoprintStop.Execute;
            ResetTrayIcon;
            SetMMState(msWaiting);
          end;

        // Naechstes Intervall, Schichtwechsel
        cMMSystemInfo: begin
            if gSystemMsg.NextInterval > 0 then
              laNextIntervallValue.Caption := Format('%s',
                [DateTimeToStr(gSystemMsg.NextInterval)])
            else
              laNextIntervallValue.Caption := cUnknown;

            if gSystemMsg.NextShift > 0 then
              laNextShiftValue.Caption := Format('%s',
                [DateTimeToStr(gSystemMsg.NextShift)])
            else
              laNextShiftValue.Caption := cUnknown;
          end;

        //DataAquisation
        cMMAquisation: begin
            mInAquisation := (Message.LParam > 0);
            FillTrayIcon(mInAquisation);
          end;

      else
      end; // End case (Message.WParam) of

      if gMMGuardMessage.UserStart then
        ShowStartMode(rsMsgMMUserStart)
      else
        ShowStartMode(rsMsgMMAutoStart);

      if gMMGuardMessage.Date > 1 then begin
        laTime.caption := TimeToStr(gMMGuardMessage.Date);
        laDate.caption := DateToStr(gMMGuardMessage.Date);
      end
      else begin
        laTime.caption := cUnknown;
        laDate.caption := cUnknown;
      end;
    end
    else //END if Message.Msg = mMessageApplID
      inherited WndProc(Message);

  except
    on E: Exception do
      mLog.Write(etError, Format('MMClient has an error in WndProc(). Msg = %d, WParam = %d. Error : %s.', [Message.Msg, Message.WParam, E.Message]));
  end;

end; // TFMMClientMain.WndProc cat:Undefined

//:-------------------------------------------------------------------
procedure TFMMClientMain.WriteUserDataInRegistry;
var
  xUserGroups: TStringlist;
begin
  if gMMHost = '' then begin
    MessageDlg(rsMsgNoMMHost, mtError, [mbOk], 0);
    exit;
  end;

  xUserGroups := TStringlist.Create;
  xUserGroups.Duplicates := dupIgnore;

  if CodeSite.Enabled then begin
    // wss: get user groups from local computer instead of local groups of server
    GetUserLocalGroupName(mNTUser, '', xUserGroups);
    CodeSite.SendStringList('Local User groups', xUserGroups);
    xUserGroups.Clear;

    GetUserGroupName(mNTUser, gMMHost, xUserGroups);
    CodeSite.SendStringList('gMMHost User groups', xUserGroups);
    xUserGroups.Clear;
    // Usergruppen vom DomainController  -> TStringList;
    GetUserGroupName(mNTUser, gDomainController, xUserGroups);
    CodeSite.SendStringList('gDomainController User groups', xUserGroups);
    xUserGroups.Clear;
    CodeSite.AddSeparator;
  end;

  // wss: get user groups from local computer instead of local groups of server
  GetUserLocalGroupName(mNTUser, '', xUserGroups);
  // Usergruppen auf MM-Server  -> TStringList;
  GetUserGroupName(mNTUser, gMMHost, xUserGroups);
  // Usergruppen vom DomainController  -> TStringList;
  GetUserGroupName(mNTUser, gDomainController, xUserGroups);

  with TmmRegistry.Create do
  try
    //Eintrag in Registry
    RootKey := cRegCU;
    OpenKey(cRegMMSecurityPath, True);

    WriteString(cRegDefUserName, mNTUser);
    WriteString(cRegDefUserGroups, xUserGroups.CommaText);
    WriteString(cRegDefLogonTime, DateTimeTostr(Now));
  finally
    Free;
  end;

  xUserGroups.Free;
end; // TFMMClientMain.WriteUserDataInRegistry cat:Undefined
//------------------------------------------------------------------------------
procedure TFMMClientMain.mActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  Handled := True;

  StatusBar.Panels[2].Enabled := (mBarcoAutoPrintServiceName <> '');
  acAutoprintStart.Visible := (mBarcoAutoPrintServiceName <> '');
  acAutoprintStop.Visible := (mBarcoAutoPrintServiceName <> '');
end;
//------------------------------------------------------------------------------
initialization
  CodeSite.Enabled := GetRegBoolean(cRegLM, cRegMMDebug, 'MMClient', False);
end.

