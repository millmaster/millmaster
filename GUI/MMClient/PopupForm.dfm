object frmPopup: TfrmPopup
  Left = 284
  Top = 90
  BorderStyle = bsDialog
  BorderWidth = 2
  ClientHeight = 97
  ClientWidth = 361
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object laMsg: TmmLabel
    Left = 0
    Top = 0
    Width = 361
    Height = 13
    Align = alTop
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = True
    WordWrap = True
    AutoLabel.LabelPosition = lpLeft
  end
  object pnBottom: TmmPanel
    Left = 0
    Top = 64
    Width = 361
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object bOK: TmmButton
      Left = 143
      Top = 4
      Width = 75
      Height = 25
      Anchors = [akTop]
      Caption = 'OK'
      Default = True
      TabOrder = 0
      Visible = True
      OnClick = bOKClick
      AutoLabel.LabelPosition = lpLeft
    end
  end
end
