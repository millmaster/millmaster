inherited ConfigMsgForm: TConfigMsgForm
  Left = 281
  Top = 326
  Caption = '(*)Nachrichten Konfiguration'
  ClientHeight = 91
  ClientWidth = 204
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited bOK: TmmButton
    Left = 12
    Top = 61
    OnClick = bOKClick
  end
  inherited bCancel: TmmButton
    Left = 112
    Top = 61
  end
  object mmGroupBox1: TmmGroupBox
    Left = 0
    Top = 0
    Width = 204
    Height = 49
    Align = alTop
    Caption = '(*)Meldungen anzeigen'
    Color = clBtnFace
    ParentColor = False
    TabOrder = 2
    CaptionSpace = True
    object cbQOfflimitMSG: TmmCheckBox
      Left = 10
      Top = 25
      Width = 129
      Height = 17
      Caption = '(*)QOfflimit Alarm'
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MMClientDic'
    Left = 160
    Top = 16
    TargetsData = (
      1
      2
      (
        ''
        'Hint'
        0)
      (
        ''
        'Caption'
        0))
  end
end
