program MMClient;


 // 15.07.2002 added mmMBCS to imported units
uses
  MemCheck,
  mmMBCS,
  Forms,
  LoepfeGlobal,
  BASEDialog in '..\..\..\LoepfeShares\Templates\BASEDIALOG.pas' {mmDialog},
  BASEDialogBottom in '..\..\..\LoepfeShares\Templates\BASEDIALOGBOTTOM.pas' {DialogBottom},
  BaseForm in '..\..\..\LoepfeShares\Templates\BASEFORM.pas' {mmForm},
  Broadcast in 'Broadcast.pas',
  PopupForm in 'PopupForm.pas' {frmPopup},
  MMClientMain in 'MMClientMain.pas' {FMMClientMain},
  u_ConfigMsgForm in 'u_ConfigMsgForm.pas' {ConfigMsgForm},
  u_MMClientText in 'u_MMClientText.pas';

{$R *.RES}
{$R 'Version.res'}

begin
  gApplicationName := 'MMClient';
{$IFDEF MemCheck}
  MemChk(gApplicationName);
{$ENDIF}

  // Programm nur einmal starten
  if not ApplicationExist(Application) then begin
    Application.Initialize;
    Application.CreateForm(TFMMClientMain, FMMClientMain);
  Application.Run;
  end;
end.
