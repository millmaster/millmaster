unit PopupForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Forms, StdCtrls,
  Buttons, mmLabel, ExtCtrls, mmPanel, mmButton, mmcs;

type
  TfrmPopup = class (TForm)
    bOK: TmmButton;
    laMsg: TmmLabel;
    pnBottom: TmmPanel;
    procedure bOKClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  public
    procedure Popup(aTitle, aMsg: String);
  end;
  
var
  frmPopup: TfrmPopup;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
{$R *.DFM}
const
  cMsgLabelHeight = 64;

//:-------------------------------------------------------------------
procedure TfrmPopup.bOKClick(Sender: TObject);
begin
  Close;
end;// TfrmPopup.bOKClick cat:Undefined

//:-------------------------------------------------------------------
procedure TfrmPopup.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;// TfrmPopup.FormClose cat:Undefined

//:-------------------------------------------------------------------
procedure TfrmPopup.Popup(aTitle, aMsg: String);
begin
  Caption := aTitle;
  laMsg.Caption := aMsg;
  
  if laMsg.Height > cMsgLabelHeight then
    Height := Height + (laMsg.Height - cMsgLabelHeight);
  try
    Beep;
    Position := poScreenCenter;
    Show;
  except
  end;
end;// TfrmPopup.Popup cat:Undefined

end.

