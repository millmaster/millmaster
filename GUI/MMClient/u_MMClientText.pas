(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MMControl.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Hauptprogramm und Form vom MillMaster-Guard
| Info..........: im Broadcast (Thread) werden die empfangenen Meldungen
|                 ausgewertrt
|                 MMClient ist nur sichtbar, wenn in Reg. DefaultUserRight ein
|                 Wert eingetragen ist.
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 15.04.2002  0.00  Wss | cProtMSg0 entfernt, da Text nur fuer Protokoll
|=============================================================================*)
unit u_MMClientText;

interface

resourcestring
  cMMClientMessage = '(*)Meldung vom MillMaster am %s'; // ivlm
  cMMMessage       = '(*)MillMaster Nachricht'; // ivlm

  // Dialog-Meldung
  //cMsgNoMMGuard = '(*)Keine Verbindung zum MillMaster System moeglich'; //ivlm
  rsMsgNoMMHost  = '(*)MillMaster Server nicht gefunden';  //ivlm

  rsMsgNoDebugConfig = '(*)DebugConfig.exe nicht gefunden'; //ivlm

  // Protokoll-Meldung
//  cProtMSg0 =  '(25)Kein Broadcast-Thread'; //

  // Labeltext
  rsMsgMMStarted      = '(30)MillMaster ist gestartet'; //ivlm
  rsMsgMMStarting     = '(30)MillMaster wird gestartet'; //ivlm
  rsMsgStopped        = '(30)MillMaster ist gestoppt'; //ivlm
  rsMsgStopping       = '(30)MillMaster wird gestoppt'; //ivlm
  rsMsgMMAlarm        = '(30)MillMaster Alarm'; //ivlm
  rsMsgMMUserStart    = '(30)Benutzer start'; //ivlm
  rsMsgMMAutoStart    = '(30)Auto start'; //ivlm
  rsMsgMMNotConnected = '(*)Keine Verbindung zum MillMaster System moeglich'; //ivlm
  cUnknown           = '(15)Unbekannt';//ivlm

  //AlarmText
  cQOfflimitAlarm    = '(*)Qualitaets Offlimit Alarm'; //ivlm


  //Autoprint service
  rsAutoprintServiceNotExists = '(*)Autoprint Service ist nicht installiert'; //ivlm
  rsAutoprintServiceDisabled  = '(*)Autoprint Service ist inaktiv';          //ivlm
  rsAutoprint_STOPPED         = '(*)Autoprint Service ist gestoppt';         //ivlm
  rsAutoprint_START_PENDING   = '(*)Autoprint Service wird gestartet';       //ivlm
  rsAutoprint_STOP_PENDING    = '(*)Autoprint Service wird gestoppt';        //ivlm
  rsAutoprint_RUNNING         = '(*)Autoprint Service ist gestartet';        //ivlm
  rsAutoprint_PAUSED          = '(*)Autoprint Service pausiert';             //ivlm
  rsAutoprint_ACCESS_DENIED   = '(*)Keine Zugriffserlaubnis auf den Autoprint Service'; //ivlm

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
end.
