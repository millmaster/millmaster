(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: Broadcast.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Liest und wertet die Meldungen vom MMGuard aus. Zudem werden
|                 Meldungen an die Applikationen weiter gegeben.
| Info..........:
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 05.10.1998  0.00  Sdo | Projekt erstellt
|                       |
| 10.03.2000  0.01  Sdo | neue Procedure CloseMailslot: schliesst Mailslot
| 15.03.2001  0.01  Wss | amQOfflimit implemented in read thread to show messages
| 22.03.2001  0.01  Wss | Show QOfflimit message over PopupMessage function in LoepfeGlobal
| 03.05.2001  0.01  Wss | PopupMessage solved with own window because
| 06.06.2001  0.02  SDo | Reading Q-Offlimit MSG config to amQOfflimit added
| 15.10.2002  1.07  Wss | Floor wird nun per Timer updated. Update message geht
                          nun erst an Hauptapplikation, wo ein Timer gestartet wird.
| 15.11.2002  1.41  Wss | Enumeration amMaAssChanged -> amMachineStateChange
| 26.11.2002        LOK | CodeSite Meldungen aus Execute entfernt:
|                       | ==> Die Zeile
|                       |      CodeSite.SendString('1.BroadcastReader message: ', GetEnumName(TypeInfo(TMMClientCommand), ord(xMsg.MsgTyp)));
|                       |     kann Probleme bereiten, wenn aus irgend einem Grund xMsg.MsgTyp keinen g�ltigen Wert
|                       |     beinhaltet. Die Funktion 'GetEnumName' wirft dann eine Exception. Da dies im Thread geschieht,
|                       |     verabschiedet sich die Applikation per Dr. Watson.
|=============================================================================*)


// Achtung : Der TMailSlotReader erhaelt alternierend die Fehlermeldung
//           122 (Insufficient_Buffer). Also fuer jede Anfrage an den MMGuard
//           kommt zuerst eine Fehlermeldung und dann die korrekte Meldung.
//           -> Eintrag in Protokoll

unit Broadcast;

interface

uses
  Classes, Windows, SysUtils, Messages, Forms, Controls,
  Mailslot, BaseGlobal, mmEventLog, MMMessages, BaseForm, LoepfeGlobal, mmcs;

type
  TMMGuardMessage = record
      Date : TDateTime;
      //Msg  : TApplMsg;//Word;
      Msg  : TApplMsgRec;//Word;
      UserStart : Boolean;
  end;

  TSystemMsg = record
      NextInterval : TDateTime;
      NextShift    : TDateTime;
  end;


  TPopupWindow = class (TWinControl)
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public
    constructor Create(aOwner: TComponent); override;
    procedure ShowPopup(aMsg: String);
  end;
  
  TBroadCastReader = class (TThread)
  private
    fError: String;
    fMessageApplID: Word;
    //fShowOfflimitAlarm: Boolean;
    mHint: TPopupWindow;
    mLog: TEventLogWriter;
    mParentMMControlHandle: THandle;
    mReader: TMailSlotReader;
  protected
    procedure Execute; override;
  public
    property Error: String read fError;
    property MessageApplID: Word read FMessageApplID write FMessageApplID;
    //property ShowOfflimitAlarm: Boolean read fShowOfflimitAlarm write fShowOfflimitAlarm;
    constructor Create(aHandle: THandle); virtual;
    destructor Destroy; override;
    function Init: Boolean;
  end;
  
var gMMGuardMessage : TMMGuardMessage;
    //gLastState: TMMClientCommand;
    gSystemMsg : TSystemMsg;
//    gIsAquisationWorking : Boolean;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  RzCSIntf, Dialogs, mmDialogs, NTApis, PopupForm, u_MMClientText, IvDictio;

//:-------------------------------------------------------------------
constructor TPopupWindow.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  SetBounds(0, 0, 200, 100);
end;// TPopupWindow.Create cat:Undefined

//:-------------------------------------------------------------------
procedure TPopupWindow.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  with Params do
  begin
    Style := WS_POPUP or WS_BORDER;
    WindowClass.Style := WindowClass.Style or CS_SAVEBITS;
    if NewStyleControls then ExStyle := WS_EX_TOOLWINDOW;
    AddBiDiModeExStyle(ExStyle);
  end;
end;// TPopupWindow.CreateParams cat:Undefined

//:-------------------------------------------------------------------
procedure TPopupWindow.ShowPopup(aMsg: String);
var
  xRect: TRect;
begin
  try
    xRect := Rect(0, 0, 200, 100);
    Caption := aMsg;
  {
      Inc(Rect.Bottom, 4);
      UpdateBoundsRect(Rect);
      if Rect.Top + Height > Screen.DesktopHeight then
        Rect.Top := Screen.DesktopHeight - Height;
      if Rect.Left + Width > Screen.DesktopWidth then
        Rect.Left := Screen.DesktopWidth - Width;
      if Rect.Left < Screen.DesktopLeft then Rect.Left := Screen.DesktopLeft;
      if Rect.Bottom < Screen.DesktopTop then Rect.Bottom := Screen.DesktopTop;
  {}
    SetWindowPos(Handle, HWND_TOPMOST, xRect.Left, xRect.Top, Width, Height,
      SWP_SHOWWINDOW or SWP_NOACTIVATE);
    Invalidate;
  finally
  end;
end;// TPopupWindow.ShowPopup cat:Undefined

//:-------------------------------------------------------------------
constructor TBroadCastReader.Create(aHandle: THandle);
begin
  inherited Create(TRUE);

  // owner let free this thread
  FreeOnTerminate := False;

  mParentMMControlHandle:= aHandle;
  mReader := TMailSlotReader.Create ( cChannelNames [ ttMMClientReader ], -1 );
  mLog := TEventLogWriter.Create(cEventLogClassForSubSystems,
                                 gMMHost,
                                 ssMMClient,
                                 cEventLogDefaultText[ttMMClientReader],
                                 True);

  mHint := TPopupWindow.Create(Nil);

  fError             := '';
  //fShowOfflimitAlarm := FALSE;
end;// TBroadCastReader.Create cat:Undefined

//:-------------------------------------------------------------------
destructor TBroadCastReader.Destroy;
begin
  mLog.Free;
  mReader.Timeout := 0;
  mReader.Free;
  mHint.Free;
  inherited Destroy;
end;// TBroadCastReader.Destroy cat:Undefined

//:-------------------------------------------------------------------
procedure TBroadCastReader.Execute;
var
  xMsg: TMMClientRec;
  xSizeRead: Cardinal;
  xPChar: PChar;
  i: Integer;
  xStr: String;
begin
  // Thread
  while not Terminated do begin
    SetLastError(0);
    if mReader.Read ( @xMsg, sizeof ( TMMClientRec ), xSizeRead ) then begin
      if ord(xMsg.MsgTyp) > ord(ccJobSent) then
        CodeSite.SendIntegerEx(csmCheckpoint,'1.BroadcastReader message: ', ord(xMsg.MsgTyp));

      // is Message from the right Host
//      if CompareText(xMsg.ServerName, gMMHost) = 0 then begin
      if AnsiSameText(xMsg.ServerName, gMMHost) then begin
        gMMGuardMessage.Msg := xMsg.ApplMsg;
        case xMsg.MsgTyp of
          ccMMStarted  : begin
              CodeSite.SendString('MMClient broadcast msg', 'ccMMStarted');
              // Meldung an MMClient
              try
                 gMMGuardMessage.Date      := xMsg.DateTime;
                 gMMGuardMessage.UserStart := xMsg.UserStart;
              except
              end;
                // Meldung an Appl.
              MMBroadCastMessage(FMessageApplID, cMMStarted);
            end;

          ccMMStarting : begin
              CodeSite.SendString('MMClient broadcast msg', 'ccMMStarting');
              gMMGuardMessage.UserStart := xMsg.UserStart;
              MMBroadCastMessage(FMessageApplID, cMMStarting);
            end;

          ccMMStopped, ccMMServiceStopped :  begin
              CodeSite.SendString('MMClient broadcast msg', 'ccMMStopped, ccMMServiceStopped');
              gMMGuardMessage.Date      := xMsg.DateTime;
              gMMGuardMessage.UserStart := xMsg.UserStart;
              MMBroadCastMessage(FMessageApplID, cMMStopped);
            end;

          ccMMStopping : begin
              CodeSite.SendString('MMClient broadcast msg', 'ccMMStopping');
              gMMGuardMessage.UserStart := xMsg.UserStart;
              MMBroadCastMessage(FMessageApplID, cMMStopping);
            end;

          ccMMAlert :    begin
              CodeSite.SendString('MMClient broadcast msg', 'ccMMAlert');
              gMMGuardMessage.Date      := xMsg.DateTime;
              gMMGuardMessage.UserStart := xMsg.UserStart;
              MMBroadCastMessage(FMessageApplID, cMMAlert);
            end;

          ccMMApplMsg :  begin
              CodeSite.SendString('MMClient broadcast msg', 'ccMMApplMsg');
              //case xMsg.ApplMsg of
              case xMsg.ApplMsg.ApplMsgEvent of
                amQOfflimit: begin
                    CodeSite.SendString('  ApplMsg', 'amQOfflimit');

                    // Q-Offlimit Alarm MSG setting aus Reg. lesen
 //                 if fShowOfflimitAlarm then
                    PostMessage(mParentMMControlHandle, WM_ShowClientMsg, 0, 0);
                  end;

                amStartAquisation : begin
                    CodeSite.SendString('  ApplMsg', 'amStartAquisation');
                    MMBroadCastMessage(FMessageApplID, cMMAquisation, 1);
                  end;
                amEndAquisation   : begin
                CodeSite.SendString('  ApplMsg', 'amEndAquisation');
                                      MMBroadCastMessage(FMessageApplID, cMMAquisation, 0);
                  end;
                amSystemInfo      : begin
                    CodeSite.SendString('  ApplMsg', 'amSystemInfo');
                    gSystemMsg.NextInterval := xMsg.ApplMsg.NextInterval;
                    gSystemMsg.NextShift    := xMsg.ApplMsg.NextShift;
                    MMBroadCastMessage(FMessageApplID, cMMSystemInfo);
                  end;
                amMachineStateChange: begin
                    CodeSite.SendString('  ApplMsg', 'amMachineStateChange');
                    PostMessage(mParentMMControlHandle, WM_UpdateFloor, 0, 0);
                  end;
              else
              end;
            end;
  
        else
        end; // Case
      end; // if CompareText
    end; // if mReader
  end;
end;// TBroadCastReader.Execute cat:Undefined

//:-------------------------------------------------------------------
function TBroadCastReader.Init: Boolean;
begin
  Result := mReader.Init;
  if Result then
    Resume
  else
    fError := FormatErrorText(mReader.Error);
end;// TBroadCastReader.Init cat:Undefined


//******************************************************************************
//BroadCast-Meldungen interpretieren, Anzeigen und an App. weiterleiten
//******************************************************************************
//------------------------------------------------------------------------------
{
function PopupMessage(aComputer, aFrom, aTo, aMessage: String): Boolean;
var
  xRemotePath: String;
  xMsg: String;
  xRemoteHandle: LongWord;
  xLen: DWord;
  xSA: TSECURITYATTRIBUTES;       // Security attributes.
  xpSD: PSECURITY_DESCRIPTOR;      // Pointer to SD.
  xForm: TfrmPopup;
  xHint: THintWindow;
begin
  xMsg := aFrom + #0 + aTo + #0 + aMessage;
  xRemotePath := '\\'+ aComputer + '\mailslot\messngr';
  xRemoteHandle := CreateFile(PChar(xRemotePath), GENERIC_WRITE,
                              FILE_SHARE_READ, Nil, OPEN_EXISTING, // OPEN_ALWAYS ,
                              FILE_ATTRIBUTE_NORMAL, 0 );

  Result := (xRemoteHandle <> INVALID_HANDLE_VALUE);
  if Result then begin
    Result := WriteFile(xRemoteHandle, Pointer(xMsg)^, Length(xMsg), xLen, Nil);
    CloseHandle(xRemoteHandle);
  end;
end;
{}

end.

