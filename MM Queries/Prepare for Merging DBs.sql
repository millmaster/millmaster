/*
--------------------------------------------------------------
-- helper to create update statements for each c_prod_id field

declare @T_TABLE_NAMES table
(
    name nvarchar(128) not null
);

insert into
    @T_TABLE_NAMES
select
    obj.name
from
    sys.COLUMNS col
        inner join sys.objects obj
            on col.object_id = obj.object_id
where
        obj.type = 'U'
    and col.name = 'c_prod_id';
    
select
    'update ' + tbl.name + ' set c_prod_id = c_prod_id + @offset;'
from
    @T_TABLE_NAMES tbl;
    
    
-- create scripts to DROP foreign key releations
--
select
    'ALTER TABLE dbo.' + obj.name + ' DROP CONSTRAINT [' + fk.name + ']'
from
    sys.foreign_keys fk
        inner join sys.objects obj
            on fk.parent_object_id = obj.object_id
        inner join sys.foreign_key_columns fkc
            on fk.object_id = fkc.constraint_object_id
        inner join sys.columns col
            on col.object_id = fkc.referenced_object_id and col.column_id = fkc.referenced_column_id
where
    col.name = 'c_prod_id'  
    
    
-- create scripts to CREATE foreign key releations
--
select
    'ALTER TABLE dbo.' + obj.name + '  WITH NOCHECK ADD FOREIGN KEY([c_prod_id]) REFERENCES dbo.t_prodgroup ([c_prod_id])'
from
    sys.foreign_keys fk
        inner join sys.objects obj
            on fk.parent_object_id = obj.object_id
        inner join sys.foreign_key_columns fkc
            on fk.object_id = fkc.constraint_object_id
        inner join sys.columns col
            on col.object_id = fkc.referenced_object_id and col.column_id = fkc.referenced_column_id
where
    col.name = 'c_prod_id'          
    
*/

-- drop foreign key relations because we're gonna update primary key fields
ALTER TABLE dbo.t_spindle_staff_production DROP CONSTRAINT [FK__t_spindle__c_pro__2BFE89A6];
go
ALTER TABLE dbo.t_staff_except_assign DROP CONSTRAINT [FK__t_staff_e__c_pro__30C33EC3];
go
ALTER TABLE dbo.t_spindle_interval_data DROP CONSTRAINT [FK__t_spindle__c_pro__3864608B];
go
ALTER TABLE dbo.t_dw_prodgroup_shift DROP CONSTRAINT [FK__t_dw_prod__c_pro__5E8A0973];
go
            
declare @offset int;
set @offset = 400000;
    
-- drop foreign key relations because we're gonna update primary key fields
update t_prodgroup set c_prod_id = c_prod_id + @offset;
update t_prodgroup_state set c_prod_id = c_prod_id + @offset;
update t_spindle_staff_production set c_prod_id = c_prod_id + @offset;
update t_staff_except_assign set c_prod_id = c_prod_id + @offset;
update t_spindle_interval_data set c_prod_id = c_prod_id + @offset;
update t_online_view set c_prod_id = c_prod_id + @offset;
update t_longterm_week set c_prod_id = c_prod_id + @offset;
update t_dw_prodgroup_shift set c_prod_id = c_prod_id + @offset;
update t_QO_Involved_Lot set c_prod_id = c_prod_id + @offset;
update t_longterm_tmp set c_prod_id = c_prod_id + @offset;
go
            
ALTER TABLE dbo.t_dw_prodgroup_shift  WITH NOCHECK ADD FOREIGN KEY([c_prod_id]) REFERENCES dbo.t_prodgroup ([c_prod_id]);
go
ALTER TABLE dbo.t_spindle_staff_production  WITH NOCHECK ADD FOREIGN KEY([c_prod_id]) REFERENCES dbo.t_prodgroup ([c_prod_id]);
go
ALTER TABLE dbo.t_staff_except_assign  WITH NOCHECK ADD FOREIGN KEY([c_prod_id]) REFERENCES dbo.t_prodgroup ([c_prod_id]);
go
ALTER TABLE dbo.t_spindle_interval_data  WITH NOCHECK ADD FOREIGN KEY([c_prod_id]) REFERENCES dbo.t_prodgroup ([c_prod_id]);
go            


/* -- helpers to create scripts for adapting t_longterm_week

-- drop constraints from t_longterm_week to xxxx_Cut tables
--
select
    'ALTER TABLE dbo.' + obj.name + ' DROP CONSTRAINT [' + fk.name + ']'
from
    sys.foreign_keys fk
        inner join sys.objects obj
            on fk.parent_object_id = obj.object_id
        inner join sys.foreign_key_columns fkc
            on fk.object_id = fkc.constraint_object_id
        inner join sys.columns col
            on col.object_id = fkc.referenced_object_id and col.column_id = fkc.referenced_column_id
where
    obj.name = 't_longterm_week'  
    
-- create constraints from t_longterm_week to xxxx_Cut tables
--
select
    'ALTER TABLE dbo.t_longterm_week WITH CHECK ADD FOREIGN KEY([' + col.name + ']) REFERENCES [dbo].[' + refobj.name + '] ([' + col.name + '])'
from
    sys.foreign_keys fk
        inner join sys.objects obj
            on fk.parent_object_id = obj.object_id
        inner join sys.foreign_key_columns fkc
            on fk.object_id = fkc.constraint_object_id
        inner join sys.columns col
            on col.object_id = fkc.referenced_object_id and col.column_id = fkc.referenced_column_id
        inner join sys.objects refobj
            on refobj.object_id = fkc.referenced_object_id
where
    obj.name = 't_longterm_week'  
    

*/  

-- drop constraints from t_longterm_week to xxxx_Cut tables
--
ALTER TABLE dbo.t_longterm_week DROP CONSTRAINT [FK__t_longter__c_cla__477199F1];
go
ALTER TABLE dbo.t_longterm_week DROP CONSTRAINT [FK__t_longter__c_cla__4865BE2A];
go
ALTER TABLE dbo.t_longterm_week DROP CONSTRAINT [FK__t_longter__c_spl__4959E263];
go
ALTER TABLE dbo.t_longterm_week DROP CONSTRAINT [FK__t_longter__c_spl__4A4E069C];
go
ALTER TABLE dbo.t_longterm_week DROP CONSTRAINT [FK__t_longter__c_sir__4B422AD5];
go
ALTER TABLE dbo.t_longterm_week DROP CONSTRAINT [FK__t_longter__c_sir__4C364F0E];
go

-- update FK fields to xxxx_Cut tables
--
declare @offset int;
set @offset = 400000;

update
    dbo.t_longterm_week
set
    c_classCut_id = c_classCut_id + @offset,
    c_classUncut_id = c_classUncut_id + @offset,
    c_siroCut_id = c_siroCut_id + @offset,
    c_siroUncut_id = c_siroUncut_id + @offset,
    c_spliceCut_id = c_spliceCut_id + @offset,
    c_spliceUncut_id = c_spliceUncut_id + @offset;
    
update dbo.t_longterm_classCut
set c_classCut_id =  c_classCut_id +@offset;       

update dbo.t_longterm_classUncut
set c_classUncut_id =  c_classUncut_id +@offset;       

update dbo.t_longterm_siroCut
set c_siroCut_id =  c_siroCut_id +@offset;       

update dbo.t_longterm_siroUncut
set c_siroUncut_id =  c_siroUncut_id +@offset;       

update dbo.t_longterm_spliceCut
set c_spliceCut_id =  c_spliceCut_id +@offset;       

update dbo.t_longterm_spliceUncut
set c_spliceUncut_id =  c_spliceUncut_id +@offset;       

-- create constraints from t_longterm_week to xxxx_Cut tables
--
ALTER TABLE dbo.t_longterm_week WITH CHECK ADD FOREIGN KEY([c_classCut_id]) REFERENCES [dbo].[t_longterm_classCut] ([c_classCut_id]);
go
ALTER TABLE dbo.t_longterm_week WITH CHECK ADD FOREIGN KEY([c_classUncut_id]) REFERENCES [dbo].[t_longterm_classUncut] ([c_classUncut_id]);
go
ALTER TABLE dbo.t_longterm_week WITH CHECK ADD FOREIGN KEY([c_spliceCut_id]) REFERENCES [dbo].[t_longterm_spliceCut] ([c_spliceCut_id]);
go
ALTER TABLE dbo.t_longterm_week WITH CHECK ADD FOREIGN KEY([c_spliceUncut_id]) REFERENCES [dbo].[t_longterm_spliceUncut] ([c_spliceUncut_id]);
go
ALTER TABLE dbo.t_longterm_week WITH CHECK ADD FOREIGN KEY([c_siroCut_id]) REFERENCES [dbo].[t_longterm_siroCut] ([c_siroCut_id]);
go
ALTER TABLE dbo.t_longterm_week WITH CHECK ADD FOREIGN KEY([c_siroUncut_id]) REFERENCES [dbo].[t_longterm_siroUncut] ([c_siroUncut_id]);
go


-- AFTER MERGE DATA
-- -> update machine id's via name lookup
--

;with Machines as (
    select
        tlw.c_shift_start,
        tlw.c_prod_id,
        tlw.c_machine_name,
        coalesce(tm.c_machine_id, 0 - tlw.c_machine_id) as c_machine_id
    from
        dbo.t_longterm_week tlw
            left outer join dbo.t_machine tm on tlw.c_machine_name = tm.c_machine_name)

update
    dbo.t_longterm_week
set
    c_machine_id = mcs.c_machine_id
from
    dbo.t_longterm_week tlw
        inner join Machines mcs on
                tlw.c_shift_start = mcs.c_shift_start
            and tlw.c_prod_id = mcs.c_prod_id
            and tlw.c_machine_name = mcs.c_machine_name;
